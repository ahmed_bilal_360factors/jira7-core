package com.atlassian.jira.permission;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.OperationContext;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.project.Project;
import com.opensymphony.workflow.loader.ActionDescriptor;
import org.ofbiz.core.entity.GenericValue;

/**
 * This class is used to generate {@link PermissionContext} for the given parameters.
 */
public interface PermissionContextFactory {
    /**
     * @deprecated Use {@link #getPermissionContext(Issue)} or {@link #getPermissionContext(Project)} instead. Since v5.0.
     */
    PermissionContext getPermissionContext(GenericValue projectOrIssue);

    /**
     * Get the permission context for the given issue.
     *
     * @param issue The issue for which the permission context is to be generated.
     * @return the context that should be used for permission checking.
     */
    PermissionContext getPermissionContext(Issue issue);

    /**
     * Get the permission context for the given issue at the end of the given workflow action.
     *
     * @param issue             The issue for which the permission context is to be generated.
     * @param actionDescriptor  The workflow action that is occurring.
     * @return the context that should be used for permission checking.
     */
    PermissionContext getPermissionContext(Issue issue, ActionDescriptor actionDescriptor);

    /**
     * Get the permission context for the given issue if it was in the given status.
     *
     * @param issue     The issue for which the permission context is to be generated.
     * @param status    The status that we pretend the issue is in for checking permissions.
     * @return the context that should be used for permission checking.
     * @since v7.0.3
     */
    PermissionContext getPermissionContext(Issue issue, Status status);

    /**
     * Get the permission context for the given project.
     *
     * @param project The project for which the permission context is to be generated.
     * @return the context that should be used for permission checking.
     */
    PermissionContext getPermissionContext(Project project);

    /**
     * @deprecated Use {@link #getPermissionContext(Issue, ActionDescriptor)} instead. Since v5.0.
     */
    PermissionContext getPermissionContext(OperationContext operationContext, Issue issue);
}
