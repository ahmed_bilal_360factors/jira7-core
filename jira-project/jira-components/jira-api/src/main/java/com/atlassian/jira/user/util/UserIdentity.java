package com.atlassian.jira.user.util;

import com.atlassian.annotations.PublicApi;
import com.google.common.base.Objects;

import javax.annotation.concurrent.Immutable;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Objects of this class contains three fields that identify a user in the JIRA database: id, key, username.
 * <p>
 * All three fields are always not null.
 * </p>
 *
 * @since v6.5
 */
@Immutable
@PublicApi
public final class UserIdentity {
    private final Long id;
    private final String key;
    private final String username;

    private UserIdentity(Long id, String key, String username) {
        this.id = checkNotNull(id);
        this.key = checkNotNull(key);
        this.username = checkNotNull(username);
    }

    public Long getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public String getUsername() {
        return username;
    }

    public static BuilderWithId withId(Long id) {
        return new BuilderWithId(id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserIdentity that = (UserIdentity) o;

        return Objects.equal(this.id, that.id) &&
                Objects.equal(this.key, that.key) &&
                Objects.equal(this.username, that.username);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, key, username);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("id", id)
                .add("key", key)
                .add("username", username)
                .toString();
    }

    public static class BuilderWithId {
        private final Long id;

        private BuilderWithId(final Long id) {
            this.id = checkNotNull(id);
        }

        public BuilderWithIdAndKey key(String key) {
            return new BuilderWithIdAndKey(id, key);
        }
    }

    public static class BuilderWithIdAndKey {
        private final Long id;
        private final String key;

        private BuilderWithIdAndKey(final Long id, final String key) {
            this.id = checkNotNull(id);
            this.key = checkNotNull(key);
        }

        public UserIdentity andUsername(String username) {
            return new UserIdentity(id, key, username);
        }
    }
}
