package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.crowd.embedded.api.Group;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import java.util.Set;

/**
 * Represents an Application Role in JIRA. An application role defines which users (indirectly through groups) can
 * access an application.
 * <p>
 * An {@link ApplicationRole} is <em>defined</em> by a JIRA plugin through it's module configuration.
 * An {@code ApplicationRole} is regarded as <em>licensed</em> when there is a (potentially exceeded) license present in
 * JIRA that provides user seats for the {@code ApplicationRole} that is uniquely identified by an {@link ApplicationKey}.
 * </p>
 *
 * @since v7.0
 */
@Immutable
public interface ApplicationRole {
    /**
     * Returns the canonical {@link ApplicationKey} that uniquely identifies this {@link ApplicationRole}.
     *
     * @return the canonical {@link ApplicationKey} that uniquely identifies this {@link ApplicationRole}.
     */
    @Nonnull
    ApplicationKey getKey();

    /**
     * Return the name of the {@code ApplicationRole}. The name is i18ned for the calling user if possible.
     *
     * @return the name of the {@code ApplicationRole} role. The name is i18ned for the calling user if possible.
     */
    @Nonnull
    String getName();

    /**
     * Return the set of groups associated with the role including the default ones.
     *
     * @return the groups associated with the role.
     */
    @Nonnull
    Set<Group> getGroups();

    /**
     * Return the default groups configured for the role.
     *
     * @return Set with the default groups associated with the role, set will be empty if there are no default groups.
     */
    @Nonnull
    Set<Group> getDefaultGroups();

    /**
     * Return a <strong>new</strong> {@code ApplicationRole} with its groups and default groups set to the passed
     * values.
     *
     * @param groups        the groups associated with the role.
     * @param defaultGroups default groups for the role.
     * @return a new {@code ApplicationRole} with its groups and default groups set to the passed arguments.
     * @throws java.lang.IllegalArgumentException if groups or defaultGroups contains null or if defaultGroups is not a
     *                                            subset of groups.
     */
    @Nonnull
    ApplicationRole withGroups(@Nonnull Iterable<Group> groups, @Nonnull Iterable<Group> defaultGroups);

    /**
     * Returns the total number of seats of this {@code ApplicationRole} issued with the licence.
     *
     * @return the total number of seats of this {@code ApplicationRole} issued with the licence.
     * <p>
     * {@see com.atlassian.jira.application.ApplicationRoleManager#getRemainingSeats(ApplicationKey)}
     */
    int getNumberOfSeats();

    /**
     * Determines whether {@code ApplicationRole} should be selected by default on user creation
     *
     * @return {@code true} when {@code ApplicationRole} should be selected by default on user creation
     */
    boolean isSelectedByDefault();

    /**
     * Returns a <strong>new</strong> {@code ApplicationRole} with its selected by default settings set to passed value
     *
     * @param selectedByDefault the new selected by default settings
     * @return a new {@code ApplicationRole} with its selected by default settings set to passed value
     */
    ApplicationRole withSelectedByDefault(boolean selectedByDefault);

    /**
     * Indicates if the Application/Product is defined. This is true when the {@code ApplicationRoleDefinition}
     * identified by the {@link ApplicationKey} is defined. An {@code ApplicationRole} is defined when the JIRA plugin
     * that defines it is installed.
     *
     * @return {@code true} if the {@code ApplicationRole} is defined
     */
    boolean isDefined();

    /**
     * Returns whether the role is part of the core platform.
     *
     * @return true if the application role is part of the core platform, false if it comes from elsewhere such as a
     * plugin.
     */
    boolean isPlatform();
}
