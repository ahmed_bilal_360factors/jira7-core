package com.atlassian.jira.ofbiz;

import com.atlassian.annotations.PublicApi;
import org.ofbiz.core.entity.GenericValue;

import java.sql.Timestamp;

/**
 * The methods in this interface should not be used.
 * They are here for legacy compatibility with GenericValues
 *
 * @deprecated Use the Data object getters instead. Since v5.0.
 */
@PublicApi
public interface OfBizValueWrapper {
    /**
     * Retrieve a String field.
     *
     * @param name the field name
     * @return the value for the given field.
     * @deprecated Use the Data object getters instead. Since v5.0.
     */
    String getString(String name);

    /**
     * Retrieve a timestamp field.
     *
     * @param name the field name
     * @return the value for the given field.
     * @deprecated Use the Data object getters instead. Since v5.0.
     */
    Timestamp getTimestamp(String name);

    /**
     * Retrieve a numeric field.
     *
     * @param name the field name
     * @return the value for the given field.
     * @deprecated Use the Data object getters instead. Since v5.0.
     */
    Long getLong(String name);

    /**
     * Returns a GenericValue representing this object.
     * <p>
     * Prior to JIRA 7.0, this method returned "the backing GenericValue object" - meaning that this object was
     * forced to be mutable (and not thread safe or suitable for caching).
     * As of JIRA 7.0 this is no longer guaranteed to be true.
     * <p>
     * If this object wants to be immutable, then it can choose to return a "detached" instance of GenericValue.
     * In this case mutating the GenericValue will have no effect on the values in this object, and calling
     * {@code getGenericValue()} twice will return two separate GenericValue instances.
     *
     * @return a GenericValue representing the data in this object.
     * @deprecated Use the Data object getters instead. Since v5.0.
     */
    GenericValue getGenericValue();

    /**
     * Persist this object's immediate fields.
     * <p>
     * As of JIRA 7.0 this method is considered optional.
     * If this object is actually immutable, then it will throw {@link UnsupportedOperationException} because there
     * are no possible mutations to store.
     *
     * @deprecated Use the Object's Service or Manager to save values. Since v5.0.
     */
    void store();
}
