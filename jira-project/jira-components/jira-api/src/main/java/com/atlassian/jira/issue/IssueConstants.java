package com.atlassian.jira.issue;

import com.atlassian.annotations.ExperimentalApi;
import com.google.common.base.Function;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;

import java.util.Collection;
import java.util.Comparator;

/**
 * Utility methods for {@link com.atlassian.jira.issue.IssueConstant}s.
 *
 * @since v6.2
 */
@ExperimentalApi
public final class IssueConstants {
    private static Function<IssueConstant, String> GET_ID = new Function<IssueConstant, String>() {
        @Override
        public String apply(final IssueConstant input) {
            return input.getId();
        }
    };
    private static Function<IssueConstant, String> GET_I18N_NAME = new Function<IssueConstant, String>() {
        @Override
        public String apply(final IssueConstant input) {
            return input.getNameTranslation();
        }
    };

    private IssueConstants() {
    }

    /**
     * Return a function that will return the id of the passed {@link com.atlassian.jira.issue.IssueConstant}.
     * <p>
     * The function does not accept null arguments.
     *
     * @return a function that returns the id of the passed {@code IssueConstant}
     */
    public static Function<IssueConstant, String> getId() {
        return GET_ID;
    }

    public static Function<IssueConstant, String> getIdFunc() {
        return GET_ID;
    }

    /**
     * Return a function that will return the translated name of the passed {@link com.atlassian.jira.issue.IssueConstant}.
     * <p>
     * The function does not accept null arguments.
     *
     * @return a function that returns the translated name of the passed {@code IssueConstant}
     */
    public static Function<IssueConstant, String> getTranslatedNameFunc() {
        return GET_I18N_NAME;
    }

    public static Iterable<String> getIds(Iterable<? extends IssueConstant> constants) {
        return Iterables.transform(constants, getId());
    }

    public static Function<IssueConstant, Long> getIdAsLong() {
        return new Function<IssueConstant, Long>() {
            @Override
            public Long apply(final IssueConstant input) {
                return Long.valueOf(input.getId());
            }
        };
    }

    public static Iterable<? extends String> toStringIds(final Collection<? extends IssueConstant> issueConstants) {
        return Iterables.transform(issueConstants, new Function<IssueConstant, String>() {
            @Override
            public String apply(final IssueConstant input) {
                return input != null ? input.getId() : "-1";
            }
        });
    }

    public static Iterable<? extends String> toStringIdsWithoutNulls(final Collection<? extends IssueConstant> issueConstants) {
        return Iterables.transform(Iterables.filter(issueConstants, Predicates.<IssueConstant>notNull()), new Function<IssueConstant, String>() {
            @Override
            public String apply(final IssueConstant input) {
                return input.getId();
            }
        });
    }

    public static Comparator<IssueConstant> getSequenceComparator() {
        return new Comparator<IssueConstant>() {
            @Override
            public int compare(final IssueConstant o1, final IssueConstant o2) {
                if (o1 == null && o2 == null) {
                    return 0;
                }
                if (o2 == null) {
                    return -1;
                }
                if (o1 == null) {
                    return 1;
                }

                Long s1 = o1.getSequence();
                Long s2 = o2.getSequence();

                if (s1 == null && s2 == null) {
                    return 0;
                } else if (s2 == null) {
                    // any value is less than null
                    return -1;
                } else if (s1 == null) {
                    // null is greater than any value
                    return 1;
                } else {
                    return s1.compareTo(s2);
                }
            }
        };
    }

    ;
}
