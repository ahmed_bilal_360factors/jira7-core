package com.atlassian.jira.icon;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.instrumentation.utils.dbc.Assertions;
import com.atlassian.jira.avatar.Avatar;

import javax.annotation.Nonnull;

/**
 * Indicates the type of icon.
 * That is, an entity (eg Project, IssueType, or something custom provided by a plugin) that can have icons stored against it.
 * @since v7.1
 */
@ExperimentalApi
public class IconType {
    /** An icon type for a {@link com.atlassian.jira.project.Project}. */
    public static final IconType PROJECT_ICON_TYPE = new IconType(Avatar.Type.PROJECT.getName());
    /** An icon type for a {@link com.atlassian.jira.issue.issuetype.IssueType}. */
    public static final IconType ISSUE_TYPE_ICON_TYPE = new IconType(Avatar.Type.ISSUETYPE.getName());
    /** An icon type for a {@link com.atlassian.jira.user.ApplicationUser}. */
    public static final IconType USER_ICON_TYPE = new IconType(Avatar.Type.USER.getName());

    private final String key;

    public IconType(@Nonnull String key) {
        Assertions.notNull("key", key);
        this.key = key;
    }

    @Nonnull
    public String getKey() {
        return key;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        IconType iconType = (IconType) o;
        if (!key.equals(iconType.key)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return key.hashCode();
    }

    @Override
    public String toString() {
        return key;
    }

    public static IconType of(String key) {
        return new IconType(key);
    }
}
