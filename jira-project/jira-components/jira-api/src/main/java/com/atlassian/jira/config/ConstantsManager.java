package com.atlassian.jira.config;

import com.atlassian.annotations.PublicApi;
import com.atlassian.fugue.Option;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.issue.IssueConstant;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.priority.Priority;
import com.atlassian.jira.issue.resolution.Resolution;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.lang.Pair;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.List;

/**
 * Manager for issue types, statuses, priorities and resolutions.  This manager is responsible for
 * caching these constants as well as all the usual update, delete, add operations in the database.
 */
@SuppressWarnings("unused")
@PublicApi
public interface ConstantsManager {
    /**
     * Used to retrieve all standard IssueTypes.
     */
    String ALL_STANDARD_ISSUE_TYPES = "-2";
    /**
     * Used to retrieve all sub-task IssueTypes.
     */
    String ALL_SUB_TASK_ISSUE_TYPES = "-3";

    /**
     * Used to retrieve all IssueTypes.
     */
    String ALL_ISSUE_TYPES = "-4";

    enum CONSTANT_TYPE {
        PRIORITY("Priority"),
        STATUS("Status"),
        RESOLUTION("Resolution"),
        ISSUE_TYPE("IssueType");

        private final String type;

        CONSTANT_TYPE(final String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }

        public static CONSTANT_TYPE forType(@Nonnull String type) {
            for (CONSTANT_TYPE o : CONSTANT_TYPE.values()) {
                if (type.equals(o.type)) {
                    return o;
                }
            }
            return null;
        }

        public static CONSTANT_TYPE forTypeCaseInsensitive(@Nonnull String type) {
            for (CONSTANT_TYPE o : CONSTANT_TYPE.values()) {
                if (type.equalsIgnoreCase(o.type)) {
                    return o;
                }
            }
            return null;
        }

    }

    /**
     * Used in the generic {@link #getConstantObject(String, String)} method
     *
     * @deprecated Use {@link CONSTANT_TYPE#PRIORITY}
     */
    String PRIORITY_CONSTANT_TYPE = CONSTANT_TYPE.PRIORITY.getType();

    /**
     * Used in the generic {@link #getConstantObject(String, String)} method
     *
     * @deprecated Use {@link CONSTANT_TYPE#STATUS}
     */
    String STATUS_CONSTANT_TYPE = CONSTANT_TYPE.STATUS.getType();

    /**
     * Used in the generic {@link #getConstantObject(String, String)} method
     *
     * @deprecated Use {@link CONSTANT_TYPE#RESOLUTION}
     */
    String RESOLUTION_CONSTANT_TYPE = CONSTANT_TYPE.RESOLUTION.getType();

    /**
     * Used in the generic {@link #getConstantObject(String, String)} method
     *
     * @deprecated Use {@link CONSTANT_TYPE#ISSUE_TYPE}
     */
    String ISSUE_TYPE_CONSTANT_TYPE = CONSTANT_TYPE.ISSUE_TYPE.getType();

    /**
     * Retrieve all Priorities in JIRA.
     *
     * @return A list of Priority {@link com.atlassian.jira.issue.priority.Priority}s.
     */
    @Nonnull
    Collection<Priority> getPriorities();

    /**
     * Retrieve all Priorities in JIRA.
     *
     * @return A list of {@link Priority}s.
     * @deprecated Since v7.0
     */
    @Deprecated
    @Nonnull
    default Collection<Priority> getPriorityObjects() {
        return getPriorities();
    }

    /**
     * Given a priority ID, this method retrieves that priority.
     *
     * @param id The id of the priority
     * @return A {@link Priority} object.
     */
    Priority getPriorityObject(String id);

    /**
     * Returns the priority Name for a given priority ID.
     *
     * @param id The id of a priority
     * @return The name of the priority with the given ID, or an i18n'd String indicating that
     * no priority is set (e.g. "None") if the ID is null.
     */
    String getPriorityName(String id);

    /**
     * Returns the default  priority configured in JIRA.
     *
     * @return The default priority
     */
    Priority getDefaultPriority();

    /**
     * Returns the default  priority configured in JIRA.
     *
     * @return The default priority.
     * @deprecated Since v7.0
     */
    @Deprecated
    Priority getDefaultPriorityObject();

    /**
     * Reloads all priorities from the DB.
     */
    void refreshPriorities();

    /**
     * Retrieve all Resolutions in JIRA.
     *
     * @return A List of Resolution {@link com.atlassian.jira.issue.resolution.Resolution}s.
     */
    @Nonnull
    Collection<Resolution> getResolutions();

    /**
     * Retrieve all Resolutions in JIRA.
     *
     * @return A List of {@link Resolution} objects.
     * @deprecated Since v7.0
     */
    @Deprecated
    default Collection<Resolution> getResolutionObjects() {
        return getResolutions();
    }

    /**
     * Given a resolution ID, this method retrieves that resolution.
     *
     * @param id The id of the resolution
     * @return A resolution
     */
    Resolution getResolution(String id);

    /**
     * Given a resolution ID, this method retrieves that resolution.
     *
     * @param id The id of the resolution
     * @return A {@link Resolution} object.
     * @deprecated Use {@link #getResolution(String)} instead. Since v7.0
     */
    @Deprecated
    default Resolution getResolutionObject(String id) {
        return getResolution(id);
    }

    /**
     * Reloads all resolutions from the DB.
     */
    void refreshResolutions();

    /**
     * Given an IssueType ID this method retrieves that IssueType.
     *
     * @param id The ID of the IssueType.
     * @return An IssueType
     */
    IssueType getIssueType(String id);

    /**
     * Given an IssueType ID this method retrieves that IssueType.
     *
     * @param id The ID of the IssueType.
     * @return An {@link IssueType} object
     * @deprecated Since v7.0.  Use {@link #getIssueType(String)} instead.
     */
    @Deprecated
    default IssueType getIssueTypeObject(String id) {
        return getIssueType(id);
    }

    /**
     * Retrieve regular (non-subtask) issue types.
     *
     * @return A collection of {@link IssueType}s
     */
    Collection<IssueType> getRegularIssueTypeObjects();

    /**
     * Returns a list of IssueTypes.
     *
     * @return A Collection of {@link IssueType} objects.
     */
    Collection<IssueType> getAllIssueTypeObjects();

    /**
     * Returns all issueType Ids.
     *
     * @return A list of all the IssueType Ids.
     */
    List<String> getAllIssueTypeIds();

    /**
     * Retrieves all the sub-task issue types
     *
     * @return A Collection of all sub-task {@link IssueType}s.
     */
    @Nonnull
    Collection<IssueType> getSubTaskIssueTypeObjects();

    /**
     * Retrieves an mutable list of sub-task issues.
     * The list is mutable, that is its elements can be reordered added to or removed.
     * The elements of the list, that is the issue types themselves, are not mutable.
     *
     * @return A List of editable sub-task
     */
    List<IssueType> getEditableSubTaskIssueTypes();

    /**
     * Converts the 'special' ids of issue types to a list of issue type ids
     * For example, converts a special id to a list of all sub-task issue types
     * Also see {@link #ALL_STANDARD_ISSUE_TYPES}, {@link #ALL_SUB_TASK_ISSUE_TYPES} and
     * {@link #ALL_ISSUE_TYPES}.
     *
     * @param issueTypeIds A collection of the issuetype Ids to retrieve.
     * @return A list of "actual" IssueType ID's expanded from the macro constants (or a new copy of the original list if it doesn't contain macros).
     */
    List<String> expandIssueTypeIds(Collection<String> issueTypeIds);

    /**
     * Reloads all IssueTypes from the DB.
     */
    void refreshIssueTypes();

    /**
     * Creates a new IssueType.
     * <p>
     * Note this method does not validate the input - i.e. It does not check for duplicate names etc. Use
     * this method in conjunction with {@link #validateCreateIssueType(String, String, String, String, com.atlassian.jira.util.ErrorCollection, String)}
     *
     * @param name        Name of the new IssueType
     * @param sequence    Sequence number used for ordering the issuetypes in the UI.
     * @param style       Used to record the type of issue, such as SUBTASK.  Null for regular issues.
     * @param description A short description of the new issue type.
     * @param iconurl     A URL to an icon to be used for the new issueType.
     * @return The newly created IssueType
     * @throws CreateException If there is an error creating this Issue Type.
     * @deprecated Use {@link #insertIssueType(String, Long, String, String, Long)} instead. Since v6.3.
     */
    @Deprecated
    IssueType insertIssueType(String name, Long sequence, String style, String description, String iconurl)
            throws CreateException;

    /**
     * Creates a new IssueType.
     * <p>
     * Note this method does not validate the input - i.e. It does not check for duplicate names etc. Use
     * this method in conjunction with {@link #validateCreateIssueType(String, String, String, String, com.atlassian.jira.util.ErrorCollection, String)}
     *
     * @param name        Name of the new IssueType
     * @param sequence    Sequence number used for ordering the issuetypes in the UI.
     * @param style       Used to record the type of issue, such as SUBTASK.  Null for regular issues.
     * @param description A short description of the new issue type.
     * @param avatarId    Avatar id,
     * @return The newly created IssueType
     * @throws CreateException If there is an error creating this Issue Type.
     * @since v6.3
     */
    IssueType insertIssueType(String name, Long sequence, String style, String description, Long avatarId) throws CreateException;

    /**
     * Validates creation of a new issuetype.  In particular, this function checks that a name has been submitted, no
     * other issueTypes with the same name exist, and that the icon URL exists.
     *
     * @param name          Name of the new IssueType
     * @param style         Used to record the type of issue, such as SUBTASK.  Null for regular issues.
     * @param description   A short description of the new issue type.
     * @param iconurl       A URL to an icon to be used for the new issueType.
     * @param errors        A collection of errors used to pass back any problems.
     * @param nameFieldName The field to which the errors should be added.
     */
    void validateCreateIssueType(String name, String style, String description, String iconurl, ErrorCollection errors, String nameFieldName);

    /**
     * Validates creation of a new issuetype.  In particular, this function checks that a name has been submitted, no
     * other issueTypes with the same name exist and correct avatarId is passed.
     *
     * @param name          Name of the new IssueType
     * @param style         Used to record the type of issue, such as SUBTASK.  Null for regular issues.
     * @param description   A short description of the new issue type.
     * @param avatarId      An avatar id.
     * @param errors        A collection of errors used to pass back any problems.
     * @param nameFieldName The field to which the errors should be added.
     * @since v6.3
     */
    void validateCreateIssueTypeWithAvatar(String name, String style, String description, String avatarId, ErrorCollection errors, String nameFieldName);

    /**
     * Updates an existing issueType.  This will cause a invalidate of all issue types (i.e. reload from the DB).
     *
     * @param id          ID of the existing issuetype.
     * @param name        Name of the new IssueType
     * @param sequence    Sequence number used for ordering the issuetypes in the UI.
     * @param style       Used to record the type of issue, such as SUBTASK.  Null for regular issues.
     * @param description A short description of the new issue type.
     * @param iconurl     A URL to an icon to be used for the new issueType.
     * @throws DataAccessException indicates an error in the Data Access Layer
     * @deprecated use {@link #updateIssueType(String, String, Long, String, String, Long)} since v6.3
     */
    @Deprecated
    void updateIssueType(String id, String name, Long sequence, String style, String description, String iconurl)
            throws DataAccessException;

    /**
     * Updates an existing issueType.  This will cause a invalidate of all issue types (i.e. reload from the DB).
     *
     * @param id          ID of the existing issuetype.
     * @param name        Name of the new IssueType
     * @param sequence    Sequence number used for ordering the issuetypes in the UI.
     * @param style       Used to record the type of issue, such as SUBTASK.  Null for regular issues.
     * @param description A short description of the new issue type.
     * @param avatarId    avatarid of new issueType.
     * @throws DataAccessException indicates an error in the Data Access Layer
     * @since v6.3
     */
    void updateIssueType(String id, String name, Long sequence, String style, String description, Long avatarId);

    /**
     * Removes an existing issueType. This will cause a invalidate of all issue types (i.e. reload from the DB).
     * <p>
     * THIS METHOD IS BROKEN AND SHOULD NEVER BE USED SINCE v5.0.
     *
     * @param id ID of an existing issueType
     * @throws RemoveException if the issueType with id doesn't exist, or an error occurred removing the issue.
     * @deprecated Since 7.0. Do not use this method!!!  Use {@link com.atlassian.jira.config.IssueTypeManager#removeIssueType(String, String)}.
     */
    void removeIssueType(String id) throws RemoveException;

    /**
     * Bulk operation to store a list of issueTypes.
     *
     * @param issueTypes A list of IssueType {@link GenericValue}s
     * @throws DataAccessException indicates an error in the Data Access Layer
     * @deprecated Deprecated since v7.0. Use ${@link #updateIssueType(String, String, Long, String, String, Long)} or
     * ${@link #recalculateIssueTypeSequencesAndStore(java.util.List)} instead.
     */
    @Deprecated
    void storeIssueTypes(List<GenericValue> issueTypes) throws DataAccessException;

    /**
     * Returns a Status given an id.
     *
     * @param id The id of a status
     * @return Returns a status
     */
    Status getStatus(String id);

    /**
     * Returns a Status given an id.
     *
     * @param id The id of a status
     * @return Returns a {@link Status} object.
     * @deprecated Use {@link #getStatus} instead. Deprecated since v7.0
     */
    @Deprecated
    default Status getStatusObject(String id) {
        return getStatus(id);
    }

    /**
     * Returns all statuses
     *
     * @return Returns a Collection of status {@link com.atlassian.jira.issue.status.Status}s.
     */
    Collection<Status> getStatuses();

    /**
     * Returns all statuses
     *
     * @return Returns a Collection of {@link Status} objects
     * @deprecated Since v7.0.  Use {@link #getStatuses()} instead.
     */
    @Deprecated
    default Collection<Status> getStatusObjects() {
        return getStatuses();
    }

    /**
     * Reloads all statuses from DB.
     */
    void refreshStatuses();

    /**
     * Searches for a given status by name. This is not the most efficient implementation.
     * If the name is not found, or the given name is null, then it returns null.
     *
     * @param name The name of the status.
     * @return A {@link Status} object with the given name, or <code>null</code> if none found.
     */
    Status getStatusByName(String name);

    /**
     * Searches for a given status by name ignoring case. This is not the most efficient implementation.
     * If the name is not found, or the given name is null, then it returns null.
     *
     * @param name The name of the status.
     * @return A {@link Status} object with the given name, or <code>null</code> if none found.
     */
    Status getStatusByNameIgnoreCase(String name);

    /**
     * Searches for a given status by its translated name.
     * If no matching translated name is found the true (untranslated) name
     * will be tried.
     * If the name is not found, or the given name is null, then it returns null.
     *
     * @param name The name of the status.
     * @return A {@link Status} object with the given name, or <code>null</code> if none found.
     */
    Status getStatusByTranslatedName(String name);

    /**
     * Returns an {@link IssueConstant} object for the given type & id.
     *
     * @param constantType See {@link CONSTANT_TYPE}
     * @param id           The id of the constant.
     * @return A {@link IssueConstant} object. Null if it doesn't exist.
     */
    @Nullable
    IssueConstant getConstantObject(String constantType, String id);

    /**
     * Returns all {@link IssueConstant} objects for the given type.
     *
     * @param constantType See {@link CONSTANT_TYPE}
     * @return A collection of {@link IssueConstant} object; will be {@code null} if and only if {@code constantType}
     *      is invalid
     */
    @Nullable
    Collection<? extends IssueConstant> getConstantObjects(String constantType);

    /**
     * Converts the list of ids to the objects of appropriate types
     *
     * @param constantType the constant type. Case insenstive
     * @param ids          list of constant ids or GenericValues
     * @return List of IssueConstant subclasses. Null if constantType is null or the ids are empty
     * @deprecated Use {@link #getConstantsByIds(CONSTANT_TYPE, java.util.Collection)} instead. Deprecated since v7.0
     */
    @Nullable
    List<IssueConstant> convertToConstantObjects(String constantType, Collection<?> ids);

    /**
     * Converts the list of ids to the objects of appropriate types
     *
     * @param constantType Type of constant
     * @param ids          list of constant ids
     * @return List of IssueConstant subclasses.
     */
    @Nonnull
    List<IssueConstant> getConstantsByIds(@Nonnull CONSTANT_TYPE constantType, @Nonnull Collection<String> ids);

    /**
     * Checks if a constant exists.
     *
     * @param constantType See {@link #PRIORITY_CONSTANT_TYPE}, {@link #STATUS_CONSTANT_TYPE}, {@link #RESOLUTION_CONSTANT_TYPE}, {@link #ISSUE_TYPE_CONSTANT_TYPE}
     * @param name         The name of the constant.
     * @return True if the constant exists. False otherwise
     */
    boolean constantExists(String constantType, String name);

    /**
     * Returns a constant by name.
     *
     * @param constantType See {@link #PRIORITY_CONSTANT_TYPE}, {@link #STATUS_CONSTANT_TYPE}, {@link #RESOLUTION_CONSTANT_TYPE}, {@link #ISSUE_TYPE_CONSTANT_TYPE}
     * @param name         The Name of the constant.
     * @return The IssueConstant
     */
    IssueConstant getIssueConstantByName(String constantType, String name);

    /**
     * Returns a constant by name ignoring the case of the name passed in.
     *
     * @param constantType See {@link #PRIORITY_CONSTANT_TYPE}, {@link #STATUS_CONSTANT_TYPE}, {@link #RESOLUTION_CONSTANT_TYPE}, {@link #ISSUE_TYPE_CONSTANT_TYPE}
     * @param name         The Name of the constant, case-insensitive.
     * @return An IssueConstant (or null if not found)
     */
    IssueConstant getConstantByNameIgnoreCase(String constantType, String name);

    /**
     * Converts a constant {@link GenericValue} to an {@link IssueConstant} object.
     *
     * @param issueConstantGV the constant {@link GenericValue}.
     * @return An {@link IssueConstant} object.
     * @deprecated Deprecated since v7.0.  You just shouldn't have GVs any more
     */
    IssueConstant getIssueConstant(GenericValue issueConstantGV);

    /**
     * Sets all cached copies of constant to null.  This will cause them to be re-loaded from the DB
     * the next time they are accessed.
     *
     * @deprecated since v6.2.  Use {@link #invalidateAll()}
     */
    @Deprecated
    void refresh();

    /**
     * Validates the name of issue type. If the validation passes returns Option.none, else returns na Option with a pair
     * of error message and reason.
     */
    Option<Pair<String, ErrorCollection.Reason>> validateName(String name, Option<IssueType> issueTypeToUpdate);

    void invalidateAll();

    /**
     * Clear the cache for this Issue Constant.
     * Implementations may clear additional IssueConstants at their discretion.
     */
    void invalidate(IssueConstant issueConstant);

    /**
     * Resequences the supplied issuetypes into sequential order.
     *
     * @param issueTypes
     */
    void recalculateIssueTypeSequencesAndStore(List<IssueType> issueTypes);

    /**
     * Resequences the supplied priorities into sequential order.
     *
     * @param priorities
     */
    void recalculatePrioritySequencesAndStore(List<Priority> priorities);

    /**
     * Resequences the supplied statuses into sequential order.
     *
     * @param statuses
     */
    void recalculateStatusSequencesAndStore(List<Status> statuses);

    /**
     * Resequences the supplied resolutions into sequential order.
     *
     * @param resolutions
     */
    void recalculateResolutionSequencesAndStore(List<Resolution> resolutions);
}
