package com.atlassian.jira.plugin.webfragment.conditions.cache;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.join;

@ExperimentalApi
public class ConditionCacheKeys {

    private static final String PROJECT_PERMISSION_PREFIX = "atl.jira.permission.project.request.cache";
    private static final String GLOBAL_PERMISSION_PREFIX = "atl.jira.permission.global.request.cache";
    private static final String CUSTOM_CONDITION_PREFIX = "atl.jira.condition.custom.request.cache";

    @Nonnull
    @ExperimentalApi
    public static ConditionCacheKey permission(ProjectPermissionKey permission, @Nullable ApplicationUser user, Object... args) {
        return cacheKey(PROJECT_PERMISSION_PREFIX, permission.permissionKey(), user, args);
    }

    @Nonnull
    @ExperimentalApi
    public static ConditionCacheKey permission(GlobalPermissionKey permission, @Nullable ApplicationUser user, Object... args) {
        return cacheKey(GLOBAL_PERMISSION_PREFIX, permission.getKey(), user, args);
    }

    @Nonnull
    @ExperimentalApi
    public static ConditionCacheKey custom(String conditionName, ApplicationUser user, Object... args) {
        return cacheKey(CUSTOM_CONDITION_PREFIX, conditionName, user, args);
    }

    private static ConditionCacheKey cacheKey(String prefix, String mainComponent, ApplicationUser user, Object... args) {
        return new ConditionCacheKey(format("%s:%s:%s:%s", prefix, mainComponent, user, (args != null ? join(args, ":") : "")));
    }

}
