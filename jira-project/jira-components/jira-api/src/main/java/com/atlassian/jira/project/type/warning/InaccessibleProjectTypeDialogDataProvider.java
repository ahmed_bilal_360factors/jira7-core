package com.atlassian.jira.project.type.warning;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;

/**
 * Provides the data required to render the dialog attached to
 * the warning symbol that is displayed when a project type is inaccessible.
 * <p>
 * In order to generate a fully functional project type warning on the page, you need to:
 * <p>
 * - Call {@link InaccessibleProjectTypeDialogDataProvider#shouldDisplayInaccessibleWarning(ApplicationUser, Project)} to find out
 * the project type(s) for which you need to generate the warning icon.
 * <p>
 * - For each project type that needs a warning attached, generate an DOM element with the class
 * "project-type-warning-icon-{key}" (where key is the key of the project type that the warning corresponds to)
 * <p>
 * - Call {@link InaccessibleProjectTypeDialogDataProvider#provideData(WebResourceAssembler, ApplicationUser, Project)} or
 * {@link InaccessibleProjectTypeDialogDataProvider#provideResourcesMultipleProjects(WebResourceAssembler, ApplicationUser, ProjectTypeKey...)} (depending
 * on whether only one project is displayed on the page or several)
 * <p>
 * - Require the jira.webresources:project-type-warning web-resource
 * <p>
 * - From the client side: require the "jira/project/types/warning/dialog" AMD module and call "init" on the returned JavaScript object.
 *
 * @since 7.0
 */
@PublicApi
public interface InaccessibleProjectTypeDialogDataProvider {
    /**
     * Indicates whether the inaccessible project type warning should be displayed or not, on a page that is displaying a single project.
     *
     * @param user    The user for which we are performing the check.
     * @param project The project with the type for which we will display the warning
     * @return Whether the inaccessible project type warning should be displayed.
     */
    boolean shouldDisplayInaccessibleWarning(ApplicationUser user, Project project);

    /**
     * Provides the data needed to render the dialog attached to the warning symbols displayed for the project type of the given project.
     * <p>
     * This method should be used when the warning symbols are displayed on a page that is
     * displaying a single project, since the messages of the dialogs will have nouns in singular form.
     *
     * @param assembler The assembler to be used to provide the data
     * @param user      The user for which the warning dialog will be rendered
     * @param project   The keys of the project types for which the warning dialogs will be rendered
     */
    void provideData(WebResourceAssembler assembler, ApplicationUser user, Project project);
}
