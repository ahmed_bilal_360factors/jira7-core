package com.atlassian.jira.event.issue;

import com.atlassian.annotations.PublicApi;

/**
 * Visitor interface for {@link IssueEvent}
 *
 * @since 7.2
 */
@PublicApi
public interface IssueEventVisitor<X> {

    X onIssueCreated(IssueEvent e);

    X onIssueUpdated(IssueEvent e);

    X onIssueAssigned(IssueEvent e);

    X onIssueResolved(IssueEvent e);

    X onIssueCommented(IssueEvent e);

    X onIssueCommentEdited(IssueEvent e);

    X onIssueCommentDeleted(IssueEvent e);

    X onIssueClosed(IssueEvent e);

    X onIssueReopened(IssueEvent e);

    X onIssueDeleted(IssueEvent e);

    X onIssueMoved(IssueEvent e);

    X onIssueWorkLogged(IssueEvent e);

    X onIssueWorkStarted(IssueEvent e);

    X onIssueWorkStopped(IssueEvent e);

    X onIssueWorklogUpdated(IssueEvent e);

    X onIssueWorklogDeleted(IssueEvent e);

    X onIssueGenericEvent(IssueEvent e);

    X onCustomEvent(IssueEvent e);

}
