package com.atlassian.jira.issue.fields.rest.json;

import com.atlassian.core.util.DateUtils;
import com.atlassian.jira.bc.issue.worklog.TimeTrackingConfiguration;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.fields.rest.json.beans.UserJsonBean;
import com.atlassian.jira.issue.fields.rest.json.beans.VisibilityJsonBean;
import com.atlassian.jira.issue.fields.rest.json.beans.WorklogJsonBean;
import com.atlassian.jira.issue.worklog.Worklog;
import com.atlassian.jira.rest.Dates;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.google.common.collect.Iterables;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Nullable;
import java.math.BigDecimal;
import java.net.URI;

public class DefaultWorklogBeanFactory implements WorklogBeanFactory {
    private final TimeTrackingConfiguration timeTrackingConfiguration;
    private final JiraBaseUrls jiraBaseUrls;
    private final UserBeanFactory userBeanFactory;
    private final UserManager userManager;

    public DefaultWorklogBeanFactory(final TimeTrackingConfiguration timeTrackingConfiguration, final JiraBaseUrls jiraBaseUrls, final UserBeanFactory userBeanFactory, final UserManager userManager) {
        this.timeTrackingConfiguration = timeTrackingConfiguration;
        this.jiraBaseUrls = jiraBaseUrls;
        this.userBeanFactory = userBeanFactory;
        this.userManager = userManager;
    }

    @Override
    public WorklogJsonBean createBean(final Worklog worklog, ApplicationUser loggedInUser) {
        URI self = URI.create(worklogSelf(worklog));
        return WorklogJsonBean.build()
                .setId(Long.toString(worklog.getId()))
                .setIssueId(Long.toString(worklog.getIssue().getId()))
                .setComment(worklog.getComment())
                .setTimeSpent(getTimeLoggedString(worklog.getTimeSpent()))
                .setTimeSpentSeconds(worklog.getTimeSpent())
                .setCreated(Dates.asTimeString(worklog.getCreated()))
                .setUpdated(Dates.asTimeString(worklog.getUpdated()))
                .setStarted(Dates.asTimeString(worklog.getStartDate()))
                .setSelf(self)
                .setAuthor(getUserBean(worklog.getAuthorKey(), loggedInUser))
                .setUpdateAuthor(getUserBean(worklog.getUpdateAuthorKey(), loggedInUser))
                .setVisibility(getVisibilityBean(worklog))
                .build();
    }

    private String worklogSelf(final Worklog worklog) {
        return jiraBaseUrls.restApi2BaseUrl() + "issue/" + worklog.getIssue().getId().toString() + "/worklog/" + worklog.getId().toString();
    }

    @Override
    public Iterable<WorklogJsonBean> createBeans(final Iterable<Worklog> worklogs, final ApplicationUser loggedInUser) {
        return Iterables.transform(worklogs, (worklog) -> createBean(worklog, loggedInUser));
    }

    protected UserJsonBean getUserBean(String userKey, final ApplicationUser loggedInUser) {
        ApplicationUser user = userManager.getUserByKey(userKey);
        if (user != null) {
            return userBeanFactory.createBean(user, loggedInUser);
        } else if (StringUtils.isNotBlank(userKey)) {
            UserJsonBean userJsonBean = new UserJsonBean();
            userJsonBean.setName(userKey);
            return userJsonBean;
        } else {
            return null;
        }
    }

    @Nullable
    private VisibilityJsonBean getVisibilityBean(Worklog worklog) {
        final String groupLevel = worklog.getGroupLevel();
        final ProjectRole roleLevel = worklog.getRoleLevel();
        if (groupLevel != null) {
            return new VisibilityJsonBean(VisibilityJsonBean.VisibilityType.group, groupLevel);
        } else if (roleLevel != null) {
            return new VisibilityJsonBean(VisibilityJsonBean.VisibilityType.role, roleLevel.getName());
        }
        return null;
    }

    private String getTimeLoggedString(long timeSpent) {
        final BigDecimal hoursPerDay = timeTrackingConfiguration.getHoursPerDay();
        final BigDecimal daysPerWeek = timeTrackingConfiguration.getDaysPerWeek();
        final BigDecimal secondsPerHour = BigDecimal.valueOf(DateUtils.Duration.HOUR.getSeconds());
        final long secondsPerDay = hoursPerDay.multiply(secondsPerHour).longValueExact();
        final long secondsPerWeek = daysPerWeek.multiply(hoursPerDay).multiply(secondsPerHour).longValueExact();
        return DateUtils.getDurationStringSeconds(timeSpent, secondsPerDay, secondsPerWeek);
    }
}
