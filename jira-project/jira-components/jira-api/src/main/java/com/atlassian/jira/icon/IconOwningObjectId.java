package com.atlassian.jira.icon;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.jira.entity.WithId;

import javax.annotation.Nonnull;

/**
 * Represents the id of an entity that has an icon/avatar associated with it.
 *
 * @since 7.1
 */
@ExperimentalApi
public final class IconOwningObjectId {
    @Nonnull
    private final String id;

    public IconOwningObjectId(@Nonnull final String id) {
        this.id = id;
    }

    /**
     * Returns the ID of the owner object.
     * <p>
     *     eg for Project this will be the String representation of the Project ID,
     *     for User this will be the user key.
     *
     * @return the ID of the owner object.
     */
    @Nonnull
    public String getId() {
        return id;
    }

    /**
     * Creates an IconOwningObjectId from the given numeric ID.
     * @param id the numeric ID
     * @return an IconOwningObjectId which contains a String representation of the given numeric ID.
     */
    @Nonnull
    public static IconOwningObjectId from(long id) {
        return new IconOwningObjectId(Long.toString(id));
    }

    /**
     * Creates an IconOwningObjectId from the given entity.
     * @param withId the numeric ID
     * @return an IconOwningObjectId which contains a String representation of the ID of the given entity.
     */
    @Nonnull
    public static IconOwningObjectId from(@Nonnull WithId withId) {
        return new IconOwningObjectId(Long.toString(withId.getId()));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IconOwningObjectId that = (IconOwningObjectId) o;

        if (!id.equals(that.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return "IconOwningObjectId{" +
                "id='" + id + '\'' +
                '}';
    }
}
