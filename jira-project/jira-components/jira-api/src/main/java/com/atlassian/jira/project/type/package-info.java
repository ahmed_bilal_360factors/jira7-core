/**
 * Package containing APIs to access project types.
 */
@ParametersAreNonnullByDefault package com.atlassian.jira.project.type;

import javax.annotation.ParametersAreNonnullByDefault;

