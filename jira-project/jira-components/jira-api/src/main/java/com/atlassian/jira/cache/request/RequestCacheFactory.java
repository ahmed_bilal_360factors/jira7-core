package com.atlassian.jira.cache.request;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.cache.CacheLoader;
import com.atlassian.cache.Supplier;

/**
 * Factory to create RequestCaches.
 * <p>
 * This is intended to be added to JIRA 6.4 for experimental use in performance fixes.
 * Eventually, it will likely be replaced by a similar mechanism in atlassian-cache but we can use this to iron out
 * desired usage patterns.
 * </p>
 *
 * @since v6.4.8
 */
@ExperimentalApi
public interface RequestCacheFactory {
    /**
     * Creates a new request-scoped cache with no default cache loader.
     * <p>
     * The cache will behave identically to one created by {@link #createRequestCache(String, CacheLoader)},
     * except that calls to {@link RequestCache#get(Object) get} without a supplier will throw an exception
     * for values that are not already in the cache.  This is mainly intended for cases where only the
     * {@link RequestCache#get(Object, Supplier) supplier} form of {@code get} will be used exclusively
     * and specifying a {@code CacheLoader} that would never be used is unnecessarily awkward.
     * </p>
     *
     * @param name as for {@link #createRequestCache(String, CacheLoader)}
     * @param <K>  as for {@link #createRequestCache(String, CacheLoader)}
     * @param <V>  as for {@link #createRequestCache(String, CacheLoader)}
     * @return as for {@link #createRequestCache(String, CacheLoader)}
     */
    <K, V> RequestCache<K, V> createRequestCache(final String name);

    /**
     * Creates a new request-scoped cache.
     * <p>
     * Any code that accesses the resulting request-scoped cache from within an HTTP request or a task
     * scheduled using the atlassian-scheduler API will automatically inherit a request-scoped cache.
     * See {@link JiraThreadLocalUtil} for alternative ways to establish a request context.
     * </p>
     * <p>
     * If a request cache is queried when there is no context established, then caching is completely
     * disabled.  All requests to the cache are passed on to the {@code CacheLoader}.
     * </p>
     *
     * @param name        a name to identify the cache.  Although no restrictions are currently enforced for this name
     *                    (even uniqueness), the name <strong>SHOULD</strong> follow the recommended conventions of the
     *                    {@code atlassian-cache} API.  Specifically, it should be the fully qualified name of the owning
     *                    class and the field it uses to hold the request cache reference, separated by a {@code '.'}.
     * @param cacheLoader the loader that provides cached values
     * @param <K>         the type of keys used to look up values in this request cache
     * @param <V>         the type of values returned by this request cache's loader (and therefore by the cache itself)
     * @return the newly created request cache
     */
    <K, V> RequestCache<K, V> createRequestCache(final String name, final CacheLoader<K, V> cacheLoader);
}
