package com.atlassian.jira.issue;

/**
 * Struct-like class for storing an old field value/new field value pair.
 */
public class ModifiedValue<V> {
    private final V oldValue;
    private final V newValue;

    /**
     * Construct a field modification pair. Objects are field-specific,
     * eg. from {@link com.atlassian.jira.issue.fields.CustomField#getValue}
     *
     * @param oldValue the old value
     * @param newValue the new value
     */
    public ModifiedValue(V oldValue, V newValue) {
        this.oldValue = oldValue;
        this.newValue = newValue;
    }

    public V getOldValue() {
        return oldValue;
    }

    public V getNewValue() {
        return newValue;
    }
}
