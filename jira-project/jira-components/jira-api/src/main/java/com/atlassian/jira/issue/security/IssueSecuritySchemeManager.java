package com.atlassian.jira.issue.security;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.scheme.SchemeManager;
import com.atlassian.jira.user.ApplicationUser;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@PublicApi
public interface IssueSecuritySchemeManager extends SchemeManager {
    /**
     * Gets a scheme by id from the database.
     *
     * @param id the id of the scheme to get.
     * @return the Scheme
     * @throws DataAccessException if there is trouble retrieving from the database.
     * @deprecated Use {@link #getIssueSecurityLevelScheme(Long)} instead. Since v5.2.
     */
    Scheme getSchemeObject(Long id) throws DataAccessException;

    /**
     * Returns the IssueSecurityLevelScheme for the given ID.
     *
     * @param schemeId Scheme ID
     * @return the IssueSecurityLevelScheme for the given ID.
     */
    IssueSecurityLevelScheme getIssueSecurityLevelScheme(Long schemeId);

    /**
     * Returns a list containing all defined IssueSecuritySchemes
     *
     * @return a list of IssueSecuritySchemes
     */
    Collection<IssueSecurityLevelScheme> getIssueSecurityLevelSchemes();

    /**
     * Returns the configured permissions for the given Security Level.
     *
     * @param securityLevelId the Security Level
     * @return the configured permissions for the given Security Level.
     * @deprecated Use {@link #getPermissionsBySecurityLevel(Long)} instead. Since v5.2.
     */
    public List getEntitiesBySecurityLevel(Long securityLevelId) throws GenericEntityException;

    /**
     * Returns the configured permissions for the given Security Level.
     *
     * @param securityLevelId the Security Level
     * @return the configured permissions for the given Security Level.
     */
    List<IssueSecurityLevelPermission> getPermissionsBySecurityLevel(Long securityLevelId);

    /**
     * Get all Scheme entity records for a particular scheme.
     * Inherited from SchemeManager.
     *
     * @param scheme The scheme that the entities belong to
     * @return List of (GenericValue) entities
     * @throws GenericEntityException If a DB error occurs
     */
    List<GenericValue> getEntities(GenericValue scheme) throws GenericEntityException;

    /**
     * Inherited from SchemeManager.
     *
     * @deprecated Use {@link #getPermissionsBySecurityLevel(Long)} instead. Since v5.2.
     */
    List<GenericValue> getEntities(GenericValue scheme, Long securityLevelId) throws GenericEntityException;

    /**
     * Inherited from SchemeManager.
     *
     * @deprecated Never worked for String anyway. Since v6.4.
     */
    List<GenericValue> getEntities(GenericValue scheme, String entityTypeId) throws GenericEntityException;

    /**
     * Inherited from SchemeManager.
     *
     * @deprecated Not used - presumably this was invented in SchemeManager for Permission Schemes.
     */
    List<GenericValue> getEntities(GenericValue scheme, Long entityTypeId, String parameter) throws GenericEntityException;

    /**
     * Inherited from SchemeManager.
     *
     * @deprecated Not used - presumably this was invented in SchemeManager for Permission Schemes.
     */
    List<GenericValue> getEntities(GenericValue scheme, String type, Long entityTypeId) throws GenericEntityException;

    /**
     * This is a method that is meant to quickly get you all the schemes that contain an entity of the
     * specified type and parameter.
     *
     * @param type      is the entity type
     * @param parameter is the scheme entries parameter value
     * @return Collection of GenericValues that represents a scheme
     */
    public Collection<GenericValue> getSchemesContainingEntity(String type, String parameter);

    /**
     * Returns all projects that use the given Issue Security Level Scheme.
     *
     * @param schemeId ID of the Issue Security Level Scheme
     * @return all projects that use the given Issue Security Level Scheme.
     */
    List<Project> getProjectsUsingScheme(long schemeId);

    /**
     * Set the issue security level scheme to be used by the given Project.
     *
     * @param project               The Project
     * @param issueSecuritySchemeId The desired new security level scheme to use - null indicates "no issue security levels".
     */
    void setSchemeForProject(Project project, Long issueSecuritySchemeId);

    /**
     * Set the issue security level scheme to be used by the given Project and map any old security levels to new ones.
     *
     * @param project                       The Project
     * @param newSchemeId                   The desired new security level scheme to use - null indicates "no issue security levels".
     * @param oldToNewSecurityLevelMappings Mappings of old to new security levels.
     *                                           This <b>must not</b> contain null values. '-1' is used to represent the value "none".
     * @return the URL of the progress page.
     * @since V7.1.1
     */
    public String assignSchemeToProject(@Nonnull Project project, @Nullable Long newSchemeId, Map<Long, Long> oldToNewSecurityLevelMappings);

    /**
     * Checks if the given user has permission to see the Issue Security Level of the given issue.
     * <p>
     * Note that this does not check other permissions.
     *
     * @param issue the Issue
     * @param user  the User (null for anonymous)
     * @return true if the user hase Security Level permission for the issue.
     */
    boolean hasSecurityLevelAccess(@Nonnull Issue issue, @Nullable ApplicationUser user);
}
