package com.atlassian.jira.imports.project.handler;

import com.atlassian.annotations.ExperimentalSpi;

/**
 * Defines a handler class that will be able to perform some operation given an entity name and the entities
 * attributes.
 *
 * @since v7.0
 */
@ExperimentalSpi
public interface PluggableImportOfBizEntityHandler extends PluggableImportHandler, ImportOfBizEntityHandler {
    /**
     * Provides the implementation an opportunity to perform some action when the document is starting to
     * be read.
     */
    void startDocument();

    /**
     * Provides the implementation an opportunity to perform some action when the document is finished being read.
     */
    void endDocument();

    /**
     * Return true if this handler should process the entity.
     *
     * @param entityName Entities to process.
     */
    boolean handlesEntity(String entityName);
}
