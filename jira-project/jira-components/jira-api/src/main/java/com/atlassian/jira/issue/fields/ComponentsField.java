package com.atlassian.jira.issue.fields;

import com.atlassian.jira.bc.project.component.ProjectComponent;

import java.util.Collection;

/**
 * @since v5.0
 */
public interface ComponentsField extends HideableField, RequirableField, NavigableField, OrderableField<Collection<ProjectComponent>> {
}
