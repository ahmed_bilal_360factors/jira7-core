package com.atlassian.jira.bc.user;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.annotations.Internal;
import com.atlassian.annotations.PublicApi;
import com.atlassian.application.api.ApplicationKey;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.bc.ServiceResultImpl;
import com.atlassian.jira.event.user.UserEventType;
import com.atlassian.jira.exception.AddException;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.plugin.user.WebErrorMessage;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.WarningCollection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * UserService provides User manipulation methods exposed for remote API and actions.
 *
 * @since v4.0
 */
@PublicApi
public interface UserService {
    /**
     * Validate that a new user can be created.
     * The validation instructions are specified in the {@link CreateUserRequest}.
     * <p>
     * Typical validations are:
     * <ul>
     * <li>Validate that specified (or default) application(s) has a seat available for the new user and that the default group(s) has been configured.</li>
     * <li>Validate that the specified logged in user is allowed to create a new user.</li>
     * <li>Validate that the specified or default user directory exists and is writable.</li>
     * <li>Validate that password conforms to password policy.</li>
     * <li>Validate that confirm password matches password (optional).</li>
     * <li>Validate that the new username is not currently in use.</li>
     * </ul>
     * </p>
     * {@link #createUser(CreateUserValidationResult)} should only be called when the validationResult.isValid().
     * <p>
     * Usage example:
     * <pre>
     * <code>final CreateUserRequest createUserRequest = CreateUserRequest.withUserDetails(currentApplicationUser,
     *               username, password, email, displayName)
     * result = userService.validateCreateUser(createUserRequest);
     * if(result.isValid())
     * {
     *    userService.createUser(createUserRequest);
     * }
     * </code>
     * </pre>
     * This request indicates that the user should be created in the default user directory with access to the default
     * applications.
     * </p>
     *
     * @param createUserRequest user creation request containing new user details and validation instructions.
     * @return the result that would contain error messages when validation was not successful.
     */
    CreateUserValidationResult validateCreateUser(CreateUserRequest createUserRequest);

    /**
     * Create a new application user if the provided {@link CreateUserValidationResult#isValid()}. This is typically
     * called after performing {@link #validateCreateUser(CreateUserRequest)} and verifying that the {@link
     * CreateUserValidationResult#isValid()}. If the request is not valid or {@link
     * #validateCreateUser(CreateUserRequest)}  was not called prior to this method an {@link IllegalStateException}
     * would get thrown.
     *
     * @param createUserValidationResult create user validation result returned from {@link
     *                                   #validateCreateUser(CreateUserRequest)}, this contains the user details and instructions used to create new
     *                                   user.
     * @return an application user representing the newly created user.
     * @throws PermissionException when the logged in user does not have permissions to perform user creation or when
     *                             the user directory is read-only.
     * @throws CreateException     if the user could not be created for any reason, eg username already exists
     * @see #validateCreateUser(CreateUserRequest)
     */
    ApplicationUser createUser(CreateUserValidationResult createUserValidationResult)
            throws PermissionException, CreateException;

    /**
     * Allows you to construct {@link ApplicationUser} for {@link #validateUpdateUser(ApplicationUser)}
     */
    @Nonnull
    @ExperimentalApi
    ApplicationUserBuilder newUserBuilder(@Nonnull ApplicationUser user);

    /**
     * Validates creating a user during public signup.
     * This method checks that there is a writable User Directory available.
     * It also validates that all parameters (username, email, fullname, password) have been provided.
     * Email is also checked to ensure that it is a valid email address.  The username is required to be
     * lowercase characters only and unique. The confirmPassword has to match the password provided.
     * <p>
     * This validation differs from the 'ForAdminPasswordRequired' and 'ForAdmin' validations as follows: <ul> <li>Does
     * not require global admin rights</li> <li>The password is required</li> <ul>
     *
     * @param loggedInUser    The remote user trying to add a new user
     * @param username        The username of the new user. Needs to be lowercase and unique.
     * @param password        The password for the new user.
     * @param confirmPassword The password confirmation.  Needs to match password.
     * @param email           The email for the new user.  Needs to be a valid email address.
     * @param fullname        The full name for the new user
     * @return a validation result containing appropriate errors or the new user's details
     * @since 5.0
     * @deprecated Use {@link #validateCreateUser(CreateUserRequest)} instead. Since v7.0.
     */
    CreateUserValidationResult validateCreateUserForSignup(ApplicationUser loggedInUser, String username, String password,
                                                           String confirmPassword, String email, String fullname);

    /**
     * Validates creating a user during setup of JIRA.
     * This method does not check for a writable User Directory because the embedded crowd subsystem will not be
     * initialised, and we know we will just have an Internal Directory during Setup.
     * It also validates that all parameters (username, email, fullname, password) have been
     * provided.  Email is also checked to ensure that it is a valid email address.  The username is required to be
     * lowercase characters only and unique. The confirmPassword has to match the password provided.
     * <p>
     * This validation differs from the 'ForAdminPasswordRequired' and 'ForAdmin' validations as follows: <ul> <li>Does
     * not require global admin rights</li> <li>The password is required</li> <ul>
     *
     * @param loggedInUser    The remote user trying to add a new user
     * @param username        The username of the new user. Needs to be lowercase and unique.
     * @param password        The password for the new user.
     * @param confirmPassword The password confirmation.  Needs to match password.
     * @param email           The email for the new user.  Needs to be a valid email address.
     * @param fullname        The full name for the new user
     * @return a validation result containing appropriate errors or the new user's details
     * @since 5.0
     * @deprecated Use {@link #validateCreateUser(CreateUserRequest)} instead. Since v7.0.
     */
    CreateUserValidationResult validateCreateUserForSetup(ApplicationUser loggedInUser, String username, String password,
                                                          String confirmPassword, String email, String fullname);

    /**
     * Validates creating a user during setup of JIRA or during public signup.
     * This method checks that there is a writable User Directory available.
     * It also validates that all parameters (username, email, fullname, password) have been
     * provided.  Email is also checked to ensure that it is a valid email address.  The username is required to be
     * lowercase characters only and unique. The confirmPassword has to match the password provided.
     * <p>
     * This validation differs from the 'ForAdminPasswordRequired' and 'ForAdmin' validations as follows: <ul> <li>Does
     * not require global admin rights</li> <li>The password is required</li> <ul>
     *
     * @param user            The remote user trying to add a new user
     * @param username        The username of the new user. Needs to be lowercase and unique.
     * @param password        The password for the new user.
     * @param confirmPassword The password confirmation.  Needs to match password.
     * @param email           The email for the new user.  Needs to be a valid email address.
     * @param fullname        The full name for the new user
     * @return a validation result containing appropriate errors or the new user's details
     * @since 4.0
     * @deprecated Use {@link #validateCreateUser(CreateUserRequest)} instead. Since v7.0.
     */
    CreateUserValidationResult validateCreateUserForSignupOrSetup(ApplicationUser user, String username, String password,
                                                                  String confirmPassword, String email, String fullname);

    /**
     * Validates creating a user for the admin section.  This method checks that external user management is disabled
     * and that the user performing the operation has global admin rights.  It also validates that all parameters
     * (username, email, fullname) except for the password have been provided.  Email is also checked to ensure that it
     * is a valid email address.  The username is required to be lowercase characters only and unique. The
     * confirmPassword has to match the password provided.
     * <p>
     * This validation differs from the 'ForSetup' and 'ForAdminPasswordRequired' validations as follows: <ul> <li>Does
     * require global admin rights</li> <li>The password is NOT required</li> </ul>
     *
     * @param loggedInUser    The remote user trying to add a new user
     * @param username        The username of the new user. Needs to be lowercase and unique.
     * @param password        The password for the new user.
     * @param confirmPassword The password confirmation.  Needs to match password.
     * @param email           The email for the new user.  Needs to be a valid email address.
     * @param fullname        The full name for the new user
     * @return a validation result containing appropriate errors or the new user's details
     * @since 4.3
     * @deprecated Use {@link #validateCreateUser(CreateUserRequest)} instead. Since v7.0.
     */
    CreateUserValidationResult validateCreateUserForAdmin(ApplicationUser loggedInUser, String username, String password, String confirmPassword,
                                                          String email, String fullname);

    /**
     * Validates creating a user for the admin section.  This method checks that external user management is disabled
     * and that the user performing the operation has global admin rights.  It also validates that all parameters
     * (username, email, fullname) except for the password have been provided.  Email is also checked to ensure that it
     * is a valid email address.  The username is required to be lowercase characters only and unique. The
     * confirmPassword has to match the password provided.
     * <p>
     * This method allows the caller to name a directory to create the user in and the directoryId must be valid and
     * represent a Directory with "create user" permission.
     * <p>
     * This validation differs from the 'ForSetup' and 'ForAdminPasswordRequired' validations as follows: <ul> <li>Does
     * require global admin rights</li> <li>The password is NOT required</li> </ul>
     *
     * @param loggedInUser    The remote user trying to add a new user
     * @param username        The username of the new user. Needs to be lowercase and unique.
     * @param password        The password for the new user.
     * @param confirmPassword The password confirmation.  Needs to match password.
     * @param email           The email for the new user.  Needs to be a valid email address.
     * @param fullname        The full name for the new user
     * @param directoryId     The User Directory
     * @return a validation result containing appropriate errors or the new user's details
     * @since 4.3.2
     * @deprecated Use {@link #validateCreateUser(CreateUserRequest)} instead. Since v7.0.
     */
    CreateUserValidationResult validateCreateUserForAdmin(ApplicationUser loggedInUser, String username, String password, String confirmPassword,
                                                          String email, String fullname, @Nullable Long directoryId);

    /**
     * Validates the username for a new user. The username is required to be lowercase characters only and unique across
     * all directories.
     *
     * @param loggedInUser The remote user trying to add a new user
     * @param username     The username of the new user. Needs to be lowercase and unique.
     * @return a validation result containing appropriate errors
     * @since 5.0.4
     */
    CreateUsernameValidationResult validateCreateUsername(ApplicationUser loggedInUser, String username);

    /**
     * Validates the username for a new user. The username is required to be lowercase characters only and unique in the
     * given directory.
     *
     * @param loggedInUser The remote user trying to add a new user
     * @param username     The username of the new user. Needs to be lowercase and unique.
     * @param directoryId  The directory which the new user is intended to be created in.
     * @return a validation result containing appropriate errors
     * @since 5.0.4
     */
    CreateUsernameValidationResult validateCreateUsername(ApplicationUser loggedInUser, String username, Long directoryId);

    /**
     * Given a valid validation result, this will create the user using the details provided in the validation result.
     * Email notification will be send to created user - via UserEventType.USER_SIGNUP event.
     *
     * @param result The validation result
     * @return The new user object that was created
     * @throws CreateException     if the user could not be created, eg username already exists
     * @throws PermissionException If you cannot create users in this directory (it is read-only).
     * @since 4.3
     * @deprecated Use {@link #createUser(CreateUserValidationResult)} instead. Since v7.0.
     */
    ApplicationUser createUserFromSignup(final CreateUserValidationResult result)
            throws PermissionException, CreateException;

    /**
     * Given a valid validation result, this will create the user using the details provided in the validation result.
     * Email notification will be send to created user - via UserEventType.USER_CREATED event.
     *
     * @param result The validation result
     * @return The new user object that was created
     * @throws CreateException     if the user could not be created, eg username already exists
     * @throws PermissionException If you cannot create users in this directory (it is read-only).
     * @since 4.3
     * @deprecated Use {@link #createUser(CreateUserValidationResult)} instead. Since v7.0.
     */
    ApplicationUser createUserWithNotification(CreateUserValidationResult result)
            throws PermissionException, CreateException;

    /**
     * Given a valid validation result, this will create the user using the details provided in the validation result.
     * No email notification will be send to created user.
     *
     * @param result The validation result
     * @return The new user object that was created
     * @throws CreateException     if the user could not be created, eg username already exists
     * @throws PermissionException If you cannot create users in this directory (it is read-only).
     * @since 4.3
     * @deprecated Use {@link #createUser(CreateUserValidationResult)} instead. Since v7.0.
     */
    ApplicationUser createUserNoNotification(CreateUserValidationResult result)
            throws PermissionException, CreateException;

    /**
     * Validates updating a user's details.
     *
     * @param user The user details to update
     * @return A validation result containing any errors and all user details
     */
    UpdateUserValidationResult validateUpdateUser(ApplicationUser user);

    /**
     * Execute the update using the validation result from {@link #validateUpdateUser(ApplicationUser)}.
     *
     * @param updateUserValidationResult Result from the validation, which also contains all the user's details.
     * @throws IllegalStateException if the validation result contains any errors.
     */
    void updateUser(UpdateUserValidationResult updateUserValidationResult);

    /**
     * Validates removing a user for the admin section.  This method checks that external user management is disabled
     * and that the user performing the operation has global admin rights.  It also validates that username have been
     * provided.
     * <p>
     * See {@link #validateDeleteUser(ApplicationUser, ApplicationUser)} for restrictions.
     *
     * @param loggedInUser The remote user trying to remove a user
     * @param username     The username of the user to remove. Needs to be valid
     * @return a validation result containing appropriate errors or the user object for delete
     * @since 6.0
     */
    DeleteUserValidationResult validateDeleteUser(final ApplicationUser loggedInUser, final String username);

    /**
     * Validates removing a user for the admin section.  This method checks that external user management is disabled
     * and that the user performing the operation has global admin rights.  It also validates that username have been
     * provided.
     * <p>
     * Removing the user is not allowed if:
     * <ul>
     * <li>Logged in user lacks global admin permissions</li>
     * <li>Logged in user and target user are the same user</li>
     * <li>Logged in user is not a system administrator but the target user is</li>
     * <li>Target user is the assignee for any issue</li>
     * <li>Target user is the reporter for any issue</li>
     * <li>Target user is the project lead for any project</li>
     * <li>A plugin that implements {@link com.atlassian.jira.plugin.user.PreDeleteUserErrors} rejects the request</li>
     * </ul>
     *
     * @param loggedInUser  The remote user trying to remove a user
     * @param userForDelete The user to remove. Needs to be valid
     * @return a validation result containing appropriate errors or the user object for delete
     * @since 6.0
     */
    DeleteUserValidationResult validateDeleteUser(final ApplicationUser loggedInUser, final ApplicationUser userForDelete);

    /**
     * Given a valid validation result, this will remove the user and removes the user from all the groups. All
     * components lead by user will have lead cleared.
     *
     * @param loggedInUser the user performing operation
     * @param result       The validation result
     * @deprecated Use {@link #removeUser(ApplicationUser, DeleteUserValidationResult)} instead. Since v6.0.
     */
    void removeUser(ApplicationUser loggedInUser, final DeleteUserValidationResult result);

    /**
     * Validates associating a user with an application. This method checks there is an application specified by the
     * application key. It also checks if there are any default groups assigned to that application. It validates if
     * there are any spaces in the application license as well. This also validates if the directory where the user
     * lives is not fully read-only.
     *
     * @param user           the user to be associated with the application
     * @param applicationKey the key of the application
     * @return a validation result containing appropriate errors or the details used to associate a user with an application
     * @since 7.0
     * @deprecated Use {@link #validateAddUserToApplication(ApplicationUser, ApplicationUser, ApplicationKey)} instead. Since v7.0
     */
    AddUserToApplicationValidationResult validateAddUserToApplication(ApplicationUser user, ApplicationKey applicationKey);


    /**
     * Validates associating a user with an application. This method checks if the logged in user has the permission to
     * add user to application. It then checks if there is an application specified by the application key. It also checks if
     * there are any default groups assigned to that application. It validates if there are any spaces in the application
     * license as well. This also validates if the directory where the user lives is not fully read-only.
     *
     * @param loggedInUser   The logged in user
     * @param user           the user to be associated with the application
     * @param applicationKey the key of the application
     * @return a validation result containing appropriate errors or the details used to associate a user with an application
     * @since 7.0
     */
    AddUserToApplicationValidationResult validateAddUserToApplication(ApplicationUser loggedInUser, ApplicationUser user, ApplicationKey applicationKey);

    /**
     * Given a valid validation result, this will associate a user with an application by adding that user to all the
     * default groups assigned to that application (regardless of the license).
     * <p>
     * If the user is already associated with the application but is not the member of all the default groups, that user
     * will be added to the default groups which that user does not belong to yet.
     * <p>
     * If any of the default groups assigned to this application are also assigned to other application(s), the user
     * will also be associated with those applications as well.
     * <p>
     * This method is not the exact opposite to {@link com.atlassian.jira.bc.user.UserService#removeUserFromApplication}.
     * While this method only adds user to the default groups assigned to an application, removeUserFromApplication
     * method removes user from all the group assigned to an application.
     *
     * @param result the validation result
     * @throws com.atlassian.jira.exception.AddException        the underlying directory implementation failed to add user to group
     * @throws com.atlassian.jira.exception.PermissionException the underlying directory has been configured to not allow
     *                                                          user to be added to group, or the directory/group is read-only
     * @since v7.0
     */
    void addUserToApplication(AddUserToApplicationValidationResult result)
            throws AddException, PermissionException;

    /**
     * Validates disassociating a user from an application. This method checks there is an application specified by the
     * application key.
     * <p>
     * Method expects for loggedInUser to be available via {@link com.atlassian.jira.security.JiraAuthenticationContext#getLoggedInUser()}.
     *
     * @param user           the user to be disassociated with the application
     * @param applicationKey the key of the application
     * @return a validation result containing appropriate errors or the details used to disassociate a user from an application
     * @since 7.0
     * @deprecated Use {@link UserService#validateRemoveUserFromApplication(com.atlassian.jira.user.ApplicationUser, com.atlassian.jira.user.ApplicationUser, com.atlassian.application.api.ApplicationKey)} instead. Since v 7.0.0.
     */
    @Deprecated
    @Internal
    RemoveUserFromApplicationValidationResult validateRemoveUserFromApplication(ApplicationUser user, ApplicationKey applicationKey);

    /**
     * Validates disassociating a user from an application. This method checks there is an application specified by the
     * application key.
     *
     * @param loggedInUser   application user performing the application access removal
     * @param user           the user to be disassociated with the application
     * @param applicationKey the key of the application
     * @return a validation result containing appropriate errors or the details used to disassociate a user from an application
     * @since 7.0
     */
    RemoveUserFromApplicationValidationResult validateRemoveUserFromApplication(ApplicationUser loggedInUser, ApplicationUser user, ApplicationKey applicationKey);

    /**
     * Given a valid validation result, this will disassociate a user from an application by removing that user from all
     * the groups assigned to that application. This also validates if the directory where the user lives is not fully read-only.
     * <p>
     * If the group set assigned to this application is a superset of the group set assigned to other application(s),
     * the user will also be disassociated with those applications as well.
     * <p>
     * This method is not the exact opposite to {@link com.atlassian.jira.bc.user.UserService#addUserToApplication}.
     * While this method removes user from all the group assigned to an application, addUserToApplication method
     * only adds user to the default groups assigned to an application
     *
     * @param result The validation result
     * @throws com.atlassian.jira.exception.RemoveException     the underlying directory implementation failed to remove user from group
     * @throws com.atlassian.jira.exception.PermissionException the underlying directory has been configured to not allow
     *                                                          user to be removed from group, or the directory/group is read-only
     * @since v7.0
     */
    void removeUserFromApplication(RemoveUserFromApplicationValidationResult result)
            throws RemoveException, PermissionException;

    @PublicApi
    final class CreateUserValidationResult extends ServiceResultImpl {
        private final List<WebErrorMessage> passwordErrors;
        private final Set<ApplicationKey> applicationKeys;
        private final CreateUserRequest createUserRequest;

        CreateUserValidationResult(ErrorCollection errorCollection) {
            this(errorCollection, ImmutableList.of());
        }

        CreateUserValidationResult(ErrorCollection errorCollection, List<WebErrorMessage> passwordErrors) {
            super(errorCollection);
            this.createUserRequest = null;
            this.passwordErrors = ImmutableList.copyOf(passwordErrors);
            this.applicationKeys = Collections.emptySet();
        }

        CreateUserValidationResult(final String username,
                                   final String password, final String email, final String fullname) {
            this(username, password, email, fullname, null);
        }

        CreateUserValidationResult(final String username,
                                   final String password, final String email, final String fullname, Long directoryId) {
            this(username, password, email, fullname, directoryId, Collections.emptySet());
        }

        CreateUserValidationResult(final String username, final String password, final String email,
                                   final String fullname, final Long directoryId, @Nullable final Set<ApplicationKey> applicationKeys) {
            super(new SimpleErrorCollection());
            this.createUserRequest = CreateUserRequest
                    .withUserDetails(null, username, password, email, fullname)
                    .inDirectory(directoryId);
            this.passwordErrors = ImmutableList.of();
            this.applicationKeys = applicationKeys;
        }

        CreateUserValidationResult(final CreateUserRequest createUserRequest, final Set<ApplicationKey> applicationKeys,
                                   ErrorCollection errorCollection, List<WebErrorMessage> passwordErrors, final WarningCollection warningCollection) {
            super(errorCollection, warningCollection);
            this.passwordErrors = passwordErrors;
            this.applicationKeys = applicationKeys;
            this.createUserRequest = createUserRequest;
        }

        CreateUserRequest getCreateUserRequest() {
            return createUserRequest;
        }

        boolean hasCreateUserRequest() {
            return createUserRequest != null;
        }

        public String getUsername() {
            return createUserRequest == null ? null : createUserRequest.getUsername();
        }

        public String getPassword() {
            return createUserRequest == null ? null : createUserRequest.getPassword();
        }

        public String getEmail() {
            return createUserRequest == null ? null : createUserRequest.getEmailAddress();
        }

        public String getFullname() {
            return createUserRequest == null ? null : createUserRequest.getDisplayName();
        }

        public Long getDirectoryId() {
            return createUserRequest == null ? null : createUserRequest.getDirectoryId();
        }

        public List<WebErrorMessage> getPasswordErrors() {
            return passwordErrors;
        }

        public Set<ApplicationKey> getApplicationKeys() {
            if (applicationKeys == null) {
                return ImmutableSet.of();
            } else {
                return applicationKeys;
            }
        }
    }

    @PublicApi
    final class CreateUsernameValidationResult extends ServiceResultImpl {
        private final String username;
        private final Long directoryId;

        public CreateUsernameValidationResult(final String username, final Long directoryId, final ErrorCollection errorCollection) {
            super(errorCollection);
            this.username = username;
            this.directoryId = directoryId;
        }

        public String getUsername() {
            return username;
        }

        public Long getDirectoryId() {
            return directoryId;
        }
    }

    @PublicApi
    static final class UpdateUserValidationResult extends ServiceResultImpl {
        private final ApplicationUser user;

        UpdateUserValidationResult(ErrorCollection errorCollection) {
            super(errorCollection);
            user = null;
        }

        UpdateUserValidationResult(final ApplicationUser user) {
            super(new SimpleErrorCollection());
            this.user = user;
        }

        /**
         * @return the user
         * @deprecated Use {@link #getApplicationUser()} instead. Since v6.0.
         */
        public ApplicationUser getUser() {
            return user;
        }

        public ApplicationUser getApplicationUser() {
            return user;
        }
    }

    @PublicApi
    static final class DeleteUserValidationResult extends ServiceResultImpl {
        private final ApplicationUser user;

        DeleteUserValidationResult(ErrorCollection errorCollection) {
            super(errorCollection);
            user = null;
        }

        DeleteUserValidationResult(final ApplicationUser user) {
            super(new SimpleErrorCollection());
            this.user = user;
        }

        public ApplicationUser getUser() {
            return user;
        }

        public ApplicationUser getApplicationUser() {
            return user;
        }
    }

    @PublicApi
    static final class AddUserToApplicationValidationResult extends ServiceResultImpl {
        private final ApplicationUser userToAdd;
        private final Set<Group> applicationDefaultGroups;

        AddUserToApplicationValidationResult(ErrorCollection errorCollection) {
            super(errorCollection);
            userToAdd = null;
            applicationDefaultGroups = null;
        }

        AddUserToApplicationValidationResult(final ApplicationUser userToAdd, final Set<Group> applicationDefaultGroups) {
            super(new SimpleErrorCollection());
            this.userToAdd = userToAdd;
            this.applicationDefaultGroups = applicationDefaultGroups;
        }

        public ApplicationUser getUserToAdd() {
            return userToAdd;
        }

        public Set<Group> getApplicationDefaultGroups() {
            return applicationDefaultGroups;
        }
    }

    @PublicApi
    static final class RemoveUserFromApplicationValidationResult extends ServiceResultImpl {
        private final ApplicationUser userToRemove;
        private final Set<String> applicationGroupNames;

        RemoveUserFromApplicationValidationResult(ErrorCollection errorCollection) {
            super(errorCollection);
            userToRemove = null;
            applicationGroupNames = null;
        }

        RemoveUserFromApplicationValidationResult(final ApplicationUser userToRemove, final Set<String> applicationGroupNames) {
            super(new SimpleErrorCollection());
            this.userToRemove = userToRemove;
            this.applicationGroupNames = applicationGroupNames;
        }

        public ApplicationUser getUserToRemove() {
            return userToRemove;
        }

        public Set<String> getApplicationGroupNames() {
            return applicationGroupNames;
        }
    }

    @PublicApi
    static final class FieldName {
        private FieldName() {
        }

        /**
         * The default name of HTML fields containing a User's email. Validation methods on this service will return an
         * {@link com.atlassian.jira.util.ErrorCollection} with error messages keyed to this field name.
         */
        static String EMAIL = "email";
        /**
         * The default name of HTML fields containing a User's username. Validation methods on this service will return
         * an {@link com.atlassian.jira.util.ErrorCollection} with error messages keyed to this field name.
         */
        static String NAME = "username";
        /**
         * The default name of HTML fields containing a User's full name. Validation methods on this service will return
         * an {@link com.atlassian.jira.util.ErrorCollection} with error messages keyed to this field name.
         */
        static String FULLNAME = "fullname";
        /**
         * The default name of HTML fields containing a User's password. Validation methods on this service will return
         * an {@link com.atlassian.jira.util.ErrorCollection} with error messages keyed to this field name.
         */
        static String PASSWORD = "password";
        /**
         * The default name of HTML fields containing a User's password confirmation. Validation methods on this service
         * will return an {@link com.atlassian.jira.util.ErrorCollection} with error messages keyed to this field name.
         */
        static String CONFIRM_PASSWORD = "confirm";
    }

    /**
     * This request contains all the instructions and user details that should be used during user validation and user
     * creation.
     */
    @PublicApi
    final class CreateUserRequest {
        // The logged in user performing the user creation
        private final ApplicationUser loggedInUser;

        // required user details for new user
        private final String username;
        // null or empty makes random one to generate
        private final String password;
        // required user details for new user
        private final String displayName;
        // required user details for new user
        private final String emailAddress;
        // null directoryId represents default user directory
        private final Long directoryId;

        // Validation of confirm password optional, default = null
        private final String confirmPassword;
        // null indicates Default Application Access, default = null
        private final Set<ApplicationKey> applicationKeys;
        // Notifications are sent by default, default = true
        private final boolean sendNotification;
        // Is the password required
        private final boolean passwordRequired;
        // Should perform logged in user permission check
        private final boolean performPermissionCheck;
        // Indicates whether all validations should be skipped
        private final boolean skipValidation;
        private final int userEventType;

        private CreateUserRequest(@Nullable final ApplicationUser loggedInUser,
                                  final String username, @Nullable final String password,
                                  final String emailAddress, final String displayName,
                                  @Nullable final Long directoryId,
                                  @Nullable final String confirmPassword, @Nullable Set<ApplicationKey> applicationKeys,
                                  final boolean sendNotification,
                                  final boolean passwordRequired, final boolean performPermissionCheck, final boolean skipValidation,
                                  final int userEventType) {
            this.loggedInUser = loggedInUser;
            this.username = username;
            this.password = password;
            this.emailAddress = emailAddress;
            this.displayName = displayName;
            this.directoryId = directoryId;
            this.confirmPassword = confirmPassword;
            this.applicationKeys = applicationKeys;
            this.sendNotification = sendNotification;
            this.passwordRequired = passwordRequired;
            this.performPermissionCheck = performPermissionCheck;
            this.skipValidation = skipValidation;
            this.userEventType = userEventType;
        }

        /**
         * Create a new user creation request as the provided user for the specified user details. This request
         * indicates that the new users should be created in the default user directory and have access to the default
         * applications. A Notification would be sent after user creation.
         *
         * @param loggedInUser application user performing the create. If {@code null} the user creation is being
         *                     performed during setup or signup.
         * @param username     The username of the new user. Needs to be lowercase and unique. Required.
         * @param password     The password for the new user. If empty a random password will be generated.
         * @param emailAddress The email address for the new user. Required.
         * @param displayName  The display name for the new user. Required.
         */
        public static CreateUserRequest withUserDetails(@Nullable final ApplicationUser loggedInUser,
                                                        final String username, @Nullable final String password, final String emailAddress, final String displayName) {
            return new CreateUserRequest(loggedInUser, username, password, emailAddress, displayName, null,
                    null, null, true, false, true, false, UserEventType.USER_CREATED);
        }

        /**
         * Confirm that the provided password matches the confirm password provided.
         *
         * @param confirmPassword the confirm password that should match the provided password.
         */
        public CreateUserRequest confirmPassword(final String confirmPassword) {
            return new CreateUserRequest(loggedInUser, username, password, emailAddress, displayName, directoryId,
                    confirmPassword, applicationKeys, sendNotification, passwordRequired,
                    performPermissionCheck, skipValidation, userEventType);
        }

        /**
         * Indicate that no application access is required for the new user.
         */
        public CreateUserRequest withNoApplicationAccess() {
            return new CreateUserRequest(loggedInUser, username, password, emailAddress, displayName, directoryId,
                    confirmPassword, ImmutableSet.of(), sendNotification, passwordRequired,
                    performPermissionCheck, skipValidation, userEventType);
        }

        /**
         * Specify the applications that this user required access to.
         *
         * @param applicationKeys applications that user should be granted access to, an empty Set indicates that the
         *                        user does not require access to any applications.
         * @see #withNoApplicationAccess()
         */
        public CreateUserRequest withApplicationAccess(@Nonnull final Set<ApplicationKey> applicationKeys) {
            return new CreateUserRequest(loggedInUser, username, password, emailAddress, displayName, directoryId,
                    confirmPassword, applicationKeys, sendNotification, passwordRequired,
                    performPermissionCheck, skipValidation, userEventType);
        }

        /**
         * Specify whether a notification should be sent when the new user has successfully been created.
         *
         * @param sendNotification true if notification should be sent after a new user has successfully been created.
         *                         false if no notification should be sent.
         */
        public CreateUserRequest sendNotification(final boolean sendNotification) {
            return new CreateUserRequest(loggedInUser, username, password, emailAddress, displayName, directoryId,
                    confirmPassword, applicationKeys, sendNotification, passwordRequired,
                    performPermissionCheck, skipValidation, userEventType);
        }

        /**
         * Specify that a user signup event should be sent after user creation. Typically when the new user is being
         * created during signup.
         */
        public CreateUserRequest sendUserSignupEvent() {
            return new CreateUserRequest(loggedInUser, username, password, emailAddress, displayName, directoryId,
                    confirmPassword, applicationKeys, sendNotification, passwordRequired,
                    performPermissionCheck, skipValidation, UserEventType.USER_SIGNUP);
        }

        /**
         * Specify that the user password is required.
         */
        public CreateUserRequest passwordRequired() {
            return new CreateUserRequest(loggedInUser, username, password, emailAddress, displayName, directoryId,
                    confirmPassword, applicationKeys, sendNotification, true,
                    performPermissionCheck, skipValidation, userEventType);
        }

        /**
         * Indicate that the permission check should not be performed on the logged in user to determine whether the
         * logged in user has permission to create the new user.
         *
         * @param performPermissionCheck true if the permission check should be performed else false to skip the
         *                               permission check.
         */
        public CreateUserRequest performPermissionCheck(boolean performPermissionCheck) {
            return new CreateUserRequest(loggedInUser, username, password, emailAddress, displayName, directoryId,
                    confirmPassword, applicationKeys, sendNotification, passwordRequired,
                    performPermissionCheck, skipValidation, userEventType);
        }

        /**
         * Specify what user directory should be used for new user.
         *
         * @param directoryId The directory which the new user is intended to be created in. {@code null} indicates that
         *                    the default directory should be used.
         */
        public CreateUserRequest inDirectory(final Long directoryId) {
            return new CreateUserRequest(loggedInUser, username, password, emailAddress, displayName, directoryId,
                    confirmPassword, applicationKeys, sendNotification, passwordRequired,
                    performPermissionCheck, skipValidation, userEventType);
        }

        /**
         * Specify that all validations should be skipped. This should only be used when it does not matter that user
         * gets created or added.
         */
        @Internal
        public CreateUserRequest skipValidation() {
            return new CreateUserRequest(loggedInUser, username, password, emailAddress, displayName, directoryId,
                    confirmPassword, applicationKeys, sendNotification, passwordRequired,
                    performPermissionCheck, true, userEventType);
        }

        /**
         * This method has been made public only to allow temporary backwards compatibility and will be removed in 7.0.
         * <p>
         * Set the event type to be dispatch after user creation.
         * This has been added for compatibility reasons please avoid using this.
         *
         * @param userEventType the event type that should be sent after user has been created.
         * @see UserEventType
         * @deprecated Since v7.0.
         */
        @Internal
        public CreateUserRequest withEventUserEvent(final int userEventType) {
            return new CreateUserRequest(loggedInUser, username, password, emailAddress, displayName, directoryId,
                    confirmPassword, applicationKeys, sendNotification, passwordRequired,
                    performPermissionCheck, skipValidation, userEventType);
        }

        /**
         * Determine whether the user password is required.
         *
         * @return true is user password is required else faslse.
         */
        public boolean requirePassword() {
            return passwordRequired;
        }

        /**
         * Return the user name that is used to identify the new user.
         *
         * @return the user name that is used to identify the new user.
         */
        public String getUsername() {
            return username;
        }

        /**
         * Return the password for the new user.
         *
         * @return the password for the new user.
         */
        public String getPassword() {
            return password;
        }

        /**
         * Return the display name (also known as full name) for the new user.
         *
         * @return the display name (also known as full name) for the new user.
         */
        public String getDisplayName() {
            return displayName;
        }

        /**
         * Return the email address for the new user.
         *
         * @return the email address for the new user.
         */
        public String getEmailAddress() {
            return emailAddress;
        }

        /**
         * Determine whether the default user directory should be used for the new user.
         */
        public boolean defaultDirectory() {
            return directoryId == null;
        }

        /**
         * Return the directory id (representing the user directory), that the new user should be created in. This is
         * typically left unset {@code null} indicating default.
         *
         * @return the directory id (representing the user directory), that the new user should be created in.
         */
        public Long getDirectoryId() {
            return directoryId;
        }

        /**
         * Determine whether the confirmation password should be validation or whether the confirmation password has
         * been set.
         *
         * @return true if the {@link #confirmPassword(String)} should be compared against the {@link #getPassword()}.
         */
        public boolean shouldConfirmPassword() {
            return confirmPassword != null;
        }

        /**
         * Return the confirmation password that should be compared against the {@link #getPassword()}. This is
         * typically set by {@link #confirmPassword(String)} to indicate that the confirm password should be validated
         * against the {@link #getPassword()}.
         *
         * @return the confirmation password that should be compared against the {@link #getPassword()}.
         */
        public String getConfirmPassword() {
            return confirmPassword;
        }

        /**
         * Determine whether the new user requires default application access. Default application access is the
         * configuration that indicates what applications a new user should be added to.
         *
         * @return true if the new user should be granted access to the default applications else false.
         */
        public boolean requireDefaultApplicationAccess() {
            return applicationKeys == null;
        }

        /**
         * Return the application keys that are used to specify what application access the new user requires.
         *
         * @return the application keys that are used to specify what application access the new user requires.
         * @see CreateUserRequest#requireDefaultApplicationAccess()
         */
        public Set<ApplicationKey> getApplicationKeys() {
            return applicationKeys;
        }

        /**
         * Determine whether a notification should be sent after user creation.
         *
         * @return true if a notification should be sent after user creation. false if a notification should not be sent
         * after user creation.
         * @see com.atlassian.jira.bc.user.UserService.CreateUserRequest#sendNotification(boolean)
         */
        public boolean shouldSendNotification() {
            return sendNotification;
        }

        /**
         * Determine whether a signup event should be sent after user has been created. This is typically true when the
         * user is being created via signup.
         *
         * @return true if a signup event should be sent after user has been created.
         */
        public boolean shouldSendUserSignupEvent() {
            return userEventType == UserEventType.USER_SIGNUP;
        }

        /**
         * Return the logged in user that would be performing the user creation.
         *
         * @return the application user performing the user creation.
         */
        public ApplicationUser getLoggedInUser() {
            return loggedInUser;
        }

        public boolean shouldPerformPermissionCheck() {
            return performPermissionCheck;
        }

        /**
         * Indicate that validation should be skipped.
         * This would only set the required values as if validation was successful.
         * This should only be performed when error messages not applicable.
         *
         * @return true if the validation should be skipped else false.
         */
        @Internal
        boolean shouldSkipValidation() {
            return skipValidation;
        }

        /**
         * Get user event type.
         *
         * @return the user event type.
         * @see UserEventType
         */
        @Internal
        int getUserEventType() {
            return userEventType;
        }
    }
}
