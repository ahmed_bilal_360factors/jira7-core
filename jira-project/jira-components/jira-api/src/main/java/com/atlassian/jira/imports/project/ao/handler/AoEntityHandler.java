package com.atlassian.jira.imports.project.ao.handler;

import com.atlassian.jira.exception.ParseException;
import com.atlassian.jira.imports.project.handler.AbortImportException;

import java.util.Map;

/**
 * Base SAX parser entity handler interface for AO data.
 *
 * @Internal
 * @since v7.0
 */
public interface AoEntityHandler {
    /**
     * This is the main method to implement when using this ImportEntityHandler. This method will provide the
     * entity name and a complete map of attribute key/value pairs. This includes any nested element tags that
     * will have CDATA bodies.
     *
     * @param entityName identifies the entity (i.e. Issue)
     * @param attributes complete list of the attributes listed in the XML element including the nested
     *                   elements.
     * @throws com.atlassian.jira.exception.ParseException                     if the entity is invalid a ParseException will be thrown.
     * @throws com.atlassian.jira.imports.project.handler.AbortImportException to indicate to Project Import
     *                                                                         that it should abort its SAX parsing.
     */
    void handleEntity(String entityName, Map<String, Object> attributes) throws ParseException, AbortImportException;

    /**
     * Return true if the handler should handle this entity.
     *
     * @param entityName
     * @return true if the handler should handle this entity.
     */
    boolean handlesEntity(String entityName);

    /**
     * Provides the implementation an opportunity to perform some action when the document is starting to
     * be read.
     */
    void startDocument();

    /**
     * Provides the implementation an opportunity to perform some action when the document is finished being read.
     */
    void endDocument();

    /**
     * Provides the implementation an opportunity to perform some action once all rows of a table are processed.
     *
     * @param tableName
     */
    void endTable(String tableName);
}
