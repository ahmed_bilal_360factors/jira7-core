package com.atlassian.jira.bc.project;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.template.ProjectTemplateKey;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.user.ApplicationUser;

import static java.util.Optional.ofNullable;

/**
 * Holds the information required to create a project. To create instances of this class, use {@link
 * com.atlassian.jira.bc.project.ProjectCreationData.Builder}
 *
 * @since 7.0
 */
@PublicApi
public class ProjectCreationData {
    private final String name;
    private final String key;
    private final String description;
    private final ProjectTypeKey projectTypeKey;
    private final ProjectTemplateKey projectTemplateKey;
    private final ApplicationUser lead;
    private final String url;
    private final Long assigneeType;
    private final Long avatarId;

    private ProjectCreationData(
            String name,
            String key,
            String description,
            ProjectTypeKey projectTypeKey,
            ProjectTemplateKey projectTemplateKey,
            ApplicationUser lead,
            String url,
            Long assigneeType,
            Long avatarId) {
        this.name = name;
        this.key = key;
        this.description = description;
        this.projectTypeKey = projectTypeKey;
        this.projectTemplateKey = projectTemplateKey;
        this.lead = lead;
        this.url = url;
        this.assigneeType = assigneeType;
        this.avatarId = avatarId;
    }

    public String getName() {
        return name;
    }

    public String getKey() {
        return key;
    }

    public String getDescription() {
        return description;
    }

    public ProjectTypeKey getProjectTypeKey() {
        return projectTypeKey;
    }

    public ProjectTemplateKey getProjectTemplateKey() {
        return projectTemplateKey;
    }

    public ApplicationUser getLead() {
        return lead;
    }

    public String getUrl() {
        return url;
    }

    public Long getAssigneeType() {
        return assigneeType;
    }

    public Long getAvatarId() {
        return avatarId;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final ProjectCreationData that = (ProjectCreationData) o;

        if (assigneeType != null ? !assigneeType.equals(that.assigneeType) : that.assigneeType != null) {
            return false;
        }
        if (avatarId != null ? !avatarId.equals(that.avatarId) : that.avatarId != null) {
            return false;
        }
        if (description != null ? !description.equals(that.description) : that.description != null) {
            return false;
        }
        if (key != null ? !key.equals(that.key) : that.key != null) {
            return false;
        }
        if (lead != null ? !lead.equals(that.lead) : that.lead != null) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        if (projectTypeKey != null ? !projectTypeKey.equals(that.projectTypeKey) : that.projectTypeKey != null) {
            return false;
        }
        if (projectTemplateKey != null ? !projectTemplateKey.equals(that.projectTemplateKey) : that.projectTemplateKey != null) {
            return false;
        }
        if (url != null ? !url.equals(that.url) : that.url != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (key != null ? key.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (projectTypeKey != null ? projectTypeKey.hashCode() : 0);
        result = 31 * result + (projectTemplateKey != null ? projectTemplateKey.hashCode() : 0);
        result = 31 * result + (lead != null ? lead.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (assigneeType != null ? assigneeType.hashCode() : 0);
        result = 31 * result + (avatarId != null ? avatarId.hashCode() : 0);
        return result;
    }

    /**
     * Builder for {@link com.atlassian.jira.bc.project.ProjectCreationData}
     */
    public static class Builder {
        private String name;
        private String key;
        private String description;
        private ProjectTypeKey projectTypeKey;
        private ProjectTemplateKey projectTemplateKey;
        private ApplicationUser lead;
        private String url;
        private Long assigneeType;
        private Long avatarId;

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Builder withKey(String key) {
            this.key = key;
            return this;
        }

        public Builder withDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder withType(String projectTypeKey) {
            return withType(projectTypeKey == null ? null : new ProjectTypeKey(projectTypeKey));
        }

        public Builder withType(ProjectTypeKey projectTypeKey) {
            this.projectTypeKey = projectTypeKey;
            return this;
        }

        public Builder withProjectTemplateKey(String projectTemplateKey) {
            this.projectTemplateKey = projectTemplateKey == null ? null : new ProjectTemplateKey(projectTemplateKey);
            return this;
        }

        public Builder withLead(ApplicationUser lead) {
            this.lead = lead;
            return this;
        }

        public Builder withUrl(String url) {
            this.url = url;
            return this;
        }

        public Builder withAssigneeType(Long assigneeType) {
            this.assigneeType = assigneeType;
            return this;
        }

        public Builder withAvatarId(Long avatarId) {
            this.avatarId = avatarId;
            return this;
        }

        public ProjectCreationData build() {
            return new ProjectCreationData(name, key, description, projectTypeKey, projectTemplateKey, lead, url, assigneeType, avatarId);
        }

        public Builder from(ProjectCreationData data) {
            this.name = data.getName();
            this.key = data.getKey();
            this.description = data.getDescription();
            this.projectTypeKey = data.getProjectTypeKey();
            this.projectTemplateKey = data.getProjectTemplateKey();
            this.lead = data.getLead();
            this.url = data.getUrl();
            this.assigneeType = data.getAssigneeType();
            this.avatarId = data.getAvatarId();
            return this;
        }

        /**
         * Creates a new instance of ProjectCreationData using a combination of the existing project's data
         * as well as the input provided
         * <p>
         * The following rules will be applied:
         * <ul>
         * <li>projectTypeKey - always taken from the existing project</li>
         * <li>projectTemplateKey - always set to null</li>
         * <li>name - taken from user input since it has to be different than the existing project</li>
         * <li>key - taken from user input since it has to be different than the existing project</li>
         * <li>description - taken from given creation data</li>
         * <li>url - taken from given creation data</li>
         * <li>avatarId - taken from given creation data</li>
         * <li>lead - taken from given creation data or existing project if not provided</li>
         * <li>assigneeType - taken given creation data or existing project if not provided</li>
         * </ul>
         *
         * @param existingProject The existing project to take creation data from
         * @param input           The input provided by the admin user
         * @return A builder representing data using the rules stated above
         */
        public Builder fromExistingProject(Project existingProject, ProjectCreationData input) {
            this.projectTypeKey = existingProject.getProjectTypeKey();
            this.projectTemplateKey = null;

            this.name = input.getName();
            this.key = input.getKey();
            this.description = input.getDescription();
            this.url = input.getUrl();
            this.avatarId = input.getAvatarId();

            this.lead = ofNullable(input.getLead()).orElse(existingProject.getProjectLead());
            this.assigneeType = ofNullable(input.getAssigneeType()).orElse(existingProject.getAssigneeType());
            return this;
        }
    }
}
