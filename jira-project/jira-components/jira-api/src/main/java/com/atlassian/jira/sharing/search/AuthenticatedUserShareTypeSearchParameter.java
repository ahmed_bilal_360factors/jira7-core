package com.atlassian.jira.sharing.search;

import com.atlassian.jira.sharing.type.ShareType;

/**
 * Represents the search parameters when searching for AUTHENTICATED USER ShareTypes.
 *
 * @since v7.2.2
 */
public class AuthenticatedUserShareTypeSearchParameter extends AbstractShareTypeSearchParameter {
    public static final AuthenticatedUserShareTypeSearchParameter AUTHENTICATED_USER_PARAMETER = new AuthenticatedUserShareTypeSearchParameter();

    private AuthenticatedUserShareTypeSearchParameter() {
        super(ShareType.Name.AUTHENTICATED);
    }
}
