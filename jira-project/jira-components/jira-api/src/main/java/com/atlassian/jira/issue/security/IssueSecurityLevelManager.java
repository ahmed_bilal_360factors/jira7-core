package com.atlassian.jira.issue.security;

import com.atlassian.annotations.Internal;
import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.List;

@PublicApi
public interface IssueSecurityLevelManager {
    /**
     * Returns the list of Security Levels for the given Issue Security Level Scheme.
     *
     * @param schemeId ID of the Issue Security Level Scheme.
     * @return the list of Security Levels for the given Issue Security Level Scheme.
     * @deprecated Use {@link #getIssueSecurityLevels(long)} instead. Since v5.0.
     */
    List<GenericValue> getSchemeIssueSecurityLevels(Long schemeId);

    /**
     * Returns the list of Security Levels for the given Issue Security Level Scheme.
     * The elements are ordered by name.
     *
     * @param schemeId ID of the Issue Security Level Scheme.
     * @return the list of Security Levels for the given Issue Security Level Scheme.
     * @since v5.0
     */
    List<IssueSecurityLevel> getIssueSecurityLevels(long schemeId);

    /**
     * @param id IssueSecurityLevel ID
     * @return true if exists
     * @deprecated Use {@link #getSecurityLevel(long)} != null instead. Since v5.0.
     */
    @SuppressWarnings({"UnusedDeclaration"})
    boolean schemeIssueSecurityExists(Long id);

    String getIssueSecurityName(Long id);

    String getIssueSecurityDescription(Long id);

    /**
     * Returns the IssueSecurityLevel with the given ID.
     *
     * @param id the ID
     * @return the IssueSecurityLevel with the given ID. May be null if you pass an invalid id.
     * @deprecated Use {@link #getSecurityLevel(long)} instead. Since v5.0.
     */
    @Nullable
    GenericValue getIssueSecurity(@Nonnull Long id);

    /**
     * Returns the IssueSecurityLevel with the given ID.
     *
     * @param id the ID
     * @return the IssueSecurityLevel with the given ID.
     * @since v5.0
     */
    IssueSecurityLevel getSecurityLevel(long id);


    /**
     * Updates the name and/or description of security level.
     *
     * @param issueSecurityLevel the level to update
     */
    IssueSecurityLevel updateIssueSecurityLevel(final IssueSecurityLevel issueSecurityLevel);


    /**
     * Creates an Issue Security Level with the given properties.
     *
     * @param schemeId    The Issue security scheme that this level belongs to.
     * @param name        The name of the new level
     * @param description an optional description
     * @return the newly created Issue Security Level
     */
    IssueSecurityLevel createIssueSecurityLevel(long schemeId, String name, String description);


    /**
     * Creates an Issue Security Level with the same properties as in provided Issue Security Level.
     *
     * @param level The Issue security level to be created.
     * @return the newly created Issue Security Level
     */
    IssueSecurityLevel createIssueSecurityLevel(IssueSecurityLevel level);

    /**
     * Get the different levels of security that can be set for this issue.
     *
     * @param entity This is the issue or the project that the security is being checked for
     * @param user   The user used for the security check
     * @return list containing the security levels, can be null
     * @throws GenericEntityException Exception in the OFBiz persistence layer.
     * @deprecated Use {@link #getUsersSecurityLevels(Issue, ApplicationUser)}
     * or{@link #getUsersSecurityLevels(Project, ApplicationUser)} instead. Since v5.0.
     */
    List<GenericValue> getUsersSecurityLevels(GenericValue entity, ApplicationUser user) throws GenericEntityException;

    /**
     * Get the different levels of security that can be set for this issue.
     * If you are creating a new Issue, then use {@link #getUsersSecurityLevels(com.atlassian.jira.project.Project, ApplicationUser)}.
     *
     * @param issue This is the issue that the security is being checked for
     * @param user  The user used for the security check
     * @return list containing the security levels, can be null
     * @see #getUsersSecurityLevels(com.atlassian.jira.project.Project, ApplicationUser)
     * @since v5.0
     */
    List<IssueSecurityLevel> getUsersSecurityLevels(Issue issue, ApplicationUser user);

    /**
     * Get the different levels of security that can be set for an issue created in this project.
     * If you are editing an existing Issue, then use {@link #getUsersSecurityLevels(Issue, ApplicationUser)}.
     *
     * @param project the project that the security is being checked for
     * @param user    The user used for the security check
     * @return list containing the security levels, can be null
     * @see #getUsersSecurityLevels(com.atlassian.jira.issue.Issue, ApplicationUser)
     * @since v5.0
     */
    List<IssueSecurityLevel> getUsersSecurityLevels(Project project, ApplicationUser user);

    /**
     * Get the different levels of security that the user can see across all projects.
     *
     * @param user The user used for the security check
     * @return list containing the security levels, can be null
     * @throws GenericEntityException Exception in the OFBiz persistence layer.
     * @since v4.0
     * @deprecated Use {@link #getAllSecurityLevelsForUser(ApplicationUser)} instead. Since v5.0.
     */
    Collection<GenericValue> getAllUsersSecurityLevels(ApplicationUser user) throws GenericEntityException;

    /**
     * Get the different levels of security that the user can see across all projects.
     *
     * @param user The user used for the security check
     * @return list containing the security levels
     * @since v5.0
     */
    @Nonnull
    Collection<IssueSecurityLevel> getAllSecurityLevelsForUser(final ApplicationUser user);

    /**
     * Get all the different levels of security across all schemes.
     *
     * @return list containing the security levels, can be null
     * @throws GenericEntityException Exception in the OFBiz persistence layer.
     * @since v4.0
     * @deprecated Use {@link #getAllIssueSecurityLevels()} instead. Since v5.0.
     */
    Collection<GenericValue> getAllSecurityLevels() throws GenericEntityException;

    /**
     * Get all the different levels of security across all schemes.
     *
     * @return list containing the security levels, can be null
     * @since v5.0
     */
    Collection<IssueSecurityLevel> getAllIssueSecurityLevels();

    /**
     * Get the different levels of security that a user can see that have the specified name.
     *
     * @param user              the user
     * @param securityLevelName the name of the security level.
     * @return a collection of the GenericValues representing each level they can see with the specified name.
     * @throws GenericEntityException Exception in the OFBiz persistence layer.
     * @since v4.0
     * @deprecated Use {@link #getSecurityLevelsForUserByName(ApplicationUser, String)} instead. Since v5.0.
     */
    Collection<GenericValue> getUsersSecurityLevelsByName(ApplicationUser user, String securityLevelName) throws GenericEntityException;

    /**
     * Get the different levels of security that a user can see that have the specified name.
     *
     * @param user              the user
     * @param securityLevelName the name of the security level.
     * @return a collection of each IssueSecurityLevel they can see with the specified name.
     * @since v5.0
     */
    Collection<IssueSecurityLevel> getSecurityLevelsForUserByName(ApplicationUser user, String securityLevelName);

    /**
     * Get the different levels of security that have the specified name.
     *
     * @param securityLevelName the name of the security level.
     * @return a collection of the GenericValues representing each level with the specified name.
     * @throws GenericEntityException Exception in the OFBiz persistence layer.
     * @since v4.0
     * @deprecated Use {@link #getIssueSecurityLevelsByName(String)} instead. Since v5.0.
     */
    Collection<GenericValue> getSecurityLevelsByName(String securityLevelName) throws GenericEntityException;

    /**
     * Get the different levels of security that have the specified name.
     *
     * @param securityLevelName the name of the security level.
     * @return a collection of the IssueSecurityLevels with the specified name.
     * @since v5.0
     */
    Collection<IssueSecurityLevel> getIssueSecurityLevelsByName(String securityLevelName);


    /**
     * Get the level with specified name and schemaId.
     *
     * @param securityLevelName the name of the security level.
     * @param schemaId          the id of security schema.
     * @return the IssueSecurityLevel with the specified name and security level if was found, null otherwise.
     * @since v5.0
     */
    IssueSecurityLevel getSecurityLevelByNameAndSchema(String securityLevelName, Long schemaId);

    /**
     * Returns the default Security Level as defined in the Issue Security Level scheme for the given project.
     *
     * @param project the Project
     * @return the default Security Level as defined in the Issue Security Level scheme for the given project. Can be null.
     * @throws GenericEntityException Exception in the OFBiz persistence layer.
     * @deprecated Use {@link #getDefaultSecurityLevel(com.atlassian.jira.project.Project)} instead. Since v5.0.
     */
    Long getSchemeDefaultSecurityLevel(GenericValue project) throws GenericEntityException;

    /**
     * Returns the default Security Level as defined in the Issue Security Level scheme for the given project.
     *
     * @param project the Project
     * @return the default Security Level as defined in the Issue Security Level scheme for the given project. Can be null.
     * @since v5.0
     */
    Long getDefaultSecurityLevel(Project project);

    /**
     * Returns the IssueSecurityLevel with the given ID.
     *
     * @param id the ID
     * @return the IssueSecurityLevel with the given ID.
     * @throws GenericEntityException Exception in the OFBiz persistence layer.
     * @deprecated Use {@link #getSecurityLevel(long)} instead. Since v5.0.
     */
    GenericValue getIssueSecurityLevel(Long id) throws GenericEntityException;

    /**
     * Deletes the given Issue Security Level and any child permissions.
     *
     * @param levelId Issue Security Level ID
     */
    void deleteSecurityLevel(Long levelId);

    @Internal
    void clearUsersLevels();

    @Internal
    void clearProjectLevels(Project project);

    /**
     * Get the count of issues at the given security level for a project.
     *
     * @param issueSecurityLevelId Issue Security Level
     * @param projectId            Project
     * @return count of issues at the given security level for a project.
     * @since V7.1.1
     */
    Long getIssueCount(@Nullable Long issueSecurityLevelId, @Nonnull final Long projectId);
}
