package com.atlassian.jira.issue.priority;

import com.atlassian.annotations.Internal;
import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.issue.IssueConstant;

import javax.annotation.Nullable;

/**
 * Represents priority of the issue. An issue's priority is its importance in relation to other issues.
 */
@PublicApi
public interface Priority extends IssueConstant {
    /**
     * Get status color of this priority.
     *
     * @return String describing status color of this priority.
     */
    String getStatusColor();

    /**
     * Get URL of the SVG icon.
     *
     * @return URL of the SVG icon.
     */
    @Internal
    @Nullable
    String getSvgIconUrl();

    /**
     * Get URL of the raster icon.
     *
     * @return URL of the raster icon.
     */
    @Internal
    @Nullable
    String getRasterIconUrl();
}
