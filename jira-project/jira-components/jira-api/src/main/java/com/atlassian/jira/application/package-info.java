/**
 * Package for JIRA Application API.
 *
 * @since 7.0
 */
@ParametersAreNonnullByDefault package com.atlassian.jira.application;

import javax.annotation.ParametersAreNonnullByDefault;