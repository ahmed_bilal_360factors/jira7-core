package com.atlassian.jira.imports.project.handler;

import com.atlassian.annotations.ExperimentalSpi;

/**
 * Parent interface for plugins to perform actions during the input process.
 *
 * @since v7.0
 */
@ExperimentalSpi
public interface PluggableImportRunnable extends PluggableImportHandler {
    /**
     * This will be called at the time defined by the plugin point.
     */
    void run();

}
