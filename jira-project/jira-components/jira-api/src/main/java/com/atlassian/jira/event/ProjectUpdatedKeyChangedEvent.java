package com.atlassian.jira.event;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.annotations.Internal;
import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.web.action.RequestSourceType;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Event detailing changes in a project key.
 *
 * @since v7.2
 */
@PublicApi
@ParametersAreNonnullByDefault
@EventName("jira.administration.projectdetails.updated.key")
public class ProjectUpdatedKeyChangedEvent extends AbstractEvent {
    private String requestSourceType;

    @Internal
    public ProjectUpdatedKeyChangedEvent(final RequestSourceType requestSourceType) {
        this.requestSourceType = requestSourceType.getType();
    }

    public String getRequestSourceType() {
        return requestSourceType;
    }
}
