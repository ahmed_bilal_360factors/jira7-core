package com.atlassian.jira.user.util;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.annotations.Internal;
import com.atlassian.annotations.PublicApi;
import com.atlassian.application.api.ApplicationKey;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.bc.user.UserService;
import com.atlassian.jira.bc.user.UserService.AddUserToApplicationValidationResult;
import com.atlassian.jira.bc.user.UserService.CreateUserValidationResult;
import com.atlassian.jira.exception.AddException;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.license.LicenseCountService;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserDetails;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Set;
import java.util.SortedSet;


/**
 * This is a back end service level interface that defines an API for user level operations.
 * <p>
 * In general, appropriate managers or services should be used in preference to this class, as it will be removed in
 * some future revision of JIRA.
 *
 * @see UserService
 * @see UserManager
 * @see com.atlassian.jira.bc.group.GroupService
 * @see com.atlassian.jira.security.groups.GroupManager
 */
@PublicApi
public interface UserUtil {
    public static final String META_PROPERTY_PREFIX = "jira.meta.";

    /**
     * Returns the total number of users defined in JIRA, regardless of whether they are active or not.
     *
     * @return the total number of users defined in JIRA
     * @since v4.0
     * @deprecated Use {@link UserManager#getTotalUserCount()} instead. Since 7.0.
     */
    @Deprecated
    int getTotalUserCount();

    /**
     * Clears the cache of the active user count so that it can be recalculated. This method should be used when
     * performing operations that will modify the number of active users in the system.
     *
     * @since v3.13
     * @deprecated since 6.5. Use {@link com.atlassian.jira.license.LicenseCountService#flush()} instead.
     */
    @Deprecated
    void clearActiveUserCount();

    /**
     * Returns the all users defined in JIRA, regardless of whether they are active or not.
     *
     * @return the set of all users defined in JIRA
     * @since v4.3
     * @deprecated Use {@link com.atlassian.jira.user.util.UserManager#getAllApplicationUsers()} instead. Since v6.5.
     */
    @Deprecated
    @Nonnull
    Collection<ApplicationUser> getUsers();

    /**
     * Returns the all users defined in JIRA, regardless of whether they are active or not.
     *
     * @return the set of all users defined in JIRA
     * @since v6.0
     * @deprecated Use {@link com.atlassian.jira.user.util.UserManager#getAllApplicationUsers()} instead. Since v6.5.
     */
    @Deprecated
    @Nonnull
    Collection<ApplicationUser> getAllApplicationUsers();

    /**
     * Returns a {@link Group} based on group name.
     *
     * @param groupName the user name of the group
     * @return the Group object, or null if the user cannot be found including null groupName.
     * @since v4.0
     * @deprecated Since v4.3. Use {@link com.atlassian.jira.security.groups.GroupManager#getGroup(java.lang.String)} instead.
     */
    @Deprecated
    @Nullable
    Group getGroup(@Nullable String groupName);

    /**
     * Returns a {@link Group} based on group name.
     *
     * @param groupName the user name of the group
     * @return the Group object, or null if the user cannot be found including null groupName.
     * @since v4.3
     * @deprecated since 6.5. Use {@link com.atlassian.jira.security.groups.GroupManager#getGroup(java.lang.String)} instead.
     */
    @Deprecated
    @Nullable
    Group getGroupObject(@Nullable String groupName);

    /**
     * Creates a User from supplied details.
     * <p>
     * Email notification will be send to created user.
     *
     * @param username      The username of the new user. Needs to be lowercase and unique.
     * @param password      The password for the new user.
     * @param email         The email for the new user.  Needs to be a valid email address.
     * @param fullname      The full name for the new user
     * @param userEventType The event type dispatched on user creation. Either {@link
     *                      com.atlassian.jira.event.user.UserEventType#USER_CREATED} or {@link com.atlassian.jira.event.user.UserEventType#USER_SIGNUP}
     * @return The new user object that was created
     * @throws PermissionException If the operation was not permitted.
     * @since 4.3
     * @deprecated Use {@link UserService#createUser(CreateUserValidationResult)} instead. Since v7.0.
     */
    @Deprecated
    @Nonnull
    ApplicationUser createUserWithNotification(String username, String password, String email, String fullname, int userEventType)
            throws PermissionException, CreateException;

    /**
     * Creates a User from supplied details.
     * <p>
     * Email notification will be send to created user.
     *
     * @param username      The username of the new user. Needs to be lowercase and unique.
     * @param password      The password for the new user.
     * @param email         The email for the new user.  Needs to be a valid email address.
     * @param fullname      The full name for the new user
     * @param directoryId   The directory to create the user in. Null means "first writable directory".
     * @param userEventType The event type dispatched on user creation. Either {@link
     *                      com.atlassian.jira.event.user.UserEventType#USER_CREATED} or {@link com.atlassian.jira.event.user.UserEventType#USER_SIGNUP}
     * @return The new user object that was created
     * @throws PermissionException If the operation was not permitted.
     * @since 4.3.2
     * @deprecated Use {@link UserService#createUser(CreateUserValidationResult)} instead. Since v7.0.
     */
    @Deprecated
    @Nonnull
    ApplicationUser createUserWithNotification(String username, String password, String email, String fullname, Long directoryId, int userEventType)
            throws PermissionException, CreateException;

    /**
     * Creates a User from supplied details.
     * <p>
     * No email notification will be send to created user.
     *
     * @param username     The username of the new user. Needs to be lowercase and unique.
     * @param password     The password for the new user.
     * @param emailAddress The email for the new user.  Needs to be a valid email address.
     * @param displayName  The display name for the new user
     * @return The new user object that was created
     * @throws PermissionException If the operation was not permitted.
     * @since 4.3
     * @deprecated Use {@link UserService#createUser(CreateUserValidationResult)} instead. Since v7.0.
     */
    @Deprecated
    @Nonnull
    ApplicationUser createUserNoNotification(String username, String password, String emailAddress, String displayName)
            throws PermissionException, CreateException;

    /**
     * Creates a User from supplied details.
     * <p>
     * No email notification will be send to created user.
     *
     * @param username     The username of the new user. Needs to be lowercase and unique.
     * @param password     The password for the new user.
     * @param emailAddress The email for the new user.  Needs to be a valid email address.
     * @param displayName  The display name for the new user
     * @param directoryId  The directory to create the user in. Null means "first writable directory".
     * @return The new user object that was created
     * @throws PermissionException If the operation was not permitted.
     * @since 4.3.2
     * @deprecated Use {@link UserService#createUser(CreateUserValidationResult)} instead. Since v7.0.
     */
    @Deprecated
    @Nonnull
    ApplicationUser createUserNoNotification(String username, String password, String emailAddress, String displayName, Long directoryId)
            throws PermissionException, CreateException;

    /**
     * This method is being removed - Please use {@link UserService}.
     * Creates a JIRA user.
     *
     * @param userData        user request containing user details.
     * @param sendEmail       should send email notification.
     * @param eventType       notification event type (see to {@link com.atlassian.jira.event.user.UserEventType}).
     * @param applicationKeys applications that user should be granted access to,
     *                        {@code null} if instance default applications should be used,
     *                        empty Set when user does not require access to any applications.
     * @return The newly created user.
     * @throws PermissionException If the operation was not permitted.
     * @throws CreateException     Unable to create user.
     * @since v7.0
     * @deprecated Use {@link UserService#createUser(CreateUserValidationResult)} instead. Since v7.0.
     */
    @Nonnull
    @Internal
    @ExperimentalApi
    @Deprecated
    ApplicationUser createUser(@Nonnull final UserDetails userData, final boolean sendEmail,
                               final int eventType, @Nullable Set<ApplicationKey> applicationKeys)
            throws PermissionException, CreateException;

    /**
     * This will remove the user and removes the user from all the groups. All components lead by user will have lead
     * cleared.
     *
     * @param loggedInUser the user performing operation
     * @param user         the user to delete
     */
    void removeUser(ApplicationUser loggedInUser, ApplicationUser user);

    /**
     * This is used to add a specified user to a specified group. The user will be added to the group if the user is not
     * already a member of the group.
     *
     * @param group     the group to add the user to.
     * @param userToAdd the user to add to the group.
     */
    void addUserToGroup(Group group, ApplicationUser userToAdd) throws PermissionException, AddException;

    /**
     * This is used to add a user to many groups at once.
     *
     * @param groups    a list containing the groups to add the user to.
     * @param userToAdd the user to add to the group.
     */
    void addUserToGroups(Collection<Group> groups, ApplicationUser userToAdd) throws PermissionException, AddException;

    /**
     * This is used to remove a specified user from a specified group. The user will be removed from the group only if
     * the user is already a member of the group.
     *
     * @param group        the group to add the user to.
     * @param userToRemove the user to add to the group.
     */
    void removeUserFromGroup(Group group, ApplicationUser userToRemove) throws PermissionException, RemoveException;

    /**
     * This is used to remove a user from many groups at once.
     *
     * @param groups       a list containing the groups to add the user to.
     * @param userToRemove the user to add to the group.
     */
    void removeUserFromGroups(Collection<Group> groups, ApplicationUser userToRemove) throws PermissionException, RemoveException;

    /**
     * This is used to generate a reset password token that last a certain time and allows a person to access a page
     * anonymously so they can reset their password.
     * <p>
     * The generated token will be associated with the named user so that that this information can be verified at a
     * later time.
     *
     * @param user the user in question.  This MUST not be null
     * @return a newly generated token that will live for a certain time
     */
    PasswordResetToken generatePasswordResetToken(ApplicationUser user);

    interface PasswordResetToken {
        /**
         * @return the user that the password reset token is associated with
         */
        ApplicationUser getUser();

        /**
         * @return the unique token that will be associated with a user
         */
        String getToken();

        /**
         * @return how long before the token expires, in hours
         */
        int getExpiryHours();

        /**
         * @return the time in UTC milliseconds at which the token will expire
         */
        long getExpiryTime();
    }

    /**
     * This can be called to validate a token against the user.
     *
     * @param user  the user in play
     * @param token the token they provided
     * @return a Validation object that describes how the option went
     */
    PasswordResetTokenValidation validatePasswordResetToken(ApplicationUser user, String token);

    interface PasswordResetTokenValidation {

        enum Status {
            EXPIRED, UNEQUAL, OK
        }

        Status getStatus();
    }

    /**
     * Can be called to set the password for a user.  This will delete any password request tokens associated with that
     * user
     *
     * @param user        the user in play
     * @param newPassword their new password
     * @throws UserNotFoundException          if the user does not exist
     * @throws InvalidCredentialException     if the password is invalid
     * @throws OperationNotPermittedException if the underlying User Directory is read-only
     */
    void changePassword(ApplicationUser user, String newPassword)
            throws UserNotFoundException, InvalidCredentialException, OperationNotPermittedException, PermissionException;

    /**
     * Gets the number of active users who currently count towards the license and should be charged for. This
     * method should be used when determining user counts for billing purposes, such as purchase tier recommendations
     * for plugins, and by plugins who wish to enforce tier-based licenses.
     * <p>
     * In all cases this method will exclude Connect users from the count. In Cloud this will also exclude users who
     * exist for the purpose of providing support (such as the Atlassian sysadmin user).
     * </p>
     *
     * @return the active user count
     * @see com.atlassian.jira.security.Permissions
     * @since v3.13
     * @deprecated use {@link LicenseCountService#totalBillableUsers()}. since 6.5
     */
    @Deprecated
    int getActiveUserCount();

    /**
     * Returns true if, after adding the specified number of users, the number of active users in JIRA does not exceed
     * the user limit allowed by the license. If the license does not require a user limit, this method will return true
     * immediately.
     *
     * @param numUsers the number of users to add to the JIRA instance. If 0, all things being equal, this method will
     *                 return true. Must not be negative!
     * @return False if the number of active users + the number of users to add is greater than the limit enforced by
     * the license. True otherwise.
     * @since v3.13
     * @deprecated Use {@link com.atlassian.jira.application.ApplicationRoleManager#hasSeatsAvailable(ApplicationKey, int)}
     * instead. Since v7.0
     */
    @Deprecated
    boolean canActivateNumberOfUsers(int numUsers);

    /**
     * Returns a user based on user name.
     *
     * @param userName the user name of the user
     * @return the User object, or null if the user cannot be found including null userName.
     * @since v3.13
     * @deprecated Use {@link #getUserByKey(String)} or {@link #getUserByName(String)} instead. Since v6.0.
     */
    @Deprecated
    @Nullable
    ApplicationUser getUser(String userName);

    /**
     * Returns a user based on key.
     *
     * @param userkey the key of the user
     * @return the User object, or null if the user cannot be found including null userkey.
     * @since v6.0
     * @deprecated use {@link UserManager#getUserByKey(String)}. since 7.0
     */
    @Deprecated
    @Nullable
    ApplicationUser getUserByKey(@Nullable String userkey);

    /**
     * Returns a user based on user name.
     *
     * @param username the user name of the user
     * @return the User object, or null if the user cannot be found including null userName.
     * @since v6.0
     * @deprecated use {@link UserManager#getUserByName(String)}. since 7.0
     */
    @Deprecated
    @Nullable
    ApplicationUser getUserByName(String username);

    /**
     * Returns a user based on user name.
     *
     * @param userName the user name of the user
     * @return the User object, or null if the user cannot be found including null userName.
     * @since v4.3
     * @deprecated Use {@link #getUserByKey(String)} or {@link #getUserByName(String)} instead. Since v6.0.
     */
    @Deprecated
    @Nullable
    ApplicationUser getUserObject(String userName);

    /**
     * Returns true if the a user exists with the specified userName
     *
     * @param userName the name of the user
     * @return true if t a user exists with the specified name or false if not
     */
    boolean userExists(String userName);

    /**
     * Returns a list of JIRA admin {@link User}s.
     * <p>
     * <strong>WARNING:</strong> This method will be changed in the future to return a Collection of Crowd {@link User}
     * objects. Since v4.3.
     *
     * @return a list of JIRA admin {@link User}s.
     * @since v3.13
     * @deprecated Since v4.3.  Use {@link #getJiraAdministrators()}.
     */
    @Deprecated
    Collection<ApplicationUser> getAdministrators();

    /**
     * Returns a list of JIRA admin {@link User}s.
     * <p>
     * <strong>WARNING:</strong> This method will be changed in the future to return a Collection of Crowd {@link User}
     * objects. Since v4.3.
     *
     * @return a list of JIRA admin {@link User}s.
     * @since v4.3
     */
    Collection<ApplicationUser> getJiraAdministrators();

    /**
     * Returns a list of JIRA system admin {@link User}s.
     * <p>
     * <strong>WARNING:</strong> This method will be changed in the future to return a Collection of Crowd {@link User}
     * objects. Since v4.3.
     *
     * @return a collection of {@link User}'s that are associated with the {@link com.atlassian.jira.security.Permissions#SYSTEM_ADMIN}
     * permission.
     * @since v3.13
     * @deprecated Since v4.3.  Use {@link #getJiraSystemAdministrators()} .
     */
    @Deprecated
    Collection<ApplicationUser> getSystemAdministrators();

    /**
     * Returns a list of JIRA system admin {@link User}s.
     * <p>
     * <strong>WARNING:</strong> This method will be changed in the future to return a Collection of Crowd {@link User}
     * objects. Since v4.3.
     *
     * @return a collection of {@link User}'s that are associated with the {@link com.atlassian.jira.security.Permissions#SYSTEM_ADMIN}
     * permission.
     * @since v4.3
     */
    Collection<ApplicationUser> getJiraSystemAdministrators();

    /**
     * Takes the given user and adds him/her to all the groups that grant a user the global JIRA use permission. (see
     * {@link com.atlassian.jira.security.Permissions#USE}) Note: operation is only performed if by doing so we will not
     * exceed the user limit (if the current license happens to specify a limit)
     *
     * @param user The user to be added to the USE permission
     * @since v3.13
     * @deprecated Use {@link UserService#validateAddUserToApplication(ApplicationUser, ApplicationKey)} then
     * {@link UserService#addUserToApplication(AddUserToApplicationValidationResult)} instead. Since v7.0.
     */
    @Deprecated
    void addToJiraUsePermission(ApplicationUser user) throws PermissionException;

    /**
     * Retrieve a collection of ProjectComponents - where the lead of each component is the specified user.
     *
     * @param user User leading components
     * @return Collection of project components
     */
    Collection<ProjectComponent> getComponentsUserLeads(ApplicationUser user);

    /**
     * Returns all the projects that leadUser is the project lead for.
     *
     * @param user the user in play
     * @return A collection of project {@link org.ofbiz.core.entity.GenericValue}s
     */
    Collection<Project> getProjectsLeadBy(ApplicationUser user);

    /**
     * Checking if user without SYSTEM_ADMIN rights tries to remove user with SYSTEM_ADMIN rights.
     *
     * @param loggedInUser User performing operation
     * @param user         User for remove
     * @return true if user without SYSTEM_ADMIN rights tries to remove user with SYSTEM_ADMIN rights
     */
    boolean isNonSysAdminAttemptingToDeleteSysAdmin(ApplicationUser loggedInUser, ApplicationUser user);

    /**
     * Returns number of issues reported by user
     *
     * @param loggedInUser the logged in user
     * @param user         the user to find the issue count for
     * @return number of issues reported by user
     * @throws SearchException if something goes wrong
     */
    long getNumberOfReportedIssuesIgnoreSecurity(ApplicationUser loggedInUser, ApplicationUser user)
            throws SearchException;

    /**
     * Returns number of issues assigned to user
     *
     * @param loggedInUser the logged in user
     * @param user         the user to find the issue count for
     * @return number of issues assigned to user
     * @throws SearchException if something goes wrong
     */
    long getNumberOfAssignedIssuesIgnoreSecurity(ApplicationUser loggedInUser, ApplicationUser user)
            throws SearchException;

    /**
     * Takes the given user and returns a "displayable name" by cautiously checking the different edge cases for users.
     *
     * @param user the user. May be null.
     * @return The user's full name if present and not blank, the username if present, or null otherwise.
     */
    String getDisplayableNameSafely(ApplicationUser user);

    /**
     * Returns a collection of {@link Group} objects that the user belongs to.
     *
     * @param userName A User name
     * @return the set of groups that the user belongs to
     * @since v4.3
     */
    SortedSet<Group> getGroupsForUser(String userName);

    /**
     * Returns a collection of the names of the groups that the user belongs to.
     *
     * @param userName A User name
     * @return the set of groups that the user belongs to
     * @since v4.3
     */
    SortedSet<String> getGroupNamesForUser(String userName);

    /**
     * Returns a collection of {@link User} objects that belong to any of the passed in collection of group names.
     * Prefer using {@link #getAllUsersInGroupNamesUnsorted(java.util.Collection)} and sorting the list of users only if
     * absolutely necessary rather than relying on this method to perform the sort.
     *
     * @param groupNames a collection of group name strings
     * @return the set of users that are in the named groups, sorted in {@link com.atlassian.jira.issue.comparator.UserCachingComparator}
     * order
     */
    SortedSet<ApplicationUser> getAllUsersInGroupNames(Collection<String> groupNames);

    /**
     * Returns a collection of {@link User} objects that belong to any of the passed in collection of group names.
     *
     * @param groupNames a collection of group name strings
     * @return the set of users that are in the named groups
     * @since 5.1
     */
    Set<ApplicationUser> getAllUsersInGroupNamesUnsorted(Collection<String> groupNames);

    /**
     * Returns a collection of {@link User} objects that are found within the passed in collection of group names. Null
     * users are excluded even if they exist in the underlying data.
     *
     * @param groupNames a collection of group name strings
     * @return the set of users that are in the named groups.
     * @deprecated Use {@link #getAllUsersInGroupNames(java.util.Collection)} instead. Since v4.3
     */
    @Deprecated
    SortedSet<ApplicationUser> getUsersInGroupNames(Collection<String> groupNames);

    /**
     * Returns a collection of {@link User} objects that are found within the passed in collection of {@link Group}
     * objects.
     *
     * @param groups a collection of {@link Group} objects
     * @return the set of users that are in the groups, sorted in {@link com.atlassian.jira.issue.comparator.UserCachingComparator}
     * order
     */
    SortedSet<ApplicationUser> getAllUsersInGroups(Collection<Group> groups);

    /**
     * Returns a collection of {@link User} objects that are found within the passed in collection of {@link Group}
     * objects. Null users are excluded even if they exist in the underlying data.
     *
     * @param groups a collection of {@link Group} objects
     * @return the set of users that are in the groups, sorted in {@link com.atlassian.jira.issue.comparator.UserCachingComparator}
     * order
     * @deprecated Use {@link #getAllUsersInGroups(java.util.Collection)} instead. Since v4.3
     */
    @Deprecated
    SortedSet<ApplicationUser> getUsersInGroups(Collection<Group> groups);
}
