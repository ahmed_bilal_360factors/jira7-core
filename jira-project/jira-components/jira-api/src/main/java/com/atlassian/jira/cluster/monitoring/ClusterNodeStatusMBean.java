package com.atlassian.jira.cluster.monitoring;

import com.atlassian.annotations.ExperimentalApi;

/**
 * Exposes properties of an individual node for monitoring. The intended use of this class is to monitor the progress of
 * an upgrade in a Data Center environment. Use of this class requires JMX to be enabled on
 * the target JMV, and for the Cluster Monitoring Dark Feature to be enabled.
 *
 * @see <a href="https://confluence.atlassian.com/enterprise/cluster-management-beans-858567903.html">Enterprise tools / cluster management beans</a>
 * @since v7.3
 */
@ExperimentalApi
public interface ClusterNodeStatusMBean {
    String BEAN_NAME = "com.atlassian.jira.cluster.monitoring:type=ClusterNodeStatus";

    /**
     * @return The current JIRA version running on this node, e.g. "7.2.1", "7.3.0-SNAPSHOT".
     */
    String getNodeVersion();

    /**
     * @return The current state of the cluster as it pertains to a Zero Downtime Upgrade.
     * @see com.atlassian.jira.cluster.zdu.UpgradeState
     */
    String getClusterUpgradeState();
}
