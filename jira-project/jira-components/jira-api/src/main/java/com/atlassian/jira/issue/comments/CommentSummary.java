package com.atlassian.jira.issue.comments;

import com.atlassian.annotations.PublicApi;

import java.util.List;

/**
 * Contains all information required to render the initial comment summary on the view issue page.
 *
 * @since v7.1
 */
@PublicApi
public final class CommentSummary {
    private int total;
    private List<Comment> comments;

    public CommentSummary(int total, List<Comment> comments) {
        this.total = total;
        this.comments = comments;
    }

    public int getTotal() {
        return total;
    }

    public List<Comment> getComments() {
        return comments;
    }
}
