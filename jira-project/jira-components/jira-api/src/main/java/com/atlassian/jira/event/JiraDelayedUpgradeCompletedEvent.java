package com.atlassian.jira.event;

import com.atlassian.annotations.PublicApi;

/**
 * Published when all delayed upgrade tasks have finished running.
 *
 * @since v7.3
 */
@PublicApi
public class JiraDelayedUpgradeCompletedEvent  {
    private final String buildNumber;

    public JiraDelayedUpgradeCompletedEvent(String buildNumber) {
        this.buildNumber = buildNumber;
    }

    public String getBuildNumber() {
        return buildNumber;
    }
}
