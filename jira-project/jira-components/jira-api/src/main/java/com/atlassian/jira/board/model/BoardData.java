package com.atlassian.jira.board.model;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.jira.board.Board;
import com.atlassian.jira.board.BoardId;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.collect.ImmutableList;

import java.util.List;
import java.util.Objects;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Represents the data of a {@link com.atlassian.jira.board.Board}. To create instances of this class,
 * use {@link com.atlassian.jira.board.model.BoardData.Builder}
 * 
 * @since 7.1
 */
@ExperimentalApi
public class BoardData {
    private final Board board;
    private final String jql;
    private final BoardId id;
    private final List<Issue> issues;
    private final List<ApplicationUser> people;
    private final List<BoardColumn> columns;
    private final int maxResults;
    private final int total;
    private final boolean hasFilteredDoneIssues;

    private BoardData(
            Board board,
            String jql,
            BoardId id,
            List<Issue> issues,
            List<ApplicationUser> people,
            List<BoardColumn> columns,
            int maxResults,
            int total,
            boolean hasFilteredDoneIssues
    ) {
        this.board = notNull(board);
        this.jql = notNull(jql);
        this.id = notNull(id);
        this.issues = ImmutableList.copyOf(notNull(issues));
        this.people = ImmutableList.copyOf(notNull(people));
        this.columns = ImmutableList.copyOf(notNull(columns));
        this.maxResults = maxResults;
        this.total = total;
        this.hasFilteredDoneIssues = hasFilteredDoneIssues;
    }

    public Board getBoard() {
        return board;
    }

    public String getJql() {
        return jql;
    }

    public BoardId getId() {
        return id;
    }

    public List<Issue> getIssues() {
        return issues;
    }

    public List<ApplicationUser> getPeople() {
        return people;
    }

    public List<BoardColumn> getColumns() {
        return columns;
    }

    public int getMaxResults() {
        return maxResults;
    }

    public int getTotal() {
        return total;
    }

    public boolean hasFilteredDoneIssues() {
        return hasFilteredDoneIssues;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BoardData boardData = (BoardData) o;
        return maxResults == boardData.maxResults &&
                total == boardData.total &&
                hasFilteredDoneIssues == boardData.hasFilteredDoneIssues &&
                Objects.equals(board, boardData.board) &&
                Objects.equals(jql, boardData.jql) &&
                Objects.equals(id, boardData.id) &&
                Objects.equals(issues, boardData.issues) &&
                Objects.equals(people, boardData.people) &&
                Objects.equals(columns, boardData.columns);
    }

    @Override
    public int hashCode() {
        return Objects.hash(board, jql, id, issues, people, columns, maxResults, total, hasFilteredDoneIssues);
    }

    @Override
    public String toString() {
        return "BoardData{" +
                "board=" + board +
                ", jql='" + jql + '\'' +
                ", id=" + id +
                ", issues=" + issues +
                ", people=" + people +
                ", columns=" + columns +
                ", maxResults=" + maxResults +
                ", total=" + total +
                ", hasFilteredDoneIssues=" + hasFilteredDoneIssues +
                '}';
    }

    /**
     * Builder for {@link com.atlassian.jira.board.model.BoardData}.
     */
    public static class Builder {
        private Board board;
        private String jql;
        private BoardId id;
        private List<Issue> issues;
        private List<ApplicationUser> people;
        private List<BoardColumn> columns;
        private int maxResults;
        private int total;
        private boolean hasFilteredDoneIssues;

        public Builder board(Board board) {
            this.board = board;
            return this;
        }

        public Builder jql(String jql) {
            this.jql = jql;
            return this;
        }

        public Builder id(BoardId boardId) {
            this.id = boardId;
            return this;
        }

        public Builder issues(List<Issue> issues) {
            this.issues = issues;
            return this;
        }

        public Builder people(List<ApplicationUser> people) {
            this.people = people;
            return this;
        }

        public Builder columns(List<BoardColumn> columns) {
            this.columns = columns;
            return this;
        }

        public Builder maxResults(int maxResults) {
            this.maxResults = maxResults;
            return this;
        }

        public Builder total(int total) {
            this.total = total;
            return this;
        }

        public Builder hasFilteredDoneIssues(boolean hasFilteredIssues) {
            this.hasFilteredDoneIssues = hasFilteredIssues;
            return this;
        }

        public BoardData build() {
            return new BoardData(
                    this.board,
                    this.jql,
                    this.id,
                    this.issues,
                    this.people,
                    this.columns,
                    this.maxResults,
                    this.total,
                    this.hasFilteredDoneIssues
            );
        }
    }
}
