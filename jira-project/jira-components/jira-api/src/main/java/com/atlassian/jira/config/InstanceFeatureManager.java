package com.atlassian.jira.config;

import com.atlassian.jira.tenancy.TenantAware;
import com.atlassian.jira.tenancy.TenantInfo;

import java.util.Set;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Provides information about features enabled for current  instance. Unlike {@link FeatureManager} it
 * does not take into consideration tenant-specific features (site-features) nor features for currently logged user
 * so it can be called safely from tenant-less context (i.e. in constructor, from  plugin lifecycle event listeners).
 *
 * @since v7.1
 */
@ParametersAreNonnullByDefault
@TenantInfo(TenantAware.TENANTLESS)
public interface InstanceFeatureManager {
    /**
     * Checks if JIRA is running in OnDemand mode.
     *
     * @return {@code true} if this is JIRA OnDemand; {@code false} otherwise
     */
    @Deprecated
    default boolean isOnDemand() {
        return false;
    }

    /**
     * Checks whether feature {@code featureKey} is enabled in the this JIRA instance. It only checks global instance
     * features that are not tenant specific (features defined in system properties, property files, installed plugins
     * etc.)
     * This method can be called without request context safely (i.e. during plugin initialisation).
     *
     * If the featureKey relates to a feature flag that is defined, then it will also take the
     * default values into consideration.
     *
     * @param featureKey feature key
     * @return <code>true</code>, if feature identified by <tt>featureKey</tt> is enabled, <code>false</code> otherwise
     */
    boolean isInstanceFeatureEnabled(String featureKey);

    /**
     * Returns a set containing the feature keys of all features that are currently enabled.
     *
     * @return a set containing the feature keys of all features that are currently enabled
     */
    Set<String> getEnabledFeatureKeys();
}
