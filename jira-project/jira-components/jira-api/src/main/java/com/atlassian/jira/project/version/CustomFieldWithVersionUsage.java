package com.atlassian.jira.project.version;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.annotations.PublicApi;

import javax.annotation.Nullable;

/**
 * This interface holds information about custom fields where version is used.
 * @since 7.0.10
 */
@PublicApi
@ExperimentalApi
public interface CustomFieldWithVersionUsage {
    /**
     * Custom field identifier.
     */
    long getCustomFieldId();

    /**
     * Custom field name.
     */
    @Nullable
    String getFieldName();

    /**
     * Number of issues where custom field is set to given version.
     */
    long getIssueCountWithVersionInCustomField();
}
