package com.atlassian.jira.issue.statistics;

import java.util.Map;
import java.util.Optional;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.project.Project;
import com.atlassian.query.Query;

/**
 * @since 7.1
 */
@ExperimentalApi
public interface ComponentStatisticsManager {

    /**
     * This method evaluates the query, and returns all the projects and components that the issues resulting from
     * the given query belong to, along with the issue count for each component. Issues that do not belong to any
     * component are ignored.
     * @param query the query to filter the count and project results by.
     *  Optional, in which case, the empty query (all results) is used.
     * @return Map of Projects to (Map of Project's Components to Count).
     * 
     */
    Map<Project, Map<ProjectComponent, Integer>> getProjectsWithComponentsWithIssueCount(Optional<Query> query);
}
