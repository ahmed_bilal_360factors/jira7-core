package com.atlassian.jira.plugin.icon;

import com.atlassian.annotations.ExperimentalSpi;
import com.atlassian.jira.avatar.Avatar;

import javax.annotation.Nonnull;

/**
 * Provides a way to describe a type of icon. The {@link IconTypeDefinition} describes:
 * <ul>
 *     <li>Rules around who can Create/Update/Delete icons for this type (provided by {@link #getPolicy()}}</li>
 *     <li>What type of thing the owning object ID refers to</li>
 *     <li>How to get the image data for system icons</li>
 * </ul>
 *
 * @since v7.1
 */
@ExperimentalSpi
public interface IconTypeDefinition {
    /**
     * Get a key identifying this type of icon.
     * <p>
     *     This key must uniquely identify the {@link IconTypeDefinition}.
     *     That is, there must not be more than one {@link IconTypeDefinition} with the same key.
     *
     * @return Returns the key of this icon type.
     */
    @Nonnull String getKey();

    /**
     * Get the policy for this icon type.
     */
    @Nonnull IconTypePolicy getPolicy();

    /**
     * Get the provider for system icon images.
     */
    @Nonnull SystemIconImageProvider getSystemIconImageProvider();
}
