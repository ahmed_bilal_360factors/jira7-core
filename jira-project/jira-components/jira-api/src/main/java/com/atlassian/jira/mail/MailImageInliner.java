package com.atlassian.jira.mail;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.util.InjectableComponent;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.mail.BodyPart;

import static java.util.Objects.requireNonNull;

/**
 * A component to use when required to construct an email body from HTML that may contain inlined images.
 *
 * @since v7.0
 */
@ExperimentalApi
@InjectableComponent
@ParametersAreNonnullByDefault
public interface MailImageInliner {

    /**
     * Tries to inline all the images specified by an IMG tag. Replaces the image url with the cid link or unchanged
     * path if specified path cannot be added as attachment. Will then build the related body parts for the attachments.
     * <p/>
     * NB: This method will be able to inline system images contained within provided HTML, however will not inline
     * user attachments. See {@link #inlineImages(String, Issue)} in order to inline attachments related to an issue.
     *
     * @param html HTML to change.
     * @return The {@link InlinedEmailBody} to represent the new email body, fully inlined
     */
    @Nonnull
    InlinedEmailBody inlineImages(String html);

    /**
     * Tries to inline all the images specified by an IMG tag. Replaces the image url with the cid link or unchanged
     * path if specified path cannot be added as attachment. Will then build the related body parts for the attachments.
     * <p/>
     * NB: This method will inline system images as well as attachments related to issue {@link Issue#getAttachments()}
     * that are contained within the provided html.
     *
     * @param html  HTML to change.
     * @param issue Issue that is related to HTML that contains required attachments
     * @return The {@link InlinedEmailBody} to represent the new email body, fully inlined
     * @since v7.0.5
     */
    @Nonnull
    InlinedEmailBody inlineImages(String html, Issue issue);

    /**
     * A small wrapper that contains the HTML string after it has had images inlined, as well as the {@link
     * BodyPart}'s that represent the inlined image attachments, to use when constructing an email body.
     * <p/>
     * Only useful as a return type for {@link MailImageInliner} to contain both the html and the attachment parts.
     *
     * @since v7.0
     */
    @ExperimentalApi
    @ParametersAreNonnullByDefault
    class InlinedEmailBody {

        private final String html;
        private final Iterable<BodyPart> bodyParts;

        /**
         * Constructor specifying the inlined HTML and inlined mail attachments
         * <p/>
         * No parameters can be null
         *
         * @param html      The html that has been inlined
         * @param bodyParts the body parts that are inlined by the html, can be empty, but not null
         * @throws NullPointerException If any paramter is null
         */
        InlinedEmailBody(String html, Iterable<BodyPart> bodyParts) {
            this.html = requireNonNull(html, "html is required");
            this.bodyParts = requireNonNull(bodyParts, "bodyParts is required");
        }

        /**
         * Return the inlined HTML
         *
         * @return The inlined HTML, can not be null
         */
        @Nonnull
        public String getHtml() {
            return html;
        }

        /**
         * Return the iterable of body parts that have been constructed with a content-id to unqiquely represent the
         * attachments
         *
         * @return The iterable of body parts, can not be null but can be empty
         */
        @Nonnull
        public Iterable<BodyPart> getBodyParts() {
            return bodyParts;
        }

    }
}
