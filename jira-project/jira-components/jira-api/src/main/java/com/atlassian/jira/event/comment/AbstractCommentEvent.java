package com.atlassian.jira.event.comment;

import com.atlassian.jira.issue.comments.Comment;

/**
 * Base class for comment related events.
 */
abstract class AbstractCommentEvent implements CommentEvent {

    private final Comment comment;

    protected AbstractCommentEvent(final Comment comment) {
        this.comment = comment;
    }

    @Override
    public final Comment getComment() {
        return comment;
    }
}
