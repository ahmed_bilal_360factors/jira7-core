package com.atlassian.jira.issue.fields;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.layout.field.FieldLayout;
import com.atlassian.jira.jql.context.QueryContext;
import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nullable;
import java.util.Set;

@PublicApi
public interface FieldManager {
    public static final String CUSTOM_FIELD_PREFIX = "customfield_";

    /**
     * Get a field by its id.
     *
     * @param id An {@link com.atlassian.jira.issue.IssueFieldConstants} constant, or custom field key (eg. "customfield_10010")
     * @return the Field
     */
    public Field getField(String id);

    /**
     * Returns true if the given ID is a Custom Field that actually exists in JIRA.
     *
     * @param id the field ID
     * @return true if the given ID is a Custom Field that actually exists in JIRA.
     * @deprecated Use {@link #isExistingCustomField} or {@link #isCustomFieldId} instead. Since v6.5.
     */
    public boolean isCustomField(String id);

    /**
     * Returns true if the given field ID is in the custom field format.
     * <p> This method just checks that the ID looks like a Custom Field (it starts with "customfield_") but not that
     * the custom field actually exists (it could have been deleted).
     *
     * @param id the field ID
     * @return true if the given ID is a Custom Field that actually exists in JIRA.
     * @see #isExistingCustomField
     * @since 6.5
     */
    public boolean isCustomFieldId(String id);

    /**
     * Returns true if the given ID is a Custom Field that actually exists in JIRA.
     * <p> Checking for existance comes with a performance price, so use {@link #isCustomFieldId where possible}.
     *
     * @param id the field ID
     * @return true if the given ID is a Custom Field that actually exists in JIRA.
     * @see #isCustomFieldId
     * @since 6.5
     */
    public boolean isExistingCustomField(String id);

    public boolean isCustomField(Field field);

    /**
     * Get a CustomField by its text key (eg 'customfield_10000').
     *
     * @param id Eg. 'customfield_10000'
     * @return The {@link CustomField} or null if not found.
     */
    @Nullable
    public CustomField getCustomField(String id);

    public boolean isHideableField(String id);

    public boolean isHideableField(Field field);

    public HideableField getHideableField(String id);

    public boolean isOrderableField(String id);

    public boolean isOrderableField(Field field);

    public OrderableField getOrderableField(String id);

    public ConfigurableField getConfigurableField(String id);

    public Set<OrderableField> getOrderableFields();

    public Set<NavigableField> getNavigableFields();

    public boolean isNavigableField(String id);

    public boolean isNavigableField(Field field);

    public NavigableField getNavigableField(String id);

    public boolean isRequirableField(String id);

    public boolean isRequirableField(Field field);

    public boolean isMandatoryField(String id);

    public boolean isMandatoryField(Field field);

    public boolean isRenderableField(String id);

    public boolean isRenderableField(Field field);

    public boolean isUnscreenableField(String id);

    public boolean isUnscreenableField(Field field);

    public RequirableField getRequiredField(String id);

    /**
     * Invalidates <em>all field-related caches</em> in JIRA.
     * <font color="red"><h1>WARNING</h1></font>
     * This method invalidates a whole lot of JIRA caches, which means that JIRA performance significantly degrades
     * after this method has been called. For this reason, you should <b>avoid calling this method at all costs</b>.
     * <p>
     * The correct approach to invalidate the cache entries is to do it in the "store" inside the {@code FooStore.updateFoo()}
     * method, where you can invalidate a <b>single</b> cache entry. If the cache lives in another class then the store
     * should raise a {@code FooUpdatedEvent} which that class can listen to in order to keep its caches up to date.
     * <p>
     * If you add any calls to this method in JIRA I will hunt you down and subject you to a Spanish inquisition.
     */
    public void refresh();

    public Set<Field> getUnavailableFields();

    boolean isFieldHidden(ApplicationUser remoteUser, Field field);

    /**
     * Determines whether the field with id of fieldId is NOT hidden in AT LEAST one {@link com.atlassian.jira.issue.fields.layout.field.FieldLayout} that the user can see
     * (assigned to projects for which the user has the {@link com.atlassian.jira.permission.ProjectPermissions#BROWSE_PROJECTS} permission).
     *
     * @param remoteUser the remote user.
     * @param fieldId    The Field ID
     */
    boolean isFieldHidden(ApplicationUser remoteUser, String fieldId);

    /**
     * Checks whether the given field is hidden in all of the given Field Layouts.
     * <p>
     * This method can be used in conjunction with {@link #getVisibleFieldLayouts(com.atlassian.jira.user.ApplicationUser)}
     * to provide a more performant way of looking up {@link #isFieldHidden(com.atlassian.jira.user.ApplicationUser, Field)} multiple times.
     * Instead, it is more efficient to do something like:
     * <pre>
     *     Set<FieldLayout> fieldLayouts = fieldManager.getVisibleFieldLayouts(loggedInUser);
     *     for (Field field: myFields)
     *     {
     *       if (fieldManager.isFieldHidden(fieldLayouts1, field))
     *       {
     *           ...
     *       }
     *     }
     * </pre>
     *
     * @param fieldLayouts The FieldLayouts to check
     * @param field        The field to check
     * @return true if the given field is hidden in all of the given Field Layouts.
     * @see #getVisibleFieldLayouts(com.atlassian.jira.user.ApplicationUser)
     * @see #isFieldHidden(com.atlassian.jira.user.ApplicationUser, Field)
     */
    boolean isFieldHidden(Set<FieldLayout> fieldLayouts, Field field);

    /**
     * Returns all the visible FieldLayouts for the given user.
     * <p>
     * This is used in conjunction with {@link #isFieldHidden(Set, Field)} as a performance optimisation in usages that want to call {@link #isFieldHidden(com.atlassian.jira.user.ApplicationUser, Field)} multiple times.
     * Instead, it is more efficient to do something like:
     * <pre>
     *     Set<FieldLayout> fieldLayouts = fieldManager.getVisibleFieldLayouts(loggedInUser);
     *     for (Field field: myFields)
     *     {
     *       if (fieldManager.isFieldHidden(fieldLayouts1, field))
     *       {
     *           ...
     *       }
     *     }
     * </pre>
     *
     * @param user the user whose project browsing permissions are used to limit visible FieldLayouts.
     * @return all the visible FieldLayouts for the given user.
     */
    Set<FieldLayout> getVisibleFieldLayouts(ApplicationUser user);

    /**
     * Gets all the available fields that the user can see, this is providing no context scope.
     *
     * @param user the remote user.
     * @return a set of NavigableFields that can be show because their visibility/configuration fall within what the
     * user can see.
     * @throws FieldException thrown if there is a problem looking up the fields
     */
    public Set<NavigableField> getAvailableNavigableFieldsWithScope(ApplicationUser user) throws FieldException;

    /**
     * Gets all the available fields within the defined scope of the QueryContext.
     *
     * @param user         the user making the request
     * @param queryContext the context of the search request.
     * @return a set of NavigableFields that can be show because their visibility/configuration fall within the specified
     * context
     * @throws FieldException thrown if there is a problem looking up the fields
     */
    public Set<NavigableField> getAvailableNavigableFieldsWithScope(ApplicationUser user, QueryContext queryContext) throws FieldException;

    /**
     * Retrieves custom fields in scope for the given issue
     *
     * @param remoteUser Remote com.atlassian.jira.user.ApplicationUser
     * @param issue      Issue
     * @return custom fields in scope for the given issue
     * @throws FieldException if cannot retrieve the projects the user can see, or if cannot retrieve
     *                        the field layouts for the viewable projects
     */
    public Set<CustomField> getAvailableCustomFields(ApplicationUser remoteUser, Issue issue) throws FieldException;

    public Set<NavigableField> getAllAvailableNavigableFields() throws FieldException;

    public Set<NavigableField> getAvailableNavigableFields(ApplicationUser remoteUser) throws FieldException;

    /**
     * Return all the searchable fields in the system. This set will included all defined custom fields.
     *
     * @return the set of all searchable fields in the system.
     */
    Set<SearchableField> getAllSearchableFields();

    /**
     * Return all the searchable systems fields. This set will *NOT* include defined custom fields.
     *
     * @return the set of all searchable systems fields defined.
     */
    Set<SearchableField> getSystemSearchableFields();

    // --------------------------------------------------------------------------------------------- Convenience Methods

    /**
     * Retrieve the IssueType system Field.
     *
     * @return the IssueType system Field.
     */
    public IssueTypeField getIssueTypeField();

    /**
     * Retrieve the Project system Field.
     *
     * @return the Project system Field.
     */
    public ProjectField getProjectField();

    boolean isTimeTrackingOn();
}
