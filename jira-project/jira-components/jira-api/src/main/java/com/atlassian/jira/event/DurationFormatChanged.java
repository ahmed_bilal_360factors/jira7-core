package com.atlassian.jira.event;

import com.atlassian.annotations.PublicApi;

/**
 * Event to be sent when the configuration for the default {@link com.atlassian.jira.util.JiraDurationUtils.DurationFormatter}
 * has changed.
 *
 * @since v7.2
 * @see {@link com.atlassian.jira.util.DurationFormatterProvider}
 */
@PublicApi
public class DurationFormatChanged {
    public static final DurationFormatChanged INSTANCE = new DurationFormatChanged();

    private DurationFormatChanged() {}
}
