package com.atlassian.jira.database;

import com.atlassian.annotations.PublicSpi;

/**
 * A callback interface that gives access to a managed database connection.
 *
 * @see DatabaseAccessor
 * @since v7.0
 */
@FunctionalInterface
@PublicSpi
public interface ConnectionFunction<R> {
    R run(DatabaseConnection con);
}
