package com.atlassian.jira.project;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.jira.bc.project.ProjectCreationData;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.task.context.Context;
import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Implementations of this interface are responsible for all management of project entities within JIRA.
 */
public interface ProjectManager {
    /**
     * Creates the project in the database, and adds default project roles for this project.
     * <p>
     * If no name, key, lead or project type are provided an exception will be thrown.
     *
     * @param user                The user creating the project
     * @param projectCreationData An object encapsulating all the data for the project that will get created
     * @return The newly created project.
     * @see com.atlassian.jira.bc.project.ProjectCreationData
     * @since 7.0
     */
    Project createProject(@Nonnull ApplicationUser user, @Nonnull ProjectCreationData projectCreationData);

    /**
     * Updates the project provided with the new attributes passed in.  This method is responsible for persisting
     * any changes to the database.
     *
     * @param originalProject The project to be updated.
     * @param name            The name for the updated project
     * @param description     An optional description for the project
     * @param leadKey         The userkey of the lead developer for the project
     * @param url             An optional URL for the updated project
     * @param assigneeType    The default assignee for issues created in this project.  May be either project lead, or
     *                        unassigned if unassigned issues are enabled.
     * @return The updated project
     */
    Project updateProject(Project originalProject, String name, String description, String leadKey, String url, Long assigneeType);

    /**
     * Updates the project provided with the new attributes passed in.  This method is responsible for persisting
     * any changes to the database.
     *
     * @param originalProject The project to be updated.
     * @param name            The name for the updated project
     * @param description     An optional description for the project
     * @param leadKey         The userkey of the lead developer for the project
     * @param url             An optional URL for the updated project
     * @param assigneeType    The default assignee for issues created in this project.  May be either project lead, or
     *                        unassigned if unassigned issues are enabled.
     * @param projectKey      The new project key (not updated if null)
     * @return The updated project
     * @since v6.1
     */
    Project updateProject(Project originalProject, String name, String description, String leadKey, String url, Long assigneeType, Long avatarId, String projectKey);

    /**
     * Updates the project provided with the new attributes passed in.  This method is responsible for persisting
     * any changes to the database.
     *
     * @param originalProject The project to be updated.
     * @param name            The name for the updated project
     * @param description     An optional description for the project
     * @param leadKey         The userkey of the lead developer for the project
     * @param url             An optional URL for the updated project
     * @param assigneeType    The default assignee for issues created in this project.  May be either project lead, or
     *                        unassigned if unassigned issues are enabled.
     * @param avatarId        the id of an existing avatar.
     * @return The updated project
     */
    Project updateProject(Project originalProject, String name, String description, String leadKey, String url, Long assigneeType, Long avatarId);


    /**
     * Updates the project with the given changes.
     *
     * @param parameters containing the changes to the project that needs to be made.
     * @return The updated project
     * @since v7.2
     */
    Project updateProject(UpdateProjectParameters parameters);

    /**
     * Updates the type of a project.
     *
     * @param user           The user performing the action
     * @param project        The project which type needs to be updated
     * @param newProjectType The new project type
     * @return The updated project
     * @since 7.0
     */
    Project updateProjectType(ApplicationUser user, Project project, ProjectTypeKey newProjectType);

    /**
     * Removes all issues for a particular project.  A RemoveException will be thrown on any errors removing issues.
     *
     * @param project The project for which issues will be deleted.
     * @throws RemoveException if there's any errors removing issues
     * @deprecated since v7.1.1. Use {@link #removeProjectIssues(Project, Context)}
     */
    void removeProjectIssues(Project project) throws RemoveException;

    /**
     * Removes all issues for a particular project.  A RemoveException will be thrown on any errors removing issues.
     *
     * @param project The project for which issues will be deleted.
     * @param taskContext A task context for providing progress.
     * @throws RemoveException if there's any errors removing issues
     * @since 7.1.1
     */
    void removeProjectIssues(final Project project, Context taskContext) throws RemoveException;

    /**
     * Deletes the provided project from the database.
     * Please note that this method only deletes the project itself and not any related entities.
     * Use {@link com.atlassian.jira.bc.project.ProjectService#deleteProject} to
     * properly delete a project.
     *
     * @param project The project to be deleted.
     */
    void removeProject(Project project);

    /**
     * Retrieves a single {@link Project} by its id.
     *
     * @param id ID of the Project.
     * @return Project object or null if project with that id doesn't exist.
     * @throws DataAccessException If any errors occur accessing the DB.
     */
    @Nullable
    Project getProjectObj(Long id) throws DataAccessException;

    /**
     * Returns a {@link Project} object based on the passed in project name.
     *
     * @param projectName the name of the project
     * @return the {@link Project} object specified by the supplied name or null
     */
    Project getProjectObjByName(String projectName);

    /**
     * Returns a {@link Project} object based on the passed in project key.
     *
     * @param projectKey the Project key.
     * @return the {@link Project} object specified by the key or null
     */
    Project getProjectObjByKey(String projectKey);

    /**
     * Returns the {@link Project} with the given project key.
     * <p>
     * This method will strictly only return the project whose current project key is the one given.
     * <p>
     * This method is added to the API in anticipation of being able to edit the project key, but this feature has not
     * actually been added in 6.0.
     *
     * @param projectKey the Project key.
     * @return the {@link Project} with the given project key.
     * @see #getProjectObjByKey(String)
     * @since 6.0
     */
    @ExperimentalApi
    Project getProjectByCurrentKey(String projectKey);

    /**
     * Returns a {@link Project} object based on the passed in project key, not taking into account the case
     * of the project key.
     *
     * @param projectKey the project key, case does not matter.
     * @return the project object specified by the key or null if no such project exists.
     */
    Project getProjectByCurrentKeyIgnoreCase(final String projectKey);

    /**
     * Returns a {@link Project} object based on the passed in project key, not taking into account the case
     * of the project key. Matches also by previous keys that were associated with a project.
     *
     * @param projectKey the project key, case does not matter.
     * @return the project object specified by the key or null if no such project exists.
     */
    Project getProjectObjByKeyIgnoreCase(final String projectKey);

    /**
     * Returns all project keys that are associated with {@link Project}.
     *
     * @return all project keys (including the current one) associated with the project
     */
    Set<String> getAllProjectKeys(final Long projectId);

    /**
     * Return all {@link Project}s ordered by name.
     *
     * @return all projects ordered by name.
     */
    @Nonnull
    List<Project> getProjects();

    /**
     * Return all {@link Project}s ordered by name.
     * <p>
     * This method does the same thing as getProjects() and exists for legacy reasons.
     * </p>
     *
     * @return all projects ordered by name.
     * @throws DataAccessException If any errors occur accessing the DB.
     * @see #getProjects()
     */
    @Nonnull
    List<Project> getProjectObjects() throws DataAccessException;

    /**
     * Return the total number of {@link Project}s.
     *
     * @return A long value representing tht total number of projects.
     * @throws DataAccessException if any errors occur accessing the DB.
     */
    long getProjectCount() throws DataAccessException;

    /**
     * Get the next issue ID from this project (transactional).
     * Each project maintains an internal counter for the number of issues.
     * This method may be used to construct a new issue key.
     *
     * @param project The Project
     * @return A long value representing a new issue id for the project.
     * @throws DataAccessException If any errors occur accessing the DB.
     */
    long getNextId(Project project) throws DataAccessException;

    /**
     * Causes a full refresh of the project cache.
     */
    void refresh();

    /**
     * Returns all ProjectCategories, ordered by name.
     *
     * @return all ProjectCategories, ordered by name.
     * @throws DataAccessException If any errors occur accessing the DB.
     */
    Collection<ProjectCategory> getAllProjectCategories() throws DataAccessException;

    /**
     * Returns a single project category by id.
     *
     * @param id {@link ProjectCategory} id.
     * @return the {@link ProjectCategory} for the provided id if it was found or else null
     * @throws DataAccessException If any errors occur accessing the DB.
     */
    ProjectCategory getProjectCategory(Long id) throws DataAccessException;

    /**
     * Returns a single project category by id.
     * <p>
     * Legacy synonym for {@link #getProjectCategory(Long)}
     *
     * @param id Project Category ID.
     * @return The project category
     */
    @Nullable
    ProjectCategory getProjectCategoryObject(Long id) throws DataAccessException;

    /**
     * Find a project category by name.
     *
     * @param projectCategoryName Name of the Project Category
     * @return The ProjectCategory or null if none found
     */
    ProjectCategory getProjectCategoryObjectByName(String projectCategoryName);

    /**
     * Find a project category by name ignoring the case of the category name.
     *
     * @param projectCategoryName Name of the Project Category
     * @return The ProjectCategory or null if none found
     */
    ProjectCategory getProjectCategoryObjectByNameIgnoreCase(String projectCategoryName);

    /**
     * Persist an updated project category.
     *
     * @param projectCategory project category.
     * @throws DataAccessException If any errors occur accessing the DB.
     */
    void updateProjectCategory(ProjectCategory projectCategory) throws DataAccessException;

    /**
     * Returns a list of projects in a particular category.
     *
     * @param projectCategory project category.
     * @return A collection of projects sorted by name.
     * @throws DataAccessException If any errors occur accessing the DB.
     */
    Collection<Project> getProjectsFromProjectCategory(ProjectCategory projectCategory) throws DataAccessException;

    /**
     * Returns a list of projects in a particular category.
     *
     * @param projectCategoryId project category id.
     * @return A collection of project {@link Project}s sorted by name.
     * @throws DataAccessException If any errors occur accessing the DB.
     * @since v4.0
     */
    Collection<Project> getProjectObjectsFromProjectCategory(Long projectCategoryId) throws DataAccessException;

    /**
     * Returns a list of projects without project category, sorted by project name
     *
     * @return A collection of {@link com.atlassian.jira.project.Project}s sorted by name
     * @throws DataAccessException If any errors occur accessing the DB.
     * @since v4.0
     */
    Collection<Project> getProjectObjectsWithNoCategory() throws DataAccessException;

    /**
     * Returns a project's category.
     *
     * @param project project
     * @return A ProjectCategory or null if this project has no category.
     * @throws DataAccessException If any errors occur accessing the DB.
     * @since 4.4
     */
    @Nullable
    ProjectCategory getProjectCategoryForProject(Project project) throws DataAccessException;

    /**
     * Sets a project's category.
     *
     * @param project  project
     * @param category category
     * @throws DataAccessException      If any errors occur accessing the DB.
     * @throws IllegalArgumentException if the project provided is null
     */
    void setProjectCategory(Project project, ProjectCategory category) throws DataAccessException;

    /**
     * Creates a new ProjectCategory with the given name and description.
     *
     * @param name        the Name
     * @param description the Description.
     * @return the new ProjectCategory.
     */
    ProjectCategory createProjectCategory(String name, String description);

    /**
     * Removes the given ProjectCategory.
     *
     * @param id the ProjectCategory to remove.
     */
    void removeProjectCategory(Long id);

    /**
     * Checks if a project category with a given name exists.
     *
     * @param projectCategory the name of the project category
     * @return if a category with given name exists
     */
    boolean isProjectCategoryUnique(String projectCategory);

    /**
     * Gets the default assignee for a project and/or component depending on if a component was specified.
     *
     * @param project   project
     * @param component component
     * @return the default assignee for this project/component
     * @throws DefaultAssigneeException If the default assignee does NOT have ASSIGNABLE permission OR Unassigned issues are turned off.
     * @deprecated Use {@link #getDefaultAssignee(Project, java.util.Collection)} which allows for multiple components. Since v4.4.
     */
    ApplicationUser getDefaultAssignee(Project project, ProjectComponent component);

    /**
     * Gets the default assignee for an issue given its project and list of Components.
     * <p>
     * If the default assignee configuration is invalid, then a DefaultAssigneeException is thrown.
     * This could be because the default is unassigned, and unassigned issues are not allowed, or because the default user
     * does not have permission to be assigned to issues in this project.
     *
     * @param project    project
     * @param components The components
     * @return the default assignee for this project/components
     * @throws DefaultAssigneeException If the default assignee is invalid (eg user does not have assign permission) .
     */
    ApplicationUser getDefaultAssignee(Project project, Collection<ProjectComponent> components) throws DefaultAssigneeException;

    /**
     * Returns all the projects that leadUser is the project lead for ordered by the name of the Project.
     *
     * @param leadUser Project Lead
     * @return A collection of projects
     */
    List<Project> getProjectsLeadBy(ApplicationUser leadUser);

    /**
     * Converts a collection of projectIds to a list of projects.
     * <p>
     * Will return null if incoming collection is null.
     * <p>
     * The returned list of Project Objects will have the same sort order as the incoming collection of IDs.
     *
     * @param projectIds a Collection of Project IDs
     * @return List of Projects, or null if input is null
     */
    List<Project> convertToProjectObjects(final Collection<Long> projectIds);

    /**
     * Returns the curremt issue counter for the given project. This value is for information only; you should
     * not use it to predict or create issue ids, because it may change concurrently as new issues are created.
     *
     * @param id the ID of the project for which to retrieve the counter
     * @return the current project counter (the id of the next issue to be created).
     */
    long getCurrentCounterForProject(Long id);

    /**
     * Set the project counter. <b>Warning</b> Setting the project counter is not needed in the normal
     * operations of JIRA, this method exist for functionality like project-import etc.
     *
     * @param project the project for which to set the counter (required)
     * @param counter the counter value to set
     */
    void setCurrentCounterForProject(Project project, long counter);
}
