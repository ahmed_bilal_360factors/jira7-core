package com.atlassian.jira.permission;

import com.atlassian.annotations.PublicApi;

/**
 * Type of a permission holder.
 * <p>
 * Types are stored in the DB and identifies by keys. Each type can specify whether
 * it expects an additional parameter.
 * <p>
 * Standard JIRA permission holder types are defined in the {@link JiraPermissionHolderType} enum.
 * </p>
 * <p>
 * Implementations of this interface are required to be immutable.
 * </p>
 *
 * @see com.atlassian.jira.permission.PermissionHolder
 * @see com.atlassian.jira.permission.JiraPermissionHolderType
 */
@PublicApi
public interface PermissionHolderType {
    /**
     * Returns a key that will be stored in the database and identify this holder type.
     */
    public String getKey();

    /**
     * True if this type requires an additional parameter, false otherwise.
     */
    public boolean requiresParameter();
}
