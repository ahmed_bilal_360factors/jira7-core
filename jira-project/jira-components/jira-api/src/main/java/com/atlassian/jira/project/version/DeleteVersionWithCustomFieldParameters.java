package com.atlassian.jira.project.version;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Optional;

/**
 * Parameters for {@link VersionManager#deleteVersionAndSwap(ApplicationUser, DeleteVersionWithCustomFieldParameters)} and
 * {@link com.atlassian.jira.bc.project.version.VersionService#deleteVersionAndSwap(JiraServiceContext, DeleteVersionWithCustomFieldParameters)}
 *
 * @see com.atlassian.jira.bc.project.version.DeleteVersionWithReplacementsParameterBuilder
 * @see com.atlassian.jira.bc.project.version.VersionService#createVersionDeletaAndReplaceParameters(Version)
 * @since v7.0.10
 */
@PublicApi
@ExperimentalApi
public interface DeleteVersionWithCustomFieldParameters {

    @Nonnull
    /**
     * Version that will be removed
     */
    Version getVersionToDelete();

    /**
     * What version will replace version in 'fix version' field. {@link Optional#empty()} means
     * that version will be removed.
     */
    Optional<Version> getMoveFixIssuesTo();

    /**
     * What version will replace version in 'affected version' field. {@link Optional#empty()} means
     * that version will be removed.
     */
    Optional<Version> getMoveAffectedIssuesTo();

    /**
     * List of version what will replace version in custom fields. For custom fields not specified here
     * version will be removed.
     */
    @Nonnull
    List<CustomFieldReplacement> getCustomFieldReplacementList();

    /**
     * Information about version replacement in custom field.
     */
    interface CustomFieldReplacement {
        /**
         * Custom field id.
         */
        long getCustomFieldId();

        /**
         * What version will replace version in given custom field. {@link Optional#empty()} means
         * that version will be removed.
         */
        Optional<Version> getMoveTo();
    }
}
