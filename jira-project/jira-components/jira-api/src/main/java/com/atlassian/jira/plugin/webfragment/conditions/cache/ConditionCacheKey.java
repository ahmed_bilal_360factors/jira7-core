package com.atlassian.jira.plugin.webfragment.conditions.cache;

import com.atlassian.annotations.ExperimentalApi;

/**
 * Create instances of this class with {@link ConditionCacheKeys}.
 */
@ExperimentalApi
public final class ConditionCacheKey {

    private final String key;

    ConditionCacheKey(String key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return key;
    }
}
