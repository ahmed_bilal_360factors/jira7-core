package com.atlassian.jira.project.template.hook;

import com.atlassian.annotations.PublicApi;

import java.util.List;
import java.util.Optional;

/**
 * An issue type scheme template as defined in the {@link ConfigTemplate}.
 *
 * @since 7.0
 */
@PublicApi
public interface IssueTypeSchemeTemplate {
    /**
     * Returns the name of the Issue Type Scheme.
     *
     * @return the issue type scheme name
     */
    String name();

    /**
     * Returns the description of the Issue Type Scheme
     *
     * @return the issue type scheme description
     */
    String description();

    /**
     * Returns the key of the default issue type.
     *
     * @return the default issue type key
     */
    Optional<String> defaultIssueType();

    /**
     * Returns the list of issue type templates.
     *
     * @return a list of issue type templates
     */
    List<IssueTypeTemplate> issueTypeTemplates();
}
