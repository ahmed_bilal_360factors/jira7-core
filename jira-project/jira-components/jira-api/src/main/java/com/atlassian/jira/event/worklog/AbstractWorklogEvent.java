package com.atlassian.jira.event.worklog;

import com.atlassian.annotations.Internal;
import com.atlassian.jira.issue.worklog.Worklog;
import com.google.common.base.Objects;

/**
 * Base class for worklog events.
 *
 * @since v7.0
 */
@Internal
public abstract class AbstractWorklogEvent {
    private final Worklog worklog;

    protected AbstractWorklogEvent(final Worklog worklog) {
        this.worklog = worklog;
    }

    /**
     * @return the worklog on which the operation was performed.
     */
    public Worklog getWorklog() {
        return worklog;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AbstractWorklogEvent that = (AbstractWorklogEvent) o;
        return Objects.equal(worklog, that.worklog);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(worklog);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("worklog", worklog)
                .toString();
    }
}
