package com.atlassian.jira.event.comment;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.event.issue.JiraIssueEvent;
import com.atlassian.jira.issue.comments.Comment;

/**
 * Marker interface for all comment related events.
 *
 * Comments events are also covered with {@link com.atlassian.jira.event.issue.IssueEvent}.
 */
@PublicApi
public interface CommentEvent extends JiraIssueEvent {
    /**
     * @return a comment on which the operation was performed.
     */
    Comment getComment();
}
