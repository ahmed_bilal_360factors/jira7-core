package com.atlassian.jira.bc.user;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.entity.property.EntityWithKeyPropertyService;
import com.atlassian.jira.user.ApplicationUser;

/**
 * @since v6.5
 */
@PublicApi
public interface UserPropertyService extends EntityWithKeyPropertyService<ApplicationUser> {
}
