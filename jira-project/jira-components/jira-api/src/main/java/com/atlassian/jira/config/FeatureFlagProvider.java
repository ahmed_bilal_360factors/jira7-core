package com.atlassian.jira.config;

import com.atlassian.annotations.PublicSpi;

import java.util.Set;

/**
 * A plugin can implement a provider that gives off a set of feature flags.
 *
 * @since 7.1
 */
@PublicSpi
public interface FeatureFlagProvider {
    /**
     * @return a set of feature flags
     */
    public Set<FeatureFlag> getFeatureFlags();
}
