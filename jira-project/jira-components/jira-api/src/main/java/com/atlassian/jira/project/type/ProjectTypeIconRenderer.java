package com.atlassian.jira.project.type;

import com.atlassian.annotations.PublicApi;

import java.util.Optional;

/**
 * Class responsible for rendering the icon of a project type, according to its status on the instance (uninstalled, unlicensed, installed, etc...).
 * Use this class whenever you need to render the icon for the project type associated with a project.
 *
 * @since 7.0
 */
@PublicApi
public interface ProjectTypeIconRenderer {
    /**
     * Returns the icon to render for the project type with the given key.
     * <p>
     * The icon to be rendered will be the one defined for the project type with the given key as long
     * as that project type is installed on the JIRA instance.
     * <p>
     * Otherwise, the icon will be a grayed out one, indicating that the project type is not accessible.
     *
     * @param projectTypeKey The project type key.
     * @return The icon to be rendered. The returned Optional will be empty if project types is disabled.
     */
    Optional<String> getIconToRender(ProjectTypeKey projectTypeKey);
}
