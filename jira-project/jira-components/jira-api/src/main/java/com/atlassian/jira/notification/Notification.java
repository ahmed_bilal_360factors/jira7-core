package com.atlassian.jira.notification;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.notification.type.NotificationType;

/**
 * Information about notification configured in a notification scheme.
 *
 * @since 7.0
 */
@PublicApi
public interface Notification {
    Long getId();

    NotificationType getNotificationType();

    String getParameter();

    <X> X accept(NotificationVisitor<X> notificationVisitor);
}
