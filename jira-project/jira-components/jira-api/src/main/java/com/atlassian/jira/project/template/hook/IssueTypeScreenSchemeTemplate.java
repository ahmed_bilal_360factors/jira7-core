package com.atlassian.jira.project.template.hook;

import com.atlassian.annotations.PublicApi;

import java.util.List;

/**
 * An issue type screen scheme template as defined in the {@link ConfigTemplate}.
 *
 * @since 7.0
 */
@PublicApi
public interface IssueTypeScreenSchemeTemplate {
    /**
     * Returns the name of the issue type screen scheme.
     *
     * @return The name of the issue type screen scheme.
     */
    String name();

    /**
     * Returns the description of the issue type screen scheme.
     *
     * @return The description of the issue type screen scheme.
     */
    String description();

    /**
     * Returns the default screen scheme.
     *
     * @return The default screen scheme.
     */
    String defaultScreenScheme();

    /**
     * Returns the list of screen templates.
     *
     * @return The list of screen templates.
     */
    List<ScreenTemplate> screenTemplates();

    /**
     * Returns the list of screen scheme templates.
     *
     * @return The list of screen scheme templates.
     */
    List<ScreenSchemeTemplate> screenSchemeTemplates();

    /**
     * Returns whether there is a screen scheme or not.
     *
     * @param screenSchemeKey The key of the screen scheme.
     * @return Whether there is a screen scheme.
     */
    boolean hasScreenScheme(String screenSchemeKey);
}
