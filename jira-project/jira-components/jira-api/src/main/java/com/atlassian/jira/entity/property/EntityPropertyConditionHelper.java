package com.atlassian.jira.entity.property;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.entity.WithId;
import com.atlassian.jira.plugin.webfragment.JiraWebContext;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;

import java.util.Map;
import java.util.Optional;

/**
 * Implementations of this interface are supposed to provide methods for retrieving entity and entity's property from
 * context.
 *
 * @since 7.1
 */
@PublicApi
public interface EntityPropertyConditionHelper {

    /**
     * @return the name of the entity.
     */
    String getEntityName();

    /**
     * Extracts the id of the entity from context.
     *
     * @param context - context of the web element for which the condition is evaluated. Please note,
     *                that {@param context} does not have a fixed contract.
     * @return the id of the element if found in context or empty.
     */
    Optional<Long> getEntityId(JiraWebContext context);

    /**
     * Returns the entity property for the given {@param propertyKey} and {@param entityId}.
     *
     * @param user - authenticated user, who retrieves the property.
     * @param entityId - id of the entity.
     * @param propertyKey - key of the property to retrieve.
     *
     * @return the property of invalid if the property does not exist.
     */
    EntityPropertyService.PropertyResult getProperty(ApplicationUser user, Long entityId, String propertyKey);

}
