package com.atlassian.jira.bc.license;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.annotations.PublicApi;
import com.atlassian.application.api.ApplicationKey;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.SortedSet;

/**
 * A service for get license information. (readonly)
 *
 * @since v4.0
 */
@PublicApi
public interface JiraLicenseService {
    /**
     * Gets the server ID of the JIRA instance, creates it if it doesn't already exists.
     *
     * @return the server ID for this JIRA instance.
     */
    String getServerId();

    /**
     * Retrieve a list of all products licenses installed in this instance.
     * <p>
     * In JIRA 6.3 this method returns an iterable containing at most one license. In later versions it may contain
     * more.
     *
     * @return all product licenses installed in this instance.
     * @since 6.3
     */
    @ExperimentalApi
    @Nonnull
    Iterable<LicenseDetails> getLicenses();

    /**
     * Performs minimum validation on the license. But won't check if the license gives you access to applications.
     * Populates the ValidationResult with errors while validating.
     *
     * @param i18nHelper    the helper for i18n
     * @param licenseString the license to validate
     * @return a validation result with the validated license and potential errors.
     * @see {@link JiraLicenseService#validate(com.atlassian.application.api.ApplicationKey, String, com.atlassian.jira.util.I18nHelper)}.
     */
    ValidationResult validate(I18nHelper i18nHelper, String licenseString);

    /**
     * Validates a JIRA application license string in preparation for setting that string as the license for the
     * specified application. This validation does not include checking if the license provides access to the
     * given application, and populates the returned ValidationResult with any errors.
     *
     * @param applicationKey the application key this license is for
     * @param licenseString  the license to validate
     * @param i18nHelper     the helper for i18n
     * @return a validation result with the validated license and potential errors.
     * @since v7.0
     */
    ValidationResult validate(@Nonnull final ApplicationKey applicationKey, @Nonnull final String licenseString, @Nonnull final I18nHelper i18nHelper);

    /**
     * Validates the passed license string against all installed JIRA applications in preparation for setting a new JIRA
     * application license. That is, will the new license cause loss of access to any of the applications under the
     * conditions of the current license. The returned ValidationResult will contain any errors found in the process.
     * <p>
     * It is recommended that {@link #validate(com.atlassian.application.api.ApplicationKey, java.lang.String, com.atlassian.jira.util.I18nHelper)}
     * is used instead of this method.
     *
     * @param i18nHelper    i18n helper used for messaging.
     * @param licenseString the license to validate
     * @return a validation result with the validated license and potential errors.
     * @since v7.0
     * @deprecated use {@link #validate(com.atlassian.application.api.ApplicationKey, java.lang.String, com.atlassian.jira.util.I18nHelper)}
     */
    ValidationResult validateApplicationLicense(@Nonnull I18nHelper i18nHelper, @Nonnull String licenseString);

    /**
     * Validates each license String provided in preparation for setting them. Populates the resulting ValidationResults
     * with errors while validating.
     *
     * @param i18n     the helper for i18n - used to populate the error messages
     * @param licenses the licenses to validate
     * @return all the validation results for the validated licenses including errors
     */
    Iterable<ValidationResult> validate(I18nHelper i18n, Iterable<String> licenses);

    /**
     * Checks that all {@link #getLicenses() existing licenses} are {@link #validate(I18nHelper, String) valid}.
     *
     * @return an ErrorCollection containing 0-many license validation error messages.
     * @since 7.0
     */
    @Nonnull
    Iterable<ValidationResult> validate(I18nHelper i18n);

    /**
     * Returns true if at least one license has been {@link JiraLicenseManager#setLicense(String) set}.
     *
     * @see com.atlassian.jira.license.JiraLicenseManager#isLicenseSet()
     * @since 7.0
     */
    boolean isLicenseSet();

    /**
     * Retrieves the SEN (Support Entitlement Number) of all installed licenses and orders them in a consistent manner.
     * The order is guaranteed for a given set of licenses. It may change when licenses are added or removed.
     * Clients that used to use a single SEN can easily retrieve the first item. That will suffice in most cases, though
     * ensure to consider the consequences of when the licenses changes.
     *
     * @return {@see SortedSet} of the SEN. The ordere of SENs in the set will be maintained as far as the
     * consistency of the licenses installed doesn't change.
     * @see com.atlassian.jira.license.LicenseDetails#getSupportEntitlementNumber()
     * @since 7.0
     */
    SortedSet<String> getSupportEntitlementNumbers();

    /**
     * Holds the validated license and potential errors
     */
    @PublicApi
    interface ValidationResult {
        /**
         * @return a non null {@link com.atlassian.jira.util.ErrorCollection}
         */
        ErrorCollection getErrorCollection();

        /**
         * @return the input licence string
         */
        String getLicenseString();

        /**
         * @return the license details, if available.
         */
        @Nullable
        LicenseDetails getLicenseDetails();

        /**
         * @return the version of the license that was decoded, 0 if the license was not decoded.
         * @deprecated use #getLicenseDetails().getLicenseVersion()
         */
        @Deprecated
        int getLicenseVersion();

        /**
         * @return the total number of users in the JIRA system
         * @deprecated Since 7.0 use
         * {@link com.atlassian.jira.application.ApplicationAuthorizationService#getUserCount(ApplicationKey)} or
         * {@link com.atlassian.jira.user.util.UserManager#getTotalUserCount()}.
         */
        @Deprecated
        int getTotalUserCount();
    }
}
