package com.atlassian.jira.issue.fields.rest.json.beans;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.thumbnail.ThumbnailManager;
import com.atlassian.jira.rest.Dates;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.EmailFormatter;
import com.google.common.base.Objects;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Collection;
import java.util.Date;

/**
 * @since v5.0
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AttachmentJsonBean {
    @JsonProperty
    private String self;

    @JsonProperty
    private String id;

    @JsonProperty
    private String filename;

    @JsonProperty
    private UserJsonBean author;

    @XmlJavaTypeAdapter(Dates.DateTimeAdapter.class)
    private Date created;

    @JsonProperty
    private long size;

    @JsonProperty
    private String mimeType;

    @JsonProperty
    private String content;

    @JsonProperty
    private String thumbnail;

    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public UserJsonBean getAuthor() {
        return author;
    }

    public void setAuthor(UserJsonBean author) {
        this.author = author;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    /**
     * @deprecated Use {@link #shortBeans(java.util.Collection, JiraBaseUrls, com.atlassian.jira.issue.thumbnail.ThumbnailManager, com.atlassian.jira.user.ApplicationUser, com.atlassian.jira.util.EmailFormatter)}
     */
    @Deprecated
    public static Collection<AttachmentJsonBean> shortBeans(final Collection<Attachment> attachments, final JiraBaseUrls urls, final ThumbnailManager thumbnailManager) {
        return ComponentAccessor.getComponent(AttachmentJsonBeanConverter.class).shortBeans(attachments);
    }

    /**
     * @deprecated Use {@link AttachmentJsonBeanConverter#shortBeans(java.util.Collection)}
     */
    @Deprecated
    public static Collection<AttachmentJsonBean> shortBeans(final Collection<Attachment> attachments, final JiraBaseUrls urls, final ThumbnailManager thumbnailManager, final ApplicationUser loggedInUser, final EmailFormatter emailFormatter) {
        return ComponentAccessor.getComponent(AttachmentJsonBeanConverter.class).shortBeans(attachments);
    }

    /**
     * @return null if the input is null
     * @deprecated Use {@link #shortBean(com.atlassian.jira.issue.attachment.Attachment, JiraBaseUrls, com.atlassian.jira.issue.thumbnail.ThumbnailManager, com.atlassian.jira.user.ApplicationUser, com.atlassian.jira.util.EmailFormatter)}
     */
    @Deprecated
    public static AttachmentJsonBean shortBean(final Attachment attachment, final JiraBaseUrls urls, ThumbnailManager thumbnailManager) {
        return ComponentAccessor.getComponent(AttachmentJsonBeanConverter.class).shortBean(attachment);
    }

    /**
     * @deprecated Use {@link AttachmentJsonBeanConverter#shortBean(com.atlassian.jira.issue.attachment.Attachment)}
     */
    @Deprecated
    public static AttachmentJsonBean shortBean(final Attachment attachment, final JiraBaseUrls urls, ThumbnailManager thumbnailManager, final ApplicationUser loggedInUser, final EmailFormatter emailFormatter) {
        return ComponentAccessor.getComponent(AttachmentJsonBeanConverter.class).shortBean(attachment);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(self, id, filename, author, created, size, mimeType, content, thumbnail);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final AttachmentJsonBean other = (AttachmentJsonBean) obj;
        return Objects.equal(this.self, other.self)
                && Objects.equal(this.id, other.id)
                && Objects.equal(this.filename, other.filename)
                && Objects.equal(this.author, other.author)
                && Objects.equal(this.created, other.created)
                && Objects.equal(this.size, other.size)
                && Objects.equal(this.mimeType, other.mimeType)
                && Objects.equal(this.content, other.content)
                && Objects.equal(this.thumbnail, other.thumbnail);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("self", self)
                .add("id", id)
                .add("filename", filename)
                .add("author", author)
                .add("created", created)
                .add("size", size)
                .add("mimeType", mimeType)
                .add("content", content)
                .add("thumbnail", thumbnail)
                .toString();
    }
}
