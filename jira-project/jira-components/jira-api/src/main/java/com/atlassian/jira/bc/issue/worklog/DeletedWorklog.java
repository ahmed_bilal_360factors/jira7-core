package com.atlassian.jira.bc.issue.worklog;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.entity.WithId;

import javax.annotation.concurrent.Immutable;
import java.util.Date;

/**
 * Information about removed worklog.
 */
@PublicApi
@Immutable
public class DeletedWorklog implements WithId {
    private final Long id;
    private final Date deletionTime;

    public DeletedWorklog(final Long id, final Date deletionTime) {
        this.id = id;
        this.deletionTime = deletionTime;
    }

    /**
     * @return id of the removed worklog.
     */
    public Long getId() {
        return id;
    }

    /**
     * @return time of deletion of worklog.
     */
    public Date getDeletionTime() {
        return deletionTime;
    }
}

