package com.atlassian.jira.license;

import com.atlassian.annotations.Internal;
import com.atlassian.application.api.ApplicationKey;

import javax.annotation.Nonnull;
import java.util.Set;


/**
 * Encapsulates the application/role information embedded in a {@link com.atlassian.jira.license.LicenseDetails license}.
 * <p>
 * Typical usage:
 * <pre>
 * {@code
 *     JiraLicenseManager jlm = ...;
 *     LicenseDetails licenseDetails = jlm.getLicense();
 *     LicensedApplications applications = licenseDetails.getLicensedApplications();
 *
 *     Set<ApplicationKey> keys = applications.getKeys();
 *     for (ApplicationKey key : keys)
 *     {
 *         int numSeats = roleDetails.getUserLimit( key );
 *         ...
 *     }
 * }
 * </pre>
 *
 * @see com.atlassian.extras.common.LicensePropertiesConstants#UNLIMITED_USERS
 * @see com.atlassian.jira.application.ApplicationRoleManager
 * @see LicenseDetails#getLicensedApplications()
 * @since 7.0
 */
public interface LicensedApplications {
    /**
     * Returns the {@link com.atlassian.application.api.ApplicationKey}s encoded in the source
     * {@link com.atlassian.jira.license.LicenseDetails license}. This may return an empty set, but never
     * returns null.
     */
    @Nonnull
    Set<ApplicationKey> getKeys();

    /**
     * Returns the number of seats for the given {@link com.atlassian.application.api.ApplicationKey}. It will return:
     * <ol>
     * <li>0 if the passed {@code ApplicationKey} is not licensed.</li>
     * <li>
     * {@link com.atlassian.extras.common.LicensePropertiesConstants#UNLIMITED_USERS} if the passed
     * {@code ApplicationKey} has no limit.
     * </li>
     * <li>{@code positive number} when the passed {@code ApplicationKey} has that exact limit.</li>
     * </ol>
     *
     * @return the number of users/seats for the given {@link ApplicationKey}, 0, or
     * {@link com.atlassian.extras.common.LicensePropertiesConstants#UNLIMITED_USERS}.
     */
    int getUserLimit(@Nonnull ApplicationKey key);

    /**
     * Returns a user-friendly string version of the {@link com.atlassian.application.api.Application}s
     * encoded in the source {@link com.atlassian.jira.license.LicenseDetails license}, or the empty string
     * if there are no Applications in the source license.
     */
    @Nonnull
    String getDescription();

    /**
     * Indicates whether the license key has been issued as an JIRA Application license ( role based license ) or
     * whether we have interpreted it as an JIRA Application license.
     *
     * @return true if license key was issued as an JIRA Application license ( role based license ), false if we have
     * interpreted license as JIRA Application license or when no interpretation occurred.
     * @since 7.0
     */
    @Internal
    boolean hasNativeRole();
}
