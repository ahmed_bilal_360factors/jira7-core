package com.atlassian.jira.project;

import com.atlassian.annotations.ExperimentalApi;

import java.util.Optional;

/**
 * Manager for retrieving last changed time for any issues in the project.
 * @since v7.2
 */
@ExperimentalApi
public interface ProjectChangedTimeManager {
    /**
     * Retrieves {@link ProjectChangedTime} for the given project id.
     * It will return Optional.EMPTY when:
     *  - the project ID is invalid
     *  - project contains zero issues and has never had issues
     *  - none of the issues in the project have been changed since upgrading to JIRA 7.2
     * @param projectId
     * @return {@link ProjectChangedTime}
     */
    Optional<ProjectChangedTime> getProjectChangedTime(long projectId);
}
