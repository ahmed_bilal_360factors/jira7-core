package com.atlassian.jira.issue.index;

import com.atlassian.annotations.PublicApi;
import com.google.common.primitives.Booleans;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

/**
 * Provides parameters required to conduct indexing or re-indexing JIRA issues. <br/>
 * <ul>
 * <li>indexIssues - true if issues should be indexed</li>
 * <li>indexComments - true if comments should be indexed</li>
 * <li>indexChangeHistory - true if change history should be indexed</li>
 * <li>indexWorklogs true if issue worklogs should be indexed</li>
 * <li>forceReloadFromDatabase - true if issues should be reloaded from the database before indexing</li>
 * </ul>
 * Clients should use the provided
 * {@link IssueIndexingParams.Builder} to construct an instance of this class.
 *
 * @see #INDEX_ALL
 * @see #INDEX_ISSUE_ONLY
 * @see #INDEX_NONE
 * @since v6.4
 */
@Immutable
@PublicApi
public class IssueIndexingParams {
    /**
     * Sets all values to false.
     */
    public static IssueIndexingParams INDEX_NONE = new IssueIndexingParams(false, false, false, false, false);

    /**
     * Only index the core issue values, not comments, history etc. Issues will be reloaded from the database before indexing.
     */
    public static IssueIndexingParams INDEX_ISSUE_ONLY = new IssueIndexingParams(true, false, false, false, true);

    /**
     * Sets all values to true.
     */
    public static IssueIndexingParams INDEX_ALL = new IssueIndexingParams(true, true, true, true, true);

    private final boolean indexIssues;
    private final boolean indexChangeHistory;
    private final boolean indexComments;
    private final boolean indexWorklogs;
    private final boolean forceReloadFromDatabase;

    private IssueIndexingParams(
            final boolean indexIssues,
            final boolean indexChangeHistory,
            final boolean indexComments,
            final boolean indexWorklogs,
            final boolean forceReloadFromDatabase) {
        this.indexIssues = indexIssues;
        this.indexChangeHistory = indexChangeHistory;
        this.indexComments = indexComments;
        this.indexWorklogs = indexWorklogs;
        this.forceReloadFromDatabase = forceReloadFromDatabase;
    }

    public static Builder builder() {
        return new Builder();
    }

    /**
     * Return a new builder with the values initially set to those specified by issueIndexingParams
     *
     * @param issueIndexingParams The default values for new builder. Can not be null
     * @return New Builder
     * @since v7.0
     */
    public static Builder builder(@Nonnull IssueIndexingParams issueIndexingParams) {
        return new Builder(issueIndexingParams);
    }

    public boolean isIndexIssues() {
        return indexIssues;
    }

    public boolean isIndexChangeHistory() {
        return indexChangeHistory;
    }

    public boolean isIndexComments() {
        return indexComments;
    }

    public boolean isIndexWorklogs() {
        return indexWorklogs;
    }

    /**
     * @return True if the issues should be reloaded from the database before performing the actual index
     * @since v7.0
     */
    public boolean isForceReloadFromDatabase() {
        return forceReloadFromDatabase;
    }

    public boolean isIndex() {
        return indexIssues || indexChangeHistory || indexComments || indexWorklogs;
    }

    public boolean isIndexAll() {
        return indexIssues && indexChangeHistory && indexComments && indexWorklogs;
    }

    /**
     * Return the number of indexes that needs to be updated
     *
     * @return number of indexes to be updated
     */
    public int getAffectedIndexCount() {
        return Booleans.countTrue(indexIssues, indexChangeHistory, indexComments, indexWorklogs);
    }

    @Override
    public String toString() {
        return "{" +
                "indexIssues=" + indexIssues +
                ", indexChangeHistory=" + indexChangeHistory +
                ", indexComments=" + indexComments +
                ", indexWorklogs=" + indexWorklogs +
                ", forceReloadFromDatabase=" + forceReloadFromDatabase +
                '}';
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final IssueIndexingParams that = (IssueIndexingParams) o;

        if (indexChangeHistory != that.indexChangeHistory) {
            return false;
        }
        if (indexComments != that.indexComments) {
            return false;
        }
        if (indexIssues != that.indexIssues) {
            return false;
        }
        if (indexWorklogs != that.indexWorklogs) {
            return false;
        }

        return forceReloadFromDatabase == that.forceReloadFromDatabase;
    }

    @Override
    public int hashCode() {
        int result = (indexIssues ? 1 : 0);
        result = 31 * result + (indexChangeHistory ? 1 : 0);
        result = 31 * result + (indexComments ? 1 : 0);
        result = 31 * result + (indexWorklogs ? 1 : 0);
        return 31 * result + (forceReloadFromDatabase ? 1 : 0);
    }

    public static class Builder {
        private boolean indexIssues = true;
        private boolean indexChangeHistory = false;
        private boolean indexComments = false;
        private boolean indexWorklogs = false;
        private boolean forceReloadFromDatabase = true;

        public Builder() {
        }

        /**
         * @param issueIndexingParams The parameters to default this builder to. Can not be null
         * @since v7.0
         */
        public Builder(@Nonnull IssueIndexingParams issueIndexingParams) {
            this.indexIssues = issueIndexingParams.indexIssues;
            this.indexChangeHistory = issueIndexingParams.indexChangeHistory;
            this.indexComments = issueIndexingParams.indexComments;
            this.indexWorklogs = issueIndexingParams.indexWorklogs;
            this.forceReloadFromDatabase = issueIndexingParams.forceReloadFromDatabase;
        }

        /**
         * Creates builder with ORs on each param
         */
        public Builder addIndexingParams(final IssueIndexingParams issueIndexingParams) {
            indexIssues |= issueIndexingParams.indexIssues;
            indexChangeHistory |= issueIndexingParams.indexChangeHistory;
            indexComments |= issueIndexingParams.indexComments;
            indexWorklogs |= issueIndexingParams.indexWorklogs;
            forceReloadFromDatabase |= issueIndexingParams.forceReloadFromDatabase;
            return this;
        }

        public Builder setIssues(final boolean indexIssues) {
            this.indexIssues = indexIssues;
            return this;
        }

        public Builder setChangeHistory(final boolean indexChangeHistory) {
            this.indexChangeHistory = indexChangeHistory;
            return this;
        }

        public Builder setComments(final boolean indexComments) {
            this.indexComments = indexComments;
            return this;
        }

        public Builder setWorklogs(final boolean indexWorklogs) {
            this.indexWorklogs = indexWorklogs;
            return this;
        }

        /**
         * @since v7.0
         */
        public Builder setForceReloadFromDatabase(final boolean forceReloadFromDatabase) {
            this.forceReloadFromDatabase = forceReloadFromDatabase;
            return this;
        }

        public Builder withChangeHistory() {
            this.indexChangeHistory = true;
            return this;
        }

        public Builder withComments() {
            this.indexComments = true;
            return this;
        }

        public Builder withWorklogs() {
            this.indexWorklogs = true;
            return this;
        }

        public Builder withoutIssues() {
            this.indexIssues = false;
            return this;
        }

        public IssueIndexingParams build() {
            return new IssueIndexingParams(indexIssues, indexChangeHistory, indexComments, indexWorklogs, forceReloadFromDatabase);
        }
    }
}
