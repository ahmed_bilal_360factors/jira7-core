package com.atlassian.jira.issue.export;

import com.atlassian.annotations.ExperimentalApi;

import java.util.stream.Stream;

/**
 * This represents a single entity for the {@link FieldExportParts}.
 * <p>
 * For example, in the {@code ProjectSystemField} the project ID would be one of these {@link FieldExportPart}
 * This will store the the id to uniquely identify this item in the {@link FieldExportParts}, a label to
 * represent the title of the field item, such as "Project ID". It also stores the values for this field item, this
 * could be like some fields, such as Comments, which store multiple values.
 *
 * @since 7.2.0
 */
@ExperimentalApi
public class FieldExportPart {
    final private String id;
    final private String label;
    final private Stream<String> values;

    FieldExportPart(String id, String label, Stream<String> values) {
        this.id = id;
        this.label = label;
        this.values = values;
    }

    /**
     * Represents a unique Id for this item within the {@link FieldExportParts} to allow for grouping.
     * For example, the ProjectSystemField has a Project Key item which would have the id "key" therefore, if there
     * are multiple values for this item it can calculate the maximum values for all the item in the export. This
     * does not need to be unique between different fields, only this field.
     *
     * @return a unique (in the context of this field) id representing this item
     */
    public String getId() {
        return id;
    }

    /**
     * Gets a label that can be used to uniquely label this item and it's value. For example, in a CSV export with
     * multiple comments the item label could be "Comment". The implementor of this field can internationalise this
     * value using the I18nHelper in the context if they wish.
     *
     * @return String label for this item.
     */
    public String getItemLabel() {
        return label;
    }

    /**
     * Get the values for this representation item, some field items can have multiple values, such as Comments
     * so this stores all of the different values for this item.
     *
     * @return list of values for this item
     */
    public Stream<String> getValues() {
        return values;
    }
}
