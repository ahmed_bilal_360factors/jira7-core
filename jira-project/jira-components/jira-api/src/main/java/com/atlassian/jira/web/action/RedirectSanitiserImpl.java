package com.atlassian.jira.web.action;

import com.atlassian.http.url.SameOrigin;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;

public class RedirectSanitiserImpl implements RedirectSanitiser {
    // Logs to atlassian-jira-security.log - inherited from SafeRedirectChecker
    private static final Logger securityLog = LoggerFactory.getLogger("com.atlassian.jira.login.security");

    private final VelocityRequestContextFactory velocityRequestContextFactory;

    public RedirectSanitiserImpl(VelocityRequestContextFactory velocityRequestContextFactory) {
        this.velocityRequestContextFactory = velocityRequestContextFactory;
    }

    @Override
    public boolean canRedirectTo(@Nullable String redirectUri) {
        if (redirectUri == null) {
            return true;
        }
        try {
            final URI uri = new URI(redirectUri);
            if (uri.getHost() != null && uri.getScheme() == null) {
                return false;
            }
            if (getCanonicalBaseURL() == null) {
                return !uri.isAbsolute();
            }
            final URI baseUrl = new URI(getCanonicalBaseURL());
            if (uri.isAbsolute() && !SameOrigin.isSameOrigin(uri, baseUrl)) {
                return false;
            }
            return uri.getPath() == null || !uri.getPath().contains(":");
        } catch (URISyntaxException | MalformedURLException e) {
            return false;
        }
    }

    @Nullable
    @Override
    public String makeSafeRedirectUrl(@Nullable String redirectUrl) {
        if (redirectUrl == null) {
            // NULLs are safe...
            return null;
        }

        if (!canRedirectTo(redirectUrl)) {
            securityLog.warn("Potential malicious redirect detected: " + redirectUrl);
            return null;
        }

        return redirectUrl;
    }

    /**
     * Returns the canonical base URL for JIRA.
     *
     * @return a String containing the canonical base URL
     */
    protected String getCanonicalBaseURL() {
        return velocityRequestContextFactory.getJiraVelocityRequestContext().getCanonicalBaseUrl();
    }
}
