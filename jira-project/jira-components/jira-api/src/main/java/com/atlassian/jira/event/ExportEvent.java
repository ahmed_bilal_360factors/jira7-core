package com.atlassian.jira.event;

import com.atlassian.analytics.api.annotations.EventName;

/**
 * Fired after a successful issue list export.
 *
 * @since v7.2
 */
public class ExportEvent extends AbstractEvent {

    private final String key;

    public ExportEvent(final String key) {
        super();
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    @EventName
    public String calculateEventName() {
        return "jira.export." + getKey();
    }
}
