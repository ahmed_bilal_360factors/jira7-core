package com.atlassian.jira.issue.fields.rest.json.beans;

import com.atlassian.jira.issue.security.IssueSecurityLevel;
import com.atlassian.jira.issue.security.IssueSecurityLevelScheme;
import com.atlassian.jira.util.JiraUrlCodec;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Bean representing an issue security scheme with issue security levels.
 *
 * @since 7.0
 */
public class SecuritySchemeJsonBean {
    public SecuritySchemeJsonBean(String self, Long id, String name, String description) {
        this.self = self;
        this.id = id;
        this.name = name;
        this.description = description;
    }

    @JsonProperty
    private String self;

    @JsonProperty
    private Long id;

    @JsonProperty
    private String name;

    @JsonProperty
    private String description;

    @JsonProperty
    private Long defaultSecurityLevelId;

    @JsonProperty
    private Collection<SecurityLevelJsonBean> levels;

    public String getSelf() {
        return self;
    }

    public void setSelf(final String self) {
        this.self = self;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Long getDefaultSecurityLevelId() {
        return defaultSecurityLevelId;
    }

    public void setDefaultSecurityLevelId(final Long defaultSecurityLevelId) {
        this.defaultSecurityLevelId = defaultSecurityLevelId;
    }

    public Collection<SecurityLevelJsonBean> getLevels() {
        return levels;
    }

    public void setLevels(final Collection<SecurityLevelJsonBean> levels) {
        this.levels = levels;
    }

    public static List<SecuritySchemeJsonBean> fromIssueSecuritySchemes(final List<IssueSecurityLevelScheme> schemes, final JiraBaseUrls urls) {
        return Arrays.asList(Iterables.toArray(Iterables.transform(schemes, new Function<IssueSecurityLevelScheme, SecuritySchemeJsonBean>() {
            @Override
            public SecuritySchemeJsonBean apply(final IssueSecurityLevelScheme securityLevelScheme) {
                return shortBean(securityLevelScheme, urls);
            }
        }), SecuritySchemeJsonBean.class));
    }

    public static SecuritySchemeJsonBean shortBean(final IssueSecurityLevelScheme securityLevelScheme, final JiraBaseUrls urls) {
        SecuritySchemeJsonBean bean = new SecuritySchemeJsonBean(
                getSelf(urls.restApi2BaseUrl(), JiraUrlCodec.encode(securityLevelScheme.getId().toString())),
                securityLevelScheme.getId(),
                securityLevelScheme.getName(),
                securityLevelScheme.getDescription());
        bean.setDefaultSecurityLevelId(securityLevelScheme.getDefaultSecurityLevelId());
        return bean;
    }

    public static SecuritySchemeJsonBean fullBean(final IssueSecurityLevelScheme securityLevelScheme, final JiraBaseUrls urls, Collection<IssueSecurityLevel> levels) {
        SecuritySchemeJsonBean bean = shortBean(securityLevelScheme, urls);
        bean.setLevels(SecurityLevelJsonBean.shortBeans(levels, urls));
        return bean;
    }

    public static String getSelf(String baseUrl, String encodedId) {
        return baseUrl + "issuesecurityschemes/" + encodedId;
    }
}
