package com.atlassian.jira.config.properties;

import com.atlassian.annotations.Internal;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.TreeMap;
import java.util.regex.Pattern;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Object to process some properties before display.
 *
 * @since 7.0
 */
@Internal
final class PropertiesProcessor {
    private static Pattern PASSWORD = Pattern.compile("password", Pattern.CASE_INSENSITIVE);

    private final Map<String, String> properties;

    PropertiesProcessor(Properties properties) {
        notNull("properties", properties);
        final Map<String, String> props = new LinkedHashMap<>();
        for (Map.Entry<?, ?> entry : properties.entrySet()) {
            props.put(Objects.toString(entry.getKey()), Objects.toString(entry.getValue()));
        }
        this.properties = Collections.unmodifiableMap(props);
    }

    private PropertiesProcessor(Map<String, String> properties) {
        this.properties = properties;
    }

    PropertiesProcessor sanitise(Pattern from, String to) {
        final Map<String, String> clone = new LinkedHashMap<>();
        for (Map.Entry<String, String> entry : properties.entrySet()) {
            final String key = entry.getKey();
            if (from.matcher(key).find()) {
                clone.put(key, to);
            } else {
                clone.put(key, entry.getValue());
            }
        }
        return new PropertiesProcessor(Collections.unmodifiableMap(clone));
    }

    PropertiesProcessor sort() {
        Map<String, String> props = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        props.putAll(this.properties);
        return new PropertiesProcessor(Collections.unmodifiableMap(props));
    }

    PropertiesProcessor sanitisePassword() {
        return sanitise(PASSWORD, "****");
    }

    Map<String, String> toMap() {
        return properties;
    }
}
