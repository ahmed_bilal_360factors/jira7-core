package com.atlassian.jira.event.user;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.jira.event.AbstractEvent;
import com.atlassian.jira.user.ApplicationUser;

import java.util.Map;

/**
 * Event to emit as a user resets their password.
 */
public class OnboardingEvent {

    private static final String BASE_EVENT_NAME = "onboarding.email";

    private String username;
    private boolean tokenIsTimedOut;

    /**
     * @param username          The username of the onboarding user
     * @param tokenIsTimedOut   Flag if the token has timed out
     */
    public OnboardingEvent(String username, boolean tokenIsTimedOut) {
        this.username = username;
        this.tokenIsTimedOut = tokenIsTimedOut;
    }

    /**
     * @return the username of the onboarding user
     */
    public String getUsername() {
        return username;
    }

    /**
     * @return the full name of the event.
     */
    @EventName
    public String buildEventName() {
        String eventNameModifier = ".click";
        if (tokenIsTimedOut) {
            eventNameModifier = ".linkExpired";
        }
        return BASE_EVENT_NAME + eventNameModifier;
    }

}
