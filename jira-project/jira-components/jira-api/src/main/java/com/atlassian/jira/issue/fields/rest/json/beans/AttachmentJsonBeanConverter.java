package com.atlassian.jira.issue.fields.rest.json.beans;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.issue.attachment.Attachment;

import java.util.Collection;

/**
 * @since v6.5
 */
@PublicApi
public interface AttachmentJsonBeanConverter {
    /**
     * @return null if the input is null
     */
    public Collection<AttachmentJsonBean> shortBeans(final Collection<Attachment> attachments);

    /**
     * @return null if the input is null
     */
    public AttachmentJsonBean shortBean(final Attachment attachment);

}
