package com.atlassian.jira.event.cluster;

import com.atlassian.annotations.PublicApi;

/**
 * Thrown when a clustered JIRA instance has persisted its heartbeat into DB.
 *
 * @since v7.3
 */
@PublicApi
public class HeartbeatEvent {
    public static final HeartbeatEvent INSTANCE = new HeartbeatEvent();

    private HeartbeatEvent() {
    }
}
