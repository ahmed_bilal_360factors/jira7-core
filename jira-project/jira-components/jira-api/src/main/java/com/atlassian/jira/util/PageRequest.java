package com.atlassian.jira.util;

import com.atlassian.annotations.PublicApi;

/**
 * Pagination request.
 *
 * @since 6.4.7
 */
@PublicApi
public interface PageRequest {
    /**
     * The maximum size of any page limit.
     */
    int MAX_PAGE_LIMIT = 1024 * 1024;

    /**
     * @return the maximum allowed number of elements on the page
     */
    int getLimit();

    /**
     * @return the index of the  element in the result set that this page will start at
     */
    long getStart();
}
