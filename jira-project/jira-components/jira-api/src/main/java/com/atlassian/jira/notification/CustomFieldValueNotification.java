package com.atlassian.jira.notification;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.notification.type.NotificationType;

/**
 * Notification to users who are specified in the value of a custom field.
 *
 * @since 7.0
 */
@PublicApi
public class CustomFieldValueNotification extends AbstractNotification {
    private final CustomField customField;

    public CustomFieldValueNotification(final Long id, final NotificationType notificationType, final CustomField customField, final String parameter) {
        super(id, notificationType, parameter);
        this.customField = customField;
    }

    public CustomField getCustomField() {
        return customField;
    }

    @Override
    public <X> X accept(final NotificationVisitor<X> notificationVisitor) {
        return notificationVisitor.visit(this);
    }
}
