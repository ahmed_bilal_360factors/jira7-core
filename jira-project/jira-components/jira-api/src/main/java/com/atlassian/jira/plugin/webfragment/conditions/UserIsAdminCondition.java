package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.annotations.Internal;
import com.atlassian.annotations.PublicApi;
import com.atlassian.fugue.Either;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;

import static com.atlassian.jira.plugin.webfragment.conditions.cache.ConditionCacheKeys.permission;
import static com.atlassian.jira.plugin.webfragment.conditions.cache.RequestCachingConditionHelper.cacheConditionResultInRequest;

/**
 * Checks if this user has the global admin permission.  This was previously implemented in jira-core but has been moved
 * into the API for 6.1.
 *
 * @since 6.1
 */
@PublicApi
public class UserIsAdminCondition extends AbstractWebCondition {

    private final Either<PermissionManager, GlobalPermissionManager> permissionManager;

    @Internal
    public UserIsAdminCondition(GlobalPermissionManager permissionManager) {
        this.permissionManager = Either.right(permissionManager);
    }

    /**
     * This needs to be left here as it's used by the jira-projects-plugin.
     *
     * @deprecated Use {@link UserIsAdminCondition#UserIsAdminCondition(GlobalPermissionManager)} instead. Since v7.1.
     */
    @Deprecated
    protected UserIsAdminCondition(PermissionManager permissionManager) {
        this.permissionManager = Either.left(permissionManager);
    }

    public boolean shouldDisplay(final ApplicationUser user, JiraHelper jiraHelper) {
        return cacheConditionResultInRequest(permission(GlobalPermissionKey.ADMINISTER, user),
                () -> permissionManager.fold(
                        permissionManager -> permissionManager.hasPermission(Permissions.ADMINISTER, user),
                        permissionManager -> permissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user)
                ));
    }
}
