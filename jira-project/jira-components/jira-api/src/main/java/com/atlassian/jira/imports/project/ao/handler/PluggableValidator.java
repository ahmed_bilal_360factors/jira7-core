package com.atlassian.jira.imports.project.ao.handler;

import com.atlassian.annotations.ExperimentalSpi;
import com.atlassian.jira.imports.project.core.BackupProject;
import com.atlassian.jira.imports.project.core.ValidationMessage;
import com.atlassian.jira.util.I18nHelper;

/**
 * Defines a validator class that will be able check if a project can be imported.
 * <p>A plugin can veto the import if configuration needs to be updated before an import of a project is valid and sensible.
 * </p>
 * <p>
 * The validator is called when the user selects a particular project in the UI to import. Errors and Warnings from the validation
 * will be presented to the user and they can then modify the configuration of the target system to work around any problems reported.
 * </p>
 * <p>
 * A validator would typically check the state of the target system against the data gathered in the {@link BackupProject} and
 * especially additional data from {@link BackupProject#getAdditionalData(String)} that may have been captured by the
 * plugins's own implementation of {@link com.atlassian.jira.imports.project.ao.handler.PluggableOverviewAoEntityHandler}.
 * </p>
 *
 * @since v7.0
 */
@ExperimentalSpi
public interface PluggableValidator {
    ValidationMessage validate(BackupProject backupProject, I18nHelper i18n);
}
