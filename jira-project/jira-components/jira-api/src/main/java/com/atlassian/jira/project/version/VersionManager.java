package com.atlassian.jira.project.version;

import com.atlassian.annotations.PublicApi;
import com.atlassian.fugue.Option;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Manager responsible for <a href="http://www.atlassian.com/software/jira/docs/latest/version_management.html">JIRA versions</a>.
 */
@PublicApi
public interface VersionManager {
    /**
     * Used to represent empty version fields
     */
    String NO_VERSIONS = "-1";

    /**
     * Used to retrieve all unreleased versions
     */
    String ALL_UNRELEASED_VERSIONS = "-2";

    /**
     * Used to retrieve all released versions
     */
    String ALL_RELEASED_VERSIONS = "-3";


    /**
     * Creates a new {@link Version} object.
     *
     * @param name                 the Name
     * @param releaseDate          date of release or null if not released.
     * @param description          the Description
     * @param projectId            the id of the Project of the version.
     * @param scheduleAfterVersion id of the version after which this should be sequenced or null.
     * @return the new Version.
     * @throws CreateException If there was a problem creating the version.
     * @deprecated Since JIRA version 7.0.1, this is deprecated on favour of
     * {@link #createVersion(String, java.util.Date, java.util.Date, String, Long, Long, boolean)}
     * which lets you specify the <code>startDate</code> and <code>release</code> attribute values
     * for the new <code>Version</code> object.
     */
    Version createVersion(String name, Date releaseDate, String description, Long projectId, Long scheduleAfterVersion) throws CreateException;

    /**
     * Creates a new {@link Version} object.
     *
     * @param name                 the Name
     * @param startDate            start date of the version or null
     * @param releaseDate          date of release or null if not released.
     * @param description          the Description
     * @param projectId            the id of the Project of the version.
     * @param scheduleAfterVersion id of the version after which this should be sequenced or null.
     * @return the new Version.
     * @throws CreateException If there was a problem creating the version.
     * @since v6.0
     * @deprecated Since JIRA version 7.0.1, this is deprecated on favour of
     * {@link #createVersion(String, java.util.Date, java.util.Date, String, Long, Long, boolean)}
     * which lets you specify the <code>release</code> attribute value for the new
     * <code>Version</code> object.
     */
    @Deprecated
    Version createVersion(final String name, final Date startDate, final Date releaseDate, final String description, final Long projectId, final Long scheduleAfterVersion) throws CreateException;

    /**
     * Creates a new {@link Version}.
     * This method differs from {@link #createVersion(String, java.util.Date, java.util.Date, String, Long, Long)} in
     * that it lets you specify the value for the attribute <code>release</code>.
     *
     * @param name                 Name of the version
     * @param startDate            Start date of the version
     * @param releaseDate          Release date for the version
     * @param description          Version description
     * @param projectId            The numeric id of the project this version belongs
     * @param scheduleAfterVersion Numeric id of the version to schedule after the given version object
     * @param released             Sets the <code>release</code> attribute value for the version object to create
     * @return The created version object
     * @throws CreateException
     * @since v7.0.1
     */
    Version createVersion(final String name, final Date startDate, final Date releaseDate, final String description, final Long projectId, final Long scheduleAfterVersion, final boolean released) throws CreateException;

    // ---- Scheduling Methods ----

    /**
     * Move a version to the start of the version list
     *
     * @param version the Version to move
     */
    Version moveToStartVersionSequence(Version version);

    /**
     * Move a version to have a lower sequence number - ie make it earlier
     *
     * @param version the Version
     */
    Version increaseVersionSequence(Version version);

    /**
     * Move a version to have a higher sequence number - ie make it later
     *
     * @param version the Version
     */
    Version decreaseVersionSequence(Version version);

    /**
     * Move a version to the end of the version sequence
     *
     * @param version the Version
     */
    Version moveToEndVersionSequence(Version version);

    /**
     * Move a version after another version
     *
     * @param version              version to reschedule
     * @param scheduleAfterVersion id of the version to schedule after the given version object
     */
    Version moveVersionAfter(Version version, Long scheduleAfterVersion);

    // ---- Delete Version Methods ----

    /**
     * Removes a specific version from the system.
     *
     * @param version The version to be removed.
     * @see VersionManager#deleteAndRemoveFromIssues(com.atlassian.jira.user.ApplicationUser, Version)
     */
    void deleteVersion(final Version version);

    /**
     * Removes a specific version from the system. {@code versionToDelete} is the version which is going to be removed.
     * If {@code affectsSwapVersion} is defined, then issues with {@code versionToDelete} as a value of Affects Version/s
     * system field will use {@code affectsSwapVersion} instead.
     * Similarly, {@code fixSwapVersion} will replace {@code versionToDelete} as a value in Fix Version/s system field.
     *
     * @param applicationUser    the user that will be used to update related issues
     * @param versionToDelete    the version to be deleted
     * @param affectsSwapVersion the version which replaces the version to be deleted as Affected Version/s.
     * @param fixSwapVersion     the version which replaces the version to be deleted as Fix Version/s.
     *
     * @deprecated Use {@link #deleteVersionAndSwap(ApplicationUser, DeleteVersionWithCustomFieldParameters)} that
     * supports swapping version in custom fields.
     * Since v7.0.10
     */
    void deleteVersion(final ApplicationUser applicationUser, final Version versionToDelete, Option<Version> affectsSwapVersion, Option<Version> fixSwapVersion);

    /**
     * Deletes all versions in given project.
     * Note that this method will not fire a VersionDeleteEvent upon deletion of a version.
     *
     * @param projectId the id of project in which versions will be deleted.
     */
    void deleteAllVersions(@Nonnull final Long projectId);

    // ---- Edit Version Name Methods ----

    /**
     * Updates details of an existing version.
     *
     * @param version     The version to update
     * @param name        The new version name, cannot be empty.
     * @param description The description of this version.
     * @throws IllegalArgumentException If the name is not set, or already exists.
     */
    Version editVersionDetails(Version version, String name, String description);

    /**
     * Check that the version name we are changing to is not a duplicate.
     *
     * @param version The version to update
     * @param name    The new name for the version
     * @return true if there is already a version with that name for the project
     */
    boolean isDuplicateName(Version version, final String name);

    // ---- Release Version Methods ----

    /**
     * Used to release or unrelease a version, depending on the release flag.
     *
     * @param version Version to be released (or unreleased)
     * @param release True to release a version. False to 'unrelease' a version
     */
    Version releaseVersion(Version version, boolean release);

    /**
     * Used to release versions depending on the release flag.
     *
     * @param versions Collection of {@link Version}s
     * @param release  True to release a version. False to 'unrelease' a version
     */
    Collection<Version> releaseVersions(Collection<Version> versions, boolean release);

    /**
     * Swaps the list of issues supplied from one Fix version to another.
     *
     * @param issues         the Issues
     * @param currentVersion Current fix version they will be swapped from
     * @param swapToVersion  New fix version they will be swapped to.
     * @throws IndexException if an error occurs will indexing these new issue values.
     */
    void moveIssuesToNewVersion(List<Issue> issues, Version currentVersion, Version swapToVersion) throws IndexException;

    // ---- Archive Version Methods ----

    /**
     * Method used to archive and un-archive a number of versions.
     *
     * @param idsToArchive   Archive all these Versions
     * @param idsToUnarchive Unarchive these Versions
     */
    void archiveVersions(String[] idsToArchive, String[] idsToUnarchive);

    /**
     * Archive/Un-archive a single version depending on the archive flag.
     *
     * @param version the Version to archive or unarchive
     * @param archive new archive value
     */
    Version archiveVersion(Version version, boolean archive);

    /**
     * Return all un-archived versions for a particular project
     *
     * @param projectId id of the project.
     * @return A collection of {@link Version}s
     * @since v3.10
     */
    Collection<Version> getVersionsUnarchived(Long projectId);

    /**
     * Return all archived versions for a particular project.
     *
     * @param project the Project
     * @return Archived versions for this project.
     */
    Collection<Version> getVersionsArchived(final Project project);

    /**
     * Persists updates to the specified version object.
     *
     * @param version the version
     * @return the updated version
     * @since 6.0
     */
    Version update(Version version);

    // ---- Version Release Date Mthods ----

    /**
     * Update the release date of a version.
     *
     * @param version the Version to edit
     * @param duedate new release date
     */
    Version editVersionReleaseDate(Version version, Date duedate);

    /**
     * Updates the start date of a version
     *
     * @param version   the version to edit
     * @param startDate new start date
     * @since v6.0
     */
    Version editVersionStartDate(Version version, Date startDate);

    /**
     * Updates the start and release date of a version
     *
     * @param version     the version to edit
     * @param startDate   new start date
     * @param releaseDate new release date
     * @since v6.0
     */
    Version editVersionStartReleaseDate(Version version, Date startDate, Date releaseDate);

    /**
     * Checks to see if a version is overdue.  Note: This method checks if the due date
     * set for a version is previous to last midnight. (not now()).
     *
     * @param version the Version
     * @return True if the version is overdue. (i.e. releaseDate is before last midnight)
     */
    boolean isVersionOverDue(Version version);

    /**
     * Return a list of Versions for the given project.
     *
     * @param projectId the Project
     * @return a list of Versions for the given project.
     * @see VersionManager#getVersions(Project)
     */
    List<Version> getVersions(Long projectId);

    /**
     * Return a list of Versions for the given project.
     *
     * @param projectId       the Project
     * @param includeArchived whether or not to include archived versions
     * @return a list of Versions for the given project.
     * @see VersionManager#getVersions(Project)
     */
    List<Version> getVersions(Long projectId, boolean includeArchived);

    /**
     * Return a list of Versions for the given project.
     *
     * @param project the Project
     * @return a list of Versions for the given project.
     * @see VersionManager#getVersions(Long)
     */
    List<Version> getVersions(Project project);

    /**
     * Return a collection of {@link Version}s that have the specified name.
     *
     * @param versionName the name of the version (case-insensitive)
     * @return a Collection of Version objects. Never null.
     */
    Collection<Version> getVersionsByName(String versionName);

    /**
     * Return a collection of {@link Version}s matching the ids passed in.
     *
     * @param ids Version IDs
     * @return a collection of {@link Version}s matching the ids passed in.
     */
    Collection<Version> getVersions(List<Long> ids);

    /**
     * Returns a single version.
     *
     * @param id the Version ID
     * @return A {@link Version} object.
     */
    Version getVersion(Long id);

    /**
     * Search for a version by projectID and name.
     *
     * @param projectId   the Project
     * @param versionName the Version name
     * @return A {@link Version} object.
     */
    Version getVersion(Long projectId, String versionName);

    /**
     * Gets a list of un-released versions for a particular project.
     *
     * @param projectId       The id of the project for which to return versions
     * @param includeArchived True if archived versions should be included
     * @return A Collection of {@link com.atlassian.jira.project.version.Version}s, never null
     * @since v3.10
     */
    Collection<Version> getVersionsUnreleased(Long projectId, boolean includeArchived);

    /**
     * Gets a list of released versions for a project. This list will include
     * archived versions if the 'includeArchived' flag is set to true.
     *
     * @param projectId       project id
     * @param includeArchived flag to indicate whether to include archived versions in the result.
     * @return A collection of {@link Version} objects
     */
    Collection<Version> getVersionsReleased(Long projectId, boolean includeArchived);

    /**
     * Gets a list of released versions for a project in reverse order.
     * This list will include archived versions if the 'includeArchived' flag
     * is set to true.
     *
     * @param projectId       project id
     * @param includeArchived flag to indicate whether to include archived versions in the result.
     * @return A collection of {@link Version} objects
     */
    Collection<Version> getVersionsReleasedDesc(Long projectId, boolean includeArchived);

    /**
     * Return all other versions in the project except this one
     *
     * @param version the Version
     * @return all other versions in the project except this one
     */
    Collection<Version> getOtherVersions(Version version);

    /**
     * Return all unarchived versions except this one
     *
     * @param version the Version
     * @return all unarchived versions except this one
     */
    Collection<Version> getOtherUnarchivedVersions(Version version);

    /**
     * Return all the issues in which the fix for version matches the specified version.
     *
     * @param version the fixed for version.
     * @return all the issues in which the fix for version matches the specified version.
     * @since v5.0
     */
    Collection<Issue> getIssuesWithFixVersion(Version version);

    /**
     * Return all the issues in which the affected version matches the specified version.
     *
     * @param version the affected version.
     * @return all the issues in which the affected version matches the specified version.
     * @since v5.0
     */
    Collection<Issue> getIssuesWithAffectsVersion(Version version);

    /**
     * Return all the issues in which the affects version matches the specified version.
     *
     * @param version the affects version.
     * @return all the issues in which the affects version matches the specified version.
     * @since v6.1
     */
    Collection<Long> getIssueIdsWithAffectsVersion(@Nonnull final Version version);

    /**
     * Return all the issues in which the fix version matches the specified version.
     *
     * @param version the fix version.
     * @return all the issues in which the fix version matches the specified version.
     * @since v6.1
     */
    Collection<Long> getIssueIdsWithFixVersion(@Nonnull final Version version);


    /**
     * Get all affected versions of the specified issue.
     *
     * @param issue the issue
     * @return all affected versions of the specified issue.
     */
    Collection<Version> getAffectedVersionsFor(Issue issue);

    List<ChangeItemBean> updateIssueAffectsVersions(Issue issue, Collection<Version> newValue);

    List<ChangeItemBean> updateIssueFixVersions(Issue issue, Collection<Version> newValue);

    /**
     * Get all fix for versions of the specified issue.
     *
     * @param issue the Issue
     * @return all fix for versions of the specified issue.
     */
    Collection<Version> getFixVersionsFor(Issue issue);

    /**
     * @return all versions in JIRA. Never null.
     */
    Collection<Version> getAllVersions();

    /**
     * Returns all versions that belong to the passed projects.
     *
     * @param projects        projects to search in
     * @param includeArchived whether or not to include archived versions
     * @return all versions that belong to passed projects. Never null.
     */
    Collection<Version> getAllVersionsForProjects(Collection<Project> projects, boolean includeArchived);

    /**
     * @param includeArchived whether or not to include archived versions
     * @return all released versions in JIRA. Never null.
     */
    Collection<Version> getAllVersionsReleased(boolean includeArchived);

    /**
     * @param includeArchived whether or not to include archived versions
     * @return all released versions in JIRA. Never null.
     */
    Collection<Version> getAllVersionsUnreleased(boolean includeArchived);

    /**
     * This method will update all issues that currently have {@code version} set as either affects or fix version to
     * the new {@code affectsSwapVersion} or {@code fixSwapVersion}
     * <p>
     * Both {@code affectsSwapVersion} or {@code fixSwapVersion} may be undefined in which case the {@code version} will
     * simply be removed from the issue.
     *
     * @param user               The user that will be used to update related issues
     * @param version            The version to remove from issues
     * @param affectsSwapVersion Affects version to replace version with. May be undefined to simply remove the
     *                           version.
     * @param fixSwapVersion     Fix version to replace version with. May be undefined to simply remove the version.
     * @see #merge
     * @see #deleteAndRemoveFromIssues
     */
    void swapVersionForRelatedIssues(ApplicationUser user, Version version, Option<Version> affectsSwapVersion, Option<Version> fixSwapVersion);

    /**
     * This method will perform a version merge. {@code versionToDelete} is a version that will be deleted completely.
     * All issues will have this version replaced with {@code versionToMergeTo}.
     *
     * @param user             The user that will be used to update related issues
     * @param versionToDelete  Version that will be deleted
     * @param versionToMergeTo All issues will have deleted version replaced with this one
     */
    void merge(ApplicationUser user, @Nonnull Version versionToDelete, @Nonnull Version versionToMergeTo);


    /**
     * This method deletes a version and additionally (unlinke {@link #deleteVersion}) removes it from all issues that reference it.
     *
     * @param user            User that will perform the update of issues.
     * @param versionToRemove Version to be deleted.
     */
    void deleteAndRemoveFromIssues(ApplicationUser user, @Nonnull Version versionToRemove);


    /**
     * This method gives information about usage of given version in custom fields.
     *
     * @param version The version that we want to get information about.
     * @return collection of version usage for each custom fields.
     * @since 7.0.10
     */
    @Nonnull
    Collection<CustomFieldWithVersionUsage> getCustomFieldsUsing(@Nonnull Version version);

    /**
     * This method gives total number of issues where this version is used in custom fields.
     *
     * @param version The version that we want to get information about.
     * @return collection of version usage for each custom fields.
     * @since 7.0.10
     */
    long getCustomFieldIssuesCount(@Nonnull Version version);

    /**
     * Remove specified version. According to information in {@link DeleteVersionWithCustomFieldParameters}
     * version will be completly removed from specific fields are replaced.
     *
     * @param applicationUser The user for this call
     * @param parameters      parameters build with builder obtained by
     *                        {@link com.atlassian.jira.bc.project.version.VersionService#createVersionDeletaAndReplaceParameters(Version)}
     * @see #merge(ApplicationUser, Version, Version) merge - if you want to replace all version references to given
     * version.
     * @since 7.0.10
     */
    void deleteVersionAndSwap(ApplicationUser applicationUser, DeleteVersionWithCustomFieldParameters parameters);
}

