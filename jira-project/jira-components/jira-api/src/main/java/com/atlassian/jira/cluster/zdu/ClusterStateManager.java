package com.atlassian.jira.cluster.zdu;

import com.atlassian.annotations.ExperimentalApi;

/**
 * Allows the retrieval of the current cluster upgrade state from the database.
 *
 * @since v7.3
 * @see UpgradeState
 */
@ExperimentalApi
public interface ClusterStateManager {
    UpgradeState getUpgradeState();
}
