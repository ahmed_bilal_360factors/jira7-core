package com.atlassian.jira.exception;

import com.atlassian.annotations.Internal;
import com.atlassian.annotations.PublicApi;

import javax.annotation.Nonnull;
import java.util.Collection;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Indicates that issue fields exceeded the jira character limit.
 *
 * @since: 6.4.1
 */
@PublicApi
public class IssueFieldsCharacterLimitExceededException extends AbstractCharacterLimitExceededException {
    private final Collection<String> invalidFieldIds;

    @Internal
    public IssueFieldsCharacterLimitExceededException(@Nonnull final Collection<String> invalidFieldIds, final long maxNumberOfCharacters) {
        super(maxNumberOfCharacters);
        this.invalidFieldIds = checkNotNull(invalidFieldIds);
    }

    /**
     * Returns fields that exceeds the limit.
     *
     * @return ids of fields exceeding the limit
     */
    @Nonnull
    public Collection<String> getInvalidFieldIds() {
        return invalidFieldIds;
    }

}
