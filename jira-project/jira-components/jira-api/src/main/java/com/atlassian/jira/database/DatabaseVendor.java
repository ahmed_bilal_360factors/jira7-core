package com.atlassian.jira.database;

/**
 * An enum representing the supported database vendors that JIRA can connect to.
 *
 * @since v7.0
 */
public enum DatabaseVendor {
    POSTGRES, ORACLE, SQL_SERVER, H2, MY_SQL
}