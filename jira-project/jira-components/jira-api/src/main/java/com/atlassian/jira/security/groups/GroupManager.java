package com.atlassian.jira.security.groups;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.exception.embedded.InvalidGroupException;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.Page;
import com.atlassian.jira.util.PageRequest;

import java.util.Collection;
import java.util.Set;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;

/**
 * This object can be used to manage groups in JIRA.
 *
 * @since v3.13
 */
public interface GroupManager {
    /**
     * Returns <code>true</code> if the given group name exists.
     *
     * @param groupName The group name.
     * @return <code>true</code> if the given group name exists.
     * @since v3.13
     */
    boolean groupExists(String groupName);

    /**
     * Returns <code>true</code> if the given group exists.
     *
     * @param group The group.
     * @return <code>true</code> if the given group exists.
     * @since v7.0
     */
    boolean groupExists(@Nonnull Group group);

    /**
     * Get all groups.
     *
     * @return Collection of all Groups.
     * @since v4.3
     * @deprecated Since v7.0. Only retrieve the users you really need. See {@link com.atlassian.jira.bc.group.search.GroupPickerSearchService}
     */
    @Deprecated
    Collection<Group> getAllGroups();

    /**
     * Create a group with the given name.
     *
     * @param groupName The group name.
     * @return the newly created Group.
     * @throws InvalidGroupException                                        if the group already exists in ANY associated directory or the group template does not have the required properties populated.
     * @throws com.atlassian.crowd.exception.OperationNotPermittedException if the directory has been configured to not allow the operation to be performed
     * @since v4.3
     */
    Group createGroup(String groupName) throws OperationNotPermittedException, InvalidGroupException;

    /**
     * Returns the Group for this groupName, else null if no such Group exists.
     *
     * @param groupName The group name.
     * @return The Group for this groupName, else null if no such Group exists.
     */
    Group getGroup(String groupName);

    /**
     * Returns the Group for this groupName, if no such Group exists then a proxy unknown immutable Group object is
     * returned.
     *
     * @param groupName The group name.
     * @return The Group for this groupName.
     */
    Group getGroupEvenWhenUnknown(String groupName);

    /**
     * Returns the Group for this groupName, else null if no such Group exists.
     * <p>
     * Legacy synonym for {@link #getGroup(String)}.
     *
     * @param groupName The group name.
     * @return The Group for this groupName, else null if no such Group exists.
     * @see #getGroup(String)
     * @deprecated use {@link #getGroup(String)} instead. Since 6.5
     */
    @Deprecated
    Group getGroupObject(String groupName);

    /**
     * Returns {@code true} if the user is a member of the group.
     * <p>
     * Note that if the username or groupname is null, then it will return false.
     * <p>
     * <strong>Performance note</strong>: If you have a full user object, then calling one of the other
     * {@code isUserInGroup()} methods directly is more efficient.
     *
     * @param username  user to inspect.
     * @param groupname group to inspect.
     * @return {@code true} if and only if the user is a direct or indirect (nested) member of the group.
     * @see #isUserInGroup(ApplicationUser, Group)
     * @see #isUserInGroup(ApplicationUser, String)
     * @deprecated Use one of the other {@code isUserInGroup} methods that takes a concrete user object instead.
     * Since v6.4.8.
     */
    boolean isUserInGroup(@Nullable final String username, @Nullable final String groupname);

    /**
     * Returns {@code true} if the user is a member of the group.
     * <p>
     * Note that if the User or Group object is null, then it will return false.
     * This was done to retain consistency with the old OSUser behaviour of User.inGroup() and Group.containsUser()
     *
     * @param user  user to inspect.
     * @param group group to inspect.
     * @return {@code true} if and only if the user is a direct or indirect (nested) member of the group.
     * @since v6.4.8
     */
    boolean isUserInGroup(@Nullable ApplicationUser user, @Nullable Group group);

    /**
     * Returns {@code true} if the user is a member of the named group.
     * <p>
     * If you already have the user object, then this method is faster than the alternative of passing in the username
     * because it saves on an unnecessary user lookup to find the correct User Directory.
     *
     * @param user      user to inspect.
     * @param groupName group to inspect.
     * @return {@code true} if and only if the user is a direct or indirect (nested) member of the group.
     * @since v6.4.8
     */
    boolean isUserInGroup(@Nullable ApplicationUser user, @Nullable String groupName);

    /**
     * Returns all the users in a group.
     *
     * @param groupName The group
     * @return all the users that belongs to the group.
     * @see {@link #getUsersInGroup(Group)}
     * @since v4.3
     */
    Collection<ApplicationUser> getUsersInGroup(String groupName);

    /**
     * Returns all the users in a group. Allows for additional filtering by the active flag of a user.
     *
     * @param groupName       The group
     * @param includeInactive if set to true inactive users will be returned as well
     * @return all the users that belongs to the group.
     * @since v7.0.1
     */
    Collection<ApplicationUser> getUsersInGroup(String groupName, final Boolean includeInactive);

    /**
     * Returns a page with users in a group. Users are sorted by name in ascending order.
     *
     * @param groupName       name of the group for which users are returned
     * @param includeInactive if set to true inactive users will be returned as well
     * @param pageRequest     parameters of the page to return
     * @return page with the users
     */
    Page<ApplicationUser> getUsersInGroup(final String groupName, final Boolean includeInactive, final PageRequest pageRequest);

    /**
     * Returns all the users in a group.
     * This will include nested group members.
     *
     * @param group The group
     * @return all the users that belongs to the group.
     * @throws NullPointerException if the group is null
     * @see {@link #getUsersInGroup(String)}
     * @since v4.3
     */
    Collection<ApplicationUser> getUsersInGroup(Group group);

    /**
     * Returns a count of all active users in a group.
     * This will include nested group members.
     *
     * @param group The group
     * @return a count of all the users that belongs to the group.
     * @see {@link #getUsersInGroupCount(String)}
     * @since 7.0
     */
    int getUsersInGroupCount(Group group);

    /**
     * Returns a count of all active users in a group.
     * This will include nested group members.
     *
     * @param groupName The group name
     * @return a count of all the users that belongs to the group.
     * @see {@link #getUsersInGroupCount(Group)}
     * @since 7.0
     */
    int getUsersInGroupCount(String groupName);

    /**
     * Returns a list of all active user names that are direct members in at least one of the supplied groups.
     *
     * @param groupNames A collection of group names to find direct members of
     * @param limit      A maximum number of names to return
     * @return a list of user names
     * @since 7.0
     */
    Collection<String> getNamesOfDirectMembersOfGroups(final Collection<String> groupNames, int limit);

    /**
     * Returns a collection of user names for the users that are direct members of every one of the supplied groups.
     *
     * @param userNames  A collection of user names
     * @param groupNames A collection of groups
     * @return a collection of user names
     * @since 7.0
     */
    Collection<String> filterUsersInAllGroupsDirect(Collection<String> userNames, Collection<String> groupNames);

    /**
     * Returns the names of all the users in a group.
     * This will include nested group members.
     *
     * @param group The group
     * @return all the users that belongs to the group.
     * @throws NullPointerException if the group is null
     * @see {@link #getUsersInGroup(Group)}
     * @since v5.0
     */
    Collection<String> getUserNamesInGroup(Group group);

    /**
     * Returns the names of all the users in a collection of groups.
     * This will include nested group members.
     *
     * @param groups The collection of groups
     * @return all the users that belongs to the group.
     * @throws NullPointerException if a group is null
     * @since v7.0
     */
    Collection<String> getUserNamesInGroups(Collection<Group> groups);

    /**
     * Returns the names of all the users in a group.
     * This will include nested group members.
     *
     * @param groupName The group
     * @return all the users that belongs to the group.
     * @throws NullPointerException if the group is null
     * @see {@link #getUsersInGroup(String)}
     * @since v5.0
     */
    Collection<String> getUserNamesInGroup(String groupName);

    /**
     * Returns all the users that are direct members of the group.
     * This will NOT include nested group members.
     *
     * @param group The group
     * @return all the users that belongs to the group.
     * @throws NullPointerException if the group is null
     * @see {@link #getUsersInGroup(String)}
     * @since v4.3
     */
    Collection<ApplicationUser> getDirectUsersInGroup(Group group);

    /**
     * Returns all the groups that the given user belongs to.
     *
     * @param userName The user
     * @return all the groups that the given user belongs to.
     * @see #getGroupNamesForUser(String)
     * @since v4.3
     */
    Collection<Group> getGroupsForUser(String userName);

    /**
     * Returns all the groups that the given user belongs to.
     *
     * @param user The user
     * @return all the groups that the given user belongs to.
     * @see #getGroupNamesForUser(String)
     * @since v4.3
     */
    Collection<Group> getGroupsForUser(@Nonnull ApplicationUser user);

    /**
     * Returns the names of all the groups that the given user belongs to.
     *
     * @param userName The user
     * @return all the groups that the given user belongs to.
     * @see #getGroupsForUser(String)
     * @see #getGroupNamesForUser(com.atlassian.jira.user.ApplicationUser)
     * @since v4.3
     */
    Collection<String> getGroupNamesForUser(String userName);

    /**
     * Returns the names of all the groups that the given user belongs to.
     *
     * @param user The user
     * @return all the groups that the given user belongs to.
     * @see #getGroupsForUser(String)
     * @since v6.2.5
     */
    Collection<String> getGroupNamesForUser(@Nonnull ApplicationUser user);

    /**
     * Adds a user as a member of a group.
     *
     * @param user  The user that will become a member of the group.
     * @param group The group that will gain a new member.
     * @throws UserNotFoundException          if the {@code user} could not be found
     * @throws GroupNotFoundException         if the {@code group} could not be found
     * @throws OperationNotPermittedException if the directory has been configured to not allow the operation to be performed
     * @throws OperationFailedException       If the underlying directory implementation failed to execute the operation.
     * @since v4.3
     */
    void addUserToGroup(ApplicationUser user, Group group) throws GroupNotFoundException, UserNotFoundException, OperationNotPermittedException, OperationFailedException;

    /**
     * Returns all the connect add-on users. This will return an empty {@link Set} in JIRA Server as connect plugins are only
     * supported in JIRA Cloud. This result does not filter out inactive users.
     *
     * @return all the connect add-on users.
     * @since v7.0.1
     * @deprecated since 7.2.0.
     */
    @Deprecated
    Set<ApplicationUser> getConnectUsers();
}
