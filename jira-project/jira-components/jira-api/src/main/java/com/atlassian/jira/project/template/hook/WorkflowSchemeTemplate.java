package com.atlassian.jira.project.template.hook;

import com.atlassian.annotations.PublicApi;

import java.util.List;
import java.util.Optional;

/**
 * A workflow scheme template as defined in the {@link ConfigTemplate}.
 *
 * @since 7.0
 */
@PublicApi
public interface WorkflowSchemeTemplate {
    /**
     * Returns the name of the workflow scheme.
     *
     * @return the workflow scheme name
     */
    String name();

    /**
     * Returns the description of the workflow scheme.
     *
     * @return the workflow scheme description
     */
    String description();

    /**
     * Returns the key of the default worfklow.
     *
     * @return the default workflow key
     */
    Optional<String> defaultWorkflow();

    /**
     * Returns the workflow templates of this workflow scheme.
     *
     * @return list of workflow templates.
     */
    List<WorkflowTemplate> workflowTemplates();

    /**
     * Indicates whether this workflow scheme template has a workflow which matches the specified workflow key.
     *
     * @param workflowKey unique id for the workflow
     * @return whether this workflow scheme has a workflow template with the specified key or not
     */
    boolean hasWorkflow(String workflowKey);
}
