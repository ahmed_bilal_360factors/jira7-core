package com.atlassian.jira.issue.fields.option;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.issue.fields.config.FieldConfig;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Collection;

/**
 * Manipulates the options associated with a field configuration.
 */
@ParametersAreNonnullByDefault
@PublicApi
public interface OptionSetManager {
    /**
     * Retrieves an option set that contains the options for the given field configuration.
     *
     * @param config the field configuration whose options are to be retrieved
     * @return an option set containing the options for that field configuration
     */
    @Nonnull
    OptionSet getOptionsForConfig(FieldConfig config);

    /**
     * Creates a new option set or replaces an existing one.
     * <p>
     * Well... maybe.  For historic reasons, a {@code null} value is tolerated for {@code optionIds} and is treated
     * the same as calling {@link #removeOptionSet(FieldConfig)}.  It is now annotated with {@code @Nonnull} to warn
     * callers that this should be avoided.  Please call {@link #removeOptionSet(FieldConfig)} if that's what you
     * really mean!
     * </p>
     * <p>
     * By the way, this method and {@link #updateOptionSet(FieldConfig, Collection)} do <strong>exactly</strong>
     * the same thing.  Don't ask me why we have both of them; I'm sure it seemed like a great idea at the time.
     * </p>
     *
     * @param config    the field configuration associated with the option set
     * @param optionIds the collection of options to include.  The options will be ordered by the iteration order
     *                  of the collection, so it should be a {@code List} or {@code SortedSet} if you want the
     *                  results to be predictable.
     * @return the set of options assigned to this field configuration
     */
    @Nonnull
    OptionSet createOptionSet(FieldConfig config, Collection<String> optionIds);

    /**
     * Creates a new option set or replaces an existing one.
     * <p>
     * Well... maybe.  For historic reasons, a {@code null} value is tolerated for {@code optionIds} and is treated
     * the same as calling {@link #removeOptionSet(FieldConfig)}.  It is now annotated with {@code @Nonnull} to warn
     * callers that this should be avoided.  Please call {@link #removeOptionSet(FieldConfig)} if that's what you
     * really mean!
     * </p>
     * <p>
     * By the way, this method and {@link #createOptionSet(FieldConfig, Collection)} do <strong>exactly</strong>
     * the same thing.  Don't ask me why we have both of them; I'm sure it seemed like a great idea at the time.
     * </p>
     *
     * @param config    the field configuration associated with the option set
     * @param optionIds the collection of options to include.  The options will be ordered by the iteration order
     *                  of the collection, so it should be a {@code List} or {@code SortedSet} if you want the
     *                  results to be predictable.
     * @return the set of options assigned to this field configuration
     */
    @Nonnull
    OptionSet updateOptionSet(FieldConfig config, Collection<String> optionIds);

    /**
     * Adds a new option to the option set. This will be placed in the last position of the set (highest sequence).
     *
     * @param config   the field configuration associated with the option set
     * @param optionId to add to the option set
     * @return the set of options assigned to this field configuration
     */
    @Nonnull
    OptionSet addOptionToOptionSet(FieldConfig config, String optionId);

    /**
     * Removes an option from the option set.
     *
     * @param config   the field configuration associated with the option set
     * @param optionId to remove from the option set
     * @return the set of options assigned to this field configuration
     */
    @Nonnull
    OptionSet removeOptionFromOptionSet(FieldConfig config, String optionId);

    /**
     * Removes all options from this field configuration.
     *
     * @param config the field configuration to destroy
     */
    void removeOptionSet(FieldConfig config);
}