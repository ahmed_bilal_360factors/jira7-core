package com.atlassian.jira.project.template;

import com.atlassian.annotations.PublicApi;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Optional;

/**
 * Manager to access project templates.
 *
 * @since 7.0
 */
@PublicApi
public interface ProjectTemplateManager {
    /**
     * Get the list of Project Template for a given user within a given context
     *
     * @return The list of available project templates
     */
    List<ProjectTemplate> getProjectTemplates();

    /**
     * Retrieves a project template by its key in the {@code atlassian-plugin.xml}.
     *
     * @param projectTemplateKey The key of the project template. <code>projectTemplateKey</code> can be null, or contain a null key.
     * @return The requested project template, or empty if a matching template can't be found.
     */
    Optional<ProjectTemplate> getProjectTemplate(@Nullable ProjectTemplateKey projectTemplateKey);

    /**
     * Gets a default template that can be used to create a project.
     *
     * @return The default template.
     */
    ProjectTemplate getDefaultTemplate();
}
