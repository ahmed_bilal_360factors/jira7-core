package com.atlassian.jira.imports.project.ao.handler;

import com.atlassian.annotations.ExperimentalSpi;
import com.atlassian.jira.imports.project.handler.PluggableImportHandler;

/**
 * Defines a handler class that will be able to perform some operation given an entity name and the entities
 * attributes.
 *
 * @since v7.0
 */
@ExperimentalSpi
public interface PluggableImportAoEntityHandler extends PluggableImportHandler, AoEntityHandler {
    Long WEIGHT_NONE = Long.MAX_VALUE;

    /**
     * Return the weight for this entity.  If this handler is does not handle the entity or care about it's ordering
     * return {@link #WEIGHT_NONE}.
     * <p>
     * This is ignored (not called) during the pre-import stage where the entities are supplied in the backup XML order.
     *
     * @return the weight by which to order this entity when importing.
     */
    Long getEntityWeight(String entityName);

}
