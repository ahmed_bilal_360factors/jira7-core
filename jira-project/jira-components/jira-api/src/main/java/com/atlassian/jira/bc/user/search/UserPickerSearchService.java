package com.atlassian.jira.bc.user.search;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.user.ApplicationUser;

import java.util.List;

/**
 * Service that retrieves a collection of {@link com.atlassian.jira.user.ApplicationUser} objects based on a partial query string
 *
 * @deprecated Use {@link UserSearchService} instead. Since v7.0.
 */
@Deprecated
@PublicApi
public interface UserPickerSearchService {
    /**
     * Get Users based on a query string.
     * <p>
     * Matches on start of username, full name and email as well as all the tokenised (on spaces, '.', '@' etc) words
     * Only returns active users.
     * <p>
     * Results are sorted according to the {@link com.atlassian.jira.issue.comparator.UserCachingComparator}.
     *
     * @param jiraServiceContext Jira Service Context
     * @param query              String to search for.
     * @return List of {@link ApplicationUser} objects that match criteria.
     * @see #findUsers(JiraServiceContext, String, UserSearchParams)
     */
    List<ApplicationUser> findUsers(JiraServiceContext jiraServiceContext, String query);

    /**
     * Returns a user by exact username
     *
     * @param jiraServiceContext Jira Service Context
     * @param query              String to search for.
     * @return The {@link ApplicationUser} object with supplied username.
     */
    ApplicationUser getUserByName(JiraServiceContext jiraServiceContext, String query);

    /**
     * Get Users based on a query string.
     * <p>
     * Matches on start of username, full name and email as well as all the tokenised (on spaces, '.', '@' etc) words
     * This will search even if the query passed is null or empty.
     * Only returns active users.
     * <p>
     * Results are sorted according to the {@link com.atlassian.jira.issue.comparator.UserCachingComparator}.
     *
     * @param jiraServiceContext Jira Service Context
     * @param query              String to search for.
     * @return List of {@link ApplicationUser} objects that match criteria.
     * @see #findUsers(JiraServiceContext, String, UserSearchParams)
     */
    List<ApplicationUser> findUsersAllowEmptyQuery(JiraServiceContext jiraServiceContext, String query);

    /**
     * Get Users based on a query string.
     * <p>
     * Matches on start of username, full name and email as well as all the tokenised (on spaces, '.', '@' etc) words
     * <p>
     * Results are sorted according to the {@link com.atlassian.jira.issue.comparator.UserCachingComparator}.
     * <p>
     * If the users in the database change during this call results might not be consistent if a
     * {@linkplain UserSearchParams#getPostProcessingFilter() post processing filter}
     * and a result limit is used.
     *
     * @param jiraServiceContext Jira Service Context
     * @param query              String to search for.
     * @param userSearchParams   Additional search parameters
     * @return List of {@link ApplicationUser} objects that match criteria.
     * @since 5.1.5
     */
    List<ApplicationUser> findUsers(JiraServiceContext jiraServiceContext, String query, UserSearchParams userSearchParams);

    /**
     * Get Users based on a query string.
     * <p>
     * Matches on start of username, full name and email as well as all the tokenised (on spaces, '.', '@' etc) words
     * <p>
     * Matches email only when userSearchParams.canMatchEmail() is true.
     * <p>
     * Results are sorted according to the userSearchParams.comparator.
     * If userSearchParams.comparator is null, no sorting will be performed.
     * <p>
     * If the users in the database change during this call results might not be consistent if a
     * {@linkplain UserSearchParams#getPostProcessingFilter() post processing filter}
     * and a result limit is used.
     *
     * @param query            the query to search username, display name and email address
     * @param userSearchParams the search criteria
     * @return the list of matched users
     * @since 6.2
     */
    List<ApplicationUser> findUsers(String query, UserSearchParams userSearchParams);

    /**
     * Get Users based on query strings.
     * <p>
     * Matches nameQuery on start of username, full name and email as well as all the tokenised (on spaces, '.', '@' etc) words
     * <p>
     * Matches emailQuery on start of email, as well as the tokenised words. Email matching is performed only when userSearchParams.canMatchEmail() is true.
     * <p>
     * Results are sorted according to the userSearchParams.comparator.
     * If userSearchParams.comparator is null, no sorting will be performed.
     * <p>
     * If the users in the database change during this call results might not be consistent if a
     * {@linkplain UserSearchParams#getPostProcessingFilter() post processing filter}
     * and a result limit is used.
     *
     * @param nameQuery        the query to search username and display name.
     * @param emailQuery       the query to search email address, subject to userSearchParams.canMatchEmail.
     * @param userSearchParams the search criteria
     * @return the list of matched users
     * @since 6.2
     */
    List<ApplicationUser> findUsers(String nameQuery, String emailQuery, UserSearchParams userSearchParams);

    /**
     * Determine whether a user matches the search criteria specified in the {@code userSearchParams} parameter.
     * <p>
     * allowEmptyQuery in {@code userSearchParams} is ignored.
     *
     * @param user             the user to be matched
     * @param userSearchParams the search criteria
     * @return true if the user matches the search criteria
     * @since v6.2
     */
    boolean userMatches(ApplicationUser user, UserSearchParams userSearchParams);

    /**
     * Returns true only if UserPicker Ajax search is enabled AND the user in the context has com.atlassian.jira.user.ApplicationUser Browse permission.
     *
     * @param jiraServiceContext Jira Service Context
     * @return True if enabled, otherwise false
     */
    boolean canPerformAjaxSearch(JiraServiceContext jiraServiceContext);

    /**
     * Determines whether the given user could perform AJAX search.
     *
     * @since v6.2
     */
    boolean canPerformAjaxSearch(ApplicationUser user);

    /**
     * Whether or not the UserPicker Ajax should search or show email addresses
     *
     * @param jiraServiceContext Jira Service Context
     * @return True if email addresses can be shown, otherwise false
     */
    boolean canShowEmailAddresses(JiraServiceContext jiraServiceContext);
}
