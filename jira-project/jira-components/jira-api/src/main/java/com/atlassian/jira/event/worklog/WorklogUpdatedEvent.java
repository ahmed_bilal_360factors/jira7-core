package com.atlassian.jira.event.worklog;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.issue.worklog.Worklog;

/**
 * Event indicating that worklog has been updated.
 *
 * @since v7.0
 */
@PublicApi
public class WorklogUpdatedEvent extends AbstractWorklogEvent implements WorklogEvent {
    public WorklogUpdatedEvent(final Worklog worklog) {
        super(worklog);
    }
}
