package com.atlassian.jira.database;

import com.atlassian.jira.exception.DataAccessException;

import java.sql.Connection;

/**
 * A handle to a Database Connection obtained from JIRA's connection pool.
 *
 * @see DatabaseAccessor
 * @since v7.0
 */
public interface DatabaseConnection {
    /**
     * Returns the JDBC connection wrapped by this object.
     *
     * @return the JDBC connection wrapped by this object.
     */
    Connection getJdbcConnection();

    /**
     * Convenience method to set autocommit mode on the database connection without the annoying SQLException.
     *
     * @param autoCommit <code>true</code> to enable auto-commit mode, <code>false</code> to disable it
     * @throws DataAccessException a RuntimeException wrapping any underlying SQLException.
     */
    void setAutoCommit(boolean autoCommit);

    /**
     * Commits all changes made since the previous commit or rollback.
     * This method should be used only when auto-commit mode has been disabled.
     *
     * @throws DataAccessException a RuntimeException wrapping any underlying SQLException.
     * @see #setAutoCommit
     */
    void commit();

    /**
     * Undoes all changes made in the current transaction.
     * This method should be used only when auto-commit mode has been disabled.
     * You normally don't need to call this explicitly - instead throwing a RuntimeException will indicate to
     * DatabaseAccessor that you want to rollback.
     *
     * @throws DataAccessException a RuntimeException wrapping any underlying SQLException.
     * @see #setAutoCommit
     */
    void rollback();
}
