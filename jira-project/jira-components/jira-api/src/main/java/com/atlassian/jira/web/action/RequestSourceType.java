package com.atlassian.jira.web.action;

/**
 * Enum holding the types of which an action can be invoked from.
 * Possible logical values are: page (to be used when no other type is better applicable) or dialog.
 *
 * @since v7.2
 */
public enum RequestSourceType {
    PAGE("page"),
    DIALOG("dialog");

    private final String type;

    RequestSourceType(final String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
