package com.atlassian.jira.issue.worklog;

public class WorklogKeys {
    // Length to pad the work ratios - required for lucene range query
    public static final int PADLENGTH = 5;
}
