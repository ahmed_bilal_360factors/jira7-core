package com.atlassian.jira.notification;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.notification.type.NotificationType;

/**
 * Notification to the configured role (current assignee, reporter, etc..)
 *
 * @since 7.0
 */
@PublicApi
public class RoleNotification extends AbstractNotification {
    public RoleNotification(final Long id, final NotificationType notificationType) {
        super(id, notificationType, null);
    }

    @Override
    public <X> X accept(final NotificationVisitor<X> notificationVisitor) {
        return notificationVisitor.visit(this);
    }
}
