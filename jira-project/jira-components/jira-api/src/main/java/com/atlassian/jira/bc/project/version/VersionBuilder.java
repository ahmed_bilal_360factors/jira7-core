package com.atlassian.jira.bc.project.version;

import com.atlassian.jira.project.version.Version;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Date;

/**
 * Build immutable {@link com.atlassian.jira.project.version.Version} objects.
 *
 * @since v7.0
 */
@ParametersAreNonnullByDefault
public interface VersionBuilder {
    @Nonnull
    VersionBuilder projectId(Long projectId);

    @Nonnull
    VersionBuilder name(String name);

    @Nonnull
    VersionBuilder description(@Nullable String description);

    @Nonnull
    VersionBuilder startDate(@Nullable Date startDate);

    @Nonnull
    VersionBuilder releaseDate(@Nullable Date releaseDate);

    @Nonnull
    VersionBuilder scheduleAfterVersion(@Nonnull Long scheduleAfterVersion);

    @Nonnull
    VersionBuilder released(boolean released);

    @Nonnull
    VersionBuilder archived(boolean archived);

    @Nonnull
    VersionBuilder sequence(@Nonnull Long sequence);

    @Nonnull
    Version build();
}
