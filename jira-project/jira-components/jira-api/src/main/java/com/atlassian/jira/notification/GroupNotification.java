package com.atlassian.jira.notification;

import com.atlassian.annotations.PublicApi;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.notification.type.NotificationType;

/**
 * Notification to the configured group.
 *
 * @since 7.0
 */
@PublicApi
public final class GroupNotification extends AbstractNotification {
    private final Group group;

    public GroupNotification(final Long id, final NotificationType notificationType, final Group group, final String parameter) {
        super(id, notificationType, parameter);
        this.group = group;
    }

    public Group getGroup() {
        return group;
    }

    @Override
    public <X> X accept(final NotificationVisitor<X> notificationVisitor) {
        return notificationVisitor.visit(this);
    }
}
