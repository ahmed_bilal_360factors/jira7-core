package com.atlassian.jira.project.template.hook;

import com.atlassian.annotations.PublicApi;

import java.net.URL;

/**
 * A workflow template used for Project Template configuration.
 *
 * @since 7.0
 */
@PublicApi
public interface WorkflowTemplate {
    /**
     * Returns the template key of the workflow.
     *
     * @return the key of the workflow
     */
    String key();

    /**
     * Returns the name of the workflow.
     *
     * @return the name
     */
    String name();

    /**
     * Returns the path of the workflow bundle.
     *
     * @return path of the workflow bundle
     */
    String bundlePath();

    /**
     * Returns the path of the workflow bundle as a URL.
     *
     * @return url of the workflow bundle
     */
    URL bundleUrl();
}
