package com.atlassian.jira.notification;


import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.bc.ServiceResultImpl;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.google.common.base.Supplier;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * This is a simple service that allows sending issue notification to a give set of recipients that can be defined
 * arbitrarly.
 *
 * @since 5.2
 */
@PublicApi
public interface AdhocNotificationService {
    NotificationBuilder makeBuilder();

    ValidateNotificationResult validateNotification(NotificationBuilder notification, ApplicationUser from, Issue issue);

    ValidateNotificationResult validateNotification(NotificationBuilder notification, ApplicationUser from, Issue issue, ValiationOption option);

    void sendNotification(ValidateNotificationResult result);

    static enum ValiationOption {
        FAIL_ON_NO_RECIPIENTS,
        CONTINUE_ON_NO_RECIPIENTS
    }

    static final class ValidateNotificationResult extends ServiceResultImpl {
        protected final NotificationBuilder notification;
        protected final ApplicationUser from;
        protected final Issue issue;
        protected final Supplier<Iterable<NotificationRecipient>> recipients;

        protected ValidateNotificationResult(ErrorCollection errorCollection, NotificationBuilder notification, Supplier<Iterable<NotificationRecipient>> recipients, ApplicationUser from, Issue issue) {
            super(errorCollection);
            this.notification = notNull(notification);
            this.from = notNull(from);
            this.issue = notNull(issue);
            this.recipients = notNull(recipients);
        }
    }
}
