package com.atlassian.jira.project;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.project.template.ProjectTemplateKey;

import java.util.Objects;
import java.util.Optional;

/**
 * Holds the information about a recently created project. This will be passed to {@link
 * com.atlassian.jira.project.ProjectCreateHandler}s that are registered to receive notifications about created
 * projects.
 *
 * @since 7.0
 */
@PublicApi
public class ProjectCreatedData {
    public enum Type {
        /**
         * Used when this is a simple create operation.
         */
        CREATE,
        /**
         * Used when the new project was created with shared schemes of an existing project.
         */
        CREATE_WITH_SHARED_CONFIG
    }

    private final Project project;
    private final ProjectTemplateKey projectTemplateKey;
    private final Long existingProjectId;
    private final Type type;

    private ProjectCreatedData(final Type type, final Project project, final ProjectTemplateKey projectTemplateKey, final Long existingProjectId) {
        this.type = type;
        this.project = project;
        this.projectTemplateKey = projectTemplateKey;
        this.existingProjectId = existingProjectId;
    }

    /**
     * Returns {@link Type#CREATE} if this is a normal project create call.  If this project was created based on shared
     * config with an existing project it will return {@link Type#CREATE_WITH_SHARED_CONFIG}
     *
     * @return the create operation type
     */
    public Type getType() {
        return type;
    }

    /**
     * Returns the created project.
     *
     * @return The created project.
     */
    public Project getProject() {
        return project;
    }

    /**
     * Returns the key of the project template used to create the project.
     *
     * @return The key of the project template used to create the project.
     */
    public Optional<ProjectTemplateKey> getProjectTemplateKey() {
        return Optional.ofNullable(projectTemplateKey);
    }

    /**
     * Returns the id of the the existing project that was used as a template for the new project
     *
     * @return The id of the the existing project that was used as a template for the new project
     */
    public Optional<Long> getExistingProjectId() {
        return Optional.ofNullable(existingProjectId);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProjectCreatedData that = (ProjectCreatedData) o;
        return Objects.equals(project, that.project) &&
                Objects.equals(projectTemplateKey, that.projectTemplateKey) &&
                Objects.equals(existingProjectId, that.existingProjectId) &&
                Objects.equals(type, that.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(project, projectTemplateKey, existingProjectId, type);
    }

    /**
     * Builder for {@link com.atlassian.jira.project.ProjectCreatedData}
     */
    public static class Builder {
        private Project project;
        private ProjectTemplateKey projectTemplateKey;
        private Long existingProjectId;

        public Builder withProject(Project project) {
            this.project = project;
            return this;
        }

        public Builder withProjectTemplateKey(ProjectTemplateKey projectTemplateKey) {
            this.projectTemplateKey = projectTemplateKey;
            return this;
        }

        public Builder withExistingProjectId(Long existingProjectId) {
            this.existingProjectId = existingProjectId;
            return this;
        }

        public ProjectCreatedData build() {
            Type type = existingProjectId == null ? Type.CREATE : Type.CREATE_WITH_SHARED_CONFIG;
            return new ProjectCreatedData(type, project, projectTemplateKey, existingProjectId);
        }
    }
}
