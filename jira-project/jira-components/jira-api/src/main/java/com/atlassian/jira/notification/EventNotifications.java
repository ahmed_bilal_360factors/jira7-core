package com.atlassian.jira.notification;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.event.type.EventType;

/**
 * Holds information about {@link com.atlassian.jira.notification.Notification}s which are configured for a given event.
 *
 * @since 7.0
 */
@PublicApi
public final class EventNotifications {
    private final EventType eventType;
    private final Iterable<Notification> notifications;

    public EventNotifications(final EventType eventType, final Iterable<Notification> notifications) {
        this.eventType = eventType;
        this.notifications = notifications;
    }

    public EventType getEventType() {
        return eventType;
    }

    public Iterable<Notification> getNotifications() {
        return notifications;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final EventNotifications that = (EventNotifications) o;

        if (eventType != null ? !eventType.equals(that.eventType) : that.eventType != null) {
            return false;
        }
        if (notifications != null ? !notifications.equals(that.notifications) : that.notifications != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = eventType != null ? eventType.hashCode() : 0;
        result = 31 * result + (notifications != null ? notifications.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "EventNotifications{" +
                "eventType=" + eventType +
                ", notifications=" + notifications +
                '}';
    }
}
