package com.atlassian.jira.issue.fields.config;

import com.atlassian.annotations.Internal;
import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.context.IssueContext;
import com.atlassian.jira.issue.context.JiraContextNode;
import com.atlassian.jira.issue.context.persistence.FieldConfigContextPersister;
import com.atlassian.jira.issue.fields.ConfigurableField;
import com.atlassian.jira.issue.fields.config.persistence.FieldConfigPersister;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.util.MapUtils;
import com.atlassian.jira.util.collect.CollectionUtil;
import com.atlassian.jira.util.collect.MultiMaps;
import com.atlassian.util.concurrent.LazyReference;
import com.atlassian.util.concurrent.ResettableLazyReference;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multimap;
import org.apache.commons.collections.MultiMap;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

// Should not be in the API - its leaked through FieldConfigScheme.Builder
@Internal
public class FieldConfigSchemeImpl implements FieldConfigScheme {
    private static final Logger log = LoggerFactory.getLogger(FieldConfigSchemeImpl.class);

    private final Long id;
    private final String name;
    private final String description;
    private final String fieldId;
    private final Map<String, FieldConfig> configs;
    private final FieldConfigContextPersister configContextPersister;

    private final ResettableLazyReference<List<JiraContextNode>> applicableContexts = new ResettableLazyReference<List<JiraContextNode>>() {
        @Override
        protected List<JiraContextNode> create() {
            if (configContextPersister == null) {
                // this should only happen when a new Issue Type Scheme is created through the UI and hasn't hit the db yet - therefore no context anyway
                return Collections.emptyList();
            }
            final List<JiraContextNode> applicableContexts = new ArrayList<JiraContextNode>(
                    configContextPersister.getAllContextsForConfigScheme(FieldConfigSchemeImpl.this));
            Collections.sort(applicableContexts);
            return Collections.unmodifiableList(applicableContexts);
        }
    };

    private final LazyReference<MultiMap> configsByConfig = new LazyReference<MultiMap>() {
        @Override
        protected MultiMap create() {
            return MapUtils.invertMap(getConfigs());
        }
    };

    public FieldConfigSchemeImpl(final Long id, final String fieldId, final String name, final String description, final Map<String, FieldConfig> configs, final FieldConfigContextPersister configContextPersister) {
        this.id = id;
        this.fieldId = fieldId;
        this.name = StringUtils.abbreviate(name, FieldConfigPersister.ENTITY_LONG_TEXT_LENGTH);
        this.description = description;
        this.configs = (configs != null) ? CollectionUtil.copyAsImmutableMap(configs) : Collections.<String, FieldConfig>emptyMap();
        this.configContextPersister = configContextPersister;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Map<String, FieldConfig> getConfigs() {
        return configs;
    }

    public List<JiraContextNode> getContexts() {
        return applicableContexts.get();
    }

    public Long getId() {
        return id;
    }

    String getFieldId() {
        return fieldId;
    }

    public ConfigurableField<?> getField() {
        return ComponentAccessor.getFieldManager().getConfigurableField(fieldId);
    }

    public boolean isInContext(final IssueContext issueContext) {
        final List<JiraContextNode> contexts = getContexts();
        if (contexts != null) {
            for (final JiraContextNode contextNode : contexts) {
                if (contextNode.isInContext(issueContext)) {
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public List<Project> getAssociatedProjectObjects() {
        if (isEnabled()) {
            final List<Project> list = new LinkedList<Project>();

            for (final JiraContextNode contextNode : getContexts()) {
                final Project projectObject = contextNode.getProjectObject();
                if (projectObject != null) {
                    list.add(projectObject);
                }
            }
            return list;
        } else {
            return Collections.emptyList();
        }
    }

    @Override
    @Nonnull
    public List<Long> getAssociatedProjectIds() {
        if (isEnabled()) {
            final List<Long> list = new LinkedList<Long>();

            for (final JiraContextNode contextNode : getContexts()) {
                final Long projectId = contextNode.getProjectId();
                // Not sure that skipping nulls is the right thing to do, but I am copying the logic from existing methods.
                if (projectId != null) {
                    list.add(projectId);
                }
            }
            return list;
        } else {
            return Collections.emptyList();
        }
    }

    @Override
    public Collection<IssueType> getAssociatedIssueTypes() {
        if (configs == null || configs.isEmpty() || !isEnabled()) {
            return Collections.emptyList();
        }

        final ConstantsManager constantsManager = ComponentAccessor.getConstantsManager();
        return configs.keySet().stream()
                .map(constantsManager::getIssueType)
                .collect(CollectorsUtil.toNewArrayListWithSizeOf(configs));
    }

    @Override
    @Nonnull
    public Collection<String> getAssociatedIssueTypeIds() {
        return (configs != null && isEnabled()) ? configs.keySet() : Collections.emptyList();
    }

    public boolean isGlobal() {
        return isAllProjects() && isAllIssueTypes();
    }

    public boolean isAllProjects() {
        for (final JiraContextNode contextNode : getContexts()) {
            if (contextNode.getProjectObject() == null) {
                return true;
            }
        }
        return false;
    }

    public boolean isAllIssueTypes() {
        final Collection<IssueType> issueTypes = getAssociatedIssueTypes();
        return (issueTypes != null) && issueTypes.contains(null);
    }

    public boolean isEnabled() {
        return !getContexts().isEmpty();
    }

    public boolean isBasicMode() {
        final MultiMap configsByConfig = getConfigsByConfig();
        return (configsByConfig == null) || (configsByConfig.size() <= 1);
    }

    public MultiMap getConfigsByConfig() {
        return configsByConfig.get();
    }

    @Nullable
    public FieldConfig getOneAndOnlyConfig() {
        final MultiMap configsByConfig = getConfigsByConfig();
        if (configsByConfig != null) {
            if (configsByConfig.size() == 1) {
                return (FieldConfig) configsByConfig.keySet().iterator().next();
            }
            log.warn("There is not exactly one config for this scheme ({}). Configs are {}.", id, configsByConfig);
        }
        return null;
    }

    FieldConfigContextPersister getFieldConfigContextPersister() {
        return configContextPersister;
    }

    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof FieldConfigScheme && equals((FieldConfigScheme) o));
    }

    private boolean equals(@Nonnull final FieldConfigScheme other) {
        return Objects.equals(id, other.getId()) &&
                Objects.equals(name, other.getName()) &&
                Objects.equals(description, other.getDescription());
    }

    public int compareTo(final Object obj) {
        final FieldConfigScheme o = (FieldConfigScheme) obj;
        return new CompareToBuilder()
                .append(id, o.getId())
                .append(name, o.getName())
                .append(description, o.getDescription())
                .toComparison();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description);
    }
}
