package com.atlassian.jira.issue.fields.rest.json.beans;


import com.atlassian.jira.issue.security.IssueSecurityLevel;
import com.atlassian.jira.util.JiraUrlCodec;
import com.google.common.collect.Lists;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Collection;

/**
 * @since v5.0
 */
public class SecurityLevelJsonBean {
    @JsonProperty
    private String self;

    @JsonProperty
    private String id;

    @JsonProperty
    private String description;

    @JsonProperty
    private String name;

    public SecurityLevelJsonBean(final String self, final String id, final String description, final String name) {
        this.self = self;
        this.id = id;
        this.description = description;
        this.name = name;
    }

    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static Collection<SecurityLevelJsonBean> shortBeans(final Collection<IssueSecurityLevel> securitylevels, final JiraBaseUrls urls) {
        Collection<SecurityLevelJsonBean> result = Lists.newArrayListWithCapacity(securitylevels.size());
        for (IssueSecurityLevel from : securitylevels) {
            result.add(shortBean(from, urls));
        }

        return result;
    }

    /**
     * @return null if the input is null
     */
    public static SecurityLevelJsonBean shortBean(final IssueSecurityLevel securityLevel, final JiraBaseUrls urls) {
        if (securityLevel == null) {
            return null;
        }

        return new SecurityLevelJsonBean(
                getSelf(urls.restApi2BaseUrl(), JiraUrlCodec.encode(securityLevel.getId().toString())),
                securityLevel.getId().toString(),
                securityLevel.getDescription(),
                securityLevel.getName()
        );
    }

    public static String getSelf(String baseURL, String encodedSecurityLevelId) {
        return baseURL + "securitylevel/" + encodedSecurityLevelId;
    }
}
