package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;

import java.util.Map;

/**
 * An extension of {@link com.atlassian.plugin.web.Condition} which helps to decide whether a web section or
 * web item should be displayed.
 * DO NOT USE THIS CONDITION ON A WEB RESOURCE - Because we lose performance gains if a web resources can't be
 * cached as part of a batch. Basically it just slows things down a lot for web resource loading.
 *
 * @since 7.1
 */
@ExperimentalApi
public class FeatureEnabledCondition implements Condition {
    private final FeatureManager featureManager;

    private static final String PARAMETER_NAME = "feature-key";

    private String featureKey = null;

    public FeatureEnabledCondition(final FeatureManager featureManager) {
        this.featureManager = featureManager;
    }

    public void init(final Map<String, String> params) throws PluginParseException {
        if (params != null) {
            featureKey = params.get(PARAMETER_NAME);
        }

        if (featureKey == null) {
            throw new PluginParseException("Missing parameter for feature check condition: " + PARAMETER_NAME + ". Please add it to the condition like <param name=\"" + PARAMETER_NAME + "\">sd.my.feature.flag</param>");
        }
    }

    public boolean shouldDisplay(final Map<String, Object> params) {
        return featureManager.isEnabled(featureKey);
    }
}
