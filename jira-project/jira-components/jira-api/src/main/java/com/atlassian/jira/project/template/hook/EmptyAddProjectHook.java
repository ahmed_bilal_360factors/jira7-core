package com.atlassian.jira.project.template.hook;

import com.atlassian.annotations.PublicApi;

/**
 * Default implementation for an {@link AddProjectHook} that does nothing.
 *
 * @since 7.0
 */
@PublicApi
public class EmptyAddProjectHook implements AddProjectHook {
    /**
     * Returns a default ConfigureResponse.
     *
     * @param validateData a ValidateData
     * @return a default ConfigureResponse
     */
    @Override
    public ValidateResponse validate(ValidateData validateData) {
        return ValidateResponse.create();
    }

    /**
     * Returns a default ConfigureResponse.
     *
     * @param configureData a ConfigureData
     * @return a default ConfigureResponse
     */
    @Override
    public ConfigureResponse configure(ConfigureData configureData) {
        return ConfigureResponse.create();
    }
}
