package com.atlassian.jira.util;

import com.atlassian.annotations.PublicApi;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;

/**
 * Represents an "order by" request. The request consists of a field to sort by
 * and an ordering direction (ascending or descending).
 * <p>
 * Note that this has nothing to do with JQL. This class is used in some service methods which provide
 * ordering of their results.
 * </p>
 *
 * @param <T> type of the field that we can order by ({@code String} or {@code Enum} make most sense here).
 * @since 6.4.7
 */
@PublicApi
public final class OrderByRequest<T> {
    public enum Order {
        ASC, DESC
    }

    private final T field;
    private final Order order;

    public OrderByRequest(T field, Order order) {
        this.field = Preconditions.checkNotNull(field);
        this.order = Preconditions.checkNotNull(order);
    }

    public T getField() {
        return field;
    }

    public Order getOrder() {
        return order;
    }

    public static <T extends Enum<T>> OrderByRequest<T> asc(T field) {
        return new OrderByRequest<T>(field, Order.ASC);
    }

    public static <T extends Enum<T>> OrderByRequest<T> desc(T field) {
        return new OrderByRequest<T>(field, Order.DESC);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OrderByRequest that = (OrderByRequest) o;

        return Objects.equal(this.field, that.field) &&
                Objects.equal(this.order, that.order);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(field, order);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("field", field)
                .add("type", order)
                .toString();
    }
}
