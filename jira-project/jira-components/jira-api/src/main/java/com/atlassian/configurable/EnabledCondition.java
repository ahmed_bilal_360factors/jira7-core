package com.atlassian.configurable;

import com.atlassian.annotations.PublicSpi;

/**
 * Determines whether a property should be displayed and configured. Can be used as a condition for a {@link ObjectConfigurationProperty}.
 *
 * @since v6.3
 */
@PublicSpi
public interface EnabledCondition {
    /**
     * A condition that means <strong>always enabled</strong>.
     */
    EnabledCondition TRUE = () -> true;

    /**
     * Whether or not to display and use a property.
     *
     * @return true only if the property is enabled in the current context.
     */
    boolean isEnabled();

}
