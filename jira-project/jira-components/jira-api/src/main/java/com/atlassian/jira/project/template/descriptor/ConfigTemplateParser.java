package com.atlassian.jira.project.template.descriptor;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.project.template.hook.ConfigTemplate;
import com.atlassian.plugin.Plugin;

/**
 * Parses the given configuration into an instance of {@link ConfigTemplate}.
 *
 * @since 7.0
 */
@PublicApi
public interface ConfigTemplateParser {
    /**
     * Parses the given configuration into an instance of {@link ConfigTemplate}.
     *
     * @param configFile configuration file
     * @param plugin     plugin performing the loading
     * @return parsed configuration instance
     */
    ConfigTemplate parse(String configFile, Plugin plugin);
}
