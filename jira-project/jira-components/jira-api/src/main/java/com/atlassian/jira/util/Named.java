package com.atlassian.jira.util;

import java.util.Comparator;

/**
 * @since v5.2
 */
public interface Named {
    String getName();

    Comparator<Named> NAME_COMPARATOR = Comparator.comparing(Named::getName);
}
