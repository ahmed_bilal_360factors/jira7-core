package com.atlassian.jira.license;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.permission.GlobalPermissionKey;

/**
 * Provides various counts of users in JIRA. Implementations are expected to cache the results of methods to ensure that
 * count look-ups are fast.
 *
 * @since 6.5
 */
@PublicApi
public interface LicenseCountService {
    /**
     * Gets the number of active users who currently count towards the license and should be charged for. This
     * method should be used when determining user counts for billing purposes, such as purchase tier recommendations
     * for plugins, and by plugins who wish to enforce tier-based licenses.
     * <p>
     * Implementations of this method should take performance into consideration, and ensure that the value is cached.
     * Use {@link #flush()} to clear the cache.
     * </p>
     *
     * @return the number of users in JIRA which currently count against at least one license
     * @see GlobalPermissionKey
     */
    int totalBillableUsers();

    /**
     * Clear the billable users cache. Note, despite the general name this clears only the billable users cache. Usually
     * it is not necessary to call this method as implementors are expected to clear cached state in response to
     * appropriate events.
     *
     * @deprecated use {@link LicenseCountService#flushBillableUsersCache()}. Since v7.0. Eventually we want to remove
     * this method in favour of the one with a more descriptive name.
     */
    @Deprecated
    void flush();

    /**
     * Clear the cache for billable users. Usually it is not necessary to call this method as implementors are expected
     * to clear cached state in response to appropriate events.
     */
    void flushBillableUsersCache();
}
