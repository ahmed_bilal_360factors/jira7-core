package com.atlassian.jira.application;

import com.atlassian.annotations.Internal;
import com.atlassian.application.api.ApplicationKey;
import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Set;

import static com.atlassian.application.api.ApplicationAccess.AccessError;

/**
 * A utility service to determine if a user has access to a specific application (as identified by its
 * {@link com.atlassian.application.api.ApplicationKey}).
 *
 * @see {@link com.atlassian.jira.application.ApplicationRoleAdminService}
 * @see {@link com.atlassian.jira.license.JiraLicenseManager}
 * @since 7.0
 */
public interface ApplicationAuthorizationService {
    /**
     * Determines whether the given user is allowed to access the passed application. This simply checks if there are
     * any access errors caused by the user accessing the application.
     * That is, it calls {@code return getAccessError(user, key).isEmpty()}.
     * <p>
     * You can get more fine grained control using
     * {@link #getAccessErrors(com.atlassian.jira.user.ApplicationUser, com.atlassian.application.api.ApplicationKey)}
     *
     * @param user the user to check for access rights - if this is null, this method returns false
     * @param key  the key that uniquely identifies the application.
     * @return true if the user has been assigned to the specified application and the application is backed by a
     * license.
     */
    boolean canUseApplication(@Nullable ApplicationUser user, @Nonnull ApplicationKey key);

    /**
     * Gets the errors that would occur if the passed user accessed the passed application. This includes checking
     * errors from both the license and user access. Note, this is not a specific licensing check, as long as there is a
     * license that can grant access present then there will be no access errors. That is, the access may not come from
     * the license of the specific application you are checking.
     *
     * @param user the user to check for access rights - if null this will give a {@code AccessError.NO_ACCESS}.
     * @param key  the key that uniquely identifies the application.
     * @return the set of {@link com.atlassian.application.api.ApplicationAccess.AccessError}. An empty set is returned
     * if no errors would occur.
     */
    Set<AccessError> getAccessErrors(@Nullable ApplicationUser user, @Nonnull ApplicationKey key);

    /**
     * Determines whether it is possible to access the passed application. Importantly, it does not indicate if a
     * particular user would be able to access the application. Use
     * {@link #canUseApplication(com.atlassian.jira.user.ApplicationUser, com.atlassian.application.api.ApplicationKey)}
     * to work out access permission for a particular user.
     * <p>
     * Note, this simply calls {@code getLicensingAccessErrors(key).isEmpty()}.
     * <p>
     * You can get more fine grained control using
     * {@link #getLicensingAccessErrors(com.atlassian.application.api.ApplicationKey)}
     *
     * @param key the key that uniquely identifies the application.
     * @return {@code true} if the application has a license that is not exceeded, expired, or with a version mismatch.
     * A return of {@code false} indicates that users will not be able to access the application. A return of
     * {@code true} indicates it might be possible for a user to access the application.
     */
    boolean hasNoLicensingAccessErrors(@Nonnull ApplicationKey key);

    /**
     * Get any errors that would occur when accessing the passed application. Note, this only checks access errors
     * resulting from licensing, not from particular users accessing the application. User access errors are given by
     * {@link #getAccessErrors(com.atlassian.jira.user.ApplicationUser, com.atlassian.application.api.ApplicationKey)}.
     * Furthermore, this is not a specific licensing check, as long as there is a license that can grant access present
     * then there will be no access errors. That is, the access may not come from the license of the specific
     * application you are checking.
     *
     * @param key the key that uniquely identifies the application.
     * @return the set of {@link com.atlassian.application.api.ApplicationAccess.AccessError}. An empty set is returned
     * if no errors would occur.
     */
    Set<AccessError> getLicensingAccessErrors(@Nonnull ApplicationKey key);

    /**
     * Determines whether an application is installed (running in this JIRA instance) and has a backing
     * (potentially exceeded) license.
     * <p>
     * Note: This method still returns {@code true} if the application's license has expired or has been exceeded.
     * </p>
     *
     * @param key the key that uniquely identifies the application.
     * @return {@code true} if the application is installed (running in this JIRA instance) and has a backing
     * (potentially exceeded) license.
     * @see #canUseApplication
     */
    boolean isApplicationInstalledAndLicensed(@Nonnull ApplicationKey key);

    /**
     * Returns {@code true} if any of the licenses in this JIRA instance have exceeded their respective license user
     * limit.
     * <p>
     *     Note: does NOT check that license is within expiry limits; see {@link
     * com.atlassian.jira.license.LicenseDetails#isExpired()}.
     * </p>
     *
     * @return {@code true} if any of the licenses in this JIRA instance have exceeded their respective license user
     * limit.
     */
    boolean isAnyRoleLimitExceeded();

    /**
     * Determines whether an application's license user limit have been exceeded.
     * <p>
     * Note: Does <strong>NOT</strong> check that license is within expiry limits;
     * see {@link com.atlassian.jira.license.LicenseDetails#isExpired()}.
     * </p>
     * <p>
     * Returns {@code false} when the application is not installed.
     * </p>
     *
     * @param key the key that uniquely identifies the application.
     * @return {@code true} if the application's license user limit have been exceeded or {@code false} otherwise.
     */
    boolean isExceeded(@Nonnull ApplicationKey key);

    /**
     * Retrieve the number of active users for an application as identified by the
     * {@link com.atlassian.application.api.ApplicationKey}.
     * <p>
     * It will uniquely count all users who are found in the groups associated with the application.
     * </p>
     *
     * @param key the key that uniquely identifies the application.
     * @return the number of active users for the passed {@code ApplicationRole} when the application
     * does not exist (0) would be returned.
     */
    int getUserCount(@Nonnull ApplicationKey key);

    /**
     * Determines whether {@link ApplicationRole}s are enabled.
     *
     * @return {@code true} when {@code ApplicationRole} are enabled, {@code false} otherwise.
     * @deprecated since 7.0.1 as this always returns true in JIRA 7
     */
    @Internal
    @Deprecated
    boolean rolesEnabled();
}
