package com.atlassian.jira.tenancy;

import com.atlassian.annotations.Internal;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * A marker interface for indicating whether or not a component data management (particularly any caches that it
 * uses) has been evaluated for its safe use in a multi-tenanted environment.
 * <p>
 * See {@link TenantAware} for acceptable values and their meanings.
 * </p>
 * @see TenantAware
 * @since v7.1.0
 * @deprecated Use {@link com.atlassian.annotations.tenancy.TenantAware} instead. Since v7.2.0.
 */
@Internal
@Deprecated
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.TYPE})
public @interface TenantInfo {
    TenantAware value();
    String comment() default "";
}