package com.atlassian.jira.issue.fields;

/**
 * Defines fields that are dependent on another field
 */
public interface DependentField<V> extends OrderableField<V> {
    Field getParentField();
}
