package com.atlassian.jira.plugin.projectpanel;

import com.atlassian.annotations.PublicSpi;
import com.atlassian.jira.plugin.browsepanel.TabPanel;
import com.atlassian.jira.project.browse.BrowseContext;

/**
 * A Tab panel to be displayed on the Browse Project page.
 *
 * @deprecated This is plugin module is slated for removal. https://developer.atlassian.com/x/dsfgAQ details how to
 * integrate with the new project centric view in JIRA going forward. Since 7.0.
 */
@PublicSpi
@Deprecated
public interface ProjectTabPanel extends TabPanel<ProjectTabPanelModuleDescriptor, BrowseContext> {

}