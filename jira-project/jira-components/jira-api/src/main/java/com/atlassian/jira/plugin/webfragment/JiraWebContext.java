package com.atlassian.jira.plugin.webfragment;

import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;

import java.util.Map;
import java.util.Optional;

import static com.atlassian.jira.component.ComponentAccessor.getUserUtil;
import static com.atlassian.jira.plugin.webfragment.JiraWebInterfaceManager.CONTEXT_KEY_HELPER;
import static com.atlassian.jira.plugin.webfragment.JiraWebInterfaceManager.CONTEXT_KEY_I18N;
import static com.atlassian.jira.plugin.webfragment.JiraWebInterfaceManager.CONTEXT_KEY_LOCATION;
import static com.atlassian.jira.plugin.webfragment.JiraWebInterfaceManager.CONTEXT_KEY_USER;
import static com.atlassian.jira.plugin.webfragment.JiraWebInterfaceManager.CONTEXT_KEY_USERNAME;

/**
 * Useful wrapper for web contexts.
 * <p>
 * Context is normally a map but it contains some standard entries which can be easily retrieved using this class.
 * </p>
 * <p>
 * All getters return {@link java.util.Optional#empty empty} if requested object is not set in the context map or has a type different than expected.
 * </p>
 *
 * @since v6.5
 */
public final class JiraWebContext {
    private final Map<String, Object> context;

    private JiraWebContext(final Map<String, Object> context) {
        this.context = context;
    }

    public static JiraWebContext from(Map<String, Object> context) {
        return new JiraWebContext(context);
    }

    /**
     * Tries to figure out the user from context. It checks the "user" key first
     * and if it does not contain {@code ApplicationUser} value then
     * tries to load the user based on the "username" key in the context.
     *
     * @return user in the context
     */
    public Optional<ApplicationUser> getUser() {
        return Optional.ofNullable(
                get(CONTEXT_KEY_USER, ApplicationUser.class).orElseGet(this::getUserByUsername)
        );
    }

    private ApplicationUser getUserByUsername() {
        return getUsername().map(username -> getUserUtil().getUserByName(username)).orElse(null);
    }

    public Optional<JiraHelper> getHelper() {
        return get(CONTEXT_KEY_HELPER, JiraHelper.class);
    }

    public Optional<I18nHelper> getI18n() {
        return get(CONTEXT_KEY_I18N, I18nHelper.class);
    }

    public Optional<String> getUsername() {
        return get(CONTEXT_KEY_USERNAME, String.class);
    }

    public Optional<String> getLocation() {
        return get(CONTEXT_KEY_LOCATION, String.class);
    }

    /**
     * Returns value assigned to the specified key provided it is present in the map <i>and</i> has the specified type.
     *
     * @param key  entry key
     * @param type expected type
     * @param <T>  expected type
     * @return value if present
     */
    public <T> Optional<T> get(String key, Class<T> type) {
        Object value = context.get(key);
        return type.isInstance(value) ? Optional.ofNullable(type.cast(value)) : Optional.empty();
    }
}
