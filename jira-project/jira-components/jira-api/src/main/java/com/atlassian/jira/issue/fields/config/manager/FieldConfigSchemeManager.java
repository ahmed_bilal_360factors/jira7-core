package com.atlassian.jira.issue.fields.config.manager;

import com.atlassian.jira.issue.context.IssueContext;
import com.atlassian.jira.issue.context.JiraContextNode;
import com.atlassian.jira.issue.fields.ConfigurableField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.project.Project;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public interface FieldConfigSchemeManager {
    /**
     * This magic value list contains a single null element. Do not change to an empty list.
     */
    final List<IssueType> ALL_ISSUE_TYPES = Collections.singletonList(null);

    void init();

    // ----------------------------------------------------------------------------------------------- Retrieval Methods
    List<FieldConfigScheme> getConfigSchemesForField(ConfigurableField field);

    /**
     * Retrieves the {@link com.atlassian.jira.issue.fields.config.FieldConfigScheme} associated with the
     * {@link com.atlassian.jira.issue.fields.config.FieldConfig}
     *
     * @param fieldConfig the field config to retrieve the {@link com.atlassian.jira.issue.fields.config.FieldConfigScheme} of; cannot be null.
     * @return the config scheme for the {@link com.atlassian.jira.issue.fields.config.FieldConfig}. Null if the config scheme can not be found.
     */
    FieldConfigScheme getConfigSchemeForFieldConfig(FieldConfig fieldConfig);

    FieldConfigScheme getFieldConfigScheme(Long configSchemeId);

    FieldConfigScheme createDefaultScheme(ConfigurableField field, List<JiraContextNode> contexts, List<IssueType> issueTypes);

    /**
     * Updates the config schemes with the new contexts
     *
     * @param newScheme
     * @param contexts
     * @param field
     * @return The updated scheme
     */
    FieldConfigScheme updateFieldConfigScheme(FieldConfigScheme newScheme, List<JiraContextNode> contexts, ConfigurableField field);

    /**
     * Only update the name & description of a field
     *
     * @param scheme scheme with the name &amp; description to be updated
     * @return the updated scheme
     */
    FieldConfigScheme updateFieldConfigScheme(FieldConfigScheme scheme);

    /**
     * Removes a field config scheme, as well as its associated contexts and field configs
     * (which includes option sets and generic configs)
     *
     * @param fieldConfigSchemeId the id of the field config scheme to remove
     */
    void removeFieldConfigScheme(Long fieldConfigSchemeId);

    /**
     * Returns true if the custom field has a config for the Project and Issue Type of the given IssueContext.
     * <p>
     * This is equivalent to calling
     * <pre>  getRelevantConfig(issueContext, field) != null</pre>
     * but in general can run faster because it does not have to resolve the actual FieldConfig.
     *
     * @param issueContext IssueContext whose project and issue type will be used to check if the field has a config
     * @return true if the custom field has a config for the Project and Issue Type of the given IssueContext.
     * @see #getRelevantConfig(com.atlassian.jira.issue.context.IssueContext, com.atlassian.jira.issue.fields.ConfigurableField)
     */
    boolean isRelevantForIssueContext(IssueContext issueContext, ConfigurableField field);

    /**
     * Returns the relevant field config of this custom field for the give issue context
     *
     * @param issueContext issue context to find the relevant field config for
     * @return the relevant field config of this custom field for the give issue context
     * @see #isRelevantForIssueContext(com.atlassian.jira.issue.context.IssueContext, com.atlassian.jira.issue.fields.ConfigurableField)
     */
    FieldConfig getRelevantConfig(IssueContext issueContext, ConfigurableField field);

    FieldConfigScheme createFieldConfigScheme(FieldConfigScheme newConfigScheme, List<JiraContextNode> contexts, List<IssueType> issueTypes, ConfigurableField field);

    FieldConfigScheme createDefaultScheme(ConfigurableField field, List<JiraContextNode> contexts);

    void removeSchemeAssociation(List<JiraContextNode> contexts, ConfigurableField configurableField);

    // ------------------------------------------------------------------------------------------- Informational Methods

    /**
     * Returns a non-null list of Projects associated with the given field.
     *
     * @param field the Field
     * @return a non-null list of Projects associated with the given field.
     */
    List<Project> getAssociatedProjectObjects(ConfigurableField field);

    /**
     * Returns the FieldConfigScheme for the given Project and ConfigurableField.
     *
     * @param issueContext actually we ignore the Issue Type and just use the Project.
     * @param field        the ConfigurableField
     * @return the FieldConfigScheme for the given Project and ConfigurableField.
     * @deprecated Use {@link #getRelevantConfigScheme(Project, ConfigurableField)} instead. Since v6.3.7.
     */
    @Nullable
    FieldConfigScheme getRelevantConfigScheme(IssueContext issueContext, ConfigurableField field);

    /**
     * Returns the FieldConfigScheme for the given Project and ConfigurableField.
     *
     * @param project the Project.
     * @param field   the ConfigurableField
     * @return the FieldConfigScheme for the given Project and ConfigurableField.
     */
    @Nullable
    FieldConfigScheme getRelevantConfigScheme(Project project, ConfigurableField field);

    /**
     * Returns a collection of {@link com.atlassian.jira.issue.fields.config.FieldConfigScheme}s for all Configuration
     * Contexts that will become invalid after the issuetype has been removed.  That is the configuration contexts that
     * will no longer be linked to ANY issue types after the issue type passed is has been deleted.
     *
     * @param issueType The issueType to be deleted
     * @return A collection of {@link com.atlassian.jira.issue.fields.config.FieldConfigScheme}s
     * @since v3.11
     */
    Collection getInvalidFieldConfigSchemesForIssueTypeRemoval(IssueType issueType);

    /**
     * Given an issueType, this method will correctly remove the fieldConfigSchemes if necessary.  In other words
     * if a FieldConfigScheme is linked to only a single issueType, and we're deleting that issuetype then that
     * FieldConfigScheme will be deleted.  If a FieldConfigScheme is associated with multiple issueTypes, then only
     * the association for the issueType we're deleting will be removed, but the FieldConfigScheme will remain.
     *
     * @param issueType The IssueType being deleted
     * @since v3.11
     */
    void removeInvalidFieldConfigSchemesForIssueType(IssueType issueType);

    /**
     * Given a CustomField, this method will correctly remove the fieldConfigSchemes if necessary.  In other words
     * if a FieldConfigScheme is linked to only a single CustomField, and we're deleting that field then that
     * FieldConfigScheme will be deleted.  If a FieldConfigScheme is associated with multiple fields, then only
     * the association for the field we're deleting will be removed, but the FieldConfigScheme will remain.
     *
     * @param customFieldId The id of the CustomField being deleted
     * @since v3.13
     */
    void removeInvalidFieldConfigSchemesForCustomField(String customFieldId);
}