package com.atlassian.jira.issue.statistics;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.jira.project.Project;
import com.atlassian.query.Query;

import java.util.Optional;
import java.util.Set;

/**
 *  Contains methods that produce data based on project statistics.
 *  @since 7.1
 */
@ExperimentalApi
public interface ProjectStatisticsManager {

    /**
     * Returns all the projects that the issues resulting from the given query belong to.
     * @param query the query to determine the projects of. Optional; in which case the empty query is used.
     * @return the set of Projects for which issues were found as a result of evaluating the query.
     */
    Set<Project> getProjectsResultingFrom(Optional<Query> query);
}
