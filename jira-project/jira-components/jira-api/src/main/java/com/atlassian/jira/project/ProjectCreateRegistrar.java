package com.atlassian.jira.project;

import com.atlassian.annotations.PublicApi;

/**
 * Keeps a registry of {@link ProjectCreateHandler} that need
 * to be notified every time a project is created.
 *
 * @since 7.0
 */
@PublicApi
public interface ProjectCreateRegistrar {
    /**
     * Registers a handler that will get notifications every time a project is created.
     *
     * @param handler The handler to register.
     */
    void register(ProjectCreateHandler handler);

    /**
     * Unregisters a handler, which means that it will stop getting notifications for project creation.
     *
     * @param handler The handler to unregister.
     */
    void unregister(ProjectCreateHandler handler);
}
