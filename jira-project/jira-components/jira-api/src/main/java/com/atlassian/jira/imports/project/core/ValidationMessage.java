package com.atlassian.jira.imports.project.core;

import com.atlassian.jira.util.MessageSet;

import java.io.Serializable;

/**
 * A set of messages with a display name heading.
 *
 * @since v7.0
 */
public class ValidationMessage implements Serializable {
    private final String displayName;
    private final MessageSet messageSet;

    public ValidationMessage(final String displayName, final MessageSet messageSet) {
        this.displayName = displayName;
        this.messageSet = messageSet;
    }

    public String getDisplayName() {
        return displayName;
    }

    public boolean isValidated() {
        return messageSet != null;
    }

    public MessageSet getMessageSet() {
        return messageSet;
    }
}
