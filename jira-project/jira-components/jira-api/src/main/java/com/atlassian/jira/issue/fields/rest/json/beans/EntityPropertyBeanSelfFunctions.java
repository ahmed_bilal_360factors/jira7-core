package com.atlassian.jira.issue.fields.rest.json.beans;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.fugue.Function2;

import java.util.function.BiFunction;

/**
 * Functions which calculate self address of properties for various entities.
 *
 * @since v6.2
 */
@ExperimentalApi
public class EntityPropertyBeanSelfFunctions {
    /**
     * @since 7.0.0
     */
    public static BiFunction<Long, String, String> EMPTY_BIFUNCTION = (aLong, s) -> "";

    /**
     * @deprecated In 7.0.0 {@link Function2} has been deprecated in favour of {@link BiFunction}. Use {@link #EMPTY_BIFUNCTION}
     */
    @Deprecated
    public static Function2<Long, String, String> EMPTY = EMPTY_BIFUNCTION::apply;

    /**
     * @deprecated In 7.0.0 {@link Function2} has been deprecated in favour of {@link BiFunction}. Use {@link com.atlassian.jira.issue.fields.rest.json.beans.EntityPropertyBeanSelfFunctions.IssuePropertySelfBiFunction}
     */
    @Deprecated
    public static class IssuePropertySelfFunction implements Function2<Long, String, String> {
        private final IssuePropertySelfBiFunction issuePropertySelfBiFunction = new IssuePropertySelfBiFunction();

        @Override
        public String apply(final Long entityId, final String encodedPropertyKey) {
            return issuePropertySelfBiFunction.apply(entityId, encodedPropertyKey);
        }
    }

    /**
     * @since 7.0.0
     */
    public static class IssuePropertySelfBiFunction implements BiFunction<Long, String, String> {
        @Override
        public String apply(final Long entityId, final String encodedPropertyKey) {

            return String.format("issue/%d/properties/%s", entityId, encodedPropertyKey);
        }
    }

    /**
     * @deprecated In 7.0.0 {@link Function2} has been deprecated in favour of {@link BiFunction}. Use {@link com.atlassian.jira.issue.fields.rest.json.beans.EntityPropertyBeanSelfFunctions.ProjectPropertySelfBiFunction}
     */
    @Deprecated
    public static class ProjectPropertySelfFunction implements Function2<Long, String, String> {
        private final ProjectPropertySelfBiFunction projectPropertySelfBiFunction = new ProjectPropertySelfBiFunction();

        @Override
        public String apply(final Long entityId, final String encodedPropertyKey) {
            return projectPropertySelfBiFunction.apply(entityId, encodedPropertyKey);
        }
    }

    /**
     * @since 7.0.0
     */
    public static class ProjectPropertySelfBiFunction implements BiFunction<Long, String, String> {
        @Override
        public String apply(final Long entityId, final String encodedPropertyKey) {
            return String.format("project/%d/properties/%s", entityId, encodedPropertyKey);
        }
    }

    /**
     * @deprecated In 7.0.0 {@link Function2} has been deprecated in favour of {@link BiFunction}. Use {@link CommentPropertySelfBifFunction}
     */
    @Deprecated
    public static class CommentPropertySelfFunction implements Function2<Long, String, String> {
        private final CommentPropertySelfBifFunction commentPropertySelfBifFunction = new CommentPropertySelfBifFunction();

        @Override
        public String apply(final Long entityId, final String encodedPropertyKey) {
            return commentPropertySelfBifFunction.apply(entityId, encodedPropertyKey);
        }
    }

    /**
     * @since 7.0.0
     */
    public static class CommentPropertySelfBifFunction implements BiFunction<Long, String, String> {
        @Override
        public String apply(final Long entityId, final String encodedPropertyKey) {
            return String.format("comment/%d/properties/%s", entityId, encodedPropertyKey);
        }
    }

    /**
     * @deprecated In 7.0.0 {@link Function2} has been deprecated in favour of {@link BiFunction}. Use {@link #dashboardItemPropertySelfBiFunction(String)}
     */
    @Deprecated
    public static Function2<Long, String, String> dashboardItemPropertySelfFunction(String dashboardId) {
        return new DashboardItemPropertySelfFunction(dashboardId);
    }

    public static BiFunction<Long, String, String> dashboardItemPropertySelfBiFunction(String dashboardId) {
        return new DashboardItemPropertySelfBiFunction(dashboardId);
    }

    /**
     * @deprecated In 7.0.0 {@link Function2} has been deprecated in favour of {@link BiFunction}. Use {@link DashboardItemPropertySelfBiFunction}
     */
    @Deprecated
    private static class DashboardItemPropertySelfFunction implements Function2<Long, String, String> {
        private final DashboardItemPropertySelfBiFunction dashboardItemPropertySelfBiFunction;

        public DashboardItemPropertySelfFunction(final String dashboardId) {
            this.dashboardItemPropertySelfBiFunction = new DashboardItemPropertySelfBiFunction(dashboardId);
        }

        @Override
        public String apply(final Long entityId, final String encodedPropertyKey) {
            return dashboardItemPropertySelfBiFunction.apply(entityId, encodedPropertyKey);
        }
    }

    /**
     * @since 7.0.0
     */
    private static class DashboardItemPropertySelfBiFunction implements BiFunction<Long, String, String> {
        private final String dashboardId;

        public DashboardItemPropertySelfBiFunction(final String dashboardId) {
            this.dashboardId = dashboardId;
        }

        @Override
        public String apply(final Long entityId, final String encodedPropertyKey) {
            return String.format("dashboard/%s/items/%d/properties/%s", dashboardId, entityId, encodedPropertyKey);
        }
    }

    /**
     * @deprecated In 7.0.0 {@link Function2} has been deprecated in favour of {@link BiFunction}. Use {@link IssueTypePropertySelfBiFunction}
     */
    @Deprecated
    public static class IssueTypePropertySelfFunction implements Function2<Long, String, String> {
        private final IssueTypePropertySelfBiFunction issueTypePropertySelfBiFunction = new IssueTypePropertySelfBiFunction();

        @Override
        public String apply(final Long entityId, final String encodedPropertyKey) {
            return issueTypePropertySelfBiFunction.apply(entityId, encodedPropertyKey);
        }
    }

    /**
     * @since 7.0.0
     */
    public static class IssueTypePropertySelfBiFunction implements BiFunction<Long, String, String> {
        @Override
        public String apply(final Long entityId, final String encodedPropertyKey) {
            return String.format("issuetype/%d/properties/%s", entityId, encodedPropertyKey);
        }
    }
}
