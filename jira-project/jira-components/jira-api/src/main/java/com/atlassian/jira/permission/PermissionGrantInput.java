package com.atlassian.jira.permission;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.google.common.base.Objects;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Input for building a {@link PermissionGrant}.
 * <p>
 * Use {@link #newGrant}(holder, permission) method to create new instances.
 * </p>
 */
@Immutable
@PublicApi
public final class PermissionGrantInput {
    private final PermissionHolder holder;
    private final ProjectPermissionKey permission;

    private PermissionGrantInput(final PermissionHolder holder, final ProjectPermissionKey permission) {
        this.holder = checkNotNull(holder);
        this.permission = checkNotNull(permission);
    }

    public PermissionHolder getHolder() {
        return holder;
    }

    public ProjectPermissionKey getPermission() {
        return permission;
    }

    /**
     * Creates a new instance of this class.
     *
     * @param holder        permission holder, must not be null
     * @param permissionKey permission key, must not be null
     * @return a new {@code PermissionGrantInput} instance
     */
    public static PermissionGrantInput newGrant(@Nonnull PermissionHolder holder, @Nonnull ProjectPermissionKey permissionKey) {
        return new PermissionGrantInput(holder, permissionKey);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("holder", holder)
                .add("permission", permission)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PermissionGrantInput that = (PermissionGrantInput) o;

        return Objects.equal(this.holder, that.holder) &&
                Objects.equal(this.permission, that.permission);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(holder, permission);
    }
}
