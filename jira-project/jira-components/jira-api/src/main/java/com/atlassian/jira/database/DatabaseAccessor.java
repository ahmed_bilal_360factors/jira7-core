package com.atlassian.jira.database;

import com.atlassian.annotations.PublicApi;

import javax.annotation.Nonnull;
import java.sql.Connection;
import java.util.Optional;
import java.util.function.Function;

/**
 * Provides access to database connections from the Database Connection Pool.
 * <p>
 * <strong>Important:</strong> Note that due to how customers configure DB connections, you must include the
 * {@link #getSchemaName() schema name} in your queries if one is configured.
 *
 * @since 7.0
 */
@PublicApi
public interface DatabaseAccessor {
    /**
     * Returns the vendor of the database that we are connected to.
     *
     * @return the vendor of the database that we are connected to.
     * @throws IllegalStateException if the connected DB is not one of the supported DB vendors as defined in {@link DatabaseVendor}
     * @since 7.0
     */
    @Nonnull
    DatabaseVendor getDatabaseVendor();

    /**
     * Returns the configured schema name of the configured database connection.
     * If this is not empty then SQL will need to qualify the table name with this schema name.
     *
     * @return the configured database schema name
     * @since 7.0
     */
    @Nonnull
    Optional<String> getSchemaName();

    /**
     * Executes SQL statements as defined in the callback function.
     * <p>
     * This method will borrow a new connection from the pool, pass it to the callback function and then return it to
     * the pool after the callback has completed.
     * Even if OfBiz is currently running in a ThreadLocal transaction, this will retrieve a fresh connection from
     * the pool.
     * If you want to run in an existing OfBiz transaction then see instead {@link #runInTransaction(ConnectionFunction)}
     * <p>
     * Because database connections are limited resources, your code should do its database operations and return
     * from the callback as soon as possible in order to not starve other threads of this resource.
     * In particular, you should not call any code that may in turn borrow a second connection from the pool (this
     * can lead to a deadlock).
     * In general avoid doing anything "slow" (eg I/O) nor await on other limited resources (eg locks).
     * <p>
     * <strong>Do not close the given connection</strong> - DatabaseAccessor will return it to the pool for you
     * after the method is complete.
     * If the ConnectionFunction callback throws a RuntimeException and the connection is not in auto-commit mode,
     * then this method will perform a rollback on the connection (no explicit rollback is required).
     * <p>
     * Do not hold onto the Connection after your callback completes.
     * Once the callback completes, the connection will be returned to the pool and can be immediately borrowed by
     * another thread.
     * <p>
     * The connection will have the default auto-commit value as defined by the JIRA connection pool.
     * As at JIRA 7.0 this means autocommit == true.
     * See {@link org.apache.commons.dbcp.PoolableConnectionFactory#activateObject(Object)} for details.
     * Note that this is very different to the behaviour of {@link #runInTransaction(Function)} where the
     * transaction is completely managed for you.
     * <p>
     * Example Usage for autocommit:
     * <pre>
     *     databaseAccessor.execute(connection -> runMyQuery(connection.getJdbcConnection()));
     * </pre>
     * <p>
     * Example Usage with transaction:
     * <pre>
     *     databaseAccessor.executeQuery(
     *         connection -> {
     *             connection.setAutoCommit(false);
     *             // throws RuntimeException to indicate rollback
     *             doSomeUpdates(connection.getJdbcConnection());
     *             connection.commit();
     *         }
     *     );
     * </pre>
     *
     * @param callback the callback function that can run one or more queries with the managed connection.
     * @param <R>      the return type of the callback and of this method
     * @return the value as returned by the callback function
     * @since 7.0
     */
    <R> R executeQuery(@Nonnull ConnectionFunction<R> callback);

    /**
     * Executes SQL statements as defined in the callback function and manages transaction semantics.
     * <p>
     * This method will attempt to run the callback within an existing OfBiz transaction if one is running within
     * this thread. For that reason, the commit and rollback will be completely managed for you.
     * If the callback returns successfully, then this indicates that the underlying transaction is allowed to be
     * committed at the appropriate time.
     * If you want to rollback the transaction then throw a RuntimeException. The transaction will be marked as
     * "rollback required" such that the code that started the transaction will eventually call rollback on the
     * Connection and the exception will be propagated.
     * <p>
     * Because the connection (borrowing and returning from the pool) and its transaction are managed for you,
     * then specific {@link Connection} methods are illegal to call and will cause a RuntimeException including:
     * <ul>
     * <li>{@link Connection#setAutoCommit(boolean)} autocommit is always false</li>
     * <li>{@link Connection#commit()}</li> commit will occur in the broader transaction context if no errors are encountered
     * <li>{@link Connection#rollback()}</li> rollback will occur if a RuntimeException is thrown from the callback function
     * <li>{@link Connection#close()}</li> the connection will be returned to the pool for you at the appropriate time
     * </ul>
     * Note that this is very different to the behaviour of {@link #executeQuery(ConnectionFunction)} where the
     * transaction is <em>not</em> managed.
     * <p>
     * Example Usage:
     * <pre>
     *     databaseAccessor.runInTransaction(
     *         connection -> {
     *             // throws RuntimeException to indicate rollback required
     *             doSomeUpdates(connection);
     *         }
     *     );
     * </pre>
     *
     * @param callback the callback function that can run one or more queries with the managed connection.
     * @param <R>      the return type of the callback and of this method
     * @return the value as returned by the callback function
     * @since 7.0
     */
    <R> R runInTransaction(@Nonnull final Function<Connection, R> callback);
}
