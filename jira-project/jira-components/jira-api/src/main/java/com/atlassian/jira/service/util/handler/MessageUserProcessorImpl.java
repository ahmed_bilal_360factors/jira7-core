package com.atlassian.jira.service.util.handler;

import com.atlassian.annotations.Internal;
import com.atlassian.jira.bc.user.search.UserSearchService;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.mail.MailUtils;

import javax.annotation.Nullable;
import javax.mail.Message;
import javax.mail.MessagingException;
import java.util.Iterator;
import java.util.List;

/**
 * @since v5.0
 */
@Internal
public class MessageUserProcessorImpl implements MessageUserProcessor {
    private final UserManager userManager;
    private final UserSearchService userSearchService;

    public MessageUserProcessorImpl(UserManager userManager, UserSearchService userSearchService) {
        this.userManager = userManager;
        this.userSearchService = userSearchService;
    }

    /**
     * For each sender of the given message in turn, look up a User first with a case-insensitively equal email address,
     * and failing that, with a username equal to the email address.
     * <p>
     * JIRA wants to do this because when we create users in email handlers, we set email and username equal. If a user
     * subsequently changes their email address, we must not assume they don't exist and create them with the email
     * address as the username.
     *
     * @param message the message from which to get the User.
     * @return the User matching the sender of the message or null if none found.
     * @throws javax.mail.MessagingException if there's strife getting the message sender.
     */
    @Override
    @Nullable
    public ApplicationUser getAuthorFromSender(final Message message) throws MessagingException {

        final List<String> senders = MailUtils.getSenders(message);
        ApplicationUser user = null;
        for (final String emailAddress : senders) {
            user = findUserByEmail(emailAddress);
            if (user != null) {
                break;
            }
            user = findUserByUsername(emailAddress);
            if (user != null) {
                break;
            }
        }

        return user;
    }

    /**
     * Finds the user with the given username or returns null if there is no such User. Convenience method which doesn't
     * throw up.
     *
     * @param username the username.
     * @return the User or null.
     */
    @Nullable
    public ApplicationUser findUserByUsername(final String username) {
        return userManager.getUserObject(username);
    }

    /**
     * Returns the first User found with an email address that equals the given emailAddress case insensitively.
     *
     * @param emailAddress the email address to match.
     * @return the User.
     */
    @Nullable
    public ApplicationUser findUserByEmail(final String emailAddress) {
        Iterator<ApplicationUser> users = userSearchService.findUsersByEmail(emailAddress).iterator();
        if (users.hasNext()) {
            return users.next();
        } else {
            return null;
        }
    }
}
