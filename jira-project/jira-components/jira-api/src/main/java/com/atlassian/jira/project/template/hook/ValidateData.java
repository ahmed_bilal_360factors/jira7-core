package com.atlassian.jira.project.template.hook;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nullable;

/**
 * Request object for the "validate project" hook.
 *
 * @since 7.0
 */
@PublicApi
public class ValidateData {
    private final String projectName;
    private final String projectKey;
    private final ApplicationUser lead;

    public ValidateData(String projectName, String projectKey, ApplicationUser lead) {
        this.projectName = projectName;
        this.projectKey = projectKey;
        this.lead = lead;
    }

    /**
     * Returns the project name.
     *
     * @return project name
     */
    public String projectName() {
        return projectName;
    }

    /**
     * Returns the project key.
     *
     * @return project key
     */
    public String projectKey() {
        return projectKey;
    }

    /**
     * Returns the project lead, if specified.
     *
     * @return an ApplicationUser
     */
    @Nullable
    public ApplicationUser lead() {
        return lead;
    }
}
