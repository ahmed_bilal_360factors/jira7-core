package com.atlassian.jira.issue.fields.rest;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.issue.fields.rest.json.beans.SuggestionGroupBean;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;

import java.util.List;
import java.util.Optional;

/**
 * Provides suggestion groups needed to render a project picker client-side.
 *
 * @since v7.0
 */
@PublicApi
public interface ProjectSuggestionProvider {
    /**
     * Returns a list of SuggestionGroupBeans containing all projects.
     *
     * @param selectedProject Used to highlight the currently selected project
     * @param includeRecent   True if the result should contain a separate group for the most recent projects first
     * @return A list of suggestions to render a project picker
     */
    List<SuggestionGroupBean> getProjectPickerSuggestions(final Optional<Long> selectedProject, boolean includeRecent);

    /**
     * Returns a list of SuggestionGroupBeans containing all the projects the current user can access with the provided
     * permission.
     *
     * @param permission      the permission to use to retrieve projects for the current user
     * @param selectedProject Used to highlight the currently selected project
     * @param includeRecent   True if the result should contain a separate group for the most recent projects first
     * @return A list of suggestions to render a project picker
     */
    List<SuggestionGroupBean> getProjectPickerSuggestions(final ProjectPermissionKey permission, final Optional<Long> selectedProject, boolean includeRecent);
}
