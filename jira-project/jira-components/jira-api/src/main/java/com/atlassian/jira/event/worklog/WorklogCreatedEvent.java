package com.atlassian.jira.event.worklog;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.issue.worklog.Worklog;

/**
 * Event indicating that worklog has been created.
 *
 * @since v7.0
 */
@PublicApi
public class WorklogCreatedEvent extends AbstractWorklogEvent implements WorklogEvent {
    public WorklogCreatedEvent(final Worklog worklog) {
        super(worklog);
    }
}
