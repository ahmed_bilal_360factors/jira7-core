package com.atlassian.jira.bc.user.search;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

/**
 * Service that retrieves a collection of {@link ApplicationUser} objects based on a partial query string
 *
 * @since 7.0
 */
@PublicApi
public interface UserSearchService {
    /**
     * Get Users based on a query string.
     * <p>
     * Matches on start of username, full name and email as well as all the tokenised (on spaces, '.', '@' etc) words
     * Only returns active users.
     * <p>
     * Results are sorted according to the {@link com.atlassian.jira.issue.comparator.UserCachingComparator}.
     *
     * @param jiraServiceContext Jira Service Context
     * @param query              String to search for.
     * @return List of {@link ApplicationUser} objects that match criteria.
     * @see #findUsers(JiraServiceContext, String, UserSearchParams)
     */
    List<ApplicationUser> findUsers(JiraServiceContext jiraServiceContext, String query);

    /**
     * Returns a user by exact username
     *
     * @param jiraServiceContext Jira Service Context
     * @param query              String to search for.
     * @return The {@link ApplicationUser} object with supplied username.
     */
    ApplicationUser getUserByName(JiraServiceContext jiraServiceContext, String query);

    /**
     * Get Users based on a query string.
     * <p>
     * Matches on start of username, full name and email as well as all the tokenised (on spaces, '.', '@' etc) words
     * This will search even if the query passed is null or empty.
     * Only returns active users.
     * <p>
     * Results are sorted according to the {@link com.atlassian.jira.issue.comparator.UserCachingComparator}.
     *
     * @param jiraServiceContext Jira Service Context
     * @param query              String to search for.
     * @return List of {@link ApplicationUser} objects that match criteria.
     * @see #findUsers(JiraServiceContext, String, UserSearchParams)
     */
    List<ApplicationUser> findUsersAllowEmptyQuery(JiraServiceContext jiraServiceContext, String query);

    /**
     * Get Users based on a query string.
     * <p>
     * Matches on start of username, full name and email as well as all the tokenised (on spaces, '.', '@' etc) words
     * <p>
     * Results are sorted according to the {@link com.atlassian.jira.issue.comparator.UserCachingComparator}.
     * <p>
     * If the users in the database change during this call results might not be consistent if a
     * {@linkplain UserSearchParams#getPostProcessingFilter() post processing filter}
     * and a result limit is used.
     *
     * @param jiraServiceContext Jira Service Context
     * @param query              String to search for.
     * @param userSearchParams   Additional search parameters
     * @return List of {@link ApplicationUser} objects that match criteria.
     * @since 5.1.5
     */
    List<ApplicationUser> findUsers(JiraServiceContext jiraServiceContext, String query, UserSearchParams userSearchParams);

    /**
     * Get Users based on a query string.
     * <p>
     * Matches on start of username, full name and email as well as all the tokenised (on spaces, '.', '@' etc) words
     * <p>
     * Matches email only when userSearchParams.canMatchEmail() is true.
     * <p>
     * If the users in the database change during this call results might not be consistent if a
     * {@linkplain UserSearchParams#getPostProcessingFilter() post processing filter}
     * and a result limit is used.
     *
     * @param query            the query to search username, display name and email address
     * @param userSearchParams the search criteria
     * @return the list of matched users
     * @since 6.2
     */
    List<ApplicationUser> findUsers(String query, UserSearchParams userSearchParams);

    /**
     * Get Users based on query strings.
     * <p>
     * Matches nameQuery on start of username, full name and email as well as all the tokenised (on spaces, '.', '@' etc) words
     * <p>
     * Matches emailQuery on start of email, as well as the tokenised words. Email matching is performed only when userSearchParams.canMatchEmail() is true.
     * If email matching is enabled, nameQuery AND emailQuery criteria must match returned users.
     * <p>
     * If the users in the database change during this call results might not be consistent if a
     * {@linkplain UserSearchParams#getPostProcessingFilter() post processing filter}
     * and a result limit is used.
     *
     * @param nameQuery        the query to search username and display name.
     * @param emailQuery       the query to search email address, subject to userSearchParams.canMatchEmail.
     * @param userSearchParams the search criteria
     * @return the list of matched users
     * @since 6.2
     */
    List<ApplicationUser> findUsers(String nameQuery, String emailQuery, UserSearchParams userSearchParams);

    /**
     * Get user names based on a query string.
     * <p>
     * Matches on start of username, full name and email as well as all the tokenised (on spaces, '.', '@' etc) words
     * <p>
     * Matches email only when userSearchParams.canMatchEmail() is true.
     * <p>
     * If the users in the database change during this call results might not be consistent if a
     * {@linkplain UserSearchParams#getPostProcessingFilter() post processing filter}
     * and a result limit is used.
     *
     * @param query            the query to search username, display name and email address
     * @param userSearchParams the search criteria
     * @return the list of matched user names
     * @since 7.0
     */
    List<String> findUserNames(String query, UserSearchParams userSearchParams);

    /**
     * Get user names based on query strings.
     * <p>
     * Matches nameQuery on start of username, full name and email as well as all the tokenised (on spaces, '.', '@' etc) words
     * <p>
     * Matches emailQuery on start of email, as well as the tokenised words. Email matching is performed only when userSearchParams.canMatchEmail() is true.
     * If email matching is enabled, nameQuery AND emailQuery criteria must match returned users.
     * <p>
     * If the users in the database change during this call results might not be consistent if a
     * {@linkplain UserSearchParams#getPostProcessingFilter() post processing filter}
     * and a result limit is used.
     *
     * @param nameQuery        the query to search username and display name.
     * @param emailQuery       the query to search email address, subject to userSearchParams.canMatchEmail.
     * @param userSearchParams the search criteria
     * @return the list of matched users
     * @since 7.0
     */
    List<String> findUserNames(String nameQuery, String emailQuery, UserSearchParams userSearchParams);

    /**
     * Determine whether a user matches the search criteria specified in the {@code userSearchParams} parameter.
     * <p>
     * allowEmptyQuery in {@code userSearchParams} is ignored.
     *
     * @param user             the user to be matched
     * @param userSearchParams the search criteria
     * @return true if the user matches the search criteria
     * @since v6.2
     */
    boolean userMatches(ApplicationUser user, UserSearchParams userSearchParams);

    /**
     * Returns true only if UserPicker Ajax search is enabled AND the user in the context has com.atlassian.jira.user.ApplicationUser Browse permission.
     *
     * @param jiraServiceContext Jira Service Context
     * @return True if enabled, otherwise false
     */
    boolean canPerformAjaxSearch(JiraServiceContext jiraServiceContext);

    /**
     * Determines whether the given user could perform AJAX search.
     *
     * @since v6.2
     */
    boolean canPerformAjaxSearch(ApplicationUser user);

    /**
     * Whether or not the UserPicker Ajax should search or show email addresses
     *
     * @param jiraServiceContext Jira Service Context
     * @return True if email addresses can be shown, otherwise false
     */
    boolean canShowEmailAddresses(JiraServiceContext jiraServiceContext);

    /**
     * Searches for a user with the specified full name.
     *
     * @param fullName the full name to search for.
     * @return a collection of user keys with the specified full name, empty if none were found.
     * @since 7.0
     */
    @Nonnull
    Iterable<String> findUserKeysByFullName(@Nullable String fullName);

    /**
     * Searches for a user with the specified full name.
     *
     * @param fullName the full name to search for.
     * @return a collection of users with the specified full name, empty if none were found.
     * @since 7.0
     */
    @Nonnull
    Iterable<ApplicationUser> findUsersByFullName(@Nullable String fullName);

    /**
     * Searches for a user with the specified e-mail address.
     *
     * @param email the e-mail address to search.
     * @return collection of found user keys, empty if none were found.
     * @since 7.0
     */
    @Nonnull
    Iterable<String> findUserKeysByEmail(@Nullable String email);

    /**
     * Searches for a user with the specified e-mail address.
     *
     * @param email the e-mail address to search.
     * @return collection of found users, empty if none were found.
     * @since 7.0
     */
    @Nonnull
    Iterable<ApplicationUser> findUsersByEmail(@Nullable String email);

    /**
     * Filter a list of {@link ApplicationUser} based on provided criteria and return only those users that match.
     * <p>
     * NB: The resultant list will be sorted and duplicates removed, if the {@link UserSearchParams#isSorted()} value
     * is true. If this is set to false, then the resultant list will not be sorted and no duplicate checks performed,
     * which may provide some better performance in specific scenarios, as the source list will be filtered as-is and
     * all valid results returned in the order they were initially. If a max results value is provided (>= 0) in
     * the {@link UserSearchParams} then it will be limited in size to that value.
     *
     * @param applicationUsers the user to be filtered
     * @param nameQuery        the query to search username and display name.
     * @param userSearchParams the search criteria
     * @return a new list of users from provided list that match the filter criteria
     * @since v7.3
     */
    @Nonnull
    @ExperimentalApi
    List<ApplicationUser> filterUsers(List<ApplicationUser> applicationUsers, String nameQuery, UserSearchParams userSearchParams);

    /**
     * Filter a list of {@link ApplicationUser} based on provided criteria and return only those users that match.
     * <p>
     * NB: The resultant list will be sorted and duplicates removed, if the {@link UserSearchParams#isSorted()} value
     * is true. If this is set to false, then the resultant list will not be sorted and no duplicate checks performed,
     * which may provide some better performance in specific scenarios, as the source list will be filtered as-is and
     * all valid results returned in the order they were initially. If a max results value is provided (>= 0) in
     * the {@link UserSearchParams} then it will be limited in size to that value.
     *
     * @param applicationUsers the user to be filtered
     * @param nameQuery        the query to search username and display name.
     * @param emailQuery       the query to search email address, subject to userSearchParams.canMatchEmail.
     * @param userSearchParams the search criteria
     * @return a new list of users from provided list that match the filter criteria
     * @since v7.3
     */
    @Nonnull
    @ExperimentalApi
    List<ApplicationUser> filterUsers(List<ApplicationUser> applicationUsers, String nameQuery, String emailQuery, UserSearchParams userSearchParams);
}
