package com.atlassian.jira.event.comment;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.issue.comments.Comment;

/**
 * Comment published when a comment is deleted.
 */
@PublicApi
public class CommentDeletedEvent extends AbstractCommentEvent implements CommentEvent {

    public CommentDeletedEvent(final Comment comment) {
        super(comment);
    }
}
