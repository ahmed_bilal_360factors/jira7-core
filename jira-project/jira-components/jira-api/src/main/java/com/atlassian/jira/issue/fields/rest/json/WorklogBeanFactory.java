package com.atlassian.jira.issue.fields.rest.json;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.jira.issue.fields.rest.json.beans.WorklogJsonBean;
import com.atlassian.jira.issue.worklog.Worklog;
import com.atlassian.jira.user.ApplicationUser;

/**
 * This provides a simple, dependency-free, straight forward API to generating the JSON corresponding to a Worklog.
 *
 * @since v7.0
 */
@ExperimentalApi
public interface WorklogBeanFactory {
    /**
     * Generate a bean suitable for serialisation by Jackson into JSON.
     *
     * @param worklog      Worklog which would be used for creating bean.
     * @param loggedInUser WorklogJsonBean will be created in the context of loggedInUser (i.e. hide email address if necessary)
     * @return Bean suitable for serialisation by Jackson into JSON for given worklog
     */
    WorklogJsonBean createBean(Worklog worklog, ApplicationUser loggedInUser);

    /**
     * Generate beans suitable for serialisation by Jackson into JSON.
     *
     * @param worklogs     Worklogs which would be used for creating a collection of beans.
     * @param loggedInUser WorklogJsonBean will be created in the context of loggedInUser (i.e. hide email address if necessary)
     * @return Bean suitable for serialisation by Jackson into JSON for given worklog
     */
    Iterable<WorklogJsonBean> createBeans(Iterable<Worklog> worklogs, ApplicationUser loggedInUser);
}
