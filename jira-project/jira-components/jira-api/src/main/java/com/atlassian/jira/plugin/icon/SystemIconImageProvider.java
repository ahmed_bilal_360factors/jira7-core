package com.atlassian.jira.plugin.icon;

import com.atlassian.annotations.ExperimentalSpi;
import com.atlassian.jira.avatar.Avatar;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStream;

/**
 * An implementation that can provide image data for system icons. This allows icon types to define how they
 * provide system icons.
 *
 * @since v7.1
 */
@ExperimentalSpi
public interface SystemIconImageProvider {
    /**
     * Get the input stream for this system icon.
     * @param icon The icon to get the input stream for.
     * @param size The size requested. Note that for SVG icons this can be null.
     * @return Returns the input stream for the system icon image data.
     * @throws IOException Something went wrong.
     */
    @Nonnull InputStream getSystemIconInputStream(@Nonnull Avatar icon, @Nonnull Avatar.Size size) throws IOException;
}
