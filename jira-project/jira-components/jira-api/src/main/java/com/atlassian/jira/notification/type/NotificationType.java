package com.atlassian.jira.notification.type;

import com.atlassian.annotations.PublicApi;

/**
 * A simple enumeration of the different JIRA notification types
 *
 * @since v6.0
 */
@PublicApi
public enum NotificationType {
    CURRENT_ASSIGNEE("Current_Assignee", "CurrentAssignee"),
    REPORTER("Current_Reporter", "Reporter"),
    CURRENT_USER("Remote_User", "CurrentUser"),
    PROJECT_LEAD("Project_Lead", "ProjectLead"),
    COMPONENT_LEAD("Component_Lead", "ComponentLead"),
    SINGLE_USER("Single_User", "User"),
    GROUP("Group_Dropdown", "Group"),
    PROJECT_ROLE("Project_Role", "ProjectRole"),
    SINGLE_EMAIL_ADDRESS("Single_Email_Address", "EmailAddress"),
    ALL_WATCHERS("All_Watchers", "AllWatchers"),
    USER_CUSTOM_FIELD_VALUE("User_Custom_Field_Value", "UserCustomField"),
    GROUP_CUSTOM_FIELD_VALUE("Group_Custom_Field_Value", "GroupCustomField");

    private final String dbCode;
    private final String restApiName;

    private NotificationType(String dbCode, final String restApiName) {
        this.dbCode = dbCode;
        this.restApiName = restApiName;
    }

    public String dbCode() {
        return dbCode;
    }

    public String getRestApiName() {
        return restApiName;
    }

    /**
     * Returns a NotificationType from the database code that JIRA uses internally.
     *
     * @param dbCode a magic string stored in the database tables
     * @return a notification type
     * @throws IllegalArgumentException if you give an invalid string
     */
    public static NotificationType from(String dbCode) {
        for (NotificationType notificationType : NotificationType.values()) {
            if (notificationType.dbCode().equals(dbCode)) {
                return notificationType;
            }
        }
        throw new IllegalArgumentException("You have to provide a valid mapped string");
    }
}
