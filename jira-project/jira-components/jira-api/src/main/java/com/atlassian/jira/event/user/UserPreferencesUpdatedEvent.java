package com.atlassian.jira.event.user;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.annotations.Internal;
import com.atlassian.jira.config.LocaleManager;
import com.atlassian.jira.user.ApplicationUser;

/**
 * Event indicating that a user's preferences have been updated.
 *
 * @since v5.0
 */
@EventName("profile.preferences.update")
public class UserPreferencesUpdatedEvent {
    private final String username;
    private final Long pageSize;
    private final String emailMimeType;
    private final String locale;
    private final String timeZoneId;
    private final boolean notifyOwnChangesByEmail;
    private final boolean shareByDefault;
    private final boolean keyboardShortcutsEnabled;
    private final String autowatchPreference;

    /**
     * @deprecated Pre-analytics constructor, event named "updateuserpreferences"; never whitelisted. Since 7.2.
     */
    @Deprecated
    public UserPreferencesUpdatedEvent(ApplicationUser user) {
        this(user, null, null, null, null, false, false, false, null);
    }

    @Internal
    public UserPreferencesUpdatedEvent(final ApplicationUser user,
                                       final Long pageSize,
                                       final String emailType,
                                       final String locale,
                                       final String timeZoneId,
                                       final boolean notifyOwnChanges,
                                       final boolean shareByDefault,
                                       final boolean keyboardShortcutsEnabled,
                                       final String autowatchPreference) {
        this.username = user.getName();
        this.locale = locale;
        this.pageSize = pageSize;
        this.emailMimeType = emailType;
        this.timeZoneId = timeZoneId;
        this.notifyOwnChangesByEmail = notifyOwnChanges;
        this.shareByDefault = shareByDefault;
        this.keyboardShortcutsEnabled = keyboardShortcutsEnabled;
        this.autowatchPreference = autowatchPreference;
    }

    public String getUsername() {
        return username;
    }

    public Long getPageSize() {
        return pageSize;
    }

    public String getEmailMimeType() {
        return emailMimeType;
    }

    /**
     * @returns A short string of the locale (e.g. "en_UK") or LocaleManager.DEFAULT_LOCALE for the default locale.
     */
    public String getLocale() {
        return locale;
    }

    public String getTimeZoneId() {
        return timeZoneId;
    }

    public boolean getNotifyOwnChangesByEmail() {
        return notifyOwnChangesByEmail;
    }

    public boolean getShareByDefault() {
        return shareByDefault;
    }

    public boolean getKeyboardShortcutsEnabled() {
        return keyboardShortcutsEnabled;
    }

    public String getAutowatchPreference() {
        return autowatchPreference;
    }
}
