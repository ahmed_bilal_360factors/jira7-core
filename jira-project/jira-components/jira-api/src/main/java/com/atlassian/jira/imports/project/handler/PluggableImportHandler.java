package com.atlassian.jira.imports.project.handler;

import com.atlassian.annotations.ExperimentalSpi;
import com.atlassian.jira.imports.project.core.BackupProject;
import com.atlassian.jira.imports.project.core.BackupSystemInformation;
import com.atlassian.jira.imports.project.core.ProjectImportResults;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapper;

import javax.annotation.Nullable;

/**
 * Interface for all injectable Project Import plugin points.
 *
 * @since v7.0
 */
@ExperimentalSpi
public interface PluggableImportHandler {
    /**
     * Set the backup project.
     * This will be injected after the handler is constructed, before any entities are processed.
     *
     * @param backupProject The backup project
     */
    void setBackupProject(BackupProject backupProject);

    /**
     * Set the backup system information.
     * This will be injected after the handler is constructed, before any entities are processed.
     *
     * @param backupSystemInformation The backup system information
     */
    void setBackupSystemInformation(BackupSystemInformation backupSystemInformation);

    /**
     * Set the project import mapper.
     * This will be injected after the handler is constructed, before any entities are processed.
     *
     * @param projectImportMapper The project import mapper
     */
    void setProjectImportMapper(ProjectImportMapper projectImportMapper);

    /**
     * Set the project import results.
     * This will be injected after the handler is constructed, before any entities are processed.
     * For pre-import handlers this will be null.
     *
     * @param projectImportResults The project import results
     */
    void setProjectImportResults(@Nullable ProjectImportResults projectImportResults);
}
