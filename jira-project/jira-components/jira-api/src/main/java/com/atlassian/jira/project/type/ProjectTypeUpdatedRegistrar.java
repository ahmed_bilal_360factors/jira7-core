package com.atlassian.jira.project.type;

import com.atlassian.annotations.PublicApi;

/**
 * Keeps a registry of {@link com.atlassian.jira.project.type.ProjectTypeUpdatedHandler} that need
 * to be notified every time the type of a project is updated.
 *
 * @since 7.0
 */
@PublicApi
public interface ProjectTypeUpdatedRegistrar {
    /**
     * Registers a handler that will get notifications every time the type of a project is updated.
     *
     * @param handler The handler to register.
     */
    void register(ProjectTypeUpdatedHandler handler);

    /**
     * Unregisters a handler, which means that it will stop getting notifications for project type updates.
     *
     * @param handler The handler to unregister.
     */
    void unregister(ProjectTypeUpdatedHandler handler);
}
