package com.atlassian.jira.bc.issue.search;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.search.ClauseTooComplexSearchException;
import com.atlassian.jira.issue.search.SearchContext;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.jql.context.QueryContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.Query;

import javax.annotation.Nonnull;
import java.util.Objects;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Provides functionality (search, query string generation, parsing, validation, context generation, etc...) related to
 * searching in JIRA. This deals with {@link Query} objects which contain the search criteria in JIRA.
 *
 * @since v4.0
 */
@PublicApi
public interface SearchService {
    /**
     * Search the index, and only return issues that are in the pager's range.
     * <p>
     * Note: that this method returns read only {@link Issue} objects, and should not be
     * used where you need the issue for update.
     * </p>
     *
     * @param searcher the user performing the search, which will be used to create a permission filter that filters out
     *                 any of the results the user is not able to see and will be used to provide context for the search.
     * @param query    contains the information required to perform the search.
     * @param pager    Pager filter (use {@link PagerFilter#getUnlimitedFilter()} to get all issues).
     * @return A {@link com.atlassian.jira.issue.search.SearchResults} containing the resulting issues.
     * @throws SearchException thrown if there is a severe problem encountered with lucene when searching (wraps an
     *                         IOException).
     * @since v7.0
     */
    SearchResults search(ApplicationUser searcher, Query query, PagerFilter pager) throws SearchException;

    /**
     * Search the index, and only return issues that are in the pager's range while AND'ing the raw lucene query
     * to the generated query from the provided searchQuery, not taking into account any security
     * constraints.
     * <p>
     * As the method name implies, it bypasses the security restrictions that would normally be applied
     * when performing a search.  As such, it should only be used for administrative tasks where you really
     * need to know about ALL affected issues.
     * </p>
     * <p>
     * <em>Note that this method returns read only {@link Issue} objects, and should not be
     * used where you need the issue for update</em>.  Also note that if you are only after the number of search
     * results use {@link #searchCount(ApplicationUser, Query)} as it provides better performance.
     * </p>
     *
     * @param query    contains the information required to perform the search.
     * @param searcher the user performing the search which will be used to provide context for the search.
     * @param pager    Pager filter (use {@link PagerFilter#getUnlimitedFilter()} to get all issues).
     * @return A {@link SearchResults} containing the resulting issues.
     * @throws SearchException                 thrown if there is a severe problem encountered with lucene when searching (wraps an
     *                                         IOException).
     * @throws ClauseTooComplexSearchException if the query or part of the query produces
     *                                         lucene that is too complex to be processed.
     * @since v7.0
     */
    SearchResults searchOverrideSecurity(ApplicationUser searcher, Query query, PagerFilter pager) throws SearchException;

    /**
     * Search the index, and return the count of the issues matching the query.
     *
     * @param query    contains the information required to perform the search.
     * @param searcher the user performing the search, which will be used to create a permission filter that filters out
     *                 any of the results the user is not able to see and will be used to provide context for the search.
     * @return the number of issues matching the query
     * @throws SearchException thrown if there is a severe problem encountered with lucene when searching (wraps an
     *                         IOException).
     * @since v7.0
     */
    long searchCount(ApplicationUser searcher, Query query) throws SearchException;

    /**
     * Return the number of issues matching the provided search criteria, overridding any security constraints.
     * <p>
     * As the method name implies, it bypasses the security restrictions that would normally be applied
     * when performing a search.  As such, it should only be used for administrative tasks where you really
     * need to know about ALL affected issues.
     * </p>
     * <p>
     * <b>Note:</b> Searching for the count avoids loading the issue data into memory, so this should be preferred
     * when the count is all you need to know.
     * </p>
     *
     * @param query    contains the information required to perform the search.
     * @param searcher the user performing the search which will be used to provide context for the search.
     * @return number of matching results.
     * @throws SearchException                 thrown if there is a severe problem encountered with lucene when searching (wraps an
     *                                         IOException).
     * @throws ClauseTooComplexSearchException if the query or part of the query produces
     *                                         lucene that is too complex to be processed.
     * @since v7.0
     */
    long searchCountOverrideSecurity(ApplicationUser searcher, Query query) throws SearchException;

    /**
     * Returns the query string to represent the specified SearchQuery.
     * <p>
     * The query string is prepended with &quot;<code>&amp;jqlQuery=</code>&quot; so that it is ready for use in building a URL.
     *
     * @param user  the user performing the search
     * @param query the SearchQuery to generate the query string of. Does not accept null.
     * @return a String which represents the query string of a SearchQuery (ie no context/base applied). Never null.
     * @since v7.0 with ApplicationUser, previously with User
     * @deprecated Since v7.1. Use {@link SearchService#getIssueSearchPath} instead.
     */
    String getQueryString(ApplicationUser user, Query query);

    /**
     * Returns the URL relative to the app context to represent the specified issue search.
     * <p>
     * The URL is composed by the issue search path and provided parameters (e.g. &quot;<code>&amp;/issues/?jql=...&filter=...</code>&quot;), but does NOT include the context path.
     *
     * @param user   the user performing the search
     * @param params the parameters to generate the URL. Possible parameters are filter Id and query (JQL), both are optional but you cannot use both at the same time. Does not accept null.
     * @return a String which represents the issue search URL and query string corresponding to the parameters (no context/base applied). Never null.
     * @since v7.1
     */
    @Nonnull
    String getIssueSearchPath(ApplicationUser user, @Nonnull IssueSearchParameters params);

    /**
     * Parses the query string into a JQL {@link Query}.
     *
     * @param searcher the user in context
     * @param query    the query to parse into a {@link Query}.
     * @return a result set that contains the query and a message set of any errors or warnings that occured during the parse.
     * @since v7.0
     */
    ParseResult parseQuery(ApplicationUser searcher, String query);

    /**
     * Validates the specified {@link Query} for passed user. The same as calling
     * {@code validateQuery(searcher, query, null);}.
     *
     * @param searcher the user performing the search
     * @param query    the search query to validate
     * @return a message set containing any errors encountered; never null.
     * @since v7.0
     */
    @Nonnull
    MessageSet validateQuery(ApplicationUser searcher, @Nonnull Query query);

    /**
     * Validates the specified {@link Query} for passed user and search request. This validates the
     * the passed query as if it was run as the passed search request.
     *
     * @param searcher        the user performing the search.
     * @param query           the search query to validate.
     * @param searchRequestId validate in the context of this search request. Can be null to indicate the passed
     *                        query is not currently a search request.
     * @return a message set containing any errors encountered; never null.
     * @since v7.0
     */
    @Nonnull
    MessageSet validateQuery(ApplicationUser searcher, @Nonnull Query query, Long searchRequestId);

    /**
     * Checks if a {@link Query} is capable of being shown on the simple (GUI-based) issue navigator edit screen.
     *
     * @param user  the user who is executing the query.
     * @param query the Query which to check that is displayable on the simple (GUI-based) issue navigator edit screen.
     *              Does not accept null.
     * @return true if the query is displayable on the simple (GUI-based) issue navigator edit screen, false otherwise.
     * @since v7.0
     */
    boolean doesQueryFitFilterForm(ApplicationUser user, Query query);

    /**
     * Generates a full QueryContext for the specified {@link Query} for the searching user. The full
     * QueryContext contains all explicit and implicitly specified projects and issue types from the Query.
     * <p>
     * For a better explanation of the differences between the full and simple QueryContexts, see
     * {@code QueryContextVisitor} in {@code jira-core}.
     * </p>
     *
     * @param searcher the user performing the search
     * @param query    the search query to generate the context for
     * @return a QueryContext that contains the implicit and explicit project / issue types implied by the included
     * clauses in the query.
     * @since v7.0
     */
    QueryContext getQueryContext(ApplicationUser searcher, Query query);

    /**
     * Generates a simple QueryContext for the specified {@link Query} for the searching user.
     * The simple QueryContext contains only the explicit projects and issue types specified in the Query. If none were
     * specified, it will be the Global context.
     * <p>
     * For a better explanation of the differences between the full and simple QueryContexts, see
     * {@code QueryContextVisitor} in {@code jira-core}.
     * </p>
     *
     * @param searcher the user performing the search
     * @param query    the search query to generate the context for
     * @return a QueryContext that contains only the explicit project / issue types from the included clauses in
     * the query.
     * @since v7.0
     */
    QueryContext getSimpleQueryContext(ApplicationUser searcher, Query query);

    /**
     * This produces an old-style {@link com.atlassian.jira.issue.search.SearchContext} based on the passed in
     * search query and the user that is performing the search.
     * <p>
     * This will only make sense if the query returns true for {@link #doesQueryFitFilterForm(ApplicationUser, Query)}
     * since SearchContext is only relevant for simple queries.
     * </p><p>
     * The more acurate context can be gotten by calling {@link #getQueryContext(ApplicationUser, Query)}.
     * </p><p>
     * If the query will not fit in the simple issue navigator then the generated SearchContext will be empty. This
     * method never returns a null SearchContext, even when passed a null SearchQuery.
     * </p>
     *
     * @param searcher the user performing the search, not always the SearchRequest's owner
     * @param query    the query for which you want a context
     * @return a SearchContext with the correct project/issue types if the query fits in the issue navigator, otherwise
     * an empty SearchContext. Never null.
     * @since v7.0
     */
    SearchContext getSearchContext(ApplicationUser searcher, Query query);

    /**
     * Gets the JQL string representation for the passed query. Returns the string from {@link Query#getQueryString()}
     * if it exists or generates one if it does not. Equilavent to:
     * <code><pre>
     *  if (query.getQueryString() != null)
     *    return query.getQueryString();
     *  else
     *    return getGeneratedJqlString(query);
     * </pre></code>
     *
     * @param query the query. Cannot be null.
     * @return the JQL string represenation of the passed query.
     */
    String getJqlString(Query query);

    /**
     * Generates a JQL string representation for the passed query. The JQL string is always generated, that is, {@link Query#getQueryString()}
     * is completely ignored if it exists. The returned JQL is automatically escaped as necessary.
     *
     * @param query the query. Cannot be null.
     * @return the generated JQL string representation of the passed query.
     */
    String getGeneratedJqlString(Query query);

    /**
     * Returns an equivalent {@link Query} with all the potential "information leaks" removed,
     * with regards to the specified user. For example, if the query contains the clause "project = Invisible", and the
     * specified user does not have browse permission for project "Invisible", the sanitised query will replace this
     * clause with "project = 12345" (where 12345 is the id of the project).
     *
     * @param searcher the user performing the search
     * @param query    the query to sanitise; must not be null.
     * @return the sanitised query; never null.
     * @since v7.0
     */
    Query sanitiseSearchQuery(ApplicationUser searcher, Query query);

    @PublicApi
    final class ParseResult {
        private Query query;
        private MessageSet errors;

        public ParseResult(Query query, MessageSet errors) {
            this.query = query;
            this.errors = notNull("errors", errors);
        }

        /**
         * @return the JQL {@link Query} parsed, or null if the query string was not valid.
         */
        public Query getQuery() {
            return query;
        }

        public MessageSet getErrors() {
            return errors;
        }

        public boolean isValid() {
            return !errors.hasAnyErrors();
        }
    }

    @PublicApi
    final class IssueSearchParameters {
        private final Query query;
        private final Long filterId;

        private IssueSearchParameters(Query query, Long filterId) {
            this.query = query;
            this.filterId = filterId;
        }

        public Query query() {
            return query;
        }

        public Long filterId() {
            return filterId;
        }

        public static class Builder {
            private Query query;
            private Long filterId;

            public Builder query(Query query) {
                this.query = query;
                return this;
            }

            public Builder filterId(Long filterId) {
                this.filterId = filterId;
                return this;
            }

            public IssueSearchParameters build() {
                return new IssueSearchParameters(this.query, this.filterId);
            }
        }

        public static Builder builder() {
            return new Builder();
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            final IssueSearchParameters that = (IssueSearchParameters) o;
            return Objects.equals(query, that.query) &&
                    Objects.equals(filterId, that.filterId);
        }

        @Override
        public int hashCode() {
            return Objects.hash(query, filterId);
        }
    }
}
