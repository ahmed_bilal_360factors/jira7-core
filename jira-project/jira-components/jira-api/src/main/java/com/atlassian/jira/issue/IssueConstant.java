package com.atlassian.jira.issue;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.Named;
import com.atlassian.jira.util.NamedWithDescription;

/**
 * Abstraction to represent any of the various constants like {@link com.atlassian.jira.issue.resolution.Resolution},
 * {@link com.atlassian.jira.issue.status.Status} etc.
 */
@PublicApi
public interface IssueConstant extends Comparable, Named, NamedWithDescription {
    String getId();

    String getName();

    String getDescription();

    Long getSequence();

    /**
     * Returns the url for the icon with the context path added if necessary.
     *
     * @return the url for the icon with the context path added if necessary. null will be returned if there
     * is no icon URL.
     */
    String getCompleteIconUrl();

    String getIconUrl();

    /**
     * Returns the HTML-escaped URL for this issue constant.
     *
     * @return a String containing an HTML-escaped icon URL
     * @see #getIconUrl()
     */
    String getIconUrlHtml();

    // Retrieve name translation in current locale
    String getNameTranslation();

    // Retrieve desc translation in current locale
    String getDescTranslation();

    // Retrieve name translation in specified locale
    String getNameTranslation(String locale);

    // Retrieve desc translation in specified locale
    String getDescTranslation(String locale);

    String getNameTranslation(I18nHelper i18n);

    String getDescTranslation(I18nHelper i18n);
}
