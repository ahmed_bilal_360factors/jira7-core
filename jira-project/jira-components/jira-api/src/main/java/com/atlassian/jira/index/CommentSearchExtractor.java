package com.atlassian.jira.index;

import com.atlassian.annotations.ExperimentalSpi;
import com.atlassian.jira.issue.comments.Comment;

/**
 * Interface for extractors adding fields based on comments
 *
 * @since 6.2
 */
@ExperimentalSpi
public interface CommentSearchExtractor extends EntitySearchExtractor<Comment> {

}
