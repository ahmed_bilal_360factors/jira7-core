package com.atlassian.jira.notification;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.notification.type.NotificationType;

/**
 * Notification to the configured email address.
 *
 * @since 7.0
 */
@PublicApi
public class EmailNotification extends AbstractNotification {
    private final String email;

    public EmailNotification(final Long id, final String email) {
        super(id, NotificationType.SINGLE_EMAIL_ADDRESS, email);
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public <X> X accept(final NotificationVisitor<X> notificationVisitor) {
        return notificationVisitor.visit(this);
    }
}
