package com.atlassian.jira.security;

/**
 * Represents a tri-state logical value for a project permission.
 * <p>
 * This enum allows us to find out project-wide permission information about an issue-level permission.
 * <ul>
 * <li>ALL_ISSUES : the user always has this permission for the given project</li>
 * <li>NO_ISSUES : the user definitely does not have the given permission for any issues in this project</li>
 * <li>ISSUE_SPECIFIC : the user may have this permission on some issues, but not others</li>
 * </ul>
 * </p>
 *
 * @since v6.4.8
 */
public enum ProjectWidePermission {
    ALL_ISSUES, NO_ISSUES, ISSUE_SPECIFIC
}