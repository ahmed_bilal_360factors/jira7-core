package com.atlassian.jira.project.template.hook;

import com.atlassian.annotations.PublicApi;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Optional;

/**
 * A config template used for Project Template configuration.
 *
 * @since 7.0
 */
@PublicApi
public interface ConfigTemplate {
    /**
     * Returns the associated workflow scheme template.
     *
     * @return the associated workflow scheme template
     */
    @Nonnull
    Optional<WorkflowSchemeTemplate> workflowSchemeTemplate();

    /**
     * Returns the associated issue type scheme template.
     *
     * @return the associated issue type scheme template
     */
    @Nonnull
    Optional<IssueTypeSchemeTemplate> issueTypeSchemeTemplate();

    /**
     * Returns the associated issue type screen scheme template.
     *
     * @return the associated issue type screen scheme template
     */
    @Nonnull
    Optional<IssueTypeScreenSchemeTemplate> issueTypeScreenSchemeTemplate();

    /**
     * Returns the collection of resolution templates.
     *
     * @return the collection of resolution templates
     */
    @Nonnull
    Collection<ResolutionTemplate> resolutionTemplates();
}
