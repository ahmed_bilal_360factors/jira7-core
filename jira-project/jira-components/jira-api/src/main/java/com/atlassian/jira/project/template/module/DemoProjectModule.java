package com.atlassian.jira.project.template.module;

import com.atlassian.annotations.ExperimentalApi;

import java.net.URL;
import java.util.Optional;

/**
 * @since v7.1
 */
@ExperimentalApi
public interface DemoProjectModule {
    String getKey();

    Integer getWeight();

    String getLabelKey();

    String getDescriptionKey();

    Optional<String> getLongDescriptionKey();

    Optional<String> getProjectTemplateKey();

    Optional<String> getProjectTypeKey();

    String getIconUrl();

    Optional<String> getBackgroundIconUrl();

    URL getImportFile();
}
