package com.atlassian.jira.jql.query;

import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.Set;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Default implementation of {@link QueryCreationContext}.
 *
 * @since v4.0
 */
public class QueryCreationContextImpl implements QueryCreationContext {
    private final ApplicationUser user;
    private final boolean securityOverriden;
    private final Set<String> determinedProjects;

    /**
     * Use this constructor unless you know you need to override security.
     *
     * @param user the user performing the search
     */
    public QueryCreationContextImpl(final ApplicationUser user) {
        this(user, false);
    }

    public QueryCreationContextImpl(final ApplicationUser user, final boolean securityOverriden) {
        this(user, securityOverriden, Collections.<String>emptySet());
    }

    public QueryCreationContextImpl(final QueryCreationContext queryCreationContext, final Set<String> determinedProjects) {
        this(queryCreationContext.getApplicationUser(), queryCreationContext.isSecurityOverriden(), determinedProjects);
    }

    /**
     * Use this constructor if you need to override security.
     *
     * @param user              the user performing the search
     * @param securityOverriden true if you want to override security; false otherwise
     */
    public QueryCreationContextImpl(final ApplicationUser user, final boolean securityOverriden, final Set<String> determinedProjects) {
        notNull(determinedProjects);
        this.user = user;
        this.securityOverriden = securityOverriden;
        this.determinedProjects = determinedProjects;
    }

    @Override
    public ApplicationUser getApplicationUser() {
        return user;
    }

    @Override
    public ApplicationUser getUser() {
        return user;
    }

    @Override
    public ApplicationUser getQueryUser() {
        return user;
    }

    @Override
    public boolean isSecurityOverriden() {
        return securityOverriden;
    }

    @Override
    @Nonnull
    public Set<String> getDeterminedProjects() {
        return determinedProjects;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final QueryCreationContextImpl that = (QueryCreationContextImpl) o;

        if (securityOverriden != that.securityOverriden) {
            return false;
        }
        if (!determinedProjects.equals(that.determinedProjects)) {
            return false;
        }
        if (user != null ? !user.equals(that.user) : that.user != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = user != null ? user.hashCode() : 0;
        result = 31 * result + (securityOverriden ? 1 : 0);
        result = 31 * result + determinedProjects.hashCode();
        return result;
    }
}
