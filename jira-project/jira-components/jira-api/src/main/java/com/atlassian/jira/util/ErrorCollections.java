package com.atlassian.jira.util;

import com.atlassian.annotations.ExperimentalApi;

/**
 * Static factory methods for creating simple {@link com.atlassian.jira.util.ErrorCollection} instances.
 * <p>
 * All instances returned by methods in this class are {@link java.io.Serializable}.
 * </p>
 *
 * @since v7.0
 */
@ExperimentalApi
public class ErrorCollections {
    /**
     * Creates a new instance of empty error collection.
     *
     * @return empty error collection
     */
    public static ErrorCollection empty() {
        return new SimpleErrorCollection();
    }

    /**
     * Creates a new instance of error collection that contains the specified message and reason.
     *
     * @param errorMessage error message to be contained in the returned collection
     * @param reason       reason for the message
     * @return a new error collection with the specified message and reason
     */
    public static ErrorCollection create(String errorMessage, ErrorCollection.Reason reason) {
        ErrorCollection result = empty();
        result.addErrorMessage(errorMessage, reason);
        return result;
    }

    /**
     * Creates a new instance of error collection that contains the {@code message} related to the {@code field} because of {@code reason}.
     *
     * @param field   field that the error message will pertain to
     * @param message error message related to the field
     * @param reason  reason for error
     * @return a new error collection with one message relating to the field
     */
    public static ErrorCollection create(String field, String message, ErrorCollection.Reason reason) {
        ErrorCollection result = empty();
        result.addError(field, message, reason);
        return result;
    }

    /**
     * Creates a new instance of error collection that indicates there was a validation error for the specified field.
     * <p>
     * This is equivalent to calling {@link #create(String, String, com.atlassian.jira.util.ErrorCollection.Reason)}
     * with the same field and message arguments and reason {@link com.atlassian.jira.util.ErrorCollection.Reason#VALIDATION_FAILED}.
     * </p>
     *
     * @param field   field that failed validation
     * @param message detailed error message
     * @return a new error collection with validation reason set to {@link com.atlassian.jira.util.ErrorCollection.Reason#VALIDATION_FAILED}
     */
    public static ErrorCollection validationError(String field, String message) {
        return create(field, message, ErrorCollection.Reason.VALIDATION_FAILED);
    }

    /**
     * Creates a new error collection with contents copied from the supplied argument.
     *
     * @param errorCollection error collection to copy
     * @return a new instance of error collection with the same contents as argument
     */
    public static ErrorCollection copyOf(ErrorCollection errorCollection) {
        ErrorCollection result = empty();
        result.addErrorCollection(errorCollection);
        return result;
    }

    /**
     * Creates an error collection containing all errors from given error collections
     *
     * @param errorCollections an array of error collections to join
     * @return error collection containing all errors from given error collections
     */
    public static ErrorCollection join(ErrorCollection... errorCollections) {
        ErrorCollection errorCollection = new SimpleErrorCollection();
        for (ErrorCollection collection : errorCollections) {
            errorCollection.addErrorCollection(collection);
        }
        return errorCollection;
    }
}
