package com.atlassian.jira.plugin.webfragment.conditions;


import com.atlassian.annotations.PublicSpi;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.PluginParseException;

import java.util.Map;
import java.util.Optional;

/**
 * Convenient base class for conditions which require a project permission key.
 * The permission key is read from the {@code permission} parameter.
 *
 * <p>
 *     Note that this class supports legacy permission aliases
 *     as well as proper keys from {@link ProjectPermissions}.
 *
 *     Custom permission keys are also supported.
 * </p>
 */
@PublicSpi
public abstract class AbstractProjectPermissionCondition extends AbstractWebCondition {

    private ProjectPermissionKey permissionKey;

    public final void init(Map<String, String> params) throws PluginParseException {
        String providedKey = params.get("permission");
        if (providedKey == null) {
            throw new PluginParseException("permission parameter is required");
        }
        permissionKey = getPermissionKey(providedKey);
        super.init(params);
    }

    @Override
    public final boolean shouldDisplay(Map<String, Object> context) {
        return super.shouldDisplay(context);
    }

    @Override
    public final boolean shouldDisplay(ApplicationUser user, JiraHelper jiraHelper) {
        return shouldDisplay(user, jiraHelper, permissionKey);
    }

    protected abstract boolean shouldDisplay(ApplicationUser user, JiraHelper jiraHelper, ProjectPermissionKey permissionKey);

    private ProjectPermissionKey getPermissionKey(String providedKey) {
        return legacyPermissionKey(providedKey).orElse(new ProjectPermissionKey(providedKey));
    }

    private Optional<ProjectPermissionKey> legacyPermissionKey(String providedKey) {
        return Optional.ofNullable(ProjectPermissions.systemProjectPermissionKeyByShortName(providedKey));
    }
}
