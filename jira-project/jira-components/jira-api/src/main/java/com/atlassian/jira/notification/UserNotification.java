package com.atlassian.jira.notification;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.notification.type.NotificationType;
import com.atlassian.jira.user.ApplicationUser;

/**
 * Notification to the configured user.
 *
 * @since 7.0
 */
@PublicApi
public class UserNotification extends AbstractNotification {
    private final ApplicationUser applicationUser;

    public UserNotification(final Long id, final NotificationType notificationType, final ApplicationUser applicationUser, final String parameter) {
        super(id, notificationType, parameter);
        this.applicationUser = applicationUser;
    }

    public ApplicationUser getApplicationUser() {
        return applicationUser;
    }

    @Override
    public <X> X accept(final NotificationVisitor<X> notificationVisitor) {
        return notificationVisitor.visit(this);
    }
}
