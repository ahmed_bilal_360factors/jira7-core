package com.atlassian.jira.issue.fields.rest.json.dto;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.issue.attachment.Attachment;

import java.util.Collection;
import java.util.List;

/**
 * @since v6.5
 */
@PublicApi
public interface AttachmentViewDtoConverter {
    public Collection<AttachmentViewJsonDto> convert(List<Attachment> attachments);
}
