package com.atlassian.jira.permission;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.entity.WithId;
import com.atlassian.jira.util.NamedWithDescription;

import javax.annotation.Nonnull;
import java.util.Collection;

/**
 * Permission scheme is a set of rules that govern permissions for
 * different activities within a project.
 * <p>
 * Permission scheme consists of a list of {@link PermissionGrant}s.
 * Each scheme has a unique name and an optional description.
 * </p>
 * <p>
 * Implementations of this interface are required to be immutable.
 * </p>
 */
@PublicApi
public interface PermissionScheme extends WithId, NamedWithDescription {
    /**
     * Returns an id of the permission grant as stored in DB.
     */
    @Nonnull
    Long getId();

    /**
     * Returns the permission scheme name.
     */
    @Nonnull
    String getName();

    /**
     * Returns an optional description of this scheme. If description
     * is not defined then an empty String will be returned.
     *
     * @return non-null String, may be empty.
     */
    @Nonnull
    String getDescription();

    /**
     * Returns a collection of permission grants defined in this scheme.
     */
    @Nonnull
    Collection<PermissionGrant> getPermissions();
}
