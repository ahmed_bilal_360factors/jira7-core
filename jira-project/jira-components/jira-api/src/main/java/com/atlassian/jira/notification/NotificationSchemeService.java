package com.atlassian.jira.notification;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.Page;
import com.atlassian.jira.util.PageRequest;

import javax.annotation.Nonnull;

/**
 * Service providing information about notification schemes.
 *
 * @since 7.0
 */
@PublicApi
public interface NotificationSchemeService {
    /**
     * Returns the notification scheme for given id. This method executes a permission check, in order to have permissions
     * to retrieve the notification scheme, the user is required to have permissions for administering at least one
     * project associated with the notification scheme.
     *
     * @param user user who requests the access to the notification scheme
     * @param id   id of the notification scheme to retrieve
     * @return information about the notification scheme.
     */
    ServiceOutcome<NotificationScheme> getNotificationScheme(ApplicationUser user, Long id);

    /**
     * Returns a page with notification schemes for the given {@link com.atlassian.jira.util.PageRequest} ordered by name. This method
     * executes a permission check. User is required to have administer permissions on projects associated with
     * the scheme in order to see it.
     *
     * @param user        user who requests access to notification schemes.
     * @param pageRequest information about the desired number of results and theirs offset.
     * @return the page with notification schemes. The page may be empty.
     */
    Page<NotificationScheme> getNotificationSchemes(ApplicationUser user, @Nonnull PageRequest pageRequest);

    /**
     * Returns a notification scheme for the given project identified by the project's id.
     *
     * @param user      user who requests the access to the notification scheme
     * @param projectId id of the project for which the associated notification scheme will be returned
     * @return information about the notification scheme.
     * @see #getNotificationScheme(ApplicationUser, Long)
     * @see #getNotificationSchemeForProject(ApplicationUser, String)
     */
    ServiceOutcome<NotificationScheme> getNotificationSchemeForProject(ApplicationUser user, Long projectId);

    /**
     * Returns a notification scheme for the given project identified by the project's key.
     *
     * @param user       user who requests the access to the notification scheme
     * @param projectKey key of the project for which the associated notification scheme will be returned
     * @return information about the notification scheme.
     * @see #getNotificationScheme(ApplicationUser, Long)
     * @see #getNotificationSchemeForProject(ApplicationUser, Long)
     */
    ServiceOutcome<NotificationScheme> getNotificationSchemeForProject(ApplicationUser user, String projectKey);
}
