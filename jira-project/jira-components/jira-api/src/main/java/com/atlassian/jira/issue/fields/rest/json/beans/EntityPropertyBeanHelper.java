package com.atlassian.jira.issue.fields.rest.json.beans;

import com.atlassian.annotations.Internal;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.lang.String.format;

/**
 * This allows you to create EntityProperty beans given a key and an Java object
 * and also transform them into other forms
 *
 * @since 7.1.0
 */
@Internal
public class EntityPropertyBeanHelper {

    /**
     * Creates an EntityPropertyBean by serialising object into JSON for it
     *
     * @param key            the key of the entity property
     * @param jsonJavaObject the java object to JSON serialize
     * @param selfLink       a self link to this property
     * @return a new EntityPropertyBean
     */
    public static EntityPropertyBean createFrom(String key, Object jsonJavaObject, String selfLink) {
        return new EntityPropertyBean(key, jsonSerialize(jsonJavaObject), selfLink);
    }

    /**
     * EntityPropertyBean s are represented in our Jackson beans as a List of Maps (aka JSON objects)
     * and we need to get them from that format and into serialised JSON string format
     *
     * @param jacksonEnteredProperties the wire serialised entity property values
     * @return a list of EntityPropertyBean's with the json value serialised or null if the inputs null
     */
    public static
    @Nullable
    List<EntityPropertyBean> createFrom(final List<Map<String, Object>> jacksonEnteredProperties) {
        List<EntityPropertyBean> entityPropertyBeans = null;
        if (jacksonEnteredProperties != null) {
            Function<Map<String, Object>, EntityPropertyBean> mapper = entityPropertyBean -> {
                // on the wire, portions of the json map under the key "key" qw Strings and portions
                // under the key "value" as Java objects, and we cast as appropriate
                final String key = (String) entityPropertyBean.get("key");
                final Object value = entityPropertyBean.get("value");
                return createFrom(key, value, null);
            };
            entityPropertyBeans = Lists.newArrayList(jacksonEnteredProperties.stream().map(mapper::apply).collect(Collectors.toList()));
        }
        return entityPropertyBeans;
    }

    /**
     * Turns a list of EntityPropertyBean into a Map of String -> Jackson JsonNode
     *
     * @param properties the list of EntityPropertyBean's
     * @return a Map of String -> Jackson JsonNode
     */
    public static Map<String, JsonNode> propertiesAsMap(final List<EntityPropertyBean> properties) {
        final Map<String, JsonNode> mapOfProperties = Maps.newHashMap();
        if (properties != null) {
            for (EntityPropertyBean propertyBean : properties) {
                try {
                    String jsonString = propertyBean.getValue();
                    JsonNode jsonNode = objectMapper().readTree(jsonString);

                    mapOfProperties.put(propertyBean.getKey(), jsonNode);
                } catch (IOException e) {
                    // this really should not happen since Jersey has converted JSON into a raw JSON value object
                    // and then we have to make that into a JSONObject
                    throw new RuntimeException(e);
                }
            }
        }
        return mapOfProperties;
    }

    private static String jsonSerialize(Object value) {
        // value can be any one of the JSON types, array / object / string / number / boolean etc..
        // so we use Jackson again to get a JSON serialised view of it
        try {
            ObjectMapper objectMapper = objectMapper();
            return objectMapper.writeValueAsString(value);
        } catch (IOException e) {
            // how can this happen.  it was valid JSON in so how can it not be valid JSON out
            throw new IllegalStateException(format("Unable to JSON serialize the following object '%s'", String.valueOf(value)), e);
        }
    }

    private static ObjectMapper objectMapper() {
        return new ObjectMapper();
    }
}
