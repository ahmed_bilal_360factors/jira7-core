package com.atlassian.jira.event.project.property;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.entity.property.EntityProperty;
import com.atlassian.jira.event.entity.AbstractPropertyEvent;
import com.atlassian.jira.event.entity.EntityPropertySetEvent;
import com.atlassian.jira.user.ApplicationUser;

/**
 * Event indicating project property value has been set.
 *
 * @since v6.2
 */
@PublicApi
@EventName("property.set.project")
public class ProjectPropertySetEvent extends AbstractPropertyEvent implements EntityPropertySetEvent {
    public ProjectPropertySetEvent(final EntityProperty entityProperty, final ApplicationUser user) {
        super(entityProperty, user);
    }
}
