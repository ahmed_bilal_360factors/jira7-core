package com.atlassian.jira.util;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.util.JiraDurationUtils.DurationFormatter;

/**
 * Provider for {@link DurationFormatter}.
 *
 * @since v7.2
 */
@PublicApi
public interface DurationFormatterProvider {
    /**
     * @return the duration formatter.
     */
    DurationFormatter getFormatter();
}
