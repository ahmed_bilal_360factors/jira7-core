package com.atlassian.jira.plugin.webfragment.conditions;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.atlassian.annotations.PublicApi;
import com.atlassian.fugue.Option;
import com.atlassian.fugue.Suppliers;
import com.atlassian.jira.entity.property.EntityProperty;
import com.atlassian.jira.entity.property.EntityPropertyConditionHelper;
import com.atlassian.jira.entity.property.EntityPropertyService;
import com.atlassian.jira.plugin.webfragment.JiraWebContext;
import com.atlassian.jira.plugin.webfragment.JiraWebInterfaceManager;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginParseException;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSyntaxException;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Condition that checks for equality between the expected value and a property value for a given entity in the current context.
 * <p>
 * The condition performs an equals comparison on a property value of a primitive type with an expected value.
 * For complex types the condition always evaluates to false.
 * Required parameters:
 * <ul>
 *  <li>entity - The type of entity that should be checked for comparison: issue, project, user, etc</li>
 *  <li>property key - The key of the property that should be checked for this entity.</li>
 *  <li>value - The JSON string representation that the entity property value will be checked against.</li>
 * </ul>
 * </p>
 * <p>You may also pass in an 'objectName' parameter which is the '.' separated path to the value you wish to compare
 * against in a large json object. For example, if you had the following value stored in an entity property:</p>
 * <pre>
 *     {
 *         "some": {
 *             "nested": {
 *                 "structure": true
 *             }
 *         },
 *         "unimportant": "data"
 *     }
 * </pre>
 * <p>If the 'objectName' was set to be 'some.nested.structure' then the 'value' object could be set to 'true' and this condition
 * would be true; resulting in the web fragment being shown. Critically, the 'objectName' parameter will ignore all other data in the JSON stored
 * value. In our example the 'unimportant' JSON field was ignored.</p>
 * <p>It is important to note that json fields that contain a '.' in their key cannot be referenced using 'objectName'. The 'objectName' parameter
 * expects that the '.' characters are path separators. For example, the following objectName 'one.two' would refer to the json structure:</p>
 * <pre>
 *     {
 *         "one": {
 *             "two": true
 *         }
 *     }
 * </pre>
 * <p>But it would not work for the following JSON structure:</p>
 * <pre>
 *     {
 *         "one.two": true
 *     }
 * </pre>
 * <p>The first example will be found with 'objectName' whereas the second example will not.</p>
 * <p>
 * The given entity must available in the JiraHelper context params.
 * </p>
 *
 * @since 7.0
 */
@PublicApi
public class EntityPropertyEqualToCondition extends AbstractWebCondition {
    private static final Logger log = Logger.getLogger(EntityPropertyEqualToCondition.class.getName());
    private static final JsonParser jsonParser = new JsonParser();

    private final PluginAccessor pluginAccessor;

    private ConditionParameters conditionParameters;

    public EntityPropertyEqualToCondition(PluginAccessor pluginAccessor) {
        this.pluginAccessor = pluginAccessor;
    }

    @Override
    public void init(final Map<String, String> params) throws PluginParseException {
        super.init(params);

        // Required params
        String entity = getNonNullValueFromParameter(params, "entity");
        String propertyKey = getNonNullValueFromParameter(params, "propertyKey");
        String expectedValue = getNonNullValueFromParameter(params, "value");

        // Optional params
        String objectName = StringUtils.defaultString(params.get("objectName"));

        // Validation
        validateObjectNameHasCorrectSyntax(objectName);
        final JsonElement jsonExpectedValue = validateValueIsJson(expectedValue);

        conditionParameters = new ConditionParameters(entity, propertyKey, jsonExpectedValue, objectName);
    }

    private String getNonNullValueFromParameter(final Map<String, String> params, final String parameter) {
        String value = params.get(parameter);
        if (value == null) {
            throw new PluginParseException(String.format("No '%s' parameter specified for condition: %s", parameter, this.getClass().getName()));
        }
        return value;
    }

    private static JsonElement validateValueIsJson(final String expectedValue) {
        final JsonElement jsonExpectedValue;
        try {
            jsonExpectedValue = jsonParser.parse(expectedValue);
        } catch(JsonSyntaxException e) {
            throw new PluginParseException("Could not parse the value provided", e);
        }
        return jsonExpectedValue;
    }

    private void validateObjectNameHasCorrectSyntax(final String objectName) {
        if(StringUtils.isNotBlank(objectName)) {
            final Optional<String> potentialError = validateObjectName(objectName);
            if(potentialError.isPresent()) {
                throw new PluginParseException("Failed to read the objectName property: " + potentialError.get() + " (" + objectName + ")");
            }
        }
    }

    static Optional<String> validateObjectName(String objectName) {
        if(objectName.contains("..")) return Optional.of("Two '.' characters in a row is not valid in an objectName");

        return Optional.empty();
    }

    @Override
    public boolean shouldDisplay(final Map<String, Object> context) {
        final Map<String, EntityPropertyConditionHelper> entityConditions = getEntityConditions();
        if (entityConditions.containsKey(conditionParameters.getEntity())) {
            EntityPropertyConditionHelper condition = entityConditions.get(conditionParameters.getEntity());

            final JiraWebContext jiraWebContext = JiraWebContext.from(context);
            Optional<Long> entityId = condition.getEntityId(jiraWebContext);
            if (!entityId.isPresent()) {
                return false;
            }
            EntityPropertyService.PropertyResult propertyResult = condition.getProperty(jiraWebContext.getUser().orElse(null), entityId.get(), conditionParameters.getPropertyKey());
            if (!propertyResult.isValid()) {
                return false;
            }
            Option<EntityProperty> entityProperty = propertyResult.getEntityProperty();
            return entityProperty.fold(Suppliers.alwaysFalse(), ep -> {
                // Then parse the actual value stored against this entity
                JsonElement actualValue = jsonParser.parse(ep.getValue());

                // Find the nested value that we should compare against
                if (StringUtils.isNotBlank(conditionParameters.getObjectName())) {
                    final Option<JsonElement> potentialValue = getValueForPath(actualValue, conditionParameters.getObjectName());
                    if (potentialValue.isEmpty()) return false;
                    actualValue = potentialValue.get();
                }

                final JsonElement expectedValue = conditionParameters.getExpectedValue();
                if (actualValue.equals(expectedValue)) {
                    return true;
                }

                // All of the code below this point in this method should be deleted by JDEV-36267
                // In the past this condition worked purely on string comparison and therefore boolean values and numerical values
                // would successfully compare against their string equivalents.
                if(StringUtils.isNotBlank(conditionParameters.getObjectName())) return false;
                if (!expectedValue.isJsonPrimitive() || !actualValue.isJsonPrimitive()) return false;
                final JsonPrimitive actualValuePrimitive = actualValue.getAsJsonPrimitive();
                final boolean stringComparisonEqual = actualValuePrimitive.isString() && expectedValue.getAsString().equals(actualValuePrimitive.getAsString());
                if (stringComparisonEqual) {
                    final String location = jiraWebContext.getLocation().orElse("NO_LOCATION_AVAILABLE");
                    log.warn("Deprecation Warning: EntityPropertyEqualToCondition was not equivalent by JSON comparison but was by String comparison. "
                            + "Please ensure that the entity property values and the condition 'value' are of the same json type. "
                            + "Type coercion has made the actual value: '" + ep.getValue() + "' be equivalent to the expected value"
                            + "'" + conditionParameters.getExpectedValue() + "'. Please update the expected 'value' in the entity property condition"
                            + "to match the actual value. This web fragment has the location: " + location);
                }
                return stringComparisonEqual;
            });

        } else {
            log.warn("Entity property condition is registered for invalid entity. Supported entities are " + entityConditions.keySet());
            return false;
        }
    }

    // This method only exists to prevent an API breakage. In the future change this class to implement Condition instead
    // of extending AbstractWebCondition.
    @Override
    public boolean shouldDisplay(final ApplicationUser applicationUser, final JiraHelper jiraHelper) {
        final HashMap<String, Object> context = Maps.newHashMap();
        context.putAll(jiraHelper.getContextParams());
        context.put(JiraWebInterfaceManager.CONTEXT_KEY_USER, applicationUser);
        return shouldDisplay(context);
    }

    @VisibleForTesting
    static Option<JsonElement> getValueForPath(final JsonElement jsonEntityProperty, final String path) {
        Preconditions.checkNotNull(path, "The path passed into this method may not be null");

        final String[] split = StringUtils.split(path, '.');
        JsonElement value = jsonEntityProperty;
        for (final String currentKey : split) {
            if (value == null || !value.isJsonObject()) {
                return Option.none();
            } else {
                value = value.getAsJsonObject().get(currentKey);
            }
        }
        return Option.option(value);
    }

    private Map<String, EntityPropertyConditionHelper> getEntityConditions() {
        return pluginAccessor.getEnabledModulesByClass(EntityPropertyConditionHelper.class)
                .stream()
                .collect(Collectors.toMap(EntityPropertyConditionHelper::getEntityName, Function.identity()));
    }

    private static final class ConditionParameters {
        private final String entity;
        private final String propertyKey;
        private final JsonElement expectedValue;
        private final String objectName;

        private ConditionParameters(String entity, String propertyKey, JsonElement expectedValue, final String objectName) {
            this.entity = entity;
            this.propertyKey = propertyKey;
            this.expectedValue = expectedValue;
            this.objectName = objectName;
        }

        public String getEntity() {
            return entity;
        }

        public JsonElement getExpectedValue() {
            return expectedValue;
        }

        public String getPropertyKey() {
            return propertyKey;
        }

        public String getObjectName() {
            return objectName;
        }
    }
}
