package com.atlassian.jira.event.issue;

import com.atlassian.annotations.PublicSpi;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.event.type.EventTypeManager;
import com.atlassian.jira.util.I18nHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Subclasses of IssueEventListener can now override relevant functions in the interface as opposed to having to provide
 * a dummy implementation for all methods.
 */
@PublicSpi
public abstract class AbstractIssueEventListener implements IssueEventListener {
    private static final Logger log = LoggerFactory.getLogger(AbstractIssueEventListener.class);

    public void issueCreated(final IssueEvent event) {
        handleDefaultIssueEvent(event);
    }

    public void issueUpdated(final IssueEvent event) {
        handleDefaultIssueEvent(event);
    }

    public void issueAssigned(final IssueEvent event) {
        handleDefaultIssueEvent(event);
    }

    public void issueResolved(final IssueEvent event) {
        handleDefaultIssueEvent(event);
    }

    public void issueClosed(final IssueEvent event) {
        handleDefaultIssueEvent(event);
    }

    public void issueCommented(final IssueEvent event) {
        handleDefaultIssueEvent(event);
    }

    public void issueCommentEdited(final IssueEvent event) {
        handleDefaultIssueEvent(event);
    }

    /**
     * The default behaviour for this method calls {@link #issueUpdated(IssueEvent event)}.
     * This preserves the behaviour of JIRA prior to v5.2
     *
     * @since v5.2
     */
    public void issueCommentDeleted(final IssueEvent event) {
        issueUpdated(event);
    }

    public void issueWorklogUpdated(final IssueEvent event) {
        handleDefaultIssueEvent(event);
    }

    public void issueWorklogDeleted(final IssueEvent event) {
        handleDefaultIssueEvent(event);
    }

    public void issueReopened(final IssueEvent event) {
        handleDefaultIssueEvent(event);
    }

    public void issueDeleted(final IssueEvent event) {
        handleDefaultIssueEvent(event);
    }

    public void issueWorkLogged(final IssueEvent event) {
        handleDefaultIssueEvent(event);
    }

    public void issueStarted(final IssueEvent event) {
        handleDefaultIssueEvent(event);
    }

    public void issueStopped(final IssueEvent event) {
        handleDefaultIssueEvent(event);
    }

    public void issueMoved(final IssueEvent event) {
        handleDefaultIssueEvent(event);
    }

    public void issueGenericEvent(final IssueEvent event) {
        handleDefaultIssueEvent(event);
    }

    /**
     * If you need to treat all the issue events equally, override this method and add logic to handle events.
     * All legacy event handler methods (unless overridden) in this class call this method.
     *
     * @param event The received event
     */
    protected void handleDefaultIssueEvent(final IssueEvent event) {
        // do nothing
    }

    /**
     * Determines how the event should be processed. Based on the event type ID within the event, the appropriate
     * actions are called.
     * <p>
     * An event with an unknown event type ID is logged and discarded.
     * <p>
     * The customEvent method should be implemented to deal with any custom events that are added to the system
     *
     * @param event - the IssueEvent object containing the event type ID
     */
    public void workflowEvent(final IssueEvent event) {
        final EventTypeManager eventTypeManager = ComponentAccessor.getEventTypeManager();

        final Long eventTypeId = event.getEventTypeId();
        final EventType eventType = eventTypeManager.getEventType(eventTypeId);

        if (eventType == null) {
            log.error("Issue Event Type with ID '" + eventTypeId + "' is not recognised.");
        } else {

            event.accept(new IssueEventVisitor<Void>() {
                @Override
                public Void onIssueCreated(IssueEvent e) {
                    issueCreated(event);
                    return null;
                }

                @Override
                public Void onIssueUpdated(IssueEvent e) {
                    issueUpdated(event);
                    return null;
                }

                @Override
                public Void onIssueAssigned(IssueEvent e) {
                    issueAssigned(event);
                    return null;
                }

                @Override
                public Void onIssueResolved(IssueEvent e) {
                    issueResolved(event);
                    return null;
                }

                @Override
                public Void onIssueCommented(IssueEvent e) {
                    issueCommented(event);
                    return null;
                }

                @Override
                public Void onIssueCommentEdited(IssueEvent e) {
                    issueCommentEdited(event);
                    return null;
                }

                @Override
                public Void onIssueCommentDeleted(IssueEvent e) {
                    issueCommentDeleted(event);
                    return null;
                }

                @Override
                public Void onIssueClosed(IssueEvent e) {
                    issueClosed(event);
                    return null;
                }

                @Override
                public Void onIssueReopened(IssueEvent e) {
                    issueReopened(event);
                    return null;
                }

                @Override
                public Void onIssueDeleted(IssueEvent e) {
                    issueDeleted(event);
                    return null;
                }

                @Override
                public Void onIssueMoved(IssueEvent e) {
                    issueMoved(event);
                    return null;
                }

                @Override
                public Void onIssueWorkLogged(IssueEvent e) {
                    issueWorkLogged(event);
                    return null;
                }

                @Override
                public Void onIssueWorkStarted(IssueEvent e) {
                    issueStarted(event);
                    return null;
                }

                @Override
                public Void onIssueWorkStopped(IssueEvent e) {
                    issueStopped(event);
                    return null;
                }

                @Override
                public Void onIssueWorklogUpdated(IssueEvent e) {
                    issueWorklogUpdated(event);
                    return null;
                }

                @Override
                public Void onIssueWorklogDeleted(IssueEvent e) {
                    issueWorklogDeleted(event);
                    return null;
                }

                @Override
                public Void onIssueGenericEvent(IssueEvent e) {
                    issueGenericEvent(event);
                    return null;
                }

                @Override
                public Void onCustomEvent(IssueEvent e) {
                    customEvent(event);
                    return null;
                }
            });
        }
    }

    /**
     * Implement this method to deal with any custom events within the system
     *
     * @param event IssueEvent
     */
    public void customEvent(final IssueEvent event) {
        handleDefaultIssueEvent(event);
    }

    public void init(final Map params) {
    }

    public String[] getAcceptedParams() {
        return new String[0];
    }

    public boolean isInternal() {
        return false;
    }

    public boolean isUnique() {
        return false;
    }

    public String getDescription() {
        return null;
    }

    protected I18nHelper getI18NBean() {
        return ComponentAccessor.getJiraAuthenticationContext().getI18nHelper();
    }
}
