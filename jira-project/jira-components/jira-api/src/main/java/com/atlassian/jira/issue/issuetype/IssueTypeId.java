package com.atlassian.jira.issue.issuetype;

import com.atlassian.annotations.PublicApi;

import java.util.Objects;

/**
 * Represents the id for an IssueType
 *
 * @since 7.1
 */
@PublicApi
public final class IssueTypeId {
    private final String id;

    public IssueTypeId(final String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IssueTypeId that = (IssueTypeId) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "IssueTypeId{" +
                "id='" + id + '\'' +
                '}';
    }
}
