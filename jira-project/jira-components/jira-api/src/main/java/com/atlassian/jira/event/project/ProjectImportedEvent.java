package com.atlassian.jira.event.project;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.jira.event.AbstractEvent;
import com.atlassian.jira.imports.project.core.ProjectImportResults;

/**
 * Published when a project is imported
 */
@ExperimentalApi
@EventName("jira.project.imported")
public final class ProjectImportedEvent extends AbstractEvent {
    private final int issuesCreatedCount;
    private final int attachmentsCreatedCount;
    private final int usersCreatedCount;
    private final long importDurationMillis;

    public ProjectImportedEvent(final ProjectImportResults projectImportResults) {
        this.issuesCreatedCount = projectImportResults.getIssuesCreatedCount();
        this.attachmentsCreatedCount = projectImportResults.getAttachmentsCreatedCount();
        this.usersCreatedCount = projectImportResults.getUsersCreatedCount();
        this.importDurationMillis = projectImportResults.getImportDuration();
    }

    public int getIssuesCreatedCount() {
        return issuesCreatedCount;
    }

    public int getAttachmentsCreatedCount() {
        return attachmentsCreatedCount;
    }

    public int getUsersCreatedCount() {
        return usersCreatedCount;
    }


    public long getImportDurationMillis() {
        return importDurationMillis;
    }
}
