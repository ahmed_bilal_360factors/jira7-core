package com.atlassian.jira.user;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.issue.issuetype.IssueTypeId;

import javax.annotation.Nullable;
import java.util.Optional;

/**
 * Manager to store and retrieve the last used issue type and subtask issue type for a particular user.
 *
 * This can be used to pre-populate the issue type choice for a user when creating issues.
 *
 * Implementations will handle anonymous users gracefully.
 *
 * @since v7.1
 */
@PublicApi
public interface UserIssueTypeManager {
    /**
     * Sets the last used parent issue type id for the provided user.  Anonymous (null) users will simply be ignored.
     *
     * @param user                The user to store the last used issue type for
     * @param lastUsedIssueTypeId A valid issue type id
     */
    void setLastUsedIssueTypeId(@Nullable final ApplicationUser user, final IssueTypeId lastUsedIssueTypeId);

    /**
     * Sets the last used subtask issue type id for the provided user.  Anonymous (null) users will simply be ignored.
     *
     * @param user                       The user to store the last used issue type for
     * @param lastUsedSubtaskIssueTypeId A valid subtask issue type id
     */
    void setLastUsedSubtaskIssueTypeId(@Nullable final ApplicationUser user, final IssueTypeId lastUsedSubtaskIssueTypeId);

    /**
     * Retrieves the last used parent issue type id used by the provided user.  Maybe empty if the user is anonymous (null) or
     * if no issue type was previously stored.
     *
     * @param user The currently logged in user
     * @return Last used issue type id or empty if none exists
     */
    Optional<IssueTypeId> getLastUsedIssueTypeId(@Nullable final ApplicationUser user);

    /**
     * Retrieves the last used subtask issue type id used by the provided user.  Maybe empty if the user is anonymous (null) or
     * if no subtask issue type was previously stored.
     *
     * @param user The currently logged in user
     * @return Last used subtask issue type id or empty if none exists
     */
    Optional<IssueTypeId> getLastUsedSubtaskIssueTypeId(@Nullable final ApplicationUser user);
}
