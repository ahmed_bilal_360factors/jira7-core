package com.atlassian.jira.tenancy;

import com.atlassian.annotations.Internal;

/**
 * An expression of how a multi-tenanted environment affects the behaviour of the annotated component.
 *
 * @since v7.1.0
 * @deprecated Use {@link com.atlassian.annotations.tenancy.TenancyScope} instead. Since v7.2.0.
 */
@Internal
@Deprecated
public enum TenantAware {
    /**
     * Indicates that the component uses data access patterns that require knowledge of the current tenant.
     * <p>
     * This implies that the component not must be used without first establishing a tenanted request context.
     * </p>
     */
    TENANTED,

    /**
     * Indicates that the component only accesses data that is universal across all tenants.
     * <p>
     * Tenantless components may be accessed safely even outside of a tenanted request context and must avoid
     * access to any tenant-specific information.  Calling {@link #TENANTED} components from {@code TENANTLESS}
     * ones is generally an error, since if this service calls another service that requires a tenant, then
     * this service transitively requires one as well.
     * </p>
     */
    TENANTLESS,

    /**
     * Explicitly marks that this is a component whose safety in a multi-tenanted environment has not yet
     * been evaluated or that is known to require additional work.
     * <p>
     * This is intended to be used temporarily to help track remaining multi-tenancy work.  It should not
     * be assigned in new code unless properly resolving the problem is blocked by some external dependency.
     * </p>
     */
    UNRESOLVED;
}