package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.google.common.collect.ImmutableSet;

import javax.annotation.Nonnull;
import java.util.Set;

/**
 * Event fired when one or more JIRA Applications have been configured.
 *
 * @since v7.0
 */
public class ApplicationConfigurationEvent {
    private final Set<ApplicationKey> applicationsConfigured;

    public ApplicationConfigurationEvent(final Set<ApplicationKey> applicationsConfigured) {
        this.applicationsConfigured = ImmutableSet.copyOf(applicationsConfigured);
    }

    /**
     * Get all they keys of the applications that had their configuration changed.
     *
     * @return the keys of the applications that had their configuration changed.
     */
    @Nonnull
    public Set<ApplicationKey> getApplicationsConfigured() {
        return applicationsConfigured;
    }
}