package com.atlassian.jira.issue.fields.rest.json.beans;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

/**
 * Bean representing a list of issue security schemes.
 *
 * @since 7.0
 */
public class SecuritySchemesJsonBean {
    @JsonProperty
    private List<SecuritySchemeJsonBean> issueSecuritySchemes;

    public static SecuritySchemesJsonBean fromList(List<SecuritySchemeJsonBean> beans) {
        SecuritySchemesJsonBean securitySchemesJsonBean = new SecuritySchemesJsonBean();
        securitySchemesJsonBean.issueSecuritySchemes = beans;
        return securitySchemesJsonBean;
    }
}
