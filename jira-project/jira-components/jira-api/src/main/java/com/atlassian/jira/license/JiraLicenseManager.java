package com.atlassian.jira.license;

import com.atlassian.annotations.Internal;
import com.atlassian.application.api.ApplicationKey;
import com.atlassian.extras.api.LicenseException;
import com.atlassian.fugue.Option;

import javax.annotation.Nonnull;
import java.util.Set;
import java.util.SortedSet;
import java.util.function.Consumer;

/**
 * This manager is the primary interface to performing license-related read/write operations, and to reading the
 * license state of {@link com.atlassian.application.api.ApplicationKey applications}.
 *
 * @see com.atlassian.jira.bc.license.JiraLicenseService
 * @since 4.0
 */
public interface JiraLicenseManager {
    /**
     * Gets the server ID of the JIRA instance, creates it if it doesn't already exists.
     *
     * @return the server ID for this JIRA instance.
     */
    String getServerId();

    /**
     * Returns the {@link LicenseDetails} corresponding to the given license string after decoding, or throws an
     * exception if the license string is invalid or cannot be decoded.
     *
     * @param licenseString the license string.
     * @return the {@link LicenseDetails} for license encoded by the given string.
     * @throws LicenseException if the stored license string cannot be decoded
     * @see #isDecodeable(String)
     * @see org.apache.commons.lang.StringUtils#isBlank(String)
     */
    @Nonnull
    LicenseDetails getLicense(@Nonnull String licenseString);

    /**
     * Returns {@code true} if the given application is licensed.
     *
     * @param key the application
     * @return {@code true} if the given application is licensed.
     * @since 7.0
     */
    boolean isLicensed(@Nonnull ApplicationKey key);

    /**
     * This returns true if the provided licence string can be decoded into a valid licence
     *
     * @param licenseString the license string
     * @return true if it is can be decoded and false otherwise
     */
    boolean isDecodeable(String licenseString);

    /**
     * Sets the current license of this instance.
     * <p>
     * Note that this method will fire a {@link LicenseChangedEvent}.
     *
     * @param licenseString the license string
     * @return the JIRA license of this instance, this shouldn't be null if the {@code license} is valid.
     */
    LicenseDetails setLicense(String licenseString);

    /**
     * Sets the current license of this instance.
     * <p>
     * This is a special version of {@link #setLicense(String)} that will not fire any event and is purely for use
     * during a Data Import.
     *
     * @param licenseString the license string
     * @return the JIRA license of this instance, this shouldn't be null if the {@code license} is valid.
     */
    LicenseDetails setLicenseNoEvent(String licenseString);

    /**
     * Records that the user has acknowledged that one or more of their licenses is out of maintenance. This triggers
     * JIRA to grant a 30 day grace period whereby these licenses behave as 30 day evaluation licenses. This is so that
     * admins are able to keep their JIRA running. This flag is reset automatically when all the invalid licenses
     * are updated.
     *
     * @param userName the name of the user that made the confirmation.
     * @see #hasLicenseTooOldForBuildConfirmationBeenDone()
     */
    void confirmProceedUnderEvaluationTerms(String userName);

    /**
     * Returns true if the support/maintenance period for this JIRA instance has been exceeded and an instance admin
     * has acknowledged this fact. Typically, JIRA allows for a 30 day grace (evaluation) period.
     *
     * @return {@code true} if the licenses in use are too old for the current build number and this instance is in
     * a maintenance grace period.
     * @see #confirmProceedUnderEvaluationTerms(String)
     * @since 7.0
     */
    boolean hasLicenseTooOldForBuildConfirmationBeenDone();

    /**
     * Retrieve a collection of all product licenses installed in this instance.
     * <p>
     * In pre-7.0 JIRA this method returns an iterable containing at most one license.
     * </p>
     *
     * @return all product licenses installed in this instance.
     * @since 6.3
     */
    @Nonnull
    Iterable<LicenseDetails> getLicenses();

    /**
     * Retrieve a Set of all installed product license's keys in this instance.
     *
     * @return all the installed product license's keys in this instance.
     * @since 7.0
     */
    @Nonnull
    Set<ApplicationKey> getAllLicensedApplicationKeys();

    /**
     * Retrieves the SEN (Support Entitlement Number) of all installed licenses and orders them in a consistent manner.
     * The order is guaranteed for a given set of licenses. It may change when licenses are added or removed.
     *
     * @return {@see SortedSet} of the SEN. The order of SENs in the set will be maintained as far as the
     * set of installed licenses doesn't change.
     * @see com.atlassian.jira.license.LicenseDetails#getSupportEntitlementNumber()
     * @since 7.0
     */
    SortedSet<String> getSupportEntitlementNumbers();

    /**
     * Return the {@link com.atlassian.jira.license.LicenseDetails} associated with the passed
     * {@link com.atlassian.application.api.ApplicationKey}.
     *
     * @param key the {@code ApplicationKey} to query.
     * @return the {@code LicenseDetails} associated with the passed application key or
     * {@link com.atlassian.fugue.Option#none()} if no such application exists.
     * @since 7.0
     */
    Option<LicenseDetails> getLicense(@Nonnull ApplicationKey key);

    /**
     * Removes all licenses from JIRA and replace them with the new license. If the new license cannot be decoded,
     * this method will throw an {@link IllegalArgumentException}.
     *
     * @throws IllegalArgumentException if the license cannot be decoded.
     * @since 6.4
     */
    void clearAndSetLicense(String licenseString);

    /**
     * Removes all licenses from JIRA and replace them with the new license without raising an event. If the new license
     * cannot be decoded, this method will throw an {@link IllegalArgumentException}.
     *
     * @throws IllegalArgumentException if the license cannot be decoded.
     * @since 6.4
     */
    LicenseDetails clearAndSetLicenseNoEvent(String licenseString);

    /**
     * Removes the license that grants access to the passed application.
     * Note: While this could potentially lead to losing access to another application the real world scenarios are:
     * <ul>
     * <li>You have just one ELA license with multiple applications</li>
     * <li>You have multiple licenses with a single application</li>
     * </ul>
     *
     * @param application the key of the application to check.
     * @throws IllegalStateException when removal of a license that would leave JIRA inoperable. For example,
     *                               removing the last license is not allowed.
     * @since 7.0
     */
    void removeLicense(@Nonnull ApplicationKey application) throws IllegalStateException;

    /**
     * Removes passed licenses.
     *
     * @param licenses to be removed
     * @throws IllegalStateException when removal of licenses that would leave JIRA inoperable. For example,
     *                               removing the last license is not allowed.
     * @since 7.0
     */
    void removeLicenses(@Nonnull Iterable<? extends LicenseDetails> licenses) throws IllegalStateException;

    /**
     * Returns true if at least one license has been {@link #setLicense(String) set}.
     *
     * @see com.atlassian.jira.bc.license.JiraLicenseService#isLicenseSet()
     * @since 7.0
     */
    boolean isLicenseSet();

    /**
     * Subscribe a {@link Consumer} to be called after this manager's cache has been cleared.
     *
     * @param consumer {@link Consumer} that will be called after this manager's cache has been cleared.
     * @see #unSubscribeFromClearCache(Consumer)
     * @since 7.0
     */
    @Internal
    void subscribeToClearCache(@Nonnull Consumer<Void> consumer);

    /**
     * Un-subscribe a {@link Consumer} from being called after clearing of this manager's cache.
     *
     * @param consumer {@link Consumer} to be un-subscribed from this manager.
     * @see #subscribeToClearCache(Consumer)
     * @since 7.0
     */
    @Internal
    void unSubscribeFromClearCache(@Nonnull Consumer<Void> consumer);
}
