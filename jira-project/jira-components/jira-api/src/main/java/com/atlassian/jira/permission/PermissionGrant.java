package com.atlassian.jira.permission;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.entity.WithId;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;

import javax.annotation.Nonnull;

/**
 * Permission grant holds information about a permission granted to a certain user or group of users. It is a basic building block of {@link PermissionScheme}s.
 * <p>
 * A grant consists of two things:
 * <p>
 * <dl>
 * <dt>holder</dt> <dd>A person or group of people that holds the permission, see {@link PermissionHolder} for more details.</d>
 * <dt>permission</dt> <dd> Project permission that is being granted. For example: view issues, browse projects, add comments. Standard JIRA permissions are defined in the {@link ProjectPermissions} class</dd>
 * </dl>
 * <p>
 * Implementations of this interface are required to be immutable.
 * </p>
 */
@PublicApi
public interface PermissionGrant extends WithId {
    /**
     * Returns an id of the permission grant as stored in DB.
     */
    @Nonnull
    Long getId();

    /**
     * Returns an entity to which the permission is being granted.
     */
    @Nonnull
    PermissionHolder getHolder();

    /**
     * Returns a permission that is being granted.
     */
    @Nonnull
    ProjectPermissionKey getPermission();
}
