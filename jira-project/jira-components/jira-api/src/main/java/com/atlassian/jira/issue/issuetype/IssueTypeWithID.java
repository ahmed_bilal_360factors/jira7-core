package com.atlassian.jira.issue.issuetype;

import com.atlassian.jira.entity.WithId;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

/**
 * Wrapper class which translates an IssueType to an object that implement WithId so that it can be used to store properties.
 *
 * @since 7.0
 */

@Immutable
public class IssueTypeWithID implements WithId {
    private final IssueType issueType;

    private IssueTypeWithID(final IssueType issueType) {
        this.issueType = issueType;
    }

    /**
     * Creates an IssueTypeWithId from IssueType.
     *
     * @return null if issueType is null, else IssueType wrapped with IssueTypeWithId
     */
    public static IssueTypeWithID fromIssueType(@Nullable IssueType issueType) {
        return issueType != null ? new IssueTypeWithID(issueType) : null;
    }

    @Override
    public Long getId() {
        return Long.parseLong(issueType.getId());
    }
}