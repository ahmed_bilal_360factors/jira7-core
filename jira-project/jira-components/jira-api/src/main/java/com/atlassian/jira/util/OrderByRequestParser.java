package com.atlassian.jira.util;

import com.atlassian.annotations.PublicApi;
import com.atlassian.fugue.Either;

/**
 * A component that can parse raw strings into {@link OrderByRequest} objects.
 */
@InjectableComponent
@PublicApi
public interface OrderByRequestParser {
    /**
     * Parses a string representing an order by request into the {@code OrderByRequest} object. Works
     * only if field type of the expected order by request is an enum. Expected representation is as follows:
     * <p>
     * <dl>
     * <dt>name</dt><dd>order by <b>name</b> ascending</dd>
     * <dt>+name</dt><dd>order by <b>name</b> ascending</dd>
     * <dt>-name</dt><dd>order by <b>name</b> ascending</dd>
     * </dl>
     * <p>
     * Parsing is case insensitive so the above example will succeed if there is a {@code NAME} value in the supplied enum type.
     *
     * @param orderByValue value to parse
     * @param fields       all available fields declared in an enum
     * @param <T>          enum type
     * @return parsed query or an error collection with a proper message
     */
    <T extends Enum<T>> Either<ErrorCollection, OrderByRequest<T>> parse(String orderByValue, Class<T> fields);
}
