package com.atlassian.jira.portal;

import com.atlassian.fugue.Option;
import com.atlassian.gadgets.dashboard.Color;
import com.atlassian.plugin.ModuleCompleteKey;

import java.net.URI;
import java.util.List;
import java.util.Map;

/**
 * Manager for the {@link com.atlassian.jira.portal.PortletConfiguration} domain object.
 * <p>
 * It has a legacy name, but actually manages the Google Gadgets.
 */
public interface PortletConfigurationManager {
    /**
     * Get all the {@link com.atlassian.jira.portal.PortletConfiguration} associated with the passed Portal Page.
     *
     * @param portalPageId the portal page to query.
     * @return a list of portlet configurations on the passed page.
     */
    List<PortletConfiguration> getByPortalPage(Long portalPageId);

    /**
     * Get the passed portlet configuration.
     *
     * @param portletId the id of the portlet configuration to return.
     * @return the porlet configuration identified by the passed id.
     */
    PortletConfiguration getByPortletId(Long portletId);

    /**
     * Remove the passed portlet configuration.
     *
     * @param pc the portlet configuration to remove.
     */
    void delete(PortletConfiguration pc);

    /**
     * Update the passed portlet configuration.
     *
     * @param pc the portlet configuration to change.
     */
    void store(PortletConfiguration pc);

    /**
     * Create a new portlet configuration for the passed parameters.
     *
     * @param portalPageId    the portal page the configuration will belong to.
     * @param column          the column location for the new configuration.
     * @param row             the row location for the new configuration.
     * @param gadgetXml       A URI specifying the location of the gadget XML.  May be null if this is a legacy portlet.
     * @param color           The chrome color for the gadget.
     * @param userPreferences A map of key -> value user preference pairs used to store gadget configuration.
     * @return the new portlet configuration
     * @deprecated Use {@link #addDashBoardItem(Long, Integer, Integer, com.atlassian.fugue.Option, com.atlassian.gadgets.dashboard.Color, java.util.Map, com.atlassian.fugue.Option)} instead. Since v6.4
     */
    @Deprecated
    PortletConfiguration addGadget(Long portalPageId, Integer column, Integer row, URI gadgetXml, Color color, Map<String, String> userPreferences);

    /**
     * Create a new {@link com.atlassian.jira.portal.PortletConfiguration} to the given {@link PortalPage} for the passed parameters.
     *
     * @param portalPageId      the portal page the configuration will belong to.* com.atlassian.jira.dashboard.JiraGadgetStateFactory#createGadgetState(java.net.URI)}. May be null for a generated id.
     * @param column            The column position of the portlet.
     * @param row               The row position of the portlet
     * @param openSocialSpecUri A URI specifying the location of the gadget XML.  May be an empty option if this is
     *                          a legacy portlet or dashboard item without uri replacement.
     * @param color             The chrome color for the gadget.
     * @param userPreferences   A map of key -> value user preference pairs used to store gadget configuration.
     * @param moduleKey         A complete module key of dashboard items.
     * @return The new portlet configuration.
     * @since 6.4
     */
    PortletConfiguration addDashBoardItem(Long portalPageId, Integer column, Integer row, Option<URI> openSocialSpecUri,
                                          Color color, Map<String, String> userPreferences, Option<ModuleCompleteKey> moduleKey);
}