package com.atlassian.jira.bc.issue.worklog;

import com.atlassian.annotations.PublicApi;

import java.util.List;

/**
 * Holds information about a page of worklogs which were updated or deleted since a given time.
 */
@PublicApi
public class WorklogChangedSincePage<T> {
    private final Long since;
    private final Long until;
    private final List<T> value;
    private final boolean lastPage;

    public WorklogChangedSincePage(final Long since, final Long until, final List<T> value, final boolean lastPage) {
        this.since = since;
        this.until = until;
        this.value = value;
        this.lastPage = lastPage;
    }

    /**
     * Returns timestamp of the query.
     */
    public Long getSince() {
        return since;
    }

    /**
     * Returns timestamp of the latest updated or deleted worklog. It may be used to get another page.
     */
    public Long getUntil() {
        return until;
    }

    /**
     * Returns a page with ids of worklogs which were modified.
     */
    public List<T> getChangedSince() {
        return value;
    }

    /**
     * Returns true, if this is a last page.
     */
    public boolean isLastPage() {
        return lastPage;
    }
}
