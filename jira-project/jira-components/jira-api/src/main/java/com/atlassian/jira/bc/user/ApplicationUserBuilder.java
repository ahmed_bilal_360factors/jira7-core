package com.atlassian.jira.bc.user;

import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * @since v7.0
 */
@ParametersAreNonnullByDefault
public interface ApplicationUserBuilder {
    @Nonnull
    ApplicationUserBuilder active(boolean active);

    @Nonnull
    ApplicationUserBuilder name(String name);

    @Nonnull
    ApplicationUserBuilder emailAddress(String emailAddress);

    @Nonnull
    ApplicationUserBuilder displayName(String displayName);

    @Nonnull
    ApplicationUser build();
}
