package com.atlassian.jira.security.roles;

import com.atlassian.annotations.PublicSpi;
import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Set;

/**
 * Create RoleActor instances.
 */
@PublicSpi
public interface RoleActorFactory {
    /**
     * Create instance of the ProjectRoleActor representing a role actor stored in the database.
     * In order to add a new role actor please pass a null id.
     *
     * @param id            of the ProjectRoleActor
     * @param projectRoleId of the ProjectRole associated with the actor.
     * @param projectId     of the Project associated with the actor.
     * @param type          of the actor which determines the implementation.
     * @param parameter     saying with which exact entity is actor associated ie. group name, user name.
     * @return implementation representing ProjectRoleActor.
     */
    ProjectRoleActor createRoleActor(Long id, Long projectRoleId, Long projectId, String type, String parameter) throws RoleActorDoesNotExistException;

    /**
     * if RoleActors can be aggregated and queried in a more optimised way, then optimize the set to reduce its size so
     * we reduce the number of iterations across the set.
     *
     * @param roleActors a Set of RoleActor instances
     * @return the optimized Set perhaps containing aggregations that can be queried more efficiently.
     */
    Set<RoleActor> optimizeRoleActorSet(Set<RoleActor> roleActors);

    /**
     * Find and return all role actors for the given user.
     *
     * @param user for which actors will be obtained
     * @return a set of actors being associated with the given user or empty set if none found.
     */
    @Nonnull
    Set<ProjectRoleActor> getAllRoleActorsForUser(@Nullable ApplicationUser user);
}