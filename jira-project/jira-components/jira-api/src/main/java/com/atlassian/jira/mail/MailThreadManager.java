package com.atlassian.jira.mail;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.issue.Issue;

import javax.annotation.Nullable;
import javax.mail.Message;

/**
 * @deprecated mail handlers no longer need to register processed email with this class, JIRA no longer relies on this to prevent duplicate issue being created from one email.
 */
@PublicApi
@Deprecated
public interface MailThreadManager {
    /**
     * Indicates an action in response to an incoming email.
     *
     * @since v5.2.3
     */
    enum MailAction {
        ISSUE_CREATED_FROM_EMAIL, ISSUE_COMMENTED_FROM_EMAIL
    }

    /**
     * Remembers that given messageId has been used to either create or comment on an Issue.
     * <p>
     * This creates an entry in "NotificationInstance" table that will only be removed in two cases
     * <ul>
     * <li>Issue being deleted
     * <li>Entry is deleted manually with a call to {@link #removeAssociatedEntries(Long)}
     * </ul>
     *
     * @param messageId     The incoming Message-ID
     * @param senderAddress The sender
     * @param issue         the issue that was affected (created or commented)
     * @param action        Issue created or Issue commented
     * @since v5.2.3
     */
    void storeIncomingMessageId(String messageId, String senderAddress, Issue issue, MailAction action);

    /**
     * Thread the given email which is related to the given issue.
     *
     * @param email the email to be sent
     * @param issue the issue that the email is about
     */
    void threadNotificationEmail(Email email, Issue issue);

    /**
     * Looks for an issue associated with the given message by inspecting the "In-Reply-To" header of the message.
     * <p>
     * Notifications sent from JIRA have a special form that allows us to parse out the Issue ID.
     * We also remember the incoming Message-IDs so we can tell if another recipient replies to that message.
     *
     * @param message message to analyse
     * @return associated issue or null if no issue is associated with this message.
     */
    @Nullable
    Issue getAssociatedIssueObject(Message message);

    /**
     * Looks for an issue associated with the given message by inspecting the "Message-ID" header of the message.
     *
     * @param messageId Message-ID to be checked
     * @return Issue that is already associated with this Message-ID or null if none
     */
    @Nullable
    Issue findIssueFromMessageId(String messageId);

    /**
     * Removes rows from NotificationInstance table associated with the given issue.
     * Used when we delete an issue.
     *
     * @param issueId the issue
     * @return row count
     */
    int removeAssociatedEntries(Long issueId);
}
