package com.atlassian.jira.avatar;

import com.atlassian.annotations.PublicApi;
import com.atlassian.fugue.Pair;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.icon.IconType;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSortedMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.SortedMap;

import static com.atlassian.fugue.Pair.pair;
import static com.google.common.collect.Maps.uniqueIndex;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toMap;

/**
 * Represents an icon for a project or some other entity in JIRA.
 *
 * @since v4.0
 */
@PublicApi
public interface Avatar {
    /**
     * Returns the avatar type.
     * <p>
     *     Since v7.1 plugins can add avatars to arbitrary entities.
     *     The enum will just return {@link Type#OTHER} if the Avatar is for a custom entity.
     * </p>
     *
     * @return a non null Avatar.Type.
     * @deprecated Use {@link #getIconType()} instead. Since v7.1
     */
    @Nonnull
    Avatar.Type getAvatarType();

    /**
     * Get the icon type.
     * @return The type of the icon.
     * @since v7.1
     */
    @Nonnull
    IconType getIconType();

    /**
     * The base filename to the avatar image file. The actual file name will be modified with the id etc.
     *
     * @return the non null file name.
     */
    @Nonnull
    String getFileName();

    /**
     * The MIME type of the avatar image file.
     *
     * @return the non null file name.
     */
    @Nonnull
    String getContentType();

    /**
     * The database identifier for the Avatar, may be null if it hasn't yet been stored or if database identifiers are
     * not supported. This will always return null for user avatars in some deployment configurations. Avatars for
     * other purposes (e.g. Projects) may follow this in future versions and this method will be deprecated entirely.
     * The id should not be used to construct URLs to the currently-configured avatar for anything. This method should
     * only be used when it is necessary to refer to an avatar that is not currently the configured avatar for the
     * domain object. The only use cases where this is needed are those to do with modifying or viewing detailed avatar
     * configuration.
     *
     * @return the database id or null.
     */
    Long getId();

    /**
     * Returns the identity of the domain object that this avatar is an avatar for.
     * <p>
     * For example, if it is a user avatar, it would be the user key (since that is the primary key), for a Project
     * it is the project ID as a String. The meaning of this should be determined by the {@link IconType}.
     * <p>
     * For a non-system avatar (see {@link #isSystemAvatar()}}, the owner is never null. For a system avatar, the owner
     * is always null.
     *
     * @return the ID of the domain object that this is the avatar for.
     */
    String getOwner();

    /**
     * Indicates whether the Avatar is a system-provided one or if users have defined it.
     *
     * @return true only if the Avatar is a system-provided one.
     */
    boolean isSystemAvatar();

    /**
     * An indicator of the owner type of the avatar. E.g. project, user, group, role etc.
     * @deprecated Use {@link IconType} in place of this. Since v7.1
     */
    @Deprecated
    public static enum Type {

        PROJECT("project", APKeys.JIRA_DEFAULT_AVATAR_ID),
        USER("user", APKeys.JIRA_DEFAULT_USER_AVATAR_ID),
        /**
         * @since v6.3
         */
        ISSUETYPE("issuetype", APKeys.JIRA_DEFAULT_ISSUETYPE_AVATAR_ID),
        /**
         * This means that the type cannot be represented by {@link Avatar.Type}. Call {@link Avatar#getIconType()}
         * instead.
         * @since v7.1
         */
        OTHER("other", null);

        private String name;
        private String defaultIdKey;
        private final static Map<String, Type> typesByName;

        static {
            typesByName = createNameToTypeMap();
        }

        private static ImmutableMap<String, Type> createNameToTypeMap() {
            final ImmutableMap.Builder<String, Type> typesByNameBuilder = ImmutableMap.builder();
            for (Type type : Type.values()) {
                typesByNameBuilder.put(type.getName(), type);
            }

            return typesByNameBuilder.build();
        }

        private Type(final String name, final String defaultIdKey) {
            this.name = name;
            this.defaultIdKey = defaultIdKey;
        }

        /**
         * The canonical String representation of the type.
         *
         * @return the name.
         */
        public String getName() {
            return name;
        }

        public static Type getByName(final String name) {
            return name == null ?
                    null :
                    typesByName.get(name);
        }

        /**
         * Convert from an iconType.
         * @param iconType The iconType to look up.
         * @return Returns the correct Type if this iconType can be represented, or else returns {@link #OTHER}.
         */
        public static Type getByIconType(final IconType iconType) {
            if (iconType == null) {
                return null;
            }
            if (supportsName(iconType.getKey())) {
                return getByName(iconType.getKey());
            } else {
                return OTHER;
            }
        }

        /**
         * Check if this type name is supported by {@link Avatar.Type}. JIRA supports pluggable {@link IconType}
         * implementations, not all of which are supported by {@link Avatar.Type}. This method can tell you if
         * the Avatar type name you are looking for is supported by {@link Avatar.Type}.
         * @param name The type name to look up.
         * @return true if this name is supported. That is, if true is returned, then {@link #getByName(String)}}
         *         will return a valid value.
         * @since 7.1
         */
        public static boolean supportsName(String name) {
            if (name == null) {
                return false;
            } else {
                return (typesByName.get(name) != null);
            }
        }

        public Long getDefaultId(ApplicationProperties applicationProperties) {
            if (defaultIdKey == null) {
                throw new UnsupportedOperationException("cannot retrieve default ID for custom avatar type");
            }
            String defaultAvatarId = applicationProperties.getString(this.defaultIdKey);
            return defaultAvatarId != null ? Long.valueOf(defaultAvatarId) : null;
        }
    }

    /**
     * The standard sizes for avatars.
     */
    public static enum Size {
        /**
         * A small avatar (24x24 pixels). Use when outputting user's names.
         */
        NORMAL("small", 24),

        /**
         * An extra-small avatar (16x16 pixels).
         */
        SMALL("xsmall", 16),

        /**
         * A medium avatar (32x32 pixels). Use in comments and other activity streams.
         */
        MEDIUM("medium", 32),

        /**
         * A large avatar (48x48 pixels).
         */
        LARGE("large", 48, true),

        XLARGE("xlarge", 64),
        NORMAL_3X("small@3x", 72),
        XXLARGE("xxlarge", 96),
        XXXLARGE("xxxlarge", 128),
        LARGE_3X("large@3x", 144),
        RETINA_XXLARGE("xxlarge@2x", 192),
        RETINA_XXXLARGE("xxxlarge@2x", 256),
        RETINA_XXLARGE_3X("xxlarge@3x", 288),
        RETINA_XXXLARGE_3X("xxxlarge@3x", 384);

        public static final Predicate<Size> LOW_RES = new Predicate<Avatar.Size>() {
            // TODO JRADEV-20790 - Don't output higher res URLs in our REST endpoints until system avatars have more pixels.
            @Override
            public boolean apply(final Avatar.Size input) {
                return input.getPixels() <= 48;
            }
        };

        /**
         * The value to pass back to the server for the size parameter.
         */
        final String param;

        /**
         * The number of pixels.
         */
        final Integer pixels;

        /**
         * Whether this is the default size.
         */
        final boolean isDefault;

        private static final Size largest;
        private static final Size defaultSize;
        private static final List<Size> orderedSizes;
        private static final SortedMap<String, Size> paramToSize;

        static {
            Size maxValue = SMALL;
            Size defaultValue = SMALL;
            for (Size imageSize : values()) {
                if (imageSize.isDefault) {
                    defaultValue = imageSize;
                }
                if (imageSize.pixels > maxValue.pixels) {
                    maxValue = imageSize;
                }
            }
            largest = maxValue;
            defaultSize = defaultValue;
            orderedSizes = Size.inPixelOrder();
            paramToSize = createParamToSizeMap();
        }

        private boolean isMultiplied() {
            return param.contains("@");
        }

        /**
         * Creates a map used in the {@link Size#getSizeFromParam(String)} method.
         * Note that it also adds all standard names with multipliers to the map.
         */
        private static SortedMap<String, Size> createParamToSizeMap() {
            final Map<String, Size> exactMapping = uniqueIndex(asList(Size.values()), Size::getParam);

            final List<Pair<String, Integer>> multipliers = ImmutableList.of(pair("@2x", 2), pair("@3x", 3));

            final Map<String, Size> multipliedParamsMapping = asList(Size.values()).stream()
                    .filter(size -> !size.isMultiplied())
                    .flatMap(size -> multipliers.stream()
                            .map(multiplier -> pair(size.getParam() + multiplier.left(), size.getPixels() * multiplier.right()))
                            .map(multiplied -> pair(multiplied.left(), Size.approx(multiplied.right()))))
                    .collect(toMap(Pair::left, Pair::right));

            final SortedMap<String, Size> result = Maps.newTreeMap();
            result.putAll(exactMapping);
            result.putAll(multipliedParamsMapping);

            return ImmutableSortedMap.copyOfSorted(result);
        }

        Size(String param, int pixels, boolean isDefault) {
            this.param = param;
            this.isDefault = isDefault;
            this.pixels = pixels;
        }

        Size(String param, int pixels) {
            this(param, pixels, false);
        }

        /**
         * In order to cater for future addition of larger sizes this method finds the largest image size.
         *
         * @return The largest Size
         */
        public static Size largest() {
            return largest;
        }

        /**
         * @return the default size for avatars.
         */
        public static Size defaultSize() {
            return defaultSize;
        }

        /**
         * @param pixelValue minimum number of pixels tall+wide the avatar size should be
         * @return an avatar {@link Size} that's equal to or larger than the pixelValue,
         * or null if there's no size that could cater the value.
         */
        public static Size biggerThan(int pixelValue) {
            Size theSize = null;
            for (Size aSize : Size.inPixelOrder()) {
                if (aSize.pixels >= pixelValue) {
                    theSize = aSize;
                    break;
                }
            }
            return theSize;
        }

        /**
         * @param pixelValue minimum number of pixels tall+wide the avatar size should be
         * @return an avatar {@link Size} that's equal to or larger than the pixelValue,
         * or null if there's no size that could cater the value.
         */
        public static Size smallerThan(int pixelValue) {
            Size theSize = null;
            for (Size aSize : Lists.reverse(Size.inPixelOrder())) {
                if (aSize.pixels <= pixelValue) {
                    theSize = aSize;
                    break;
                }
            }
            return theSize;
        }

        static List<Size> inPixelOrder() {
            if (null != orderedSizes) {
                return orderedSizes;
            }
            List<Size> orderedSizes = asList(Size.values());
            Collections.sort(orderedSizes, new Comparator<Size>() {
                @Override
                public int compare(final Size o1, final Size o2) {
                    if (o1.getPixels() == o2.getPixels()) {
                        return 0;
                    }
                    return (o1.getPixels() < o2.getPixels()) ? -1 : 1;
                }
            });
            return orderedSizes;
        }

        public int getPixels() {
            return pixels;
        }

        public String getParam() {
            return param;
        }

        @Override
        public String toString() {
            return String.format("<Size [%s], %dx%dpx>", param, pixels, pixels);
        }

        /**
         * Returns the {@link com.atlassian.jira.avatar.Avatar.Size} for the given AUI standard size name.
         * <p>
         * Each AUI size name can be specified with a 2x or 3x multiplier. So the following sizes are available:
         * <ul>
         * <li>xsmall, xsmall@2x, xsmall@3x</li>
         * <li>small, small@2x, small@3x</li>
         * <li>medium, medium@2x, medium@3x</li>
         * <li>large, large@2x, large@3x</li>
         * <li>xlarge, xlarge@2x, xlarge@3x</li>
         * <li>xxlarge, xxlarge@2x, xxlarge@3x</li>
         * <li>xxxlarge, xxxlarge@2x, xxxlarge@3x</li>
         * </ul>
         * </p>
         *
         * @param param the standard name of the size (e.g. small, medium, xlarge), may be multiplied
         * @return the corresponding {@link com.atlassian.jira.avatar.Avatar.Size}
         * @throws java.util.NoSuchElementException if there is no known size by that name.
         */
        public static Size getSizeFromParam(final String param) {
            if (null == param || !paramToSize.containsKey(param)) {
                throw new NoSuchElementException(param);
            }

            return paramToSize.get(param);
        }

        /**
         * Returns an avatar image size matching the text provided.
         * If none can be found, returns {@link Avatar.Size#defaultSize()}.
         *
         * @param text the images size. Will match "s", "small", "SMALL". Can also be an integer value (16, 24, etc.)
         * @return the image size enum matching the string provided
         */
        public static Avatar.Size fromString(String text) {
            if (StringUtils.isNotBlank(text)) {
                int sizeInPixels = -1;
                try {
                    sizeInPixels = Integer.parseInt(text);
                } catch (NumberFormatException nfe) {
                    // It's not a number. That's okay!
                }

                for (String param : paramToSize.keySet()) {
                    Size size = paramToSize.get(param);

                    if ((sizeInPixels > 0 && sizeInPixels == size.getPixels())
                            || StringUtils.startsWithIgnoreCase(size.name(), text)
                            || StringUtils.startsWithIgnoreCase(param, text)) {
                        return size;
                    }
                }
            }
            //fallback to default size if none could be found.
            return Avatar.Size.defaultSize();
        }

        /**
         * Returns the Size which is exactly the same size as the given pixel edge value, rounding upward if no
         * exact match is available, except if the given value is greater than the maximum available size in which
         * case the maximum size will be returned.
         *
         * @param size
         * @return
         */
        public static Size approx(final int size) {
            if (size >= largest.getPixels()) {
                return largest;
            }
            if (size <= orderedSizes.get(0).getPixels()) {
                return orderedSizes.get(0);
            }
            for (Size s : orderedSizes) {
                if (s.getPixels() >= size) {
                    return s;
                }
            }
            // this shouldn't happen because we've exhausted the options
            return Size.defaultSize;
        }
    }

    /**
     * These are the filenames of avatars that used to be available as system avatars, but were
     * to be removed from the list of avatar options available to new projects.
     */
    static final List<String> demotedSystemProjectAvatars = ImmutableList.of(
            "codegeist.png",
            "jm_black.png",
            "jm_brown.png",
            "jm_orange.png",
            "jm_red.png",
            "jm_white.png",
            "jm_yellow.png",
            "monster.png"
    );

    static final List<String> demotedSystemUserAvatars = ImmutableList.of(
            "Avatar-1.png",
            "Avatar-2.png",
            "Avatar-3.png",
            "Avatar-4.png",
            "Avatar-5.png",
            "Avatar-6.png",
            "Avatar-7.png",
            "Avatar-8.png",
            "Avatar-9.png",
            "Avatar-10.png",
            "Avatar-11.png",
            "Avatar-12.png",
            "Avatar-13.png",
            "Avatar-14.png",
            "Avatar-15.png",
            "Avatar-16.png",
            "Avatar-17.png",
            "Avatar-18.png",
            "Avatar-19.png",
            "Avatar-20.png",
            "Avatar-21.png",
            "Avatar-22.png",
            "Avatar-addnew.png",
            "Avatar-default.png",
            "Avatar-unknown.png"
    );
}
