package com.atlassian.jira.project.template.hook;

import com.atlassian.annotations.PublicApi;

import java.util.List;

/**
 * A screen tab template used for Project Template configuration.
 *
 * @since 7.0
 */
@PublicApi
public interface ScreenTabTemplate {
    /**
     * Returns the name of the tab.
     *
     * @return The name of the tab.
     */
    String name();

    /**
     * Returns the list of fields for this tab.
     *
     * @return The list of fields for this tab.
     */
    List<String> fields();
}
