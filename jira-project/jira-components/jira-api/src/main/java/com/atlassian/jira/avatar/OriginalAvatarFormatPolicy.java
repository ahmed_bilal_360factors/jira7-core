package com.atlassian.jira.avatar;

import com.atlassian.annotations.Internal;
import com.atlassian.jira.io.ResourceData;

import java.io.IOException;

/**
 * A policy that will always use the actual content type of the avatar.
 *
 * @see AvatarFormatPolicy
 * @since 7.0.1
 */
@Internal
class OriginalAvatarFormatPolicy extends AvatarFormatPolicy {
    @Override
    ResourceData getData(
            final Avatar avatar,
            final InputStreamSupplier originalData,
            final InputStreamSupplier transcodedData
    ) throws IOException {
        return new ResourceData(originalData.get(), avatar.getContentType());
    }
}
