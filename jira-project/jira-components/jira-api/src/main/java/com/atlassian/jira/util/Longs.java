package com.atlassian.jira.util;

import com.google.common.base.Optional;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Contains static utility methods pertaining to the {@code Long} type.
 *
 * @since 6.4.9
 */
public class Longs {
    /**
     * Parses the string argument as a {@code Long} by delegating to {@link Long#parseLong(String)}, but returns an
     * {@link Optional<Long>} instead of throwing a {@link NumberFormatException}.
     *
     * @param value a {@code String} containing the {@code Long} representation to be parsed
     * @return an {@code Optional<Long>}, containing either the parsed {@code Long} value or nothing if the value could
     * not be parsed
     */
    @Nonnull
    public static Optional<Long> toLong(@Nullable String value) {
        try {
            return Optional.of(Long.parseLong(value));
        } catch (NumberFormatException e) {
            return Optional.absent();
        }
    }

    /**
     * Compares safely two nullable longs.
     * @return {@code true} if both values are null or contain equal numbers. {@code false} otherwise
     */
    public static boolean nullableLongsEquals(@Nullable Long number1, @Nullable Long number2) {
        if (number1 == null) {
            return null == number2;
        } else {
            return number1.equals(number2);
        }
    }
}
