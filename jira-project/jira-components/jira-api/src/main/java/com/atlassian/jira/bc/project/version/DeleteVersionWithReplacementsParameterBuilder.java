package com.atlassian.jira.bc.project.version;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.project.version.DeleteVersionWithCustomFieldParameters;
import com.atlassian.jira.project.version.Version;

import javax.annotation.Nullable;

@PublicApi
@ExperimentalApi
/**
 * Builds paramters for {@link VersionService#deleteVersionAndSwap(JiraServiceContext, DeleteVersionWithCustomFieldParameters)}
 *
 * @since 7.0.10
 */
public interface DeleteVersionWithReplacementsParameterBuilder {

    /**
     * Set version that will replace given version in 'fix verision' field
     *
     * @param moveFixIssuesTo version that will replace given version in fix version, can be null in which
     *                        case version will be remove from field
     */
    DeleteVersionWithReplacementsParameterBuilder moveFixIssuesTo(@Nullable Version moveFixIssuesTo);

    /**
     * Set version that will replace given version in 'affects verision' field
     *
     * @param moveAffectedIssuesTo version that will replace given version in 'affects version', can be null in which
     *                             case version will be remove from field
     */
    DeleteVersionWithReplacementsParameterBuilder moveAffectedIssuesTo(@Nullable Version moveAffectedIssuesTo);

    /**
     * Set version that will replace given version in given custom field
     *
     * @param customFieldId     custom field identifier where this version will be replaced
     * @param moveCustomFieldTo version that will replace given version in custom field, can be null in which
     *                          case version will be remove from field
     */
    DeleteVersionWithReplacementsParameterBuilder moveCustomFieldTo(long customFieldId, @Nullable Version moveCustomFieldTo);

    /**
     * Build parameters for
     *
     * @return {@link VersionService#deleteVersionAndSwap(JiraServiceContext, DeleteVersionWithCustomFieldParameters)}
     */
    DeleteVersionWithCustomFieldParameters build();
}
