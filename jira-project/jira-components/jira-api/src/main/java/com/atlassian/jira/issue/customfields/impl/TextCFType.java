package com.atlassian.jira.issue.customfields.impl;

import com.atlassian.annotations.PublicApi;
import com.atlassian.annotations.PublicSpi;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.imports.project.customfield.ProjectCustomFieldImporter;
import com.atlassian.jira.imports.project.customfield.ProjectImportableCustomField;
import com.atlassian.jira.imports.project.customfield.TextLengthValidatingCustomFieldImporter;
import com.atlassian.jira.issue.customfields.SortableCustomField;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.customfields.persistence.PersistenceFieldType;
import com.atlassian.jira.issue.fields.TextFieldCharacterLengthValidator;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.security.JiraAuthenticationContext;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Nonnull;

/**
 * @deprecated Use {@link GenericTextCFType} instead. Since v5.0.
 */
@Deprecated
@PublicApi
@PublicSpi
public class TextCFType extends StringCFType implements SortableCustomField<String>, ProjectImportableCustomField {
    private final ProjectCustomFieldImporter projectCustomFieldImporter;

    public TextCFType(final CustomFieldValuePersister customFieldValuePersister, final GenericConfigManager genericConfigManager,
                      final TextFieldCharacterLengthValidator textFieldCharacterLengthValidator, final JiraAuthenticationContext jiraAuthenticationContext) {
        super(customFieldValuePersister, genericConfigManager, textFieldCharacterLengthValidator, jiraAuthenticationContext);
        projectCustomFieldImporter = new TextLengthValidatingCustomFieldImporter(textFieldCharacterLengthValidator);
    }

    /**
     * Constructor.
     *
     * @deprecated Use {@link #TextCFType(com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister, com.atlassian.jira.issue.customfields.manager.GenericConfigManager, com.atlassian.jira.issue.fields.TextFieldCharacterLengthValidator, com.atlassian.jira.security.JiraAuthenticationContext)} instead. Since v6.4.1
     */
    @Deprecated
    public TextCFType(final CustomFieldValuePersister customFieldValuePersister, final GenericConfigManager genericConfigManager) {
        super(customFieldValuePersister, genericConfigManager);
        projectCustomFieldImporter = new TextLengthValidatingCustomFieldImporter(ComponentAccessor.getComponent(TextFieldCharacterLengthValidator.class));
    }

    public String getStringFromSingularObject(final Object value) {
        assertObjectImplementsType(String.class, value);
        // convert null to empty string
        return StringUtils.defaultString((String) value);
    }

    public Object getSingularObjectFromString(final String string) throws FieldValidationException {
        return string;
    }

    public int compare(@Nonnull final String customFieldObjectValue1, @Nonnull final String customFieldObjectValue2, final FieldConfig fieldConfig) {
        return customFieldObjectValue1.compareTo(customFieldObjectValue2);
    }

    @Nonnull
    @Override
    protected PersistenceFieldType getDatabaseType() {
        return PersistenceFieldType.TYPE_LIMITED_TEXT;
    }

    /**
     * This method will return a {@link TextLengthValidatingCustomFieldImporter}, be mindful that if you are extending
     * this class you need to have a good hard think about whether this is the right field importer for your custom
     * field values.
     *
     * @return a {@link TextLengthValidatingCustomFieldImporter}
     * @see com.atlassian.jira.imports.project.customfield.ProjectImportableCustomField#getProjectImporter()
     */
    public ProjectCustomFieldImporter getProjectImporter() {
        return projectCustomFieldImporter;
    }

    @Override
    public Object accept(VisitorBase visitor) {
        if (visitor instanceof Visitor) {
            return ((Visitor) visitor).visitText(this);
        }

        return super.accept(visitor);
    }

    public interface Visitor<T> extends VisitorBase<T> {
        T visitText(TextCFType textCustomFieldType);
    }
}
