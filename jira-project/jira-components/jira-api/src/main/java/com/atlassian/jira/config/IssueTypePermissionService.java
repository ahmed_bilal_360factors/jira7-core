package com.atlassian.jira.config;

import com.atlassian.jira.user.ApplicationUser;

public interface IssueTypePermissionService {
    /**
     * Checks if the user has permissions to view the given issue type.
     */
    boolean hasPermissionToViewIssueType(ApplicationUser applicationUser, String id);

    /**
     * Checks if the user has permissions to edit the given issue type.
     */
    boolean hasPermissionToEditIssueType(ApplicationUser applicationUser);
}
