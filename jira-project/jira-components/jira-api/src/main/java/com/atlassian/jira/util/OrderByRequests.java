package com.atlassian.jira.util;

import com.atlassian.annotations.PublicApi;
import com.google.common.collect.Ordering;

/**
 * Class containing useful methods for sorting in accordance with {@link OrderByRequest}s.
 */
@PublicApi
public class OrderByRequests {
    /**
     * A field implementing this interface provides a method for extracting the field's value from an entity
     * of type {@code T}.
     *
     * @param <T> Entity type
     */
    public interface ExtractableField<T> {
        public Comparable getValue(T entity);
    }

    /**
     * Given an order query defined for fields implementing the {@link com.atlassian.jira.util.OrderByRequests.ExtractableField}
     * interface, creates an {@link com.google.common.collect.Ordering} that can sort according to the {@code orderByRequest}.
     *
     * @param orderByRequest order query to to create ordering from
     * @param <T>            type of the entities
     * @param <F>            type of the fields by which we sort
     * @return an ordering that can sort {@code T}-s according to the {@code orderByRequest}
     */
    public static <T, F extends ExtractableField<T>> Ordering<T> toOrdering(final OrderByRequest<F> orderByRequest) {
        Ordering<T> ordering = Ordering.natural().nullsLast().onResultOf(new com.google.common.base.Function<T, Comparable>() {
            @Override
            public Comparable apply(final T input) {
                return orderByRequest.getField().getValue(input);
            }
        });
        if (orderByRequest.getOrder() == OrderByRequest.Order.DESC) {
            ordering = ordering.reverse();
        }

        return ordering;
    }
}
