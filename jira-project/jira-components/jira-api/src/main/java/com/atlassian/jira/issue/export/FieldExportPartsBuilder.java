package com.atlassian.jira.issue.export;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

/**
 * Used to build the {@link FieldExportParts}.
 *
 * @since 7.2.0
 */
@ParametersAreNonnullByDefault
public class FieldExportPartsBuilder {
    private final Set<String> ids = Sets.newHashSet();
    private final List<FieldExportPart> items = Lists.newArrayList();

    /**
     * Used to add a {@link FieldExportPart} with multiple item. This can be used if
     * there is a Field component that can have multiple values.
     * @param id of the item, used for sorting and grouping the items over multiple issues
     * @param label to display for the column in this item
     * @param values for the field
     * @return itself
     */
    public FieldExportPartsBuilder addItem(final String id, final String label, final Stream<String> values) {
        if (ids.contains(id)) {
            throw new IllegalStateException("Id '" + id + "' has already been set previously, use a different id.");
        }
        items.add(new FieldExportPart(id, label, values));
        ids.add(id);
        return this;
    }

    /**
     * Used to add a {@link FieldExportPart} with a single item. E.g. Project Key from the
     * ProjectSystemField.
     * @param id of the item, used for sorting and grouping the items over multiple issues
     * @param label to display for the column in this item
     * @param value for the field
     * @return itself
     */
    public FieldExportPartsBuilder addItem(final String id, final String label, final String value) {
        return addItem(id, label, Stream.of(value));
    }

    /**
     * This should be called if you have been using the {@link #addItem} to generate each item.
     * @return the representation of the field.
     */
    public FieldExportParts build() {
        return new FieldExportParts(items);
    }

    /**
     * This generates a single {@link FieldExportPart} with a single value. For example,
     * a single text field would use this as there is only one value possible.
     * @param id that represents this item, used for sorting and grouping the items over multiple issues
     * @param label that would be used for the header of the column.
     * @param value for this issue's field, if null it will be replaced by an empty string
     * @return a representation for the field.
     */
    public static FieldExportParts buildSinglePartRepresentation(final String id, final String label, @Nullable final String value) {
        if (value == null) {
            return buildSinglePartRepresentation(id, label, Stream.of(""));
        } else {
            return buildSinglePartRepresentation(id, label, Stream.of(value));
        }
    }

    /**
     * This generates a representation for the field with a single {@link FieldExportPart} but contains
     * multiple values for this. An example of the use of this is CommentSystemField
     * there is only one item but can have many comments.
     * @param id that represents this item, used for sorting and grouping the items over multiple issues
     * @param label for each of the columns of the single item
     * @param values for each of the columns of the item
     * @return representation of this field
     */
    public static FieldExportParts buildSinglePartRepresentation(String id, final String label, final Stream<String> values) {
        final FieldExportPart item = new FieldExportPart(id, label, values);

        return new FieldExportParts(ImmutableList.of(item));
    }
}
