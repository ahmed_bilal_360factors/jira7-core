package com.atlassian.jira.event.web.action;

import com.atlassian.jira.web.action.JiraWebActionSupport;

import java.util.Collection;
import java.util.Map;

/**
 * Abstract event that can be used to capture analytics in subclasses of JiraWebActionSupport.
 * The event will include the command that triggered the event, global error messages and
 * form errors.
 */
public abstract class JiraWebActionSupportEvent {
    private final String command;

    private final Collection<String> errorMessages;

    private final Map<String, String> formErrors;

    public JiraWebActionSupportEvent(JiraWebActionSupport jiraWebActionSupport) {
        this.command = jiraWebActionSupport.getCommandName();
        this.errorMessages = jiraWebActionSupport.getErrorMessages();
        this.formErrors = jiraWebActionSupport.getErrors();
    }

    /**
     * @return true when the action doesn't have a command, doesn't have error message and has no form errors.
     */
    public boolean isEmpty() {
        return (command == null || command.isEmpty()) &&
                (errorMessages == null || errorMessages.isEmpty()) &&
                (formErrors == null || formErrors.isEmpty());
    }

    /**
     * @return analytics property "command"
     */
    public String getCommand() {
        return command;
    }

    /**
     * @return analytics property errorMessages, use "errorMessages.size" and "errorMessages[0]" in the whitelist.
     */
    public Collection<String> getErrorMessages() {
        return errorMessages;
    }

    /**
     * @return analytics property formErrors, use "formErrors.size" in the whitelist.
     */
    public Map<String, String> getFormErrors() {
        return formErrors;
    }

}
