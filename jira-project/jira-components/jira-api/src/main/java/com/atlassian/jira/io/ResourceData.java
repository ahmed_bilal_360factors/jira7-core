package com.atlassian.jira.io;

import com.atlassian.annotations.Internal;

import java.io.InputStream;

/**
 * Contains information about data content type and handle to data.
 *
 * @since v7.0.1
 */
@Internal
public class ResourceData {
    private final InputStream inputStream;
    private final String contentType;

    public ResourceData(final InputStream inputStream, final String contentType) {
        this.inputStream = inputStream;
        this.contentType = contentType;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public String getContentType() {
        return contentType;
    }
}
