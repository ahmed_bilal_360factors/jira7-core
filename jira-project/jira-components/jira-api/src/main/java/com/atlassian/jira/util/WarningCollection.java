package com.atlassian.jira.util;

import java.util.Collection;

/**
 * A very simple interface to collect warnings. This is typically used in Services to validate actions or intentions.
 * <p>
 *
 * @since v7.0
 */
public interface WarningCollection {
    /**
     * <p>
     * Add a warning to the collection
     * <p>
     *
     * @param warningMessage to be added
     */
    void addWarning(String warningMessage);

    /**
     * <p>
     * Retrieve all the warnings
     * <p>
     *
     * @return all warnings
     */
    Collection<String> getWarnings();

    /**
     * @return true when there is at least one warning
     */
    boolean hasAnyWarnings();
}
