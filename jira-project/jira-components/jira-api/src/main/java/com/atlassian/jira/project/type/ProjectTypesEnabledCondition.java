package com.atlassian.jira.project.type;

import com.atlassian.annotations.Internal;
import com.atlassian.jira.plugin.webfragment.conditions.AbstractWebCondition;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;

/**
 * Condition that returns true when the dark feature for project types for a given user is enabled.
 *
 * <pre>
 * *******************************
 * This is in the DMZ; search for usages before removing it. ProjectConfigPlugin is one known user.
 * *******************************
 * </pre>
 * @deprecated this always returns true, so there's no need to use it, since 7.0.1
 * @since 7.0
 */
@Deprecated
@Internal
public class ProjectTypesEnabledCondition extends AbstractWebCondition {

    public ProjectTypesEnabledCondition() {

    }

    @Override
    public boolean shouldDisplay(final ApplicationUser user, final JiraHelper jiraHelper) {
        return true;
    }
}
