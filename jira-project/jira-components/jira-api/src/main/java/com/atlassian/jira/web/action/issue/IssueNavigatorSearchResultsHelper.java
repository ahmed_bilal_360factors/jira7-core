package com.atlassian.jira.web.action.issue;

import com.atlassian.annotations.Internal;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.query.Query;

/**
 * Utility for getting search results for issue navigation
 *
 * @since v5.2
 * @deprecated  Since 7.1. Storing this on the httpSession does not work for cloud. Storing information on the httpsession is obsoleted. Please move this functionality to the frontend or other type of storage. This class will be removed in 8.0.
 */
@Internal
@Deprecated
public interface IssueNavigatorSearchResultsHelper {
    SearchResultsInfo getSearchResults(Query query, boolean isPageChanged) throws SearchException;

    void ensureAnIssueIsSelected(SearchResultsInfo searchResults, boolean isPagingToPreviousPage);

    void resetPagerAndSelectedIssue();

}
