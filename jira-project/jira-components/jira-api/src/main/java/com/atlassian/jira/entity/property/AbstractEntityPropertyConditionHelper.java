package com.atlassian.jira.entity.property;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.entity.WithId;
import com.atlassian.jira.plugin.webfragment.JiraWebContext;
import com.atlassian.jira.user.ApplicationUser;

import java.util.Map;
import java.util.Optional;

/**
 * Base class for entity property conditions.
 *
 * @param <T> - the type of entity
 * @since v7.1
 */
@PublicApi
public abstract class AbstractEntityPropertyConditionHelper<T extends WithId> implements EntityPropertyConditionHelper {

    private final EntityPropertyService<T> propertyService;
    private final Class<T> propertyEntityType;
    private final String entityName;

    protected AbstractEntityPropertyConditionHelper(EntityPropertyService<T> propertyService, Class<T> propertyEntityType, String entityName) {
        this.propertyService = propertyService;
        this.propertyEntityType = propertyEntityType;
        this.entityName = entityName;
    }

    /**
     * This implementation expects to find the entity of the class {@code T} in the context by the {@code "entityName"} key.
     * ID is then taken directly from this entity.
     *
     * @param context - context of the web element for which the condition is evaluated.
     * @return ID of the entity taken from context or empty.
     */
    @Override
    public Optional<Long> getEntityId(JiraWebContext context) {
        return context.get(entityName, propertyEntityType).map(WithId::getId);
    }

    @Override
    public final String getEntityName() {
        return entityName;
    }

    @Override
    public final EntityPropertyService.PropertyResult getProperty(ApplicationUser user, Long entityId, String propertyKey) {
        return propertyService.getProperty(user, entityId, propertyKey);
    }

}
