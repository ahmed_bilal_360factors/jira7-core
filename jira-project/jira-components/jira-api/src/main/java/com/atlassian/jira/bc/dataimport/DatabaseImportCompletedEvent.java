package com.atlassian.jira.bc.dataimport;

import com.atlassian.annotations.PublicApi;

/**
 * Raised after a JIRA has imported its database contents and before plugin data is imported.
 *
 * @since v7.2
 */
@PublicApi
public final class DatabaseImportCompletedEvent {
}
