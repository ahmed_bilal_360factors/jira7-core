package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.jira.bc.ServiceOutcome;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Set;

/**
 * Provides access to {@link com.atlassian.jira.application.ApplicationRole} management functionality.
 *
 * @since 7.0
 */
public interface ApplicationRoleAdminService {
    /**
     * The key for any group related errors reported.
     */
    String ERROR_GROUPS = "groups";

    /**
     * The key for any defaultGroup related errors reported.
     */
    String ERROR_DEFAULT_GROUPS = "defaultGroups";

    /**
     * Returns an immutable {@link Set} of all {@link com.atlassian.jira.application.ApplicationRole}s
     * that are backed by a (potentially exceeded) license.
     *
     * @return the {@link Set} of all {@link ApplicationRole}s that are backed by a (potentially exceeded) license
     * or an error.
     */
    @Nonnull
    ServiceOutcome<Set<ApplicationRole>> getRoles();

    /**
     * Save the passed list of {@link ApplicationRole} information to the database.
     * This method will only accept the passed role if:
     * <ol>
     * <li>Each role is backed by a (potentially exceeded) license.</li>
     * <li>Each roles only contains currently valid groups.</li>
     * <li>The default groups are contained within each role</li>
     * </ol>
     *
     * @param roles the roles to save
     * @return the roles as persisted to the database or an error
     */
    ServiceOutcome<Set<ApplicationRole>> setRoles(@Nonnull Collection<ApplicationRole> roles);

    /**
     * Return the {@link com.atlassian.jira.application.ApplicationRole} backed by a (potentially exceeded) license.
     *
     * @param key the key that identifies the {@code ApplicationRole}.
     * @return the {@link com.atlassian.jira.application.ApplicationRole} that is backed by a (potentially exceeded)
     * license or an error if not possible.
     */
    @Nonnull
    ServiceOutcome<ApplicationRole> getRole(@Nonnull ApplicationKey key);

    /**
     * Save the passed {@link com.atlassian.jira.application.ApplicationRole} information to the database.
     * This method will only accept the passed role if:
     * <ol>
     * <li>The role is backed by a (potentially exceeded) license.</li>
     * <li>The role only contains currently valid groups.</li>
     * <li>The default groups are contained within the role.</li>
     * </ol>
     *
     * @param role the role to save.
     * @return the role as persisted to the database or an error.
     */
    @Nonnull
    ServiceOutcome<ApplicationRole> setRole(@Nonnull ApplicationRole role);
}
