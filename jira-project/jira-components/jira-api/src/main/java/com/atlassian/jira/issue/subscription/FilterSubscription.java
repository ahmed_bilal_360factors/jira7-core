package com.atlassian.jira.issue.subscription;

import com.atlassian.jira.entity.WithId;

import javax.annotation.Nullable;
import java.util.Date;

/**
 * A Filter Subscription Entity Bean
 *
 * @since v6.2
 */
public interface FilterSubscription extends WithId {
    /**
     * Get the Id of the subscription.
     *
     * @return the Id of the subscription.
     */
    Long getId();

    /**
     * Get the Id of the filter subscribed to.
     *
     * @return the Id of the filter subscribed to.
     */
    Long getFilterId();

    /**
     * Get the User Key of the subscription owner.
     *
     * @return the  User Key of the subscription owner.
     */
    String getUserKey();

    /**
     * Get the name of the group subscribed.
     *
     * @return the  User Key of the group subscribed.
     */
    @Nullable
    String getGroupName();

    /**
     * Get the time the subscription was last sent.
     *
     * @return the time the subscription was last sent.
     */
    @Nullable
    Date getLastRunTime();

    /**
     * Is this subscription sent, even if no issues are selected by the filter.
     *
     * @return true if this subscription sent, even if no issues are selected by the filter.
     */
    boolean isEmailOnEmpty();
}
