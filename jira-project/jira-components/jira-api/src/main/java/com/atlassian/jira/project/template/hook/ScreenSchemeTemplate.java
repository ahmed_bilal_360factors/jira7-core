package com.atlassian.jira.project.template.hook;

import com.atlassian.annotations.PublicApi;

import java.util.Optional;

/**
 * A screen scheme template used for Project Template configuration.
 *
 * @since 7.0
 */
@PublicApi
public interface ScreenSchemeTemplate {
    /**
     * Returns the key of the screen scheme.
     *
     * @return The key of the screen scheme.
     */
    String key();

    /**
     * Returns the name of the screen scheme.
     *
     * @return The name of the screen scheme.
     */
    String name();

    /**
     * Returns the description of the screen scheme.
     *
     * @return The description of the screen scheme.
     */
    String description();

    /**
     * Returns the name of the default screen.
     *
     * @return The name of the default screen.
     */
    String defaultScreen();

    /**
     * Returns the name of the creation screen.
     *
     * @return The name of the creation screen.
     */
    Optional<String> createScreen();

    /**
     * Returns the name of the edit screen.
     *
     * @return The name of the edit screen.
     */
    Optional<String> editScreen();

    /**
     * Returns the name of the view screen.
     *
     * @return The name of the view screen.
     */
    Optional<String> viewScreen();
}
