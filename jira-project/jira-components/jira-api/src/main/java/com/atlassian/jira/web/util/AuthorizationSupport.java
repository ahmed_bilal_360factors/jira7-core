package com.atlassian.jira.web.util;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;

/**
 * Groups a collection of authorization checks used by JIRA's view layer.
 *
 * @see com.atlassian.jira.web.action.JiraWebActionSupport
 * @since v4.3
 */
public interface AuthorizationSupport {
    /**
     * Returns true if the logged in user has the given permission type.
     *
     * @param permissionsId the permission type
     * @return true if the logged in user has the given permission type.
     * @deprecated Use {@link #hasGlobalPermission(com.atlassian.jira.permission.GlobalPermissionKey)} instead. Since v6.4.
     */
    boolean hasPermission(int permissionsId);

    /**
     * Returns true if the logged in user has the given global permission.
     * <p>
     * This method is intended to be used in Java code. If you are using JSP / Velocity / Soy Templates, it is
     * probably easier to call {@link #hasGlobalPermission(String)} instead.
     *
     * @param globalPermissionKey the permission to check
     * @return true if the logged in user has the given global permission.
     * @see #hasGlobalPermission(String)
     */
    boolean hasGlobalPermission(GlobalPermissionKey globalPermissionKey);

    /**
     * Returns true if the logged in user has the given global permission.
     * <p>
     * This method is intended to be used in JSP / Velocity / Soy Templates. If you are using Java directly, it is
     * recommended to call {@link #hasGlobalPermission(com.atlassian.jira.permission.GlobalPermissionKey)} instead.
     * <p>
     * Note that this method takes a Global Permission Key, which is a different value to the old "permission name"
     * that some previous methods would accept - see {@link GlobalPermissionKey} for correct values of the system
     * permissions.
     *
     * @param permissionKey the permission to check
     * @return true if the logged in user has the given global permission.
     * @see #hasGlobalPermission(com.atlassian.jira.permission.GlobalPermissionKey)
     * @since 7.0
     */
    boolean hasGlobalPermission(String permissionKey);

    /**
     * Returns true if the logged in user has the given permission type on the given Issue.
     * <p>
     * This method is intended for use in Velocity templates / JSPs etc.
     * Within Java code you should prefer the method that takes a ProjectPermissionKey.
     * <p>
     * Note that this method takes a Permission Key, which is a different value to the old "permission name" that
     * some previous methods would accept - see {@link com.atlassian.jira.permission.ProjectPermissions} for correct
     * values of the system permissions.
     *
     * @param permissionKey the permission key as a String
     * @param issue         the Issue
     * @return true if the logged in user has the given permission type on the given Issue.
     * @see #hasIssuePermission(com.atlassian.jira.security.plugin.ProjectPermissionKey, com.atlassian.jira.issue.Issue)
     */
    boolean hasIssuePermission(String permissionKey, Issue issue);

    /**
     * Returns true if the logged in user has the given permission type on the given Issue.
     *
     * @param permissionsId the permission type
     * @param issue         the Issue
     * @return true if the logged in user has the given permission type on the given Issue.
     * @deprecated Use {@link #hasIssuePermission(com.atlassian.jira.security.plugin.ProjectPermissionKey, com.atlassian.jira.issue.Issue)} instead. Since v6.4.
     */
    boolean hasIssuePermission(int permissionsId, Issue issue);

    /**
     * Returns true if the logged in user has the given permission on the given Issue.
     *
     * @param projectPermissionKey the permission to check
     * @param issue                the Issue
     * @return true if the logged in user has the given permission on the given Issue.
     * @see #hasIssuePermission(String, com.atlassian.jira.issue.Issue)
     * @since 7.0
     */
    boolean hasIssuePermission(ProjectPermissionKey projectPermissionKey, Issue issue);

    /**
     * Returns true if the logged in user has the given permission type on the given Project.
     *
     * @param permissionsId the permission type
     * @param project       the Project
     * @return true if the logged in user has the given permission type on the given Project.
     * @deprecated Use {@link #hasProjectPermission(com.atlassian.jira.security.plugin.ProjectPermissionKey, com.atlassian.jira.project.Project)} instead. Since v6.4.
     */
    boolean hasProjectPermission(int permissionsId, Project project);

    /**
     * Returns true if the logged in user has the given permission on the given project.
     *
     * @param projectPermissionKey the permission to check
     * @param project              the project
     * @return true if the logged in user has the given permission on the given Issue.
     */
    boolean hasProjectPermission(ProjectPermissionKey projectPermissionKey, Project project);

}
