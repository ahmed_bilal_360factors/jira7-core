package com.atlassian.jira.event.issuetype.property;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.entity.property.EntityProperty;
import com.atlassian.jira.event.entity.AbstractPropertyEvent;
import com.atlassian.jira.event.entity.EntityPropertySetEvent;
import com.atlassian.jira.user.ApplicationUser;

/**
 * Event indicating an issue type property value has been set.
 *
 * @since 7.0
 */
@PublicApi
@EventName("property.set.issuetype")
public class IssueTypePropertySetEvent extends AbstractPropertyEvent implements EntityPropertySetEvent {
    public IssueTypePropertySetEvent(final EntityProperty entityProperty, final ApplicationUser user) {
        super(entityProperty, user);
    }
}
