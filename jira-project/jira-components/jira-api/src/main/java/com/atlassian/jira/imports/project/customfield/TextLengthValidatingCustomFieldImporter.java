package com.atlassian.jira.imports.project.customfield;

import com.atlassian.annotations.Internal;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapper;
import com.atlassian.jira.issue.fields.TextFieldCharacterLengthValidator;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;

/**
 * Implements ProjectCustomFieldImporter for limited length text custom fields. Rejects values that exceed the character
 * limit.
 */
@Internal
public class TextLengthValidatingCustomFieldImporter implements ProjectCustomFieldImporter {
    private final TextFieldCharacterLengthValidator textFieldCharacterLengthValidator;

    public TextLengthValidatingCustomFieldImporter(final TextFieldCharacterLengthValidator textFieldCharacterLengthValidator) {
        this.textFieldCharacterLengthValidator = textFieldCharacterLengthValidator;
    }

    public MessageSet canMapImportValue(final ProjectImportMapper projectImportMapper, final ExternalCustomFieldValue customFieldValue, final FieldConfig fieldConfig, final I18nHelper i18n) {
        final int length = customFieldValue.getValue().length();
        if (textFieldCharacterLengthValidator.isTextTooLong(length)) {
            final MessageSet messageSet = new MessageSetImpl();
            messageSet.addErrorMessage(i18n.getText("admin.errors.project.import.field.text.too.long",
                    String.valueOf(length),
                    String.valueOf(textFieldCharacterLengthValidator.getMaximumNumberOfCharacters())));
            return messageSet;
        } else {
            return null;
        }
    }

    public ProjectCustomFieldImporter.MappedCustomFieldValue getMappedImportValue(final ProjectImportMapper projectImportMapper, final ExternalCustomFieldValue customFieldValue, final FieldConfig fieldConfig) {
        // No mapping required, just return the original String.
        return new ProjectCustomFieldImporter.MappedCustomFieldValue(customFieldValue.getValue());
    }
}