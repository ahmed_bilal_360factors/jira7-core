package com.atlassian.jira.bc;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.util.ErrorCollection;

import javax.annotation.Nonnull;
import java.util.function.Function;

/**
 * A service result that has a value.
 *
 * @since v4.2
 */
@PublicApi
public interface ServiceOutcome<T> extends ServiceResult {

    /**
     * Returns the value that was returned by the service, or null.
     *
     * @return the value returned by the service, or null
     */
    T getReturnedValue();

    /**
     * Returns the value that was returned by the service, or null.
     *
     * @return the value returned by the service, or null
     * @since 6.2
     */
    T get();

    /**
     * Folds this outcome into a value using the given functions. Note that this
     * method does not provide access to any warnings returned by {@link #getWarningCollection()}.
     * If your use case requires you to process warnings, use the other methods in
     * this interface.
     *
     * @param onSuccess the function to apply when {@link #isValid()} returns true
     * @param onFailure the function to apply when {@link #isValid()} returns false
     * @param <R> the return type
     * @return see above
     * @since 7.1
     */
    default <R> R fold(@Nonnull Function<? super T, R> onSuccess,
                       @Nonnull Function<? super ErrorCollection, R> onFailure) {
        if (isValid()) {
            return onSuccess.apply(get());
        }
        return onFailure.apply(getErrorCollection());
    }
}
