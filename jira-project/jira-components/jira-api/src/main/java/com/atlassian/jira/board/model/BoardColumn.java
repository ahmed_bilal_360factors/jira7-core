package com.atlassian.jira.board.model;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.jira.issue.status.Status;
import com.google.common.collect.ImmutableList;

import java.util.List;
import java.util.Objects;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Represents a column in a {@link com.atlassian.jira.board.model.BoardData}.
 * 
 * @since 7.1
 */
@ExperimentalApi
public class BoardColumn {
    private final Status name;
    private final List<Status> statuses;

    public BoardColumn(Status name, List<Status> statuses) {
        this.name = notNull(name);
        this.statuses = ImmutableList.copyOf(notNull(statuses));
    }

    public Status getName() {
        return name;
    }

    public List<Status> getStatuses() {
        return statuses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BoardColumn that = (BoardColumn) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(statuses, that.statuses);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, statuses);
    }

    @Override
    public String toString() {
        return "BoardColumn{" +
                "name=" + name +
                ", statuses=" + statuses +
                '}';
    }
}
