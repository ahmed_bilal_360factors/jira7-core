package com.atlassian.jira.project.type;

import com.atlassian.annotations.PublicApi;

/**
 * Represents the key for a project type.
 *
 * @since 7.0
 */
@PublicApi
public class ProjectTypeKey {
    private final String key;

    public ProjectTypeKey(final String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final ProjectTypeKey that = (ProjectTypeKey) o;

        return !(key != null ? !key.equals(that.key) : that.key != null);

    }

    @Override
    public int hashCode() {
        return key != null ? key.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "ProjectTypeKey{" +
                "key='" + key + '\'' +
                '}';
    }
}
