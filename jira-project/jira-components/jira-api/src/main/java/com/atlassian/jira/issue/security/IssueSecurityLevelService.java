package com.atlassian.jira.issue.security;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;


/**
 * Service which can be used to add or edit security levels.
 * It validates the name provided before creating or updating IssueSecurityLevel
 *
 * @since 7.1
 */
@PublicApi
public interface IssueSecurityLevelService {

    /**
     * Method that will validate arguments for an update of IssueSecurityLevel operation
     *
     * @param user           the user who tries to validate an IssueSecurityLevel
     * @param currentLevel   the level to update
     * @param newName        the new name current level will be updated with
     * @param newDescription the new description current level will be updated with
     * @return validation result for provided name and description
     */
    UpdateValidationResult validateUpdate(ApplicationUser user, IssueSecurityLevel currentLevel, String newName, String newDescription);

    /**
     * This method will update IssueSecurityLevel carried in UpdateValidationResult if it is valid.
     * It will throw an Exception otherwise
     *
     * @param callingUser      the user who updates the level
     * @param validationResult it carry validation data and IssueSecurityLevel to update
     * @return an IssueSecurityLevel that has been updated
     */
    ServiceOutcome<IssueSecurityLevel> update(ApplicationUser callingUser, UpdateValidationResult validationResult);

    /**
     * Method that will validate arguments for and creation of IssueSecurityLevel operation
     *
     * @param user        the user who is trying to create an IssueSecurityLevel
     * @param schemeId    the scheme that new IssueSecurityLevel will belong to
     * @param name        the name for the new security level
     * @param description the description for the new security level
     * @return validation result for provided arguments
     */
    CreateValidationResult validateCreate(ApplicationUser user, long schemeId, String name, String description);

    /**
     * This method will create new IssueSecurityLevel carried int CreateValidationResult if it's valid
     * it will throw an Exception otherwise
     *
     * @param callingUser      the user who updates the level
     * @param validationResult it carry validation data and IssueSecurityLevel to update
     * @return
     */
    ServiceOutcome<IssueSecurityLevel> create(ApplicationUser callingUser, CreateValidationResult validationResult);


    /**
     * A simple object that holds the information about validating an create IssueSecurityLevel operation. This object should not be
     * constructed directly, you should invoke the {@link com.atlassian.jira.issue.security.IssueSecurityLevelService#validateCreate(ApplicationUser, long, String, String)}
     * method to obtain this.
     */
    class CreateValidationResult extends LevelValidationResult {
        CreateValidationResult(IssueSecurityLevel level, ErrorCollection errors) {
            super(level, errors);
        }
    }

    /**
     * A simple object that holds the information about validating an update IssueSecurityLevel operation. This object should not be
     * constructed directly, you should invoke the {@link com.atlassian.jira.issue.security.IssueSecurityLevelService#validateUpdate(ApplicationUser, IssueSecurityLevel, String, String)}
     * method to obtain this.
     */
    class UpdateValidationResult extends LevelValidationResult {
        UpdateValidationResult(IssueSecurityLevel level, ErrorCollection errors) {
            super(level, errors);
        }
    }

    abstract class LevelValidationResult {
        private final IssueSecurityLevel level;
        private final ErrorCollection errors;

        public IssueSecurityLevel getLevel() {
            return level;
        }

        public ErrorCollection getErrors() {
            return errors;
        }

        public boolean isValid() {
            return !this.getErrors().hasAnyErrors();
        }

        protected LevelValidationResult(IssueSecurityLevel level, ErrorCollection errors) {
            this.level = level;
            this.errors = errors;
        }

    }
}
