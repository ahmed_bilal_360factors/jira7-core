package com.atlassian.jira.index;

import com.atlassian.annotations.ExperimentalSpi;
import org.apache.lucene.document.Document;

import java.util.Set;

/**
 * Provides ability to add fields to Document during indexing
 *
 * @since 6.2
 */
@ExperimentalSpi
public interface EntitySearchExtractor<T> {
    @ExperimentalSpi
    interface Context<T> {

        T getEntity();

        /**
         * currently as for 6.2 one of issues, comments, changes, worklogs.   See constants in
         * {@code SearchProviderFactory} for available indexes.
         *
         * @return index name
         */
        String getIndexName();

    }

    /**
     * Extracts fields from document provided by {@link Context} and adds them to document.
     * All the filed names that were added to documents as indexed to document must be returned as output of this method
     *
     * @param ctx context for this operation
     * @param doc lucene document to which values should be added
     * @return ids of the document fields that were added by call of this method
     */
    Set<String> indexEntity(Context<T> ctx, Document doc);
}
