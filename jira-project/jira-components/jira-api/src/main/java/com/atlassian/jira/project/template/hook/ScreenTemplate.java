package com.atlassian.jira.project.template.hook;

import com.atlassian.annotations.PublicApi;

import java.util.List;

/**
 * A screen template used for Project Template configuration.
 *
 * @since 7.0
 */
@PublicApi
public interface ScreenTemplate {
    /**
     * Returns the key of the screen template.
     *
     * @return The key of the screen template.
     */
    String key();

    /**
     * Returns the name of the screen template.
     *
     * @return The name of the screen template.
     */
    String name();

    /**
     * Returns the description of the screen template.
     *
     * @return The description of the screen template.
     */
    String description();

    /**
     * Returns the list of tabs for this screen.
     *
     * @return The list of tabs for this screen.
     */
    List<ScreenTabTemplate> tabs();
}
