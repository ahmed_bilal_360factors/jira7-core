package com.atlassian.jira.issue.customfields.converters;

import com.atlassian.annotations.Internal;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.project.Project;

@Internal
public interface ProjectConverter {
    /**
     * Get the String value that represents the given Project.
     *
     * @param project Project to convert
     * @return the String value that represents the given Project.
     */
    public String getString(Project project);

    /**
     * Get the Project that this String value represents.
     * <p>
     * Same as getProject() and exists for legacy reasons.
     *
     * @param stringValue the String representation.
     * @return the Project that this String value represents.
     * @throws com.atlassian.jira.issue.customfields.impl.FieldValidationException if we are unable to convert the String representation.
     */
    public Project getProjectObject(String stringValue) throws FieldValidationException;

    /**
     * Get the Project for the given ID.
     * If a null projectId is passed in, then a null Project is returned.
     * <p>
     * Same as getProject() and exists for legacy reasons.
     *
     * @param projectId the Project ID.
     * @return the Project for the given ID.
     * @throws com.atlassian.jira.issue.customfields.impl.FieldValidationException if the Project ID is invalid.
     */
    public Project getProjectObject(Long projectId) throws FieldValidationException;

    /**
     * Get the Project that this String value represents.
     *
     * @param stringValue the String representation.
     * @return the Project that this String value represents.
     * @throws com.atlassian.jira.issue.customfields.impl.FieldValidationException if we are unable to convert the String representation.
     */
    public Project getProject(String stringValue) throws FieldValidationException;

    /**
     * Get the Project for the given ID.
     *
     * @param projectId the Project ID.
     * @return the Project for the given ID.
     * @throws com.atlassian.jira.issue.customfields.impl.FieldValidationException if the Project ID is invalid.
     */
    public Project getProject(Long projectId) throws FieldValidationException;
}
