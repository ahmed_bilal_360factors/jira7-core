package com.atlassian.jira.issue.context;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.util.collect.MapBuilder;

import java.util.Map;

@PublicApi
public class GlobalIssueContext extends AbstractJiraContext {
    public static final String GLOBAL_CONTEXT_STR = "Global Context";

    private static final GlobalIssueContext INSTANCE = new GlobalIssueContext();

    private GlobalIssueContext() {
    }

    @Override
    public Map<String, Object> appendToParamsMap(final Map<String, Object> input) {
        return MapBuilder.newBuilder(input)
                .add(FIELD_PROJECT, null)
                .toMap();
        // props.put(FIELD_ISSUE_TYPE, null);
    }

    @Override
    public IssueType getIssueTypeObject() {
        return null;
    }

    @Override
    public IssueType getIssueType() {
        return null;
    }

    public String getIssueTypeId() {
        return null;
    }

    @Override
    public Project getProjectObject() {
        return null;
    }

    @Override
    public Long getProjectId() {
        return null;
    }

    @Override
    public boolean isInContext(final IssueContext issueContext) {
        return true;
    }

    @Override
    public JiraContextNode getParent() {
        return null;
    }

    @Override
    public boolean hasParentContext() {
        return false;
    }

    public static JiraContextNode getInstance() {
        return INSTANCE;
    }


    @Override
    public String toString() {
        return GLOBAL_CONTEXT_STR;
    }

}
