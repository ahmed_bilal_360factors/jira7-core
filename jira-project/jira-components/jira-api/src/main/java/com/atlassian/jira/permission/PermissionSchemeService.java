package com.atlassian.jira.permission;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.List;

/**
 * Service for managing permission schemes.
 */
@ParametersAreNonnullByDefault
@PublicApi
public interface PermissionSchemeService {
    /**
     * Returns all permission schemes sorted alphabetically by name.
     * <p>
     * Only schemes the user has access to will be returned. That means all the schemes if the user
     * is a JIRA administrator, or the schemes assigned to projects the user administers.
     * </p>
     *
     * @param user user that performs the operation
     * @return A service outcome containing an immutable list of permission schemes or an error collection if something went wrong.
     */
    ServiceOutcome<? extends List<PermissionScheme>> getPermissionSchemes(@Nullable ApplicationUser user);

    /**
     * Returns a permission scheme with the specified id.
     * <p>
     * The user needs to have access to the permission scheme. That means the user needs
     * to be a JIRA administrator or administer a project which has the scheme assigned.
     * </p>
     *
     * @param user user that performs the operation
     * @param id   identifier of the permission scheme
     * @return A service outcome containing the permission scheme or an error collection if something went wrong.
     */
    ServiceOutcome<PermissionScheme> getPermissionScheme(@Nullable ApplicationUser user, Long id);

    /**
     * Creates a new permission scheme. Permission scheme entities will also be created if provided.
     * <p>
     * The permission scheme is validated thoroughly, e.g. if there is a permission for a group, then the
     * group must exist, ditto for project roles, users, custom fields etc. Custom fields
     * must be of proper types.
     * </p>
     * <p>
     * Note that permission scheme names must be unique.
     * </p>
     *
     * @param user             user that performs the operation. The user needs to be a JIRA administrator.
     * @param permissionScheme permission scheme to create
     * @return A service outcome containing the created permission scheme or an error collection if something went wrong.
     */
    ServiceOutcome<PermissionScheme> createPermissionScheme(@Nullable ApplicationUser user, PermissionSchemeInput permissionScheme);

    /**
     * Updates a permission scheme identified by the given id. Simple fields like name and description will be updated
     * as well as permission scheme entities. After the update, the stored permission scheme will be exactly the same as the argument.
     * <p>
     * Validation is the same as in the {@link #createPermissionScheme} method.
     * </p>
     *
     * @param user             user that performs the operation. The user needs to be a JIRA administrator.
     * @param id               id of the scheme to be updated
     * @param permissionScheme new data of the permission scheme
     * @return A service outcome containing the updated permission scheme or an error collection if something went wrong.
     */
    ServiceOutcome<PermissionScheme> updatePermissionScheme(@Nullable ApplicationUser user, Long id, PermissionSchemeInput permissionScheme);

    /**
     * Deletes a permission scheme.
     *
     * @param user user that performs the operation. The user needs to be a JIRA administrator.
     * @param id   identifier of the permission scheme to be deleted
     * @return A service result marked as valid if successful, error collection otherwise
     */
    ServiceResult deletePermissionScheme(@Nullable ApplicationUser user, Long id);

    /**
     * Sets the specified permission scheme in the given project.
     *
     * @param user      user that performs the operation. The user needs to be a JIRA administrator.
     * @param schemeId  scheme to assign to the project
     * @param projectId project to assign the scheme to
     * @return service result marked as valid if successful, error collection otherwise
     */
    ServiceResult assignPermissionSchemeToProject(@Nullable ApplicationUser user, Long schemeId, Long projectId);

    /**
     * Returns a permission scheme assigned to the specified project.
     * <p>
     * If there is no scheme explicitly assigned, the default scheme is returned.
     * </p>
     *
     * @param user      user that performs the operation. The user needs a permission to administer the project.
     * @param projectId identifier of the project
     * @return A service outcome containing the permission scheme or an error collection if something went wrong.
     */
    ServiceOutcome<PermissionScheme> getSchemeAssignedToProject(@Nullable ApplicationUser user, Long projectId);
}
