package com.atlassian.jira.board;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.board.model.BoardData;
import com.atlassian.jira.user.ApplicationUser;

/**
 * Provides services for performing operations on a board's data, which describes the
 * issues, statuses, assignees and columns for a given board.
 *
 * @since 7.1
 */
@ExperimentalApi
public interface BoardDataService {
    /**
     * As the given user, get the data for a board. This will return an object containing
     * the issues, statuses, assignees and columns for the board. The columns will also be
     * ordered based on the status they contain, with the initial statuses of all workflows
     * in the board's query context appearing first, and then the rest sorted first by status
     * category and then by status id.
     *
     * There will be an error if an exception occurs while retrieving the board's issues,
     * or if there is an error while parsing the board JQL.
     * 
     * @param user  the user to retrieve the data for the board as
     * @param board the board to get the data for
     * @return      the board data if successful,
     *              or {@link com.atlassian.jira.util.ErrorCollection.Reason.VALIDATION_FAILED}
     *              if there was an error parsing the JQL,
     *              or {@link com.atlassian.jira.util.ErrorCollection.Reason.SERVER_ERROR}
     *              if there was an exception while searching
     */
    ServiceOutcome<BoardData> getDataForBoard(ApplicationUser user, Board board);
}
