package com.atlassian.jira.issue.fields.option;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.issue.IssueConstant;
import com.atlassian.jira.issue.fields.config.FieldConfig;

import java.util.Collection;

/**
 * A set of options representing the listing order for issue constants.
 * <p>
 * For example, the {@link FieldConfig field configuration} for a project might change the order that
 * issue types are listed in from the default order to some other order and need not include all of the
 * issue types that exist.
 * </p>
 *
 * @since 3.4
 */
@PublicApi
public interface OptionSet {
    /**
     * List of options for this set
     *
     * @return List of {@link Option} objects
     */
    Collection<Option> getOptions();

    /**
     * Get the list of options ids
     *
     * @return List of {@link String} objects
     */
    Collection<String> getOptionIds();

    /**
     * Adds the option to the underlying list.
     * <p>
     * Note that this does not write through to the database.
     * Use {@link OptionSetManager#updateOptionSet(FieldConfig, Collection)} for that.
     * </p>
     *
     * @param constantType the type of issue constant that the new option will represent, such as an IssueType
     * @param constantId the {@link IssueConstant#getId() issue constant ID} for the issue constant that this
     *                   option will represent.
     */
    void addOption(String constantType, String constantId);
}
