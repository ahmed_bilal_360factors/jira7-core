package com.atlassian.jira.issue.fields;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.project.Project;

/**
 * Represents the Project System Field.
 *
 * @since v4.3
 */
@PublicApi
public interface ProjectField extends OrderableField<Project>, NavigableField, HideableField, RequirableField {
}
