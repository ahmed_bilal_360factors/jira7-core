package com.atlassian.jira.util;

import com.google.common.collect.ImmutableList;

import javax.annotation.Nonnull;
import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * A simple implementation of the warning collection interface
 *
 * @since v7.0.0
 */
public class SimpleWarningCollection implements WarningCollection, Serializable {
    private static final long serialVersionUID = 1667504127494141328L;

    private final List<String> warnings = new LinkedList<>();

    /**
     * Creates an empty warning collection.
     */
    public SimpleWarningCollection() {
    }

    /**
     * Copies warnings from an existing warning collection.
     *
     * @param warningsCollection the original collection to copy.
     *
     * @since v7.1.8
     */
    public SimpleWarningCollection(@Nonnull WarningCollection warningsCollection){
        this.warnings.addAll(requireNonNull(warningsCollection.getWarnings()));
    }

    @Override
    public void addWarning(final String warningMessage) {
        warnings.add(warningMessage);
    }

    @Override
    public Collection<String> getWarnings() {
        return ImmutableList.copyOf(warnings);
    }

    @Override
    public boolean hasAnyWarnings() {
        return !warnings.isEmpty();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final SimpleWarningCollection that = (SimpleWarningCollection) o;

        return !(warnings != null ? !warnings.equals(that.warnings) : that.warnings != null);

    }

    @Override
    public int hashCode() {
        return warnings != null ? warnings.hashCode() : 0;
    }
}
