package com.atlassian.jira.event.user;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.user.ApplicationUser;

/**
 * Published when a user logs out.
 */
@PublicApi
public final class LogoutEvent extends UserEvent {
    public LogoutEvent(ApplicationUser user) {
        super(user, UserEventType.USER_LOGOUT);
    }
}
