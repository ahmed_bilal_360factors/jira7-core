package com.atlassian.jira.issue.context;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.context.manager.JiraContextTreeManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.util.collect.MapBuilder;

import java.util.Map;

@PublicApi
public class ProjectContext extends AbstractJiraContext {
    protected Long projectCategory;
    protected Long projectId;

    private final ProjectManager projectManager;

    public ProjectContext(final Long projectId) {
        this(projectId, ComponentAccessor.getProjectManager());
    }

    /**
     * @deprecated Use {@link #ProjectContext(Long, com.atlassian.jira.project.ProjectManager)} instead. Since v6.4.
     */
    public ProjectContext(final Long projectId, JiraContextTreeManager treeManager) {
        this.projectId = projectId;
        if (treeManager == null) {
            projectManager = ComponentAccessor.getProjectManager();
        } else {
            projectManager = treeManager.getProjectManager();
        }
    }

    /**
     * @since 6.4
     */
    public ProjectContext(final Long projectId, ProjectManager projectManager) {
        this.projectId = projectId;
        this.projectManager = projectManager;
    }

    /**
     * @deprecated Use {@link #ProjectContext(Long, com.atlassian.jira.project.ProjectManager)} instead. Since v6.4.
     */
    public ProjectContext(final Project project, final JiraContextTreeManager treeManager) {
        this((project != null) ? project.getId() : null, treeManager);
    }

    /**
     * @deprecated Use {@link #ProjectContext(Long, com.atlassian.jira.project.ProjectManager)} instead. Since v6.4.
     */
    public ProjectContext(final IssueContext issueContext, final JiraContextTreeManager treeManager) {
        this(issueContext.getProjectId(), treeManager);
    }

    public JiraContextNode getParent() {
        return GlobalIssueContext.getInstance();
    }

    public boolean hasParentContext() {
        return true;
    }

    public Map<String, Object> appendToParamsMap(final Map<String, Object> input) {
        return MapBuilder.newBuilder(input).add(FIELD_PROJECT, projectId).toMap();
    }

    public Project getProjectObject() {
        if (projectId == null) {
            return null;
        }
        return projectManager.getProjectObj(projectId);
    }

    public Long getProjectId() {
        return projectId;
    }

    public IssueType getIssueTypeObject() {
        return null;
    }

    public IssueType getIssueType() {
        return null;
    }

    public String getIssueTypeId() {
        return null;
    }

    @SuppressWarnings("RedundantIfStatement")
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if ((o == null) || !(o instanceof JiraContextNode)) {
            return false;
        }

        final ProjectContext projectContext = (ProjectContext) o;
        if (projectId != null ? !projectId.equals(projectContext.projectId) : projectContext.projectId != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return 59 * (projectId != null ? projectId.hashCode() : 0) + 397;
    }

    @Override
    public String toString() {
        return "ProjectContext[projectCategoryId=" + projectCategory + ",projectId=" + projectId + ']';
    }
}
