package com.atlassian.jira.board;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * Provide services for storing and retrieving a board's configuration.
 * @since v7.1
 */
@ExperimentalApi
public interface BoardService {
    /**
     * Create a default board for the specified project. Call this instead of createBoard when a board should be created as part of project creation.
     * Only a user with EDIT_PROJECT_CONFIG {@link com.atlassian.jira.bc.project.ProjectAction} permission can perform this operation.
     * We don't validate jql here in order to allow admin without view project permission still can create the board.
     * Otherwise, this method will return a no permission error.
     * It returns the newly created board if it's created successfully.
     *
     * @param user      the user creating the board
     * @param projectId the project id
     * @return the newly created board if it's created successfully, an ErrorCollection otherwise.
     */
    ServiceOutcome<Board> createDefaultBoardForProject(ApplicationUser user, long projectId);

    /**
     * Create a board according to the data provided.
     * Only user with CREATE_SHARED_OBJECTS {@link com.atlassian.jira.permission.GlobalPermissionKey} can perform this operation.
     * Otherwise, this method will return a no permission error.
     *
     * @param user              the user creating the board
     * @param boardCreationData board creation data
     * @return the newly created board if it's created successfully, otherwise get an ErrorCollection.
     */
    ServiceOutcome<Board> createBoard(ApplicationUser user, @Nonnull BoardCreationData boardCreationData);

    /**
     * Get a list of boards related to the project.
     * Only user with VIEW_PROJECT {@link com.atlassian.jira.bc.project.ProjectAction} permission can perform this operation.
     * Otherwise, this method will return a no permission error.
     * @param user  the user to get the boards
     * @param projectId the id of the project to link this board to
     * @return a list of boards related to the project.
     */
    ServiceOutcome<List<Board>> getBoardsForProject(ApplicationUser user, long projectId);

    /**
     * Returns whether there is at least one visible board for the user in the project.
     *
     * @param user the logged in user
     * @param projectId the project id
     * @return true if there is at least one board, false otherwise
     */
    ServiceOutcome<Boolean> hasBoardInProject(ApplicationUser user, long projectId);

    /**
     * get the board for board id
     * Get the board for specified board id.
     * Use has to be able to view at least one project in the JIRA instance to perform this operation.
     * Otherwise, this method will return a no permission error.
     * @param user the user to get the board
     * @param boardId the board id
     * @return the board
     */
    ServiceOutcome<Board> getBoard(ApplicationUser user, @Nonnull BoardId boardId);

    /**
     * Delete the board.
     * Only user with CREATE_SHARED_OBJECTS {@link com.atlassian.jira.permission.GlobalPermissionKey} can perform this operation.
     * Otherwise, it'll get no permission error.
     * It'll return true when the specified board is deleted successfully. It'll return false if the specified board is not found.
     * @param user  the user to delete the board
     * @param boardId board id
     * @return return true if the board is deleted.
     */
    ServiceOutcome<Boolean> deleteBoard(ApplicationUser user, @Nonnull BoardId boardId);
}
