package com.atlassian.jira.issue.fields;

import com.atlassian.jira.project.version.Version;

import java.util.Collection;

/**
 * @since v5.0
 */
public interface FixVersionsField extends HideableField, RequirableField, NavigableField, OrderableField<Collection<Version>> {
}
