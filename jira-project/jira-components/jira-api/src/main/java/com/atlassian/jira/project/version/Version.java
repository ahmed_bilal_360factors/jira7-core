package com.atlassian.jira.project.version;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.entity.WithId;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectConstant;
import com.atlassian.jira.util.NamedWithDescription;

import javax.annotation.Nullable;
import java.util.Date;

/**
 * Project version. Immutable since v7.0.
 */
@PublicApi
public interface Version extends ProjectConstant, NamedWithDescription, WithId {
    /**
     * Returns project this version relates to.
     *
     * @return project this version relates to.
     */
    Project getProject();

    /**
     * Returns the ID of the project that this version belongs to.
     *
     * @return the ID of the project that this version belongs to.
     * @since v5.2
     */
    Long getProjectId();

    /**
     * Returns project this version relates to.
     * <p>Same as getProject() and exists for legacy reasons.
     *
     * @return project domain object
     * @since v3.10
     * @deprecated Please use {@link #getProject()}. Since v7.0
     */
    Project getProjectObject();

    @Nullable
    Long getId();

    String getName();

    @Nullable
    String getDescription();

    Long getSequence();

    boolean isArchived();

    boolean isReleased();

    @Nullable
    Date getReleaseDate();

    /**
     * Returns the start date of the version
     *
     * @return The start date of the version
     * @since v6.0
     */
    @Nullable
    Date getStartDate();
}