package com.atlassian.jira.project.template.hook;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;

/**
 * Response object for the "validate project" hook.
 *
 * @since 7.0
 */
@PublicApi
public class ValidateResponse {
    private final Map<String, String> errors;
    private final List<String> errorMessages;

    /**
     * Creates a new ValidateResponse.
     *
     * @return a new ValidateResponse
     */
    public static ValidateResponse create() {
        return new ValidateResponse();
    }

    private ValidateResponse() {
        this.errors = Maps.newHashMap();
        this.errorMessages = Lists.newArrayList();
    }

    /**
     * Returns a new ValidateResponse with the given field error added to it.
     *
     * @param field a field name
     * @param error an field error
     * @return this
     */
    public ValidateResponse addError(String field, String error) {
        errors.put(field, error);
        return this;
    }

    /**
     * Returns a new ValidateResponse with the given error added to it.
     *
     * @param errorMessage an error message
     * @return this
     */
    public ValidateResponse addErrorMessage(String errorMessage) {
        errorMessages.add(errorMessage);
        return this;
    }

    /**
     * Transforms the errors into a {@link ErrorCollection}.
     *
     * @return An ErrorCollection.
     */
    public ErrorCollection toErrorCollection() {
        SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        errorCollection.addErrors(errors);
        errorCollection.addErrorMessages(errorMessages);

        return errorCollection;
    }
}
