package com.atlassian.jira.bc.issuetype.property;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.jira.entity.property.EntityPropertyService;
import com.atlassian.jira.issue.issuetype.IssueTypeWithID;

/**
 * The service used to add, update, retrieve and delete properties from {@link com.atlassian.jira.issue.issuetype.IssueType}'s. Each method of this service
 * ensures that the user has permission to perform the operation. For each operation an appropriate event is published.
 *
 * @since v7.0
 */
@ExperimentalApi
public interface IssueTypePropertyService extends EntityPropertyService<IssueTypeWithID> {
}
