package com.atlassian.jira.issue.subscription;

import com.atlassian.annotations.PublicApi;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.user.ApplicationUser;
import org.ofbiz.core.entity.GenericEntityException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Date;
import java.util.List;

@PublicApi
public interface SubscriptionManager {
    public static final String SUBSCRIPTION_IDENTIFIER = "SUBSCRIPTION_ID";

    boolean hasSubscription(ApplicationUser user, Long filterId) throws GenericEntityException;

    FilterSubscription getFilterSubscription(ApplicationUser user, Long subId) throws GenericEntityException;


    List<FilterSubscription> getFilterSubscriptions(ApplicationUser user, Long filterId) throws GenericEntityException;

    /**
     * Creates a new subscription based on the passed in filter id and fired
     * in accordance with the passed in trigger
     *
     * @param user           the current user performing this operation
     * @param filterId       Id of the filter subscribing to
     * @param groupName      Sent ot group
     * @param cronExpression The Cron expression for the subscription
     * @param emailOnEmpty   send email if filter returns no results
     * @return FilterSubscription representing new subscription
     */
    FilterSubscription createSubscription(ApplicationUser user, Long filterId, String groupName, String cronExpression, Boolean emailOnEmpty);

    void deleteSubscription(Long subId) throws GenericEntityException;

    void runSubscription(Long subId) throws GenericEntityException;

    /**
     * Run this subscription now.
     *
     * @param user  the current user performing this operation
     * @param subId identifies the subscription to update
     */
    void runSubscription(ApplicationUser user, Long subId) throws GenericEntityException;

    /**
     * Get a subscription by Id
     *
     * @param subId Subscription Id
     * @return Subscription
     * @throws GenericEntityException
     * @since v6.2
     */
    FilterSubscription getFilterSubscription(Long subId) throws GenericEntityException;

    /**
     * @param user           the current user performing this operation
     * @param subscriptionId identifies the subscription to update
     * @param groupName      (optional) the name of the group to receive the email
     * @param cronExpression The Cron expression to update the subscription with
     * @throws DataAccessException if there is a problem persisting the data.
     */
    void updateSubscription(ApplicationUser user, Long subscriptionId, String groupName, String cronExpression, Boolean emailOnEmpty) throws DataAccessException;

    List<FilterSubscription> getAllFilterSubscriptions(Long filterId);

    List<FilterSubscription> getAllFilterSubscriptions();

    /**
     * Delete all subscriptions owned by a user.
     *
     * @param user the current user performing this operation
     */
    void deleteSubscriptionsForUser(ApplicationUser user) throws GenericEntityException;

    void deleteSubscriptionsForGroup(Group group) throws GenericEntityException;

    /**
     * Retrieve the cron expression associated with this subscription
     *
     * @param subscription
     * @return the cron expression associated with this subscription
     */
    String getCronExpressionForSubscription(FilterSubscription subscription);

    /**
     * Returns the next send time for this subscription.
     * This may return null if the scheduler does not support the reporting of next send times.
     *
     * @param sub The subscription
     * @return Next send time
     */
    @Nullable
    Date getNextSendTime(@Nonnull FilterSubscription sub);
}
