package com.atlassian.jira.bc.user.search;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.user.UserFilter;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Set;

/**
 * Optional parameters to restrict a user search.
 * <p>
 * This allows you to include or exclude active and inactive users and allow or disallow empty search queries.
 *
 * @since v5.1.5
 */
public class UserSearchParams {
    public static final UserSearchParams ACTIVE_USERS_IGNORE_EMPTY_QUERY = new UserSearchParams(false, true, false,
            false, null, null);
    public static final UserSearchParams ACTIVE_USERS_ALLOW_EMPTY_QUERY = new UserSearchParams(true, true, false,
            false, null, null);

    private final boolean allowEmptyQuery;
    private final boolean includeActive;
    private final boolean includeInactive;
    /**
     * Indicate whether the search would apply the query to email address as well.
     *
     * @since v6.2
     */
    private final boolean canMatchEmail;
    /**
     * The additional filters to be applied to the search.
     *
     * @since v6.2
     */
    private final UserFilter userFilter;
    /**
     * The list of project ids to be used in conjunction with the roles in {@link #userFilter}.
     *
     * @since v6.2
     */
    private final Set<Long> projectIds;

    /**
     * Filter that runs after everything else.
     *
     * @since v7.0
     */
    @Nonnull
    private final Predicate<User> postProcessingFilter;

    /**
     * The maximum number of results to retrieve.
     *
     * @since v7.0
     */
    private final Integer maxResults;

    /**
     * Whether the results must be sorted.  By default sorting is active, but it can be disabled for a performance boost.
     *
     * @since v7.0
     */
    private final boolean sorted;

    /**
     * A flag that indicates whether a browser users permission check will be performed.  If false,
     * the logged in user must have browser users permission to list users or else an empty list is always returned.
     * If true, searches are not restricted by the logged in user's permissions.
     *
     * @since v7.0
     */
    private final boolean ignorePermissionCheck;

    /**
     * Creates user search params.
     *
     * @param allowEmptyQuery if true, empty queries that would retrieve all results are allowed.
     * @param includeActive   active users are searched.
     * @param includeInactive inactive users are searched.
     */
    public UserSearchParams(boolean allowEmptyQuery, boolean includeActive, boolean includeInactive) {
        this(allowEmptyQuery, includeActive, includeInactive, false, null, null);
    }

    /**
     * Creates user search params.
     *
     * @param allowEmptyQuery if true, empty queries that would retrieve all results are allowed.
     * @param includeActive   active users are searched.
     * @param includeInactive inactive users are searched.
     * @param canMatchEmail   if search applies to email address as well.
     * @param userFilter      filter users by groups or roles.
     * @param projectIds      the list of project ids to be used in conjunction with the roles in user filter.
     * @since v6.2
     */
    public UserSearchParams(boolean allowEmptyQuery, boolean includeActive, boolean includeInactive,
                            boolean canMatchEmail,
                            final UserFilter userFilter, final Set<Long> projectIds) {
        this(allowEmptyQuery, includeActive, includeInactive, canMatchEmail, userFilter,
                projectIds, Predicates.<User>alwaysTrue());
    }

    /**
     * Creates user search params.
     *
     * @param allowEmptyQuery      if true, empty queries that would retrieve all results are allowed.
     * @param includeActive        active users are searched.
     * @param includeInactive      inactive users are searched.
     * @param canMatchEmail        if search applies to email address as well.
     * @param userFilter           filter users by groups or roles.
     * @param projectIds           the list of project ids to be used in conjunction with the roles in user filter.
     * @param postProcessingFilter a filter applied after search results are retrieved.  Use this for more complex
     *                             filter logic that can't be expressed in <code>userFilter</code> and the other parameters.
     * @since v7.0
     */
    public UserSearchParams(boolean allowEmptyQuery, boolean includeActive, boolean includeInactive,
                            boolean canMatchEmail,
                            final UserFilter userFilter, final Set<Long> projectIds,
                            final Predicate<User> postProcessingFilter) {
        this(allowEmptyQuery, includeActive, includeInactive, canMatchEmail, userFilter,
                projectIds, postProcessingFilter, null, true, false);
    }

    /**
     * Creates user search params.
     *
     * @param allowEmptyQuery      if true, empty queries that would retrieve all results are allowed.
     * @param includeActive        active users are searched.
     * @param includeInactive      inactive users are searched.
     * @param canMatchEmail        if search applies to email address as well.
     * @param userFilter           filter users by groups or roles.
     * @param projectIds           the list of project ids to be used in conjunction with the roles in user filter.
     * @param postProcessingFilter a filter applied after search results are retrieved.  Use this for more complex
     *                             filter logic that can't be expressed in <code>userFilter</code> and the other parameters.
     * @param maxResults           the maximum number of results to retrieve.  Use <code>null</code> for unlimited results.
     * @since v7.0
     */
    public UserSearchParams(boolean allowEmptyQuery, boolean includeActive, boolean includeInactive,
                            boolean canMatchEmail,
                            final UserFilter userFilter, final Set<Long> projectIds,
                            final Predicate<User> postProcessingFilter,
                            Integer maxResults) {
        this(allowEmptyQuery, includeActive, includeInactive, canMatchEmail, userFilter, projectIds,
                postProcessingFilter, maxResults, true, false);
    }

    /**
     * Creates user search params.
     *
     * @param allowEmptyQuery      if true, empty queries that would retrieve all results are allowed.
     * @param includeActive        active users are searched.
     * @param includeInactive      inactive users are searched.
     * @param canMatchEmail        if search applies to email address as well.
     * @param userFilter           filter users by groups or roles.
     * @param projectIds           the list of project ids to be used in conjunction with the roles in user filter.
     * @param postProcessingFilter a filter applied after search results are retrieved.  Use this for more complex
     *                             filter logic that can't be expressed in <code>userFilter</code> and the other parameters.
     * @param maxResults           the maximum number of results to retrieve.  Use <code>null</code> for unlimited results.
     * @param sorted               if true, results are guaranteed to be sorted.  If false, results may be unsorted which might
     *                             have better performance.
     * @since v7.0
     */
    public UserSearchParams(boolean allowEmptyQuery, boolean includeActive, boolean includeInactive,
                            boolean canMatchEmail,
                            final UserFilter userFilter, final Set<Long> projectIds,
                            final Predicate<User> postProcessingFilter,
                            Integer maxResults,
                            boolean sorted) {
        this(allowEmptyQuery, includeActive, includeInactive, canMatchEmail, userFilter, projectIds,
                postProcessingFilter, maxResults, sorted, false);
    }

    /**
     * Creates user search params.
     *
     * @param allowEmptyQuery       if true, empty queries that would retrieve all results are allowed.
     * @param includeActive         active users are searched.
     * @param includeInactive       inactive users are searched.
     * @param canMatchEmail         if search applies to email address as well.
     * @param userFilter            filter users by groups or roles.
     * @param projectIds            the list of project ids to be used in conjunction with the roles in user filter.
     * @param postProcessingFilter  a filter applied after search results are retrieved.  Use this for more complex
     *                              filter logic that can't be expressed in <code>userFilter</code> and the other parameters.
     * @param maxResults            the maximum number of results to retrieve.  Use <code>null</code> for unlimited results.
     * @param sorted                if true, results are guaranteed to be sorted.  If false, results may be unsorted which might
     *                              have better performance.
     * @param ignorePermissionCheck a flag that indicates whether a browser users permission check will be performed.
     * @since v7.0
     */
    public UserSearchParams(boolean allowEmptyQuery, boolean includeActive, boolean includeInactive,
                            boolean canMatchEmail,
                            final UserFilter userFilter, final Set<Long> projectIds,
                            final Predicate<User> postProcessingFilter,
                            Integer maxResults,
                            boolean sorted,
                            boolean ignorePermissionCheck) {
        this.allowEmptyQuery = allowEmptyQuery;
        this.includeActive = includeActive;
        this.includeInactive = includeInactive;
        this.canMatchEmail = canMatchEmail;
        this.userFilter = userFilter;
        this.projectIds = projectIds;
        if (postProcessingFilter == null)
            this.postProcessingFilter = Predicates.alwaysTrue();
        else
            this.postProcessingFilter = postProcessingFilter;

        this.maxResults = maxResults;
        this.sorted = sorted;
        this.ignorePermissionCheck = ignorePermissionCheck;
    }

    public boolean allowEmptyQuery() {
        return allowEmptyQuery;
    }

    public boolean includeActive() {
        return includeActive;
    }

    public boolean includeInactive() {
        return includeInactive;
    }

    public boolean canMatchEmail() {
        return canMatchEmail;
    }

    public UserFilter getUserFilter() {
        return userFilter;
    }

    public Set<Long> getProjectIds() {
        return projectIds;
    }

    /**
     * @since v7.0
     */
    public boolean ignorePermissionCheck() {
        return ignorePermissionCheck;
    }

    /**
     * @since v7.0
     */
    @Nonnull
    public Predicate<User> getPostProcessingFilter() {
        return postProcessingFilter;
    }

    /**
     * @since v7.0
     */
    public Integer getMaxResults() {
        return maxResults;
    }

    /**
     * @since v7.0
     */
    public boolean isSorted() {
        return sorted;
    }

    @Override
    public boolean equals(final Object o) {
        return EqualsBuilder.reflectionEquals(this, o);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Builder builder(UserSearchParams prototype) {
        return new Builder(prototype);
    }

    public static class Builder {
        private boolean allowEmptyQuery = false;
        private boolean includeActive = true;
        private boolean includeInactive = false;
        private boolean canMatchEmail = false;
        private UserFilter userFilter = null;
        private Set<Long> projectIds = null;
        private Predicate<User> postProcessingFilter;
        private Integer maxResults;
        private boolean sorted = true;
        private boolean ignorePermissionCheck = false;

        public Builder() {
        }

        private Builder(UserSearchParams prototype) {
            this.allowEmptyQuery = prototype.allowEmptyQuery;
            this.includeActive = prototype.includeActive;
            this.includeInactive = prototype.includeInactive;
            this.canMatchEmail = prototype.canMatchEmail;
            this.userFilter = prototype.userFilter;
            this.projectIds = prototype.projectIds;
            this.postProcessingFilter = prototype.postProcessingFilter;
            this.maxResults = prototype.maxResults;
            this.sorted = prototype.sorted;
            this.ignorePermissionCheck = prototype.ignorePermissionCheck;
        }

        public UserSearchParams build() {
            return new UserSearchParams(allowEmptyQuery, includeActive, includeInactive, canMatchEmail,
                    userFilter, projectIds, postProcessingFilter, maxResults, sorted, ignorePermissionCheck);
        }

        /**
         * @param allowEmptyQuery if true, empty queries that would retrieve all results are allowed.
         */
        public Builder allowEmptyQuery(boolean allowEmptyQuery) {
            this.allowEmptyQuery = allowEmptyQuery;
            return this;
        }

        /**
         * @param includeActive active users are searched.
         */
        public Builder includeActive(boolean includeActive) {
            this.includeActive = includeActive;
            return this;
        }

        /**
         * @param includeInactive inactive users are searched.
         */
        public Builder includeInactive(boolean includeInactive) {
            this.includeInactive = includeInactive;
            return this;
        }

        /**
         * @param canMatchEmail if search applies to email address as well.
         */
        public Builder canMatchEmail(boolean canMatchEmail) {
            this.canMatchEmail = canMatchEmail;
            return this;
        }

        /**
         * @param userFilter filter users by groups or roles.
         * @since v6.2
         */
        public Builder filter(UserFilter userFilter) {
            this.userFilter = userFilter;
            return this;
        }

        /**
         * @param projectIds the list of project ids to be used in conjunction with the roles in user filter.
         * @since v6.2
         */
        public Builder filterByProjectIds(Collection<Long> projectIds) {
            this.projectIds = projectIds == null ? null : ImmutableSet.copyOf(projectIds);
            return this;
        }

        /**
         * @param postProcessingFilter a filter applied after search results are retrieved.  Use this for more complex
         *                             filter logic that can't be expressed in a {@link UserFilter} or other parameters.
         * @since v7.0
         */
        public Builder filter(Predicate<User> postProcessingFilter) {
            this.postProcessingFilter = postProcessingFilter;
            return this;
        }

        /**
         * @param maxResults the maximum number of results to retrieve.
         * @since v7.0
         */
        public Builder maxResults(Integer maxResults) {
            this.maxResults = maxResults;
            return this;
        }

        /**
         * @param sorted true to ensure results are sorted, false if results may be unsorted.  Turning sorting off
         *               may have better performance.
         * @since v7.0
         */
        public Builder sorted(boolean sorted) {
            this.sorted = sorted;
            return this;
        }

        /**
         * @param ignorePermissionCheck a flag indicating whether the permission checking has already been
         *                              performed.  If false (the default), the logged in user must have browser users permission to list
         *                              users or else an empty list is always returned.
         *                              If true, searches are not restricted by the logged in user's permissions.
         * @since v7.0
         */
        public Builder ignorePermissionCheck(boolean ignorePermissionCheck) {
            this.ignorePermissionCheck = ignorePermissionCheck;
            return this;
        }
    }
}
