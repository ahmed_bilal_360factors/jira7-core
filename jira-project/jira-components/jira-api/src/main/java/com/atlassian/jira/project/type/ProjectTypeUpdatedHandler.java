package com.atlassian.jira.project.type;

import com.atlassian.annotations.PublicSpi;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;

/**
 * Interface to be implemented by clients who wish to be notified once the project type of a project has been updated.
 * <p>
 * Implementations of this class need to be registered through {@link ProjectTypeUpdatedRegistrar#register(ProjectTypeUpdatedHandler)} for the notifications to be triggered.
 *
 * @since 7.0
 */
@PublicSpi
public interface ProjectTypeUpdatedHandler {
    /**
     * Returns a unique identifier for the handler.
     *
     * @return A unique identifier for the handler.
     */
    String getHandlerId();

    /**
     * Method called every time the type of a project is updated.
     * This gives the opportunity for plugins to execute some logic once the project type has been updated.
     * If this method returns failure, then the update will be vetoed. All other handlers that have already been called
     * will now have their onProjectTypeUpdateRolledBack() methods called.
     *
     * @param user              The user performing the update.
     * @param project           The project for which the type has been updated.
     * @param oldProjectTypeKey The old project type key.
     * @param newProjectTypeKey The new project type key.
     * @return a {@link ProjectTypeUpdatedOutcome} indicating whether the operation succeeded or failed  .
     */
    ProjectTypeUpdatedOutcome onProjectTypeUpdated(ApplicationUser user, Project project, ProjectTypeKey oldProjectTypeKey, ProjectTypeKey newProjectTypeKey);

    /**
     * This method will only be called if the update to the project type is rolled back.
     * <p>
     * That could happen if one of the {@link ProjectTypeUpdatedHandler} returns an error on {@link #projectTypeUpdated(Project, ProjectType, ProjectType)}.
     * <p>
     * On this method, the handler should undo all of the changes made by it when {@link #projectTypeUpdated(Project, ProjectType, ProjectType)} was called.
     * The parameters passed to this method will be exactly the same as the ones when {@link #projectTypeUpdated(Project, ProjectType, ProjectType)} was called,
     * with the addition of the {@link ProjectTypeUpdatedOutcome} returned from that call.
     *
     * @param user              The user performing the update.
     * @param project           The project type for which the project type was updated.
     * @param oldProjectTypeKey The old project type key.
     * @param newProjectTypeKey The new project type key.
     * @param outcome           the {@link ProjectTypeUpdatedOutcome} returned from the update operation.
     */
    void onProjectTypeUpdateRolledBack(ApplicationUser user, Project project, ProjectTypeKey oldProjectTypeKey, ProjectTypeKey newProjectTypeKey, ProjectTypeUpdatedOutcome outcome);
}
