package com.atlassian.jira.plugin.customfield;

import com.atlassian.annotations.PublicSpi;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.rest.json.JsonData;

/**
 * This interface is supposed to be implemented by classes which provide an extra rest serializer for a custom field.
 * This serializer serializes the custom field to another json representation of this custom field. These representations are
 * returned in issue and search rest endpoint in the <b>versionedRepresentation<b> field which requires to be expanded.
 * The rest serializer may be defined in the customfield's module descriptor through a <b>rest-serializer</b> element.
 * This element requires a name as an attribute and a reference to the class that implements this interface in the <b>class</b> attribute.
 * This interface may be also implemented as a component.
 *
 * @since v6.4
 */
@PublicSpi
public interface CustomFieldRestSerializer {
    JsonData getJsonData(final CustomField field, Issue issue);
}
