package com.atlassian.jira.bc.project;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.annotations.Internal;
import com.atlassian.annotations.PublicApi;
import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.bc.ServiceResultImpl;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.UpdateProjectParameters;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.ErrorCollections;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.web.action.RequestSourceType;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@PublicApi
public interface ProjectService {
    /**
     * The default name of HTML fields containing a Project's name. Validation methods on this service
     * (isValidAllProjectData) will return an {@link com.atlassian.jira.util.ErrorCollection} with error messages keyed
     * to this field name.
     */
    public static final String PROJECT_NAME = "projectName";

    /**
     * The default name of HTML fields containing a Project's key. Validation methods on this service
     * (isValidAllProjectData) will return an {@link com.atlassian.jira.util.ErrorCollection} with error messages keyed
     * to this field name.
     */
    public static final String PROJECT_KEY = "projectKey";

    /**
     * The default name of HTML fields containing a Project's lead. Validation methods on this service
     * (isValidAllProjectData) will return an {@link com.atlassian.jira.util.ErrorCollection} with error messages keyed
     * to this field name.
     */
    public static final String PROJECT_LEAD = "projectLead";

    /**
     * The default name of HTML fields containing a Project's URL. Validation methods on this service
     * (isValidAllProjectData) will return an {@link com.atlassian.jira.util.ErrorCollection} with error messages keyed
     * to this field name.
     */
    public static final String PROJECT_URL = "projectUrl";

    /**
     * The default name of HTML fields containing a Project's description. Validation methods on this service
     * (isValidAllProjectData) will return an {@link com.atlassian.jira.util.ErrorCollection} with error messages keyed
     * to this field name.
     */
    public static final String PROJECT_DESCRIPTION = "projectDescription";

    /**
     * The default name of HTML fields containing the ID of a project's category.
     */
    public static final String PROJECT_CATEGORY_ID = "projectCategoryId";

    /**
     * The default name of HTML fields containing a Project's type. Validation methods on this service will return an
     * {@link com.atlassian.jira.util.ErrorCollection} with error messages keyed to this field name.
     */
    public static final String PROJECT_TYPE = "projectType";

    /**
     * The maximum length allowed for the project name field.
     *
     * @deprecated use getMaximumNameLength() instead;
     */
    public static final int MAX_NAME_LENGTH = 80;

    /**
     * The minimum length allowed for the project name field.
     */
    public static final int MIN_NAME_LENGTH = 2;

    /**
     * The maximum length allowed for the project key field.
     *
     * @deprecated use getMaximumKeyLength() instead
     */
    public static final int MAX_KEY_LENGTH = 10;
    /**
     * Default project name length
     */
    public static final int DEFAULT_NAME_LENGTH = 80;

    /**
     * This method needs to be called before creating a project to ensure all parameters are correct.  There are a
     * number of required parameters, such as a project name, key and lead. The validation will also check if a project
     * with the name or key provided already exists and throw an appropriate error. The project key will be validated
     * that it matches the allowed key pattern, and it is not a reserved word. A validation error will also be added if no
     * user exists for the lead username provided.
     * <p/>
     * Optional validation will be done for the url, assigneetype and avatarId parameters. The url needs to be a valid
     * URL and the assigneeType needs to be either {@link com.atlassian.jira.project.AssigneeTypes#PROJECT_LEAD},
     * {@link com.atlassian.jira.project.AssigneeTypes#UNASSIGNED}, or null to let JIRA decide on the best default
     * assignee.  UNASSIGNED will also only be valid, if unassigned issues are enabled in the General Configuration.
     * <p/>
     * All the projects created in JIRA must have a project type, and its value must correspond to one of the project types defined on the JIRA
     * instance. A validation error will be reported if no project type is passed or if it does not meet the previous criteria.
     * <p/>
     * There are two ways to provide the project type to this method: directly via {@link ProjectCreationData#getProjectTypeKey()} or
     * indirectly via {@link ProjectCreationData#getProjectTemplateKey()}.
     * <p/>
     * If a project template key is provided, this method will look up the project template and use the template's project type as
     * the type of the created project.
     * <p/>
     * If both a project type and a template key are provided on the received {@link ProjectCreationData}, the project
     * type defined on the template will be compared to the project type defined in the project creation date and, if
     * they don't match, a validation error will be reported.
     * <p/>
     * Providing a template key is the preferred method for creating a project since templates ensure projects are created in a more
     * useful way for a particular use case.
     * <p/>
     * The method will return a {@link com.atlassian.jira.bc.project.ProjectService.CreateProjectValidationResult} which
     * contains an ErrorCollection with any potential errors and all the project's details.
     *
     * @param user                The user performing the action
     * @param projectCreationData An object encapsulating all the data for the project that will get created
     * @return A validation result containing any errors and all project details
     * @since 7.0
     */
    @Nonnull
    CreateProjectValidationResult validateCreateProject(ApplicationUser user, @Nonnull ProjectCreationData projectCreationData);

    /**
     * Using the validation result from {@link #validateCreateProject(ApplicationUser, ProjectCreationData)} a new project will be created.
     * This method will throw an IllegalStateException if the validation result contains any errors.
     * <p/>
     * Project creation involves creating the project itself and setting some defaults for workflow schemes and issue
     * type screen schemes.
     * <p/>
     * If the validation result contains an existing project id, the new project will be created with shared schemes, that is
     * the a new project will be associated with the the same schemes as the existing project:
     * <ul>
     * <li>Permission Scheme</li>
     * <li>Notification Scheme</li>
     * <li>Issue Security Scheme</li>
     * <li>Workflow Scheme</li>
     * <li>Issue Type Scheme</li>
     * <li>Issue Type Screen Scheme</li>
     * </ul>
     * <p/>
     * In addition this method will also add the new project to the same project category as the existing project.
     *
     * @param createProjectValidationResult Result from the validation, which also contains all the project's details.
     * @return The new project
     * @throws IllegalStateException if the validation result contains any errors.
     */
    Project createProject(CreateProjectValidationResult createProjectValidationResult);

    /**
     * Validates if the current user can create a new project with shared schemes based on the existing project
     * provided.
     * <p/>
     * This method validates that the passed project exists and that the current user has {@link
     * ProjectAction#EDIT_PROJECT_CONFIG} permissions for it. Beyond these checks, validation rules will be the same as {@link
     * #validateCreateProject(ApplicationUser, ProjectCreationData)}.
     *
     * @param user                The user performing the action
     * @param existingProjectId   An existing project id to use as the base for the new project being created
     * @param projectCreationData An object encapsulating all the data for the project that will get created
     * @return A validation result containing any errors and all project details
     * @since 7.0
     */
    @Nonnull
    CreateProjectValidationResult validateCreateProjectBasedOnExistingProject(ApplicationUser user, @Nonnull Long existingProjectId, ProjectCreationData projectCreationData);

    /**
     * Validates that the given user is authorised to update a project. A project can be updated by any user with the
     * global admin permission or project admin permission for the project in question.
     *
     * @param user The user trying to update a project
     * @param key  The project key of the project to update.
     * @return a ServiceResult, which will contain errors if the user is not authorised to update the project
     * @deprecated use {@link #validateUpdateProject(ApplicationUser, UpdateProjectRequest)} instead; since v7.2
     */
    ServiceResult validateUpdateProject(final ApplicationUser user, final String key);

    /**
     * Validates updating a project's details.  The project is looked up by the key provided.  If no project with the
     * key provided can be found, an appropriate error will be added to the result.
     * <p/>
     * Validation performed will be the same as for the {@link #validateCreateProject(ApplicationUser, ProjectCreationData)} method.
     * The only difference is that the project key will obviously not be validated.
     * <p/>
     * A project can be updated by any user with the global admin permission or project admin permission for the project
     * in question.
     *
     * @param user         The user trying to update a project
     * @param name         The name of the new project
     * @param key          The project key of the project to update.
     * @param description  An optional description for the project
     * @param leadName     The username of the lead developer for the project
     * @param url          An optional URL for the project
     * @param assigneeType The default assignee for issues created in this project.  May be either project lead, or
     *                     unassigned if unassigned issues are enabled.
     * @return A validation result containing any errors and all project details
     * @deprecated use {@link #validateUpdateProject(ApplicationUser, UpdateProjectRequest)} instead; since v7.2
     */
    UpdateProjectValidationResult validateUpdateProject(ApplicationUser user, String name, String key, String description,
                                                        String leadName, String url, Long assigneeType);

    /**
     * Validates updating a project's details.  The project is looked up by the key provided.  If no project with the
     * key provided can be found, an appropriate error will be added to the result.
     * <p/>
     * Validation performed will be the same as for the {@link #validateCreateProject(ApplicationUser, ProjectCreationData)} method.
     * The only difference is that the project key will obviously not be validated.
     * <p/>
     * A project can be updated by any user with the global admin permission or project admin permission for the project
     * in question.
     * <p/>
     * <strong>WARNING</strong>: In 6.0-6.0.5, this method is available but does not work properly for renamed users (JRA-33843).
     *
     * @param user         The user trying to update a project
     * @param name         The name of the new project
     * @param key          The project key of the project to update.
     * @param description  An optional description for the project
     * @param lead         The lead developer for the project
     * @param url          An optional URL for the project
     * @param assigneeType The default assignee for issues created in this project.  May be either project lead, or
     *                     unassigned if unassigned issues are enabled.
     * @return A validation result containing any errors and all project details
     * @since 6.0.6 (see warning in method description)
     * @deprecated use {@link #validateUpdateProject(ApplicationUser, UpdateProjectRequest)} instead; since v7.2
     */
    UpdateProjectValidationResult validateUpdateProject(ApplicationUser user, String name, String key, String description,
                                                        ApplicationUser lead, String url, Long assigneeType);

    /**
     * Validates updating a project's details. The project is looked up by the project object. You may change the project key.
     * <p/>
     * Validation performed will be the same as for the {@link #validateCreateProject(ApplicationUser, ProjectCreationData)} method.
     * <p/>
     * A project can be updated by any user with the global admin permission or project admin permission for the project
     * in question. A project key can be updated by any user with the global admin permission.
     *
     * @param user            The user trying to update a project
     * @param originalProject The project to update with the values put in arguments ({@link Project} object should not be modified)
     * @param name            The name of the new project
     * @param key             The new project key
     * @param description     An optional description for the project
     * @param lead            The lead developer for the project
     * @param url             An optional URL for the project
     * @param assigneeType    The default assignee for issues created in this project.  May be either project lead, or
     *                        unassigned if unassigned issues are enabled.
     * @return A validation result containing any errors and all project details
     * @deprecated use {@link #validateUpdateProject(ApplicationUser, UpdateProjectRequest)} instead; since v7.2
     */
    UpdateProjectValidationResult validateUpdateProject(ApplicationUser user, Project originalProject, String name, String key, String description,
                                                        ApplicationUser lead, String url, Long assigneeType, Long avatarId);

    /**
     * Validates updating a project's details. The project is looked up by the project object. You may change the project key.
     * <p/>
     * Validation performed will be the same as for the {@link #validateCreateProject(ApplicationUser, ProjectCreationData)} method.
     * <p/>
     * A project can be updated by any user with the global admin permission or project admin permission for the project
     * in question. A project key can be updated by any user with the global admin permission.
     *
     * @param user            The user trying to update a project
     * @param originalProject The project to update with the values put in arguments ({@link Project} object should not be modified)
     * @param name            The name of the new project
     * @param key             The new project key
     * @param description     An optional description for the project
     * @param leadName        The user key for the lead developer for the project
     * @param url             An optional URL for the project
     * @param assigneeType    The default assignee for issues created in this project.  May be either project lead, or
     *                        unassigned if unassigned issues are enabled.
     * @return A validation result containing any errors and all project details
     * @deprecated use {@link #validateUpdateProject(ApplicationUser, UpdateProjectRequest)} instead; since v7.2
     */
    UpdateProjectValidationResult validateUpdateProject(ApplicationUser user, Project originalProject, String name, String key, String description,
                                                        String leadName, String url, Long assigneeType, Long avatarId);

    /**
     * Validates updating a project's details.  The project is looked up by the key provided.  If no project with the
     * key provided can be found, an appropriate error will be added to the result.
     * <p/>
     * Validation performed will be the same as for the {@link #validateCreateProject(ApplicationUser, ProjectCreationData)} method.
     * The only difference is that the project key will obviously not be validated.
     * <p/>
     * A project can be updated by any user with the global admin permission or project admin permission for the project
     * in question.
     *
     * @param user         The user trying to update a project
     * @param name         The name of the new project
     * @param key          The project key of the project to update.
     * @param description  An optional description for the project
     * @param leadName     The username of the lead developer for the project
     * @param url          An optional URL for the project
     * @param assigneeType The default assignee for issues created in this project.  May be either project lead, or
     *                     unassigned if unassigned issues are enabled.
     * @param avatarId     the id of an existing avatar.
     * @return A validation result containing any errors and all project details
     * @deprecated use {@link #validateUpdateProject(ApplicationUser, UpdateProjectRequest)} instead; since v7.2
     */
    UpdateProjectValidationResult validateUpdateProject(ApplicationUser user, String name, String key, String description,
                                                        String leadName, String url, Long assigneeType, Long avatarId);

    /**
     * Validates updating a project's details.  The project is looked up by the key provided.  If no project with the
     * key provided can be found, an appropriate error will be added to the result.
     * <p/>
     * Validation performed will be the same as for the {@link #validateCreateProject(ApplicationUser, ProjectCreationData)} method.
     * The only difference is that the project key will obviously not be validated.
     * <p/>
     * A project can be updated by any user with the global admin permission or project admin permission for the project
     * in question.
     * <p/>
     * <strong>WARNING</strong>: In 6.0-6.0.5, this method is available but does not work properly for renamed users (JRA-33843).
     *
     * @param user         The user trying to update a project
     * @param name         The name of the new project
     * @param key          The project key of the project to update.
     * @param description  An optional description for the project
     * @param lead         The lead developer for the project
     * @param url          An optional URL for the project
     * @param assigneeType The default assignee for issues created in this project.  May be either project lead, or
     *                     unassigned if unassigned issues are enabled.
     * @param avatarId     the id of an existing avatar.
     * @return A validation result containing any errors and all project details
     * @since 6.0.6 (see warning in method description)
     * @deprecated use {@link #validateUpdateProject(ApplicationUser, UpdateProjectRequest)} instead; since v7.2
     */
    UpdateProjectValidationResult validateUpdateProject(ApplicationUser user, String name, String key, String description,
                                                        ApplicationUser lead, String url, Long assigneeType, Long avatarId);

    /**
     * Validates updating a project's details.  The project is looked up by the key provided.  If no project with the
     * key provided can be found, an appropriate error will be added to the result.
     * <p/>
     * Validation performed will be the same as for the {@link #validateCreateProject(ApplicationUser, ProjectCreationData)} method.
     * The only difference is that the project key will obviously not be validated.
     * <p/>
     *
     * @param updateProjectRequest includes the changes that will be made to the project
     * @return A validation result containing any errors and all project details
     * @since 7.2
     */
    UpdateProjectValidationResult validateUpdateProject(ApplicationUser user, UpdateProjectRequest updateProjectRequest);

    /**
     * Using the validation result from {@link #validateUpdateProject(ApplicationUser, String, String,
     * String, String, String, Long)} this method performs the actual update on the project.
     *
     * @param updateProjectValidationResult Result from the validation, which also contains all the project's details.
     * @return The updated project
     * @throws IllegalStateException if the validation result contains any errors.
     */
    Project updateProject(UpdateProjectValidationResult updateProjectValidationResult);

    /**
     * Updates the type of a project.
     * If the update completes successfully, all the registered {@link com.atlassian.jira.project.type.ProjectTypeUpdatedHandler}s will be notified.
     * If an exception is thrown by one of the {@link com.atlassian.jira.project.type.ProjectTypeUpdatedHandler}, the project type update will be rolled back.
     *
     * @param user           The user performing the action
     * @param project        The project which type needs to be updated
     * @param newProjectType The new project type
     * @return Either the updated project if everything went well or an error collection indicating the reasons of the failure
     * @throws PermissionException If the user does not have permission to perform the operation
     * @since 7.0
     */
    Either<Project, ErrorCollection> updateProjectType(ApplicationUser user, Project project, ProjectTypeKey newProjectType);

    /**
     * Validation to delete a project is quite straightforward.  The user must have global admin rights and the project
     * about to be deleted needs to exist.
     *
     * @param user The user trying to delete a project
     * @param key  The key of the project to delete
     * @return A validation result containing any errors and all project details
     */
    DeleteProjectValidationResult validateDeleteProject(ApplicationUser user, String key);

    /**
     * Deletes the project provided by the deleteProjectValidationResult.  There's a number of steps involved in
     * deleting a project, which are carried out in the following order:
     * <ul>
     * <li>Delete all the issues in the project</li>
     * <li>Remove any custom field associations for the project</li>
     * <li>Remove the IssueTypeScreenSchemeAssocation for the project</li>
     * <li>Remove any other associations of this project (to permission schemes, notification schemes...)</li>
     * <li>Remove any versions in this project</li>
     * <li>Remove any components in this project</li>
     * <li>Delete all portlets that rely on this project (either directly or via filters)</li>
     * <li>Delete all the filters for this project</li>
     * <li>Delete the project itself in the database</li>
     * <li>Flushing the issue, project and workflow scheme caches</li>
     * </ul>
     *
     * @param user                          The user trying to delete a project
     * @param deleteProjectValidationResult Result from the validation, which also contains all the project's details.
     * @return A result containing any errors.  Users of this method should check the result.
     */
    DeleteProjectResult deleteProject(ApplicationUser user, DeleteProjectValidationResult deleteProjectValidationResult);

    /**
     * Deletes the project provided by the deleteProjectValidationResult in a backgound task and provides a progress URL
     * that may be used to monitor the delete progress.
     * <p/>
     * There's a number of steps involved in
     * deleting a project, which are carried out in the following order:
     * <ul>
     * <li>Delete all the issues in the project</li>
     * <li>Remove any custom field associations for the project</li>
     * <li>Remove the IssueTypeScreenSchemeAssocation for the project</li>
     * <li>Remove any other associations of this project (to permission schemes, notification schemes...)</li>
     * <li>Remove any versions in this project</li>
     * <li>Remove any components in this project</li>
     * <li>Delete all portlets that rely on this project (either directly or via filters)</li>
     * <li>Delete all the filters for this project</li>
     * <li>Delete the project itself in the database</li>
     * <li>Flushing the issue, project and workflow scheme caches</li>
     * </ul>
     *
     * @param user                          The user trying to delete a project
     * @param deleteProjectValidationResult Result from the validation, which also contains all the project's details.
     * @return URL of the progress page for monitoring this event.
     * @since v7.1.1
     */
    DeleteProjectResult deleteProjectAsynchronous(ApplicationUser user, DeleteProjectValidationResult deleteProjectValidationResult);

    /**
     * If the scheme ids are not null or -1 (-1 is often used to reset schemes), then an attempt will be made to
     * retrieve the scheme.  If this attempt fails an error will be added.  IssueSecuritySchemes will only be validated
     * in enterprise edition.
     *
     * @param permissionSchemeId    The permission scheme that the new project should use
     * @param notificationSchemeId  The notification scheme that the new project should use. Optional.
     * @param issueSecuritySchemeId The issue security scheme that the new project should use. Optional.
     * @return A validation result containing any errors and all scheme ids
     */
    UpdateProjectSchemesValidationResult validateUpdateProjectSchemes(ApplicationUser user, final Long permissionSchemeId,
                                                                      final Long notificationSchemeId, final Long issueSecuritySchemeId);

    /**
     * Updates the project schemes for a particular project, given a validation result and project to update.
     *
     * @param result  Result from the validation, which also contains all the schemes details.
     * @param project The project which will have its schemes updated.
     * @throws IllegalStateException if the validation result contains any errors.
     */
    void updateProjectSchemes(UpdateProjectSchemesValidationResult result, Project project);

    /**
     * Use {@link com.atlassian.jira.bc.project.ProjectService#validateCreateProject} instead.
     * <p/>
     * Validates the given project fields. Any errors will be added to the
     * {@link com.atlassian.jira.bc.JiraServiceContext}.
     * <p/>
     * The project type is mandatory and its value must correspond to one of the project types defined on the JIRA
     * instance. A validation error will be reported if the passed value is null or if it does not meet the previous criteria.
     *
     * @param serviceContext      containing the ErrorCollection that will be populated with any validation errors
     *                            that are encountered. It also contains the logged in user so the correct locale is used for the error messages.
     * @param projectCreationData An object encapsulating all the data for the project that will get created
     * @return true if project data is valid, false otherwise
     */
    @Deprecated
    boolean isValidAllProjectData(JiraServiceContext serviceContext, ProjectCreationData projectCreationData);

    /**
     * Validate the fields required for creating a project. Any errors will be added to the
     * {@link com.atlassian.jira.bc.JiraServiceContext}.
     *
     * @param serviceContext      containing the ErrorCollection that will be populated with any validation errors that are
     *                            encountered
     * @param projectCreationData An object encapsulating all the data for the project that will get created
     * @return true if project data is valid, false otherwise
     */
    boolean isValidRequiredProjectData(JiraServiceContext serviceContext, ProjectCreationData projectCreationData);

    /**
     * Validates the given project key. Any errors will be added to the
     * {@link com.atlassian.jira.bc.JiraServiceContext}.
     *
     * @param serviceContext containing the ErrorCollection that will be populated with any validation errors that are
     *                       encountered
     * @param key            The key to validate @Nonnull
     * @return true if project key is valid, false otherwise
     */
    boolean isValidProjectKey(JiraServiceContext serviceContext, String key);

    /**
     * Get the project key description from the properties file. If the user has specified a custom regex that project
     * keys must conform to and a description for that regex, this method should return the description.
     * <p/>
     * If the user has not specified a custom regex, this method will return the default project key description:
     * <p/>
     * "Usually the key is just 3 letters - i.e. if your project name is Foo Bar Raz, a key of FBR would make sense.<br>
     * The key must contain only uppercase alphabetic characters, and be at least 2 characters in length.<br> <i>It is
     * recommended to use only ASCII characters, as other characters may not work."
     *
     * @return a String description of the project key format
     */
    String getProjectKeyDescription();

    /**
     * Used to retrieve a {@link com.atlassian.jira.project.Project} object by id.
     * <p>
     * This method returns a {@link com.atlassian.jira.bc.project.ProjectService.GetProjectResult}. The project will be null if no project for the id
     * specified can be found, or if the user making the request does not have the BROWSE project permission for the
     * project. In both of these cases, the errorCollection in the result object will contain an appropriate error
     * message.
     * </p>
     * <p>
     * The current user will be looked up via the <tt>JiraAuthenticationContext</tt>,
     * so <strong>do not call this method from a background thread</strong>.
     * Use {@link #getProjectById(com.atlassian.jira.user.ApplicationUser, Long)} instead.
     * </p>
     *
     * @param id The id of the project.
     * @return A ProjectResult object
     * @since v6.4
     */
    GetProjectResult getProjectById(Long id);

    /**
     * Used to retrieve a {@link com.atlassian.jira.project.Project} object by id.  This method returns a {@link
     * com.atlassian.jira.bc.project.ProjectService.GetProjectResult}. The project will be null if no project for the id
     * specified can be found, or if the user making the request does not have the BROWSE project permission for the
     * project. In both of these cases, the errorCollection in the result object will contain an appropriate error
     * message.
     *
     * @param user The user retrieving the project.
     * @param id   The id of the project.
     * @return A ProjectResult object
     */
    GetProjectResult getProjectById(ApplicationUser user, Long id);

    /**
     * Used to retrieve a {@link com.atlassian.jira.project.Project} object by id providing the user can perform the
     * passed action on the project. This method returns a {@link
     * com.atlassian.jira.bc.project.ProjectService.GetProjectResult}. The project will be null if no project for the
     * id specified can be found, or if the user making the request cannot perform the passed action on the project.
     * In both of these cases, the errorCollection in the result object will contain an appropriate error message.
     *
     * @param user   The user retrieving the project.
     * @param id     The id of the project.
     * @param action the action the user must be able to perform on the project.
     * @return A ProjectResult object
     */
    GetProjectResult getProjectByIdForAction(ApplicationUser user, Long id, ProjectAction action);

    /**
     * <p>Used to retrieve a {@link com.atlassian.jira.project.Project} object by key.  This method returns a {@link
     * com.atlassian.jira.bc.project.ProjectService.GetProjectResult}. The project will be null if no project for the
     * key specified can be found, or if the user making the request does not have the BROWSE project permission for the
     * project. In both of these cases, the errorCollection in the result object will contain an appropriate error
     * message.</p>
     * <p>
     * The current user will be looked up via the <tt>JiraAuthenticationContext</tt>,
     * so <strong>do not call this method from a background thread</strong>.
     * Use {@link #getProjectByKey(com.atlassian.jira.user.ApplicationUser, String)} instead.
     * </p>
     *
     * @param key The key of the project.
     * @return A GetProjectResult object
     * @since v6.4
     */
    GetProjectResult getProjectByKey(String key);

    /**
     * Used to retrieve a {@link com.atlassian.jira.project.Project} object by key.  This method returns a {@link
     * com.atlassian.jira.bc.project.ProjectService.GetProjectResult}. The project will be null if no project for the
     * key specified can be found, or if the user making the request does not have the BROWSE project permission for the
     * project. In both of these cases, the errorCollection in the result object will contain an appropriate error
     * message.
     *
     * @param user The user retrieving the project.
     * @param key  The key of the project.
     * @return A GetProjectResult object
     */
    GetProjectResult getProjectByKey(ApplicationUser user, String key);

    /**
     * Used to retrieve the maximum length allowed for new project names.
     *
     * @return The configured maximum project length
     */
    int getMaximumNameLength();

    /**
     * Used to retrieve a {@link com.atlassian.jira.project.Project} object by key providing the user can perform the
     * passed action on the project. This method returns a {@link
     * com.atlassian.jira.bc.project.ProjectService.GetProjectResult}. The project will be null if no project for the
     * key specified can be found, or if the user making the request cannot perform the passed action on the project.
     * In both of these cases, the errorCollection in the result object will contain an appropriate error message.
     *
     * @param user   The user retrieving the project.
     * @param key    The key of the project.
     * @param action the action the user must be able to perform on the project.
     * @return A GetProjectResult object
     */
    GetProjectResult getProjectByKeyForAction(ApplicationUser user, String key, ProjectAction action);

    /**
     * Used to retrieve the maximum length allowed for new project keys.
     *
     * @return The configured maximum project length
     */
    int getMaximumKeyLength();

    /**
     * Used to retrieve the total number of {@link Project}s, regardless of the permission to see those projects.
     *
     * @return A long value representing the total number of projects.
     */
    long getProjectCount();

    /**
     * Used to retrieve a list of {@link com.atlassian.jira.project.Project} objects. This method returns a
     * {@link com.atlassian.jira.bc.ServiceOutcome} containing a list of projects. The list will be empty, if the user does not have
     * the BROWSE project permission for any project or no projects are visible when using anonymous access.
     *
     * @param user The user retrieving the list of projects or NULL when using anonymous access.
     * @return A ServiceOutcome containing a list of projects
     * @since v6.0
     */
    ServiceOutcome<List<Project>> getAllProjects(ApplicationUser user);

    /**
     * Used to retrieve a list of {@link com.atlassian.jira.project.Project} objects. This method returns a
     * {@link com.atlassian.jira.bc.ServiceOutcome} containing a list of projects that the user can perform the passed
     * action on. The list will be empty if no projects match the passed action.
     *
     * @param user   The user retrieving the list of projects or NULL when using anonymous access.
     * @param action the action the user must be able to perform on the returned projects.
     * @return A ServiceOutcome containing a list of projects the user can perform the passed action on.
     * @since v6.0
     */
    ServiceOutcome<List<Project>> getAllProjectsForAction(ApplicationUser user, ProjectAction action);

    @PublicApi
    public static class UpdateProjectSchemesValidationResult extends ServiceResultImpl {
        private Long permissionSchemeId;
        private Long notificationSchemeId;
        private Long issueSecuritySchemeId;

        @Internal
        public UpdateProjectSchemesValidationResult(ErrorCollection errorCollection) {
            super(errorCollection);
        }

        @Internal
        public UpdateProjectSchemesValidationResult(ErrorCollection errorCollection, Long permissionSchemeId,
                                                    Long notificationSchemeId, Long issueSecuritySchemeId) {
            super(errorCollection);
            this.permissionSchemeId = permissionSchemeId;
            this.notificationSchemeId = notificationSchemeId;
            this.issueSecuritySchemeId = issueSecuritySchemeId;
        }

        public Long getPermissionSchemeId() {
            return permissionSchemeId;
        }

        public Long getNotificationSchemeId() {
            return notificationSchemeId;
        }

        public Long getIssueSecuritySchemeId() {
            return issueSecuritySchemeId;
        }
    }

    @PublicApi
    public abstract static class AbstractProjectValidationResult extends ServiceResultImpl {
        private final String name;
        private final String key;
        private final String description;
        private final String leadName;
        private final String url;
        private final Long assigneeType;
        private final Long avatarId;
        private final boolean keyChanged;
        private final ApplicationUser user;

        @Internal
        public AbstractProjectValidationResult(ErrorCollection errorCollection) {
            super(errorCollection);
            name = null;
            key = null;
            description = null;
            leadName = null;
            url = null;
            assigneeType = null;
            avatarId = null;
            keyChanged = false;
            user = null;
        }

        @Internal
        public AbstractProjectValidationResult(ErrorCollection errorCollection, String name, String key,
                                               String description, String leadName, String url, Long assigneeType, final Long avatarId, final boolean keyChanged,
                                               final ApplicationUser user) {
            super(errorCollection);
            this.name = name;
            this.key = key;
            this.description = description;
            this.leadName = leadName;
            this.url = url;
            this.assigneeType = assigneeType;
            this.avatarId = avatarId;
            this.keyChanged = keyChanged;
            this.user = user;
        }

        public String getName() {
            return name;
        }

        public String getKey() {
            return key;
        }

        public String getDescription() {
            return description;
        }

        public String getLeadUsername() {
            return leadName;
        }

        /**
         * @return the username of the requested project lead
         * @deprecated Use {@link #getLeadUsername()} instead. Since v6.0.
         */
        public String getLead() {
            return leadName;
        }

        public String getUrl() {
            return url;
        }

        public Long getAssigneeType() {
            return assigneeType;
        }

        public Long getAvatarId() {
            return avatarId;
        }

        @ExperimentalApi
        public boolean isKeyChanged() {
            return keyChanged;
        }

        /**
         * @return the user that initiated the action
         * @since v6.1
         */
        @ExperimentalApi
        @Nullable
        public ApplicationUser getUser() {
            return user;
        }
    }

    @PublicApi
    public static class CreateProjectValidationResult extends ServiceResultImpl {
        private final ApplicationUser user;
        private final Optional<Long> existingProjectId;
        private final ProjectCreationData projectCreationData;

        @Internal
        public CreateProjectValidationResult(ErrorCollection errorCollection) {
            this(errorCollection, null, null, Optional.empty());
        }

        @Internal
        public CreateProjectValidationResult(ErrorCollection errorCollection, ApplicationUser user, ProjectCreationData projectCreationData) {
            this(errorCollection, user, projectCreationData, Optional.empty());
        }

        public CreateProjectValidationResult(ErrorCollection errorCollection, ApplicationUser user, ProjectCreationData projectCreationData, Optional<Long> existingProjectId) {
            super(errorCollection);
            this.user = user;
            this.existingProjectId = existingProjectId;
            this.projectCreationData = projectCreationData;
        }

        public ProjectCreationData getProjectCreationData() {
            return projectCreationData;
        }

        public ApplicationUser getUser() {
            return user;
        }

        public Optional<Long> getExistingProjectId() {
            return existingProjectId;
        }
    }

    @PublicApi
    class UpdateProjectValidationResult extends AbstractProjectValidationResult {
        private final Project originalProject;
        private final UpdateProjectRequest updateProjectRequest;

        @Internal
        public UpdateProjectValidationResult(ErrorCollection errorCollection) {
            super(errorCollection);
            this.originalProject = null;
            this.updateProjectRequest = null;
        }

        @Deprecated
        @Internal
        public UpdateProjectValidationResult(ErrorCollection errorCollection, String name, String key,
                                             String description, String lead, String url, Long assigneeType, Long avatarId, Project originalProject) {
            this(errorCollection, name, key, description, lead, url, assigneeType, avatarId, originalProject, false, null);
        }

        @Deprecated
        @Internal
        public UpdateProjectValidationResult(ErrorCollection errorCollection, String name, String key,
                                             String description, String leadUsername, String url, Long assigneeType, Long avatarId, Project originalProject,
                                             boolean keyChanged, ApplicationUser user) {
            super(errorCollection, name, key, description, leadUsername, url, assigneeType, avatarId, keyChanged, user);
            this.originalProject = originalProject;

            this.updateProjectRequest = new UpdateProjectRequest(originalProject)
                    .name(name)
                    .key(key)
                    .description(description)
                    .leadUsername(leadUsername)
                    .url(url)
                    .assigneeType(assigneeType)
                    .avatarId(avatarId);
        }

        @Internal
        UpdateProjectValidationResult(ErrorCollection errorCollection, Project originalProject, ApplicationUser user, UpdateProjectRequest updateProjectRequest) {
            super(errorCollection,
                    updateProjectRequest.getUpdateProjectParameters().getName().getOrElse(originalProject.getName()),
                    updateProjectRequest.getUpdateProjectParameters().getKey().getOrElse(originalProject.getKey()),
                    updateProjectRequest.getUpdateProjectParameters().getDescription().getOrElse(originalProject.getDescription()),
                    updateProjectRequest.getUpdateProjectParameters().getLeadUsername().getOrElse(originalProject.getLeadUserName()),
                    updateProjectRequest.getUpdateProjectParameters().getUrl().getOrElse(originalProject.getUrl()),
                    updateProjectRequest.getUpdateProjectParameters().getAssigneeType().getOrElse(originalProject.getAssigneeType()),
                    updateProjectRequest.getUpdateProjectParameters().getAvatarId().getOrElse(originalProject.getAvatar().getId()),
                    keyChanged(originalProject, updateProjectRequest),
                    user);
            this.originalProject = originalProject;
            this.updateProjectRequest = updateProjectRequest;
        }

        private static boolean keyChanged(Project originalProject, UpdateProjectRequest updateProjectRequest) {
            Option<String> newProjectKey = updateProjectRequest.getUpdateProjectParameters().getKey();
            return newProjectKey.isDefined() && !newProjectKey.get().equals(originalProject.getKey());
        }

        public Project getOriginalProject() {
            return originalProject;
        }

        public UpdateProjectRequest getUpdateProjectRequest() {
            return updateProjectRequest;
        }
    }

    @PublicApi
    public static abstract class AbstractProjectResult extends ServiceResultImpl implements ServiceOutcome<Project> {
        private Project project;

        @Internal
        public AbstractProjectResult(ErrorCollection errorCollection) {
            super(errorCollection);
        }

        @Internal
        public AbstractProjectResult(ErrorCollection errorCollection, Project project) {
            super(errorCollection);
            this.project = project;
        }

        public Project getProject() {
            return project;
        }

        @Override
        public Project get() {
            return project;
        }

        @Override
        public Project getReturnedValue() {
            return project;
        }
    }

    @PublicApi
    public static class GetProjectResult extends AbstractProjectResult {
        @Internal
        public GetProjectResult(ErrorCollection errorCollection) {
            super(errorCollection);
        }

        @Internal
        public GetProjectResult(Project project) {
            super(new SimpleErrorCollection(), project);
        }

        @Internal
        public GetProjectResult(ErrorCollection errorCollection, Project project) {
            super(errorCollection, project);
        }
    }

    @PublicApi
    public static class DeleteProjectValidationResult extends AbstractProjectResult {
        @Internal
        public DeleteProjectValidationResult(final ErrorCollection errorCollection) {
            super(errorCollection);
        }

        @Internal
        public DeleteProjectValidationResult(final ErrorCollection errorCollection, final Project project) {
            super(errorCollection, project);
        }
    }

    @PublicApi
    public static class DeleteProjectResult extends ServiceResultImpl implements Serializable {
        private String redirectUrl = null;

        @Internal
        public DeleteProjectResult(final ErrorCollection errorCollection) {
            super(errorCollection);
        }

        @Internal
        public DeleteProjectResult(final String redirectUrl) {
            super(ErrorCollections.empty());
            this.redirectUrl = redirectUrl;
        }

        public String getRedirectUrl() {
            return redirectUrl;
        }
    }

    @PublicApi
    class UpdateProjectRequest {
        private UpdateProjectParameters updateProjectParameters;
        private RequestSourceType requestSourceType = RequestSourceType.PAGE;

        @Internal
        public UpdateProjectRequest(final Project originalProject) {
            updateProjectParameters = UpdateProjectParameters.forProject(originalProject.getId());
        }

        public UpdateProjectRequest key(String key) {
            updateProjectParameters.key(key);
            return this;
        }

        public UpdateProjectRequest name(String name) {
            updateProjectParameters.name(name);
            return this;
        }

        public UpdateProjectRequest description(String description) {
            updateProjectParameters.description(description);
            return this;
        }

        public UpdateProjectRequest leadUserKey(String leadUserKey) {
            updateProjectParameters.leadUserKey(leadUserKey);
            return this;
        }

        /**
         * Set lead to a user identified by name. Always use {@link
         * #leadUserKey(String)} whenever possible. If both name and key are
         * set, key is used.
         *
         * @param leadUsername
         * @return this
         */
        public UpdateProjectRequest leadUsername(String leadUsername) {
            updateProjectParameters.leadUsername(leadUsername);
            return this;
        }

        public UpdateProjectRequest url(String url) {
            updateProjectParameters.url(url);
            return this;
        }

        public UpdateProjectRequest assigneeType(Long assigneeType) {
            updateProjectParameters.assigneeType(assigneeType);
            return this;
        }

        public UpdateProjectRequest avatarId(Long avatarId) {
            updateProjectParameters.avatarId(avatarId);
            return this;
        }

        public UpdateProjectRequest projectType(String projectType) {
            updateProjectParameters.projectType(projectType);
            return this;
        }

        public UpdateProjectRequest projectCategoryId(Long projectCategoryId) {
            updateProjectParameters.projectCategoryId(projectCategoryId);
            return this;
        }

        /**
         * Stores how the action was invoked (useful for analytics, for example)
         * @param requestSourceType
         * @return the respective type
         */
        public UpdateProjectRequest requestSourceType(final RequestSourceType requestSourceType) {
            this.requestSourceType = requestSourceType;
            return this;
        }

        protected UpdateProjectParameters getUpdateProjectParameters() {
            return updateProjectParameters;
        }

        protected RequestSourceType getRequestSourceType() {
            return requestSourceType;
        }
    }
}
