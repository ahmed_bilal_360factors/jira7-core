package com.atlassian.jira.project;

public class DefaultAssigneeException extends RuntimeException {
    public DefaultAssigneeException(String message) {
        super(message);
    }
}
