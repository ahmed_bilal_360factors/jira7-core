package com.atlassian.jira.config;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.bean.SubTaskBean;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.util.IssueUpdateBean;
import com.atlassian.jira.user.ApplicationUser;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.List;

/**
 * Manages SubTasks - issues that are "part of" other issues.
 */
@PublicApi
public interface SubTaskManager {
    public static final String SUB_TASK_ISSUE_TYPE_STYLE = "jira_subtask";

    public static final String SUB_TASK_LINK_TYPE_NAME = "jira_subtask_link";
    public static final String SUB_TASK_LINK_TYPE_STYLE = "jira_subtask";
    public static final String SUB_TASK_LINK_TYPE_INWARD_NAME = "jira_subtask_inward";
    public static final String SUB_TASK_LINK_TYPE_OUTWARD_NAME = "jira_subtask_outward";

    // General Sub-Task methods

    /**
     * Enables sub-tasks on the instance. If there are no sub-task issue types present, then the default
     * sub-task issue type will be created.
     *
     * @throws CreateException if the default sub-task issue type creation failed.
     */
    public void enableSubTasks() throws CreateException;

    /**
     * Enables sub-tasks on the instance.
     *
     * @param createDefaultIfMissing if {@code true} and there are no sub-task issue types present, then the default
     *                               sub-task issue type will be created.
     * @throws CreateException if the default sub-task issue type creation failed.
     */
    void enableSubTasks(boolean createDefaultIfMissing) throws CreateException;

    public boolean isSubTasksEnabled();

    public void disableSubTasks();

    /**
     * Return true if the given issue is a subtask.
     *
     * @param issue the issue
     * @return true if the given issue is a subtask.
     */
    public boolean isSubTask(Issue issue);

    /**
     * Returns the parent issue ID of this the given issue.
     * Will return null if the given issue is not a subtask.
     *
     * @param issue the issue
     * @return the parent issue ID of this the given issue, or null.
     * @deprecated Use {@link #getParentIssueId(Issue)} instead. Since v6.4.
     */
    @Nullable
    public Long getParentIssueId(GenericValue issue);

    /**
     * Returns the parent issue ID of this the given issue.
     * Will return null if the given issue is not a subtask.
     *
     * @param issue the issue
     * @return the parent issue ID of this the given issue, or null.
     * @see #getParentIssueId(Long)
     */
    @Nullable
    public Long getParentIssueId(Issue issue);

    /**
     * Returns the parent issue ID of this the given issue.
     * Will return null if the given issue is not a subtask.
     *
     * @param issueId the issue ID of the subtask
     * @return the parent issue ID of this the given issue, or null.
     * @see #getParentIssueId(Issue)
     */
    @Nullable
    public Long getParentIssueId(Long issueId);

    /**
     * Returns the SubTaskBean for the given parent issue in the context of the given user.
     *
     * @param issue      the Issue
     * @param remoteUser the user
     * @return the SubTaskBean for the given parent issue in the context of the given user.
     */
    public SubTaskBean getSubTaskBean(Issue issue, ApplicationUser remoteUser);

    public void moveSubTask(Issue issue, Long currentSequence, Long sequence);

    public void resetSequences(Issue issue);

    // Subtask Issue Types

    /**
     * Create new issue type and adds it to default scheme.
     *
     * @deprecated Use {@link #insertSubTaskIssueType(String, Long, String, Long)} since v6.3.
     */
    @Deprecated
    public IssueType insertSubTaskIssueType(String name, Long sequence, String description, String iconurl) throws CreateException;

    /**
     * Create new issue type and adds it to default scheme.
     *
     * @since v6.3
     */
    public IssueType insertSubTaskIssueType(String name, Long sequence, String description, Long avatarId) throws CreateException;

    /**
     * @deprecated Use {@link #updateSubTaskIssueType(String, String, Long, String, Long)} instead. Since v6.3.
     */
    @Deprecated
    public void updateSubTaskIssueType(String id, String name, Long sequence, String description, String iconurl) throws DataAccessException;

    /**
     * Update existing sub-task issue type.
     *
     * @since v6.3
     */
    public void updateSubTaskIssueType(String id, String name, Long sequence, String description, Long avatarId) throws DataAccessException;

    public void removeSubTaskIssueType(String name) throws RemoveException;

    /**
     * Retrieves all the sub-task issue types
     *
     * @return A Collection of all sub-task {@link IssueType}s.
     * @since 4.1
     */
    public Collection<IssueType> getSubTaskIssueTypeObjects();

    public boolean issueTypeExistsById(String id);

    public boolean issueTypeExistsByName(String name);

    public void moveSubTaskIssueTypeUp(String id) throws DataAccessException;

    public void moveSubTaskIssueTypeDown(String id) throws DataAccessException;

    /**
     * Returns the SubTask IssueType with the given ID.
     *
     * @param id the ID
     * @return SubTask IssueType with the given ID.
     * @deprecated Use {@link #getSubTaskIssueType(String)} instead. Since v5.0.
     */
    public IssueType getSubTaskIssueTypeById(String id);

    /**
     * Returns the SubTask IssueType with the given ID.
     *
     * @param id the ID
     * @return SubTask IssueType with the given ID.
     */
    public IssueType getSubTaskIssueType(String id);

    // Sub-Task Issue Links

    public void createSubTaskIssueLink(Issue parentIssue, Issue subTaskIssue, ApplicationUser remoteUser) throws CreateException;

    public Collection<Long> getAllSubTaskIssueIds();

    /**
     * Returns a list of issue links associated with the issue
     *
     * @param issueId issue id
     * @return a list of issue links
     */
    public List<IssueLink> getSubTaskIssueLinks(Long issueId);

    public Collection<Issue> getSubTaskObjects(Issue issue);

    /**
     * Change the parent of the given subtask to the given new parent on behalf
     * of the given user.
     *
     * @param subTask     The SubTask
     * @param parentIssue The parent Issue
     * @param currentUser The user
     * @return an IssueUpdateBean representing the change action.
     * @throws RemoveException if there's a problem unlinking original parent.
     * @throws CreateException if there's a problem linking new parent.
     */
    public IssueUpdateBean changeParent(Issue subTask, Issue parentIssue, ApplicationUser currentUser)
            throws RemoveException, CreateException;
}
