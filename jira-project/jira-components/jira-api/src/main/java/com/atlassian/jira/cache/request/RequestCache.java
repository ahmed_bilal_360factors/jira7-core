package com.atlassian.jira.cache.request;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.cache.Supplier;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Represents a "request-scoped" cache local to the calling thread.
 * <p>
 * Note that "request" here means a short-lived job including but not limited to HTTP requests and scheduled
 * jobs.
 * </p>
 * <p>
 * <strong>WARNING</strong>: Request-scoped caches revert to read-through behaviour if a thread attempts
 * to access one without first establishing a request context.  Under those conditions, calls to
 * {@link #get(Object)} and {@link #get(Object, Supplier)} effectively always "miss" and use the
 * {@code CacheLoader} or {@code Supplier} respectively.  This can have a serious performance impact,
 * so background threads are advised to create a temporary request context explicitly with {@code JiraThreadLocalUtil}
 * when performing JQL queries, examining issue data, or any other significant piece of work.
 * </p>
 * <p>
 * <strong>WARNING</strong>: Request caches are effectively unbounded.  In the unusual case of
 * a single request examining a large number of issues or any similarly large piece of work,
 * it is strongly recommended that it be broken into batches so that the request caches can
 * be explicitly cleared between the batches.
 * </p>
 *
 * @see RequestCacheFactory
 * @since v6.4.8
 */
@ExperimentalApi
@ParametersAreNonnullByDefault
public interface RequestCache<K, V> {
    /**
     * Retrieves the value for a key by retrieving it from the request cache if possible and by using the
     * request cache's cache loader, otherwise.
     * <p>
     * When there is no request-scoped context defined, the request cache is always empty and the
     * value is lazily loaded.  If there is a request-scoped context but the value is not yet in
     * the cache, it is lazily loaded and the result is saved to the cache.
     * </p>
     *
     * @param key the key for which to retrieve a value
     * @return the retrieved or loaded value
     */
    @Nonnull
    V get(K key);

    /**
     * Retrieves the value for a key by retrieving it from the request cache if possible and otherwise giving
     * up and returning {@code null}.
     * <p>
     * When there is no request-scoped context defined or if the value is not already in the request cache,
     * then {@code null} is returned without attempting to load the value.
     * </p>
     *
     * @param key the key for which to retrieve a value
     * @return the value, if an only if it is already present in the cache
     */
    @Nullable
    V getIfPresent(K key);

    /**
     * Retrieves the value for a key by retrieving it from the request cache if possible and by using the
     * specified supplier, otherwise.
     * <p>
     * This is useful when the key used to identify the data does not have enough information to construct
     * the value or when using it would be less efficient.  For example, we may have a whole {@code Issue}
     * available but only want to use its id as the key for the request cache.  If the cache loader then
     * had to use the {@code IssueManager} to reload the issue's data, this would be very wasteful.  But the
     * {@code Supplier} can hold onto the {@code Issue} that we already have and still allow just the id
     * to be used as the cache key.
     * </p>
     * <p>
     * When there is no request-scoped context defined, the request cache is always empty and the
     * value is lazily loaded.  If there is a request-scoped context but the value is not yet in
     * the cache, it is lazily loaded and the result is saved to the cache.
     * </p>
     *
     * @param key      the key for which to retrieve a value
     * @param supplier a supplier that can load the value if it is not in the cache; this will be used instead
     *                 of the {@code CacheLoader} supplied when the cache was created.  The {@code Supplier} must not
     *                 return a {@code null} value.
     * @return the retrieved or loaded value
     */
    @Nonnull
    V get(K key, Supplier<V> supplier);

    /**
     * Discards any value cached for the given key.
     * If there is no request-scoped context active, then this method has no effect.
     *
     * @param key the key to be invalidated / removed from the cache
     */
    void remove(K key);

    /**
     * Discards all values from this request cache.
     * If there is no request-scoped context active, then this method has no effect.
     */
    void removeAll();
}
