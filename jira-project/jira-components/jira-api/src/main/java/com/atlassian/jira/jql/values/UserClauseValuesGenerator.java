package com.atlassian.jira.jql.values;

import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.user.search.UserSearchService;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.EmailFormatter;

import java.util.ArrayList;
import java.util.List;

import static com.atlassian.jira.component.ComponentAccessor.getComponent;

/**
 * Gets all the users for the specified value.
 *
 * @since v4.0
 */
public class UserClauseValuesGenerator implements ClauseValuesGenerator {
    private final UserSearchService userSearchService;
    private final EmailFormatter emailFormatter;

    /**
     * @deprecated since 6.4.9
     */
    @Deprecated
    public UserClauseValuesGenerator(final UserSearchService userSearchService) {
        this(userSearchService, getComponent(EmailFormatter.class));
    }

    public UserClauseValuesGenerator(final UserSearchService userSearchService, final EmailFormatter emailFormatter) {
        this.userSearchService = userSearchService;
        this.emailFormatter = emailFormatter;
    }

    public Results getPossibleValues(final ApplicationUser searcher, final String jqlClauseName, final String valuePrefix, final int maxNumResults) {
        List<Result> userValues = new ArrayList<Result>();
        final JiraServiceContextImpl serviceContext = new JiraServiceContextImpl(searcher);

        if (userSearchService.canPerformAjaxSearch(serviceContext)) {
            final List<ApplicationUser> users = userSearchService.findUsersAllowEmptyQuery(serviceContext, valuePrefix);

            for (ApplicationUser user : users) {
                if (userValues.size() == maxNumResults) {
                    break;
                }

                Result userValue = new Result(user.getName(), assembleDisplayNameParts(user, searcher));

                userValues.add(userValue);
            }
        }
        return new Results(userValues);
    }

    private String[] assembleDisplayNameParts(final ApplicationUser user, final ApplicationUser searcher) {
        final String fullName = user.getDisplayName();
        final String name = user.getName();
        final String email = emailFormatter.formatEmail(user, searcher);

        if (email != null) {
            return new String[]{fullName, "- " + email, " (" + name + ")"};
        } else {
            return new String[]{fullName, " (" + name + ")"};
        }
    }
}
