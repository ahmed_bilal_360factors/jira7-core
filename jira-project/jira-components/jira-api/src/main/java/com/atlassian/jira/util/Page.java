package com.atlassian.jira.util;

import javax.annotation.Nullable;
import java.util.List;

/**
 * A single page of values.
 *
 * @since 6.4.7
 */
public interface Page<T> {
    /**
     * @return the offset into the overall results set this page starts at (0 based).
     */
    long getStart();

    /**
     * @return the number of results in this 'page' of results.
     */
    int getSize();

    /**
     * @return the total number of requested entities. This information might not be available, in this case {@code null} is returned.
     */
    @Nullable
    Long getTotal();

    /**
     * @return a list of the values on this page. Each service returning a page should specify
     * the ordering on the list
     */
    List<T> getValues();

    /**
     * @return true if this is the last page, i.e. if the page with {@code start = this.getStart + this.getSize}  would be empty.
     */
    boolean isLast();
}
