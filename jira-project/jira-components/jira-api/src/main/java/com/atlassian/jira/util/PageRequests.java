package com.atlassian.jira.util;

import com.atlassian.annotations.PublicApi;

import javax.annotation.Nullable;

import static com.google.common.base.MoreObjects.firstNonNull;

/**
 * Class containing static factory methods used to create {@link PageRequest} instances.
 */
@PublicApi
public final class PageRequests {
    /**
     * Creates an instance of {@code PageRequest}.
     * <p>
     * Start of the page request can't be lower then 0 and the limit must be a number greater then 0 but lower then {@link com.atlassian.jira.util.PageRequest#MAX_PAGE_LIMIT}.
     *
     * @param start values offset, will be set to 0 if {@code null}
     * @param limit maximum number of results on a page, will be set to {@link com.atlassian.jira.util.PageRequest#MAX_PAGE_LIMIT} if {@code null}
     * @return a page request object
     */
    public static PageRequest request(@Nullable final Long start, @Nullable final Integer limit) {
        return new PageRequestImpl(firstNonNull(start, 0L), firstNonNull(limit, PageRequest.MAX_PAGE_LIMIT));
    }

    /**
     * Takes a page request and if its {@code limit} is greater than {@code maxSize} then
     * returns a new page request with {@code limit} equal to {@code maxSize}. Otherwise
     * the same page request is returned.
     *
     * @param pageRequest original page request
     * @param maxSize     maximum limit in page request
     * @return page request with limit no greater than maxSize
     */
    public static PageRequest limit(PageRequest pageRequest, int maxSize) {
        return pageRequest.getLimit() > maxSize ? request(pageRequest.getStart(), maxSize) : pageRequest;
    }

    private static class PageRequestImpl implements PageRequest {
        private final long start;
        private final int limit;

        public PageRequestImpl(final long start, final int limit) {
            this.start = start < 0 ? 0 : start;
            this.limit = limit <= 0 ? 1 : Math.min(limit, PageRequest.MAX_PAGE_LIMIT);
        }

        @Override
        public int getLimit() {
            return limit;
        }

        @Override
        public long getStart() {
            return start;
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            final PageRequestImpl that = (PageRequestImpl) o;

            if (limit != that.limit) {
                return false;
            }
            if (start != that.start) {
                return false;
            }

            return true;
        }

        @Override
        public int hashCode() {
            int result = (int) (start ^ (start >>> 32));
            result = 31 * result + limit;
            return result;
        }
    }
}
