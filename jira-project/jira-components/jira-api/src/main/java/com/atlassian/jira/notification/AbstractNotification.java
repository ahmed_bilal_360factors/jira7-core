package com.atlassian.jira.notification;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.notification.type.NotificationType;

import javax.annotation.Nullable;

/**
 * Base class describing notification recipients in a notification scheme.
 *
 * @since 7.0
 */
@PublicApi
public abstract class AbstractNotification implements Notification {
    private final Long id;
    private final NotificationType notificationType;
    private final String parameter;

    public AbstractNotification(final Long id, final NotificationType notificationType, final String parameter) {
        this.notificationType = notificationType;
        this.id = id;
        this.parameter = parameter;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public NotificationType getNotificationType() {
        return notificationType;
    }

    @Override
    @Nullable
    public String getParameter() {
        return parameter;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final AbstractNotification that = (AbstractNotification) o;

        if (id != null ? !id.equals(that.id) : that.id != null) {
            return false;
        }
        if (notificationType != that.notificationType) {
            return false;
        }
        if (parameter != that.parameter) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (notificationType != null ? notificationType.hashCode() : 0);
        result = 31 * result + (parameter != null ? parameter.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "AbstractNotification{" +
                "id=" + id +
                ", notificationType=" + notificationType +
                ", parameter='" + parameter + '\'' +
                '}';
    }
}
