package com.atlassian.jira.event.worklog;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.issue.worklog.Worklog;

/**
 * Marker interface for all worklog events.
 *
 * @since 7.0
 */
@PublicApi
public interface WorklogEvent {
    /**
     * @return the worklog on which the operation was performed.
     */
    Worklog getWorklog();
}