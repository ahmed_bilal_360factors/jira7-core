package com.atlassian.jira.project.template;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.jira.project.template.module.DemoProjectModule;

import java.util.List;

/**
 * @since v7.1
 */
@ExperimentalApi
public interface DemoProjectManager {
    List<DemoProjectModule> getDemoProjects();
}
