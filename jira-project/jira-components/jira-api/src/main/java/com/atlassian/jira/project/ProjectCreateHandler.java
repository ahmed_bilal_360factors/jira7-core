package com.atlassian.jira.project;

import com.atlassian.jira.exception.CreateException;

/**
 * Interface to be implemented by clients who wish to be notified when a project is created.
 * <p>
 * Implementations of this class need to be registered through {@link ProjectCreateRegistrar#register(ProjectCreateHandler)} for the notifications to be triggered.
 *
 * @since 7.0
 */
public interface ProjectCreateHandler {
    /**
     * Returns a unique identifier for the handler.
     *
     * @return A unique identifier for the handler.
     */
    String getHandlerId();

    /**
     * Method called every time a project is created.
     * This gives the opportunity for plugins to execute some logic once the project has been created.
     * If this method throws any exception, then the update will be vetoed. All other handlers that have already been called
     * will now have their onProjectCreationRolledBack() methods called.
     *
     * @param projectCreatedData Object encapsulating the information about the recently created project.
     * @throws CreateException An exception if something went wrong.
     */
    void onProjectCreated(ProjectCreatedData projectCreatedData) throws CreateException;

    /**
     * This method will only be called if the creation of the project is rolled back.
     * <p>
     * That could happen if one of the {@link ProjectCreateHandler} returns an error on {@link #onProjectCreated(Project)}
     * <p>
     * On this method, the handler should undo all of the changes made by it when {@link #onProjectCreated(Project)} was called.
     * The parameters passed to this method will be exactly the same as the ones when {@link #onProjectCreated(Project)} was called.
     *
     * @param projectCreatedData Object encapsulating the information about the recently created project.
     */
    void onProjectCreationRolledBack(ProjectCreatedData projectCreatedData);
}
