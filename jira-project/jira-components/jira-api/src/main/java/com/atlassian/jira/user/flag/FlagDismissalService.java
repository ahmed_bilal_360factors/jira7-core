package com.atlassian.jira.user.flag;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.user.ApplicationUser;

import java.util.Set;

/**
 * Keeps a record of notification flags dismissed per-user, and provides a means
 * to clear dismissals for a given flag for all users.
 * <p>
 * This is achieved by storing the time of dismissal and the reset time for a flag,
 * and the latest one takes precedence.
 *
 * @since 6.4
 */
@PublicApi
public interface FlagDismissalService {
    /**
     * Remove the dismiss entry for the current user. Removing a dismiss entry means that it is no longer
     * considered hidden by the user.
     *
     * @param flagKey An arbitrary identifier for a given flag
     * @param user    The user to remove the flag for
     * @since 7.0
     */
    void removeDismissFlagForUser(String flagKey, ApplicationUser user);

    /**
     * @param flagKey An arbitrary identifier for a given flag
     * @param user    The user dismissing the flag
     */
    void dismissFlagForUser(String flagKey, ApplicationUser user);

    /**
     * @param flagKey The flag to start showing everyone again
     */
    void resetFlagDismissals(String flagKey);

    /**
     * @param user The user for whom we wish to see dismissed flags
     * @return The list of flags the user has dismissed since they were last reset
     */
    Set<String> getDismissedFlagsForUser(ApplicationUser user);
}
