package com.atlassian.jira.web.action.admin.workflow.tabs;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.opensymphony.workflow.loader.ActionDescriptor;

import javax.annotation.Nonnull;
import java.util.Map;
import java.util.Optional;

/**
 * Utility methods relating to tabs in the workflow transition configuration UI.
 */
@PublicApi
public final class WorkflowTransitionContextUtils {

    /**
     * The context key for the number of items in this tab.
     */
    public static final String COUNT_KEY = "count";

    /**
     * The context key for the workflow transition.
     */
    public static final String TRANSITION_KEY = "workflow_transition";

    /**
     * The context key for the workflow.
     */
    public static final String WORKFLOW_KEY = "workflow";

    /**
     * Returns the transition from the given context.
     *
     * @param context the context from which to obtain the transition
     * @return empty if the context is null or does not contain a transition under
     * the key "<code>{@value WorkflowTransitionContextUtils#TRANSITION_KEY}</code>"
     */
    @Nonnull
    public static Optional<ActionDescriptor> getTransition(@Nonnull final Map<String, Object> context) {
        return get(TRANSITION_KEY, ActionDescriptor.class, context);
    }

    /**
     * Returns the workflow from the given context.
     *
     * @param context the context from which to obtain the workflow
     * @return empty if the context is null or does not contain a workflow under
     * the key "<code>{@value WorkflowTransitionContextUtils#WORKFLOW_KEY}</code>"
     */
    @Nonnull
    public static Optional<JiraWorkflow> getWorkflow(@Nonnull final Map<String, Object> context) {
        return get(WORKFLOW_KEY, JiraWorkflow.class, context);
    }

    @Nonnull
    private static <V> Optional<V> get(@Nonnull final String key, @Nonnull final Class<V> valueType,
                                       @Nonnull final Map<String, Object> map) {
        final Object value = map.get(key);
        if (valueType.isInstance(value)) {
            return Optional.of(valueType.cast(value));
        }
        return Optional.empty();
    }

    private WorkflowTransitionContextUtils() {}
}
