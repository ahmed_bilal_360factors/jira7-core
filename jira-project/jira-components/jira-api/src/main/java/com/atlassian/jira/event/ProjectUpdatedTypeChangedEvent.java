package com.atlassian.jira.event;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.annotations.Internal;
import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.web.action.RequestSourceType;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Event detailing changes in a project's type.
 *
 * @since v7.2
 */
@PublicApi
@ParametersAreNonnullByDefault
@EventName("jira.administration.projectdetails.updated.type")
public class ProjectUpdatedTypeChangedEvent extends AbstractEvent {
    private String from;
    private String to;
    private String requestSourceType;

    @Internal
    public ProjectUpdatedTypeChangedEvent(final ProjectTypeKey oldProjectTypeKey, final ProjectTypeKey newProjectTypeKey, final RequestSourceType requestSourceType) {
        this.from = oldProjectTypeKey.getKey();
        this.to = newProjectTypeKey.getKey();
        this.requestSourceType = requestSourceType.getType();
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public String getRequestSourceType() {
        return requestSourceType;
    }
}
