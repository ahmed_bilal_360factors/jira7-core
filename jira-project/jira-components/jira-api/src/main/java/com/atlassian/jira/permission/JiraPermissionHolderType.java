package com.atlassian.jira.permission;

import com.atlassian.annotations.PublicApi;
import com.atlassian.fugue.Option;

import javax.annotation.Nullable;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;
import static com.google.common.base.Strings.isNullOrEmpty;

/**
 * Permission holder types defined by JIRA.
 */
@PublicApi
public enum JiraPermissionHolderType implements PermissionHolderType {
    /**
     * Group. Parameter is required and it should be a group name.
     */
    GROUP("group", true),
    /**
     * Anyone, including anonymous users. No parameter.
     */
    ANYONE("group", false),
    /**
     * Specific user. Parameter is the user's id.
     */
    USER("user", true),
    /**
     * Project role. Parameter is the role's id.
     */
    PROJECT_ROLE("projectrole", true),
    /**
     * Issue reporter. No parameter.
     */
    REPORTER("reporter", false),
    /**
     * Project lead. No parameter.
     */
    PROJECT_LEAD("lead", false),
    /**
     * Issue assignee. No parameter.
     */
    ASSIGNEE("assignee", false),
    /**
     * User selected in the user custom field specified by the parameter.
     */
    USER_CUSTOM_FIELD("userCF", true),
    /**
     * Group selected in the group custom field specified by the parameter.
     */
    GROUP_CUSTOM_FIELD("groupCF", true),
    /**
     * This type can be used only with {@link ProjectPermissions#BROWSE_PROJECTS} permission to
     * show only projects where the user has create permission and issues within that where they are the reporter.
     * <p>
     * <b>Note: </b>This holder type is optional and it's not normally available.
     */
    REPORTER_WITH_CREATE_PERMISSION("reportercreate", false),
    /**
     * This type can be used only with {@link ProjectPermissions#BROWSE_PROJECTS} permission to
     * show only projects where the user has the assignable permission and issues within that where they are the assignee.
     * <p>
     * <b>Note: </b>This holder type is optional and it's not normally available.
     */
    ASSIGNEE_WITH_ASSIGNABLE_PERMISSION("assigneeassignable", false),
    /**
     * Application Role. Parameter is required and it should be a valid application name.
     */
    APPLICATION_ROLE("applicationRole", true);

    private final String key;
    private final boolean requiresParameter;

    private JiraPermissionHolderType(String key, boolean requiresParameter) {
        this.key = key;
        this.requiresParameter = requiresParameter;
    }

    /**
     * Key as stored in the database.
     */
    public String getKey() {
        return key;
    }

    /**
     * Whether the type requires additional argument (e.g. user id).
     *
     * @return true if argument is required, false otherwise.
     */
    public boolean requiresParameter() {
        return requiresParameter;
    }

    /**
     * Given a type key and an optional parameter tries to resolve the type.
     * Parameter is required as an argument because there are some types which
     * have the same key but are differentiated by the parameter presence or
     * absence.
     *
     * @param key       type key
     * @param parameter parameter. The value doesn't really matter, rather whether it's provided or not.
     * @return A type if key/parameter was identified as one of the standard JIRA types, {@code none}  otherwise
     */
    public static Option<JiraPermissionHolderType> fromKey(String key, @Nullable String parameter) {
        boolean isParameterDefined = !isNullOrEmpty(parameter);
        for (JiraPermissionHolderType type : JiraPermissionHolderType.values()) {
            if (type.getKey().equals(key) && isParameterDefined == type.requiresParameter()) {
                return some(type);
            }
        }
        return none();
    }
}