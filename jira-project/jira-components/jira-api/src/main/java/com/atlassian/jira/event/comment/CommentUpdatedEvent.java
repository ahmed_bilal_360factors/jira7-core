package com.atlassian.jira.event.comment;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.issue.comments.Comment;

/**
 * Comment published when a comment is updated.
 */
@PublicApi
public class CommentUpdatedEvent extends AbstractCommentEvent implements CommentEvent {

    public CommentUpdatedEvent(final Comment comment) {
        super(comment);
    }

}