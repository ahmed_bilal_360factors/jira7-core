package com.atlassian.jira.index;

import com.atlassian.annotations.ExperimentalSpi;
import com.atlassian.jira.issue.Issue;

/**
 * Interface for extractors adding fields based on issues
 *
 * @since 6.2
 */
@ExperimentalSpi
public interface IssueSearchExtractor extends EntitySearchExtractor<Issue> {
}
