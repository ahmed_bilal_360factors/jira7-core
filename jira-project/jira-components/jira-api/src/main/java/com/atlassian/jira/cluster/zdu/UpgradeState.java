package com.atlassian.jira.cluster.zdu;

import com.atlassian.annotations.ExperimentalApi;

/**
 * This defines which state the cluster is currently operating in.
 *
 * @since 7.3
 */
@ExperimentalApi
public enum UpgradeState {
    /**
     * Stable state. This is the default state that the cluster is in when there are no upgrades taking place.
     */
    STABLE,
    /**
     * Cluster nodes are ready to be brought down and upgraded.
     */
    READY_TO_UPGRADE,
    /**
     * At least one node has been upgraded and re-joined the cluster with a higher build number. {@link #READY_TO_UPGRADE}
     * still applies.
     */
    MIXED,
    /**
     * All nodes have re-joined the cluster with a higher build number. All nodes are on the same build number. The
     * upgrade is ready to be approved and the upgrade tasks can be run after approval.
     */
    READY_TO_RUN_UPGRADE_TASKS,
    /**
     * The delayed upgrade tasks are currently running on a node. Once this is done the state will switch back to {@link #STABLE}.
     */
    RUNNING_UPGRADE_TASKS
}
