package com.atlassian.jira.issue.fields.rest.json.beans;

import com.atlassian.jira.bc.issue.worklog.TimeTrackingConfiguration;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.issue.RendererManager;
import com.atlassian.jira.issue.fields.renderer.IssueRenderContext;
import com.atlassian.jira.issue.fields.rest.json.UserBeanFactory;
import com.atlassian.jira.issue.fields.rest.json.WorklogBeanFactory;
import com.atlassian.jira.issue.worklog.Worklog;
import com.atlassian.jira.rest.Dates;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.EmailFormatter;
import com.atlassian.jira.util.JiraDurationUtils;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;

/**
 * @since v4.2
 */
@XmlRootElement(name = "worklog")
@XmlAccessorType(XmlAccessType.FIELD)
public class WorklogJsonBean {
    @XmlElement
    private URI self;

    @XmlElement
    private UserJsonBean author;

    @XmlElement
    private UserJsonBean updateAuthor;

    @XmlElement
    private String comment;

    @XmlElement
    private String created;

    @XmlElement
    private String updated;

    @XmlElement
    private VisibilityJsonBean visibility;

    @JsonIgnore
    private boolean isVisibilitySet;

    @XmlElement
    private String started;

    @XmlElement
    private String timeSpent;

    @XmlElement
    private Long timeSpentSeconds;

    @XmlElement
    private String id;

    @XmlElement
    private String issueId;

    public WorklogJsonBean() {
    }

    private WorklogJsonBean(final URI self, final UserJsonBean author, final UserJsonBean updateAuthor, final String comment, final String created, final String updated, final VisibilityJsonBean visibility, final boolean isVisibilitySet, final String started, final String timeSpent, final Long timeSpentSeconds, final String id, final String issueId) {
        this.self = self;
        this.author = author;
        this.updateAuthor = updateAuthor;
        this.comment = comment;
        this.created = created;
        this.updated = updated;
        this.visibility = visibility;
        this.isVisibilitySet = isVisibilitySet;
        this.started = started;
        this.timeSpent = timeSpent;
        this.timeSpentSeconds = timeSpentSeconds;
        this.id = id;
        this.issueId = issueId;
    }

    public static Builder build() {
        return new Builder();
    }

    @Deprecated
    /**
     * @deprecated Use {@link com.atlassian.jira.issue.fields.rest.json.WorklogBeanFactory#createBeans(Iterable, ApplicationUser)} instead. Since v7.0.
     */
    public static List<WorklogJsonBean> asBeans(final List<Worklog> worklogs, final JiraBaseUrls uriInfo, final UserManager userManager, final TimeTrackingConfiguration timeTrackingConfiguration) {
        ApplicationUser loggedInUser = ComponentAccessor.getComponent(JiraAuthenticationContext.class).getLoggedInUser();
        return ImmutableList.copyOf(ComponentAccessor.getComponent(WorklogBeanFactory.class).createBeans(worklogs, loggedInUser));
    }

    @Deprecated
    /**
     * @deprecated Use {@link com.atlassian.jira.issue.fields.rest.json.WorklogBeanFactory#createBeans(Iterable, ApplicationUser)} instead. Since v7.0.
     */
    public static List<WorklogJsonBean> asBeans(final List<Worklog> worklogs, final JiraBaseUrls uriInfo, final UserManager userManager, final TimeTrackingConfiguration timeTrackingConfiguration, final ApplicationUser loggedInUser, final EmailFormatter emailFormatter) {
        return ImmutableList.copyOf(ComponentAccessor.getComponent(WorklogBeanFactory.class).createBeans(worklogs, loggedInUser));
    }

    @Deprecated
    /**
     * @deprecated Use {@link com.atlassian.jira.issue.fields.rest.json.WorklogBeanFactory#createBean(Worklog, ApplicationUser)} instead. Since v7.0.
     */
    public static WorklogJsonBean getWorklog(final Worklog log, final JiraBaseUrls baseUrls, final UserManager userManager, final TimeTrackingConfiguration timeTrackingConfiguration) {
        ApplicationUser loggedInUser = ComponentAccessor.getComponent(JiraAuthenticationContext.class).getLoggedInUser();
        return ComponentAccessor.getComponent(WorklogBeanFactory.class).createBean(log, loggedInUser);
    }

    @Deprecated
    /**
     * @deprecated Use {@link com.atlassian.jira.issue.fields.rest.json.WorklogBeanFactory#createBean(Worklog, ApplicationUser)} instead. Since v7.0.
     */
    public static WorklogJsonBean getWorklog(final Worklog log, final JiraBaseUrls baseUrls, final UserManager userManager, final TimeTrackingConfiguration timeTrackingConfiguration, final ApplicationUser loggedInUser, final EmailFormatter emailFormatter) {
        return ComponentAccessor.getComponent(WorklogBeanFactory.class).createBean(log, loggedInUser);
    }

    private static void addNonRenderableData(WorklogJsonBean bean, final Worklog log, final JiraBaseUrls baseUrls, final UserManager userManager, final ApplicationUser loggedInUser, final EmailFormatter emailFormatter) {
        try {
            bean.self = new URI(baseUrls.restApi2BaseUrl() + "issue/" + log.getIssue().getId().toString() + "/worklog/" + log.getId().toString());
        } catch (URISyntaxException e) {
            throw new RuntimeException("Failed to generate worklog self url", e);
        }
        bean.author = getUserBean(baseUrls, log.getAuthor(), userManager, loggedInUser, emailFormatter);
        bean.updateAuthor = getUserBean(baseUrls, log.getUpdateAuthor(), userManager, loggedInUser, emailFormatter);
        bean.id = Long.toString(log.getId());
        bean.issueId = Long.toString(log.getIssue().getId());

        final String groupLevel = log.getGroupLevel();
        final ProjectRole roleLevel = log.getRoleLevel();
        if (groupLevel != null) {
            bean.visibility = new VisibilityJsonBean(VisibilityJsonBean.VisibilityType.group, groupLevel);
        } else if (roleLevel != null) {
            bean.visibility = new VisibilityJsonBean(VisibilityJsonBean.VisibilityType.role, roleLevel.getName());
        }
    }

    @Deprecated
    public static List<WorklogJsonBean> asRenderedBeans(final List<Worklog> worklogs, final JiraBaseUrls uriInfo,
                                                        final String rendererType, final IssueRenderContext renderContext) {
        return asRenderedBeans(worklogs, uriInfo, rendererType, renderContext, ComponentAccessor.getComponent(JiraAuthenticationContext.class).getUser(), ComponentAccessor.getComponent(EmailFormatter.class));
    }

    public static List<WorklogJsonBean> asRenderedBeans(final List<Worklog> worklogs, final JiraBaseUrls uriInfo,
                                                        final String rendererType, final IssueRenderContext renderContext, final ApplicationUser loggedInUser, final EmailFormatter emailFormatter) {
        List<WorklogJsonBean> result = Lists.newArrayListWithCapacity(worklogs.size());
        for (Worklog worklog : worklogs) {
            result.add(getRenderedWorklog(worklog, uriInfo, rendererType, renderContext, loggedInUser, emailFormatter));
        }

        return result;
    }

    @Deprecated
    public static WorklogJsonBean getRenderedWorklog(final Worklog log, final JiraBaseUrls baseUrls,
                                                     String rendererType, IssueRenderContext renderContext) {
        return getRenderedWorklog(log, baseUrls, rendererType, renderContext, ComponentAccessor.getComponent(JiraAuthenticationContext.class).getUser(), ComponentAccessor.getComponent(EmailFormatter.class));
    }

    public static WorklogJsonBean getRenderedWorklog(final Worklog log, final JiraBaseUrls baseUrls,
                                                     String rendererType, IssueRenderContext renderContext, final ApplicationUser loggedInUser, final EmailFormatter emailFormatter) {
        final WorklogJsonBean bean = new WorklogJsonBean();
        addNonRenderableData(bean, log, baseUrls, ComponentAccessor.getUserManager(), loggedInUser, emailFormatter);
        if (StringUtils.isNotBlank(rendererType)) {
            RendererManager rendererManager = ComponentAccessor.getComponent(RendererManager.class);
            bean.comment = rendererManager.getRenderedContent(rendererType, log.getComment(), renderContext);
        } else {
            bean.comment = log.getComment();
        }

        JiraDurationUtils jiraDurationUtils = ComponentAccessor.getComponent(JiraDurationUtils.class);
        bean.timeSpent = jiraDurationUtils.getFormattedDuration(log.getTimeSpent(), ComponentAccessor.getJiraAuthenticationContext().getLocale());

        DateTimeFormatterFactory dateTimeFormatterFactory = ComponentAccessor.getComponent(DateTimeFormatterFactory.class);
        bean.created = log.getCreated() == null ? "" : dateTimeFormatterFactory.formatter().forLoggedInUser().format(log.getCreated());
        bean.updated = log.getUpdated() == null ? "" : dateTimeFormatterFactory.formatter().forLoggedInUser().format(log.getUpdated());
        bean.started = log.getStartDate() == null ? "" : dateTimeFormatterFactory.formatter().forLoggedInUser().format(log.getStartDate());
        return bean;
    }

    /**
     * Returns a UserBean for the user with the given name. If the user does not exist, the returned bean contains only
     * the username and no more info.
     *
     * @param uriInfo        a UriInfo
     * @param username       a String containing a user name
     * @param userManager    Manager for users
     * @param emailFormatter
     * @param loggedInUser
     * @return a UserBean
     */
    protected static UserJsonBean getUserBean(final JiraBaseUrls uriInfo, String username, final UserManager userManager, final ApplicationUser loggedInUser, final EmailFormatter emailFormatter) {
        ApplicationUser user = userManager.getUser(username);
        if (user != null) {
            return ComponentAccessor.getComponent(UserBeanFactory.class).createBean(user, loggedInUser, uriInfo, emailFormatter, ComponentAccessor.getComponent(TimeZoneManager.class));
        } else if (StringUtils.isNotBlank(username)) {
            UserJsonBean userJsonBean = new UserJsonBean();
            userJsonBean.setName(username);
            return userJsonBean;
        } else {
            return null;
        }

    }

    public static final WorklogJsonBean UPDATE_DOC_EXAMPLE = WorklogJsonBean.build()
            .setComment("I did some work here.")
            .setVisibility(new VisibilityJsonBean(VisibilityJsonBean.VisibilityType.group, "jira-developers"))
            .setStarted(Dates.asTimeString(new Date()))
            .setTimeSpentSeconds(12000L)
            .build();

    public static final WorklogJsonBean DOC_EXAMPLE = WorklogJsonBean.build()
            .setSelf(URI.create("http://www.example.com/jira/rest/api/2/issue/10010/worklog/10000"))
            .setAuthor(UserJsonBean.USER_SHORT_DOC_EXAMPLE)
            .setUpdateAuthor(UserJsonBean.USER_SHORT_DOC_EXAMPLE)
            .setComment("I did some work here.")
            .setVisibility(new VisibilityJsonBean(VisibilityJsonBean.VisibilityType.group, "jira-developers"))
            .setStarted(Dates.asTimeString(new Date()))
            .setUpdated(Dates.asTimeString(new Date()))
            .setTimeSpent("3h 20m")
            .setTimeSpentSeconds(12000L)
            .setId("100028")
            .setIssueId("10002")
            .build();

    public UserJsonBean getAuthor() {
        return author;
    }

    public UserJsonBean getUpdateAuthor() {
        return updateAuthor;
    }

    public String getComment() {
        return comment;
    }

    public Date getCreated() {
        return Dates.fromTimeString(created);
    }

    public Date getUpdated() {
        return Dates.fromTimeString(updated);
    }

    public Date getStarted() {
        return Dates.fromTimeString(started);
    }

    public String getTimeSpent() {
        return timeSpent;
    }

    public Long getTimeSpentSeconds() {
        return timeSpentSeconds;
    }

    @JsonProperty
    public VisibilityJsonBean getVisibility() {
        return visibility;
    }

    @JsonProperty
    public void setVisibility(VisibilityJsonBean visibility) {
        this.isVisibilitySet = true;
        this.visibility = visibility;
    }

    @JsonIgnore
    public boolean isVisibilitySet() {
        return isVisibilitySet;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIssueId() {
        return issueId;
    }

    public void setIssueId(String issueId) {
        this.issueId = issueId;
    }

    public URI getSelf() {
        return self;
    }

    public static class Builder {
        private URI self;
        private UserJsonBean author;
        private UserJsonBean updateAuthor;
        private String comment;
        private String created;
        private String updated;
        private VisibilityJsonBean visibility;
        private boolean isVisibilitySet;
        private String started;
        private String timeSpent;
        private Long timeSpentSeconds;
        private String id;
        private String issueId;

        public Builder setSelf(final URI self) {
            this.self = self;
            return this;
        }

        public Builder setAuthor(final UserJsonBean author) {
            this.author = author;
            return this;
        }

        public Builder setUpdateAuthor(final UserJsonBean updateAuthor) {
            this.updateAuthor = updateAuthor;
            return this;
        }

        public Builder setComment(final String comment) {
            this.comment = comment;
            return this;
        }

        public Builder setCreated(final String created) {
            this.created = created;
            return this;
        }

        public Builder setUpdated(final String updated) {
            this.updated = updated;
            return this;
        }

        public Builder setVisibility(final VisibilityJsonBean visibility) {
            this.visibility = visibility;
            return this;
        }

        public Builder setIsVisibilitySet(final boolean isVisibilitySet) {
            this.isVisibilitySet = isVisibilitySet;
            return this;
        }

        public Builder setStarted(final String started) {
            this.started = started;
            return this;
        }

        public Builder setTimeSpent(final String timeSpent) {
            this.timeSpent = timeSpent;
            return this;
        }

        public Builder setTimeSpentSeconds(final Long timeSpentSeconds) {
            this.timeSpentSeconds = timeSpentSeconds;
            return this;
        }

        public Builder setId(final String id) {
            this.id = id;
            return this;
        }

        public Builder setIssueId(final String issueId) {
            this.issueId = issueId;
            return this;
        }

        public WorklogJsonBean build() {
            return new WorklogJsonBean(self, author, updateAuthor, comment, created, updated, visibility, isVisibilitySet, started, timeSpent, timeSpentSeconds, id, issueId);
        }
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("self", self)
                .append("author", author)
                .append("updateAuthor", updateAuthor)
                .append("comment", comment)
                .append("created", created)
                .append("updated", updated)
                .append("visibility", visibility)
                .append("isVisibilitySet", isVisibilitySet)
                .append("started", started)
                .append("timeSpent", timeSpent)
                .append("timeSpentSeconds", timeSpentSeconds)
                .append("id", id)
                .append("issueId", issueId)
                .toString();
    }
}
