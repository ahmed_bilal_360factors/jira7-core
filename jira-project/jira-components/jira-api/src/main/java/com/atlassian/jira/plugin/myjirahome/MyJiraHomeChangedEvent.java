package com.atlassian.jira.plugin.myjirahome;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.user.ApplicationUser;

/**
 * Fired when a user changes his My JIRA Home location. The event should not contain the location itself
 * but the complete plugin module keys instead.
 *
 * @since 5.1
 */
@PublicApi
public class MyJiraHomeChangedEvent {
    private final ApplicationUser user;
    private final String fromHomePluginModuleKey;
    private final String toHomePluginModuleKey;

    /**
     * @since 6.4
     */
    public MyJiraHomeChangedEvent(final ApplicationUser user, final String fromHomePluginModuleKey,
                                  final String toHomePluginModuleKey) {
        this.user = user;
        this.fromHomePluginModuleKey = fromHomePluginModuleKey;
        this.toHomePluginModuleKey = toHomePluginModuleKey;
    }

    /**
     * @since 6.4
     */
    public ApplicationUser getApplicationUser() {
        return user;
    }

    public String getFromHomePluginModuleKey() {
        return fromHomePluginModuleKey;
    }

    public String getToHomePluginModuleKey() {
        return toHomePluginModuleKey;
    }
}
