package com.atlassian.jira.util;

import com.atlassian.annotations.ExperimentalApi;

import java.util.Date;
import java.util.Optional;

/**
 * Provides information about this jira instance.
 *
 * @since v7.1
 */
@ExperimentalApi
public interface InstanceInfo {

    /**
     * Returns create date (date with timezone information) of JIRA instance.
     *
     * @return create date (date with timezone information) of JIRA instance
     */
    Optional<Date> getInstanceCreatedDate();
}
