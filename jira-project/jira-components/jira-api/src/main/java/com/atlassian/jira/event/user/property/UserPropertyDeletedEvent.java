package com.atlassian.jira.event.user.property;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.entity.property.EntityProperty;
import com.atlassian.jira.event.entity.AbstractPropertyEvent;
import com.atlassian.jira.event.entity.EntityPropertyDeletedEvent;
import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.concurrent.Immutable;

/**
 * @since v6.5
 */
@PublicApi
@Immutable
public final class UserPropertyDeletedEvent extends AbstractPropertyEvent implements EntityPropertyDeletedEvent {
    public UserPropertyDeletedEvent(final EntityProperty entityProperty, final ApplicationUser user) {
        super(entityProperty, user);
    }
}
