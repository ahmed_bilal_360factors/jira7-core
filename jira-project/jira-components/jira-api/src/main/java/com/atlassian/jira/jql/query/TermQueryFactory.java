package com.atlassian.jira.jql.query;

import com.atlassian.jira.issue.index.DocumentConstants;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;

/**
 * This class is used to generate clauses to be added to Lucene's boolean queries.
 * JIRA uses two custom fields to keep track of what fields are "visible" (to JQL) in a document,
 * and what fields have missing value.
 *
 * Since Lucene doesn't index fields with missing values, we keep the custom field 'nonemptyfieldids'
 * up to date with all the field names in that document which don't have a value assigned to them.
 * This is used for example in queries such as "value IS empty".
 *
 *
 * @since v4.3
 */
final class TermQueryFactory {
    /**
     * @param fieldName the index field to be visible
     * @return the term query <code>visiblefieldids:fieldName</code>
     */
    static Query visibilityQuery(final String fieldName) {
        return new TermQuery(new Term(DocumentConstants.ISSUE_VISIBLE_FIELD_IDS, fieldName));
    }

    /**
     * @param fieldName the index field to be non empty
     * @return the term query <code>nonemptyfieldids:fieldName</code>
     */
    static Query nonEmptyQuery(final String fieldName) {
        return new TermQuery(new Term(DocumentConstants.ISSUE_NON_EMPTY_FIELD_IDS, fieldName));
    }
}
