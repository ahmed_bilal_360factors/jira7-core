package com.atlassian.jira.jql.resolver;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionManager;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Resolves Version objects and ids from their names.
 *
 * @since v4.0
 */
public class VersionResolver implements NameResolver<Version> {
    private final VersionManager versionManager;

    public VersionResolver(final VersionManager versionManager) {
        this.versionManager = versionManager;
    }

    public List<String> getIdsFromName(final String name) {
        notNull("name", name);

        //noinspection ConstantConditions - explicitly filtering out null values
        return versionManager.getVersionsByName(name).stream()
                .map(Version::getId)
                .filter(Objects::nonNull)
                .map(String::valueOf)
                .collect(CollectorsUtil.toImmutableList());
    }

    public boolean nameExists(final String name) {
        notNull("name", name);
        Collection<Version> versions = versionManager.getVersionsByName(name);
        return !versions.isEmpty();
    }

    public boolean idExists(final Long id) {
        notNull("id", id);
        return versionManager.getVersion(id) != null;
    }

    public Version get(final Long id) {
        return versionManager.getVersion(id);
    }

    ///CLOVER:OFF
    public Collection<Version> getAll() {
        return versionManager.getAllVersions();
    }
    ///CLOVER:ON
}
