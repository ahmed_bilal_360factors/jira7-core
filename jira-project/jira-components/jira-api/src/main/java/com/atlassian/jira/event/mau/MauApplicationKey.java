package com.atlassian.jira.event.mau;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.application.api.ApplicationKey;
import com.atlassian.jira.project.type.ProjectTypeKey;

import javax.annotation.Nonnull;
import java.util.Objects;

/**
 * Represents the application type that will be sent along side an eyeball event to track MAU (monthly active users).
 * <p>
 * This could be one of:
 * <ul>
 * <li>family - for common platform functionality (e.g. viewing a global dashboard)</li>
 * <li>unknown - for new pages that have not been classified yet</li>
 * <li>jira-core - for any business related pages (e.g. viewing an issue in a business project)</li>
 * <li>jira-software - for any software related pages (e.g. viewing a software board)</li>
 * <li>jira-servicedesk - for any servicedesk related pages (e.g. view agent queues)</li>
 * <li>future applications...</li>
 * </ul>
 *
 * @since v7.0.1
 */
@ExperimentalApi
public final class MauApplicationKey {
    private final String key;

    private static final MauApplicationKey FAMILY = new MauApplicationKey("family");
    private static final MauApplicationKey UNKNOWN = new MauApplicationKey("unknown");

    private MauApplicationKey(final String key) {
        this.key = key;
    }

    public static MauApplicationKey family() {
        return FAMILY;
    }

    public static MauApplicationKey unknown() {
        return UNKNOWN;
    }

    public static MauApplicationKey forApplication(@Nonnull ApplicationKey applicationKey) {
        return new MauApplicationKey(applicationKey.value());
    }

    public static MauApplicationKey forProjectTypeKey(@Nonnull ProjectTypeKey projectTypeKey) {
        switch (projectTypeKey.getKey()) {
            case "business":
                return new MauApplicationKey("jira-core");
            case "software":
                return new MauApplicationKey("jira-software");
            case "service_desk":
                return new MauApplicationKey("jira-servicedesk");
            default:
                return FAMILY;
        }
    }

    public String getKey() {
        return key;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final MauApplicationKey that = (MauApplicationKey) o;
        return Objects.equals(key, that.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key);
    }
}