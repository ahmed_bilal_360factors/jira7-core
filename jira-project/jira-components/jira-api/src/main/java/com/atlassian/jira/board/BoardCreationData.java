package com.atlassian.jira.board;

import com.atlassian.annotations.ExperimentalApi;

/**
 * Use this to create board
 * @since v7.1
 */
@ExperimentalApi
public class BoardCreationData {
    private String jql;
    private long projectId;

    private BoardCreationData(String jql, long projectId) {
        this.jql = jql;
        this.projectId = projectId;
    }

    public String getJql() {
        return jql;
    }

    public long getProjectId() {
        return projectId;
    }

    public static class Builder {
        private String jql;
        private long projectId;

        public Builder jql(String jql) {
            this.jql = jql;
            return this;
        }

        public Builder projectId(long projectId) {
            this.projectId = projectId;
            return this;
        }

        public BoardCreationData build() {
            return new BoardCreationData(this.jql, projectId);
        }
    }
}
