package com.atlassian.jira.config.properties;

import com.atlassian.annotations.Internal;
import com.atlassian.annotations.PublicApi;

@PublicApi
public interface APKeys {
    String JIRA_TITLE = "jira.title";
    String JIRA_BASEURL = "jira.baseurl";
    String JIRA_SETUP = "jira.setup";
    String JIRA_SETUP_IS_INSTANT = "jira.setup.is.instant";
    String JIRA_SETUP_INSTANT_USER = "jira.setup.instant.user";
    String JIRA_SETUP_MODE_DECIDED = "jira.setup.mode.decided";
    String JIRA_SETUP_CHOSEN_BUNDLE = "jira.setup.chosen.bundle";
    String JIRA_SETUP_WEB_SUDO_TOKEN = "jira.setup.web.sudo.token";
    String JIRA_MODE = "jira.mode";
    /**
     * @deprecated since 7.0.0. JIRA no longer uses this property so no replacement will be provided.
     */
    @Deprecated
    String JIRA_EDITION = "jira.edition";
    String JIRA_PATH_ATTACHMENTS = "jira.path.attachments";
    String JIRA_PRIMARY_STORAGE = "jira.primary.storage";
    String JIRA_SECONDARY_STORAGE = "jira.secondary.storage";
    /**
     * We are going to backup the attachments in the secondary location
     */
    String JIRA_SECONDARY_HOME = "jira.secondary.home";
    String JIRA_ATTACHMENT_PATH_ALLOWED = "jira.attachment.set.allowed";
    String JIRA_PATH_ATTACHMENTS_USE_DEFAULT_DIRECTORY = "jira.path.attachments.use.default.directory";
    String JIRA_ATTACHMENT_SIZE = "webwork.multipart.maxSize";
    /**
     * Backup path. @deprecated  Since 4.2.2
     */
    @Deprecated
    String JIRA_PATH_BACKUP = "jira.path.backup";
    String JIRA_INTRODUCTION = "jira.introduction";
    String JIRA_ALERT_HEADER = "jira.alertheader";
    String JIRA_ALERT_HEADER_VISIBILITY = "jira.alertheader.visibility";
    String JIRA_OPTION_USER_ASSIGNNEW = "jira.option.user.assignnew";
    String JIRA_OPTION_USER_EXTERNALMGT = "jira.option.user.externalmanagement";
    String JIRA_OPTION_USER_CROWD_ALLOW_RENAME = "jira.option.user.crowd.allow.rename";
    String JIRA_OPTION_ALLOWUNASSIGNED = "jira.option.allowunassigned";
    String JIRA_OPTION_VOTING = "jira.option.voting";
    String JIRA_OPTION_WATCHING = "jira.option.watching";
    String JIRA_OPTION_GLOBAL_SHARING = "jira.option.globalsharing";
    String JIRA_OPTION_ALLOWATTACHMENTS = "jira.option.allowattachments";
    String JIRA_OPTION_ALLOWSUBTASKS = "jira.option.allowsubtasks";
    /**
     * @deprecated since 6.3.3 JIRA needs indexing and it cannot be disabled. See {@link com.atlassian.jira.util.index.IndexLifecycleManager#isIndexAvailable()}
     */
    @Deprecated
    String JIRA_OPTION_INDEXING = "jira.option.indexing";
    String JIRA_OPTION_ISSUELINKING = "jira.option.issuelinking";
    /**
     * @deprecated since 6.2
     */
    @Deprecated
    String JIRA_OPTION_AUDITING = "jira.option.auditing";
    String JIRA_OPTION_AUDITING_LOG_RETENTION_PERIOD_IN_MONTHS = "jira.option.auditing.log.retention.period.in.months";
    String JIRA_OPTION_AUDITING_LOG_RETENTION_PERIOD_LAST_CHANGE_TIMESTAMP = "jira.option.auditing.log.retention.period.last.change.timestamp";

    String JIRA_OPTION_LOGOUT_CONFIRM = "jira.option.logoutconfirm";
    String JIRA_OPTION_EMAIL_VISIBLE = "jira.option.emailvisible";
    String JIRA_OPTION_EXCLUDE_PRECEDENCE_EMAIL_HEADER = "jira.option.precedence.header.exclude";
    String JIRA_OPTION_ALLOW_COOKIES = "jira.option.allowcookies";
    String JIRA_OPTION_IGNORE_URL_WITH_KEY = "jira.option.ignore.url.with.key";
    String JIRA_OPTION_KEY_DETECTION_BACKWARDS_COMPATIBLE = "jira.option.key.detection.backwards.compatible";
    String JIRA_OPTION_CAPTCHA_ON_SIGNUP = "jira.option.captcha.on.signup";
    String JIRA_OPTION_OPERATIONS_DISABLE = "jira.disable.operations.bar";
    String JIRA_OPTION_ENABLED_DARK_FEATURES = "jira.enabled.dark.features";

    /**
     * @deprecated since JIRA 6.3. Use {@link com.atlassian.jira.avatar.GravatarSettings} instead.
     */
    @Deprecated
    String JIRA_OPTION_USER_AVATAR_FROM_GRAVATAR = "jira.user.avatar.gravatar.enabled";

    String JIRA_OPTION_DISABLE_INLINE_EDIT = "jira.issue.inline.edit.disabled";

    String JIRA_OPTION_WEB_USEGZIP = "jira.option.web.usegzip";
    String JIRA_PATCHED_VERSION = "jira.version.patched";
    String JIRA_VERSION = "jira.version";
    String JIRA_DOWNGRADE_VERSION = "jira.downgrade.minimum.version";
    String JIRA_PATH_INDEX_USE_DEFAULT_DIRECTORY = "jira.path.index.use.default.directory";
    String JIRA_PATH_INDEX = "jira.path.index";
    String JIRA_UPGRADE_DELAY_MINS = "jira.upgrade.delay.minutes";
    String JIRA_UPGRADE_FORCE_MANUAL_SCHEDULE = "jira.upgrade.force.manual.schedule";

    //Note that this is a webwork property that will be used by the UI tag.
    //see ApplicationPropertiesConfiguration &  JiraConfiguration
    String JIRA_WEBWORK_ENCODING = "webwork.i18n.encoding";

    // Mail encoding
    String JIRA_MAIL_ENCODING = "jira.i18n.email.encoding";

    // Message handling
    String JIRA_OPTION_IGNORE_EMAIL_MESSAGE_ATTACHMENTS = "jira.option.ignore.email.message.attachments";
    String JIRA_MAIL_DEADLETTERS_EXPIRATION_TIME_DAYS = "jira.email.deadletters.expiration.time.days";

    //Property that holds the maximum number of issues that should be sent in an email.
    String JIRA_MAIL_MAX_ISSUES = "jira.subscription.email.max.issues";

    // internationalization (i18n)
    String JIRA_I18N_LANGUAGE_INPUT = "jira.i18n.language.index";
    String JIRA_I18N_DEFAULT_LOCALE = "jira.i18n.default.locale";
    String JIRA_I18N_SQL_LOCALE = "jira.i18n.sql.locale";
    String JIRA_I18N_SQL_COLLATOR_STRENGTH = "jira.i18n.sql.collator.strength";
    String JIRA_DEFAULT_TIMEZONE = "jira.default.timezone";

    /**
     * Specifies whether JIRA should embed i18n key meta-data when rendering pages.
     */
    String JIRA_I18N_INCLUDE_META_DATA = "jira.i18n.include.meta-data";

    // XSRF keys
    String JIRA_XSRF_ENABLED = "jira.xsrf.enabled";

    // Database Transaction Support Is Disabled
    String JIRA_DB_TXN_DISABLED = "jira.db.txns.disabled";

    //Look and Feel properties.
    // Note that you should not access these directly, but instead get an instance of
    // com.atlassian.jira.config.properties.LookAndFeelBean
    String JIRA_LF_LOGO_URL = "jira.lf.logo.url";
    String JIRA_LF_LOGO_WIDTH = "jira.lf.logo.width";
    String JIRA_LF_LOGO_HEIGHT = "jira.lf.logo.height";

    String JIRA_LF_FAVICON_URL = "jira.lf.favicon.url";
    String JIRA_LF_FAVICON_HIRES_URL = "jira.lf.favicon.hires.url";

    String JIRA_LF_TOP_BGCOLOUR = "jira.lf.top.bgcolour";
    String JIRA_LF_TOP_TEXTCOLOUR = "jira.lf.top.textcolour";
    String JIRA_LF_TOP_HIGHLIGHTCOLOR = "jira.lf.top.hilightcolour";
    String JIRA_LF_TOP_TEXTHIGHLIGHTCOLOR = "jira.lf.top.texthilightcolour";
    String JIRA_LF_TOP_SEPARATOR_BGCOLOR = "jira.lf.top.separator.bgcolor";


    String JIRA_LF_MENU_BGCOLOUR = "jira.lf.menu.bgcolour";
    String JIRA_LF_MENU_TEXTCOLOUR = "jira.lf.menu.textcolour";
    String JIRA_LF_MENU_SEPARATOR = "jira.lf.menu.separator";

    String JIRA_LF_HERO_BUTTON_TEXTCOLOUR = "jira.lf.hero.button.text.colour";
    String JIRA_LF_HERO_BUTTON_BASEBGCOLOUR = "jira.lf.hero.button.base.bg.colour";

    String JIRA_LF_TEXT_LINKCOLOUR = "jira.lf.text.linkcolour";
    String JIRA_LF_TEXT_ACTIVE_LINKCOLOUR = "jira.lf.text.activelinkcolour";

    String JIRA_LF_GADGET_COLOR_PREFIX = "jira.lf.gadget.";

    String JIRA_LF_APPLICATION_ID = "jira.lf.application.id";


    String JIRA_LF_TEXT_HEADINGCOLOUR = "jira.lf.text.headingcolour";

    String JIRA_LF_FIELD_LABEL_WIDTH = "jira.lf.field.label.width";

    //Date time format fields
    String JIRA_LF_DATE_TIME = "jira.lf.date.time";
    String JIRA_LF_DATE_DAY = "jira.lf.date.day";
    String JIRA_LF_DATE_COMPLETE = "jira.lf.date.complete";
    String JIRA_LF_DATE_DMY = "jira.lf.date.dmy";
    String JIRA_LF_DATE_RELATIVE = "jira.lf.date.relativize";


    //Descriptions for create issue fields
    String JIRA_ISSUE_DESC_ISSUETYPE = "jira.issue.desc.issuetype";
    String JIRA_ISSUE_DESC_SUMMARY = "jira.issue.desc.summary";
    String JIRA_ISSUE_DESC_PRIORITY = "jira.issue.desc.priority";
    String JIRA_ISSUE_DESC_COMPONENTS = "jira.issue.desc.components";
    String JIRA_ISSUE_DESC_VERSIONS = "jira.issue.desc.versions";
    String JIRA_ISSUE_DESC_FIXFOR = "jira.issue.desc.fixfor";
    String JIRA_ISSUE_DESC_ASSIGNEE = "jira.issue.desc.assignee";
    String JIRA_ISSUE_DESC_ENVIRONMENT = "jira.issue.desc.environment";
    String JIRA_ISSUE_DESC_DESCRIPTION = "jira.issue.desc.description";
    String JIRA_ISSUE_DESC_ORIGINAL_TIMETRACK = "jira.issue.desc.original.timetrack";
    String JIRA_ISSUE_DESC_TIMETRACK = "jira.issue.desc.timetrack";
    String JIRA_ISSUE_CACHE_CAPACITY = "jira.issue.cache.capacity";
    String JIRA_ISSUE_EXPIRE_TIME = "jira.issue.expire.time";
    String JIRA_ISSUE_FIELDS_CONFIG = "jira.issue.fields.config";

    //Default values for constants in the system
    String JIRA_CONSTANT_DEFAULT_ISSUE_TYPE = "jira.constant.default.issue.type";
    String JIRA_CONSTANT_DEFAULT_PRIORITY = "jira.constant.default.priority";
    String JIRA_CONSTANT_DEFAULT_RESOLUTION = "jira.constant.default.resolution";
    String JIRA_CONSTANT_DEFAULT_STATUS = "jira.constant.default.status";

    //Confirmed install of new version under Evaluation Terms with old license
    /**
     * @since 4.0
     * @deprecated since 7.0. See {@link com.atlassian.jira.license.JiraLicenseManager#getLicenses()}
     */
    @Deprecated
    String JIRA_LICENSE = "License20"; // since 4.0, until 7.0 for storing the license

    String JIRA_CONFIRMED_INSTALL_WITH_OLD_LICENSE = "jira.install.oldlicense.confirmed";
    String JIRA_CONFIRMED_INSTALL_WITH_OLD_LICENSE_USER = "jira.install.oldlicense.confirmed.user";
    String JIRA_CONFIRMED_INSTALL_WITH_OLD_LICENSE_TIMESTAMP = "jira.install.oldlicense.confirmed.timestamp";
    String JIRA_DATE_PICKER_JAVA_FORMAT = "jira.date.picker.java.format";
    String JIRA_DATE_PICKER_JAVASCRIPT_FORMAT = "jira.date.picker.javascript.format";
    String JIRA_ISSUENAV_CRITERIA_AUTOUPDATE = "jira.issuenav.criteria.autoupdate";
    String JIRA_DATE_TIME_PICKER_JAVA_FORMAT = "jira.date.time.picker.java.format";
    String JIRA_DATE_TIME_PICKER_JAVASCRIPT_FORMAT = "jira.date.time.picker.javascript.format";

    // The value jira.date.time.picker.use.iso8061 is incorrect (should be iso8601) but needs an upgrade task to change
    String JIRA_DATE_TIME_PICKER_USE_ISO8601 = "jira.date.time.picker.use.iso8061";
    @Deprecated
    /*
     * @Deprecated Use APKeys#JIRA_DATE_TIME_PICKER_USE_ISO8601
     */ String JIRA_DATE_TIME_PICKER_USE_ISO8061 = JIRA_DATE_TIME_PICKER_USE_ISO8601;

    String JIRA_THUMBNAIL_MAX_WIDTH = "jira.thumbnail.maxwidth";
    String JIRA_THUMBNAIL_MAX_HEIGHT = "jira.thumbnail.maxheight";
    String JIRA_OPTION_ALLOWTHUMBNAILS = "jira.option.allowthumbnails";
    String JIRA_THUMBNAILS_SCALING_FACTOR = "jira.thumbnails.scaling.factor";

    String JIRA_SCREENSHOTAPPLET_ENABLED = "jira.screenshotapplet.enabled";
    String JIRA_SCREENSHOTAPPLET_LINUX_ENABLED = "jira.screenshotapplet.linux.enabled";
    String JIRA_SEARCH_MAXCLAUSES = "jira.search.maxclauses";

    String JIRA_SENDMAIL_RECIPENT_BATCH_SIZE = "jira.sendmail.recipient.batch.size";

    // Clone issue
    String JIRA_CLONE_PREFIX = "jira.clone.prefix";
    String JIRA_CLONE_LINKTYPE_NAME = "jira.clone.linktype.name";
    /**
     * @deprecated since JIRA 6.1.1
     */
    @Deprecated
    String JIRA_CLONE_LINK_LEGACY_DIRECTION = "jira.clone.link.legacy.direction";

    //Maximum length of Project Names
    String JIRA_PROJECTNAME_MAX_LENGTH = "jira.projectname.maxlength";
    //Maximum length of Project Keys
    String JIRA_PROJECTKEY_MAX_LENGTH = "jira.projectkey.maxlength";

    // Project Key Regular Expressions
    String JIRA_PROJECTKEY_PATTERN = "jira.projectkey.pattern";
    String JIRA_PROJECTKEY_WARNING = "jira.projectkey.warning";
    String JIRA_PROJECTKEY_DESCRIPTION = "jira.projectkey.description";

    /**
     * List of reserved words that cannot be used for Project Keys
     */
    String JIRA_PROJECTKEY_RESERVEDWORDS_LIST = "jira.projectkey.reservedwords.list";

    // Import/Export keys
    String JIRA_IMPORT_CLEAN_XML = "jira.exportimport.cleanxml";
    String INCLUDE_USER_IN_MAIL_FROMADDRESS = "jira.option.include.user.in.mail.from.address";

    String EMAIL_FROMHEADER_FORMAT = "jira.email.fromheader.format";

    // Plugins
    String GLOBAL_PLUGIN_STATE_PREFIX = "jira.plugin.state-";
    String JIRA_PATH_PLUGINS = "jira.plugins";
    String JIRA_PATH_INSTALLED_PLUGINS = "jira.plugins.installed";
    String JIRA_PATH_PENDING_PLUGINS = "jira.plugins.pending";
    String JIRA_PATH_UNINSTALLED_PLUGINS = "jira.plugins.uninstalled";

    // Auto-Export
    String JIRA_AUTO_EXPORT = "jira.autoexport";

    String IMPORT_ID_PREFIX = "jira.importid.prefix";
    String IMPORT_ID_PREFIX_UNCONFIGURED = "unconfigured";
    String FULL_CONTENT_VIEW_PAGEBREAKS = "jira.search.fullcontentview.pagebreaks";

    // Database
    String DATABASE_QUERY_BATCH_SIZE = "jira.databasequery.batch.size";
    String ISSUE_INDEX_FETCH_SIZE = "jira.issueindex.fetch.size";
    String DEFAULT_JNDI_NAME = "jira.default.jndi.name";

    // Bulk User Management
    String USER_MANAGEMENT_MAX_DISPLAY_MEMBERS = "jira.usermanagement.maxdisplaymembers";

    // CVS
    String VIEWCVS_ROOT_TYPE = "jira.viewcvs.root.type";

    // Default schemes
    String DEFAULT_ISSUE_TYPE_SCHEME = "jira.scheme.default.issue.type";

    // Columns when viewing list of issues in the dashboard
    String ISSUE_TABLE_COLS_DASHBOARD = "jira.table.cols.dashboard";

    // Columns when viewing subtasks
    String ISSUE_TABLE_COLS_SUBTASK = "jira.table.cols.subtasks";

    /**
     * Sorting order of the linked issues listed in view issue screen
     */
    String JIRA_VIEW_ISSUE_LINKS_SORT_ORDER = "jira.view.issue.links.sort.order";

    /**
     * Wait time for a Lucene index file lock
     */
    String JIRA_INDEX_LOCK_WAITTIME = "jira.index.lock.waittime";

    /**
     * Time Tracking related property keys
     */
    String JIRA_OPTION_TIMETRACKING = "jira.option.timetracking";
    String JIRA_OPTION_TIMETRACKING_ESTIMATES_LEGACY_BEHAVIOUR = "jira.timetracking.estimates.legacy.behaviour";
    String JIRA_TIMETRACKING_COPY_COMMENT_TO_WORK_DESC_ON_TRANSITION = "jira.timetracking.copy.comment.to.work.desc.on.transition";
    String JIRA_TIMETRACKING_FORMAT = "jira.timetracking.format";
    String JIRA_TIMETRACKING_DEFAULT_UNIT = "jira.timetracking.default.unit";
    String JIRA_TIMETRACKING_HOURS_PER_DAY = "jira.timetracking.hours.per.day";
    String JIRA_TIMETRACKING_DAYS_PER_WEEK = "jira.timetracking.days.per.week";

    /**
     * Onboarding keys
     */
    String ONBOARDING_APP_USER_ID_THRESHOLD = "jira.onboarding.app_user.id.threshold";

    /**
     * Unsupported browser warnings
     */
    String JIRA_BROWSER_UNSUPPORTED_WARNINGS_DISABLED = "jira.browser.unsupported.warnings.disabled";

    /**
     * Number of issue indexes updates before automatic index optimization is triggered
     */
    String JIRA_MAX_REINDEXES = "jira.index.max.reindexes";

    /**
     * Number of issue indexes performed in bulk that will trigger index optimization
     */
    String JIRA_BULK_INDEX_UPDATE_OPTIMIZATION = "jira.index.update.bulk.optimization";

    /**
     * Whether AJAX enhancements for the edit issue screens should be enabled
     */
    String JIRA_ISSUE_OPERATIONS_AJAX_ENABLED = "jira.issue.operations.ajax.enabled";

    /**
     * Number of results to display in the AJAX autocomplete pickers
     */
    String JIRA_AJAX_AUTOCOMPLETE_LIMIT = "jira.ajax.autocomplete.limit";

    /**
     * Number of results to display in the label suggestions.  May be 0 for all suggestions.
     */
    String JIRA_AJAX_LABEL_SUGGESTION_LIMIT = "jira.ajax.autocomplete.labelsuggestion.limit";

    /**
     * Whether or not the issue picker is ajaxified or not
     */
    String JIRA_AJAX_ISSUE_PICKER_ENABLED = "jira.ajax.autocomplete.issuepicker.enabled";

    /**
     * The limit to the number of issue keys to store in the prev/next cache
     */
    String JIRA_PREVIOUS_NEXT_CACHE_SIZE = "jira.previous.next.cache.size";

    /**
     * The maximum number of results the issue navigator search views will request
     */
    String JIRA_SEARCH_VIEWS_DEFAULT_MAX = "jira.search.views.default.max";

    /**
     * The maximum number of results the issue navigator search views will return
     */
    String JIRA_SEARCH_VIEWS_MAX_LIMIT = "jira.search.views.max.limit";

    /**
     * The maxium number of history records to keep for a user
     */
    String JIRA_MAX_HISTORY_ITEMS = "jira.max.history.items";

    /**
     * The max number of entries to show for the history drop down
     */
    String JIRA_MAX_ISSUE_HISTORY_DROPDOWN_ITEMS = "jira.max.issue.history.dropdown.items";

    /**
     * The max number of entries to show for the history drop down
     */
    String JIRA_MAX_ADMIN_HISTORY_DROPDOWN_ITEMS = "jira.max.AdminPage.history.items";

    /**
     * The max number of entries to show for the filters drop down
     */
    String JIRA_MAX_FILTER_DROPDOWN_ITEMS = "jira.max.issue.filter.dropdown.items";

    /**
     * regardless of the above, users in this group will be able to request very large search requests
     */
    String JIRA_SEARCH_VIEWS_MAX_UNLIMITED_GROUP = "jira.search.views.max.unlimited.group";

    /**
     * limits the number of issues a user may select and edit in one go. *
     */
    String JIRA_BULK_EDIT_LIMIT_ISSUE_COUNT = "jira.bulk.edit.limit.issue.count";

    /**
     * limits the number of bulk transition errors shown on UI.
     */
    String JIRA_BULK_EDIT_LIMIT_TRANSITION_ERRORS = "jira.bulk.edit.limit.transition.errors";

    /**
     * Defines if a multipart get request should be handle *
     */
    String JIRA_DISABLE_MULTIPART_GET_HTTP_REQUEST = "jira.disable.multipart.get.http.request";

    /**
     * Defines policy for attachment downloads and ie mime sniffing.
     */
    String JIRA_OPTION_IE_MIME_SNIFFING = "jira.attachment.download.mime.sniffing.workaround";

    /**
     * Option to allow downloading attachments as a ZIP file
     */
    String JIRA_OPTION_ALLOW_ZIP_SUPPORT = "jira.attachment.allow.zip.support";

    /**
     * Defines the number of zip entries to show on the view issue screen
     */
    String JIRA_ATTACHMENT_NUMBER_OF_ZIP_ENTRIES_TO_SHOW = "jira.attachment.number.of.zip.entries";

    /**
     * Defines a comma-separated list of the file extensions that JIRA won't expand as a ZIP in the view issue screen.
     */
    String JIRA_ATTACHMENT_DO_NOT_EXPAND_AS_ZIP_EXTENSIONS_LIST = "jira.attachment.do.not.expand.as.zip.extensions.list";

    /**
     * mime sniffing policy allowing inlining of all attachments.
     */
    String MIME_SNIFFING_OWNED = "insecure";

    /**
     * mime sniffing policy allowing inlining of attachments except in ie when it would detect html and run scripts.
     */
    String MIME_SNIFFING_WORKAROUND = "workaround";

    /**
     * mime sniffing policy allowing no inlining of attachments forcing download with Content-Disposition header.
     */
    String MIME_SNIFFING_PARANOID = "secure";

    /**
     * maximum number of issues to display in a fragment in the Browse Project Summary tab panel
     */
    String JIRA_PROJECT_SUMMARY_MAX_ISSUES = "jira.project.summary.max.issues";

    /**
     * introduced as part of http://jira.atlassian.com/browse/JRA-6344
     */
    String JIRA_ASSIGNEE_CHANGE_IS_SENT_TO_BOTH_PARTIES = "jira.assignee.change.is.sent.to.both.parties";

    /**
     * Prefix for days previous limits for charts
     */
    String JIRA_CHART_DAYS_PREVIOUS_LIMIT_PREFIX = "jira.chart.days.previous.limit.";

    /**
     * Database id of the default avatar.
     */
    String JIRA_DEFAULT_AVATAR_ID = "jira.avatar.default.id";

    /**
     * Database id of the default user avatar.
     */
    String JIRA_DEFAULT_USER_AVATAR_ID = "jira.avatar.user.default.id";

    /**
     * Database id of the avatar for anonymous users
     */
    String JIRA_ANONYMOUS_USER_AVATAR_ID = "jira.avatar.user.anonymous.id";

    /**
     * Flag for whether to enable or disable autocomplete for JQL.
     */
    String JIRA_JQL_AUTOCOMPLETE_DISABLED = "jira.jql.autocomplete.disabled";

    /**
     * Used to limit the number of gadgets per dashboard. 20 by default
     */
    String JIRA_DASHBOARD_MAX_GADGETS = "jira.dashboard.max.gadgets";

    /**
     * If this is set to true, the login gadget wont get added to the system dashboard for logged out users
     */
    String JIRA_DISABLE_LOGIN_GADGET = "jira.disable.login.gadget";

    /**
     * This is the maximum failed authentication attempts allowed before things get serious
     */
    String JIRA_MAXIMUM_AUTHENTICATION_ATTEMPTS_ALLOWED = "jira.maximum.authentication.attempts.allowed";

    /**
     * Determines if the gadget upgrade message for applinks should still be displayed for administrators
     *
     * @deprecated no longer used as of JIRA 6.1. Will be removed in JIRA 7.0.
     */
    @Deprecated
    String JIRA_GADGET_APPLINK_UPGRADE_FINISHED = "jira.gadget.applink.upgrade.finished";

    /**
     * Returns a list of newline separated strings representing the http whitelist for JIRA
     *
     * @deprecated no longer used as of JIRA 6.1. Will be removed in JIRA 7.0.
     */
    @Deprecated
    String JIRA_WHITELIST_RULES = "jira.whitelist.rules";

    /**
     * Returns true if the http whitelist should be disabled.
     *
     * @deprecated no longer used as of JIRA 6.1. Will be removed in JIRA 7.0.
     */
    @Deprecated
    String JIRA_WHITELIST_DISABLED = "jira.whitelist.disabled";

    /**
     * Either asc or desc to define the default comments order.
     */
    String JIRA_ISSUE_ACTIONS_ORDER = "jira.issue.actions.order";

    /**
     * Returns true if we show the email form on the contact administrators form.
     */
    String JIRA_SHOW_CONTACT_ADMINISTRATORS_FORM = "jira.show.contact.administrators.form";

    /**
     * Returns message to show on the contact administrators form.
     */
    String JIRA_CONTACT_ADMINISTRATORS_MESSSAGE = "jira.contact.administrators.message";

    /**
     * The maximum number of characters to be entered for a single field.
     */
    String JIRA_TEXT_FIELD_CHARACTER_LIMIT = "jira.text.field.character.limit";

    /**
     * The limit of IDs to retrieve when doing a stable search.
     */
    String JIRA_STABLE_SEARCH_MAX_RESULTS = "jira.search.stable.max.results";

    /**
     * The number of Issues to keep in the cache
     */
    String JIRA_SEARCH_CACHE_MAX_SIZE = "jira.search.cache.max.size";

    /**
     * Returns true if advertisements in JIRA are disabled
     */
    String JIRA_OPTION_ADS_DISABLED = "jira.ads.disabled";

    /**
     * Returns mode of project description, either html or wiki
     */
    String JIRA_OPTION_PROJECT_DESCRIPTION_HTML_ENABLED = "jira.project.description.html.enabled";

    /**
     * Determines which editor should be used, RTE or the old one
     */
    String JIRA_OPTION_RTE_ENABLED = "jira.rte.enabled";

    /**
     * Returns if HTML(and JS) description is enabled.
     */
    String JIRA_OPTION_HTML_IN_CUSTOM_FIELDS_ENABLED = "jira.custom.field.html.enabled";

    /**
     * The number of entries to keep in the security level to permissions cache
     * @deprecated since 7.1.1
     */
    @Deprecated
    String JIRA_SECURITY_LEVEL_PERMISSIONS_CACHE_MAX_SIZE = "jira.security.level.permission.cache.max.size";

    /**
     * @deprecated since 6.2
     */
    @Deprecated
    String JIRA_OPTION_BTF_ANALYTICS_ENABLED = "jira.btf.analytics.enabled";

    @Internal
    String CACHE_ALL_USERS_AND_GROUPS = "jira.fullUserCache";

    @Internal
    String INTERN_USER_VALUES = "jira.internUserValues";

    /**
     * True to use Lucene for user searches, false to always use the database.
     *
     * @since 7.2
     */
    @Internal
    String LUCENE_USER_SEARCHER = "jira.luceneUserSearcher";

    class WebSudo {
        public static final String IS_DISABLED = "jira.websudo.is.disabled";
        public static final String TIMEOUT = "jira.websudo.timeout";

        private WebSudo() {
        }
    }

    /**
     * This setting is no longer needed and no longer has any effect.
     *
     * @deprecated No replacement needed. Since v7.0 (but pointless since v4.3 or so).
     */
    @Deprecated
    class TrustedApplications {
        public static final String USER_NAME_TRANSFORMER_CLASS = "jira.trustedapps.user.name.transformation.policy.class";

        private TrustedApplications() {
        }
    }

    final class Export {
        private static final String PREFIX = "jira.export.";

        public static final String FETCH_SIZE = PREFIX + "fetchsize";
    }

    final class Import {
        private static final String PREFIX = "jira.import.";

        public static final String MAX_QUEUE_SIZE = PREFIX + "maxqueuesize";
        public static final String THREADS = PREFIX + "threads";
    }

    /**
     * Lucene IndexWriter configuration
     */
    final class JiraIndexConfiguration {
        private static final String PREFIX = "jira.index.";

        public static final String MAX_FIELD_LENGTH = PREFIX + "maxfieldlength";
        public static final String COMMIT_FREQUENCY = PREFIX + "commitfrequency";
        public static final String COMMIT_POLICY = PREFIX + "commitpolicy";

        /**
         * Batch mode config
         */
        public static final class Batch {
            private static final String BATCH_PREFIX = PREFIX + "batch.";

            public static final String MERGE_FACTOR = BATCH_PREFIX + "mergefactor";
            public static final String MAX_MERGE_DOCS = BATCH_PREFIX + "maxmergedocs";
            public static final String MAX_BUFFERED_DOCS = BATCH_PREFIX + "maxbuffereddocs";
            public static final String MAX_RAM_BUFFER_SIZE_MB = BATCH_PREFIX + "maxrambuffermb";
        }

        /**
         * Interactive mode config
         */
        public static final class Interactive {
            private static final String INTERACTIVE_PREFIX = PREFIX + "interactive.";

            public static final String MERGE_FACTOR = INTERACTIVE_PREFIX + "mergefactor";
            public static final String MAX_MERGE_DOCS = INTERACTIVE_PREFIX + "maxmergedocs";
            public static final String MAX_BUFFERED_DOCS = INTERACTIVE_PREFIX + "maxbuffereddocs";
            public static final String MAX_RAM_BUFFER_SIZE_MB = INTERACTIVE_PREFIX + "maxrambuffermb";
        }

        /**
         * Merge policy config
         */
        public static final class MergePolicy {
            private static final String MERGE_POLICY_PREFIX = PREFIX + "mergepolicy.";

            public static final String EXPUNGE_DELETES_PCT_ALLOWED = MERGE_POLICY_PREFIX + "expungedeletespctallowed";
            public static final String FLOOR_SEGMENT_MB = MERGE_POLICY_PREFIX + "floorsegmentmb";
            public static final String MAX_MERGE_AT_ONCE = MERGE_POLICY_PREFIX + "maxmergeatonce";
            public static final String MAX_MERGE_AT_ONCE_EXPLICIT = MERGE_POLICY_PREFIX + "maxmergeatonceexplicit";
            public static final String MAX_MERGED_SEGMENT_MB = MERGE_POLICY_PREFIX + "maxmergedsegmentmb";
            public static final String NO_CFS_PCT = MERGE_POLICY_PREFIX + "nocfspct";
            public static final String SEGMENTS_PER_TIER = MERGE_POLICY_PREFIX + "segmentspertier";
            public static final String USE_COMPOUND_FILE = MERGE_POLICY_PREFIX + "usecompoundfile";
        }

        /**
         * Issue indexing
         */
        public static final class Issue {
            private static final String ISSUE_PREFIX = PREFIX + "issue.";

            public static final String MIN_BATCH_SIZE = ISSUE_PREFIX + "minbatchsize";
            public static final String MAX_QUEUE_SIZE = ISSUE_PREFIX + "maxqueuesize";
            public static final String THREADS = ISSUE_PREFIX + "threads";
        }

        /**
         * SharedEntity indexing
         */
        public static final class SharedEntity {
            private static final String SHARED_ENTITY_PREFIX = PREFIX + "sharedentity.";

            public static final String MIN_BATCH_SIZE = SHARED_ENTITY_PREFIX + "minbatchsize";
            public static final String MAX_QUEUE_SIZE = SHARED_ENTITY_PREFIX + "maxqueuesize";
            public static final String THREADS = SHARED_ENTITY_PREFIX + "threads";
        }

    }

    /**
     * Max number of schemes that can be compared in the scheme comparison tool
     */
    String JIRA_MAX_SCHEMES_FOR_COMPARISON = "jira.schemes.comparison.max";

    /**
     * Flag that enables/disables either Project Roles & Groups or Project Roles.
     */
    String COMMENT_LEVEL_VISIBILITY_GROUPS = "jira.comment.level.visibility.groups";

    /**
     * The minimum number of comments that can be hidden by comment collapsing. 0 means no comment can be hidden.
     */
    String COMMENT_COLLAPSING_MINIMUM_HIDDEN = "jira.comment.collapsing.minimum.hidden";

    /**
     * Key for JIRA's SID
     */
    String JIRA_SID = "jira.sid.key";

    /**
     * The last date at which all web resources should be flushed.
     */
    String WEB_RESOURCE_FLUSH_COUNTER = "jira.webresource.flushcounter";

    /**
     * Counter that can be used to flush super batched web-resources.
     */
    String WEB_RESOURCE_SUPER_BATCH_FLUSH_COUNTER = "jira.webresource.superbatch.flushcounter";

    /**
     * Show marketing links.  These is a cross product property.
     */
    String JIRA_SHOW_MARKETING_LINKS = "show.plugin.marketing.hints";

    /**
     * Database id of the default issueType avatar.
     */
    String JIRA_DEFAULT_ISSUETYPE_AVATAR_ID = "jira.avatar.issuetype.default.id";
    /**
     * Database id of the default issueType subtask avatar.
     */
    String JIRA_DEFAULT_ISSUETYPE_SUBTASK_AVATAR_ID = "jira.avatar.issuetype.subtask.default.id";


    /**
     * These are the Lucene indexing Languages, not the languages for displaying user messages.
     */
    class Languages {
        public static final String ARMENIAN = "armenian";
        public static final String BASQUE = "basque";
        public static final String BRAZILIAN = "brazilian";
        public static final String BULGARIAN = "bulgarian";
        public static final String CATALAN = "catalan";
        public static final String CHINESE = "chinese";
        public static final String CJK = "cjk";
        public static final String CZECH = "czech";
        public static final String DANISH = "danish";
        public static final String DUTCH = "dutch";
        public static final String ENGLISH = "english";
        public static final String ENGLISH_MODERATE_STEMMING = "english-moderate-stemming";
        public static final String ENGLISH_MINIMAL_STEMMING = "english-minimal-stemming";
        public static final String FINNISH = "finnish";
        public static final String FRENCH = "french";
        public static final String GERMAN = "german";
        public static final String GREEK = "greek";
        public static final String HUNGARIAN = "hungarian";
        public static final String ITALIAN = "italian";
        public static final String NORWEGIAN = "norwegian";
        public static final String PORTUGUESE = "portuguese";
        public static final String ROMANIAN = "romanian";
        public static final String RUSSIAN = "russian";
        public static final String SPANISH = "spanish";
        public static final String SWEDISH = "swedish";
        public static final String THAI = "thai";
        public static final String OTHER = "other";
    }
}
