package com.atlassian.jira.project.type;

import com.atlassian.annotations.PublicApi;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;

import javax.annotation.Nonnull;

/**
 * Utility class to format a {@link com.atlassian.jira.project.type.ProjectTypeKey}
 * so it can be displayed to users on copies.
 *
 * @since 7.0
 */
@PublicApi
public class ProjectTypeKeyFormatter {
    /**
     * Formats the given project type key and returns a version of it that is suitable to be displayed to users.
     * It substitutes underscores with spaces and capitalises the first letter of every word on the project type key.
     * <p>
     * Some examples:
     * <p>
     * - key: "business" -> formatted: "Business"
     * - key: "service_desk" -> formatted: "Service Desk"
     *
     * @param projectTypeKey The project type key to format
     * @return The formatted version of the project type key
     */
    public static String format(@Nonnull ProjectTypeKey projectTypeKey) {
        String key = projectTypeKey.getKey();
        if (StringUtils.isEmpty(key)) {
            return "Unknown";
        }
        String fromattedKey = StringUtils.replace(key, "_", " ").trim();
        return WordUtils.capitalize(fromattedKey);
    }
}
