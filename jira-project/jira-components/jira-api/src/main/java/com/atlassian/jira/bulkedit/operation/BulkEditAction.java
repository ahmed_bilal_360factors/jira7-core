package com.atlassian.jira.bulkedit.operation;

import com.atlassian.jira.issue.fields.OrderableField;
import com.atlassian.jira.web.bean.BulkEditBean;

public interface BulkEditAction {
    public boolean isAvailable(BulkEditBean bulkEditBean);

    public String getUnavailableMessage();

    public OrderableField getField();

    public String getFieldName();
}
