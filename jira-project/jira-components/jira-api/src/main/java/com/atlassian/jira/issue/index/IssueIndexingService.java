package com.atlassian.jira.issue.index;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.util.IssuesIterable;
import com.atlassian.jira.issue.worklog.Worklog;
import com.atlassian.jira.task.context.Context;
import com.atlassian.jira.util.InjectableComponent;

import java.util.Collection;
import java.util.Set;

/**
 * Provides services related to the indexing of issue data.
 * This service largely replaces the {@code IssueIndexManager} that previously provided
 * those services.
 *
 * @since v7.0
 */
@InjectableComponent
@PublicApi
public interface IssueIndexingService {
    /**
     * Reindex all issues.
     *
     * @return Reindex time in ms.
     */
    long reIndexAll() throws IndexException;

    /**
     * Reindex all issues.
     *
     * @param context                    used to report progress back to the user or to the logs. Must not be null
     * @param useBackgroundReindexing    whether to index in the background or not. If the useBackgroundReindexing option
     *                                   is set to true, then all related fields will not be reindexed.
     * @param updateReplicatedIndexStore whether to update the replicated index or not
     * @return Reindex time in ms.
     */
    long reIndexAll(Context context, boolean useBackgroundReindexing, boolean updateReplicatedIndexStore)
            throws IndexException;

    /**
     * Reindex all issues. If the useBackgroundReindexing option is set to true, then only the basic issue information
     * will be reindexed, unless the indexing parameters are also set. This is considered the normal mode for background
     * re-indexing and is sufficient to correct the index for changes in the system configuration, but not for changes
     * to the indexing language. If useBackgroundReindexing is set to false, than everything is always reindexed.
     *
     * @param context                    used to report progress back to the user or to the logs. Must not be null
     * @param useBackgroundReindexing    whether to index in the background or not
     * @param issueIndexingParams        determines witch related objects should be indexed together with issues. Only relevant
     *                                   for background reindex operations.
     * @param updateReplicatedIndexStore whether to update the replicated index or not
     * @return Reindex time in ms.
     */
    long reIndexAll(Context context, boolean useBackgroundReindexing, IssueIndexingParams issueIndexingParams,
                    boolean updateReplicatedIndexStore) throws IndexException;

    /**
     * Reindex a list of issues, passing an optional event that will be set progress
     *
     * @param issuesIterable IssuesIterable
     * @param context        used to report progress back to the user or to the logs. Must not be null.
     * @return Reindex time in ms.
     */
    long reIndexIssues(IssuesIterable issuesIterable, Context context) throws IndexException;

    /**
     * Reindex a list of issues, passing an optional event that will be set progress. This method can optionally also
     * index the comments and change history.
     *
     * @param issuesIterable      IssuesIterable
     * @param context             used to report progress back to the user or to the logs. Must not be null.
     * @param issueIndexingParams determines witch related objects should be indexed together with issue.
     * @return Reindex time in ms.
     */
    long reIndexIssues(IssuesIterable issuesIterable, Context context, IssueIndexingParams issueIndexingParams)
            throws IndexException;

    /**
     * Reindex an issue (eg. after field updates).
     */
    void reIndex(Issue issue) throws IndexException;

    /**
     * Reindex an issue (eg. after field updates).
     */
    void reIndex(Issue issue, IssueIndexingParams issueIndexingParams) throws IndexException;

    /**
     * Reindexes a collection of comments.
     *
     * @param comments a collection of Comment
     */
    long reIndexComments(Collection<Comment> comments) throws IndexException;

    /**
     * Reindexes a collection of comments.
     *
     * @param comments a collection of Comment
     * @param context  used to report progress back to the user or to the logs. Must not be null.
     */
    long reIndexComments(Collection<Comment> comments, Context context) throws IndexException;

    /**
     * Reindexes a collection of comments.
     *
     * @param comments                   a collection of Comment
     * @param context                    used to report progress back to the user or to the logs. Must not be null.
     * @param updateReplicatedIndexStore whether to update the replicated index or not
     */
    long reIndexComments(Collection<Comment> comments, Context context, boolean updateReplicatedIndexStore)
            throws IndexException;

    /**
     * Reindexes a collection of worklogs.
     *
     * @param worklogs a collection of Worklogs
     */
    long reIndexWorklogs(Collection<Worklog> worklogs) throws IndexException;

    /**
     * Reindexes a collection of worklogs.
     *
     * @param worklogs a collection of Worklogs
     * @param context  used to report progress back to the user or to the logs. Must not be null.
     */
    long reIndexWorklogs(Collection<Worklog> worklogs, Context context) throws IndexException;

    /**
     * Reindexes a collection of worklogs.
     *
     * @param worklogs                   a collection of Worklogs
     * @param context                    used to report progress back to the user or to the logs. Must not be null.
     * @param updateReplicatedIndexStore whether to update the replicated index or not
     */
    long reIndexWorklogs(Collection<Worklog> worklogs, Context context, boolean updateReplicatedIndexStore)
            throws IndexException;

    /**
     * Remove an issue from the search index.
     */
    void deIndex(Issue issue) throws IndexException;

    /**
     * Remove a set of issues from the search index.
     */
    void deIndexIssueObjects(Set<Issue> issuesToDelete, boolean updateReplicatedIndexStore) throws IndexException;

    /**
     * Reindex a set of issues.
     *
     * @param issueObjects Set of {@link com.atlassian.jira.issue.Issue}s to reindex.
     * @return Reindex time in ms.
     */
    long reIndexIssueObjects(final Collection<? extends Issue> issueObjects) throws IndexException;

    /**
     * Reindex a set of issues.
     *
     * @param issueObjects        Set of {@link com.atlassian.jira.issue.Issue}s to reindex.
     * @param issueIndexingParams Determines witch related objects should be indexed together with issues.
     * @return Reindex time in ms.
     */
    long reIndexIssueObjects(final Collection<? extends Issue> issueObjects, IssueIndexingParams issueIndexingParams)
            throws IndexException;

    /**
     * Reindex a set of issues.
     *
     * @param issueObjects               Set of {@link com.atlassian.jira.issue.Issue}s to reindex.
     * @param issueIndexingParams        Determines witch related objects should be indexed together with issues.
     * @param updateReplicatedIndexStore whether to store index operations in the replicated index store
     * @return Reindex time in ms.
     */
    long reIndexIssueObjects(final Collection<? extends Issue> issueObjects, IssueIndexingParams issueIndexingParams,
                             boolean updateReplicatedIndexStore) throws IndexException;
}
