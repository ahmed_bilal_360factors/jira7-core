package com.atlassian.jira.plugin.icon;

import com.atlassian.annotations.ExperimentalSpi;
import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.icon.IconOwningObjectId;
import com.atlassian.jira.icon.IconType;
import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * The IconTypePolicy defines the permissions around creating, deleting and viewing icons.
 * @since 7.1
 */
@ExperimentalSpi
public interface IconTypePolicy {
    /**
     * Check if the user can view the icon.
     *
     * @param remoteUser user whose permissions should be used. Null means anonymous access.
     * @param icon     The icon that the user wishes to access.
     * @return Returns true if the user is allowed to view the avatar, or false otherwise.
     */
    boolean userCanView(@Nullable ApplicationUser remoteUser, @Nonnull Avatar icon);

    /**
     * Check if the user can delete the icon.
     *
     * @param remoteUser user whose permissions should be used. Null means anonymous access.
     * @param icon     The icon that the user wishes to delete.
     * @return Returns true if the user is allowed to delete the avatar, or false otherwise.
     */
    boolean userCanDelete(@Nullable ApplicationUser remoteUser, @Nonnull Avatar icon);

    /**
     * Check if the user could create an avatar for this owning object.
     *
     * @param remoteUser     user whose permissions should be used. Null means anonymous access.
     * @param owningObjectId id of the object to which this avatar is connected to. This helps set the scope for
     *                       the provider of the {@link IconType} to determine permissions for icons that
     *                       have not been created yet.
     */
    boolean userCanCreateFor(@Nullable ApplicationUser remoteUser, @Nonnull final IconOwningObjectId owningObjectId);

}
