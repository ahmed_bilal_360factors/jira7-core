package com.atlassian.jira.config.properties;

import com.atlassian.annotations.Internal;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.gadgets.dashboard.Color;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.plugin.webresource.util.HashBuilder;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import static org.apache.commons.lang3.StringUtils.defaultString;
import static org.apache.commons.lang3.StringUtils.isEmpty;

/**
 * Bean mainly used to maintain a version number for all the menu colours.  This allows us
 * to cache this information until it is updated (i.e.:version number is incremented).
 */
public class LookAndFeelBean {
    private final ApplicationProperties applicationProperties;

    @Internal
    @ParametersAreNonnullByDefault
    enum LnF {
        TopBackgroundColor(APKeys.JIRA_LF_TOP_BGCOLOUR, DefaultCommonColours.TOP_BGCOLOUR),
        TopHighlightColor(APKeys.JIRA_LF_TOP_HIGHLIGHTCOLOR, DefaultCommonColours.TOP_HIGHLIGHTCOLOUR),
        TopTxtColour(APKeys.JIRA_LF_TOP_TEXTCOLOUR, DefaultColours.TOP_TEXTCOLOUR),
        TopTextHighlightColor(APKeys.JIRA_LF_TOP_TEXTHIGHLIGHTCOLOR, DefaultColours.TOP_TEXTHIGHLIGHTCOLOUR),
        TopSeparatorBackgroundColor(APKeys.JIRA_LF_TOP_SEPARATOR_BGCOLOR, DefaultCommonColours.TOP_SEPARATOR_BGCOLOUR),
        MenuTxtColour(APKeys.JIRA_LF_MENU_TEXTCOLOUR, DefaultColours.MENU_TEXTCOLOUR),
        MenuBackgroundColour(APKeys.JIRA_LF_MENU_BGCOLOUR, DefaultColours.MENU_BGCOLOUR),
        MenuSeparatorColour(APKeys.JIRA_LF_MENU_SEPARATOR, DefaultColours.MENU_SEPARATOR),
        TextHeadingColour(APKeys.JIRA_LF_TEXT_HEADINGCOLOUR, DefaultColours.TEXT_HEADINGCOLOR),
        TextLinkColour(APKeys.JIRA_LF_TEXT_LINKCOLOUR, DefaultColours.TEXT_LINKCOLOR),
        TextActiveLinkColour(APKeys.JIRA_LF_TEXT_ACTIVE_LINKCOLOUR, DefaultColours.TEXT_ACTIVELINKCOLOR),
        HeroButtonTextColour(APKeys.JIRA_LF_HERO_BUTTON_TEXTCOLOUR, DefaultCommonColours.HERO_BUTTON_TXTCOLOUR),
        HeroButtonBaseBGColour(APKeys.JIRA_LF_HERO_BUTTON_BASEBGCOLOUR, DefaultCommonColours.HERO_BUTTON_BASEBGCOLOUR),
        FaviconHiResUrl(APKeys.JIRA_LF_FAVICON_HIRES_URL, null),
        FaviconUrl(APKeys.JIRA_LF_FAVICON_URL, null),
        LogoHeight(APKeys.JIRA_LF_LOGO_HEIGHT, null),
        LogoWidth(APKeys.JIRA_LF_LOGO_WIDTH, null),
        LogoUrl(APKeys.JIRA_LF_LOGO_URL, null);

        @Nullable
        private final String defaultValue;
        private final String propertyKey;

        LnF(final String propertyKey, @Nullable final String defaultValue) {
            this.defaultValue = defaultValue;
            this.propertyKey = propertyKey;
        }
    }

    protected LookAndFeelBean(final ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }

    public static LookAndFeelBean getInstance(final ApplicationProperties ap) {
        return new LookAndFeelBean(ap);
    }

    /**
     * @return true
     * @deprecated in JIRA 6.0 -- the common header is always enabled.
     */
    @Deprecated
    public boolean isCommonHeader() {
        return true;
    }

    /**
     * @param colour
     * @return
     * @deprecated Since v7.0
     */
    @Deprecated
    public String stripHash(String colour) {
        if (StringUtils.isNotBlank(colour) && colour.startsWith("#")) {
            return colour.substring(1, colour.length());
        }
        return colour;
    }

    private void setValue(final LnF config, final String value) {
        setValue(config.propertyKey, value);
    }

    private void setValue(final String propertyKey, final String value) {
        applicationProperties.setString(propertyKey, value);
        updateSettingsHash();
    }

    /**
     * Convenience method used by data import (see JRA-11680) to update version number to
     * the greater version number after the import (to make sure LF values wont be cached).
     *
     * @param oldVersion the previous version
     * @deprecated Since 7.0.0. Use {@link #updateSettingsHash()}.
     */
    @Deprecated
    public void updateVersion(long oldVersion) {
        updateSettingsHash();
    }

    /**
     * @return 1 as we don't track version any more.
     * @deprecated Since 7.0.0. Use {@link #getSettingsHash()} to get the state of Look and feel.
     */
    @Deprecated
    public long getVersion() {
        return 1;
    }

    @Nonnull
    public String updateSettingsHash() {
        final String settingsHash = calculateHash();
        applicationProperties.setString(APKeys.WEB_RESOURCE_FLUSH_COUNTER, settingsHash);

        EventPublisher publisher = ComponentAccessor.getComponent(EventPublisher.class);
        if (publisher != null) {
            publisher.publish(new LookAndFeelChangedEvent());
        }

        return settingsHash;
    }

    @Nonnull
    public String getSettingsHash() {
        final String editVersion = applicationProperties.getDefaultBackedString(APKeys.WEB_RESOURCE_FLUSH_COUNTER);
        return isEmpty(editVersion) ? updateSettingsHash() : editVersion;
    }

    @Nonnull
    private String calculateHash() {
        final HashBuilder md5hash = new HashBuilder();
        for (LnF lnf : LnF.values()) {
            md5hash.add(lnf.propertyKey);
            md5hash.add("=");
            md5hash.add(defaultString(getValue(lnf), ""));
            md5hash.add("|");
        }

        for (Color color : Color.values()) {
            //color8 is chromeless and therefore special.
            if (color.equals(Color.color8)) {
                continue;
            }
            md5hash.add(APKeys.JIRA_LF_GADGET_COLOR_PREFIX);
            md5hash.add(color.name());
            md5hash.add("=");
            md5hash.add(defaultString(getGadgetChromeColor(color.name()), ""));
            md5hash.add("|");
        }
        return md5hash.build();
    }

    /*
     * ======== LOGO ==================
     */
    public String getLogoUrl() {
        return getValue(LnF.LogoUrl);
    }

    public void setLogoUrl(final String logoUrl) {
        setValue(LnF.LogoUrl, logoUrl);
    }

    public String getAbsoluteLogoUrl() {
        String jiraLogo = getLogoUrl();
        if (jiraLogo != null && !jiraLogo.startsWith("http://") && !jiraLogo.startsWith("https://")) {
            jiraLogo = ComponentAccessor.getComponent(WebResourceUrlProvider.class).getStaticResourcePrefix(UrlMode.AUTO) + jiraLogo;
        }

        return jiraLogo;
    }

    public String getLogoWidth() {
        return getValue(LnF.LogoWidth);
    }

    public void setLogoWidth(final String logoWidth) {
        setValue(LnF.LogoWidth, logoWidth);
    }

    public String getLogoPixelWidth() {
        return getLogoWidth() + "px";
    }

    public String getLogoHeight() {
        return getValue(LnF.LogoHeight);
    }

    public void setLogoHeight(final String logoHeight) {
        setValue(LnF.LogoHeight, logoHeight);
    }

    public String getLogoPixelHeight() {
        return getLogoHeight() + "px";
    }

    /*
    * ======== FAVICON ==================
    */
    public String getFaviconUrl() {
        return getValue(LnF.FaviconUrl);
    }

    public void setFaviconUrl(String faviconUrl) {
        setValue(LnF.FaviconUrl, faviconUrl);
    }

    public String getFaviconWidth() {
        return DefaultFaviconDimensions.FAVICON_DIMENSION;
    }

    public String getFaviconHeight() {
        return DefaultFaviconDimensions.FAVICON_DIMENSION;
    }

    public String getFaviconHiResUrl() {
        return getValue(LnF.FaviconHiResUrl);
    }

    public void setFaviconHiResUrl(String faviconUrl) {
        setValue(LnF.FaviconHiResUrl, faviconUrl);
    }

    public String getFaviconHiResWidth() {
        return DefaultFaviconDimensions.FAVICON_HIRES_DIMENSION;
    }

    public String getFaviconHiResHeight() {
        return DefaultFaviconDimensions.FAVICON_HIRES_DIMENSION;
    }

    /*
    * ======== TOP ==================
    */
    public String getTopBackgroundColour() {
        return getValue(LnF.TopBackgroundColor);
    }

    public void setTopBackgroundColour(final String topBackgroundColour) {
        setValue(LnF.TopBackgroundColor, topBackgroundColour);
    }

    public String getTopTxtColour() {
        return getValue(LnF.TopTxtColour);
    }

    public void setTopTxtColour(final String topTxtColour) {
        setValue(LnF.TopTxtColour, topTxtColour);
    }

    public String getTopHighlightColor() {
        return getValue(LnF.TopHighlightColor);
    }

    public void setTopHighlightColor(final String newValue) {
        setValue(LnF.TopHighlightColor, newValue);
    }

    @Nullable
    protected String getValue(@Nonnull LnF config) {
        return config.defaultValue != null ? getDefaultBackedString(config.propertyKey, config.defaultValue) : getDefaultBackedString(config.propertyKey);
    }

    public String getTopTextHighlightColor() {
        return getValue(LnF.TopTextHighlightColor);
    }

    public void setTopTextHighlightColor(final String newValue) {
        setValue(LnF.TopTextHighlightColor, newValue);
    }

    public String getTopSeparatorBackgroundColor() {
        return getValue(LnF.TopSeparatorBackgroundColor);
    }

    public void setTopSeparatorBackgroundColor(final String newValue) {
        setValue(LnF.TopSeparatorBackgroundColor, newValue);
    }

    /*
     * ======== MENU NAVIGATION ==================
     */
    public String getMenuTxtColour() {
        return getValue(LnF.MenuTxtColour);
    }

    public void setMenuTxtColour(final String menuTxtColour) {
        setValue(LnF.MenuTxtColour, menuTxtColour);
    }

    public String getMenuBackgroundColour() {
        return getValue(LnF.MenuBackgroundColour);
    }

    public void setMenuBackgroundColour(final String menuBackgroundColour) {
        setValue(LnF.MenuBackgroundColour, menuBackgroundColour);
    }

    public String getMenuSeparatorColour() {
        return getValue(LnF.MenuSeparatorColour);
    }

    public void setMenuSeparatorColour(final String menuSeparatorColour) {
        setValue(LnF.MenuSeparatorColour, menuSeparatorColour);
    }

    /*
     * ======== JIRA TEXT AND LINKS ==================
     */
    public String getTextHeadingColour() {
        return getValue(LnF.TextHeadingColour);
    }

    public void setTextHeadingColour(final String textHeadingColour) {
        setValue(LnF.TextHeadingColour, textHeadingColour);
    }

    public String getTextLinkColour() {
        return getValue(LnF.TextLinkColour);
    }

    public void setTextLinkColour(final String textLinkColour) {
        setValue(LnF.TextLinkColour, textLinkColour);
    }

    public String getTextActiveLinkColour() {
        return getValue(LnF.TextActiveLinkColour);
    }

    public void setTextActiveLinkColour(final String textActiveLinkColour) {
        setValue(LnF.TextActiveLinkColour, textActiveLinkColour);
    }

    /*
     * ======== HERO BUTTON COLOURS ===================
     */
    public String getHeroButtonTextColour() {
        return getValue(LnF.HeroButtonTextColour);
    }

    public void setHeroButtonTextColour(final String heroButtonTextColour) {
        setValue(LnF.HeroButtonTextColour, heroButtonTextColour);
    }

    public String getHeroButtonBaseBGColour() {
        return getValue(LnF.HeroButtonBaseBGColour);
    }

    public void setHeroButtonBaseBGColour(final String heroButtonBaseBGColour) {
        setValue(LnF.HeroButtonBaseBGColour, heroButtonBaseBGColour);
    }

    /*
     * ======== GADGET CHROME COLORS ==================
     */
    public String getGadgetChromeColor(final String id) {
        return applicationProperties.getDefaultBackedString(APKeys.JIRA_LF_GADGET_COLOR_PREFIX + id);
    }

    public void setGadgetChromeColor(final String id, final String gadgetChromeColor) {
        setValue(APKeys.JIRA_LF_GADGET_COLOR_PREFIX + id, gadgetChromeColor);
    }

    /*
     * ======== MISC ==================
     */
    public String getApplicationID() {
        return applicationProperties.getDefaultBackedString(APKeys.JIRA_LF_APPLICATION_ID);
    }

    /**
     * Performs a lookup on the application properties for the specified key. If the key returns a null value, returns
     * the default value specified. This would happen if no value exists in the database AND no value exists in the
     * jira-application.properties file.
     *
     * @param key          the Application Properties key to look up
     * @param defaultValue the value to return if the key yields null
     * @return the value of the key in the Application Properties, or the default value specified
     */
    public String getDefaultBackedString(@Nonnull final String key, @Nullable final String defaultValue) {
        final String value = applicationProperties.getString(key);
        return value == null ? defaultValue : value;
    }

    public String getDefaultBackedString(@Nonnull final String key) {
        // Rely on the real defaults if not set
        final String propertyValue = applicationProperties.getString(key);
        return propertyValue != null ? propertyValue : applicationProperties.getDefaultBackedString(key);
    }

    public static class LookAndFeelChangedEvent {
    }

    /**
     * These are the "intended" colours for the JIRA Header.
     *
     * @since v4.0
     */
    public static final class DefaultColours {
        public static final String TOP_HIGHLIGHTCOLOUR = "#296ca3";
        public static final String TOP_TEXTHIGHLIGHTCOLOUR = "#f0f0f0";
        public static final String TOP_SEPARATOR_BGCOLOUR = "#2e3d54";

        public static final String TOP_BGCOLOUR = "#205081";
        public static final String TOP_TEXTCOLOUR = "#ffffff";

        public static final String MENU_BGCOLOUR = "#3b73af";
        public static final String MENU_TEXTCOLOUR = "#ffffff";
        public static final String MENU_SEPARATOR = "#f0f0f0";

        public static final String TEXT_LINKCOLOR = "#3b73af";
        public static final String TEXT_ACTIVELINKCOLOR = "#3b73af";
        public static final String TEXT_HEADINGCOLOR = "#333333";

        private DefaultColours() {
        }
    }

    /**
     * These are the intended colours for the new common Header.
     *
     * @since v5.2
     */
    public static final class DefaultCommonColours {
        /**
         * @deprecated since JIRA 6.0. use {@link DefaultColours#TOP_HIGHLIGHTCOLOUR} directly.
         */
        public static final String TOP_HIGHLIGHTCOLOUR = DefaultColours.TOP_HIGHLIGHTCOLOUR;
        /**
         * @deprecated since JIRA 6.0. use {@link DefaultColours#TOP_SEPARATOR_BGCOLOUR} directly.
         */
        public static final String TOP_SEPARATOR_BGCOLOUR = DefaultColours.TOP_SEPARATOR_BGCOLOUR;
        /**
         * @deprecated since JIRA 6.0. use {@link DefaultColours#TOP_BGCOLOUR} directly.
         */
        public static final String TOP_BGCOLOUR = DefaultColours.TOP_BGCOLOUR;

        public static final String HERO_BUTTON_TXTCOLOUR = "#ffffff";
        public static final String HERO_BUTTON_BASEBGCOLOUR = "#3b7fc4";

        private DefaultCommonColours() {
        }
    }

    public static class DefaultFaviconDimensions {
        public static final String FAVICON_DIMENSION = "16";
        public static final String FAVICON_HIRES_DIMENSION = "32";

        private DefaultFaviconDimensions() {
        }
    }
}
