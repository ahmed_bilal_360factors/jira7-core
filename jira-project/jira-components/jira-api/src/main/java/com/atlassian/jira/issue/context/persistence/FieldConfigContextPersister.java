package com.atlassian.jira.issue.context.persistence;

import com.atlassian.annotations.Internal;
import com.atlassian.jira.issue.context.IssueContext;
import com.atlassian.jira.issue.context.JiraContextNode;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.project.Project;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.List;

// Should not be in the API - its leaked through FieldConfigScheme.Builder and FieldConfigSchemeImpl
@Internal
public interface FieldConfigContextPersister {
    /**
     * Store a fieldId/contextNode/scheme triplet.
     *
     * @since 6.4
     */
    void store(String fieldId, JiraContextNode contextNode, FieldConfigScheme fieldConfigScheme);

    List<JiraContextNode> getAllContextsForConfigScheme(FieldConfigScheme fieldConfigScheme);

    void removeContextsForConfigScheme(@Nonnull FieldConfigScheme fieldConfigScheme);

    /**
     * Bulk store fieldId/contextNode/scheme triplets.
     *
     * @since 6.4
     */
    void store(String fieldId, Collection<? extends JiraContextNode> contextNodes, FieldConfigScheme fieldConfigScheme);

    void removeContextsForProject(Project project);

    @Nullable
    Long getRelevantConfigSchemeId(@Nullable final Long projectId, @Nonnull final String fieldId);

    @Nullable
    Long getRelevantConfigSchemeId(@Nonnull final IssueContext issueContext, @Nonnull final String fieldId);
}
