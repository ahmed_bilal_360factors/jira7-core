package com.atlassian.jira.servermetrics;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.annotations.PublicApi;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.ThreadSafe;
import java.time.Duration;

/**
 * Allows services/manager to insert details about request processing time into
 * JIRA server metrics.
 *
 * Implementations automatically obtain information about current request from current thread.
 */
@ThreadSafe
@PublicApi
@ExperimentalApi
@ParametersAreNonnullByDefault
public interface ServerMetricsDetailCollector {
    /**
     * Insert information that some named point in request processing was reached.<br>
     * For server metrics purposes request will be divided into several named blocks
     * and time spent in each block will be logged.
     *
     * @param checkpointName name of checkpoint - should be whitelisted in jira analytics whitelist:
     *                       {@code jira-components/jira-plugins/jira-analytics-whitelist-plugin/src/main/resources/jira-analytics-whitelist-plugin-whitelist.json}
     *                       event {@code jira.http.request.stats}
     */
    void checkpointReached(String checkpointName);

    /**
     * This is special case of {@link #checkpointReached(String)} - add information about checkpoint being reached only
     * if checkpoint with given name was not visited before.
     */
    void checkpointReachedOnce(String checkpointName);

    /**
     * This is special case of {@link #checkpointReached(String)} - override previous information about checkpoint
     * with current information.
     */
    void checkpointReachedOverride(String checkpointName);

    /**
     * Adds time spent in given activity
     * @param activityName activity name i.e.: databaseRead, cacheLoad, etc...  - should be whitelisted in jira analytics whitelist:
     *                       {@code jira-components/jira-plugins/jira-analytics-whitelist-plugin/src/main/resources/jira-analytics-whitelist-plugin-whitelist.json}
     * @param duration duration that will be added to current activity time
     */
    void addTimeSpentInActivity(String activityName, Duration duration);
}
