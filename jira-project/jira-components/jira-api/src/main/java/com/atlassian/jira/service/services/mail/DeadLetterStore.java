package com.atlassian.jira.service.services.mail;

import com.atlassian.annotations.Internal;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.ofbiz.core.entity.EntityExpr;
import org.ofbiz.core.entity.EntityExprList;
import org.ofbiz.core.entity.EntityOperator;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.time.Clock;
import java.time.Duration;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * All parameters are not null by default except for messageId. When messageId is {@code null} no actions are taken
 * (methods that return a boolean will return {@code false}), effectively disabling the DeadLetterStore.
 * <p>
 * Expired dead letter entries are to be deleted <em>manually</em> with a call to {@link #deleteOldDeadLetters()}.
 * This method removes entries from dead letter table based on their {@code lastSeen} value.
 * Expiration time of 30 days is used by default. The value may be changed with {@link APKeys#JIRA_MAIL_DEADLETTERS_EXPIRATION_TIME_DAYS}
 * application property. Negative value effectively disables the dead letter store. Value of {@code 0} will remove all
 * dead letter entries on each call and any value &gt; 0 is interpreted as the number of days to keep a dead letter entry
 * before it's deleted.
 * <p>
 *
 * @since 7.0
 */
@Internal
@ParametersAreNonnullByDefault
class DeadLetterStore {
    private static final int DEFAULT_DEADLETTER_EXPIRATION_DAYS = 30;
    private static final Duration DEFAULT_DEADLETTER_EXPIRATION = Duration.ofDays(DEFAULT_DEADLETTER_EXPIRATION_DAYS);

    private static final String ENTITY = "DeadLetter";
    private static final String MESSAGE_ID = "messageId";
    private static final String LAST_SEEN = "lastSeen";
    private static final String MAILSERVER_ID = "mailServerId";
    private static final String FOLDER_NAME = "folderName";

    private static final AtomicBoolean logWarningOnlyOnce = new AtomicBoolean(false);

    private final Clock clock;
    private final Logger log;
    private final DeadLetterDao deadLetterDao;

    private final Duration expirationDuration;

    @VisibleForTesting
    DeadLetterStore(final OfBizDelegator ofBizDelegator, final Clock clock, final ApplicationProperties applicationProperties, final Logger log) {
        this.deadLetterDao = new DeadLetterDao(ofBizDelegator);
        this.clock = clock;
        this.log = log;
        this.expirationDuration = getDeadLetterExpirationDuration(applicationProperties);
    }

    DeadLetterStore(final Logger log) {
        this.clock = Clock.systemDefaultZone();
        this.log = checkNotNull(log);
        this.deadLetterDao = new DeadLetterDao(ComponentAccessor.getOfBizDelegator());
        this.expirationDuration = getDeadLetterExpirationDuration(ComponentAccessor.getApplicationProperties());
    }

    /**
     * Checks if corresponding dead letter entry exists.
     *
     * @param messageId    message id
     * @param mailServerId mail server id
     * @param folderName   folder name
     * @return {@code true} if for given messageId, mailServerId and folder exists an entry in dead letter table, {@code false} otherwise
     */
    public boolean exists(@Nullable final String messageId, final Long mailServerId, final String folderName) {
        if (messageId == null || isDisabled()) {
            return false;
        }
        checkNotNull(mailServerId);
        checkNotNull(folderName);
        return deadLetterDao.exists(messageId, mailServerId, folderName);
    }

    /**
     * Creates new dead letter entry with lastSeen value set to current time or updates lastSeen value of existing dead letter entry to current time.
     *
     * @param messageId    message id
     * @param mailServerId mail server id
     * @param folderName   folder name
     */
    void createOrUpdate(@Nullable final String messageId, final Long mailServerId, final String folderName) {
        if (messageId == null || isDisabled()) {
            return;
        }
        checkNotNull(mailServerId);
        checkNotNull(folderName);
        if (deadLetterDao.createOrUpdate(messageId, mailServerId, clock.instant().toEpochMilli(), folderName)) {
            logDebug("Adding to dead letter store. Will delete it the next run, message: %s", messageId);
        } else {
            logDebug("Message already added to dead letter store, updating its lastSeen time. Will delete it the next run, message: %s", messageId);
        }
    }

    /**
     * Removes dead letter entry.
     *
     * @param messageId    message id
     * @param mailServerId mail server id
     * @param folderName   folder name
     */
    public void delete(@Nullable final String messageId, final Long mailServerId, final String folderName) {
        if (messageId == null || isDisabled()) {
            return;
        }
        checkNotNull(mailServerId);
        checkNotNull(folderName);
        if (deadLetterDao.delete(messageId, mailServerId, folderName)) {
            logDebug("Removed from dead letter store, message: %s", messageId);
        }
    }

    /**
     * Removes all dead letter entries older than defined expiration time.
     */
    void deleteOldDeadLetters() {
        if (isDisabled()) {
            return;
        }
        final int removedDeadLetters = deadLetterDao.deleteOlderThan(clock.instant().minus(expirationDuration).toEpochMilli());
        logDebug("Removed %d expired dead letters from store", removedDeadLetters);
    }

    private boolean isDisabled() {
        return expirationDuration.isNegative();
    }

    private Duration getDeadLetterExpirationDuration(final ApplicationProperties applicationProperties) {
        final String expireTimeString = applicationProperties.getDefaultBackedString(APKeys.JIRA_MAIL_DEADLETTERS_EXPIRATION_TIME_DAYS);
        if (StringUtils.isEmpty(expireTimeString)) {
            logDebug("Property '%s' is not set. Using default value of %d days.", APKeys.JIRA_MAIL_DEADLETTERS_EXPIRATION_TIME_DAYS, DEFAULT_DEADLETTER_EXPIRATION_DAYS);
            return DEFAULT_DEADLETTER_EXPIRATION;
        }
        try {
            return Duration.ofDays(Integer.parseInt(expireTimeString));
        } catch (NumberFormatException e) {
            if (logWarningOnlyOnce.compareAndSet(false, true)) {
                log.warn(String.format("Invalid format of '%s' property = '%s'. Expected value to be an int. Using default value of %d days instead.",
                        APKeys.JIRA_MAIL_DEADLETTERS_EXPIRATION_TIME_DAYS, expireTimeString, DEFAULT_DEADLETTER_EXPIRATION_DAYS));
            }
            return DEFAULT_DEADLETTER_EXPIRATION;
        }
    }

    private void logDebug(final String msg, final Object... params) {
        if (log.isDebugEnabled()) {
            log.debug(String.format(msg, params));
        }
    }

    private static class DeadLetterDao {
        private final OfBizDelegator ofBizDelegator;

        DeadLetterDao(final OfBizDelegator ofBizDelegator) {
            this.ofBizDelegator = ofBizDelegator;
        }

        boolean createOrUpdate(final String messageId, final Long mailServerId, final long timestamp, final String folderName) {
            if (exists(messageId, mailServerId, folderName)) {
                ofBizDelegator.bulkUpdateByAnd(ENTITY, ImmutableMap.of(LAST_SEEN, timestamp),
                        ImmutableMap.of(MESSAGE_ID, messageId, MAILSERVER_ID, mailServerId, FOLDER_NAME, folderName));
                return false;
            } else {
                ofBizDelegator.createValue(ENTITY, ImmutableMap.of(MESSAGE_ID, messageId, MAILSERVER_ID, mailServerId, LAST_SEEN, timestamp, FOLDER_NAME, folderName));
                return true;
            }
        }

        boolean exists(final String messageId, final Long mailServerId, final String folderName) {
            return ofBizDelegator.findByCondition(ENTITY, messageIdMailServerAndFolderCondition(messageId, mailServerId, folderName), null, null).size() > 0;
        }

        boolean delete(final String messageId, final Long mailServerId, final String folderName) {
            return ofBizDelegator.removeByCondition(ENTITY, messageIdMailServerAndFolderCondition(messageId, mailServerId, folderName)) > 0;
        }

        private EntityExprList messageIdMailServerAndFolderCondition(final String messageId, final Long mailServerId, final String folderName) {
            final EntityExpr messageIdCondition = new EntityExpr(MESSAGE_ID, EntityOperator.EQUALS, messageId);
            final EntityExpr mailServerIdCondition = new EntityExpr(MAILSERVER_ID, EntityOperator.EQUALS, mailServerId);
            final EntityExpr folderNameCondition = new EntityExpr(FOLDER_NAME, EntityOperator.EQUALS, folderName);
            return new EntityExprList(ImmutableList.of(messageIdCondition, mailServerIdCondition, folderNameCondition), EntityOperator.AND);
        }

        int deleteOlderThan(final long timestamp) {
            return ofBizDelegator.removeByCondition(ENTITY, new EntityExpr(LAST_SEEN, EntityOperator.LESS_THAN, timestamp));
        }
    }

}
