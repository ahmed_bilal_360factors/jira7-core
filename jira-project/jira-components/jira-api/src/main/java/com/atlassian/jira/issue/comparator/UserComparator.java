package com.atlassian.jira.issue.comparator;

import com.atlassian.jira.user.ApplicationUser;

import java.util.Comparator;

public class UserComparator implements Comparator<ApplicationUser> {
    public int compare(ApplicationUser o1, ApplicationUser o2) {
        if (o1 == o2)
            return 0;

        if (o1 == null)
            return -1;

        if (o2 == null)
            return 1;

        return o1.getName().compareTo(o2.getName());
    }
}
