package com.atlassian.jira.issue.link;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.user.ApplicationUser;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.List;

/**
 * The implementations of this class are used to manage {@link IssueLinkType issue link types}
 * and {@link IssueLink issue links}.
 * <p>
 * <strong>WARNING</strong>: Prior to JIRA v7.0.1, most issue link queries were globally cached.
 * This is no longer the case, and developers are advised to cache this information locally where
 * they may have previously relied upon that caching behaviour for acceptable performance.
 * </p>
 */
@PublicApi
public interface IssueLinkManager {
    /**
     * Constructs a new issuelink from the sourceIssueId to the destinationId and persists it.  This operation will
     * cause a re-index of the associated issues.
     *
     * @param sourceIssueId      The source issue.
     * @param destinationIssueId The destination issue.
     * @param issueLinkTypeId    The type of issuelink
     * @param sequence           In which order the link will appear in the UI
     * @param remoteUser         Needed for creation of change items.
     * @throws CreateException If there is an error when creating the "Change Item" for this operation. Note that the Link itself has most likely been created.
     */
    void createIssueLink(Long sourceIssueId, Long destinationIssueId, Long issueLinkTypeId, Long sequence, ApplicationUser remoteUser) throws CreateException;

    /**
     * Removes a single issue link
     * We do not check for permission here. It should be done before this method is called. For example, in the action.
     *
     * @param issueLink  the issue link to remove
     * @param remoteUser needed for creation of change items
     * @throws IllegalArgumentException if the supplied issueLink is null.
     */
    void removeIssueLink(IssueLink issueLink, ApplicationUser remoteUser);

    /**
     * Removes ALL incoming and outgoing issuelinks from the issue supplied.
     *
     * @param issue      the Issue
     * @param remoteUser the remote user
     * @return The total number of issuelinks deleted.
     * @deprecated Use {@link #removeIssueLinks(com.atlassian.jira.issue.Issue, com.atlassian.crowd.embedded.api.User)} instead. Since v5.0.
     */
    int removeIssueLinks(GenericValue issue, ApplicationUser remoteUser);

    /**
     * Removes ALL incoming and outgoing issuelinks from the issue supplied.
     *
     * @param issue      the Issue
     * @param remoteUser the remote user
     * @return The total number of issuelinks deleted.
     */
    int removeIssueLinks(Issue issue, ApplicationUser remoteUser);

    /**
     * Removes ALL incoming and outgoing issuelinks from the issue supplied without creating ChangeItems for the Change History.
     * <p>
     * You would normally want to use the other method which creates the ChangeItems - this method is only intended for
     * use during Issue Delete.
     *
     * @param issue the Issue
     * @return The total number of issuelinks deleted.
     * @see #removeIssueLinks(Issue, ApplicationUser)
     */
    int removeIssueLinksNoChangeItems(Issue issue);

    /**
     * Constructs a {@link LinkCollection} for a given issue.
     *
     * @param issue      the Issue
     * @param remoteUser the remote user
     * @return A {@link LinkCollection} with all the issues ingoing and outgoing issue links
     * @deprecated use {@link #getLinkCollection(Issue, ApplicationUser)} instead
     */
    LinkCollection getLinkCollection(GenericValue issue, ApplicationUser remoteUser);

    /**
     * @param issue              the issue
     * @param remoteUser         the user performing the search
     * @param excludeSystemLinks whether or not to exclude system links
     * @return A {@link LinkCollection} with all the issues ingoing and outgoing issue links
     * @see #getLinkCollection(com.atlassian.jira.issue.Issue, com.atlassian.crowd.embedded.api.User)
     * @since v4.4.2
     */
    LinkCollection getLinkCollection(Issue issue, ApplicationUser remoteUser, boolean excludeSystemLinks);

    /**
     * Constructs a {@link LinkCollection} for a given issue.
     *
     * @param issue      the issue
     * @param remoteUser the user performing the search
     * @return A {@link LinkCollection} with all the issues ingoing and outgoing issue links
     * @since v4.0
     */
    LinkCollection getLinkCollection(Issue issue, ApplicationUser remoteUser);

    /**
     * Constructs a {@link LinkCollection} for a given issue, ignoring security.
     *
     * @param issue the issue
     * @return A {@link LinkCollection} with all the issues ingoing and outgoing issue links
     * @since v4.0
     */
    LinkCollection getLinkCollectionOverrideSecurity(Issue issue);

    /**
     * Returns a collection of all {@link IssueLink}s for a particular issue link type
     *
     * @param issueLinkTypeId ID of the Issue Link Type
     * @return A collection of {@link IssueLink}s
     */
    Collection<IssueLink> getIssueLinks(Long issueLinkTypeId);

    /**
     * Returns the {@link IssueLink} with the specified id.
     *
     * @param issueLinkId the issue link id.
     * @return the {@link IssueLink}. Can be NULL, if no issue link found.
     */
    IssueLink getIssueLink(Long issueLinkId);

    /**
     * Get links from an issue.
     *
     * @param sourceIssueId Eg. from {@link Issue#getId()}
     * @return List of {@link IssueLink}s. This list will be immutable.
     */
    List<IssueLink> getOutwardLinks(Long sourceIssueId);

    /**
     * Get links to an issue.
     *
     * @param destinationIssueId Eg. from {@link Issue#getId()}
     * @return List of {@link IssueLink}s. This list will be immutable.
     */
    List<IssueLink> getInwardLinks(Long destinationIssueId);

    /**
     * Moves an issue link to a different position in the list of issuelink.
     * <b>NOTE:</b> This is currently only used when re-ordering sub-tasks.
     *
     * @param issueLinks      The list of issueLinks
     * @param currentSequence The postion of the issuelink about to be moved
     * @param sequence        The target position of the issuelink
     * @throws IllegalArgumentException If currentSequence or sequence are null
     */
    void moveIssueLink(List<IssueLink> issueLinks, Long currentSequence, Long sequence);

    /**
     * Sets the sequence number for each issueLink in the List of issueLinks provided
     * according to its position in the List.
     *
     * @param issueLinks A list of issue links to be recalculated
     */
    void resetSequences(List<IssueLink> issueLinks);

    /**
     * Retrieves an issue link given a source, destination and a link type.
     *
     * @param sourceId        the ID of the source issue
     * @param destinationId   the ID of the destination issue
     * @param issueLinkTypeId the issue link type
     * @return an {@link IssueLink} or null if this link does not exist
     */
    @Nullable
    IssueLink getIssueLink(Long sourceId, Long destinationId, Long issueLinkTypeId);

    /**
     * Changes the type of an issue link.
     * <b>NOTE:</b> It is not possible to convert a system link type to a non-system link type and vice versa.
     *
     * @param issueLink    the issue link
     * @param swapLinkType the new link type
     * @param remoteUser   the user requesting this change
     */
    void changeIssueLinkType(IssueLink issueLink, IssueLinkType swapLinkType, ApplicationUser remoteUser);

    /**
     * Returns whether Issue Linking is currently enabled in JIRA. Issue Linking can be enabled or
     * disabled in the Admin section of JIRA.
     */
    boolean isLinkingEnabled();

    /**
     * Does nothing.
     * This manager no longer has a cache.
     *
     * @deprecated No replacement needed; just stop calling it. Since v7.0.
     */
    @Deprecated
    default void clearCache() {}
}
