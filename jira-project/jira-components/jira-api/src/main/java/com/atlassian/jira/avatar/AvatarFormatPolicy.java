package com.atlassian.jira.avatar;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.annotations.Internal;
import com.atlassian.jira.io.ResourceData;

import java.io.IOException;
import java.io.InputStream;

/**
 * Used to request a specific format of an avatar image.
 *
 * @since v7.0.1
 */
@ExperimentalApi
public abstract class AvatarFormatPolicy {
    /**
     * @return a policy that will always use the actual content type of the avatar.
     */
    public static AvatarFormatPolicy createOriginalDataPolicy() {
        return new OriginalAvatarFormatPolicy();
    }

    /**
     * @return a policy builder that is expecting avatar in PNG format. If not possible fallback strategy will be applied.
     */
    public static PngAvatarFormatPolicyBuilder createPngFormatPolicy() {
        return new PngAvatarFormatPolicyBuilder();
    }

    /**
     * Builder used for building {@link PngAvatarFormatPolicy}.
     */
    public static class PngAvatarFormatPolicyBuilder {
        /**
         * Please use {@link AvatarFormatPolicy#createPngFormatPolicy} in order to create an instance.
         */
        private PngAvatarFormatPolicyBuilder() {
        }

        /**
         * @return a policy that is expecting avatar in PNG format and fail with throwing
         * an {@link IllegalArgumentException} if not possible.
         */
        public AvatarFormatPolicy withRejectingFallbackStrategy() {
            return new PngAvatarFormatPolicy(originalData -> {
                throw new IllegalArgumentException("Strictly requested PNG avatar data, but the avatar is of different type and cannot be transcoded");
            });
        }

        /**
         * @return a policy that is expecting avatar in PNG format and fall back to avatar's original data
         * if transcoding is not possible.
         */
        public AvatarFormatPolicy withFallingBackToOriginalDataStrategy() {
            return new PngAvatarFormatPolicy(originalData -> originalData);
        }
    }

    @Internal
    abstract ResourceData getData(
            final Avatar avatar,
            final InputStreamSupplier originalData,
            final InputStreamSupplier transcodedData
    ) throws IOException;

    @Internal
    interface InputStreamSupplier {
        InputStream get() throws IOException;
    }
}
