package com.atlassian.jira.application;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.annotations.Internal;
import com.atlassian.application.api.ApplicationKey;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.fugue.Option;
import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Set;

/**
 * Provides low-level read and write capabilities regarding {@link ApplicationRole}s. It is generally recommended to use
 * {@link ApplicationAuthorizationService} or {@link ApplicationRoleAdminService} rather than this class, as the service
 * layer contains additional validation and business logic.
 *
 * @see com.atlassian.jira.application.ApplicationAuthorizationService
 * @see com.atlassian.jira.application.ApplicationRoleAdminService
 * @see ApplicationRole
 * @since 7.0
 */
public interface ApplicationRoleManager {
    /**
     * Returns the {@link ApplicationRole} identified by the given {@link com.atlassian.application.api.ApplicationKey},
     * or {@link Option#none} if no such {@link ApplicationRole} exists.
     * <p>
     * Note: {@link ApplicationRole}s returned by this method are guaranteed to be backed by a (potentially exceeded)
     * license, however the installation state of the plugin/product that uses this role may not necessarily
     * physically installed.
     * </p>
     *
     * @param key the {@link com.atlassian.application.api.ApplicationKey} of the role to search for.
     * @return the {@link ApplicationRole} associated with the passed role identifier, or {@link Option#none}.
     */
    @Nonnull
    Option<ApplicationRole> getRole(@Nonnull ApplicationKey key);

    /**
     * Returns an immutable {@link Set} of all {@link ApplicationRole}s that are backed by a (potentially exceeded)
     * license.
     *
     * @return the {@link Set} of all {@link ApplicationRole}s that are backed by a (potentially exceeded) license.
     */
    @Nonnull
    Set<ApplicationRole> getRoles();

    /**
     * Returns an immutable {@link Set} of {@link ApplicationRole}s that are configured be default for new users.
     *
     * @return the {@link Set} of {@link ApplicationRole}s that are configured be default for new users.
     * @deprecated Use {@link #getDefaultApplicationKeys()} instead. Since v7.0
     */
    @Nonnull
    @Deprecated
    @ExperimentalApi
    Set<ApplicationRole> getDefaultRoles();

    /**
     * Returns an immutable {@link Set} of {@link ApplicationKey}s that are configured as the default applications for new users.
     *
     * @return the {@link Set}  of {@link ApplicationKey}s that are configured as the default applications for new users.
     * @see ApplicationRoleManager#getDefaultRoles() for defaul roles.
     */
    @Nonnull
    Set<ApplicationKey> getDefaultApplicationKeys();

    /**
     * Returns true if the given user has been assigned to any {@link ApplicationRole} that is backed by a
     * (potentially exceeded) license.
     *
     * @return true if the given user has been assigned to any {@link ApplicationRole}
     * that is backed by a (potentially exceeded) license.
     */
    boolean hasAnyRole(@Nullable ApplicationUser user);

    /**
     * Returns {@code true} if the passed user belongs to the {@code ApplicationRole} associated with the given key.
     * NOTE: This does not check that the application is licensed with a valid license.
     *
     * @param user the user to check - if this is null, returns {@code false}
     * @param key  the key corresponding to the {@code ApplicationRole}
     * @return true if the user belongs to a group of the given application
     */
    boolean userHasRole(@Nullable ApplicationUser user, ApplicationKey key);

    /**
     * Returns {@code true} if the passed user occupies a seat in the {@code ApplicationRole} associated with the given key.
     *
     * @param user the user to check - if this is null, returns {@code false}
     * @param key  the key corresponding to the {@code ApplicationRole}
     * @return true if the user occupies seat in the given application
     */
    boolean userOccupiesRole(@Nullable ApplicationUser user, ApplicationKey key);

    /**
     * Returns true if the user limits of all of the given user's assigned {@link ApplicationRole}s have been exceeded.
     * The user limit of an {@link ApplicationRole} is exceeded when the combined number of users assigned to that role
     * (through assigned groups) exceeds the number of seats granted for that role in its license.
     * <p>
     * <strong>Note</strong>: this method does NOT take into account whether licenses for application roles are
     * {@link com.atlassian.jira.license.LicenseDetails#isExpired() expired}; this must be tested separately.
     * </p>
     *
     * @param user the user whose roles will be checked.
     * @return {@code true} if all of the {@code user}'s roles have user counts that exceed the number of seats
     * granted by licenses.
     */
    boolean hasExceededAllRoles(@Nonnull ApplicationUser user);

    /**
     * Returns true if the number of users assigned to any {@link ApplicationRole} exceeds the number of seats granted
     * to that role by its license.
     * <p>
     * <strong>Note</strong>: this method does NOT take into account whether licenses for installed roles are
     * {@link com.atlassian.jira.license.LicenseDetails#isExpired() expired}; this must be tested separately.
     * </p>
     *
     * @return true if the number of users for any role is greater than the number of seats granted to that role by
     * its license.
     */
    boolean isAnyRoleLimitExceeded();

    /**
     * Returns {@code true} if the number of users assigned to the given {@code ApplicationRole} exceeds the
     * number of seats granted by its license. This method unconditionally returns {@code false} if the passed role
     * is not considered to be installed (ie: backed by a license).
     * <p>
     * <strong>Note</strong>: this method does NOT take into account whether licenses for installed roles are
     * {@link com.atlassian.jira.license.LicenseDetails#isExpired() expired}; this must be tested separately.
     * </p>
     *
     * @param role the identifier of the {@code ApplicationRole}.
     * @return {@code true} if the installed {@link ApplicationRole} is exceeded or {@code false} otherwise.
     */
    boolean isRoleLimitExceeded(@Nonnull ApplicationKey role);

    /**
     * Returns the {@link Set} of {@link ApplicationRole}s granted to the given {@link ApplicationUser}.
     *
     * @param user the user whose roles will be checked.
     * @return the {@link Set} of {@link ApplicationRole}s for the given {@code user}.
     */
    Set<ApplicationRole> getRolesForUser(@Nonnull ApplicationUser user);

    /**
     * Returns the {@link Set} of {@link ApplicationRole}s that the given {@link ApplicationUser} occupies seats in.
     * {@link ApplicationUser} has to actively take a seat in the returned {@link ApplicationRole}s
     * eg. for an {@link ApplicationUser} who has access to SOFTWARE and implicitly (or explicitly) CORE, they will only have SOFTWARE returned.
     * For an {@link ApplicationUser} who has explicit access to SOFTWARE and CORE - with the SOFTWARE license exceeded,
     * this will return both as the user effectively occupies a seat in both CORE and SOFTWARE now.
     *
     * @param user the user whose roles will be checked.
     * @return the {@link Set} of {@link ApplicationRole}s that the given {@code user} is actively taking seats.
     */
    Set<ApplicationRole> getOccupiedLicenseRolesForUser(@Nonnull ApplicationUser user);

    /**
     * Returns the {@link Set} of {@link ApplicationRole}s associated with the given {@link Group}, or groups for
     * which the given group is a nested group (sub-group).
     *
     * @param group the group which roles will be checked.
     * @return the {@link Set} of {@link ApplicationRole}s associated with the group or its super-groups.
     */
    Set<ApplicationRole> getRolesForGroup(@Nonnull Group group);

    /**
     * Get the {@link Set} of group names that have been associated with all the {@link ApplicationRole}s
     * that have a backing (but potentially exceeded) license.
     *
     * @return Groups associated with all the {@link ApplicationRole}s that have a backing (but potentially
     * exceeded) license.
     */
    @Nonnull
    Set<Group> getGroupsForLicensedRoles();

    /**
     * Removes any/all associations of the given group from all {@link ApplicationRole}s
     * (irrespective of whether the {@link ApplicationRole}s is backed by a license).
     *
     * @param group the group to remove.
     */
    void removeGroupFromRoles(@Nonnull Group group);

    /**
     * <p>
     * Determines whether an application identified by the given {@link ApplicationKey}
     * is installed and running in this JIRA instance <strong>AND</strong> has a backing license key.
     * </p>
     *
     * @param key the key that identifies the {@code ApplicationRole} backed by a (potentially exceeded) license.
     * @return {@code true} when the {@link ApplicationRole} backed by a (potentially exceeded) license
     * has an associated application installed and running in this JIRA instance.
     */
    boolean isRoleInstalledAndLicensed(@Nonnull ApplicationKey key);

    /**
     * Save the passed {@link ApplicationRole} information to the database.
     * This method will only accept the passed role if:
     * <ol>
     * <li>The role is backed by a (potentially exceeded) license.</li>
     * <li>The role only contains currently valid groups.</li>
     * <li>The default group are not a subset of the groups in the role.</li>
     * </ol>
     *
     * @param role the role to save.
     * @return the role as persisted to the database.
     * @throws java.lang.IllegalArgumentException if passed role does not contain valid groups, valid default groups
     *                                            or if the role does not have a backing license.
     */
    @Nonnull
    ApplicationRole setRole(@Nonnull ApplicationRole role);

    /**
     * Retrieve the number of active users for the given {@link ApplicationRole}.
     * It will uniquely count all users who are found in the groups associated with the application.
     *
     * @param key the key that identifies the {@code ApplicationRole}.
     * @return the number of active users for the given {@code ApplicationRole}, or zero if the
     * {@code ApplicationRole} does not exist.
     */
    int getUserCount(@Nonnull ApplicationKey key);

    /**
     * Retrieve the number of available (unoccupied) user seats for the {@link ApplicationRole}
     * backed by a (potentially exceeded) license.
     *
     * @param key the key that identifies the licensed {@code ApplicationRole}.
     * @return the number of remaining users seats. Will return zero when there are more users than seats licensed, the
     * {@code ApplicationRole} is not valid or there are as many active users as seats in the license.
     * When the license is unlimited, it will return minus one (-1)
     * @see ApplicationRoleManager#getUserCount(com.atlassian.application.api.ApplicationKey) for finding occupied user seats.
     */
    int getRemainingSeats(@Nonnull ApplicationKey key);

    /**
     * Determines whether the {@link ApplicationRole} backed by a license
     * has the requested number of user seats available.
     *
     * @param key       the key that identifies the {@code ApplicationRole}.
     * @param seatCount the number of user seats that this {@code ApplicationRole} should have capacity for.
     * @return {@code true} if the {@code ApplicationRole} for the provided {@code ApplicationKey}
     * has the number of user seats available.
     * {@code false} if the there are not enough seats available.
     */
    boolean hasSeatsAvailable(@Nonnull ApplicationKey key, int seatCount);

    /**
     * Get the default {@link com.atlassian.crowd.embedded.api.Group}s associated with the {@link ApplicationRole}
     * backed by a (potentially exceeded) license.
     *
     * @param key the key that identifies the {@code ApplicationRole}.
     * @return the {@code Set} of default groups associated with the {@code ApplicationRole}.
     */
    @Nonnull
    Set<Group> getDefaultGroups(@Nonnull ApplicationKey key);

    /**
     * Determines whether {@link ApplicationRole}s are enabled.
     *
     * @return {@code true} when {@code ApplicationRole} are enabled, {@code false} otherwise.
     * @deprecated since 7.0.1 as this always returns true in JIRA 7
     */
    @Internal
    @Deprecated
    default boolean rolesEnabled() {
        return true;
    }
}
