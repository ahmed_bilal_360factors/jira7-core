package com.atlassian.jira.plugin.util;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.web.bean.PagerFilter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * @since v3.10
 */
public class TabPanelUtil {
    public static final int MAX_ISSUES_TO_DISPLAY = 50;
    public static final PagerFilter PAGER_FILTER = new PagerFilter(MAX_ISSUES_TO_DISPLAY); //only show the top X voted issues
    private static final Long NO_VOTES = (long) 0;

    /**
     * Returns a sub-set of the collection that has up to 'subset' number of entries.
     * Returns an empty collection in case of the collection being null or empty.
     *
     * @param collection collection of versions
     * @param subset     max number of entries in the returning collection
     * @return collection of entries, may be empty but never null
     */
    public static Collection subSetCollection(Collection collection, int subset) {
        if (collection == null || collection.isEmpty()) {
            return Collections.emptyList();
        } else if (subset >= 0 && subset <= collection.size()) {
            //if subset is set, only return a subset of versions.
            return new ArrayList(collection).subList(0, subset);
        } else {
            return collection;
        }
    }


    /**
     * Removes the issue with 0 votes from the given list.
     * Note: It also removes it if {@link com.atlassian.jira.issue.Issue#getVotes()} returns null.
     *
     * @param issues list of {@link com.atlassian.jira.issue.Issue} objects
     */
    public static void filterIssuesWithNoVotes(List issues) {
        if (issues != null) {
            for (Iterator iterator = issues.iterator(); iterator.hasNext(); ) {
                final Long votes = ((Issue) iterator.next()).getVotes();
                if (votes == null || NO_VOTES.equals(votes)) {
                    iterator.remove();
                }
            }
        }
    }

}
