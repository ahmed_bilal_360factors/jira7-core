package com.atlassian.jira.issue.customfields.config.item;

import com.atlassian.annotations.Internal;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.option.GenericImmutableOptions;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.FieldConfigItemType;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;

@Internal
public class ProjectOptionsConfigItem implements FieldConfigItemType {
    private static final Logger log = LoggerFactory.getLogger(ProjectOptionsConfigItem.class);

    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final PermissionManager permissionManager;

    public ProjectOptionsConfigItem(PermissionManager permissionManager, JiraAuthenticationContext jiraAuthenticationContext) {
        this.permissionManager = permissionManager;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    public String getDisplayName() {
        return "Project options";
    }

    public String getDisplayNameKey() {
        return "admin.issuefields.customfields.config.project.options";
    }

    public String getViewHtml(FieldConfig fieldConfig, FieldLayoutItem fieldLayoutItem) {
        return jiraAuthenticationContext.getI18nHelper().getText("admin.issuefields.customfields.config.project.options.all");
    }

    public String getObjectKey() {
        return "options";
    }

    public Object getConfigurationObject(Issue issue, FieldConfig config) {
        try {
            final List<Project> originalList = new ArrayList<>(permissionManager.getProjects(BROWSE_PROJECTS, jiraAuthenticationContext.getUser()));
            return new GenericImmutableOptions<>(originalList, config);
        } catch (UnsupportedOperationException e) {
            log.error("Unable to retrieve projects. Likely to be an issue with SubvertedPermissionManager. Please restart to resolve the problem.", e);
            return null;
        }
    }

    public String getBaseEditUrl() {
        return null;
    }
}
