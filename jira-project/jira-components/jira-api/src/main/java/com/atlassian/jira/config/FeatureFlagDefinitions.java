package com.atlassian.jira.config;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import java.util.Set;

import static java.lang.String.format;
import static org.apache.commons.lang.Validate.isTrue;
import static org.apache.commons.lang.Validate.notNull;

/**
 * A FeatureFlagDefinitions is designed to be used in a static variable declaration while still capturing the flags
 * defined via it.
 * <p>
 * Declare a static of this type and then declare specific {@link com.atlassian.jira.config.FeatureFlag}
 * definitions as static variables as well.
 * <p>
 * <pre>
 * <code>
 *
 * public class WellKnownFeatureFlags
 * {
 *    static FeatureFlagDefinitions FLAGS = new FeatureFlagDefinitions();
 *
 *    public final static FeatureFlag TEST_FEATURE_ONE = FLAGS.featureFlag("test.feature.one").define();
 *
 *    public final static FeatureFlag TEST_FEATURE_TWO = FLAGS.featureFlag("test.feature.two").onByDefault().define();
 * }
 * </code>
 * </pre>
 *
 * @since 7.1
 */
public class FeatureFlagDefinitions {
    private final Set<FeatureFlag> flags = Sets.newCopyOnWriteArraySet();

    /**
     * @return the set of feature flags defined by this object
     */
    public Set<FeatureFlag> getFeatureFlags() {
        return ImmutableSet.copyOf(flags);
    }

    /**
     * Begin a a new feature flag definition that can be stored in a static variable.  It must follow the validation
     * rules of this class
     *
     * @param featureKey the key of the feature
     * @return an intermediary builder object that can be called on to finish the registration
     */
    public Definition featureFlag(String featureKey) {
        validate(featureKey);
        return new Definition(FeatureFlag.featureFlag(featureKey));
    }

    private void validate(final String featureKey) {
        notNull(featureKey, "featureKey must not be null");

        isTrue(featureKeyIsNotEndWithPrefix(featureKey),
                "Invalid feature key provided, key should be of the form 'my.feature.flag', NOT 'my.feature.flag.enabled' or 'my.feature.flag.disabled'");

        isTrue(featureKeyDoesNotExists(featureKey), format("Feature key %s already registered", featureKey));
    }

    private boolean featureKeyIsNotEndWithPrefix(final String featureKey) {
        return !(featureKey.endsWith(FeatureFlag.POSTFIX_ENABLED) || featureKey.endsWith(FeatureFlag.POSTFIX_DISABLED));
    }

    private boolean featureKeyDoesNotExists(final String featureKey) {
        for (FeatureFlag featureFlag : flags) {
            if (featureKey.equals(featureFlag.featureKey())) {
                return false;
            }
        }
        return true;
    }


    /**
     * A builder intermediate class that allows for static definition
     */
    public class Definition {
        private FeatureFlag flag;

        Definition(final FeatureFlag flag) {
            this.flag = flag;
        }

        public Definition offByDefault() {
            this.flag = flag.offByDefault();
            return this;
        }

        public Definition onByDefault() {
            this.flag = flag.onByDefault();
            return this;
        }

        /**
         * This will define the feature flag, add it to the parent FeatureFlagDefinitions and return the created.
         *
         * @return the created feature flag
         */
        public FeatureFlag define() {
            flags.add(flag);
            return flag;
        }
    }

}
