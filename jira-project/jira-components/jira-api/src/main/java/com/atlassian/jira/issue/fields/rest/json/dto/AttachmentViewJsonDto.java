package com.atlassian.jira.issue.fields.rest.json.dto;

import com.google.common.base.Objects;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.joda.time.ReadableInstant;

import javax.annotation.concurrent.Immutable;
import java.net.URI;

/**
 * A Data Transfer Object used for serving information that are required for attachment presentation
 *
 * @since v6.5
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect
@Immutable
public class AttachmentViewJsonDto {
    @JsonProperty
    private final long id;

    @JsonProperty
    private final boolean latest;

    @JsonProperty
    private final boolean deletable;

    @JsonProperty
    private final boolean expandable;

    @JsonProperty
    private final URI thumbnailUrl;

    @JsonProperty
    private final int thumbnailWidth;

    @JsonProperty
    private final int thumbnailHeight;

    @JsonProperty
    private final URI attachmentUrl;

    @JsonProperty
    private final String authorDisplayName;

    @JsonProperty
    private final String authorKey;

    @JsonProperty
    private final String fileSize;

    @JsonProperty
    private final String fileName;

    @JsonProperty
    private final String mimeType;

    @JsonProperty
    private final ReadableInstant createdIso8601;

    @JsonProperty
    private final String createdDateTime;

    AttachmentViewJsonDto(
            final long id,
            final boolean latest,
            final boolean deletable,
            final boolean expandable,
            final URI thumbnailUrl,
            final int thumbnailWidth,
            final int thumbnailHeight,
            final URI attachmentUrl,
            final String authorDisplayName,
            final String authorKey,
            final String fileSize,
            final String fileName,
            final String mimeType,
            final ReadableInstant createdIso8601,
            final String createdDateTime) {
        this.id = id;
        this.thumbnailUrl = thumbnailUrl;
        this.authorDisplayName = authorDisplayName;
        this.fileName = fileName;
        this.mimeType = mimeType;
        this.latest = latest;
        this.deletable = deletable;
        this.expandable = expandable;
        this.thumbnailWidth = thumbnailWidth;
        this.thumbnailHeight = thumbnailHeight;
        this.attachmentUrl = attachmentUrl;
        this.authorKey = authorKey;
        this.fileSize = fileSize;
        this.createdIso8601 = createdIso8601;
        this.createdDateTime = createdDateTime;
    }

    public long getId() {
        return id;
    }

    public boolean isLatest() {
        return latest;
    }

    public boolean isDeletable() {
        return deletable;
    }

    public boolean isExpandable() {
        return expandable;
    }

    public URI getThumbnailUrl() {
        return thumbnailUrl;
    }

    public int getThumbnailWidth() {
        return thumbnailWidth;
    }

    public int getThumbnailHeight() {
        return thumbnailHeight;
    }

    public URI getAttachmentUrl() {
        return attachmentUrl;
    }

    public String getAuthorDisplayName() {
        return authorDisplayName;
    }

    public String getAuthorKey() {
        return authorKey;
    }

    public String getFileSize() {
        return fileSize;
    }

    public String getFileName() {
        return fileName;
    }

    public String getMimeType() {
        return mimeType;
    }

    public ReadableInstant getCreatedIso8601() {
        return createdIso8601;
    }

    public String getCreatedDateTime() {
        return createdDateTime;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, latest, deletable, expandable, thumbnailUrl, thumbnailWidth, thumbnailHeight, attachmentUrl, authorDisplayName, authorKey, fileSize, fileName, mimeType, createdIso8601, createdDateTime);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final AttachmentViewJsonDto other = (AttachmentViewJsonDto) obj;
        return Objects.equal(this.id, other.id)
                && Objects.equal(this.latest, other.latest)
                && Objects.equal(this.deletable, other.deletable)
                && Objects.equal(this.expandable, other.expandable)
                && Objects.equal(this.thumbnailUrl, other.thumbnailUrl)
                && Objects.equal(this.thumbnailWidth, other.thumbnailWidth)
                && Objects.equal(this.thumbnailHeight, other.thumbnailHeight)
                && Objects.equal(this.attachmentUrl, other.attachmentUrl)
                && Objects.equal(this.authorDisplayName, other.authorDisplayName)
                && Objects.equal(this.authorKey, other.authorKey)
                && Objects.equal(this.fileSize, other.fileSize)
                && Objects.equal(this.fileName, other.fileName)
                && Objects.equal(this.mimeType, other.mimeType)
                && Objects.equal(this.createdIso8601, other.createdIso8601)
                && Objects.equal(this.createdDateTime, other.createdDateTime);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("id", id)
                .add("latest", latest)
                .add("deletable", deletable)
                .add("expandable", expandable)
                .add("thumbnailUrl", thumbnailUrl)
                .add("thumbnailWidth", thumbnailWidth)
                .add("thumbnailHeight", thumbnailHeight)
                .add("attachmentUrl", attachmentUrl)
                .add("authorDisplayName", authorDisplayName)
                .add("authorKey", authorKey)
                .add("fileSize", fileSize)
                .add("fileName", fileName)
                .add("mimeType", mimeType)
                .add("createdIso8601", createdIso8601)
                .add("createdDateTime", createdDateTime)
                .toString();
    }
}
