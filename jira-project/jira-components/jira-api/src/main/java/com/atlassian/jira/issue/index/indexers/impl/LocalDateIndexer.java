package com.atlassian.jira.issue.index.indexers.impl;

import com.atlassian.jira.datetime.LocalDate;
import com.atlassian.jira.datetime.LocalDateFactory;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.index.DocumentConstants;
import com.atlassian.jira.util.LuceneUtils;
import com.atlassian.jira.web.FieldVisibilityManager;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * @since v4.4
 */
public class LocalDateIndexer extends AbstractCustomFieldIndexer {
    private static final Logger log = LoggerFactory.getLogger(LocalDateIndexer.class);

    public LocalDateIndexer(FieldVisibilityManager fieldVisibilityManager, CustomField customField) {
        super(fieldVisibilityManager, customField);
    }

    @Override
    public void addDocumentFieldsSearchable(Document doc, Issue issue) {
        addDocumentFields(doc, issue, Field.Index.NOT_ANALYZED_NO_NORMS);
    }

    @Override
    public void addDocumentFieldsNotSearchable(Document doc, Issue issue) {
        addDocumentFields(doc, issue, Field.Index.NO);
    }

    private void addDocumentFields(final Document doc, final Issue issue, final Field.Index indexType) {
        Object value = customField.getValue(issue);
        if (value instanceof Date || value == null) {
            Date date = (Date) value;
            try {
                LocalDate localDate = LocalDateFactory.from(date);
                if (date != null) {
                    doc.add(new Field(getDocumentFieldId(), LuceneUtils.localDateToString(localDate), Field.Store.YES, indexType));
                }

                final LocalDate localSortDate;
                if (indexType == Field.Index.NOT_ANALYZED_NO_NORMS) {
                    localSortDate = localDate;
                } else {
                    // if it is not indexable for searching, then always ensure it's value is handled as null for sorting
                    localSortDate = null;
                }
                doc.add(new Field(DocumentConstants.LUCENE_SORTFIELD_PREFIX + getDocumentFieldId(), LuceneUtils.localDateToString(localSortDate), Field.Store.NO, Field.Index.NOT_ANALYZED_NO_NORMS));
            } catch (RuntimeException ex) {
                // eg LocalDateFactory.from(date); will throw IllegalArgumentException on BCE dates
                log.warn("Unable to index custom date field '" + customField.getName() + "(" + customField.getId() + ") with value: " + value);
            }
        }
    }
}
