package com.atlassian.jira.issue.customfields.config.item;

import com.atlassian.annotations.Internal;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.option.GenericImmutableOptions;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.FieldConfigItemType;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;

@Internal
public class VersionOptionsConfigItem implements FieldConfigItemType {
    private static final Logger log = LoggerFactory.getLogger(VersionOptionsConfigItem.class);

    private final VersionManager versionManager;
    private final JiraAuthenticationContext jiraAuthenticationContext;

    /**
     * @deprecated since 6.4.7.
     */
    @Deprecated
    public VersionOptionsConfigItem(VersionManager versionManager) {
        this.versionManager = versionManager;
        this.jiraAuthenticationContext = ComponentAccessor.getJiraAuthenticationContext();
    }

    public VersionOptionsConfigItem(VersionManager versionManager, JiraAuthenticationContext jiraAuthenticationContext) {
        this.versionManager = versionManager;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    public String getDisplayName() {
        return "Version options";
    }

    public String getDisplayNameKey() {
        return "admin.issuefields.customfields.config.version.options";
    }

    public String getViewHtml(FieldConfig fieldConfig, FieldLayoutItem fieldLayoutItem) {
        return jiraAuthenticationContext.getI18nHelper().getText("admin.issuefields.customfields.config.version.options.all");
    }

    public String getObjectKey() {
        return "options";
    }

    public Object getConfigurationObject(Issue issue, FieldConfig config) {
        if (issue != null && issue.getProjectId() != null) {
            return new GenericImmutableOptions(versionManager.getVersions(issue.getProjectId()), config);
        } else {
            return new GenericImmutableOptions(Collections.emptyList(), config);
        }
    }

    public String getBaseEditUrl() {
        return null;
    }
}
