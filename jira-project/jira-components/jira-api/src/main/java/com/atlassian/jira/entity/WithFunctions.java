package com.atlassian.jira.entity;

import com.atlassian.annotations.ExperimentalApi;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;

import javax.annotation.Nullable;
import java.util.Collection;

/**
 * @since v7.0
 */
@ExperimentalApi
public class WithFunctions {

    public static Function<WithId, Long> getId() {
        return new Function<WithId, Long>() {
            @Override
            @Nullable
            public Long apply(@Nullable WithId input) {
                return input != null ? input.getId() : null;
            }
        };
    }

    public static Iterable<Long> getIds(Iterable<? extends WithId> withIds) {
        return Iterables.transform(withIds, getId());
    }

    public static Iterable<Long> getIds(Collection<? extends WithId> withIds) {
        return Iterables.transform(withIds, getId());
    }

    public static Function<WithKey, String> getKey() {
        return new Function<WithKey, String>() {
            @Override
            @Nullable
            public String apply(@Nullable WithKey input) {
                return input != null ? input.getKey() : null;
            }
        };
    }
}
