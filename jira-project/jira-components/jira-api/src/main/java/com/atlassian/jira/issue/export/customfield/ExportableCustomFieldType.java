package com.atlassian.jira.issue.export.customfield;

import com.atlassian.annotations.ExperimentalSpi;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.export.FieldExportPart;
import com.atlassian.jira.issue.export.FieldExportParts;

/**
 * A Custom Field that can export a {@link FieldExportParts} for use by exporters.
 *
 * @since 7.2.0
 */
@ExperimentalSpi
public interface ExportableCustomFieldType {
    /**
     * Get the custom field representation of the issue, this object contains the column headers and values for each
     * of the {@link FieldExportPart}.
     *
     * @param issue to get the representation for
     * @param context which contains information such as the i18nHelper and field
     * @return the exportable parts of this issue
     */
    FieldExportParts getRepresentationFromIssue(Issue issue, CustomFieldExportContext context);
}
