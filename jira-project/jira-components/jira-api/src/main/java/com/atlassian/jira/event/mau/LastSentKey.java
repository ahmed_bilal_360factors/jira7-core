package com.atlassian.jira.event.mau;

import java.util.Objects;

/**
 * Public class that provides an object which contains the email address, userID and application key for the
 * current MAU request.
 *
 * @since v7.1.0
 */
public class LastSentKey {
    private final String email;
    private final long userId;
    private final MauApplicationKey applicationKey;

    public LastSentKey(final String email, final long userId, final MauApplicationKey applicationKey) {
        this.email = email;
        this.userId = userId;
        this.applicationKey = applicationKey;

    }

    public String getEmail() {
        return email;
    }

    public long getUserId() {
        return userId;
    }

    public MauApplicationKey getApplicationKey() {
        return applicationKey;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final LastSentKey that = (LastSentKey) o;
        return Objects.equals(email, that.email) &&
                Objects.equals(applicationKey, that.applicationKey);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, applicationKey);
    }
}