package com.atlassian.jira.issue.fields.layout.column;

import com.atlassian.annotations.PublicApi;

@PublicApi
public interface EditableDefaultColumnLayout extends EditableColumnLayout {
}
