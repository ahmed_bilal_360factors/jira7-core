package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.annotations.Internal;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;

import java.util.Map;

/**
 * Checks if this user has the global admin permission.  This class does not rely on passed context as {@link
 * com.atlassian.jira.plugin.webfragment.conditions.UserIsAdminCondition} does, but retrieves logged user from {@link
 * com.atlassian.jira.security.JiraAuthenticationContext}; This is temporary workaround to make this condition working
 * with dashboard-item, as currently dashboard-plugin does not pass correct context parameters. This class will be
 * removed once https://ecosystem.atlassian.net/browse/AG-1466 is resolved.
 */
@Internal
public class IsAdminCondition implements Condition {

    private final JiraAuthenticationContext authenticationContext;
    private final GlobalPermissionManager permissionManager;

    public IsAdminCondition(final JiraAuthenticationContext authenticationContext, final GlobalPermissionManager permissionManager) {
        this.authenticationContext = authenticationContext;
        this.permissionManager = permissionManager;
    }

    @Override
    public void init(final Map<String, String> params) throws PluginParseException {
    }

    @Override
    public boolean shouldDisplay(final Map<String, Object> context) {
        final ApplicationUser user = authenticationContext.getLoggedInUser();
        return user != null && permissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user);
    }
}
