package com.atlassian.jira.event.worklog;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.issue.worklog.Worklog;

/**
 * Event indicating that worklog has been deleted.
 *
 * @since v7.0
 */
@PublicApi
public class WorklogDeletedEvent extends AbstractWorklogEvent implements WorklogEvent {
    public WorklogDeletedEvent(final Worklog worklog) {
        super(worklog);
    }
}
