package com.atlassian.jira.event.issuetype.property;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.entity.property.EntityProperty;
import com.atlassian.jira.event.entity.AbstractPropertyEvent;
import com.atlassian.jira.event.entity.EntityPropertyDeletedEvent;
import com.atlassian.jira.user.ApplicationUser;

/**
 * Event indicating an issue type property has been deleted.
 *
 * @since 7.0
 */
@PublicApi
@EventName("property.deleted.issuetype")
public class IssueTypePropertyDeletedEvent extends AbstractPropertyEvent implements EntityPropertyDeletedEvent {
    public IssueTypePropertyDeletedEvent(final EntityProperty entityProperty, final ApplicationUser user) {
        super(entityProperty, user);
    }
}
