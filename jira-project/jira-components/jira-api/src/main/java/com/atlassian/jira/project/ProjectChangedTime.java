package com.atlassian.jira.project;

import com.atlassian.annotations.ExperimentalApi;

import javax.annotation.Nonnull;
import java.sql.Timestamp;

/**
 * Represents the last time any issue was changed in a given Project.
 *
 * @since v7.2
 */
@ExperimentalApi
public interface ProjectChangedTime {
    long getProjectId();

    /**
     * @return Last time that any issue in the given project was created, updated, assigned, transitioned, deleted, or moved.
     */
    @Nonnull
    Timestamp getIssueChangedTime();
}
