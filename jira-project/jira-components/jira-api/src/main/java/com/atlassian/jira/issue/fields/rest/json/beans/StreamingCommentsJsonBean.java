package com.atlassian.jira.issue.fields.rest.json.beans;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.rest.json.serializer.StreamingCommentsJsonBeanSerializer;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * @since 7.2
 */
@JsonSerialize(using = StreamingCommentsJsonBeanSerializer.class)
public class StreamingCommentsJsonBean {
    private final Issue issue;
    private final boolean isRendered;
    private final String rendererType;

    public StreamingCommentsJsonBean(Issue issue, boolean isRendered, String rendererType) {
        this.issue = issue;
        this.isRendered = isRendered;
        this.rendererType = rendererType;
    }

    public Issue getIssue() {
        return issue;
    }

    public boolean isRendered() {
        return isRendered;
    }

    public String getRendererType() {
        return rendererType;
    }
}
