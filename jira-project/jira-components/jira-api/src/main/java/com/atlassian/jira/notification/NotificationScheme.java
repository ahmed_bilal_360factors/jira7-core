package com.atlassian.jira.notification;

import com.atlassian.annotations.PublicApi;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * Information about the notification scheme.
 */
@PublicApi
public final class NotificationScheme {
    private final Long id;
    private final String name;
    private final String description;
    private final List<EventNotifications> eventNotifications;

    public NotificationScheme(final Long id,
                              final String name,
                              final String description,
                              final Iterable<EventNotifications> notificationEvent) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.eventNotifications = ImmutableList.copyOf(notificationEvent);
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<EventNotifications> getEventNotifications() {
        return eventNotifications;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final NotificationScheme that = (NotificationScheme) o;

        if (description != null ? !description.equals(that.description) : that.description != null) {
            return false;
        }
        if (id != null ? !id.equals(that.id) : that.id != null) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        if (eventNotifications != null ? !eventNotifications.equals(that.eventNotifications) : that.eventNotifications != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (eventNotifications != null ? eventNotifications.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "NotificationScheme{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", eventNotifications=" + eventNotifications +
                '}';
    }
}
