package com.atlassian.jira.issue.export.customfield;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.export.FieldExportParts;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.util.I18nHelper;

/**
 * Context used when generating the {@link FieldExportParts} for
 * {@link ExportableCustomFieldType#getRepresentationFromIssue(Issue, CustomFieldExportContext)}. This
 * provides items useful for generating the {@link FieldExportParts} such as the  {@link CustomField}.
 *
 * @since 7.2.0
 */
@ExperimentalApi
public interface CustomFieldExportContext {
    /**
     * The field for this {@link CustomFieldType}, can be used for generating the value for the issue.
     * @return custom field implementing this type.
     */
    CustomField getCustomField();

    /**
     * @return the i18nHelper that can be used for generating text for the column header if need be.
     */
    I18nHelper getI18nHelper();

    /**
     * @return the default column header for the custom field, can be used if a custom header is not needed.
     */
    String getDefaultColumnHeader();
}
