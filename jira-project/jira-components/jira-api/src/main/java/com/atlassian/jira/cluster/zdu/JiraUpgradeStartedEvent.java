package com.atlassian.jira.cluster.zdu;

import com.atlassian.annotations.ExperimentalApi;

/**
 * This event is fired when the cluster enters {@link UpgradeState#READY_TO_UPGRADE}.
 *
 * @since v7.3
 * @see UpgradeState
 */
@ExperimentalApi
public class JiraUpgradeStartedEvent {
    private final NodeBuildInfo nodeBuildInfo;

    public JiraUpgradeStartedEvent(final NodeBuildInfo nodeBuildInfo) {
        this.nodeBuildInfo = nodeBuildInfo;
    }

    public NodeBuildInfo getNodeBuildInfo() {
        return nodeBuildInfo;
    }
}
