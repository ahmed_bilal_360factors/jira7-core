package com.atlassian.jira.issue.comments;

import com.atlassian.annotations.PublicApi;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.entity.WithId;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.user.ApplicationUser;

import java.util.Date;

/**
 * Represents a Comment on a JIRA {@link Issue}
 */
@PublicApi
public interface Comment extends WithId {

    /**
     * Returns the key for the user that created the comment
     *
     * @return the key for the user that created the comment
     * @deprecated Use {@link #getAuthorKey()} or {@link #getAuthorApplicationUser()} instead. Since v6.0.
     */
    public String getAuthor();

    /**
     * Returns the key for the user that created the comment
     *
     * @return the key for the user that created the comment
     * @deprecated Use {@link #getAuthorKey()} or {@link #getAuthorApplicationUser()} instead. Since v6.0.
     */
    public String getAuthorKey();

    /**
     * Returns the {@link User} that created the comment
     *
     * @return the {@link User} that created the comment.
     * @deprecated Use {@link #getAuthorApplicationUser()} instead. Since v6.0.
     */
    public ApplicationUser getAuthorUser();

    /**
     * Returns the {@link ApplicationUser user} that created the comment
     *
     * @return the {@link ApplicationUser user} that created the comment
     */
    public ApplicationUser getAuthorApplicationUser();

    public String getAuthorFullName();

    public String getBody();

    public Date getCreated();

    /**
     * Returns the name of the group to which comment visibility will be restricted
     *
     * @return the name of the group to which comment visibility will be restricted.
     */
    public String getGroupLevel();

    public Long getId();

    public Long getRoleLevelId();

    public ProjectRole getRoleLevel();

    public Issue getIssue();

    /**
     * @return userkey of the update author
     * @deprecated Use {@link #getUpdateAuthorApplicationUser()} instead. Since v6.0.
     */
    public String getUpdateAuthor();

    /**
     * @return a {@link User} object
     * @deprecated Use {@link #getUpdateAuthorApplicationUser()} instead. Since v6.0.
     * <p>
     * Get the user that performed the update
     */
    public ApplicationUser getUpdateAuthorUser();

    /**
     * Get the user that performed the update
     *
     * @return an {@link ApplicationUser object}
     */
    public ApplicationUser getUpdateAuthorApplicationUser();

    public String getUpdateAuthorFullName();

    public Date getUpdated();

}
