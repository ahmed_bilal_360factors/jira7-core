package com.atlassian.jira.event.comment;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.issue.comments.Comment;

/**
 * Event published when a new comment is created.
 */
@PublicApi
public final class CommentCreatedEvent extends AbstractCommentEvent implements CommentEvent {

    public CommentCreatedEvent(Comment comment) {
        super(comment);
    }

}
