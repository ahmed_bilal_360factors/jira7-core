package com.atlassian.jira.project.template.hook;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.issue.fields.screen.FieldScreenScheme;
import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenScheme;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.resolution.Resolution;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.workflow.JiraWorkflow;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import static java.util.Collections.emptyMap;
import static java.util.Collections.unmodifiableMap;

/**
 * Request object for the "configure project" hook.
 *
 * @since 7.0
 */
@PublicApi
public class ConfigureData {
    /**
     * Creates a new ConfigureData.
     *
     * @param project               a Project
     * @param workflowScheme        The workflow scheme for the new project
     * @param createdWorkflows      a Map of created workflows and schemes
     * @param issueTypeScheme       The issue type scheme for the new project
     * @param createdIssueTypes     The issue types created for the new project
     * @param issueTypeScreenScheme The issue type screen scheme for the new project
     * @param screenSchemes         The screen scheme for the new project
     * @param screens               The screens for the new project
     * @param createdResolutions    The resolutions created for the new project
     * @return a new ConfigureData
     */
    public static ConfigureData create(Project project, Scheme workflowScheme, Map<String, JiraWorkflow> createdWorkflows,
                                       FieldConfigScheme issueTypeScheme, Map<String, IssueType> createdIssueTypes,
                                       IssueTypeScreenScheme issueTypeScreenScheme, Map<String, FieldScreenScheme> screenSchemes,
                                       Map<String, FieldScreen> screens, Collection<Resolution> createdResolutions) {
        return new ConfigureData(project, workflowScheme, createdWorkflows, issueTypeScheme, createdIssueTypes,
                issueTypeScreenScheme, screenSchemes, screens, createdResolutions);
    }

    public static ConfigureData create(Project project, Scheme workflowScheme, Map<String, JiraWorkflow> createdWorkflows,
                                       FieldConfigScheme issueTypeScheme, Map<String, IssueType> createdIssueTypes) {
        return new ConfigureData(project, workflowScheme, createdWorkflows, issueTypeScheme, createdIssueTypes,
                null, Collections.<String, FieldScreenScheme>emptyMap(), Collections.<String, FieldScreen>emptyMap(),
                new ArrayList<>());
    }

    private final Project project;
    private final Scheme workflowScheme;
    private final Map<String, JiraWorkflow> createdWorkflows;
    private final FieldConfigScheme issueTypeScheme;
    private final Map<String, IssueType> createdIssueTypes;
    private final IssueTypeScreenScheme issueTypeScreenScheme;
    private final Map<String, FieldScreenScheme> screenSchemes;
    private final Map<String, FieldScreen> screens;
    private final Collection<Resolution> createdResolutions;

    private ConfigureData(Project project, Scheme workflowScheme, Map<String, JiraWorkflow> createdWorkflows,
                          FieldConfigScheme issueTypeScheme, Map<String, IssueType> createdIssueTypes,
                          IssueTypeScreenScheme issueTypeScreenScheme, Map<String, FieldScreenScheme> screenSchemes,
                          Map<String, FieldScreen> screens, Collection<Resolution> createdResolutions) {
        this.project = project;
        this.workflowScheme = workflowScheme;
        this.createdWorkflows = toImmutable(createdWorkflows);
        this.issueTypeScheme = issueTypeScheme;
        this.createdIssueTypes = toImmutable(createdIssueTypes);
        this.issueTypeScreenScheme = issueTypeScreenScheme;
        this.screenSchemes = toImmutable(screenSchemes);
        this.screens = toImmutable(screens);
        this.createdResolutions = createdResolutions;
    }

    private static <T> Map<String, T> toImmutable(Map<String, T> map) {
        if (map == null) {
            return emptyMap();
        }
        return unmodifiableMap(map);
    }

    /**
     * Returns the Project that was created.
     *
     * @return created Project
     */
    public Project project() {
        return project;
    }

    /**
     * Returns the workflow scheme that was created.
     *
     * @return created Workflow Scheme
     */
    public Scheme workflowScheme() {
        return workflowScheme;
    }

    /**
     * Returns the JiraWorkflow instances that have been created for this project, mapped to their template key.
     *
     * @return mapping of template workflow keys and the created workflow
     */
    public Map<String, JiraWorkflow> createdWorkflows() {
        return createdWorkflows;
    }

    /**
     * Returns the issue type scheme that was created.
     *
     * @return created Issue Type Scheme
     */
    public FieldConfigScheme issueTypeScheme() {
        return issueTypeScheme;
    }

    /**
     * Returns the IssueType instances that have been created for this project, mapped to their template key.
     *
     * @return mapping of template issue type keys and the created issue type
     */
    public Map<String, IssueType> createdIssueTypes() {
        return createdIssueTypes;
    }

    /**
     * Returns the issue type screen scheme that was created.
     *
     * @return created Issue Type Screen Scheme
     */
    public IssueTypeScreenScheme issueTypeScreenScheme() {
        return issueTypeScreenScheme;
    }

    /**
     * Returns the screen scheme instances that have been created for this project, mapped to their template key.
     *
     * @return mapping of template screen scheme keys and the created screen schemes
     */
    public Map<String, FieldScreenScheme> createdScreenSchemes() {
        return screenSchemes;
    }

    /**
     * Returns the screen instances that have been created for this project, mapped to their template key.
     *
     * @return mapping of template screen keys and the created screens
     */
    public Map<String, FieldScreen> createdScreens() {
        return screens;
    }

    /**
     * Returns the collection of resolutions that have been created
     *
     * @return collection of created resolutions
     */
    public Collection<Resolution> createdResolutions() {
        return createdResolutions;
    }
}
