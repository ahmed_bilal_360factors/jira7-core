package com.atlassian.jira.issue.customfields;

import com.atlassian.annotations.PublicApi;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

/**
 * Configures a clone option for a given custom field in the context of an issue. A clone option configuration
 * controls whether an option is displayed on the clone dialog, the label displayed when the option is displayed, and
 * the option's initial state, i.e. whether it's selected by default.
 *
 * @since 7.2
 */
@PublicApi
public final class CloneOptionConfiguration {
    /**
     * An instance of CloneOptionConfiguration that indicates the clone option should not be displayed on the clone dialog.
     */
    public static final CloneOptionConfiguration DO_NOT_DISPLAY = new CloneOptionConfiguration(false, "", false);

    private final boolean shouldDisplayOption;
    private final String optionLabel;
    private final boolean optionSelectedByDefault;

    private CloneOptionConfiguration(boolean shouldDisplayOption, String optionLabel, boolean optionSelectedByDefault) {
        this.shouldDisplayOption = shouldDisplayOption;
        this.optionLabel = optionLabel;
        this.optionSelectedByDefault = optionSelectedByDefault;
    }

    /**
     * @return A boolean that indicates whether this option should be displayed on the clone dialog.
     */
    public boolean shouldDisplayOption() {
        return shouldDisplayOption;
    }

    /**
     * @return Label to be displayed for this clone option.
     */
    @Nonnull
    public String getOptionLabel() {
        return optionLabel;
    }

    /**
     * @return A boolean that indicates whether this option is selected by default.
     */
    public boolean isOptionSelectedByDefault() {
        return optionSelectedByDefault;
    }

    /**
     * Creates an instance of CloneOptionConfiguration to represent a clone option that gets displayed in the clone dialog
     * with the specified label. This clone option will not be selected by default.
     *
     * @param optionLabel Label of clone option
     * @return An instance of CloneOptionConfiguration to represent a clone option that gets displayed in the clone dialog
     * with the specified label.
     */
    @Nonnull
    public static CloneOptionConfiguration withOptionLabel(String optionLabel) {
        checkArgument(StringUtils.isNotBlank(optionLabel), "Option label needs to be non-null and non-empty when option is being displayed.");
        return new CloneOptionConfiguration(true, optionLabel, false);
    }

    /**
     * Creates an instance of CloneOptionConfiguration to represent a clone option that gets displayed in the clone dialog
     * with the same label as this option's label and is selected by default.
     *
     * @return An instance of CloneOptionConfiguration to represent a clone option that gets displayed in the clone dialog
     * with the same label as this option's label and is selected by default.
     */
    @Nonnull
    public CloneOptionConfiguration selectOptionByDefault() {
        checkState(shouldDisplayOption(), "Shouldn't be setting option's initial state on configuration for option that is not displayed.");
        return new CloneOptionConfiguration(true, optionLabel, true);
    }
}
