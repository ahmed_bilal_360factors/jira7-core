package com.atlassian.jira.project.template.hook;

import com.atlassian.annotations.PublicApi;

/**
 * An 'add project' plugin module as defined in the {@code atlassian-plugin.xml}
 *
 * @since 7.0
 */
@PublicApi
public interface AddProjectModule {
    /**
     * Indicates whether a Project Template Config is defined.
     *
     * @return whether an Project Template Config is defined
     */
    boolean hasConfigTemplate();

    /**
     * Return the Configuration Template
     *
     * @return the configuration template
     */
    ConfigTemplate configTemplate();

    /**
     * Indicates whether an AddProjectHook has been defined.
     *
     * @return whether an AddProjectHook has been defined.
     */
    boolean hasProjectCreateHook();

    /**
     * Creates a new instance of the configured AddProjectHook.
     *
     * @return a new instance of the AddProjectHook configured for this ProjectTemplateModule
     */
    AddProjectHook addProjectHook();
}
