package com.atlassian.jira.avatar;

import com.atlassian.annotations.Internal;
import com.atlassian.jira.io.ResourceData;

import java.io.IOException;
import java.util.function.Function;

import static com.atlassian.jira.avatar.AvatarManager.PNG_CONTENT_TYPE;
import static com.atlassian.jira.avatar.AvatarManager.SVG_CONTENT_TYPE;

/**
 * Chooses the PNG format if possible. Chooses a fallback otherwise.
 *
 * @since v7.0.1
 */
@Internal
class PngAvatarFormatPolicy extends AvatarFormatPolicy {
    private final Function<ResourceData, ResourceData> fallback;

    PngAvatarFormatPolicy(final Function<ResourceData, ResourceData> fallback) {
        this.fallback = fallback;
    }

    @Override
    ResourceData getData(
            final Avatar avatar,
            final AvatarFormatPolicy.InputStreamSupplier originalData,
            final AvatarFormatPolicy.InputStreamSupplier transcodedData
    ) throws IOException {
        final String originalType = avatar.getContentType();

        if (PNG_CONTENT_TYPE.equals(originalType)) {
            return new ResourceData(originalData.get(), originalType);
        } else if (canTranscode(avatar)) {
            return new ResourceData(transcodedData.get(), PNG_CONTENT_TYPE);
        } else {
            return fallback.apply(new ResourceData(originalData.get(), originalType));
        }
    }

    private boolean canTranscode(final Avatar avatar) {
        final String type = avatar.getContentType();
        return SVG_CONTENT_TYPE.equals(type) && avatar.isSystemAvatar();
    }
}