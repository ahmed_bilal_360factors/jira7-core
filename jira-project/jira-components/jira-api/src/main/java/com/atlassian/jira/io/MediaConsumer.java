package com.atlassian.jira.io;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.jira.util.Consumer;

import javax.annotation.Nonnull;
import java.io.InputStream;
import java.util.Optional;

/**
 * Consumes a media stream and its type.
 * <p>
 * The media type has to be represented by {@link String} instead of
 * <a href="https://docs.oracle.com/javaee/6/api/javax/ws/rs/core/MediaType.html"><tt>MediaType</tt></a> because of JIRA
 * OSGi limitations. The <tt>jira-api</tt> module cannot import classes from OSGi. Providing that class would cause
 * {@link ClassCastException}s for clients importing it from OSGi.
 *
 * @since v7.0.1
 */
@ExperimentalApi
public class MediaConsumer {
    private final Optional<Consumer<String>> contentTypeConsumer;
    private final Consumer<InputStream> dataConsumer;

    public MediaConsumer(@Nonnull final Consumer<String> contentTypeConsumer, @Nonnull final Consumer<InputStream> dataConsumer) {
        this.contentTypeConsumer = Optional.of(contentTypeConsumer);
        this.dataConsumer = dataConsumer;
    }

    public MediaConsumer(final Consumer<InputStream> dataConsumer) {
        this.contentTypeConsumer = Optional.empty();
        this.dataConsumer = dataConsumer;
    }

    public void consumeData(final InputStream inputStream) {
        dataConsumer.consume(inputStream);
    }

    public void consumeContentType(final String contentType) {
        contentTypeConsumer
                .orElse(this::noOpConsume)
                .consume(contentType);
    }

    private void noOpConsume(final String contentType) {
    }
}
