<%@ taglib prefix="ww" uri="webwork" %>
<%@ taglib prefix="ui" uri="webwork" %>
<%@ taglib prefix="aui" uri="webwork" %>
<%@ taglib prefix="page" uri="sitemesh-page" %>
<%@ page import="com.atlassian.jira.component.ComponentAccessor" %>
<%@ page import="com.atlassian.jira.web.action.util.FieldsResourceIncluder" %>
<%@ page import="com.atlassian.plugin.webresource.WebResourceManager" %>
<html>
<head>
    <ww:if test="/ableToCreateIssueInSelectedProject == true">
        <title><ww:text name="'createissue.title'"/></title>
        <content tag="section">find_link</content>
        <%
            final FieldsResourceIncluder fieldResourceIncluder = ComponentAccessor.getComponent(FieldsResourceIncluder.class);
            fieldResourceIncluder.includeFieldResourcesForCurrentUser();

            final WebResourceManager wrm = ComponentAccessor.getWebResourceManager();
            wrm.requireResourcesForContext("jira.create.issue");
        %>
    </ww:if>
    <ww:else>
        <title><ww:text name="'common.words.error'"/></title>
        <meta name="decorator" content="message" />
    </ww:else>
</head>
<body class="aui-page-focused aui-page-focused-large">
<ww:if test="/ableToCreateIssueInSelectedProject == true">
    <ui:soy moduleKey="'jira.webresources:soy-templates'" template="'JIRA.Templates.Headers.pageHeader'">
        <ui:param name="'mainContent'">
            <h1><ww:text name="'createissue.title'"/></h1>
        </ui:param>
    </ui:soy>
    <ui:soy moduleKey="'com.atlassian.auiplugin:aui-experimental-soy-templates'" template="'aui.page.pagePanel'">
        <ui:param name="'content'">
            <ui:soy moduleKey="'com.atlassian.auiplugin:aui-experimental-soy-templates'" template="'aui.page.pagePanelContent'">
                <ui:param name="'content'">

            <page:applyDecorator id="issue-create" name="auiform">
                <page:param name="action">CreateIssueDetails.jspa</page:param>
                <page:param name="submitButtonName">Create</page:param>
                <page:param name="submitButtonText"><ww:property value="submitButtonName" escape="false" /></page:param>
                <page:param name="cancelLinkURI"><ww:url value="'default.jsp'" atltoken="false"/></page:param>
                <page:param name="isMultipart">true</page:param>

                <aui:component name="'pid'" template="hidden.jsp" theme="'aui'" />
                <aui:component name="'issuetype'" template="hidden.jsp" theme="'aui'" />
                <aui:component name="'formToken'" template="hidden.jsp" theme="'aui'" />

                <page:applyDecorator name="auifieldgroup">
                    <aui:component id="'project-name'" label="text('issue.field.project')" name="'project/name'" template="formFieldValue.jsp" theme="'aui'" />
                </page:applyDecorator>

                <page:applyDecorator name="auifieldgroup">
                    <aui:component id="'issue-type'" label="text('issue.field.issuetype')" name="'issueType'" template="formIssueType.jsp" theme="'aui'">
                        <aui:param name="'issueType'" value="/constantsManager/issueType(issuetype)" />
                    </aui:component>
                </page:applyDecorator>

                <ww:component template="issuefields.jsp" name="'createissue'">
                    <ww:param name="'displayParams'" value="/displayParams"/>
                    <ww:param name="'issue'" value="/issueObject"/>
                    <ww:param name="'tabs'" value="/fieldScreenRenderTabs"/>
                    <ww:param name="'errortabs'" value="/tabsWithErrors"/>
                    <ww:param name="'selectedtab'" value="/selectedTab"/>
                    <ww:param name="'ignorefields'" value="/ignoreFieldIds"/>
                    <ww:param name="'create'" value="'true'"/>
                </ww:component>

                <jsp:include page="/includes/panels/updateissue_comment.jsp" />

            </page:applyDecorator>

                </ui:param>
            </ui:soy>
        </ui:param>
    </ui:soy>
</ww:if>
<ww:else>
    <div class="form-body">
        <header>
            <h1><ww:text name="'common.words.error'"/></h1>
        </header>
        <%@ include file="/includes/createissue-notloggedin.jsp" %>
    </div>
</ww:else>
<script type="text/javascript">
AJS.$(document).ready(function () {
	taskTypeParam = "<%=request.getParameter("taskType")%>";
	cm_surveyTaskType = "<%=request.getParameter("predict_survey_task_type")%>";
	
	if(typeof cm_surveyTaskType !="undefined" && cm_surveyTaskType !="null"){
		taskTypeParam = cm_surveyTaskType;
		cm_surveyUserName = "<%=request.getParameter("predict_survey_user_name")%>";
		cm_controlTestId = "<%=request.getParameter("predict_control_test_id")%>";
		cm_controlTestName = "<%=request.getParameter("predict_control_test_name")%>";
		cm_controlOwner = "<%=request.getParameter("predict_control_owner")%>";
		cm_controlTester = "<%=request.getParameter("predict_control_tester")%>";
		cm_controlOwnerMessage = "<%=request.getParameter("predict_control_owner_message")%>";
		cm_controlTesterMessage = "<%=request.getParameter("predict_control_tester_message")%>";
	}
	if(taskTypeParam != "null"){
		if(taskTypeParam == 'controlTestType'){
			AJS.$(".aui-page-header-main").find("h1").html("Create Control Test");
			AJS.$("#"+CF_SUB_TYPE_ID).val(CF_SUB_TYPE_OPT_CONTROL_TEST); // Sub Type
			AJS.$("#"+SITE_FIELD_ID).parent().hide(); //Site field
			AJS.$("#"+CF_ORGANIZATION_UNIT_SELECTOR_ID).parent().hide(); //Organization Unit Field
			AJS.$("#single_assignee").parent().parent().hide(); // Assignee type
			AJS.$("#"+CF_GROUP_ASSIGNEE_ID).parent().hide(); // group field
			AJS.$("#"+CF_TEMPORARY_ASSIGNEE_ID).val(CF_TEMPORARY_ASSIGNEE_OPT_NONE); // Temp Assignee
			AJS.$("#"+CF_CONTROL_OWNER_ID+"_container").find("a").hide();
			AJS.$("#"+CF_CONTROL_TESTER_ID+"_container").find("a").hide();
			AJS.$("#"+CF_ASSESSMENT_TEMPLATE_ID).parent().hide(); //Assessment Template
			AJS.$("#"+CF_SUBJECT_AREA_CATEGORY).parent().hide(); // Subject Area
			
		}else if(taskTypeParam == 'assessment'){
			AJS.$(".aui-page-header-main").find("h1").html("Create Assessment");
			AJS.$("#"+CF_CONTROL_OWNER_ID).parent().parent().hide(); // Control Owner Field
			AJS.$("#"+CF_CONTROL_TESTER_ID).parent().parent().hide();
			//For Site multi select selector
			if(AJS.$("#viewpanel_" + SITE_FIELD_ID).length){
				AJS.$("#viewpanel_" + SITE_FIELD_ID).hide();
			}
			AJS.$("#"+SITE_FIELD_ID).parent().hide(); //Site field
			AJS.$("#"+CF_ORGANIZATION_UNIT_SELECTOR_ID).parent().hide(); //Organization Unit Field
			if(AJS.$("#viewpanel_" + CF_ORGANIZATION_UNIT_SELECTOR_ID).length){
				AJS.$("#viewpanel_" + CF_ORGANIZATION_UNIT_SELECTOR_ID).hide();
			}
			AJS.$("#"+CF_SUB_TYPE_ID).val(CF_SUB_TYPE_OPT_ASSESSMENT); // Sub Type
			AJS.$("#"+CF_CONTROL_TEST_TEMPLATE_ID).parent().hide(); //Control Test Template
		}else if(taskTypeParam == 'taskType'){
			AJS.$(".aui-page-header-main").find("h1").html("Create " + AJS.$("#issue-create-issue-type").html());
			AJS.$("#"+CF_SUB_TYPE_ID).val(CF_SUB_TYPE_OPT_NONE); // Sub Type
			AJS.$("#"+CF_CONTROL_OWNER_ID).parent().parent().hide(); // Control Owner Field
			AJS.$("#"+CF_CONTROL_TESTER_ID).parent().parent().hide();
			AJS.$("#"+CF_ASSESSMENT_TEMPLATE_ID).parent().hide(); //Assessment Template
			AJS.$("#"+CF_CONTROL_TEST_TEMPLATE_ID).parent().hide(); //Control Test Template
			AJS.$("#"+CF_SUBJECT_AREA_CATEGORY).parent().hide(); // Subject Area
		}
	} else {
		if(AJS.$("#"+CF_SUB_TYPE_ID).val() == CF_SUB_TYPE_OPT_CONTROL_TEST){
			AJS.$(".aui-page-header-main").find("h1").html("Create Control Test");
			AJS.$("#"+SITE_FIELD_ID).parent().hide(); //Site field
			AJS.$("#"+CF_ORGANIZATION_UNIT_SELECTOR_ID).parent().hide(); //Organization Unit Field
			AJS.$("#single_assignee").parent().parent().hide(); // Assignee type
			AJS.$("#"+CF_GROUP_ASSIGNEE_ID).parent().hide(); // group field
			AJS.$("#"+CF_TEMPORARY_ASSIGNEE_ID).val(CF_TEMPORARY_ASSIGNEE_OPT_NONE); // Temp Assignee
			AJS.$("#"+CF_CONTROL_OWNER_ID+"_container").find("a").hide();
			AJS.$("#"+CF_CONTROL_TESTER_ID+"_container").find("a").hide();
			AJS.$("#"+CF_ASSESSMENT_TEMPLATE_ID).parent().hide(); //Assessment Template
			AJS.$("#"+CF_SUBJECT_AREA_CATEGORY).parent().hide(); // Subject Area
		}else if(AJS.$("#"+CF_SUB_TYPE_ID).val() == CF_SUB_TYPE_OPT_ASSESSMENT){
			AJS.$(".aui-page-header-main").find("h1").html("Create Assessment");
			AJS.$("#"+CF_CONTROL_OWNER_ID).parent().parent().hide(); // Control Owner Field
			AJS.$("#"+CF_CONTROL_TESTER_ID).parent().parent().hide();
			//For Site multi select selector
			if(AJS.$("#viewpanel_" + SITE_FIELD_ID).length){
				AJS.$("#viewpanel_" + SITE_FIELD_ID).hide();
			}
			AJS.$("#"+SITE_FIELD_ID).parent().hide(); //Site field
			AJS.$("#"+CF_ORGANIZATION_UNIT_SELECTOR_ID).parent().hide(); //Organization Unit Field
			if(AJS.$("#viewpanel_" + CF_ORGANIZATION_UNIT_SELECTOR_ID).length){
				AJS.$("#viewpanel_" + CF_ORGANIZATION_UNIT_SELECTOR_ID).hide();
			}
			AJS.$("#"+CF_CONTROL_TEST_TEMPLATE_ID).parent().hide(); //Control Test Template
		}else if(AJS.$("#"+CF_SUB_TYPE_ID).val() == CF_SUB_TYPE_OPT_NONE || AJS.$("#"+CF_SUB_TYPE_ID).val() == '-1'){
			AJS.$(".aui-page-header-main").find("h1").html("Create " + AJS.$("#issue-create-issue-type").html());
			AJS.$("#"+CF_CONTROL_OWNER_ID).parent().parent().hide(); // Control Owner Field
			AJS.$("#"+CF_CONTROL_TESTER_ID).parent().parent().hide();
			AJS.$("#"+CF_ASSESSMENT_TEMPLATE_ID).parent().hide(); //Assessment Template
			AJS.$("#"+CF_CONTROL_TEST_TEMPLATE_ID).parent().hide(); //Control Test Template
			AJS.$("#"+CF_SUBJECT_AREA_CATEGORY).parent().hide(); // Subject Area
		}
	}
	AJS.$("#"+CF_SUB_TYPE_ID).parent().attr("id", CF_SUB_TYPE_ID+"_div");
	AJS.$("#"+CF_TEMPORARY_ASSIGNEE_ID).parent().attr("id", CF_TEMPORARY_ASSIGNEE_ID+"_div");
	if(typeof cm_surveyTaskType !="undefined" && cm_surveyTaskType !="null"){
		taskTypeParam = 'controlTestType';
		if(typeof cm_controlTestId !="undefined" && cm_controlTestId !="null"){
			AJS.$("#"+CF_CONTROL_TEST_TEMPLATE_ID).find('datalist').find('aui-option').attr("label",cm_controlTestName);
			AJS.$("#"+CF_CONTROL_TEST_TEMPLATE_ID).find('datalist').find('aui-option').text(cm_controlTestName);
			AJS.$("#"+CF_CONTROL_TEST_TEMPLATE_ID).find("#"+CF_CONTROL_TEST_TEMPLATE_ID+"-input").val(cm_controlTestName);
			AJS.$("#"+CF_CONTROL_TEST_TEMPLATE_ID).find('select').find('option:selected').text(cm_controlTestName);
			AJS.$("#"+CF_CONTROL_TEST_TEMPLATE_ID).find('select').find('option:selected').val(cm_controlTestId);
			AJS.$("#"+CF_CONTROL_TEST_TEMPLATE_ID).find('input').attr('disabled', true);
			AJS.$("#"+CF_CONTROL_TEST_TEMPLATE_ID).find('button').attr('disabled', true);
			AJS.$("#"+CF_CONTROL_TEST_TEMPLATE_ID).find("#"+CF_CONTROL_TEST_TEMPLATE_ID+"-input").attr('aria-controls', "");
		}
		if(typeof cm_controlTestName !="undefined" && cm_controlTestName !="null"){
			AJS.$("#summary").val(cm_controlTestName);
			AJS.$("#summary").attr('readonly', "readonly");
		}
		if(typeof cm_surveyUserName !="undefined" && cm_surveyUserName !="null"){
			AJS.$("#assignee").trigger('set-selection-value', cm_surveyUserName);
		}
		if(typeof cm_controlOwner !="undefined" && cm_controlOwner !="null"){
			AJS.$("#"+CF_CONTROL_OWNER_ID).val(cm_controlOwner);
		}
		if(typeof cm_controlTester !="undefined" && cm_controlTester !="null"){
			AJS.$("#"+CF_CONTROL_TESTER_ID).val(cm_controlTester);
		}
		if(typeof cm_controlOwnerMessage !="undefined" && cm_controlOwnerMessage !="null" && cm_controlOwnerMessage !=""){
			AJS.$("#"+CF_CONTROL_OWNER_ID+"_container").after('<div id="controlOwnerWarning" class="aui-message aui-message-warning closeable shadowed"> <p class="title"><strong> '+cm_controlOwnerMessage+' </strong></p></div>');
		}	
		if(typeof cm_controlTesterMessage !="undefined" && cm_controlTesterMessage !="null" && cm_controlTesterMessage !=""){
			AJS.$("#"+CF_CONTROL_TESTER_ID+"_container").after('<div id="controlTesterWarning" class="aui-message aui-message-warning closeable shadowed"> <p class="title"><strong> '+cm_controlTesterMessage+' </strong></p></div>');
		}
	}
});
AJS.$(".localHelp").remove();
AJS.$("#issue-create-project-name").parent().hide();
AJS.$("#issue-create-issue-type").parent().hide();
AJS.$(".help-lnk").remove();

AJS.$('#issue-create').submit(function(ev) {
    //ev.preventDefault(); // to stop the form from submitting
    
	if (AJS.$("#customfield_12927").length>0){
		var userName = AJS.$("#customfield_12927").val()+ "@" +cm_selectedProjectKey;
        AJS.$("#customfield_12927").val(userName);
		console.log(userName);
		}

	if (AJS.$("#customfield_13208").length>0){
		var userName = AJS.$("#customfield_13208").val()+"@"+cm_selectedProjectKey;
        AJS.$("#customfield_13208").val(userName);
		console.log(userName);
	}

	if(AJS.$("#assignee").val() != null && AJS.$("#assignee").length > 0 && AJS.$("#assignee").val()[0].trim() != "" && AJS.$("#assignee").val()[0].trim() != "-1" ){
		var original_Assignee = AJS.$("#assignee").val()[0].trim()+"@"+cm_selectedProjectKey;
		AJS.$('#assignee').append($("<option></option>").attr("value",original_Assignee).text(original_Assignee)); 
		AJS.$("#assignee").val(original_Assignee).change();
	}
	
	if(AJS.$("#customfield_12804").length > 0){
		sanitizeUsers("customfield_12804");
	}
	if(AJS.$("#customfield_12805").length > 0){
		sanitizeUsers("customfield_12805");
	}
	
	if(AJS.$("#customfield_13559").length > 0){
		sanitizeUsers("customfield_13559");
	}
	
	if(AJS.$("#customfield_12808").length > 0){
		sanitizeUsers("customfield_12808");
	}
	
	if(AJS.$("#customfield_12805").length > 0){
		sanitizeUsers("customfield_12805");
	}
	
	if(AJS.$("#customfield_12106").length > 0){
		sanitizeUsers("customfield_12106");
	}
	if (AJS.$("#duedate").length && AJS.$("#duedate").val().length==0){
		if (AJS.$("#customfield_10709").length>0){
			//AJS.$("#duedate").val(AJS.$("#customfield_10709").val());
		}
	}
	
	AJS.$(".icon-default.aui-icon.aui-icon-small.aui-iconfont-admin-roles").each(function( index ) {
			if(AJS.$( this ).parent().prev().length > 0){
				var customFieldId = AJS.$( this ).parent().prev().attr("id");
				//We will remove this check in future as we can now remove individual checks on ids in the start of submit
				if(customFieldId !="assignee" && customFieldId !="customfield_12804" && customFieldId !="customfield_12805" && customFieldId !="customfield_13559" && customFieldId !="customfield_12808"	&& customFieldId !="customfield_12805" &&customFieldId !="customfield_12106"){
					sanitizeUsers(customFieldId);
				}												
			}
	});
	
	if (AJS.$("#"+CF_COMPLIANCE_LEAD).length>0){
		var userName = AJS.$("#"+CF_COMPLIANCE_LEAD).val()+ "@" +cm_selectedProjectKey;
		AJS.$("#"+CF_COMPLIANCE_LEAD).val(userName);
		console.log(userName);
	}
	
    if (AJS.$("#customfield_12801").val()!=null && AJS.$("#customfield_12801").val().length>0){
		AJS.$("#customfield_12802").val(AJS.$("#customfield_12801").val());
		
	}else if ((AJS.$("#customfield_12801").val()== null  ||  AJS.$("#customfield_12801").val()== "")  
		&& AJS.$("#customfield_12802").val()!= "" && AJS.$("#customfield_12802").val()!= null
	 ){
		AJS.$("#customfield_12802").val(cm_selectedProjectKey + "-" + AJS.$("#customfield_12802").val());
	}

    	//if this case is linked to Survey than do this to make link
    	if(cm_openCreateCaseDialog == "true")
    	{
    		if(cm_predict_createriskregistercase == "true"){
    			//Set Summary
    			if(cm_predefined_summary){
    				AJS.$("#summary").val(cm_predefined_summary);
    				AJS.$("#summary").enable(false);
    			}	
    		}else{
    			AJS.$("#customfield_10900").val(cm_surveyId);
    			AJS.$("#customfield_11400").val(cm_surveyName);
    		}
    	}
	AJS.$("#issue-create-submit").prop("disabled",true);
});

AJS.$(document).ready(function () {
	
	if(cm_selectedProjectKey == 'HLF'){
		if(taskTypeParam != "null"){
			if(taskTypeParam == 'assessment'){
	    		AJS.$(".aui-page-header-main").find("h1").html("Create Inspection");
			}
		}
	}
	
	if(AJS.$("#assignee").length > 0 && AJS.$("#assignee").val()[0].trim() != "" && AJS.$("#assignee").val()[0].trim() != "-1"){
		antiSanitizeUsers("assignee");
	}
	
	if(AJS.$("#customfield_12804").length > 0 && AJS.$("#customfield_12804").val().trim() != ""){
		antiSanitizeUsers("customfield_12804");
	}
	
	if(AJS.$("#customfield_12805").length > 0 && AJS.$("#customfield_12805").val().trim() != ""){
		antiSanitizeUsers("customfield_12805");
	}
	
	if(AJS.$("#customfield_13559").length > 0 && AJS.$("#customfield_13559").val().trim() != ""){
		antiSanitizeUsers("customfield_13559");
	}
	
	if(AJS.$("#customfield_12808").length > 0 && AJS.$("#customfield_12808").val().trim() != ""){
		antiSanitizeUsers("customfield_12808");
	}
	
	if(AJS.$("#customfield_12805").length > 0 && AJS.$("#customfield_12805").val().trim() != ""){
		antiSanitizeUsers("customfield_12805");
	}
	
	if(AJS.$("#customfield_12106").length > 0 && AJS.$("#customfield_12106").val().trim() != ""){
		antiSanitizeUsers("customfield_12106");
	}
	
	AJS.$(".icon-default.aui-icon.aui-icon-small.aui-iconfont-admin-roles").each(function( index ) {
		if(AJS.$( this ).parent().prev().length > 0){
			var customFieldId = AJS.$( this ).parent().prev().attr("id");
			if(customFieldId !="customfield_12804" && customFieldId !="customfield_12805" && customFieldId !="customfield_13559" && customFieldId !="customfield_12808"	&& customFieldId !="customfield_12805" &&customFieldId !="customfield_12106"){
				antiSanitizeUsers(customFieldId);
			}												
		}
});	

})



</script>


</body>
</html>
