package com.atlassian.jira.configurator.db;

import com.atlassian.jira.config.database.DatabaseType;
import com.atlassian.jira.configurator.config.Settings;
import com.atlassian.jira.configurator.config.ValidationException;
import com.atlassian.jira.exception.ParseException;

import java.sql.SQLException;

/**
 * This class is used for setting up a Postgres configuration in Db Config Tool triggered in the text mode.
 * It adds a new parameter to a Postgres configuration - "schema name". This class is not thread safe same as the whole db configuration tool
 * implementation including {@link com.atlassian.jira.configurator.db.DatabaseConfigConsoleImpl}
 *
 * @author pfarid
 * @see com.atlassian.jira.configurator.db.DatabaseConfigConsoleImpl
 */

public class PostgresConfigConsole implements DatabaseConfigConsole {
    private ConfigField[] postgresFields;

    private final ConfigField schemaName = new ConfigField("Schema name");

    private final DatabaseConfigConsole coreCofiguration = new DatabaseConfigConsoleImpl(DatabaseType.POSTGRES);

    @Override
    public ConfigField[] getFields() {
        if (postgresFields == null) {
            final ConfigField[] superFields = coreCofiguration.getFields();
            this.postgresFields = new ConfigField[superFields.length + 1];
            System.arraycopy(superFields, 0, this.postgresFields, 0, superFields.length);
            postgresFields[superFields.length] = schemaName;
        }

        return this.postgresFields;
    }

    @Override
    public void setSettings(final Settings settings) throws ParseException {
        coreCofiguration.setSettings(settings);

        this.schemaName.setValue(settings.getSchemaName());
    }

    @Override
    public void saveSettings(final Settings newSettings) throws ValidationException {
        coreCofiguration.saveSettings(newSettings);

        newSettings.setSchemaName(getSchemaName());
    }

    @Override
    public String getInstanceName() {
        return coreCofiguration.getInstanceName();
    }
    
    @Override
    public String getDatabaseType() {
        return coreCofiguration.getDatabaseType();
    }

    @Override
    public void testConnection() throws ClassNotFoundException, SQLException, ValidationException {
        coreCofiguration.testConnection();
    }

    @Override
    public String getUsername() {
        return coreCofiguration.getUsername();
    }

    @Override
    public String getPassword() {
        return coreCofiguration.getPassword();
    }

    @Override
    public String getUrl() throws ValidationException {
        return coreCofiguration.getUrl();
    }

    @Override
    public String getClassName() {
        return coreCofiguration.getClassName();
    }

    public String getSchemaName() {
        return schemaName.getValue();
    }
}
