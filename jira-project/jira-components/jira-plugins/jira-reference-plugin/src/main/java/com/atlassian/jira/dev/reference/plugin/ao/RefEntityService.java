package com.atlassian.jira.dev.reference.plugin.ao;

import com.atlassian.activeobjects.tx.Transactional;

import java.util.List;

/**
 * Simple interface to add RefEntity transactionally
 *
 * @since v5.0
 */
public interface RefEntityService {
    @Transactional
    RefEntity add(String description);

    @Transactional
    List<RefEntity> allEntities();

    @Transactional
    void deleteAll();

    void addButRollbackExternal(String description);

    void addInExternalTransaction(String description);

    void addTwoInExternalTransaction(String description, String description2);

    void addInTransaction(String description);
}
