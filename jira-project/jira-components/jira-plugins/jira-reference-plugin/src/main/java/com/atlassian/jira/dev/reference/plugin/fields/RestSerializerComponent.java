package com.atlassian.jira.dev.reference.plugin.fields;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.rest.json.JsonData;
import org.codehaus.jackson.annotate.JsonProperty;
import org.springframework.stereotype.Component;

@Component
public class RestSerializerComponent {
    public JsonData getJsonData(final CustomField field, final Issue issue) {
        final String value = (String) field.getValue(issue);
        return new JsonData(new JsonBeanTwo("custom_text", value));
    }

    static class JsonBeanTwo {
        @JsonProperty
        private String value;

        @JsonProperty
        private String type;

        public JsonBeanTwo(final String type, final String value) {
            this.type = type;
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public void setValue(final String value) {
            this.value = value;
        }

        public String getType() {
            return type;
        }

        public void setType(final String type) {
            this.type = type;
        }
    }
}
