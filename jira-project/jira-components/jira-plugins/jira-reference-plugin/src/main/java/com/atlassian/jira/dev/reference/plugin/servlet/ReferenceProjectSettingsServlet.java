package com.atlassian.jira.dev.reference.plugin.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ReferenceProjectSettingsServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        resp.getWriter().write(
                String.format("%sHello World: the url for this page is <div id='path'>%s</div> " + "" +
                                "and the query parameter string is <div id='query'>%s</div>%s",
                        "<html><head><meta name=\"decorator\" content=\"atl.admin\"/></head><body>",
                        req.getPathInfo(),
                        req.getQueryString(),
                        "</body></html>"
                )
        );
    }
}
