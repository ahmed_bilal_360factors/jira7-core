package com.atlassian.jira.dev.reference.plugin.imports.project;

import com.atlassian.jira.exception.ParseException;
import com.atlassian.jira.imports.project.ao.handler.PluggableOverviewAoEntityHandler;
import com.atlassian.jira.imports.project.core.BackupOverviewBuilder;
import com.atlassian.jira.imports.project.handler.AbortImportException;

import java.util.Map;

/**
 * Reference plugin to gather AO data and add it to the Backup project overview
 *
 * @since v7.0
 */
public class ReferenceAoOverview implements PluggableOverviewAoEntityHandler {
    private BackupOverviewBuilder backupOverviewBuilder;

    @Override
    public void handleEntity(final String entityName, final Map<String, Object> attributes)
            throws ParseException, AbortImportException {
        // Store the ref entity and map the id in case others need it later.
        if (entityName.equals("AO_98E482_REF_ENTITY")) {
            // Add a flag for the projects we use for testing.  This is HSP project.
            // Validation will fail if this is set for a project.
            backupOverviewBuilder.addAdditionalData("com.atlassian.jira.dev.reference-plugin", "10000", Boolean.TRUE);
        }
    }

    @Override
    public boolean handlesEntity(final String entityName) {
        return entityName.equals("AO_98E482_REF_ENTITY");
    }

    @Override
    public void startDocument() {
    }

    @Override
    public void endDocument() {
    }

    @Override
    public void endTable(final String tableName) {
    }

    @Override
    public void setBackupOverviewBuilder(final BackupOverviewBuilder backupOverviewBuilder) {
        this.backupOverviewBuilder = backupOverviewBuilder;
    }
}
