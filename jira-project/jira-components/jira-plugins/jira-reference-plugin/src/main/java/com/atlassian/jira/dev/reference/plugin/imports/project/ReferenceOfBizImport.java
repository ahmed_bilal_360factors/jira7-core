package com.atlassian.jira.dev.reference.plugin.imports.project;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.crowd.exception.embedded.InvalidGroupException;
import com.atlassian.fugue.Option;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.exception.ParseException;
import com.atlassian.jira.imports.project.core.BackupProject;
import com.atlassian.jira.imports.project.core.BackupSystemInformation;
import com.atlassian.jira.imports.project.core.ProjectImportResults;
import com.atlassian.jira.imports.project.handler.AbortImportException;
import com.atlassian.jira.imports.project.handler.PluggableImportOfBizEntityHandler;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapper;
import com.atlassian.jira.imports.project.mapper.SimpleProjectImportIdMapper;
import com.atlassian.jira.imports.project.mapper.SimpleProjectImportIdMapperImpl;
import com.atlassian.jira.security.groups.GroupManager;

import javax.annotation.Nullable;
import java.util.Map;

import static com.atlassian.fugue.Option.option;

/**
 * Reference plugin to observe AO data during the initial stages of a project import.
 * This is going to add any missing groups
 *
 * @since v7.0
 */
public class ReferenceOfBizImport implements PluggableImportOfBizEntityHandler {

    private boolean docStarted = false;
    private boolean docEnded = false;
    private BackupProject backupProject;
    private BackupSystemInformation backupSystemInformation;
    private ProjectImportMapper projectImportMapper;
    private Option<ProjectImportResults> projectImportResults;

    private SimpleProjectImportIdMapper groupMapper = new SimpleProjectImportIdMapperImpl();
    private GroupManager groupManager;

    @Override
    public void handleEntity(final String entityName, final Map<String, String> attributes)
            throws ParseException, AbortImportException {
        if (groupMapper == null) {
            throw new IllegalStateException("Group mapper is not available in the projectImportMapper.");
        }

        if (entityName.equals("Group")) {
            final String groupName = attributes.get("groupName");
            if (!groupManager.groupExists(groupName)) {
                try {
                    final Group group = groupManager.createGroup(groupName);
                } catch (OperationNotPermittedException e) {
                    throw new RuntimeException(e);
                } catch (InvalidGroupException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    @Override
    public boolean handlesEntity(final String entityName) {
        return entityName.equals("Group");
    }

    @Override
    public void startDocument() {
        docStarted = true;
        groupManager = ComponentAccessor.getGroupManager();
    }

    @Override
    public void endDocument() {
        docEnded = true;
        boolean allSet = backupProject != null;
        allSet &= backupSystemInformation != null;
        allSet &= projectImportMapper != null;
        allSet &= projectImportResults != null;
        final ApplicationProperties applicationProperties = ComponentAccessor.getApplicationProperties();
        applicationProperties.setOption("ReferenceOfBizPostImport.args.set", allSet);
    }

    @Override
    public void setBackupProject(final BackupProject backupProject) {
        this.backupProject = backupProject;
    }

    @Override
    public void setBackupSystemInformation(final BackupSystemInformation backupSystemInformation) {
        this.backupSystemInformation = backupSystemInformation;
    }

    @Override
    public void setProjectImportMapper(final ProjectImportMapper projectImportMapper) {
        groupMapper = projectImportMapper.getNamedIdMapper("groupMapper");
        this.projectImportMapper = projectImportMapper;
    }

    @Override
    public void setProjectImportResults(@Nullable final ProjectImportResults projectImportResults) {
        this.projectImportResults = option(projectImportResults);
    }

    public boolean isDocStarted() {
        return docStarted;
    }

    public boolean isDocEnded() {
        return docEnded;
    }

    public BackupProject getBackupProject() {
        return backupProject;
    }

    public BackupSystemInformation getBackupSystemInformation() {
        return backupSystemInformation;
    }

    public ProjectImportMapper getProjectImportMapper() {
        return projectImportMapper;
    }

    @Nullable
    public ProjectImportResults getProjectImportResults() {
        return projectImportResults.getOrNull();
    }
}
