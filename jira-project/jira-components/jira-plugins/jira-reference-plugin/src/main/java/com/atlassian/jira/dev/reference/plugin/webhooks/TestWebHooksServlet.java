package com.atlassian.jira.dev.reference.plugin.webhooks;

import com.google.common.base.Splitter;
import com.google.common.collect.Maps;
import org.codehaus.jackson.map.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

public class TestWebHooksServlet extends HttpServlet {
    private final ConcurrentMap<String, BlockingQueue<Publication>> publications = Maps.newConcurrentMap();
    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String eventIdentifier = req.getPathInfo().substring(req.getPathInfo().lastIndexOf('/') + 1);
        StringBuilder body = new StringBuilder();
        char[] buffer = new char[1024];
        int len;
        while ((len = req.getReader().read(buffer)) > -1) {
            body.append(buffer, 0, len);
        }
        System.out.println("Receiving web hook '" + eventIdentifier + "' with body\n" + body.toString());
        Publication publication = new Publication(eventIdentifier, body.toString(), queryParameters(req));
        publicationsOf(eventIdentifier).offer(publication);
    }

    private BlockingQueue<Publication> publicationsOf(String bucket) {
        publications.putIfAbsent(bucket, new ArrayBlockingQueue<Publication>(2));
        return publications.get(bucket);
    }

    private Map<String, String> queryParameters(final HttpServletRequest req) {
        return req.getQueryString() != null ? Splitter.on('&').trimResults().withKeyValueSeparator("=").split(req.getQueryString()) : Collections.<String, String>emptyMap();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            resp.setContentType("application/json");

            final Map<String, String> queryParameters = queryParameters(req);

            final BlockingQueue<Publication> queue = publicationsOf(queryParameters.get("bucket"));
            final Publication publication = queue.poll(5, TimeUnit.SECONDS);
            resp.getWriter().write("true".equals(queryParameters.get("params")) ? mapper.writeValueAsString(publication.params) : publication.body);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void doDelete(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        for (BlockingQueue<Publication> queue : publications.values()) {
            queue.clear();
        }
    }

    private static class Publication {
        private final String eventIdentifier;
        private final String body;
        private final Map<String, String> params;

        public Publication(String eventIdentifier, String body, final Map<String, String> params) {
            this.eventIdentifier = eventIdentifier;
            this.body = body;
            this.params = params;
        }
    }
}
