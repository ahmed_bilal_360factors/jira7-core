package com.atlassian.jira.dev.reference.plugin.ao;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.core.ofbiz.util.CoreTransactionUtil;
import com.atlassian.jira.util.dbc.Assertions;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.google.common.collect.Lists;
import org.ofbiz.core.entity.GenericTransactionException;

import java.util.List;

import static com.atlassian.jira.util.dbc.Assertions.notNull;


public class RefEntityServiceImpl implements RefEntityService {
    private final ActiveObjects ao;

    public RefEntityServiceImpl(ActiveObjects ao) {
        this.ao = Assertions.notNull(ao);
    }

    @Override
    public RefEntity add(final String description) {
        final RefEntity entity = ao.create(RefEntity.class);
        entity.setDescription(notNull("Description should not be null", description));
        entity.save();
        return entity;
    }

    @Override
    public void addButRollbackExternal(final String description) {
        boolean began = false;
        try {
            began = CoreTransactionUtil.begin();
            ao.executeInTransaction(new TransactionCallback<RefEntity>() {
                @Override
                public RefEntity doInTransaction() {
                    final RefEntity entity = ao.create(RefEntity.class);
                    entity.setDescription(notNull("Description should not be null", description));
                    entity.save();
                    return entity;
                }
            });
        } finally {
            try {
                CoreTransactionUtil.rollback(began);
                throw new RuntimeException("We didn't do that because we don't like you");
            } catch (GenericTransactionException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public void addInExternalTransaction(final String description) {
        try {
            boolean began = CoreTransactionUtil.begin();
            try {
                ao.executeInTransaction(new TransactionCallback<RefEntity>() {
                    @Override
                    public RefEntity doInTransaction() {
                        final RefEntity entity = ao.create(RefEntity.class);
                        entity.setDescription(notNull("Description should not be null", description));
                        entity.save();
                        return entity;
                    }
                });
            } catch (Exception e) {
                // Ignore. This would be bad to do in production code
            }
            CoreTransactionUtil.commit(began);
        } catch (GenericTransactionException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void addTwoInExternalTransaction(final String description, final String description2) {
        try {
            boolean began = CoreTransactionUtil.begin();
            try {
                ao.executeInTransaction(new TransactionCallback<RefEntity>() {
                    @Override
                    public RefEntity doInTransaction() {
                        final RefEntity entity = ao.create(RefEntity.class);
                        entity.setDescription(notNull("Description should not be null", description));
                        entity.save();
                        return entity;
                    }
                });
            } catch (Exception e) {
                // Ignore. This would be bad to do in production code
            }
            try {
                ao.executeInTransaction(new TransactionCallback<RefEntity>() {
                    @Override
                    public RefEntity doInTransaction() {
                        final RefEntity entity = ao.create(RefEntity.class);
                        entity.setDescription(notNull("Description should not be null", description2));
                        entity.save();
                        return entity;
                    }
                });
            } catch (Exception e) {
                // Ignore. This would be bad to do in production code
            }
            CoreTransactionUtil.commit(began);
        } catch (GenericTransactionException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void addInTransaction(final String description) {
        ao.executeInTransaction(new TransactionCallback<RefEntity>() {
            @Override
            public RefEntity doInTransaction() {
                final RefEntity entity = ao.create(RefEntity.class);
                entity.setDescription(notNull("Description should not be null", description));
                entity.save();
                return entity;
            }
        });
    }

    @Override
    public List<RefEntity> allEntities() {
        return Lists.newArrayList(ao.find(RefEntity.class));
    }

    @Override
    public void deleteAll() {
        ao.delete(ao.find(RefEntity.class));
    }
}
