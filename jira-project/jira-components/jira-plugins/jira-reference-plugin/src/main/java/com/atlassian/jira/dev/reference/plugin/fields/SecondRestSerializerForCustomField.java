package com.atlassian.jira.dev.reference.plugin.fields;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.rest.json.JsonData;
import com.atlassian.jira.plugin.customfield.CustomFieldRestSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SecondRestSerializerForCustomField implements CustomFieldRestSerializer {
    private final RestSerializerComponent restSerializerComponent;

    @Autowired
    public SecondRestSerializerForCustomField(final RestSerializerComponent restSerializerComponent) {
        this.restSerializerComponent = restSerializerComponent;
    }

    @Override
    public JsonData getJsonData(final CustomField field, final Issue issue) {
        return restSerializerComponent.getJsonData(field, issue);
    }
}

