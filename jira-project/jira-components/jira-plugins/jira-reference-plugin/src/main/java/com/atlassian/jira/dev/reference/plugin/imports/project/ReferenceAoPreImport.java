package com.atlassian.jira.dev.reference.plugin.imports.project;

import com.atlassian.fugue.Option;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.exception.ParseException;
import com.atlassian.jira.imports.project.ao.handler.PluggableImportAoEntityHandler;
import com.atlassian.jira.imports.project.core.BackupProject;
import com.atlassian.jira.imports.project.core.BackupSystemInformation;
import com.atlassian.jira.imports.project.core.ProjectImportResults;
import com.atlassian.jira.imports.project.handler.AbortImportException;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapper;
import com.atlassian.jira.imports.project.mapper.SimpleProjectImportIdMapper;
import com.atlassian.jira.imports.project.mapper.SimpleProjectImportIdMapperImpl;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.atlassian.fugue.Option.option;

/**
 * Reference plugin to observe AO data during the initial stages of a project import.
 *
 * @since v7.0
 */
public class ReferenceAoPreImport implements PluggableImportAoEntityHandler {

    private boolean docStarted = false;
    private boolean docEnded = false;
    private List<String> tables = new ArrayList<String>();
    private BackupProject backupProject;
    private BackupSystemInformation backupSystemInformation;
    private Option<ProjectImportResults> projectImportResults;
    private ProjectImportMapper projectImportMapper;
    private SimpleProjectImportIdMapper refentityMapper = new SimpleProjectImportIdMapperImpl();

    @Override
    public void handleEntity(final String entityName, final Map<String, Object> attributes)
            throws ParseException, AbortImportException {
        if (entityName.equals("AO_98E482_REF_ENTITY")) {
            refentityMapper.registerOldValue(attributes.get("ID").toString(), attributes.get("DESCRIPTION").toString());
        }
    }

    @Override
    public boolean handlesEntity(final String entityName) {
        return entityName.equals("AO_98E482_REF_ENTITY");
    }

    @Override
    public Long getEntityWeight(final String entityName) {
        return WEIGHT_NONE;
    }

    @Override
    public void startDocument() {
        docStarted = true;
    }

    @Override
    public void endDocument() {
        docEnded = true;
        boolean allSet = backupProject != null;
        allSet &= backupSystemInformation != null;
        allSet &= projectImportMapper != null;
        allSet &= projectImportResults != null;
        final ApplicationProperties applicationProperties = ComponentAccessor.getApplicationProperties();
        applicationProperties.setOption("ReferenceAoPreImport.args.set", allSet);
    }

    @Override
    public void endTable(final String tableName) {
        tables.add(tableName);
    }

    @Override
    public void setBackupProject(final BackupProject backupProject) {
        this.backupProject = backupProject;
    }

    @Override
    public void setBackupSystemInformation(final BackupSystemInformation backupSystemInformation) {
        this.backupSystemInformation = backupSystemInformation;
    }

    @Override
    public void setProjectImportMapper(final ProjectImportMapper projectImportMapper) {
        projectImportMapper.putNamedIdMapper("refentityMapper", refentityMapper);
        this.projectImportMapper = projectImportMapper;
    }

    @Override
    public void setProjectImportResults(@Nullable final ProjectImportResults projectImportResults) {
        this.projectImportResults = option(projectImportResults);
    }

    public boolean isDocStarted() {
        return docStarted;
    }

    public boolean isDocEnded() {
        return docEnded;
    }

    public List<String> getTables() {
        return tables;
    }

    public BackupProject getBackupProject() {
        return backupProject;
    }

    public BackupSystemInformation getBackupSystemInformation() {
        return backupSystemInformation;
    }

    public ProjectImportMapper getProjectImportMapper() {
        return projectImportMapper;
    }

    @Nullable
    public ProjectImportResults getProjectImportResults() {
        return projectImportResults.getOrNull();
    }
}
