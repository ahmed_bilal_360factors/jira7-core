package com.atlassian.jira.dev.reference.plugin.jql;

import com.atlassian.jira.JiraDataType;
import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.plugin.jql.function.AbstractJqlFunction;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;
import com.google.common.collect.Iterables;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.List;

/**
 * Echoes the the string passed in as an argument.
 *
 * @since v4.4
 */
public class EchoJqlFunction extends AbstractJqlFunction {
    public MessageSet validate(ApplicationUser searcher, @Nonnull FunctionOperand operand, @Nonnull TerminalClause terminalClause) {
        return validateNumberOfArgs(operand, 1);
    }

    public List<QueryLiteral> getValues(@Nonnull QueryCreationContext queryCreationContext,
                                        @Nonnull FunctionOperand operand, @Nonnull TerminalClause terminalClause) {
        return Collections.singletonList(new QueryLiteral(operand, Iterables.get(operand.getArgs(), 0)));
    }

    public int getMinimumNumberOfExpectedArguments() {
        return 1;
    }

    public JiraDataType getDataType() {
        return JiraDataTypes.TEXT;
    }
}
