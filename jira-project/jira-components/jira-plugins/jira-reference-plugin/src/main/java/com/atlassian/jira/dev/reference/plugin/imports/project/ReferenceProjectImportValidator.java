package com.atlassian.jira.dev.reference.plugin.imports.project;

import com.atlassian.jira.external.beans.ExternalProject;
import com.atlassian.jira.imports.project.ao.handler.PluggableValidator;
import com.atlassian.jira.imports.project.core.BackupProject;
import com.atlassian.jira.imports.project.core.ValidationMessage;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;

import java.util.Collection;

/**
 * Reference plugin validate A Project for import is OK for this plugin
 *
 * @since v7.0
 */
public class ReferenceProjectImportValidator implements PluggableValidator {
    @Override
    public ValidationMessage validate(final BackupProject backupProject, final I18nHelper i18n) {
        // We check the Overview has not vetoed this project from import
        MessageSet messages = new MessageSetImpl();
        ExternalProject project = backupProject.getProject();
        Collection<Object> refAdditionalData = backupProject.getAdditionalData("com.atlassian.jira.dev.reference-plugin");
        if (refAdditionalData != null && refAdditionalData.contains(Boolean.TRUE)) {
            messages.addMessage(MessageSet.Level.ERROR, i18n.getText("reference.imports.project.error.no.ref.for.project", project.getKey()));
        }
        return new ValidationMessage("Reference Plugin", messages);
    }

}