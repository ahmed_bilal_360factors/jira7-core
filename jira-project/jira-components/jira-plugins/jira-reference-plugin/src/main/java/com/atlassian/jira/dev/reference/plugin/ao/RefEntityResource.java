package com.atlassian.jira.dev.reference.plugin.ao;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import javax.annotation.Nullable;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.List;

import static org.apache.commons.lang3.StringUtils.isEmpty;

/**
 * Resource for testing Transactions in AO
 *
 * @since v7.0
 */
@Path("refentity")
@AnonymousAllowed
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class RefEntityResource {
    private final RefEntityService refEntityService;

    public RefEntityResource(final RefEntityService refEntityService) {
        this.refEntityService = refEntityService;
    }

    @GET
    public Response getAll() {
        List<RefEntity> refs = refEntityService.allEntities();
        List<String> descriptions = Lists.newArrayList(Iterables.transform(refs, new Function<RefEntity, String>() {
            @Override
            public String apply(@Nullable final RefEntity refEntity) {
                return refEntity.getDescription();
            }
        }));
        Collections.sort(descriptions);

        return Response.ok().entity(descriptions.toArray()).cacheControl(never()).build();
    }

    @DELETE
    public Response deleteAll() {
        refEntityService.deleteAll();
        return Response.status(Response.Status.NO_CONTENT).cacheControl(never()).cacheControl(never()).build();
    }

    @POST
    public Response add(@QueryParam("description") final String description) {
        refEntityService.add(description);
        return Response.noContent().cacheControl(never()).build();
    }

    @POST
    @Path("withTransaction")
    public Response addInTransaction(@QueryParam("description") final String description) {
        refEntityService.addInTransaction(description);
        return Response.noContent().cacheControl(never()).build();
    }

    @POST
    @Path("withExternalTransaction")
    public Response addInExternalTransaction(@QueryParam("description") final String description, @QueryParam("description2") final String description2) {
        if (isEmpty(description2)) {
            refEntityService.addInExternalTransaction(description);
        } else {
            refEntityService.addTwoInExternalTransaction(description, description2);
        }
        return Response.noContent().cacheControl(never()).build();
    }

    @POST
    @Path("butRollbackExternal")
    public Response addButRollbackExternal(@QueryParam("description") final String description) {
        try {
            refEntityService.addButRollbackExternal(description);
        } catch (Exception e) {
            return Response.notModified().cacheControl(never()).build();
        }
        return Response.noContent().cacheControl(never()).build();
    }

    private static javax.ws.rs.core.CacheControl never() {
        javax.ws.rs.core.CacheControl cacheNever = new javax.ws.rs.core.CacheControl();
        cacheNever.setNoStore(true);
        cacheNever.setNoCache(true);

        return cacheNever;
    }
}
