package com.atlassian.jira.dev.reference.plugin.imports.project;

import com.atlassian.fugue.Option;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.imports.project.core.BackupProject;
import com.atlassian.jira.imports.project.core.BackupSystemInformation;
import com.atlassian.jira.imports.project.core.ProjectImportResults;
import com.atlassian.jira.imports.project.handler.PluggableImportRunnable;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapper;

import javax.annotation.Nullable;

import static com.atlassian.fugue.Option.option;

/**
 * This should run after the project has been completed.
 *
 * @since v7.0
 */
public class ReferencePostImportPluginModule implements PluggableImportRunnable {
    private BackupProject backupProject;
    private BackupSystemInformation backupSystemInformation;
    private ProjectImportMapper projectImportMapper;
    private Option<ProjectImportResults> projectImportResults;

    @Override
    public void run() {
        final ApplicationProperties applicationProperties = ComponentAccessor.getApplicationProperties();
        applicationProperties.setOption("ReferencePostImportPluginRun", true);
        boolean allSet = backupProject != null;
        allSet &= backupSystemInformation != null;
        allSet &= projectImportMapper != null;
        allSet &= projectImportResults != null;
        applicationProperties.setOption("ReferencePostImportPluginRun.args.set", allSet);
    }

    @Override
    public void setBackupProject(final BackupProject backupProject) {
        this.backupProject = backupProject;
    }

    @Override
    public void setBackupSystemInformation(final BackupSystemInformation backupSystemInformation) {
        this.backupSystemInformation = backupSystemInformation;
    }

    @Override
    public void setProjectImportMapper(final ProjectImportMapper projectImportMapper) {
        this.projectImportMapper = projectImportMapper;
    }

    @Override
    public void setProjectImportResults(@Nullable final ProjectImportResults projectImportResults) {
        this.projectImportResults = option(projectImportResults);
    }
}
