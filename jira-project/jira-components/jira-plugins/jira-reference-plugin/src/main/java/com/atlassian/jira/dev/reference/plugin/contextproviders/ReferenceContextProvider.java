package com.atlassian.jira.dev.reference.plugin.contextproviders;

import com.atlassian.core.util.map.EasyMap;
import com.atlassian.jira.plugin.webfragment.contextproviders.AbstractJiraContextProvider;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;

import java.util.Map;

public class ReferenceContextProvider extends AbstractJiraContextProvider {
    @Override
    public Map getContextMap(ApplicationUser user, JiraHelper jiraHelper) {
        return EasyMap.build("test", "original");
    }
}
