package com.atlassian.jira.dev.reference.plugin.contextproviders;

import com.atlassian.jira.plugin.webfragment.contextproviders.AbstractJiraContextProvider;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.PluginParseException;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

/**
 * This class populates the context of velocity template with entityType parameter.
 */
public class EntityPropertyTypeConditionProvider extends AbstractJiraContextProvider {

    private String entityType;

    @Override
    public void init(Map params) throws PluginParseException {
        super.init(params);
        entityType = (String) params.get("entityType");
    }

    @Override
    public Map getContextMap(ApplicationUser user, JiraHelper jiraHelper) {
        return ImmutableMap.of("entityType", entityType);
    }
}
