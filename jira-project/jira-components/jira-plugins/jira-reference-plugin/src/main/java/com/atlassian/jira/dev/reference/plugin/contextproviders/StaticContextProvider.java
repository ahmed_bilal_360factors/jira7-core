package com.atlassian.jira.dev.reference.plugin.contextproviders;

import com.atlassian.jira.plugin.webfragment.contextproviders.AbstractJiraContextProvider;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.PluginParseException;

import java.util.Map;

import static java.util.stream.Collectors.toMap;

public class StaticContextProvider extends AbstractJiraContextProvider {

    private Map<String, Object> contextMap;

    @Override
    public void init(Map params) throws PluginParseException {
        super.init(params);
        contextMap = ((Map<String, Object>) params).entrySet().stream()
                .filter(e -> e.getValue() instanceof String)
                .collect(toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    @Override
    public Map getContextMap(ApplicationUser user, JiraHelper jiraHelper) {
        return contextMap;
    }
}
