package com.atlassian.jira.dev.reference.plugin.fields;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.rest.json.JsonData;
import com.atlassian.jira.plugin.customfield.CustomFieldRestSerializer;
import org.codehaus.jackson.annotate.JsonProperty;

public class FirstRestSerializerForCustomField implements CustomFieldRestSerializer {
    @Override
    public JsonData getJsonData(final CustomField field, final Issue issue) {
        final String value = (String) field.getValue(issue);
        return new JsonData(new JsonBeanOne(value));
    }

    static class JsonBeanOne {
        @JsonProperty
        private String value;

        public JsonBeanOne(final String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public void setValue(final String value) {
            this.value = value;
        }
    }
}

