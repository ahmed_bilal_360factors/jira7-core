package com.atlassian.jira.less.impl;

import com.atlassian.event.api.EventListener;
import com.atlassian.jira.config.properties.LookAndFeelBean;
import com.atlassian.lesscss.spi.UriResolverStateChangedEvent;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.URI;

import static com.atlassian.jira.less.impl.DynamicLookAndFeelUtil.isDynamicLookAndFeelURI;

/**
 */
@Component
public class DynamicLookAndFeelEventForwarder implements InitializingBean, DisposableBean {
    private final PluginEventManager pluginEventManager;

    @Autowired
    public DynamicLookAndFeelEventForwarder(@ComponentImport PluginEventManager pluginEventManager) {
        this.pluginEventManager = pluginEventManager;
    }

    @EventListener
    public void onStateChanged(LookAndFeelBean.LookAndFeelChangedEvent event) {
        pluginEventManager.broadcast(new DynamicUriResolverStateChangedEvent("look-and-feel"));
    }

    @Override
    public void destroy() throws Exception {
        pluginEventManager.unregister(this);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        pluginEventManager.register(this);
    }

    public static class DynamicUriResolverStateChangedEvent extends UriResolverStateChangedEvent {
        public DynamicUriResolverStateChangedEvent(final Object source) {
            super(source);
        }

        @Override
        public boolean hasChanged(final URI uri) {
            return isDynamicLookAndFeelURI(uri);
        }
    }
}