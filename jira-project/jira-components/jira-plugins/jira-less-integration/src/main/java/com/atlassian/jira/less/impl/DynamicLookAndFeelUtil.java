package com.atlassian.jira.less.impl;

import java.net.URI;

public class DynamicLookAndFeelUtil {
    static boolean isDynamicLookAndFeelURI(final URI uri) {
        if ("dynamic".equals(uri.getScheme())) {
            String special = uri.getSchemeSpecificPart();
            if ("lookandfeel.less".equals(special)) {
                return true;
            }
        }
        return false;
    }
}
