package com.atlassian.jira.less.impl;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.web.less.LookAndFeelLessProvider;
import com.atlassian.lesscss.spi.DimensionAwareUriResolver;
import com.atlassian.lesscss.spi.EncodeStateResult;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.impl.PrebakeErrorFactory;
import com.atlassian.webresource.api.assembler.resource.PrebakeError;
import com.atlassian.webresource.api.prebake.Coordinate;
import com.atlassian.webresource.api.prebake.Dimensions;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.Optional;

import static com.atlassian.jira.less.impl.DynamicLookAndFeelUtil.isDynamicLookAndFeelURI;

/**
 */
@Scanned
public class DynamicLookAndFeelUriResolver implements DimensionAwareUriResolver {
    private LookAndFeelLessProvider lessGenerator;

    @Autowired
    public DynamicLookAndFeelUriResolver(
            @ComponentImport WebResourceIntegration webResourceIntegration,
            @ComponentImport ApplicationProperties applicationProperties
    ) {
        this.lessGenerator = new LookAndFeelLessProvider(applicationProperties, webResourceIntegration);
    }

    @Override
    public boolean exists(final URI uri) {
        return isDynamicLookAndFeelURI(uri);
    }

    @Override
    public EncodeStateResult encodeState(URI uri, Coordinate coordinate) {
        PrebakeError prebakeError = PrebakeErrorFactory.from("Less resource makes use of JIRA Look & Feel properties");
        String encodedState = lessGenerator.encodeState();
        return new EncodeStateResult(encodedState, Optional.of(prebakeError));
    }

    @Override
    public String encodeState(final URI uri) {
        return lessGenerator.encodeState();
    }

    @Override
    public InputStream open(final URI uri) throws IOException {
        String less = lessGenerator.makeLookAndFeelLess();
        return new ByteArrayInputStream(less.getBytes(Charset.forName("UTF-8")));
    }

    @Override
    public boolean supports(final URI uri) {
        return isDynamicLookAndFeelURI(uri);
    }

    @Override
    public Dimensions computeDimensions() {
        return Dimensions.empty();
    }
}