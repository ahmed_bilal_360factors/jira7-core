require([
    'aui/restful-table',
    'jira/util/formatter',
    'jira/application-properties/row',
    'jira/application-properties/edit-row',
    'jira/application-properties/key-view',
    'wrm/context-path',
    'jquery'
], function(RestfulTable, formatter, row, editRow, keyView, wrmContextPath, $) {
    // Initialises the application properties table on the advanced configuration page
    $(function(){
        var propRest = wrmContextPath() + "/rest/api/latest/application-properties";
        var appPropertyTable = $("#application-properties-table");

        new RestfulTable({
            el: appPropertyTable, // table to add entries to. Entries are appended to the tables <tbody> element
            resources: {
                all: propRest + "/advanced-settings",
                self: propRest
            },
            columns: [
                {
                    id: "key",
                    header: "Key",
                    styleClass: "application-property-key-col",
                    allowEdit: false,
                    readView: keyView
                },
                {
                    id: "value",
                    header: "Value",
                    styleClass: "application-property-value-col",
                    emptyText: formatter.I18n.getText("admin.advancedconfiguration.setvalue")
                }
            ],
            allowEdit: true,
            allowCreate: false,
            views: {
                editRow: editRow,
                row: row
            }
        });
    });
});
