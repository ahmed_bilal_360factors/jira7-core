define('jira/application-properties/key-view', ['aui/restful-table'], function(RestfulTable) {
    return RestfulTable.CustomReadView.extend({
        render: function(args) {
            args.desc = this.model.get("desc");
            return JIRA.Templates.AppProperty.key(args);
        }
    });
});

AJS.namespace('JIRA.Admin.AppProperty.KeyView', null, require('jira/application-properties/key-view'));
