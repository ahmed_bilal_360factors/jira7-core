
define('jira/application-properties/row', ['aui/restful-table'], function(RestfulTable) {
    /**
     * Readonly view of Application Property row
     *
     * @class AppProperty.Row
     */
    return RestfulTable.Row.extend({

        initialize: function () {

            // call super
            RestfulTable.Row.prototype.initialize.apply(this, arguments);

            this.bind(RestfulTable.Events.RENDER, function () {
                this.$el.attr("data-row-key", this.model.get("key"));
            });

            // crap work around to handle backbone not extending events
            // (https://github.com/documentcloud/backbone/issues/244)
            this.events["click .application-property-revert"] = "_revert";
            this.delegateEvents();
        },


        _revert: function (e) {
            this.trigger("focus");

            var defaultValue = this.$el.find(".application-property-value-default").val();
            this.sync({value: defaultValue});
        },

        renderOperations: function (update, all) {
            return JIRA.Templates.AppProperty.operations(all);
        }

    });
});

AJS.namespace('JIRA.Admin.AppProperty.Row', null, require('jira/application-properties/row'));
