define('jira/application-properties/edit-row', ['aui/restful-table'], function(RestfulTable) {
    /**
     * Edit/Create view of Application Property row
     *
     * @class AppProperty.EditRow
     */
    return RestfulTable.EditRow.extend({

        initialize: function () {

            // call super
            RestfulTable.EditRow.prototype.initialize.apply(this, arguments);

            this.bind(RestfulTable.Events.RENDER, function () {
                this.$el.attr("data-row-key", this.model.get("key"));
            });
        },

        submit: function () {
            RestfulTable.EditRow.prototype.submit.apply(this, arguments);
        }
    });
});

AJS.namespace('JIRA.Admin.AppProperty.EditRow', null, require('jira/application-properties/edit-row'));
