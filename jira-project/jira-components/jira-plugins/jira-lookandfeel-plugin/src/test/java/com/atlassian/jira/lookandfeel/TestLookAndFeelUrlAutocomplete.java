package com.atlassian.jira.lookandfeel;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.util.I18nHelper;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.either;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.core.StringStartsWith.startsWith;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

/**
 * @see LookAndFeelUrlAutocomplete
 */
public class TestLookAndFeelUrlAutocomplete {

    @Rule
    public final MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private I18nHelper i18nHelper;

    @InjectMocks
    public LookAndFeelUrlAutocomplete lookAndFeelUrlAutocomplete;

    @Before
    public void setup() {
        MockComponentWorker mockComponentWorker = new MockComponentWorker();
        mockComponentWorker.addMock(LookAndFeelUrlAutocomplete.class, lookAndFeelUrlAutocomplete);
        mockComponentWorker.init();

        ComponentAccessor.getApplicationProperties().setString(APKeys.JIRA_BASEURL, "https://jira.atlassian.com");

        when(i18nHelper.getText(anyString())).then(returnsFirstArg());
    }

    @Test
    public void shouldComplete() {
        assertThat(lookAndFeelUrlAutocomplete.completeUrl("/it/has/slash/before"),
                both(endsWith("/it/has/slash/before")).and(either(startsWith("http://")).or(startsWith("https://"))));
    }

    @Test
    public void shouldNotComplete() {
        assertEquals("http://an.url/to/resource", lookAndFeelUrlAutocomplete.completeUrl("http://an.url/to/resource"));
        assertEquals("https://an.url/to/resource", lookAndFeelUrlAutocomplete.completeUrl("https://an.url/to/resource"));

        assertEquals("without/slash/before", lookAndFeelUrlAutocomplete.completeUrl("without/slash/before"));
        assertEquals("../dots/before/path", lookAndFeelUrlAutocomplete.completeUrl("../dots/before/path"));
        assertEquals("./single/dot/before", lookAndFeelUrlAutocomplete.completeUrl("./single/dot/before"));
    }

    @Test
    public void shouldCreateTip() {
        assertEquals("jira.lookandfeel.upload.url.invalid.noclue", lookAndFeelUrlAutocomplete.completionTip("http://#"));
        assertEquals("jira.lookandfeel.upload.url.invalid.http", lookAndFeelUrlAutocomplete.completionTip("it/has/slash/before"));
    }
}
