package com.atlassian.jira.lookandfeel.image;

import com.atlassian.jira.util.I18nHelper;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @see ImageDescriptorFactoryImpl
 */
public class TestImageDescriptorFactoryImpl {

    @Rule
    public final MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private I18nHelper i18nHelper;

    @InjectMocks
    private ImageDescriptorFactoryImpl imageDescriptorFactory;

    @Test
    public void urlsSupported() {
        assertTrue(imageDescriptorFactory.isUrlSupported("http://a"));
        assertTrue(imageDescriptorFactory.isUrlSupported("http://images/icon-jira-logo.png"));
        assertTrue(imageDescriptorFactory.isUrlSupported("https://www.atlassian.com/docroot/wac/resources/wac/img/social-icons/atlassian_logo.jpg"));
    }

    @Test
    public void urlsNotSupported() {
        assertFalse(imageDescriptorFactory.isUrlSupported("http"));
        assertFalse(imageDescriptorFactory.isUrlSupported("http:"));
        assertFalse(imageDescriptorFactory.isUrlSupported("http:/"));
        assertFalse(imageDescriptorFactory.isUrlSupported("http://"));
        assertFalse(imageDescriptorFactory.isUrlSupported("%^$^&^%&^"));
        assertFalse(imageDescriptorFactory.isUrlSupported("url"));
        assertFalse(imageDescriptorFactory.isUrlSupported("we/dont/support/this/path"));
        assertFalse(imageDescriptorFactory.isUrlSupported("/images/icon-jira-logo.png"));
        assertFalse(imageDescriptorFactory.isUrlSupported("./images/icon-jira-logo.png"));
        assertFalse(imageDescriptorFactory.isUrlSupported("../../../../../sw/java/tomcat/apache-tomcat-8.0.20/webapps/ROOT/tomcat.png"));
        assertFalse(imageDescriptorFactory.isUrlSupported("."));
        assertFalse(imageDescriptorFactory.isUrlSupported("https://!@#$%^&*"));
    }
}
