package com.atlassian.jira.lookandfeel.image;

import com.atlassian.jira.util.I18nHelper;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.IOException;

/**
 * @see URLImageDescriptor
 */
public class TestURLImageDescriptor {

    @Rule
    public final MockitoRule rule = MockitoJUnit.rule();

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Mock
    private I18nHelper i18nHelper;

    @Test
    public void absoluteUrlAccepted() throws IOException {
        exception.expect(IOException.class);
        new URLImageDescriptor("http://an.url/to/resource", i18nHelper);
    }

    @Test
    public void traverseNotAccepted() throws IOException {
        exception.expect(IllegalArgumentException.class);
        new URLImageDescriptor("../../../../../i/am/trying/to/traverse/", i18nHelper);
    }

    @Test
    public void RelativeUrlWithDotNotAccepted() throws IOException {
        exception.expect(IllegalArgumentException.class);
        new URLImageDescriptor("./an/relative/url/to/resource", i18nHelper);
    }

    @Test
    public void noSlashRelativeUrlNotAccepted() throws IOException {
        exception.expect(IllegalArgumentException.class);
        new URLImageDescriptor("an/relative/url/to/resource", i18nHelper);
    }

    @Test
    public void fileSystemPathNotAccepted() throws IOException {
        exception.expect(IllegalArgumentException.class);
        new URLImageDescriptor("/dev/null", i18nHelper);
    }
}
