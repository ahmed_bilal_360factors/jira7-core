package com.atlassian.jira.lookandfeel.image;

import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.UrlValidator;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

/**
 * This represents an image that is uploaded from a provided URL
 *
 * @since v4.4
 */
public class URLImageDescriptor extends ImageDescriptor {
    private final I18nHelper i18nHelper;

    public URLImageDescriptor(String url, I18nHelper i18nHelper) throws IOException {
        this.i18nHelper = i18nHelper;
        if (isUrlSupported(url)) {
            handleAbsoluteUrl(url);
        } else {
            throw new IllegalArgumentException("The provided url is not supported.");
        }
    }

    private void handleAbsoluteUrl(final String filename) throws IOException {
        URL url = new URL(filename);
        URLConnection urlConnection = url.openConnection();
        urlConnection.connect();
        contentType = urlConnection.getContentType();
        fileName = filename;
        imageData = urlConnection.getInputStream();
    }

    public static boolean isUrlSupported(String url) {
        return UrlValidator.isValid(url);
    }

    @Override
    public String getImageDescriptorType() {
        return i18nHelper.getText("jira.lookandfeel.urlimagedescriptor.type");
    }
}
