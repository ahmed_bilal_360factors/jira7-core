package com.atlassian.jira.lookandfeel;

/**
 * Used to help build proper url for image upload
 *
 * @since v7.1
 */
public interface UrlAutocomplete {
    String completeUrl(String url);
    String completionTip(String url);
}
