package com.atlassian.jira.lookandfeel.image;

import webwork.multipart.MultiPartRequestWrapper;

import java.io.IOException;

/**
 * Used to construct {@link ImageDescriptor} objects using url or http request with image data.
 *
 * @since v7.1
 */
public interface ImageDescriptorFactory {
    boolean isUrlSupported(String url);
    ImageDescriptor withUrl(String url) throws IOException;
    ImageDescriptor withRequest(String parameterName, MultiPartRequestWrapper multiPartRequest) throws IOException;
}
