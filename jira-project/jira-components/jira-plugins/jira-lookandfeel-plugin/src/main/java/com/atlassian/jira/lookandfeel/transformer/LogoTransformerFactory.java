package com.atlassian.jira.lookandfeel.transformer;

import com.atlassian.jira.lookandfeel.LookAndFeelProperties;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.plugin.webresource.transformer.CharSequenceDownloadableResource;
import com.atlassian.plugin.webresource.transformer.TransformerParameters;
import com.atlassian.plugin.webresource.transformer.UrlReadingWebResourceTransformer;
import com.atlassian.plugin.webresource.url.UrlBuilder;
import com.atlassian.webresource.api.prebake.Coordinate;
import com.atlassian.webresource.api.prebake.DimensionAwareTransformerUrlBuilder;
import com.atlassian.webresource.api.prebake.DimensionAwareWebResourceTransformerFactory;
import com.atlassian.webresource.api.prebake.Dimensions;

/**
 * @since v6.3
 */
public class LogoTransformerFactory implements DimensionAwareWebResourceTransformerFactory {
    private final LookAndFeelProperties lookAndFeelProperties;
    private final WebResourceUrlProvider webResourceUrlProvider;

    public LogoTransformerFactory(LookAndFeelProperties lookAndFeelProperties, WebResourceUrlProvider provider) {
        this.lookAndFeelProperties = lookAndFeelProperties;
        this.webResourceUrlProvider = provider;
    }

    @Override
    public Dimensions computeDimensions() {
        return Dimensions.empty(); // we don't use any query params
    }

    @Override
    public DimensionAwareTransformerUrlBuilder makeUrlBuilder(final TransformerParameters transformerParameters) {
        return new DimensionAwareTransformerUrlBuilder() {
            @Override
            public void addToUrl(final UrlBuilder urlBuilder) {
                urlBuilder.addToHash("defaultLogo", lookAndFeelProperties.getDefaultCssLogoUrl());
                urlBuilder.addToHash("defaultLogo", lookAndFeelProperties.getMaxLogoHeight());
            }

            @Override
            public void addToUrl(final UrlBuilder urlBuilder, final Coordinate coord) {
                addToUrl(urlBuilder); // we don't use query params, so can ignore coordinate
            }
        };
    }

    @Override
    public UrlReadingWebResourceTransformer makeResourceTransformer(final TransformerParameters transformerParameters) {
        return (transformableResource, params) -> new CharSequenceDownloadableResource(transformableResource.nextResource()) {
            protected String transform(final CharSequence originalContent) {
                String replacedContent = originalContent.toString();
                String prefix = webResourceUrlProvider.getStaticResourcePrefix(UrlMode.AUTO);
                // JRADEV-6590 If this is the root app, then ignore the slash
                if ("/".equals(prefix)) {
                    prefix = "";
                }
                String defaultLogoUrl = lookAndFeelProperties.getDefaultCssLogoUrl();
                if (defaultLogoUrl != null && !defaultLogoUrl.startsWith("http://") && !defaultLogoUrl.startsWith(".")) {
                    defaultLogoUrl = prefix + defaultLogoUrl;
                }
                replacedContent = replacedContent.replace("@defaultLogoUrl", defaultLogoUrl);
                replacedContent = replacedContent.replace("@maxLogoHeight", String.valueOf(lookAndFeelProperties.getMaxLogoHeight()));
                return replacedContent;
            }
        };
    }
}
