package com.atlassian.jira.lookandfeel.image;

import com.atlassian.jira.util.I18nHelper;
import webwork.multipart.MultiPartRequestWrapper;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Used to construct {@link ImageDescriptor} objects.
 *
 * Constructed objects need to be closed
 * @see ImageDescriptor#closeImageStreamQuietly()
 *
 * @since v7.1
 */
public class ImageDescriptorFactoryImpl implements ImageDescriptorFactory {

    private I18nHelper i18nHelper;

    public ImageDescriptorFactoryImpl(I18nHelper i18nHelper) {
        this.i18nHelper = i18nHelper;
    }

    /**
     * Checks if url can be used to construct {@link ImageDescriptor} using {@link #withUrl(String)}
     *
     * @param url - url to check
     * @return true if url can be used; false if url should not be used (will cause Exception)
     */
    @Override
    public boolean isUrlSupported(String url) {
        return URLImageDescriptor.isUrlSupported(url);
    }

    /**
     * If provided url is supported it constructs {@link ImageDescriptor} object with it
     *
     * @param url - url to create {@link ImageDescriptor} with
     * @return {@link ImageDescriptor} object containing image data that was pointed by provided url
     *
     * @throws IOException - when fails to open stream to image resource provided by url. Can occur with:
     * @see URL#openConnection()
     * @see URLConnection#connect()
     * @see URLConnection#getInputStream()
     *
     * @throws IllegalArgumentException - if url is not supported
     * @see URLImageDescriptor#isUrlSupported(String)
     */
    @Override
    public ImageDescriptor withUrl(String url) throws IOException {
        if (URLImageDescriptor.isUrlSupported(url)) {
            return new URLImageDescriptor(url, i18nHelper);
        } else {
            throw new IllegalArgumentException("The provided url is not supported.");
        }
    }

    /**
     *
     * @param parameterName - parameter name for uploaded image contained in provided {@link MultiPartRequestWrapper}
     * @param multiPartRequest - multi part request containing image
     * @return {@link ImageDescriptor} object containing image data uploaded by request
     * @throws IOException - with constructor for {@link FileInputStream#FileInputStream(String)}
     */
    @Override
    public ImageDescriptor withRequest(String parameterName, MultiPartRequestWrapper multiPartRequest) throws IOException {
        return new MultiPartImageDescriptor(parameterName, multiPartRequest, i18nHelper);
    }
}
