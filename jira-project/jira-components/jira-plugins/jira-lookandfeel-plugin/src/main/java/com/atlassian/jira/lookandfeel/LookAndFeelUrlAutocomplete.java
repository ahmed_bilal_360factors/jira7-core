package com.atlassian.jira.lookandfeel;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.util.I18nHelper;

/**
 *
 * @since v7.1
 */
public class LookAndFeelUrlAutocomplete implements UrlAutocomplete {

    private I18nHelper i18nHelper;

    public LookAndFeelUrlAutocomplete(I18nHelper i18nHelper) {
        this.i18nHelper = i18nHelper;
    }

    /**
     * Completes URL that starts with "/" to contain full server address as a prefix
     *
     * @param url - url to be completed
     * @return completed url
     */
    @Override
    public String completeUrl(String url) {
        if (url != null && url.startsWith("/")) {
            url = ComponentAccessor.getApplicationProperties().getString(APKeys.JIRA_BASEURL) + url;
        }
        return url;
    }

    /**
     * Used to give tips about provided by user url.
     * When url is invalid it will try to provide instructions how to fix it.
     *
     * @param url - url to be analysed for any tip
     * @return message to be printed for user
     */
    @Override
    public String completionTip(String url) {
        if (url == null || (!url.startsWith("http://") && !url.startsWith("https://"))) {
            return i18nHelper.getText("jira.lookandfeel.upload.url.invalid.http");
        } else {
            return i18nHelper.getText("jira.lookandfeel.upload.url.invalid.noclue");
        }
    }
}
