require([
    'jira-look-and-feel/edit-look-and-feel',
    'jira-look-and-feel/edit-mode-init',
    'jira-look-and-feel/properties-table-init',
    'jquery'
], function(EditLNF, EditMode, PropertiesTable, jQuery) {
    'use strict';

    jQuery(function(){
        EditLNF.initEditLookAndFeel();
        EditMode.init();
        PropertiesTable.init();
    });
});
