/**
 *Initialises the application properties table on the advanced configuration page
 */
define('jira-look-and-feel/properties-table-init', [
    'jira-look-and-feel/properties-table',
    'jquery',
    'jira/util/formatter',
    'wrm/context-path',
    'aui/restful-table'
], function(LookAndFeelProperty, jQuery, formatter, wrmContextPath, RestfulTable) {
    'use strict';

    var propRest = wrmContextPath() + "/rest/api/latest/application-properties";

    function initColoursRestfulTable() {
        var appPropertyTable = jQuery("#lookandfeel-colours-properties-table");

        new RestfulTable({
            el: appPropertyTable, // table to add entries to. Entries are appended to the tables <tbody> element
            resources: {
                all: propRest + "?permissionLevel=ADMIN&keyFilter=" + encodeURIComponent("jira\\.lf\\..*colou?r"),
                self: propRest
            },
            columns: [
                {
                    id: "key",
                    header: "",
                    styleClass: "property-key-col",
                    allowEdit: false,
                    readView: LookAndFeelProperty.KeyView,
                    editView: LookAndFeelProperty.KeyView
                },
                {
                    id: "value",
                    header: "",
                    styleClass: "property-value-col",
                    emptyText: formatter.I18n.getText("admin.advancedconfiguration.setvalue"),
                    readView: LookAndFeelProperty.ColourReadView,
                    editView: LookAndFeelProperty.ColourEditView
                }
            ],
            allowEdit: true,
            allowCreate: false,
            views: {
                editRow: LookAndFeelProperty.EditRow,
                row: LookAndFeelProperty.Row
            }
        });
    }

    function initGadgetColoursRestfulTable() {
        var appPropertyTable = jQuery("#lookandfeel-gadget-colours-properties-table");

        new RestfulTable({
            el: appPropertyTable, // table to add entries to. Entries are appended to the tables <tbody> element
            resources: {
                all: propRest + "?permissionLevel=ADMIN&keyFilter=" + encodeURIComponent("jira\\.lf\\..*gadget\\.colou?r\\d?"),
                self: propRest
            },
            columns: [

                {
                    id: "key",
                    header: "",
                    styleClass: "property-key-col",
                    allowEdit: false,
                    readView: LookAndFeelProperty.KeyView,
                    editView: LookAndFeelProperty.KeyView
                },
                {
                    id: "value",
                    header: "",
                    styleClass: "property-value-col",
                    emptyText: formatter.I18n.getText("admin.advancedconfiguration.setvalue"),
                    readView: LookAndFeelProperty.ColourReadView,
                    editView: LookAndFeelProperty.ColourEditView
                }
            ],
            allowEdit: true,
            allowCreate: false,
            views: {
                editRow: LookAndFeelProperty.EditRow,
                row: LookAndFeelProperty.Row
            }
        });
    }

    function initDateTimeRestfulTable() {
        var appPropertyTable = jQuery("#lookandfeel-date-time-properties-table");

        new RestfulTable({
            el: appPropertyTable, // table to add entries to. Entries are appended to the tables <tbody> element
            resources: {
                all: propRest + "?permissionLevel=ADMIN&keyFilter=" + encodeURIComponent("jira\\.lf\\.date\\..*|jira\\.date\\.time\\.picker\\.use\\.iso8061"),
                self: propRest
            },
            columns: [
                {
                    id: "key",
                    header: "",
                    styleClass: "property-key-col",
                    allowEdit: false,
                    readView: LookAndFeelProperty.KeyView,
                    editView: LookAndFeelProperty.KeyView
                },
                {
                    id: "value",
                    header: "",
                    styleClass: "property-value-col",
                    emptyText: formatter.I18n.getText("admin.advancedconfiguration.setvalue"),
                    readView: LookAndFeelProperty.DateReadView,
                    editView: LookAndFeelProperty.DateEditView
                }
            ],
            allowEdit: true,
            allowCreate: false,
            views: {
                editRow: LookAndFeelProperty.EditRow,
                row: LookAndFeelProperty.Row
            }
        });
    }

    return {
        init: function initLookAndFeelPropertiesTables() {
            initColoursRestfulTable();
            initGadgetColoursRestfulTable();
            initDateTimeRestfulTable();
        }
    };
});
