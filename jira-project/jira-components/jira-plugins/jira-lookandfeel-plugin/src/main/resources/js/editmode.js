define('jira-look-and-feel/edit-mode-init', ['jquery'], function(jQuery) {
    'use strict';

    function init() {
        var warningMessage = jQuery('#mode_warning');
        var initialMode = jQuery('#mode_select option:selected').val();
        jQuery('#mode_select').change(function() {
            if (initialMode === jQuery('#mode_select option:selected').val()) {
                warningMessage.hide();
            } else {
                warningMessage.show();
            }
        });
    }

    return {
        init: init
    };
});
