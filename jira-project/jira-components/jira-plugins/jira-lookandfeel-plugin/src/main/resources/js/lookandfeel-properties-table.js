define('jira-look-and-feel/properties-table', [
    'jquery',
    'jira/util/formatter',
    'wrm/context-path',
    'aui/restful-table',
    'jira-look-and-feel/property/templates'
], function(jQuery, formatter, wrmContextPath, RestfulTable, LookandFeelProperty) {
    'use strict';

    function refreshLookAndFeel() {
        var url = formatter.format(wrmContextPath() +
            "/secure/admin/LookAndFeel!refreshResources.jspa?atl_token={0}", atl_token());
        jQuery.get(url);
    }

    return {

        /**
         * Edit/Create view of Application Property row
         *
         * @class LookAndFeelProperty.EditRow
         */
        EditRow: RestfulTable.EditRow.extend({

            initialize: function () {

                // call super
                RestfulTable.EditRow.prototype.initialize.apply(this, arguments);

                this.bind(RestfulTable.Events.RENDER, function () {
                    this.$el.attr("data-row-key", this.model.get("key"));
                    if (this.model.get("type") === "colour") {
                        var $input = this.$el.find("input[name='value']");
                        var $swatch = this.$el.find("span.swatch");
                        var $colorPicker = this.$el.find("div.jira-swatch-colour");
                        /* eslint-disable new-cap */
                        $colorPicker.ColorPicker({
                            color:  this.model.get("value"),
                            onSubmit: function(hsb, hex) {
                                $input.val('#' + hex);
                                $colorPicker.ColorPickerHide();
                            },
                            onBeforeShow: function () {
                                $colorPicker.ColorPickerSetColor($input.val());
                            },
                            onChange: function (hsb, hex) {
                                $input.val('#' + hex);
                                $swatch.css('backgroundColor', '#' + hex);
                            }
                        });
                        $colorPicker.ColorPickerShow();
                        /* eslint-enable new-cap */
                    }
                });

                this.bind(RestfulTable.Events.UPDATED, function () {
                    refreshLookAndFeel();
                });
            },

            submit: function () {
                if (this.model.get("type") === "boolean") {
                    var $check = this.$el.find("input.checkbox");
                    if ($check.attr("checked"))
                    {
                        $check.val("true");
                    } else {
                        $check.val("false");
                    }
                }
                RestfulTable.EditRow.prototype.submit.apply(this, arguments);
            }
        }),

        /**
         * Readonly view of Application Property row
         *
         * @class LookAndFeelProperty.Row
         */
        Row: RestfulTable.Row.extend({

            initialize: function () {

                // call super
                RestfulTable.Row.prototype.initialize.apply(this, arguments);

                this.bind(RestfulTable.Events.RENDER, function () {
                    this.$el.attr("data-row-key", this.model.get("key"));
                });

                // crap work around to handle backbone not extending events
                // (https://github.com/documentcloud/backbone/issues/244)
                this.events["click .application-property-revert"] = "_revert";
                this.delegateEvents();
            },

            _revert: function () {
                this.trigger("focus");

                var defaultValue = this.$el.find(".application-property-value-default").val();
                this.sync({value: defaultValue});
                refreshLookAndFeel();
            },

            renderOperations: function (update, all) {
                return LookandFeelProperty.operations(all);
            }
        }),

        KeyView: RestfulTable.CustomReadView.extend({
            render: function(args) {
                args.name = this.model.get("name");
                args.desc = this.model.get("desc");
                return LookandFeelProperty.key(args);
            }
        }),

        ColourReadView: RestfulTable.CustomReadView.extend({
            render: function(args) {
                args.colourHexValue = this.model.get("value");
                args.defaultColourHexValue = this.model.get("defaultValue");
                return LookandFeelProperty.colourRead(args);
            }
        }),

        ColourEditView: RestfulTable.CustomEditView.extend({
            render: function(args) {
                args.value = this.model.get("value");
                args.defaultValue = this.model.get("defaultValue");
                return LookandFeelProperty.colourEdit(args);
            }
        }),

        DateReadView: RestfulTable.CustomReadView.extend({
            render: function(args) {
                args.value = this.model.get("value");
                args.defaultColourHexValue = this.model.get("defaultValue");
                args.example = this.model.get("example");
                if (this.model.get("type") === "boolean"){
                    return LookandFeelProperty.booleanRead(args);
                } else { // Assume is date format
                    return LookandFeelProperty.dateFormatRead(args);
                }
            }
        }),

        DateEditView: RestfulTable.CustomEditView.extend({
            render: function(args) {
                args.value = this.model.get("value");
                args.defaultValue = this.model.get("defaultValue");
                if (this.model.get("type") === "boolean") {
                    args.key = this.model.get("key");
                    return LookandFeelProperty.booleanEdit(args);
                }
                else { // Assume is date format
                    return LookandFeelProperty.editValue(args);
                }
            }
        })

    };

});
