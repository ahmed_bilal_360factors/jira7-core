define('jira-look-and-feel/edit-look-and-feel', [
    'jquery',
    'jira/util/formatter',
    'aui/message',
    'wrm/context-path'
], function(jQuery, formatter, Messages, wrmContextPath) {

    function initLogoOptions() {
        var $logoFile = jQuery("input#logoFile");
        var $logoURL = jQuery("input#logoUrl");
        $logoFile.change(function() {
            $logoURL.val('');
        });
        $logoURL.change(function() {
            $logoFile.val('');
        });
    }

    function initFaviconOptions() {
        var $faviconFile = jQuery("input#faviconFile");
        var $faviconURL = jQuery("input#faviconUrl");
        $faviconFile.change(function() {
            $faviconURL.val('');
        });
        $faviconURL.change(function() {
            $faviconFile.val('');
        });
    }

    function initAutoColorMessage() {
        var $form = jQuery("#upload-logo-form");
        if (!$form.length) {
            return;
        }

        jQuery.ajax({
            type: "GET",
            url: wrmContextPath() + "/rest/lookandfeel/1.0/auto/justupdated",
            contentType: "application/json",
            success: function(data) {
                if (data.isJustUpdated) {
                    var token = $form.find("input[name='atl_token']").val() || atl_token();
                    var undoAction = wrmContextPath() + "/secure/admin/LookAndFeel!restoreColorScheme.jspa?atl_token=" + token;
                    Messages.info("#upload-logo-form", {
                        body: formatter.I18n.getText("jira.lookandfeel.updatecolors.justupdated", "<a href='" + undoAction + "'>", "</a>"),
                        closeable: false,
                        insert: "prepend"
                    });
                }
            }
        });
    }

    return {
        initEditLookAndFeel: function() {
            initLogoOptions();
            initFaviconOptions();
            initAutoColorMessage();
        }
    };

});
