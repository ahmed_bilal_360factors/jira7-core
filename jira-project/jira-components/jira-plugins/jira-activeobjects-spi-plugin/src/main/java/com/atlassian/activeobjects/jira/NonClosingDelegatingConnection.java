package com.atlassian.activeobjects.jira;

import org.ofbiz.core.entity.jdbc.interceptors.connection.DelegatingConnection;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * A connection to pass out to AO that will never be closed directly.
 * ao calls close() on connections when it really shouldn't, i.e. in the middle of transactions.
 * We always close the connection after a call in the outer transaction to commit() or rollback().
 * <p>
 * This connection wrapper should only ever be used in the scope of an ao transaction.
 *
 * @since v7.0
 */
public class NonClosingDelegatingConnection extends DelegatingConnection {
    public NonClosingDelegatingConnection(final Connection connection) {
        super(connection);
    }

    @Override
    public void close() throws SQLException {
        // No we won't do that.
    }
}
