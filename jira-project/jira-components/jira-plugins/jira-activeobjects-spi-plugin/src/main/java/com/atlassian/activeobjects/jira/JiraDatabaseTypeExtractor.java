package com.atlassian.activeobjects.jira;

import org.ofbiz.core.entity.jdbc.dbtype.DatabaseType;

import java.sql.Connection;

/**
 * @since 7.0 moved from activeobjects 0.28.1
 */
public interface JiraDatabaseTypeExtractor {
    public DatabaseType getDatabaseType(Connection connection);
}
