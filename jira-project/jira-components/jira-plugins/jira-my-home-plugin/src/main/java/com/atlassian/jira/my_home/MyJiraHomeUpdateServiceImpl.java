package com.atlassian.jira.my_home;

import com.atlassian.jira.plugin.myjirahome.MyJiraHomeUpdateException;
import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nonnull;

import static com.google.common.base.Strings.isNullOrEmpty;

/**
 * Applies validation before storing the new plugin module key.
 */
public class MyJiraHomeUpdateServiceImpl implements MyJiraHomeUpdateService {
    private final MyJiraHomeValidator validator;
    private final MyJiraHomeStorage storage;

    public MyJiraHomeUpdateServiceImpl(@Nonnull final MyJiraHomeValidator validator, @Nonnull final MyJiraHomeStorage storage) {
        this.validator = validator;
        this.storage = storage;
    }

    @Override
    public void updateHome(@Nonnull final ApplicationUser user, @Nonnull final String completePluginModuleKey) {
        if (!isNullOrEmpty(completePluginModuleKey.trim()) && validator.isInvalid(completePluginModuleKey)) {
            throw new MyJiraHomeUpdateException("The plugin module key is not usable. Please double check the URL query string and make sure it contains a valid Atlassian plugin module key.");
        }

        storage.store(user, completePluginModuleKey);
    }
}
