package com.atlassian.jira.plugin.corereports.filters;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.web.servlet.api.ForwardAuthorizer;
import java.net.URI;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Component;

import static java.util.Optional.of;

@ExportAsService
@Component
public class ReportKeyRewriteAuthorizer implements ForwardAuthorizer {

    private static final Pattern CONFIGURE_OLD_REPORT_PATTERN = Pattern.compile(
            Pattern.quote("/secure/ConfigureReport")
                    + ".*"
                    + Pattern.quote("reportKey=com.atlassian.jira.plugin.system.reports%3A")
    );

    @Override
    public Optional<Boolean> authorizeForward(HttpServletRequest source, URI target) {
        // We filter on identity at the end of this stream to make sure we always return Optional.empty() instead of
        // Optional.of(false). A collection of these authorizers will be checked by JIRA in a chain. If _any_ are
        // false, then the forward is denied. We want to return empty() to denote that _we_ don't need to forward, but
        // someone else might.
        return of(target)
                .map(URI::toString)
                .map(CONFIGURE_OLD_REPORT_PATTERN::matcher)
                .map(Matcher::find)
                .filter(authorized -> authorized);
    }

}
