package com.atlassian.jira.plugin.corereports;

import com.atlassian.jira.issue.statistics.ProjectStatisticsManager;
import com.atlassian.jira.portal.PortletConfigurationManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

@SuppressWarnings("unused")
public class ComponentImports {
    //We just need the package of this class so we get the OSGI import for various report value generators
    @ComponentImport
    private ProjectStatisticsManager projectStatisticsManager;

    @ComponentImport
    private PortletConfigurationManager portletConfigurationManager;
}
