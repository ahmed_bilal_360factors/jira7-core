package com.atlassian.jira.plugin.corereports.fieldtype;

import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.osgi.external.ListableModuleDescriptorFactory;
import com.atlassian.plugin.osgi.external.SingleModuleDescriptorFactory;
import com.atlassian.plugin.spring.scanner.annotation.export.ModuleType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@ModuleType (ListableModuleDescriptorFactory.class)
@Component
public class FieldTypeModuleTypeFactory extends SingleModuleDescriptorFactory<FieldTypeModuleDescriptor> {
    @Autowired
    public FieldTypeModuleTypeFactory(HostContainer hostContainer) {
        super(hostContainer, "report-field", FieldTypeModuleDescriptor.class);
    }
}
