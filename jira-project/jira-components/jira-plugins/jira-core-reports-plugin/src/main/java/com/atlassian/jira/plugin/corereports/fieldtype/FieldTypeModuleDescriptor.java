package com.atlassian.jira.plugin.corereports.fieldtype;

import com.atlassian.jira.plugin.AbstractJiraModuleDescriptor;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

public class FieldTypeModuleDescriptor extends AbstractJiraModuleDescriptor<ReportFieldType> {

    private final ReportFieldType module = new ReportFieldType();

    public FieldTypeModuleDescriptor(@ComponentImport JiraAuthenticationContext authenticationContext, @ComponentImport ModuleFactory moduleFactory) {
        super(authenticationContext, moduleFactory);
    }

    public ReportFieldType getModule() {
        return module;
    }
}
