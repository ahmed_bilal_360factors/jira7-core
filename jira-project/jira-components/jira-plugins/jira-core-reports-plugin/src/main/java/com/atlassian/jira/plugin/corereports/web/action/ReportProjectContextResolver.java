package com.atlassian.jira.plugin.corereports.web.action;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.configurable.ObjectConfiguration;
import com.atlassian.configurable.ObjectConfigurationException;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.plugin.report.ReportModuleDescriptor;
import com.atlassian.jira.project.Project;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.collect.Iterables;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.Optional.of;
import static java.util.Optional.ofNullable;

/**
 * Given a Report and a request to render or configure a report, return the project context for the report (if any
 * exists). This helps us figure out which sidebar we should show on a report page (sometimes a different project can be
 * encoded in a URL than what is present in the session).
 */
@Named
class ReportProjectContextResolver {

    private static final Logger LOG = LoggerFactory.getLogger(ReportProjectContextResolver.class);

    // All reports should have a 'selectedProjectId' URL parameter which contains the id of the project that the report
    // was created from (we bake it into the ConfigureReport form).
    private static final String PARAMETER_SELECTED_PROJECT_ID = "selectedProjectId";

    // We have one pluggable field type for reports ('filterprojectpicker') that allows users to specify a project for a
    // report. When a value from one of these field types is present, we want to check the value so that we can update
    // the project context if necessary.
    private static final String FIELD_TYPE_FILTER_OR_PROJECT_PICKER = "filterprojectpicker";

    // Values for 'filterprojectpicker' fields are in the form 'filter-10100' or 'project-10100' (for some project or
    // filter id). We need to parse the project id out of these parameters.
    private static final Pattern PATTERN_MATCH_SELECTED_PROJECT = Pattern.compile("project-([0-9]+)");
    private static final int PATTERN_PROJECT_ID_GROUP_INDEX = 1;

    private final ProjectService projectService;

    @Inject
    public ReportProjectContextResolver(@ComponentImport ProjectService projectService) {
        this.projectService = projectService;
    }

    public Optional<Project> getProjectContext(ReportModuleDescriptor report, HttpServletRequest request) {
        List<Long> projectIdsInRequest = getProjectContextParameterValues(report, request).stream()
                .map(this::matchProjectIdFromParameterValue)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(CollectorsUtil.toImmutableList());

        // We can only reliably resolve a project context from the URL if there's only one option for the project.
        if (projectIdsInRequest.size() == 1) {
            return ofNullable(Iterables.getFirst(projectIdsInRequest, null)).flatMap(this::getProjectById);
        } else {
            return getDefaultSelectedProjectId(request).flatMap(this::getProjectById);
        }
    }

    private Optional<Project> getProjectById(Long projectId) {
        return ofNullable(projectService.getProjectById(projectId).getProject());
    }

    private Optional<Long> getDefaultSelectedProjectId(HttpServletRequest request) {
        return ofNullable(request.getParameter(PARAMETER_SELECTED_PROJECT_ID)).map(Long::valueOf);
    }

    private Set<String> getProjectContextParameterValues(ReportModuleDescriptor report, HttpServletRequest request) {
        try {
            ObjectConfiguration configuration = getEmptyReportObjectConfiguration(report);
            Set<String> projectContextParameters = new HashSet<>();

            for (String fieldKey : configuration.getFieldKeys()) {
                String fieldType = configuration.getFieldTypeName(fieldKey);
                if (isProjectCentricFieldType(fieldType)) {
                    String param = request.getParameter(fieldKey);
                    if (param != null) {
                        projectContextParameters.add(param);
                    }
                }
            }

            return projectContextParameters;
        } catch (ObjectConfigurationException e) {
            // There's something wrong with the object configuration, which has to be handled somewhere else.
            // Regardless, we don't have a project to use so we're returning empty.
            LOG.warn("Invalid ObjectConfiguration, can't resolve project", e);
            return Collections.emptySet();
        }
    }

    private ObjectConfiguration getEmptyReportObjectConfiguration(ReportModuleDescriptor report) throws ObjectConfigurationException {
        return report.getObjectConfiguration(Collections.emptyMap());
    }

    private Optional<Long> matchProjectIdFromParameterValue(String value) {
        return of(value).map(PATTERN_MATCH_SELECTED_PROJECT::matcher)
                .filter(Matcher::find)
                .map(matcher -> matcher.group(PATTERN_PROJECT_ID_GROUP_INDEX))
                .map(Long::valueOf);
    }

    /**
     * Some field types on configure report pages can contain a value that represents a project context (e.g. a project
     * picker). These can yield values in the form "project-10100" to denote a selected project with id 10100. We need
     * to check what values are in this field so that we know what project-sidebar we should display on the report
     * result page.
     *
     * @param fieldTypeName The "type" of the field to check (i.e. is it a "checkbox" or a "multiselect" field.
     * @return True if this field type can have a value that represents a project.
     */
    private boolean isProjectCentricFieldType(String fieldTypeName) {
        return fieldTypeName.equals(FIELD_TYPE_FILTER_OR_PROJECT_PICKER);
    }

}
