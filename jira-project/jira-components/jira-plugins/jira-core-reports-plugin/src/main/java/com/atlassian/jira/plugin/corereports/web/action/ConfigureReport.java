package com.atlassian.jira.plugin.corereports.web.action;

import com.atlassian.configurable.ObjectConfiguration;
import com.atlassian.configurable.ObjectConfigurationException;
import com.atlassian.configurable.ObjectConfigurationTypes;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.event.web.action.browser.ReportConfiguredEvent;
import com.atlassian.jira.event.web.action.browser.ReportViewedEvent;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.plugin.corereports.fieldtype.FieldTypeModuleDescriptor;
import com.atlassian.jira.plugin.report.Report;
import com.atlassian.jira.plugin.report.ReportModuleDescriptor;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.projects.api.sidebar.ProjectSidebarRenderer;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.util.JiraUrlCodec;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.web.action.ActionViewData;
import com.atlassian.jira.web.action.ProjectActionSupport;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.seraph.util.RedirectUtils;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import webwork.action.ActionContext;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.lang.String.format;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;

@Named
public class ConfigureReport extends ProjectActionSupport {
    private static final String CONFIGURE_REPORTS_CONTEXT = "com.atlassian.jira.jira-core-reports-plugin.configure-report";
    private static final String REPORT_RESULT_CONTEXT = "com.atlassian.jira.jira-core-reports-plugin.report-result";

    private final PluginAccessor pluginAccessor;
    private final EventPublisher eventPublisher;
    private final PageBuilderService page;
    private final SoyTemplateRenderer soy;
    private final ApplicationProperties appProperties;
    private final ProjectSidebarRenderer projectSidebarRenderer;
    private final ReportProjectContextResolver reportResultProjectContextResolver;

    private String reportKey;
    private ReportModuleDescriptor descriptor;
    private ObjectConfiguration oc;
    private String generatedReport;
    private Report report;

    /**
     * @param projectManager
     * @param permissionManager
     * @param pluginAccessor
     * @param eventPublisher
     * @since 5.2
     */
    @Inject
    public ConfigureReport(
            @ComponentImport final ProjectManager projectManager,
            @ComponentImport final PermissionManager permissionManager,
            @ComponentImport final PluginAccessor pluginAccessor,
            @ComponentImport final EventPublisher eventPublisher,
            @ComponentImport final PageBuilderService page,
            @ComponentImport final SoyTemplateRenderer soy,
            @ComponentImport final ApplicationProperties appProperties,
            @ComponentImport final ProjectSidebarRenderer projectSidebarRenderer,
            final ReportProjectContextResolver reportResultProjectContextResolver) {
        super(projectManager, permissionManager);
        this.pluginAccessor = pluginAccessor;
        this.eventPublisher = eventPublisher;
        this.page = page;
        this.soy = soy;
        this.appProperties = appProperties;
        this.projectSidebarRenderer = projectSidebarRenderer;
        this.reportResultProjectContextResolver = reportResultProjectContextResolver;
    }

    public String getParamValue(final String key) {
        final Map<String, Object> inputParams = makeReportParams();
        String value = (String) inputParams.get(key);
        if (value == null) {
            try {
                value = getObjectConfiguration().getFieldDefault(key);
            } catch (ObjectConfigurationException objectConfigurationException) {
                if (log.isDebugEnabled()) {
                    log.debug(
                            format("The configuration property with the key: %s could not be found for the "
                                    + "report module descriptor with the key: %s", key, reportKey),
                            objectConfigurationException
                    );
                }
            }
        }
        return value;
    }

    @ActionViewData(key = "sidebarHtml")
    public String getSidebarHtml() {
        return projectSidebarRenderer.render(getSelectedProject(), "com.atlassian.jira.jira-projects-plugin:report-page");
    }

    public List<String> getParamValues(String key) {
        final Map<String, Object> inputParams = makeReportParams();
        Object values = inputParams.get(key);
        if (values == null) {
            try {
                values = getObjectConfiguration().getFieldDefault(key);
            } catch (ObjectConfigurationException objectConfigurationException) {
                if (log.isDebugEnabled()) {
                    log.debug(
                            format("The configuration property with the key: %s could not be found for the "
                                    + "report module descriptor with the key: %s", key, reportKey),
                            objectConfigurationException
                    );
                }
                return emptyList();
            }
        } else if (values instanceof String[]) {
            return asList((String[]) values);
        }

        return singletonList(values.toString());
    }

    public String doDefault() throws Exception {
        //JRA-13939: Need to be null safe here, as some crawlers may be too stupid to submit a &amp; in a URL and will
        //submit an invalid reportKey param
        if (!validReport()) {
            return "noreporterror";
        }
        eventPublisher.publish(new ReportConfiguredEvent(getReportKey()));

        page.assembler().resources().requireContext(CONFIGURE_REPORTS_CONTEXT);

        return super.doDefault();
    }

    protected String doExecute() throws Exception {
        //JRA-13939: Need to be null safe here, as some crawlers may be too stupid to submit a &amp; in a URL and will
        //submit an invalid reportKey param
        if (!validReport()) {
            return "noreporterror";
        }
        getReportModule().validate(this, makeReportParams());
        if (getReasons().contains(Reason.NOT_LOGGED_IN)) {
            return forceRedirect(RedirectUtils.getLoginUrl(request));
        }
        if (invalidInput()) {
            return INPUT;
        }
        generatedReport = getReportModule().generateReportHtml(this, makeReportParams());
        eventPublisher.publish(new ReportViewedEvent(getReportKey()));

        page.assembler().resources().requireContext(REPORT_RESULT_CONTEXT);

        return SUCCESS;
    }

    @Override
    public Project getSelectedProject() {
        // Try to resolve the project from the report URL. If we don't find anything, fallback to the super
        // implementation (which looks up a project from the session). We need the fallback, or we risk breaking
        // existing reports (our TimeTracking report will break, as well as any number of third-party reports who rely
        // on a project always being present).
        return reportResultProjectContextResolver.getProjectContext(getReport(), getHttpRequest())
                .orElse(super.getSelectedProject());
    }

    //JRA-13939: Need to be null safe here, as some crawlers may be too stupid to submit a &amp; in a URL and will
    //submit an invalid reportKey param
    private boolean validReport() {
        if (StringUtils.isEmpty(getReportKey())) {
            addErrorMessage(getText("report.configure.error.no.report.key"));
            return false;
        }

        if (getReport() == null || !getReportModule().showReport()) {
            addErrorMessage(getText("report.configure.error.no.report", getReportKey()));
            return false;
        }

        return true;
    }

    public String doExcelView() throws Exception {
        getHttpResponse().setHeader("Cache-Control", "private, must-revalidate, max-age=5"); // http 1.1
        getHttpResponse().setHeader("Pragma", ""); // http 1.0
        getHttpResponse().setDateHeader("Expires", System.currentTimeMillis() + 300); // prevent proxy caching
        getHttpResponse().setContentType("application/vnd.ms-excel;charset=" + getEncoding());
        generatedReport = getReportModule().generateReportExcel(this, makeReportParams());

        return "excel";
    }

    /**
     * Makes report params from action params.
     *
     * @return a map of report parameters
     */
    private Map<String, Object> makeReportParams() {
        @SuppressWarnings({"unchecked"})
        Map<String, String[]> params = ActionContext.getParameters();
        Map<String, Object> reportParams = new LinkedHashMap<String, Object>(params.size());

        for (final Map.Entry entry : params.entrySet()) {
            final String key = (String) entry.getKey();
            if (((String[]) entry.getValue()).length == 1) {
                reportParams.put(key, ((String[]) entry.getValue())[0]);
            } else {
                reportParams.put(key, entry.getValue());
            }
        }
        return reportParams;
    }

    @ActionViewData(key = "queryString")
    public String getQueryString() {
        final Map<String, Object> params = makeReportParams();
        StringBuilder stringBuilder = new StringBuilder();
        boolean isFirstKey = true;

        for (final String key : params.keySet()) {
            Object value = params.get(key);
            if (value instanceof String) {
                isFirstKey = appendUrlParameter(isFirstKey, key, (String) value, stringBuilder);
            } else if (value instanceof String[]) {
                for (int i = 0; i < ((String[]) value).length; i++) {
                    String s = ((String[]) value)[i];
                    isFirstKey = appendUrlParameter(isFirstKey, key, s, stringBuilder);
                }
            }
        }

        return stringBuilder.toString();
    }

    @ActionViewData(key = "encoding")
    public String getEncoding() {
        return appProperties.getEncoding();
    }

    private boolean appendUrlParameter(final boolean firstKey, String key, String value, StringBuilder stringBuilder) {
        if (firstKey) {
            stringBuilder.append(encode(key)).append('=').append(encode(value));
        } else {
            stringBuilder.append('&').append(encode(key)).append('=').append(encode(value));
        }
        return false;
    }

    private String encode(final String key) {
        return JiraUrlCodec.encode(key);
    }

    private Report getReportModule() {
        if (report == null) {
            report = getReport().getModule();
        }

        return report;
    }

    @ActionViewData(key = "generatedReportContent")
    public String getGeneratedReport() {
        return generatedReport;
    }

    @ActionViewData(key = "reportKey")
    public String getReportKey() {
        return reportKey;
    }

    public void setReportKey(String reportKey) {
        this.reportKey = reportKey;
    }

    @ActionViewData(key = "report")
    public ReportModuleDescriptor getReport() {
        if (descriptor == null) {
            descriptor = (ReportModuleDescriptor) pluginAccessor.getEnabledPluginModule(reportKey);
        }

        return descriptor;
    }

    @Override
    @ActionViewData(key = "errorMessages")
    public Collection<String> getFlushedErrorMessages() {
        return super.getFlushedErrorMessages();
    }

    @Override
    @ActionViewData(key = "selectedProjectId")
    public Long getSelectedProjectId() {
        return super.getSelectedProjectId();
    }

    @ActionViewData(key = "atlToken")
    public String getAtlToken() {
        return getXsrfToken();
    }

    @ActionViewData(key = "objectConfiguration")
    public ObjectConfiguration getObjectConfiguration() throws ObjectConfigurationException {
        if (oc == null) {
            final Map objectConfigurationParameters =
                    MapBuilder.build
                            (
                                    "project", getSelectedProject() == null ? null : getSelectedProject().getGenericValue(),
                                    "User", getLoggedInUser()
                            );
            oc = getReport().getObjectConfiguration(objectConfigurationParameters);
        }

        return oc;
    }

    @ActionViewData(key = "objectConfigurationFields")
    public Collection<? extends ObjectConfigurationField> getObjectConfigurationFields() throws ObjectConfigurationException {
        ObjectConfiguration objectConfiguration = getObjectConfiguration();
        return Stream.of(objectConfiguration.getFieldKeys())
                .map(key -> new ObjectConfigurationField(key, objectConfiguration))
                .collect(toList());
    }

    @ActionViewData(key = "loginAdvisable")
    public boolean getLoginAdvisable() {
        return (this.getLoggedInApplicationUser() == null && this.hasAnyErrors());
    }

    @ActionViewData(key = "loginAdviceMessage")
    public String getLoginAdviceMessage() {
        StringBuilder loginLink = new StringBuilder("<a rel=\"nofollow\" href=\"");
        loginLink.append(RedirectUtils.getLinkLoginURL(request));
        loginLink.append("\">");
        return this.getText("report.loginadvised", loginLink.toString(), "</a>");
    }

    @ActionViewData(key = "showSaveButton")
    public boolean isShowSaveButton() throws ObjectConfigurationException {
        //If any select field has no values then disable save button
        for (ObjectConfigurationField field : getObjectConfigurationFields()) {
            if (field.getType() == ObjectConfigurationTypes.SELECT && field.getValues().isEmpty())
                return false;
        }

        //Otherwise just use the request attribute
        return !Boolean.FALSE.equals(getHttpRequest().getAttribute("jira.portletform.showsavebutton"));
    }

    public String getFilterProjectName(String key) {
        // This is currently hard coded to pull from the URL parameters. Previously getFilterProjectName didn't exist.
        return this.getParamValue("projectOrFilterName");
    }

    public String getFilterName(String key) {
        // This is currently hard coded to pull from the URL parameters
        return this.getParamValue("filterName");
    }

    private static <T> List<? extends T> mapAndConvertToList(Map<?, ?> fieldValues, Function<Map.Entry<?, ?>, T> mapper) {
        return fieldValues.entrySet().stream()
                .map(mapper)
                .collect(toList());
    }

    public class ObjectConfigurationField {
        private final ObjectConfiguration objectConfiguration;
        private final String fieldKey;

        public ObjectConfigurationField(String fieldKey, ObjectConfiguration objectConfiguration) {
            this.fieldKey = fieldKey;
            this.objectConfiguration = objectConfiguration;
        }

        public boolean isEnabled() {
            return objectConfiguration.isEnabled(fieldKey);
        }

        public String getName() throws ObjectConfigurationException {
            return objectConfiguration.getFieldName(fieldKey);
        }

        public String getKey() {
            return fieldKey;
        }

        @SuppressWarnings("unused")
        public String getParamValue() {
            return ConfigureReport.this.getParamValue(fieldKey);
        }

        @SuppressWarnings("unused")
        public List<String> getParamValues() {
            return ConfigureReport.this.getParamValues(fieldKey);
        }

        @SuppressWarnings("unused")
        public String getDescription() throws ObjectConfigurationException {
            return objectConfiguration.getFieldDescription(fieldKey);
        }

        @SuppressWarnings("unused")
        public String getDefaultValue() throws ObjectConfigurationException {
            return objectConfiguration.getFieldDefault(fieldKey);
        }

        @SuppressWarnings("unused")
        public String getFilterProjectName() {
            return ConfigureReport.this.getFilterProjectName(fieldKey);
        }

        @SuppressWarnings("unused")
        public String getFilterName() {
            return ConfigureReport.this.getFilterName(fieldKey);
        }

        public List<? extends FieldOption> getValues() throws ObjectConfigurationException {
            return mapAndConvertToList(objectConfiguration.getFieldValues(fieldKey), this::toFieldOption);
        }

        @SuppressWarnings("unused")
        public List<? extends FieldOption> getValuesHtmlEncoded() throws ObjectConfigurationException {
            return mapAndConvertToList(
                    objectConfiguration.getFieldValuesHtmlEncoded(fieldKey),
                    this::toFieldOption);
        }

        @SuppressWarnings("unused")
        public List<? extends SelectableFieldOption> getSelectableValuesHtmlEncoded() throws ObjectConfigurationException {
            return mapAndConvertToList(
                    objectConfiguration.getFieldValuesHtmlEncoded(fieldKey),
                    toSelectableFieldOption());
        }

        private FieldOption toFieldOption(Map.Entry<?,?> entry) {
            return new FieldOption(entry.getKey().toString(), entry.getValue().toString());
        }

        private Function<Map.Entry<?, ?>, SelectableFieldOption> toSelectableFieldOption() {
            HashSet<String> paramValues = new HashSet<>(getParamValues());
            return entry -> {
                String value = entry.getValue().toString();
                String key = entry.getKey().toString();
                return new SelectableFieldOption(key, value, paramValues.contains(key));
            };
        }

        @SuppressWarnings("unused")
        public boolean getUserPickerPermission() {
            return hasGlobalPermission(GlobalPermissionKey.USER_PICKER);
        }

        public int getType() throws ObjectConfigurationException {
            return objectConfiguration.getFieldType(fieldKey);
        }

        public String getTypeName() throws ObjectConfigurationException {
            return objectConfiguration.getFieldTypeName(fieldKey);
        }

        @SuppressWarnings("unused")
        public String getErrorMessage() {
            return getErrors().get(fieldKey);
        }

        @SuppressWarnings("unused")
        public String getConfigurationHtml() throws ObjectConfigurationException {

            FieldTypeModuleDescriptor descriptor = findFieldType();
            if (descriptor == null) {
                return null;
            }

            return descriptor.getHtml("view", Collections.singletonMap("field", this));
        }

        @SuppressWarnings("unused")
        public Collection<DatePickerParameter> getDatePickerParameters() {
            Calendar calendar = Calendar.getInstance(getLocale());
            Date now = DateTime.now().toDate();
            String startingTimestamp = getDmyDateFormatter().format(now);
            boolean useISO8601WeekNumbers = appProperties.getOption(APKeys.JIRA_DATE_TIME_PICKER_USE_ISO8601);

            return ImmutableList.of(
                    new DatePickerParameter("firstDay", Integer.toString(calendar.getFirstDayOfWeek() - 1)),
                    new DatePickerParameter("date", startingTimestamp),
                    new DatePickerParameter("todayDate", startingTimestamp),
                    new DatePickerParameter("useISO8601WeekNumbers", Boolean.toString(useISO8601WeekNumbers)),
                    new DatePickerParameter("ifFormat", getDateFormat())
            );
        }

        private FieldTypeModuleDescriptor findFieldType() throws ObjectConfigurationException {
            List<? extends FieldTypeModuleDescriptor> fieldTypes = pluginAccessor.getEnabledModuleDescriptorsByClass(FieldTypeModuleDescriptor.class);
            String fieldTypeKey = getTypeName();

            if (fieldTypeKey == null)
                return null;

            //It's a linear search, but there won't be that many of these so it's probably OK and not worth overhead and
            //maintenance of maintaining a cache of them
            for (FieldTypeModuleDescriptor fieldType : fieldTypes) {
                if (fieldTypeKey.equals(fieldType.getKey()))
                    return fieldType;
            }

            return null;
        }
    }

    public class FieldOption {
        private final String value;
        private final String text;

        public FieldOption(String value, String text) {
            this.text = text;
            this.value = value;
        }

        public String getText() {
            return ConfigureReport.this.getText(text);
        }

        public String getValue() {
            return value;
        }
    }

    public class SelectableFieldOption extends FieldOption {
        private final boolean selected;

        public SelectableFieldOption(String value, String text, boolean selected) {
            super(value, text);
            this.selected = selected;
        }

        @SuppressWarnings("unused")
        public boolean getSelected() {
            return selected;
        }
    }

    /**
     * A simple key/value holder for passing configurable options into a date field (e.g. what day to start on, what
     * format to use, etc.).
     */
    public static final class DatePickerParameter {
        private final String key;
        private final String value;

        public DatePickerParameter(String key, String value) {
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public String getValue() {
            return value;
        }
    }
}
