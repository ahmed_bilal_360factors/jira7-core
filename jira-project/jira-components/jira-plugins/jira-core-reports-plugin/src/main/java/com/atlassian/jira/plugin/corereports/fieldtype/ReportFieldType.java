package com.atlassian.jira.plugin.corereports.fieldtype;

/**
 * Empty class required by our report-field module descriptor (it needs a type parameter).
 */
public class ReportFieldType {

}
