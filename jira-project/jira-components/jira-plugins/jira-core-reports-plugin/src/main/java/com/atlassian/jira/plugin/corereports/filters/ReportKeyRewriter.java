package com.atlassian.jira.plugin.corereports.filters;

import com.atlassian.core.filters.AbstractHttpFilter;
import com.atlassian.uri.UriBuilder;
import java.io.IOException;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.inject.Named;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static java.util.Optional.ofNullable;

/**
 * You can specify a report key as a url parameter. Reports have moved from inside core JIRA, to a P2 plugin (hence why
 * the report's paths have changed). We don't expect any of our stuff to still link to the old paths, but users might
 * have old paths bookmarked.
 * <p>
 * This filter exists primarily to make sure those bookmarks still work.
 */
@Named
public class ReportKeyRewriter extends AbstractHttpFilter {

    private static final Pattern OLD_REPORT_PREFIX = Pattern.compile(
            "^" + Pattern.quote("com.atlassian.jira.plugin.system.reports:")
    );

    private static final String NEW_REPORT_PREFIX = "com.atlassian.jira.jira-core-reports-plugin:";

    private static final String REPORT_KEY_PARAMETER = "reportKey";

    @Override
    protected void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {

        Optional<String> redirectLocation = ofNullable(request.getParameter(REPORT_KEY_PARAMETER))
                .map(OLD_REPORT_PREFIX::matcher)
                .filter(Matcher::find)
                .map(matcher -> updateRequestParameterWithNewReportKey(request, matcher));

        if (redirectLocation.isPresent()) {
            // Can't just map here because #sendRedirect throws around checked exceptions.
            response.sendRedirect(redirectLocation.get());
        } else {
            filterChain.doFilter(request, response);
        }
    }

    private String updateRequestParameterWithNewReportKey(HttpServletRequest request, Matcher oldReportKeyMatcher) {
        return new UriBuilder(request)
                .putQueryParameter(REPORT_KEY_PARAMETER, oldReportKeyMatcher.replaceAll(NEW_REPORT_PREFIX))
                .toString();
    }

}
