package com.atlassian.jira.plugin.corereports.report.impl;

import com.atlassian.jira.issue.util.AggregateTimeTrackingCalculatorFactory;
import com.atlassian.jira.issue.util.AggregateTimeTrackingCalculatorFactoryImpl;
import com.atlassian.jira.mock.MockPermissionManager;
import com.atlassian.jira.mock.security.MockAuthenticationContext;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;

/**
 * Handy for tests.
 */
class CalculatorFactory {
    static AggregateTimeTrackingCalculatorFactory get(String userName) {
        ApplicationUser user = new MockApplicationUser(userName);
        JiraAuthenticationContext authContext = new MockAuthenticationContext(user);
        PermissionManager perms = new MockPermissionManager(true);
        return new AggregateTimeTrackingCalculatorFactoryImpl(authContext, null, perms);
    }
}