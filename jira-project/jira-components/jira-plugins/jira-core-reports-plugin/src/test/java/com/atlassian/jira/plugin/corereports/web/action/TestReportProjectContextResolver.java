package com.atlassian.jira.plugin.corereports.web.action;

import com.atlassian.configurable.ObjectConfiguration;
import com.atlassian.configurable.ObjectConfigurationException;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.mock.servlet.MockHttpServletRequest;
import com.atlassian.jira.plugin.report.ReportModuleDescriptor;
import com.atlassian.jira.project.Project;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestReportProjectContextResolver {

    private static final String PARAMETER_SELECTED_PROJECT_ID = "selectedProjectId";

    @Mock
    private ProjectService projectService;

    private MockReportModuleDescriptorBuilder reportModuleDescriptorBuilder;

    private ReportProjectContextResolver reportResultProjectContextResolver;

    @Before
    public void setUp() {
        reportModuleDescriptorBuilder = new MockReportModuleDescriptorBuilder();
        reportResultProjectContextResolver = new ReportProjectContextResolver(projectService);
    }

    @Test
    public void noAdditionalProjectCentricFields() throws ObjectConfigurationException {
        ReportModuleDescriptor report = reportModuleDescriptorBuilder.build();
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter(PARAMETER_SELECTED_PROJECT_ID, "10000");
        Project project = withMockProject(10000L);

        Optional<Project> context = reportResultProjectContextResolver.getProjectContext(report, request);

        assertThat(context, is(Optional.of(project)));
    }

    @Test
    public void singleProjectCentricField() throws ObjectConfigurationException {
        ReportModuleDescriptor report = reportModuleDescriptorBuilder.withFilterOrProjectPickerField("filterOrProject").build();
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter(PARAMETER_SELECTED_PROJECT_ID, "10000");
        request.setParameter("filterOrProject", "project-10100");
        Project project = withMockProject(10100L);

        Optional<Project> context = reportResultProjectContextResolver.getProjectContext(report, request);

        assertThat(context, is(Optional.of(project)));
    }

    @Test
    public void projectCentricFieldWithFilter() throws ObjectConfigurationException {
        ReportModuleDescriptor report = reportModuleDescriptorBuilder.withFilterOrProjectPickerField("filterOrProject")
                .withFilterPickerField("filter")
                .build();
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter(PARAMETER_SELECTED_PROJECT_ID, "10000");
        request.setParameter("filterOrProject", "project-10100");
        request.setParameter("filter", "filter-20200");
        Project project = withMockProject(10100L);

        Optional<Project> context = reportResultProjectContextResolver.getProjectContext(report, request);

        assertThat(context, is(Optional.of(project)));
    }

    @Test
    public void multipleProjectCentricFields() throws ObjectConfigurationException {
        ReportModuleDescriptor report = reportModuleDescriptorBuilder.withFilterOrProjectPickerField("filterOrProjectA")
                .withFilterOrProjectPickerField("filterOrProjectB")
                .build();
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter(PARAMETER_SELECTED_PROJECT_ID, "10000");
        request.setParameter("filterOrProjectA", "project-10100");
        request.setParameter("filterOrProjectB", "project-20200");
        Project project = withMockProject(10000L);

        Optional<Project> context = reportResultProjectContextResolver.getProjectContext(report, request);

        assertThat(context, is(Optional.of(project)));
    }

    @Test
    public void nonProjectCentricFields() throws ObjectConfigurationException {
        ReportModuleDescriptor report = reportModuleDescriptorBuilder.withLongField("aLong")
                .withStringField("aString")
                .withFilterPickerField("filterPicker")
                .withFilterOrProjectPickerField("filterOrProjectPickerA")
                .withFilterOrProjectPickerField("filterOrProjectPickerB")
                .build();
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter(PARAMETER_SELECTED_PROJECT_ID, "10000");
        request.setParameter("aLong", "1");
        request.setParameter("aString", "test");
        request.setParameter("filterPicker", "filter-20000");
        request.setParameter("filterOrProjectPickerA", "project-10100");
        request.setParameter("filterOrProjectPickerB", "filter-30000");
        Project project = withMockProject(10100L);

        Optional<Project> context = reportResultProjectContextResolver.getProjectContext(report, request);

        assertThat(context, is(Optional.of(project)));
    }

    @Test
    public void projectCentricFieldWithNoResolution() throws ObjectConfigurationException {
        ReportModuleDescriptor report = reportModuleDescriptorBuilder.withFilterOrProjectPickerField("filterOrProject")
                .build();
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter(PARAMETER_SELECTED_PROJECT_ID, "10000");
        request.setParameter("filterOrProject", "project-10100");
        withNullProject(10100L);

        Optional<Project> context = reportResultProjectContextResolver.getProjectContext(report, request);

        assertThat(context, is(Optional.empty()));
    }

    @Test
    public void noDefaultSelectedProject() throws ObjectConfigurationException {
        ReportModuleDescriptor report = reportModuleDescriptorBuilder.build();
        MockHttpServletRequest request = new MockHttpServletRequest();

        Optional<Project> context = reportResultProjectContextResolver.getProjectContext(report, request);

        assertThat(context, is(Optional.empty()));
    }

    @Test
    public void unresolvedDefaultSelectedProject() throws ObjectConfigurationException {
        ReportModuleDescriptor report = reportModuleDescriptorBuilder.build();
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter(PARAMETER_SELECTED_PROJECT_ID, "10000");
        withNullProject(10000L);

        Optional<Project> context = reportResultProjectContextResolver.getProjectContext(report, request);

        assertThat(context, is(Optional.empty()));
    }

    @Test
    public void invalidObjectConfigurationReturnsEmpty() throws ObjectConfigurationException {
        ReportModuleDescriptor report = mock(ReportModuleDescriptor.class);
        when(report.getObjectConfiguration(any(Map.class))).thenThrow(new ObjectConfigurationException("test"));
        MockHttpServletRequest request = new MockHttpServletRequest();

        Optional<Project> context = reportResultProjectContextResolver.getProjectContext(report, request);

        assertThat(context, is(Optional.empty()));
    }

    private Project withMockProject(Long projectId) {
        Project project = mock(Project.class);
        ProjectService.GetProjectResult result = mock(ProjectService.GetProjectResult.class);

        when(result.getProject()).thenReturn(project);
        when(projectService.getProjectById(projectId)).thenReturn(result);

        return project;
    }

    private void withNullProject(Long projectId) {
        ProjectService.GetProjectResult result = mock(ProjectService.GetProjectResult.class);
        when(result.getProject()).thenReturn(null);
        when(projectService.getProjectById(projectId)).thenReturn(result);
    }

    private class MockReportModuleDescriptorBuilder {
        private static final String FIELD_TYPE_FILTER = "filterpicker";
        private static final String FIELD_TYPE_FILTER_OR_PROJECT = "filterprojectpicker";
        private static final String FIELD_TYPE_STRING = "string";
        private static final String FIELD_TYPE_LONG = "long";

        private ReportModuleDescriptor reportModuleDescriptor;
        private ObjectConfiguration objectConfiguration;
        private List<String> fieldNames;

        public MockReportModuleDescriptorBuilder() {
            reportModuleDescriptor = mock(ReportModuleDescriptor.class);
            objectConfiguration = mock(ObjectConfiguration.class);
            fieldNames = new LinkedList<>();
        }

        public ReportModuleDescriptor build() throws ObjectConfigurationException {
            String[] fieldNamesArray = new String[fieldNames.size()];
            when(objectConfiguration.getFieldKeys()).thenReturn(fieldNames.toArray(fieldNamesArray));
            when(reportModuleDescriptor.getObjectConfiguration(any(Map.class))).thenReturn(objectConfiguration);
            return reportModuleDescriptor;
        }

        public MockReportModuleDescriptorBuilder withFilterPickerField(String fieldName) throws ObjectConfigurationException {
            when(objectConfiguration.getFieldTypeName(fieldName)).thenReturn(FIELD_TYPE_FILTER);
            fieldNames.add(fieldName);
            return this;
        }

        public MockReportModuleDescriptorBuilder withFilterOrProjectPickerField(String fieldName) throws ObjectConfigurationException {
            when(objectConfiguration.getFieldTypeName(fieldName)).thenReturn(FIELD_TYPE_FILTER_OR_PROJECT);
            fieldNames.add(fieldName);
            return this;
        }

        public MockReportModuleDescriptorBuilder withStringField(String fieldName) throws ObjectConfigurationException {
            when(objectConfiguration.getFieldTypeName(fieldName)).thenReturn(FIELD_TYPE_STRING);
            fieldNames.add(fieldName);
            return this;
        }


        public MockReportModuleDescriptorBuilder withLongField(String fieldName) throws ObjectConfigurationException {
            when(objectConfiguration.getFieldTypeName(fieldName)).thenReturn(FIELD_TYPE_LONG);
            fieldNames.add(fieldName);
            return this;
        }
    }

}
