package com.atlassian.jira.plugin.corereports.web.action;

import com.atlassian.configurable.ObjectConfiguration;
import com.atlassian.configurable.ObjectConfigurationException;
import com.atlassian.configurable.ObjectConfigurationTypes;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.datetime.DateTimeStyle;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.plugin.report.Report;
import com.atlassian.jira.plugin.report.ReportModuleDescriptor;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.projects.api.sidebar.ProjectSidebarRenderer;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserProjectHistoryManager;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.HttpServletVariables;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.atlassian.webresource.api.assembler.RequiredResources;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.runners.MockitoJUnitRunner;
import webwork.action.ActionContext;

import static com.atlassian.jira.security.Permissions.BROWSE;
import static java.util.Collections.singletonMap;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestConfigureReport {
    private static final String CONFIGURE_REPORTS_CONTEXT = "com.atlassian.jira.jira-core-reports-plugin.configure-report";
    private static final String REPORT_RESULT_CONTEXT = "com.atlassian.jira.jira-core-reports-plugin.report-result";

    @Rule
    public RuleChain init = MockitoMocksInContainer.forTest(this);
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private EventPublisher mockEventPublisher;
    @Mock
    private PermissionManager mockPermissionManager;
    @Mock
    private PluginAccessor mockPluginAccessor;
    @Mock
    private ProjectManager mockProjectManager;

    @Mock
    private PageBuilderService mockPageBuilder;

    @Mock
    private WebResourceAssembler webResourceAssembler;

    @Mock
    private RequiredResources requiredResources;

    @Mock
    private SoyTemplateRenderer mockSoyRenderer;

    @Mock
    @AvailableInContainer
    private ApplicationProperties mockAppProperties;

    @Mock
    @AvailableInContainer
    private JiraAuthenticationContext mockAuthenticationContext;

    @Mock
    @AvailableInContainer
    private UserProjectHistoryManager userProjectHistoryManager;

    @Mock
    @AvailableInContainer
    private ProjectManager projectManager;

    @Mock
    @AvailableInContainer
    private HttpServletVariables httpServletVariables;

    @Mock
    @AvailableInContainer
    private DateTimeFormatterFactory dateTimeFormatterFactory;

    @Mock
    private HttpServletRequest mockRequest;

    @Mock
    private ApplicationUser mockUser;

    @Mock
    private ReportModuleDescriptor reportDescriptor;

    @Mock
    private Report report;

    @Mock
    private ObjectConfiguration reportObjectConfiguration;

    @Mock
    private ProjectSidebarRenderer projectSidebarRenderer;

    @Mock
    private ReportProjectContextResolver reportProjectContextResolver;

    @Mock
    private I18nHelper i18nHelper;

    @Mock
    private DateTimeFormatter dateTimeFormatter;

    @Mock
    private Project resolvedResultProject;

    private ConfigureReport configureReport;

    @Before
    @SuppressWarnings("unchecked")
    public void setUp() throws Exception {
        when(mockPageBuilder.assembler()).thenReturn(webResourceAssembler);
        when(webResourceAssembler.resources()).thenReturn(requiredResources);
        when(ComponentAccessor.getComponent(ApplicationProperties.class).getEncoding()).thenReturn("UTF-8");
        when(mockPluginAccessor.getEnabledPluginModule(anyString())).thenReturn((ModuleDescriptor) reportDescriptor);
        when(reportDescriptor.getObjectConfiguration(anyMap())).thenReturn(reportObjectConfiguration);
        when(report.showReport()).thenReturn(true);
        when(reportDescriptor.getModule()).thenReturn(report);
        when(httpServletVariables.getHttpRequest()).thenReturn(mockRequest);
        when(ComponentAccessor.getComponent(HttpServletVariables.class).getHttpRequest()).thenReturn(mockRequest); //Not the same one apparently
        when(mockAuthenticationContext.getLoggedInUser()).thenReturn(mockUser);
        when(ComponentAccessor.getComponent(JiraAuthenticationContext.class).getI18nHelper()).thenReturn(i18nHelper);
        when(ComponentAccessor.getComponent(DateTimeFormatterFactory.class).formatter()).thenReturn(dateTimeFormatter);
        when(dateTimeFormatter.forLoggedInUser()).thenReturn(dateTimeFormatter);
        when(dateTimeFormatter.withStyle(any(DateTimeStyle.class))).thenReturn(dateTimeFormatter);
        when(resolvedResultProject.getId()).thenReturn(10000L);
        when(projectManager.getProjectObj(10000L)).thenReturn(resolvedResultProject);
        when(ComponentAccessor.getComponent(ProjectManager.class).getProjectObj(10000L)).thenReturn(resolvedResultProject);
        when(reportProjectContextResolver.getProjectContext(
                any(ReportModuleDescriptor.class), any(HttpServletRequest.class))).thenReturn(Optional.of(resolvedResultProject));
        when(userProjectHistoryManager.getCurrentProject(BROWSE, null))
                .thenReturn(resolvedResultProject);
        when(ComponentAccessor.getComponent(UserProjectHistoryManager.class).getCurrentProject(BROWSE, null))
                .thenReturn(resolvedResultProject);

        configureReport = new ConfigureReport(
                mockProjectManager,
                mockPermissionManager,
                mockPluginAccessor,
                mockEventPublisher,
                mockPageBuilder,
                mockSoyRenderer,
                mockAppProperties,
                projectSidebarRenderer,
                reportProjectContextResolver);

        configureReport.setReportKey("test.report.key");
    }

    // TODO Someone who knows the difference can rename these two tests...
    @Test
    public void testGetQueryString1() {
        final Map<String, String[]> parameters = new LinkedHashMap<String, String[]>();
        parameters.put("a", toArray("b"));
        parameters.put("encode", toArray("yes sir"));
        parameters.put("funny", toArray("ch(ara=cter&s"));
        parameters.put("done", toArray("ok"));

        assertQueryString(parameters, "a=b&encode=yes+sir&funny=ch%28ara%3Dcter%26s&done=ok");
    }

    @Test
    public void testGetQueryString2() {
        final Map<String, String[]> parameters = singletonMap("one", toArray("value"));
        assertQueryString(parameters, "one=value");
    }

    @Test
    public void showSaveButtonWhenThereAreNoEmptySelectLists() throws ObjectConfigurationException {
        when(reportObjectConfiguration.getFieldKeys()).thenReturn(new String[]{"field1"});
        when(reportObjectConfiguration.getFieldType(anyString())).thenReturn(ObjectConfigurationTypes.SELECT);
        when(reportObjectConfiguration.getFieldValues(anyString())).thenReturn(ImmutableMap.of("option", "option"));
        assertThat(configureReport.isShowSaveButton(), is(true));
    }

    @Test
    public void dontShowSaveButtonWhenThereAreEmptySelectLists() throws ObjectConfigurationException {
        when(reportObjectConfiguration.getFieldKeys()).thenReturn(new String[]{"field1"});
        when(reportObjectConfiguration.getFieldType(anyString())).thenReturn(ObjectConfigurationTypes.SELECT);
        when(reportObjectConfiguration.getFieldValues(anyString())).thenReturn(Collections.emptyMap());
        assertThat(configureReport.isShowSaveButton(), is(false));
    }

    @Test
    public void sidebarIsRendered() {
        configureReport.setSelectedProject(new MockProject());
        configureReport.getSidebarHtml();
        verify(projectSidebarRenderer).render(any(Project.class), anyString());
    }

    @Test
    public void reportResultsResourcesAreIncludedOnDoExecute() throws Exception {
        configureReport.doExecute();

        verify(requiredResources).requireContext(REPORT_RESULT_CONTEXT);
    }

    @Test
    public void configureReportsResourcesAreIncludedOnDoDefault() throws Exception {
        configureReport.doDefault();

        verify(requiredResources).requireContext(CONFIGURE_REPORTS_CONTEXT);
    }

    @Test
    public void datePickerParameters() throws ObjectConfigurationException {
        when(reportObjectConfiguration.getFieldKeys()).thenReturn(new String[]{"field1"});
        when(i18nHelper.getLocale()).thenReturn(Locale.ENGLISH);
        when(dateTimeFormatter.format(Mockito.any(Date.class))).thenReturn("formatted-date");
        when(ComponentAccessor.getApplicationProperties()
                .getDefaultBackedString(APKeys.JIRA_DATE_PICKER_JAVASCRIPT_FORMAT))
                .thenReturn("date-format");

        ConfigureReport.ObjectConfigurationField field = Iterables.get(configureReport.getObjectConfigurationFields(), 0);
        Collection<ConfigureReport.DatePickerParameter> parameters = field.getDatePickerParameters();

        for (ConfigureReport.DatePickerParameter parameter : parameters) {
            String key = parameter.getKey();
            String value = parameter.getValue();
            switch (key) {
                case "firstDay":
                    // Note: This value (the first day of the week) is locale dependent, but should always be zero for
                    // English.
                    assertThat(value, is("0"));
                    break;
                case "date":
                case "todayDate":
                    assertThat(value, is("formatted-date"));
                    break;
                case "useISO8601WeekNumbers":
                    assertThat(value, is("false"));
                    break;
                case "ifFormat":
                    assertThat(value, is("date-format"));
                    break;
                default:
                    fail("Unexpected parameter key: " + key);
            }
        }
    }

    @Test
    public void getsProjectFromContextResolver() throws Exception {
        configureReport.doDefault();

        Project project = configureReport.getSelectedProject();

        assertThat(project, is(resolvedResultProject));
    }

    @Test
    public void fallsBackToProjectInSessionWhenContextResolutionFails() {
        when(reportProjectContextResolver.getProjectContext(
                any(ReportModuleDescriptor.class), any(HttpServletRequest.class))).thenReturn(Optional.empty());
        when(userProjectHistoryManager.getCurrentProject(BROWSE, null))
                .thenReturn(resolvedResultProject);

        Project project = configureReport.getSelectedProject();

        assertThat(project, is(resolvedResultProject));
    }

    private void assertQueryString(final Map<String, String[]> parameters, final String expectedQueryString) {
        // Set up
        ActionContext.setParameters(parameters);

        // Invoke and check
        assertEquals(expectedQueryString, configureReport.getQueryString());
    }

    private String[] toArray(final String val) {
        return new String[]{val};
    }
}
