package com.atlassian.jira.plugin.corereports.filters;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestReportKeyRewriter {

    private ReportKeyRewriter filter;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private FilterChain chain;

    @Before
    public void setUp() {
        filter = new ReportKeyRewriter();

        // These values are of no significance, we just need something in them to avoid NPEs in the UriBuilder.
        when(request.getScheme()).thenReturn("https");
        when(request.getServerName()).thenReturn("test");
        when(request.getRequestURI()).thenReturn("/Test.jspa");
        when(request.getQueryString()).thenReturn("x=y");
    }

    @Test
    public void noRedirectWhenTheReportKeyIsNotPresent() throws IOException, ServletException {
        when(request.getParameter("reportKey")).thenReturn(null);

        filter.doFilter(request, response, chain);

        verify(response, never()).sendRedirect(any());
        verify(chain).doFilter(request, response);
    }

    @Test
    public void redirectWithNewParametersWhenAnOldReportKeyIsPresent() throws IOException, ServletException {
        when(request.getParameter("reportKey")).thenReturn("com.atlassian.jira.plugin.system.reports:abc");

        filter.doFilter(request, response, chain);

        verify(response).sendRedirect(
                "https://test/Test.jspa?x=y&reportKey=com.atlassian.jira.jira-core-reports-plugin%3Aabc"
        );
    }

    @Test
    public void noRedirectWhenNewReportKeysArePresent() throws IOException, ServletException {
        when(request.getParameter("reportKey")).thenReturn("com.atlassian.jira.jira-core-reports-plugin:abc");

        filter.doFilter(request, response, chain);

        verify(response, never()).sendRedirect(any());
        verify(chain).doFilter(request, response);
    }

}
