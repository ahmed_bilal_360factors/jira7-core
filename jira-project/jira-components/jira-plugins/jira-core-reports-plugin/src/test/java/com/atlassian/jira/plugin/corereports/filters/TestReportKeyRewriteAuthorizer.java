package com.atlassian.jira.plugin.corereports.filters;

import com.atlassian.uri.Uri;
import com.atlassian.uri.UriBuilder;
import java.net.URI;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class TestReportKeyRewriteAuthorizer {

    private static final String VALID_PATH = "/secure/ConfigureReport!default.jspa";
    private static final String INVALID_PATH = "/someother/path";
    private static final String NEW_REPORT_KEY = "com.atlassian.jira.jira-core-reports-plugin:";
    private static final String OLD_REPORT_KEY = "com.atlassian.jira.plugin.system.reports:";
    private static final String REPORT_NAME = "singlelevelgroupby";

    private ReportKeyRewriteAuthorizer authorizer;

    @Mock
    private HttpServletRequest source;

    private URI target;

    @Before
    public void setUp() {
        target = URI.create("http://test.com?abc=123");
        authorizer = new ReportKeyRewriteAuthorizer();
    }

    @Test
    public void shouldAuthorizeForwardForOldReportKeysInConfigureReport() {
        setPath(VALID_PATH);
        setReportKey(OLD_REPORT_KEY + REPORT_NAME);

        Optional<Boolean> authorized = authorizer.authorizeForward(source, target);

        assertThat(authorized, is(Optional.of(true)));
    }

    @Test
    public void shouldNotAuthorizeForwardForNewReportKeysInConfigureReport() {
        setPath(VALID_PATH);
        setReportKey(NEW_REPORT_KEY + REPORT_NAME);

        Optional<Boolean> authorized = authorizer.authorizeForward(source, target);

        assertThat(authorized, is(Optional.empty()));
    }

    @Test
    public void shouldNotAuthorizeForArbitraryUrls() {
        setPath(INVALID_PATH);

        Optional<Boolean> authorized = authorizer.authorizeForward(source, target);

        assertThat(authorized, is(Optional.empty()));
    }

    @Test
    public void shouldNotAuthorizeWhenOldReportKeyIsPresentInArbitraryUrl() {
        setPath(INVALID_PATH);
        setReportKey(OLD_REPORT_KEY + REPORT_NAME);

        Optional<Boolean> authorized = authorizer.authorizeForward(source, target);

        assertThat(authorized, is(Optional.empty()));
    }

    private void setPath(String path) {
        target = new UriBuilder(Uri.fromJavaUri(target))
                .setPath(path)
                .toUri()
                .toJavaUri();
    }

    private void setReportKey(String reportKey) {
        target = new UriBuilder(Uri.fromJavaUri(target))
                .addQueryParameter("reportKey", reportKey)
                .toUri()
                .toJavaUri();
    }

}
