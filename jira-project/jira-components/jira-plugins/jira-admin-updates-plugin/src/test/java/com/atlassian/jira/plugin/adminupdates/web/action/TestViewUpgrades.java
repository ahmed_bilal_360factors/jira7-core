package com.atlassian.jira.plugin.adminupdates.web.action;

import com.atlassian.jira.cluster.zdu.ClusterUpgradeStateManager;
import com.atlassian.jira.cluster.zdu.UpgradeState;
import com.atlassian.sal.api.features.DarkFeatureManager;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.atlassian.webresource.api.assembler.RequiredResources;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import webwork.action.Action;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestViewUpgrades {
    private static final String CONTEXT = "com.atlassian.jira.jira-admin-updates-plugin.view-upgrades-ctx";
    private static final String VIEW = "viewUpgradesPage";
    private static final String CLUSTER_UPGRADE_STATE_DARK_FEATURE = "jira.zdu.cluster-upgrade-state";
    private static final String ADMIN_UPDATES_UI_DARK_FEATURE = "jira.zdu.admin-updates-ui";

    @Mock
    private WebResourceAssembler webResourceAssembler;

    @Mock
    private PageBuilderService mockPageBuilder;

    @Mock
    private ClusterUpgradeStateManager clusterUpgradeStateManager;

    @Mock
    private DarkFeatureManager darkFeatureManager;

    @Mock
    private RequiredResources requiredResources;

    private ViewUpgrades viewUpgrades;

    @Before
    public void setUp() {
        when(mockPageBuilder.assembler()).thenReturn(webResourceAssembler);
        when(webResourceAssembler.resources()).thenReturn(requiredResources);
        when(clusterUpgradeStateManager.getUpgradeState()).thenReturn(UpgradeState.STABLE);
        when(darkFeatureManager.isFeatureEnabledForCurrentUser(CLUSTER_UPGRADE_STATE_DARK_FEATURE)).thenReturn(true);
        when(darkFeatureManager.isFeatureEnabledForCurrentUser(ADMIN_UPDATES_UI_DARK_FEATURE)).thenReturn(true);

        viewUpgrades = new ViewUpgrades(mockPageBuilder, clusterUpgradeStateManager, darkFeatureManager);
    }

    @Test
    public void doDefaultReturnsView() {
        final String view = viewUpgrades.doDefault();

        assertThat(view, equalTo(VIEW));
    }

    @Test
    public void doDefaultReturnsErrorWhenClusterUpgradeDarkFeatureIsOff() {
        when(darkFeatureManager.isFeatureEnabledForCurrentUser(CLUSTER_UPGRADE_STATE_DARK_FEATURE)).thenReturn(false);

        final String view = viewUpgrades.doDefault();

        assertThat(view, equalTo(Action.ERROR));
    }

    @Test
    public void doDefaultReturnsErrorWhenAdminUpdatesUIDarkFeatureIsOff() {
        when(darkFeatureManager.isFeatureEnabledForCurrentUser(ADMIN_UPDATES_UI_DARK_FEATURE)).thenReturn(false);

        final String view = viewUpgrades.doDefault();

        assertThat(view, equalTo(Action.ERROR));
    }

    @Test
    public void resourcesAreIncluded() {
        viewUpgrades.doDefault();

        verify(requiredResources).requireContext(CONTEXT);
    }

    @Test
    public void parametersInStableState() {
        viewUpgrades.doDefault();

        assertThat(viewUpgrades.isExpandPreUpdateTasks(), is(true));
        assertThat(viewUpgrades.isActiveStartButton(), is(true));
        assertThat(viewUpgrades.isActiveFinishButton(), is(false));
        assertThat(viewUpgrades.isActiveCancelButton(), is(false));
        assertThat(viewUpgrades.isShowWarningMessage(), is(false));
    }

    @Test
    public void parametersInReadyToUpgradeState() {
        when(clusterUpgradeStateManager.getUpgradeState()).thenReturn(UpgradeState.READY_TO_UPGRADE);

        viewUpgrades.doDefault();

        assertThat(viewUpgrades.isExpandPreUpdateTasks(), is(false));
        assertThat(viewUpgrades.isActiveStartButton(), is(false));
        assertThat(viewUpgrades.isActiveFinishButton(), is(false));
        assertThat(viewUpgrades.isActiveCancelButton(), is(true));
        assertThat(viewUpgrades.isShowWarningMessage(), is(true));
    }

    @Test
    public void parametersInMixedState() {
        when(clusterUpgradeStateManager.getUpgradeState()).thenReturn(UpgradeState.MIXED);

        viewUpgrades.doDefault();

        assertThat(viewUpgrades.isExpandPreUpdateTasks(), is(false));
        assertThat(viewUpgrades.isActiveStartButton(), is(false));
        assertThat(viewUpgrades.isActiveFinishButton(), is(false));
        assertThat(viewUpgrades.isActiveCancelButton(), is(false));
        assertThat(viewUpgrades.isShowWarningMessage(), is(true));
    }

    @Test
    public void parametersInReadyToRunUpgradeTasksState() {
        when(clusterUpgradeStateManager.getUpgradeState()).thenReturn(UpgradeState.READY_TO_RUN_UPGRADE_TASKS);

        viewUpgrades.doDefault();

        assertThat(viewUpgrades.isExpandPreUpdateTasks(), is(false));
        assertThat(viewUpgrades.isActiveStartButton(), is(false));
        assertThat(viewUpgrades.isActiveFinishButton(), is(true));
        assertThat(viewUpgrades.isActiveCancelButton(), is(false));
        assertThat(viewUpgrades.isShowWarningMessage(), is(true));
    }

    @Test
    public void parametersInRunningUpgradeTasksState() {
        when(clusterUpgradeStateManager.getUpgradeState()).thenReturn(UpgradeState.RUNNING_UPGRADE_TASKS);

        viewUpgrades.doDefault();

        assertThat(viewUpgrades.isExpandPreUpdateTasks(), is(false));
        assertThat(viewUpgrades.isActiveStartButton(), is(false));
        assertThat(viewUpgrades.isActiveFinishButton(), is(false));
        assertThat(viewUpgrades.isActiveCancelButton(), is(false));
        assertThat(viewUpgrades.isShowWarningMessage(), is(true));
    }
}
