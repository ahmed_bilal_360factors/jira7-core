package com.atlassian.jira.plugin.adminupdates.web.resource.dataprovider;

import com.atlassian.jira.cluster.zdu.ClusterUpgradeStateManager;
import com.atlassian.jira.cluster.zdu.UpgradeState;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.io.StringWriter;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestViewUpgradesDataProvider {
    @Mock
    private ClusterUpgradeStateManager clusterUpgradeStateManager;

    private ViewUpgradesDataProvider viewUpgradesDataProvider;

    @Before
    public void setUp() {
        when(clusterUpgradeStateManager.getUpgradeState()).thenReturn(UpgradeState.STABLE);

        viewUpgradesDataProvider = new ViewUpgradesDataProvider(clusterUpgradeStateManager);
    }

    @Test
    public void clusterStateIsInJsonObject() throws IOException, JSONException {
        final StringWriter writer = new StringWriter();

        viewUpgradesDataProvider.get().write(writer);

        final JSONObject jsonObject = new JSONObject(writer.toString());
        assertThat(jsonObject.get("clusterState").toString(), equalTo(UpgradeState.STABLE.name()));
    }
}
