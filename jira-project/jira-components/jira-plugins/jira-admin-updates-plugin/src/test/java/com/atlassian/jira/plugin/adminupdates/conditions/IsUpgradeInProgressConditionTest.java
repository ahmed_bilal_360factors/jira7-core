package com.atlassian.jira.plugin.adminupdates.conditions;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.cluster.ClusterMessageConsumer;
import com.atlassian.jira.cluster.ClusterMessagingService;
import com.atlassian.jira.cluster.zdu.ClusterUpgradeStateManager;
import com.atlassian.jira.cluster.zdu.UpgradeState;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.jira.cluster.zdu.ClusterUpgradeStateManager.CLUSTER_UPGRADE_STATE_CHANGED;
import static com.atlassian.jira.cluster.zdu.UpgradeState.MIXED;
import static com.atlassian.jira.cluster.zdu.UpgradeState.READY_TO_RUN_UPGRADE_TASKS;
import static com.atlassian.jira.cluster.zdu.UpgradeState.READY_TO_UPGRADE;
import static com.atlassian.jira.cluster.zdu.UpgradeState.RUNNING_UPGRADE_TASKS;
import static com.atlassian.jira.cluster.zdu.UpgradeState.STABLE;
import static java.util.Collections.emptyMap;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class IsUpgradeInProgressConditionTest {
    private IsUpgradeInProgressCondition condition;

    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private ClusterUpgradeStateManager clusterStateManager;
    @Mock
    private ClusterMessagingService clusterMessagingService;

    ClusterMessageConsumer consumer;

    @Before
    public void setUp() {
        mockClusterUpgradeState(STABLE);

        doAnswer(invocationOnMock -> {
            consumer = invocationOnMock.getArgumentAt(1, ClusterMessageConsumer.class);
            return null;
        }).when(clusterMessagingService).registerListener(eq(CLUSTER_UPGRADE_STATE_CHANGED), any());
        condition = new IsUpgradeInProgressCondition(clusterStateManager, eventPublisher, clusterMessagingService);
    }

    @Test
    public void stableStateReturnsFalse() {
        initialize();

        assertShouldNotDisplay();
    }

    @Test
    public void mixedStateReturnsTrue() {
        stateReturnsTrue(MIXED);
    }

    @Test
    public void readyToUpgradeStateReturnsTrue() {
        stateReturnsTrue(READY_TO_UPGRADE);
    }

    @Test
    public void readyToRunUpgradeTasksStateReturnsTrue() {
        stateReturnsTrue(READY_TO_RUN_UPGRADE_TASKS);
    }

    @Test
    public void runningUpgradeTasksStateReturnsTrue() {
        stateReturnsTrue(RUNNING_UPGRADE_TASKS);
    }

    @Test
    public void wiresUpEventPublisherRegisterCorrectly() throws Exception {
        condition.afterPropertiesSet();

        verify(eventPublisher, times(1)).register(condition);
    }

    @Test
    public void wiresUpEventPublisherUnregisterCorrectly() throws Exception {
        condition.destroy();

        verify(eventPublisher, times(1)).unregister(condition);
    }

    @Test
    public void wiresUpMessageHandlerRegisterCorrectly() throws Exception {
        condition.afterPropertiesSet();

        verify(clusterMessagingService, times(1)).registerListener(CLUSTER_UPGRADE_STATE_CHANGED, consumer);
    }

    @Test
    public void wiresUpMessageHandlerUnregisterCorrectly() throws Exception {
        condition.afterPropertiesSet();
        condition.destroy();

        verify(clusterMessagingService, times(1)).unregisterListener(consumer);
    }

    @Test
    public void updatesStateWhenStarted() {
        mockClusterUpgradeState(STABLE);
        initialize();
        condition.onUpgradeStarted(null);

        assertShouldDisplay();
    }

    @Test
    public void updatesStateWhenCancelled() {
        mockClusterUpgradeState(MIXED);
        initialize();
        condition.onUpgradeCancelled(null);

        assertShouldNotDisplay();
    }

    @Test
    public void updatesStateWhenFinished() {
        mockClusterUpgradeState(MIXED);
        initialize();
        condition.onUpgradeFinished(null);

        assertShouldNotDisplay();
    }

    @Test
    public void updatesStateWhenOtherNodeStarted() throws Exception {
        mockClusterUpgradeState(STABLE);
        initialize();
        condition.afterPropertiesSet();

        mockClusterUpgradeState(MIXED);
        consumer.receive(CLUSTER_UPGRADE_STATE_CHANGED, MIXED.toString(), "node2");

        assertShouldDisplay();
    }

    @Test
    public void updatesStateWhenOtherNodeCancelled() throws Exception {
        mockClusterUpgradeState(READY_TO_UPGRADE);
        initialize();
        condition.afterPropertiesSet();

        mockClusterUpgradeState(STABLE);
        consumer.receive(CLUSTER_UPGRADE_STATE_CHANGED, STABLE.toString(), "node2");

        assertShouldNotDisplay();
    }

    @Test
    public void updatesStateWhenOtherNodeFinished() throws Exception {
        mockClusterUpgradeState(RUNNING_UPGRADE_TASKS);
        initialize();
        condition.afterPropertiesSet();

        mockClusterUpgradeState(STABLE);
        consumer.receive(CLUSTER_UPGRADE_STATE_CHANGED, STABLE.toString(), "node2");

        assertShouldNotDisplay();
    }

    @Test
    public void doesNotTrustClusterMessageStateAndUsesDatabaseInstead() throws Exception {
        mockClusterUpgradeState(READY_TO_UPGRADE);
        initialize();
        condition.afterPropertiesSet();

        mockClusterUpgradeState(STABLE);
        consumer.receive(CLUSTER_UPGRADE_STATE_CHANGED, MIXED.toString(), "node2");

        assertShouldNotDisplay();
    }



    private void assertShouldDisplay() {
        assertThat(condition.shouldDisplay(emptyMap()), is(true));
    }

    private void assertShouldNotDisplay() {
        assertThat(condition.shouldDisplay(emptyMap()), is(false));
    }

    private void stateReturnsTrue(final UpgradeState state) {
        mockClusterUpgradeState(state);
        initialize();

        assertShouldDisplay();
    }

    private void mockClusterUpgradeState(final UpgradeState state) {
        when(clusterStateManager.getUpgradeState()).thenReturn(state);
    }

    private void initialize() {
        condition.init(emptyMap());
    }
}
