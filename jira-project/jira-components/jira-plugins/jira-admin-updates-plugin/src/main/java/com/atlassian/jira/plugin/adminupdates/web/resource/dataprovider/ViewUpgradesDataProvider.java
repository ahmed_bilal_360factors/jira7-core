package com.atlassian.jira.plugin.adminupdates.web.resource.dataprovider;

import com.atlassian.jira.cluster.zdu.ClusterUpgradeStateManager;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.webresource.api.data.WebResourceDataProvider;
import com.google.common.collect.Maps;
import org.codehaus.jackson.map.JsonMappingException;

import java.util.Map;

public class ViewUpgradesDataProvider implements WebResourceDataProvider {
    private final ClusterUpgradeStateManager clusterUpgradeStateManager;

    public ViewUpgradesDataProvider(
            @ComponentImport final ClusterUpgradeStateManager clusterUpgradeStateManager
    ) {
        this.clusterUpgradeStateManager = clusterUpgradeStateManager;
    }

    @Override
    public Jsonable get() {
        return writer -> {
            try {
                getJsonData().write(writer);
            } catch (JSONException e) {
                throw new JsonMappingException(e.getMessage(), e);
            }
        };
    }

    private JSONObject getJsonData() {
        final Map<String, Object> values = Maps.newHashMap();
        values.put("clusterState", clusterUpgradeStateManager.getUpgradeState().name());

        return new JSONObject(values);
    }
}
