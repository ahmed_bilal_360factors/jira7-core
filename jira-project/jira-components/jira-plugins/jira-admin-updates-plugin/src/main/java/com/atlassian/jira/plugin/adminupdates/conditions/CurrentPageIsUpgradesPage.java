package com.atlassian.jira.plugin.adminupdates.conditions;

import com.atlassian.jira.plugin.webfragment.conditions.AbstractWebCondition;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
import webwork.action.ActionContext;

import java.util.Objects;

/**
 * Returns true whenever a webaction called {@code ViewUpgrades!default} is being executed.
 * @see ActionContext#getName()
 */
public class CurrentPageIsUpgradesPage extends AbstractWebCondition {
    @Override
    public boolean shouldDisplay(final ApplicationUser user, final JiraHelper jiraHelper) {
        return Objects.equals("ViewUpgrades!default", ActionContext.getName());
    }
}
