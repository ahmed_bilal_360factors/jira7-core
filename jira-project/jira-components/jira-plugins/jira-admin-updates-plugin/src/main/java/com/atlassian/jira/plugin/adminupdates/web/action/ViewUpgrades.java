package com.atlassian.jira.plugin.adminupdates.web.action;

import com.atlassian.jira.cluster.zdu.ClusterUpgradeStateManager;
import com.atlassian.jira.cluster.zdu.UpgradeState;
import com.atlassian.jira.web.action.ActionViewData;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.features.DarkFeatureManager;
import com.atlassian.webresource.api.assembler.PageBuilderService;

public class ViewUpgrades extends JiraWebActionSupport {
    private static final String CONTEXT = "com.atlassian.jira.jira-admin-updates-plugin.view-upgrades-ctx";
    private static final String VIEW = "viewUpgradesPage";
    private static final String CLUSTER_UPGRADE_STATE_DARK_FEATURE = "jira.zdu.cluster-upgrade-state";
    private static final String ADMIN_UPDATES_UI_DARK_FEATURE = "jira.zdu.admin-updates-ui";

    private final PageBuilderService page;
    private final ClusterUpgradeStateManager clusterUpgradeStateManager;
    private final DarkFeatureManager darkFeatureManager;

    private UpgradeState upgradeState;

    public ViewUpgrades(
            @ComponentImport final PageBuilderService page,
            @ComponentImport final ClusterUpgradeStateManager clusterUpgradeStateManager,
            @ComponentImport final DarkFeatureManager darkFeatureManager
    ) {
        this.page = page;
        this.clusterUpgradeStateManager = clusterUpgradeStateManager;
        this.darkFeatureManager = darkFeatureManager;
    }

    @Override
    public String doDefault() {
        if(!this.darkFeatureManager.isFeatureEnabledForCurrentUser(CLUSTER_UPGRADE_STATE_DARK_FEATURE) ||
                !this.darkFeatureManager.isFeatureEnabledForCurrentUser(ADMIN_UPDATES_UI_DARK_FEATURE)) {
            return ERROR;
        }

        page.assembler().resources().requireContext(CONTEXT);

        upgradeState = clusterUpgradeStateManager.getUpgradeState();

        return VIEW;
    }

    @ActionViewData
    public boolean isExpandPreUpdateTasks() {
        return upgradeState == UpgradeState.STABLE;
    }

    @ActionViewData
    public boolean isActiveStartButton() {
        return upgradeState == UpgradeState.STABLE;
    }

    @ActionViewData
    public boolean isActiveFinishButton() {
        return upgradeState == UpgradeState.READY_TO_RUN_UPGRADE_TASKS;
    }

    @ActionViewData
    public boolean isActiveCancelButton() {
        return upgradeState == UpgradeState.READY_TO_UPGRADE;
    }

    @ActionViewData
    public boolean isShowWarningMessage() {
        return upgradeState != UpgradeState.STABLE;
    }
}
