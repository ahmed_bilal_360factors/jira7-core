package com.atlassian.jira.plugin.adminupdates.conditions;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.cluster.ClusterMessageConsumer;
import com.atlassian.jira.cluster.ClusterMessagingService;
import com.atlassian.jira.cluster.zdu.ClusterStateManager;
import com.atlassian.jira.cluster.zdu.ClusterUpgradeStateManager;
import com.atlassian.jira.cluster.zdu.JiraUpgradeCancelledEvent;
import com.atlassian.jira.cluster.zdu.JiraUpgradeFinishedEvent;
import com.atlassian.jira.cluster.zdu.JiraUpgradeStartedEvent;
import com.atlassian.jira.plugin.webfragment.conditions.AbstractWebCondition;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import javax.inject.Inject;
import java.util.Map;

import static com.atlassian.jira.cluster.zdu.ClusterUpgradeStateManager.CLUSTER_UPGRADE_STATE_CHANGED;
import static com.atlassian.jira.cluster.zdu.UpgradeState.STABLE;

/**
 * This condition returns {@code true} when an upgrade is in progress.
 * <p></p>
 * The implementation is optimized so that the {@link ClusterUpgradeStateManager} is not queried every time, instead
 * it keeps the state internal and reacts to {@link JiraUpgradeStartedEvent}, {@link JiraUpgradeCancelledEvent} and
 * {@link JiraUpgradeFinishedEvent} to keep it up to date.
 */
public class IsUpgradeInProgressCondition extends AbstractWebCondition implements InitializingBean, DisposableBean {

    private final ClusterStateManager clusterStateManager;
    private final EventPublisher eventPublisher;
    private final ClusterMessagingService clusterMessagingService;

    private boolean upgradeActive = false;

    @Inject
    public IsUpgradeInProgressCondition(@ComponentImport ClusterUpgradeStateManager clusterStateManager,
                                        @ComponentImport EventPublisher eventPublisher,
                                        @ComponentImport ClusterMessagingService clusterMessagingService) {
        this.clusterStateManager = clusterStateManager;
        this.eventPublisher = eventPublisher;
        this.clusterMessagingService = clusterMessagingService;
    }

    @Override
    public void init(final Map<String, String> params) throws PluginParseException {
        updateUpgradeActiveState();
    }

    public boolean shouldDisplay(final ApplicationUser applicationUser, final JiraHelper jiraHelper) {
        return upgradeActive;
    }

    private final ClusterMessageConsumer clusterMessageConsumer = (channel, message, senderId) -> {
        updateUpgradeActiveState();
    };

    @EventListener
    public void onUpgradeStarted(JiraUpgradeStartedEvent event) {
        upgradeActive = true;
    }

    @EventListener
    public void onUpgradeFinished(JiraUpgradeFinishedEvent event) {
        upgradeActive = false;
    }

    @PluginEventListener
    public void onUpgradeCancelled(JiraUpgradeCancelledEvent event) {
        upgradeActive = false;
    }

    @Override
    public void destroy() throws Exception {
        eventPublisher.unregister(this);
        clusterMessagingService.unregisterListener(clusterMessageConsumer);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        eventPublisher.register(this);
        clusterMessagingService.registerListener(CLUSTER_UPGRADE_STATE_CHANGED, clusterMessageConsumer);
    }

    private void updateUpgradeActiveState() {
        upgradeActive = clusterStateManager.getUpgradeState() != STABLE;
    }
}
