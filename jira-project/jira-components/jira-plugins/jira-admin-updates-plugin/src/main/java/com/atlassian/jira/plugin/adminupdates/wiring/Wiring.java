package com.atlassian.jira.plugin.adminupdates.wiring;

import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

/**
 * Ensures that classes referenced in non-Java files (i.e. atlassian-plugin.xml) are picked up through OSGi.
 */
@SuppressWarnings("unused")
public class Wiring {
    /**
     * Required to validate whether JIRA is in cluster mode.
     * @see com.atlassian.jira.plugin.webfragment.conditions.IsClusteredCondition
     */
    @ComponentImport
    private ClusterManager clusterManager;

    /**
     * Required to validate whether the current user is a sysadmin.
     * @see com.atlassian.jira.plugin.webfragment.conditions.UserIsSysAdminCondition
     */
    @ComponentImport
    private GlobalPermissionManager globalPermissionManager;

    /**
     * Required to validate whether dark features are enabled.
     * @see com.atlassian.jira.plugin.webfragment.conditions.IsFeatureEnabledCondition
     */
    @ComponentImport
    private FeatureManager featureManager;
}
