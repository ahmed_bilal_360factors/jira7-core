define('admin-updates/views/update/clusterstate', ['require'], function(require) {
    "use strict";

    var Marionette = require('marionette');
    var Templates = require('admin-updates/templates');
    var ClusterStateModel = require('admin-updates/models/clusterstate');
    var formatter = require('jira/util/formatter');

    return Marionette.ItemView.extend({
        tagName: 'span',
        template: Templates.clusterState,
        ui: {
            lozenge: "#cluster-state-lozenge",
            spinner: "#cluster-state-spinner-container",
            infoText: "#cluster-state-info-text"
        },
        initialize: function() {
            this.listenTo(this.model, {
                "fetch:started": this._showSpinner,
                "fetch:finished": this._hideSpinner,
                "fetch:failed": this._hideSpinner,
                "change:state": this.render
            });
        },
        serializeData: function() {
            return {
                id: 'cluster-state-lozenge',
                text: this._replaceUnderscores(this.model.get("state")),
                infoText: this._getInfoText(),
                extraClasses: this._getLozengeStyle()
            };
        },
        /**
         * @param {string} str
         * @returns {string}
         * @private
         */
        _replaceUnderscores: function(str) {
            return str.replace(/_/g, " ");
        },
        _showSpinner: function() {
            this.ui.spinner.html(Templates.spinner({
                text: '',
                accessibilityText: formatter.I18n.getText('update.state.spinner.accessibility.text')
            }));
            this.ui.spinner.removeClass('hidden');
        },
        _hideSpinner: function() {
            this.ui.spinner.addClass('hidden');
        },
        _getLozengeStyle: function() {
            return this.model.get("state") === ClusterStateModel.states.STABLE ?
                'aui-lozenge-success' : 'aui-lozenge-current';
        },
        _getInfoText: function() {
            switch(this.model.get("state")) {
                case ClusterStateModel.states.READY_TO_UPGRADE:
                    return formatter.I18n.getText('update.state.readytoupgrade.infotext');
                case ClusterStateModel.states.MIXED:
                    return formatter.I18n.getText('update.state.mixed.infotext');
                case ClusterStateModel.states.READY_TO_RUN_UPGRADE_TASKS:
                    return formatter.I18n.getText('update.state.readytorunupgradetasks.infotext');
                case ClusterStateModel.states.RUNNING_UPGRADE_TASKS:
                    return formatter.I18n.getText('update.state.runningupgradetasks.infotext');
                case ClusterStateModel.states.STABLE:
                    return formatter.I18n.getText('update.state.stable.infotext');
            }

            return "";
        }
    });
});
