AJS.test.require([
    "com.atlassian.jira.jira-admin-updates-plugin:js-app"
], function() {
    "use strict";

    require([
        'jquery',
        'underscore',
        'backbone',
        'admin-updates/controllers/updateviewmanager',
        'admin-updates/models/clusterstate'
    ], function (
        jQuery,
        _,
        Backbone,
        ViewManager,
        ClusterStateModel
    ) {
        // Without this timeout, QUnit will wait indefinitely for async tests whose done() callback isn't called.
        QUnit.config.testTimeout = 5000;

        function spyOnMultipleMethods(sinon, obj, methods) {
            methods.forEach(function (method) {
                sinon.spy(obj, method);
            });
        }

        module("admin-updates/controllers/updateviewmanager", {
            setup: function () {
                this.sandbox = sinon.sandbox.create();

                this.startDialog = jQuery('<div id="start-dialog-container">');
                this.finishDialog = jQuery('<div id="finish-dialog-container">');
                this.cancelDialog = jQuery('<div id="cancel-dialog-container">');
                this.infoInlineDialog = jQuery('<div id="inline-dialog-container">');

                this.fixture = jQuery('#qunit-fixture');
                this.fixture
                    .append(jQuery('<div id="start-update-container">'))
                    .append(jQuery('<div id="finish-update-container">'))
                    .append(jQuery('<div id="warning-message-container">'))
                    .append(jQuery('<div id="cluster-state-container">'))
                    .append(jQuery('<div id="dialog-container">'))
                    .append(this.startDialog)
                    .append(this.finishDialog)
                    .append(this.cancelDialog)
                    .append(this.infoInlineDialog);

                this.model = new ClusterStateModel();
                this.model.set("state", ClusterStateModel.states.STABLE);

                this.defaultOpts = {
                    el: this.fixture,
                    model: this.model,
                    startDialog: {el: this.startDialog},
                    finishDialog: {el: this.finishDialog},
                    cancelDialog: {el: this.cancelDialog},
                    infoInlineDialog: {el: this.infoInlineDialog}
                };

                this.viewManager = new ViewManager(this.defaultOpts);

                this.listener = _.extend({}, Backbone.Events);

                spyOnMultipleMethods(this.sandbox, this.viewManager.startUpdate.currentView, [
                    "enableStartButton", "disableStartButton"
                ]);
                spyOnMultipleMethods(this.sandbox, this.viewManager.finishUpdate.currentView, [
                    "enableFinishButton", "disableFinishButton", "enableCancelButton", "disableCancelButton"
                ]);
                spyOnMultipleMethods(this.sandbox, this.viewManager.warningMessage.currentView, [
                    "showMessage", "hideMessage"
                ]);

                this.sandbox.spy(this.viewManager.dialogContainer, "show");
            },
            teardown: function () {
                this.sandbox.restore();

                if(this.viewManager.dialogContainer.currentView) {
                    this.viewManager.dialogContainer.currentView.dialog.remove();
                }
            }
        });

        test("It listens to update:start and shows a confirmation dialog", function () {
            this.viewManager.startUpdate.currentView.trigger("update:start");

            sinon.assert.calledWith(this.viewManager.dialogContainer.show, sinon.match({
                $el: this.defaultOpts.startDialog.el
            }));
        });

        test("It listens to update:finish and shows a confirmation dialog", function () {
            this.viewManager.finishUpdate.currentView.trigger("update:finish");

            sinon.assert.calledWith(this.viewManager.dialogContainer.show, sinon.match({
                $el: this.defaultOpts.finishDialog.el
            }));
        });

        test("It listens to update:cancel and shows a confirmation dialog", function () {
            this.viewManager.finishUpdate.currentView.trigger("update:cancel");

            sinon.assert.calledWith(this.viewManager.dialogContainer.show, sinon.match({
                $el: this.defaultOpts.cancelDialog.el
            }));
        });

        test("It listens to dialog:confirm and triggers update:start when the start dialog is open", function(assert) {
            assert.expect(0);
            this.viewManager.startUpdate.currentView.trigger("update:start");
            this.listener.listenTo(this.viewManager, "update:start", assert.async());

            this.viewManager.dialogContainer.currentView.ui.confirmButton.click();
        });

        test("It listens to dialog:confirm and triggers update:finish when the finish dialog is open", function(assert) {
            assert.expect(0);
            this.viewManager.finishUpdate.currentView.trigger("update:finish");
            this.listener.listenTo(this.viewManager, "update:finish", assert.async());

            this.viewManager.dialogContainer.currentView.ui.confirmButton.click();
        });

        test("It listens to dialog:confirm and triggers update:cancel when the cancel dialog is open", function(assert) {
            assert.expect(0);
            this.viewManager.finishUpdate.currentView.trigger("update:cancel");
            this.listener.listenTo(this.viewManager, "update:cancel", assert.async());

            this.viewManager.dialogContainer.currentView.ui.confirmButton.click();
        });

        test("Its views listens to change:state and update themselves accordingly (STABLE)", function () {
            // explicitly trigger this event, as we're already in STABLE state.
            this.viewManager.model.trigger("change:state", this.model);

            sinon.assert.calledOnce(this.viewManager.startUpdate.currentView.enableStartButton);
            sinon.assert.calledOnce(this.viewManager.finishUpdate.currentView.disableFinishButton);
            sinon.assert.calledOnce(this.viewManager.finishUpdate.currentView.disableCancelButton);
            sinon.assert.calledOnce(this.viewManager.warningMessage.currentView.hideMessage);
        });

        test("Its views listens to change:state and update themselves accordingly (READY_TO_UPGRADE)", function () {
            this.model.set("state", ClusterStateModel.states.READY_TO_UPGRADE);

            sinon.assert.calledOnce(this.viewManager.startUpdate.currentView.disableStartButton);
            sinon.assert.calledOnce(this.viewManager.finishUpdate.currentView.disableFinishButton);
            sinon.assert.calledOnce(this.viewManager.finishUpdate.currentView.enableCancelButton);
            sinon.assert.calledOnce(this.viewManager.warningMessage.currentView.showMessage);
        });

        test("Its views listens to change:state and update themselves accordingly (MIXED)", function () {
            this.model.set("state", ClusterStateModel.states.MIXED);

            sinon.assert.calledOnce(this.viewManager.startUpdate.currentView.disableStartButton);
            sinon.assert.calledOnce(this.viewManager.finishUpdate.currentView.disableFinishButton);
            sinon.assert.calledOnce(this.viewManager.finishUpdate.currentView.disableCancelButton);
            sinon.assert.calledOnce(this.viewManager.warningMessage.currentView.showMessage);
        });

        test("Its views listens to change:state and update themselves accordingly (READY_TO_RUN_UPGRADE_TASKS)", function () {
            this.model.set("state", ClusterStateModel.states.READY_TO_RUN_UPGRADE_TASKS);

            sinon.assert.calledOnce(this.viewManager.startUpdate.currentView.disableStartButton);
            sinon.assert.calledOnce(this.viewManager.finishUpdate.currentView.enableFinishButton);
            sinon.assert.calledOnce(this.viewManager.finishUpdate.currentView.disableCancelButton);
            sinon.assert.calledOnce(this.viewManager.warningMessage.currentView.showMessage);
        });

        test("Its views listens to change:state and update themselves accordingly (RUNNING_UPGRADE_TASKS)", function () {
            this.model.set("state", ClusterStateModel.states.RUNNING_UPGRADE_TASKS);

            sinon.assert.calledOnce(this.viewManager.startUpdate.currentView.disableStartButton);
            sinon.assert.calledOnce(this.viewManager.finishUpdate.currentView.disableFinishButton);
            sinon.assert.calledOnce(this.viewManager.finishUpdate.currentView.disableCancelButton);
            sinon.assert.calledOnce(this.viewManager.warningMessage.currentView.showMessage);
        });

        test("It propagates the update:start event from the Start view", function() {
            var callback = this.sandbox.stub();
            this.listener.listenTo(this.viewManager, "update:start:dialog", callback);

            this.viewManager.startUpdate.currentView.trigger("update:start");

            sinon.assert.calledOnce(callback);
        });

        test("It propagates the update:finish event from the Finish view", function() {
            var callback = this.sandbox.stub();
            this.listener.listenTo(this.viewManager, "update:finish:dialog", callback);

            this.viewManager.finishUpdate.currentView.trigger("update:finish");

            sinon.assert.calledOnce(callback);
        });

        test("It propagates the update:cancel event from the Finish view", function() {
            var callback = this.sandbox.stub();
            this.listener.listenTo(this.viewManager, "update:cancel:dialog", callback);

            this.viewManager.finishUpdate.currentView.trigger("update:cancel");

            sinon.assert.calledOnce(callback);
        });

        test("It propagates the click:cancel-tooltip event from its child", function() {
            var callback = this.sandbox.stub();
            this.listener.listenTo(this.viewManager, "click:info-tooltip", callback);

            this.viewManager.finishUpdate.currentView.trigger("click:info-tooltip");

            sinon.assert.calledOnce(callback);
        });
    });
});
