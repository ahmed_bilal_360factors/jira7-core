define("admin-updates/views/preupdate/togglecontentbutton", ['require'], function (require) {
    "use strict";

    var Marionette = require('marionette');
    var Templates = require('admin-updates/templates');
    var ClusterStateModel = require('admin-updates/models/clusterstate');
    var formatter = require('jira/util/formatter');

    return Marionette.Layout.extend({
        template: Templates.toggleContentButton,
        ui: {
            button: "#toggle-content-button",
            buttonIcon: ".aui-icon",
            buttonLabel: ".aui-button-label"
        },
        events: {
            "click @ui.button": function(e) {
                e.preventDefault();
                if(this._isExpanded()) {
                    this.trigger("content:collapse");
                } else {
                    this.trigger("content:expand");
                }
            }
        },
        initialize: function() {
            this.listenTo(this.model, "change:state", function() {
                if(this._shouldSetButtonExpanded()) {
                    this.setButtonExpanded();
                } else {
                    this.setButtonCollapsed();
                }
            });
        },
        serializeData: function() {
            return {
                expanded: this._shouldSetButtonExpanded()
            };
        },
        _shouldSetButtonExpanded: function() {
            return this.model.get("state") === ClusterStateModel.states.STABLE;
        },
        _isExpanded: function() {
            return this.ui.button.attr('aria-expanded') === "true";
        },
        setButtonExpanded: function() {
            this.ui.button.attr('aria-expanded', 'true');
            this.ui.buttonIcon.addClass('aui-iconfont-expanded');
            this.ui.buttonIcon.removeClass('aui-iconfont-collapsed');
            this.ui.buttonIcon.text(formatter.I18n.getText('preupdate.table.collapse.accessibility.text'));
            this.ui.buttonLabel.text(formatter.I18n.getText('preupdate.table.collapse'));
        },
        setButtonCollapsed: function() {
            this.ui.button.attr('aria-expanded', 'false');
            this.ui.buttonIcon.addClass('aui-iconfont-collapsed');
            this.ui.buttonIcon.removeClass('aui-iconfont-expanded');
            this.ui.buttonIcon.text(formatter.I18n.getText('preupdate.table.expand.accessibility.text'));
            this.ui.buttonLabel.text(formatter.I18n.getText('preupdate.table.expand'));
        }
    });
});
