define("admin-updates/models/healthcheck", ['require'], function (require) {
    "use strict";

    var Backbone = require('backbone');

    /**
     * This object represents the Severity enum in support-healthcheck-plugin.
     *
     * @type Object
     *
     * @property UNDEFINED Indicates a Health Check was successful.
     * @property MINOR Not in use.
     * @property WARNING Indicates a Health Check is not healthy.
     * @property MAJOR Indicates a Health Check failed.
     * @property CRITICAL Not in use.
     *
     * @see {@link com.atlassian.support.healthcheck.SupportHealthStatus.Severity}
     */
    var severity = {
        UNDEFINED: "undefined",
        MINOR: "minor",
        WARNING: "warning",
        MAJOR: "major",
        CRITICAL: "critical"
    };

    return Backbone.Model.extend({
        idAttribute: "completeKey",
        isError: function() {
            return this.get("severity") === severity.CRITICAL || this.get("severity") === severity.MAJOR;
        }
    }, {
        severity: severity,
        severityOrder: [
            severity.UNDEFINED,
            severity.MINOR,
            severity.WARNING,
            severity.MAJOR,
            severity.CRITICAL
        ]
    });
});
