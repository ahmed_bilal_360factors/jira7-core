AJS.test.require([
    "com.atlassian.jira.jira-admin-updates-plugin:js-app"
], function() {
    "use strict";

    require([
        'underscore',
        'backbone',
        'admin-updates/services/update/clusterzdu'
    ], function (
        _,
        Backbone,
        ClusterZDUService
    ) {
        // Without this timeout, QUnit will wait indefinitely for async tests whose done() callback isn't called.
        QUnit.config.testTimeout = 5000;

        function resolvedPromise(value) {
            return new Promise(function(resolve) {
                resolve(value);
            });
        }

        function rejectedPromise(value) {
            return new Promise(function(resolve, reject) {
                reject(value);
            });
        }

        module("admin-updates/models/clusterstate", {
            setup: function () {
                this.listener = _.extend({}, Backbone.Events);

                this.sandbox = sinon.sandbox.create();
                this.sandbox.stub(ClusterZDUService, "startUpdate").returns(resolvedPromise());
                this.sandbox.stub(ClusterZDUService, "cancelUpdate").returns(resolvedPromise());
                this.sandbox.stub(ClusterZDUService, "finishUpdate").returns(resolvedPromise());
                this.sandbox.stub(ClusterZDUService, "clusterState").returns(resolvedPromise({state: "foo"}));

                this.context = AJS.test.mockableModuleContext();
                this.context.mock("admin-updates/services/update/clusterzdu", ClusterZDUService);

                this.ClusterState = this.context.require("admin-updates/models/clusterstate");
                this.model = new this.ClusterState();
                this.sandbox.spy(this.model, "fetch");
            },
            teardown: function() {
                this.sandbox.restore();
            }
        });

        test("fetch() should get the cluster state", function() {
            this.model.fetch();

            sinon.assert.calledOnce(ClusterZDUService.clusterState);
        });

        test("fetch() should set the internal state", function(assert) {
            assert.expect(1);
            var done = assert.async();

            this.model.fetch();

            Promise.resolve(ClusterZDUService.clusterState).then(function() {
                equal(this.model.get("state"), "foo");
                done();
            }.bind(this));
        });

        test("startUpdate() should start the update", function() {
            this.model.startUpdate();

            sinon.assert.calledOnce(ClusterZDUService.startUpdate);
        });

        test("startUpdate() should call fetch", function(assert) {
            assert.expect(2);
            var done = assert.async();

            this.model.startUpdate();

            Promise.all([
                ClusterZDUService.startUpdate,
                ClusterZDUService.clusterState
            ]).then(function() {
                sinon.assert.calledOnce(this.model.fetch);
                equal(this.model.get("state"), "foo");
                done();
            }.bind(this));
        });

        test("startUpdate() when the request fails because the transition is illegal, it should call fetch() to update state", function(assert) {
            assert.expect(2);
            var done = assert.async();
            ClusterZDUService.startUpdate.restore();
            this.sandbox.stub(ClusterZDUService, "startUpdate").returns(rejectedPromise({status: 409}));

            this.model.startUpdate();

            Promise.reject(ClusterZDUService.startUpdate).then(null, function() {
                Promise.resolve(ClusterZDUService.clusterState).then(function() {
                    sinon.assert.calledOnce(this.model.fetch);
                    equal(this.model.get("state"), "foo");
                    done();
                }.bind(this));
            }.bind(this));
        });

        test("startUpdate() when the request is rejected due to network/application error, it should emit a sync:error event (500 Internal Error)", function(assert) {
            assert.expect(3);
            var done = assert.async();
            ClusterZDUService.startUpdate.restore();
            this.sandbox.stub(ClusterZDUService, "startUpdate").returns(rejectedPromise({status: 500}));

            this.listener.listenTo(this.model, "sync:error", function(action, error, xhr) {
                equal(action, "update.error.start.title");
                equal(error, "update.error.network.application.text");
                equal(xhr.status, 500);
                done();
            });

            this.model.startUpdate();
        });

        test("startUpdate() when the request is rejected due to authorization error, it should emit a sync:error event (401 Unauthorized)", function(assert) {
            assert.expect(3);
            var done = assert.async();
            ClusterZDUService.startUpdate.restore();
            this.sandbox.stub(ClusterZDUService, "startUpdate").returns(rejectedPromise({status: 401}));

            this.listener.listenTo(this.model, "sync:error", function(action, error, xhr) {
                equal(action, "update.error.start.title");
                equal(error, "update.error.login.text");
                equal(xhr.status, 401);
                done();
            });

            this.model.startUpdate();
        });

        test("startUpdate() when the request is rejected due to permission error, it should emit a sync:error event (403 Forbidden)", function(assert) {
            assert.expect(3);
            var done = assert.async();
            ClusterZDUService.startUpdate.restore();
            this.sandbox.stub(ClusterZDUService, "startUpdate").returns(rejectedPromise({status: 403}));

            this.listener.listenTo(this.model, "sync:error", function(action, error, xhr) {
                equal(action, "update.error.start.title");
                equal(error, "update.error.permission.text");
                equal(xhr.status, 403);
                done();
            });

            this.model.startUpdate();
        });

        test("startUpdate() when the request is rejected due to state transition error, it should emit a sync:error event (409 Conflict)", function(assert) {
            assert.expect(3);
            var done = assert.async();
            ClusterZDUService.startUpdate.restore();
            this.sandbox.stub(ClusterZDUService, "startUpdate").returns(rejectedPromise({status: 409}));

            this.listener.listenTo(this.model, "sync:error", function(action, error, xhr) {
                equal(action, "update.error.start.title");
                equal(error, "update.error.transition.text");
                equal(xhr.status, 409);
                done();
            });

            this.model.startUpdate();
        });

        test("finishUpdate() should finish the update", function() {
            this.model.finishUpdate();

            sinon.assert.calledOnce(ClusterZDUService.finishUpdate);
        });

        test("finishUpdate() should call fetch", function(assert) {
            assert.expect(2);
            var done = assert.async();

            this.model.finishUpdate();

            Promise.all([
                ClusterZDUService.finishUpdate,
                ClusterZDUService.clusterState
            ]).then(function() {
                sinon.assert.calledOnce(this.model.fetch);
                equal(this.model.get("state"), "foo");
                done();
            }.bind(this));
        });

        test("finishUpdate() when the request fails because the transition is illegal, it should call fetch() to update state", function(assert) {
            assert.expect(2);
            var done = assert.async();
            ClusterZDUService.finishUpdate.restore();
            this.sandbox.stub(ClusterZDUService, "finishUpdate").returns(rejectedPromise({status: 409}));

            this.model.finishUpdate();

            Promise.reject(ClusterZDUService.finishUpdate).then(null, function() {
                Promise.resolve(ClusterZDUService.clusterState).then(function() {
                    sinon.assert.calledOnce(this.model.fetch);
                    equal(this.model.get("state"), "foo");
                    done();
                }.bind(this));
            }.bind(this));
        });

        test("finishUpdate() when the request is rejected due to network/application error, it should emit a sync:error event (500 Internal Error)", function(assert) {
            assert.expect(3);
            var done = assert.async();
            ClusterZDUService.finishUpdate.restore();
            this.sandbox.stub(ClusterZDUService, "finishUpdate").returns(rejectedPromise({status: 500}));

            this.listener.listenTo(this.model, "sync:error", function(action, error, xhr) {
                equal(action, "update.error.finish.title");
                equal(error, "update.error.network.application.text");
                equal(xhr.status, 500);
                done();
            });

            this.model.finishUpdate();
        });

        test("finishUpdate() when the request is rejected due to authorization error, it should emit a sync:error event (401 Unauthorized)", function(assert) {
            assert.expect(3);
            var done = assert.async();
            ClusterZDUService.finishUpdate.restore();
            this.sandbox.stub(ClusterZDUService, "finishUpdate").returns(rejectedPromise({status: 401}));

            this.listener.listenTo(this.model, "sync:error", function(action, error, xhr) {
                equal(action, "update.error.finish.title");
                equal(error, "update.error.login.text");
                equal(xhr.status, 401);
                done();
            });

            this.model.finishUpdate();
        });

        test("finishUpdate() when the request is rejected due to permission error, it should emit a sync:error event (403 Forbidden)", function(assert) {
            assert.expect(3);
            var done = assert.async();
            ClusterZDUService.finishUpdate.restore();
            this.sandbox.stub(ClusterZDUService, "finishUpdate").returns(rejectedPromise({status: 403}));

            this.listener.listenTo(this.model, "sync:error", function(action, error, xhr) {
                equal(action, "update.error.finish.title");
                equal(error, "update.error.permission.text");
                equal(xhr.status, 403);
                done();
            });

            this.model.finishUpdate();
        });

        test("finishUpdate() when the request is rejected due to state transition error, it should emit a sync:error event (409 Conflict)", function(assert) {
            assert.expect(3);
            var done = assert.async();
            ClusterZDUService.finishUpdate.restore();
            this.sandbox.stub(ClusterZDUService, "finishUpdate").returns(rejectedPromise({status: 409}));

            this.listener.listenTo(this.model, "sync:error", function(action, error, xhr) {
                equal(action, "update.error.finish.title");
                equal(error, "update.error.transition.text");
                equal(xhr.status, 409);
                done();
            });

            this.model.finishUpdate();
        });

        test("cancelUpdate() should cancel the update", function() {
            this.model.cancelUpdate();

            sinon.assert.calledOnce(ClusterZDUService.cancelUpdate);
        });

        test("cancelUpdate() should call fetch", function(assert) {
            assert.expect(2);
            var done = assert.async();

            this.model.cancelUpdate();

            Promise.all([
                ClusterZDUService.cancelUpdate,
                ClusterZDUService.clusterState
            ]).then(function() {
                sinon.assert.calledOnce(this.model.fetch);
                equal(this.model.get("state"), "foo");
                done();
            }.bind(this));
        });

        test("cancelUpdate() when the request fails because the transition is illegal, it should call fetch() to update state", function(assert) {
            assert.expect(2);
            var done = assert.async();
            ClusterZDUService.cancelUpdate.restore();
            this.sandbox.stub(ClusterZDUService, "cancelUpdate").returns(rejectedPromise({status: 409}));

            this.model.cancelUpdate();

            Promise.reject(ClusterZDUService.cancelUpdate).then(null, function() {
                Promise.resolve(ClusterZDUService.clusterState).then(function() {
                    sinon.assert.calledOnce(this.model.fetch);
                    equal(this.model.get("state"), "foo");
                    done();
                }.bind(this));
            }.bind(this));
        });

        test("cancelUpdate() when the request is rejected due to network/application error, it should emit a sync:error event (500 Internal Error)", function(assert) {
            assert.expect(3);
            var done = assert.async();
            ClusterZDUService.cancelUpdate.restore();
            this.sandbox.stub(ClusterZDUService, "cancelUpdate").returns(rejectedPromise({status: 500}));

            this.listener.listenTo(this.model, "sync:error", function(action, error, xhr) {
                equal(action, "update.error.cancel.title");
                equal(error, "update.error.network.application.text");
                equal(xhr.status, 500);
                done();
            });

            this.model.cancelUpdate();
        });

        test("cancelUpdate() when the request is rejected due to authorization error, it should emit a sync:error event (401 Unauthorized)", function(assert) {
            assert.expect(3);
            var done = assert.async();
            ClusterZDUService.cancelUpdate.restore();
            this.sandbox.stub(ClusterZDUService, "cancelUpdate").returns(rejectedPromise({status: 401}));

            this.listener.listenTo(this.model, "sync:error", function(action, error, xhr) {
                equal(action, "update.error.cancel.title");
                equal(error, "update.error.login.text");
                equal(xhr.status, 401);
                done();
            });

            this.model.cancelUpdate();
        });

        test("cancelUpdate() when the request is rejected due to permission error, it should emit a sync:error event (403 Forbidden)", function(assert) {
            assert.expect(3);
            var done = assert.async();
            ClusterZDUService.cancelUpdate.restore();
            this.sandbox.stub(ClusterZDUService, "cancelUpdate").returns(rejectedPromise({status: 403}));

            this.listener.listenTo(this.model, "sync:error", function(action, error, xhr) {
                equal(action, "update.error.cancel.title");
                equal(error, "update.error.permission.text");
                equal(xhr.status, 403);
                done();
            });

            this.model.cancelUpdate();
        });

        test("cancelUpdate() when the request is rejected due to state transition error, it should emit a sync:error event (409 Conflict)", function(assert) {
            assert.expect(3);
            var done = assert.async();
            ClusterZDUService.cancelUpdate.restore();
            this.sandbox.stub(ClusterZDUService, "cancelUpdate").returns(rejectedPromise({status: 409}));

            this.listener.listenTo(this.model, "sync:error", function(action, error, xhr) {
                equal(action, "update.error.cancel.title");
                equal(error, "update.error.transition.text");
                equal(xhr.status, 409);
                done();
            });

            this.model.cancelUpdate();
        });

        test("When the state changes, it should emit a change:state event", function(assert) {
            // For some reason, calling assert.async()'s done() callback doesn't count as an assert.
            assert.expect(0);

            // Don't worry though, this test will fail due to timeout if the assert.async() done callback isn't called.
            this.listener.listenTo(this.model, "change:state", assert.async());

            this.model.fetch();
        });

        test("It should have all cluster zdu states", function() {
            deepEqual(this.ClusterState.states, {
                STABLE: "STABLE",
                READY_TO_UPGRADE: "READY_TO_UPGRADE",
                MIXED: "MIXED",
                READY_TO_RUN_UPGRADE_TASKS: "READY_TO_RUN_UPGRADE_TASKS",
                RUNNING_UPGRADE_TASKS: "RUNNING_UPGRADE_TASKS"
            });
        });
    });
});
