AJS.test.require('com.atlassian.jira.jira-admin-updates-plugin:js-app', function() {
    'use strict';

    var jQuery = require('jquery');
    var Backbone = require('backbone');
    var _ = require('underscore');
    var ClusterStateModel = require('admin-updates/models/clusterstate');
    var Templates = require('admin-updates/templates');
    var ProductVersionsView = require('admin-updates/views/preupdate/productversions');

    module('admin-updates/views/preupdate/productversions', {
        setup: function() {
            this.sandbox = sinon.sandbox.create();

            jQuery('#qunit-fixture').html(Templates.productVersions({expanded: true}));

            this.listener = _.extend({}, Backbone.Events);

            this.view = new ProductVersionsView({
                el: '#qunit-fixture',
                model: new ClusterStateModel({state: ClusterStateModel.states.STABLE})
            });
            this.view.bindUIElements();
        },
        teardown: function() {
            this.sandbox.restore();
        }
    });

    test("It emits an event when the 'Download latest' link is clicked", function() {
        var callback = this.sandbox.stub();
        this.listener.listenTo(this.view, 'click:download-latest', callback);

        this.view.ui.downloadLatestLink.click();

        sinon.assert.calledOnce(callback);
    });
});
