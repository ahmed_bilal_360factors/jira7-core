define("admin-updates/views/update/start", ['require'], function (require) {
    "use strict";

    var Marionette = require('marionette');
    var Templates = require('admin-updates/templates');
    var ClusterStateModel = require('admin-updates/models/clusterstate');

    return Marionette.Layout.extend({
        template: Templates.startUpdate,
        ui: {
            button: "#start-update-button"
        },
        events: {
            "click @ui.button": function(e) {
                e.preventDefault();
                this.trigger("update:start");
            }
        },
        initialize: function() {
            this.listenTo(this.model, "change:state", function() {
                if(this._shouldShowButton()) {
                    this.enableStartButton();
                } else {
                    this.disableStartButton();
                }
            });
        },
        /**
         * Serializes the data that feeds into the template
         * @returns {{activeStartButton: (boolean)}}
         */
        serializeData: function() {
            return {
                activeStartButton: this._shouldShowButton()
            };
        },
        /**
         * Whether or not to show the 'start' button based on the model state
         * @returns {boolean}
         * @private
         */
        _shouldShowButton: function() {
            switch(this.model.get("state")) {
                case ClusterStateModel.states.STABLE:
                    return true;
                default:
                    return false;
            }
        },
        /**
         * Enable/disable a button
         * @param {boolean} enable
         * @private
         */
        _toggleButton: function(enable) {
            var disabled = !enable;
            this.ui.button.prop("disabled", disabled);
            this.ui.button.attr("aria-disabled", disabled.toString());
        },
        enableStartButton: function() {
            this._toggleButton(true);
        },
        disableStartButton: function() {
            this._toggleButton(false);
        }
    });
});
