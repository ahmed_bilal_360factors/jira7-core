AJS.test.require([
    "com.atlassian.jira.jira-admin-updates-plugin:js-app"
], function() {
    "use strict";

    require(['jquery', 'underscore', 'backbone'], function (jQuery, _, Backbone) {
        module("admin-updates/modules/updates", {
            setup: function () {
                this.sandbox = sinon.sandbox.create();

                this.makeRequest = this.sandbox.stub();
                this.makeRequest
                    .returns(new jQuery.Deferred().resolve());
                this.makeRequest.withArgs(sinon.match({url: sinon.match("state")}))
                    .returns(new jQuery.Deferred().resolve({state: "foo"}));

                this.SmartAjax = {
                    makeRequest: this.makeRequest
                };

                this.context = AJS.test.mockableModuleContext();
                this.context.mock("jira/ajs/ajax/smart-ajax", this.SmartAjax);

                this.Updates = this.context.require("admin-updates/modules/updates");

                this.fixture = jQuery("#qunit-fixture");

                this.fixture.html(JIRA.Templates.Admin.Updates.Templates.body({
                    expandPreUpdateTasks: true,
                    activeStartButton: true,
                    activeFinishButton: false,
                    activeCancelButton: false,
                    showWarningMessage: false
                }));

                this.module = new this.Updates({
                    el: this.fixture,
                    clusterState: 'foo'
                });

                this.listener = _.extend({}, Backbone.Events);

                this.sandbox.spy(this.module.clusterStateModel, "startUpdate");
                this.sandbox.spy(this.module.clusterStateModel, "finishUpdate");
                this.sandbox.spy(this.module.clusterStateModel, "cancelUpdate");
            },
            teardown: function() {
                this.sandbox.restore();
            }
        });

        test("It should start the update when update:start is triggered", function() {
            this.module.updateViewManager.trigger("update:start");

            sinon.assert.calledOnce(this.module.clusterStateModel.startUpdate);
        });

        test("It should finish the update when update:finish is triggered", function() {
            this.module.updateViewManager.trigger("update:finish");

            sinon.assert.calledOnce(this.module.clusterStateModel.finishUpdate);
        });

        test("It should cancel the update when update:cancel is triggered", function() {
            this.module.updateViewManager.trigger("update:cancel");

            sinon.assert.calledOnce(this.module.clusterStateModel.cancelUpdate);
        });

        test("It should display a flag when sync:error is triggered", function() {
            this.module.clusterStateModel.trigger("sync:error", "foo", "bar");

            var $flag = jQuery('.aui-flag');
            ok($flag.is(':visible'));
            ok($flag.text().indexOf("foo") > -1);
            ok($flag.text().indexOf("bar") > -1);
        });

        test("initialize() should use the element from options to render itself", function() {
            equal(this.module.el, this.fixture);
        });

        test("initialize() should initialise the pre-update tasks controller", function() {
            equal(this.module.preUpdateViewManager.el, this.module.el.find('#pre-update-tasks-container').get(0));
        });

        test("initialize() should initialise the update view manager", function() {
            equal(this.module.updateViewManager.el, this.module.el.find('#update-controls-container').get(0));
        });

        test("It emits an event when the user clicks the introduction documentation link", function() {
            var callback = this.sandbox.stub();
            this.listener.listenTo(this.module, 'click:documentation', callback);

            this.module.pageIntroView.trigger("click:documentation");

            sinon.assert.calledOnce(callback);
        });

        test("It emits an event when the user views their system's health checks", function() {
            var callback = this.sandbox.stub();
            this.listener.listenTo(this.module, "click:health-check", callback);

            this.module.preUpdateViewManager.trigger("click:health-check");

            sinon.assert.calledOnce(callback);
        });

        test("It emits an event when the user checks the compatibility of their addons", function() {
            var callback = this.sandbox.stub();
            this.listener.listenTo(this.module, "click:check-addons", callback);

            this.module.preUpdateViewManager.trigger("click:check-addons");

            sinon.assert.calledOnce(callback);
        });

        test("It emits an event when the user follows the link to download the latest JIRA", function() {
            var callback = this.sandbox.stub();
            this.listener.listenTo(this.module, "click:download-latest", callback);

            this.module.preUpdateViewManager.trigger("click:download-latest");

            sinon.assert.calledOnce(callback);
        });

        test("It emits an event when the user opens the dialog to begin an update", function() {
            var callback = this.sandbox.stub();
            this.listener.listenTo(this.module, "update:start:dialog", callback);

            this.module.updateViewManager.trigger("update:start:dialog");

            sinon.assert.calledOnce(callback);
        });

        test("It emits an event when the user confirms their choice to begin an update", function() {
            var callback = this.sandbox.stub();
            this.listener.listenTo(this.module, "update:start:confirm", callback);

            this.module.updateViewManager.trigger("update:start");

            sinon.assert.calledOnce(callback);
        });

        test("It emits an event when the user opens the dialog to complete an update", function() {
            var callback = this.sandbox.stub();
            this.listener.listenTo(this.module, "update:finish:dialog", callback);

            this.module.updateViewManager.trigger("update:finish:dialog");

            sinon.assert.calledOnce(callback);
        });

        test("It emits an event when the user confirms their choice to complete an update", function() {
            var callback = this.sandbox.stub();
            this.listener.listenTo(this.module, "update:finish:confirm", callback);

            this.module.updateViewManager.trigger("update:finish");

            sinon.assert.calledOnce(callback);
        });

        test("It emits an event when the user opens the dialog to cancel an update", function() {
            var callback = this.sandbox.stub();
            this.listener.listenTo(this.module, "update:cancel:dialog", callback);

            this.module.updateViewManager.trigger("update:cancel:dialog");

            sinon.assert.calledOnce(callback);
        });

        test("It emits an event when the user confirms their choice to cancel an update", function() {
            var callback = this.sandbox.stub();
            this.listener.listenTo(this.module, "update:cancel:confirm", callback);

            this.module.updateViewManager.trigger("update:cancel");

            sinon.assert.calledOnce(callback);
        });

        test("It emits an event when the user clicks the information tooltip (including cluster state)", function() {
            this.module.clusterStateModel.set("state", "STABLE");
            var callback = this.sandbox.stub();
            this.listener.listenTo(this.module, "click:info-tooltip", callback);

            this.module.updateViewManager.trigger("click:info-tooltip");

            sinon.assert.calledOnce(callback);
            sinon.assert.calledWith(callback, {clusterState: "STABLE"});
        });
    });
});
