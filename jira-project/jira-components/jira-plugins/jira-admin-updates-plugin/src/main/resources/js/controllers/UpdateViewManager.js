define("admin-updates/controllers/updateviewmanager", ['require'], function (require) {
    "use strict";

    var Marionette = require('marionette');
    var StartUpdateView = require('admin-updates/views/update/start');
    var FinishUpdateView = require('admin-updates/views/update/finish');
    var WarningMessageView = require('admin-updates/views/update/warningmessage');
    var ClusterStateView = require('admin-updates/views/update/clusterstate');
    var ConfirmDialogView = require('admin-updates/views/update/confirmdialog');
    var InfoInlineDialogView = require('admin-updates/views/update/infoinlinedialog');
    var formatter = require('jira/util/formatter');

    return Marionette.Layout.extend({
        regions: {
            startUpdate: "#start-update-container",
            finishUpdate: "#finish-update-container",
            warningMessage: "#warning-message-container",
            clusterState: "#cluster-state-container",
            dialogContainer: "#updates-dialog-container",
            inlineDialogContainer: "#info-inline-dialog-container"
        },
        /**
         * @param {Backbone.Model} options.model
         * @param {jQuery} options.startDialog.el
         * @param {jQuery} options.finishDialog.el
         * @param {jQuery} options.cancelDialog.el
         */
        initialize: function(options) {
            this.startDialogElement = options.startDialog.el;
            this.finishDialogElement = options.finishDialog.el;
            this.cancelDialogElement = options.cancelDialog.el;
            this.infoInlineDialogElement = options.infoInlineDialog.el;

            this.startUpdate.show(new StartUpdateView({model: this.model}));
            this.finishUpdate.show(new FinishUpdateView({model: this.model}));
            this.warningMessage.show(new WarningMessageView({model: this.model}));
            this.clusterState.show(new ClusterStateView({model: this.model}));
            this._setupInfoInlineDialog();

            this.listenTo(this.startUpdate.currentView, {
                "update:start": function() {
                    this._setupStartDialog();
                    this.trigger("update:start:dialog");
                }
            });

            this.listenTo(this.finishUpdate.currentView, {
                "update:finish": function() {
                    this._setupFinishDialog();
                    this.trigger("update:finish:dialog");
                },
                "update:cancel": function() {
                    this._setupCancelDialog();
                    this.trigger("update:cancel:dialog");
                },
                "click:info-tooltip": function() {this.trigger('click:info-tooltip');}
            });
        },
        _setupStartDialog: function() {
            this._setupConfirmDialog(
                new ConfirmDialogView({
                    el: this.startDialogElement,
                    dialogId: 'start-dialog',
                    titleText: formatter.I18n.getText('update.start.dialog.title'),
                    content: formatter.I18n.getText('update.start.dialog.text'),
                    contentExtended: formatter.I18n.getText('update.start.dialog.text.extended')
                }),
                this.trigger.bind(this, "update:start"),
                this.dialogContainer.close
            );
        },
        _setupFinishDialog: function() {
            this._setupConfirmDialog(
                new ConfirmDialogView({
                    el: this.finishDialogElement,
                    dialogId: 'finish-dialog',
                    titleText: formatter.I18n.getText('update.finish.dialog.title'),
                    content: formatter.I18n.getText('update.finish.dialog.text'),
                    contentExtended: formatter.I18n.getText('update.finish.dialog.text.extended')
                }),
                this.trigger.bind(this, "update:finish"),
                this.dialogContainer.close
            );
        },
        _setupCancelDialog: function() {
            this._setupConfirmDialog(
                new ConfirmDialogView({
                    el: this.cancelDialogElement,
                    dialogId: 'cancel-dialog',
                    titleText: formatter.I18n.getText('update.cancel.dialog.title'),
                    content: formatter.I18n.getText('update.cancel.dialog.text'),
                    contentExtended: formatter.I18n.getText('update.cancel.dialog.text.extended')
                }),
                this.trigger.bind(this, "update:cancel"),
                this.dialogContainer.close
            );
        },
        /**
         * Set up a confirm dialog
         * @see {@link admin-updates/views/update/confirmdialog}
         *
         * @param {Backbone.View} view
         * @param {function} confirmCallback
         * @param {function} hideCallback
         * @private
         */
        _setupConfirmDialog: function(view, confirmCallback, hideCallback) {
            this.dialogContainer.show(view);
            this.listenTo(this.dialogContainer.currentView, "dialog:confirm", confirmCallback);
            this.listenTo(this.dialogContainer.currentView, "dialog:hide", hideCallback);
        },
        _setupInfoInlineDialog: function() {
            this.inlineDialogContainer.show(new InfoInlineDialogView({
                el: this.infoInlineDialogElement,
                model: this.model,
                dialogId: 'info-inline-dialog'
            }));
        }
    });
});
