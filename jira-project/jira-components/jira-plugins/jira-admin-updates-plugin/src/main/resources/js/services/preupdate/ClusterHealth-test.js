AJS.test.require([
    "com.atlassian.jira.jira-admin-updates-plugin:js-app"
], function() {
    "use strict";

    require([
        'jquery',
        'wrm/context-path'
    ], function(
        jQuery,
        contextPath
    ) {
        // Without this timeout, QUnit will wait indefinitely for async tests whose done() callback isn't called.
        QUnit.config.testTimeout = 5000;

        module("admin-updates/services/preupdate/clusterhealth", {
            setup: function() {
                this.sandbox = sinon.sandbox.create();
                this.SmartAjax = {
                    makeRequest: this.sandbox.stub().returns(new jQuery.Deferred().resolve())
                };
                this.context = AJS.test.mockableModuleContext();
                this.context.mock("jira/ajs/ajax/smart-ajax", this.SmartAjax);

                this.ClusterHealth = this.context.require("admin-updates/services/preupdate/clusterhealth");

                this.NOOP = function() {notOk(true, "NOOP should never be called.");};
            },
            teardown: function() {
                this.sandbox.restore();
            }
        });

        test("checkDetails() should do a GET request", function() {
            this.ClusterHealth.checkDetails();

            sinon.assert.calledWith(this.SmartAjax.makeRequest, sinon.match({type: 'GET'}));
        });

        test("checkDetails() should hit /rest/supportHealthCheck/1.0/checkDetails", function() {
            var expectedUrl = contextPath() + '/rest/supportHealthCheck/1.0/checkDetails';

            this.ClusterHealth.checkDetails();

            sinon.assert.calledWith(this.SmartAjax.makeRequest, sinon.match({url: expectedUrl}));
        });

        test("checkDetails() should return a Promise (resolve)", function(assert) {
            assert.expect(1);
            var done = assert.async();

            var promise = this.ClusterHealth.checkDetails();

            promise.then(function() {
                ok(true);
                done();
            });
        });

        test("checkDetails() should return a Promise (reject)", function(assert) {
            this.SmartAjax.makeRequest = this.sandbox.stub().returns(new jQuery.Deferred().reject());
            assert.expect(1);
            var done = assert.async();

            var promise = this.ClusterHealth.checkDetails();

            promise.then(this.NOOP, function() {
                ok(true);
                done();
            });
        });
    });
});
