define('admin-updates/modules/analytics', ['require'], function(require) {
    'use strict';

    var logger = require('jira/util/logger');
    var analytics = require('jira/analytics');

    return {
        recordInitialPageLoad: function(data) {
            if (!data || !data.clusterState) {
                logger.error("page-load event doesn't include cluster state data");
                return;
            }

            analytics.send({
                name: 'zdu.admin-updates-ui.page-load',
                properties: {clusterState: data.clusterState}
            });
        },
        recordIntroDocumentationClick: function() {
            analytics.send({name: 'zdu.admin-updates-ui.documentation'});
        },
        recordViewHealthChecksClick: function() {
            analytics.send({name: 'zdu.admin-updates-ui.pre-update.view-health-checks'});
        },
        recordCheckAddonsClick: function() {
            analytics.send({name: 'zdu.admin-updates-ui.pre-update.check-addons'});
        },
        recordDownloadLatestClick: function() {
            analytics.send({name: 'zdu.admin-updates-ui.pre-update.download-latest'});
        },
        recordBeginUpdateDialog: function() {
            analytics.send({name: 'zdu.admin-updates-ui.dialog-begin-update'});
        },
        recordBeginUpdateConfirm: function() {
            analytics.send({name: 'zdu.admin-updates-ui.confirm-begin-update'});
        },
        recordCompleteUpdateDialog: function() {
            analytics.send({name: 'zdu.admin-updates-ui.dialog-complete-update'});
        },
        recordCompleteUpdateConfirm: function() {
            analytics.send({name: 'zdu.admin-updates-ui.confirm-complete-update'});
        },
        recordCancelUpdateDialog: function() {
            analytics.send({name: 'zdu.admin-updates-ui.dialog-cancel-update'});
        },
        recordCancelUpdateConfirm: function() {
            analytics.send({name: 'zdu.admin-updates-ui.confirm-cancel-update'});
        },
        recordInfoTooltipClick: function(data) {
            if (!data || !data.clusterState) {
                logger.error("info-tooltip event doesn't include cluster state data");
                return;
            }

            analytics.send({
                name: 'zdu.admin-updates-ui.info-tooltip',
                properties: {clusterState: data.clusterState}
            });
        }
    };
});
