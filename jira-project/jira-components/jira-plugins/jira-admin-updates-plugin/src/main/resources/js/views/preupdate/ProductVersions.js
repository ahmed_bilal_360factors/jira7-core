define("admin-updates/views/preupdate/productversions", ['require'], function (require) {
    "use strict";

    var Marionette = require('marionette');
    var Templates = require('admin-updates/templates');
    var ClusterStateModel = require('admin-updates/models/clusterstate');

    return Marionette.Layout.extend({
        template: Templates.productVersions,
        ui: {
            content: "#product-versions-content",
            downloadLatestLink: "#product-versions-download-latest-link"
        },
        triggers: {
            "click @ui.downloadLatestLink": {
                event: "click:download-latest",
                preventDefault: false
            }
        },
        initialize: function() {
            this.listenTo(this.model, "change:state", function() {
                if(this._shouldExpandContent()) {
                    this.expandContent();
                } else {
                    this.collapseContent();
                }
            });
        },
        serializeData: function() {
            return {
                expanded: this._shouldExpandContent()
            };
        },
        _shouldExpandContent: function() {
            return this.model.get("state") === ClusterStateModel.states.STABLE;
        },
        expandContent: function() {
            this.ui.content.removeClass('hidden');
        },
        collapseContent: function() {
            this.ui.content.addClass('hidden');
        }
    });
});
