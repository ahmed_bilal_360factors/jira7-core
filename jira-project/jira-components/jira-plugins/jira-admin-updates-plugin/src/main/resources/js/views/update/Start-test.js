AJS.test.require([
    "com.atlassian.jira.jira-admin-updates-plugin:js-app"
], function() {
    "use strict";

    require([
        'underscore',
        'backbone',
        'admin-updates/views/update/start',
        'admin-updates/models/clusterstate'
    ], function (
        _,
        Backbone,
        Start,
        ClusterStateModel
    ) {
        // Without this timeout, QUnit will wait indefinitely for async tests whose done() callback isn't called.
        QUnit.config.testTimeout = 5000;

        module("admin-updates/views/update/start", {
            setup: function () {
                this.listener = _.extend({}, Backbone.Events);

                this.start = new Start({
                    el: '#qunit-fixture',
                    model: new ClusterStateModel({state: ClusterStateModel.states.STABLE})
                });
                this.start.render();
            }
        });

        test("clicking the button triggers the update:start event", function(assert) {
            assert.expect(0);
            var done = assert.async();
            this.listener.listenTo(this.start, "update:start", done);

            this.start.ui.button.click();
        });

        test("enableStartButton() enables the button", function () {
            this.start.ui.button.prop("disabled", true);
            this.start.enableStartButton();

            notOk(this.start.ui.button.prop("disabled"));
        });

        test("disableStartButton() disables the button", function () {
            this.start.ui.button.prop("disabled", false);
            this.start.disableStartButton();

            ok(this.start.ui.button.prop("disabled"));
        });
    });
});
