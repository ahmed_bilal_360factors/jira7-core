define("admin-updates/views/update/finish", ['require'], function (require) {
    "use strict";

    var Marionette = require('marionette');
    var Templates = require('admin-updates/templates');
    var ClusterStateModel = require('admin-updates/models/clusterstate');

    return Marionette.Layout.extend({
        template: Templates.finishUpdate,
        ui: {
            finishButton: "#finish-update-button",
            cancelButton: "#cancel-update-button",
            infoButton: "#info-button"
        },
        events: {
            "click @ui.finishButton": function(e) {
                e.preventDefault();
                this.trigger("update:finish");
            },
            "click @ui.cancelButton": function(e) {
                e.preventDefault();
                this.trigger("update:cancel");
            },
            "click @ui.infoButton": function(e) {
                e.preventDefault();
                this.trigger("click:info-tooltip");
            }
        },
        initialize: function() {
            this.listenTo(this.model, "change:state", function() {
                if(this._shouldShowFinishButton()) {
                    this.enableFinishButton();
                } else {
                    this.disableFinishButton();
                }

                if(this._shouldShowCancelButton()) {
                    this.enableCancelButton();
                } else {
                    this.disableCancelButton();
                }
            });
        },
        /**
         * Serializes the data that feeds into the template
         * @returns {{activeFinishButton: (boolean), activeCancelButton: (boolean)}}
         */
        serializeData: function() {
            return {
                activeFinishButton: this._shouldShowFinishButton(),
                activeCancelButton: this._shouldShowCancelButton()
            };
        },
        /**
         * Whether or not to show the 'finish' button based on the model state
         * @returns {boolean}
         * @private
         */
        _shouldShowFinishButton: function() {
            switch(this.model.get("state")) {
                case ClusterStateModel.states.READY_TO_RUN_UPGRADE_TASKS:
                    return true;
                default:
                    return false;
            }
        },
        /**
         * Whether or not to show the 'cancel' button based on the model state
         * @returns {boolean}
         * @private
         */
        _shouldShowCancelButton: function() {
            switch(this.model.get("state")) {
                case ClusterStateModel.states.READY_TO_UPGRADE:
                    return true;
                default:
                    return false;
            }
        },
        /**
         * Enable/disable a button
         * @param {jQuery} button
         * @param {boolean} enable
         * @private
         */
        _toggleButton: function(button, enable) {
            var disabled = !enable;
            button.prop("disabled", disabled);
            button.attr("aria-disabled", disabled.toString());
        },
        enableFinishButton: function() {
            this._toggleButton(this.ui.finishButton, true);
        },
        disableFinishButton: function() {
            this._toggleButton(this.ui.finishButton, false);
        },
        enableCancelButton: function() {
            this._toggleButton(this.ui.cancelButton, true);
        },
        disableCancelButton: function() {
            this._toggleButton(this.ui.cancelButton, false);
        }
    });
});
