AJS.test.require([
    "com.atlassian.jira.jira-admin-updates-plugin:js-app"
], function() {
    "use strict";

    require([
        'jquery',
        'underscore',
        'backbone',
        'admin-updates/models/healthcheckcollection',
        'admin-updates/models/healthcheck',
        'admin-updates/models/clusterstate',
        'admin-updates/templates'
    ], function (
        jQuery,
        _,
        Backbone,
        HealthCheckCollection,
        HealthCheckModel,
        ClusterStateModel,
        Templates
    ) {
        // Without this timeout, QUnit will wait indefinitely for async tests whose done() callback isn't called.
        QUnit.config.testTimeout = 5000;

        function createHC(severity, name) {
            return {
                severity: severity,
                name: name,
                completeKey: severity + "." + name,
                isHealthy: severity === HealthCheckModel.severity.UNDEFINED,
                failureReason: name + " reason"
            };
        }

        module("admin-updates/views/preupdate/clusterhealth", {
            setup: function () {
                this.sandbox = sinon.sandbox.create();

                this.listener = _.extend({}, Backbone.Events);

                this.fixture = jQuery('#qunit-fixture');

                this.healthChecks = {
                    critical: createHC(HealthCheckModel.severity.CRITICAL, "critical"),
                    major: createHC(HealthCheckModel.severity.MAJOR, "major"),
                    warning: createHC(HealthCheckModel.severity.WARNING, "warning"),
                    minor: createHC(HealthCheckModel.severity.MINOR, "minor"),
                    success: createHC(HealthCheckModel.severity.UNDEFINED, "undefined")
                };

                this.model = new ClusterStateModel({state: ClusterStateModel.states.STABLE});

                this.collection = new HealthCheckCollection([
                    this.healthChecks.success,
                    this.healthChecks.minor,
                    this.healthChecks.warning,
                    this.healthChecks.major,
                    this.healthChecks.critical
                ]);

                this.sandbox.stub(this.collection, "fetch");

                this.context = AJS.test.mockableModuleContext();

                this.context.mock("admin-updates/templates", Templates);
                var ClusterHealth = this.context.require("admin-updates/views/preupdate/clusterhealth");

                this.clusterHealth = new ClusterHealth({
                    el: this.fixture,
                    model: this.model,
                    collection: this.collection
                });

                this.clusterHealth.render();

                this.sandbox.spy(this.clusterHealth, "expandContent");
                this.sandbox.spy(this.clusterHealth, "collapseContent");
                this.sandbox.spy(this.clusterHealth, "_getAggregatedText");
                this.sandbox.spy(Templates, "spinner");
            },
            teardown: function() {
                this.sandbox.restore();
            }
        });

        test("It should listen to change:state from model and run health checks", function () {
            this.model.trigger("change:state");

            sinon.assert.calledOnce(this.collection.fetch);
        });

        test("It should listen to change:state from model and expand content (STABLE)", function () {
            this.model.trigger("change:state");

            sinon.assert.calledOnce(this.clusterHealth.expandContent);
        });

        test("It should listen to change:state from model and collapse content (READY_TO_UPGRADE)", function () {
            this.model.set("state", ClusterStateModel.states.READY_TO_UPGRADE);

            sinon.assert.calledOnce(this.clusterHealth.collapseContent);
        });

        test("It should listen to change:state from model and collapse content (MIXED)", function () {
            this.model.set("state", ClusterStateModel.states.MIXED);

            sinon.assert.calledOnce(this.clusterHealth.collapseContent);
        });

        test("It should listen to change:state from model and collapse content (READY_TO_RUN_UPGRADE_TASKS)", function () {
            this.model.set("state", ClusterStateModel.states.READY_TO_RUN_UPGRADE_TASKS);

            sinon.assert.calledOnce(this.clusterHealth.collapseContent);
        });

        test("It should listen to change:state from model and collapse content (RUNNING_UPGRADE_TASKS)", function () {
            this.model.set("state", ClusterStateModel.states.RUNNING_UPGRADE_TASKS);

            sinon.assert.calledOnce(this.clusterHealth.collapseContent);
        });

        test("It should listen to healthcheck:started from collection and show spinner", function () {
            this.collection.trigger("healthcheck:started");

            sinon.assert.calledOnce(Templates.spinner);
        });

        test("It should listen to healthcheck:finished from collection and hide spinner", function () {
            notOk(this.clusterHealth.ui.waitContainer.hasClass("hidden"));

            this.collection.trigger("healthcheck:finished");

            ok(this.clusterHealth.ui.waitContainer.hasClass("hidden"));
        });

        test("It should listen to healthcheck:finished from collection and trigger change:icon (warning)", function (assert) {
            assert.expect(1);
            var done = assert.async();
            this.listener.listenTo(this.clusterHealth, "change:icon", function(icon) {
                equal(icon, "zdu-icon-warning");
                done();
            });

            this.collection.trigger("healthcheck:finished");
        });

        test("It should listen to healthcheck:finished from collection and trigger change:icon (success)", function (assert) {
            assert.expect(1);
            var done = assert.async();
            this.sandbox.stub(this.collection, "areAllChecksHealthy").returns(true);
            this.listener.listenTo(this.clusterHealth, "change:icon", function(icon) {
                equal(icon, "zdu-icon-success");
                done();
            });

            this.collection.trigger("healthcheck:finished");
        });

        test("It should listen to healthcheck:finished from collection and show results (warning)", function () {
            this.collection.trigger("healthcheck:finished");

            notOk(this.clusterHealth.ui.detailContainer.hasClass("hidden"));
            notOk(this.clusterHealth.ui.aggregateContainer.hasClass("hidden"));
        });

        test("It should listen to healthcheck:finished from collection and show results (success)", function () {
            this.sandbox.stub(this.collection, "areAllChecksHealthy").returns(true);

            this.collection.trigger("healthcheck:finished");

            notOk(this.clusterHealth.ui.detailContainer.hasClass("hidden"));
            ok(this.clusterHealth.ui.aggregateContainer.hasClass("hidden"));
        });

        test("The detailed health check should be the most severe issue", function () {
            this.collection.trigger("healthcheck:finished");

            ok(this.clusterHealth.ui.detailContainer.text().indexOf(this.healthChecks.critical.name) > -1);
        });

        test("The detailed health check should show the right icon (error)", function () {
            this.collection.trigger("healthcheck:finished");

            ok(this.clusterHealth.ui.detailContainer.find('.aui-icon').attr('class').indexOf("error") > -1);
        });

        test("The detailed health check should show the right icon (warning)", function () {
            this.collection.reset();
            this.collection.add(createHC(HealthCheckModel.severity.WARNING, "warning"));

            this.collection.trigger("healthcheck:finished");

            ok(this.clusterHealth.ui.detailContainer.find('.aui-icon').attr('class').indexOf("warning") > -1);
        });

        test("The aggregated health check should show the right number of checks (0 errors, 2 warnings)", function () {
            this.sandbox.stub(this.collection, "getMostSevereHealthCheck").returns(
                new HealthCheckModel(createHC(HealthCheckModel.severity.MAJOR, "major"))
            );
            this.collection.reset();
            this.collection.add([
                createHC(HealthCheckModel.severity.WARNING, "warning1"),
                createHC(HealthCheckModel.severity.WARNING, "warning2")
            ]);

            this.collection.trigger("healthcheck:finished");

            var text = this.clusterHealth.ui.aggregateContainer.text();
            sinon.assert.calledWith(this.clusterHealth._getAggregatedText, 0, 2);
            ok(text.indexOf('preupdate.instancehealth.checks.warnings.text') > -1);
            ok(text.indexOf('preupdate.instancehealth.checks.error') === -1);
        });

        test("The aggregated health check should show the right number of checks (0 errors, 1 warning)", function () {
            this.sandbox.stub(this.collection, "getMostSevereHealthCheck").returns(
                new HealthCheckModel(createHC(HealthCheckModel.severity.MAJOR, "major"))
            );
            this.collection.reset();
            this.collection.add(createHC(HealthCheckModel.severity.WARNING, "warning"));

            this.collection.trigger("healthcheck:finished");

            var text = this.clusterHealth.ui.aggregateContainer.text();
            sinon.assert.calledWith(this.clusterHealth._getAggregatedText, 0, 1);
            ok(text.indexOf('preupdate.instancehealth.checks.warning.text') > -1);
            ok(text.indexOf('preupdate.instancehealth.checks.error') === -1);
        });

        test("The aggregated health check should show the right number of checks (1 errors, 0 warning)", function () {
            this.sandbox.stub(this.collection, "getMostSevereHealthCheck").returns(
                new HealthCheckModel(createHC(HealthCheckModel.severity.CRITICAL, "critical"))
            );
            this.collection.reset();
            this.collection.add(createHC(HealthCheckModel.severity.MAJOR, "major"));

            this.collection.trigger("healthcheck:finished");

            var text = this.clusterHealth.ui.aggregateContainer.text();
            sinon.assert.calledWith(this.clusterHealth._getAggregatedText, 1, 0);
            ok(text.indexOf('preupdate.instancehealth.checks.error.text') > -1);
            ok(text.indexOf('preupdate.instancehealth.checks.warning') === -1);
        });

        test("The aggregated health check should show the right number of checks (2 errors, 2 warning)", function () {
            this.sandbox.stub(this.collection, "getMostSevereHealthCheck").returns(
                new HealthCheckModel(createHC(HealthCheckModel.severity.CRITICAL, "critical"))
            );
            this.collection.reset();
            this.collection.add(createHC(HealthCheckModel.severity.MAJOR, "major1"));
            this.collection.add(createHC(HealthCheckModel.severity.MAJOR, "major2"));
            this.collection.add(createHC(HealthCheckModel.severity.WARNING, "warning1"));
            this.collection.add(createHC(HealthCheckModel.severity.WARNING, "warning2"));

            this.collection.trigger("healthcheck:finished");

            var text = this.clusterHealth.ui.aggregateContainer.text();
            sinon.assert.calledWith(this.clusterHealth._getAggregatedText, 2, 2);
            ok(text.indexOf('preupdate.instancehealth.checks.errors.text') > -1);
            ok(text.indexOf('preupdate.instancehealth.checks.warnings.text') > -1);
        });

        test("The aggregated health check should be hidden if there are none left", function () {
            this.sandbox.stub(this.collection, "getMostSevereHealthCheck").returns(
                new HealthCheckModel(createHC(HealthCheckModel.severity.MAJOR, "major"))
            );
            this.collection.reset();

            this.collection.trigger("healthcheck:finished");

            ok(this.clusterHealth.ui.aggregateContainer.is(":empty"));
        });

        test("It emits an event when the health check link is clicked", function() {
            var callback = this.sandbox.stub();
            this.listener.listenTo(this.clusterHealth, "click:health-check", callback);

            this.clusterHealth.ui.healthCheckLink.click();

            sinon.assert.calledOnce(callback);
        });
    });
});
