define("admin-updates/views/update/confirmdialog", ['require'], function (require) {
    "use strict";

    var _ = require('underscore');
    var Marionette = require('marionette');
    var Templates = require('admin-updates/templates');

    return Marionette.ItemView.extend({
        template: Templates.confirmDialog,
        ui: {
            confirmButton: "#dialog-confirm-button",
            cancelButton: "#dialog-cancel-button"
        },
        /**
         * @param {boolean} options.hide Whether or not to hide the dialog upon creation
         * @param {string} options.dialogId Unique ID for the dialog
         * @param {string} options.titleText Title text for the dialog header
         * @param {string} options.content Content text for the dialog body
         * @param {string} options.contentExtended More content text for the dialog body
         */
        initialize: function(options) {
            options = _.extend({}, options);
            this.hide = Boolean(options.hide);
            this.dialogId = options.dialogId;
            this.titleText = options.titleText;
            this.content = options.content;
            this.contentExtended = options.contentExtended;
        },
        /**
         * Serializes the data that feeds into the template
         * @returns {{id: (string), titleText: (string), content: (string), contentExtended: (string)}}
         */
        serializeData: function() {
            return {
                id: this.dialogId,
                titleText: this.titleText,
                content: this.content,
                contentExtended: this.contentExtended
            };
        },
        onShow: function() {
            this.dialog = AJS.dialog2(document.getElementById(this.dialogId));

            if(!this.hide) {
                this.dialog.show();
            }

            this.dialog.on("hide", this.trigger.bind(this, "dialog:hide"));

            this.ui.confirmButton.click(function() {
                this.dialog.hide();
                this.trigger("dialog:confirm");
            }.bind(this));

            this.ui.cancelButton.click(function() {
                this.dialog.hide();
            }.bind(this));
        }
    });
});
