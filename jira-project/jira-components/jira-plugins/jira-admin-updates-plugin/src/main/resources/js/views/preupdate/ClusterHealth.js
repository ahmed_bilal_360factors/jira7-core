define("admin-updates/views/preupdate/clusterhealth", ['require'], function (require) {
    "use strict";

    var Marionette = require('marionette');
    var Templates = require('admin-updates/templates');
    var ClusterStateModel = require('admin-updates/models/clusterstate');
    var HealthCheckModel = require('admin-updates/models/healthcheck');
    var formatter = require('jira/util/formatter');

    return Marionette.Layout.extend({
        template: Templates.clusterHealth,
        ui: {
            content: "#cluster-health-content",
            waitContainer: "#healthcheck-wait-container",
            detailContainer: "#healthcheck-detail-container",
            aggregateContainer: "#healthcheck-aggregate-container",
            healthCheckLink: "#cluster-health-check-link"
        },
        triggers: {
            "click @ui.healthCheckLink": {
                event: "click:health-check",
                preventDefault: false
            }
        },
        iconClass: {
            info: "zdu-icon-info",
            warning: "zdu-icon-warning",
            success: "zdu-icon-success"
        },
        /**
         * @property {ClusterStateModel} model
         * @property {HealthCheckCollection} collection
         */
        initialize: function() {
            this.listenTo(this.model, "change:state", function() {
                this.collection.fetch();

                if(this._shouldExpandContent()) {
                    this.expandContent();
                } else {
                    this.collapseContent();
                }
            });

            this.listenTo(this.collection, {
                "healthcheck:started": function() {
                    this._hideDetailedHealthCheck();
                    this._hideAggregatedHealthChecks();
                    this._showSpinner();
                },
                "healthcheck:finished": function() {
                    this._hideSpinner();

                    if(this.collection.areAllChecksHealthy()) {
                        this._showHealthChecksOK();
                        this.trigger("change:icon", this.iconClass.success);
                    } else {
                        var model = this.collection.getMostSevereHealthCheck();
                        this._showDetailedHealthCheck(model);
                        this._showAggregatedHealthChecks(model);
                        this.trigger("change:icon", this.iconClass.warning);

                    }
                },
                "healthcheck:cancelled": function() {
                    this._hideSpinner();
                }
            });
        },
        serializeData: function() {
            return {
                expanded: this._shouldExpandContent()
            };
        },
        _showSpinner: function() {
            this.ui.waitContainer.html(Templates.spinner({
                text: formatter.I18n.getText('preupdate.instancehealth.spinner.text'),
                accessibilityText: formatter.I18n.getText('preupdate.instancehealth.spinner.accessibility.text')
            }));
            this.ui.waitContainer.removeClass("hidden");
        },
        _hideSpinner: function() {
            this.ui.waitContainer.addClass("hidden");
        },
        _showHealthChecksOK: function() {
            this.ui.detailContainer.html(Templates.healthCheck({
                icon: "approve",
                titleText: formatter.I18n.getText('preupdate.instancehealth.checks.all.ok.text')
            }));
            this.ui.detailContainer.removeClass("hidden");
        },
        /**
         * @param {HealthCheckModel} model
         * @private
         */
        _showDetailedHealthCheck: function(model) {
            this.ui.detailContainer.html(Templates.healthCheck({
                icon: model.isError() ? "error" : "warning",
                titleText: model.get("name"),
                accessibilityText: model.isError() ?
                    formatter.I18n.getText('preupdate.instancehealth.details.error.text') :
                    formatter.I18n.getText('preupdate.instancehealth.details.warning.text'),
                content: model.get("failureReason")
            }));
            this.ui.detailContainer.removeClass("hidden");
        },
        _hideDetailedHealthCheck: function() {
            this.ui.detailContainer.addClass("hidden");
        },
        /**
         * @param {int} errorCount
         * @returns {string}
         * @private
         */
        _getErrorText: function(errorCount) {
            return errorCount !== 1 ?
                formatter.I18n.getText('preupdate.instancehealth.checks.errors.text', errorCount) :
                formatter.I18n.getText('preupdate.instancehealth.checks.error.text');
        },
        /**
         * @param {int} warningCount
         * @returns {string}
         * @private
         */
        _getWarningText: function(warningCount) {
            return warningCount !== 1 ?
                formatter.I18n.getText('preupdate.instancehealth.checks.warnings.text', warningCount) :
                formatter.I18n.getText('preupdate.instancehealth.checks.warning.text');
        },
        /**
         * @param {int} errorCount
         * @param {int} warningCount
         * @returns {*|string}
         * @private
         */
        _getAggregatedText: function(errorCount, warningCount) {
            if(errorCount && warningCount) {
                return this._getErrorText(errorCount) + " " + formatter.I18n.getText('preupdate.instancehealth.checks.glue') + " " +
                    this._getWarningText(warningCount);
            }
            if(errorCount) {
                return this._getErrorText(errorCount);
            }
            if(warningCount) {
                return this._getWarningText(warningCount);
            }
            return "";
        },
        _filterModel: function(excludedModel, model) {
            return model.get("completeKey") !== excludedModel.get("completeKey");
        },
        /**
         * @param {HealthCheckModel} excludedModel
         * @private
         */
        _showAggregatedHealthChecks: function(excludedModel) {
            var errors = this.collection.getErrors();
            var warnings = this.collection.getWarnings();

            if(excludedModel) {
                if(excludedModel.get("severity") === HealthCheckModel.severity.WARNING) {
                    warnings = warnings.filter(this._filterModel.bind(this, excludedModel));
                } else {
                    errors = errors.filter(this._filterModel.bind(this, excludedModel));
                }
            }

            var models = errors.concat(warnings);

            if(models.length) {
                this.ui.aggregateContainer.html(Templates.healthCheck({
                    icon: "warning",
                    titleText: this._getAggregatedText(errors.length, warnings.length)
                }));
                this.ui.aggregateContainer.removeClass("hidden");
            }
        },
        _hideAggregatedHealthChecks: function() {
            this.ui.aggregateContainer.addClass("hidden");
        },
        _shouldExpandContent: function() {
            return this.model.get("state") === ClusterStateModel.states.STABLE;
        },
        expandContent: function() {
            this.ui.content.removeClass('hidden');
        },
        collapseContent: function() {
            this.ui.content.addClass('hidden');
        }
    });
});
