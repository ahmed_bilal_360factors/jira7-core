AJS.test.require('com.atlassian.jira.jira-admin-updates-plugin:js-app', function() {
    'use strict';

    var jQuery = require('jquery');
    var Backbone = require('backbone');
    var _ = require('underscore');
    var Templates = require('admin-updates/templates');
    var PageIntro = require('admin-updates/views/intro/pageintro');

    module('admin-updates/views/intro/pageintro', {
        setup: function() {
            this.sandbox = sinon.sandbox.create();

            jQuery('#qunit-fixture').append(Templates.pageIntro({
                id: 'page-intro-container',
            })).find('#page-intro-container').append('<a id="page-intro-documentation-link"/>');

            this.listener = _.extend({}, Backbone.Events);

            this.view = new PageIntro();
            this.view.bindUIElements();
        },
        teardown: function() {
            this.sandbox.restore();
        }
    });

    test("it emits an event when the documentation link is clicked", function() {
        var callback = this.sandbox.stub();
        this.listener.listenTo(this.view, 'click:documentation', callback);

        this.view.ui.documentationLink.click();

        sinon.assert.calledOnce(callback);
    });
});
