AJS.test.require([
    "com.atlassian.jira.jira-admin-updates-plugin:js-app"
], function() {
    "use strict";

    require([
        'jquery',
        'underscore',
        'backbone',
        'admin-updates/views/update/infoinlinedialog',
        'admin-updates/models/clusterstate'
    ], function (
        jQuery,
        _,
        Backbone,
        InfoInlineDialog,
        ClusterStateModel
    ) {
        // Without this timeout, QUnit will wait indefinitely for async tests whose done() callback isn't called.
        QUnit.config.testTimeout = 5000;

        module("admin-updates/views/update/infoinlinedialog", {
            setup: function () {
                this.listener = _.extend({}, Backbone.Events);

                this.triggerId = 'trigger';
                this.dialogId = 'info-dialog';

                this.trigger = jQuery('<a id="' + this.triggerId + '" data-aui-trigger aria-controls="' +
                    this.dialogId + '">test</a>');
                this.container = jQuery('<div id="dialog-container">');
                this.fixture = jQuery('#qunit-fixture')
                    .append(this.trigger)
                    .append(this.container);

                this.model = new ClusterStateModel({state: ClusterStateModel.states.STABLE});
                this.defaultOpts = {
                    el: this.container,
                    model: this.model,
                    dialogId: this.dialogId
                };

                this.view = new InfoInlineDialog(this.defaultOpts);
                this.view.render();
            }
        });

        test("When the state changes, it should update its contents (STABLE)", function() {
            var inlineDialog = jQuery(document.getElementById(this.dialogId));
            ok(inlineDialog.text().indexOf('update.info.dialog.stable.title') > -1);
            ok(inlineDialog.text().indexOf('update.info.dialog.stable.text') > -1);
        });

        test("When the state changes, it should update its contents (READY_TO_UPGRADE)", function(assert) {
            assert.expect(2);
            var done = assert.async();
            this.listener.listenTo(this.model, "change:state", function() {
                var inlineDialog = jQuery(document.getElementById(this.dialogId));
                ok(inlineDialog.text().indexOf('update.info.dialog.readytoupgrade.title') > -1);
                ok(inlineDialog.text().indexOf('update.info.dialog.readytoupgrade.text') > -1);
                done();
            }.bind(this));

            this.model.set("state", ClusterStateModel.states.READY_TO_UPGRADE);
        });

        test("When the state changes, it should update its contents (MIXED)", function(assert) {
            assert.expect(2);
            var done = assert.async();
            this.listener.listenTo(this.model, "change:state", function() {
                var inlineDialog = jQuery(document.getElementById(this.dialogId));
                ok(inlineDialog.text().indexOf('update.info.dialog.mixed.title') > -1);
                ok(inlineDialog.text().indexOf('update.info.dialog.mixed.text') > -1);
                done();
            }.bind(this));

            this.model.set("state", ClusterStateModel.states.MIXED);
        });

        test("When the state changes, it should update its contents (READY_TO_RUN_UPGRADE_TASKS)", function(assert) {
            assert.expect(2);
            var done = assert.async();
            this.listener.listenTo(this.model, "change:state", function() {
                var inlineDialog = jQuery(document.getElementById(this.dialogId));
                ok(inlineDialog.text().indexOf('update.info.dialog.readytorunupgradetasks.title') > -1);
                ok(inlineDialog.text().indexOf('update.info.dialog.readytorunupgradetasks.text') > -1);
                done();
            }.bind(this));

            this.model.set("state", ClusterStateModel.states.READY_TO_RUN_UPGRADE_TASKS);
        });

        test("When the state changes, it should update its contents (RUNNING_UPGRADE_TASKS)", function(assert) {
            assert.expect(2);
            var done = assert.async();
            this.listener.listenTo(this.model, "change:state", function() {
                var inlineDialog = jQuery(document.getElementById(this.dialogId));
                ok(inlineDialog.text().indexOf('update.info.dialog.runningupgradetasks.title') > -1);
                ok(inlineDialog.text().indexOf('update.info.dialog.runningupgradetasks.text') > -1);
                done();
            }.bind(this));

            this.model.set("state", ClusterStateModel.states.RUNNING_UPGRADE_TASKS);
        });
    });
});
