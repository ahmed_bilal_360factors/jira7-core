AJS.test.require('com.atlassian.jira.jira-admin-updates-plugin:js-app', function() {
    'use strict';

    module('admin-updates/modules/analytics', {
        setup: function() {
            this.sandbox = sinon.sandbox.create();
            this.context = AJS.test.mockableModuleContext();

            this.output = {send: this.sandbox.stub()};
            this.context.mock('jira/analytics', this.output);

            this.logger = {error: this.sandbox.stub()};
            this.context.mock('jira/util/logger', this.logger);

            this.analytics = this.context.require('admin-updates/modules/analytics');
        },
        teardown: function() {
            this.sandbox.restore();
        }
    });

    test("it triggers documentation link events", function() {
        this.analytics.recordIntroDocumentationClick();

        sinon.assert.calledOnce(this.output.send);
        sinon.assert.calledWith(this.output.send, {name: 'zdu.admin-updates-ui.documentation'});
    });

    test("it triggers view health check events", function() {
        this.analytics.recordViewHealthChecksClick();

        sinon.assert.calledOnce(this.output.send);
        sinon.assert.calledWith(this.output.send, {name: 'zdu.admin-updates-ui.pre-update.view-health-checks'});
    });

    test("it triggers check addon events", function() {
        this.analytics.recordCheckAddonsClick();

        sinon.assert.calledOnce(this.output.send);
        sinon.assert.calledWith(this.output.send, {name: 'zdu.admin-updates-ui.pre-update.check-addons'});
    });

    test("it triggers download latest events", function() {
        this.analytics.recordDownloadLatestClick();

        sinon.assert.calledOnce(this.output.send);
        sinon.assert.calledWith(this.output.send, {name: 'zdu.admin-updates-ui.pre-update.download-latest'});
    });

    test("it triggers begin update (dialog) events", function() {
        this.analytics.recordBeginUpdateDialog();

        sinon.assert.calledOnce(this.output.send);
        sinon.assert.calledWith(this.output.send, {name: 'zdu.admin-updates-ui.dialog-begin-update'});
    });

    test("it triggers begin update (confirm) events", function() {
        this.analytics.recordBeginUpdateConfirm();

        sinon.assert.calledOnce(this.output.send);
        sinon.assert.calledWith(this.output.send, {name: 'zdu.admin-updates-ui.confirm-begin-update'});
    });

    test("it triggers complete update (dialog) events", function() {
        this.analytics.recordCompleteUpdateDialog();

        sinon.assert.calledOnce(this.output.send);
        sinon.assert.calledWith(this.output.send, {name: 'zdu.admin-updates-ui.dialog-complete-update'});
    });

    test("it triggers complete update (confirm) events", function() {
        this.analytics.recordCompleteUpdateConfirm();

        sinon.assert.calledOnce(this.output.send);
        sinon.assert.calledWith(this.output.send, {name: 'zdu.admin-updates-ui.confirm-complete-update'});
    });

    test("it triggers cancel update (dialog) events", function() {
        this.analytics.recordCancelUpdateDialog();

        sinon.assert.calledOnce(this.output.send);
        sinon.assert.calledWith(this.output.send, {name: 'zdu.admin-updates-ui.dialog-cancel-update'});
    });

    test("it triggers cancel update (confirm) events", function() {
        this.analytics.recordCancelUpdateConfirm();

        sinon.assert.calledOnce(this.output.send);
        sinon.assert.calledWith(this.output.send, {name: 'zdu.admin-updates-ui.confirm-cancel-update'});
    });

    test("it triggers info tooltip events", function() {
        this.analytics.recordInfoTooltipClick({clusterState: 'STABLE'});

        sinon.assert.calledOnce(this.output.send);
        sinon.assert.calledWith(this.output.send, {
            name: 'zdu.admin-updates-ui.info-tooltip',
            properties: {clusterState: 'STABLE'}
        });
    });

    test("it logs an error if cluster state is not provided to info tooltip events", function() {
        this.analytics.recordInfoTooltipClick();

        sinon.assert.notCalled(this.output.send);
        sinon.assert.calledWith(this.logger.error, "info-tooltip event doesn't include cluster state data");
    });

    test("it records the initial page load event", function() {
        this.analytics.recordInitialPageLoad({clusterState: 'STABLE'});

        sinon.assert.calledOnce(this.output.send);
        sinon.assert.calledWith(this.output.send, {
            name: 'zdu.admin-updates-ui.page-load',
            properties: {clusterState: 'STABLE'}
        });
    });

    test("it logs an error if cluster state is not provided to page load events", function() {
        this.analytics.recordInitialPageLoad();

        sinon.assert.notCalled(this.output.send);
        sinon.assert.calledWith(this.logger.error, "page-load event doesn't include cluster state data");
    });
});
