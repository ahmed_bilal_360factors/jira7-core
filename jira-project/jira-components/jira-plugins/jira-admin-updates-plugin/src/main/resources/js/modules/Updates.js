define("admin-updates/modules/updates", ['require'], function(require) {
    "use strict";

    var Marionette = require('marionette');
    var jQuery = require('jquery');
    var PageIntroView = require('admin-updates/views/intro/pageintro');
    var PreUpdateViewManager = require('admin-updates/controllers/preupdateviewmanager');
    var UpdateViewManager = require('admin-updates/controllers/updateviewmanager');
    var ClusterStateModel = require('admin-updates/models/clusterstate');
    var HealthCheckCollection = require('admin-updates/models/healthcheckcollection');
    var formatter = require('jira/util/formatter');

    return Marionette.Controller.extend({
        /**
         * @param {jQuery} options.el
         * @param {string} options.clusterState
         * @param {int} options.pollingInterval interval in millis to fetch cluster state
         */
        initialize: function(options) {
            this.el = options.el;
            this.pollingInterval = options.pollingInterval || 10000;
            this.clusterStateModel = new ClusterStateModel({
                state: options.clusterState
            });
            this.healthCheckCollection = new HealthCheckCollection();

            this.pageIntroView = new PageIntroView();
            this.preUpdateViewManager = new PreUpdateViewManager({
                el: jQuery("#pre-update-tasks-container"),
                model: this.clusterStateModel,
                collection: this.healthCheckCollection
            });
            this.updateViewManager = new UpdateViewManager({
                el: jQuery("#update-controls-container"),
                model: this.clusterStateModel,
                startDialog: {el: jQuery("#start-dialog-container")},
                finishDialog: {el: jQuery("#finish-dialog-container")},
                cancelDialog: {el: jQuery("#cancel-dialog-container")},
                infoInlineDialog: {el: jQuery("#info-inline-dialog-container")}
            });

            this.listenTo(this.updateViewManager, {
                "update:start":     function() {this.clusterStateModel.startUpdate();},
                "update:finish":    function() {this.clusterStateModel.finishUpdate();},
                "update:cancel":    function() {this.clusterStateModel.cancelUpdate();}
            });

            this.listenTo(this.clusterStateModel, "sync:error", function(action, message) {
                AJS.flag({
                    type: 'error',
                    close: 'auto',
                    title: action,
                    body: message
                });
            });

            this.listenTo(this.healthCheckCollection, "sync:error", function() {
                AJS.flag({
                    type: 'error',
                    close: 'auto',
                    title: formatter.I18n.getText('preupdate.instancehealth.error.title'),
                    body: formatter.I18n.getText('preupdate.instancehealth.error.text')
                });
            });

            this.healthCheckCollection.fetch();

            setInterval(function() {
                this.clusterStateModel.fetch();
            }.bind(this), this.pollingInterval);

            this.propagateComponentEvents();
        },
        /**
         * Attach events listeners to each major view component, and bubble key behavioural events up. This allows to
         * attach an analytics listener outside the app to record analytic events.
         *
         * Some of these events are already handled by separate listeners (e.g. UpdateViewManager#update:start is
         * bound to clusterStateModel::startUpdate during initialisation), but these handlers _only_ propagate events
         * up - they're intentionally separate from any behavioural triggers (e.g. beginning or cancelling an update).
         */
        propagateComponentEvents: function() {
            this.listenTo(this.pageIntroView, {
                "click:documentation": function() {
                    this.trigger("click:documentation");
                }
            });

            this.listenTo(this.preUpdateViewManager, {
                "click:health-check": function() {
                    this.trigger("click:health-check");
                },
                "click:check-addons": function() {
                    this.trigger("click:check-addons");
                },
                "click:download-latest": function() {
                    this.trigger("click:download-latest");
                }
            });

            this.listenTo(this.updateViewManager, {
                "update:start:dialog": function() {
                    this.trigger("update:start:dialog");
                },
                "update:start": function() {
                    this.trigger("update:start:confirm");
                },
                "update:finish:dialog": function() {
                    this.trigger("update:finish:dialog");
                },
                "update:finish": function() {
                    this.trigger("update:finish:confirm");
                },
                "update:cancel:dialog": function() {
                    this.trigger("update:cancel:dialog");
                },
                "update:cancel": function() {
                    this.trigger("update:cancel:confirm");
                },
                "click:info-tooltip": function() {
                    this.trigger("click:info-tooltip", {clusterState: this.clusterStateModel.get('state')});
                }
            });
        }
    });
});
