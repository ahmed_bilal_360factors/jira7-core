AJS.test.require([
    "com.atlassian.jira.jira-admin-updates-plugin:js-app"
], function() {
    "use strict";

    require([
        'jquery',
        'admin-updates/views/update/clusterstate',
        'admin-updates/models/clusterstate'
    ], function (
        jQuery,
        ClusterStateView,
        ClusterStateModel
    ) {
        module("admin-updates/views/update/clusterstate", {
            setup: function () {
                this.fixture = jQuery('#qunit-fixture');

                this.model = new ClusterStateModel({state: ClusterStateModel.states.STABLE});

                this.defaultOpts = {
                    el: this.fixture,
                    model: this.model
                };

                this.clusterState = new ClusterStateView(this.defaultOpts);
                this.clusterState.render();
            }
        });

        test("It should listen to fetch:started from model and show the spinner", function() {
            var $spinner = jQuery(this.clusterState.ui.spinner.selector);

            this.model.trigger("fetch:started");

            equal($spinner.find(".aui-icon-wait").size(), 1);
            notOk($spinner.hasClass("hidden"));
        });

        test("It should listen to fetch:finished from model and hide the spinner", function() {
            var $spinner = jQuery(this.clusterState.ui.spinner.selector);

            this.model.trigger("fetch:finished");

            ok($spinner.hasClass("hidden"));
        });

        test("It should listen to fetch:failed from model and hide the spinner", function() {
            var $spinner = jQuery(this.clusterState.ui.spinner.selector);

            this.model.trigger("fetch:finished");

            ok($spinner.hasClass("hidden"));
        });

        test("It should listen to change:state from model and render the view", function() {
            ok(this.fixture.text().indexOf('STABLE') > -1);

            this.model.set("state", ClusterStateModel.states.READY_TO_UPGRADE);

            ok(this.fixture.text().indexOf('READY TO UPGRADE') > -1);
        });

        test("serializeData() should return the state without underscores", function() {
            this.model.set("state", ClusterStateModel.states.READY_TO_RUN_UPGRADE_TASKS);

            var data = this.clusterState.serializeData();

            equal(data.text, "READY TO RUN UPGRADE TASKS");
        });

        test("serializeData() should return the correct info text (STABLE)", function() {
            var data = this.clusterState.serializeData();

            equal(data.infoText, "update.state.stable.infotext");
        });

        test("serializeData() should return the correct info text (READY_TO_UPGRADE)", function() {
            this.model.set("state", ClusterStateModel.states.READY_TO_UPGRADE);

            var data = this.clusterState.serializeData();

            equal(data.infoText, "update.state.readytoupgrade.infotext");
        });

        test("serializeData() should return the correct info text (MIXED)", function() {
            this.model.set("state", ClusterStateModel.states.MIXED);

            var data = this.clusterState.serializeData();

            equal(data.infoText, "update.state.mixed.infotext");
        });

        test("serializeData() should return the correct info text (READY_TO_RUN_UPGRADE_TASKS)", function() {
            this.model.set("state", ClusterStateModel.states.READY_TO_RUN_UPGRADE_TASKS);

            var data = this.clusterState.serializeData();

            equal(data.infoText, "update.state.readytorunupgradetasks.infotext");
        });

        test("serializeData() should return the correct info text (RUNNING_UPGRADE_TASKS)", function() {
            this.model.set("state", ClusterStateModel.states.RUNNING_UPGRADE_TASKS);

            var data = this.clusterState.serializeData();

            equal(data.infoText, "update.state.runningupgradetasks.infotext");
        });

        test("serializeData() should return the correct lozenge style (STABLE)", function() {
            var data = this.clusterState.serializeData();

            equal(data.extraClasses, "aui-lozenge-success");
        });

        test("serializeData() should return the correct lozenge style (READY_TO_UPGRADE)", function() {
            this.model.set("state", ClusterStateModel.states.READY_TO_UPGRADE);

            var data = this.clusterState.serializeData();

            equal(data.extraClasses, "aui-lozenge-current");
        });

        test("serializeData() should return the correct lozenge style (MIXED)", function() {
            this.model.set("state", ClusterStateModel.states.MIXED);

            var data = this.clusterState.serializeData();

            equal(data.extraClasses, "aui-lozenge-current");
        });

        test("serializeData() should return the correct lozenge style (READY_TO_RUN_UPGRADE_TASKS)", function() {
            this.model.set("state", ClusterStateModel.states.READY_TO_RUN_UPGRADE_TASKS);

            var data = this.clusterState.serializeData();

            equal(data.extraClasses, "aui-lozenge-current");
        });

        test("serializeData() should return the correct lozenge style (RUNNING_UPGRADE_TASKS)", function() {
            this.model.set("state", ClusterStateModel.states.RUNNING_UPGRADE_TASKS);

            var data = this.clusterState.serializeData();

            equal(data.extraClasses, "aui-lozenge-current");
        });
    });
});
