/**
 * This file is used to wrap the templates exported in the global namespace
 * by Google Closure Compile with an AMD module. The goal of this AMD module
 * is to hold a reference to the templates, even when they are removed from
 * the global namespace.
 *
 * Please, note that this AMD can be used only in JavaScript land. Soy templates
 * still have to use the global reference. In other words, if someone removes
 * the global reference, it needs to be restored *before* executing a template.
 */
define("admin-updates/templates", function () {
    "use strict";

    return JIRA.Templates.Admin.Updates.Templates;
});
