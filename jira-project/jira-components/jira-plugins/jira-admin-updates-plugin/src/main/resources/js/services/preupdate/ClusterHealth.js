define("admin-updates/services/preupdate/clusterhealth", ['require'], function (require) {
    "use strict";

    var SmartAjax = require('jira/ajs/ajax/smart-ajax');
    var contextPath = require('wrm/context-path');

    /**
     * This service wraps around the STP Health Check REST API
     */
    return {
        /**
         * Builds a request and wraps it in a Promise
         * @param {Object} requestOptions
         * @returns {Promise}
         * @private
         */
        _request: function (requestOptions) {
            return new Promise(function (resolve, reject) {
                SmartAjax.makeRequest(requestOptions)
                    .done(resolve)
                    .fail(reject);
            });
        },
        /**
         * Runs the health checks and returns the results
         * @returns {Promise}
         */
        checkDetails: function () {
            return this._request({
                url: contextPath() + '/rest/supportHealthCheck/1.0/checkDetails',
                type: 'GET'
            });
        }
    };
});
