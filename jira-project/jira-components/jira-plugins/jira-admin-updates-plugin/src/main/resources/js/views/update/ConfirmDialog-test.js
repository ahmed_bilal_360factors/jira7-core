AJS.test.require([
    "com.atlassian.jira.jira-admin-updates-plugin:js-app"
], function() {
    "use strict";

    require([
        'jquery',
        'underscore',
        'backbone',
        'admin-updates/views/update/confirmdialog'
    ], function (
        jQuery,
        _,
        Backbone,
        ConfirmDialog
    ) {
        // Without this timeout, QUnit will wait indefinitely for async tests whose done() callback isn't called.
        QUnit.config.testTimeout = 5000;

        function createDialog(options, prevDialog) {
            if(prevDialog && prevDialog.dialog) {
                prevDialog.dialog.remove();
            }

            var dialog = new ConfirmDialog(options);
            dialog.render();
            dialog.onShow();
            return dialog;
        }

        module("admin-updates/views/update/confirmdialog", {
            setup: function () {
                this.listener = _.extend({}, Backbone.Events);
                this.fixture = jQuery('#qunit-fixture');

                this.defaultOpts = {
                    el: this.fixture,
                    dialogId: 'confirm-dialog',
                    titleText: 'foo',
                    content: 'bar',
                    contentExtended: 'baz'
                };

                this.dialog = createDialog(this.defaultOpts);
            },
            teardown: function() {
                this.dialog.dialog.remove(); // ensure the dialog is hidden so other tests run OK
            }
        });

        test("It should contain the title text", function() {
            ok(this.dialog.dialog.$el.text().indexOf(this.defaultOpts.titleText) > -1);
        });

        test("It should contain the content text", function() {
            ok(this.dialog.dialog.$el.text().indexOf(this.defaultOpts.content) > -1);
        });

        test("It should contain the extended content text", function() {
            ok(this.dialog.dialog.$el.text().indexOf(this.defaultOpts.contentExtended) > -1);
        });

        test("It should be open by default", function() {
            ok(this.dialog.dialog.$el.is(":visible"));
        });

        test("It should be hidden when 'hide' property is set to false", function() {
            var opts = _.extend({hide: false}, this.defaultOpts);
            this.dialog = createDialog(opts, this.dialog);

            ok(this.dialog.dialog.$el.is(":visible"));
        });

        test("It should be hidden when 'hide' property is set to true", function() {
            var opts = _.extend({hide: true}, this.defaultOpts);
            this.dialog = createDialog(opts, this.dialog);

            notOk(this.dialog.dialog.$el.is(":visible"));
        });

        test("It should be hidden when the 'cancel' button is clicked", function() {
            this.dialog.ui.cancelButton.click();

            notOk(this.dialog.dialog.$el.is(":visible"));
        });

        test("It should be hidden when the 'confirm' button is clicked", function() {
            this.dialog.ui.confirmButton.click();

            notOk(this.dialog.dialog.$el.is(":visible"));
        });

        test("It should trigger dialog:confirm when the 'confirm' button is clicked", function(assert) {
            assert.expect(0);
            this.listener.listenTo(this.dialog, "dialog:confirm", assert.async());

            this.dialog.ui.confirmButton.click();
        });

        test("It should trigger dialog:hide when the dialog is hidden", function(assert) {
            assert.expect(0);
            this.listener.listenTo(this.dialog, "dialog:hide", assert.async());

            this.dialog.dialog.hide();
        });
    });
});
