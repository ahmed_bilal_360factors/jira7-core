define("admin-updates/views/update/infoinlinedialog", ['require'], function (require) {
    "use strict";

    var _ = require('underscore');
    var jQuery = require('jquery');
    var Marionette = require('marionette');
    var Templates = require('admin-updates/templates');
    var ClusterStateModel = require('admin-updates/models/clusterstate');
    var formatter = require('jira/util/formatter');

    return Marionette.ItemView.extend({
        template: Templates.inlineDialog,
        initialize: function(options) {
            options = _.extend({}, options);
            this.dialogId = options.dialogId;
            this.open = Boolean(options.hide);
            this.alignment = options.alignment || 'right middle';
            this.respondsTo = options.respondsTo || 'toggle';

            this.listenTo(this.model, "change:state", function() {
                // Because AUI moves the inline dialog outside of this view (and therefore out of its control),
                // we destroy the dialog so it gets rendered properly.
                jQuery(document.getElementById(this.dialogId)).remove();
                this._updateDialogContent();
                this.render();
            });

            this._updateDialogContent();
        },
        serializeData: function() {
            return {
                id: this.dialogId,
                title: this.title,
                content: this.content,
                open: this.open,
                alignment: this.alignment,
                respondsTo: this.respondsTo
            };
        },
        _updateDialogContent: function() {
            switch(this.model.get("state")) {
                case ClusterStateModel.states.STABLE:
                    this.title = formatter.I18n.getText('update.info.dialog.stable.title');
                    this.content = formatter.I18n.getText('update.info.dialog.stable.text');
                    break;
                case ClusterStateModel.states.READY_TO_UPGRADE:
                    this.title = formatter.I18n.getText('update.info.dialog.readytoupgrade.title');
                    this.content = formatter.I18n.getText('update.info.dialog.readytoupgrade.text');
                    break;
                case ClusterStateModel.states.MIXED:
                    this.title = formatter.I18n.getText('update.info.dialog.mixed.title');
                    this.content = formatter.I18n.getText('update.info.dialog.mixed.text');
                    break;
                case ClusterStateModel.states.READY_TO_RUN_UPGRADE_TASKS:
                    this.title = formatter.I18n.getText('update.info.dialog.readytorunupgradetasks.title');
                    this.content = formatter.I18n.getText('update.info.dialog.readytorunupgradetasks.text');
                    break;
                case ClusterStateModel.states.RUNNING_UPGRADE_TASKS:
                    this.title = formatter.I18n.getText('update.info.dialog.runningupgradetasks.title');
                    this.content = formatter.I18n.getText('update.info.dialog.runningupgradetasks.text');
                    break;
            }
        }
    });
});
