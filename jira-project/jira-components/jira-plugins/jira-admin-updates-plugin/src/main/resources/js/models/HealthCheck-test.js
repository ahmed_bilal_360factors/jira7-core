AJS.test.require([
    "com.atlassian.jira.jira-admin-updates-plugin:js-app"
], function() {
    "use strict";

    require([
        'admin-updates/models/healthcheck'
    ], function (
        HealthCheck
    ) {
        module("admin-updates/models/healthcheck", {
            setup: function () {
                this.model = new HealthCheck();
            }
        });

        test("It should indicate an error if the severity is 'critical'", function() {
            this.model.set('severity', HealthCheck.severity.CRITICAL);

            ok(this.model.isError());
        });

        test("It should indicate an error if the severity is 'major'", function() {
            this.model.set('severity', HealthCheck.severity.MAJOR);

            ok(this.model.isError());
        });

        test("It should not indicate an error if the severity is 'warning'", function() {
            this.model.set('severity', HealthCheck.severity.WARNING);

            notOk(this.model.isError());
        });

        test("It should not indicate an error if the severity is 'minor'", function() {
            this.model.set('severity', HealthCheck.severity.MINOR);

            notOk(this.model.isError());
        });

        test("It should not indicate an error if the severity is 'undefined'", function() {
            this.model.set('severity', HealthCheck.severity.UNDEFINED);

            notOk(this.model.isError());
        });

        test("It should have all severities", function() {
            deepEqual(HealthCheck.severity, {
                UNDEFINED: "undefined",
                MINOR: "minor",
                WARNING: "warning",
                MAJOR: "major",
                CRITICAL: "critical"
            });
        });
    });
});
