define("admin-updates/models/clusterstate", ['require'], function (require) {
    "use strict";

    var Backbone = require('backbone');
    var ClusterZDUService = require('admin-updates/services/update/clusterzdu');
    var formatter = require('jira/util/formatter');

    var states = {
        STABLE: "STABLE",
        READY_TO_UPGRADE: "READY_TO_UPGRADE",
        MIXED: "MIXED",
        READY_TO_RUN_UPGRADE_TASKS: "READY_TO_RUN_UPGRADE_TASKS",
        RUNNING_UPGRADE_TASKS: "RUNNING_UPGRADE_TASKS"
    };

    return Backbone.Model.extend({
        /**
         * Fetch the cluster state from the server and set it on the model
         * @returns {Promise}
         */
        fetch: function() {
            this.trigger("fetch:started");
            return ClusterZDUService.clusterState().then(function(data) {
                this.set("state", data.state);
                this.trigger("fetch:finished");
            }.bind(this), function(xhr) {
                this._rejectHandler(formatter.I18n.getText("update.error.fetch.title"), xhr);
                this.trigger("fetch:failed");
            }.bind(this));
        },
        /**
         * Start the Zero Downtime Upgrade
         * @returns {Promise}
         */
        startUpdate: function() {
            return ClusterZDUService.startUpdate().then(
                this.fetch.bind(this),
                this._rejectHandler.bind(this, formatter.I18n.getText("update.error.start.title"))
            );
        },
        /**
         * Finish the Zero Downtime Upgrade
         * @returns {Promise}
         */
        finishUpdate: function() {
            return ClusterZDUService.finishUpdate().then(
                this.fetch.bind(this),
                this._rejectHandler.bind(this, formatter.I18n.getText("update.error.finish.title"))
            );
        },
        /**
         * Cancel the Zero Downtime Upgrade
         * @returns {Promise}
         */
        cancelUpdate: function() {
            return ClusterZDUService.cancelUpdate().then(
                this.fetch.bind(this),
                this._rejectHandler.bind(this, formatter.I18n.getText("update.error.cancel.title"))
            );
        },
        /**
         * Handle a rejected promise from the cluster ZDU service.
         * @param {string} action Describes the action that was attempted to be performed.
         * @param {XMLHttpRequest} xhr
         * @private
         */
        _rejectHandler: function(action, xhr) {
            var error;

            switch(xhr.status) {
                case 401:
                    error = formatter.I18n.getText("update.error.login.text");
                    break;
                case 403:
                    error = formatter.I18n.getText("update.error.permission.text");
                    break;
                case 409:
                    error = formatter.I18n.getText("update.error.transition.text");
                    this.fetch();
                    break;
                default:
                    error = formatter.I18n.getText("update.error.network.application.text");
                    break;
            }

            this.trigger("sync:error", action, error, xhr);
        }
    }, {
        states: states
    });
});
