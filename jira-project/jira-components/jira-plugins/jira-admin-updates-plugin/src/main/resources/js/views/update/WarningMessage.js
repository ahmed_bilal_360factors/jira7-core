define("admin-updates/views/update/warningmessage", ['require'], function (require) {
    "use strict";

    var Marionette = require('marionette');
    var Templates = require('admin-updates/templates');
    var ClusterStateModel = require('admin-updates/models/clusterstate');

    return Marionette.ItemView.extend({
        template: Templates.warningMessage,
        ui: {
            message: ".aui-message"
        },
        initialize: function() {
            this.listenTo(this.model, "change:state", function() {
                if(this._shouldShowMessage()) {
                    this.showMessage();
                } else {
                    this.hideMessage();
                }
            });
        },
        /**
         * Serializes the data that feeds into the template
         * @returns {{show: (boolean)}}
         */
        serializeData: function() {
            return {
                show: this._shouldShowMessage()
            };
        },
        /**
         * Whether or not to show the warning message based on the model state
         * @returns {boolean}
         * @private
         */
        _shouldShowMessage: function() {
            switch(this.model.get("state")) {
                case ClusterStateModel.states.STABLE:
                    return false;
                default:
                    return true;
            }
        },
        showMessage: function() {
            this.ui.message.removeClass('hidden');
        },
        hideMessage: function() {
            this.ui.message.addClass('hidden');
        }
    });
});
