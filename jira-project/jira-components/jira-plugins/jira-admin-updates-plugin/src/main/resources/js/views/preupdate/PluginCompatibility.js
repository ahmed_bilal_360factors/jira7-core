define("admin-updates/views/preupdate/plugincompatibility", ['require'], function (require) {
    "use strict";

    var Marionette = require('marionette');
    var Templates = require('admin-updates/templates');
    var ClusterStateModel = require('admin-updates/models/clusterstate');

    return Marionette.Layout.extend({
        template: Templates.pluginCompatibility,
        ui: {
            content: "#plugin-compatibility-content",
            checkAddOnsLink: "#plugin-compatibility-check-addons-link"
        },
        triggers: {
            "click @ui.checkAddOnsLink": {
                event: "click:check-addons",
                preventDefault: false
            }
        },
        initialize: function() {
            this.listenTo(this.model, "change:state", function() {
                if(this._shouldExpandContent()) {
                    this.expandContent();
                } else {
                    this.collapseContent();
                }
            });
        },
        serializeData: function() {
            return {
                expanded: this._shouldExpandContent()
            };
        },
        _shouldExpandContent: function() {
            return this.model.get("state") === ClusterStateModel.states.STABLE;
        },
        expandContent: function() {
            this.ui.content.removeClass('hidden');
        },
        collapseContent: function() {
            this.ui.content.addClass('hidden');
        }
    });
});
