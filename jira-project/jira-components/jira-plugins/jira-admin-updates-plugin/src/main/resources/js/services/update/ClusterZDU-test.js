AJS.test.require([
    "com.atlassian.jira.jira-admin-updates-plugin:js-app"
], function() {
    "use strict";

    require([
        'jquery',
        'wrm/context-path'
    ], function(
        jQuery,
        contextPath
    ) {
        // Without this timeout, QUnit will wait indefinitely for async tests whose done() callback isn't called.
        QUnit.config.testTimeout = 5000;

        module("admin-updates/services/update/clusterzdu", {
            setup: function() {
                this.sandbox = sinon.sandbox.create();
                this.SmartAjax = {
                    makeRequest: this.sandbox.stub().returns(new jQuery.Deferred().resolve())
                };
                this.context = AJS.test.mockableModuleContext();
                this.context.mock("jira/ajs/ajax/smart-ajax", this.SmartAjax);

                this.ClusterZDU = this.context.require("admin-updates/services/update/clusterzdu");

                this.NOOP = function() {notOk(true, "NOOP should never be called.");};
            },
            teardown: function() {
                this.sandbox.restore();
            }
        });

        test("startUpdate() should do a POST request", function() {
            this.ClusterZDU.startUpdate();

            sinon.assert.calledWith(this.SmartAjax.makeRequest, sinon.match({type: 'POST'}));
        });

        test("startUpdate() should hit /rest/api/2/cluster/zdu/start", function() {
            var expectedUrl = contextPath() + '/rest/api/2/cluster/zdu/start';

            this.ClusterZDU.startUpdate();

            sinon.assert.calledWith(this.SmartAjax.makeRequest, sinon.match({url: expectedUrl}));
        });

        test("startUpdate() should return a Promise (resolve)", function(assert) {
            assert.expect(1);
            var done = assert.async();

            var promise = this.ClusterZDU.startUpdate();

            promise.then(function() {
                ok(true);
                done();
            });
        });

        test("startUpdate() should return a Promise (reject)", function(assert) {
            this.SmartAjax.makeRequest = this.sandbox.stub().returns(new jQuery.Deferred().reject());
            assert.expect(1);
            var done = assert.async();

            var promise = this.ClusterZDU.startUpdate();

            promise.then(this.NOOP, function() {
                ok(true);
                done();
            });
        });

        test("cancelUpdate() should do a POST request", function() {
            this.ClusterZDU.cancelUpdate();

            sinon.assert.calledWith(this.SmartAjax.makeRequest, sinon.match({type: 'POST'}));
        });

        test("cancelUpdate() should hit /rest/api/2/cluster/zdu/cancel", function() {
            var expectedUrl = contextPath() + '/rest/api/2/cluster/zdu/cancel';

            this.ClusterZDU.cancelUpdate();

            sinon.assert.calledWith(this.SmartAjax.makeRequest, sinon.match({url: expectedUrl}));
        });

        test("cancelUpdate() should return a Promise (resolve)", function(assert) {
            assert.expect(1);
            var done = assert.async();

            var promise = this.ClusterZDU.cancelUpdate();

            promise.then(function() {
                ok(true);
                done();
            });
        });

        test("cancelUpdate() should return a Promise (reject)", function(assert) {
            this.SmartAjax.makeRequest = this.sandbox.stub().returns(new jQuery.Deferred().reject());
            assert.expect(1);
            var done = assert.async();

            var promise = this.ClusterZDU.cancelUpdate();

            promise.then(this.NOOP, function() {
                ok(true);
                done();
            });
        });

        test("finishUpdate() should do a POST request", function() {
            this.ClusterZDU.finishUpdate();

            sinon.assert.calledWith(this.SmartAjax.makeRequest, sinon.match({type: 'POST'}));
        });

        test("finishUpdate() should hit /rest/api/2/cluster/zdu/approve", function() {
            var expectedUrl = contextPath() + '/rest/api/2/cluster/zdu/approve';

            this.ClusterZDU.finishUpdate();

            sinon.assert.calledWith(this.SmartAjax.makeRequest, sinon.match({url: expectedUrl}));
        });

        test("finishUpdate() should return a Promise (resolve)", function(assert) {
            assert.expect(1);
            var done = assert.async();

            var promise = this.ClusterZDU.finishUpdate();

            promise.then(function() {
                ok(true);
                done();
            });
        });

        test("finishUpdate() should return a Promise (reject)", function(assert) {
            this.SmartAjax.makeRequest = this.sandbox.stub().returns(new jQuery.Deferred().reject());
            assert.expect(1);
            var done = assert.async();

            var promise = this.ClusterZDU.finishUpdate();

            promise.then(this.NOOP, function() {
                ok(true);
                done();
            });
        });

        test("clusterState() should do a GET request", function() {
            this.ClusterZDU.clusterState();

            sinon.assert.calledWith(this.SmartAjax.makeRequest, sinon.match({type: 'GET'}));
        });

        test("clusterState() should hit /rest/api/2/cluster/zdu/state", function() {
            var expectedUrl = contextPath() + '/rest/api/2/cluster/zdu/state';

            this.ClusterZDU.clusterState();

            sinon.assert.calledWith(this.SmartAjax.makeRequest, sinon.match({url: expectedUrl}));
        });

        test("clusterState() should return a Promise (resolve)", function(assert) {
            assert.expect(1);
            var done = assert.async();

            var promise = this.ClusterZDU.clusterState();

            promise.then(function() {
                ok(true);
                done();
            });
        });

        test("clusterState() should return a Promise (reject)", function(assert) {
            this.SmartAjax.makeRequest = this.sandbox.stub().returns(new jQuery.Deferred().reject());
            assert.expect(1);
            var done = assert.async();

            var promise = this.ClusterZDU.clusterState();

            promise.then(this.NOOP, function() {
                ok(true);
                done();
            });
        });
    });
});
