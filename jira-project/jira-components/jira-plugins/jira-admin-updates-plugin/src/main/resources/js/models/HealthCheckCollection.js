define("admin-updates/models/healthcheckcollection", ['require'], function (require) {
    "use strict";

    var Backbone = require('backbone');
    var ClusterHealthService = require('admin-updates/services/preupdate/clusterhealth');
    var HealthCheck = require('admin-updates/models/healthcheck');

    return Backbone.Collection.extend({
        model: HealthCheck,
        /**
         * Compares health checks based on healthiness and severity. Most severe checks go first, healthy ones go last.
         * @param {HealthCheck} a
         * @param {HealthCheck} b
         * @returns {number}
         */
        comparator: function(a, b) {
            if(a.get("isHealthy") && b.get("isHealthy")) { return 0; }
            else if(a.get("isHealthy") && !b.get("isHealthy")) { return 1; }
            else if(!a.get("isHealthy") && b.get("isHealthy")) { return -1; }

            // both are not healthy
            var sevA = HealthCheck.severityOrder.indexOf(a.get("severity"));
            var sevB = HealthCheck.severityOrder.indexOf(b.get("severity"));

            return sevA === sevB ? 0 : sevA < sevB ? 1 : -1;
        },
        /**
         * Whether or not all health checks have passed
         * @returns {boolean}
         */
        areAllChecksHealthy: function() {
            return this.every(function(model) {
                return model.get("isHealthy");
            });
        },
        /**
         * Gets the health check with the highest severity (critical, major, warning, minor, undefined)
         * @returns {HealthCheck}
         */
        getMostSevereHealthCheck: function() {
            // The models are internally sorted based on severity. Thus we return the first element in the models array.
            if(this.models && this.models.length) {
                return this.models[0];
            }
        },
        /**
         * Gets all health checks with a severity 'critical' or 'major'
         * @returns {Array}
         */
        getErrors: function() {
            return this.filter(function(model) {
                return model.isError();
            });
        },
        /**
         * Gets all health checks with a severity 'warning'
         * @returns {Array}
         */
        getWarnings: function() {
            return this.where({"severity": HealthCheck.severity.WARNING});
        },
        /**
         * Run all health checks and populate the collection
         * @returns {Promise}
         */
        fetch: function() {
            this.trigger("healthcheck:started");
            return ClusterHealthService.checkDetails().then(function(jsonData) {
                if(jsonData && Array.isArray(jsonData.statuses)) {
                    this.add(jsonData.statuses, {merge: true});
                    this.trigger("healthcheck:finished");
                }
            }.bind(this), this._rejectHandler.bind(this));
        },
        /**
         * Handle a rejected promise.
         * @param {XMLHttpRequest} xhr
         * @private
         */
        _rejectHandler: function(xhr) {
            this.trigger("healthcheck:cancelled");
            this.trigger("sync:error", xhr);
        }
    });
});
