define("admin-updates/services/update/clusterzdu", ['require'], function (require) {
    "use strict";

    var SmartAjax = require('jira/ajs/ajax/smart-ajax');
    var contextPath = require('wrm/context-path');
    var trace = require('jira/util/logger').trace;

    /**
     * This service wraps around the ZDU REST API, providing methods to start, finish, cancel and get status of an
     * upgrade.
     *
     * @see {@link com.atlassian.jira.rest.v2.cluster.zdu.ClusterUpgradeStateResource}
     */
    return {
        /**
         * Builds a request and wraps it in a Promise
         * @param {Object} requestOptions
         * @returns {Promise}
         * @private
         */
        _request: function(requestOptions) {
            return new Promise(function(resolve, reject) {
                SmartAjax.makeRequest(requestOptions)
                    .done(resolve)
                    .fail(reject);
            });
        },

        /**
         * Starts an upgrade
         * @returns {Promise}
         */
        startUpdate: function() {
            return this._request({
                url: contextPath() + '/rest/api/2/cluster/zdu/start',
                type: 'POST'
            }).then(function() {
                trace('jira.zdu.started');
            });
        },

        /**
         * Cancels an upgrade
         * @returns {Promise}
         */
        cancelUpdate: function() {
            return this._request({
                url: contextPath() + '/rest/api/2/cluster/zdu/cancel',
                type: 'POST'
            }).then(function() {
                trace('jira.zdu.cancelled');
            });
        },

        /**
         * Finishes an upgrade
         * @returns {Promise}
         */
        finishUpdate: function() {
            return this._request({
                url: contextPath() + '/rest/api/2/cluster/zdu/approve',
                type: 'POST'
            }).then(function() {
                trace('jira.zdu.finished');
            });
        },

        /**
         * Get the current state of the cluster
         * @returns {Promise}
         */
        clusterState: function() {
            return this._request({
                url: contextPath() + '/rest/api/2/cluster/zdu/state',
                type: 'GET'
            });
        }
    };
});
