require([
    'jquery',
    'backbone',
    'underscore',
    'admin-updates/modules/updates',
    'admin-updates/modules/analytics',
    'wrm/data'
], function(
    jQuery,
    Backbone,
    _,
    UpdatesModule,
    zduUiAnalytics,
    WrmData
) {
    "use strict";
    jQuery(function() {
        WrmData.claim("com.atlassian.jira.jira-admin-updates-plugin:view-upgrades.view-upgrades-data", function(options) {
            // Start the UI app.
            var app = new UpdatesModule({
                el: jQuery("#view-updates-container"),
                clusterState: options.clusterState
            });

            // Listen for analytic events.
            var listener = _.extend({}, Backbone.Events);
            listener.listenTo(app, {
                "click:documentation": zduUiAnalytics.recordIntroDocumentationClick,
                "click:health-check": zduUiAnalytics.recordViewHealthChecksClick,
                "click:check-addons": zduUiAnalytics.recordCheckAddonsClick,
                "click:download-latest": zduUiAnalytics.recordDownloadLatestClick,
                "update:start:dialog": zduUiAnalytics.recordBeginUpdateDialog,
                "update:start:confirm": zduUiAnalytics.recordBeginUpdateConfirm,
                "update:finish:dialog": zduUiAnalytics.recordCompleteUpdateDialog,
                "update:finish:confirm": zduUiAnalytics.recordCompleteUpdateConfirm,
                "update:cancel:dialog": zduUiAnalytics.recordCancelUpdateDialog,
                "update:cancel:confirm": zduUiAnalytics.recordCancelUpdateConfirm,
                "click:info-tooltip": zduUiAnalytics.recordInfoTooltipClick
            });

            zduUiAnalytics.recordInitialPageLoad({clusterState: options.clusterState});
        });
    });
});
