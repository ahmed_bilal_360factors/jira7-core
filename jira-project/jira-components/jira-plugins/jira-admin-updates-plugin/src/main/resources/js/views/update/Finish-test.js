AJS.test.require([
    "com.atlassian.jira.jira-admin-updates-plugin:js-app"
], function() {
    "use strict";

    require([
        'jquery',
        'underscore',
        'backbone',
        'admin-updates/views/update/finish',
        'admin-updates/models/clusterstate'
    ], function (
        jQuery,
        _,
        Backbone,
        Finish,
        ClusterStateModel
    ) {
        // Without this timeout, QUnit will wait indefinitely for async tests whose done() callback isn't called.
        QUnit.config.testTimeout = 5000;

        module("admin-updates/views/update/finish", {
            setup: function () {
                this.fixture = jQuery('#qunit-fixture');

                this.listener = _.extend({}, Backbone.Events);

                this.finish = new Finish({
                    el: this.fixture,
                    model: new ClusterStateModel({state: ClusterStateModel.states.STABLE})
                });
                this.finish.render();
            }
        });

        test("clicking the finish button triggers the update:finish event", function(assert) {
            this.finish.model.set("state", ClusterStateModel.states.READY_TO_RUN_UPGRADE_TASKS);
            assert.expect(0);
            var done = assert.async();
            this.listener.listenTo(this.finish, "update:finish", done);

            this.finish.ui.finishButton.click();
        });

        test("clicking the cancel button triggers the update:cancel event", function(assert) {
            this.finish.model.set("state", ClusterStateModel.states.READY_TO_UPGRADE);
            assert.expect(0);
            var done = assert.async();
            this.listener.listenTo(this.finish, "update:cancel", done);

            this.finish.ui.cancelButton.click();
        });

        test("clicking the cancel tooltip triggers the click:info-tooltip event", function(assert) {
            assert.expect(0);
            var done = assert.async();
            this.listener.listenTo(this.finish, "click:info-tooltip", done);

            this.finish.ui.infoButton.click();
        });

        test("enableFinishButton() enables the button", function () {
            this.finish.ui.finishButton.prop("disabled", true);
            this.finish.enableFinishButton();

            notOk(this.finish.ui.finishButton.prop("disabled"));
        });

        test("disableFinishButton() disables the button", function () {
            this.finish.ui.finishButton.prop("disabled", false);
            this.finish.disableFinishButton();

            ok(this.finish.ui.finishButton.prop("disabled"));
        });

        test("enableCancelButton() enables the button", function () {
            this.finish.ui.cancelButton.prop("disabled", true);
            this.finish.enableCancelButton();

            notOk(this.finish.ui.cancelButton.prop("disabled"));
        });

        test("disableCancelButton() disables the button", function () {
            this.finish.ui.cancelButton.prop("disabled", false);
            this.finish.disableCancelButton();

            ok(this.finish.ui.cancelButton.prop("disabled"));
        });
    });
});
