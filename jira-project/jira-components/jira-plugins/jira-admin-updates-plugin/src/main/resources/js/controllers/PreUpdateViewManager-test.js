AJS.test.require([
    "com.atlassian.jira.jira-admin-updates-plugin:js-app"
], function() {
    "use strict";

    require([
        'jquery',
        'underscore',
        'backbone',
        'admin-updates/controllers/preupdateviewmanager',
        'admin-updates/models/clusterstate',
        'admin-updates/models/healthcheckcollection',
        'admin-updates/templates'
    ], function (
        jQuery,
        _,
        Backbone,
        ViewManager,
        ClusterStateModel,
        HealthCheckCollection,
        Templates
    ) {
        // Without this timeout, QUnit will wait indefinitely for async tests whose done() callback isn't called.
        QUnit.config.testTimeout = 5000;

        function spyOnMultipleMethods(sinon, obj, methods) {
            methods.forEach(function (method) {
                sinon.spy(obj, method);
            });
        }

        module("admin-updates/controllers/preupdateviewmanager", {
            setup: function () {
                this.sandbox = sinon.sandbox.create();

                this.fixture = jQuery('#qunit-fixture');
                this.fixture.html(Templates.preUpdateTasks({
                    expanded: true
                }));

                this.model = new ClusterStateModel();
                this.model.set("state", ClusterStateModel.states.STABLE);

                this.collection = new HealthCheckCollection();

                this.defaultOpts = {
                    el: this.fixture,
                    model: this.model,
                    collection: this.collection
                };

                this.listener = _.extend({}, Backbone.Events);

                this.viewManager = new ViewManager(this.defaultOpts);

                spyOnMultipleMethods(this.sandbox, this.viewManager, ["expandContent", "collapseContent"]);
                spyOnMultipleMethods(this.sandbox, this.viewManager.clusterHealth.currentView, ["expandContent", "collapseContent"]);
                spyOnMultipleMethods(this.sandbox, this.viewManager.pluginCompatibility.currentView, ["expandContent", "collapseContent"]);
                spyOnMultipleMethods(this.sandbox, this.viewManager.productVersions.currentView, ["expandContent", "collapseContent"]);
                spyOnMultipleMethods(this.sandbox, this.viewManager.toggleContentButton.currentView, ["setButtonExpanded", "setButtonCollapsed"]);
            },
            teardown: function () {
                this.sandbox.restore();
            }
        });

        test("It should render the toggle content button view", function() {
            notOk(this.fixture.find(this.viewManager.toggleContentButton.el).is(":empty"));
        });

        test("It should listen to content:expand from toggle button and expand content", function () {
            this.viewManager.toggleContentButton.currentView.trigger("content:expand");

            sinon.assert.calledOnce(this.viewManager.expandContent);
        });

        test("It should listen to content:collapse from toggle button and collapse content", function () {
            this.viewManager.toggleContentButton.currentView.trigger("content:collapse");

            sinon.assert.calledOnce(this.viewManager.collapseContent);
        });

        test("It should listen to change:state from model and expand content (STABLE)", function () {
            this.viewManager.$el.removeClass('expanded');

            this.model.trigger("change:state");

            ok(this.viewManager.$el.hasClass('expanded'));
        });

        test("It should listen to change:state from model and unmark the container as 'expanded' (READY_TO_UPGRADE)", function () {
            this.viewManager.$el.addClass('expanded');

            this.model.set("state", ClusterStateModel.states.READY_TO_UPGRADE);

            notOk(this.viewManager.$el.hasClass('expanded'));
        });

        test("It should listen to change:state from model and unmark the container as 'expanded' (MIXED)", function () {
            this.viewManager.$el.addClass('expanded');

            this.model.set("state", ClusterStateModel.states.MIXED);

            notOk(this.viewManager.$el.hasClass('expanded'));
        });

        test("It should listen to change:state from model and unmark the container as 'expanded' (READY_TO_RUN_UPGRADE_TASKS)", function () {
            this.viewManager.$el.addClass('expanded');

            this.model.set("state", ClusterStateModel.states.READY_TO_RUN_UPGRADE_TASKS);

            notOk(this.viewManager.$el.hasClass('expanded'));
        });

        test("It should listen to change:state from model and unmark the container as 'expanded' (RUNNING_UPGRADE_TASKS)", function () {
            this.viewManager.$el.addClass('expanded');

            this.model.set("state", ClusterStateModel.states.RUNNING_UPGRADE_TASKS);

            notOk(this.viewManager.$el.hasClass('expanded'));
        });

        test("It should listen to change:icon from cluster health view and change icon", function() {
            this.viewManager.clusterHealth.currentView.trigger("change:icon", "foo");

            ok(this.viewManager.clusterHealth.$el.hasClass("foo"));
        });

        test("expandContent() should expand content on all views", function() {
            this.viewManager.expandContent();

            sinon.assert.calledOnce(this.viewManager.clusterHealth.currentView.expandContent);
            sinon.assert.calledOnce(this.viewManager.pluginCompatibility.currentView.expandContent);
            sinon.assert.calledOnce(this.viewManager.productVersions.currentView.expandContent);
            sinon.assert.calledOnce(this.viewManager.toggleContentButton.currentView.setButtonExpanded);
        });

        test("expandContent() should mark the container as 'expanded'", function() {
            this.viewManager.$el.removeClass('expanded');

            this.viewManager.expandContent();

            ok(this.viewManager.$el.hasClass('expanded'));
        });

        test("collapseContent() should collapse content on all views", function() {
            this.viewManager.collapseContent();

            sinon.assert.calledOnce(this.viewManager.clusterHealth.currentView.collapseContent);
            sinon.assert.calledOnce(this.viewManager.pluginCompatibility.currentView.collapseContent);
            sinon.assert.calledOnce(this.viewManager.productVersions.currentView.collapseContent);
            sinon.assert.calledOnce(this.viewManager.toggleContentButton.currentView.setButtonCollapsed);
        });

        test("collapseContent() should unmark the container as 'expanded'", function() {
            this.viewManager.$el.addClass('expanded');

            this.viewManager.collapseContent();

            notOk(this.viewManager.$el.hasClass('expanded'));
        });

        test("It propagates the click:health-check event from the ClusterHealth view", function() {
            var callback = this.sandbox.stub();
            this.listener.listenTo(this.viewManager, "click:health-check", callback);

            this.viewManager.clusterHealth.currentView.trigger("click:health-check");

            sinon.assert.calledOnce(callback);
        });

        test("It propagates the click:check-addons event from the PluginCompatibility view", function() {
            var callback = this.sandbox.stub();
            this.listener.listenTo(this.viewManager, "click:check-addons", callback);

            this.viewManager.pluginCompatibility.currentView.trigger("click:check-addons");

            sinon.assert.calledOnce(callback);
        });

        test("It propagates the click:download-latest event from the ProductVersions view", function() {
            var callback = this.sandbox.stub();
            this.listener.listenTo(this.viewManager, "click:download-latest", callback);

            this.viewManager.productVersions.currentView.trigger("click:download-latest");

            sinon.assert.calledOnce(callback);
        });
    });
});
