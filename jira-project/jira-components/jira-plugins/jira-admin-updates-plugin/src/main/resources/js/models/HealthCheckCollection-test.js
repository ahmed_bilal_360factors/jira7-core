AJS.test.require([
    "com.atlassian.jira.jira-admin-updates-plugin:js-app"
], function() {
    "use strict";

    require([
        'underscore',
        'backbone',
        'admin-updates/models/healthcheck',
        'admin-updates/services/preupdate/clusterhealth'
    ], function (
        _,
        Backbone,
        HealthCheckModel,
        ClusterHealthService
    ) {
        // Without this timeout, QUnit will wait indefinitely for async tests whose done() callback isn't called.
        QUnit.config.testTimeout = 5000;

        function resolvedPromise(value) {
            return new Promise(function(resolve) {
                resolve(value);
            });
        }

        function rejectedPromise(value) {
            return new Promise(function(resolve, reject) {
                reject(value);
            });
        }

        function createHC(severity, name) {
            return {
                severity: severity,
                name: name,
                completeKey: severity + "." + name,
                isHealthy: severity === HealthCheckModel.severity.UNDEFINED,
                failureReason: name + " reason"
            };
        }

        module("admin-updates/models/healthcheck", {
            setup: function () {
                this.listener = _.extend({}, Backbone.Events);

                this.sandbox = sinon.sandbox.create();
                this.sandbox.stub(ClusterHealthService, "checkDetails").returns(resolvedPromise());

                this.context = AJS.test.mockableModuleContext();
                this.context.mock("admin-updates/services/preupdate/clusterhealth", ClusterHealthService);

                this.healthChecks = {
                    critical: [
                        createHC(HealthCheckModel.severity.CRITICAL, "critical1")
                    ],
                    major: [
                        createHC(HealthCheckModel.severity.MAJOR, "major1")
                    ],
                    warning: [
                        createHC(HealthCheckModel.severity.WARNING, "warning1"),
                        createHC(HealthCheckModel.severity.WARNING, "warning2"),
                        createHC(HealthCheckModel.severity.WARNING, "warning3")
                    ],
                    minor: [],
                    success: [
                        createHC(HealthCheckModel.severity.UNDEFINED, "undefined1"),
                        createHC(HealthCheckModel.severity.UNDEFINED, "undefined2"),
                        createHC(HealthCheckModel.severity.UNDEFINED, "undefined3"),
                        createHC(HealthCheckModel.severity.UNDEFINED, "undefined4"),
                        createHC(HealthCheckModel.severity.UNDEFINED, "undefined5"),
                        createHC(HealthCheckModel.severity.UNDEFINED, "undefined6"),
                        createHC(HealthCheckModel.severity.UNDEFINED, "undefined7"),
                        createHC(HealthCheckModel.severity.UNDEFINED, "undefined8")
                    ]
                };

                this.HealthCheckCollection = this.context.require("admin-updates/models/healthcheckcollection");
                this.collection = new this.HealthCheckCollection(
                    this.healthChecks.success.concat(
                        this.healthChecks.major,
                        this.healthChecks.critical,
                        this.healthChecks.minor,
                        this.healthChecks.warning
                    )
                );
                this.sandbox.spy(this.collection, "fetch");
            },
            teardown: function() {
                this.sandbox.restore();
            }
        });

        test("fetch() should get the health checks", function() {
            this.collection.fetch();

            sinon.assert.calledOnce(ClusterHealthService.checkDetails);
        });

        test("When a request is rejected, it should emit a sync:error event", function(assert) {
            assert.expect(0);
            ClusterHealthService.checkDetails.restore();
            this.sandbox.stub(ClusterHealthService, "checkDetails").returns(rejectedPromise());

            this.listener.listenTo(this.collection, "sync:error", assert.async());

            this.collection.fetch();
        });

        test("When a request is rejected, it should emit a healthcheck:cancelled event", function(assert) {
            assert.expect(0);
            ClusterHealthService.checkDetails.restore();
            this.sandbox.stub(ClusterHealthService, "checkDetails").returns(rejectedPromise());

            this.listener.listenTo(this.collection, "healthcheck:cancelled", assert.async());

            this.collection.fetch();
        });

        test("comparator() should sort properly", function() {
            var success = new HealthCheckModel(createHC(HealthCheckModel.severity.UNDEFINED, "undefined"));
            var minor = new HealthCheckModel(createHC(HealthCheckModel.severity.MINOR, "minor"));
            var warning = new HealthCheckModel(createHC(HealthCheckModel.severity.WARNING, "warning"));
            var major = new HealthCheckModel(createHC(HealthCheckModel.severity.MAJOR, "major"));
            var critical = new HealthCheckModel(createHC(HealthCheckModel.severity.CRITICAL, "critical"));

            equal(this.collection.comparator(success, success), 0);
            equal(this.collection.comparator(success, minor), 1);
            equal(this.collection.comparator(success, warning), 1);
            equal(this.collection.comparator(success, major), 1);
            equal(this.collection.comparator(success, critical), 1);

            equal(this.collection.comparator(minor, success), -1);
            equal(this.collection.comparator(minor, minor), 0);
            equal(this.collection.comparator(minor, warning), 1);
            equal(this.collection.comparator(minor, major), 1);
            equal(this.collection.comparator(minor, critical), 1);

            equal(this.collection.comparator(warning, success), -1);
            equal(this.collection.comparator(warning, minor), -1);
            equal(this.collection.comparator(warning, warning), 0);
            equal(this.collection.comparator(warning, major), 1);
            equal(this.collection.comparator(warning, critical), 1);

            equal(this.collection.comparator(major, success), -1);
            equal(this.collection.comparator(major, minor), -1);
            equal(this.collection.comparator(major, warning), -1);
            equal(this.collection.comparator(major, major), 0);
            equal(this.collection.comparator(major, critical), 1);

            equal(this.collection.comparator(critical, success), -1);
            equal(this.collection.comparator(critical, minor), -1);
            equal(this.collection.comparator(critical, warning), -1);
            equal(this.collection.comparator(critical, major), -1);
            equal(this.collection.comparator(critical, critical), 0);

        });

        test("areAllChecksHealthy() should return false", function() {
            notOk(this.collection.areAllChecksHealthy());
        });

        test("areAllChecksHealthy() should return true", function() {
            var collection = new this.HealthCheckCollection([
                createHC(HealthCheckModel.severity.UNDEFINED, "success1"),
                createHC(HealthCheckModel.severity.UNDEFINED, "success2"),
                createHC(HealthCheckModel.severity.UNDEFINED, "success3")
            ]);

            ok(collection.areAllChecksHealthy());
        });

        test("getMostSevereHealthCheck() should return the most severe health check (duh)", function() {
            var healthCheck = this.collection.getMostSevereHealthCheck();

            deepEqual(healthCheck.get("completeKey"), this.healthChecks.critical[0].completeKey);
        });

        test("getErrors() should return a list of 'critical' and 'major' errors", function() {
            var errors = this.collection.getErrors();

            equal(errors.length, this.healthChecks.critical.length + this.healthChecks.major.length);
        });

        test("getWarnings() should return a list of warnings", function() {
            var warnings = this.collection.getWarnings();

            equal(warnings.length, this.healthChecks.warning.length);
        });
    });
});
