define('admin-updates/views/intro/pageintro', ['require'], function(require) {
    'use strict';

    var Marionette = require('marionette');

    return Marionette.ItemView.extend({
        el: '#page-intro-container',
        ui: {
            documentationLink: '#page-intro-documentation-link'
        },
        triggers: {
            'click @ui.documentationLink': {
                event: 'click:documentation',
                preventDefault: false
            }
        }
    });
});
