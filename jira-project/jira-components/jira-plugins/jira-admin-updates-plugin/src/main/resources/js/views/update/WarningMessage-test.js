AJS.test.require([
    "com.atlassian.jira.jira-admin-updates-plugin:js-app"
], function() {
    "use strict";

    require([
        'jquery',
        'admin-updates/views/update/warningmessage',
        'admin-updates/models/clusterstate'
    ], function (
        jQuery,
        WarningMessage,
        ClusterStateModel
    ) {
        module("admin-updates/views/update/warningmessage", {
            setup: function () {
                this.fixture = jQuery('#qunit-fixture');

                this.defaultOpts = {
                    el: this.fixture,
                    model: new ClusterStateModel({state: ClusterStateModel.states.STABLE})
                };

                this.warningMessage = new WarningMessage(this.defaultOpts);
                this.warningMessage.render();
            }
        });

        test("showMessage() should show the message", function () {
            this.warningMessage.ui.message.addClass('hidden');

            this.warningMessage.showMessage();

            notOk(this.warningMessage.ui.message.hasClass('hidden'));
        });

        test("hideMessage() should hide the message", function () {
            this.warningMessage.ui.message.removeClass('hidden');

            this.warningMessage.hideMessage();

            ok(this.warningMessage.ui.message.hasClass('hidden'));
        });
    });
});
