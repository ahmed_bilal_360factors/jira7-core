define("admin-updates/controllers/preupdateviewmanager", ['require'], function (require) {
    "use strict";

    var Marionette = require('marionette');
    var ClusterHealthView = require('admin-updates/views/preupdate/clusterhealth');
    var PluginCompatibilityView = require('admin-updates/views/preupdate/plugincompatibility');
    var ProductVersionsView = require('admin-updates/views/preupdate/productversions');
    var ToggleContentButtonView = require('admin-updates/views/preupdate/togglecontentbutton');
    var ClusterStateModel = require('admin-updates/models/clusterstate');

    return Marionette.Layout.extend({
        el: "#pre-update-tasks-container",
        regions: {
            toggleContentButton: "#toggle-content-button-container",
            clusterHealth: "#cluster-health-container",
            pluginCompatibility: "#plugin-compatibility-container",
            productVersions: "#product-versions-container"
        },
        /**
         * @property {ClusterStateModel} model
         * @property {HealthCheckCollection} collection
         */
        initialize: function() {
            this.clusterHealth.show(new ClusterHealthView({
                model: this.model,
                collection: this.collection
            }));
            this.pluginCompatibility.show(new PluginCompatibilityView({model: this.model}));
            this.productVersions.show(new ProductVersionsView({model: this.model}));
            this.toggleContentButton.show(new ToggleContentButtonView({model: this.model}));

            this.listenTo(this.model, "change:state", function() {
                if(this.model.get("state") === ClusterStateModel.states.STABLE) {
                    this._setContainerExpanded();
                } else {
                    this._setContainerCollapsed();
                }
            });

            this.listenTo(this.toggleContentButton.currentView, {
                "content:expand": function() {
                    this.expandContent();
                },
                "content:collapse": function() {
                    this.collapseContent();
                }
            });

            this.listenTo(this.clusterHealth.currentView, {
                "change:icon": function(icon) {
                    this._changeBlockIcon(this.clusterHealth, icon);
                },
                "click:health-check": function() {
                    this.trigger("click:health-check");
                }
            });

            this.listenTo(this.pluginCompatibility.currentView, {
                "click:check-addons": function() {
                    this.trigger("click:check-addons");
                }
            });

            this.listenTo(this.productVersions.currentView, {
                "click:download-latest": function() {
                    this.trigger("click:download-latest");
                }
            });
        },
        /**
         * @param {Marionette.Region} region
         * @param {string} icon
         * @private
         */
        _changeBlockIcon: function(region, icon) {
            if(!region.$el.hasClass(icon)) {
                region.$el.removeClass(function(k, classes) {
                    return classes.replace(/^.*(zdu-icon-[^ ]+).*$/, "$1");
                });
                region.$el.addClass(icon);
            }
        },
        _setContainerExpanded: function() {
            this.$el.addClass('expanded');
        },
        _setContainerCollapsed: function() {
            this.$el.removeClass('expanded');
        },
        expandContent: function() {
            this._setContainerExpanded();
            this.clusterHealth.currentView.expandContent();
            this.pluginCompatibility.currentView.expandContent();
            this.productVersions.currentView.expandContent();
            this.toggleContentButton.currentView.setButtonExpanded();
        },
        collapseContent: function() {
            this._setContainerCollapsed();
            this.clusterHealth.currentView.collapseContent();
            this.pluginCompatibility.currentView.collapseContent();
            this.productVersions.currentView.collapseContent();
            this.toggleContentButton.currentView.setButtonCollapsed();
        }
    });
});
