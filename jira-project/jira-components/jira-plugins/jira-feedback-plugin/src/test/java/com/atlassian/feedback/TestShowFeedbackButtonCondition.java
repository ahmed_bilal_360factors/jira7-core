package com.atlassian.feedback;

import com.atlassian.jira.config.FeatureManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestShowFeedbackButtonCondition {
    @Mock
    private FeatureManager featureManager;

    private ShowFeedbackButtonCondition showFeedbackButtonCondition;

    @Before
    public void setup() {
        showFeedbackButtonCondition = new ShowFeedbackButtonCondition(featureManager);

        when(featureManager.isEnabled(FeedbackPluginDarkFeatures.ENABLE_HEADER_FEEDBACK_KEY)).thenReturn(false);
        when(featureManager.isEnabled(FeedbackPluginDarkFeatures.DISABLE_HEADER_FEEDBACK_KEY)).thenReturn(false);
    }

    @Test
    public void shouldReturnNotDisplayIfNoDarkFeatureFlagIsEnabled() {
        assertThat(showFeedbackButtonCondition.shouldDisplay(null, null), is(false));
    }

    @Test
    public void shouldReturnNotDisplayWhenDisableDarkFeatureFlagIsEnabled() {
        when(featureManager.isEnabled(FeedbackPluginDarkFeatures.ENABLE_HEADER_FEEDBACK_KEY)).thenReturn(true);
        assertThat(showFeedbackButtonCondition.shouldDisplay(null, null), is(true));

        when(featureManager.isEnabled(FeedbackPluginDarkFeatures.DISABLE_HEADER_FEEDBACK_KEY)).thenReturn(true);
        assertThat(showFeedbackButtonCondition.shouldDisplay(null, null), is(false));
    }

    @Test
    public void shouldReturnDisplayWhenEnableDarkFeatureIsEnabled() {
        when(featureManager.isEnabled(FeedbackPluginDarkFeatures.ENABLE_HEADER_FEEDBACK_KEY)).thenReturn(true);
        assertThat(showFeedbackButtonCondition.shouldDisplay(null, null), is(true));
    }
}
