package com.atlassian.feedback;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.json.marshal.Jsonable;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestFeedbackButtonWebResourceDataProvider {

    @Mock
    private FeatureManager featureManager;

    @Mock
    private DefaultIssueCollectorUrlManager defaultIssueCollectorUrlManager;

    private FeedbackButtonWebResourceDataProvider feedbackButtonWebResourceDataProvider;

    /**
     * This gets a Jsonable object from the WebResourceDataProvider and converts it into a JsonElement,
     * then returns if the object whether it has the property.
     * @param property
     * @return JsonElement is the property exist or null otherwise;
     * @throws IOException
     */
    private JsonElement getPropertyFromDataProvider(final String property) throws IOException {
        final Jsonable jsonable = feedbackButtonWebResourceDataProvider.get();
        Writer writer = new StringWriter();
        jsonable.write(writer);

        final JsonElement parsedJson = new JsonParser().parse(writer.toString());
        return parsedJson.getAsJsonObject().get(property);
    }

    @Before
    public void setup() {
        feedbackButtonWebResourceDataProvider = new FeedbackButtonWebResourceDataProvider(featureManager, defaultIssueCollectorUrlManager);

        when(featureManager.isEnabled(FeedbackPluginDarkFeatures.ENABLE_HEADER_FEEDBACK_KEY)).thenReturn(false);
        when(featureManager.isEnabled(FeedbackPluginDarkFeatures.DISABLE_HEADER_FEEDBACK_KEY)).thenReturn(false);
    }

    @Test
    public void feedbackButtonShouldBeMarkedAsEnabledIfFeedbackEnableDarkFeatureIsPresent() throws IOException {
        when(featureManager.isEnabled(FeedbackPluginDarkFeatures.ENABLE_HEADER_FEEDBACK_KEY)).thenReturn(true);

        final JsonElement propertyFromJson = getPropertyFromDataProvider("isHeaderFeedbackButtonEnabled");

        assertNotNull("The property is missing from the Jsonable Object", propertyFromJson);
        assertThat(propertyFromJson.toString(), is("true"));
    }

    @Test
    public void feedbackButtonShouldBeMarkedAsDisabledIfFeedbackEnableDarkFeatureIsNotPresent() throws IOException {
        final JsonElement propertyFromJson = getPropertyFromDataProvider("isHeaderFeedbackButtonEnabled");

        assertNotNull("The property is missing from the Jsonable Object", propertyFromJson);
        assertThat(propertyFromJson.toString(), is("false"));
    }

    @Test
    public void feedbackButtonShouldBeMarkedAsDisabledIfHeaderFeedbackDisableIsPresent() throws IOException {
        when(featureManager.isEnabled(FeedbackPluginDarkFeatures.ENABLE_HEADER_FEEDBACK_KEY)).thenReturn(true);
        when(featureManager.isEnabled(FeedbackPluginDarkFeatures.DISABLE_HEADER_FEEDBACK_KEY)).thenReturn(true);

        final JsonElement propertyFromJson = getPropertyFromDataProvider("isHeaderFeedbackButtonEnabled");

        assertNotNull("The property is missing from the Jsonable Object", propertyFromJson);
        assertThat(propertyFromJson.toString(), is("false"));
    }
}
