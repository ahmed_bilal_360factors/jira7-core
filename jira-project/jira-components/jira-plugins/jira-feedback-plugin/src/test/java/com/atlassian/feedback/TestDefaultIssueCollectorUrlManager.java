package com.atlassian.feedback;

import com.atlassian.jira.config.properties.ApplicationProperties;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestDefaultIssueCollectorUrlManager {

    @Mock
    private ApplicationProperties jiraApplicationProperties;

    private DefaultIssueCollectorUrlManager defaultIssueCollectorUrlManager;

    @Before
    public void setup() {
        defaultIssueCollectorUrlManager = new DefaultIssueCollectorUrlManager(jiraApplicationProperties);
        when(jiraApplicationProperties.getString(DefaultIssueCollectorUrlManager.JiraType.CORE.getKey())).thenReturn(null);
    }

    @Test
    public void defaultIssueCollectorManagerReturnsAnUrlForCore() throws IOException {
        final DefaultIssueCollectorUrlManager.JiraType core = DefaultIssueCollectorUrlManager.JiraType.CORE;
        String actualUrl = defaultIssueCollectorUrlManager.getUrl(core);

        Assert.assertEquals("defaultIssueCollectorManager did not return the correct URL", core.getDefaultUrl(), actualUrl);
    }

    @Test
    public void defaultIssueCollectorManagerReturnsAnUrlForSoftware() throws IOException {
        final DefaultIssueCollectorUrlManager.JiraType software = DefaultIssueCollectorUrlManager.JiraType.SOFTWARE;
        String actualUrl = defaultIssueCollectorUrlManager.getUrl(software);

        Assert.assertEquals("defaultIssueCollectorManager did not return the correct URL", software.getDefaultUrl(), actualUrl);
    }

    @Test
    public void defaultIssueCollectorManagerReturnsAnUrlForServiceDesk() throws IOException {
        final DefaultIssueCollectorUrlManager.JiraType serviceDesk = DefaultIssueCollectorUrlManager.JiraType.SERVICE_DESK;
        String actualUrl = defaultIssueCollectorUrlManager.getUrl(serviceDesk);

        Assert.assertEquals("defaultIssueCollectorManager did not return the correct URL", serviceDesk.getDefaultUrl(), actualUrl);
    }

    @Test
    public void defaultIssueCollectorManagerReturnsAnUrlForDefault() throws IOException {
        final DefaultIssueCollectorUrlManager.JiraType unknown = DefaultIssueCollectorUrlManager.JiraType.DEFAULT;
        String actualUrl = defaultIssueCollectorUrlManager.getUrl(unknown);

        Assert.assertEquals("defaultIssueCollectorManager did not return the correct URL", unknown.getDefaultUrl(), actualUrl);
    }

    @Test
    public void defaultIssueCollectorManagerShouldReturnOverrideUrlWhenSet() throws IOException {
        String expectedUrl = "https://www.override.com/default";

        final DefaultIssueCollectorUrlManager.JiraType jiraType = DefaultIssueCollectorUrlManager.JiraType.CORE;
        when(jiraApplicationProperties.getString(jiraType.getKey())).thenReturn(expectedUrl);

        String actualUrl = defaultIssueCollectorUrlManager.getUrl(jiraType);

        Assert.assertEquals("defaultIssueCollectorManager did not return the correct URL", expectedUrl, actualUrl);
    }

    @Test
    public void defaultIssueCollectorManagerShouldReturnDefaultUrlsWhenNotSet() throws IOException {
        final DefaultIssueCollectorUrlManager.JiraType jiraType = DefaultIssueCollectorUrlManager.JiraType.CORE;
        when(jiraApplicationProperties.getString(jiraType.getKey())).thenReturn(null);

        String actualUrl = defaultIssueCollectorUrlManager.getUrl(jiraType);

        Assert.assertEquals("defaultIssueCollectorManager did not return the correct URL", jiraType.getDefaultUrl(), actualUrl);
    }
}
