require([
    'jquery',
    'jira/loading/loading',
    'underscore',
    'jira/message',
    'jira/ajs/ajax/smart-ajax',
    'wrm/data',
    'jira/feedback/issue-collector'
], function(
    jQuery,
    LoadingIndicator,
    _,
    Messages,
    SmartAjax,
    WrmData,
    IssueCollector
) {
    "use strict";

    /**
     * These keys are used to get the Issue Collector URL from the Data Provider.
     * @type {String} Keys to use for the data provider.
     */
    var projectKey = {
        'software': "jira.feedback.plugin.issue.collector.software",
        'business': "jira.feedback.plugin.issue.collector.core",
        'service_desk': "jira.feedback.plugin.issue.collector.service.desk",
        'unknown': "jira.feedback.plugin.issue.collector.default"
    };

    var loading = false;
    function initializeIssueCollector() {
        if (loading) {
            return;
        }

        loadingStarts();

        // once all the Issue Collector resources are downloaded and evaluated, there is a call to window.ATL_JQ_PAGE_PROPS.triggerFunction
        window.ATL_JQ_PAGE_PROPS = {
            "triggerFunction": function(showIssueCollectorDialog) {
                // The downloaded Issue Collector resources call this method and then create a DOM node to use as the container for the dialog.
                // We need to defer this call so the container is already on the page before showing the dialog.
                _.defer(function() {
                    loadingEnds(true);
                    showIssueCollectorDialog();
                });
            },
            fieldValues: IssueCollector.getFieldValues()
        };

        jQuery.ajax({
            url: feedbackCollectorData[projectKey[IssueCollector.getProjectType()]],
            type: "get",
            cache: true,
            timeout: 10000, // timeout before error function will be called - this is needed for errors like bad gateway where jQuery will not reject the promise
            dataType: "script"
        })
            .fail(function(xhr) {
                loadingEnds();
                Messages.showErrorMsg(SmartAjax.buildSimpleErrorContent(xhr), {
                    closeable: true
                });
            });
    }

    function loadingStarts() {
        loading = true;
        AJS.dim();
        LoadingIndicator.showLoadingIndicator();
    }

    function loadingEnds(success) {
        loading = false;
        // remove transition from the dim so there is no "blink" when Issue Collector gets displayed
        // do not do it on failure, so the layer is disabled smoothly then
        if (success) {
            AJS.dim.$dim.css("transition", "none");
        }
        AJS.undim();

        //Some pages (I'm looking at you, IssueNav) provide a different implementation of AJS.undim()
        //that removes AJS.dim.$dim, so we have to check for that special case here.
        if (success && AJS.dim.$dim) {
            AJS.dim.$dim.css("transition", "");
        }
        LoadingIndicator.hideLoadingIndicator();
    }

    var feedbackCollectorData = WrmData.claim("com.atlassian.feedback.jira-feedback-plugin:button-resources-init.data");

    if (feedbackCollectorData && feedbackCollectorData.isHeaderFeedbackButtonEnabled) {
        jQuery(document).on("click", ".jira-feedback-plugin", function(e) {
            e.preventDefault();
            initializeIssueCollector();
        });
    }
});
