define('jira/feedback/issue-collector',['require'], function(require) {
    "use strict";

    var jQuery = require('jquery');
    var Meta = require('jira/util/data/meta');

    /**
     * Maps project type to the selection values in project_type_id custom field in Feedback Project on JAC.
     * @type {String} selection value to represent the project type
     */
    var projectTypeMap = {
        'software': '13831',
        'business': '13830',
        'service_desk': '13832',
        'unknown': '13833'
    };

    /**
     * Maps the hidden fields to the actual custom field number for the Feedback Project on JAC.
     * @type {String} the property name in fieldValues to store the data in.
     */
    var customFieldMap = {
        'page_path': 'customfield_16632',
        'project_type_id': 'customfield_16630',
        'version': 'customfield_10932'
    };

    /**
     * This expects @link {getMetadata()} to include a meta tag with the issue-project-type.
     * If this does not exist, e.g. in the JIRA admin page, then it defaults it to 'unknown'
     *
     * This will work if used in the Global Issue Viewer or Project Issue Viewer.
     *
     * @returns {string | undefined} the projectType or undefined
     */
    function getProjectTypeFromMeta() {
        var projectType = Meta.get('issue-project-type');

        return projectType;
    }

    /**
     * This attempts to get the project type from the project sidebar.
     *
     * projectData can throw an exception if the project side does not exist.
     *
     * TODO: JDEV-36364 Instead of catching exceptions, check if the project sidebar exist by looking at the URL
     *
     * @returns {string | undefined} the projectType or undefined
     */
    function getProjectTypeFromProjectSidebar() {
        var projectType;
        try {
            var projectData = require('jira/api/projects');
            projectType = projectData.getCurrentProjectType();
        }
        catch (ignored) {
            projectType = undefined;
        }

        return projectType;
    }

    return {
        /**
         * This returns the current version of JIRA.
         *
         * @returns {string} JIRA version or 'not detected' if Meta.get returns 'undefined'
         */
        getJiraVersion : function() {
            var version = Meta.get('version-number');

            return version ? version : 'not detected';
        },

        getProjectType : function() {
            var projectType = getProjectTypeFromMeta();

            if (!projectType || !projectTypeMap[projectType]) {
                projectType = getProjectTypeFromProjectSidebar();

                if (!projectType || !projectTypeMap[projectType]) {
                    projectType = 'unknown';
                }
            }

            return projectType ? projectType : 'unknown';
        },

        /**
         * This attempts to get the the projectType from the issue meta data or the project sidebar then maps it to the
         * projectTypeId.
         *
         * It can fail to find the projectType if the user is on a page that is not associated to a project
         * i.e. JIRA Admin Page, Dashboard.
         *
         * @param projectTypeMap maps a project type to a selection values in JAC
         * @returns {string} projectTypeId is the selection value for the Feedback Plugin in JAC
         */
        getProjectTypeId : function() {
            var projectType = this.getProjectType();

            return projectTypeMap[projectType] ? projectTypeMap[projectType] : projectTypeMap['unknown'];
        },

        getPath : function() {
            return window.location.pathname;
        },

        /**
         * This gathers the information for the hidden custom fields for the Issue Collector.
         *
         * Customfield_ids are hardcoded because this uses the Issue Collector from JAC and will not change.
         *
         * @returns {object} fieldValues that contains 3 custom fields
         */
        getFieldValues : function() {
            var fieldValues = {};

            fieldValues[customFieldMap['page_path']] = this.getPath();
            fieldValues[customFieldMap['project_type_id']] = this.getProjectTypeId(projectTypeMap);
            fieldValues[customFieldMap['version']] = this.getJiraVersion();

            return fieldValues;
        }
    };
});
