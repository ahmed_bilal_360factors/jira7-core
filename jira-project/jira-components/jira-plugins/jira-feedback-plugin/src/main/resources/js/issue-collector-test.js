AJS.test.require(["com.atlassian.feedback.jira-feedback-plugin:button-resources-test"], function () {
    "use strict";

    var projectTypeMap = {
        'software': '13831',
        'business': '13830',
        'service_desk': '13832',
        'unknown': '13833'
    };

    var customFieldMap = {
        'page_path': 'customfield_16632',
        'project_type_id': 'customfield_16630',
        'version': 'customfield_10932'
    };

    var jQuery = require('jquery');

    module('jira/feedback/issue-collector', {

        setup: function () {
            this.Meta = {
                get : sinon.stub()
            };

            this.projectData = {
                getCurrentProjectType : sinon.stub()
            };

            this.fakeJquery = sinon.stub();

            this.context = AJS.test.mockableModuleContext();
            this.context.mock('jira/util/data/meta', this.Meta);
            this.context.mock('jira/api/projects', this.projectData);
            this.context.mock('jquery', this.fakeJquery);

            this.issueCollector = this.context.require('jira/feedback/issue-collector');
        }
    });

    test('getProjectType should be able to get the projectType for software from Meta Data', function() {
        this.Meta.get.withArgs('issue-project-type').returns("software");

        equal(this.issueCollector.getProjectType(), "software");
    });

    test('getProjectType should be able to get the projectType for business from Meta Data', function() {
        this.Meta.get.withArgs('issue-project-type').returns("business");

        equal(this.issueCollector.getProjectType(), "business");
    });

    test('getProjectType should be able to get the projectType for service desk from Meta Data', function() {
        this.Meta.get.withArgs('issue-project-type').returns("service_desk");

        equal(this.issueCollector.getProjectType(), "service_desk");
    });

    test('getProjectType should return unknown for an unseen projectType', function() {
        this.Meta.get.withArgs('issue-project-type').returns("doge");
        this.projectData.getCurrentProjectType.returns("doge");

        equal(this.issueCollector.getProjectType(), "unknown");
    });

    test('getProjectType should return unknown if the projectType is undefined', function() {
        this.Meta.get.withArgs('issue-project-type').returns(undefined);
        this.projectData.getCurrentProjectType.returns(undefined);

        equal(this.issueCollector.getProjectType(), "unknown");
    });

    test('getProjectType should return unknown if the projectType is an empty string', function() {
        this.Meta.get.withArgs('issue-project-type').returns("");
        this.projectData.getCurrentProjectType.returns("");

        equal(this.issueCollector.getProjectType(), "unknown");
    });

    test('getProjectType should be able to get the projectType for software from Project Sidebar', function() {
        this.Meta.get.withArgs('issue-project-type').returns(undefined);
        this.projectData.getCurrentProjectType.returns("software");

        equal(this.issueCollector.getProjectType(), "software");
    });

    test('getProjectType should be able to get the projectType for business from Project Sidebar', function() {
        this.Meta.get.withArgs('issue-project-type').returns(undefined);
        this.projectData.getCurrentProjectType.returns("business");

        equal(this.issueCollector.getProjectType(), "business");
    });

    test('getProjectType should be able to get the projectType for service_desk from Project Sidebar', function() {
        this.Meta.get.withArgs('issue-project-type').returns(undefined);
        this.projectData.getCurrentProjectType.returns("service_desk");

        equal(this.issueCollector.getProjectType(), "service_desk");
    });

    test('getProjectTypeID should map service desk to projectTypeId', function() {
        this.issueCollector.getProjectType = function() {
            return "service_desk";
        };

        equal(this.issueCollector.getProjectTypeId(), projectTypeMap.service_desk);
    });

    test('getProjectTypeID should map business to projectTypeId', function() {
        this.issueCollector.getProjectType = function() {
            return "business";
        };

        equal(this.issueCollector.getProjectTypeId(), projectTypeMap.business);
    });

    test('getProjectTypeID should map software to projectTypeId', function() {
        this.issueCollector.getProjectType = function() {
            return "software";
        };

        equal(this.issueCollector.getProjectTypeId(), projectTypeMap.software);
    });

    test('getProjectTypeID should map an incorrect projectType to unknown', function() {
        this.issueCollector.getProjectType = function() {
            return "doge";
        };

        equal(this.issueCollector.getProjectTypeId(), projectTypeMap.unknown);
    });


    test('getJiraVersion should return the jira version provided by Meta', function() {
        var expected = "7.1.0-OD-03-038";
        this.Meta.get.withArgs('version-number').returns(expected);

        equal(this.issueCollector.getJiraVersion(), expected);
    });

    test('getJiraVersion should return "non detected" if Meta does not have the information', function() {
        this.Meta.get.withArgs('version-number').returns(undefined);

        equal(this.issueCollector.getJiraVersion(), "not detected");
    });

    test('getFieldValues should call getJiraVersion, getProjectTypeId and getPath', function () {
        var spy1 = sinon.spy(this.issueCollector, "getJiraVersion");
        var spy2 = sinon.spy(this.issueCollector, "getProjectTypeId");
        var spy3 = sinon.spy(this.issueCollector, "getPath");

        this.issueCollector.getFieldValues();

        ok(spy1.calledOnce);
        ok(spy2.calledOnce);
        ok(spy3.calledOnce);
    });

    test('getFieldValues should assign the version, projectTypeId and path correctly to the custom field', function () {
        var path = "some/path/";
        var version = "jira-version-123";
        var projectTypeId = "43";

        this.issueCollector.getPath = function() {
            return path;
        };

        this.issueCollector.getProjectTypeId = function() {
            return projectTypeId;
        };

        this.issueCollector.getJiraVersion = function() {
            return version;
        };

        var fieldValues = this.issueCollector.getFieldValues();

        equal(fieldValues[customFieldMap['page_path']], path);
        equal(fieldValues[customFieldMap['project_type_id']], projectTypeId);
        equal(fieldValues[customFieldMap['version']], version);
    });
});
