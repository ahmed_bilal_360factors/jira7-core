package com.atlassian.feedback;

public class FeedbackPluginDarkFeatures {
    /**
     * This enables the feedback button in the headernav.right.context and disables the feedback button in the sidebar
     */
    public static final String ENABLE_HEADER_FEEDBACK_KEY = "com.atlassian.feedback.feedback-button-move-to-header-enable";

    /**
     * This disables the feedback button in the headernav.right.context
     */
    public static final String DISABLE_HEADER_FEEDBACK_KEY = "com.atlassian.feedback.header-feedback-button-disable";
}
