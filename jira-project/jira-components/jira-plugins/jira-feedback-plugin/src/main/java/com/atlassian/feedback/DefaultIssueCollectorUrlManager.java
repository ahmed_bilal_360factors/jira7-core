package com.atlassian.feedback;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.annotations.VisibleForTesting;

import javax.inject.Inject;
import javax.inject.Named;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * DefaultIssueCollectorUrlManager is used to retrieve and store the Issue Collector URLs. This is
 * mainly for testing purposes, to avoid having to rely on JAC during the web driver tests.
 */
@Named
public class DefaultIssueCollectorUrlManager {

    private final ApplicationProperties jiraApplicationProperties;

    @Inject
    public DefaultIssueCollectorUrlManager(@ComponentImport ApplicationProperties jiraApplicationProperties) {
        this.jiraApplicationProperties = jiraApplicationProperties;
    }

    /**
     * Retrieves the Issue Collector URL stored in application properties against one of the keys in JiraType,
     * or the default URL for the product type if none can be found. If validation fails, then it returns default the URL.
     *
     * @param type {JiraType} Jira project type
     * @return {String} Issue Collector URL for the specified project type
     */
    public String getUrl(JiraType type) {
        final String dirtyUrl = jiraApplicationProperties.getString(type.getKey());

        if (dirtyUrl == null) {
            return type.getDefaultUrl();
        }

        try {
            URL url = new URL(dirtyUrl);
            return url.toExternalForm();
        } catch (MalformedURLException e) {
            return type.getDefaultUrl();
        }
    }

    /**
     * Sets and overrides the Issue Collector URL for a specific project type in the Application Properties.
     *
     * NOTE: This is only provided for testing purposes and should not be used in production.
     *
     * @param dirtyUrl {String} URL to an Issue Collector
     * @throws MalformedURLException if dirtyURL does not adhere to the format of a URL.
     */
    @VisibleForTesting
    public void setUrl(JiraType type, String dirtyUrl) throws MalformedURLException {
        final URL url = new URL(dirtyUrl);
        jiraApplicationProperties.setString(type.getKey(), url.toExternalForm());
    }

    /**
     * Represents the different JIRA project types and their respective Issue Collectors.
     *
     * The key is used to retrieve the URL from the data provider.
     */
    public enum JiraType {
        DEFAULT ("jira.feedback.plugin.issue.collector.default", "https://jira.atlassian.com/s/576e9ab86257d4f65f6ea5b6dd50de44-T/en_UK3ljiw5/71006/b6b48b2829824b869586ac216d119363/2.0.11/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector-embededjs/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector-embededjs.js?locale=en-UK&collectorId=abbf546d"),
        SERVICE_DESK ("jira.feedback.plugin.issue.collector.service.desk", "https://jira.atlassian.com/s/576e9ab86257d4f65f6ea5b6dd50de44-T/en_UK3ljiw5/71006/b6b48b2829824b869586ac216d119363/2.0.11/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector.js?locale=en-UK&collectorId=a698db21"),
        SOFTWARE ("jira.feedback.plugin.issue.collector.software", DEFAULT.getDefaultUrl()),
        CORE ("jira.feedback.plugin.issue.collector.core", DEFAULT.getDefaultUrl());

        private final String key;
        private final String url;

        JiraType(final String key, final String url) {
            this.key = key;
            this.url = url;
        }

        public String getKey() {
            return key;
        }

        protected String getDefaultUrl() {
            return url;
        }
    }
}
