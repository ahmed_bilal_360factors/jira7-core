package com.atlassian.feedback;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.plugin.webfragment.conditions.AbstractWebCondition;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

public class ShowFeedbackButtonCondition extends AbstractWebCondition {
    private final FeatureManager featureManager;

    public ShowFeedbackButtonCondition(@ComponentImport final FeatureManager featureManager) {
        this.featureManager = featureManager;
    }

    @Override
    public boolean shouldDisplay(ApplicationUser applicationUser, JiraHelper jiraHelper) {
        return featureManager.isEnabled(FeedbackPluginDarkFeatures.ENABLE_HEADER_FEEDBACK_KEY) &&
                !featureManager.isEnabled(FeedbackPluginDarkFeatures.DISABLE_HEADER_FEEDBACK_KEY);
    }
}
