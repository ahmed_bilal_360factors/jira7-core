package com.atlassian.feedback;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.webresource.api.data.WebResourceDataProvider;
import com.google.common.collect.Maps;

import java.util.Map;
import java.util.stream.Stream;

public class FeedbackButtonWebResourceDataProvider implements WebResourceDataProvider {
    private final FeatureManager featureManager;
    private final DefaultIssueCollectorUrlManager defaultIssueCollectorUrlManager;

    public FeedbackButtonWebResourceDataProvider(@ComponentImport final FeatureManager featureManager,
                                                 final DefaultIssueCollectorUrlManager defaultIssueCollectorUrlManager) {
        this.featureManager = featureManager;
        this.defaultIssueCollectorUrlManager = defaultIssueCollectorUrlManager;
    }

    @Override
    public Jsonable get() {
        JSONObject jsonData = getJsonData();
        return writer -> {
            try {
                jsonData.write(writer);
            } catch (JSONException e) {
                throw new Jsonable.JsonMappingException(e);
            }
        };
    }

    private JSONObject getJsonData() {
        boolean isHeaderFeedbackButtonEnabled = featureManager.isEnabled(FeedbackPluginDarkFeatures.ENABLE_HEADER_FEEDBACK_KEY)
                && !featureManager.isEnabled(FeedbackPluginDarkFeatures.DISABLE_HEADER_FEEDBACK_KEY);

        Map<String, Object> values = Maps.newHashMap();
        values.put("isHeaderFeedbackButtonEnabled", isHeaderFeedbackButtonEnabled);

        Stream.of(DefaultIssueCollectorUrlManager.JiraType.values()).forEach(t -> values.put(t.getKey(), defaultIssueCollectorUrlManager.getUrl(t)));

        return new JSONObject(values);
    }
}
