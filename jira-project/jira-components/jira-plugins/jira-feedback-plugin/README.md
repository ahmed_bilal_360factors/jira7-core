# JIRA Feedback Plugin

## About

The feedback button is located in the top header.nav right context. It opens dialog with an iframe that links to a issue collector.  
This was originally located in JIRA Projects Plugin but moved into a separate plug-in for maintainability.

## How to disable the feature
The feedback button plugin is hidden behind a dark feature by default. 

## How to enable the feature
* go to <jira_instance>/secure/SiteDarkFeatures!default.jspa
* add: `"com.atlassian.feedback.feedback-button-move-to-header-enable`

Other dark feature flags can be found at `com.atlassian.feedback.feedback-button-move-to-header-enable`


## WebDriver Tests
You can find the tests here:
/jira-webdriver-tests/src/main/java/com/atlassian/jira/webtest/webdriver/tests/plugin/feedback/TestFeedbackPlugin.java