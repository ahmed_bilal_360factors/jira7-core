package com.atlassian.sal.jira.license;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.sal.api.license.ProductLicense;
import com.atlassian.sal.api.license.SingleProductLicenseDetailsView;

import javax.annotation.Nonnull;

import static com.atlassian.application.api.ApplicationKey.isValid;
import static com.google.common.base.Preconditions.checkArgument;

/**
 * @since v7.0
 */
class SingleProductLicenseDetailsViewImpl extends BaseLicenseDetailsImpl implements SingleProductLicenseDetailsView {
    final private ProductLicense productLicense;

    SingleProductLicenseDetailsViewImpl(@Nonnull LicenseDetails licenseDetails, @Nonnull final String productKey) {
        super(licenseDetails);

        checkArgument(isValid(productKey), String.format("%s is not a valid key.", productKey));

        final ApplicationKey wanted = ApplicationKey.valueOf(productKey);
        if (licenseDetails.hasApplication(wanted)) {
            productLicense = new ApplicationProductLicense(
                    wanted, licenseDetails.getLicensedApplications().getUserLimit(wanted), licenseDetails.getJiraLicense());
        } else {
            throw new IllegalArgumentException("No product license with key '" + wanted + "'.");
        }
    }

    @Nonnull
    @Override
    public String getProductKey() {
        return productLicense.getProductKey();
    }

    @Override
    public boolean isUnlimitedNumberOfUsers() {
        return productLicense.isUnlimitedNumberOfUsers();
    }

    @Override
    public int getNumberOfUsers() {
        return productLicense.getNumberOfUsers();
    }

    @Nonnull
    @Override
    public String getProductDisplayName() {
        return productLicense.getProductDisplayName();
    }
}
