package com.atlassian.sal.jira.timezone;

import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.timezone.TimeZoneService;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.sal.api.user.UserKey;

import javax.annotation.Nonnull;
import java.util.TimeZone;

/**
 * @since v4.4
 */
public class JiraTimeZoneManager implements com.atlassian.sal.api.timezone.TimeZoneManager {
    private final TimeZoneService timeZoneService;
    private final TimeZoneManager timeZoneManager;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final UserManager userManager;

    public JiraTimeZoneManager(TimeZoneService timeZoneService, TimeZoneManager timeZoneManager,
                               JiraAuthenticationContext jiraAuthenticationContext, UserManager userManager) {
        this.timeZoneService = timeZoneService;
        this.timeZoneManager = timeZoneManager;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.userManager = userManager;
    }

    @Nonnull
    @Override
    public TimeZone getUserTimeZone() {
        return timeZoneManager.getLoggedInUserTimeZone();
    }

    @Nonnull
    @Override
    public TimeZone getUserTimeZone(@Nonnull final UserKey user) {
        final ApplicationUser applicationUser = userManager.getUserByKey(user.getStringValue());
        if (applicationUser == null) {
            return getDefaultTimeZone();
        } else {
            return timeZoneManager.getTimeZoneforUser(applicationUser);
        }
    }

    @Nonnull
    @Override
    public TimeZone getDefaultTimeZone() {
        return timeZoneService.getDefaultTimeZoneInfo(
                new JiraServiceContextImpl(jiraAuthenticationContext.getLoggedInUser())).toTimeZone();
    }
}
