package com.atlassian.sal.jira.license;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationRoleDefinitions;
import com.atlassian.jira.application.ApplicationRoleDefinitions.ApplicationRoleDefinition;
import com.atlassian.jira.application.UndefinedApplicationRoleName;
import com.atlassian.sal.api.license.ProductLicense;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.atlassian.extras.common.LicensePropertiesConstants.DESCRIPTION;
import static com.atlassian.extras.common.LicensePropertiesConstants.NAMESPACE_SEPARATOR;
import static com.atlassian.jira.application.ApplicationKeys.SERVICE_DESK;
import static com.atlassian.jira.component.ComponentAccessor.getComponent;
import static com.atlassian.jira.license.DefaultLicensedApplications.getApplicationLicensePropertyName;
import static com.atlassian.jira.license.ServiceDeskLicenseConstants.SD_LEGACY_NAMESPACE;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * Implements a license view of a single Application (role-based) {@link ProductLicense}. In JIRA 7.0, this includes
 * JIRA Software, JIRA Core, and JIRA ServiceDesk.
 * <p>
 * Products in licenses that are not Applications (eg: add-ons, other atlassian products, jira6 and earlier) should be
 * represented as {@link com.atlassian.sal.jira.license.DefaultProductLicense}s.
 * </p>
 *
 * @see com.atlassian.sal.api.license.MultiProductLicenseDetails
 * @see com.atlassian.sal.jira.license.DefaultProductLicense
 * @since 7.0
 */
class ApplicationProductLicense implements ProductLicense {
    private final ApplicationKey key;
    private final int userLimit;
    private final com.atlassian.extras.api.ProductLicense license;

    ApplicationProductLicense(@Nonnull ApplicationKey key, int userLimit,
                              @Nonnull com.atlassian.extras.api.ProductLicense license) {
        checkNotNull(key, "key");
        if (userLimit != UNLIMITED_USER_COUNT && userLimit <= 0) {
            throw new IllegalArgumentException("Invalid userLimit of " + userLimit + " provided.");
        }

        this.key = key;
        this.userLimit = userLimit;
        this.license = license;
    }

    @Nonnull
    @Override
    public String getProductKey() {
        return key.value();
    }

    @Override
    public boolean isUnlimitedNumberOfUsers() {
        return userLimit == UNLIMITED_USER_COUNT;
    }

    @Override
    public int getNumberOfUsers() {
        return userLimit;
    }

    @Nonnull
    @Override
    public String getProductDisplayName() {
        // try role name as defined by installed plugin
        ApplicationRoleDefinitions roleDefinitions = getComponent(ApplicationRoleDefinitions.class);
        Option<ApplicationRoleDefinition> role = roleDefinitions.getDefined(key);
        if (role.isDefined()) {
            return role.get().name();
        }

        // else try to get from license property
        String name = getProperty(DESCRIPTION);
        if (!isBlank(name)) {
            return name;
        }

        // else derive heuristically
        return UndefinedApplicationRoleName.of(key).getName();
    }

    @Nullable
    @Override
    public String getProperty(@Nonnull String property) {
        String value = license.getProperty(getApplicationLicensePropertyName(key, property));
        if (value != null) {
            return value;
        }

        // service desk application is special-cased to also look in the service-desk plugin namespace
        // if property doesn't exist in the application namespace.
        if (key.equals(SERVICE_DESK)) {
            return license.getProperty(SD_LEGACY_NAMESPACE + NAMESPACE_SEPARATOR + property);
        }

        // property not defined
        return null;
    }

    @Override
    public final boolean equals(final Object objectComparingTo) {
        return objectComparingTo instanceof ProductLicense
                && getProductKey().equals(((ProductLicense) objectComparingTo).getProductKey());
    }

    @Override
    public final int hashCode() {
        return getProductKey().hashCode();
    }

    @Override
    public final String toString() {
        return format("%s(%s)", getClass().getSimpleName(), getProductKey());
    }
}
