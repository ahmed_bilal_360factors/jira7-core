package com.atlassian.sal.jira.license;

import com.atlassian.extras.api.jira.JiraLicense;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.sal.api.license.BaseLicenseDetails;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Date;

/**
 * @since v7.0
 */
class BaseLicenseDetailsImpl implements BaseLicenseDetails {
    private final LicenseDetails licenseDetails;

    BaseLicenseDetailsImpl(@Nonnull LicenseDetails licenseDetails) {
        this.licenseDetails = licenseDetails;
    }

    protected LicenseDetails getLicenseDetails() {
        return licenseDetails;
    }

    @Override
    public boolean isEvaluationLicense() {
        return getJiraLicense().isEvaluation();
    }

    @Nonnull
    @Override
    public String getLicenseTypeName() {
        return licenseDetails.getLicenseType().name();
    }

    @Override
    public String getOrganisationName() {
        return getJiraLicense().getOrganisation().getName();
    }

    @Nullable
    @Override
    public String getSupportEntitlementNumber() {
        return getJiraLicense().getSupportEntitlementNumber();
    }

    @Override
    public String getDescription() {
        return getJiraLicense().getDescription();
    }

    @Override
    public String getServerId() {
        return getJiraLicense().getServerId();
    }

    @Override
    public boolean isPerpetualLicense() {
        return getLicenseExpiryDate() == null;
    }

    @Nullable
    @Override
    public Date getLicenseExpiryDate() {
        return getJiraLicense().getExpiryDate();
    }

    @Nullable
    @Override
    public Date getMaintenanceExpiryDate() {
        return getJiraLicense().getMaintenanceExpiryDate();
    }

    @Override
    public boolean isDataCenter() {
        return licenseDetails.isDataCenter();
    }

    @Override
    public boolean isEnterpriseLicensingAgreement() {
        return licenseDetails.isEnterpriseLicenseAgreement();
    }

    @Nullable
    @Override
    public String getProperty(@Nonnull final String key) {
        return getJiraLicense().getProperty(key);
    }

    JiraLicense getJiraLicense() {
        return licenseDetails.getJiraLicense();
    }
}
