package com.atlassian.sal.jira.pluginsettings;

import com.atlassian.sal.api.pluginsettings.PluginSettings;

/**
 * A component that keeps critical plugin settings consistent in cluster.
 *
 * <p>
 *     Plugin settings are stored via property sets that are <strong>eventually consistent</strong> which is fine
 *     in general but some of them need to be <strong>consistent</strong>.
 * </p>
 * <p>
 *     A prominent example of such settings are AO build numbers used in critical path of upgrade tasks.
 * </p>
 * <p>
 *     For settings that need to be <strong>consistent</strong> this component delegates the calls to
 *     {@link #get(String)} to {@link CacheInvalidatingPluginSettings}.
 * </p>
 */
class ClusterSafePluginSettings implements PluginSettings {

    private final CacheInvalidatingPluginSettings cacheInvalidatingPluginSettings;
    private final PluginSettings defaultPluginSettings;

    private static final String AO_BUILD_NUMBER_PREFIX = "AO_";
    private static final String AO_BUILD_NUMBER_SUFFIX = "_#";

    ClusterSafePluginSettings(
            final CacheInvalidatingPluginSettings cacheInvalidatingPluginSettings,
            final PluginSettings defaultPluginSettings
    ) {
        this.cacheInvalidatingPluginSettings = cacheInvalidatingPluginSettings;
        this.defaultPluginSettings = defaultPluginSettings;
    }

    @Override
    public Object get(final String key) {
        if (isAoBuildNumberProperty(key)) {
            return cacheInvalidatingPluginSettings.get(key);
        }
        return defaultPluginSettings.get(key);
    }

    private boolean isAoBuildNumberProperty(final String key) {
        return key!= null && key.startsWith(AO_BUILD_NUMBER_PREFIX) && key.endsWith(AO_BUILD_NUMBER_SUFFIX);
    }

    @Override
    public Object put(final String key, final Object value) {
        return defaultPluginSettings.put(key, value);
    }

    @Override
    public Object remove(final String key) {
        return defaultPluginSettings.remove(key);
    }
}
