package com.atlassian.sal.jira.rdbms;

import com.atlassian.core.ofbiz.util.CoreTransactionUtil;
import com.atlassian.fugue.Option;
import com.atlassian.jira.database.DatabaseAccessor;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.sal.api.rdbms.ConnectionCallback;
import com.atlassian.sal.spi.HostConnectionAccessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Provide a {@link Connection} and optional schema name for use by {@link com.atlassian.sal.api.rdbms.TransactionalExecutor}
 *
 * @since v7.0
 */
public class JiraHostConnectionAccessor implements HostConnectionAccessor {
    private static final Logger log = LoggerFactory.getLogger(JiraHostConnectionAccessor.class);
    private final DatabaseAccessor databaseAccessor;
    private final Option<String> schemaName;

    public JiraHostConnectionAccessor(final DatabaseAccessor databaseAccessor) {
        this.databaseAccessor = databaseAccessor;
        this.schemaName = Option.option(databaseAccessor.getSchemaName().orElse(null));
    }

    @Override
    public <A> A execute(final boolean readOnly, final boolean newTransaction, @Nonnull final ConnectionCallback<A> callback) {
        if (newTransaction) {
            // Need to borrow a connection from the pool and start a new transaction
            return borrowConnectionAndExecute(readOnly, callback);
        } else {
            final Connection existingConnection = CoreTransactionUtil.getConnection();
            if (existingConnection == null) {
                // no current transaction in OfBiz
                return borrowConnectionAndExecute(readOnly, callback);
            } else {
                // Developer has requested to participate in existing transaction, and OfBiz actually has one running ...
                // I don't think it is a good idea to set read-only on an existing transaction ... ignoring
                return executeInExistingTransaction(callback, existingConnection);
            }
        }
    }

    private <A> A borrowConnectionAndExecute(final boolean readOnly, final ConnectionCallback<A> callback) {
        return databaseAccessor.executeQuery(connection -> {
                    // By contract must run in a Transaction
                    connection.setAutoCommit(false);
                    try {
                        connection.getJdbcConnection().setReadOnly(readOnly);
                    } catch (SQLException e) {
                        throw new DataAccessException(e);
                    }
                    // If this returns successfully then SAL's DefaultTransactionalExecutor will commit the transaction
                    // If it throws RuntimeException then DatabaseAcccessor and DefaultTransactionalExecutor will both
                    // cause a rollback() ... seems harmless.
                    try {
                        return callback.execute(connection.getJdbcConnection());
                    } finally {
                        if (readOnly) {
                            // Need to return the connection to the default state of NOT read-only.
                            try {
                                connection.getJdbcConnection().setReadOnly(false);
                            } catch (SQLException ex) {
                                log.error("Unable to unset the read-only flag on a borrowed DB Connection ... chaos will ensue.", ex);
                            }
                        }
                        // Connection Pool will set default auto-commit mode
                    }
                }
        );
    }

    private <A> A executeInExistingTransaction(final ConnectionCallback<A> callback, final Connection existingConnection) {
        // SAL will run the developer's callback and then call commit() or rollback()
        // Because we have an existing transaction, we need to ignore the commit() call and appropriate the rollback()
        // call to be "set rollback required"
        final Connection nestedConection = new ManagedOfBizConnection(existingConnection);
        return callback.execute(nestedConection);
    }

    @Nonnull
    @Override
    public Option<String> getSchemaName() {
        return schemaName;
    }
}
