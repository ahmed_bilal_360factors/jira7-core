package com.atlassian.sal.jira.license;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.extras.api.Product;
import com.atlassian.extras.api.jira.JiraLicense;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.license.LicensedApplications;
import com.atlassian.sal.api.license.MultiProductLicenseDetails;
import com.atlassian.sal.api.license.ProductLicense;
import com.google.common.collect.Iterables;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.HashSet;
import java.util.Set;

import static com.atlassian.jira.license.DefaultLicensedApplications.JIRA_PRODUCT_NAMESPACE;
import static com.google.common.collect.Sets.newHashSetWithExpectedSize;

/**
 * @since v7.0
 */
class MultiProductLicenseDetailsImpl extends BaseLicenseDetailsImpl implements MultiProductLicenseDetails {
    MultiProductLicenseDetailsImpl(@Nonnull LicenseDetails licenseDetails) {
        super(licenseDetails);
    }

    @Nonnull
    @Override
    public Set<ProductLicense> getProductLicenses() {
        LicenseDetails licenseDetails = getLicenseDetails();
        LicensedApplications licensedApps = licenseDetails.getLicensedApplications();

        Set<ApplicationKey> applicationKeys = licensedApps.getKeys();
        Set<ProductLicense> productLicenses = newHashSetWithExpectedSize(applicationKeys.size());

        for (ApplicationKey application : applicationKeys) {
            productLicenses.add(new ApplicationProductLicense(
                    application, licensedApps.getUserLimit(application), licenseDetails.getJiraLicense()));
        }

        // if no applications and the license contains a pre-7.0 JIRA product (ie: "jira.active"), then return
        // a product license for pre-7.0 jira.
        if (productLicenses.isEmpty() && Iterables.contains(licenseDetails.getJiraLicense().getProducts(), Product.JIRA)) {
            productLicenses.add(new DefaultProductLicense(Product.JIRA.getNamespace(), licenseDetails.getJiraLicense()));
        }

        return productLicenses;
    }

    @Nonnull
    @Override
    public Set<ProductLicense> getEmbeddedLicenses() {
        Set<ProductLicense> otherEmbeddedLicenses = new HashSet<>();
        JiraLicense license = getJiraLicense();

        for (Product p : license.getProducts()) {
            if (p.getNamespace().startsWith(JIRA_PRODUCT_NAMESPACE)) {
                // then it's an Application, will be returned by #getProductLicenses()
                continue;
            } else if (p.equals(Product.JIRA)) {
                // then it's the pre-7.0 JIRA product, will be returned by #getProductLicenses()
                continue;
            }

            otherEmbeddedLicenses.add(new DefaultProductLicense(p.getNamespace(), license));
        }

        return otherEmbeddedLicenses;
    }

    @Nullable
    @Override
    public ProductLicense getProductLicense(@Nonnull final String productKey) {
        for (ProductLicense productLicense : getProductLicenses()) {
            if (productLicense.getProductKey().equals(productKey)) {
                return productLicense;
            }
        }

        for (ProductLicense productLicense : getEmbeddedLicenses()) {
            if (productLicense.getProductKey().equals(productKey)) {
                return productLicense;
            }
        }

        return null;
    }
}
