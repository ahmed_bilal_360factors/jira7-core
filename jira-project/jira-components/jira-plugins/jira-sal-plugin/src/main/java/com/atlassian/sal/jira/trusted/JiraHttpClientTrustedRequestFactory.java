package com.atlassian.sal.jira.trusted;

import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.TrustedRequest;
import com.atlassian.sal.api.net.TrustedRequestFactory;
import com.atlassian.sal.core.net.HttpClientTrustedRequestFactory;

/**
 * @since 7.0
 */
public class JiraHttpClientTrustedRequestFactory implements TrustedRequestFactory {

    private final HttpClientTrustedRequestFactory httpClientTrustedRequestFactory;

    public JiraHttpClientTrustedRequestFactory(HttpClientTrustedRequestFactory httpClientTrustedRequestFactory) {
        this.httpClientTrustedRequestFactory = httpClientTrustedRequestFactory;
    }

    @Override
    public TrustedRequest createTrustedRequest(final Request.MethodType methodType, final String url) {
        return (TrustedRequest) httpClientTrustedRequestFactory.createTrustedRequest(methodType, url);
    }

    @Override
    public Request<?, ?> createRequest(final Request.MethodType methodType, final String url) {
        return (TrustedRequest) httpClientTrustedRequestFactory.createTrustedRequest(methodType, url);
    }

    @Override
    public boolean supportsHeader() {
        return true;
    }
}
