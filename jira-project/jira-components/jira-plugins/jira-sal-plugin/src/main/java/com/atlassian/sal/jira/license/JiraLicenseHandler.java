package com.atlassian.sal.jira.license;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationConfigurationHelper;
import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.application.ApplicationRoleDefinitions;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.application.ApplicationRoleStore;
import com.atlassian.jira.bc.license.JiraLicenseService;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.license.LicenseChangedEvent;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.license.LicensedApplications;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.sal.api.i18n.InvalidOperationException;
import com.atlassian.sal.api.license.BaseLicenseDetails;
import com.atlassian.sal.api.license.LicenseHandler;
import com.atlassian.sal.api.license.MultiProductLicenseDetails;
import com.atlassian.sal.api.license.SingleProductLicenseDetailsView;
import com.atlassian.sal.api.validate.ValidationResult;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.SortedSet;
import java.util.stream.Collectors;

import static com.atlassian.jira.component.ComponentAccessor.getComponent;
import static com.atlassian.sal.api.license.ProductLicense.UNLIMITED_USER_COUNT;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Collections.unmodifiableList;

/**
 * Jira implementation of license handler
 */
public class JiraLicenseHandler implements LicenseHandler, InitializingBean, DisposableBean {
    private final JiraLicenseService jiraLicenseService;
    private final JiraLicenseManager jiraLicenseManager;
    private final I18nHelper.BeanFactory i18nBeanFactory;
    private final ApplicationProperties applicationProperties;
    private final EventPublisher eventPublisher;
    private final ApplicationRoleManager applicationRoleManager;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final GlobalPermissionManager permissionManager;
    private final GroupManager groupManager;
    private final ApplicationConfigurationHelper appConfigHelper;

    public JiraLicenseHandler(@Nonnull JiraLicenseService jiraLicenseService, @Nonnull JiraLicenseManager jiraLicenseManager,
                              @Nonnull I18nHelper.BeanFactory i18nBeanFactory, @Nonnull ApplicationProperties applicationProperties,
                              @Nonnull EventPublisher eventPublisher, @Nonnull JiraAuthenticationContext jiraAuthenticationContext,
                              @Nonnull GlobalPermissionManager permissionManager, @Nonnull GroupManager groupManager) {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.jiraLicenseService = checkNotNull(jiraLicenseService, "jiraLicenseService");
        this.jiraLicenseManager = checkNotNull(jiraLicenseManager, "jiraLicenseManager");
        this.i18nBeanFactory = checkNotNull(i18nBeanFactory, "i18nBeanFactory");
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
        this.eventPublisher = checkNotNull(eventPublisher, "eventPublisher");
        this.applicationRoleManager = checkNotNull(getComponent(ApplicationRoleManager.class), "applicationRoleManager");
        this.permissionManager = permissionManager;
        this.groupManager = groupManager;
        this.appConfigHelper = checkNotNull(getComponent(ApplicationConfigurationHelper.class), "appConfigHelper");
    }

    /**
     * Sets the license, going through the regular validation steps as if you used the web UI
     *
     * @param license The license string
     */
    @Override
    public void setLicense(String license) {
        JiraLicenseService.ValidationResult validationResult = jiraLicenseService.validate(i18nBeanFactory.getInstance(Locale.getDefault()), license);
        if (validationResult.getErrorCollection().hasAnyErrors()) {
            throw new IllegalArgumentException("Specified license was invalid.");
        }
        jiraLicenseManager.clearAndSetLicense(license);
    }

    @Override
    public boolean hostAllowsMultipleLicenses() {
        return true;
    }

    @Override
    public boolean hostAllowsCustomProducts() {
        return true;
    }

    @Override
    public Set<String> getProductKeys() {
        final Set<String> productKeys = Sets.newHashSet();
        for (LicenseDetails licenseDetails : jiraLicenseManager.getLicenses()) {
            for (final ApplicationKey key : licenseDetails.getLicensedApplications().getKeys()) {
                productKeys.add(key.value());
            }
        }
        return productKeys;
    }

    @Override
    public void addProductLicense(@Nonnull final String productKey, @Nonnull final String license)
            throws InvalidOperationException {
        checkNotNull(productKey, "Product Key required");
        checkNotNull(license, "License required");
        ValidationResult validationResult = validateProductLicense(productKey, license, Locale.ENGLISH);
        if (validationResult.hasErrors()) {
            String join = StringUtils.join(validationResult.getErrorMessages().iterator(), ",\n");
            throw new InvalidOperationException(join, join);
        }
        jiraLicenseManager.setLicense(license);
    }

    @Override
    public void removeProductLicense(@Nonnull final String productKey)
            throws InvalidOperationException {
        checkArgument(ApplicationKey.isValid(productKey), "productKey is invalid.");
        try {
            jiraLicenseManager.removeLicense(ApplicationKey.valueOf(productKey));
        } catch (IllegalStateException e) {
            //IllegalStateException("Unable to remove license, JIRA needs to contain at least one license to be functional")
            throw new InvalidOperationException("Unable to remove license, JIRA needs to contain at least one license to be functional",
                    i18nBeanFactory.getInstance(jiraAuthenticationContext.getLoggedInUser()).getText("admin.errors.cannot.remove.license.required"));
        }
    }

    @Nonnull
    @Override
    public ValidationResult validateProductLicense(@Nonnull String productKey, @Nonnull String license,
                                                   @Nullable Locale locale) {
        final Locale localeForMessages = (locale == null ? applicationProperties.getDefaultLocale() : locale);
        final I18nHelper i18nHelper = i18nBeanFactory.getInstance(localeForMessages);

        if (StringUtils.isBlank(productKey) || !ApplicationKey.isValid(productKey)) {
            return ValidationResult.withErrorMessages(Sets.newHashSet(i18nHelper.getText("jira.license.validation.invalid.product.key")));
        }

        final ApplicationKey applicationKeyForLicense = ApplicationKey.valueOf(productKey);
        final JiraLicenseService.ValidationResult validationResult = jiraLicenseService.validate(
                applicationKeyForLicense, license, i18nHelper);
        if (validationResult.getErrorCollection().hasAnyErrors()) {
            return ValidationResult.withErrorMessages(validationResult.getErrorCollection().getErrors().values());
        }

        final LicensedApplications applicationsToLicense = jiraLicenseManager.getLicense(license).getLicensedApplications();
        final ValidationResult productLicenseErrors = validateProductLicenseErrors(i18nHelper, applicationsToLicense,
                applicationKeyForLicense);
        if (productLicenseErrors.hasErrors()) {
            return productLicenseErrors;
        }

        final ValidationResult licenseValidationResult = validateProductLicenseWarnings(i18nHelper,
                applicationsToLicense);
        if (licenseValidationResult.isValid()) {
            final Optional<String> appConfigMessage =
                    appConfigHelper.validateApplicationForConfiguration(applicationKeyForLicense,
                            jiraAuthenticationContext.getLoggedInUser());
            if (appConfigMessage.isPresent()) {
                return ValidationResult.withWarningMessages(Sets.newHashSet(appConfigMessage.get()));
            }
        }
        return licenseValidationResult;
    }

    /**
     * @param appsToLicense LicensedApplications included in the license that is being validated
     * @param appKey        ApplicationKey that the license is being checked for
     */
    private ValidationResult validateProductLicenseErrors(final I18nHelper i18nHelper,
                                                          final LicensedApplications appsToLicense, final ApplicationKey appKey) {
        if (!appsToLicense.getKeys().contains(appKey)) {
            final String notThisProductErrorMessage = i18nHelper.getText("jira.license.validation.product.key.not.for.license");
            return ValidationResult.withErrorMessages(Sets.newHashSet(notThisProductErrorMessage));
        }
        return ValidationResult.valid();
    }

    private ValidationResult validateProductLicenseWarnings(final I18nHelper i18nHelper,
                                                            final LicensedApplications licensedApplications) {
        ValidationResult userLimit = validateProductLicenseUserLimit(i18nHelper, licensedApplications);
        if (userLimit.hasWarnings()) {
            return userLimit;
        }
        ValidationResult activableUsers = validateActivateExistingUsers(i18nHelper, licensedApplications);
        if (activableUsers.hasWarnings()) {
            return activableUsers;
        }
        return ValidationResult.valid();
    }

    private ValidationResult validateProductLicenseUserLimit(final I18nHelper i18nHelper,
                                                             final LicensedApplications licensedApplications) {
        final Set<String> warningMessages = Sets.newHashSet();
        for (ApplicationRole licensedApplicationRole : applicationRoleManager.getRoles()) {
            final ApplicationKey licensedApplicationKey = licensedApplicationRole.getKey();
            if (licensedApplications.getKeys().contains(licensedApplicationKey)) {
                final String productName = licensedApplicationRole.getName();
                final int licRoleUserLimit = licensedApplications.getUserLimit(licensedApplicationKey);
                final int activeUserCount = applicationRoleManager.getUserCount(licensedApplicationKey);
                if (licRoleUserLimit != UNLIMITED_USER_COUNT && activeUserCount > licRoleUserLimit) {
                    warningMessages.add(i18nHelper.getText("jira.license.validation.product.active.user.count.exceeds.license.limit", productName));
                }
            }
        }

        if (!warningMessages.isEmpty()) {
            return ValidationResult.withWarningMessages(warningMessages);
        }

        return ValidationResult.valid();
    }

    private static Set<String> getConfiguredGroupsForRole(ApplicationKey applicationToLicense) {
        // looking up direct in store as we know role is not licensed & therefore not available through manager
        ApplicationRoleStore applicationRoleStore = getComponent(ApplicationRoleStore.class);
        final ApplicationRoleStore.ApplicationRoleData applicationRoleData = applicationRoleStore.get(applicationToLicense);
        return applicationRoleData.getGroups();
    }

    /**
     * Checks if licensing given applications will reactivate users. This can happen if there is a previous (latent)
     * configuration in the database.
     */
    private ValidationResult validateActivateExistingUsers(final I18nHelper i18nHelper, LicensedApplications licensedApplications) {
        Predicate<ApplicationUser> willUserBeActivated = user -> !permissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user)
                && applicationRoleManager.getRolesForUser(user).isEmpty();

        Predicate<Option<ApplicationRoleDefinitions.ApplicationRoleDefinition>> isLatentConfigurationPresent = roleDefinition ->
                roleDefinition.isDefined() && applicationRoleManager.getRole(roleDefinition.get().key()).isEmpty();

        final ApplicationRoleDefinitions roleDefinitions = getComponent(ApplicationRoleDefinitions.class);
        final Set<ApplicationKey> applicationsToLicense = licensedApplications.getKeys();

        List<String> warnings = Lists.newArrayListWithCapacity(applicationsToLicense.size());
        for (ApplicationKey applicationToLicense : applicationsToLicense) {
            Option<ApplicationRoleDefinitions.ApplicationRoleDefinition> roleDefinition = roleDefinitions.getDefined(applicationToLicense);
            if (isLatentConfigurationPresent.apply(roleDefinition)) {
                final Set<String> groups = getConfiguredGroupsForRole(applicationToLicense);
                Set<ApplicationUser> activableUsers = groups.stream()      // Groups associated with the role
                        .map(group -> groupManager.getUsersInGroup(group)) // All users in all those groups
                        .flatMap(users -> users.stream())
                        .filter(user -> willUserBeActivated.apply(user))   // That do not have any other roles and is not admin
                        .collect(Collectors.toSet());

                if (!activableUsers.isEmpty()) {
                    warnings.add(i18nHelper.getText("jira.license.validation.activate.user",
                            Lists.newArrayList(new Integer(activableUsers.size()), roleDefinition.get().name())));
                }
            }
        }

        if (!warnings.isEmpty()) {
            return ValidationResult.withWarningMessages(warnings);
        }

        return ValidationResult.valid();
    }

    /**
     * Gets the server ID of the currently running application.  The server ID format is four quadruples of alphanumeric
     * characters, each separated by a dash (<tt>-</tt>).
     *
     * @return the server ID
     * @since 2.7
     */
    @Override
    public String getServerId() {
        return jiraLicenseService.getServerId();
    }

    /**
     * Gets the Support Entitlement Number (SEN) for the currently running application.
     *
     * @return the Support Entitlement Number, or {@code null} if there is no current support entitlement.
     * @deprecated use {@link BaseLicenseDetails#getSupportEntitlementNumber()} instead
     */
    @Override
    public String getSupportEntitlementNumber() {
        throw new UnsupportedOperationException("getSupportEntitlementNumber has been deprecated in favor of "
                + "BaseLicenseDetails#getSupportEntitlementNumber() since 7.0 with the introduction of "
                + "JIRA Applications Roles and multiple JIRA application licenses.");
    }

    /**
     * Gets the Support Entitlement Numbers (SENs) for all licenses in the currently running application. The SENs are
     * in an ordered set, guaranteeing to return the SEN in the same order until the installed license state changes.
     * <p>
     * Note that licensed plugin SENs are not included in the results, unless they are being treated as application
     * licenses.
     *
     * @return an ordered set of all the SENs. The set does not contain any empty or null SEN strings. If there is no
     * SEN it will return an empty set.
     * @since 7.0
     */
    @Nonnull
    @Override
    public SortedSet<String> getAllSupportEntitlementNumbers() {
        return jiraLicenseManager.getSupportEntitlementNumbers();
    }

    @Nullable
    protected LicenseDetails getLicenseDetailsByApplicationKey(@Nonnull final ApplicationKey application) {
        checkNotNull(application, "application");
        return Iterables.find(jiraLicenseManager.getLicenses(), license -> license.hasApplication(application), null);
    }

    @Nullable
    @Override
    public String getRawProductLicense(final String productKey) {
        if (StringUtils.isBlank(productKey)) {
            return null;
        }
        checkArgument(ApplicationKey.isValid(productKey), "productKey is invalid.");

        final ApplicationKey key = ApplicationKey.valueOf(productKey);
        final LicenseDetails licenseDetails = getLicenseDetailsByApplicationKey(key);
        return licenseDetails != null ? licenseDetails.getLicenseString() : null;
    }

    @Nullable
    @Override
    public SingleProductLicenseDetailsView getProductLicenseDetails(@Nonnull final String productKey) {
        checkArgument(ApplicationKey.isValid(productKey), "productKey is invalid.");

        final ApplicationKey key = ApplicationKey.valueOf(productKey);
        final LicenseDetails licenseDetails = getLicenseDetailsByApplicationKey(key);
        return licenseDetails != null ? new SingleProductLicenseDetailsViewImpl(licenseDetails, productKey) : null;
    }

    @Nonnull
    @Override
    public List<MultiProductLicenseDetails> getAllProductLicenses() {
        List<MultiProductLicenseDetails> productLicenses = new ArrayList<>();
        for (LicenseDetails license : jiraLicenseManager.getLicenses()) {
            productLicenses.add(new MultiProductLicenseDetailsImpl(license));
        }

        return unmodifiableList(productLicenses);
    }

    @Nonnull
    @Override
    public MultiProductLicenseDetails decodeLicenseDetails(@Nonnull final String license) {
        checkNotNull(license, "License required");
        return new MultiProductLicenseDetailsImpl(jiraLicenseManager.getLicense(license));
    }

    @SuppressWarnings({"UnusedDeclaration"})
    @EventListener
    public void onLicenseChanged(LicenseChangedEvent event) {
        eventPublisher.publish(new SalLicenseChangedEvent(event.getPreviousLicenseDetails().getOrNull(), event.getNewLicenseDetails().getOrNull()));
    }

    @Override
    public void afterPropertiesSet() {
        eventPublisher.register(this);
    }

    @Override
    public void destroy() {
        eventPublisher.unregister(this);
    }

    private static class SalLicenseChangedEvent implements com.atlassian.sal.api.license.LicenseChangedEvent {
        private final BaseLicenseDetails previousLicense;
        private final BaseLicenseDetails newLicense;

        private SalLicenseChangedEvent(final LicenseDetails previousLicenseDetails, final LicenseDetails newLicenseDetails) {
            this.previousLicense = previousLicenseDetails == null ? null : new BaseLicenseDetailsImpl(previousLicenseDetails);
            this.newLicense = newLicenseDetails == null ? null : new BaseLicenseDetailsImpl(newLicenseDetails);
        }

        @Nullable
        @Override
        public BaseLicenseDetails getPreviousLicense() {
            return previousLicense;
        }

        @Nullable
        @Override
        public BaseLicenseDetails getNewLicense() {
            return newLicense;
        }
    }
}
