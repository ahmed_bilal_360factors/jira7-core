package com.atlassian.sal.jira.license;

import com.atlassian.sal.api.license.ProductLicense;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.atlassian.extras.common.LicensePropertiesConstants.DESCRIPTION;
import static com.atlassian.extras.common.LicensePropertiesConstants.MAX_NUMBER_OF_USERS;
import static com.atlassian.extras.common.LicensePropertiesConstants.NAMESPACE_SEPARATOR;
import static java.lang.Integer.parseInt;
import static java.lang.String.format;


/**
 * Implements a license view of a single non-Application (non-role-based) {@link ProductLicense}. In JIRA, this
 * includes add-ons, other top-level products like Confluence, Hipchat etc, but also the singular pre-JIRA 7.0 product.
 * <p>
 * Products in licenses that are {@link com.atlassian.application.api.Application}s (ie: they have an
 * {@link com.atlassian.application.api.ApplicationKey}) will be returned as
 * {@link com.atlassian.sal.jira.license.ApplicationProductLicense}s.
 * </p>
 *
 * @see com.atlassian.sal.api.license.MultiProductLicenseDetails
 * @see com.atlassian.sal.jira.license.ApplicationProductLicense
 * @see com.atlassian.application.api.ApplicationKey
 * @see com.atlassian.application.api.Application
 * @since 7.0
 */
class DefaultProductLicense implements ProductLicense {
    private final String productNamespace;
    private final com.atlassian.extras.api.ProductLicense productLicense;

    public DefaultProductLicense(@Nonnull String productNamespace, @Nonnull com.atlassian.extras.api.ProductLicense license) {
        this.productNamespace = productNamespace;
        this.productLicense = license;
    }

    @Nonnull
    @Override
    public String getProductKey() {
        return productNamespace;
    }

    @Override
    public boolean isUnlimitedNumberOfUsers() {
        return getNumberOfUsers() == UNLIMITED_USER_COUNT;
    }

    @Override
    public int getNumberOfUsers() {
        String userLimit = getProperty(MAX_NUMBER_OF_USERS);
        return userLimit != null ? parseInt(userLimit) : UNLIMITED_USER_COUNT;
    }

    @Nonnull
    @Override
    public String getProductDisplayName() {
        String desc = getProperty(DESCRIPTION);
        return desc != null ? desc : getProductKey();
    }

    @Nullable
    @Override
    public String getProperty(@Nonnull String property) {
        return productLicense.getProperty(productNamespace + NAMESPACE_SEPARATOR + property);
    }

    @Override
    public final boolean equals(Object other) {
        return other instanceof ProductLicense
                && getProductKey().equals(((ProductLicense) other).getProductKey());
    }

    @Override
    public final int hashCode() {
        return getProductKey().hashCode();
    }

    @Override
    public String toString() {
        return format("%s(%s)", getClass().getSimpleName(), getProductKey());
    }
}
