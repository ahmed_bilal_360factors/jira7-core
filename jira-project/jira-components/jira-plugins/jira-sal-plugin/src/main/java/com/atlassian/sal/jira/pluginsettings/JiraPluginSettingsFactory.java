package com.atlassian.sal.jira.pluginsettings;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.PropertiesManager;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.propertyset.CachingOfBizPropertyEntryStore;
import com.atlassian.jira.propertyset.JiraPropertySetFactory;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.util.concurrent.Supplier;
import com.atlassian.util.concurrent.Suppliers;
import com.opensymphony.module.propertyset.PropertySet;

public class JiraPluginSettingsFactory implements PluginSettingsFactory {
    private static final String JIRA_PROPERTIES_ENTITY_NAME = "jira.properties";
    private static final Long JIRA_PROPERTIES_ENTITY_ID = 1L;


    private final JiraPropertySetFactory jiraPropertySetFactory;
    private final ProjectManager projectManager;

    public JiraPluginSettingsFactory(
            final JiraPropertySetFactory jiraPropertySetFactory,
            final ProjectManager projectManager
    ) {
        this.jiraPropertySetFactory = jiraPropertySetFactory;
        this.projectManager = projectManager;
    }

    public PluginSettings createSettingsForKey(final String key) {
        if (key == null) {
            return createGlobalSettings();
        }

        final Supplier<? extends PropertySet> propertySet =
                Suppliers.memoize(
                        LazyProjectMigratingPropertySet.create(
                                projectManager,
                                jiraPropertySetFactory,
                                jiraPropertySetFactory.buildCachingDefaultPropertySet(key), key
                        )
                );

        return new JiraPluginSettings(propertySet);
    }

    public PluginSettings createGlobalSettings() {
        final Supplier<? extends PropertySet> propertySet =
                ComponentAccessor.getComponent(PropertiesManager.class).getPropertySetReference();

        final Runnable invalidateCacheOperation = ComponentAccessor
                .getComponentSafely(CachingOfBizPropertyEntryStore.class)
                .map(this::invalidateCacheOperation)
                .orElse(() -> {});

        final JiraPluginSettings defaultPluginSettings = new JiraPluginSettings(propertySet);

        final CacheInvalidatingPluginSettings cacheInvalidatingPluginSettings =
                new CacheInvalidatingPluginSettings(defaultPluginSettings, invalidateCacheOperation);

        return new ClusterSafePluginSettings(cacheInvalidatingPluginSettings, defaultPluginSettings);
    }

    private Runnable invalidateCacheOperation(final CachingOfBizPropertyEntryStore store) {
        return () -> store.invalidateCacheEntry(JIRA_PROPERTIES_ENTITY_NAME, JIRA_PROPERTIES_ENTITY_ID);
    }
}
