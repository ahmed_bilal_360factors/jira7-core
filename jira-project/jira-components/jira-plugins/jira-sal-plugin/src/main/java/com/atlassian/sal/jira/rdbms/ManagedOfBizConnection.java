package com.atlassian.sal.jira.rdbms;

import com.atlassian.core.ofbiz.util.CoreTransactionUtil;
import com.atlassian.jira.database.AbstractDelegatingConnection;
import org.ofbiz.core.entity.GenericTransactionException;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Wraps a JDBC connection that is be part of a broader OfBiz transaction.
 *
 * @since v7.0
 */
class ManagedOfBizConnection extends AbstractDelegatingConnection {
    private static final boolean BEGAN_TRANSACTION_FALSE = false;

    ManagedOfBizConnection(final Connection rawConnection) {
        super(rawConnection);
    }

    @Override
    public void rollback() throws SQLException {
        try {
            // We did not begin this transaction.
            CoreTransactionUtil.setRollbackOnly(BEGAN_TRANSACTION_FALSE);
        } catch (GenericTransactionException ex) {
            // Seriously? AFAICT OfBiz declares this but never actually throws it :(
            throw new SQLException("OfBiz setRollbackOnly failed.", ex);
        }
    }

    @Override
    public void setAutoCommit(final boolean autoCommit) throws SQLException {
        if (autoCommit) {
            throw new UnsupportedOperationException("This connection is already in a transaction");
        }
    }

    @Override
    public void commit() throws SQLException {
        // silently ignore - we will hopefully get committed later when the larger transaction completes
    }

    @Override
    public void close() throws SQLException {
        throw new UnsupportedOperationException("You may not close this connection - it will be returned to the pool when all code is finished with it.");
    }
}
