package com.atlassian.sal.jira.pluginsettings;

import com.atlassian.sal.api.pluginsettings.PluginSettings;

/**
 * A component that wraps another {@link PluginSettings} and makes sure that queried results are always up to date
 * by running {@link #invalidateCacheOperation} before the call to {@link #get(String)} is delegated.
 */
class CacheInvalidatingPluginSettings implements PluginSettings {

    private final PluginSettings delegate;
    private final Runnable invalidateCacheOperation;

    CacheInvalidatingPluginSettings(final PluginSettings delegate, final Runnable invalidateCacheOperation) {
        this.delegate = delegate;
        this.invalidateCacheOperation = invalidateCacheOperation;
    }

    @Override
    public Object get(final String key) {
        invalidateCacheOperation.run();
        return delegate.get(key);
    }

    @Override
    public Object put(final String key, final Object value) {
        return delegate.put(key, value);
    }

    @Override
    public Object remove(final String key) {
        return delegate.remove(key);
    }
}
