package com.atlassian.sal.jira.project;

import com.atlassian.jira.project.Project;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * JIRA implementation of the SAL project manager
 */
public class JiraProjectManager implements com.atlassian.sal.api.project.ProjectManager {
    private com.atlassian.jira.project.ProjectManager projectManager;

    public JiraProjectManager(com.atlassian.jira.project.ProjectManager projectManager) {
        this.projectManager = projectManager;
    }

    /**
     * Get all project keys
     *
     * @return All the project keys
     */
    public Collection<String> getAllProjectKeys() {
        List<Project> projects = projectManager.getProjectObjects();
        Collection<String> results = new ArrayList<String>();
        for (Project project : projects) {
            results.add(project.getKey());
        }
        return results;
    }

}
