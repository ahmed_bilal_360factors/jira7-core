package com.atlassian.sal.jira.project;

import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.google.common.collect.ImmutableList;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Collection;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.mockito.Mockito.when;

public class TestJiraProjectManager {
    @Rule
    public TestRule initMocks = new InitMockitoMocks(this);

    @Mock
    ProjectManager mockProjectManager;

    @InjectMocks
    JiraProjectManager jiraProjectManager;

    @Test
    public void testGetAllProjectKeys() {
        final Project p1 = new MockProject(1L, "p1");
        final Project p2 = new MockProject(2L, "p2");
        when(mockProjectManager.getProjectObjects()).thenReturn(ImmutableList.of(p1, p2));

        final Collection<String> keys = jiraProjectManager.getAllProjectKeys();

        assertThat(keys, containsInAnyOrder("p1", "p2"));
    }

}
