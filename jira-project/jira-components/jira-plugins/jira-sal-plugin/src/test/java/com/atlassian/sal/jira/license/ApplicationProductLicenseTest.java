package com.atlassian.sal.jira.license;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationRoleDefinitions;
import com.atlassian.jira.application.ApplicationRoleDefinitions.ApplicationRoleDefinition;
import com.atlassian.jira.application.MockApplicationRoleDefinition;
import com.atlassian.jira.application.UndefinedApplicationRoleName;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.sal.api.license.ProductLicense;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.extras.common.LicensePropertiesConstants.DESCRIPTION;
import static com.atlassian.extras.common.LicensePropertiesConstants.MAX_NUMBER_OF_USERS;
import static com.atlassian.extras.common.LicensePropertiesConstants.NAMESPACE_SEPARATOR;
import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.option;
import static com.atlassian.jira.application.ApplicationKeys.SERVICE_DESK;
import static com.atlassian.jira.license.DefaultLicensedApplications.JIRA_PRODUCT_NAMESPACE;
import static com.atlassian.jira.license.ServiceDeskLicenseConstants.SD_LEGACY_NAMESPACE;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests {@link com.atlassian.sal.jira.license.ApplicationProductLicense}.
 *
 * @since 7.0
 */
@RunWith(MockitoJUnitRunner.class)
public class ApplicationProductLicenseTest {
    static final String PRODUCT_KEY = "com.company.application";
    static final ApplicationKey APPLICATION_KEY = ApplicationKey.valueOf(PRODUCT_KEY);
    static final String DEFAULT_PRODUCT_NAME = UndefinedApplicationRoleName.of(APPLICATION_KEY).getName();

    @Mock
    com.atlassian.extras.api.ProductLicense productLicense;
    @Mock
    @AvailableInContainer
    ApplicationRoleDefinitions roleDefinitions;
    @Rule
    public MockitoContainer initMyMockage = MockitoMocksInContainer.rule(this);

    ProductLicense license;
    int numUsers = 10;

    @Before
    public void init() {
        assert ApplicationKey.isValid(PRODUCT_KEY);

        license = new ApplicationProductLicense(APPLICATION_KEY, numUsers, productLicense);
    }

    @Test
    public void getProperty() {
        when(productLicense.getProperty(JIRA_PRODUCT_NAMESPACE + PRODUCT_KEY + ".value")).thenReturn("exists");

        assertThat(license.getProperty("value"), equalTo("exists"));
        assertThat(license.getProperty(PRODUCT_KEY + ".value"), nullValue());
    }

    @Test
    public void getNumberOfUsers() {
        assertThat(license.getNumberOfUsers(), equalTo(10));

        when(productLicense.getProperty(numberOfUsersPropertyFor(PRODUCT_KEY))).thenReturn("369");
        assertThat(license.getNumberOfUsers(), equalTo(10));
    }

    @Test
    public void getProductDisplayNameReturnsHeuristicNameWhenNoNameInLicenseNorPluginInstalled() {
        // no products installed, no license installed
        when(roleDefinitions.getDefined(any(ApplicationKey.class))).thenReturn(none());
        when(productLicense.getProperty(descriptionPropertyFor(PRODUCT_KEY))).thenReturn(null);
        assertThat(license.getProductDisplayName(), equalTo(DEFAULT_PRODUCT_NAME));
    }

    @Test
    public void getProductDisplayNameReturnsLicenseNameWhenPluginNotInstalled() {
        // no products installed, but license installed
        when(roleDefinitions.getDefined(any(ApplicationKey.class))).thenReturn(none());
        when(productLicense.getProperty(descriptionPropertyFor(PRODUCT_KEY))).thenReturn("Application Name");
        assertThat(license.getProductDisplayName(), equalTo("Application Name"));
    }

    @Test
    public void getProductDisplayNameReturnsPluginNameWhenInstalled() {
        // product installed, but no license installed
        Option<ApplicationRoleDefinition> role = option(new MockApplicationRoleDefinition(PRODUCT_KEY, "My Application"));
        when(roleDefinitions.getDefined(any(ApplicationKey.class))).thenReturn(role);
        when(productLicense.getProperty(descriptionPropertyFor(PRODUCT_KEY))).thenReturn(null);
        assertThat(license.getProductDisplayName(), equalTo("My Application"));
    }

    @Test
    public void getProductDisplayNameReturnsPluginNameEvenWhenLicensePresent() {
        // product installed, and license installed
        Option<ApplicationRoleDefinition> role = option(new MockApplicationRoleDefinition(PRODUCT_KEY, "My Application"));
        when(roleDefinitions.getDefined(any(ApplicationKey.class))).thenReturn(role);
        when(productLicense.getProperty(descriptionPropertyFor(PRODUCT_KEY))).thenReturn("Application Name");
        assertThat(license.getProductDisplayName(), equalTo("My Application"));
    }

    @Test
    public void equalsOnlyCaresAboutProductKey() {
        assertEquals(license, new ApplicationProductLicense(APPLICATION_KEY, 10, productLicense));
    }

    @Test
    public void hashCodeOnlyCaresAboutProductKey() {
        assertEquals(license.hashCode(), new ApplicationProductLicense(APPLICATION_KEY, 10, productLicense).hashCode());
    }

    @Test
    public void getPropertyForServiceDeskFallsBackToPluginNamespaceWhenNotPresent() {
        license = new ApplicationProductLicense(SERVICE_DESK, numUsers, productLicense);

        when(productLicense.getProperty(SD_LEGACY_NAMESPACE + ".value")).thenReturn("exists");

        assertThat(license.getProperty("value"), equalTo("exists"));
        verify(productLicense).getProperty(JIRA_PRODUCT_NAMESPACE + SERVICE_DESK.value() + ".value");
    }

    static String numberOfUsersPropertyFor(String productKey) {
        return JIRA_PRODUCT_NAMESPACE + productKey + NAMESPACE_SEPARATOR + MAX_NUMBER_OF_USERS;
    }

    static String descriptionPropertyFor(String productKey) {
        return JIRA_PRODUCT_NAMESPACE + productKey + NAMESPACE_SEPARATOR + DESCRIPTION;
    }
}
