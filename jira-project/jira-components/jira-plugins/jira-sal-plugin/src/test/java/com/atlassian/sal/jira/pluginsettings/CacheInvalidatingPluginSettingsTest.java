package com.atlassian.sal.jira.pluginsettings;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public class CacheInvalidatingPluginSettingsTest {

    @Rule
    public MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private Runnable invalidateOperation;

    @Mock
    private JiraPluginSettings delegate;

    private CacheInvalidatingPluginSettings cacheInvalidatingPluginSettings;

    @Before
    public void setUp() {
        cacheInvalidatingPluginSettings = new CacheInvalidatingPluginSettings(delegate, invalidateOperation);
    }

    @Test
    public void getShouldInvalidateCache() {
        final String consistentPropertyKey = "AO_60DB71_#";

        cacheInvalidatingPluginSettings.get(consistentPropertyKey);

        verify(invalidateOperation).run();
    }

    @Test
    public void putShouldNotInvalidateCache() {
        final String consistentPropertyKey = "AO_60DB71_#";
        final String propertyValue = "1";

        cacheInvalidatingPluginSettings.put(consistentPropertyKey, propertyValue);

        verify(invalidateOperation, never()).run();
    }

    @Test
    public void removeShouldNotInvalidateCache() {
        final String consistentPropertyKey = "AO_60DB71_#";

        cacheInvalidatingPluginSettings.remove(consistentPropertyKey);

        verify(invalidateOperation, never()).run();
    }
}
