package com.atlassian.sal.jira.executor;

import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.security.JiraAuthenticationContextImpl;
import com.atlassian.jira.tenancy.JiraTenantContext;
import com.atlassian.jira.tenancy.JiraTenantImpl;
import com.atlassian.jira.util.velocity.SimpleVelocityRequestContext;
import com.atlassian.jira.util.velocity.VelocityRequestContext;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.tenancy.api.Tenant;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.mockito.Mockito.when;

public class TestJiraThreadLocalContextManager {
    @Rule
    public TestRule initMockito = new InitMockitoMocks(this);

    @Mock
    JiraAuthenticationContextImpl mockAuthenticationContext;

    @Mock
    VelocityRequestContextFactory mockVelocityRequestContextFactory;

    @Mock
    JiraTenantContext mockTenantContext;

    @Test
    public void testGetThreadLocalContext() throws InterruptedException {
        final VelocityRequestContext requestContext = new SimpleVelocityRequestContext("someurl");
        final Tenant tenant = new JiraTenantImpl("BaseTenant");

        when(mockAuthenticationContext.getUser()).thenReturn(null);
        when(mockTenantContext.getCurrentTenant()).thenReturn(tenant);
        when(mockVelocityRequestContextFactory.getJiraVelocityRequestContext()).thenReturn(requestContext);

        final JiraThreadLocalContextManager manager = new JiraThreadLocalContextManager(mockAuthenticationContext, mockVelocityRequestContextFactory, mockTenantContext);
        final Object ctx = manager.getThreadLocalContext();

        assertThat(ctx, notNullValue());

        final JiraThreadLocalContextManager.JiraThreadLocalContext context = (JiraThreadLocalContextManager.JiraThreadLocalContext) ctx;

        assertThat(context.getUser(), nullValue());
        assertThat(context.getVelocityRequestContext(), sameInstance(requestContext));
        assertThat(context.getTenant(), sameInstance(tenant));
    }
}
