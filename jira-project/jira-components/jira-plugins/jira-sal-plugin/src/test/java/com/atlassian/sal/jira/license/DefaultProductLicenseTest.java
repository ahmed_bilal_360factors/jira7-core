package com.atlassian.sal.jira.license;

import com.atlassian.sal.api.license.ProductLicense;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.extras.common.LicensePropertiesConstants.DESCRIPTION;
import static com.atlassian.extras.common.LicensePropertiesConstants.MAX_NUMBER_OF_USERS;
import static com.atlassian.extras.common.LicensePropertiesConstants.NAMESPACE_SEPARATOR;
import static com.atlassian.extras.common.LicensePropertiesConstants.UNLIMITED_USERS;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Tests {@link com.atlassian.sal.jira.license.DefaultProductLicense}.
 *
 * @since 7.0
 */
@RunWith(MockitoJUnitRunner.class)
public class DefaultProductLicenseTest {
    static final String PRODUCT_KEY = "com.company.product";

    @Mock
    com.atlassian.extras.api.ProductLicense productLicense;
    ProductLicense license;

    @Before
    public void init() {
        license = new DefaultProductLicense(PRODUCT_KEY, productLicense);
    }

    @Test
    public void getProperty() {
        when(productLicense.getProperty(PRODUCT_KEY + ".value")).thenReturn("exists");

        assertThat(license.getProperty("value"), equalTo("exists"));
        assertThat(license.getProperty(PRODUCT_KEY + ".value"), nullValue());
    }

    @Test
    public void getNumberOfUsers() {
        when(productLicense.getProperty(numberOfUsersPropertyFor(PRODUCT_KEY))).thenReturn("69");
        assertThat(license.getNumberOfUsers(), equalTo(69));

        when(productLicense.getProperty(numberOfUsersPropertyFor(PRODUCT_KEY))).thenReturn(null);
        assertThat(license.getNumberOfUsers(), equalTo(UNLIMITED_USERS));
    }

    @Test
    public void getProductDisplayName() {
        assertThat(license.getProductDisplayName(), equalTo(PRODUCT_KEY));

        when(productLicense.getProperty(descriptionPropertyFor(PRODUCT_KEY))).thenReturn("Product Name");
        assertThat(license.getProductDisplayName(), equalTo("Product Name"));
    }

    @Test
    public void equalsOnlyCaresAboutProductKey() {
        assertEquals(license, new DefaultProductLicense(PRODUCT_KEY, productLicense));
    }

    @Test
    public void hashCodeOnlyCaresAboutProductKey() {
        assertEquals(license.hashCode(), new DefaultProductLicense(PRODUCT_KEY, productLicense).hashCode());
    }

    static String numberOfUsersPropertyFor(String productKey) {
        return productKey + NAMESPACE_SEPARATOR + MAX_NUMBER_OF_USERS;
    }

    static String descriptionPropertyFor(String productKey) {
        return productKey + NAMESPACE_SEPARATOR + DESCRIPTION;
    }
}
