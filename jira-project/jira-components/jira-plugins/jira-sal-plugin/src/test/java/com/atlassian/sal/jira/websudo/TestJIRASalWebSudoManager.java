package com.atlassian.sal.jira.websudo;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.security.websudo.InternalWebSudoManager;
import com.atlassian.oauth.util.RequestAnnotations;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.mock.web.MockHttpServletRequest;

import javax.servlet.http.HttpSession;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

public class TestJIRASalWebSudoManager {
    @Rule
    public TestRule initMockitoMocks = new InitMockitoMocks(this);

    @Mock
    private InternalWebSudoManager internalWebSudoManager;

    @Mock
    private ApplicationProperties applicationProperties;

    @InjectMocks
    private JIRASalWebSudoManager webSudoManager;

    @Before
    public void setUp() throws Exception {
        when(internalWebSudoManager.isEnabled()).thenReturn(true);
        when(internalWebSudoManager.hasValidSession(any(HttpSession.class))).thenReturn(false);
    }

    @Test
    public void webSudoIsBypassedForOAuthAuthenticatedRequest() {
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        RequestAnnotations.markAsOAuthRequest(mockRequest);
        assertTrue(webSudoManager.canExecuteRequest(mockRequest));
    }

    @Test
    public void webSudoIsNotBypassedForNonOAuthRequest() {
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        assertFalse(webSudoManager.canExecuteRequest(mockRequest));
    }
}
