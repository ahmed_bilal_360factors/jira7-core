package com.atlassian.sal.jira.license;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.application.api.ApplicationManager;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.extras.api.LicenseException;
import com.atlassian.extras.api.LicenseType;
import com.atlassian.extras.api.Product;
import com.atlassian.extras.api.jira.JiraLicense;
import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationConfigurationHelper;
import com.atlassian.jira.application.ApplicationKeys;
import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.application.ApplicationRoleDefinitions;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.application.ApplicationRoleStore;
import com.atlassian.jira.application.MockApplicationRole;
import com.atlassian.jira.bc.license.JiraLicenseService;
import com.atlassian.jira.bc.license.JiraLicenseServiceImpl;
import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.help.MockHelpUrl;
import com.atlassian.jira.help.MockHelpUrls;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.license.MockLicenseDetailsBuilder;
import com.atlassian.jira.license.MockLicensedApplications;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.sal.api.license.MultiProductLicenseDetails;
import com.atlassian.sal.api.license.SingleProductLicenseDetailsView;
import com.atlassian.sal.api.validate.ValidationResult;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Matchers;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.SortedSet;

import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.isOneOf;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyCollection;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class JiraLicenseHandlerTest {
    //Raw License String
    private static final String LIC_STR_SERVICE_DESK = "SD_LIC";
    private static final String LIC_STR_SOFTWARE = "SOFT_LIC";
    private static final String LIC_STR_OTHER = "OTHER_LIC";
    private static final String LIC_STR_STARTER = "STARTER_LIC";
    private static final String LIC_STR_INVALID = "INVALID_LIC";
    private static final String LIC_STR_MULTI_PRODUCT = "MULTI_LIC";
    private static final String LIC_STR_NULL = null;

    //Application Role Key equivalent to product key
    private static final String PRODUCT_KEY_SOFTWARE = ApplicationKeys.SOFTWARE.toString();
    private static final String PRODUCT_KEY_SERVICE_DESK = ApplicationKeys.SERVICE_DESK.toString();
    private static final String PRODUCT_KEY_OTHER = "com.atlassian.other";
    private static final String PRODUCT_NAME_SERVICE_DESK = "Service Desk";
    //Application Role key equivalent to product key
    private static final String PRODUCT_NAME_SOFTWARE = "Software";
    private static final String PRODUCT_NAME_OTHER = "Other";
    private static final ApplicationKey LIC_APP_KEY_OTHER = ApplicationKey.valueOf(PRODUCT_KEY_OTHER);
    private static final String BAD_STRING = "~!@#$%^&*())(\n\n^.*[+.%{}()<>\\\\[\\\\]|\\\"\\\\_].*%$\\``";

    @Rule
    public MockitoContainer mockitoContainer = MockitoMocksInContainer.rule(this);
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();
    @Mock
    private I18nHelper.BeanFactory i18nBeanFactory;
    @Mock
    private I18nHelper i18nHelper;
    private JiraLicenseService jiraLicenseService;
    @Mock
    private JiraLicenseManager jiraLicenseManager;
    @Mock
    private ApplicationProperties applicationProperties;
    private JiraLicenseHandler jiraLicenseHandler;
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private ClusterManager clusterManager;
    @Mock
    private ApplicationManager applicationManager;
    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;
    @Mock
    @AvailableInContainer
    private ApplicationRoleManager applicationRoleManager;
    @Mock
    @AvailableInContainer
    private ApplicationConfigurationHelper appConfigHelper;
    @Mock
    private UserManager userManager;
    @Mock
    private GlobalPermissionManager permissionManager;
    @Mock
    @AvailableInContainer
    private ApplicationRoleDefinitions roleDefinitions;
    @Mock
    @AvailableInContainer
    private ApplicationRoleStore applicationRoleStore;
    @Mock
    private GroupManager groupManager;
    private MockHelpUrls helpUrls = new MockHelpUrls();
    private MockHelpUrl mockHelpUrl = new MockHelpUrl();


    @Before
    public void setUp() throws Exception {
        when(applicationProperties.getDefaultLocale()).thenReturn(Locale.getDefault());
        when(applicationManager.getApplications()).thenReturn(Sets.newHashSet());
        when(roleDefinitions.getDefined(any(ApplicationKey.class))).thenReturn(Option.none());
        when(applicationManager.getApplication(any())).thenReturn(Option.none());
        when(jiraLicenseManager.isLicenseSet()).thenAnswer(invocation ->
                !Iterables.isEmpty(jiraLicenseManager.getLicenses()));

        jiraLicenseService = spy(new JiraLicenseServiceImpl(jiraLicenseManager,
                clusterManager, applicationManager, userManager, helpUrls));

        when(i18nBeanFactory.getInstance(Matchers.<Locale>any())).thenReturn(i18nHelper);

        when(i18nHelper.getText(anyString())).thenAnswer(args -> args.getArguments()[0]);
        when(i18nHelper.getText(anyString(), anyString()))
                .thenAnswer(args -> args.getArguments()[0] + "." + args.getArguments()[1]);
        when(i18nHelper.getText(anyString(), anyString(), anyString()))
                .thenAnswer(args -> args.getArguments()[0] + "." + args.getArguments()[1] + "." + args.getArguments()[2]);
        when(i18nHelper.getText(anyString(), anyString(), anyString(), anyString(), anyString()))
                .thenAnswer(args -> args.getArguments()[0] + "." + args.getArguments()[1] + "." + args.getArguments()[2] + "." + args.getArguments()[3] + "." + args.getArguments()[4]);
        when(i18nHelper.getText(anyString(), anyCollection()))
                .thenAnswer(args -> args.getArguments()[0] + "." + args.getArguments()[1].toString());

        mockitoContainer.getMockComponentContainer().addMockComponent(ApplicationConfigurationHelper.class, appConfigHelper);
        when(appConfigHelper.validateApplicationForConfiguration(
                org.mockito.Matchers.any(ApplicationKey.class),
                org.mockito.Matchers.any(ApplicationUser.class))).thenReturn(Optional.<String>empty());

        jiraLicenseHandler = new JiraLicenseHandler(jiraLicenseService, jiraLicenseManager, i18nBeanFactory,
                applicationProperties, eventPublisher, jiraAuthenticationContext, permissionManager, groupManager);

        mockHelpUrl.setUrl("test-url");
        mockHelpUrl.setKey("license.compatibility");
        helpUrls.addUrl(mockHelpUrl);
    }

    @Test
    public void shouldAllowUserToInstallELALicenseWhenLicenseHasMoreSeatsThanActiveUserCount() {
        //Given
        setupInstallProductLicense(ApplicationKeys.SERVICE_DESK, LIC_STR_SERVICE_DESK, 10);
        setupInstallProductPlugin(ApplicationKeys.SERVICE_DESK, PRODUCT_NAME_SERVICE_DESK);
        setupSetActiveUserCount(ApplicationKeys.SERVICE_DESK, 10);

        setupInstallProductLicense(ApplicationKeys.SOFTWARE, LIC_STR_SOFTWARE, 20);
        setupInstallProductPlugin(ApplicationKeys.SOFTWARE, PRODUCT_NAME_SOFTWARE);
        setupSetActiveUserCount(ApplicationKeys.SOFTWARE, 20);

        setupInstallProductLicense(LIC_APP_KEY_OTHER, LIC_STR_OTHER, 30);
        setupInstallProductPlugin(LIC_APP_KEY_OTHER, PRODUCT_NAME_OTHER);
        setupSetActiveUserCount(LIC_APP_KEY_OTHER, 30);

        //License Under Validation
        LicenseDetails mocLicenseDetailsForValidation = new MockLicenseDetailsBuilder()
                .setRawLicense(LIC_STR_MULTI_PRODUCT)
                        //Same as active user count
                .setApplicationRoleWithLimit(ApplicationKeys.SERVICE_DESK, 10)
                        //Unlimited
                .setApplicationRoleWithLimit(ApplicationKeys.SOFTWARE, -1)
                        //One more than active user count
                .setApplicationRoleWithLimit(LIC_APP_KEY_OTHER, 31)
                .build();
        when(jiraLicenseManager.getLicense(LIC_STR_MULTI_PRODUCT)).thenReturn(mocLicenseDetailsForValidation);

        //When
        final ValidationResult validationResult = jiraLicenseHandler.validateProductLicense(PRODUCT_KEY_SOFTWARE, LIC_STR_MULTI_PRODUCT, Locale.ENGLISH);

        //Then
        assertValidationResultContainsNoWarnings(validationResult);
    }

    @Test
    public void shouldAllowUserToInstallLicenseWhenProductNotInstalled() {
        //License Under Validation
        LicenseDetails mocLicenseDetailsForValidation = new MockLicenseDetailsBuilder()
                .setRawLicense(LIC_STR_MULTI_PRODUCT)
                        //Same as active user count
                .setApplicationRoleWithLimit(ApplicationKeys.SERVICE_DESK, 10)
                        //Unlimited
                .setApplicationRoleWithLimit(ApplicationKeys.SOFTWARE, -1)
                        //One more than active user count
                .setApplicationRoleWithLimit(LIC_APP_KEY_OTHER, 31)
                .build();
        when(jiraLicenseManager.getLicense(LIC_STR_MULTI_PRODUCT)).thenReturn(mocLicenseDetailsForValidation);
        when(jiraLicenseManager.getLicenses()).thenReturn(installedProductLicenses);

        //When
        final ValidationResult validationResult = jiraLicenseHandler.validateProductLicense(PRODUCT_KEY_SOFTWARE, LIC_STR_MULTI_PRODUCT, Locale.ENGLISH);

        //Then
        assertValidationResultContainsNoWarnings(validationResult);
    }

    @Test
    public void shouldReturnWarningMessageWhenUnableToConfigureApplication() {
        //Given
        LicenseDetails mocLicenseDetailsForValidation = new MockLicenseDetailsBuilder()
                .setRawLicense(LIC_STR_OTHER)
                .setApplicationRoleWithLimit(LIC_APP_KEY_OTHER, 1)
                .build();
        when(jiraLicenseManager.getLicense(LIC_STR_OTHER)).thenReturn(mocLicenseDetailsForValidation);
        when(jiraLicenseManager.getLicenses()).thenReturn(installedProductLicenses);
        when(appConfigHelper.validateApplicationForConfiguration(LIC_APP_KEY_OTHER,
                jiraAuthenticationContext.getLoggedInUser())).thenReturn(Optional.of("warning"));

        //When
        final ValidationResult validationResult = jiraLicenseHandler.validateProductLicense(PRODUCT_KEY_OTHER,
                LIC_STR_OTHER, Locale.ENGLISH);

        //Then
        assertValidationResultContainsWarnings(validationResult, "warning");
    }

    @Test
    public void shouldWarnUserWhenInstallingMultiLicenseWithSeatsLessThanActiveUserCountForProducts() {
        //Given
        setupInstallProductLicense(ApplicationKeys.SERVICE_DESK, LIC_STR_SERVICE_DESK, 60);
        setupInstallProductPlugin(ApplicationKeys.SERVICE_DESK, PRODUCT_NAME_SERVICE_DESK);
        setupSetActiveUserCount(ApplicationKeys.SERVICE_DESK, 50);

        setupInstallProductLicense(ApplicationKeys.SOFTWARE, LIC_STR_SOFTWARE, 80);
        setupInstallProductPlugin(ApplicationKeys.SOFTWARE, PRODUCT_NAME_SOFTWARE);
        setupSetActiveUserCount(ApplicationKeys.SOFTWARE, 70);

        //License Under Validation
        LicenseDetails mocLicenseDetailsForValidation = new MockLicenseDetailsBuilder()
                .setRawLicense(LIC_STR_MULTI_PRODUCT)
                        //Has one less than active user count
                .setApplicationRoleWithLimit(ApplicationKeys.SERVICE_DESK, 49)
                        //Has only 1 seat
                .setApplicationRoleWithLimit(ApplicationKeys.SOFTWARE, 1)
                .build();
        when(jiraLicenseManager.getLicense(LIC_STR_MULTI_PRODUCT)).thenReturn(mocLicenseDetailsForValidation);

        //When
        final ValidationResult validationResult = jiraLicenseHandler.validateProductLicense(PRODUCT_KEY_SOFTWARE, LIC_STR_MULTI_PRODUCT, Locale.ENGLISH);

        //Then
        assertValidationResultContainsWarnings(validationResult,
                mockMessage("jira.license.validation.product.active.user.count.exceeds.license.limit", PRODUCT_NAME_SOFTWARE),
                mockMessage("jira.license.validation.product.active.user.count.exceeds.license.limit", PRODUCT_NAME_SERVICE_DESK));
    }

    @Test
    public void shouldWarnUserWhenInstallingMultiLicenseWithSeatsLessThanActiveUserCountForSingleProduct() {
        //Given
        setupInstallProductLicense(ApplicationKeys.SERVICE_DESK, LIC_STR_SERVICE_DESK, 60);
        setupInstallProductPlugin(ApplicationKeys.SERVICE_DESK, PRODUCT_NAME_SERVICE_DESK);
        setupSetActiveUserCount(ApplicationKeys.SERVICE_DESK, 50);

        setupInstallProductLicense(ApplicationKeys.SOFTWARE, LIC_STR_SOFTWARE, 80);
        setupInstallProductPlugin(ApplicationKeys.SOFTWARE, PRODUCT_NAME_SOFTWARE);
        setupSetActiveUserCount(ApplicationKeys.SOFTWARE, 70);

        //License Under Validation
        LicenseDetails mocLicenseDetailsForValidation = new MockLicenseDetailsBuilder()
                .setRawLicense(LIC_STR_MULTI_PRODUCT)
                        //One more than active user count
                .setApplicationRoleWithLimit(ApplicationKeys.SERVICE_DESK, 51)
                        //Has less that active user count
                .setApplicationRoleWithLimit(ApplicationKeys.SOFTWARE, 40)
                .build();
        when(jiraLicenseManager.getLicense(LIC_STR_MULTI_PRODUCT)).thenReturn(mocLicenseDetailsForValidation);

        //When
        final ValidationResult validationResult = jiraLicenseHandler.validateProductLicense(PRODUCT_KEY_SOFTWARE, LIC_STR_MULTI_PRODUCT, Locale.ENGLISH);

        //Then
        assertValidationResultContainsWarnings(validationResult,
                mockMessage("jira.license.validation.product.active.user.count.exceeds.license.limit",
                        PRODUCT_NAME_SOFTWARE));
    }

    @Test
    public void shouldWarnUserWhenInstallingLicenseWithSeatsLessThanActiveUserCountForSingleProduct() {
        //Given
        setupInstallProductLicense(ApplicationKeys.SERVICE_DESK, LIC_STR_SERVICE_DESK, 60);
        setupInstallProductPlugin(ApplicationKeys.SERVICE_DESK, PRODUCT_NAME_SERVICE_DESK);
        setupSetActiveUserCount(ApplicationKeys.SERVICE_DESK, 50);

        //License Under Validation
        LicenseDetails mocLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(LIC_STR_SERVICE_DESK)
                        //Has less than active user count
                .setApplicationRoleWithLimit(ApplicationKeys.SERVICE_DESK, 30)
                .build();
        when(jiraLicenseManager.getLicense(LIC_STR_SERVICE_DESK)).thenReturn(mocLicenseDetails);

        //When
        final ValidationResult validationResult = jiraLicenseHandler.validateProductLicense(PRODUCT_KEY_SERVICE_DESK, LIC_STR_SERVICE_DESK, Locale.ENGLISH);

        //Then
        assertValidationResultContainsWarnings(validationResult,
                mockMessage("jira.license.validation.product.active.user.count.exceeds.license.limit", PRODUCT_NAME_SERVICE_DESK));
    }

    private void setupLatentConfiguration() {
        // Set latent configuration
        ApplicationRoleDefinitions.ApplicationRoleDefinition swDefinition = mock(ApplicationRoleDefinitions.ApplicationRoleDefinition.class);
        when(swDefinition.key()).thenReturn(ApplicationKeys.SOFTWARE);
        when(swDefinition.name()).thenReturn(PRODUCT_NAME_SOFTWARE);
        Option<ApplicationRoleDefinitions.ApplicationRoleDefinition> swDefinitionOption = Option.some(swDefinition);
        when(roleDefinitions.getDefined(ApplicationKeys.SOFTWARE)).thenReturn(swDefinitionOption);
        when(applicationRoleManager.getRole(ApplicationKeys.SOFTWARE)).thenReturn(Option.none());
        // Set group for latent configuration
        ApplicationRoleStore.ApplicationRoleData roleData = new ApplicationRoleStore.ApplicationRoleData(ApplicationKeys.SOFTWARE,
                Sets.newHashSet("jira-developers"), Sets.newHashSet("jira-developers"), true);
        when(applicationRoleStore.get(ApplicationKeys.SOFTWARE)).thenReturn(roleData);

    }

    private void makeLicenseValid(String licenseString, ApplicationKey appToLicense) {
        JiraLicenseService.ValidationResult validationResult = mock(JiraLicenseService.ValidationResult.class);
        when(validationResult.getErrorCollection()).thenReturn(new SimpleErrorCollection());
        LicenseDetails details = mock(LicenseDetails.class);
        when(details.getLicensedApplications()).thenReturn(new MockLicensedApplications(newArrayList(appToLicense)));
        when(jiraLicenseManager.getLicense(licenseString)).thenReturn(details);

        doReturn(validationResult).when(jiraLicenseService).validate(ApplicationKeys.SOFTWARE, licenseString, i18nHelper);
        doReturn(validationResult).when(jiraLicenseService).validate(i18nHelper, licenseString);
    }

    @Test
    public void shouldWarnWhenReactivatingUsers() {
        setupLatentConfiguration();
        makeLicenseValid(LIC_STR_SOFTWARE, ApplicationKeys.SOFTWARE);

        //Set users for group
        ApplicationUser inactiveUser1 = new MockApplicationUser("sw1");
        ApplicationUser inactiveUser2 = new MockApplicationUser("sw2");
        ApplicationUser admin = new MockApplicationUser("swadmin");
        when(permissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, admin)).thenReturn(true);
        when(groupManager.getUsersInGroup("jira-developers")).thenReturn(Sets.newHashSet(inactiveUser1, inactiveUser2, admin));
        // Users are inactive
        when(applicationRoleManager.getRolesForUser(argThat(isOneOf(inactiveUser1, inactiveUser2, admin))))
                .thenReturn(Collections.emptySet());

        //When
        final ValidationResult validationResult = jiraLicenseHandler.validateProductLicense(PRODUCT_KEY_SOFTWARE, LIC_STR_SOFTWARE, Locale.ENGLISH);

        //Then
        assertValidationResultContainsWarnings(validationResult,
                mockMessage("jira.license.validation.activate.user",
                        newArrayList(2, PRODUCT_NAME_SOFTWARE).toString()));
    }

    @Test
    public void shouldNotWarnWhenAdminDoesNotHaveOtherRoles() {
        // If there is an admin associated with the role to license this is not considered a reactivation because
        // admins always have some sort of access.
        setupLatentConfiguration();
        makeLicenseValid(LIC_STR_SOFTWARE, ApplicationKeys.SOFTWARE);

        ApplicationUser admin = new MockApplicationUser("swadmin");
        when(permissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, admin)).thenReturn(true);
        when(groupManager.getUsersInGroup("jira-developers")).thenReturn(Sets.newHashSet(admin));
        // Admin does not have other roles
        when(applicationRoleManager.getRolesForUser(admin)).thenReturn(Collections.emptySet());

        final ValidationResult validationResult = jiraLicenseHandler.validateProductLicense(PRODUCT_KEY_SOFTWARE, LIC_STR_SOFTWARE, Locale.ENGLISH);
        assertTrue("License should be valid", validationResult.isValid());
        assertFalse("License should not have warnings", validationResult.hasWarnings());
    }

    @Test
    public void latentConfigurationNotCheckedForActiveRoles() {
        makeLicenseValid(LIC_STR_SOFTWARE, ApplicationKeys.SOFTWARE);
        setupLatentConfiguration();
        when(applicationRoleManager.getRole(ApplicationKeys.SOFTWARE))
                .thenReturn(Option.some(new MockApplicationRole(ApplicationKeys.SOFTWARE)));

        final ValidationResult validationResult = jiraLicenseHandler.validateProductLicense(PRODUCT_KEY_SOFTWARE,
                LIC_STR_SOFTWARE, Locale.ENGLISH);
        assertTrue("License should be valid", validationResult.isValid());
        assertFalse("License should not have warnings", validationResult.hasWarnings());
        verifyZeroInteractions(applicationRoleStore);
        verify(groupManager, never()).getUsersInGroup("jira-developers");
    }

    @Test
    public void shouldFailValidationLicenseForOtherProduct() {
        //Given
        setupInstallProductLicense(ApplicationKeys.SERVICE_DESK, LIC_STR_SERVICE_DESK, 60);
        setupInstallProductPlugin(ApplicationKeys.SERVICE_DESK, PRODUCT_NAME_SERVICE_DESK);
        setupSetActiveUserCount(ApplicationKeys.SERVICE_DESK, 50);

        //License Under Validation
        LicenseDetails mocLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(LIC_STR_SERVICE_DESK)
                        //Has less than active user count
                .setApplicationRoleWithLimit(ApplicationKeys.SERVICE_DESK, 30)
                .build();
        when(jiraLicenseManager.getLicense(LIC_STR_SERVICE_DESK)).thenReturn(mocLicenseDetails);

        //When
        final ValidationResult validationResult = jiraLicenseHandler.validateProductLicense(PRODUCT_KEY_SOFTWARE, LIC_STR_SERVICE_DESK, Locale.ENGLISH);

        //Then
        assertValidationResultContainsError(validationResult, "jira.license.validation.product.key.not.for.license");
    }

    private String mockMessage(final String key, final String... params) {
        StringBuilder stb = new StringBuilder(key);
        for (String param : params) {
            stb.append(".");
            stb.append(param);
        }
        return stb.toString();
    }

    private void setupSetActiveUserCount(final ApplicationKey key, final int activeUserCount) {
        when(applicationRoleManager.getUserCount(key)).thenReturn(activeUserCount);
    }

    private final Set<ApplicationRole> installedApplications = Sets.newHashSet();

    private void setupInstallProductPlugin(final ApplicationKey key, final String productName) {
        when(applicationRoleManager.isRoleInstalledAndLicensed(key)).thenReturn(Boolean.TRUE);
        ApplicationRole mockApplicationRole = new MockApplicationRole().key(key).name(productName);
        when(applicationRoleManager.getRole(key)).thenReturn(Option.option(mockApplicationRole));
        installedApplications.add(mockApplicationRole);
        when(applicationRoleManager.getRoles()).thenReturn(installedApplications);
    }

    private final List<LicenseDetails> installedProductLicenses = newArrayList();

    private LicenseDetails setupInstallProductLicense(final ApplicationKey key, final String licenseString, final int productLicenseUserLimit) {
        LicenseDetails mocLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(licenseString)
                .setApplicationRoleWithLimit(key, productLicenseUserLimit)
                .build();

        installedProductLicenses.add(mocLicenseDetails);

        when(jiraLicenseManager.getLicenses()).thenReturn(installedProductLicenses);

        return mocLicenseDetails;
    }

    @Test
    public void shouldPassValidation() {
        //Given:
        LicenseDetails mockServiceDeskLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(LIC_STR_SERVICE_DESK)
                .setApplicationRoleWithLimit(ApplicationKeys.SERVICE_DESK, 11)
                .setLicenseType(LicenseType.COMMERCIAL)
                .build();

        //When:
        when(jiraLicenseManager.getLicenses()).thenReturn(newArrayList(mockServiceDeskLicenseDetails));
        when(jiraLicenseManager.getLicense(LIC_STR_SERVICE_DESK)).thenReturn(mockServiceDeskLicenseDetails);

        //Then:
        final ValidationResult validationResult = jiraLicenseHandler.validateProductLicense(PRODUCT_KEY_SERVICE_DESK, LIC_STR_SERVICE_DESK, Locale.ENGLISH);
        assertTrue(validationResult.isValid());
    }

    @Test
    public void shouldProvideErrorMessageWhenInvalidProductKey() {
        assertInvalidProductKey(null);
        assertInvalidProductKey("");
        assertInvalidProductKey("    ");
        assertInvalidProductKey("1");
        assertInvalidProductKey(BAD_STRING);
    }

    private void assertInvalidProductKey(String productKey) {
        assertValidationResultContainsError(
                jiraLicenseHandler.validateProductLicense(productKey, LIC_STR_SERVICE_DESK, Locale.ENGLISH),
                "jira.license.validation.invalid.product.key");
    }

    @Test
    public void shouldProvideErrorMessageWhenInvalidLicenseKey() {
        when(jiraLicenseManager.getLicense("1")).thenThrow(LicenseException.class);
        when(jiraLicenseManager.getLicense(BAD_STRING)).thenThrow(LicenseException.class);
        when(jiraLicenseManager.getLicense(LIC_STR_SERVICE_DESK + "A")).thenThrow(LicenseException.class);

        assertInvalidLicenseKey(null);
        assertInvalidLicenseKey("");
        assertInvalidLicenseKey("    ");
        assertInvalidLicenseKey("1");
        assertInvalidLicenseKey(BAD_STRING);
        assertInvalidLicenseKey(LIC_STR_SERVICE_DESK + "A");
    }

    private void assertInvalidLicenseKey(String licenseKey) {
        assertValidationResultContainsError(
                jiraLicenseHandler.validateProductLicense(PRODUCT_KEY_SERVICE_DESK, licenseKey, Locale.ENGLISH),
                "jira.license.validation.invalid.license.key");
    }

    @Test
    public void shouldTolerateSwitchingFromStarterLicense() {
        LicenseDetails installedLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(LIC_STR_STARTER)
                .setLicenseType(LicenseType.STARTER)
                .setApplicationRoleWithLimit(ApplicationKeys.SERVICE_DESK, 11)
                .build();
        LicenseDetails mockServiceDeskLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(LIC_STR_SERVICE_DESK)
                .setLicenseType(LicenseType.ACADEMIC)
                .setApplicationRoleWithLimit(ApplicationKeys.SERVICE_DESK, 44)
                .build();

        //When:
        when(jiraLicenseManager.getLicense(LIC_STR_STARTER)).thenReturn(installedLicenseDetails);
        when(jiraLicenseManager.getLicenses()).thenReturn(Collections.singletonList(installedLicenseDetails));
        when(jiraLicenseManager.getLicense(LIC_STR_SERVICE_DESK)).thenReturn(mockServiceDeskLicenseDetails);

        ValidationResult validationResult = jiraLicenseHandler.validateProductLicense(PRODUCT_KEY_SERVICE_DESK, LIC_STR_SERVICE_DESK, Locale.ENGLISH);
        assertTrue(Iterables.isEmpty(validationResult.getErrorMessages()));
        assertTrue(Iterables.isEmpty(validationResult.getWarningMessages()));
    }

    @Test
    public void shouldTolerateSwitchingFromAnyToStarterLicense() {
        LicenseDetails installedLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(LIC_STR_SERVICE_DESK)
                .setLicenseType(LicenseType.ACADEMIC)
                .setApplicationRoleWithLimit(ApplicationKeys.SERVICE_DESK, 11)
                .build();
        LicenseDetails mockServiceDeskLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(LIC_STR_STARTER)
                .setLicenseType(LicenseType.STARTER)
                .setApplicationRoleWithLimit(ApplicationKeys.SERVICE_DESK, 44)
                .build();

        //When:
        when(jiraLicenseManager.getLicense(LIC_STR_SERVICE_DESK)).thenReturn(installedLicenseDetails);
        when(jiraLicenseManager.getLicenses()).thenReturn(Collections.singletonList(installedLicenseDetails));
        when(jiraLicenseManager.getLicense(LIC_STR_STARTER)).thenReturn(mockServiceDeskLicenseDetails);

        ValidationResult validationResult = jiraLicenseHandler.validateProductLicense(PRODUCT_KEY_SERVICE_DESK, LIC_STR_SERVICE_DESK, Locale.ENGLISH);
        assertTrue(Iterables.isEmpty(validationResult.getErrorMessages()));
        assertTrue(Iterables.isEmpty(validationResult.getWarningMessages()));
    }

    @Test
    public void shouldRejectSwitchingBetweenLicenseTypesForDifferentApplications() {
        LicenseDetails installedLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(LIC_STR_SERVICE_DESK)
                .setLicenseType(LicenseType.ACADEMIC)
                .setApplicationRoleWithLimit(ApplicationKeys.SERVICE_DESK, 11)
                .build();
        LicenseDetails mockServiceDeskLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(LIC_STR_OTHER)
                .setLicenseType(LicenseType.COMMERCIAL)
                .setApplicationRoleWithLimit(ApplicationKeys.SOFTWARE, 44)
                .build();

        //When:
        when(jiraLicenseManager.getLicense(LIC_STR_SERVICE_DESK)).thenReturn(installedLicenseDetails);
        when(jiraLicenseManager.getLicenses()).thenReturn(Collections.singletonList(installedLicenseDetails));
        when(jiraLicenseManager.getLicense(LIC_STR_OTHER)).thenReturn(mockServiceDeskLicenseDetails);
        when(i18nHelper.getText(anyString(), anyObject())).thenReturn("i18n message");

        ValidationResult validationResult = jiraLicenseHandler.validateProductLicense(PRODUCT_KEY_SERVICE_DESK, LIC_STR_OTHER, Locale.ENGLISH);
        assertEquals(1, Iterables.size(validationResult.getErrorMessages()));
        assertTrue(Iterables.isEmpty(validationResult.getWarningMessages()));
    }

    @Test
    public void validationShouldFailWhenAddingNotPaidLicenseTypeWithMultiplePaidTypesInstalled() {
        LicenseDetails installedLicenseDetails1 = new MockLicenseDetailsBuilder()
                .setRawLicense(LIC_STR_SERVICE_DESK)
                .setLicenseType(LicenseType.ACADEMIC)
                .setApplicationRoleWithLimit(ApplicationKeys.SERVICE_DESK, 11)
                .build();
        LicenseDetails installedLicenseDetails2 = new MockLicenseDetailsBuilder()
                .setRawLicense(LIC_STR_SOFTWARE)
                .setLicenseType(LicenseType.ACADEMIC)
                .setApplicationRoleWithLimit(ApplicationKeys.SOFTWARE, 11)
                .build();
        LicenseDetails mockServiceDeskLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(LIC_STR_OTHER)
                .setLicenseType(LicenseType.COMMUNITY)
                .setApplicationRoleWithLimit(ApplicationKeys.SERVICE_DESK, 44)
                .build();

        //When:
        when(jiraLicenseManager.getLicense(LIC_STR_SERVICE_DESK)).thenReturn(installedLicenseDetails1);
        when(jiraLicenseManager.getLicense(LIC_STR_SOFTWARE)).thenReturn(installedLicenseDetails2);
        when(jiraLicenseManager.getLicenses()).thenReturn(Arrays.asList(installedLicenseDetails1, installedLicenseDetails2));
        when(jiraLicenseManager.getLicense(LIC_STR_OTHER)).thenReturn(mockServiceDeskLicenseDetails);
        when(i18nHelper.getText(anyString(), anyObject())).thenReturn("i18n message");

        ValidationResult validationResult = jiraLicenseHandler.validateProductLicense(PRODUCT_KEY_SERVICE_DESK, LIC_STR_OTHER, Locale.ENGLISH);
        assertThat(validationResult.getErrorMessages(),
                equalTo(ImmutableSet.of("jira.license.validation.does.not.match.existing.license.COMMUNITY.ACADEMIC.<a href=\"test-url\">.</a>")));
        assertTrue(Iterables.isEmpty(validationResult.getWarningMessages()));
    }


    @Test
    public void shouldAllowSwitchingBetweenLicenseTypesForTheSameApplicationAndWithOneExistingLicense() {
        LicenseDetails installedLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(LIC_STR_SERVICE_DESK)
                .setLicenseType(LicenseType.ACADEMIC)
                .setApplicationRoleWithLimit(ApplicationKeys.SERVICE_DESK, 11)
                .build();
        LicenseDetails mockServiceDeskLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(LIC_STR_OTHER)
                .setLicenseType(LicenseType.COMMERCIAL)
                .setApplicationRoleWithLimit(ApplicationKeys.SERVICE_DESK, 44)
                .build();

        //When:
        when(jiraLicenseManager.getLicense(LIC_STR_SERVICE_DESK)).thenReturn(installedLicenseDetails);
        when(jiraLicenseManager.getLicenses()).thenReturn(Collections.singletonList(installedLicenseDetails));
        when(jiraLicenseManager.getLicense(LIC_STR_OTHER)).thenReturn(mockServiceDeskLicenseDetails);
        when(i18nHelper.getText(anyString(), anyObject())).thenReturn("i18n message");

        ValidationResult validationResult = jiraLicenseHandler.validateProductLicense(PRODUCT_KEY_SERVICE_DESK, LIC_STR_OTHER, Locale.ENGLISH);
        assertTrue(Iterables.isEmpty(validationResult.getErrorMessages()));
        assertTrue(Iterables.isEmpty(validationResult.getWarningMessages()));
    }

    private void assertValidationResultContainsNoWarnings(final ValidationResult validationResult) {
        assertTrue(validationResult.getErrorMessages().toString(), validationResult.isValid());
        assertFalse(validationResult.getErrorMessages().toString(), validationResult.hasErrors());
        assertFalse(validationResult.getWarningMessages().toString(), validationResult.hasWarnings());
        assertEquals(0, Iterables.size(validationResult.getWarningMessages()));
        assertEquals(0, Iterables.size(validationResult.getErrorMessages()));
    }

    private void assertValidationResultContainsWarnings(final ValidationResult validationResult, String... warningKeys) {
        assertTrue(validationResult.getErrorMessages().toString(), validationResult.isValid());
        assertFalse(validationResult.getErrorMessages().toString(), validationResult.hasErrors());
        assertTrue(validationResult.getWarningMessages().toString(), validationResult.hasWarnings());
        assertEquals(warningKeys.length, Iterables.size(validationResult.getWarningMessages()));
        assertThat(validationResult.getWarningMessages(), containsInAnyOrder(warningKeys));
        assertEquals(0, Iterables.size(validationResult.getErrorMessages()));
    }

    private void assertValidationResultContainsError(final ValidationResult validationResult, String error) {
        assertFalse(validationResult.getErrorMessages().toString(), validationResult.isValid());
        assertTrue(validationResult.getErrorMessages().toString(), validationResult.hasErrors());
        assertFalse(validationResult.getWarningMessages().toString(), validationResult.hasWarnings());
        assertThat(validationResult.getErrorMessages(), containsInAnyOrder(error));
        assertEquals(0, Iterables.size(validationResult.getWarningMessages()));
    }

    @Test
    public void shouldGetCorrectLicenseDetailsForProductKey() {
        //Given:
        LicenseDetails mockServiceDeskLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(LIC_STR_SERVICE_DESK)
                .setApplicationRoleWithLimit(ApplicationKeys.SERVICE_DESK, 11)
                .build();
        //Software and other in same license
        LicenseDetails mockSoftwareLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(LIC_STR_SOFTWARE)
                .setApplicationRoleWithLimit(ApplicationKeys.SOFTWARE, 33)
                .setApplicationRoleWithLimit(LIC_APP_KEY_OTHER, 22)
                .build();

        //When:
        when(jiraLicenseManager.getLicenses()).thenReturn(
                newArrayList(mockServiceDeskLicenseDetails, mockSoftwareLicenseDetails));

        //Then:
        LicenseDetails serviceDeskLicenseDetailsFound = jiraLicenseHandler.getLicenseDetailsByApplicationKey(ApplicationKeys.SERVICE_DESK);
        assertNotNull(serviceDeskLicenseDetailsFound);
        assertEquals(11, serviceDeskLicenseDetailsFound.getLicensedApplications().getUserLimit(ApplicationKeys.SERVICE_DESK));
        LicenseDetails softwareLicenseDetailsFound = jiraLicenseHandler.getLicenseDetailsByApplicationKey(ApplicationKeys.SOFTWARE);
        assertNotNull(softwareLicenseDetailsFound);
        assertEquals(22, softwareLicenseDetailsFound.getLicensedApplications().getUserLimit(LIC_APP_KEY_OTHER));
        assertEquals(33, softwareLicenseDetailsFound.getLicensedApplications().getUserLimit(ApplicationKeys.SOFTWARE));
    }

    @Test
    public void shouldFailToGetLicenseDetailsByInvalidProductKey() {
        //When:
        final LicenseDetails mock = mock(LicenseDetails.class);
        when(mock.getLicensedApplications()).thenReturn(new MockLicensedApplications());
        when(jiraLicenseManager.getLicenses()).thenReturn(ImmutableList.of(mock));

        //Then:
        assertNull(jiraLicenseHandler.getLicenseDetailsByApplicationKey(LIC_APP_KEY_OTHER));
    }

    @Test
    public void shouldGetRawProductLicense() {
        //Given:
        LicenseDetails mockServiceDeskLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(LIC_STR_SERVICE_DESK)
                .setApplicationRoleWithLimit(ApplicationKeys.SERVICE_DESK, 11)
                .build();

        //When:
        when(jiraLicenseManager.getLicenses()).thenReturn(newArrayList(mockServiceDeskLicenseDetails));

        //Then:
        assertEquals(LIC_STR_SERVICE_DESK, jiraLicenseHandler.getRawProductLicense(PRODUCT_KEY_SERVICE_DESK));
        verify(jiraLicenseManager, times(1)).getLicenses();
    }

    @Test
    public void shouldFailToGetRawProductLicense() {
        //Given:
        LicenseDetails mockServiceDeskLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(LIC_STR_SERVICE_DESK)
                .setApplicationRoleWithLimit(ApplicationKeys.SERVICE_DESK, 11)
                .build();

        //When:
        when(jiraLicenseManager.getLicenses()).thenReturn(newArrayList(mockServiceDeskLicenseDetails));

        //Then:
        assertNull(jiraLicenseHandler.getRawProductLicense(PRODUCT_KEY_OTHER));
        verify(jiraLicenseManager, times(1)).getLicenses();
    }

    @Test
    public void shouldGetAllProductKeys() {

        //Given:
        LicenseDetails mockServiceDeskLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(LIC_STR_SERVICE_DESK)
                .setApplicationRoleWithLimit(ApplicationKeys.SERVICE_DESK, 11)
                .build();
        //Software and other in same license
        LicenseDetails mockSoftwareLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(LIC_STR_SOFTWARE)
                .setApplicationRoleWithLimit(ApplicationKeys.SOFTWARE, 33)
                .setApplicationRoleWithLimit(LIC_APP_KEY_OTHER, 22)
                .build();

        //When:
        when(jiraLicenseManager.getLicenses()).thenReturn(
                newArrayList(mockServiceDeskLicenseDetails, mockSoftwareLicenseDetails));

        //Then:
        assertThat(jiraLicenseHandler.getProductKeys(), (Matcher) hasItems(PRODUCT_KEY_SERVICE_DESK,
                PRODUCT_KEY_SOFTWARE, PRODUCT_KEY_OTHER));
    }

    @Test
    public void shouldSetLicense() {
        //Given:
        makeLicenseValid(LIC_STR_SERVICE_DESK, ApplicationKeys.SERVICE_DESK);

        //When:
        jiraLicenseHandler.setLicense(LIC_STR_SERVICE_DESK);

        //Then:
        verify(jiraLicenseManager, times(1)).clearAndSetLicense(LIC_STR_SERVICE_DESK);
    }

    @Test
    public void shouldFailToSetInvalidLicense() {
        //Given:
        I18nHelper mockI18nHelper = mock(I18nHelper.class);
        when(i18nBeanFactory.getInstance(Locale.getDefault())).thenReturn(mockI18nHelper);
        JiraLicenseService.ValidationResult validationResult = mock(JiraLicenseService.ValidationResult.class);
        final SimpleErrorCollection errors = new SimpleErrorCollection();
        errors.addErrorMessage("Invalid License");
        when(validationResult.getErrorCollection()).thenReturn(errors);
        doReturn(validationResult).when(jiraLicenseService).validate(mockI18nHelper, LIC_STR_INVALID);

        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Specified license was invalid.");
        //When:
        jiraLicenseHandler.setLicense(LIC_STR_INVALID);
    }

    @Test
    public void shouldGetNullRawProductLicenseIfNullProductKeyProvided() {
        assertNull(jiraLicenseHandler.getRawProductLicense(null));
    }

    @Test
    public void shouldGetAllProductLicenses() {
        //Given:
        LicenseDetails mockLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(LIC_STR_MULTI_PRODUCT)
                .setApplicationRoleWithLimit(ApplicationKeys.SERVICE_DESK, 11)
                .setApplicationRoleWithLimit(ApplicationKeys.SOFTWARE, 33)
                .build();
        LicenseDetails mockOtherLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(LIC_STR_OTHER)
                .setApplicationRoleWithLimit(LIC_APP_KEY_OTHER, 44)
                .build();

        //When:
        when(jiraLicenseManager.getLicenses()).thenReturn(newArrayList(mockLicenseDetails, mockOtherLicenseDetails));

        //Then:
        Collection<MultiProductLicenseDetails> allMultiProductLicenseDetails = jiraLicenseHandler.getAllProductLicenses();
        assertEquals(2, allMultiProductLicenseDetails.size());
    }

    @Test
    public void shouldFailToGetNonExistentProductKey() {
        //Given:
        LicenseDetails mockLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(LIC_STR_MULTI_PRODUCT)
                .setApplicationRoleWithLimit(ApplicationKeys.SERVICE_DESK, 11)
                .setApplicationRoleWithLimit(ApplicationKeys.SOFTWARE, 33)
                .build();

        //When:
        when(jiraLicenseManager.getLicenses()).thenReturn(newArrayList(mockLicenseDetails));

        //Then:
        SingleProductLicenseDetailsView singleProductLicenseDetailsView = jiraLicenseHandler.getProductLicenseDetails(PRODUCT_KEY_OTHER);
        assertNull(singleProductLicenseDetailsView);
    }

    @Test
    public void shouldGetSingleProductLicense() {
        //Given:
        LicenseDetails mockLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(LIC_STR_MULTI_PRODUCT)
                .setApplicationRoleWithLimit(ApplicationKeys.SERVICE_DESK, 11)
                .setApplicationRoleWithLimit(ApplicationKeys.SOFTWARE, 33)
                .build();
        LicenseDetails mockOtherLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(LIC_STR_OTHER)
                .setApplicationRoleWithLimit(LIC_APP_KEY_OTHER, 44)
                .build();

        //When:
        when(jiraLicenseManager.getLicenses()).thenReturn(newArrayList(mockLicenseDetails, mockOtherLicenseDetails));

        //Then:
        SingleProductLicenseDetailsView singleProductLicenseDetailsView = jiraLicenseHandler.getProductLicenseDetails(PRODUCT_KEY_SERVICE_DESK);
        assertNotNull(singleProductLicenseDetailsView);
        assertEquals(11, singleProductLicenseDetailsView.getNumberOfUsers());
    }

    @Test
    public void shouldDecodeSingleLicenseStringContainingMultipleProducts() {
        //Given:
        LicenseDetails mockLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(LIC_STR_MULTI_PRODUCT)
                .setApplicationRoleWithLimit(ApplicationKeys.SERVICE_DESK, 11)
                .setApplicationRoleWithLimit(ApplicationKeys.SOFTWARE, 33)
                .build();
        LicenseDetails mockOtherLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(LIC_STR_OTHER)
                .setApplicationRoleWithLimit(LIC_APP_KEY_OTHER, 44)
                .build();

        //When:
        when(jiraLicenseManager.getLicense(LIC_STR_MULTI_PRODUCT)).thenReturn(mockLicenseDetails);
        when(jiraLicenseManager.getLicense(LIC_STR_OTHER)).thenReturn(mockOtherLicenseDetails);

        //Then:
        MultiProductLicenseDetails multiProductLicenseDetails = jiraLicenseHandler.decodeLicenseDetails(LIC_STR_MULTI_PRODUCT);
        assertEquals(2, multiProductLicenseDetails.getProductLicenses().size());
        multiProductLicenseDetails = jiraLicenseHandler.decodeLicenseDetails(LIC_STR_OTHER);
        assertEquals(1, multiProductLicenseDetails.getProductLicenses().size());
    }

    @Test
    public void shouldDecodeLicenseStringWithoutAnyProducts() {
        //Given:
        LicenseDetails mockLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(LIC_STR_MULTI_PRODUCT)
                .build();

        JiraLicense jiraLicense = mock(JiraLicense.class);
        when(jiraLicense.getProducts()).thenReturn(ImmutableList.<Product>of());
        when(mockLicenseDetails.getJiraLicense()).thenReturn(jiraLicense);

        //When:
        when(jiraLicenseManager.getLicense(LIC_STR_MULTI_PRODUCT)).thenReturn(mockLicenseDetails);

        //Then:
        MultiProductLicenseDetails multiProductLicenseDetails = jiraLicenseHandler.decodeLicenseDetails(LIC_STR_MULTI_PRODUCT);
        assertEquals(0, multiProductLicenseDetails.getProductLicenses().size());
    }

    @Test
    public void shouldNotAllowNullLicenseWhenDecoding() {
        expectedException.expect(NullPointerException.class);
        expectedException.expectMessage(containsString("License required"));
        jiraLicenseHandler.decodeLicenseDetails(LIC_STR_NULL);
    }

    @Test
    public void shouldAddProductLicense() throws Exception {
        LicenseDetails mockLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(LIC_STR_SERVICE_DESK)
                .setApplicationRoleWithLimit(ApplicationKeys.SERVICE_DESK, 11)
                .build();

        //When:
        when(jiraLicenseManager.getLicense(LIC_STR_SERVICE_DESK)).thenReturn(mockLicenseDetails);
        when(jiraLicenseManager.getLicenses()).thenReturn(Collections.singletonList(mockLicenseDetails));
        when(mockLicenseDetails.getLicenseType()).thenReturn(LicenseType.COMMERCIAL);
        jiraLicenseHandler.addProductLicense(PRODUCT_KEY_SERVICE_DESK, LIC_STR_SERVICE_DESK);

        //Then:
        verify(jiraLicenseManager, times(1)).setLicense(LIC_STR_SERVICE_DESK);
    }

    @Test
    public void shouldGetServerId() {
        //Given:
        String serverId = "B123";

        //When:
        when(jiraLicenseService.getServerId()).thenReturn(serverId);

        //Then:
        assertEquals(serverId, jiraLicenseHandler.getServerId());
    }

    @Test
    public void testThatGetSupportEntitlementNumberIsUnsupportedDuringRenaissance() {
        expectedException.expect(UnsupportedOperationException.class);
        expectedException.expectMessage(containsString("getSupportEntitlementNumber"));
        expectedException.expectMessage(containsString("BaseLicenseDetails"));
        jiraLicenseHandler.getSupportEntitlementNumber();
    }

    @Test (expected = UnsupportedOperationException.class)
    public void shouldFailToGetSupportEntitlementNumber() {
        jiraLicenseHandler.getSupportEntitlementNumber();
    }

    @Test
    public void shouldReturnEmptySetWhenNoSupportEntitlementNumbersAreReturnedFromLicenseManager() {
        //Given:
        when(jiraLicenseManager.getSupportEntitlementNumbers()).thenReturn(ImmutableSortedSet.<String>of());

        //when
        SortedSet<String> sens = jiraLicenseHandler.getAllSupportEntitlementNumbers();

        //Then:
        assertThat("2 SENs are present", sens.size(), is(0));
    }

    @Test
    public void shouldAllowMultipleLicense() {
        assertTrue(jiraLicenseHandler.hostAllowsMultipleLicenses());
    }

    @Test
    public void shouldAllowCustomProducts() {
        assertTrue(jiraLicenseHandler.hostAllowsCustomProducts());
    }
}