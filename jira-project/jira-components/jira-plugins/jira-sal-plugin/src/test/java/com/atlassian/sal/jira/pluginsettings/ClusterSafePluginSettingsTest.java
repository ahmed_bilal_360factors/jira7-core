package com.atlassian.sal.jira.pluginsettings;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

public class ClusterSafePluginSettingsTest {

    @Rule
    public MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private CacheInvalidatingPluginSettings cacheInvalidatingPluginSettings;

    @Mock
    private JiraPluginSettings defaultPluginSettings;

    private ClusterSafePluginSettings clusterSafePluginSettings;

    @Before
    public void setUp() {
        clusterSafePluginSettings = new ClusterSafePluginSettings(
                cacheInvalidatingPluginSettings,
                defaultPluginSettings
        );
    }

    @Test
    public void getShouldDelegateConsistentProperty() {
        final String consistentPropertyKey = "AO_60DB71_#";
        final String expectedValue = "1";

        when(cacheInvalidatingPluginSettings.get(consistentPropertyKey)).thenReturn(expectedValue);

        final Object actualValue = clusterSafePluginSettings.get(consistentPropertyKey);
        assertThat(actualValue, equalTo(expectedValue));
    }

    @Test
    public void getShouldDelegateEventuallyConsistentProperty() {
        final String propertyKey = "someProperty";
        final String expectedValue = "somePropertyValue";

        when(defaultPluginSettings.get(propertyKey)).thenReturn(expectedValue);

        final Object actualValue = clusterSafePluginSettings.get(propertyKey);
        assertThat(actualValue, equalTo(expectedValue));
    }

    @Test
    public void putShouldDelegate() {
        final String propertyKey = "someProperty";
        final String propertyValue = "somePropertyValue";

        when(defaultPluginSettings.put(propertyKey, propertyValue)).thenReturn(propertyValue);

        final Object actualValue = clusterSafePluginSettings.put(propertyKey, propertyValue);
        assertThat(actualValue, equalTo(propertyValue));
    }

    @Test
    public void removeShouldDelegate() {
        final String propertyKey = "someProperty";
        final String expectedValue = "somePropertyValue";

        when(defaultPluginSettings.remove(propertyKey)).thenReturn(expectedValue);

        final Object actualValue = clusterSafePluginSettings.remove(propertyKey);
        assertThat(actualValue, equalTo(expectedValue));
    }
}