package com.atlassian.sal.jira.license;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.application.api.ApplicationManager;
import com.atlassian.core.util.Clock;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.extras.api.Product;
import com.atlassian.extras.api.jira.JiraLicense;
import com.atlassian.jira.application.ApplicationConfigurationHelper;
import com.atlassian.jira.application.ApplicationRoleDefinitions;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.application.ApplicationRoleStore;
import com.atlassian.jira.bc.license.JiraLicenseService;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.license.LicenseDetailsFactoryImpl;
import com.atlassian.jira.license.LicensedApplications;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.util.ExternalLinkUtil;
import com.atlassian.sal.api.license.MultiProductLicenseDetails;
import com.atlassian.sal.api.license.ProductLicense;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static com.atlassian.extras.common.LicensePropertiesConstants.ACTIVE_FLAG;
import static com.atlassian.extras.common.LicensePropertiesConstants.ACTIVE_VALUE;
import static com.atlassian.extras.common.LicensePropertiesConstants.NAMESPACE_SEPARATOR;
import static com.atlassian.extras.common.LicensePropertiesConstants.UNLIMITED_USERS;
import static com.atlassian.jira.license.DefaultLicensedApplications.JIRA_PRODUCT_NAMESPACE;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_ABP;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_JIRA7;
import static com.google.common.collect.Iterables.transform;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests {@link com.atlassian.sal.jira.license.MultiProductLicenseDetailsImpl}.
 *
 * @see com.atlassian.sal.api.license.MultiProductLicenseDetails
 * @see com.atlassian.sal.api.license.ProductLicense
 * @since 7.0
 */
@RunWith(MockitoJUnitRunner.class)
public class MultiProductLicenseDetailsImplTest {
    @Rule
    public MockitoContainer mockitoContainer = MockitoMocksInContainer.rule(this);

    JiraLicenseHandler jiraLicenseHandler;

    @Mock
    JiraLicenseService licenseService;
    @Mock
    JiraLicenseManager licenseManager;
    @Mock
    I18nHelper.BeanFactory i18n;
    @Mock
    ApplicationProperties appProps;
    @Mock
    EventPublisher eventPublisher;
    @Mock
    JiraAuthenticationContext jiraAuthenticationContextProvider;
    @Mock
    @AvailableInContainer
    ApplicationRoleManager roleManager;
    @Mock
    private GlobalPermissionManager permissionManager;
    @Mock
    @AvailableInContainer
    private ApplicationRoleDefinitions roleDefinitions;
    @Mock
    @AvailableInContainer
    private ApplicationRoleStore applicationRoleStore;
    @Mock
    @AvailableInContainer
    private GroupManager groupManager;
    @Mock
    @AvailableInContainer
    private ApplicationManager applicationManager;
    @Mock
    @AvailableInContainer
    ApplicationConfigurationHelper appConfigHelper;

    LicenseDetails licenseForJira6x = createLicenseWithProperties(
            "jira.active", "true");

    LicenseDetails licenseForJiraSoftware = createLicenseWithProperties(
            "jira.product.jira-software.active", "true");

    LicenseDetails licenseForJiraSoftwareWithPlugins = createLicenseWithProperties(
            "jira.product.jira-software.active", "true",
            "com.acme.plugin1.active", "true",
            "com.acme.plugin2.active", "true");

    LicenseDetails licenseForServiceDeskPlugin = createLicenseWithProperties(
            "com.atlassian.servicedesk.active", "true");

    LicenseDetails licenseEla = createLicenseWithProperties(
            "jira.active", "true",
            "conf.active", "true",
            "hipchat.active", "true",
            "jira.product.jira-core.active", "true",
            "jira.product.jira-software.active", "true",
            "jira.product.jira-servicedesk.active", "true",
            "com.acme.plugin1.active", "true",
            "com.acme.plugin2.active", "true");

    @Before
    public void setUp() throws Exception {
        mockitoContainer.getMockComponentContainer().addMockComponent(ApplicationConfigurationHelper.class, appConfigHelper);
        when(appConfigHelper.validateApplicationForConfiguration(
                org.mockito.Matchers.any(ApplicationKey.class),
                org.mockito.Matchers.any(ApplicationUser.class))).thenReturn(Optional.<String>empty());
        jiraLicenseHandler = new JiraLicenseHandler(licenseService, licenseManager, i18n, appProps, eventPublisher,
                jiraAuthenticationContextProvider, permissionManager, groupManager);
    }

    @Test
    public void applicationsAreReturnedFromAnApplicationLicense() {
        when(licenseManager.getLicenses()).thenReturn(ImmutableList.of(licenseForJiraSoftware));

        assertThat(getProductLicenseKeys(), equalToSetOf("jira-software"));
        assertThat(getEmbeddedLicenseKeys(), equalToSetOf());
    }

    @Test
    public void jiraIsReturnedAsProductForJira6LicenseWithNoApplications() {
        when(licenseManager.getLicenses()).thenReturn(ImmutableList.of(licenseForJira6x));

        assertThat(getProductLicenseKeys(), equalToSetOf("jira"));
        assertThat(getEmbeddedLicenseKeys(), equalToSetOf());
    }

    @Test
    public void productAndNonProductLicensesAreReturnedForMixedLicense() {
        when(licenseManager.getLicenses()).thenReturn(ImmutableList.of(licenseForJiraSoftwareWithPlugins));

        assertThat(getProductLicenseKeys(), equalToSetOf("jira-software"));
        assertThat(getEmbeddedLicenseKeys(), equalToSetOf("com.acme.plugin1", "com.acme.plugin2"));
    }

    @Test
    public void productAndNonProductLicensesAreReturnedForEla() {
        when(licenseManager.getLicenses()).thenReturn(ImmutableList.of(licenseEla));

        assertThat(getProductLicenseKeys(), equalToSetOf("jira-core", "jira-software", "jira-servicedesk"));
        assertThat(getEmbeddedLicenseKeys(), equalToSetOf("com.acme.plugin1", "com.acme.plugin2", "conf", "hipchat"));
    }

    @Test
    public void productAndNonProductLicensesFromMultipleLicensesAreReturned() {
        when(licenseManager.getLicenses()).thenReturn(
                ImmutableList.of(licenseForServiceDeskPlugin, licenseForJiraSoftwareWithPlugins));

        assertThat(getProductLicenseKeys(), equalToSetOf("jira-software"));
        assertThat(getEmbeddedLicenseKeys(), equalToSetOf("com.acme.plugin1", "com.acme.plugin2", "com.atlassian.servicedesk"));
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    public void productKeyLookupsBothApplicationsAndPlugins() {
        when(licenseManager.getLicenses()).thenReturn(ImmutableList.of(licenseEla));
        MultiProductLicenseDetails license = jiraLicenseHandler.getAllProductLicenses().get(0);

        assertThat(license.getProductLicense("jira-software").getProductKey(), equalTo("jira-software"));
        assertThat(license.getProductLicense("com.acme.plugin2").getProductKey(), equalTo("com.acme.plugin2"));
        assertThat(license.getProductLicense("conf").getProductKey(), equalTo("conf"));

        assertThat(license.getProductLicense("jira.product.non-existent-application"), nullValue());
        assertThat(license.getProductLicense("com.non-existent.plugin"), nullValue());
    }

    @Test
    public void propertyLookupRespectsProductNamespace() {
        LicenseDetails licenseWithProperties = createLicenseWithProperties(
                "jira.product.jira-software.active", "true",
                "jira.product.jira-software.property1", "value1",
                "jira.product.jira-software.property2", "value2",
                "com.acme.plugin.active", "true",
                "com.acme.plugin.coyote", "roadrunner"
        );
        when(licenseManager.getLicenses()).thenReturn(ImmutableList.of(licenseWithProperties));
        MultiProductLicenseDetails license = jiraLicenseHandler.getAllProductLicenses().get(0);

        ProductLicense application = license.getProductLicense("jira-software");
        ProductLicense plugin = license.getProductLicense("com.acme.plugin");
        assert application != null && plugin != null;

        assertThat(application.getProperty("property1"), equalTo("value1"));
        assertThat(application.getProperty("property2"), equalTo("value2"));
        assertThat(plugin.getProperty("coyote"), equalTo("roadrunner"));
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    public void serviceDeskReturnsBothAsApplicationAndPluginWhenBothAreDefinedInLicense() {
        String licenseText = LICENSE_SERVICE_DESK_JIRA7.getLicenseString();

        LicenseDetailsFactoryImpl factory = new LicenseDetailsFactoryImpl(
                mock(ApplicationProperties.class), mock(ExternalLinkUtil.class), mock(BuildUtilsInfo.class),
                mock(I18nHelper.BeanFactory.class), mock(DateTimeFormatter.class), mock(Clock.class));

        LicenseDetails sdLicense = factory.getLicense(licenseText);
        when(licenseManager.getLicenses()).thenReturn(ImmutableList.of(sdLicense));

        MultiProductLicenseDetails license = jiraLicenseHandler.getAllProductLicenses().get(0);

        assertThat(getProductLicenseKeys(), equalToSetOf("jira-servicedesk"));
        assertThat(getEmbeddedLicenseKeys(), equalToSetOf("com.atlassian.servicedesk"));

        assertThat(license.getProductLicense("jira-servicedesk").getProperty("NumberOfUsers"), equalTo("11"));
        assertThat(license.getProductLicense("com.atlassian.servicedesk").getProperty("numRoleCount"), equalTo("10"));
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    public void serviceDeskReturnsApplicationPropertiesWhenLicenseContainsApplicationProperties() {
        String licenseText = LICENSE_SERVICE_DESK_ABP.getLicenseString();

        LicenseDetailsFactoryImpl factory = new LicenseDetailsFactoryImpl(
                mock(ApplicationProperties.class), mock(ExternalLinkUtil.class), mock(BuildUtilsInfo.class),
                mock(I18nHelper.BeanFactory.class), mock(DateTimeFormatter.class), mock(Clock.class));

        LicenseDetails sdLicense = factory.getLicense(licenseText);
        when(licenseManager.getLicenses()).thenReturn(ImmutableList.of(sdLicense));

        MultiProductLicenseDetails license = jiraLicenseHandler.getAllProductLicenses().get(0);

        assertThat(getProductLicenseKeys(), equalToSetOf("jira-servicedesk"));
        assertThat(getEmbeddedLicenseKeys(), equalToSetOf("com.atlassian.servicedesk"));

        // numRoleCount returns correct num users for both app key and plugin key, even though app key is NOT in license
        assertThat(license.getProductLicense("jira-servicedesk").getProperty("numRoleCount"), equalTo("100"));
        assertThat(license.getProductLicense("com.atlassian.servicedesk").getProperty("numRoleCount"), equalTo("100"));
    }

    // end of tests

    private static Matcher<Set<String>> equalToSetOf(String... productKeys) {
        return Matchers.<Set<String>>equalTo(ImmutableSet.copyOf(productKeys));
    }

    private Set<String> getProductLicenseKeys() {
        Set<ProductLicense> productLicenses = new HashSet<>();
        for (MultiProductLicenseDetails license : jiraLicenseHandler.getAllProductLicenses()) {
            productLicenses.addAll(license.getProductLicenses());
        }

        return ImmutableSet.copyOf(transform(productLicenses, toProductKey));
    }

    private Set<String> getEmbeddedLicenseKeys() {
        Set<ProductLicense> embeddedLicenses = new HashSet<>();
        for (MultiProductLicenseDetails license : jiraLicenseHandler.getAllProductLicenses()) {
            embeddedLicenses.addAll(license.getEmbeddedLicenses());
        }

        return ImmutableSet.copyOf(transform(embeddedLicenses, toProductKey));
    }

    private static final Function<ProductLicense, String> toProductKey = new Function<ProductLicense, String>() {
        @Override
        public String apply(final ProductLicense productLicense) {
            return productLicense.getProductKey();
        }
    };

    LicenseDetails createLicenseWithProperties(String... properties) {
        Map<String, String> props = new HashMap<>();
        for (int i = 1; i < properties.length; i++) {
            props.put(properties[i - 1], properties[i]);
        }

        LicenseDetails license = mock(LicenseDetails.class);
        JiraLicense underlying = mock(JiraLicense.class);

        when(license.getLicensedApplications()).thenAnswer(getApplicationsFrom(props));
        when(license.getJiraLicense()).thenReturn(underlying);

        when(underlying.getProperty(anyString())).thenAnswer(getPropertyValueFrom(props));
        when(underlying.getProducts()).thenAnswer(getProductsFrom(props));

        return license;
    }

    private Answer<Iterable<Product>> getProductsFrom(final Map<String, String> props) {
        return new Answer<Iterable<Product>>() {
            @Override
            public Iterable<Product> answer(InvocationOnMock invocation) throws Throwable {
                List<Product> products = new ArrayList<>();
                for (String property : props.keySet()) {
                    if (property.endsWith(DOT_ACTIVE) && ACTIVE_VALUE.equals(props.get(property))) {
                        String productKey = property.substring(0, property.lastIndexOf(DOT_ACTIVE));
                        products.add(Product.fromNamespace(productKey));
                    }
                }

                return products;
            }
        };
    }

    private Answer<LicensedApplications> getApplicationsFrom(final Map<String, String> props) {
        return new Answer<LicensedApplications>() {
            @Override
            public LicensedApplications answer(InvocationOnMock invocation) throws Throwable {
                LicensedApplications apps = mock(LicensedApplications.class);
                when(apps.getKeys()).thenAnswer(getApplicationKeysFrom(props));
                when(apps.getUserLimit(any(ApplicationKey.class))).thenReturn(UNLIMITED_USERS);

                return apps;
            }
        };
    }

    private static final String DOT_ACTIVE = NAMESPACE_SEPARATOR + ACTIVE_FLAG;

    private Answer<Set<ApplicationKey>> getApplicationKeysFrom(final Map<String, String> props) {
        return new Answer<Set<ApplicationKey>>() {
            @Override
            public Set<ApplicationKey> answer(InvocationOnMock invocation) throws Throwable {
                Set<ApplicationKey> appKeys = new HashSet<>();
                for (String prop : props.keySet()) {
                    if (prop.startsWith(JIRA_PRODUCT_NAMESPACE) && prop.endsWith(DOT_ACTIVE)) {
                        String productKey = prop.substring(JIRA_PRODUCT_NAMESPACE.length(), prop.lastIndexOf(DOT_ACTIVE));
                        appKeys.add(ApplicationKey.valueOf(productKey));
                    }
                }

                return appKeys;
            }
        };
    }

    private Answer<String> getPropertyValueFrom(final Map<String, String> props) {
        return new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation) throws Throwable {
                return props.get(invocation.getArguments()[0].toString());
            }
        };
    }
}
