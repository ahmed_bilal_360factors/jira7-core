(function(bluebird){
    "use strict";

    define('bluebird/Promise', [], function() {
        return bluebird;
    });

}(Promise.noConflict()));
