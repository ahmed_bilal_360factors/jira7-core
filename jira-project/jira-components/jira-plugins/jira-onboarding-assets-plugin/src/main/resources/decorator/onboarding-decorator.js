(function() {
    var KeyboardShortcutToggle = require('jira/ajs/keyboardshortcut/keyboard-shortcut-toggle');
    var $ = require('jquery');

    $(document).ready(function() {
        KeyboardShortcutToggle.disable();

        $('body#jira').addClass("disabled-navigation");
        var logoHref = $('#logo a').attr("href");
        if (logoHref) {
            $('#logo a').removeAttr('href').attr("data-href", logoHref);
        }
    });
})();
