define("jira/onboarding/jira-first-use-flow", ['require'], function (require) {
    "use strict";

    var NextStepSequence = require('jira/onboarding/next-step-sequence');
    var ChooseLanguageSequence = require('jira/onboarding/choose-language-sequence');
    var AvatarPickerSequence = require('jira/onboarding/avatar-picker-sequence');
    var Browser = require('jira/util/browser');
    var CyoaSequence = require('jira/onboarding/choose-your-own-adventure');
    var AjsContextPath = require('wrm/context-path');
    var WrmData = require('wrm/data');

    var jQuery = require('jquery');

    function goToDashboard() {
        Browser.reloadViaWindowLocation(AjsContextPath() + "/secure/Dashboard.jspa");
    }

    /**
     * @name FirstUseFlowFactory
     *
     * @param {Function} complete function to tell the server that the flow is complete.
     * @return {FirstUseFlow}
     */
    var languageData;
    var avatarData;
    var cyoaData;
    var nextStepData;
    return function (complete) {
        languageData = languageData || WrmData.claim("com.atlassian.jira.jira-onboarding-assets-plugin:choose-language-sequence.data");
        avatarData = avatarData || WrmData.claim("com.atlassian.jira.jira-onboarding-assets-plugin:avatar-picker-sequence.data");
        cyoaData = cyoaData || WrmData.claim("com.atlassian.jira.jira-onboarding-assets-plugin:choose-your-own-adventure-sequence.data");
        nextStepData = nextStepData || WrmData.claim('com.atlassian.jira.jira-onboarding-assets-plugin:next-step-sequence.data');

        var cyoaSequence = new CyoaSequence(jQuery.extend({}, cyoaData, {
            complete: complete
        }));

        var cyoaConfig = {
            instance: cyoaSequence,
            resolve: function (args) {
                switch (args) {
                    case CyoaSequence.EVENTS.CLICKED_SAMPLE_DATA:
                        complete();
                        return "cyoa:return";
                    case CyoaSequence.EVENTS.CLICKED_EMPTY_PROJECT:
                        complete();
                        return "cyoa:return";
                    case CyoaSequence.EVENTS.CLICKED_IMPORT:
                        complete().then(cyoaSequence.handleImporter.bind(cyoaSequence), cyoaSequence.handleImporter.bind(cyoaSequence));
                        break;
                    default:
                        complete().then(goToDashboard, goToDashboard);
                }
            },
            reject: "nextStep"
        };

        //Note: If the key or sequence keys are changed the onboarding-analytics.json file must be updated.
        return {
            key: "jiraFirstUseFlow",
            start: "chooseLanguage",
            sequences: {
                "chooseLanguage": {
                    instance: new ChooseLanguageSequence(languageData),
                    resolve: "avatar",
                    reject: "avatar"
                },
                "avatar": {
                    instance: new AvatarPickerSequence(avatarData),
                    resolve: "cyoa",
                    reject: "cyoa"
                },
                "cyoa": cyoaConfig,
                "cyoa:return": cyoaConfig,
                "nextStep": {
                    instance: new NextStepSequence(nextStepData),
                    resolve: complete,
                    reject: function () {
                        // if no permissions for the next step
                        complete().then(goToDashboard, goToDashboard);
                    }
                }
            }
        };
    };
});
