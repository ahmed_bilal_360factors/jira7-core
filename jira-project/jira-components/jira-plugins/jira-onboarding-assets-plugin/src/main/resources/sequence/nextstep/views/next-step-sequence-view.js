define('jira/onboarding/next-step-sequence-view', [
    'jquery',
    'underscore',
    'backbone'
], function ($, _, Backbone) {
    'use strict';

    /**
     * @name NextStepSequenceViewOptions
     * @global
     *
     * @property {Step[]} steps
     */

    /**
     * @name Step
     * @global
     *
     * Represents a step that a user can complete with icon and url
     *
     * @property {String} key unique for step
     * @property {String} url to go to on click
     * @property {String} label to display for step
     * @property {String} icon image src url for step
     */

    /**
     * @module NextStepSequenceView
     * @global
     *
     * @event NextStepSequenceView#stepSelected
     * @param {Step} step that was selected.
     */
    return Backbone.View.extend({

        templates: JIRA.Onboarding.Sequence.NextStep,

        events: {
            'click .next-step': 'selectStep'
        },

        /**
         *
         * @param {NextStepSequenceViewOptions} options
         */
        initialize: function (options) {
            options = options || {};
            this._validateOptions(options);

            this.steps = options.steps;
        },

        /**
         * @param {NextStepSequenceViewOptions} options
         * @private
         */
        _validateOptions: function (options) {
            if (!options.steps || !options.steps.length) {
                throw new Error('Should have passed steps with a length > 0');
            }
        },

        render: function() {
            this.$el.html(this.templates.render({
                steps: this.steps
            }));
        },

        /**
         * @fires NextStepSequenceView#stepSelected
         * @param {jQuery.Event} event for selection
         */
        selectStep: function(event) {
            event.preventDefault();
            var stepElement = $(event.currentTarget);

            var step = this.findStep(stepElement.attr('data-step-key'));

            this.trigger('stepSelected', step);
        },

        /**
         * @param {String} stepKey
         * @returns {Step}
         */
        findStep: function(stepKey) {
            return _.find(this.steps, function(step) {
                return step.key === stepKey;
            });
        }
    });
});
