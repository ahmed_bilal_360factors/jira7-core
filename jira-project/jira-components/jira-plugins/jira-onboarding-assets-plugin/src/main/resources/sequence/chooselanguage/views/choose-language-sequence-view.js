/* global define*/
define('jira/onboarding/choose-language-sequence-view', ['require'], function(require) {
    'use strict';

    var Backbone = require('backbone');
    var _ = require('underscore');
    var jQuery = require('jquery');
    var Model = require('jira/onboarding/choose-language-sequence-model');
    var Templates = JIRA.Onboarding.Sequence.ChooseLanguage;

    var $pageTitleEl = jQuery('title', 'head');

    /**
     * Render the UI for choosing a language.
     *
     * Needs to be provided with a model containing all available languages and translations
     * to display to the user.
     *
     * @class
     * @extends Backbone.View
     * @exports jira/onboarding/choose-language-sequence-view
     * @param {Object} options
     * @param {@link module:jira/onboarding/choose-language-sequence-model} options.model The model with language choices.
     */
    return Backbone.View.extend({
        model: Model,

        initialize: function() {
            this.listenTo(this.model, 'change:selected', this.render);
            this.listenTo(this.model, 'request', this._saving);
        },

        events: {
            'change input[name=locale]': '_localeChanged',
            'click #skip': '_skip',
            'click #next': '_next'
        },

        render: function() {
            var data = this.model.toJSON();
            data.languages = _.map(data.languages, function(lang) {
                return {
                    id: 'locale_' + lang.value,
                    labelText: lang.name,
                    value: lang.value,
                    isChecked: lang.isSelected
                };
            });
            this._setPageTitle(data.titleText);
            this.$el.html(Templates.render(data));

            // Focus checked language
            var checkedLanguage = _.find(data.languages, function(lang) {
                return lang.isChecked;
            });
            if (checkedLanguage && checkedLanguage.id) {
                jQuery('#' + checkedLanguage.id).focus();
            }
            return this;
        },

        _setPageTitle: function(text) {
            if (!text) {
                return;
            }
            var oldTitle = $pageTitleEl.text();
            var title = oldTitle.split(' - ');
            title[0] = text;
            $pageTitleEl.text(title.join(' - '));
        },

        _saving: function onModelSave(model, xhr) {
            var $buttons = this.$('#next, #skip');
            $buttons.attr('disabled', 'disabled').attr('aria-disabled', true);
            xhr.always(function() {
                $buttons.removeAttr('disabled').attr('aria-disabled', false);
            });
        },

        _localeChanged: function(e) {
            var el = e.target;
            var value = el.value;
            this.model.set('selected', value);
        },

        _skip: function onSkip(e) {
            e.preventDefault();
            this.trigger('skip');
        },

        _next: function onNext(e) {
            e.preventDefault();
            this.trigger('done');
        }
    });
});
