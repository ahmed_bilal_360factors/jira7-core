AJS.test.require('com.atlassian.jira.jira-onboarding-assets-plugin:nutshell-component', function() {
    'use strict';

    var NutshellDialogView = require('jira/onboarding/nutshell-sequence/views/nutshell-dialog-view');
    var SequenceModel = require('jira/onboarding/nutshell-sequence/models/sequence-model');
    var SequenceStep = require('jira/onboarding/nutshell-sequence/models/sequence-step');

    module('onboarding/nutshell-dialog-view', {
        setup: function () {
            this.dummyModel = new SequenceModel({
                steps: [
                    // Projects
                    new SequenceStep({
                        text: 'text.1',
                        trigger: '.trigger-1',
                        id: 'id1'
                    }),
                    new SequenceStep({
                        text: 'text.2',
                        trigger: '.trigger-2',
                        id: 'id2'
                    }),
                    new SequenceStep({
                        text: 'text.3',
                        trigger: '.trigger-3',
                        id: 'id3'
                    })
                ]
            });
        }
    });

    test('Should reject an invalid model', function() {
        try {
            new NutshellDialogView({
                model: null,
                id: 'testDialog'
            });
            ok(false, 'Should have thrown an error on creation with no model');
        } catch (e) {
            ok(true);
        }
    });

    asyncTest('Should render a proper InlineDialog2', function() {
        var dialog = new NutshellDialogView({
            model: this.dummyModel,
            id: 'testDialog'
        });
        setTimeout(function(){
            equal(dialog.$el.size(), 1, 'There must be only one nutshellDialog');
            equal(_getDialogText(dialog.$el), 'text.1');
            equal(_getDialogButton(dialog.$el, 'button.next').size(), 1, 'Next button should be present');
            equal(_getDialogButton(dialog.$el, 'button.prev').size(), 0, 'Back button should not be present');
            start();
        }.bind(this), 0);
    });

    function asyncTrain (context){
        var train = [];

        var self = {};

        self.next = function (func) {
            train.push(func);
            return self;
        };

        self.run = function () {
            if (train.length) {
                var current = train.shift();
                setTimeout(function(){
                    current.apply(context);
                    self.run();
                }, 0);
            } else {
                start();
            }
        };

        return self;
    }

    asyncTest('Should successfully navigate between steps 1 to 3', function() {
        var dialog = new NutshellDialogView({
            model: this.dummyModel,
            id: 'testDialog'
        });

        asyncTrain(this)
            .next(function(){
                equal(_getCurrentStep(dialog.$el), 1, 'Step 1');
                equal(_getTotalSteps(dialog.$el), 3, 'of 3');
            })
            .next(function(){
                _getDialogButton(dialog.$el, 'button.next').click();
            })
            .next(function(){
                equal(_getCurrentStep(dialog.$el), 2, 'Second step should be selected');
                equal(_getDialogButton(dialog.$el, 'button.next').size(), 1, 'Next button should be present');
                equal(_getDialogButton(dialog.$el, 'button.prev').size(), 1, 'Back button should be present as well');
            })
            .next(function(){
                _getDialogButton(dialog.$el, 'button.next').click();
            })
            .next(function(){
                equal(_getCurrentStep(dialog.$el), 3, 'Third step should be selected');
                equal(_getDialogButton(dialog.$el, 'button.next').text(), 'common.words.done', 'Next button should have been renamed to DONE');
                equal(_getDialogButton(dialog.$el, 'button.prev').size(), 1, 'Back button should be present as well');
            })
            .next(function(){
                _getDialogButton(dialog.$el, 'button.prev').click();
            })
            .next(function(){
                equal(_getCurrentStep(dialog.$el), 2, 'Second step should be selected');
                equal(_getDialogButton(dialog.$el, 'button.next').text(), 'common.forms.next', 'Next button should have been renamed to NEXT');
                equal(_getDialogButton(dialog.$el, 'button.prev').size(), 1, 'Back button should stay in place');
            })
            .next(function(){
                _getDialogButton(dialog.$el, 'button.prev').click();
            })
            .next(function(){
                equal(_getCurrentStep(dialog.$el), 1, 'First step should be selected again');
                equal(_getDialogButton(dialog.$el, 'button.prev').size(), 0, 'Back button should not be present');
            })
            .run();
    });

    function _getDialogText($el) {
        return $el.find('.aui-inline-dialog-contents > p').text();
    }
    function _getDialogButton($el, buttonSelector) {
        return $el.find('.aui-inline-dialog-contents > ' + buttonSelector);
    }
    function _getCurrentStep($el) {
        return _getStepValue($el, 0);
    }
    function _getTotalSteps($el) {
        return _getStepValue($el, 1);
    }
    function _getStepValue($el, position) {
        return parseInt($el.find('.aui-inline-dialog-contents > span').text().split('/')[position], 10);
    }
});
