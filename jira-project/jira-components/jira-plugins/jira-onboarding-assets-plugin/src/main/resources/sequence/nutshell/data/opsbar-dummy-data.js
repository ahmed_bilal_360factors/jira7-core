define("jira/onboarding/nutshell-sequence/data/opsbar-dummy-data", ["jira/util/formatter"], function(formatter) {
    return {
        "operations": {
            "linkGroups": [{
                "id": "view.issue.opsbar",
                "groups": [{
                    "id": "edit-issue_container",
                    "links": [{
                        "id": "edit-issue",
                        "styleClass": "issueaction-edit-issue",
                        "iconClass": "aui-icon aui-icon-small aui-iconfont-edit",
                        "label": formatter.I18n.getText("common.forms.edit"),
                        "href": "",
                    }],
                    "groups": []
                }, {
                    "id": "comment-issue_container",
                    "links": [{
                        "id": "comment-issue",
                        "styleClass": "issueaction-comment-issue add-issue-comment",
                        "iconClass": "aui-icon aui-icon-small aui-iconfont-comment icon-comment",
                        "label": formatter.I18n.getText("comment.add.label"),
                        "href": "",
                    }],
                    "groups": []
                }, {
                    "id": "opsbar-transitions",
                    "links": [{
                        "id": "action-stop-progress",
                        "styleClass": "issueaction-workflow-transition",
                        "label": formatter.I18n.getText("stopprogress.title"),
                        "href": "",
                    }, {
                        "id": "action-done",
                        "styleClass": "issueaction-workflow-transition",
                        "label": formatter.I18n.getText("common.words.done"),
                        "href": "",
                    }],
                    "groups": []
                }]
            }]
        }
    };
});
