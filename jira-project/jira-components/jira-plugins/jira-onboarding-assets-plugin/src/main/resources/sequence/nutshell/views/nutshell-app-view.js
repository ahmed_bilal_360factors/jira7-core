/* global AJS */
/* global JIRA.WorkflowDesigner */
define("jira/onboarding/nutshell-sequence/views/nutshell-app-view", ["require"], function (require) {
    "use strict";

    var Backbone = require("backbone");
    var $ = require("jquery");
    var _ = require("underscore");

    var ProjectKeyGenerator = require("jira/project/project-key-generator");
    var SequenceStep = require("jira/onboarding/nutshell-sequence/models/sequence-step");
    var opsbarData = require("jira/onboarding/nutshell-sequence/data/opsbar-dummy-data");

    var formatter = require('jira/util/formatter');

    var keygen = new ProjectKeyGenerator();
    var ProjectFactory = {
        data: null,
        setData: function (data) {
            if (!data.defaultProjectAvatarUrl) {
                throw new Error("Should have supplied the default project avatar url");
            }
            ProjectFactory.data = data;
        },
        createProject: function (name) {
            var data = ProjectFactory.data;
            var projectTypeAvatar;
            if (data.projectType && data.projectTypeAvatars) {
                var projectType = ProjectFactory.data.projectType;
                projectTypeAvatar = ProjectFactory.data.projectTypeAvatars[projectType];
            }
            return {
                name: name,
                key: keygen.generateKey(name),
                avatarUrl: data.defaultProjectAvatarUrl,
                typeAvatarBase64: projectTypeAvatar
            };
        }
    };

    function _renderDummyProjects() {
        return $(JIRA.Onboarding.sequence.nutshell.projects({
            projects: [
                ProjectFactory.createProject(formatter.I18n.getText("onboarding.nutshell.dummy.project.1")),
                ProjectFactory.createProject(formatter.I18n.getText("onboarding.nutshell.dummy.project.2")),
                ProjectFactory.createProject(formatter.I18n.getText("onboarding.nutshell.dummy.project.3"))
            ]
        }));
    }

    function _renderDummyIssue(options, issueOverrides) {
        var project = ProjectFactory.createProject(formatter.I18n.getText("onboarding.nutshell.dummy.project.1"));
        var issueId = 1;

        return $(JIRA.Onboarding.sequence.nutshell.issue(_.extend({
                issue: _.extend({
                    project: project,
                    key: formatter.formatText("{0}-{1}", project.key, issueId),
                    summary: formatter.I18n.getText("onboarding.nutshell.dummy.issue.summary"),
                    assignee: {
                        name: ProjectFactory.data.userFullname,

                        //Gets it from the top right icon as a data provider would be a stale image if it was
                        //changed from the avatar picker.  The avatar picker updates the icon when one is chosen,
                        //so it should always be correct
                        avatarUrl: $("#user-options").find(".aui-avatar-inner > img").attr('src')
                    },
                    statusText: formatter.I18n.getText("jira.translation.status.inprogress.name"),
                    statusType: "current"
                }, issueOverrides),
                toolbarData: opsbarData
            }, options)))
            .on("click", "a", false) // don't allow navigation by clicking on anchors
            .find("#action-done").parent()
            .toggleClass("active", !!options && !!options.isDone)
            .end().end();
    }

    function _renderDummyWorkflow(options) {
        return $(JIRA.Onboarding.sequence.nutshell.workflow(_.extend({
            statuses: {
                toDo: formatter.I18n.getText("common.statuscategory.new").toUpperCase(),
                inProgress: formatter.I18n.getText("jira.translation.status.inprogress.name").toUpperCase(),
                done: formatter.I18n.getText("common.statuscategory.done").toUpperCase()
            },
            transitions: {
                create: formatter.I18n.getText("common.words.create"),
                startProgress: formatter.I18n.getText("startprogress.title"),
                stopProgress: formatter.I18n.getText("stopprogress.title"),
                done: formatter.I18n.getText("common.words.done"),
                reopen: formatter.I18n.getText("issue.operations.reopen")
            },
            isDone: false
        }, options)));
    }

    return Backbone.View.extend({
        initialize: function (options) {
            ProjectFactory.setData(options.data);

            this.subtitle = this.$el.find(".subtitle");
            this.projectsContainer = this.$el.find(".projects");
            this.issueContainer = this.$el.find(".issue");
            this.workflowContainer = this.$el.find(".workflow");

            this.listenTo(this.model, "step", this.render);
            if (_.isObject(this.model.current())) {
                this.render();
            }
        },
        render: function () {
            var currentStep = this.model.current();

            switch (currentStep.id) {
            case SequenceStep.PROJECTS:
            case SequenceStep.PROJECTS_ISSUES:
                this.subtitle.html(formatter.I18n.getText("onboarding.nutshell.subtitle"));
                this._renderSection(this.projectsContainer, _renderDummyProjects());
                this._destroySection(this.issueContainer);
                this._destroySection(this.workflowContainer);
                break;
            case SequenceStep.ISSUE:
            case SequenceStep.ISSUE_SUMMARY:
            case SequenceStep.ISSUE_KEY:
                this.subtitle.html(formatter.I18n.getText("onboarding.nutshell.subtitle.issues"));
                this._destroySection(this.projectsContainer);
                this._renderSection(this.issueContainer, _renderDummyIssue());
                this._destroySection(this.workflowContainer);
                break;
            case SequenceStep.ISSUE_ASSIGNEE:
                this.subtitle.html(formatter.I18n.getText("onboarding.nutshell.subtitle.assignee"));
                this._destroySection(this.projectsContainer);
                this._renderSection(this.issueContainer, _renderDummyIssue({
                    hasAssignee: true
                }));
                this._destroySection(this.workflowContainer);
                break;
            case SequenceStep.ISSUE_STATUS:
                this.subtitle.html(formatter.I18n.getText("onboarding.nutshell.subtitle.status"));
                this._destroySection(this.projectsContainer);
                this._renderSection(this.issueContainer, _renderDummyIssue({
                    hasAssignee: true,
                    hasStatus: true
                }));
                this._destroySection(this.workflowContainer);
                break;
            case SequenceStep.WORKFLOW:
                this.subtitle.html(formatter.I18n.getText("onboarding.nutshell.subtitle.workflow"));
                this._destroySection(this.projectsContainer);
                this._renderSection(this.issueContainer, _renderDummyIssue({
                    hasAssignee: true,
                    hasStatus: true
                }));
                this._renderSection(this.workflowContainer, _renderDummyWorkflow());
                break;
            case SequenceStep.WORKFLOW_BUTTONS:
                this._destroySection(this.projectsContainer);
                this._renderSection(this.issueContainer, _renderDummyIssue({
                    hasAssignee: true,
                    hasStatus: true,
                    hasButtons: true
                }));
                this._renderSection(this.workflowContainer, _renderDummyWorkflow());
                break;
            case SequenceStep.WORKFLOW_CLICK:
                this.subtitle.html(formatter.I18n.getText("onboarding.nutshell.subtitle.workflow"));
                this._destroySection(this.projectsContainer);
                this._renderSection(this.issueContainer, _renderDummyIssue({
                    hasAssignee: true,
                    hasStatus: true,
                    hasButtons: true,
                    isDone: true
                }));
                this._renderSection(this.workflowContainer, _renderDummyWorkflow());
                break;
            case SequenceStep.WORKFLOW_DONE:
                this.subtitle.html(formatter.I18n.getText("onboarding.nutshell.subtitle.status2"));
                this._destroySection(this.projectsContainer);
                this._renderSection(this.issueContainer, _renderDummyIssue({
                    hasAssignee: true,
                    hasStatus: true,
                    hasButtons: true
                }, {
                    statusText: formatter.I18n.getText("common.statuscategory.done"),
                    statusType: "success"
                }));
                this._renderSection(this.workflowContainer, _renderDummyWorkflow({
                    isDone: true
                }));
                break;
            case SequenceStep.ALL_DONE:
                this.subtitle.html(formatter.I18n.getText("onboarding.nutshell.subtitle.done"));
                this._destroySection(this.projectsContainer);
                this._renderSection(this.issueContainer, _renderDummyIssue({
                    hasAssignee: true,
                    hasStatus: true,
                    hasButtons: true
                }, {
                    statusText: formatter.I18n.getText("common.statuscategory.done"),
                    statusType: "success"
                }));
                this._renderSection(this.workflowContainer, _renderDummyWorkflow({
                    isDone: true
                }));
                break;
            }

            return this;
        },
        _renderSection: function (container, contents) {
            $(container).html(contents).show();
        },
        _destroySection: function (container) {
            $(container).hide().empty();
        }
    });
});
