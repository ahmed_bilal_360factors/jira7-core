/* global require, sinon, module, test, ok, equal */
AJS.test.require("com.atlassian.jira.jira-onboarding-assets-plugin:nutshell-component", function () {
    "use strict";

    var _ = require("underscore");

    var SequenceModel = require("jira/onboarding/nutshell-sequence/models/sequence-model");
    var SequenceStep = require("jira/onboarding/nutshell-sequence/models/sequence-step");

    var STEPS = [step(), step(), step(), step(), step()];

    module("onboarding/nutshell-sequence/models/sequence-model");

    test("initialization with default values", function () {
        var model = createModel(sinon.spy());
        equal(model.current(), STEPS[0], "current() is first item");
        ok(model.hasNext(), "hasNext()");
        ok(!model.hasPrev(), "hasPrev()");
    });

    test("initialize with index within bounds", function () {
        var index = Math.floor(STEPS.length / 2);
        var model = createModel(sinon.spy(), index);
        equal(model.current(), STEPS[index], "current item");
        ok(model.hasNext(), "2 hasNext()");
        ok(model.hasPrev(), "2 hasPrev()");
    });

    test("initialize with index outside bounds", function () {
        var maxIndex = STEPS.length - 1;
        var stepCallback = sinon.spy();
        var model = createModel(stepCallback, maxIndex + 1);
        assertBounds(model, maxIndex, "upper bounds");
        var minIndex = 0;
        model = createModel(stepCallback, minIndex - 1);
        assertBounds(model, minIndex, "lower bounds");
    });

    test("hasPrev()/hasNext()", function () {
        var model = createModel(sinon.spy(), 0);
        ok(model.hasNext(), "first hasNext()");
        ok(!model.hasPrev(), "first !hasPrev()");
        model = createModel(sinon.spy(), STEPS.length - 1);
        ok(!model.hasNext(), "last hasNext()");
        ok(model.hasPrev(), "last hasPrev()");
    });

    test("navigate forwards within bounds", function () {
        var index = 0;
        var stepCallback = sinon.spy();
        var model = createModel(stepCallback, index);
        model.next();
        index += 1;
        equal(model.current(), STEPS[index], "current item");
        sinon.assert.calledWith(stepCallback, index);
    });

    test("navigate backwards within bounds", function () {
        var index = STEPS.length - 1;
        var stepCallback = sinon.spy();
        var model = createModel(stepCallback, index);
        model.prev();
        index -= 1;
        equal(model.current(), STEPS[index], "current item");
        sinon.assert.calledWith(stepCallback, index);
    });

    test("navigate forwards outside bounds", function () {
        var maxIndex = STEPS.length - 1;
        var stepCallback = sinon.spy();
        var model = createModel(stepCallback, maxIndex - 1);
        model.next();
        model.next();
        assertBounds(model, maxIndex, stepCallback, maxIndex + 1, "next() upper bounds");
    });

    test("navigate backwards outside bounds", function () {
        var minIndex = 0;
        var stepCallback = sinon.spy();
        var model = createModel(stepCallback, minIndex + 1);
        model.prev();
        model.prev();
        assertBounds(model, minIndex, stepCallback, minIndex - 1, "prev() lower bounds");
    });

    test("reset()", function () {
        var startFrom = 0;
        var stepCallback = sinon.spy();
        var model = createModel(stepCallback, startFrom);
        model.next();
        model.reset();
        sinon.assert.calledWith(stepCallback, startFrom);
    });

    test("done event", function () {
        var startFrom = STEPS.length - 2;
        var doneCallback = sinon.spy();
        var model = createModel(sinon.spy(), startFrom);
        model.on("done", doneCallback);
        model.next();
        model.next();
        sinon.assert.called(doneCallback);
    });

    function createModel(spy, startFrom) {
        var model;
        if (startFrom == null) {
            model = new SequenceModel({
                steps: STEPS
            });
        } else {
            model = new SequenceModel({
                startFrom: startFrom,
                steps: STEPS
            });
        }
        model.on("step", spy);

        return model;
    }

    function assertBounds(model, realIndex, spy, outsideIndex, tag) {
        if (arguments.length === 5) {
            sinon.assert.calledWith(spy, realIndex);
            sinon.assert.neverCalledWith(spy, outsideIndex);
        } else {
            tag = arguments[2];
        }
        equal(model.current(), STEPS[realIndex], tag + ": current item");
    }

    var stepIndex = 0;

    function step() {
        return new SequenceStep({
            text: stepIndex++
        });
    }
});
