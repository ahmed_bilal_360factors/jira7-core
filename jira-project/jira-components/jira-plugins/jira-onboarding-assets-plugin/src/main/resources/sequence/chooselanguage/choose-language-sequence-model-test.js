/* global require, sinon, module, test, ok, equal */
AJS.test.require("com.atlassian.jira.jira-onboarding-assets-plugin:choose-language-component", function() {
    "use strict";

    var Backbone = require("backbone");
    var _ = require("underscore");

    function fakeLanguageData(number) {
        var languages = {};
        _.times(number, function (i) {
            var n = i + 1;
            var key = "en_" + n;
            languages[key] = {
                name: "English #" + n,
                value: key
            };
        });
        return { languages: languages };
    }

    function fakeLoggedInUser() {
        return {
            fullName: function() {
                return "A fake user";
            }
        };
    }

    module("ChooseLanguageSequenceModel", {
        setup: function() {
            this.context = AJS.test.mockableModuleContext();
            this.context.mock("jira/util/users/logged-in-user", fakeLoggedInUser());
            this.Model = this.context.require("jira/onboarding/choose-language-sequence-model");
        }
    });

    test("has no default initial value", function() {
        var model = new this.Model();
        equal(model.get("selected"), undefined, "should have no default value");
    });

    test("is never considered 'new'", function() {
        var model = new this.Model();
        equal(model.isNew(), false, "should not be considered new, since a user always has a language preference");
    });

    test("should be provided with a user preference key", function() {
        var model = new this.Model(undefined, { userPrefKey: "something.or.other" });
        var url = parseUri(model.url());
        ok(url.query.indexOf("something.or.other") > -1, "preference key should be present in the URL");
    });

    test("#save will 'update' the user preference", function() {
        var model = new this.Model({ selected: "en_1" });
        var sync = this.stub(Backbone, "sync");
        model.set("selected", "en_3");
        model.save();
        ok(sync.calledOnce, "should call sync");
        ok(sync.calledWith("update", model, sinon.match.object), "should make an update call and pass the model");

        var datum = sync.getCall(0).args[2];
        equal(datum.data, "en_3", "should be passing the raw selected value as the data for the sync");
    });

    test("#toJSON returns an array of available languages, indicating the currently selected one", function () {
        var langs = fakeLanguageData(4);
        var model = new this.Model({ selected: "en_1" }, { languages: langs.languages });
        var result;

        // Get the initial json data
        result = model.toJSON();
        ok(result.languages instanceof Array, "should have an array of languages");
        equal(result.languages.length, 4, "should be four languages");
        equal(result.languages[0].value, "en_1");
        equal(result.languages[0].isSelected, true);

        // Change the selected option, check new response
        model.set("selected", "en_4");
        result = model.toJSON();
        equal(result.languages[0].isSelected, false);
        equal(result.languages[3].isSelected, true);
    });
});
