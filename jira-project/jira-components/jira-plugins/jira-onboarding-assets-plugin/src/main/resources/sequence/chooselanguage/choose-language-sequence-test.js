/* global require, sinon, module, test, ok, equal, deepEqual */
AJS.test.require("com.atlassian.jira.jira-onboarding-assets-plugin:choose-language-component", function() {
    "use strict";

    var $ = require("jquery");
    var _ = require("underscore");
    var Backbone = require("backbone");
    var Model = require("jira/onboarding/choose-language-sequence-model");
    var Browser;
    var LocalStorage;

    function fakeLanguageData(number) {
        var languages = {};
        var translations = {};
        _.times(number, function(i) {
            var n = i + 1;
            var key = "en_" + n;
            languages[key] = "English #" + n;
            translations[key] = {
                "title": "Welcome to JIRA " + n + "!",
                "paragraph": "Something something " + n + " something..."
            };
        });
        return { languages: languages, translations: translations };
    }

    function fakeLocalStorage(sandbox) {
        return LocalStorage = {
            setItem: sandbox.stub(),
            getItem: sandbox.stub(),
            removeItem: sandbox.stub(),
            clear: sandbox.stub()
        };
    }

    function fakeBrowser(sandbox) {
        return Browser = {
            reloadViaWindowLocation: sandbox.spy()
        };
    }

    module("ChooseLanguageSequence", {
        setup: function () {
            this.sandbox = sinon.sandbox.create();
            this.clock = this.sandbox.useFakeTimers();
            this.server = this.sandbox.useFakeServer();
            this.sandbox.stub(Model.prototype, "url").returns("/rest/api/fake/mypreferences");
            this.sandbox.stub(Model.prototype, "toJSON");
            var fakeModel = this.fakeModel = new Model();
            var FakeModelClass = this.FakeModelClass = sinon.stub().returns(fakeModel);
            var View = Backbone.View.extend({render: this.sandbox.stub()});
            var fakeView = this.fakeView = new View();
            var FakeViewClass = this.FakeViewClass = sinon.stub().returns(fakeView);
            this.context = AJS.test.mockableModuleContext();
            this.context.mock("jira/onboarding/choose-language-sequence-model", FakeModelClass);
            this.context.mock("jira/onboarding/choose-language-sequence-view", FakeViewClass);
            this.context.mock("jira/data/local-storage", fakeLocalStorage(this.sandbox));
            this.context.mock("jira/util/browser", fakeBrowser(this.sandbox));
            this.analytics = {
                pushEvent: this.sandbox.spy()
            };
            this.$el = $("<div></div>");
        },
        teardown: function () {
            this.sandbox.restore();
        }
    });

    test("Can be constructed", 1, function () {
        var ChooseLanguageSequence = this.context.require("jira/onboarding/choose-language-sequence");

        try {
            new ChooseLanguageSequence();
            ok(true, "Expected no errors to be thrown when no options provided");
        }
        catch (e) {
            equal(e.message, false, "Expected no errors raised, but got one.");
        }
    });

    test("#init rejects immediately if there are no languages", function () {
        var ChooseLanguageSequence = this.context.require("jira/onboarding/choose-language-sequence");
        var sequence = new ChooseLanguageSequence({languages: {}});

        var promise = sequence.init(this.$el, this.analytics);

        this.clock.tick(100);

        equal(promise.isRejected(), true, "No languages");
        equal(promise.reason(), ChooseLanguageSequence.EVENTS.NO_LANGUAGES, "Reason should be that there were no languages to choose from.");
        ok(this.analytics.pushEvent.calledWith(ChooseLanguageSequence.EVENTS.NO_LANGUAGES), "should have fired NO_LANGUAGES event");
    });

    test("#init initialises the view with the provided element", function() {
        var ChooseLanguageSequence = this.context.require("jira/onboarding/choose-language-sequence");
        var sequence = new ChooseLanguageSequence(fakeLanguageData(3));
        sequence.init(this.$el, this.analytics);

        this.clock.tick(100);

        ok(this.FakeViewClass.calledOnce, "should construct the view");

        var callArg = this.FakeViewClass.getCall(0).args[0];
        ok(typeof callArg === "object", "should be an object");
        ok(callArg.el === this.$el, "our element was passed in to the view");
        ok(this.fakeView.render.calledOnce, "should render the view immediately");
    });

    test("#init initialises the model with the provided languages", function() {
        var data = fakeLanguageData(3);
        var ChooseLanguageSequence = this.context.require("jira/onboarding/choose-language-sequence");
        var sequence = new ChooseLanguageSequence(data);
        sequence.init(this.$el, this.analytics);

        this.clock.tick(100);

        ok(this.FakeModelClass.calledOnce, "should construct the model");

        var callArgs = this.FakeModelClass.getCall(0).args;
        equal(typeof callArgs[0], "object", "should have an attributes object");
        equal(callArgs[0].selected, "-1", "should be -1 by default");
        equal(typeof callArgs[1], "object", "should have an options objects");
        equal(callArgs[1].languages['en_1'].name, data.languages['en_1'], "should have been passed our language name");
        equal(callArgs[1].languages['en_1'].i18n, data.translations['en_1'], "should have been passed translations for the langauge");
    });

    test("starts with a chosen langauge of -1 (for system default", function() {
        var data = fakeLanguageData(3);
        var ChooseLanguageSequence = this.context.require("jira/onboarding/choose-language-sequence");
        new ChooseLanguageSequence(data).init(this.$el, this.analytics);

        ok(this.FakeModelClass.calledWith({ selected: "-1" }), "starts with system default language");
    });

    test("rejects if view fires a 'skip' event", function() {
        var ChooseLanguageSequence = this.context.require("jira/onboarding/choose-language-sequence");
        var sequence = new ChooseLanguageSequence(fakeLanguageData(3));
        var promise = sequence.init(this.$el, this.analytics);

        this.clock.tick(100);
        equal(promise.isPending(), true, "nothing has happened yet");

        this.fakeView.trigger("skip");
        this.clock.tick(100);

        equal(promise.isPending(), false, "should be finished");
        equal(promise.isFulfilled(), false, "should have finished with a skip");
        equal(promise.isRejected(), true, "should have finished with a skip");
        ok(this.analytics.pushEvent.calledWith(ChooseLanguageSequence.EVENTS.SKIPPED), "should have fired skip event");
    });

    test("causes model to save when view fires a 'done' event", function() {
        var ChooseLanguageSequence = this.context.require("jira/onboarding/choose-language-sequence");
        var sequence = new ChooseLanguageSequence(fakeLanguageData(3));
        var promise = sequence.init(this.$el, this.analytics);

        this.clock.tick(100);
        equal(promise.isPending(), true, "nothing has happened yet");

        this.sandbox.spy(this.fakeModel, "save");
        this.fakeView.trigger("done");
        this.clock.tick(100);

        equal(promise.isPending(), true, "should not be finished yet");
        equal(this.fakeModel.save.callCount, 1, "should have triggered a save on our model");
        equal(this.server.requests.length, 1, "should have made a REST call");
    });

    /*
     * Resolving the flow via localstorage is sub-optimal, but it is an approximation for a full server interaction.
     */
    test("sets localstorage value and reloads browser when language preference is changed successfully", function() {
        var ChooseLanguageSequence = this.context.require("jira/onboarding/choose-language-sequence");
        var sequence = new ChooseLanguageSequence(fakeLanguageData(3));
        var promise = sequence.init(this.$el, this.analytics);
        this.fakeModel.set("selected", "en_3");
        this.fakeView.trigger("done");

        // make the model save call return a success
        this.server.respondWith("PUT", this.fakeModel.url(), [204, { "Content-Type": "application/json" }, ""]);
        this.server.respond();

        // check everything worked as expected
        ok(promise.isPending(), "should not finish the promise");
        ok(Browser.reloadViaWindowLocation.calledOnce, "should call for a reload of the window");
        ok(Browser.reloadViaWindowLocation.calledWith(window.location.href), "reloads current window location");
        ok(LocalStorage.setItem.calledWith("jira.onboarding.choose.language.success", "en_3"), "should set the chosen language for pickup later");
    });

    /*
     * Resolving the flow via localstorage is sub-optimal, but it is an approximation for a full server interaction.
     */
    test("sets localstorage value and redirects to login if user session times out before saving language preference", function() {
        var ChooseLanguageSequence = this.context.require("jira/onboarding/choose-language-sequence");
        var sequence = new ChooseLanguageSequence(fakeLanguageData(3));
        var promise = sequence.init(this.$el, this.analytics);
        this.fakeModel.set("selected", "en_2");
        this.fakeView.trigger("done");

        // make the model save call return a success
        this.server.respondWith("PUT", this.fakeModel.url(), [401, { "Content-Type": "application/json" }, '{ "message": "You need to be logged in", "status-code": 401 }']);
        this.server.respond();

        // check everything worked as expected
        var browserCall = Browser.reloadViaWindowLocation.getCall(0);
        ok(promise.isPending(), "should not finish the promise");
        ok(browserCall, "should call for a reload of the window");
        ok(browserCall.args[0].indexOf("login.jsp") > -1, "redirects to login");
        ok(browserCall.args[0].indexOf(encodeURIComponent(window.location.href)) > -1, "should return here once login is done");
        ok(LocalStorage.setItem.calledWith("jira.onboarding.choose.language.data", "en_2"), "should set the chosen language for pickup later");
        ok(this.analytics.pushEvent.calledWith(ChooseLanguageSequence.EVENTS.NOT_AUTHORIZED), "should fire an auth fail analytics event");
    });

    /*
     * Resolving the flow via localstorage is sub-optimal, but it is an approximation for a full server interaction.
     */
    test("remembers user's language choice (but doesn't automatically choose it) in the event their session timed out", function() {
        var data = fakeLanguageData(3);
        data.currentLocale = "en_2";
        LocalStorage.getItem.withArgs("jira.onboarding.choose.language.data").returns("en_3");
        var ChooseLanguageSequence = this.context.require("jira/onboarding/choose-language-sequence");
        new ChooseLanguageSequence(data).init(this.$el, this.analytics);

        ok(this.FakeModelClass.calledWith({ selected: "en_3" }), "should prefer the localStorage value over currentLocale value");
        ok(LocalStorage.removeItem.calledWith("jira.onboarding.choose.language.data"), "should clean up after itself");
    });

    test("rejects flow if setting the user preference yields server error", function() {
        var ChooseLanguageSequence = this.context.require("jira/onboarding/choose-language-sequence");
        var sequence = new ChooseLanguageSequence(fakeLanguageData(3));
        var promise = sequence.init(this.$el, this.analytics);
        this.fakeModel.set("selected", "en_2");
        this.fakeView.trigger("done");

        // make the model save call return a success
        this.server.respondWith("PUT", this.fakeModel.url(), [500, { "Content-Type": "application/json" }, '{ "message": "Something weird happened", "status-code": 505 }']);
        this.server.respond();

        // check everything worked as expected
        equal(promise.isPending(), false, "should finish the promise");
        equal(promise.isRejected(), true, "should rejeect the promise");
        equal(promise.reason(), ChooseLanguageSequence.EVENTS.ERROR_ON_SAVE, "should have a reason for rejection");
        equal(this.analytics.pushEvent.calledWith(ChooseLanguageSequence.EVENTS.ERROR_ON_SAVE, sinon.match.object), true, "should fire an analytics event with the error");
        deepEqual(this.analytics.pushEvent.args[0][1], { error: { message: "Something weird happened", statusCode: 500 }, language: "en_2" }, "should fire an analytics event with the error");
    });

    /*
     * Resolving the flow via localstorage is sub-optimal, but it is an approximation for a full server interaction.
     */
    test("resolves flow immediately when (successful) language choice found in localStorage", function() {
        var data = fakeLanguageData(3);
        LocalStorage.getItem.withArgs("jira.onboarding.choose.language.success").returns("en_3");
        var ChooseLanguageSequence = this.context.require("jira/onboarding/choose-language-sequence");
        var sequence = new ChooseLanguageSequence(data);
        var promise = sequence.init(this.$el, this.analytics);

        this.clock.tick(100);
        equal(promise.isPending(), false, "we should be finished");
        equal(promise.isFulfilled(), true, "should have succeeded");
        equal(LocalStorage.removeItem.calledWith("jira.onboarding.choose.language.success"), true, "should clean up after itself");
        equal(this.analytics.pushEvent.calledWith(ChooseLanguageSequence.EVENTS.DONE, sinon.match.has("language", "en_3")), true, "should have fired analytics to denote the language chosen by the user");
    });

    /*
     * Resolving the flow via localstorage is sub-optimal, but it is an approximation for a full server interaction.
     */
    test("resolves flow without page reload when language choice is the same as currently selected language", function() {
        var data = fakeLanguageData(3);
        data.currentLocale = "en_42";
        var ChooseLanguageSequence = this.context.require("jira/onboarding/choose-language-sequence");
        var sequence = new ChooseLanguageSequence(data);
        var promise = sequence.init(this.$el, this.analytics);
        this.fakeModel.set("selected", "en_42");
        this.fakeView.trigger("done");
        this.clock.tick(100);

        // check everything worked as expected
        equal(promise.isPending(), false, "promise should not be pending");
        equal(promise.isFulfilled(), true, "promise should be done");
        equal(this.analytics.pushEvent.calledWith(ChooseLanguageSequence.EVENTS.DONE, sinon.match.has("language", "en_42")), true, "should have fired analytics to denote the language chosen by the user");
        // check none of the persistence stuff happened
        equal(this.server.requests.length, 0, "server should not have been involved");
        equal(Browser.reloadViaWindowLocation.notCalled, true, "should not call for a reload of the window");
        equal(LocalStorage.setItem.notCalled, true, "should not persist the language to localStorage");
        equal(LocalStorage.removeItem.notCalled, true, "should not need to clear the language from localStorage");
    });

    test("analytics for system default language", function() {
        var data = fakeLanguageData(3);
        data.languages["-1"] = "System default language";
        data.currentLocale = "-1";
        data.defaultLocale = "en_FOO";
        this.fakeModel.set("selected", "-1");
        var ChooseLanguageSequence = this.context.require("jira/onboarding/choose-language-sequence");
        new ChooseLanguageSequence(data).init(this.$el, this.analytics);
        this.fakeView.trigger("done");

        var call = this.analytics.pushEvent.getCall(0);
        equal(this.analytics.pushEvent.calledWith(ChooseLanguageSequence.EVENTS.DONE, sinon.match.object), true, "should have expanded system default language locale in analytics");
        equal(call.args[1].language, "en_FOO", "should have expanded system default language locale in analytics");
        equal(call.args[1].choseDefault, true, "should note the language chosen was the default choice");
        equal(call.args[1].oldLanguage, "-1", "should note the previous language value");
    });

    test("destroys the model if language selection is changed to -1 ", function() {
        var data = fakeLanguageData(3);
        this.sandbox.spy(this.fakeModel, "destroy");
        data.languages["-1"] = "System default language";
        data.currentLocale = "en_UK";
        data.defaultLocale = "en_FOO";
        this.fakeModel.set("selected", "-1");
        var ChooseLanguageSequence = this.context.require("jira/onboarding/choose-language-sequence");
        new ChooseLanguageSequence(data).init(this.$el, this.analytics);
        this.fakeView.trigger("done");

        equal(this.fakeModel.destroy.called, true, "should call destroy method when locale is changed to system default");
        equal(this.server.requests.length, 1, "should have made a REST call");
        equal(this.server.requests[0].method, "DELETE", "method should be DELETE");
    });
});
