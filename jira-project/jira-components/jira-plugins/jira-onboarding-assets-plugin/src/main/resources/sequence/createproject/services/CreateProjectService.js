/* global define */
define('jira/onboarding/services/create-project-service', ['require'], function(require) {
    var wrmContextPath = require('wrm/context-path');
    var Deferred = require('jira/jquery/deferred');
    var $ = require('jquery');

    return {
        /**
         * create a new project with the data provided.
         * @param fields
         */
        createProject: function(fields) {
            var deferred = new Deferred();

            $.ajax({
                url: wrmContextPath() + '/rest/project-templates/1.0/templates',
                type: 'POST',
                data: fields,
                timeout: 50000,
                headers: {
                    'X-Atlassian-Token': 'no-check'
                }
            }).done(deferred.resolve).fail(deferred.reject);

            return deferred.promise();
        }

    };
});
