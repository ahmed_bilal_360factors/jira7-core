/* global require, sinon, module, test, ok, equal */
AJS.test.require("com.atlassian.jira.jira-onboarding-assets-plugin:choose-language-component", function() {
    "use strict";

    var Backbone = require("backbone");
    var Templates = JIRA.Onboarding.Sequence.ChooseLanguage;

    var fakeJSONData = {
        languages: [
            { name: "English 1", value: "value_1", isSelected: false },
            { name: "Second English", value: "value_2", isSelected: true },
            { name: "English the Third", value: "value_3", isSelected: false }
        ]
    };

    function fakeModel() {
        var model = new Backbone.Model();
        sinon.stub(model, "toJSON").returns(fakeJSONData);
        sinon.stub(model, "set");
        return model;
    }

    module("ChooseLanguageSequenceView", {
        setup: function() {
            this.fakeModel = fakeModel();
            this.context = AJS.test.mockableModuleContext();
            this.context.mock("jira/onboarding/choose-language-sequence-model", this.fakeModel);
            this.View = this.context.require("jira/onboarding/choose-language-sequence-view");
        }
    });

    test("#render calls template with data passed in from the model", function() {
        this.stub(Templates, "render");
        var view = new this.View();
        view.render();

        ok(Templates.render.getCall(0), "should have been called");
        ok(typeof Templates.render.getCall(0).args[0] === "object", "should have been given an object");

        var args = Templates.render.getCall(0).args[0];
        ok(args.languages instanceof Array, "should pass an array of languages");
        equal(args.languages[0].id, "locale_value_1", "converts language value in to ID");
        equal(args.languages[0].value, "value_1", "should keep the value");
        equal(args.languages[0].isChecked, false, "first should not be checked");
        equal(args.languages[1].isChecked, true, "second language should be checked");
    });

    test("clicking the 'continue' button fires a 'done' event", function() {
        var handler = this.spy();
        var view = new this.View();
        view.bind("done", handler);
        view.render();
        view.$el.find("#next").click();

        equal(handler.callCount, 1, "should have fired a 'done' event");
    });

    test("changing a language option causes the model to be updated", function() {
        var view = new this.View();
        view.render();
        view.$el.find(":radio").first().click().change();

        ok(this.fakeModel.set.calledOnce, "should be called");
        ok(this.fakeModel.set.calledWith("selected", "value_1"), "should update to the locale's value");
    });
});
