/* global AJS */
define("jira/onboarding/nutshell-sequence/views/nutshell-dialog-view", ["require"], function (require) {
    "use strict";

    var Backbone = require("backbone");
    var _ = require("underscore");
    var $ = require("jquery");
    var skate = require("jira/skate");
    var formatter = require('jira/util/formatter');
    require("aui/inline-dialog2");

    return Backbone.View.extend({
        template: JIRA.Onboarding.sequence.nutshell.dialogContents,
        events: {
            "click .prev": function () {
                this.model.prev();
            },
            "click .next": function () {
                this.model.next();
            }
        },
        initialize: function () {
            this.listenTo(this.model, "step", this.render);
            if (_.isObject(this.model.current())) {
                this.render();
            }
        },
        render: function () {
            var currentStep = this.model.current();
            var dialogContents = this.template({
                text: [].concat(currentStep.text),
                primaryButtonText: this.model.hasNext() ? formatter.I18n.getText("common.forms.next") : formatter.I18n.getText("common.words.done"),
                backButtonText: formatter.I18n.getText("common.concepts.back"),
                hasBack: this.model.hasPrev(),
                step: this.model.currentIndex() + 1,
                totalSteps: this.model.get("steps").length
            });
            this._renderDialog(dialogContents, currentStep.trigger, {
                alignment: currentStep.alignment || "right middle"
            });

            return this;
        },
        _renderDialog: function (dialogContents, trigger, options) {
            // aui-inline-dialog2 needs to be recreated when we change the trigger element
            $("#inline-dialog-nutshellDialog").remove();
            var $dialog = $(JIRA.Onboarding.sequence.nutshell.inlineDialog2({
                alignment: options.alignment,
                content: dialogContents
            }));

            // only the trigger element should point to the InlineDialog2 instance
            $(trigger).attr('aria-controls', $(trigger).data('aria-controls'));

            $(document.body).append($dialog);

            this.setElement($dialog);
        },
        remove: function() {
            return Backbone.View.prototype.remove.apply(this, arguments);
        }
    });
});
