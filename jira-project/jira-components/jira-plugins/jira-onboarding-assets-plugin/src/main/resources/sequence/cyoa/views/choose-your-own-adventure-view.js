/* global define */
define('jira/onboarding/choose-your-own-adventure-view',
[
    'backbone',
    'jquery'
], function(
    Backbone,
    jQuery
) {
    'use strict';

    var Templates = JIRA.Onboarding.Sequence.ChooseYourOwnAdventure;

    /**
     * Render the UI for choose your own adventure page
     *
     * @class
     * @extends Backbone.View
     * @exports jira/onboarding/choose-language-sequence-view
     * @param {Object} options
     * @param {@link module:jira/onboarding/choose-language-sequence-model} options.model The model with language choices.
     */
    return Backbone.View.extend({

        events: {
            'click #sampleData': '_sampleData',
            'click #emptyProject': '_emptyProject',
            'click #import': '_import'
        },

        initialize: function(options) {
            this._validateOptions(options);

            this.username = options.username;
            this.userFullname = options.userFullname;
            this.boxes = options.boxes;
        },

        _validateOptions: function(options) {
            if (!options) {
                throw new Error('options should be provided');
            }

            if (!options.username) {
                throw new Error('Should have supplied the username of the current user');
            }

            if (!options.userFullname) {
                throw new Error('Should have supplied the fullname of the current user');
            }

            if (!options.boxes || !options.boxes.length) {
                throw new Error('Should have passed boxes with a length > 0');
            }
        },

        render: function() {
            this.$el.html(Templates.render(this._getRenderData()));
            jQuery('body').addClass('next-steps-bg');
            return this;
        },

        _getRenderData: function() {
            return {
                username: this.username,
                userFullname: this.userFullname,
                boxes: this.boxes
            };
        },

        _sampleData: function sampleData(e) {
            this.trigger('sampleDataClicked');
        },

        _emptyProject: function emptyProject(e) {
            this.trigger('emptyProjectClicked');
        },

        _import: function importData(e) {
            this.trigger('importClicked');
        }
    });
});
