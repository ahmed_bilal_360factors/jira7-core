AJS.test.require("com.atlassian.jira.jira-onboarding-assets-plugin:choose-your-own-adventure-component", function() {
    "use strict";

    var Templates = JIRA.Onboarding.Sequence.ChooseYourOwnAdventure;

    module("ChooseYourOwnAdventureView", {
        setup: function() {
            this.context = AJS.test.mockableModuleContext();
            this.View = this.context.require("jira/onboarding/choose-your-own-adventure-view");

            this.getValidOptions = function () {
                var getBox = function (id, buttonId) {
                    return {
                        icon: 'icon-' + id,
                        heading: 'heading-' + id,
                        description: 'description-' + id,
                        buttonId: buttonId,
                        buttonText: 'buttonText-' + id
                    };
                };

                var boxes = [
                    getBox('1', 'sampleData'),
                    getBox('2', 'emptyProject'),
                    getBox('3', 'import')
                ];

                return {
                    username: "username",
                    userFullname: "userFullname",
                    boxes: boxes
                };
            };
        }
    });

    test('#_validateOptions performs correct validation', function(asserts) {
        var assertThrow = asserts['throws'];

        assertThrow(function () {
            var view = new this.View();
        }, 'Should throw exception when no options are provided');

        assertThrow(function () {
            var options = this.getValidOptions();
            delete options.username;
            var view = new this.View(options);
        }, 'Should throw exception when no username is provided');

        assertThrow(function () {
            var options = this.getValidOptions();
            delete options.userFullname;
            var view = new this.View(options);
        }, 'Should throw exception when no userFullname is provided');

        assertThrow(function () {
            var options = this.getValidOptions();
            delete options.boxes;
            var view = new this.View(options);
        }, 'Should throw exception when no boxes are provided');

        assertThrow(function () {
            var options = this.getValidOptions();
            options.boxes = [];
            var view = new this.View(options);
        }, 'Should throw exception when empty boxes are provided');
    });

    test('#render calls template with data passed in from the options', function () {
        var options = this.getValidOptions();
        this.stub(Templates, "render");
        var view = new this.View(options);

        view.render();

        sinon.assert.calledOnce(Templates.render);
        ok(typeof Templates.render.getCall(0).args[0] === "object", "should have been given an object");
        var args = Templates.render.getCall(0).args[0];
        equal(args.username, "username", "'username' should be passed to renderer as username");
        equal(args.userFullname, "userFullname", "'userFullname' should be passed to renderer as userFullname");
        ok(args.boxes instanceof Array, "should pass an array of boxes");
        equal(args.boxes.length, 3, "there should be 3 boxes");
    });

    test("clicking the 'sampleData' button fires a 'sampleDataClicked' event", function() {
        var handler = this.spy();
        var view = new this.View(this.getValidOptions());
        view.bind("sampleDataClicked", handler);
        view.render();

        view.$el.find("#sampleData").click();

        sinon.assert.calledOnce(handler);
    });

    test("clicking the 'emptyProject' button fires a 'emptyProjectClicked' event", function() {
        var handler = this.spy();
        var view = new this.View(this.getValidOptions());
        view.bind("emptyProjectClicked", handler);
        view.render();

        view.$el.find("#emptyProject").click();

        sinon.assert.calledOnce(handler);
    });

    test("clicking the 'import' button fires a 'importClicked' event", function() {
        var handler = this.spy();
        var view = new this.View(this.getValidOptions());
        view.bind("importClicked", handler);
        view.render();

        view.$el.find("#import").click();

        sinon.assert.calledOnce(handler);
    });
});
