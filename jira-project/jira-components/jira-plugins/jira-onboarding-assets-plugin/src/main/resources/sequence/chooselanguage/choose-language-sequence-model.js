/* global define, AJS */
define('jira/onboarding/choose-language-sequence-model', ['require'], function(require) {
    var Backbone = require('backbone');
    var _ = require('underscore');
    var LoggedInUser = require('jira/util/users/logged-in-user');

    var wrmContextPath = require('wrm/context-path');

    var formatter = require('jira/util/formatter');

    var defaultI18ns = {
        buttonText: formatter.I18n.getText('common.forms.continue'),
        headingText: formatter.I18n.getText('onboarding.welcome.user.to.jira', LoggedInUser.fullName()),
        introductionText: formatter.I18n.getText('onboarding.choose.language.introduction'),
        titleText: formatter.I18n.getText('onboarding.welcome.to.jira')
    };

    /**
     * Encapsulates the user's language choice, as well as the logic for persisting that
     * value to the server via JIRA's REST API.
     *
     * @class
     * @extends Backbone.Model
     * @exports jira/onboarding/choose-language-sequence-model
     * @property {Object} attributes
     * @property {String} attributes.selected - The locale string for the user's language choice.
     */
    return Backbone.Model.extend({
        /**
         * Never new; this means we'll always be updating the preference when sending to the server.
         * @ignore
         */
        isNew: function() {
            return false;
        },

        /**
         * @constructs
         * @param {Object} attrs - Initial values for the model.
         * @param {Object} options - Additional configuration required by the model.
         * @param {Object} options.languages - Set of languages available for the user to choose.
         * All keys should be the locale string for a language, all values the name of that language (translated in that language).
         * Should be used in validation checks for the user's language selection.
         * @param {String} options.userPrefKey - the key used to store the user's language preference in their PropertySet on the server.
         * Required by the JIRA REST API endpoint in order to actually persist the user's choice.
         */
        initialize: function(attrs, options) {
            options || (options = {});
            this.languages = options.languages;
            this.userPrefKey = options.userPrefKey;
        },

        url: function() {
            return wrmContextPath() + '/rest/api/2/mypreferences?key=' + this.userPrefKey;
        },

        /**
         * We override the normal model sync method, because
         * JIRA's REST API does not adhere to or understand 'normal' JSON payloads.
         *
         * We need to provide the literal string value as the entire payload, which
         * isn't valid JSON, but it's what the REST API will accept.
         * @inheritdoc
         * @ignore
         */
        sync: function(method, model, options) {
            options.data = this.get('selected');
            options.dataType = 'json';
            options.contentType = 'application/json; charset=utf-8';
            return Backbone.sync.call(this, method, model, options);
        },

        getSelectedLanguage: function() {
            var selected = this.get('selected');
            return this.languages[selected];
        },

        getTranslationsFor: function(locale) {
            var lang = this.languages[locale] || {};
            return _.defaults(lang.i18n || {}, defaultI18ns);
        },

        /**
         * Used by the {@link module:jira/onboarding/choose-language-sequence-view} to get its data
         * for rendering. Might've been used by {@link #sync} in a normal model, but that was adjusted
         * for this one.
         *
         * @returns {{languages: Array, buttonText: String, headingText: String, introductionText: String, titleText: String}}
         */
        toJSON: function() {
            var selected = this.get('selected');
            var i18ns = this.getTranslationsFor(selected);
            var langs = [];
            _.forEach(this.languages, function(lang) {
                var isSelected = (selected === lang.value);
                langs.push({ name: lang.name, value: lang.value, isSelected: isSelected });
            });
            return {
                languages: langs,
                buttonText: i18ns.buttonText,
                headingText: i18ns.headingText,
                introductionText: i18ns.introductionText,
                titleText: i18ns.titleText
            };
        }
    });
});
