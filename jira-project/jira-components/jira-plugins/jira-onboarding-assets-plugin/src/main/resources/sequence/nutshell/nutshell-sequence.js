/* global AJS */
define("jira/onboarding/nutshell-sequence", ["require"], function (require) {
    "use strict";

    var $ = require("jquery");

    var SequenceModel = require("jira/onboarding/nutshell-sequence/models/sequence-model");
    var SequenceStep = require("jira/onboarding/nutshell-sequence/models/sequence-step");
    var AppView = require("jira/onboarding/nutshell-sequence/views/nutshell-app-view");
    var DialogView = require("jira/onboarding/nutshell-sequence/views/nutshell-dialog-view");
    var Promise = require("bluebird/Promise");

    var formatter = require('jira/util/formatter');

    function initSkeleton(container) {
        return $(container)
            .html(JIRA.Onboarding.sequence.nutshell.render())
            .find(".content");
    }

    function initModel() {
        return new SequenceModel({
            steps: [
                // Projects
                new SequenceStep({
                    text: formatter.I18n.getText("onboarding.nutshell.step.projects.comprised"),
                    trigger: ".project-list-trigger",
                    id: SequenceStep.PROJECTS
                }),
                new SequenceStep({
                    text: formatter.I18n.getText("onboarding.nutshell.step.projects.issues"),
                    trigger: ".project-list-trigger",
                    id: SequenceStep.PROJECTS_ISSUES
                }),
                // Issues
                new SequenceStep({
                    text: formatter.I18n.getText("onboarding.nutshell.step.issues.issue"),
                    trigger: ".issue-trigger",
                    id: SequenceStep.ISSUE
                }),
                new SequenceStep({
                    text: formatter.I18n.getText("onboarding.nutshell.step.issues.summary"),
                    trigger: ".summary-trigger",
                    id: SequenceStep.ISSUE_SUMMARY
                }),
                new SequenceStep({
                    text: formatter.I18n.getText("onboarding.nutshell.step.issues.key"),
                    trigger: ".key-trigger",
                    id: SequenceStep.ISSUE_KEY
                }),
                new SequenceStep({
                    text: formatter.I18n.getText("onboarding.nutshell.step.issues.assignee"),
                    trigger: ".assignee-trigger",
                    id: SequenceStep.ISSUE_ASSIGNEE
                }),
                new SequenceStep({
                    text: formatter.I18n.getText("onboarding.nutshell.step.issues.status"),
                    trigger: ".status-trigger",
                    id: SequenceStep.ISSUE_STATUS
                }),
                // Workflow
                new SequenceStep({
                    text: formatter.I18n.getText("onboarding.nutshell.step.workflow.example"),
                    trigger: ".workflow-trigger",
                    alignment: "top center",
                    id: SequenceStep.WORKFLOW
                }),
                new SequenceStep({
                    text: formatter.I18n.getText("onboarding.nutshell.step.workflow.buttons"),
                    trigger: ".opsbar",
                    alignment: "top center",
                    id: SequenceStep.WORKFLOW_BUTTONS
                }),
                new SequenceStep({
                    text: formatter.I18n.getText(
                        "onboarding.nutshell.step.workflow.click",
                        formatter.I18n.getText("common.words.done")
                    ),
                    trigger: ".opsbar-trigger",
                    id: SequenceStep.WORKFLOW_CLICK
                }),
                new SequenceStep({
                    text: formatter.I18n.getText("onboarding.nutshell.step.workflow.done"),
                    trigger: ".workflow-done-trigger",
                    id: SequenceStep.WORKFLOW_DONE
                }),
                new SequenceStep({
                    text: [
                        formatter.I18n.getText("onboarding.nutshell.step.all.done.1"),
                        formatter.I18n.getText("onboarding.nutshell.step.all.done.2")
                    ],
                    trigger: ".done-trigger",
                    id: SequenceStep.ALL_DONE
                })
            ]
        });
    }

    function initAppView(container, model, data) {
        return new AppView({
            model: model,
            data: data,
            el: container
        });
    }

    function initDialogView(model) {
        return new DialogView({
            model: model,
            id: "nutshellDialog"
        });
    }

    function init(container, analytics, data) {
        return new Promise(function (resolve) {
            var contentContainer = initSkeleton(container);

            var model = initModel();
            var appView = initAppView(contentContainer, model, data);
            var dialogView = initDialogView(model);
            var $skipButton = $(container).find(".onboarding-nutshell-skip");

            function handleKeyDown(e) {
                var KEY_SPACE = 32;
                var KEY_ENTER = 13;
                if (e.which === KEY_SPACE || e.which === KEY_ENTER) {
                    // Don't perform the action if a link or input is focused.
                    // We can't simply check if the activeElement is body since IE seems to allow any
                    // element to be become the activeElement when clicked.
                    if ($(e.target).closest(':input, a').length === 0) {
                        e.preventDefault();
                        model.next();
                    }
                }
            }

            function onSkipClick() {
                analytics.pushEvent(NutshellSequence.EVENTS.IS_SKIPPED);
                model.trigger("done");
            }

            $(document).on("keydown", handleKeyDown);
            $skipButton.one("click", onSkipClick);

            // resolve the promise when the dialog "Done" button is pressed
            model.once("done", function () {
                $(document).off("keydown", handleKeyDown);
                $skipButton.off("click", onSkipClick);
                dialogView.remove();
                analytics.pushEvent(NutshellSequence.EVENTS.DONE);
                resolve();
            });
        });
    }

    function NutshellSequence(data) {
        this.data = data;
    }

    NutshellSequence.prototype.init = function(container, analytics) {
        return init(container, analytics, this.data);
    };

    NutshellSequence.EVENTS = {
        DONE: "done",
        IS_SKIPPED: "isSkipped"
    };

    return NutshellSequence;
});
