/* global require, sinon */
AJS.test.require("com.atlassian.jira.jira-onboarding-assets-plugin:choose-your-own-adventure-component", function() {
    "use strict";

    var $ = require("jquery");
    var Backbone = require("backbone");
    var Promise = require('bluebird/Promise');
    var Browser;
    var LocalStorage;

    function fakeLocalStorage(sandbox) {
        return LocalStorage = {
            setItem: sandbox.stub(),
            getItem: sandbox.stub(),
            removeItem: sandbox.stub(),
            clear: sandbox.stub()
        };
    }

    function fakeBrowser(sandbox) {
        return Browser = {
            reloadViaWindowLocation: sandbox.spy()
        };
    }

    module("ChooseYourOwnAdventure", {
        setup: function () {
            var that = this;
            this.sandbox = sinon.sandbox.create();
            this.clock = this.sandbox.useFakeTimers();
            this.server = this.sandbox.useFakeServer();
            var View = Backbone.View.extend({render: this.sandbox.stub()});
            var fakeView = this.fakeView = new View();
            var FakeViewClass = this.FakeViewClass = sinon.stub().returns(fakeView);
            this.context = AJS.test.mockableModuleContext();
            this.context.mock("jira/onboarding/choose-your-own-adventure-view", FakeViewClass);
            this.context.mock("jira/data/local-storage", fakeLocalStorage(this.sandbox));
            this.context.mock("jira/util/browser", fakeBrowser(this.sandbox));
            this.ChooseYourOwnAdventure = this.context.require("jira/onboarding/choose-your-own-adventure");

            this.analytics = {
                pushEvent: this.sandbox.spy()
            };
            this.$el = $("<div></div>");
            this.getValidOptions = function() {
                return {
                    username: "username1",
                    userFullname: "userFullname1",
                    hasAdministerPermission: true,
                    complete: that.sandbox.spy()
                };
            };
        },
        teardown: function () {
            this.sandbox.restore();
        }
    });

    test('constructor should throw when invalid options are passed', function (asserts) {
        var assertThrow = asserts['throws'];

        assertThrow(function () {
            var cyoa = new this.ChooseYourOwnAdventure();
        }, 'Should throw exception when no options are provided');

        assertThrow(function () {
            var options = this.getValidOptions();
            delete options.username;
            var cyoa = new this.ChooseYourOwnAdventure(options);
        }, 'Should throw exception when no username is provided');

        assertThrow(function () {
            var options = this.getValidOptions();
            delete options.userFullname;
            var cyoa = new this.ChooseYourOwnAdventure(options);
        }, 'Should throw exception when no userFullname is provided');

        assertThrow(function () {
            var options = this.getValidOptions();
            delete options.hasAdministerPermission;
            var cyoa = new this.ChooseYourOwnAdventure(options);
        }, 'Should throw exception when no hasAdministerPermission parameter is provided');

        assertThrow(function () {
            var options = this.getValidOptions();
            options.complete = "complete";
            var cyoa = new this.ChooseYourOwnAdventure(options);
        }, 'Should throw exception when complete is not a function');
    });

    test('constructor should not throw exception when provided with correct data', function () {
        var options = this.getValidOptions();
        new this.ChooseYourOwnAdventure(options);
        ok(true, "Expected no errors to be thrown when valid options provided");
    });

    test('#init should bind on view events generated after buttons are clicked', function () {
        this.fakeView.on = this.sandbox.spy();
        var options = this.getValidOptions();
        var cyoa = new this.ChooseYourOwnAdventure(options);
        cyoa.init(this.$el, this.analytics);

        sinon.assert.calledThrice(this.fakeView.on);
        sinon.assert.calledWith(this.fakeView.on, 'sampleDataClicked');
        sinon.assert.calledWith(this.fakeView.on, 'emptyProjectClicked');
        sinon.assert.calledWith(this.fakeView.on, 'importClicked');
    });

    test('promise returned by #init should be unresolved (pending)', function () {
        var options = this.getValidOptions();
        var cyoa = new this.ChooseYourOwnAdventure(options);
        var promise = cyoa.init(this.$el, this.analytics);

        this.clock.tick(100);

        ok(promise.isPending(), "Promise should be pending (not rejected/done)");
    });

    // validate render on init
    test('render should be called by #init', function () {
        this.fakeView.render = this.sandbox.spy();
        var options = this.getValidOptions();
        var cyoa = new this.ChooseYourOwnAdventure(options);
        cyoa.init(this.$el, this.analytics);

        sinon.assert.calledOnce(this.fakeView.render);
    });

    test('clicking sampleData button should resolve promise and trigger analytics', function () {
        var options = this.getValidOptions();
        var cyoa = new this.ChooseYourOwnAdventure(options);
        var promise = cyoa.init(this.$el, this.analytics);

        this.clock.tick(100);
        ok(promise.isPending(), 'Promise should be pending (not rejected/done)');
        sinon.assert.notCalled(this.analytics.pushEvent);

        this.fakeView.trigger('sampleDataClicked');
        this.clock.tick(100);

        sinon.assert.calledOnce(this.analytics.pushEvent);
        sinon.assert.calledWith(this.analytics.pushEvent, 'sampleData');
        this.clock.tick(100);
        ok(promise.isFulfilled(), 'Promise should be resolved(fulfilled)');
        equal(promise.value(), 'sampleData', 'Promise should be resolved with "sampleData"');
    });

    test('clicking emptyProject button should resolve promise and trigger analytics', function () {
        var options = this.getValidOptions();
        var cyoa = new this.ChooseYourOwnAdventure(options);
        var promise = cyoa.init(this.$el, this.analytics);

        this.clock.tick(100);
        ok(promise.isPending(), 'Promise should be pending (not rejected/done)');
        sinon.assert.notCalled(this.analytics.pushEvent);

        this.fakeView.trigger('emptyProjectClicked');
        this.clock.tick(100);

        sinon.assert.calledOnce(this.analytics.pushEvent);
        sinon.assert.calledWith(this.analytics.pushEvent, 'emptyProject');
        this.clock.tick(100);
        ok(promise.isFulfilled(), 'Promise should be resolved(fulfilled)');
        equal(promise.value(), 'emptyProject', 'Promise should be resolved with "emptyProject"');
    });

    test('clicking import button should resolve promise and trigger analytics', function () {
        var options = this.getValidOptions();
        var cyoa = new this.ChooseYourOwnAdventure(options);
        var promise = cyoa.init(this.$el, this.analytics);

        this.clock.tick(100);
        ok(promise.isPending(), 'Promise should be pending (not rejected/done)');
        sinon.assert.notCalled(this.analytics.pushEvent);

        this.fakeView.trigger('importClicked');
        this.clock.tick(100);

        sinon.assert.calledOnce(this.analytics.pushEvent);
        sinon.assert.calledWith(this.analytics.pushEvent, 'import');
        this.clock.tick(100);
        ok(promise.isFulfilled(), 'Promise should be resolved(fulfilled)');
        equal(promise.value(), 'import', 'Promise should be resolved with "import"');
    });

    test('Non admin user should reject promise', function () {
        var options = this.getValidOptions();
        options.hasAdministerPermission = false;
        var cyoa = new this.ChooseYourOwnAdventure(options);

        var promise = cyoa.init(this.$el, this.analytics);

        this.clock.tick(100);
        ok(promise.isRejected(), 'Promise should be rejected');
        equal(promise.reason(), 'noPermission', 'Promise should be rejected with "noPermission"');
        sinon.assert.calledOnce(this.analytics.pushEvent);
        sinon.assert.calledWith(this.analytics.pushEvent, 'noPermission');
    });

    test('Admin should immediately complete flow when there at least one project exists', function () {
        var options = this.getValidOptions();
        options.projectCount = 1;
        var cyoa = new this.ChooseYourOwnAdventure(options);

        var promise = cyoa.init(this.$el, this.analytics);

        ok(options.complete.calledOnce);
    });
});
