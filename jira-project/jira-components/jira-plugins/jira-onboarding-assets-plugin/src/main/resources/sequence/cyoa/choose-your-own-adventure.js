/* global define */
define('jira/onboarding/choose-your-own-adventure', [
    'jira/util/formatter',
    'underscore',
    'jquery',
    'jira/onboarding/choose-your-own-adventure-view',
    'jira/util/browser',
    'wrm/context-path',
    'jira/ajs/keyboardshortcut/keyboard-shortcut-toggle',
    'bluebird/Promise'
], function(
    formatter,
    _,
    $,
    ChooseYourOwnAdventureView,
    Browser,
    AjsContextPath,
    KeyboardShortcutToggle,
    Promise) {
    'use strict';

    /**
     * Allows the user to choose one of several final options:
     * - create project with sample data
     * - create empty project
     * - import data from other system
     *
     * @param {Object} options - the data for the object; typically provided by the CyoaDataProvider java class.
     * @param {String} options.username - username of logged user.
     * @param {String} options.userFullname - full name of logged user.
     * @param {boolean} options.hasAdministerPermission - Indicates if user has admin permissions.
     * @constructor
     */
    var ChooseYourOwnAdventure = function(options) {
        this._validateOptions(options);
        options = _.extend({}, options);

        this.username = options.username;
        this.userFullname = options.userFullname;
        this.hasAdministerPermission = options.hasAdministerPermission;
        this.projectCount = options.projectCount;
        this._completeFlow = options.complete;
    };

    ChooseYourOwnAdventure.prototype._validateOptions = function(options) {
        if (!options) {
            throw new Error('Options should be supplied');
        }
        if (!options.username) {
            throw new Error('Should have supplied the username of the current user');
        }

        if (!options.userFullname) {
            throw new Error('Should have supplied the fullname of the current user');
        }
        if (!(options.hasAdministerPermission === true || options.hasAdministerPermission === false)) {
            throw new Error('Should have supplied valid value for "hasAdministerPermission" property');
        }

        if (!$.isFunction(options.complete)) {
            throw new Error('Should have passed complete flow function');
        }
    };

    /**
     * @param {Node} container
     * @param {OnboardingAnalytics} analytics
     * @returns {Promise}
     */
    ChooseYourOwnAdventure.prototype.init = function(container, analytics) {
        var instance = this;

        if( this.hasAdministerPermission !== true ) {
            analytics.pushEvent(ChooseYourOwnAdventure.EVENTS.NO_PERMISSION);
            return Promise.reject(ChooseYourOwnAdventure.EVENTS.NO_PERMISSION);
        }

        if (this._shouldEnableTopNav()) {
            KeyboardShortcutToggle.enable();
            $('body#jira').removeClass("disabled-navigation");
            var $logoLink = $('#logo a');
            $logoLink.attr('href', $logoLink.attr("data-href"));
        }

        return new Promise(function (resolve, reject) {
            instance.view = new ChooseYourOwnAdventureView({
                el: container,
                username: instance.username,
                userFullname: instance.userFullname,
                boxes: instance._generateBoxes()
            });
            instance.view.on('sampleDataClicked', function(response) {
                analytics.pushEvent(ChooseYourOwnAdventure.EVENTS.CLICKED_SAMPLE_DATA);
                resolve(ChooseYourOwnAdventure.EVENTS.CLICKED_SAMPLE_DATA);
            });
            instance.view.on('emptyProjectClicked', function(response) {
                analytics.pushEvent(ChooseYourOwnAdventure.EVENTS.CLICKED_EMPTY_PROJECT);
                resolve(ChooseYourOwnAdventure.EVENTS.CLICKED_EMPTY_PROJECT);
            });
            instance.view.on('importClicked', function(response) {
                analytics.pushEvent(ChooseYourOwnAdventure.EVENTS.CLICKED_IMPORT);
                resolve(ChooseYourOwnAdventure.EVENTS.CLICKED_IMPORT);
            });

            if (instance._shouldEnableTopNav()) {
                instance._completeFlow().then(instance.handleSecondAdminVisited.bind(instance), instance.handleSecondAdminVisited.bind(instance));
            } else {
                instance.view.render();
            }
        });
    };

    ChooseYourOwnAdventure.prototype._shouldEnableTopNav = function() {
        return this.hasAdministerPermission && this.projectCount > 0;
    };

    ChooseYourOwnAdventure.prototype._generateBoxes = function() {
        var boxes = [];

        boxes.push(ChooseYourOwnAdventure.BOXES.SAMPLE_DATA);
        boxes.push(ChooseYourOwnAdventure.BOXES.EMPTY_PROJECT);
        boxes.push(ChooseYourOwnAdventure.BOXES.IMPORT);

        return boxes;
    };

    ChooseYourOwnAdventure.prototype.handleSecondAdminVisited = function() {
        this.view.render();
    };

    ChooseYourOwnAdventure.prototype.handleImporter = function() {
        Browser.reloadViaWindowLocation(AjsContextPath() + "/secure/JIMOnboardingPage.jspa");
    };

    ChooseYourOwnAdventure.EVENTS = {
        CLICKED_SAMPLE_DATA: 'sampleData',
        CLICKED_EMPTY_PROJECT: 'emptyProject',
        CLICKED_IMPORT: 'import',
        NO_PERMISSION: 'noPermission'
    };

    ChooseYourOwnAdventure.BOXES = {
        SAMPLE_DATA: {
            icon: 'cyoa-search.svg',
            heading: formatter.I18n.getText('onboarding.cyoa.box.sample.heading'),
            description: formatter.I18n.getText('onboarding.cyoa.box.sample.content'),
            buttonId: 'sampleData',
            buttonText: formatter.I18n.getText('onboarding.cyoa.box.sample.btn'),
            buttonExtraClasses: "add-demo-project-trigger"
        },
        EMPTY_PROJECT: {
            icon: 'cyoa-create.svg',
            heading: formatter.I18n.getText('onboarding.cyoa.box.project.heading'),
            description: formatter.I18n.getText('onboarding.cyoa.box.project.content'),
            buttonId: 'emptyProject',
            buttonText: formatter.I18n.getText('onboarding.cyoa.box.project.btn'),
            buttonExtraClasses: "add-project-trigger"
        },
        IMPORT: {
            icon: 'cyoa-import.svg',
            heading: formatter.I18n.getText('onboarding.cyoa.box.import.heading'),
            description: formatter.I18n.getText('onboarding.cyoa.box.import.content'),
            buttonId: 'import',
            buttonText: formatter.I18n.getText('onboarding.cyoa.box.import.btn')
        }
    };

    return ChooseYourOwnAdventure;
});
