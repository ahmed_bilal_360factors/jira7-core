/* global define */
define('jira/onboarding/choose-language-sequence', ['require'], function(require) {
    'use strict';

    var _ = require('underscore');
    var Browser = require('jira/util/browser');
    var LocalStorage = require('jira/data/local-storage');
    var ChooseLanguageSequenceView = require('jira/onboarding/choose-language-sequence-view');
    var ChooseLanguageSequenceModel = require('jira/onboarding/choose-language-sequence-model');
    var wrmContextPath = require('wrm/context-path');
    var Promise = require('bluebird/Promise');

    var SYSTEM_DEFAULT_LOCALE = '-1';
    var SELECTED_LANG_STORAGE_KEY = 'jira.onboarding.choose.language.data';
    var SUCCESS_STORAGE_KEY = 'jira.onboarding.choose.language.success';

    /**
     * Allows the user to choose their language before getting in to JIRA proper.
     *
     * Wrangles with JIRA's REST API to set the user's language preferences, and refreshes the browser
     * if the preference necessitates a new set of javascript/assets to be generated and downloaded.
     *
     * @param {Object} options - the data for the sequence; typically provided by the ChooseLanguageSequenceDataProvider java class.
     * @param {String} options.userPrefKey - the key used to store the user's language preference in their PropertySet on the server.
     * Required by the JIRA REST API endpoint in order to actually persist the user's choice.
     * @param {Object} options.languages - set of all languages the user should be able to choose from.
     * All keys should be the locale string for a language, all values the name of that language (translated in that language).
     * @param {Object} options.translations - set of pre-translated strings required for rendering the choose language sequence UI.
     * See {@link module:jira/onboarding/choose-language-sequence-view} for the specific strings necessary.
     * @param {String} [options.currentLocale] - the locale string for the user's currently chosen language.
     * Typically during onboarding this will be the system default language locale string.
     * If provided, the UI should reflect this value as the user's current language choice.
     * @param {String} [options.defaultLocale] - the locale string for the system default language.
     * The value is used here primarily for analytics purposes.
     * @constructor
     */
    var ChooseLanguageSequence = function(options) {
        options = _.extend({ languages: {}, translations: {} }, options);
        this.currentLocale = options.currentLocale;
        this.defaultLocale = options.defaultLocale;
        this.userPrefKey = options.userPrefKey;
        this.languages = {};
        _.forEach(options.languages, function(language, key) {
            this.languages[key] = {
                name: language,
                value: key,
                i18n: options.translations[key]
            };
        }, this);
    };

    /**
     * @param {Node} container
     * @param {OnboardingAnalytics} analytics
     * @returns {Promise}
     */
    ChooseLanguageSequence.prototype.init = function(container, analytics) {
        var instance = this;
        var currentLocale = this.currentLocale;

        return new Promise(function (resolve, reject) {
            if (!instance.languages || _.keys(instance.languages).length < 2) {
                analytics.pushEvent(ChooseLanguageSequence.EVENTS.NO_LANGUAGES);
                reject(ChooseLanguageSequence.EVENTS.NO_LANGUAGES);
                return;
            }

            function finishSequence(lang, oldLang) {
                var data = { language: lang, oldLanguage: oldLang, choseDefault: false };
                if (lang === SYSTEM_DEFAULT_LOCALE && instance.defaultLocale) {
                    data.language = instance.defaultLocale;
                    data.choseDefault = true;
                }
                analytics.pushEvent(ChooseLanguageSequence.EVENTS.DONE, data);
                resolve(ChooseLanguageSequence.EVENTS.DONE);
            }

            // Check what the user chose when last they were on this screen
            var userChoice = SYSTEM_DEFAULT_LOCALE;
            if (LocalStorage.getItem(SELECTED_LANG_STORAGE_KEY)) {
                userChoice = LocalStorage.getItem(SELECTED_LANG_STORAGE_KEY);
                LocalStorage.removeItem(SELECTED_LANG_STORAGE_KEY);
            }

            // Check if the user successfully changed their preference
            if (LocalStorage.getItem(SUCCESS_STORAGE_KEY)) {
                var lang = LocalStorage.getItem(SUCCESS_STORAGE_KEY);
                LocalStorage.removeItem(SUCCESS_STORAGE_KEY);
                finishSequence(lang, userChoice); // In this scenario, userChoice is actually what their previous lang was.
                return;
            }

            var model = new ChooseLanguageSequenceModel({ selected: userChoice }, {
                languages: instance.languages,
                userPrefKey: instance.userPrefKey
            });

            var view = new ChooseLanguageSequenceView({
                el: container,
                model: model
            });
            view.once('done', function(response) {
                var locale = model.get('selected');
                if (currentLocale && currentLocale === locale) {
                    finishSequence(locale, currentLocale);
                    return;
                }

                var options = {
                    success: function(model, response, opts) {
                        LocalStorage.setItem(SELECTED_LANG_STORAGE_KEY, currentLocale);
                        LocalStorage.setItem(SUCCESS_STORAGE_KEY, opts.data);
                        Browser.reloadViaWindowLocation(window.location.href);
                    },
                    error: function(model, response, opts) {
                        var error = { message: "Unknown error", statusCode: response.status };
                        try {
                            error.message = JSON.parse(response.responseText).message;
                        } catch (e) {
                            // JIRA's REST APIs stopped working like we'd expect them to.
                            error.message += "; JSON parse of error response failed";
                        }
                        if (error.statusCode === 401) {
                            analytics.pushEvent(ChooseLanguageSequence.EVENTS.NOT_AUTHORIZED);
                            LocalStorage.setItem(SELECTED_LANG_STORAGE_KEY, opts.data);
                            Browser.reloadViaWindowLocation(wrmContextPath() + '/login.jsp?os_destination=' + encodeURIComponent(window.location.href));
                            return;
                        }
                        analytics.pushEvent(ChooseLanguageSequence.EVENTS.ERROR_ON_SAVE, { error: error, language: opts.data });
                        reject(ChooseLanguageSequence.EVENTS.ERROR_ON_SAVE);
                    }
                };

                //if the selected locale is the default one we actually need to make sure that the property is empty
                if (locale === SYSTEM_DEFAULT_LOCALE) {
                    model.destroy(options);
                } else {
                    model.save(null, options);
                }
            });
            view.once('skip', function() {
                analytics.pushEvent(ChooseLanguageSequence.EVENTS.SKIPPED);
                reject(ChooseLanguageSequence.EVENTS.SKIPPED);
            });

            view.render();

        });
    };

    ChooseLanguageSequence.EVENTS = {
        NO_LANGUAGES: 'noLanguages',
        NOT_AUTHORIZED: 'notAuthorized',
        ERROR_ON_SAVE: 'errorOnSave',
        DONE: 'done',
        SKIPPED: 'skipped'
    };

    return ChooseLanguageSequence;
});
