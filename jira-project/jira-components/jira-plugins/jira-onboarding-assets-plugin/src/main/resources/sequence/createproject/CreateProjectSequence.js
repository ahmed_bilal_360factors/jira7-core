/* global define */
define('jira/onboarding/create-project-sequence', [
    'underscore',
    'jira/onboarding/create-project-sequence-view',
    'bluebird/Promise'
], function(_, CreateProjectSequenceView, Promise) {
    "use strict";

    var CreateProjectSequence = function(options) {
        var sanitizedOptions = this.validateOptions(options);

        _.extend(this, sanitizedOptions);
    };

    CreateProjectSequence.prototype.validateOptions = function(options) {
        if (!options.username) {
            throw new Error('Should have supplied the username of the current user');
        }

        if (typeof options.canCompleteCreateProjectSequence === 'undefined') {
            throw new Error('Should have defined whether they can complete the create project sequence');
        }

        return _.pick(options, 'username', 'canCompleteCreateProjectSequence', 'maxKeyLength', 'maxNameLength', 'projectTemplateKey');
    };

    /**
     *
     * @param {Node} container
     * @param {OnboardingAnalytics} analytics
     * @returns {Promise}
     */
    CreateProjectSequence.prototype.init = function(container, analytics) {
        var instance = this;

        if (!instance.canCompleteCreateProjectSequence || !instance.username) {
            analytics.pushEvent(CreateProjectSequence.EVENTS.NO_PERMISSION);
            return Promise.reject(CreateProjectSequence.EVENTS.NO_PERMISSION);
        }

        if (!instance.projectTemplateKey) {
            analytics.pushEvent(CreateProjectSequence.EVENTS.NO_TEMPLATE);
            return Promise.reject(CreateProjectSequence.EVENTS.NO_TEMPLATE);
        }

        return new Promise(function (resolve, reject) {
            var createProjectView = new CreateProjectSequenceView({
                el: container,
                leadName: instance.username,
                maxKeyLength: instance.maxKeyLength,
                maxNameLength: instance.maxNameLength,
                projectTemplateKey: instance.projectTemplateKey
            });

            createProjectView.once('done', function (response) {
                analytics.pushEvent(CreateProjectSequence.EVENTS.CREATED);
                resolve(response);
            });

            createProjectView.once('skip', function () {
                analytics.pushEvent(CreateProjectSequence.EVENTS.SKIPPED);
                reject(CreateProjectSequence.EVENTS.SKIPPED);
            });

            createProjectView.render();
        });
    };

    CreateProjectSequence.EVENTS = {
        CREATED: 'projectCreated',
        SKIPPED: 'skipped',
        NO_PERMISSION: 'noPermission',
        NO_TEMPLATE: 'noProjectTemplateFound'
    };

    return CreateProjectSequence;
});
