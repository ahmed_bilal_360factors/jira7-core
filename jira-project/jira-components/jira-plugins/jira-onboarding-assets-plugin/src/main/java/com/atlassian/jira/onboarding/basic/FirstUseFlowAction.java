package com.atlassian.jira.onboarding.basic;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.util.JiraUrlCodec;
import com.atlassian.jira.web.action.ActionViewData;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.webresource.api.assembler.PageBuilderService;

import javax.servlet.http.HttpServletRequest;

public class FirstUseFlowAction extends JiraWebActionSupport {
    private final PageBuilderService pageBuilderService;
    private final PluginAccessor pluginAccessor;
    private final FeatureManager featureManager;

    public FirstUseFlowAction(
            @ComponentImport final PluginAccessor pluginAccessor,
            @ComponentImport final PageBuilderService pageBuilderService,
            @ComponentImport final FeatureManager featureManager) {
        this.pageBuilderService = pageBuilderService;
        this.pluginAccessor = pluginAccessor;
        this.featureManager = featureManager;
    }

    @Override
    protected String doExecute() throws Exception {
        if (getLoggedInUser() == null) {
            return redirectToLogin();
        }

        if (pluginAccessor.isPluginEnabled("com.atlassian.analytics.analytics-client")) {
            pageBuilderService.assembler().resources().requireWebResource("com.atlassian.analytics.analytics-client:js-events");
        }
        if (featureManager.isEnabled("jira.onboarding.cyoa")) {
            pageBuilderService.assembler().resources().requireWebResource("com.atlassian.jira.jira-onboarding-assets-plugin:jira-first-use-flow-new");
            pageBuilderService.assembler().resources().requireContext("atl.general");
            pageBuilderService.assembler().resources().requireContext("jira.project.add");
            pageBuilderService.assembler().resources().requireWebResource("jira.webresources:jira-fields");
        } else {
            pageBuilderService.assembler().resources().requireWebResource("com.atlassian.jira.jira-onboarding-assets-plugin:jira-first-use-flow");
        }

        return super.doExecute();
    }

    /**
     * @return a login redirect URL so after logging in they return to this page
     */
    private String redirectToLogin() {
        HttpServletRequest request = getHttpRequest();
        String returnURL = request.getRequestURI().substring(request.getContextPath().length());
        if (request.getQueryString() != null) {
            returnURL += "?" + request.getQueryString();
        }

        return getRedirect("/login.jsp?os_destination=" + JiraUrlCodec.encode(returnURL), false);
    }

    @ActionViewData
    public String getPageTitle() {
        return getText("onboarding.welcome.to.jira");
    }

}
