package com.atlassian.jira.onboarding.data;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.project.type.ProjectType;
import com.atlassian.jira.project.type.ProjectTypeManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;

public class NutshellDataProvider extends AbstractOnboardingDataProvider {
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final AvatarService avatarService;
    private final ProjectTypeManager projectTypeManager;

    public NutshellDataProvider(
            @ComponentImport final JiraAuthenticationContext jiraAuthenticationContext,
            @ComponentImport final AvatarService avatarService,
            @ComponentImport final ProjectTypeManager projectTypeManager) {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.avatarService = avatarService;
        this.projectTypeManager = projectTypeManager;
    }

    JSONObject getJsonData() {
        final ApplicationUser user = jiraAuthenticationContext.getUser();

        final Map<String, Object> values = Maps.newHashMap();

        values.put("defaultProjectAvatarUrl", getDefaultProjectAvatarUrl());
        values.put("userFullname", user.getDisplayName());
        addProjectTypeAvatars(values);

        return new JSONObject(values);
    }

    private String getDefaultProjectAvatarUrl() {
        return String.valueOf(avatarService.getProjectDefaultAvatarAbsoluteURL(Avatar.Size.defaultSize()));
    }

    private void addProjectTypeAvatars(final Map<String, Object> values) {
        final List<ProjectType> allProjectTypes = projectTypeManager.getAllProjectTypes();
        final Map<String, String> projectTypeAvatars = getAvatars(allProjectTypes);
        if (projectTypeAvatars.containsKey("business")) {
            values.put("projectTypeAvatars", projectTypeAvatars);
            values.put("projectType", "business");
        }
    }

    private Map<String, String> getAvatars(final List<ProjectType> allProjectTypes) {
        final Map<String, String> projectTypeAvatars = Maps.newHashMap();

        for (ProjectType projectType : allProjectTypes) {
            final String projectTypeKeyName = projectType.getKey().getKey();
            final String projectTypeAvatar = projectType.getIcon();
            projectTypeAvatars.put(projectTypeKeyName, projectTypeAvatar);
        }

        return projectTypeAvatars;
    }
}
