package com.atlassian.jira.onboarding.basic;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.features.DarkFeatureManager;

/**
 * {@link com.atlassian.sal.api.features.DarkFeatureEnabledCondition}, used in atlassian-plugin.xml needs
 * {@link com.atlassian.sal.api.features.DarkFeatureManager} to work, so we have to import this interface here
 */
public class DarkFeatureManagerComponentImporter {
    DarkFeatureManager darkFeatureManager;

    public DarkFeatureManagerComponentImporter(
            @ComponentImport DarkFeatureManager darkFeatureManager
    ) {
        this.darkFeatureManager = darkFeatureManager;
    }
}
