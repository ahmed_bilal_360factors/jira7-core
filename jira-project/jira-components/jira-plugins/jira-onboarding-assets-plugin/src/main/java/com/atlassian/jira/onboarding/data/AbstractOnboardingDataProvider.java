package com.atlassian.jira.onboarding.data;

import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.webresource.api.data.WebResourceDataProvider;

import java.io.IOException;
import java.io.Writer;

/**
 * We seem to be writing a bunch of data providers that return a JSON object,
 * and we have to write the boilerplate to transform a {@link com.atlassian.jira.util.json.JSONObject}
 * in to a {@link com.atlassian.json.marshal.Jsonable}. This class does the common stuff.
 *
 * @since 6.5
 */
public abstract class AbstractOnboardingDataProvider implements WebResourceDataProvider {
    @Override
    public Jsonable get() {
        return new Jsonable() {
            @Override
            public void write(final Writer writer) throws IOException {
                try {
                    getJsonData().write(writer);
                } catch (JSONException e) {
                    throw new JsonMappingException(e);
                }
            }
        };
    }

    abstract JSONObject getJsonData();
}
