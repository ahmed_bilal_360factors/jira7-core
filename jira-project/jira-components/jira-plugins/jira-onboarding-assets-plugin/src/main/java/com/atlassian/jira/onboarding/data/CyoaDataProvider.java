package com.atlassian.jira.onboarding.data;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.collect.ImmutableMap;

import java.util.List;
import java.util.Map;

public class CyoaDataProvider extends AbstractOnboardingDataProvider {
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final GlobalPermissionManager globalPermissionManager;
    private final ProjectService projectService;

    public CyoaDataProvider(
            @ComponentImport final JiraAuthenticationContext jiraAuthenticationContext,
            @ComponentImport final GlobalPermissionManager globalPermissionManager,
            @ComponentImport final ProjectService projectService) {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.globalPermissionManager = globalPermissionManager;
        this.projectService = projectService;
    }

    JSONObject getJsonData() {
        final ApplicationUser user = jiraAuthenticationContext.getLoggedInUser();
        final ServiceOutcome<List<Project>> accessibleProjects = projectService.getAllProjects(user);

        final Map<String, Object> values = ImmutableMap.of(
                "username", getUsername(user),
                "userFullname", getUserFullName(user),
                "hasAdministerPermission", globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user),
                "projectCount", accessibleProjects.isValid() ? accessibleProjects.getReturnedValue().size() : 0
        );

        return new JSONObject(values);
    }

    private String getUsername(final ApplicationUser user) {
        return user.getUsername();
    }

    private String getUserFullName(final ApplicationUser user) {
        return user.getDisplayName();
    }

}
