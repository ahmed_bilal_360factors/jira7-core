package com.atlassian.jira.onboarding.data;

import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.project.template.ProjectTemplateManager;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.collect.Maps;

import java.util.Map;

public class CreateProjectSequenceData extends AbstractOnboardingDataProvider {
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final GlobalPermissionManager globalPermissionManager;
    private final ProjectService projectService;
    private final ProjectTemplateManager projectTemplateManager;

    public CreateProjectSequenceData(
            @ComponentImport final JiraAuthenticationContext jiraAuthenticationContext,
            @ComponentImport final GlobalPermissionManager globalPermissionManager,
            @ComponentImport final ProjectService projectService,
            @ComponentImport final ProjectTemplateManager projectTemplateManager) {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.globalPermissionManager = globalPermissionManager;
        this.projectService = projectService;
        this.projectTemplateManager = projectTemplateManager;
    }

    JSONObject getJsonData() {
        final ApplicationUser user = jiraAuthenticationContext.getUser();

        final Map<String, Object> values = Maps.newHashMap();
        values.put("username", getUsername(user));
        values.put("canCompleteCreateProjectSequence", getCanCompleteCreateProjectSequence(user));
        values.put("maxKeyLength", projectService.getMaximumKeyLength());
        values.put("maxNameLength", projectService.getMaximumNameLength());
        values.put("projectTemplateKey", getProjectTemplateKey());
        return new JSONObject(values);
    }


    private String getUsername(final ApplicationUser user) {
        return user.getUsername();
    }

    private String getProjectTemplateKey() {
        try {
            return projectTemplateManager.getDefaultTemplate().getKey().getKey();
        } catch (IllegalStateException e) {
            return "";
        }
    }

    public Boolean getCanCompleteCreateProjectSequence(final ApplicationUser user) {
        return globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user);
    }
}
