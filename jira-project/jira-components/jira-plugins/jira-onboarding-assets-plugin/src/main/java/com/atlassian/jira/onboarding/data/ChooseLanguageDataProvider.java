package com.atlassian.jira.onboarding.data;

import com.atlassian.jira.config.LocaleManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.preferences.PreferenceKeys;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.LocaleParser;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.collect.Maps;

import java.util.Locale;
import java.util.Map;

public class ChooseLanguageDataProvider extends AbstractOnboardingDataProvider {
    private LocaleManager localeManager;
    private I18nHelper i18nHelper;
    private I18nHelper.BeanFactory i18nFactory;
    private JiraAuthenticationContext jiraAuthenticationContext;
    private ApplicationProperties applicationProperties;

    public ChooseLanguageDataProvider(
            @ComponentImport final LocaleManager localeManager,
            @ComponentImport final I18nHelper i18nHelper,
            @ComponentImport final I18nHelper.BeanFactory i18nFactory,
            @ComponentImport final JiraAuthenticationContext jiraAuthenticationContext,
            @ComponentImport final ApplicationProperties applicationProperties
    ) {
        this.localeManager = localeManager;
        this.i18nHelper = i18nHelper;
        this.i18nFactory = i18nFactory;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.applicationProperties = applicationProperties;
    }

    JSONObject getJsonData() {
        Map<String, String> languages = getInstalledLocales();
        Map<String, Object> translations = Maps.newHashMap();
        for (String locale : languages.keySet()) {
            translations.put(locale, getTranslationsFor(locale));
        }
        Map<String, Object> values = Maps.newHashMap();
        values.put("languages", languages);
        values.put("translations", translations);
        values.put("defaultLocale", applicationProperties.getDefaultLocale().toString());
        values.put("currentLocale", jiraAuthenticationContext.getLocale().toString());
        values.put("userPrefKey", PreferenceKeys.USER_LOCALE);
        return new JSONObject(values);
    }

    /**
     * @return the installed locales with the default option at the top
     */
    private Map<String, String> getInstalledLocales() {
        return localeManager.getInstalledLocalesWithDefault(applicationProperties.getDefaultLocale(), i18nHelper);
    }

    private Map<String, String> getTranslationsFor(final String localeString) {
        Locale locale = LocaleParser.parseLocale(localeString);
        I18nHelper i18n = i18nFactory.getInstance(locale);
        Map<String, String> values = Maps.newHashMap();
        values.put("buttonText", i18n.getText("common.forms.continue"));
        values.put("headingText", i18n.getText("onboarding.welcome.user.to.jira", jiraAuthenticationContext.getUser().getDisplayName()));
        values.put("introductionText", i18n.getText("onboarding.choose.language.introduction"));
        values.put("titleText", i18n.getText("onboarding.welcome.to.jira"));
        return values;
    }
}
