package com.atlassian.jira.onboarding.data;

import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.collect.Maps;

import java.util.Map;

public class NextStepDataProvider extends AbstractOnboardingDataProvider {
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final PermissionManager permissionManager;

    public NextStepDataProvider(
            @ComponentImport final JiraAuthenticationContext jiraAuthenticationContext,
            @ComponentImport final PermissionManager permissionManager) {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.permissionManager = permissionManager;
    }

    JSONObject getJsonData() {
        final ApplicationUser user = jiraAuthenticationContext.getUser();

        final Map<String, Object> values = Maps.newHashMap();
        values.put("canBrowseProjects", canBrowseProjects(user));
        values.put("canCreateIssues", canCreateIssues(user));
        values.put("canSearchIssues", canSearchIssues(user));
        return new JSONObject(values);
    }

    private boolean canCreateIssues(final ApplicationUser user) {
        return permissionManager.hasProjects(ProjectPermissions.CREATE_ISSUES, user);
    }

    private boolean canBrowseProjects(final ApplicationUser user) {
        return permissionManager.hasProjects(ProjectPermissions.BROWSE_PROJECTS, user);
    }

    private boolean canSearchIssues(final ApplicationUser user) {
        return canBrowseProjects(user);
    }
}
