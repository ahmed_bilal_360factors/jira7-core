package com.atlassian.jira.onboarding.data;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.collect.Maps;

import java.util.Map;

public class AvatarPickerDataProvider extends AbstractOnboardingDataProvider {
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final AvatarService avatarService;
    private final AvatarManager avatarManager;


    public AvatarPickerDataProvider(
            @ComponentImport final JiraAuthenticationContext jiraAuthenticationContext,
            @ComponentImport final AvatarService avatarService,
            @ComponentImport final AvatarManager avatarManager) {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.avatarService = avatarService;
        this.avatarManager = avatarManager;
    }

    JSONObject getJsonData() {
        final ApplicationUser user = jiraAuthenticationContext.getUser();

        final Map<String, Object> values = Maps.newHashMap();
        values.put("username", getUsername(user));
        values.put("userFullname", getUserFullName(user));
        values.put("userAvatarId", getUserAvatarId(user));
        values.put("userAvatarUrl", getUserAvatarUrl(user));
        values.put("defaultUserAvatarId", getDefaultUserAvatarId());
        return new JSONObject(values);
    }

    private Long getUserAvatarId(final ApplicationUser user) {
        return avatarService.getAvatar(user, user).getId();
    }

    private String getUserAvatarUrl(final ApplicationUser user) {
        return avatarService.getAvatarAbsoluteURL(user, user, Avatar.Size.defaultSize()).toString();
    }

    private String getDefaultUserAvatarId() {
        return String.valueOf(avatarManager.getDefaultAvatarId(Avatar.Type.USER));
    }

    private String getUsername(final ApplicationUser user) {
        return user.getUsername();
    }

    private String getUserFullName(final ApplicationUser user) {
        return user.getDisplayName();
    }

}
