package com.atlassian.jira.onboarding.data;

import com.atlassian.jira.onboarding.OnboardingService;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.jira.web.action.ActionViewData;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.collect.Maps;

import java.util.Map;

public class SequenceControllerDataProvider extends AbstractOnboardingDataProvider {
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final OnboardingService onboardingService;

    public SequenceControllerDataProvider(
            @ComponentImport final JiraAuthenticationContext jiraAuthenticationContext,
            @ComponentImport final OnboardingService onboardingService) {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.onboardingService = onboardingService;
    }

    JSONObject getJsonData() {
        final ApplicationUser user = jiraAuthenticationContext.getUser();

        final Map<String, Object> values = Maps.newHashMap();
        values.put("currentSequenceKey", getCurrentSequenceKey(user));
        values.put("currentFlow", getCurrentFlowKey(user));
        return new JSONObject(values);
    }


    @ActionViewData
    public String getCurrentSequenceKey(final ApplicationUser user) {
        return onboardingService.getCurrentFirstUseFlowSequence(user);
    }

    @ActionViewData
    public String getCurrentFlowKey(final ApplicationUser user) {
        return onboardingService.getStartedFirstUseFlowKey(user);
    }
}
