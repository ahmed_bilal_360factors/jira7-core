package com.atlassian.jira.onboarding.data;

import java.util.List;

import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.json.JSONObject;

import com.google.common.collect.Lists;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestCyoaDataProvider {
    private CyoaDataProvider cyoaDataProvider;

    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;

    @Mock
    GlobalPermissionManager globalPermissionManager;

    @Mock
    ProjectService projectService;

    private final static String USERNAME = "someUsername";
    private final static String USERFULLNAME = "someFullUsername";

    @Before
    public void setUp() {
        ApplicationUser mockUser = mock(ApplicationUser.class);
        when(mockUser.getUsername()).thenReturn(USERNAME);
        when(mockUser.getDisplayName()).thenReturn(USERFULLNAME);
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, mockUser)).thenReturn(Boolean.TRUE);
        when(jiraAuthenticationContext.getLoggedInUser()).thenReturn(mockUser);
        when(projectService.getAllProjects(mockUser)).thenReturn(new ServiceOutcomeImpl<List<Project>>(new SimpleErrorCollection(), Lists.<Project>newArrayList()));

        cyoaDataProvider = new CyoaDataProvider(jiraAuthenticationContext, globalPermissionManager, projectService);
    }

    @Test
    public void jsonDataHasAllRequiredProperties() {
        JSONObject jsonData = cyoaDataProvider.getJsonData();

        try {
            assertThat("proper username should be generated", jsonData.get("username"), Matchers.equalTo(USERNAME));
        } catch (Exception e) {
            fail("JSON data should have key 'username'");
        }

        try {
            assertThat("proper userFullname should be generated", jsonData.get("userFullname"), Matchers.equalTo(USERFULLNAME));
        } catch (Exception e) {
            fail("JSON data should have key 'userFullname'");
        }

        try {
            assertThat("proper hasAdministerPermission should be generated", jsonData.get("hasAdministerPermission"), Matchers.equalTo(true));
        } catch (Exception e) {
            fail("JSON data should have key 'hasAdministerPermission'");
        }

        try {
            assertThat("no projects available for this suer", jsonData.get("projectCount"), Matchers.equalTo(0));
        } catch (Exception e) {
            fail("JSON data should have key 'projectCount'");
        }
    }
}
