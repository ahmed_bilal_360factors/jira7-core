package com.atlassian.jira.onboarding.data;

import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.project.type.ProjectType;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.project.type.ProjectTypeManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestNutshellDataProvider {
    private NutshellDataProvider mockNutshellDataProvider;

    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;

    @Mock
    private AvatarService avatarService;

    @Mock
    private ProjectTypeManager projectTypeManager;

    @Before
    public void setUp() {
        mockNutshellDataProvider = new NutshellDataProvider(jiraAuthenticationContext, avatarService, projectTypeManager);

        ApplicationUser mockUser = mock(ApplicationUser.class);
        when(jiraAuthenticationContext.getUser()).thenReturn(mockUser);
    }


    @Test
    public void jsonDataDoesNotHaveProjectTypeAndAvatarsWhenBusinessProjectTypeIsNotFound() {
        projectTypeManagerHasNoTypes();

        assertJsonDataDoesNotHaveProjectTypeAndAvatars();
    }

    @Test
    public void jsonDataHasProjectTypeAndAvatarsWhenBusinessProjectTypeIsFound() {
        projectTypeManagerHasBusinessType();

        assertJsonDataHasProjectTypeAndAvatars();
    }

    private void assertJsonDataHasProjectTypeAndAvatars() {
        JSONObject jsonData = mockNutshellDataProvider.getJsonData();

        assertTrue("JSON data should have key 'projectType'", jsonData.has("projectType"));

        try {
            HashMap projectTypeAvatars = (HashMap) jsonData.get("projectTypeAvatars");
            assertTrue("JSON data should have key 'projectTypeAvatars.business'", projectTypeAvatars.containsKey("business"));
        } catch (Exception e) {
            fail("JSON data should have key 'projectTypeAvatars'");
        }
    }

    private void assertJsonDataDoesNotHaveProjectTypeAndAvatars() {
        JSONObject jsonData = mockNutshellDataProvider.getJsonData();

        assertFalse("JSON data should not have key 'projectType'", jsonData.has("projectType"));
        assertFalse("JSON data should not have key 'projectTypeAvatars'", jsonData.has("projectTypeAvatars"));
    }

    private void projectTypeManagerHasBusinessType() {
        ArrayList<ProjectType> allProjectTypes = new ArrayList<>();

        final ProjectTypeKey key = new ProjectTypeKey("business");
        final ProjectType projectType = new ProjectType(key, "", "", "", 0);
        allProjectTypes.add(projectType);

        when(projectTypeManager.getAllProjectTypes()).thenReturn(allProjectTypes);
    }

    private void projectTypeManagerHasNoTypes() {
        ArrayList<ProjectType> emptyList = new ArrayList<>();
        when(projectTypeManager.getAllProjectTypes()).thenReturn(emptyList);

    }
}
