(function () {

    'use strict';
    require([
        'jquery',
        'jira/postsetup/announcements-view-lib',
        'jira/postsetup/announcements-initializer'
    ], function (
        $,
        Announcements,
        AnnouncementsInitializer
    ) {
        // import
        var Announcement = Announcements.Announcement;

        var ifOnPage = Announcements.ifOnPage;
        var findElement = Announcements.findElement;
        var inViewPort = Announcements.inViewPort;
        var holdOnTillFlagsAreGone = Announcements.holdOnTillFlagsAreGone;
        var soyTemplateAnnouncementBody = Announcements.soyTemplateAnnouncementBody;
        var GRAVITY_LEFT = Announcements.GRAVITY_LEFT;
        var GRAVITY_BELOW = Announcements.GRAVITY_BELOW;

        function init() {
            var DEFINED_ANNOUNCEMENTS = {
                'database.setup': new Announcement(
                    'database.setup',
                    soyTemplateAnnouncementBody(JIRA.Templates.PostSetup.dbDiscoveryContent),
                    holdOnTillFlagsAreGone(findElement("#system-admin-menu")),
                    GRAVITY_BELOW,
                    0,
                    true),
                'app.properties.setup': new Announcement(
                    'app.properties.setup',
                    soyTemplateAnnouncementBody(JIRA.Templates.PostSetup.appPropertiesDiscoveryContent),
                    ifOnPage("admin/ViewApplicationProperties.jspa",
                        holdOnTillFlagsAreGone(findElement("#edit-app-properties"))),
                    GRAVITY_BELOW,
                    20,
                    true
                ),
                'mail.properties.setup': new Announcement(
                    'mail.properties.setup',
                    soyTemplateAnnouncementBody(JIRA.Templates.PostSetup.mailPropertiesDiscoveryContent),
                    ifOnPage("admin/ViewApplicationProperties.jspa",
                        inViewPort(findElement("#outgoing_mail"))),
                    GRAVITY_LEFT,
                    -10, // this is async, it doesn't block other announcements
                    false
                )
                // don't forget to whitelist analytics events when you are adding new annoucements
            };

            AnnouncementsInitializer.createFlagsFromDataProvider(DEFINED_ANNOUNCEMENTS);
        }

        $(function () {
            init();
        });
    });
})();
