define('jira/postsetup/announcements-initializer', [
    'wrm/data',
    'jira/postsetup/announcements-view-lib'
], function(
    data,
    Announcements
){
    var wrmAnnouncements = data.claim('com.atlassian.jira.jira-postsetup-announcements-plugin:post-setup-announcements.announcements') || [];

    return {
        createFlagsFromDataProvider: function (definedAnnouncements) {
            var controller = new Announcements.AnnouncementsController(definedAnnouncements, wrmAnnouncements);
            controller.createFlags();
        }
    };
});
