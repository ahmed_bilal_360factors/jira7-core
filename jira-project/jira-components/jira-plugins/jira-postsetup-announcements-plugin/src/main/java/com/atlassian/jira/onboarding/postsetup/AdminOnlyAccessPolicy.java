package com.atlassian.jira.onboarding.postsetup;

import com.atlassian.fugue.Option;
import com.atlassian.fugue.Suppliers;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.base.Function;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * @since v6.4
 */
@Component
public class AdminOnlyAccessPolicy implements AnnouncementAccessPolicy {
    private final GlobalPermissionManager permissionManager;

    @Inject
    public AdminOnlyAccessPolicy(@ComponentImport final GlobalPermissionManager permissionManager) {
        this.permissionManager = permissionManager;
    }

    @Override
    public boolean canRead(final Option<ApplicationUser> userOption) {
        return isAdmin(userOption);
    }

    @Override
    public boolean canUpdate(final Option<ApplicationUser> userOption) {
        return isAdmin(userOption);
    }

    private Boolean isAdmin(final Option<ApplicationUser> userOption) {
        return userOption.fold(Suppliers.alwaysFalse(), new Function<ApplicationUser, Boolean>() {
            @Override
            public Boolean apply(final ApplicationUser user) {
                final boolean isAdmin = permissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user);
                return isAdmin;
            }
        });
    }
}
