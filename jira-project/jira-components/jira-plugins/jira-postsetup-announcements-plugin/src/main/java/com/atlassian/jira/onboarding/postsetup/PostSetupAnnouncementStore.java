package com.atlassian.jira.onboarding.postsetup;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * @since v6.4
 */
public interface PostSetupAnnouncementStore {
    @Nonnull
    List<PostSetupAnnouncementStatus> getAllStatuses();

    void updateStatuses(@Nonnull Iterable<PostSetupAnnouncementStatus> postSetupActivityStatuses);
}
