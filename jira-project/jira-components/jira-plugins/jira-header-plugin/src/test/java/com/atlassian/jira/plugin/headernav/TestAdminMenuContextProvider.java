package com.atlassian.jira.plugin.headernav;

import com.atlassian.jira.plugin.webfragment.SimpleLinkManager;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.plugin.webfragment.model.SimpleLink;
import com.atlassian.jira.plugin.webfragment.model.SimpleLinkSection;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.sitemesh.AdminDecoratorSectionLinkItemHelper;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.atlassian.jira.plugin.headernav.AdminMenuContextProvider.CONTEXT_SECTIONS_KEY;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestAdminMenuContextProvider {
    private static final String URL = "url";
    private final SimpleLinkManager simpleLinkManagerMock = mock(SimpleLinkManager.class);
    private final JiraAuthenticationContext authenticationContextMock = mock(JiraAuthenticationContext.class);
    private final ApplicationUser userMock = mock(ApplicationUser.class);
    private final JiraHelper helper = new JiraHelper();
    private final AdminDecoratorSectionLinkItemHelper adminDecoratorSectionLinkItemHelper = mock(AdminDecoratorSectionLinkItemHelper.class);
    private final SimpleLinkSection sectionMock = mock(SimpleLinkSection.class);

    private final SimpleLinkSection subSectionMock = mock(SimpleLinkSection.class);
    private final SimpleLink sectionsLinkMock = mock(SimpleLink.class);
    private final SimpleLink linkMock = mock(SimpleLink.class);
    private final SimpleLink adminSummaryMock = mock(SimpleLink.class);
    private final SimpleLink relatedWebLink = mock(SimpleLink.class);

    private AdminMenuContextProvider provider;

    @Before
    public void setUp() {
        when(authenticationContextMock.getUser()).thenReturn(userMock);
        when(sectionMock.getId()).thenReturn("root-section");
        when(subSectionMock.getId()).thenReturn("sub-section");
        when(linkMock.getId()).thenReturn("link");
        when(adminSummaryMock.getId()).thenReturn("view_projects");
        when(adminDecoratorSectionLinkItemHelper.findSectionLink(subSectionMock, userMock, helper)).thenReturn(Optional.empty());
        provider = new AdminMenuContextProvider(simpleLinkManagerMock, authenticationContextMock, adminDecoratorSectionLinkItemHelper);
    }

    @AnonymousUser
    @Test
    public void testNoLinksVisibleAsAnonymousUser() {
        final Map<String, Object> result = whenContextProviderIsInvoked();
        assertNotNull(result);
        assertThat(result.get(CONTEXT_SECTIONS_KEY), is(ContextProviderMatcher.emptyMap()));
    }

    @AuthenticatedUser
    @Test
    public void testAuthenticatedUser() {
        givenOneNonEmptyAdminSection();
        givenOneAdminLinkForTheFirstAdminSection();
        givenOneAdditionalAdminLink();
        final Map<String, Object> result = whenContextProviderIsInvoked();
        assertNotNull(result);
        assertThat((Map<SimpleLinkSection, ?>) result.get(CONTEXT_SECTIONS_KEY), hasKey(sectionMock));
    }

    @Test
    public void testNoLinksForSection() {
        givenOneNonEmptyAdminSection();
        givenEmptySubSectionForTheFirstAdminSection();
        final Map<String, Object> result = whenContextProviderIsInvoked();
        assertNotNull(result);
        assertThat((Map<SimpleLinkSection, ?>) result.get(CONTEXT_SECTIONS_KEY), hasKey(sectionMock));
    }

    @Test
    public void returnsDefinedWebLinkUrlForMainSection() {
        givenOneNonEmptyAdminSection();
        givenEmptySubSectionForTheFirstAdminSection();
        givenThatSubSectionHasRelatedWebLinkDefined();

        final Map<String, Object> result = whenContextProviderIsInvoked();
        assertNotNull(result);
        assertThat(((Map<SimpleLinkSection, List<SimpleLink>>) result.get(CONTEXT_SECTIONS_KEY)).get(sectionMock), Matchers.contains(new SimpleLinkSectionWithLinkMatcher(URL)));
    }

    private void givenThatSubSectionHasRelatedWebLinkDefined() {
        when(adminDecoratorSectionLinkItemHelper.findSectionLink(subSectionMock, userMock, helper)).thenReturn(Optional.of(relatedWebLink));
        when(relatedWebLink.getUrl()).thenReturn(URL);
    }

    private void givenOneNonEmptyAdminSection() {
        when(simpleLinkManagerMock.getNotEmptySectionsForLocation(AdminMenuContextProvider.ADMIN_TOP_NAVIGATION_BAR_LOCATION, userMock, helper))
                .thenReturn(Collections.singletonList(sectionMock));
    }

    private void givenOneAdminLinkForTheFirstAdminSection() {
        when(simpleLinkManagerMock.getNotEmptySectionsForLocation(sectionMock.getId(), userMock, helper))
                .thenReturn(Collections.singletonList(subSectionMock));
        when(simpleLinkManagerMock.getLinksForSection(subSectionMock.getId(), userMock, helper))
                .thenReturn(Collections.singletonList(sectionsLinkMock));
    }

    private void givenEmptySubSectionForTheFirstAdminSection() {
        when(simpleLinkManagerMock.getNotEmptySectionsForLocation(sectionMock.getId(), userMock, helper))
                .thenReturn(Collections.singletonList(subSectionMock));
    }

    private void givenOneAdditionalAdminLink() {
        when(simpleLinkManagerMock.getLinksForSection(AdminMenuContextProvider.ADMIN_TOP_NAVIGATION_BAR_LOCATION, userMock, helper))
                .thenReturn(Arrays.asList(adminSummaryMock, linkMock));
    }

    private Map<String, Object> whenContextProviderIsInvoked() {
        return provider.getContextMap(Collections.<String, Object>singletonMap("helper", helper));
    }

    @Ignore
    private class SimpleLinkSectionWithLinkMatcher extends BaseMatcher<SimpleLink> {

        private final String url;

        public SimpleLinkSectionWithLinkMatcher(String url) {
            this.url = url;
        }

        @Override
        public boolean matches(final Object item) {
            SimpleLink link = (SimpleLink) item;
            return url.equals(link.getUrl());

        }

        @Override
        public void describeTo(final Description description) {
            description.appendText(this.toString());
        }

        @Override
        public String toString() {
            return "<Has url: " + url + ">";
        }
    }
}
