package com.atlassian.jira.plugin.headernav.newsletter;

import com.atlassian.fugue.Option;
import com.atlassian.jira.help.HelpUrl;
import com.atlassian.jira.help.HelpUrls;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.io.IOException;
import java.io.StringWriter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class TestNewsletterSignupDataProvider {
    private final String url = "http://www.atlassian.com/newsletter/{0}/signup";
    private final String newsletterTitle = "title";
    private final String newsletterDescription = "description";
    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private NewsletterUserPreferencesManager newsletterUserPreferencesManager;
    @Mock
    private HelpUrls helpUrls;
    @Mock
    private HelpUrl helpUrl;
    @Mock
    private I18nHelper i18nHelper;

    private final String userEmail = "fred@example.com";
    private ApplicationUser fred = new MockApplicationUser("fred", "Fred", userEmail);
    private NewsletterSignupDataProvider dataProvider;

    @Before
    public void setUp() throws Exception {
        initMocks(this);

        when(helpUrl.getUrl()).thenReturn(url);
        when(helpUrls.getUrl("newsletter.signup.form")).thenReturn(helpUrl);
        when(i18nHelper.getText("newsletter.signup.tip.title")).thenReturn(newsletterTitle);
        when(i18nHelper.getText("newsletter.signup.tip.description")).thenReturn(newsletterDescription);

        when(authenticationContext.getUser()).thenReturn(fred);
        when(newsletterUserPreferencesManager.shouldShowSignupTip(Option.option(fred))).thenReturn(true);

        dataProvider = new NewsletterSignupDataProvider(authenticationContext, newsletterUserPreferencesManager, helpUrls, i18nHelper);
    }

    @Test
    public void anonymousUserDoesntIncludeEmail() throws Exception {
        when(authenticationContext.getUser()).thenReturn(null);
        when(newsletterUserPreferencesManager.shouldShowSignupTip(Option.<ApplicationUser>none())).thenReturn(false);

        final JSONObject json = getJsonObject();

        assertFalse(json.getBoolean("showNewsletterTip"));
        assertFalse(json.has("userEmail"));
        assertEquals(url, json.getString("formUrl"));
    }

    @Test
    public void userPassedToPreferencesManager() throws Exception {

        final JSONObject json = getJsonObject();

        assertTrue(json.getBoolean("showNewsletterTip"));
        assertEquals(userEmail, json.getString("userEmail"));
        assertEquals(url, json.getString("formUrl"));
    }

    @Test
    public void shouldReturnSoftwareNewsletter() throws Exception {
        final JSONObject json = getJsonObject();

        assertEquals(true, json.getBoolean("showNewsletterTip"));
        assertEquals(newsletterTitle, json.getString("signupTitle"));
        assertEquals(newsletterDescription, json.getString("signupDescription"));
        assertEquals(userEmail, json.getString("userEmail"));
        assertEquals(url, json.getString("formUrl"));
    }

    private JSONObject getJsonObject() throws IOException, JSONException {
        final StringWriter writer = new StringWriter();
        dataProvider.get().write(writer);
        return new JSONObject(writer.toString());
    }
}