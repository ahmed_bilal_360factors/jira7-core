package com.atlassian.jira.plugin.headernav;

import com.atlassian.jira.plugin.webfragment.SimpleLinkManager;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.plugin.webfragment.model.MockSimpleLink;
import com.atlassian.jira.plugin.webfragment.model.MockSimpleLinkSection;
import com.atlassian.jira.plugin.webfragment.model.SimpleLink;
import com.atlassian.jira.plugin.webfragment.model.SimpleLinkSection;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.collect.MapBuilder;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestMainHeaderLinksContextProvider {
    private static final String LOCATION = MainHeaderLinksContextProvider.SYSTEM_TOP_NAVIGATION_BAR;
    private static final String CONTEXT_HEADER_LINKS = MainHeaderLinksContextProvider.CONSTRUCTED_TOPLEVEL_LINKS_KEY;

    private final SimpleLinkManager simpleLinkManagerMock = mock(SimpleLinkManager.class);
    private final JiraAuthenticationContext authenticationContextMock = mock(JiraAuthenticationContext.class);
    private final ApplicationUser userMock = mock(ApplicationUser.class);
    private final JiraHelper helper = new JiraHelper();

    private final SimpleLink nonLazyLinkMock = mock(SimpleLink.class, "nonLazyLink");
    private final SimpleLinkSection subSectionMock = mock(SimpleLinkSection.class);
    private final SimpleLink lazyLinkMock = mock(SimpleLink.class, "lazyLink");
    private final SimpleLink linkMock = mock(SimpleLink.class);

    private final MainHeaderLinksContextProvider provider = new MainHeaderLinksContextProvider(simpleLinkManagerMock, authenticationContextMock);

    @Rule
    public final JiraAuthenticationContextRule authenticationContextRule = new JiraAuthenticationContextRule(authenticationContextMock, userMock);

    @Before
    public void setUp() {
        when(nonLazyLinkMock.getId()).thenReturn("non-lazy-main-link");
        when(subSectionMock.getId()).thenReturn("sub-section");
        when(linkMock.getId()).thenReturn("sub-section-link");

        when(lazyLinkMock.getId()).thenReturn("lazy-main-link");

        when(simpleLinkManagerMock.shouldSectionBeLazy(nonLazyLinkMock.getId())).thenReturn(Boolean.FALSE);
        when(simpleLinkManagerMock.getSectionsForLocation(nonLazyLinkMock.getId(), userMock, helper)).thenReturn(Collections.singletonList(subSectionMock));
        when(simpleLinkManagerMock.getLinksForSection(createCompoundKey(nonLazyLinkMock, subSectionMock), userMock, helper)).thenReturn(Collections.singletonList(linkMock));

        when(simpleLinkManagerMock.shouldLocationBeLazy(lazyLinkMock.getId(), userMock, helper)).thenReturn(Boolean.TRUE);
    }

    @AnonymousUser
    @Test
    public void testAnonymous() {
        final Map<String, Object> result = provider.getContextMap(createContext());
        assertNotNull(result);
        assertThat(result.get(CONTEXT_HEADER_LINKS), is(ContextProviderMatcher.emptyCollection()));
    }


    @AuthenticatedUser
    @Test
    public void testOnlyLazyToplevelLinks() {
        givenSomeSections(lazyLinkMock);
        final Map<String, Object> result = provider.getContextMap(createContext());
        final List<MainHeaderLinksContextProvider.HeaderLink> links = (List<MainHeaderLinksContextProvider.HeaderLink>) result.get(CONTEXT_HEADER_LINKS);
        final MainHeaderLinksContextProvider.HeaderLink link = links.get(0);
        assertThat(link.getIsLazy(), is(true));
        assertThat(link.getLabel(), is(lazyLinkMock.getLabel()));
    }

    @AuthenticatedUser
    @Test
    public void testOnlyNonLazyToplevelLinks() {
        givenSomeSections(nonLazyLinkMock);
        final Map<String, Object> result = provider.getContextMap(createContext());
        final List<MainHeaderLinksContextProvider.HeaderLink> links = (List<MainHeaderLinksContextProvider.HeaderLink>) result.get(CONTEXT_HEADER_LINKS);
        final MainHeaderLinksContextProvider.HeaderLink link = links.get(0);
        assertThat(link.getIsLazy(), is(false));
        assertThat(link.getId(), is(nonLazyLinkMock.getId()));
    }

    @AuthenticatedUser
    @Test
    public void testBothSortsOfToplevelLinks() {
        givenSomeSections(lazyLinkMock, nonLazyLinkMock);
        final Map<String, Object> result = provider.getContextMap(createContext());
        final List<MainHeaderLinksContextProvider.HeaderLink> links = (List<MainHeaderLinksContextProvider.HeaderLink>) result.get(CONTEXT_HEADER_LINKS);
        assertThat(links, hasSize(2));

        assertThat(links.get(0).getId(), equalTo(lazyLinkMock.getId()));
        assertThat(links.get(0).getSections(), hasSize(0));

        assertThat(links.get(1).getId(), equalTo(nonLazyLinkMock.getId()));
        assertThat(links.get(1).getSections(), hasSize(1));
        assertThat(links.get(1).getSections().get(0).getId(), equalTo(subSectionMock.getId()));
        assertThat(links.get(1).getSections().get(0).getItems(), hasSize(1));
        assertThat(links.get(1).getSections().get(0).getItems(), is(ContextProviderMatcher.aListWithValues(linkMock)));
    }

    /**
     * https://jira.atlassian.com/browse/JRA-36859
     */
    @AuthenticatedUser
    @Test
    public void testDuplicateIdInvalidBehaviour() {
        final String duplicatedId = "duplicated-id";
        final String anotherId = "another-id";

        //2 links have exact the same id
        MockSimpleLink duplicatedLink1 = new MockSimpleLink(duplicatedId);
        MockSimpleLink duplicatedLink2 = new MockSimpleLink(duplicatedId);
        MockSimpleLink anotherLink = new MockSimpleLink(anotherId);
        givenSomeSections(duplicatedLink1, anotherLink, duplicatedLink2);

        //and we assume that for given id there are some sections
        //but unfortunately they are indexed by id so there is no way to distinguish those two links
        final MockSimpleLinkSection section1 = new MockSimpleLinkSection("dupl-sect1");
        final MockSimpleLinkSection section2 = new MockSimpleLinkSection("dupl-sect2");
        when(simpleLinkManagerMock.getSectionsForLocation(duplicatedId, userMock, helper)).thenReturn(ImmutableList.<SimpleLinkSection>of(section1, section2));

        final Map<String, Object> result = provider.getContextMap(createContext());
        final List<MainHeaderLinksContextProvider.HeaderLink> links = (List<MainHeaderLinksContextProvider.HeaderLink>) result.get(CONTEXT_HEADER_LINKS);
        assertThat("top level links contain all 3", links, hasSize(3));

        assertThat("first duplicate should have two sections", links.get(0).getSections(), hasSize(2));
        assertThat(links.get(0).getSections().get(0).getId(), equalTo(section1.getId()));
        assertThat(links.get(0).getSections().get(1).getId(), equalTo(section2.getId()));

        assertThat("second duplicate should also have two sections", links.get(2).getSections(), hasSize(2));
        assertThat(links.get(2).getSections().get(0).getId(), equalTo(section1.getId()));
        assertThat(links.get(2).getSections().get(1).getId(), equalTo(section2.getId()));

        assertThat("another link has no sections", links.get(1).getSections(), hasSize(0));
    }

    private String createCompoundKey(@Nonnull final SimpleLink firstKey, @Nonnull final SimpleLinkSection secondKey) {
        return firstKey.getId() + "/" + secondKey.getId();
    }

    private void givenSomeSections(@Nonnull final SimpleLink... sections) {
        when(simpleLinkManagerMock.getLinksForSection(LOCATION, userMock, helper)).thenReturn(Arrays.asList(sections));
    }

    private Map<String, Object> createContext() {
        return MapBuilder.<String, Object>build("helper", helper);
    }
}
