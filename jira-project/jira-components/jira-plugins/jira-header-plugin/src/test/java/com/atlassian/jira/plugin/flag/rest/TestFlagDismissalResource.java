package com.atlassian.jira.plugin.flag.rest;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.flag.FlagDismissalService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import javax.ws.rs.core.Response;

import static javax.ws.rs.core.Response.Status.NO_CONTENT;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class TestFlagDismissalResource {
    private static final String FLAG_KEY = "flag.key";

    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private FlagDismissalService flagDismissalService;
    @Mock
    private ApplicationUser user;

    private FlagDismissalResource flagDismissalResource;

    @Before
    public void setup() {
        initMocks(this);
        when(authenticationContext.getLoggedInUser()).thenReturn(user);
        flagDismissalResource = new FlagDismissalResource(
                authenticationContext, flagDismissalService);
    }

    @Test
    public void testDismissReturnsNoContent() {
        Response dismissalResult =
                flagDismissalResource
                        .dismiss(FLAG_KEY);

        assertThat(
                dismissalResult.getStatus(),
                equalTo(NO_CONTENT.getStatusCode()));
    }

    @Test
    public void testResetReturnsNoContent() {
        Response resetResult =
                flagDismissalResource
                        .reset(FLAG_KEY);

        assertThat(
                resetResult.getStatus(),
                equalTo(NO_CONTENT.getStatusCode()));
    }

    @Test
    public void testDismissDismissesFlagForCurrentUser() {
        flagDismissalResource
                .dismiss(FLAG_KEY);

        verify(flagDismissalService).dismissFlagForUser(FLAG_KEY, user);
    }

    @Test
    public void testResetResetsFlag() {
        flagDismissalResource
                .reset(FLAG_KEY);

        verify(flagDismissalService).resetFlagDismissals(FLAG_KEY);
    }

    @Test
    public void removeDismissFlag() {
        flagDismissalResource.removeDismissFlagForUser(FLAG_KEY);

        verify(flagDismissalService).removeDismissFlagForUser(FLAG_KEY, user);
    }
}
