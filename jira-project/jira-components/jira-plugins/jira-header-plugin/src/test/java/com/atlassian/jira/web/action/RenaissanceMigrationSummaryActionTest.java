package com.atlassian.jira.web.action;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.application.api.ApplicationManager;
import com.atlassian.core.ofbiz.test.UtilsForTests;
import com.atlassian.jira.application.ApplicationKeys;
import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.application.ApplicationRoleAdminService;
import com.atlassian.jira.application.MockApplication;
import com.atlassian.jira.application.MockApplicationRole;
import com.atlassian.jira.application.MockPlatformApplication;
import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.junit.rules.JiraWebActionSupportDependencies;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.MockApplicationProperties;
import com.atlassian.jira.mock.servlet.MockHttpServletResponse;
import com.atlassian.jira.web.landingpage.RenaissanceMigrationPageRedirect;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.atlassian.webresource.api.assembler.RequiredResources;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;
import webwork.action.Action;
import webwork.action.ServletActionContext;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.jira.upgrade.tasks.role.MoveJira6xABPServiceDeskPermissions.PROPERTY_SERVICEDESK_NOT_MIGRATED_GROUPS;
import static com.atlassian.jira.web.landingpage.RenaissanceMigrationPageRedirect.PROPERTY_POST_MIGRATION_PAGE_DISPLAYED;
import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RenaissanceMigrationSummaryActionTest {
    @Rule
    public final MockitoContainer mockContainer = MockitoMocksInContainer.rule(this);
    @Rule
    public TestRule webActionSupport = JiraWebActionSupportDependencies.build(mockContainer);

    @Mock
    private ApplicationManager applicationManager;
    @Mock
    private RenaissanceMigrationPageRedirect pageRedirect;
    @Mock
    private PageBuilderService pageBuilderService;
    @Mock
    private ApplicationRoleAdminService applicationRoleAdminService;


    private MockApplicationProperties applicationProperties;
    private MockHttpServletResponse response;

    private RenaissanceMigrationSummaryAction action;

    @Before
    public void setup() {
        UtilsForTests.cleanWebWork();

        WebResourceAssembler assembler = mock(WebResourceAssembler.class);
        when(assembler.resources()).thenReturn(mock(RequiredResources.class));
        when(pageBuilderService.assembler()).thenReturn(assembler);

        response = new MockHttpServletResponse();
        ServletActionContext.setResponse(response);

        applicationProperties = new MockApplicationProperties();
        action = new RenaissanceMigrationSummaryAction(applicationManager, applicationProperties, pageRedirect,
                pageBuilderService, applicationRoleAdminService);
    }

    @Test
    public void dismissSetsDisplayedPropertyAndRedirectsToJiraHome() throws Exception {
        action.setDismiss(true);

        String result = action.doExecute();

        assertThat(result, is(Action.NONE));
        assertThat(response.getRedirect(), is("/secure/MyJiraHome.jspa"));
        assertTrue(applicationProperties.getOption(PROPERTY_POST_MIGRATION_PAGE_DISPLAYED));
    }

    @Test
    public void noDismissReturnsSuccess() throws Exception {
        action.setDismiss(false);

        assertThat(action.doExecute(), is(Action.SUCCESS));
    }

    @Test
    public void coreApplicationIsNotReturnedInDataIfThereIsAnotherApp() {
        when(applicationRoleAdminService.getRoles()).thenReturn(ServiceOutcomeImpl.ok(ImmutableSet.of()));
        when(applicationManager.getApplications()).thenReturn(Arrays.asList(new MockApplication(ApplicationKeys.CORE),
                new MockApplication(ApplicationKeys.SERVICE_DESK)));
        when(applicationManager.getPlatform()).thenReturn(new MockPlatformApplication(ApplicationKeys.CORE));

        Map<String, Object> data = action.getData();

        assertThat(data.get("productName"), is(ApplicationKeys.SERVICE_DESK + "-name"));
        assertThat(data.get("productVersion"), is(ApplicationKeys.SERVICE_DESK + "-version"));
    }

    @Test
    public void coreApplicationIsReturnedInDataIfThisIsOnlyProduct() {
        when(applicationRoleAdminService.getRoles()).thenReturn(ServiceOutcomeImpl.ok(ImmutableSet.of()));
        when(applicationManager.getApplications()).thenReturn(singletonList(new MockPlatformApplication(ApplicationKeys.CORE)));
        when(applicationManager.getPlatform()).thenReturn(new MockPlatformApplication(ApplicationKeys.CORE));
        Map<String, Object> data = action.getData();

        assertThat(data.get("productName"), is(ApplicationKeys.CORE + "-name"));
        assertThat(data.get("productVersion"), is(ApplicationKeys.CORE + "-version"));
    }

    @Test
    public void serviceDeskGroupsAreReturnedIfAny() {
        when(applicationRoleAdminService.getRoles()).thenReturn(ServiceOutcomeImpl.ok(ImmutableSet.of()));
        when(applicationManager.getApplications()).thenReturn(singletonList(new MockPlatformApplication(ApplicationKeys.CORE)));
        when(applicationManager.getPlatform()).thenReturn(new MockPlatformApplication(ApplicationKeys.CORE));

        applicationProperties.setString(PROPERTY_SERVICEDESK_NOT_MIGRATED_GROUPS, "group1, group2, group3");
        Map<String, Object> data = action.getData();

        assertThat((List<String>) data.get("notMigratedGroups"), containsInAnyOrder("group1", "group2", "group3"));
    }

    @Test
    public void notDefinedRolesAreReturnedIfAny() {
        when(applicationManager.getApplications()).thenReturn(singletonList(new MockPlatformApplication(ApplicationKeys.CORE)));
        when(applicationManager.getPlatform()).thenReturn(new MockPlatformApplication(ApplicationKeys.CORE));

        ApplicationKey keyDefined = ApplicationKey.valueOf("defined");
        ApplicationKey keyNotDefined = ApplicationKey.valueOf("notDefined");

        ApplicationRole definedApplicationRole = new MockApplicationRole(keyDefined).defined(true);
        ApplicationRole notDefinedApplicationRole = new MockApplicationRole(keyNotDefined).defined(false);

        final Set<ApplicationRole> applicationRoleSet = ImmutableSet.of(definedApplicationRole, notDefinedApplicationRole);
        when(applicationRoleAdminService.getRoles()).thenReturn(ServiceOutcomeImpl.ok(applicationRoleSet));
        Map<String, Object> data = action.getData();
        assertThat((Collection<ApplicationRole>) data.get("notDefinedRoles"), hasSize(1));
        assertThat((Collection<ApplicationRole>) data.get("notDefinedRoles"), contains(notDefinedApplicationRole));
    }

    @Test
    public void notDefinedRolesAreEmptyIfNone() {
        when(applicationManager.getApplications()).thenReturn(singletonList(new MockPlatformApplication(ApplicationKeys.CORE)));
        when(applicationManager.getPlatform()).thenReturn(new MockPlatformApplication(ApplicationKeys.CORE));

        when(applicationRoleAdminService.getRoles()).thenReturn(ServiceOutcomeImpl.ok(Collections.emptySet()));
        Map<String, Object> data = action.getData();
        assertThat((Collection<ApplicationRole>) data.get("notDefinedRoles"), hasSize(0));

        when(applicationRoleAdminService.getRoles()).thenReturn(ServiceOutcomeImpl.ok(null));
        data = action.getData();
        assertThat((Collection<ApplicationRole>) data.get("notDefinedRoles"), hasSize(0));
    }
}