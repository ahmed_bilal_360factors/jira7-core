package com.atlassian.jira.plugin.flag.rest;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.flag.FlagDismissalService;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Rest resource to access the plugin's dark feature, allowing clients to enable or disable the plugin.
 */
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@Path("/flags/{flagKey}")
public class FlagDismissalResource {
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final FlagDismissalService flagDismissalService;

    public FlagDismissalResource(
            JiraAuthenticationContext jiraAuthenticationContext,
            FlagDismissalService flagDismissalService) {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.flagDismissalService = flagDismissalService;
    }

    @PUT
    @Path("/dismiss")
    @AnonymousAllowed
    public Response dismiss(@PathParam("flagKey") String flagKey) {
        final ApplicationUser user = jiraAuthenticationContext.getLoggedInUser();
        flagDismissalService.dismissFlagForUser(flagKey, user);
        return Response.noContent().build();
    }

    @PUT
    @Path("/reset")
    public Response reset(@PathParam("flagKey") String flagKey) {
        flagDismissalService.resetFlagDismissals(flagKey);
        return Response.noContent().build();
    }

    @PUT
    @Path("/removeDismiss")
    public Response removeDismissFlagForUser(@PathParam("flagKey") String flagKey) {
        final ApplicationUser user = jiraAuthenticationContext.getLoggedInUser();
        flagDismissalService.removeDismissFlagForUser(flagKey, user);
        return Response.noContent().build();
    }
}
