package com.atlassian.jira.plugin.headernav.newsletter;

import com.atlassian.fugue.Option;
import com.atlassian.jira.help.HelpUrls;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.webresource.api.data.WebResourceDataProvider;
import com.google.common.collect.Maps;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

/**
 * Data provider to return if a newsletter signup tooltip should be shown the currently logged in user.
 *
 * @since v6.4
 */
public class NewsletterSignupDataProvider implements WebResourceDataProvider {
    private final JiraAuthenticationContext authenticationContext;
    private final NewsletterUserPreferencesManager newsletterUserPreferencesManager;
    private final HelpUrls helpUrls;
    private final I18nHelper i18nHelper;

    public NewsletterSignupDataProvider(
            final JiraAuthenticationContext authenticationContext,
            final NewsletterUserPreferencesManager newsletterUserPreferencesManager,
            final HelpUrls helpUrls,
            final I18nHelper i18nHelper) {
        this.authenticationContext = authenticationContext;
        this.newsletterUserPreferencesManager = newsletterUserPreferencesManager;
        this.helpUrls = helpUrls;
        this.i18nHelper = i18nHelper;
    }

    @Override
    public Jsonable get() {
        return new Jsonable() {
            @Override
            public void write(final Writer writer) throws IOException {
                try {
                    getJsonData().write(writer);
                } catch (JSONException e) {
                    throw new JsonMappingException(e);
                }
            }
        };
    }

    private JSONObject getJsonData() {
        final Map<String, Object> values = Maps.newHashMap();
        ApplicationUser applicationUser = authenticationContext.getUser();
        final Option<ApplicationUser> user = Option.option(applicationUser);

        values.put("showNewsletterTip", newsletterUserPreferencesManager.shouldShowSignupTip(user));
        values.put("formUrl", helpUrls.getUrl("newsletter.signup.form").getUrl());
        values.put("signupTitle", i18nHelper.getText("newsletter.signup.tip.title"));
        values.put("signupDescription",  i18nHelper.getText("newsletter.signup.tip.description"));
        values.put("signupId", "newsletter-signup-tip");

        if (user.isDefined()) {
            values.put("userEmail", user.get().getEmailAddress());
        }

        return new JSONObject(values);
    }
}