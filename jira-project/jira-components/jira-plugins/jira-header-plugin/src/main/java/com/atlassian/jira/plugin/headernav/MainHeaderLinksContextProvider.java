package com.atlassian.jira.plugin.headernav;

import com.atlassian.jira.plugin.webfragment.SimpleLinkManager;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.plugin.webfragment.model.SimpleLink;
import com.atlassian.jira.plugin.webfragment.model.SimpleLinkSection;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.util.dbc.Assertions;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.ContextProvider;
import com.google.common.collect.Lists;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MainHeaderLinksContextProvider implements ContextProvider {
    static final String SYSTEM_TOP_NAVIGATION_BAR = "system.top.navigation.bar";
    static final String CONSTRUCTED_TOPLEVEL_LINKS_KEY = "headerLinks";

    private final SimpleLinkManager simpleLinkManager;
    private final JiraAuthenticationContext authenticationContext;

    public MainHeaderLinksContextProvider(@Nonnull final SimpleLinkManager simpleLinkManager, @Nonnull final JiraAuthenticationContext authenticationContext) {
        this.simpleLinkManager = Assertions.notNull(simpleLinkManager);
        this.authenticationContext = Assertions.notNull(authenticationContext);
    }

    @Override
    public void init(Map<String, String> params) throws PluginParseException {
    }

    @Override
    public Map<String, Object> getContextMap(Map<String, Object> context) {
        final ApplicationUser user = authenticationContext.getUser();
        final JiraHelper helper = (JiraHelper) context.get("helper");
        final List<SimpleLink> mainHeaderLinks = simpleLinkManager.getLinksForSection(SYSTEM_TOP_NAVIGATION_BAR, user, helper);
        final List<HeaderLink> constructedMainHeaderLinks = constructHeaderLinks(mainHeaderLinks, user, helper);

        return MapBuilder.newBuilder(context)
                .add(CONSTRUCTED_TOPLEVEL_LINKS_KEY, Lists.newArrayList(constructedMainHeaderLinks))
                .toHashMap();
    }

    private List<HeaderLink> constructHeaderLinks(@Nonnull List<SimpleLink> links, @Nullable final ApplicationUser user, @Nonnull final JiraHelper helper) {
        return links.stream().map(link -> {
            final String id  = link.getId();
            final boolean isLazy = simpleLinkManager.shouldLocationBeLazy(id, user, helper);
            final List<HeaderDropdownSection> sections;
            if (!isLazy) {
                final List<SimpleLinkSection> simpleSections = simpleLinkManager.getSectionsForLocation(id, user, helper);
                sections = simpleSections.stream().map(section -> {
                    final String sectionKey = id + "/" + section.getId();
                    final List<SimpleLink> linksForSection = simpleLinkManager.getLinksForSection(sectionKey, user, helper);
                    return new HeaderDropdownSection(section, linksForSection);
                }).collect(Collectors.toList());
            } else {
                sections = Collections.emptyList();
            }
            return new HeaderLink(link, isLazy, sections);
        }).collect(Collectors.toList());
    }

    public class HeaderLink {
        private SimpleLink link;
        private boolean isLazy;
        private List<HeaderDropdownSection> sections;

        HeaderLink(SimpleLink link, boolean isLazy, List<HeaderDropdownSection> sections) {
            this.link = link;
            this.isLazy = isLazy;
            this.sections = sections;
        }

        public String getId() {
            return link.getId();
        }

        public String getTitle() {
            return link.getTitle();
        }

        public String getLabel() {
            return link.getLabel();
        }

        public String getAccessKey() {
            return link.getAccessKey();
        }

        public String getUrl() {
            return link.getUrl();
        }

        public String getStyleClass() {
            return link.getStyleClass();
        }

        public boolean getIsLazy() {
            return isLazy;
        }

        public List<HeaderDropdownSection> getSections() {
            return sections;
        }
    }

    public class HeaderDropdownSection {
        private String style;
        private String id;
        private String label;
        private List<SimpleLink> items;

        HeaderDropdownSection(SimpleLinkSection section, List<SimpleLink> links) {
            this.id = section.getId();
            this.label = section.getLabel();
            this.style = section.getStyleClass();
            this.items = links;
        }

        public String getStyle() {
            return style;
        }

        public String getId() {
            return id;
        }

        public String getLabel() {
            return label;
        }

        public List<SimpleLink> getItems() {
            return items;
        }
    }
}
