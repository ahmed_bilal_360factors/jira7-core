/**
 * Event listener to fire Analytics events when the home button is clicked
 */

/* global AJS */
require(['jira/util/logger', 'jquery'], function(logger, $) {

    var standardAnalyticsPrefix = 'jira.navigation.header.';

    var traceString = 'jira.header.analytics.event';

    var isStandardTopLevelMenuItem = function(headerMenuDropdownIds, dropDownId) {
        return headerMenuDropdownIds.some(function(menuControl) {
            return menuControl.id === dropDownId;
        });
    };

    function getAnalyticsKeyFromId(headerMenuDropdownIds, dropDownId) {
        var analyticsEventString = undefined;
        headerMenuDropdownIds.forEach(function(headerMenuObject){
            if (headerMenuObject.id === dropDownId) {
                analyticsEventString = headerMenuObject.analyticEventKey;
            }
        });
        return analyticsEventString;
    }

    var fireStandardMenuEvent = function(dropDownId,headerMenuDropdownIds, eventSuffix) {
        var analyticsEventString = getAnalyticsKeyFromId(headerMenuDropdownIds, dropDownId);

        var eventName = standardAnalyticsPrefix + analyticsEventString + eventSuffix;
        AJS.EventQueue.push({
            name: eventName
        });
        logger.trace(traceString);
    };

    var $document = $(document);

    $document.on('click', '#logo', function() {
        AJS.EventQueue.push({
            name: standardAnalyticsPrefix + 'home'
        });
        logger.trace(traceString);
    });


    var standardHeaderMenuItemDropdownIds = [
        {id:'home_link-content',analyticEventKey:'dashboards'},
        {id:'browse_link-content',analyticEventKey:'projects'},
        {id:'find_link-content',analyticEventKey:'issues'},
        {id:'tempo_menu-content',analyticEventKey:'tempo'},
        {id:'bonfire_top_menu_dropdown-content',analyticEventKey:'capture'},
        {id:'greenhopper_menu-content',analyticEventKey:'boards'},
        {id:'plugins-jira-webitem-main-content',analyticEventKey:'portfolio'},
        {id:'system-help-menu-content',analyticEventKey:'help'},
        {id:'system-admin-menu-content',analyticEventKey:'admin'}
    ];

    var triggerAjaxDropdownEventsSelector = '#system-help-menu-content, #user-options-content, #system-admin-menu-content';


    var headerStringToBindDropdownListener = standardHeaderMenuItemDropdownIds.map(function(headerMenuItemObject){
        return '#' + headerMenuItemObject.id;
    }).join(', ');


    $document.on('aui-dropdown2-show',headerStringToBindDropdownListener, function() {
        var dropDownId = $(this).attr('id');
        if (isStandardTopLevelMenuItem(standardHeaderMenuItemDropdownIds, dropDownId)) {
            fireStandardMenuEvent(dropDownId, standardHeaderMenuItemDropdownIds, '.open');
        }
    });

    $document.on('aui-dropdown2-hide',headerStringToBindDropdownListener,function(){
        var dropDownId = $(this).attr('id');

        if (isStandardTopLevelMenuItem(standardHeaderMenuItemDropdownIds, dropDownId)) {
            fireStandardMenuEvent(dropDownId, standardHeaderMenuItemDropdownIds, '.close');
        }
    });

    /* Help, admin and profile dropdown components are not ajax-dropdown any more, therefore they stopped
        triggering aui-dropdown2-show-before and aui-dropdown2-show-after events, which are necessary to
        trace the analytics of those components.
       Here we are triggering those events manually.
     */
    $document.on('aui-dropdown2-show', triggerAjaxDropdownEventsSelector, function(){
        var dropdown = $(this);
        dropdown.trigger('aui-dropdown2-show-before');
        dropdown.trigger('aui-dropdown2-show-after');
    });


    var headerStringToBindClickListener = standardHeaderMenuItemDropdownIds.reduce(function(finalString,headerMenuItemObject){
        return finalString + 'a.aui-dropdown2-trigger[aria-owns=' + headerMenuItemObject.id + '], ';
    },"");


    $document.on('click',headerStringToBindClickListener,function(){
        var dropDownId = this.getAttribute('aria-controls');
        if (isStandardTopLevelMenuItem(standardHeaderMenuItemDropdownIds, dropDownId)) {
            fireStandardMenuEvent(dropDownId, standardHeaderMenuItemDropdownIds, '.click');
        }
    });

    $document.on('click', '.aui-dropdown2 .aui-dropdown2-section a', function(event) {

        var auiDropdown = $(event.target).closest(headerStringToBindDropdownListener).first();

        if (auiDropdown === null) {
            return;
        }

        var clickIndex = auiDropdown.find('a').index(event.target);
        var auiDropdownId = auiDropdown.attr('id');
        var topLevelMenuItemText = getAnalyticsKeyFromId(standardHeaderMenuItemDropdownIds,auiDropdownId);

        AJS.EventQueue.push({
            name: standardAnalyticsPrefix + topLevelMenuItemText + '.item.click',
            itemIndex: clickIndex
        });
        logger.trace(traceString);
    });
});


