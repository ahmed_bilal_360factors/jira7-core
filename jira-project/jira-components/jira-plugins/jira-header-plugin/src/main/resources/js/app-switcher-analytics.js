/**
 * Adds analytics to the app switcher (hamburger menu in the top left corner). There are events for opening and closing
 * the menu, and an event for clicking on an entry.
 */
require(['jquery'], function ($) {
    // App switcher open.
    $(document).on('aui-dropdown2-show-before', '#app-switcher', function () {
        AJS.trigger('analyticsEvent', {
            name: 'navigation.header.appswitcher.open'
        });
    });

    $(document).on('click', '#app-switcher a, #app-switcher button', function (event) {
        var $el = $(event.target);
        var index = $el.closest("li").index();
        var linkType;

        if ($el.is('.nav-link-edit')) {
            linkType = 'configure';
        } else if ($el.is('.app-discovery-link')) {
            // The id of app-discovery-link is the name of the product, e.g. 'bamboo' or 'confluence'.
            linkType = this.id ? this.id : "unknown.app.discovery";
        } else if ($el.is('.app-discovery-cancel-button')) {
            linkType = 'dont.show';
            // Because of the different DOM structure of the "Don't show this again" button, the method used to
            // calculate the index returns -1, so the index is instead explicitly set to 0.
            index = 0;
        } else {
            linkType = 'application';
        }

        AJS.trigger('analyticsEvent', {
            name: 'navigation.header.appswitcher.click',
            data: {
                position: index,
                linkType: linkType
            }
        });
    });

    // App switcher close.
    $(document).on('aui-dropdown2-hide', '#app-switcher', function () {
        AJS.trigger('analyticsEvent', {
            name: 'navigation.header.appswitcher.close'
        });
    });
});
