require([
    'jquery',
    'jira/shifter'
], function ($,
    Shifter) {
    $(document).on("click", "#admin-search-link", function (e) {
        Shifter.show();
        e.preventDefault();
    });
});
