/**
 * JIRA's implementation of AUI Dropdown2 remote API (https://extranet.atlassian.com/display/AUI/Dropdown2+remote+API).
 *
 * This predates AUI Dropdown2, and allows the application header menus to be dynamic and populated via AJAX. This is
 * required for menus that are dynamic (e.g. Issues or Projects).
 *
 * In addition to the normal `aui-dropdown2-show` event that is triggered on the dropdown, two extra events are
 * triggered to expose the asynchronous nature of the menus:
 *
 * - `aui-dropdown2-show-before` is triggered _before_ a dropdown is shown (due to implementation details, this is
 *    unfortunately triggered after `aui-dropdown2-show`).
 * - `aui-dropdown2-show-after` is triggered when the dropdown is populated and visible to the user. It is triggered for
 *    both remote dropdowns and normal dropdowns.
 *
 * The class `aui-dropdown2-loading` is added to the dropdown between `aui-dropdown2-show-before` and
 * `aui-dropdown2-show-after`.
 */
require([
    'jira/ajs/ajax/smart-ajax',
    'jira/skate',
    'jira/util/data/meta',
    'wrm/context-path',
    'jquery'
], function (
    SmartAjax,
    skate,
    Meta,
    wrmContextPath,
    $
) {
    /**
     * JIRA's extension of Dropdown2 to remotely fetch items from the server.
     * Currently exists for the following reasons:
     *
     * - Applies AUI web components to JIRA's own menu JSON data structure (see: SimpleLink.java)
     * - Supports injection of CSS classes and attributes in the AUI markup pattern, which
     * - Supports binding of dialog and other dynamic handlers to menu items
     * - Adds some keyboard accessibility issue patches that aren't in AUI 5.9.17
     *
     * This predates Dropdown2 remote, and should be removed when Dropdown2 remote API is sufficient.
     */
    skate('aui-dropdown2-ajax', {
        type: skate.type.CLASSNAME,
        attached: function ajaxDropdownAttached(el) {
            var $dropdown = $(document.getElementById(el.getAttribute('aria-controls')));
            var $trigger = $(el);
            var ajaxKey = $dropdown.data('aui-dropdown2-ajax-key');

            // START JSEV-483 - workaround for the delayed loading of items
            var FIRST = 0; // timeout for setting keyboard interaction value
            var SECOND = FIRST + 4; // timeout to reset the previous interaction. Should occur on next tick in JS
            function focusDropdownItemIfNecessary() {
                var triggerIsActive = (document.activeElement === $trigger.get(0));
                if (triggerIsActive) {
                    $dropdown.get(0).focusItem(0);
                }
            }
            function resetLazyKbdState() {
                $trigger.data('kbd-interacted', false);
            }
            $trigger.on('keydown.aui-dropdown2-ajax', function() {
                $trigger.data('kbd-interacted', true);
                setTimeout(resetLazyKbdState, SECOND);
            });
            $dropdown.on('aui-dropdown2-show-after.aui-dropdown2-ajax', resetLazyKbdState);
            resetLazyKbdState();
            // END JSEV-483

            $dropdown.on('aui-dropdown2-show.aui-dropdown2-ajax', function () {
                $dropdown.trigger('aui-dropdown2-show-before');

                if (ajaxKey) {
                    var checkedKeyboard = $.Deferred();
                    var fetchedDropdownItems = $.Deferred();
                    setTimeout(function() {
                        checkedKeyboard.resolve($trigger.data('kbd-interacted'));
                    }, FIRST);

                    $dropdown.empty();
                    $dropdown.addClass('aui-dropdown2-loading');
                    $trigger.attr('aria-busy', 'true'); // JSEV-483 - use same a11y pattern as AUI to denote contents loading

                    SmartAjax.makeRequest({
                        url: wrmContextPath() + '/rest/api/1.0/menus/' + ajaxKey,
                        data: {
                            inAdminMode: Meta.getBoolean('in-admin-mode')
                        },
                        dataType: 'json',
                        cache: false,
                        success: function(data) { fetchedDropdownItems.resolve(data); },
                        error: fetchedDropdownItems.reject
                    });

                    $.when(checkedKeyboard, fetchedDropdownItems).done(function(openedByKeyboard, data) {
                        $dropdown.html(JIRA.Templates.Menu.Dropdowns.dropdown2Fragment(data));
                        if (openedByKeyboard) {
                            setTimeout(focusDropdownItemIfNecessary, 0);
                        }
                    }).always(function() {
                        $trigger.attr('aria-busy', 'false'); // JSEV-483 - use same a11y pattern as AUI to denote contents loading
                        $dropdown.removeClass('aui-dropdown2-loading');
                        $dropdown.trigger('aui-dropdown2-show-after');
                    });
                } else {
                    $dropdown.trigger('aui-dropdown2-show-after');
                }
            });
        },
        detached: function ajaxDropdownRemoved(el) {
            var $dropdown = $(document.getElementById(el.getAttribute('aria-controls')));
            var $trigger = $(el);

            $dropdown.off('.aui-dropdown2-ajax');
            $trigger.off('.aui-dropdown2-ajax');
        }
    });
});

// create-header does not propagate aui-dropdoown2-ajax class
// details: https://bitbucket.org/atlassian/aui/annotate/672223b2f19a3c0d25f5bc7bff795861f70579ae/src/js/aui/internal/header/create-header.js?at=5.10.x&fileviewer=file-view-default#create-header.js-195
(function($) {
    $(document).on('aui-responsive-menu-item-created', '.aui-header.aui-dropdown2-trigger-group', function(e) {
        var detail = e.originalEvent.detail;
        if ($(detail.originalItem).find('> .aui-dropdown2-ajax').length > 0) {
            $(detail.newItem).find('> .aui-dropdown2-trigger').addClass('aui-dropdown2-ajax');
        }
    });
})(require("jquery"));
