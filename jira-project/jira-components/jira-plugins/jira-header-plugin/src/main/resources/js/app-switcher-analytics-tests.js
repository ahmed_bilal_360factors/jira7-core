AJS.test.require(['com.atlassian.jira.jira-header-plugin:jira-header'], function () {

    var $ = require("jquery");

    module('App switcher analytics', {
        setup: function () {
            this.appSwitcher = $("<div>").attr("id", "app-switcher").appendTo("#qunit-fixture");
            sinon.spy(AJS, "trigger");

            // Build up the app switcher dropdown menu.
            var appLinksSection = $("<div>").attr("class", "aui-dropdown2-section").appendTo(this.appSwitcher);
            var configureSection = $("<div>").attr("class", "aui-dropdown2-section").appendTo(this.appSwitcher);
            var appDiscoverySection = $("<div>").attr("class", "aui-dropdown2-section").appendTo(this.appSwitcher);

            // Build application links section.
            var appLinksLi = new Array(3);
            this.appLinks = new Array(3);
            for (var i = 0; i < appLinksLi.length; i++) {
                appLinksLi[i] = $("<li>").appendTo(appLinksSection);
                this.appLinks[i] = $("<a>").appendTo(appLinksLi[i]);
            }

            // Build configuration section.
            var configureLi = $("<li>").appendTo(configureSection);
            this.configureLink = $("<a>").attr("class", "nav-link-edit").appendTo(configureLi);

            // Build app discovery section.
            var appDiscoveryLi = new Array(2);
            this.appDiscoveryLinks = new Array(2);
            for (var i = 0; i < appDiscoveryLi.length; i++) {
                appDiscoveryLi[i] = $("<li>").appendTo(appDiscoverySection);
                this.appDiscoveryLinks[i] = $("<a>")
                    .attr("class", "app-discovery-link")
                    .appendTo(appDiscoveryLi[i]);
            }
            this.appDiscoveryLinks[0].attr("id", "confluence");

            // Build don't show again section. Note that the DOM structure for this is different to the other links in
            // the app switcher.
            this.dontShowAgainLink = $("<button>")
                .attr("class", "app-discovery-cancel-button")
                .appendTo(this.appSwitcher);
        },

        teardown: function () {
            AJS.trigger.restore();
            this.appSwitcher.remove();
        }
    });

    test("Analytics event when app switcher menu is opened", function () {
        this.appSwitcher.trigger('aui-dropdown2-show-before');

        ok(AJS.trigger.calledWith('analyticsEvent', {
                name: 'navigation.header.appswitcher.open'
            }),
            "Open app switcher menu analytics event");
    });

    test("Analytics event when app switcher menu is closed", function () {
        this.appSwitcher.trigger('aui-dropdown2-hide');

        ok(AJS.trigger.calledWith('analyticsEvent', {
                name: 'navigation.header.appswitcher.close'
            }),
            "Close app switcher menu analytics event");
    });

    test("Analytics event when app switcher menu item is clicked", function () {
        this.appLinks[1].trigger('click');
        ok(AJS.trigger.calledWith('analyticsEvent', {
            name: 'navigation.header.appswitcher.click',
            data: {
                position: 1,
                linkType: "application"
            }
        }));
    });

    test("Analytics event when configure is clicked", function () {
        this.configureLink.trigger('click');
        ok(AJS.trigger.calledWith('analyticsEvent', {
            name: 'navigation.header.appswitcher.click',
            data: {
                position: 0,
                linkType: "configure"
            }
        }));
    });

    test("Analytics event when try app link is clicked", function () {
        this.appDiscoveryLinks[0].trigger('click');
        ok(AJS.trigger.calledWith('analyticsEvent', {
            name: 'navigation.header.appswitcher.click',
            data: {
                position: 0,
                linkType: "confluence"
            }
        }));

        // Check for default behavior where no id is present.
        this.appDiscoveryLinks[1].trigger('click');
        ok(AJS.trigger.calledWith('analyticsEvent', {
            name: 'navigation.header.appswitcher.click',
            data: {
                position: 1,
                linkType: "unknown.app.discovery"
            }
        }));
    });

    test("Analytics event when don't show this again is clicked", function () {
        this.dontShowAgainLink.trigger('click');
        ok(AJS.trigger.calledWith('analyticsEvent', {
            name: 'navigation.header.appswitcher.click',
            data: {
                position: 0,
                linkType: "dont.show"
            }
        }));
    });
});
