AJS.test.require(["com.atlassian.jira.jira-header-plugin:newsletter-signup-tip"], function () {
    var jQuery = require("jquery");
    var HelpTipManager = require("jira-help-tips/feature/help-tip-manager");
    module('Newsletter render code', {
        setup: function () {
            this.newsLetterSignup = require('jira/newsletter/signuptip');
            this.container = jQuery("<div>").attr("id", "content").appendTo("#qunit-fixture");
            this.trigger = jQuery("<div id=\"user-options\"></div>").appendTo("#qunit-fixture");

            this.sandbox = sinon.sandbox.create();
            this.sandbox.useFakeServer();
            this.sandbox.useFakeTimers();

            HelpTipManager.dismissedTipIds = [];
        },

        teardown: function () {
            jQuery("body").off("submit", "form.aui.insiders-signup-form"); //unbind the body submit handler to ensure consecutive tests don't interfere with each other

            this.sandbox.clock.tick(0);
            this.sandbox.restore();
        },

        _initNewsletterSignupDialog: function (email, url) {
            this.newsLetterSignup.render({
                userEmail: email,
                formUrl: url,
                signupTitle: "title",
                signupDescription: "description",
                signupId: "id"
            });

            this.sandbox.clock.tick(0); //give the helptip some time to show
        }
    });

    test("Test e-mail validation", function () {
        this._initNewsletterSignupDialog("test@example.com", "https://www.atlassian.com/newsletter/signup");

        this.container.find("input[name=jira-newsletter-user-email]").val("invalidemail!!!");
        this.container.find("form.aui.insiders-signup-form").submit();

        equal(this.container.find("input[name=jira-newsletter-user-email]").next(".error").text(), "newsletter.signup.tip.error.email", "Should have rendered an error about e-mail");
        equal(this.sandbox.server.requests.length, 0, "Invalid form should not call any request.");
    });

    test("Test opt-in validation", function () {
        this._initNewsletterSignupDialog("test@example.com", "https://www.atlassian.com/newsletter/signup");

        this.container.find("input[name=jira-newsletter-user-email]").val("test@email.com");
        this.container.find("form.aui.insiders-signup-form").submit();

        equal(this.container.find("input[name=jira-newsletter-opt-in]").parent().next(".error").text(), "newsletter.signup.tip.error.opt-in", "Should have rendered an error about required opt-in");

        equal(this.sandbox.server.requests.length, 0, "Invalid form should not call any request.");
    });

    test("Valid e-mail submit triggers cross-domain request to address entered", function () {
        this._initNewsletterSignupDialog("valid@email.com", "https://www.atlassian.com/newsletter/signup/{0}");

        this.container.find("input[name=jira-newsletter-user-email]").val("valid@email.com");
        this.container.find("select[name=jira-newsletter-role]").val("qa");
        this.container.find("input[name=jira-newsletter-opt-in]").click();
        this.container.find("form.aui.insiders-signup-form").submit();

        equal(this.sandbox.server.requests[0].method, "POST", "Correct request method used");
        equal(this.sandbox.server.requests[0].url, "https://www.atlassian.com/newsletter/signup/valid@email.com", "Correct url used");
        equal(this.sandbox.server.requests[0].requestBody, JSON.stringify({ role: "qa" }), "Selected role attached to request");
    });

    test("Test description and title rendered correctly", function () {
        this._initNewsletterSignupDialog("someOtherMail@test.com", "https://www.atlassian.com/newsletter/signup2/{0}");

        var title = this.container.find("h2.helptip-title").text();
        var description = this.container.find("div>p").text();

        equal(title, "title", "Title rendered correctly");
        equal(description, "description", "Description rendered correctly");
    });
});
