define('jira/newsletter/signuptip', [
    'jira/util/formatter',
    'jira/newsletter/roles',
    'jira-help-tips/feature/help-tip',
    'underscore',
    'jquery'
], function(formatter, roles, HelpTip, _, $) {

    // taken from setup-mac-util.js
    // http://www.w3.org/TR/html5/forms.html#valid-e-mail-address without &
    var emailRegex = /^[a-zA-Z0-9.!#$%'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

    function validateEmail(email) {
        if (email.length > 255) {
            return false;
        }
        return emailRegex.test(email);
    }

    function checksum(value) {
        return _.reduce(value, function(r, ch) {
            r = ((r << 5) - r) + ch.charCodeAt(0);
            return r & r;
        }, 0);
    }

    return {
        render : function (newsletterFormDetails, helpTipOptions) {
            var defaults = {
                anchor: "#user-options",
                isSequence: false,
                showCloseButton: false
            };

            defaults.id = newsletterFormDetails.signupId;
            defaults.title = newsletterFormDetails.signupTitle;
            defaults.bodyHtml = JIRA.Templates.newsletterSignupTip({
                userEmail: newsletterFormDetails.userEmail,
                description: newsletterFormDetails.signupDescription,
                roles: roles.getRoles()
            });

            var tip = new HelpTip($.extend(defaults, helpTipOptions));
            if(tip.isDismissed()) {
                return;
            }

            tip.show();

            AJS.trigger('analyticsEvent', { name: "jira.newsletter.signuptip.shown" });

            var $body = $("body");
            $body.on("submit", "form.aui.insiders-signup-form", function (e) {
                e.preventDefault();

                var $form = $(this);
                $form.find(".error").remove();

                var $emailInput = $form.find("#jira-newsletter-user-email");
                var $roleSelect = $form.find("#jira-newsletter-role");
                var $optInCheckbox = $form.find("#jira-newsletter-opt-in");
                var email = $emailInput.val();
                var role = $roleSelect.val();
                var optIn = $optInCheckbox.prop('checked');

                var emailIsValid = validateEmail(email);
                var optInSelected = optIn === true;

                var formIsValid = emailIsValid && optInSelected;

                if (formIsValid) {
                    $.ajax({
                        type: 'POST',
                        url: formatter.format(newsletterFormDetails.formUrl, encodeURI(email)),
                        dataType: 'json',
                        contentType: 'application/json',
                        data: JSON.stringify({
                            role: role
                        })
                    }).success(function() {
                        AJS.trigger('analyticsEvent', {
                            name: "jira.newsletter.signuptip.submitted",
                            data: {
                                role: role,
                                // https://extranet.atlassian.com/x/aDhGvw
                                checksum: checksum(email.split('@')[0])
                            }
                        });
                    }).error(function(xhr) {
                        AJS.trigger('analyticsEvent', {
                            name: "jira.newsletter.signuptip.error",
                            data: {
                                statusCode: xhr.status
                            }
                        });
                    });

                    if(newsletterFormDetails.userEmail !== email) {
                        AJS.trigger('analyticsEvent', { name: "jira.newsletter.signuptip.email.changed" });
                    }

                    tip.dismiss("newslettersubscribed");
                }
                else {
                    if (!emailIsValid) {
                        AJS.trigger('analyticsEvent', {name: "jira.newsletter.signuptip.email.validationerror"});
                        $emailInput.after(aui.form.fieldError({message: formatter.I18n.getText('newsletter.signup.tip.error.email')})); // eslint-disable-line no-undef
                    }
                    if (!optInSelected) {
                        AJS.trigger('analyticsEvent', {name: "jira.newsletter.signuptip.opt-in.validationerror"});
                        $optInCheckbox.parent().after(aui.form.fieldError({message: formatter.I18n.getText('newsletter.signup.tip.error.opt-in')})); // eslint-disable-line no-undef
                    }
                }
            });

            $body.on("click", "form.aui.insiders-signup-form a.cancel", function (e) {
                e.preventDefault();
                AJS.trigger('analyticsEvent', { name: "jira.newsletter.signuptip.dismissed" });
                tip.dismiss("newslettercancelled");
            });
        }
    };
});
