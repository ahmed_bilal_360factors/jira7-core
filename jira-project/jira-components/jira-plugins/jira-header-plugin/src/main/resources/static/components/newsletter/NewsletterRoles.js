define("jira/newsletter/roles", ["underscore", "exports"], function(_, exports) {
    "use strict";
    exports.getRoles = function() {
        // this function originates from NPS plugin, forked, because it may diverge
        var prefix = [
            {
                value: "",
                text: AJS.I18n.getText("newsletter.signup.tip.choose.role"),
                disabled: true,
                selected: true
            }
        ];

        var roles = [
            {
                value: "software-engineer",
                text: AJS.I18n.getText("nps.survey.inline.dialog.roles.software.engineer")
            },
            {
                value: "product-manager",
                text: AJS.I18n.getText("nps.survey.inline.dialog.roles.product.manager")
            },
            {
                value: "qa",
                text: AJS.I18n.getText("nps.survey.inline.dialog.roles.qa")
            },
            {
                value: "design",
                text: AJS.I18n.getText("nps.survey.inline.dialog.roles.design")
            },
            {
                value: "management",
                text: AJS.I18n.getText("nps.survey.inline.dialog.roles.management")
            },
            {
                value: "sys-admin",
                text: AJS.I18n.getText("nps.survey.inline.dialog.roles.sys.admin")
            }
        ];

        var suffix = [
            {
                value: "other",
                text: AJS.I18n.getText("nps.survey.inline.dialog.roles.other")
            }
        ];

        return prefix.concat(_.shuffle(roles)).concat(suffix);
    };
});
