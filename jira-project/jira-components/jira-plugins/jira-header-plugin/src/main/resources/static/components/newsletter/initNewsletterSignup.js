define('jira/newsletter/init-signup', [
    'wrm/data',
    'wrm/require',
    'require'
], function (wrmData, wrmRequire, require) {
    return function () {
        var newsletterSignupData = wrmData.claim("com.atlassian.jira.jira-header-plugin:newsletter-signup-tip-init.newsletterSignup");

        if (newsletterSignupData && newsletterSignupData.showNewsletterTip) {
            wrmRequire('wr!com.atlassian.jira.jira-header-plugin:newsletter-signup-tip').then(function() {
                require('jira/newsletter/signuptip').render({
                    userEmail: newsletterSignupData.userEmail,
                    formUrl: newsletterSignupData.formUrl,
                    signupTitle: newsletterSignupData.signupTitle,
                    signupDescription: newsletterSignupData.signupDescription,
                    signupId: newsletterSignupData.signupId
                });
            });
        }
    };
});

require([
    'jquery',
    'jira/newsletter/init-signup'
], function ($,
    InitNewsletterSignup) {
    $(InitNewsletterSignup);
});

