AJS.test.require(["com.atlassian.jira.jira-header-plugin:newsletter-signup-tip-init"], function () {
    var $ = require("jquery");

    module('Newsletter initialisation code', {
        setup: function () {
            this.newsLetterSignup = {
                render:sinon.spy()
            };
            this.mockedContext = AJS.test.mockableModuleContext();
            this.mockedContext.mock('jira/newsletter/signuptip', this.newsLetterSignup);

            this.wrmData = {
                claim: sinon.stub()
            };
            this.mockedContext.mock('wrm/data', this.wrmData);

            this.wrmRequire = sinon.stub();
            this.mockedContext.mock('wrm/require', this.wrmRequire);

            this.wrmRequire.withArgs("wr!com.atlassian.jira.jira-header-plugin:newsletter-signup-tip")
                .returns(new $.Deferred().resolve());
        }
    });

    test("Render function is called when dataprovider says so", function () {
        this.wrmData.claim.withArgs("com.atlassian.jira.jira-header-plugin:newsletter-signup-tip-init.newsletterSignup").returns({
            showNewsletterTip: true,
            userEmail: "stub@mail.com",
            formUrl: "http://stubUrl",
            signupTitle: "title",
            signupDescription: "description",
            signupId: "id"
        });

        this.mockedContext.require('jira/newsletter/init-signup')();

        var signupArguments = {
            userEmail: "stub@mail.com",
            formUrl: "http://stubUrl",
            signupTitle: "title",
            signupDescription: "description",
            signupId: "id"
        };
        sinon.assert.calledWith(this.newsLetterSignup.render, signupArguments);
    });

    test("Render function is *not* called when dataprovider says so", function () {
        this.wrmData.claim.withArgs("com.atlassian.jira.jira-header-plugin:newsletter-signup-tip-init.newsletterSignup").returns({
            showNewsletterTip: false
        });

        this.mockedContext.require('jira/newsletter/init-signup')();

        sinon.assert.notCalled(this.newsLetterSignup.render);
    });
});
