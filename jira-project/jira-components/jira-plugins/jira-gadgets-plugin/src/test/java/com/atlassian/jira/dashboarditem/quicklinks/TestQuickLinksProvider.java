package com.atlassian.jira.dashboarditem.quicklinks;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.jql.builder.JqlClauseBuilder;
import com.atlassian.jira.jql.builder.JqlClauseBuilderFactory;
import com.atlassian.jira.jql.builder.JqlClauseBuilderFactoryImpl;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.jql.util.JqlDateSupport;
import com.atlassian.jira.jql.util.JqlDateSupportImpl;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.ConstantClock;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static com.atlassian.jira.permission.GlobalPermissionKey.ADMINISTER;
import static com.atlassian.jira.permission.ProjectPermissions.ADMINISTER_PROJECTS;
import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static com.atlassian.jira.permission.ProjectPermissions.CREATE_ISSUES;
import static org.jfree.date.DateUtilities.createDate;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

public class TestQuickLinksProvider {
    public static final String REPORTED_ISSUES_LINK = "gadget.quicklinks.reported.issues";
    public static final String VOTED_ISSUES_LINK = "gadget.quicklinks.voted.issues";
    public static final String WATCHED_ISSUES_LINK = "gadget.quicklinks.watched.issues";
    public static final String BROWSE_PROJECTS_LINK = "gadget.quicklinks.browse.projects";
    public static final String FIND_ISSUES_LINK = "gadget.quicklinks.find.issues";
    public static final String CREATE_ISSUES_LINK = "gadget.quicklinks.create.issue";
    public static final String ADMIN_LINK = "gadget.quicklinks.adminstration";
    @Rule
    public MockitoContainer container = MockitoMocksInContainer.rule(this);

    @Mock
    private JiraAuthenticationContext context;
    @Mock
    private GlobalPermissionManager globalPermissionManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private ApplicationProperties properties;
    @Mock
    private SearchService searchService;
    @Mock
    @AvailableInContainer
    private JqlClauseBuilderFactory jqlClauseBuilderFactory;

    @Mock
    @AvailableInContainer
    private TimeZoneManager timeZoneManager;

    @Mock
    private JqlClauseBuilder jqlClauseBuilder;

    private ApplicationUser mockUser;

    private QuickLinksProvider provider;
    private JqlDateSupport dateSupport;

    @Before
    public void setUp() throws Exception {
        dateSupport = new JqlDateSupportImpl(new ConstantClock(createDate(2007, 1, 12)), timeZoneManager);

        mockUser = new MockApplicationUser("admin");
        when(context.getLoggedInUser()).thenReturn(mockUser);

        when(jqlClauseBuilderFactory.newJqlClauseBuilder(any(JqlQueryBuilder.class))).thenAnswer(new Answer<Object>() {
            private final JqlClauseBuilderFactory jqlClauseBuilderFactory = new JqlClauseBuilderFactoryImpl(dateSupport);

            @Override
            public JqlClauseBuilder answer(final InvocationOnMock invocationOnMock) throws Throwable {
                return jqlClauseBuilderFactory.newJqlClauseBuilder((JqlQueryBuilder) invocationOnMock.getArguments()[0]);
            }
        });

        provider = new QuickLinksProvider(context, globalPermissionManager, permissionManager, properties, searchService);
    }

    @Test
    public void expectedQuickLinksArePresentIfTheUserIsAnAdmin() throws Exception {
        givenAdminPermissions();
        withWatchingEnabled();

        LinkLists lists = getQuickLinksList();

        assertLinksExistsWithTexts(lists.getCommonLinks(),
                REPORTED_ISSUES_LINK,
                VOTED_ISSUES_LINK,
                WATCHED_ISSUES_LINK);

        assertLinksExistsWithTexts(lists.getNavigationLinks(),
                BROWSE_PROJECTS_LINK,
                FIND_ISSUES_LINK,
                CREATE_ISSUES_LINK,
                ADMIN_LINK);
    }

    @Test
    public void expectedQuickLinksArePresentIfTheUserIsAnAdminAndWatchingIsNotEnabled() throws Exception {
        givenAdminPermissions();

        LinkLists lists = getQuickLinksList();

        assertLinksExistsWithTexts(lists.getCommonLinks(),
                REPORTED_ISSUES_LINK);

        assertLinksExistsWithTexts(lists.getNavigationLinks(),
                BROWSE_PROJECTS_LINK,
                FIND_ISSUES_LINK,
                CREATE_ISSUES_LINK,
                ADMIN_LINK);
    }

    @Test
    public void expectedQuickLinksArePresentIfTheUserIsARegularUser() throws Exception {
        givenUserPermissions();
        withWatchingEnabled();

        LinkLists lists = getQuickLinksList();

        assertLinksExistsWithTexts(lists.getCommonLinks(),
                REPORTED_ISSUES_LINK,
                VOTED_ISSUES_LINK,
                WATCHED_ISSUES_LINK);

        assertLinksExistsWithTexts(lists.getNavigationLinks(),
                BROWSE_PROJECTS_LINK,
                FIND_ISSUES_LINK,
                CREATE_ISSUES_LINK);
    }

    @Test
    public void expectedQuickLinksArePresentIfTheUserIsAProjectAdmin() throws Exception {
        givenProjectAdminPermissions();
        withWatchingEnabled();

        LinkLists lists = getQuickLinksList();

        assertLinksExistsWithTexts(lists.getCommonLinks(),
                REPORTED_ISSUES_LINK,
                VOTED_ISSUES_LINK,
                WATCHED_ISSUES_LINK);

        assertLinksExistsWithTexts(lists.getNavigationLinks(),
                BROWSE_PROJECTS_LINK,
                FIND_ISSUES_LINK,
                CREATE_ISSUES_LINK);
    }

    @Test
    public void expectedQuickLinksArePresentIfTheUserIsAnAdminWithNoProjects() throws Exception {
        givenAdminWithoutProjectsPermissions();
        withWatchingEnabled();

        LinkLists lists = getQuickLinksList();

        assertLinksExistsWithTexts(lists.getCommonLinks(),
                REPORTED_ISSUES_LINK,
                VOTED_ISSUES_LINK,
                WATCHED_ISSUES_LINK);

        assertLinksExistsWithTexts(lists.getNavigationLinks(),
                BROWSE_PROJECTS_LINK,
                FIND_ISSUES_LINK,
                CREATE_ISSUES_LINK);
    }

    @Test
    public void expectedQuickLinksArePresentIfTheUserIsGlobalAdminButNotProjectAdminAndThereAreNoProjects() throws Exception {
        when(permissionManager.hasProjects(any(ProjectPermissionKey.class), any(ApplicationUser.class))).thenReturn(false);
        when(globalPermissionManager.hasPermission(eq(GlobalPermissionKey.ADMINISTER), any(ApplicationUser.class))).thenReturn(true);

        LinkLists lists = getQuickLinksList();
        assertTrue(!lists.getCommonLinks().isEmpty() && lists.getNavigationLinks().isEmpty());
    }

    @Test
    public void expectedQuickLinksArePresentIfTheUserIsNotAnAdminAndThereAreNoProjects() throws Exception {
        when(permissionManager.hasProjects(any(ProjectPermissionKey.class), any(ApplicationUser.class))).thenReturn(false);
        when(globalPermissionManager.hasPermission(eq(GlobalPermissionKey.ADMINISTER), any(ApplicationUser.class))).thenReturn(false);

        LinkLists lists = getQuickLinksList();

        assertTrue(lists.getNavigationLinks().isEmpty());
        assertLinksExistsWithTexts(lists.getCommonLinks(), REPORTED_ISSUES_LINK);
    }

    @Test
    public void testAnonymous() throws Exception {
        givenAnonymousUser();
        withWatchingEnabled();

        LinkLists lists = getQuickLinksList();

        assertTrue(lists.getCommonLinks().isEmpty() && lists.getNavigationLinks().isEmpty());
    }

    private LinkLists getQuickLinksList() throws Exception {
        LinkLists lists = provider.getLinks();
        assertNotNull(lists);
        return lists;
    }

    private void assertLinksExistsWithTexts(Collection<Link> links, String... texts) {
        assertEquals("Should be the same number of links as expected", texts.length, links.size());
        Set<String> expectedTexts = new HashSet<String>(Arrays.asList(texts));
        for (Link link : links) {
            expectedTexts.remove(link.getText());
        }
        assertEquals("Expected texts not found: " + expectedTexts, 0, expectedTexts.size());
    }

    private void withWatchingEnabled() {
        when(properties.getOption("jira.option.voting")).thenReturn(true);
        when(properties.getOption("jira.option.watching")).thenReturn(true);
    }

    private void givenAnonymousUser() {
        when(context.getLoggedInUser()).thenReturn(null);
        when(permissionManager.hasProjects(BROWSE_PROJECTS, mockUser)).thenReturn(true);
        when(permissionManager.hasProjects(CREATE_ISSUES, mockUser)).thenReturn(false);
        when(permissionManager.hasProjects(ADMINISTER_PROJECTS, mockUser)).thenReturn(false);
        when(globalPermissionManager.hasPermission(ADMINISTER, mockUser)).thenReturn(false);
    }


    private void givenAdminPermissions() {
        when(permissionManager.hasProjects(BROWSE_PROJECTS, mockUser)).thenReturn(true);
        when(permissionManager.hasProjects(CREATE_ISSUES, mockUser)).thenReturn(true);
        when(permissionManager.hasProjects(ADMINISTER_PROJECTS, mockUser)).thenReturn(true);
        when(globalPermissionManager.hasPermission(ADMINISTER, mockUser)).thenReturn(true);
    }

    private void givenUserPermissions() {
        when(permissionManager.hasProjects(BROWSE_PROJECTS, mockUser)).thenReturn(true);
        when(permissionManager.hasProjects(CREATE_ISSUES, mockUser)).thenReturn(true);
        when(permissionManager.hasProjects(ADMINISTER_PROJECTS, mockUser)).thenReturn(false);
        when(globalPermissionManager.hasPermission(ADMINISTER, mockUser)).thenReturn(false);
    }

    private void givenAdminWithoutProjectsPermissions() {
        when(permissionManager.hasProjects(BROWSE_PROJECTS, mockUser)).thenReturn(true);
        when(permissionManager.hasProjects(CREATE_ISSUES, mockUser)).thenReturn(true);
        when(permissionManager.hasProjects(ADMINISTER_PROJECTS, mockUser)).thenReturn(false);
        when(globalPermissionManager.hasPermission(ADMINISTER, mockUser)).thenReturn(true);
    }

    private void givenProjectAdminPermissions() {
        when(permissionManager.hasProjects(BROWSE_PROJECTS, mockUser)).thenReturn(true);
        when(permissionManager.hasProjects(CREATE_ISSUES, mockUser)).thenReturn(true);
        when(permissionManager.hasProjects(ADMINISTER_PROJECTS, mockUser)).thenReturn(true);
        when(globalPermissionManager.hasPermission(ADMINISTER, mockUser)).thenReturn(false);
    }
}