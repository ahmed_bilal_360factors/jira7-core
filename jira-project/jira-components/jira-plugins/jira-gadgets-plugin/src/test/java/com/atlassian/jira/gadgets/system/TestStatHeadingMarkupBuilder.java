package com.atlassian.jira.gadgets.system;

import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponentImpl;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.gadgets.system.util.StatsMarkupFieldValueToDisplayTransformer;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.IssueConstant;
import com.atlassian.jira.issue.customfields.CustomFieldSearcher;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.statistics.util.FieldValueToDisplayTransformer;
import com.atlassian.jira.issue.statistics.util.ObjectToFieldValueMapper;
import com.atlassian.jira.issue.status.SimpleStatus;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.mock.project.MockVersion;
import com.atlassian.jira.plugin.customfield.CustomFieldSearcherModuleDescriptor;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectImpl;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.template.soy.SoyTemplateRendererProvider;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.ofbiz.core.entity.GenericValue;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 */
public class TestStatHeadingMarkupBuilder {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    private static final String BASE_URL = "http://www.lolcats.com";
    private static final String FULL_URL = BASE_URL + "/someUrl";

    @Mock
    JiraAuthenticationContext authenticationContext;
    @Mock
    ConstantsManager constantsManager;
    @Mock
    CustomFieldManager cfm;
    @Mock
    SoyTemplateRendererProvider soyTemplateRendererProvider;
    @Mock
    private SoyTemplateRenderer soyTemplateRenderer;

    private ApplicationUser user;
    private FieldValueToDisplayTransformer<StatsMarkup> transformer;


    @Before
    public void setUp() throws Exception {
        user = new MockApplicationUser("fooGuy", "Foo Guy", "foo@bar.com");

        transformer = new StatsMarkupFieldValueToDisplayTransformer(authenticationContext, constantsManager, cfm, null, soyTemplateRendererProvider);
        when(authenticationContext.getLoggedInUser()).thenReturn(user);
        when(authenticationContext.getI18nHelper()).thenReturn(new MockI18nHelper());
        when(soyTemplateRendererProvider.getRenderer()).thenReturn(soyTemplateRenderer);
    }

    @Test
    public void testHeadingMarkupForProjectGV() {
        GenericValue value = new MockGenericValue("dubya", ImmutableMap.of("name", "theProject"));
        String s = ObjectToFieldValueMapper.transform("project", value, FULL_URL, transformer).getHtml();
        assertEquals("<a href='" + FULL_URL + "'>theProject</a>", s);
    }

    @Test
    public void testHeadingMarkupForProject() {
        GenericValue value = new MockGenericValue("dubya", ImmutableMap.of("name", "theProject"));
        Project project = new ProjectImpl(value);
        String s = ObjectToFieldValueMapper.transform("project", project, FULL_URL, transformer).getHtml();
        assertEquals("<a href='" + FULL_URL + "'>theProject</a>", s);
    }

    @Test
    public void testHeadingMarkupForAssigness() {
        String s = ObjectToFieldValueMapper.transform("assignees", user, FULL_URL, transformer).getHtml();
        assertEquals("<a href='" + FULL_URL + "'>Foo Guy</a>", s);
    }

    @Test
    public void testHeadingMarkupForNoAssignee() {
        String s = ObjectToFieldValueMapper.transform("assignees", null, FULL_URL, transformer).getHtml();
        assertEquals("<a href='" + FULL_URL + "'>gadget.filterstats.assignee.unassigned</a>", s);
    }

    @Test
    public void testHeadingMarkupForResolutionGV() {
        final IssueConstant issueConstant = mockIssueConstant();
        final MockGenericValue constantGenericValue = new MockGenericValue("foo");
        when(constantsManager.getIssueConstant(constantGenericValue)).thenReturn(issueConstant);

        String s = ObjectToFieldValueMapper.transform("resolution", constantGenericValue, FULL_URL, transformer).getHtml();

        assertEquals("<img src=\"http://jira/icon.gif\" height=\"16\" width=\"16\" alt=\"theNameTranslated\" title=\"theNameTranslated - theDescTranslated\"/><a href='" + FULL_URL + "' title='theDescTranslated'>theNameTranslated</a>", s);
    }

    @Test
    public void testHeadingMarkupForResolution() {
        final IssueConstant issueConstant = mockIssueConstant();

        String s = ObjectToFieldValueMapper.transform("resolution", issueConstant, FULL_URL, transformer).getHtml();

        assertEquals("<img src=\"http://jira/icon.gif\" height=\"16\" width=\"16\" alt=\"theNameTranslated\" title=\"theNameTranslated - theDescTranslated\"/><a href='" + FULL_URL + "' title='theDescTranslated'>theNameTranslated</a>", s);

    }

    @Test
    public void testHeadingMarkupForNoResolution() {
        String s = ObjectToFieldValueMapper.transform("resolution", null, FULL_URL, transformer).getHtml();

        assertEquals("<a href='" + FULL_URL + "'>common.resolution.unresolved</a>", s);
    }

    /*
     * Check here if soy renderer is called once and transformer returns correct markup
     */
    @Test
    public void testHeadingMarkupForStatusesGV() throws Exception {
        final IssueConstant issueConstant = mockStatusIssueConstantWithStatusRendered("myid");
        MockGenericValue statusGenericValue = new MockGenericValue("foo");
        when(constantsManager.getIssueConstant(statusGenericValue)).thenReturn(issueConstant);

        final String html = ObjectToFieldValueMapper.transform("statuses", statusGenericValue, FULL_URL, transformer).getHtml();
        assertEquals("<span>Status lozenge for id myid</span>", html);
    }

    /*
     * Check here if soy renderer is called once and transformer returns correct markup
     */
    @Test
    public void testHeadingMarkupForStatuses() throws Exception {
        final IssueConstant issueConstant = mockStatusIssueConstantWithStatusRendered("1");
        final String html = ObjectToFieldValueMapper.transform("statuses", issueConstant, FULL_URL, transformer).getHtml();
        assertEquals("<span>Status lozenge for id 1</span>", html);
    }

    private IssueConstant mockStatusIssueConstantWithStatusRendered(final String statusId) throws Exception {
        final IssueConstant issueConstant = mock(IssueConstant.class);
        when(issueConstant.getId()).thenReturn(statusId);

        final Status status = mock(Status.class);
        final SimpleStatus simpleStatus = mock(SimpleStatus.class);
        when(status.getSimpleStatus()).thenReturn(simpleStatus);
        when(constantsManager.getStatusObject(statusId)).thenReturn(status);

        when(soyTemplateRenderer.render(
                        "jira.webresources:issue-statuses",
                        "JIRA.Template.Util.Issue.Status.issueStatusResolver",
                        ImmutableMap.<String, Object>of("issueStatus", simpleStatus, "isSubtle", true))
        ).thenReturn("<span>Status lozenge for id " + statusId + "</span>");

        return issueConstant;
    }

    @Test
    public void testHeadingMarkupForIssueTypesGV() {
        final IssueConstant issueConstant = mockIssueConstant();
        when(constantsManager.getIssueConstant(Mockito.any(GenericValue.class))).thenReturn(issueConstant);

        String s = ObjectToFieldValueMapper.transform("issuetype", new MockGenericValue("foo"), FULL_URL, transformer).getHtml();

        assertEquals("<img src=\"http://jira/icon.gif\" height=\"16\" width=\"16\" alt=\"theNameTranslated\" title=\"theNameTranslated - theDescTranslated\"/><a href='" + FULL_URL + "' title='theDescTranslated'>theNameTranslated</a>", s);
    }

    @Test
    public void testHeadingMarkupForIssueTypes() {
        final IssueConstant issueConstant = mockIssueConstant();
        String s = ObjectToFieldValueMapper.transform("issuetype", issueConstant, FULL_URL, transformer).getHtml();

        assertEquals("<img src=\"http://jira/icon.gif\" height=\"16\" width=\"16\" alt=\"theNameTranslated\" title=\"theNameTranslated - theDescTranslated\"/><a href='" + FULL_URL + "' title='theDescTranslated'>theNameTranslated</a>", s);
    }

    @Test
    public void testHeadingMarkupForPrioritiesGV() {
        final IssueConstant issueConstant = mockIssueConstant();
        when(constantsManager.getIssueConstant(Mockito.any(GenericValue.class))).thenReturn(issueConstant);

        String s = ObjectToFieldValueMapper.transform("priorities", new MockGenericValue("foo"), FULL_URL, transformer).getHtml();

        assertEquals("<img src=\"http://jira/icon.gif\" height=\"16\" width=\"16\" alt=\"theNameTranslated\" title=\"theNameTranslated - theDescTranslated\"/><a href='" + FULL_URL + "' title='theDescTranslated'>theNameTranslated</a>", s);
    }

    @Test
    public void testHeadingMarkupForPriorities() {
        final IssueConstant issueConstant = mockIssueConstant();
        String s = ObjectToFieldValueMapper.transform("priorities", issueConstant, FULL_URL, transformer).getHtml();

        assertEquals("<img src=\"http://jira/icon.gif\" height=\"16\" width=\"16\" alt=\"theNameTranslated\" title=\"theNameTranslated - theDescTranslated\"/><a href='" + FULL_URL + "' title='theDescTranslated'>theNameTranslated</a>", s);
    }

    @Test
    public void testHeadingMarkupForNoPriority() {
        String s = ObjectToFieldValueMapper.transform("priorities", null, FULL_URL, transformer).getHtml();

        assertEquals("<a href='" + FULL_URL + "'>gadget.filterstats.priority.nopriority</a>", s);
    }

    @Test
    public void testHeadingMarkupForComponentsGV() {
        GenericValue value = new MockGenericValue("dubya", ImmutableMap.of("name", "theComponent"));
        final StatsMarkup statsMarkup = ObjectToFieldValueMapper.transform("components", value, FULL_URL, transformer);

        String s = statsMarkup.getHtml();
        assertEquals("<a href='" + FULL_URL + "'>theComponent</a>", s);
        assertThat(statsMarkup.getClasses(), containsInAnyOrder("default_image", "default_image_component"));
    }

    @Test
    public void testHeadingMarkupForComponents() {
        ProjectComponent comp = new ProjectComponentImpl("theComponent", "theComponentDesc", null, 1);

        final StatsMarkup statsMarkup = ObjectToFieldValueMapper.transform("components", comp, FULL_URL, transformer);

        String s = statsMarkup.getHtml();
        assertEquals("<a href='" + FULL_URL + "' title='theComponentDesc'>theComponent</a>", s);
        assertThat(statsMarkup.getClasses(), containsInAnyOrder("default_image", "default_image_component"));
    }

    @Test
    public void testHeadingMarkupForNoComponent() {
        String s = ObjectToFieldValueMapper.transform("components", null, FULL_URL, transformer).getHtml();

        assertEquals("<a href='" + FULL_URL + "'>gadget.filterstats.component.nocomponent</a>", s);
    }

    @Test
    public void testHeadingMarkupForVersionArchivedAndReleased() {
        Version version = makeVersion(true, true, "archivedAndReleased", "archivedAndReleasedDesc");

        final StatsMarkup statsMarkup = ObjectToFieldValueMapper.transform("version", version, FULL_URL, transformer);

        assertVersionMarkup(version, statsMarkup);
    }

    @Test
    public void testHeadingMarkupForAllVersionArchivedAndReleased() {
        Version version = makeVersion(true, true, "archivedAndReleased", "archivedAndReleasedDesc");

        final StatsMarkup statsMarkup = ObjectToFieldValueMapper.transform("allVersion", version, FULL_URL, transformer);

        assertVersionMarkup(version, statsMarkup);
    }

    @Test
    public void testHeadingMarkupForVersionNotArchivedAndReleased() {
        Version version = makeVersion(false, true, "notArchivedAndReleased", "notArchivedAndReleasedDesc");

        final StatsMarkup statsMarkup = ObjectToFieldValueMapper.transform("version", version, FULL_URL, transformer);

        assertVersionMarkup(version, statsMarkup);
    }

    @Test
    public void testHeadingMarkupForAllVersionNotArchivedAndReleased() {
        Version version = makeVersion(false, true, "notArchivedAndReleased", "notArchivedAndReleasedDesc");

        final StatsMarkup statsMarkup = ObjectToFieldValueMapper.transform("allVersion", version, FULL_URL, transformer);

        assertVersionMarkup(version, statsMarkup);
    }

    @Test
    public void testHeadingMarkupForVersionArchivedAndNotReleased() {
        Version version = makeVersion(true, false, "archivedAndNotReleased", "archivedAndNotReleasedDesc");

        final StatsMarkup statsMarkup = ObjectToFieldValueMapper.transform("version", version, FULL_URL, transformer);

        assertVersionMarkup(version, statsMarkup);
    }

    @Test
    public void testHeadingMarkupForAllVersionArchivedAndNotReleased() {
        Version version = makeVersion(true, false, "archivedAndNotReleased", "archivedAndNotReleasedDesc");

        final StatsMarkup statsMarkup = ObjectToFieldValueMapper.transform("allVersion", version, FULL_URL, transformer);

        assertVersionMarkup(version, statsMarkup);
    }

    @Test
    public void testHeadingMarkupForVersionNotArchivedAndNotReleased() {
        Version version = makeVersion(false, false, "notArchivedAndNotReleased", "notArchivedAndNotReleasedDesc");

        final StatsMarkup statsMarkup = ObjectToFieldValueMapper.transform("version", version, FULL_URL, transformer);

        assertVersionMarkup(version, statsMarkup);
    }

    @Test
    public void testHeadingMarkupForAllVersionNotArchivedAndNotReleased() {
        Version version = makeVersion(false, false, "notArchivedAndNotReleased", "notArchivedAndNotReleasedDesc");

        final StatsMarkup statsMarkup = ObjectToFieldValueMapper.transform("allVersion", version, FULL_URL, transformer);

        assertVersionMarkup(version, statsMarkup);
    }

    @Test
    public void testHeadingMarkupForNoVersions() {
        Version version = null;
        final StatsMarkup statsMarkup = ObjectToFieldValueMapper.transform("version", version, FULL_URL, transformer);

        String s = statsMarkup.getHtml();
        assertEquals("<a href='" + FULL_URL + "'>gadget.filterstats.raisedin.unscheduled</a>", s);
    }

    @Test
    public void testHeadingMarkupForFixForArchivedAndReleased() {
        Version version = makeVersion(true, true, "archivedAndReleased", "archivedAndReleasedDesc");

        final StatsMarkup statsMarkup = ObjectToFieldValueMapper.transform("fixfor", version, FULL_URL, transformer);

        assertVersionMarkup(version, statsMarkup);
    }

    @Test
    public void testHeadingMarkupForAllFixForArchivedAndReleased() {
        Version version = makeVersion(true, true, "archivedAndReleased", "archivedAndReleasedDesc");

        final StatsMarkup statsMarkup = ObjectToFieldValueMapper.transform("allFixfor", version, FULL_URL, transformer);

        assertVersionMarkup(version, statsMarkup);
    }

    @Test
    public void testHeadingMarkupForFixForNotArchivedAndReleased() {
        Version version = makeVersion(false, true, "notArchivedAndReleased", "notArchivedAndReleasedDesc");

        final StatsMarkup statsMarkup = ObjectToFieldValueMapper.transform("fixfor", version, FULL_URL, transformer);

        assertVersionMarkup(version, statsMarkup);
    }

    @Test
    public void testHeadingMarkupForAllFixForNotArchivedAndReleased() {
        Version version = makeVersion(false, true, "notArchivedAndReleased", "notArchivedAndReleasedDesc");

        final StatsMarkup statsMarkup = ObjectToFieldValueMapper.transform("allFixfor", version, FULL_URL, transformer);

        assertVersionMarkup(version, statsMarkup);
    }

    @Test
    public void testHeadingMarkupForFixForArchivedAndNotReleased() {
        Version version = makeVersion(true, false, "archivedAndNotReleased", "archivedAndNotReleasedDesc");

        final StatsMarkup statsMarkup = ObjectToFieldValueMapper.transform("fixfor", version, FULL_URL, transformer);

        assertVersionMarkup(version, statsMarkup);
    }

    @Test
    public void testHeadingMarkupForAllFixForArchivedAndNotReleased() {
        Version version = makeVersion(true, false, "archivedAndNotReleased", "archivedAndNotReleasedDesc");

        final StatsMarkup statsMarkup = ObjectToFieldValueMapper.transform("allFixfor", version, FULL_URL, transformer);

        assertVersionMarkup(version, statsMarkup);
    }

    @Test
    public void testHeadingMarkupForFixForNotArchivedAndNotReleased() {
        Version version = makeVersion(false, false, "notArchivedAndNotReleased", "notArchivedAndNotReleasedDesc");

        final StatsMarkup statsMarkup = ObjectToFieldValueMapper.transform("fixfor", version, FULL_URL, transformer);

        assertVersionMarkup(version, statsMarkup);
    }

    @Test
    public void testHeadingMarkupForAllFixForNotArchivedAndNotReleased() {
        Version version = makeVersion(false, false, "notArchivedAndNotReleased", "notArchivedAndNotReleasedDesc");

        final StatsMarkup statsMarkup = ObjectToFieldValueMapper.transform("allFixfor", version, FULL_URL, transformer);

        assertVersionMarkup(version, statsMarkup);
    }

    @Test
    public void testHeadingMarkupForNoFixFor() {
        Version version = null;
        final StatsMarkup statsMarkup = ObjectToFieldValueMapper.transform("fixfor", version, FULL_URL, transformer);

        String s = statsMarkup.getHtml();
        assertEquals("<a href='" + FULL_URL + "'>gadget.filterstats.fixfor.unscheduled</a>", s);
    }

    private void assertVersionMarkup(Version v, StatsMarkup markup) {
        StringBuilder specificClass = new StringBuilder();
        if (v.isReleased()) {
            specificClass.append("released_");
        } else {
            specificClass.append("unreleased_");
        }

        if (v.isArchived()) {
            specificClass.append("archived_");
        } else {
            specificClass.append("unarchived_");
        }
        specificClass.append("version");

        if (v.isArchived()) {
            assertThat(markup.getClasses(), containsInAnyOrder("default_image", "archived_version", specificClass.toString()));
        } else {
            assertThat(markup.getClasses(), containsInAnyOrder("default_image", specificClass.toString()));
        }

        assertEquals("<a href='" + FULL_URL + "' title='" + v.getDescription() + "'>" + v.getName() + "</a>", markup.getHtml());

    }

    private Version makeVersion(boolean archived, boolean released, String name, String desc) {
        MockVersion version = new MockVersion(123L, name);
        version.setArchived(archived);
        version.setReleased(released);
        version.setDescription(desc);
        return version;
    }

    @Test
    public void testHeadingMarkupForCustomFields() {

        CustomField cf = mock(CustomField.class);
        CustomFieldSearcher cfs = mock(CustomFieldSearcher.class);
        CustomFieldSearcherModuleDescriptor cfmd = mock(CustomFieldSearcherModuleDescriptor.class);

        when(cfm.getCustomFieldObject("customfield_nick")).thenReturn(cf);
        when(cf.getCustomFieldSearcher()).thenReturn(cfs);
        when(cfs.getDescriptor()).thenReturn(cfmd);
        when(cfmd.getStatHtml(cf, "nick", FULL_URL)).thenReturn("CustomField Markup");

        final StatsMarkup statsMarkup = ObjectToFieldValueMapper.transform("customfield_nick", "nick", FULL_URL, transformer);

        assertEquals("CustomField Markup", statsMarkup.getHtml());

    }

    @Test
    public void testHeadingMarkupForNoCustomFields() {
        final StatsMarkup statsMarkup = ObjectToFieldValueMapper.transform("customfield_nick", "nick", FULL_URL, transformer);

        assertEquals("<a href='" + FULL_URL + "'>common.words.none</a>", statsMarkup.getHtml());
    }

    @Test
    public void testHeadingMarkupForNoValueCustomFields() {
        final StatsMarkup statsMarkup = ObjectToFieldValueMapper.transform("customfield_nick", null, FULL_URL, transformer);

        assertEquals("<a href='" + FULL_URL + "'>common.words.none</a>", statsMarkup.getHtml());
    }

    private IssueConstant mockIssueConstant() {
        final IssueConstant issueConstant = mock(IssueConstant.class);
        when(issueConstant.getNameTranslation()).thenReturn("theNameTranslated");
        when(issueConstant.getDescTranslation()).thenReturn("theDescTranslated");
        when(issueConstant.getIconUrl()).thenReturn("http://jira/icon.gif");
        return issueConstant;
    }


}
