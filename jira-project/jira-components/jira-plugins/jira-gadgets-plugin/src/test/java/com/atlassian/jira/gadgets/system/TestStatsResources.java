package com.atlassian.jira.gadgets.system;

import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.charts.util.ChartUtils;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.statistics.FilterStatisticsValuesGenerator;
import com.atlassian.jira.issue.statistics.StatisticsMapper;
import com.atlassian.jira.issue.statistics.TwoDimensionalStatsMap;
import com.atlassian.jira.issue.statistics.util.FieldValueToDisplayTransformer;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.rest.api.messages.TextMessage;
import com.atlassian.jira.rest.v1.model.errors.ValidationError;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.velocity.VelocityRequestContext;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.web.bean.StatisticAccessorBean;
import com.atlassian.jira.web.bean.StatisticMapWrapper;
import com.atlassian.query.QueryImpl;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;

import javax.ws.rs.core.Response;
import java.util.Collection;
import java.util.Map;

import static com.atlassian.jira.gadgets.system.util.ResponseMatchers.hasStatus;
import static com.atlassian.jira.gadgets.system.util.ResponseMatchers.sameAs;
import static com.atlassian.jira.rest.v1.util.CacheControl.NO_CACHE;
import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestStatsResources {
    @Rule
    public InitMockitoMocks init = new InitMockitoMocks(this);
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Mock
    SearchService mockSearchService;
    @Mock
    JiraAuthenticationContext mockAuthCtx;
    @Mock
    TwoDimensionalStatsMap statsMap;
    @Mock
    StatisticTypesProvider statisticTypesProvider;
    @Mock
    ChartUtils utils;
    @Mock
    SearchRequestService srs;
    @Mock
    PermissionManager permissionManager;
    @Mock
    ProjectManager projectManager;
    @Mock
    I18nHelper helper;
    @Mock
    StatisticsMapper mapper;
    @Mock
    StatisticAccessorBean accessor;
    @Mock
    StatsSearchUrlBuilder builder;
    @Mock
    FieldValueToDisplayTransformer<StatsMarkup> mockTransformer;
    @Mock
    VelocityRequestContextFactory velocity;
    @Mock
    VelocityRequestContext velocityRequestContext;

    private final SearchRequest request = new SearchRequest(new QueryImpl());

    @Test
    public void projectSearch() throws Exception {
        assertCorrectForQueryType("project");
    }

    @Test
    public void filterSearch() throws Exception {
        assertCorrectForQueryType("filter");
    }

    private void assertCorrectForQueryType(final String type) throws Exception {
        final ApplicationUser user = new MockApplicationUser("fooGuy", "Foo Guy", "foo@bar.com");

        final MockSearchQueryBackedResource resource = makeResource();

        when(helper.getText("common.concepts.irrelevant.desc")).thenReturn("The field is not present on some issues");
        when(helper.getText("common.concepts.irrelevant")).thenReturn("Irrelevant");
        when(mockAuthCtx.getUser()).thenReturn(user);
        when(mockAuthCtx.getI18nHelper()).thenReturn(helper);

        when(statisticTypesProvider.getDisplayName("project")).thenReturn("Project");

        final MockGenericValue key1 = new MockGenericValue("proj1");
        key1.set("name", "proj1");
        final MockGenericValue key2 = new MockGenericValue("proj2");
        key2.set("name", "proj2");
        final Map<Object, Number> map = ImmutableMap.of(key2, 2, key1, 1);

        final StatisticMapWrapper<Object, Number> mapWrapper = new StatisticMapWrapper<Object, Number>(map, 4, 1);
        when(accessor.getWrapper(mapper, StatisticAccessorBean.OrderBy.NATURAL, StatisticAccessorBean.Direction.ASC)).thenReturn(mapWrapper);
        when(mockSearchService.getIssueSearchPath(user, SearchService.IssueSearchParameters.builder().query(request.getQuery()).build())).thenReturn("/issues/?urlForRequest");

        resource.setSearchRequest(request);
        resource.setExpectedQueryString(type + "-111");

        final FilterStatisticsValuesGenerator generator = mock(FilterStatisticsValuesGenerator.class);
        when(generator.getStatsMapper("project")).thenReturn(mapper);

        if (type.equals("filter")) {
            when(srs.getFilter(new JiraServiceContextImpl(user, new SimpleErrorCollection()), 111L)).thenReturn(new SearchRequest(new QueryImpl(), new MockApplicationUser("foo"), "theFilter", "theFilterDesc"));
        } else {
            final Project projMock = mock(Project.class);
            when(projMock.getName()).thenReturn("theProject");
            when(projectManager.getProjectObj(111L)).thenReturn(projMock);
        }

        resource.setGenerator(generator);
        resource.setBuilder(builder);

        when(builder.getSearchUrlForHeaderCell(key1, mapper, request)).thenReturn("?keyParam1");
        when(builder.getSearchUrlForHeaderCell(key2, mapper, request)).thenReturn("?keyParam2");

        when(mockTransformer.transformFromProject("project", key1, "http://localhost:8090/JIRA/issues/?keyParam1")).thenReturn(new StatsMarkup("<a href='http://localhost:8090/JIRA/issues/?keyParam1'>proj1</a>"));
        when(mockTransformer.transformFromProject("project", key2, "http://localhost:8090/JIRA/issues/?keyParam2")).thenReturn(new StatsMarkup("<a href='http://localhost:8090/JIRA/issues/?keyParam2'>proj2</a>"));

        final Response response = resource.getData(type + "-111", "project", true, "ascending", "natural");
        final StatsResource.Results results = (StatsResource.Results) response.getEntity();

        if (type.equals("filter")) {
            assertEquals("theFilter", results.filterOrProjectName);
        } else {
            assertEquals("theProject", results.filterOrProjectName);
        }
        assertEquals("Project", results.statTypeDescription);
        assertEquals("http://localhost:8090/JIRA/issues/?urlForRequest", results.filterOrProjectLink);

        assertThat(results.rows, hasItems(
                new StatsResource.StatsRow("<a href='http://localhost:8090/JIRA/issues/?keyParam1'>proj1</a>", 1, 25, null, "http://localhost:8090/JIRA/issues/?keyParam1"),
                new StatsResource.StatsRow("<a href='http://localhost:8090/JIRA/issues/?keyParam2'>proj2</a>", 2, 50, null, "http://localhost:8090/JIRA/issues/?keyParam2"),
                new StatsResource.StatsRow("<span title=\"The field is not present on some issues\">Irrelevant</span>", 1, 25, null, null)
        ));
    }

    private MockSearchQueryBackedResource makeResource() {
        when(velocity.getJiraVelocityRequestContext()).thenReturn(velocityRequestContext);
        when(velocityRequestContext.getCanonicalBaseUrl()).thenReturn("http://localhost:8090/JIRA");

        return new MockSearchQueryBackedResource(utils, mockAuthCtx, mockSearchService, srs, permissionManager, statisticTypesProvider, projectManager, velocity, mockTransformer);
    }

    @Test
    public void testFailingValidation() {
        final MockSearchQueryBackedResource resource = makeResource();
        when(statisticTypesProvider.getDisplayName("foo")).thenReturn("foo");
        resource.addErrorsToReturn(new ValidationError("filterId", "invalid filter"));
        resource.setExpectedQueryString("foo");

        final Response response = resource.validate("foo", "foo");
        assertThat(response, hasStatus(equalTo(400)));
    }

    @Test
    public void testPassingValidation() {
        final MockSearchQueryBackedResource resource = makeResource();
        when(statisticTypesProvider.getDisplayName("foo")).thenReturn("foo");
        resource.setExpectedQueryString("filter-foo");

        final Response response = resource.validate("filter-foo", "foo");
        assertThat(response, sameAs(Response.ok(new TextMessage("No input validation errors found.")).cacheControl(NO_CACHE).build()));
    }

    class MockSearchQueryBackedResource extends StatsResource {
        private String expectedQueryString;
        private final Collection<ValidationError> errorsToReturn = Lists.newArrayList();
        private SearchRequest searchRequest;
        private StatsSearchUrlBuilder builder;

        public MockSearchQueryBackedResource(final ChartUtils chartUtils, final JiraAuthenticationContext authenticationContext,
                                             final SearchService searchService, final SearchRequestService searchRequestService,
                                             final PermissionManager permissionManager, final StatisticTypesProvider statisticTypesProvider,
                                             final ProjectManager projectManager, final VelocityRequestContextFactory velocityRequestContextFactory,
                                             final FieldValueToDisplayTransformer<StatsMarkup> fieldValueToDisplayTransformer) {
            super(chartUtils, authenticationContext, searchService, searchRequestService, permissionManager, statisticTypesProvider, projectManager, velocityRequestContextFactory, fieldValueToDisplayTransformer);
        }

        protected StatsSearchUrlBuilder getHeadingUrlBuilder() {
            return builder;
        }

        protected StatisticAccessorBean getStatisticsAcessorBean(final SearchRequest searchRequest) {
            return accessor;
        }

        public void setBuilder(final StatsSearchUrlBuilder builder) {
            this.builder = builder;
        }

        public void setSearchRequest(final SearchRequest searchRequest) {
            this.searchRequest = searchRequest;
        }

        public void setExpectedQueryString(final String anExpectedQueryString) {
            this.expectedQueryString = anExpectedQueryString;
        }

        public void addErrorsToReturn(final ValidationError... errorsToAdd) {
            if (errorsToAdd != null) {
                errorsToReturn.addAll(asList(errorsToAdd));
            }
        }

        protected StatsSearchUrlBuilder getStatsSearchUrlBuilder() {
            return builder;
        }

        @Override
        protected SearchRequest getSearchRequestAndValidate(final String queryString, final Collection<ValidationError> errors, final Map<String, Object> params) {
            assertEquals(expectedQueryString, queryString);
            assertNotNull(errors);
            assertNotNull(params);
            errors.addAll(errorsToReturn);
            return searchRequest;
        }
    }
}
