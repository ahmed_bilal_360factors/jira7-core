package com.atlassian.jira.gadgets.system;

import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.query.QueryImpl;
import com.google.common.collect.Lists;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Collection;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class TestFavouriteFiltersResource {
    @Rule
    public TestRule initMocks = new InitMockitoMocks(this);

    @Mock
    JiraAuthenticationContext jac;
    @Mock
    private SearchRequestService searchRequestService;
    @Mock
    private SearchProvider sp = mock(SearchProvider.class);

    private ApplicationUser user = new MockApplicationUser("fooGuy");

    @Before
    public void setUp() {
        when(jac.getUser()).thenReturn(user);
    }


    @Test
    public void testWithCount() throws SearchException {
        ArrayList<SearchRequest> searchRequests = Lists.newArrayList();
        SearchRequest request = new SearchRequest(new QueryImpl(), new MockApplicationUser("foo"), "bar", "troz");
        searchRequests.add(request);
        when(searchRequestService.getFavouriteFilters(user)).thenReturn(searchRequests);
        when(sp.searchCount(request.getQuery(), user)).thenReturn(3L);
        FavouriteFiltersResource testObj = new FavouriteFiltersResource(jac, searchRequestService, sp, null, null);

        Response favouriteFilters = testObj.getFavouriteFilters(true);

        assertThat(favouriteFilters.getStatus(), equalTo(200));
        FavouriteFiltersResource.FilterList list = (FavouriteFiltersResource.FilterList) favouriteFilters.getEntity();
        Collection<FavouriteFiltersResource.Filter> filterCollection = list.getFilters();
        assertThat(filterCollection, contains(samePropertyValuesAs(
                new FavouriteFiltersResource.Filter("bar", "troz", null, 3L)
        )));
    }

    @Test
    public void testWithOutCount() {

        ArrayList<SearchRequest> searchRequests = new ArrayList<SearchRequest>();
        SearchRequest request = new SearchRequest(new QueryImpl(), new MockApplicationUser("foo"), "bar", "troz");
        searchRequests.add(request);
        when(searchRequestService.getFavouriteFilters(user)).thenReturn(searchRequests);
        FavouriteFiltersResource testObj = new FavouriteFiltersResource(jac, searchRequestService, sp, null, null);

        Response favouriteFilters = testObj.getFavouriteFilters(true);

        assertThat(favouriteFilters.getStatus(), equalTo(200));
        FavouriteFiltersResource.FilterList list = (FavouriteFiltersResource.FilterList) favouriteFilters.getEntity();
        Collection<FavouriteFiltersResource.Filter> filterCollection = list.getFilters();
        assertThat(filterCollection, contains(samePropertyValuesAs(
                new FavouriteFiltersResource.Filter("bar", "troz", null, 0L)
        )));
    }

    @Test
    public void testNoUser() {
        JiraAuthenticationContext nonAuthenticatedContext = mock(JiraAuthenticationContext.class);
        final FavouriteFiltersResource favouriteFiltersResource = new FavouriteFiltersResource(nonAuthenticatedContext, null, null, null, null);

        final Response favouriteFiltersResponse = favouriteFiltersResource.getFavouriteFilters(false);

        Object responseEntity = favouriteFiltersResponse.getEntity();
        assertThat(responseEntity, Matchers.instanceOf(FavouriteFiltersResource.NotLoggedIn.class));
    }
}
