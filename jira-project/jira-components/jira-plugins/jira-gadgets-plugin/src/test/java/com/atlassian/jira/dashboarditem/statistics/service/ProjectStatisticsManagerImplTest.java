package com.atlassian.jira.dashboarditem.statistics.service;


import com.atlassian.fugue.Either;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.dashboarditem.statistics.service.statistics.StatisticsService;
import com.atlassian.jira.dashboarditem.statistics.service.statistics.StatisticsServiceImpl;
import com.atlassian.jira.dashboarditem.statistics.service.statistics.beans.OneDimensionalStatisticsResultBean;
import com.atlassian.jira.dashboarditem.statistics.service.statistics.searcher.StatisticsSearcher;
import com.atlassian.jira.dashboarditem.statistics.service.statistics.searcher.beans.StatisticsSearchResultBean;
import com.atlassian.jira.dashboarditem.statistics.util.ProjectOrFilterQueryParser;
import com.atlassian.jira.dashboarditem.statistics.util.QueryParser;
import com.atlassian.jira.gadgets.system.StatisticTypesProvider;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.Serializable;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProjectStatisticsManagerImplTest {
    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private StatisticsSearcher statisticsSearcher;
    @Mock
    private StatisticTypesProvider statisticTypesProvider;
    @Mock
    private QueryParser queryParser;
    @Mock
    private ProjectOrFilterQueryParser projectOrFilterQueryParser;
    @Mock
    private ApplicationUser user;
    @Mock
    private Query query;
    @Mock
    private SearchRequestService searchRequestService;
    @Mock
    private SearchRequest searchRequest;

    private StatisticsService statisticsService;
    private static final String MOCK_JQL = "assignee = bob";
    private static final String MOCK_STAT_TYPE = "assignee";
    private static final Long MOCK_FILTER_ID = 10000L;

    @Before
    public void before() {
        I18nHelper i18nHelper = mock(I18nHelper.class);
        when(authenticationContext.getLoggedInUser()).thenReturn(user);
        when(authenticationContext.getI18nHelper()).thenReturn(i18nHelper);

        when(i18nHelper.getText(anyString())).then(returnsFirstArg());
        when(i18nHelper.getText(anyString(), Matchers.<Serializable[]>anyVararg())).then(returnsFirstArg());

        statisticsService = new StatisticsServiceImpl(authenticationContext, searchRequestService, statisticsSearcher,
                statisticTypesProvider, queryParser, projectOrFilterQueryParser);
    }

    @Test
    public void invalidQueryParsingReturnsError() {
        failedQueryParse();

        Either<OneDimensionalStatisticsResultBean, ErrorCollection> errorOrResult =
                statisticsService.aggregateOneDimensionalStats(MOCK_JQL, MOCK_FILTER_ID, MOCK_STAT_TYPE);

        assertThat("Should have resulted in an error", errorOrResult.isRight(), is(true));
    }

    @Test
    public void invalidStatTypeReturnsError() {
        successfulQueryParse();
        failedStatType();

        Either<OneDimensionalStatisticsResultBean, ErrorCollection> errorOrResult =
                statisticsService.aggregateOneDimensionalStats(MOCK_JQL, MOCK_FILTER_ID, MOCK_STAT_TYPE);

        assertThat("Should have resulted in an error", errorOrResult.isRight(), is(true));
    }

    @Test
    public void searchExceptionReturnsError() {
        successfulQueryParse();
        successfulStatType();
        searchException();

        Either<OneDimensionalStatisticsResultBean, ErrorCollection> errorOrResult =
                statisticsService.aggregateOneDimensionalStats(MOCK_JQL, MOCK_FILTER_ID, MOCK_STAT_TYPE);

        assertThat("Should have resulted in an error", errorOrResult.isRight(), is(true));
    }

    @Test
    public void successfulSearchWithJqlAndFilterIdReturnsOk() {
        successfulQueryParse();
        successfulStatType();
        successfulSearch();
        successfulFilterSearch();

        Either<OneDimensionalStatisticsResultBean, ErrorCollection> errorOrResult =
                statisticsService.aggregateOneDimensionalStats(MOCK_JQL, MOCK_FILTER_ID, MOCK_STAT_TYPE);

        assertThat("Should have returned a statistics result bean", errorOrResult.isLeft(), is(true));
    }

    @Test
    public void successfulJqlSearchReturnsOk() {
        successfulQueryParse();
        successfulStatType();
        successfulSearch();

        Either<OneDimensionalStatisticsResultBean, ErrorCollection> errorOrResult =
                statisticsService.aggregateOneDimensionalStats(MOCK_JQL, null, MOCK_STAT_TYPE);

        assertThat(errorOrResult.isLeft(), is(true));
    }

    @Test
    public void successfulFilterSearchReturnsOk() {
        successfulQueryParse();
        successfulStatType();
        successfulSearch();
        successfulFilterSearch();

        Either<OneDimensionalStatisticsResultBean, ErrorCollection> errorOrResult =
                statisticsService.aggregateOneDimensionalStats(null, MOCK_FILTER_ID, MOCK_STAT_TYPE);

        assertThat(errorOrResult.isLeft(), is(true));
    }

    @Test
    public void filterSearchWhereFilterDoesNotExistReturnsError() {
        successfulQueryParse();
        successfulStatType();
        successfulSearch();
        unsuccessfulFilterSearch();

        Either<OneDimensionalStatisticsResultBean, ErrorCollection> errorOrResult =
                statisticsService.aggregateOneDimensionalStats(null, MOCK_FILTER_ID, MOCK_STAT_TYPE);

        assertThat(errorOrResult.isRight(), is(true));
    }

    @Test
    public void neitherFilterIdNorJqlProvidedReturnsError() {
        Either<OneDimensionalStatisticsResultBean, ErrorCollection> errorOrResult =
                statisticsService.aggregateOneDimensionalStats(null, null, MOCK_STAT_TYPE);

        assertThat(errorOrResult.isRight(), is(true));
    }

    private void failedQueryParse() {
        ErrorCollection errorCollection = mock(ErrorCollection.class);
        Either<Query, ErrorCollection> error = Either.right(errorCollection);
        when(queryParser.getQuery(eq(user), eq(MOCK_JQL))).thenReturn(error);
    }

    private void successfulQueryParse() {
        Either<Query, ErrorCollection> successfulQuery = Either.left(query);
        when(queryParser.getQuery(eq(user), eq(MOCK_JQL))).thenReturn(successfulQuery);
    }

    private void failedStatType() {
        when(statisticTypesProvider.getDisplayName(eq(MOCK_STAT_TYPE))).thenReturn(null);
    }

    private void successfulStatType() {
        when(statisticTypesProvider.getDisplayName(eq(MOCK_STAT_TYPE))).thenReturn(MOCK_STAT_TYPE);
    }

    private void searchException() {
        try {
            when(statisticsSearcher.completeOneDimensionalSearch(eq(user), eq(query), eq(MOCK_STAT_TYPE))).thenThrow(new SearchException());
        } catch (SearchException e) {
            //Do nothing
        }
    }

    private void successfulSearch() {
        StatisticsSearchResultBean statisticsSearchResultBean = mock(StatisticsSearchResultBean.class);
        try {
            when(statisticsSearcher.completeOneDimensionalSearch(eq(user), eq(query), eq(MOCK_STAT_TYPE))).thenReturn(statisticsSearchResultBean);
        } catch (SearchException e) {
            //Do nothing
        }

        ProjectOrFilterQueryParser.QueryInformation queryInformation = mock(ProjectOrFilterQueryParser.QueryInformation.class);
        when(projectOrFilterQueryParser.getQueryInformation(eq(user), eq(query))).thenReturn(queryInformation);
        when(queryInformation.getName()).thenReturn("MOCK_NAME");
        when(queryInformation.getUrl()).thenReturn("MOCK_Url");
    }

    private void successfulFilterSearch() {
        when(searchRequestService.getFilter(any(), eq(MOCK_FILTER_ID))).thenReturn(searchRequest);
        when(searchRequest.getQuery()).thenReturn(query);
        when(query.getQueryString()).thenReturn(MOCK_JQL);
    }

    private void unsuccessfulFilterSearch() {
        when(searchRequestService.getFilter(any(), eq(MOCK_FILTER_ID))).thenReturn(null);
    }
}
