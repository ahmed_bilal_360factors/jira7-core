package com.atlassian.jira.gadgets.system;

import com.atlassian.jira.issue.search.SearchRequestManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Tests the REST endpoint of {@link com.atlassian.jira.gadgets.system.PickerResource}
 *
 * @since 7.0
 */
@RunWith(MockitoJUnitRunner.class)
public class TestPickerResource {
    @Mock
    private PermissionManager permissionManager;

    @Mock
    private JiraAuthenticationContext authenticationContext;

    @Mock
    private SearchRequestManager searchRequestManager;


    PickerResource pickerResource;

    @Before
    public void setUp() {
        pickerResource = new PickerResource(permissionManager, authenticationContext, searchRequestManager);
    }


    @Test
    public void canDetermineIfQueryHasDelims() {
        assertFalse(pickerResource.hasDelims("qwe"));

        assertTrue(pickerResource.hasDelims("z-filter"));

        assertTrue(pickerResource.hasDelims("z-filter-two"));
    }

    @Test
    public void regexCharactersStillMatchDelim() {
        assertTrue(pickerResource.hasDelims("qwe^kljasd"));
    }


    @Test
    public void buildNonDelimLuceneQuery() {
        final String query = pickerResource.buildLuceneQuery("test");

        assertEquals("+test", query);
    }

    @Test
    public void buildDelimLuceneQuery() {
        final String query = pickerResource.buildLuceneQuery("z-filter");

        assertEquals("z-filter* (+z +filter)", query);
    }
}
