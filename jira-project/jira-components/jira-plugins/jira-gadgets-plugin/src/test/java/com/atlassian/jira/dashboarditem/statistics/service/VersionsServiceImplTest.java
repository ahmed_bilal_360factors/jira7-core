package com.atlassian.jira.dashboarditem.statistics.service;


import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.dashboarditem.statistics.service.versions.VersionsService;
import com.atlassian.jira.dashboarditem.statistics.service.versions.VersionsServiceImpl;
import com.atlassian.jira.dashboarditem.statistics.service.versions.beans.ProjectVersionBean;
import com.atlassian.jira.dashboarditem.statistics.util.ProjectOrFilterQueryParser;
import com.atlassian.jira.dashboarditem.statistics.util.QueryParser;
import com.atlassian.jira.dashboarditem.statistics.util.QueryUrlSupplier;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.search.managers.SearchHandlerManager;
import com.atlassian.jira.issue.search.searchers.IssueSearcher;
import com.atlassian.jira.issue.search.searchers.transformer.ProjectSearchInputTransformer;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.query.Query;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class VersionsServiceImplTest {
    @Mock
    private VersionManager versionManager;

    @Mock
    private QueryParser queryParser;

    @Mock
    private QueryUrlSupplier queryUrlSupplier;

    @Mock
    private ProjectOrFilterQueryParser projectOrFilterQueryParser;

    @Mock
    private SearchHandlerManager searchHandlerManager;

    @Mock
    private ProjectService projectService;

    @Mock
    private JiraAuthenticationContext authenticationContext;

    @Mock
    private Query query;

    @Mock
    private ApplicationUser user;

    private VersionsService versionsService;


    private static final String MOCK_JQL = "assignee = bob";

    private static final Set<String> MOCK_PROJECTS = Sets.newHashSet("1", "2", "3");

    @Mock
    private Project validProject1;

    @Mock
    private Project validProject2;

    @Before
    public void before() {
        I18nHelper i18nHelper = mock(I18nHelper.class);
        when(authenticationContext.getUser()).thenReturn(user);
        when(authenticationContext.getI18nHelper()).thenReturn(i18nHelper);

        when(i18nHelper.getText(anyString())).then(returnsFirstArg());
        when(i18nHelper.getText(anyString(), Matchers.<Serializable[]>anyVararg())).then(returnsFirstArg());

        versionsService = new VersionsServiceImpl(versionManager, searchHandlerManager, projectService,
                authenticationContext, queryParser, projectOrFilterQueryParser, queryUrlSupplier);
    }

    @Test
    public void invalidQueryParsingReturnsError() {
        failedQueryParse();

        Either<List<ProjectVersionBean>, ErrorCollection> errorOrResult = versionsService.getVersions(MOCK_JQL);

        assertThat("Should have resulted in an error", errorOrResult.isRight(), is(true));
    }

    @Test
    public void createsValidAllProjectVersionsFromResult() {
        successfulQueryParse();
        isNotFilterQuery();
        getProjects();


        Either<List<ProjectVersionBean>, ErrorCollection> resultOrError = versionsService.getVersions(MOCK_JQL);

        assertThat("Should have resulted in an error", resultOrError.isLeft(), is(true));

        List<ProjectVersionBean> versions = resultOrError.left().get();
        assertThat(versions.size(), is(4));
    }


    private void failedQueryParse() {
        ErrorCollection errorCollection = mock(ErrorCollection.class);
        Either<Query, ErrorCollection> error = Either.right(errorCollection);
        when(queryParser.getQuery(eq(user), eq(MOCK_JQL))).thenReturn(error);
    }

    private void successfulQueryParse() {
        Either<Query, ErrorCollection> successfulQuery = Either.left(query);
        when(queryParser.getQuery(eq(user), eq(MOCK_JQL))).thenReturn(successfulQuery);
    }

    private void isNotFilterQuery() {
        when(projectOrFilterQueryParser.getFilterQuery(eq(user), eq(query))).thenReturn(Option.<Query>none());
    }

    private void getProjects() {
        IssueSearcher issueSearcher = mock(IssueSearcher.class);
        ProjectSearchInputTransformer searchInputTransformer = mock(ProjectSearchInputTransformer.class);
        when(searchHandlerManager.getSearcher(eq(IssueFieldConstants.PROJECT))).thenReturn(issueSearcher);
        when(issueSearcher.getSearchInputTransformer()).thenReturn(searchInputTransformer);
        when(searchInputTransformer.getIdValuesAsStrings(eq(user), eq(query))).thenReturn(MOCK_PROJECTS);

        ProjectService.GetProjectResult validResult1 = mock(ProjectService.GetProjectResult.class);
        when(validResult1.isValid()).thenReturn(true);
        when(validResult1.getProject()).thenReturn(validProject1);

        ProjectService.GetProjectResult validResult2 = mock(ProjectService.GetProjectResult.class);
        when(validResult2.isValid()).thenReturn(true);
        when(validResult2.getProject()).thenReturn(validProject2);

        ProjectService.GetProjectResult invalidResult = mock(ProjectService.GetProjectResult.class);
        when(invalidResult.isValid()).thenReturn(false);

        when(projectService.getProjectById(eq(user), eq(Long.valueOf("1")))).thenReturn(validResult1);
        when(projectService.getProjectById(eq(user), eq(Long.valueOf("2")))).thenReturn(validResult2);
        when(projectService.getProjectById(eq(user), eq(Long.valueOf("3")))).thenReturn(invalidResult);

        when(validProject1.getId()).thenReturn(1L);
        when(validProject2.getId()).thenReturn(2L);

        getVersionsForProjects();
    }

    private void getVersionsForProjects() {
        Version releasedVersion = mock(Version.class);
        when(releasedVersion.isReleased()).thenReturn(true);
        when(queryUrlSupplier.getUrlForQueryWithVersion(eq(query), eq(releasedVersion))).thenReturn("versionURL1");

        Version unreleasedVersion = mock(Version.class);
        when(unreleasedVersion.isReleased()).thenReturn(false);
        when(queryUrlSupplier.getUrlForQueryWithVersion(eq(query), eq(unreleasedVersion))).thenReturn("versionURL2");


        when(versionManager.getVersions(eq(validProject1.getId()), eq(false))).thenReturn(Lists.newArrayList(releasedVersion, unreleasedVersion));

        when(versionManager.getVersions(eq(validProject2.getId()), eq(false))).thenReturn(Lists.newArrayList(releasedVersion, unreleasedVersion));
    }
}
