package com.atlassian.jira.gadgets.system;

import com.atlassian.jira.charts.Chart;
import com.atlassian.jira.charts.ChartFactory;
import com.atlassian.jira.charts.util.ChartUtils;
import com.atlassian.jira.gadgets.system.util.ResourceDateValidator;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.rest.api.messages.TextMessage;
import com.atlassian.jira.rest.v1.model.errors.ErrorCollection;
import com.atlassian.jira.rest.v1.model.errors.ValidationError;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import org.jfree.chart.urls.XYURLGenerator;
import org.jfree.data.time.TimeSeriesCollection;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.charts.ChartFactory.PeriodName.quarterly;
import static com.atlassian.jira.gadgets.system.util.ResponseMatchers.sameAs;
import static com.atlassian.jira.rest.v1.util.CacheControl.NO_CACHE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyCollectionOf;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Unit Test for {@link com.atlassian.jira.gadgets.system.ResolutionTimeResource}
 *
 * @since v4.0
 */
public class TestResolutionTimeResource {
    @Rule
    public InitMockitoMocks init = new InitMockitoMocks(this);

    @Mock
    TimeZoneManager timeZoneManager;
    @Mock
    ResourceDateValidator resourceDateValidator;
    @Mock
    ChartUtils chartUtils;
    @Mock
    JiraAuthenticationContext mockJiraAuthenticationContext;
    @Mock
    ChartFactory chartFactory;

    private final ApplicationUser barney = new MockApplicationUser("barney");
    private ResolutionTimeResource rtr;

    @Before
    public void setUp() {
        rtr = new ResolutionTimeResource(chartUtils, mockJiraAuthenticationContext, null, null, chartFactory, resourceDateValidator, null, timeZoneManager);
    }

    @Test
    public void failedDaysAndPeriodValidationEndsIn400() {
        final String query = "filter-foobar";
        final String testNumDays = "somedays";
        final String testPeriodName = "periodname";

        whenChartUtilsIsQueriedItSuccessfullyMakesSearchRequest(query);
        when(resourceDateValidator.validateDaysPrevious(
                eq(ResolutionTimeResource.DAYS),
                any(ChartFactory.PeriodName.class),
                eq(testNumDays),
                anyCollectionOf(ValidationError.class)))
                .thenAnswer(putErrorToErrorCollection(new ValidationError("days field", "mock days error"), 3, -1));
        when(resourceDateValidator.validatePeriod(
                eq(ResolutionTimeResource.PERIOD_NAME),
                eq(testPeriodName),
                anyCollectionOf(ValidationError.class)))
                .thenAnswer(putErrorToErrorCollection(new ValidationError("period field", "mock  period error"), 2, null));

        final Response response = rtr.validate(query, testNumDays, testPeriodName);

        final List<ValidationError> expectedErrors = Arrays.asList(
                new ValidationError("period field", "mock  period error"),
                new ValidationError("days field", "mock days error")
        );
        assertThat(response, sameAs(Response.status(400).entity(ErrorCollection.Builder.newBuilder(expectedErrors).build()).build()));
    }

    @Test
    public void validateSucceeds() {
        final String query = "filter-foobar";
        final String testNumDays = "somedays";
        final String testPeriodName = "periodname";

        when(resourceDateValidator.validatePeriod(eq(ResolutionTimeResource.PERIOD_NAME), eq(testPeriodName), anyCollectionOf(ValidationError.class))).thenReturn(quarterly);
        when(resourceDateValidator.validateDaysPrevious(eq(ResolutionTimeResource.DAYS), eq(quarterly), eq(testNumDays), anyCollectionOf(ValidationError.class))).thenReturn(-123);
        whenChartUtilsIsQueriedItSuccessfullyMakesSearchRequest(query);

        final Response response = rtr.validate(query, testNumDays, testPeriodName);
        assertThat(response, sameAs(Response.ok(new TextMessage("No input validation errors found.")).cacheControl(NO_CACHE).build()));
    }

    @Test
    public void testGetData() {
        final HashMap<String, Object> params = new HashMap<String, Object>();
        final TimeSeriesCollection dataSet = mock(TimeSeriesCollection.class);
        final XYURLGenerator urlGenerator = mock(XYURLGenerator.class);
        params.put("completeDataset", dataSet);
        params.put("completeDatasetUrlGenerator", urlGenerator);

        final Chart chart = mock(Chart.class);
        when(chart.getParameters()).thenReturn(params);

        final TimeChart.TimeDataRow[] dataRows = new TimeChart.TimeDataRow[0];
        final TimeChart.Generator generator = mock(TimeChart.Generator.class);
        when(generator.generateDataSet(dataSet, urlGenerator, timeZoneManager)).thenReturn(dataRows);
        assertSame(dataRows, rtr.getData(generator, chart)); // deliberate reference equality assertion
    }

    @Test
    public void chartCreation() {
        final String filterTitle = "theFilterTitle";
        final String filterUrl = "theFilterURL";

        rtr = new ResolutionTimeResource(chartUtils, mockJiraAuthenticationContext, null, null, chartFactory, resourceDateValidator, null, timeZoneManager) {
            @Override
            protected String getFilterTitle(final Map<String, Object> params) {
                return filterTitle;
            }

            @Override
            protected String getFilterUrl(final Map<String, Object> params) {
                return filterUrl;
            }
        };

        final Map<String, Object> params = new HashMap<String, Object>();
        final String chartLocation = "http://one.bogus/location.png";
        final String imageMap = "<map>yadda yadda image map</map>";

        final Chart chart = new Chart(chartLocation, imageMap, null, new HashMap<String, Object>());
        final TimeChart timeChart = rtr.createChart(1001, 999, params, chart, null);
        assertEquals(chartLocation, timeChart.location);
        assertEquals(1001, timeChart.width);
        assertEquals(999, timeChart.height);
        assertEquals(filterTitle, timeChart.filterTitle);
        assertEquals(filterUrl, timeChart.filterUrl);
        assertNull(timeChart.getData());
    }

    @Test
    public void chartEndpointGeneratesResponseWithChart() {
        final String query = "filter-foobar";
        final String testNumDays = "somedays";
        final String testPeriodName = "periodname";
        final int mockDays = -123;
        when(resourceDateValidator.validatePeriod(eq(ResolutionTimeResource.PERIOD_NAME), eq(testPeriodName), anyCollectionOf(ValidationError.class))).thenReturn(quarterly);
        when(resourceDateValidator.validateDaysPrevious(eq(ResolutionTimeResource.DAYS), eq(quarterly), eq(testNumDays), anyCollectionOf(ValidationError.class))).thenReturn(mockDays);

        final Chart mockChart = mock(Chart.class);
        final String chartLocation = "http://bogus/chart-location.png";
        final String base64Image = "base64Image==";
        when(mockChart.getLocation()).thenReturn(chartLocation);
        when(mockChart.getBase64Image()).thenReturn(base64Image);

        when(mockJiraAuthenticationContext.getUser()).thenReturn(barney);

        final String filterTitle = "theFilterTitle";
        final String filterUrl = "theFilterURL";
        final TimeChart expectedChart = new TimeChart(chartLocation, filterTitle, filterUrl, null, null, null, 700, 9000, base64Image);
        whenChartUtilsIsQueriedItSuccessfullyMakesSearchRequest(query);

        when(chartFactory.generateDateRangeTimeChart((ChartFactory.ChartContext) argThat(allOf(
                        hasProperty("width", equalTo(700)),
                        hasProperty("height", equalTo(9000)),
                        hasProperty("remoteUser", equalTo(barney)))),
                eq(mockDays), eq(quarterly), anyLong(), anyString(), anyString())).thenReturn(mockChart);


        rtr = new ResolutionTimeResource(chartUtils, mockJiraAuthenticationContext, null, null, chartFactory, resourceDateValidator, null, timeZoneManager) {
            @Override
            protected String getFilterTitle(final Map<String, Object> params) {
                return filterTitle;
            }

            @Override
            protected String getFilterUrl(final Map<String, Object> params) {
                return filterUrl;
            }
        };

        final Response chart = rtr.getChart(query, testNumDays, testPeriodName, false, 700, 9000, true);
        assertThat(chart, sameAs(Response.ok(expectedChart).build()));
    }

    private void whenChartUtilsIsQueriedItSuccessfullyMakesSearchRequest(final String forQuery) {
        final SearchRequest searchRequest = mock(SearchRequest.class);
        when(chartUtils.retrieveOrMakeSearchRequest(eq(forQuery), anyMapOf(String.class, Object.class))).thenAnswer(new Answer<Object>() {
            @Override
            public Object answer(final InvocationOnMock invocationOnMock) throws Throwable {
                ((Map<String, Object>) invocationOnMock.getArguments()[1]).put("searchRequest", searchRequest);
                return null;
            }
        });
    }

    private Answer<Object> putErrorToErrorCollection(final ValidationError error, final int whichParam, final Object returnValue) {
        return new Answer<Object>() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                ((Collection<ValidationError>) invocation.getArguments()[whichParam]).add(error);
                return returnValue;
            }
        };
    }
}
