package com.atlassian.jira.gadgets.system;

import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.timezone.TimeZoneManager;
import org.jfree.chart.urls.XYURLGenerator;
import org.jfree.data.time.Day;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.time.TimeSeriesDataItem;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.Arrays;
import java.util.Calendar;
import java.util.TimeZone;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.when;

/**
 * Tests the {@link com.atlassian.jira.gadgets.system.TimeChart} class.
 *
 * @since v4.0
 */
public class TestTimeChart {
    @Rule
    public InitMockitoMocks init = new InitMockitoMocks(this);

    @Mock
    XYURLGenerator mockXyurlGenerator;
    @Mock
    TimeZoneManager timeZoneManager;

    @Before
    public void setUp() throws Exception {
        when(timeZoneManager.getLoggedInUserTimeZone()).thenReturn(TimeZone.getDefault());
    }

    @Test
    public final void testGenerateDataSetWithNoUrlGenerator() {
        doTest(false);
    }

    @Test
    public final void testGenerateDataSetWithUrlGenerator() {
        doTest(true);
    }

    private void doTest(final boolean withUrlGenerator) {
        final Calendar cal = Calendar.getInstance();
        final Day day0 = new Day(cal.getTime());
        cal.add(Calendar.DAY_OF_YEAR, 1);
        final Day day1 = new Day(cal.getTime());
        cal.add(Calendar.DAY_OF_YEAR, 1);
        final Day day2 = new Day(cal.getTime());

        final TimeSeries issuesSeries = new TimeSeries("issues");
        issuesSeries.add(new TimeSeriesDataItem(day0, 4));
        issuesSeries.add(new TimeSeriesDataItem(day1, 5));
        issuesSeries.add(new TimeSeriesDataItem(day2, 6));

        final TimeSeries totalSeries = new TimeSeries("total");
        totalSeries.add(new TimeSeriesDataItem(day0, 14));
        totalSeries.add(new TimeSeriesDataItem(day1, 15));
        totalSeries.add(new TimeSeriesDataItem(day2, 16));

        final TimeSeries averageSeries = new TimeSeries("average");
        averageSeries.add(new TimeSeriesDataItem(day0, 7));
        averageSeries.add(new TimeSeriesDataItem(day1, 8));
        averageSeries.add(new TimeSeriesDataItem(day2, 9));

        final TimeSeriesCollection input = new TimeSeriesCollection();
        input.addSeries(issuesSeries);
        input.addSeries(totalSeries);
        input.addSeries(averageSeries);

        if (withUrlGenerator) {
            when(mockXyurlGenerator.generateURL(same(input), eq(0), anyInt())).then(new Answer<String>() {
                @Override
                public String answer(final InvocationOnMock invocationOnMock) throws Throwable {
                    return "url+" + invocationOnMock.getArguments()[2];
                }
            });
        }

        final TimeChart.TimeDataRow[] actual = new TimeChart.Generator().generateDataSet(input, (withUrlGenerator ? mockXyurlGenerator : null), timeZoneManager);

        final TimeChart.TimeDataRow[] expected = new TimeChart.TimeDataRow[3];
        expected[0] = new TimeChart.TimeDataRow(day0.toString(), 4, (withUrlGenerator ? "url+0" : null), 14, 7);
        expected[1] = new TimeChart.TimeDataRow(day1.toString(), 5, (withUrlGenerator ? "url+1" : null), 15, 8);
        expected[2] = new TimeChart.TimeDataRow(day2.toString(), 6, (withUrlGenerator ? "url+2" : null), 16, 9);

        assertEquals(Arrays.asList(expected), Arrays.asList(actual));
    }
}
