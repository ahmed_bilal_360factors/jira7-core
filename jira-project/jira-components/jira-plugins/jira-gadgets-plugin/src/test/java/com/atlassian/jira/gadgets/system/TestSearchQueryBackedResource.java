package com.atlassian.jira.gadgets.system;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.charts.util.ChartUtils;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.matchers.MapMatchers;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.rest.v1.model.errors.ValidationError;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.query.Query;
import com.atlassian.query.QueryImpl;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operator.Operator;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.Collection;
import java.util.Map;

import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests the parent of all search query-backed resources
 *
 * @since v4.0
 */
public class TestSearchQueryBackedResource {
    @Rule
    public RuleChain init = MockitoMocksInContainer.forTest(this);

    @Mock
    ChartUtils mockChartUtils;
    @Mock
    JiraAuthenticationContext mockAuthCtx;
    @Mock
    SearchService mockSearchService;
    @Mock
    PermissionManager mockPermissionManager;
    @Mock
    VelocityRequestContextFactory velocityRequestContextFactory;

    @AvailableInContainer
    @Mock
    TimeZoneManager timeZoneManager;

    @InjectMocks
    private TestableSearchQueryBackedResource instance;

    ApplicationUser mockUser = new MockApplicationUser("user");

    @Before
    public void setUp() throws Exception {
        when(mockAuthCtx.getLoggedInUser()).thenReturn(mockUser);
    }

    @Test
    public final void getSearchRequestAndValidateForFilter() {
        final String query = "filter-100";
        final String expectedQuery = "filter-100";
        testSuccessfulValidation(query, expectedQuery, new Object());
    }

    @Test
    public final void getSearchRequestAndValidateForProject() {
        final String query = "project-100";
        final String expectedQuery = "project-100";
        final Project p100 = mockProject(100L, "HOMOSAP", "homosapien");
        testSuccessfulValidation(query, expectedQuery, p100);
    }

    @Test
    public void validationFailsForInvalidKeyWithFilterQuery() {
        final String query = "filter-100";
        final ValidationError expectedErrors = new ValidationError("projectOrFilterId", "dashboard.item.error.invalid.filter");
        testValidationExpectingErrors(query, "bad", new Object(), expectedErrors);
    }

    @Test
    public void validationFailsForInvalidKeyWithProjectQuery() {
        final String query = "project-100";
        final ValidationError expectedErrors = new ValidationError("projectOrFilterId", "dashboard.item.error.invalid.project");
        testValidationExpectingErrors(query, "bad", new Object(), expectedErrors);
    }

    @Test
    public void validationFailsForInvalidProject() {
        final String query = "project-100";
        final Project p100 = mockProject(100L, "HOMOSAP", "homosapien");
        final ValidationError expectedErrors = new ValidationError("projectOrFilterId", "dashboard.item.error.invalid.project");
        testValidationExpectingErrors(query, "project", p100, expectedErrors);
    }

    @Test
    public void validationFailsForInvalidJql() {
        final String query = "jql-100";
        final ValidationError expectedErrors = new ValidationError("projectOrFilterId", "dashboard.item.error.invalid.jql");
        testValidationExpectingErrors(query, "bad", new Object(), expectedErrors);
    }

    @Test
    public void validationFailsForInvalidQuery() {
        final String query = "bad-100";
        final ValidationError expectedErrors = new ValidationError("projectOrFilterId", "dashboard.item.error.invalid.projectOrFilterId");
        testValidationExpectingErrors(query, "bad", new Object(), expectedErrors);
    }

    @Test
    public void validationFailsForNullQuery() {
        testValidationExpectingErrors(null, "bad", new Object(),
                new ValidationError("projectOrFilterId", "dashboard.item.error.required.query"));
    }

    @Test
    public void validationFailsForEmptyQuery() {
        testValidationExpectingErrors("", "bad", new Object(),
                new ValidationError("projectOrFilterId", "dashboard.item.error.required.query"));
    }

    @Test
    public final void getFilterTitleForProject() {
        final Project project = mockProject(100L, "pkey", "pname");
        final Map<String, Object> params = MapBuilder.<String, Object>newBuilder().add("project", project).toHashMap();

        final String actual = instance.getFilterTitle(params);
        assertThat(actual, equalTo("pname"));
    }

    @Test
    public final void getFilterTitleForSearchRequest() {
        final SearchRequest r = new SearchRequest();
        r.setName("blah");
        final Map<String, Object> params = MapBuilder.<String, Object>newBuilder().add("searchRequest", r).toHashMap();

        final String actual = instance.getFilterTitle(params);
        assertThat(actual, equalTo("blah"));
    }

    @Test
    public final void getFilterTitleForUnsavedSearch() {
        final String actual = instance.getFilterTitle(Maps.newHashMap());
        assertEquals("dashboard.item.anonymous.filter", actual);
    }

    @Test
    public final void getFilterUrlForProject() {
        final Project project = mockProject(100L, "pkey", "pname");
        final Map<String, Object> params = MapBuilder.<String, Object>newBuilder().add("project", project).toHashMap();
        when(mockSearchService.getIssueSearchPath(mockUser, SearchService.IssueSearchParameters.builder().query(
                        new QueryImpl(new TerminalClauseImpl("project", Operator.EQUALS, "pkey")))
        .build())).thenReturn("/issues/?jql=blah");

        final String actual = instance.getFilterUrl(params);
        assertThat(actual, equalTo("/issues/?jql=blah"));
    }

    @Test
    public final void getFilterUrlForLoadedSearchRequest() {
        final Query mockQuery = mock(Query.class);
        final SearchRequest r = new SearchRequest(mockQuery, new MockApplicationUser("owner"), "name", "desc", 123L, 50L);
        when(mockSearchService.getIssueSearchPath(mockUser, SearchService.IssueSearchParameters.builder().filterId(123L)
                .build())).thenReturn("/issues/?filter=123");

        final Map<String, Object> params = MapBuilder.<String, Object>newBuilder().add("searchRequest", r).toHashMap();

        final String actual = instance.getFilterUrl(params);
        assertThat(actual, equalTo("/issues/?filter=123"));
    }

    @Test
    public final void getFilterUrlForNotLoadedSearchRequest() {
        final Query mockQuery = mock(Query.class);
        final SearchRequest r = new SearchRequest(mockQuery);
        when(mockSearchService.getIssueSearchPath(mockUser, SearchService.IssueSearchParameters.builder().query(mockQuery)
                .build())).thenReturn("/issues/?jql=blah");

        final Map<String, Object> params = MapBuilder.<String, Object>newBuilder().add("searchRequest", r).toHashMap();

        final String actual = instance.getFilterUrl(params);
        assertThat(actual, equalTo("/issues/?jql=blah"));
    }

    @Test
    public final void getFilterUrlForNullSearchRequest() {
        when(mockSearchService.getIssueSearchPath(eq(mockUser), any(SearchService.IssueSearchParameters.class))).thenReturn("/issues/?jql=foo");
        final Map<String, Object> params = MapBuilder.<String, Object>newBuilder().add("searchRequest", null).toHashMap();

        final String actual = instance.getFilterUrl(params);
        assertEquals("/issues/?jql=foo", actual);
    }

    @Test
    public final void getFilterUrlForNoSearchRequest() {
        final String actual = instance.getFilterUrl(Maps.newHashMap());
        assertEquals("", actual);
    }

    private Project mockProject(final long pid, final String projKey, final String projName) {
        final Project project = mock(Project.class);
        when(project.getId()).thenReturn(pid);
        when(project.getKey()).thenReturn(projKey);
        when(project.getName()).thenReturn(projName);
        return project;
    }

    class ChartUtilsMakeSearchRequestAnswer implements Answer<SearchRequest> {
        private String key;
        private Object value;
        private SearchRequest searchRequest = new SearchRequest();

        ChartUtilsMakeSearchRequestAnswer(final Object value) {
            this.value = value;
        }

        ChartUtilsMakeSearchRequestAnswer(final String key, final Object value) {
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public SearchRequest getSearchRequest() {
            return searchRequest;
        }

        @Override
        public SearchRequest answer(final InvocationOnMock invocationOnMock) throws Throwable {
            final String q = (String) invocationOnMock.getArguments()[0];
            final Map<String, Object> params = (Map<String, Object>) invocationOnMock.getArguments()[1];
            if (key == null) {
                if (q.startsWith("filter-")) {
                    key = "searchRequest";
                } else if (q.startsWith("project-")) {
                    key = "project";
                } else if (q.startsWith("jql-")) {
                    key = "searchRequest";
                }
            }
            params.put(key, value);
            return searchRequest;
        }
    }

    private static class TestableSearchQueryBackedResource extends SearchQueryBackedResource {

        public TestableSearchQueryBackedResource(final ChartUtils chartUtils, final JiraAuthenticationContext authenticationContext, final SearchService searchService, final PermissionManager permissionManager, final VelocityRequestContextFactory velocityRequestContextFactory) {
            super(chartUtils, authenticationContext, searchService, permissionManager, velocityRequestContextFactory);
        }
    }

    private void testSuccessfulValidation(final String query, final String expectedQuery, final Project value) {
        when(mockPermissionManager.hasPermission(BROWSE_PROJECTS, value, mockUser)).thenReturn(true);
        testSuccessfulValidation(query, expectedQuery, (Object) value);
    }

    private void testSuccessfulValidation(final String query, final String expectedQuery, final Object value) {
        final ChartUtilsMakeSearchRequestAnswer chartUtilsAnswer = new ChartUtilsMakeSearchRequestAnswer(value);
        when(mockChartUtils.retrieveOrMakeSearchRequest(eq(expectedQuery), anyMapOf(String.class, Object.class))).
                thenAnswer(chartUtilsAnswer);

        final Collection<ValidationError> errors = Lists.newArrayList();
        final Map<String, Object> params = Maps.newHashMap();
        final SearchRequest actual = instance.getSearchRequestAndValidate(query, errors, params);

        assertSame(chartUtilsAnswer.getSearchRequest(), actual);
        assertThat(params, MapMatchers.hasKeyThat(equalTo(chartUtilsAnswer.getKey())));
        assertThat(errors, empty());
    }

    private void testValidationExpectingErrors(final String query,
                                               final String key, final Project project, final ValidationError... expectedErrors) {
        if (StringUtils.isNotEmpty(query)) {
            when(mockPermissionManager.hasPermission(BROWSE_PROJECTS, project, mockUser)).thenReturn(false);
        }
        testValidationExpectingErrors(query, key, (Object) project, expectedErrors);
    }

    private void testValidationExpectingErrors(final String query,
                                               final String key, final Object value, final ValidationError... expectedErrors) {
        if (StringUtils.isNotEmpty(query)) {
            when(mockChartUtils.retrieveOrMakeSearchRequest(eq(query), anyMapOf(String.class, Object.class))).
                    thenAnswer(new ChartUtilsMakeSearchRequestAnswer(key, value));
        }

        final Collection<ValidationError> errors = Lists.newArrayList();
        final Map<String, Object> params = Maps.newHashMap();
        final SearchRequest actual = instance.getSearchRequestAndValidate(query, errors, params);

        assertNull(actual);
        assertEquals(CollectionBuilder.newBuilder(expectedErrors).asList(), errors);
    }

}
