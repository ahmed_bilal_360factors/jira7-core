package com.atlassian.jira.gadgets.system;

import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.fields.FieldException;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.NavigableField;
import com.atlassian.jira.issue.fields.layout.column.ColumnLayoutItem;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.issue.search.util.SearchSortUtil;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.issue.fields.MockNavigableField;
import com.atlassian.jira.rest.api.messages.TextMessage;
import com.atlassian.jira.rest.v1.model.errors.ErrorCollection;
import com.atlassian.jira.rest.v1.model.errors.ValidationError;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.jira.web.component.TableLayoutUtils;
import com.atlassian.query.Query;
import com.atlassian.query.order.OrderBy;
import com.atlassian.query.order.SearchSort;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.jira.gadgets.system.util.MockHttpServletRequest.defaultServletRequestStub;
import static com.atlassian.jira.gadgets.system.util.ResponseMatchers.hasStatus;
import static com.atlassian.jira.gadgets.system.util.ResponseMatchers.sameAs;
import static com.atlassian.jira.gadgets.system.util.ResponseMatchers.statusOk;
import static com.atlassian.jira.rest.v1.util.CacheControl.NO_CACHE;
import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyCollectionOf;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit test for {@link com.atlassian.jira.gadgets.system.IssueTableResource}.
 *
 * @since v4.0
 */
public class TestIssueTableResource {
    @Rule
    public RuleChain init = MockitoMocksInContainer.forTest(this);
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @AvailableInContainer
    @Mock
    TimeZoneManager timeZoneManager;
    @Mock
    ApplicationProperties applicationProperties;
    @Mock
    JiraAuthenticationContext mockJiraAuthenticationContext;
    @Mock
    FieldManager mockFieldManager;
    @Mock
    SearchSortUtil mockSearchSortUtil;
    @Mock
    SearchRequestService mockSearchRequestService;
    @Mock
    SearchService searchService;
    @Mock
    SearchProvider searchProvider;
    @Mock
    VelocityRequestContextFactory velocityRequestContextFactory;
    @Mock
    TableLayoutUtils mockTableLayoutUtils;

    private final ApplicationUser barney = new MockApplicationUser("barney");

    private IssueTableResource itr;

    @Before
    public void setUp() {
        itr = new IssueTableResource(mockJiraAuthenticationContext,
                searchService,
                searchProvider,
                mockTableLayoutUtils,
                mockSearchRequestService,
                mockFieldManager,
                mockSearchSortUtil,
                null, null, null,
                velocityRequestContextFactory,
                applicationProperties,
                null, null);
    }

    @Test
    public void validateJql() {
        assertOkValidationResponse(itr.validateJql("50", null));
        assertOkValidationResponse(itr.validateJql("2", null));
        assertOkValidationResponse(itr.validateJql("20", null));
        assertOkValidationResponse(itr.validateJql("1", null));

        final ValidationError negativeError = new ValidationError(IssueTableResource.NUM_FIELD, "gadget.common.num.negative");
        assertSingleError(itr.validateJql("-51", null), negativeError);
        assertSingleError(itr.validateJql("0", null), negativeError);
        assertSingleError(itr.validateJql("-0", null), negativeError);

        final ValidationError tooHighError = new ValidationError(IssueTableResource.NUM_FIELD, "gadget.common.num.overlimit", "50");
        assertSingleError(itr.validateJql("51", null), tooHighError);
        assertSingleError(itr.validateJql("500", null), tooHighError);

        final ValidationError nanError = new ValidationError(IssueTableResource.NUM_FIELD, "gadget.common.num.nan");
        assertSingleError(itr.validateJql("fruitcake", null), nanError);
        assertSingleError(itr.validateJql("five", null), nanError);
    }

    @Test
    public void validateJqlWithModifiedMax() {
        final ValidationError tooHighError = new ValidationError(IssueTableResource.NUM_FIELD, "gadget.common.num.overlimit", "50");

        when(applicationProperties.getDefaultBackedString("jira.table.gadget.max.rows")).thenReturn("50");
        assertOkValidationResponse(itr.validateJql("50", null));

        when(applicationProperties.getDefaultBackedString("jira.table.gadget.max.rows")).thenReturn("20");
        assertSingleError(itr.validateJql("50", null), new ValidationError(IssueTableResource.NUM_FIELD, "gadget.common.num.overlimit", "20"));

        when(applicationProperties.getDefaultBackedString("jira.table.gadget.max.rows")).thenReturn(null);
        assertOkValidationResponse(itr.validateJql("50", null));

        when(applicationProperties.getDefaultBackedString("jira.table.gadget.max.rows")).thenReturn(null);
        assertSingleError(itr.validateJql("51", null), tooHighError);

        when(applicationProperties.getDefaultBackedString("jira.table.gadget.max.rows")).thenReturn("");
        assertOkValidationResponse(itr.validateJql("50", null));

        when(applicationProperties.getDefaultBackedString("jira.table.gadget.max.rows")).thenReturn("");
        assertSingleError(itr.validateJql("51", null), tooHighError);

        when(applicationProperties.getDefaultBackedString("jira.table.gadget.max.rows")).thenReturn("ab");
        assertOkValidationResponse(itr.validateJql("50", null));

        when(applicationProperties.getDefaultBackedString("jira.table.gadget.max.rows")).thenReturn("ab");
        assertSingleError(itr.validateJql("51", null), tooHighError);
    }

    @Test
    public void validateColumnNames() throws FieldException {
        when(mockJiraAuthenticationContext.getUser()).thenReturn(barney);

        final Set<NavigableField> navigableFields = ImmutableSet.<NavigableField>of(
                new MockNavigableField("col1"), new MockNavigableField("col2"),
                new MockNavigableField("col3"), new MockNavigableField("col4"));
        when(mockFieldManager.getAvailableNavigableFields(barney)).thenReturn(navigableFields);

        final List<ValidationError> errors = Lists.newArrayList();
        itr.validateColumnNames(asList("col1", "col2", "col3", "col4", "--Default--"), errors);
        assertThat(errors, empty());
    }

    @Test
    public void validateColumnNamesNotFound() throws FieldException {
        when(mockJiraAuthenticationContext.getUser()).thenReturn(barney);
        final Set<NavigableField> navigableFields = ImmutableSet.<NavigableField>of(
                new MockNavigableField("col1"), new MockNavigableField("col2"));
        when(mockFieldManager.getAvailableNavigableFields(barney)).thenReturn(navigableFields);

        final List<ValidationError> errors = Lists.newArrayList();
        itr.validateColumnNames(asList("col1", "col2", "col3", "col4"), errors);
        assertEquals(ImmutableList.of(new ValidationError(IssueTableResource.COLUMN_NAMES, "gadget.issuetable.common.cols.not.found", "col3, col4")),
                errors);
    }

    @Test
    public void validateColumnNamesEmpty() throws FieldException {
        final List<ValidationError> errors = Lists.newArrayList();
        itr.validateColumnNames(null, errors);
        assertThat(errors, empty());
    }

    @Test
    public void testStripFilterPrefix() {
        assertEquals(new Long(123L), itr.stripFilterPrefix("filter-123", "filter-"));
        assertEquals(new Long(0L), itr.stripFilterPrefix("foobar-0", "foobar-"));
        assertEquals(new Long(-31L), itr.stripFilterPrefix("project--31", "project-"));
        assertEquals(new Long(10094L), itr.stripFilterPrefix("10094", "project-"));

        exception.expect(NumberFormatException.class);
        itr.stripFilterPrefix("filter-filter-123", "filter-");
    }

    @Test
    public void addOrderByToSearchRequestOrderByEmpty() {
        // checking that we don't blow up for blank sortby
        itr.addOrderByToSearchRequest(null, null);
        itr.addOrderByToSearchRequest(null, "");
        itr.addOrderByToSearchRequest(null, " ");
    }

    @Test
    public void addOrderByToSearchRequest() {
        when(mockJiraAuthenticationContext.getUser()).thenReturn(barney);
        final OrderBy mockOrderBy = mock(OrderBy.class);
        when(mockSearchSortUtil.getOrderByClause(isA(Map.class))).thenReturn(mockOrderBy);

        final SearchRequest searchRequest = new SearchRequest();
        itr.addOrderByToSearchRequest(searchRequest, "foobar:ASC");

        assertNotNull(searchRequest.getQuery());
        verify(mockSearchSortUtil).mergeSearchSorts(eq(barney), anyCollectionOf(SearchSort.class), anyCollectionOf(SearchSort.class), eq(100));
    }

    @Test
    public void getFilterTable() throws Exception {
        when(mockJiraAuthenticationContext.getUser()).thenReturn(barney);

        final Query query = JqlQueryBuilder.newBuilder().buildQuery();
        final SearchRequest mockSearchRequest = mock(SearchRequest.class);
        when(mockSearchRequest.getId()).thenReturn(888L);
        when(mockSearchRequest.getName()).thenReturn("the search request");
        when(mockSearchRequest.getDescription()).thenReturn("the description");
        when(mockSearchRequest.isLoaded()).thenReturn(true);
        when(mockSearchRequest.getQuery()).thenReturn(query);

        when(mockSearchRequestService.getFilter(new JiraServiceContextImpl(barney), 888L)).thenReturn(mockSearchRequest);
        when(searchService.validateQuery(barney, query, 888L)).thenReturn(new MessageSetImpl());
        when(searchProvider.search(eq(query), eq(barney), any(PagerFilter.class))).thenReturn(mock(SearchResults.class));

        final List<String> columnNames = ImmutableList.of("status", "priority");
        final List<ColumnLayoutItem> columnLayoutItems = ImmutableList.of(
                mock(ColumnLayoutItem.class),
                mock(ColumnLayoutItem.class)
        );
        when(mockTableLayoutUtils.getColumns(barney, "context", columnNames, true)).thenReturn(columnLayoutItems);

        final Response response = itr.getFilterTable(defaultServletRequestStub(), "context", "filter-888", columnNames, null, false, 0, "50", false, false, false, true);
        assertThat(response, statusOk());
    }

    @Test
    public void getFilterTableWithJqlAsFilterId() throws Exception {
        when(mockJiraAuthenticationContext.getUser()).thenReturn(barney);

        final Query query = JqlQueryBuilder.newBuilder().buildQuery();

        when(searchService.parseQuery(barney, "summary ~ Test")).thenReturn(new SearchService.ParseResult(query, new MessageSetImpl()));
        when(searchService.getIssueSearchPath(barney, SearchService.IssueSearchParameters.builder().query(query).build())).thenReturn("summary ~ Test");
        when(searchService.validateQuery(barney, query, null)).thenReturn(new MessageSetImpl());
        when(searchProvider.search(eq(query), eq(barney), any(PagerFilter.class))).thenReturn(mock(SearchResults.class));

        final List<String> columnNames = ImmutableList.of("status", "priority");
        final List<ColumnLayoutItem> columnLayoutItems = ImmutableList.of(
                mock(ColumnLayoutItem.class),
                mock(ColumnLayoutItem.class)
        );
        when(mockTableLayoutUtils.getColumns(barney, "context", columnNames, true)).thenReturn(columnLayoutItems);

        final Response response = itr.getFilterTable(defaultServletRequestStub(), "context", "jql-summary ~ Test", columnNames, null, false, 0, "50", false, false, false, true);
        assertThat(response, statusOk());
    }

    @Test
    public void getFilterTableWithValidationFailure() throws Exception {
        when(mockJiraAuthenticationContext.getUser()).thenReturn(barney);
        final Query query = JqlQueryBuilder.newBuilder().buildQuery();

        final SearchRequest mockSearchRequest = mock(SearchRequest.class);
        when(mockSearchRequest.getQuery()).thenReturn(query);

        final SearchRequestService mockSearchRequestService = mock(SearchRequestService.class);
        when(mockSearchRequestService.getFilter(new JiraServiceContextImpl(barney), 888L)).thenReturn(mockSearchRequest);

        final MessageSetImpl validationMessageSet = new MessageSetImpl();
        validationMessageSet.addErrorMessage("failed to validate");
        when(searchService.validateQuery(barney, query, 888L)).thenReturn(validationMessageSet);

        final List<String> columnNames = asList("status", "priority");

        final Response response = itr.getFilterTable(null, "context", "filter-888", columnNames, null, false, 0, "50", false, false, false, false);
        assertThat(response, hasStatus(equalTo(400)));
    }

    @Test
    public void getFilterTableWithJqlAsFilterIdDoesNotParse() throws FieldException {
        when(mockJiraAuthenticationContext.getUser()).thenReturn(barney);

        final MessageSet errors = new MessageSetImpl();
        errors.addErrorMessage("Don't Like You");
        when(searchService.parseQuery(barney, "summary ~ Test")).thenReturn(new SearchService.ParseResult(null, errors));

        final List<String> columnNames = asList("status", "priority");

        final Response response = itr.getFilterTable(null, "context", "jql-summary ~ Test", columnNames, null, false, 0, "50", false, false, false, true);
        assertThat(response, hasStatus(equalTo(400)));
    }

    private void assertSingleError(final Response response, final ValidationError expectedError) {
        assertThat(response, sameAs(Response.status(400).entity(ErrorCollection.Builder.newBuilder(expectedError).build()).build()));
    }

    private void assertOkValidationResponse(final Response response) {
        assertThat(response, sameAs(Response.ok(new TextMessage("No input validation errors found.")).cacheControl(NO_CACHE).build()));
    }
}
