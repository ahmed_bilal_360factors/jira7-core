package com.atlassian.jira.dashboarditem.statistics.service.datecount;


import com.atlassian.fugue.Either;
import com.atlassian.jira.dashboarditem.statistics.service.datecount.beans.DateCountInPeriodResultBean;
import com.atlassian.jira.dashboarditem.statistics.service.datecount.field.Field;
import com.atlassian.jira.dashboarditem.statistics.service.datecount.operation.Operation;
import com.atlassian.jira.dashboarditem.statistics.service.datecount.period.TimePeriod;
import com.atlassian.jira.dashboarditem.statistics.service.versions.VersionsService;
import com.atlassian.jira.dashboarditem.statistics.util.ProjectOrFilterQueryParser;
import com.atlassian.jira.dashboarditem.statistics.util.QueryParser;
import com.atlassian.jira.dashboarditem.statistics.util.QueryUrlSupplier;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.query.Query;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.io.IOException;
import java.util.Set;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

public class DateCountInPeriodServiceImplTest {
    @Rule
    public MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private QueryParser queryParser;
    @Mock
    private QueryUrlSupplier queryUrlSupplier;
    @Mock
    private ProjectOrFilterQueryParser projectOrFilterQueryParser;
    @Mock
    private SearchProvider searchProvider;
    @Mock
    private TimeZoneManager timeZoneManager;
    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private I18nHelper i18nHelper;
    @Mock
    private VersionsService versionsService;

    private DateCountInPeriodServiceImpl dateCountInPeriodService;

    private final String MOCK_JQL = "assignee = bob";
    private final Set<Field> MOCK_FIELDS = Sets.newHashSet(Field.created, Field.resolved);
    private final TimePeriod MOCK_PERIOD = TimePeriod.daily;
    private final Operation MOCK_OPERATION = Operation.count;
    private final Integer MOCK_DAYS = 2;

    @Before
    public void before() {
        when(authenticationContext.getI18nHelper()).thenReturn(i18nHelper);

        when(i18nHelper.getText(anyString())).then(returnsFirstArg());
        when(i18nHelper.getText(anyString(), Matchers.<String>anyVararg())).then(returnsFirstArg());

        dateCountInPeriodService = new DateCountInPeriodServiceImpl(queryParser, queryUrlSupplier, projectOrFilterQueryParser, versionsService,
                searchProvider, timeZoneManager, authenticationContext);

        new MockComponentWorker().init();
    }

    @Test
    public void nullJqlReturnsError() throws IOException, SearchException {
        Either<DateCountInPeriodResultBean, ErrorCollection> resultOrError =
                dateCountInPeriodService.collectData(getParameters(null, MOCK_FIELDS, MOCK_PERIOD, MOCK_DAYS, false, MOCK_OPERATION));

        assertThat(resultOrError.isRight(), is(true));

        ErrorCollection errors = resultOrError.right().get();

        assertThat(errors.getErrorMessages().contains("datecount.service.jql.null"), is(true));
    }

    @Test
    public void nullFieldsReturnsError() throws IOException, SearchException {
        Either<DateCountInPeriodResultBean, ErrorCollection> resultOrError =
                dateCountInPeriodService.collectData(getParameters(MOCK_JQL, null, MOCK_PERIOD, MOCK_DAYS, false, MOCK_OPERATION));

        assertThat(resultOrError.isRight(), is(true));

        ErrorCollection errors = resultOrError.right().get();

        assertThat(errors.getErrorMessages().contains("datecount.service.fields.empty"), is(true));
    }

    @Test
    public void emptyFieldsReturnsError() throws IOException, SearchException {
        Either<DateCountInPeriodResultBean, ErrorCollection> resultOrError =
                dateCountInPeriodService.collectData(getParameters(MOCK_JQL, Sets.<Field>newHashSet(), MOCK_PERIOD, MOCK_DAYS, false, MOCK_OPERATION));

        assertThat(resultOrError.isRight(), is(true));

        ErrorCollection errors = resultOrError.right().get();

        assertThat(errors.getErrorMessages().contains("datecount.service.fields.empty"), is(true));
    }

    @Test
    public void nullFieldReturnsError() throws IOException, SearchException {
        Either<DateCountInPeriodResultBean, ErrorCollection> resultOrError =
                dateCountInPeriodService.collectData(getParameters(MOCK_JQL, Sets.newHashSet((Field) null), MOCK_PERIOD, MOCK_DAYS, false, MOCK_OPERATION));

        assertThat(resultOrError.isRight(), is(true));

        ErrorCollection errors = resultOrError.right().get();

        assertThat(errors.getErrorMessages().contains("datecount.service.fields.hasnull"), is(true));
    }

    @Test
    public void nullPeriodReturnsError() throws IOException, SearchException {
        Either<DateCountInPeriodResultBean, ErrorCollection> resultOrError =
                dateCountInPeriodService.collectData(getParameters(MOCK_JQL, MOCK_FIELDS, null, MOCK_DAYS, false, MOCK_OPERATION));

        assertThat(resultOrError.isRight(), is(true));

        ErrorCollection errors = resultOrError.right().get();

        assertThat(errors.getErrorMessages().contains("datecount.service.period.null"), is(true));
    }

    @Test
    public void nullDaysPreviousReturnsError() throws IOException, SearchException {
        Either<DateCountInPeriodResultBean, ErrorCollection> resultOrError =
                dateCountInPeriodService.collectData(getParameters(MOCK_JQL, MOCK_FIELDS, MOCK_PERIOD, null, false, MOCK_OPERATION));

        assertThat(resultOrError.isRight(), is(true));

        ErrorCollection errors = resultOrError.right().get();

        assertThat(errors.getErrorMessages().contains("datecount.service.daysprevious.null"), is(true));
    }

    @Test
    public void negativeDaysPreviousReturnsError() throws IOException, SearchException {
        Either<DateCountInPeriodResultBean, ErrorCollection> resultOrError =
                dateCountInPeriodService.collectData(getParameters(MOCK_JQL, MOCK_FIELDS, MOCK_PERIOD, -2, false, MOCK_OPERATION));

        assertThat(resultOrError.isRight(), is(true));

        ErrorCollection errors = resultOrError.right().get();

        assertThat(errors.getErrorMessages().contains("datecount.service.daysprevious.positive"), is(true));
    }

    @Test
    public void nullOperationReturnsError() throws IOException, SearchException {
        Either<DateCountInPeriodResultBean, ErrorCollection> resultOrError =
                dateCountInPeriodService.collectData(getParameters(MOCK_JQL, MOCK_FIELDS, MOCK_PERIOD, MOCK_DAYS, false, null));

        assertThat(resultOrError.isRight(), is(true));

        ErrorCollection errors = resultOrError.right().get();

        assertThat(errors.getErrorMessages().contains("datecount.service.operation.null"), is(true));
    }

    @Test
    public void timeBoundedQueryRespectsField() throws IOException, SearchException {
        final Query mockFilterQuery = JqlQueryBuilder.newBuilder().where().savedFilter().eq(10000L).buildQuery();

        final Query createdBoundQuery = dateCountInPeriodService.buildTimeBoundedQuery(Field.valueOf("created"), mockFilterQuery, 90);
        assertThat(createdBoundQuery.toString(), is("{filter = 10000} AND {created >= \"-90d\"}"));

        final Query resolvedBoundQuery = dateCountInPeriodService.buildTimeBoundedQuery(Field.valueOf("resolved"), mockFilterQuery, 60);
        assertThat(resolvedBoundQuery.toString(), is("{filter = 10000} AND {resolved >= \"-60d\"}"));

        final Query unBoundQuery = dateCountInPeriodService.buildTimeBoundedQuery(Field.valueOf("unresolvedTrend"), mockFilterQuery, 90);
        assertThat(unBoundQuery.toString(), is("{filter = 10000}"));
    }

    private DateCountInPeriodService.RequestParameters getParameters(String jql, Set<Field> fields, TimePeriod period, Integer daysPervious, boolean includeVersions, Operation operation) {
        return new DateCountParametersBuilder()
                .jql(jql)
                .fields(fields)
                .period(period)
                .daysPrevious(daysPervious)
                .includeVersions(includeVersions)
                .operation(operation).build();
    }
}
