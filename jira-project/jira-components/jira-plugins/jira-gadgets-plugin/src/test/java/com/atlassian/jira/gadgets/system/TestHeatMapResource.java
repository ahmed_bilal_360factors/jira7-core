package com.atlassian.jira.gadgets.system;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.statistics.StatisticsMapper;
import com.atlassian.jira.issue.statistics.util.FieldValueToDisplayTransformer;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.velocity.VelocityRequestContext;
import com.atlassian.query.Query;
import com.atlassian.query.QueryImpl;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit test for {@link com.atlassian.jira.gadgets.system.HeatMapResource}
 *
 * @since v4.1
 */
public class TestHeatMapResource {
    private static final String CANONICAL_BASE_URL = "http://a";
    private static final double DELTA = 0.001;

    @Test
    public void createSearchUrl() {
        final ApplicationUser mockUser = new MockApplicationUser("test");
        final SearchRequest mockSearchRequest = mock(SearchRequest.class);
        final SearchService mockSearchService = mock(SearchService.class);
        final VelocityRequestContext mockVelocityRequestContext = mock(VelocityRequestContext.class);
        final StatisticsMapper mockStatMapper = mock(StatisticsMapper.class);
        final Query mockSearchQuery = mock(Query.class);
        final Object obj = new Object();

        when(mockStatMapper.getSearchUrlSuffix(eq(obj), eq(mockSearchRequest)))
                .thenReturn(mockSearchRequest);
        when(mockVelocityRequestContext.getCanonicalBaseUrl())
                .thenReturn(CANONICAL_BASE_URL);
        when(mockSearchRequest.getQuery())
                .thenReturn(mockSearchQuery);
        when(mockSearchService.getIssueSearchPath(eq(mockUser), any()))
                .thenReturn("/issues/?queryString");

        // Based on our mock inputs, does the right url get generated?
        final HeatMapResource hmr = new HeatMapResource(null, null, null, null, mockSearchService, null, null);
        final String url = hmr.createSearchUrl(mockUser, mockSearchRequest, mockStatMapper, mockVelocityRequestContext, obj);

        final String expectedUrl = CANONICAL_BASE_URL + "/issues/?queryString";
        assertEquals(expectedUrl, url);

        // It's possible no url suffix was returned, we test that too
        reset(mockStatMapper);
        reset(mockVelocityRequestContext);
        reset(mockSearchRequest);
        reset(mockSearchService);
        ArgumentCaptor<SearchService.IssueSearchParameters> queryCaptor = ArgumentCaptor.forClass(SearchService.IssueSearchParameters.class);
        when(mockStatMapper.getSearchUrlSuffix(eq(obj), eq(mockSearchRequest)))
                .thenReturn(null);
        when(mockVelocityRequestContext.getCanonicalBaseUrl())
                .thenReturn(CANONICAL_BASE_URL);
        when(mockSearchService.getIssueSearchPath(eq(mockUser), queryCaptor.capture())).thenReturn("/issues/");

        final String noSuffixUrl = hmr.createSearchUrl(mockUser, mockSearchRequest, mockStatMapper, mockVelocityRequestContext, obj);
        assertEquals(queryCaptor.getValue().query(), new QueryImpl());

        final String expectedNoSuffixUrl = CANONICAL_BASE_URL + "/issues/";
        assertEquals(expectedNoSuffixUrl, noSuffixUrl);
    }

    @Test
    public void createDataRow() {
        // Just testing that the data row produced actually contains the values we passed in. Only real
        // calculation is figuring out the percentage.
        @SuppressWarnings("unchecked")
        final FieldValueToDisplayTransformer<String> mockFieldValueToDisplayTransformer = mock(FieldValueToDisplayTransformer.class);
        final Object fieldValue = new Object();
        final String rowKey = "rowKey";
        final String statType = "statType";
        final int totalIssues = 200;
        final int expectedIssues = 20;
        // This should be 100 * issues / totalIssues
        final int expectedPercentage = 10;
        final int expectedFontSize = 30;
        final HeatMapResource.DataRow expectedDataRow = new HeatMapResource.DataRow(rowKey, CANONICAL_BASE_URL, expectedIssues, expectedPercentage, expectedFontSize);


        HeatMapResource hmr = new HeatMapResource(null, null, null, null, null, null, null) {
            @Override
            double calculateFontSize(int numberOfData, double percentage) {
                return expectedFontSize;
            }
        };
        when(mockFieldValueToDisplayTransformer.transformFromCustomField(eq(statType), eq(fieldValue),
                eq(CANONICAL_BASE_URL)))
                .thenReturn(rowKey);

        HeatMapResource.DataRow result = hmr.createDataRow(fieldValue, mockFieldValueToDisplayTransformer, statType, totalIssues, CANONICAL_BASE_URL, 10, expectedIssues);
        verify(mockFieldValueToDisplayTransformer).transformFromCustomField(eq(statType), eq(fieldValue), eq(CANONICAL_BASE_URL));
        assertEquals(result, expectedDataRow);

    }

    @Test
    public void calculateFontSize() {
        HeatMapResource hmr = new HeatMapResource(null, null, null, null, null, null, null);

        // font size based on number of issues and percentage of these issues
        assertEquals(hmr.calculateFontSize(0, 0), HeatMapResource.MIN_FONT, DELTA);
        assertEquals(hmr.calculateFontSize(100, 100), HeatMapResource.MIN_FONT + HeatMapResource.MAX_FONT, DELTA);
        assertEquals(hmr.calculateFontSize(10, 100), 62.0, DELTA);
    }
}
