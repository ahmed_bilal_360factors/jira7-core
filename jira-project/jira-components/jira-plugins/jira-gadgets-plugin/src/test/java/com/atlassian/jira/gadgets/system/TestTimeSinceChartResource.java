package com.atlassian.jira.gadgets.system;

import com.atlassian.jira.charts.ChartFactory;
import com.atlassian.jira.charts.util.ChartUtils;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.gadgets.system.util.ResourceDateValidator;
import com.atlassian.jira.issue.fields.DateField;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.rest.v1.model.errors.ErrorCollection;
import com.atlassian.jira.rest.v1.model.errors.ValidationError;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.emptyCollectionOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyCollectionOf;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.withSettings;

/**
 * Unit test for {@link com.atlassian.jira.gadgets.system.TimeSinceChartResource}.
 *
 * @since v4.0
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTimeSinceChartResource {
    @Mock
    private ApplicationProperties applicationProperties;
    @Mock
    private FieldManager fieldManager;

    @Test
    public void testValidate() {
        final String expectedQueryString = "some query";
        final String expectedDateField = "the date field";
        final String days = "440";
        final String period = "someperiod";
        ChartUtils chartUtils = getParamsUpdatingChartUtilsMock();

        when(fieldManager.getField(expectedDateField)).thenReturn(mock(Field.class, withSettings().extraInterfaces(DateField.class)));
        when(fieldManager.isCustomField(any(Field.class))).thenReturn(false);

        final ResourceDateValidator mockValidator = mock(ResourceDateValidator.class);
        when(mockValidator.validatePeriod(eq(TimeSinceChartResource.PERIOD_NAME), eq(period), anyCollectionOf(ValidationError.class))).thenReturn(null);
        when(mockValidator.validateDaysPrevious(eq(TimeSinceChartResource.DAYS), (ChartFactory.PeriodName) isNull(), eq(days), anyCollectionOf(ValidationError.class))).thenReturn(-800);

        TimeSinceChartResource tscr = new TimeSinceChartResource(chartUtils, null, null, null, null, fieldManager, mockValidator, null);
        final Response response = tscr.validateChart(expectedQueryString, expectedDateField, days, period);

        verify(chartUtils).retrieveOrMakeSearchRequest(eq(expectedQueryString), anyMapOf(String.class, Object.class));
        verify(fieldManager).getField(eq(expectedDateField));
        assertThat(response.getStatus(), Matchers.equalTo(200));
    }

    @Test
    public void testValidateDateFieldHappy() {
        Field mockField = mock(Field.class, withSettings().extraInterfaces(DateField.class));
        final String fieldName = "someDateField";
        when(fieldManager.getField(fieldName)).thenReturn(mockField);
        TimeSinceChartResource tscr = new TimeSinceChartResource(null, null, null, null, null, fieldManager, null, applicationProperties);
        final List<ValidationError> errors = Collections.emptyList();

        final Field field = tscr.validateDateField(fieldName, errors);

        assertThat(field, sameInstance(mockField));
        assertThat(errors, emptyCollectionOf(ValidationError.class));
    }

    @Test
    public void testValidateDateFieldSad() {
        Field mockField = mock(Field.class);
        final String fieldId = "customfield_123";
        final String fieldName = "theFieldName";
        when(mockField.getName()).thenReturn(fieldName);
        when(fieldManager.getField(fieldId)).thenReturn(mockField);
        TimeSinceChartResource tscr = new TimeSinceChartResource(null, null, null, null, null, fieldManager, null, applicationProperties);
        final List<ValidationError> errors = new ArrayList<ValidationError>();

        tscr.validateDateField(fieldId, errors);

        assertThat(errors, contains(samePropertyValuesAs(
                new ValidationError(
                        TimeSinceChartResource.DAYS,
                        "gadget.time.since.not.date.field",
                        Arrays.asList(fieldId, fieldName))
        )));
    }

    @Test
    public void testValidateDateFieldDoesntExist() {
        final String fieldId = "customfield_123";
        when(fieldManager.getField(fieldId)).thenReturn(null);
        TimeSinceChartResource tscr = new TimeSinceChartResource(null, null, null, null, null, fieldManager, null, applicationProperties);
        final List<ValidationError> errors = new ArrayList<ValidationError>();

        final Field field = tscr.validateDateField(fieldId, errors);

        assertThat(field, is(nullValue()));

        assertThat(errors, contains(samePropertyValuesAs(
                new ValidationError(
                        TimeSinceChartResource.DAYS,
                        "gadget.time.since.invalid.date.field",
                        Collections.singletonList(fieldId))
        )));
    }

    @Test
    public void testGenerateChartInvalid() {
        JiraAuthenticationContext mockJiraAuthenticationContext = mock(JiraAuthenticationContext.class);
        final ApplicationUser barney = new MockApplicationUser("barney", "Barney Google", "testing@other.peoples.code.com");
        when(mockJiraAuthenticationContext.getLoggedInUser()).thenReturn(barney);

        final ValidationError daysError = new ValidationError("daysField", "daysValidationError");
        final ValidationError periodError = new ValidationError("periodField", "periodValidationError");
        final ResourceDateValidator failingDateValidator = new ResourceDateValidator(applicationProperties) {
            @Override
            public int validateDaysPrevious(final String fieldName, final ChartFactory.PeriodName period, final String days, final Collection<ValidationError> errors) {
                errors.add(daysError);
                return -1;
            }

            @Override
            public ChartFactory.PeriodName validatePeriod(final String fieldName, final String periodName, final Collection<ValidationError> errors) {
                errors.add(periodError);
                return null;
            }
        };


        ChartUtils chartUtils = getParamsUpdatingChartUtilsMock();

        when(fieldManager.getField(anyString())).thenReturn(mock(Field.class, withSettings().extraInterfaces(DateField.class)));
        when(fieldManager.isCustomField(any(Field.class))).thenReturn(false);

        TimeSinceChartResource tscr = new TimeSinceChartResource(chartUtils, mockJiraAuthenticationContext, null, null, null, fieldManager, failingDateValidator, null);
        Response response = tscr.generateChart(null, "queryString", null, null, null, true, 100, 101, true);
        assertEquals(400, response.getStatus());

        final ErrorCollection errors = (ErrorCollection) response.getEntity();
        assertThat(errors.getErrors(), containsInAnyOrder(periodError, daysError));
    }

    private ChartUtils getParamsUpdatingChartUtilsMock() {
        ChartUtils chartUtils = mock(ChartUtils.class);
        when(chartUtils.retrieveOrMakeSearchRequest(anyString(), anyMapOf(String.class, Object.class))).then(new Answer<Object>() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                Map<String, Object> params = (Map<String, Object>) invocation.getArguments()[1];
                params.put(TimeSinceChartResource.QUERY_STRING, "filter-abcd");
                params.put("searchRequest", "searchRequest");
                return null;
            }
        });
        return chartUtils;
    }
}
