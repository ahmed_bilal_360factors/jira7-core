package com.atlassian.jira.dashboarditem.login;

import com.atlassian.jira.bc.security.login.LoginProperties;
import com.atlassian.jira.bc.security.login.LoginService;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.help.MockHelpUrl;
import com.atlassian.jira.help.MockHelpUrls;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.RecoveryMode;
import com.atlassian.jira.user.util.StaticRecoveryMode;
import com.google.common.collect.ImmutableMap;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

public class LoginContextProviderTest {
    @Rule
    public InitMockitoMocks mockitoMocks = new InitMockitoMocks(this);
    @Mock
    private ApplicationProperties applicationProperties;
    @Mock
    private LoginService loginService;
    private JiraAuthenticationContext context = MockSimpleAuthenticationContext.createNoopContext((ApplicationUser) null);
    private RecoveryMode recoveryMode = StaticRecoveryMode.disabled();
    private MockHelpUrl url = new MockHelpUrl().setUrl("http://example.com").setTitle("Something").setKey("recovery-mode");
    private MockHelpUrls urls = new MockHelpUrls().addUrl(url);
    private LoginProperties properties = LoginProperties.builder().build();
    private LoginContextProvider provider;

    @Before
    public void setup() {
        when(loginService.getLoginProperties(Mockito.any(ApplicationUser.class), Mockito.any(HttpServletRequest.class))).thenReturn(properties);
        provider = new LoginContextProvider(applicationProperties, loginService, context, recoveryMode, urls);
    }

    @Test
    public void loginContextProviderReturnsCorrectRecoveryVariablesWhenRecoveryModeEnabled() {
        //given
        final String recoveryAdmin = "recovery_admin";
        final StaticRecoveryMode recoveryMode = StaticRecoveryMode.enabled(recoveryAdmin);
        final LoginContextProvider provider =
                new LoginContextProvider(applicationProperties, loginService, context, recoveryMode, urls);

        //when
        final Map<String, Object> contextMap = provider.getContextMap(ImmutableMap.<String, Object>of());

        //then
        assertThat(contextMap, Matchers.<String, Object>hasEntry("recoveryModeEnabled", true));
        assertThat(contextMap, Matchers.<String, Object>hasEntry("recoveryModeHelpUrl", url.getUrl()));
        assertThat(contextMap, Matchers.<String, Object>hasEntry("recoveryModeUser", recoveryAdmin));
    }

    @Test
    public void loginContextProviderReturnsCorrectRecoveryVariablesWhenRecoveryModeDisabled() {
        //when
        final Map<String, Object> contextMap = provider.getContextMap(ImmutableMap.<String, Object>of());

        //then
        assertThat(contextMap, Matchers.<String, Object>hasEntry("recoveryModeEnabled", false));
        assertThat(contextMap, Matchers.<String, Object>hasEntry("recoveryModeHelpUrl", url.getUrl()));
        assertThat(contextMap, Matchers.hasEntry("recoveryModeUser", null));
    }
}
