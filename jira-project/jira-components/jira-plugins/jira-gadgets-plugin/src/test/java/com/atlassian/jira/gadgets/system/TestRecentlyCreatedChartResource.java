package com.atlassian.jira.gadgets.system;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.charts.Chart;
import com.atlassian.jira.charts.ChartFactory;
import com.atlassian.jira.charts.util.ChartUtils;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.rest.api.messages.TextMessage;
import com.atlassian.jira.rest.v1.model.errors.ErrorCollection;
import com.atlassian.jira.rest.v1.model.errors.ValidationError;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.google.common.collect.ImmutableMap;
import org.jfree.chart.urls.CategoryURLGenerator;
import org.jfree.data.category.CategoryDataset;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

import static com.atlassian.jira.gadgets.system.util.MockHttpServletRequest.defaultServletRequestStub;
import static com.atlassian.jira.gadgets.system.util.ResponseMatchers.sameAs;
import static com.atlassian.jira.rest.v1.util.CacheControl.NO_CACHE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests REST endpoint of recently created chart resource
 *
 * @since v4.0
 */
public class TestRecentlyCreatedChartResource {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    ChartUtils mockChartUtils;
    @Mock
    JiraAuthenticationContext mockAuthCtx;
    @Mock
    SearchService mockSearchService;
    @Mock
    PermissionManager mockPermissionManager;
    @Mock
    ChartFactory mockChartFactory;
    @Mock
    ApplicationProperties mockApplicationProperties;

    private final ApplicationUser mockUser = new MockApplicationUser("fred");

    @InjectMocks
    private MockSearchQueryBackedResource instance;

    @Test
    public final void validateSuccessfulWithVerbatimQuery() {
        final String query = "blah-100";
        final String days = "30";
        final String periodName = "daily";
        final String expectedQuery = "blah-100";
        validateExpectingSuccess(query, days, periodName, expectedQuery);
    }

    @Test
    public final void validateSuccessfulDefaultsToFilter() {
        final String query = "filter-100";
        final String days = "30";
        final String periodName = "daily";
        final String expectedQuery = "filter-100";
        validateExpectingSuccess(query, days, periodName, expectedQuery);
    }

    @Test
    public final void validationFailsOnBadQuery() {
        final String query = "bad-query";
        final String days = "10";
        final String periodName = "daily";
        final ValidationError error = new ValidationError("foo", "bar");

        when(mockApplicationProperties.getDefaultBackedString("jira.chart.days.previous.limit." + periodName))
                .thenReturn(days);

        validateExpectingSearchError(query, days, periodName, error);
    }

    @Test
    public final void validationFailsOnInvalidPeriodName() {
        final String query = "good-query";
        final String days = "10";
        final String periodName = "rarely";
        final ValidationError error = new ValidationError("periodName", "gadget.common.invalid.period");
        validateExpectingError(query, days, periodName, error);
    }

    @Test
    public final void validationFailsOnNegativeDays() {
        final String query = "good-query";
        final String days = "-1";
        final String periodName = "daily";
        final ValidationError error = new ValidationError("daysprevious", "gadget.common.negative.days");
        validateExpectingError(query, days, periodName, error);
    }

    @Test
    public final void validationFailsOnNonNumericDays() {
        final String query = "good-query";
        final String days = "nan";
        final String periodName = "daily";
        final ValidationError error = new ValidationError("daysprevious", "gadget.common.days.nan");
        validateExpectingError(query, days, periodName, error);
    }

    @Test
    public final void generateSucceedsWithNoData() {
        final String query = "blah-100";
        final int days = 30;
        final String periodName = "daily";
        final String expectedQuery = "blah-100";
        generateExpectingSuccess(query, days, periodName, expectedQuery, null);
    }

    @Test
    public final void generateDefaultsToFilter() {
        final String query = "filter-100";
        final int days = 30;
        final String periodName = "daily";
        final String expectedQuery = "filter-100";
        generateExpectingSuccess(query, days, periodName, expectedQuery, null);
    }

    @Test
    public void generateSucceedsWithData() {
        final String query = "blah-100";
        final int days = 30;
        final String periodName = "daily";
        final String expectedQuery = "blah-100";
        final RecentlyCreatedChartResource.DataRow[] data = new RecentlyCreatedChartResource.DataRow[2];
        data[0] = new RecentlyCreatedChartResource.DataRow("key1", 3, 1, "resolvedUrl1", 2, "unresolvedUrl1");
        data[1] = new RecentlyCreatedChartResource.DataRow("key2", 30, 10, "resolvedUrl1", 20, "unresolvedUrl1");
        generateExpectingSuccess(query, days, periodName, expectedQuery, data);
    }

    @Test
    public final void generateFailsOnBadQuery() {
        final String query = "bad-query";
        final String days = "10";
        final String periodName = "daily";
        final ValidationError error = new ValidationError("foo", "bar");

        when(mockApplicationProperties.getDefaultBackedString("jira.chart.days.previous.limit." + periodName))
                .thenReturn(days);

        generateExpectingSearchError(query, days, periodName, error);
    }

    @Test
    public final void generateFailsOnInvalidPeriod() {
        final String query = "good-query";
        final String days = "10";
        final String periodName = "rarely";
        final ValidationError error = new ValidationError("periodName", "gadget.common.invalid.period");
        generateExpectingError(query, days, periodName, error);
    }

    @Test
    public final void generateFailsOnNegativeDays() {
        final String query = "good-query";
        final String days = "-1";
        final String periodName = "daily";
        final ValidationError error = new ValidationError("daysprevious", "gadget.common.negative.days");
        generateExpectingError(query, days, periodName, error);
    }

    @Test
    public final void generateFailsOnNonNumericDays() {
        final String query = "good-query";
        final String days = "nan";
        final String periodName = "daily";
        final ValidationError error = new ValidationError("daysprevious", "gadget.common.days.nan");
        generateExpectingError(query, days, periodName, error);
    }

    static class MockSearchQueryBackedResource extends RecentlyCreatedChartResource {
        private String expectedQueryString;
        private final Collection<ValidationError> errorsToReturn = new ArrayList<ValidationError>();

        MockSearchQueryBackedResource(final ChartUtils chartUtils, final JiraAuthenticationContext authenticationContext, final SearchService searchService, final PermissionManager permissionManager, final ChartFactory chartFactory, final ApplicationProperties applicationProperties) {
            super(chartUtils, authenticationContext, searchService, permissionManager, chartFactory, null, applicationProperties);
        }

        public void setExpectedQueryString(final String anExpectedQueryString) {
            this.expectedQueryString = anExpectedQueryString;
        }

        public void addErrorsToReturn(final ValidationError... errorsToAdd) {
            if (errorsToAdd != null) {
                errorsToReturn.addAll(Arrays.asList(errorsToAdd));
            }
        }

        @Override
        protected SearchRequest getSearchRequestAndValidate(final String queryString, final Collection<ValidationError> errors, final Map<String, Object> params) {
            assertEquals(expectedQueryString, queryString);
            assertNotNull(errors);
            assertNotNull(params);
            errors.addAll(errorsToReturn);
            return new SearchRequest();
        }

        @Override
        protected String getFilterTitle(final Map<String, Object> params) {
            return "filterTitle";
        }

        @Override
        protected String getFilterUrl(final Map<String, Object> params) {
            return "filterUrl";
        }
    }

    private void validateExpectingSuccess(final String query, final String days, final String periodName, final String expectedQuery) {
        instance.setExpectedQueryString(expectedQuery);
        when(mockApplicationProperties.getDefaultBackedString("jira.chart.days.previous.limit." + periodName))
                .thenReturn(days);

        final Response response = instance.validateChart(query, days, periodName);
        assertThat(response, sameAs(Response.ok(new TextMessage("No input validation errors found.")).cacheControl(NO_CACHE).build()));
    }

    private void validateExpectingSearchError(final String expectedQuery, final String days, final String periodName, final ValidationError expectedError) {
        instance.addErrorsToReturn(expectedError);
        validateExpectingError(expectedQuery, days, periodName, expectedError);
    }

    private void validateExpectingError(final String expectedQuery, final String days, final String periodName, final ValidationError expectedError) {
        instance.setExpectedQueryString(expectedQuery);

        final Response response = instance.validateChart(expectedQuery, days, periodName);
        assertThat(response, sameAs(Response.status(400).entity(ErrorCollection.Builder.newBuilder(expectedError).build()).build()));
    }

    private void generateExpectingSuccess(final String query, final int days, final String periodName,
                                          final String expectedQuery, final RecentlyCreatedChartResource.DataRow[] expectedReturnData) {
        when(mockAuthCtx.getLoggedInUser()).thenReturn(mockUser);

        final CategoryDataset dataSet = mock(CategoryDataset.class);
        final CategoryURLGenerator mockUrlGenerator = mock(CategoryURLGenerator.class);

        if (expectedReturnData != null) {
            when(dataSet.getColumnCount()).thenReturn(expectedReturnData.length);

            for (int i = 0; i < expectedReturnData.length; i++) {
                when(dataSet.getColumnKey(i)).thenReturn(expectedReturnData[i].getKey());
                when(dataSet.getValue(0, i)).thenReturn(expectedReturnData[i].getUnresolvedValue());
                when(mockUrlGenerator.generateURL(dataSet, 0, i)).thenReturn(expectedReturnData[i].getUnresolvedUrl());
                when(dataSet.getValue(1, i)).thenReturn(expectedReturnData[i].getResolvedValue());
                when(mockUrlGenerator.generateURL(dataSet, 1, i)).thenReturn(expectedReturnData[i].getResolvedUrl());
            }
        }

        instance.setExpectedQueryString(expectedQuery);

        when(mockApplicationProperties.getDefaultBackedString("jira.chart.days.previous.limit." + periodName))
                .thenReturn(String.valueOf(days));

        when(mockChartFactory.generateRecentlyCreated(
                isA(ChartFactory.ChartContext.class), eq(days), eq(ChartFactory.PeriodName.daily))).
                thenReturn(new Chart("location", "imageMap", "imageMapName",
                        ImmutableMap.<String, Object>builder().
                                put("numIssues", 3).
                                put("completeDataset", dataSet).
                                put("completeDatasetUrlGenerator", mockUrlGenerator).
                                put("width", 400).
                                put("base64Image", "base64Image==").
                                put("height", 250).build()));

        final Response response = instance.generateChart(defaultServletRequestStub(), query, String.valueOf(days), periodName, expectedReturnData != null, 400, 250, true);

        final RecentlyCreatedChartResource.RecentlyCreatedChart expectedChart = new RecentlyCreatedChartResource.RecentlyCreatedChart(
                "location", "filterTitle", "filterUrl", "imageMap", "imageMapName", 3, 400, 250, expectedReturnData, "base64Image==");
        assertThat(response, sameAs(Response.ok(expectedChart).build()));
    }

    private void generateExpectingSearchError(final String expectedQuery, final String days, final String periodName, final ValidationError expectedError) {
        instance.addErrorsToReturn(expectedError);
        generateExpectingError(expectedQuery, days, periodName, expectedError);
    }

    private void generateExpectingError(final String expectedQuery, final String days, final String periodName, final ValidationError expectedError) {
        instance.setExpectedQueryString(expectedQuery);
        when(mockAuthCtx.getLoggedInUser()).thenReturn(mockUser);

        final Response response = instance.generateChart(defaultServletRequestStub(), expectedQuery, days, periodName, false, 400, 250, true);
        assertThat(response, sameAs(Response.status(400).entity(ErrorCollection.Builder.newBuilder(expectedError).build()).build()));
    }
}