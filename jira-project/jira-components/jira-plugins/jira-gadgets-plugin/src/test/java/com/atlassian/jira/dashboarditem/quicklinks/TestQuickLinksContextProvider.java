package com.atlassian.jira.dashboarditem.quicklinks;

import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.google.common.collect.ImmutableMap;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class TestQuickLinksContextProvider {
    @Rule
    public MockitoContainer container = MockitoMocksInContainer.rule(this);

    @Mock
    private QuickLinksProvider quicklinksProvider;

    private QuickLinksContextProvider quicklinksContextProvider;

    @Before
    public void setUp() throws Exception {
        quicklinksContextProvider = new QuickLinksContextProvider(quicklinksProvider);
    }

    @Test
    public void standardSetOfLinksIsPresentInProvidedContext() {
        List<Link> commonLinks = new ArrayList<>();
        commonLinks.add(new Link("text", "http://url.com", "title"));
        List<Link> navigationLinks = new ArrayList<>();
        navigationLinks.add(new Link("text2", "http://url2.com", "title2"));
        when(quicklinksProvider.getLinks()).thenReturn(new LinkLists(navigationLinks, commonLinks));

        final Map<String, Object> contextMap = quicklinksContextProvider.getContextMap(ImmutableMap.<String, Object>of());

        assertThat(contextMap, Matchers.hasEntry("commonLinks", commonLinks));
        assertThat(contextMap, Matchers.hasEntry("navigationLinks", navigationLinks));
    }

    @Test
    public void noCommonOrNavigationLinksStillReturnsValidContextMap() {
        List<Link> commonLinks = new ArrayList<>();
        List<Link> navigationLinks = new ArrayList<>();
        when(quicklinksProvider.getLinks()).thenReturn(new LinkLists(navigationLinks, commonLinks));

        final Map<String, Object> contextMap = quicklinksContextProvider.getContextMap(ImmutableMap.<String, Object>of());

        assertThat(contextMap, Matchers.hasEntry("commonLinks", commonLinks));
        assertThat(contextMap, Matchers.hasEntry("navigationLinks", navigationLinks));
    }
}