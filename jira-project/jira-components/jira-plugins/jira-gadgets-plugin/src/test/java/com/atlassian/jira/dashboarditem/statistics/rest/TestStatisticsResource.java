package com.atlassian.jira.dashboarditem.statistics.rest;


import com.atlassian.fugue.Either;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.dashboarditem.statistics.service.statistics.StatisticsService;
import com.atlassian.jira.dashboarditem.statistics.service.statistics.beans.OneDimensionalStatisticsResultBean;
import com.atlassian.jira.dashboarditem.statistics.service.statistics.searcher.beans.StatisticsSearchResultBean;
import com.atlassian.jira.dashboarditem.statistics.service.statistics.searcher.beans.StatisticsSearchResultRowBean;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.query.Query;
import org.apache.commons.httpclient.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestStatisticsResource {
    @Mock
    private StatisticsService statisticsService;
    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private SearchRequestService searchRequestService;
    @Mock
    private SearchService searchService;
    @Mock
    private ApplicationUser user;
    @Mock
    private SearchRequest searchRequest;
    @Mock
    private I18nHelper i18nHelper;

    private StatisticsResource statisticsResource;
    private List<String> errorMessages;

    @Before
    public void setUp() {
        statisticsResource = new StatisticsResource(authenticationContext, searchRequestService, searchService, statisticsService);
        errorMessages = Arrays.asList("One error", "Second error");
        when(authenticationContext.getLoggedInUser()).thenReturn(user);
    }

    @Test
    public void testSuccessfulJqlSearchReturnsValidResponse() {
        final OneDimensionalStatisticsResultBean statisticsResult = statisticsResultForValidRequest();
        Either<OneDimensionalStatisticsResultBean, ErrorCollection> result = Either.left(statisticsResult);
        when(statisticsService.aggregateOneDimensionalStats(anyString(), anyLong(), anyString())).thenReturn(result);

        Response response = statisticsResource.oneDimensionalSearch("jql", null, "statType");

        assertThat(response.getStatus(), is(200));
        assertThat(response.getEntity(), is((Object) statisticsResult));
    }

    @Test
    public void testUnsuccesfulSearchReturnsErrorCodeFromReason() {
        Either<OneDimensionalStatisticsResultBean, ErrorCollection> result = resultWithErrorReason(ErrorCollection.Reason.FORBIDDEN);
        when(statisticsService.aggregateOneDimensionalStats(anyString(), anyLong(), anyString())).thenReturn(result);

        Response response = statisticsResource.oneDimensionalSearch("jql", null, "statType");

        assertThat(response.getStatus(), is(HttpStatus.SC_FORBIDDEN));
        assertThat(response.getEntity(), is((Object) errorMessages));
    }

    @Test
    public void neitherFilterIdNorJql() {
        mockForErrorResponse("someError");

        Response response = statisticsResource.oneDimensionalSearch(null, null, "statType");

        assertEquals(400, response.getStatus());
        LinkedList<String> errorMessages = (LinkedList) response.getEntity();

        // We should get two errors back, one for no filter and one for no Jql.
        assertEquals("someError", errorMessages.get(0));
        assertEquals("someError", errorMessages.get(1));
    }

    @Test
    public void returnsBadRequestResponseIfNoStatTypeGiven() {
        mockForErrorResponse("someError");

        Response response = statisticsResource.oneDimensionalSearch("jql", 10000L, null);

        assertEquals(400, response.getStatus(), 400);
        assertEquals("someError", ((LinkedList) response.getEntity()).getFirst());
    }

    @Test
    public void validSearchForFilterWithNoJqlAndFilterId() {
        assertResponseValidToFilterIdQuery(null, 10000L, "statType");
    }

    @Test
    public void validSearchForFilterWithJqlAndFilterId() {
        // StatisticsResource should use filter id over the jql provided
        assertResponseValidToFilterIdQuery("someJql", 10000L, "statType");
    }

    @Test
    public void validSearchForFilterWhereFilterDoesNotExist() {
        when(searchRequestService.getFilter(any(JiraServiceContext.class), eq(10005L))).thenReturn(null);
        mockForErrorResponse("someErrorMessage");

        Response response = statisticsResource.oneDimensionalSearch(null, 10005L, "statType");

        assertEquals(404, response.getStatus());
        assertEquals("someErrorMessage", ((LinkedList) response.getEntity()).getFirst());
    }

    private void assertResponseValidToFilterIdQuery(final String jql, final Long filterId, final String statType) {
        OneDimensionalStatisticsResultBean statisticsResult = statisticsResultForValidRequest();
        Either<OneDimensionalStatisticsResultBean, ErrorCollection> result = Either.left(statisticsResult);
        when(statisticsService.aggregateOneDimensionalStats(anyString(), anyLong(), anyString())).thenReturn(result);
        when(searchRequestService.getFilter(any(JiraServiceContext.class), eq(filterId))).thenReturn(searchRequest);
        when(searchService.getJqlString(any(Query.class))).thenReturn("SomeJqlString");
        when(searchRequest.getName()).thenReturn("Title");

        Response response = statisticsResource.oneDimensionalSearch(jql, filterId, statType);
        OneDimensionalStatisticsResultBean resultEntity = (OneDimensionalStatisticsResultBean) response.getEntity();

        assertEquals(200, response.getStatus());
        assertEquals(statisticsResult.getFilterTitle(), resultEntity.getFilterTitle());
        assertEquals(statisticsResult.getFilterUrl(), resultEntity.getFilterUrl());
        assertEquals(statisticsResult.getIssueCount(), resultEntity.getIssueCount());
        assertEquals(statisticsResult.getResults(), resultEntity.getResults());
    }

    private Either<OneDimensionalStatisticsResultBean, ErrorCollection> resultWithErrorReason(ErrorCollection.Reason reason) {
        ErrorCollection errors = new SimpleErrorCollection();
        errors.addErrorMessages(errorMessages);
        errors.addReason(reason);
        return Either.right(errors);
    }

    private OneDimensionalStatisticsResultBean statisticsResultForValidRequest() {
        final List<StatisticsSearchResultRowBean> statisticRows = new ArrayList<StatisticsSearchResultRowBean>();
        statisticRows.add(new StatisticsSearchResultRowBean("key", "url", 5L));

        final StatisticsSearchResultBean statisticsSearchResultBean = new StatisticsSearchResultBean(1, statisticRows);
        return new OneDimensionalStatisticsResultBean("Title", "url", "statType", statisticsSearchResultBean);
    }

    private void mockForErrorResponse(String errorMessage) {
        when(authenticationContext.getI18nHelper()).thenReturn(i18nHelper);
        when(i18nHelper.getText(anyString())).thenReturn(errorMessage);
        when(i18nHelper.getText(anyString(), anyString())).thenReturn(errorMessage);
    }
}
