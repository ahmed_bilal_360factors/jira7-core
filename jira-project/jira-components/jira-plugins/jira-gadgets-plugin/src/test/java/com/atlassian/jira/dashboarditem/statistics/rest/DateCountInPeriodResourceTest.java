package com.atlassian.jira.dashboarditem.statistics.rest;

import com.atlassian.fugue.Either;
import com.atlassian.jira.dashboarditem.statistics.service.datecount.DateCountInPeriodService;
import com.atlassian.jira.dashboarditem.statistics.service.datecount.beans.DateCountInPeriodResultBean;
import com.atlassian.jira.dashboarditem.statistics.service.datecount.operation.Operation;
import com.atlassian.jira.dashboarditem.statistics.service.datecount.period.TimePeriod;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.rest.util.ResponseFactory;
import com.atlassian.jira.rest.util.ResponseFactoryImpl;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.google.common.collect.Lists;
import org.apache.commons.httpclient.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DateCountInPeriodResourceTest {
    @Mock
    private DateCountInPeriodService dateCountInPeriodService;

    @Mock
    private JiraAuthenticationContext authenticationContext;

    @Mock
    private I18nHelper i18nHelper;

    @Mock
    private DateCountInPeriodResultBean serviceCorrectResponse;

    private ResponseFactory responseFactory = new ResponseFactoryImpl(null);

    private DateCountInPeriodResource dateCountInPeriodResource;

    private static final String MOCK_JQL = "assignee = bob";

    private static final List<String> MOCK_FIELDS = Lists.newArrayList("created", "resolved");

    private static final String MOCK_PERIOD_NAME = "weekly";

    private static final Integer MOCK_DAYS_PREVIOUS = 20;

    private static final String MOCK_OPERATION_NAME = "count";

    @Before
    public void before() {
        when(authenticationContext.getI18nHelper()).thenReturn(i18nHelper);

        when(i18nHelper.getText(anyString())).then(returnsFirstArg());
        when(i18nHelper.getText(anyString(), Matchers.<String>anyVararg())).then(returnsFirstArg());
        dateCountInPeriodResource = new DateCountInPeriodResource(dateCountInPeriodService, authenticationContext, responseFactory);
    }


    @Test
    public void testIncorrectFieldReturns400() throws IOException, SearchException {
        List<String> incorrectFields = Lists.newArrayList("badfield");
        Response response = dateCountInPeriodResource.completeCreatedVsResolved(MOCK_JQL, incorrectFields, MOCK_PERIOD_NAME, MOCK_DAYS_PREVIOUS, false, MOCK_OPERATION_NAME);

        assertEquals("Should have returned a failed response", response.getStatus(), HttpStatus.SC_BAD_REQUEST);

        @SuppressWarnings("unchecked")
        Map<String, String> errors = (Map<String, String>) response.getEntity();

        assertThat(errors.get(DateCountInPeriodResource.FIELD), is("rest.invalid.field.valid.fields"));
    }

    @Test
    public void testMissingPeriodReturns400() throws IOException, SearchException {
        Response response = dateCountInPeriodResource.completeCreatedVsResolved(MOCK_JQL, MOCK_FIELDS, null, MOCK_DAYS_PREVIOUS, false, MOCK_OPERATION_NAME);

        assertEquals("Should have returned a failed response", response.getStatus(), HttpStatus.SC_BAD_REQUEST);

        @SuppressWarnings("unchecked")
        Map<String, String> errors = (Map<String, String>) response.getEntity();

        assertThat(errors.get(DateCountInPeriodResource.PERIOD), is("rest.missing.field"));
    }

    @Test
    public void testOnlyValidPeriodsWork() throws IOException, SearchException {
        for (TimePeriod period : TimePeriod.values()) {
            final String periodName = period.toString();
            successfulServiceCall();
            Response response = dateCountInPeriodResource.completeCreatedVsResolved(MOCK_JQL, MOCK_FIELDS, periodName, MOCK_DAYS_PREVIOUS, false, MOCK_OPERATION_NAME);

            assertEquals("Should have returned a successful response for " + periodName, response.getStatus(), HttpStatus.SC_OK);
        }


        Response response = dateCountInPeriodResource.completeCreatedVsResolved(MOCK_JQL, MOCK_FIELDS, "notvalidperiodname", MOCK_DAYS_PREVIOUS, false, MOCK_OPERATION_NAME);

        assertEquals("Should have failed on invalid period name", response.getStatus(), HttpStatus.SC_BAD_REQUEST);


        @SuppressWarnings("unchecked")
        Map<String, String> errors = (Map<String, String>) response.getEntity();

        assertThat(errors.get(DateCountInPeriodResource.PERIOD), is("rest.invalid.field.valid.fields"));
    }

    @Test
    public void testMissingOperationFieldReturns400() throws IOException, SearchException {
        Response response = dateCountInPeriodResource.completeCreatedVsResolved(MOCK_JQL, MOCK_FIELDS, MOCK_PERIOD_NAME, MOCK_DAYS_PREVIOUS, false, null);

        assertEquals("Should have returned a failed response", response.getStatus(), HttpStatus.SC_BAD_REQUEST);

        @SuppressWarnings("unchecked")
        Map<String, String> errors = (Map<String, String>) response.getEntity();

        assertThat(errors.get(DateCountInPeriodResource.OPERATION), is("rest.missing.field"));
    }

    @Test
    public void testOnlyValidOperationsWork() throws IOException, SearchException {
        for (final Operation operation : Operation.values()) {
            final String operationName = operation.toString();
            successfulServiceCall();
            Response response = dateCountInPeriodResource.completeCreatedVsResolved(MOCK_JQL, MOCK_FIELDS, MOCK_PERIOD_NAME, MOCK_DAYS_PREVIOUS, false, operationName);

            assertEquals("Should have returned a successful response for operation: " + operation, response.getStatus(), HttpStatus.SC_OK);
        }

        Response response = dateCountInPeriodResource.completeCreatedVsResolved(MOCK_JQL, MOCK_FIELDS, MOCK_PERIOD_NAME, MOCK_DAYS_PREVIOUS, false, "wrongoperationname");

        assertEquals("Should have failed on invalid period name", response.getStatus(), HttpStatus.SC_BAD_REQUEST);

        @SuppressWarnings("unchecked")
        Map<String, String> errors = (Map<String, String>) response.getEntity();

        assertThat(errors.get(DateCountInPeriodResource.OPERATION), is("rest.invalid.field.valid.fields"));
    }

    @Test
    public void testServiceErrorReturns500() throws IOException, SearchException {
        final String ERROR_MESSAGE = "error";
        failedServiceCall(ERROR_MESSAGE);

        Response response = dateCountInPeriodResource.completeCreatedVsResolved(MOCK_JQL, MOCK_FIELDS, MOCK_PERIOD_NAME, MOCK_DAYS_PREVIOUS, false, MOCK_OPERATION_NAME);

        assertEquals("Should have returned a failed response", response.getStatus(), HttpStatus.SC_INTERNAL_SERVER_ERROR);

        @SuppressWarnings("unchecked")
        Collection<String> errors = (Collection<String>) response.getEntity();

        assertThat(errors.contains(ERROR_MESSAGE), is(true));
    }

    @Test
    public void testServiceOkayReturns200() throws IOException, SearchException {
        successfulServiceCall();
        Response response = dateCountInPeriodResource.completeCreatedVsResolved(MOCK_JQL, MOCK_FIELDS, MOCK_PERIOD_NAME, MOCK_DAYS_PREVIOUS, false, MOCK_OPERATION_NAME);

        assertEquals("Should have returned a failed response", response.getStatus(), HttpStatus.SC_OK);
    }

    public void failedServiceCall(final String errorMessage) throws IOException, SearchException {
        ErrorCollection serviceErrorResponse = new SimpleErrorCollection();
        serviceErrorResponse.addErrorMessage(errorMessage);
        Either<DateCountInPeriodResultBean, ErrorCollection> serviceResponse = Either.right(serviceErrorResponse);

        when(dateCountInPeriodService.collectData(any(DateCountInPeriodService.RequestParameters.class))).thenReturn(serviceResponse);
    }

    public void successfulServiceCall() throws IOException, SearchException {
        Either<DateCountInPeriodResultBean, ErrorCollection> serviceResponse = Either.left(serviceCorrectResponse);

        when(dateCountInPeriodService.collectData(any(DateCountInPeriodService.RequestParameters.class))).thenReturn(serviceResponse);
    }

}
