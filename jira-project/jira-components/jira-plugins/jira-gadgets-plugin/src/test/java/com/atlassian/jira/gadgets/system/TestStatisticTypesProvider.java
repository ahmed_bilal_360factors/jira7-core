package com.atlassian.jira.gadgets.system;


import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.customfields.CustomFieldSearcher;
import com.atlassian.jira.issue.customfields.searchers.CustomFieldLabelsSearcher;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.statistics.StatisticsMapper;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class TestStatisticTypesProvider {

    @Rule
    public MethodRule mockitoRule = MockitoJUnit.rule();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Mock
    JiraAuthenticationContext authenticationContext;
    @Mock
    CustomFieldManager customFieldManager;
    @Mock
    SystemStatisticTypesFactory systemStatisticTypesFactory;


    StatisticTypesProvider statisticTypesProvider;

    @Before
    public void setUp() {
        statisticTypesProvider = new StatisticTypesProvider(
                authenticationContext,
                customFieldManager,
                systemStatisticTypesFactory
        );
    }

    @Test
    public void shouldGetMapperForSystemField() {
        final String field = "system.field";
        final StatisticsMapper expectedMapper = mock(StatisticsMapper.class);

        when(systemStatisticTypesFactory.getSystemMapper(eq(field))).thenReturn(Optional.of(expectedMapper));

        final StatisticsMapper actualMapper = statisticTypesProvider.getStatsMapper(field);

        assertThat(actualMapper, equalTo(expectedMapper));
        verifyNoMoreInteractions(customFieldManager);
    }

    @Test
    public void shouldGetMapperForCustomField() {

        final String field = "custom.field";
        final CustomField customField = mock(CustomField.class);
        final CustomFieldLabelsSearcher customFieldSearcher = mock(CustomFieldLabelsSearcher.class);
        final StatisticsMapper expectedMapper = mock(StatisticsMapper.class);

        when(customField.getCustomFieldSearcher()).thenReturn(customFieldSearcher);
        when(customFieldSearcher.getStatisticsMapper(eq(customField))).thenReturn(expectedMapper);

        when(systemStatisticTypesFactory.getSystemMapper(eq(field))).thenReturn(Optional.empty());
        when(customFieldManager.getCustomFieldObject(eq(field))).thenReturn(customField);

        final StatisticsMapper actualMapper = statisticTypesProvider.getStatsMapper(field);

        assertThat(actualMapper, equalTo(expectedMapper));
    }

    @Test
    public void shouldNotGetMapperForNonStattableField() {

        final String field = "non.stattable.custom.field";
        final CustomField customField = mock(CustomField.class);
        final CustomFieldSearcher customFieldSearcher = mock(CustomFieldSearcher.class);

        when(customField.getCustomFieldSearcher()).thenReturn(customFieldSearcher);

        when(systemStatisticTypesFactory.getSystemMapper(eq(field))).thenReturn(Optional.empty());
        when(customFieldManager.getCustomFieldObject(eq(field))).thenReturn(customField);

        final StatisticsMapper actualMapper = statisticTypesProvider.getStatsMapper(field);

        assertThat(actualMapper, equalTo(null));
    }

    @Test
    public void shouldFailToGetMapperForAbsentField() {

        thrown.expect(RuntimeException.class);

        final String field = "non.existing.field";

        when(systemStatisticTypesFactory.getSystemMapper(eq(field))).thenReturn(Optional.empty());
        when(customFieldManager.getCustomFieldObject(eq(field))).thenReturn(null);

        statisticTypesProvider.getStatsMapper(field);
    }

    @Test
    public void shouldTranslateSystemField() {

        final String field = "assignees";
        final String statisticsType = "gadget.filterstats.field.statistictype.assignees";
        final String expectedDisplayName = "assignees.translation";
        final I18nHelper i18n = mock(I18nHelper.class);

        when(authenticationContext.getI18nHelper()).thenReturn(i18n);
        when(i18n.getText(eq(statisticsType))).thenReturn(expectedDisplayName);

        final String actualDisplayName = statisticTypesProvider.getDisplayName(field);

        assertThat(actualDisplayName, equalTo(expectedDisplayName));
    }

    @Test
    public void shouldTranslateCustomField() {

        final String field = "custom.field";
        final CustomField customField = mock(CustomField.class);
        final String expectedDisplayName = "custom.field.translation";
        final I18nHelper i18n = mock(I18nHelper.class);

        when(customFieldManager.getCustomFieldObject(eq(field))).thenReturn(customField);
        when(customField.getName()).thenReturn(expectedDisplayName);
        when(authenticationContext.getI18nHelper()).thenReturn(i18n);

        final String actualDisplayName = statisticTypesProvider.getDisplayName(field);

        assertThat(actualDisplayName, equalTo(expectedDisplayName));
        verifyNoMoreInteractions(i18n);
    }

    @Test
    public void shouldNotTranslateAbsentField() {

        final String field = "custom.field";
        final String expectedDisplayName = "";
        final I18nHelper i18n = mock(I18nHelper.class);

        when(customFieldManager.getCustomFieldObject(eq(field))).thenReturn(null);
        when(authenticationContext.getI18nHelper()).thenReturn(i18n);

        final String actualDisplayName = statisticTypesProvider.getDisplayName(field);

        assertThat(actualDisplayName, equalTo(expectedDisplayName));
        verifyNoMoreInteractions(i18n);
    }
}