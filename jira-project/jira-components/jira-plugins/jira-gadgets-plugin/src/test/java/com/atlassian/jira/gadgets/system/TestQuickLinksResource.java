package com.atlassian.jira.gadgets.system;

import com.atlassian.jira.dashboarditem.quicklinks.Link;
import com.atlassian.jira.dashboarditem.quicklinks.LinkLists;
import com.atlassian.jira.dashboarditem.quicklinks.QuickLinksProvider;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.timezone.TimeZoneManager;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Tests QuickLinksResource
 *
 * @since v4.0
 */
public class TestQuickLinksResource {
    @Rule
    public MockitoContainer container = MockitoMocksInContainer.rule(this);

    @Mock
    private QuickLinksProvider quicklinksProvider;

    @Mock
    @AvailableInContainer
    private TimeZoneManager timeZoneManager;

    private QuickLinksResource resource;

    @Before
    public void setUp() throws Exception {
        resource = new QuickLinksResource(quicklinksProvider);
    }

    @Test
    public void testHasResponse() throws Exception {
        Link createIssue = new Link("gadget.quicklinks.create.issue", "/secure/CreateIssue!default.jspa", "gadget.quicklinks.create.issue.title");
        Link viewProjects = new Link("gadget.quicklinks.adminstration", "/secure/project/ViewProjects.jspa", "gadget.quicklinks.administration.title");
        Link browseProjects = new Link("gadget.quicklinks.browse.projects", "/secure/BrowseProjects.jspa", "gadget.quicklinks.browse.projects.title");
        Link findIssues = new Link("gadget.quicklinks.find.issues", "/secure/IssueNavigator.jspa?mode=show", "gadget.quicklinks.find.issues.title");

        Collection<Link> navigationLinks = linkCollectionFromLinks(browseProjects, findIssues);
        Collection<Link> commonLinks = linkCollectionFromLinks(viewProjects, createIssue);
        LinkLists lists = new LinkLists(navigationLinks, commonLinks);

        when(quicklinksProvider.getLinks()).thenReturn(lists);

        Response response = resource.getQuickLinks();
        assertTrue(response.getStatus() == 200);
        assertTrue(response.getEntity().getClass().equals(LinkLists.class));

        LinkLists responseLinks = (LinkLists) response.getEntity();
        assertEquals(responseLinks.getCommonLinks(), lists.getCommonLinks());
        assertEquals(responseLinks.getNavigationLinks(), lists.getNavigationLinks());
    }

    @Test
    public void testAnonymous() throws Exception {
        Collection<Link> navigationLinks = new ArrayList<>();
        Collection<Link> commonLinks = new ArrayList<>();
        LinkLists lists = new LinkLists(navigationLinks, commonLinks);

        when(quicklinksProvider.getLinks()).thenReturn(lists);

        Response response = resource.getQuickLinks();
        QuickLinksResource.Warning responseWarning = (QuickLinksResource.Warning) response.getEntity();

        assertTrue(response.getStatus() == 200);
        assertTrue(responseWarning.isNoDataNoUser());
    }

    private Collection<Link> linkCollectionFromLinks(Link... links) {
        Collection<Link> newList = new ArrayList<>();
        Collections.addAll(newList, links);
        return newList;
    }
}
