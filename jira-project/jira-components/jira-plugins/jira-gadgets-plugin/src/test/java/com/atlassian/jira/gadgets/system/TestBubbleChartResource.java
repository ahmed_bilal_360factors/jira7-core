package com.atlassian.jira.gadgets.system;

import com.atlassian.jira.bc.issue.comment.CommentService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.charts.util.ChartUtils;
import com.atlassian.jira.gadgets.system.bubblechart.BubbleChartResource;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.jql.builder.JqlClauseBuilderFactory;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.Query;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestBubbleChartResource {
    @Rule
    public MockitoContainer container = MockitoMocksInContainer.rule(this);

    private static final String PROJECT_QUERY_STRING = "project-12345";
    private static final String FILTER_QUERY_STRING = "filter-10000";
    private static final String VOTES_BUBBLE_TYPE = "votes";
    private static final String PROJECT_NAME = "someProject";
    private static final String PROJECT_KEY = "SPK";
    private static final Long SEARCH_ID = 10101L;

    @Mock
    private ChartUtils chartUtils;
    @Mock
    private SearchService searchService;
    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;
    @Mock
    private CommentService commentService;
    @Mock
    private SearchResults searchResults;
    @Mock
    private I18nHelper i18nHelper;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private VelocityRequestContextFactory velocityRequestContextFactory;
    @Mock
    @AvailableInContainer
    private JqlClauseBuilderFactory jqlClauseBuilderFactory;
    @Mock
    @AvailableInContainer
    private TimeZoneManager timeZoneManager;

    private BubbleChartResource resource;

    private MockIssue issue1;
    private MockIssue issue2;
    private List<Issue> issues;

    @Before
    public void setUp() {
        when(jiraAuthenticationContext.getI18nHelper()).thenReturn(i18nHelper);

        resource = new BubbleChartResource(
                chartUtils,
                jiraAuthenticationContext,
                searchService,
                permissionManager,
                velocityRequestContextFactory,
                commentService);

        issue1 = new MockIssue();
        issue1.setVotes(34L);
        issue2 = new MockIssue();
        issue2.setVotes(11L);
        issues = Lists.newArrayList(issue1, issue2);
    }

    @Test
    public void resourceShouldReturnAnErrorWhenGivenNonExistentProjectOrFilterId() {
        final Response chartResponse = resource.generateChart(PROJECT_QUERY_STRING, VOTES_BUBBLE_TYPE, 1);

        assertThat(chartResponse.getStatus(), is(Response.Status.BAD_REQUEST.getStatusCode()));
    }

    @Test
    public void resourceShouldReturnTheCorrectResponseForProjectAndVoteSearch() throws Exception {
        mockSearch(PROJECT_QUERY_STRING, 76, "some project");

        final BubbleChartResource.ResponseContainer response = (BubbleChartResource.ResponseContainer) resource.generateChart(PROJECT_QUERY_STRING, "votes", 1).getEntity();

        assertResponseIsCorrect(response, 76, "some project");
    }

    @Test
    public void resourceShouldReturnTheCorrectResponseForFilterAndParticipantsSearch() throws Exception {
        mockSearch(FILTER_QUERY_STRING, 23, "somefilter");

        final BubbleChartResource.ResponseContainer response = (BubbleChartResource.ResponseContainer) resource.generateChart(FILTER_QUERY_STRING, "participants", 1).getEntity();

        assertResponseIsCorrect(response, 23, "somefilter");
    }

    private void mockSearch(String queryString, Integer totalIssues, String filterTitle) throws Exception {
        searchServiceReturnsMockedSearchResults();
        mockChartUtilsRetrieveOrMakeSearchRequest(queryString, true);
        userHasCorrectPermissions();
        when(searchResults.getTotal()).thenReturn(totalIssues);
        when(searchResults.getIssues()).thenReturn(issues);
        when(i18nHelper.getText(anyString())).thenReturn(filterTitle);
    }

    private void assertResponseIsCorrect(BubbleChartResource.ResponseContainer response, Integer totalIssues, String filterTitle)
            throws Exception {
        assertThat(response.getFilterTitle(), is(filterTitle));
        assertThat(response.getIssueCount(), is(totalIssues));
        assertThat(response.getData().size(), is(issues.size()));
    }

    private void userHasCorrectPermissions() {
        when(permissionManager.hasPermission(any(ProjectPermissionKey.class), any(Project.class), any(ApplicationUser.class))).thenReturn(true);
    }

    private void searchServiceReturnsMockedSearchResults() throws Exception {
        when(searchService.search(eq(jiraAuthenticationContext.getLoggedInUser()), any(Query.class), any(PagerFilter.class))).thenReturn(searchResults);
    }

    private void mockChartUtilsRetrieveOrMakeSearchRequest(String searchQueryString, boolean requestIsLoaded) {
        when(chartUtils.retrieveOrMakeSearchRequest(eq(searchQueryString), any(Map.class))).thenAnswer(invocationOnMock -> {
            Map<String, Object> map = (Map<String, Object>) invocationOnMock.getArguments()[1];
            String projectOrFilterId = ((String) map.get("projectOrFilterId")).replace("projectOrFilterId=", "");

            // Replicate side-effects of `retrieveOrMakeSearchRequest`
            if (projectOrFilterId.startsWith("project-")) {
                final MockProject project = new MockProject();
                project.setName(PROJECT_NAME);
                project.setKey(PROJECT_KEY);
                map.put("project", project);
            } else if (projectOrFilterId.startsWith("filter-")) {
                SearchRequest searchRequest = mock(SearchRequest.class);
                map.put("searchRequest", searchRequest);
                when(searchRequest.getId()).thenReturn(SEARCH_ID);
                when(searchRequest.isLoaded()).thenReturn(requestIsLoaded);
                when(searchRequest.getName()).thenReturn(PROJECT_NAME);
            }

            return new SearchRequest();
        });
    }
}
