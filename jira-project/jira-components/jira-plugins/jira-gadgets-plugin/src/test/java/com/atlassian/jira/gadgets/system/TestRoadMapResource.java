package com.atlassian.jira.gadgets.system;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.datetime.DateTimeStyle;
import com.atlassian.jira.event.mau.MauEventService;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.project.MockVersion;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectCategory;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.rest.api.messages.TextMessage;
import com.atlassian.jira.rest.v1.model.errors.ErrorCollection;
import com.atlassian.jira.rest.v1.model.errors.ValidationError;
import com.atlassian.jira.rest.v1.util.CacheControl;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.Function;
import com.atlassian.jira.util.Predicate;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.jira.util.collect.CollectionUtil;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.query.Query;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import javax.ws.rs.core.Response;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.gadgets.system.RoadMapResource.PROJECT_OR_CATEGORY_IDS;
import static com.atlassian.jira.gadgets.system.RoadMapResource.DAYS;
import static com.atlassian.jira.gadgets.system.RoadMapResource.NUM;
import static com.atlassian.jira.rest.v1.util.CacheControl.NO_CACHE;
import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.mock;

/**
 * Tests RoadMapResource
 *
 * @since v4.0
 */
public class TestRoadMapResource {
    private static final DateFormat df = new SimpleDateFormat("dd/MMM/yy");
    private static final DateFormat dfIso8601 = new SimpleDateFormat("yyyy-MM-dd'T'HH-mm");
    private static final ToStringStyle TO_STRING_STYLE = ToStringStyle.SHORT_PREFIX_STYLE;

    @Rule
    public final MockitoContainer mockitoMocks = MockitoMocksInContainer.rule(this);

    // mock objects injected via constructor
    @Mock
    private JiraAuthenticationContext mockAuthenticationContext;
    @Mock
    private PermissionManager mockPermissionManager;
    @Mock
    private ProjectManager mockProjectManager;
    @Mock
    private VersionManager mockVersionManager;
    @Mock
    private SearchService mockSearchService;
    @Mock
    private MauEventService mauEventService;
    @Mock
    private ApplicationProperties mockApplicationProperties;
    @Mock
    private DateTimeFormatter dateTimeFormatter;
    @Mock
    private DateTimeFormatter humanDateTimeFormatter;
    @Mock
    private DateTimeFormatter isoDateTimeFormatter;

    @Mock
    private ApplicationUser mockUser;

    @Before
    public void setUp() {
        when(mockAuthenticationContext.getLoggedInUser()).thenReturn(mockUser);
        when(dateTimeFormatter.withStyle(DateTimeStyle.DATE)).thenReturn(humanDateTimeFormatter);
        when(dateTimeFormatter.withStyle(DateTimeStyle.ISO_8601_DATE))
            .thenReturn(isoDateTimeFormatter);

        when(humanDateTimeFormatter.format(Mockito.any(Date.class)))
            .thenAnswer(invocation -> df.format(invocation.getArguments()[0]));

        when(isoDateTimeFormatter.format(Mockito.any(Date.class)))
            .thenAnswer(invocation -> dfIso8601.format(invocation.getArguments()[0]));
    }

    private RoadMapResource createInstance() {
        return new RoadMapResource(mockAuthenticationContext, mockPermissionManager,
            mockProjectManager, mockVersionManager, mockSearchService, mauEventService, null, dateTimeFormatter);
    }

    @Test
    public final void testValidate_all_projects_and_defaults() {
        RoadMapResource instance = createInstance();

        Response actualRes = instance.validate("123|allprojects", "30", "10");

        assertThat(200, equalTo(actualRes.getStatus()));
    }

    @Test
    public final void testValidate_single_project() {
        RoadMapResource instance = createInstance();

        stubProject(123L);

        Response actualRes = instance.validate("123", "30", "10");

        assertEquals(
            Response.ok(new TextMessage("No input validation errors found.")).cacheControl(NO_CACHE)
                    .build(), actualRes);
    }

    @Test
    public final void testValidate_multiple_projects() {
        RoadMapResource instance = createInstance();

        stubProjects(123L, 456L);

        Response actualRes = instance.validate("123|456", "30", "10");

        assertEquals(
            Response.ok(new TextMessage("No input validation errors found.")).cacheControl(NO_CACHE)
                    .build(), actualRes);
    }

    @Test
    public final void testValidate_nonexistent_project() {
        RoadMapResource instance = createInstance();

        stubProject(123L);

        Response actualRes = instance.validate("123|456", "30", "10");

        Response expectedRes = Response.status(400).entity(ErrorCollection.Builder.newBuilder(
            new ValidationError(PROJECT_OR_CATEGORY_IDS, "gadget.common.invalid.project")
        ).build()).cacheControl(CacheControl.NO_CACHE).build();

        assertEquals(expectedRes, actualRes);
    }

    @Test
    public final void testValidate_single_category() {
        RoadMapResource instance = createInstance();

        stubCategory(123L);

        Response actualRes = instance.validate("cat123", "30", "10");

        assertEquals(
            Response.ok(new TextMessage("No input validation errors found.")).cacheControl(NO_CACHE)
                    .build(), actualRes);
    }

    @Test
    public final void testValidate_multiple_categories() {
        RoadMapResource instance = createInstance();

        stubCategories(123L, 456L);

        Response actualRes = instance.validate("cat123|cat456", "30", "10");

        assertEquals(
            Response.ok(new TextMessage("No input validation errors found.")).cacheControl(NO_CACHE)
                    .build(), actualRes);
    }

    @Test
    public final void testValidate_nonexistent_category() {
        RoadMapResource instance = createInstance();

        stubCategory(123L);

        Response actualRes = instance.validate("cat123|cat456", "30", "10");

        Response expectedRes = Response.status(400).entity(ErrorCollection.Builder.newBuilder(
            new ValidationError(PROJECT_OR_CATEGORY_IDS, "gadget.common.invalid.projectCategory")
        ).build()).cacheControl(CacheControl.NO_CACHE).build();

        assertEquals(expectedRes, actualRes);
    }

    @Test
    public final void testValidate_both_project_category_selected() {
        RoadMapResource instance = createInstance();

        stubProject(123L);
        stubCategory(456L);

        Response actualRes = instance.validate("123|cat456", "30", "10");

        assertEquals(
            Response.ok(new TextMessage("No input validation errors found.")).cacheControl(NO_CACHE)
                    .build(), actualRes);
    }

    @Test
    public final void testValidate_none_selected() {
        RoadMapResource instance = createInstance();

        Response actualRes = instance.validate(null, "30", "10");

        Response expectedRes = Response.status(400).entity(ErrorCollection.Builder.newBuilder(
            new ValidationError(PROJECT_OR_CATEGORY_IDS,
                "gadget.common.projects.and.categories.none.selected")
        ).build()).cacheControl(CacheControl.NO_CACHE).build();

        assertEquals(expectedRes, actualRes);
    }

    @Test
    public final void testValidate_days_zero() {
        RoadMapResource instance = createInstance();

        Response actualRes = instance.validate("allprojects", "0", "10");

        assertThat(200, equalTo(actualRes.getStatus()));
    }

    @Test
    public final void testValidate_days_not_positive_nor_zero() {
        RoadMapResource instance = createInstance();

        Response actualRes = instance.validate("allprojects", "-1", "10");

        Response expectedRes = Response.status(400).entity(ErrorCollection.Builder.newBuilder(
            new ValidationError(DAYS, "gadget.common.negative.days")
        ).build()).cacheControl(CacheControl.NO_CACHE).build();

        assertEquals(expectedRes, actualRes);
    }

    @Test
    public final void testValidate_days_upper_limit() {
        RoadMapResource instance = createInstance();

        Response actualResponse = instance.validate("allprojects", "1000", "10");

        assertEquals(
            Response.ok(new TextMessage("No input validation errors found.")).cacheControl(NO_CACHE)
                    .build(), actualResponse);

        actualResponse = instance.validate("allprojects", "1001", "10");

        Response expectedRes = Response.status(400).entity(ErrorCollection.Builder.newBuilder(
            new ValidationError(DAYS, "gadget.common.days.overlimit", "1000")
        ).build()).cacheControl(CacheControl.NO_CACHE).build();

        assertEquals(expectedRes, actualResponse);
    }

    @Test
    public final void testValidate_days_not_numeric() {
        RoadMapResource instance = createInstance();

        Response actualRes = instance.validate("allprojects", "abc", "10");

        Response expectedRes = Response.status(400).entity(ErrorCollection.Builder.newBuilder(
            new ValidationError(DAYS, "gadget.common.days.nan")
        ).build()).cacheControl(CacheControl.NO_CACHE).build();

        assertEquals(expectedRes, actualRes);
    }

    @Test
    public final void testValidate_num_not_positive() {
        RoadMapResource instance = createInstance();

        Response actualRes = instance.validate("allprojects", "30", "0");

        Response expectedRes = Response.status(400).entity(ErrorCollection.Builder.newBuilder(
            new ValidationError(NUM, "gadget.common.num.negative")
        ).build()).cacheControl(CacheControl.NO_CACHE).build();

        assertEquals(expectedRes, actualRes);
    }

    @Test
    public final void testValidate_num_upper_limit() {
        RoadMapResource instance = createInstance();

        Response actualResponse = instance.validate("allprojects", "30", "50");

        assertEquals(
            Response.ok(new TextMessage("No input validation errors found.")).cacheControl(NO_CACHE)
                    .build(), actualResponse);

        actualResponse = instance.validate("allprojects", "30", "51");

        Response expectedRes = Response.status(400).entity(ErrorCollection.Builder.newBuilder(
            new ValidationError(NUM, "gadget.common.num.overlimit", "50")
        ).build()).cacheControl(CacheControl.NO_CACHE).build();

        assertEquals(expectedRes, actualResponse);
    }

    @Test
    public final void testValidate_num_not_numeric() {
        RoadMapResource instance = createInstance();

        Response actualRes = instance.validate("allprojects", "30", "abc");

        Response expectedRes = Response.status(400).entity(ErrorCollection.Builder.newBuilder(
            new ValidationError(NUM, "gadget.common.num.nan")
        ).build()).cacheControl(CacheControl.NO_CACHE).build();

        assertEquals(expectedRes, actualRes);
    }

    @Test
    public final void testGenerate_all_projects_and_day_limit() throws SearchException {
        Project p100 = stubProject(100L, "HOMOSAP", "homosapien");
        Project p101 = stubProject(101L, "MKY", "monkey");
        Collection<Project> mockAllBrowsableProjects =
            CollectionBuilder.<Project>newBuilder(p100, p101).asList();
        when(mockPermissionManager.getProjects(ProjectPermissions.BROWSE_PROJECTS, mockUser)).
                                                                                 thenReturn(
                                                                                     mockAllBrowsableProjects);

        Version p100v1 = stubVersion(1L, p100, "v1", "version 1", -3);
        Version p100v2 = stubVersion(2L, p100, "v2", "version 2", 3);
        Version p100v3 = stubVersion(3L, p100, "v3", "version 3", 31);
        Version p101v1 = stubVersion(4L, p101, "v1.0", "version 1.0", -2);
        Version p101v2 = stubVersion(5L, p101, "v2.0", "version 2.0", 4);
        Version p101v3 = stubVersion(6L, p101, "v3.0", "version 3.0", 6);
        Version p101v4 = stubVersion(7L, p101, "v4.0", "version 4.0", null);
        when(mockVersionManager.getVersionsUnreleased(100L, false))
            .thenReturn(asList(p100v1, p100v2, p100v3));
        when(mockVersionManager.getVersionsUnreleased(101L, false))
            .thenReturn(asList(p101v1, p101v2, p101v3, p101v4));

        Map<Long, SearchExpectation> searchExpects =
            MapBuilder.<Long, SearchExpectation>newBuilder().
                                                                add(1L,
                                                                    new SearchExpectation(1L, 7L,
                                                                        0L)).
                                                                add(2L,
                                                                    new SearchExpectation(2L, 3L,
                                                                        2L)).
                                                                add(4L,
                                                                    new SearchExpectation(4L, 3L,
                                                                        1L)).
                                                                add(5L,
                                                                    new SearchExpectation(5L, 7L,
                                                                        7L)).
                                                                add(6L,
                                                                    new SearchExpectation(6L, 0L,
                                                                        0L))
                                                            .toHashMap();

        RoadMapResource instance = new PartiallyStubbedOutRoadMapResource(searchExpects);

        Response actualRes = instance.generate("123|allprojects", 30, 10);

        RoadMapResource.ProjectData pd100 = projectData(100L, "HOMOSAP", "homosapien");
        RoadMapResource.ProjectData pd101 = projectData(101L, "MKY", "monkey");
        Response expectedRes = Response.ok(new RoadMapResource.RoadMapData(
            CollectionBuilder.newBuilder(
                versionData(1L, "v1", "version 1", pd100, -3, 7, resolvedData(7, 100),
                    unResolvedData(0, 0)),
                versionData(4L, "v1.0", "version 1.0", pd101, -2, 3, resolvedData(2, 66),
                    unResolvedData(1, 34)),
                versionData(2L, "v2", "version 2", pd100, 3, 3, resolvedData(1, 34),
                    unResolvedData(2, 66)),
                versionData(5L, "v2.0", "version 2.0", pd101, 4, 7, resolvedData(0, 0),
                    unResolvedData(7, 100)),
                versionData(6L, "v3.0", "version 3.0", pd101, 6, 0, resolvedData(0, 100),
                    unResolvedData(0, 0))
            ).asList(),
            30, getFormattedDate(30)
        )).build();

        assertEquals(expectedRes, actualRes);

        verify(mockAuthenticationContext, atLeastOnce()).getLoggedInUser();
    }

    @Test
    public final void testGenerate_result_limit() throws SearchException {
        Project p100 = stubProject(100L, "HOMOSAP", "homosapien");
        Project p101 = stubProject(101L, "MKY", "monkey");
        Collection<Project> mockAllBrowsableProjects =
            CollectionBuilder.<Project>newBuilder(p100, p101).asList();
        when(mockPermissionManager.getProjects(ProjectPermissions.BROWSE_PROJECTS, mockUser)).
                                                                                 thenReturn(
                                                                                     mockAllBrowsableProjects);

        Version p100v1 = stubVersion(1L, p100, "v1", "version 1", -3);
        Version p100v2 = stubVersion(2L, p100, "v2", "version 2", 3);
        Version p100v3 = stubVersion(3L, p100, "v3", "version 3", 10);
        Version p101v1 = stubVersion(4L, p101, "v1.0", "version 1.0", -2);
        Version p101v2 = stubVersion(5L, p101, "v2.0", "version 2.0", 4);
        Version p101v3 = stubVersion(6L, p101, "v3.0", "version 3.0", 20);
        when(mockVersionManager.getVersionsUnreleased(100L, false))
            .thenReturn(asList(p100v1, p100v2, p100v3));
        when(mockVersionManager.getVersionsUnreleased(101L, false))
            .thenReturn(asList(p101v1, p101v2, p101v3));

        Map<Long, SearchExpectation> searchExpects =
            MapBuilder.<Long, SearchExpectation>newBuilder().
                                                                add(1L,
                                                                    new SearchExpectation(1L, 7L,
                                                                        0L)).
                                                                add(2L,
                                                                    new SearchExpectation(2L, 3L,
                                                                        2L)).
                                                                add(4L,
                                                                    new SearchExpectation(4L, 3L,
                                                                        1L)).
                                                                add(5L,
                                                                    new SearchExpectation(5L, 7L,
                                                                        7L))
                                                            .toHashMap();

        RoadMapResource instance = new PartiallyStubbedOutRoadMapResource(searchExpects);

        Response actualRes = instance.generate("123|allprojects", 30, 4);

        RoadMapResource.ProjectData pd100 = projectData(100L, "HOMOSAP", "homosapien");
        RoadMapResource.ProjectData pd101 = projectData(101L, "MKY", "monkey");
        Response expectedRes = Response.ok(new RoadMapResource.RoadMapData(
            CollectionBuilder.newBuilder(
                versionData(1L, "v1", "version 1", pd100, -3, 7, resolvedData(7, 100),
                    unResolvedData(0, 0)),
                versionData(4L, "v1.0", "version 1.0", pd101, -2, 3, resolvedData(2, 66),
                    unResolvedData(1, 34)),
                versionData(2L, "v2", "version 2", pd100, 3, 3, resolvedData(1, 34),
                    unResolvedData(2, 66)),
                versionData(5L, "v2.0", "version 2.0", pd101, 4, 7, resolvedData(0, 0),
                    unResolvedData(7, 100))
            ).asList(),
            30, getFormattedDate(30)
        )).build();

        assertEquals(expectedRes, actualRes);

        verify(mockAuthenticationContext, atLeastOnce()).getLoggedInUser();
    }

    @Test
    public final void testGenerate_single_browsable_project() throws SearchException {
        Project p100 = stubProject(100L, "HOMOSAP", "homosapien");
        when(mockProjectManager.getProjectObj(100L)).thenReturn(p100);
        when(mockPermissionManager.hasPermission(ProjectPermissions.BROWSE_PROJECTS, p100, mockUser)).
                                                                                         thenReturn(
                                                                                             true);

        Version p100v1 = stubVersion(1L, p100, "v1", "version 1", -3);
        Version p100v2 = stubVersion(2L, p100, "v2", "version 2", 3);
        Version p100v3 = stubVersion(3L, p100, "v3", "version 3", 31);
        when(mockVersionManager.getVersionsUnreleased(100L, false))
            .thenReturn(asList(p100v1, p100v2, p100v3));

        Map<Long, SearchExpectation> searchExpects =
            MapBuilder.<Long, SearchExpectation>newBuilder().
                                                                add(1L,
                                                                    new SearchExpectation(1L, 7L,
                                                                        0L)).
                                                                add(2L,
                                                                    new SearchExpectation(2L, 3L,
                                                                        2L))
                                                            .toHashMap();

        RoadMapResource instance = new PartiallyStubbedOutRoadMapResource(searchExpects);

        Response actualRes = instance.generate("100", 30, 10);

        RoadMapResource.ProjectData pd100 = projectData(100L, "HOMOSAP", "homosapien");
        Response expectedRes = Response.ok(new RoadMapResource.RoadMapData(
            CollectionBuilder.newBuilder(
                versionData(1L, "v1", "version 1", pd100, -3, 7, resolvedData(7, 100),
                    unResolvedData(0, 0)),
                versionData(2L, "v2", "version 2", pd100, 3, 3, resolvedData(1, 34),
                    unResolvedData(2, 66))
            ).asList(),
            30, getFormattedDate(30)
        )).build();

        assertEquals(expectedRes, actualRes);

        verify(mockAuthenticationContext, atLeastOnce()).getLoggedInUser();
    }

    @Test
    public final void testGenerate_single_not_browsable_project() throws SearchException {
        RoadMapResource instance = createInstance();

        Project p100 = stubProject(100L, "HOMOSAP", "homosapien");
        when(mockProjectManager.getProjectObj(100L)).thenReturn(p100);
        when(mockPermissionManager.hasPermission(ProjectPermissions.BROWSE_PROJECTS, p100, mockUser)).
                                                                                         thenReturn(
                                                                                             false);

        Response actualRes = instance.generate("100", 30, 10);

        Response expectedRes = Response.ok(new RoadMapResource.RoadMapData(
            new ArrayList<RoadMapResource.VersionData>(), 30, getFormattedDate(30)
        )).build();

        assertEquals(expectedRes, actualRes);

        verify(mockAuthenticationContext, atLeastOnce()).getLoggedInUser();
    }

    @Test
    public final void testGenerate_multiple_projects() throws SearchException {
        Project p100 = stubProject(100L, "HOMOSAP", "homosapien");
        Project p101 = stubProject(101L, "MKY", "monkey");
        Project p102 = stubProject(102L, "CHIMP", "chimpanzee");
        when(mockProjectManager.getProjectObj(100L)).thenReturn(p100);
        when(mockPermissionManager.hasPermission(ProjectPermissions.BROWSE_PROJECTS, p100, mockUser)).
                                                                                         thenReturn(
                                                                                             true);
        when(mockProjectManager.getProjectObj(101L)).thenReturn(p101);
        when(mockPermissionManager.hasPermission(ProjectPermissions.BROWSE_PROJECTS, p101, mockUser)).
                                                                                         thenReturn(
                                                                                             true);
        when(mockProjectManager.getProjectObj(102L)).thenReturn(p102);
        when(mockPermissionManager.hasPermission(ProjectPermissions.BROWSE_PROJECTS, p102, mockUser)).
                                                                                         thenReturn(
                                                                                             false);

        Version p100v1 = stubVersion(1L, p100, "v1", "version 1", -3);
        Version p100v2 = stubVersion(2L, p100, "v2", "version 2", 3);
        Version p100v3 = stubVersion(3L, p100, "v3", "version 3", 31);
        Version p101v1 = stubVersion(4L, p101, "v1.0", "version 1.0", -2);
        Version p101v2 = stubVersion(5L, p101, "v2.0", "version 2.0", 4);
        Version p101v3 = stubVersion(6L, p101, "v3.0", "version 3.0", null);
        when(mockVersionManager.getVersionsUnreleased(100L, false))
            .thenReturn(asList(p100v1, p100v2, p100v3));
        when(mockVersionManager.getVersionsUnreleased(101L, false))
            .thenReturn(asList(p101v1, p101v2, p101v3));

        Map<Long, SearchExpectation> searchExpects =
            MapBuilder.<Long, SearchExpectation>newBuilder().
                                                                add(1L,
                                                                    new SearchExpectation(1L, 7L,
                                                                        0L)).
                                                                add(2L,
                                                                    new SearchExpectation(2L, 3L,
                                                                        2L)).
                                                                add(4L,
                                                                    new SearchExpectation(4L, 3L,
                                                                        1L)).
                                                                add(5L,
                                                                    new SearchExpectation(5L, 7L,
                                                                        7L))
                                                            .toHashMap();

        RoadMapResource instance = new PartiallyStubbedOutRoadMapResource(searchExpects);

        Response actualRes = instance.generate("100|101|102", 30, 10);

        RoadMapResource.ProjectData pd100 = projectData(100L, "HOMOSAP", "homosapien");
        RoadMapResource.ProjectData pd101 = projectData(101L, "MKY", "monkey");
        Response expectedRes = Response.ok(new RoadMapResource.RoadMapData(
            CollectionBuilder.newBuilder(
                versionData(1L, "v1", "version 1", pd100, -3, 7, resolvedData(7, 100),
                    unResolvedData(0, 0)),
                versionData(4L, "v1.0", "version 1.0", pd101, -2, 3, resolvedData(2, 66),
                    unResolvedData(1, 34)),
                versionData(2L, "v2", "version 2", pd100, 3, 3, resolvedData(1, 34),
                    unResolvedData(2, 66)),
                versionData(5L, "v2.0", "version 2.0", pd101, 4, 7, resolvedData(0, 0),
                    unResolvedData(7, 100))
            ).asList(),
            30, getFormattedDate(30)
        )).build();

        assertEquals(expectedRes, actualRes);

        verify(mockAuthenticationContext, atLeastOnce()).getLoggedInUser();
    }

    @Test
    public final void testGenerate_nonexistent_project() throws SearchException {
        RoadMapResource instance = createInstance();

        when(mockProjectManager.getProjectObj(100L)).thenReturn(null);

        Response actualRes = instance.generate("100", 30, 10);

        Response expectedRes = Response.ok(new RoadMapResource.RoadMapData(
            new ArrayList<RoadMapResource.VersionData>(), 30, getFormattedDate(30)
        )).build();

        assertEquals(expectedRes, actualRes);


    }

    @Test
    public final void testGenerate_single_category() throws SearchException {
        Project p100 = stubProject(100L, "HOMOSAP", "homosapien");
        Project p101 = stubProject(101L, "MKY", "monkey");
        Project p102 = stubProject(102L, "CHIMP", "chimpanzee");
        Collection<Project> mockAllSelectedProjects =
            CollectionBuilder.<Project>newBuilder(p100, p101, p102).asList();
        when(mockProjectManager.getProjectObjectsFromProjectCategory(123L))
            .thenReturn(mockAllSelectedProjects);
        when(mockProjectManager.getProjectObj(100L)).thenReturn(p100);
        when(mockPermissionManager.hasPermission(ProjectPermissions.BROWSE_PROJECTS, p100, mockUser)).
                                                                                         thenReturn(
                                                                                             true);
        when(mockProjectManager.getProjectObj(101L)).thenReturn(p101);
        when(mockPermissionManager.hasPermission(ProjectPermissions.BROWSE_PROJECTS, p101, mockUser)).
                                                                                         thenReturn(
                                                                                             true);
        when(mockProjectManager.getProjectObj(102L)).thenReturn(p102);
        when(mockPermissionManager.hasPermission(ProjectPermissions.BROWSE_PROJECTS, p102, mockUser)).
                                                                                         thenReturn(
                                                                                             false);

        Version p100v1 = stubVersion(1L, p100, "v1", "version 1", -3);
        Version p100v2 = stubVersion(2L, p100, "v2", "version 2", 3);
        Version p100v3 = stubVersion(3L, p100, "v3", "version 3", 31);
        Version p101v1 = stubVersion(4L, p101, "v1.0", "version 1.0", -2);
        Version p101v2 = stubVersion(5L, p101, "v2.0", "version 2.0", 4);
        Version p101v3 = stubVersion(6L, p101, "v3.0", "version 3.0", null);
        when(mockVersionManager.getVersionsUnreleased(100L, false))
            .thenReturn(asList(p100v1, p100v2, p100v3));
        when(mockVersionManager.getVersionsUnreleased(101L, false))
            .thenReturn(asList(p101v1, p101v2, p101v3));

        Map<Long, SearchExpectation> searchExpects =
            MapBuilder.<Long, SearchExpectation>newBuilder().
                                                                add(1L,
                                                                    new SearchExpectation(1L, 7L,
                                                                        0L)).
                                                                add(2L,
                                                                    new SearchExpectation(2L, 3L,
                                                                        2L)).
                                                                add(4L,
                                                                    new SearchExpectation(4L, 3L,
                                                                        1L)).
                                                                add(5L,
                                                                    new SearchExpectation(5L, 7L,
                                                                        7L))
                                                            .toHashMap();

        RoadMapResource instance = new PartiallyStubbedOutRoadMapResource(searchExpects);

        Response actualRes = instance.generate("cat123", 30, 10);

        RoadMapResource.ProjectData pd100 = projectData(100L, "HOMOSAP", "homosapien");
        RoadMapResource.ProjectData pd101 = projectData(101L, "MKY", "monkey");
        Response expectedRes = Response.ok(new RoadMapResource.RoadMapData(
            CollectionBuilder.newBuilder(
                versionData(1L, "v1", "version 1", pd100, -3, 7, resolvedData(7, 100),
                    unResolvedData(0, 0)),
                versionData(4L, "v1.0", "version 1.0", pd101, -2, 3, resolvedData(2, 66),
                    unResolvedData(1, 34)),
                versionData(2L, "v2", "version 2", pd100, 3, 3, resolvedData(1, 34),
                    unResolvedData(2, 66)),
                versionData(5L, "v2.0", "version 2.0", pd101, 4, 7, resolvedData(0, 0),
                    unResolvedData(7, 100))
            ).asList(),
            30, getFormattedDate(30)
        )).build();

        assertEquals(expectedRes, actualRes);

        verify(mockAuthenticationContext, atLeastOnce()).getLoggedInUser();
    }

    @Test
    public final void testGenerate_multiple_categories() throws SearchException {
        Project p100 = stubProject(100L, "HOMOSAP", "homosapien");
        Project p101 = stubProject(101L, "BEE", "bumblebee");
        Project p102 = stubProject(102L, "MKY", "monkey");
        Collection<Project> mockPrimateProjects =
            CollectionBuilder.<Project>newBuilder(p100, p102).asList();
        Collection<Project> mockInsectProjects =
            CollectionBuilder.<Project>newBuilder(p101).asList();
        when(mockProjectManager.getProjectObjectsFromProjectCategory(123L))
            .thenReturn(mockPrimateProjects);
        when(mockProjectManager.getProjectObjectsFromProjectCategory(456L))
            .thenReturn(mockInsectProjects);
        when(mockProjectManager.getProjectObj(100L)).thenReturn(p100);
        when(mockPermissionManager.hasPermission(ProjectPermissions.BROWSE_PROJECTS, p100, mockUser)).
                                                                                         thenReturn(
                                                                                             true);
        when(mockProjectManager.getProjectObj(101L)).thenReturn(p101);
        when(mockPermissionManager.hasPermission(ProjectPermissions.BROWSE_PROJECTS, p101, mockUser)).
                                                                                         thenReturn(
                                                                                             true);
        when(mockProjectManager.getProjectObj(102L)).thenReturn(p102);
        when(mockPermissionManager.hasPermission(ProjectPermissions.BROWSE_PROJECTS, p102, mockUser)).
                                                                                         thenReturn(
                                                                                             false);

        Version p100v1 = stubVersion(1L, p100, "v1", "version 1", -3);
        Version p100v2 = stubVersion(2L, p100, "v2", "version 2", 3);
        Version p100v3 = stubVersion(3L, p100, "v3", "version 3", 31);
        Version p101v1 = stubVersion(4L, p101, "v1.0", "version 1.0", -2);
        Version p101v2 = stubVersion(5L, p101, "v2.0", "version 2.0", 4);
        Version p101v3 = stubVersion(6L, p101, "v3.0", "version 3.0", null);
        when(mockVersionManager.getVersionsUnreleased(100L, false))
            .thenReturn(asList(p100v1, p100v2, p100v3));
        when(mockVersionManager.getVersionsUnreleased(101L, false))
            .thenReturn(asList(p101v1, p101v2, p101v3));

        Map<Long, SearchExpectation> searchExpects =
            MapBuilder.<Long, SearchExpectation>newBuilder().
                                                                add(1L,
                                                                    new SearchExpectation(1L, 7L,
                                                                        0L)).
                                                                add(2L,
                                                                    new SearchExpectation(2L, 3L,
                                                                        2L)).
                                                                add(4L,
                                                                    new SearchExpectation(4L, 3L,
                                                                        1L)).
                                                                add(5L,
                                                                    new SearchExpectation(5L, 7L,
                                                                        7L))
                                                            .toHashMap();

        RoadMapResource instance = new PartiallyStubbedOutRoadMapResource(searchExpects);

        Response actualRes = instance.generate("cat123|cat456", 30, 10);

        RoadMapResource.ProjectData pd100 = projectData(100L, "HOMOSAP", "homosapien");
        RoadMapResource.ProjectData pd101 = projectData(101L, "BEE", "bumblebee");
        Response expectedRes = Response.ok(new RoadMapResource.RoadMapData(
            CollectionBuilder.newBuilder(
                versionData(1L, "v1", "version 1", pd100, -3, 7, resolvedData(7, 100),
                    unResolvedData(0, 0)),
                versionData(4L, "v1.0", "version 1.0", pd101, -2, 3, resolvedData(2, 66),
                    unResolvedData(1, 34)),
                versionData(2L, "v2", "version 2", pd100, 3, 3, resolvedData(1, 34),
                    unResolvedData(2, 66)),
                versionData(5L, "v2.0", "version 2.0", pd101, 4, 7, resolvedData(0, 0),
                    unResolvedData(7, 100))
            ).asList(),
            30, getFormattedDate(30)
        )).build();

        assertEquals(expectedRes, actualRes);

        verify(mockAuthenticationContext, atLeastOnce()).getLoggedInUser();
    }

    @Test
    public final void testGenerate_both_project_category_selected() throws SearchException {
        Project p100 = stubProject(100L, "HOMOSAP", "homosapien");
        Project p101 = stubProject(101L, "BEE", "bumblebee");
        Project p102 = stubProject(102L, "MKY", "monkey");
        Collection<Project> mockPrimateProjects =
            CollectionBuilder.<Project>newBuilder(p100, p102).asList();
        when(mockProjectManager.getProjectObjectsFromProjectCategory(123L))
            .thenReturn(mockPrimateProjects);
        when(mockProjectManager.getProjectObj(100L)).thenReturn(p100);
        when(mockPermissionManager.hasPermission(ProjectPermissions.BROWSE_PROJECTS, p100, mockUser)).
                                                                                         thenReturn(
                                                                                             true);
        when(mockProjectManager.getProjectObj(101L)).thenReturn(p101);
        when(mockPermissionManager.hasPermission(ProjectPermissions.BROWSE_PROJECTS, p101, mockUser)).
                                                                                         thenReturn(
                                                                                             true);
        when(mockProjectManager.getProjectObj(102L)).thenReturn(p102);
        when(mockPermissionManager.hasPermission(ProjectPermissions.BROWSE_PROJECTS, p102, mockUser)).
                                                                                         thenReturn(
                                                                                             false);

        Version p100v1 = stubVersion(1L, p100, "v1", "version 1", -3);
        Version p100v2 = stubVersion(2L, p100, "v2", "version 2", 3);
        Version p100v3 = stubVersion(3L, p100, "v3", "version 3", 31);
        Version p101v1 = stubVersion(4L, p101, "v1.0", "version 1.0", -2);
        Version p101v2 = stubVersion(5L, p101, "v2.0", "version 2.0", 4);
        Version p101v3 = stubVersion(6L, p101, "v3.0", "version 3.0", null);
        when(mockVersionManager.getVersionsUnreleased(100L, false))
            .thenReturn(asList(p100v1, p100v2, p100v3));
        when(mockVersionManager.getVersionsUnreleased(101L, false))
            .thenReturn(asList(p101v1, p101v2, p101v3));

        Map<Long, SearchExpectation> searchExpects =
            MapBuilder.<Long, SearchExpectation>newBuilder().
                                                                add(1L,
                                                                    new SearchExpectation(1L, 7L,
                                                                        0L)).
                                                                add(2L,
                                                                    new SearchExpectation(2L, 3L,
                                                                        2L)).
                                                                add(4L,
                                                                    new SearchExpectation(4L, 3L,
                                                                        1L)).
                                                                add(5L,
                                                                    new SearchExpectation(5L, 7L,
                                                                        7L))
                                                            .toHashMap();

        RoadMapResource instance = new PartiallyStubbedOutRoadMapResource(searchExpects);

        Response actualRes = instance.generate("101|cat123", 30, 10);

        RoadMapResource.ProjectData pd100 = projectData(100L, "HOMOSAP", "homosapien");
        RoadMapResource.ProjectData pd101 = projectData(101L, "BEE", "bumblebee");
        Response expectedRes = Response.ok(new RoadMapResource.RoadMapData(
            CollectionBuilder.newBuilder(
                versionData(1L, "v1", "version 1", pd100, -3, 7, resolvedData(7, 100),
                    unResolvedData(0, 0)),
                versionData(4L, "v1.0", "version 1.0", pd101, -2, 3, resolvedData(2, 66),
                    unResolvedData(1, 34)),
                versionData(2L, "v2", "version 2", pd100, 3, 3, resolvedData(1, 34),
                    unResolvedData(2, 66)),
                versionData(5L, "v2.0", "version 2.0", pd101, 4, 7, resolvedData(0, 0),
                    unResolvedData(7, 100))
            ).asList(),
            30, getFormattedDate(30)
        )).build();

        assertEquals(expectedRes, actualRes);

        verify(mockAuthenticationContext, atLeastOnce()).getLoggedInUser();
    }

    @Test
    public final void testSearchCount() throws SearchException {
        Query q = mock(Query.class);
        when(mockSearchService.searchCount(mockUser, q)).thenReturn(123L);
        assertThat(123L, equalTo(createInstance().searchCount(q)));
        verify(mockAuthenticationContext, atLeastOnce()).getLoggedInUser();
    }

    @Test
    public final void testGetQueryString() throws SearchException {
        Query q = mock(Query.class);
        when(mockSearchService.getQueryString(mockUser, q)).thenReturn("abc");
        assertThat("abc", equalTo(createInstance().getQueryString(q)));
        verify(mockAuthenticationContext, atLeastOnce()).getLoggedInUser();
    }

    private RoadMapResource.ProjectData projectData(Long id, String key, String name) {
        return new RoadMapResource.ProjectData(id, key, name);
    }

    private RoadMapResource.VersionData versionData(Long id, String name, String desc,
                                                    RoadMapResource.ProjectData proj,
                                                    int daysToAdd, int allCount, int[] resolvedData,
                                                    int[] unresolvedData) {
        return new RoadMapResource.VersionData(id, name, desc, proj, getFormattedDate(daysToAdd),
            getIso8601FormattedDate(daysToAdd), daysToAdd < 0,
            allCount, resolvedData(id, resolvedData[0], resolvedData[1]),
            unResolvedData(id, unresolvedData[0], unresolvedData[1]));
    }

    private int[] resolvedData(int count, int percentage) {
        return new int[]{count, percentage};
    }

    private int[] unResolvedData(int count, int percentage) {
        return new int[]{count, percentage};
    }

    private RoadMapResource.ResolutionData resolvedData(long vid, int count, int percentage) {
        return new RoadMapResource.ResolutionData(count, percentage, "resolved for " + vid);
    }

    private RoadMapResource.ResolutionData unResolvedData(long vid, int count, int percentage) {
        return new RoadMapResource.ResolutionData(count, percentage, "unresolved for " + vid);
    }

    private List<Project> stubProjects(Long... ids) {
        return CollectionUtil.transform(asList(ids), new Function<Long, Project>() {
            public Project get(final Long input) {
                return stubProject(input);
            }
        });
    }

    private Project stubProject(Long id) {
        MockProject mockProject = new MockProject(id);
        when(mockProjectManager.getProjectObj(id)).thenReturn(mockProject);
        return mockProject;
    }

    private Project stubProject(final Long id, final String key, final String name) {
        return new MockProject(id, key, name);
    }

    private Version stubVersion(final Long id, final Project parent, final String name,
                                final String desc,
                                final Integer daysToRelease) {

        final MockVersionWithDescription v = new MockVersionWithDescription(id, name, parent, desc);
        v.setReleaseDate(getDate(daysToRelease));
        return v;
    }

    private List<ProjectCategory> stubCategories(Long... ids) {
        return CollectionUtil.transform(asList(ids), new Function<Long, ProjectCategory>() {
            public ProjectCategory get(final Long input) {
                return stubCategory(input);
            }
        });
    }

    private ProjectCategory stubCategory(Long id) {
        ProjectCategory c = mock(ProjectCategory.class);
        when(c.getId()).thenReturn(id);
        when(mockProjectManager.getProjectCategory(id)).thenReturn(c);
        return c;
    }

    private Date getDate(Integer daysToAdd) {
        if (daysToAdd == null) {
            return null;
        }

        //set h m s to fixed values so we avoid random test failures!
        final DateTime dateTime =
            DateTime.now()
                    .plusDays(daysToAdd)
                    .withHourOfDay(10)
                    .withMinuteOfHour(10)
                    .withSecondOfMinute(10);
        return dateTime.toDate();
    }

    private String getFormattedDate(int daysToAdd) {
        return df.format(getDate(daysToAdd));
    }

    private String getIso8601FormattedDate(int daysToAdd) {
        return dfIso8601.format(getDate(daysToAdd));
    }

    private class SearchExpectation {
        private Long versionId;
        private Query mockAllQuery = mock(Query.class);
        private Query mockUnresolvedQuery = mock(Query.class);
        private Query mockResolvedQuery = mock(Query.class);
        private long allCount;
        private long unresolvedCount;

        SearchExpectation(Long vId, long allCount, long unresolvedCount) {
            this.versionId = vId;
            this.allCount = allCount;
            this.unresolvedCount = unresolvedCount;
        }

        public Query getMockAllQuery() {
            return mockAllQuery;
        }

        public Query getMockUnresolvedQuery() {
            return mockUnresolvedQuery;
        }

        public Query getMockResolvedQuery() {
            return mockResolvedQuery;
        }

        public boolean owns(Query q) {
            return q == mockAllQuery || q == mockUnresolvedQuery || q == mockResolvedQuery;
        }

        public long count(Query q) {
            if (owns(q)) {
                if (q == mockAllQuery) {
                    return allCount;
                } else if (q == mockUnresolvedQuery) {
                    return unresolvedCount;
                } else {
                    return allCount - unresolvedCount;
                }
            } else {
                return -1;
            }
        }

        public String toQueryString(Query q) {
            if (owns(q)) {
                if (q == mockUnresolvedQuery) {
                    return "unresolved for " + versionId;
                } else if (q == mockResolvedQuery) {
                    return "resolved for " + versionId;
                } else {
                    return "all for " + versionId;
                }
            } else {
                return "";
            }
        }
    }

    private class PartiallyStubbedOutRoadMapResource extends RoadMapResource {
        private Map<Long, SearchExpectation> expectations;

        PartiallyStubbedOutRoadMapResource(Map<Long, SearchExpectation> expectations) {
            super(mockAuthenticationContext, mockPermissionManager, mockProjectManager, mockVersionManager,
                mockSearchService, mauEventService, null, dateTimeFormatter);
            this.expectations = expectations;
        }

        @Override
        Query buildAllIssuesForFixVersionQuery(final Version version) {
            return expectations.get(version.getId()).getMockAllQuery();
        }

        @Override
        Query buildUnresolvedIssuesForFixVersionQuery(final Version version) {
            return expectations.get(version.getId()).getMockUnresolvedQuery();
        }

        @Override
        Query buildResolvedIssuesForFixVersionQuery(final Version version) {
            return expectations.get(version.getId()).getMockResolvedQuery();
        }

        @Override
        long searchCount(final Query query) throws SearchException {
            SearchExpectation se = CollectionUtil.findFirstMatch(
                expectations.values(), new OwningSearchExpection(query));
            return se == null ? -1 : se.count(query);
        }

        @Override
        String getQueryString(final Query query) {
            SearchExpectation se = CollectionUtil.findFirstMatch(
                expectations.values(), new OwningSearchExpection(query));
            return se == null ? "" : se.toQueryString(query);
        }
    }

    private static class OwningSearchExpection implements Predicate<SearchExpectation> {
        private Query query;

        public OwningSearchExpection(Query query) {
            this.query = query;
        }

        public boolean evaluate(final SearchExpectation input) {
            return input.owns(query);
        }
    }

    /**
     * Asserts that the REST responses equal.
     * <p>
     * Currently, only status and entity are checked. Cache control is not checked.
     *
     * @param expected the expected response
     * @param actual the actual response
     */
    private void assertEquals(Response expected, Response actual) {
        assertThat("Response has an unexpected status code. expected <" + expected.getStatus()
                + "> but was:<" +
                actual.getStatus() + ">. Response expected:<" +
                ToStringBuilder.reflectionToString(expected, TO_STRING_STYLE) + "> but was:<" +
                ToStringBuilder.reflectionToString(actual, TO_STRING_STYLE) + ">",
            actual.getStatus(), equalTo(expected.getStatus())
        );
        if (expected.getEntity() instanceof ErrorCollection && actual
            .getEntity() instanceof ErrorCollection) {
            assertEquals((ErrorCollection) expected.getEntity(),
                (ErrorCollection) actual.getEntity());
        } else {
            assertEntityEquals(expected.getEntity(), actual.getEntity());
        }
    }

    /**
     * Intended to be overriden by subclass where it cannot reply on the equals() method implementation of the entity
     * class.
     * <p>
     * It is recommended to override {@link Object#equals(Object)} method for all classes referenced by entities
     * contained in the response.
     *
     * @param expected the expected entity contained in the {@link javax.ws.rs.core.Response} object
     * @param actual the actual entity contained in the {@link javax.ws.rs.core.Response} object
     */
    private void assertEntityEquals(final Object expected, final Object actual) {
        assertThat(actual, equalTo(expected));
    }

    /**
     * Asserts that the error collections equal.
     *
     * @param expected the expected error collection
     * @param actual the actual error collection
     */
    private void assertEquals(ErrorCollection expected, ErrorCollection actual) {
        assertThat(expected.hasAnyErrors(), equalTo(actual.hasAnyErrors()));
        assertThat(expected.getErrorMessages(), equalTo(actual.getErrorMessages()));
        List<ValidationError> expectedVes = new ArrayList<ValidationError>(expected.getErrors());
        List<ValidationError> actualVes = new ArrayList<ValidationError>(actual.getErrors());
        assertThat(expectedVes.size(), equalTo(actualVes.size()));
        for (int i = 0; i < expectedVes.size(); i++) {
            ValidationError e = expectedVes.get(i);
            ValidationError a = actualVes.get(i);
            assertThat("expected:<" + ToStringBuilder.reflectionToString(e, TO_STRING_STYLE) +
                    "> but was:<" + ToStringBuilder.reflectionToString(a, TO_STRING_STYLE) + ">",
                EqualsBuilder.reflectionEquals(e, a), is(true)
            );
        }
    }

    public static class MockVersionWithDescription extends MockVersion {
        private final String description;

        public MockVersionWithDescription(final long id, final String name, final Project project,
                                          final String description) {
            super(id, name, project);
            this.description = description;
        }

        @Override
        public String getDescription() {
            return description;
        }
    }
}
