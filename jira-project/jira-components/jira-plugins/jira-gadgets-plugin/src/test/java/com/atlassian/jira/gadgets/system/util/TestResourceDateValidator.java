package com.atlassian.jira.gadgets.system.util;

import com.atlassian.jira.charts.ChartFactory;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.rest.v1.model.errors.ValidationError;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.emptyIterable;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * Unit test for {@link ResourceDateValidator}.
 *
 * @since v4.0
 */
public class TestResourceDateValidator {
    private static final String DAYS_FIELD = "daysField";

    @Rule
    public InitMockitoMocks initMocks = new InitMockitoMocks(this);

    @Mock
    private ApplicationProperties applicationProperties;

    private ResourceDateValidator rdv;

    @Before
    public void setUp() throws Exception {
        rdv = new ResourceDateValidator(applicationProperties);
    }

    @Test
    public void testValidateDaysPreviousNoApplicationProperties() {

        assertNoValidateDaysAgainstPeriodErrors("10", ChartFactory.PeriodName.hourly);
    }

    @Test
    public void testValidateDaysPreviousAgainstPeriod() throws Exception {
        when(applicationProperties.getDefaultBackedString(Mockito.anyString())).thenReturn("10");

        assertNoValidateDaysAgainstPeriodErrors("10", ChartFactory.PeriodName.hourly);
        assertValidateDaysAgainstPeriodErrors("11", ChartFactory.PeriodName.hourly);

        assertNoValidateDaysAgainstPeriodErrors("10", ChartFactory.PeriodName.daily);
        assertValidateDaysAgainstPeriodErrors("11", ChartFactory.PeriodName.daily);

        assertNoValidateDaysAgainstPeriodErrors("10", ChartFactory.PeriodName.weekly);
        assertValidateDaysAgainstPeriodErrors("11", ChartFactory.PeriodName.weekly);

        assertNoValidateDaysAgainstPeriodErrors("10", ChartFactory.PeriodName.monthly);
        assertValidateDaysAgainstPeriodErrors("11", ChartFactory.PeriodName.monthly);

        assertNoValidateDaysAgainstPeriodErrors("10", ChartFactory.PeriodName.quarterly);
        assertValidateDaysAgainstPeriodErrors("11", ChartFactory.PeriodName.quarterly);

        assertNoValidateDaysAgainstPeriodErrors("10", ChartFactory.PeriodName.yearly);
        assertValidateDaysAgainstPeriodErrors("11", ChartFactory.PeriodName.yearly);
    }

    @Test
    public void testValidateDaysPreviousHappy() {
        assertNoValidateDaysPreviousErrors("3033");
        assertNoValidateDaysPreviousErrors("1");
        assertNoValidateDaysPreviousErrors("0");
        assertNoValidateDaysPreviousErrors("30");
    }

    @Test
    public void testValidateDaysPreviousSad() {
        assertValidateDaysPreviousError("999999999999999999999999999999999999999999999999999999999999999999999999");
        assertValidateDaysPreviousError("a");
        assertValidateDaysPreviousError("-44");
        assertValidateDaysPreviousError("3.3");
    }

    @Test
    public void testValidatePeriodHappy() {
        assertValidatePeriod(com.atlassian.jira.charts.ChartFactory.PeriodName.hourly);
        assertValidatePeriod(com.atlassian.jira.charts.ChartFactory.PeriodName.daily);
        assertValidatePeriod(com.atlassian.jira.charts.ChartFactory.PeriodName.weekly);
        assertValidatePeriod(com.atlassian.jira.charts.ChartFactory.PeriodName.monthly);
        assertValidatePeriod(com.atlassian.jira.charts.ChartFactory.PeriodName.quarterly);
        assertValidatePeriod(com.atlassian.jira.charts.ChartFactory.PeriodName.yearly);
    }

    @Test
    public void testValidatePeriodSad() {
        assertValidatePeriodError("foobar");
        assertValidatePeriodError("hour");
        assertValidatePeriodError("day");
        assertValidatePeriodError("Daily");
    }

    private void assertValidatePeriodError(String periodName) {
        List<ValidationError> errors = new ArrayList<ValidationError>();
        ChartFactory.PeriodName period = rdv.validatePeriod("periodField", periodName, errors);
        assertNull(period);
        assertThat(errors, Matchers.<ValidationError>iterableWithSize(1));
        assertEquals("periodField", errors.get(0).getField());
    }

    public void assertValidatePeriod(ChartFactory.PeriodName period) {
        List<ValidationError> errors = new ArrayList<ValidationError>();
        ChartFactory.PeriodName p = rdv.validatePeriod("aPeriodField", period.name(), errors);
        assertEquals(p, period);
        assertThat(errors, emptyIterable());
    }

    private void assertNoValidateDaysAgainstPeriodErrors(String days, ChartFactory.PeriodName period) {
        List<ValidationError> errors = new ArrayList<ValidationError>();
        rdv.validateDaysPrevious(DAYS_FIELD, period, days, errors);
        assertThat(errors, emptyIterable());
    }

    private void assertValidateDaysAgainstPeriodErrors(String days, ChartFactory.PeriodName period) {
        List<ValidationError> errors = new ArrayList<ValidationError>();
        rdv.validateDaysPrevious(DAYS_FIELD, period, days, errors);
        assertThat(errors, Matchers.<ValidationError>iterableWithSize(1));
        assertEquals(DAYS_FIELD, errors.get(0).getField());
    }

    private void assertNoValidateDaysPreviousErrors(String days) {
        when(applicationProperties.getDefaultBackedString("jira.chart.days.previous.limit.yearly")).thenReturn("9999999");

        List<ValidationError> errors = new ArrayList<ValidationError>();
        rdv.validateDaysPrevious(DAYS_FIELD, ChartFactory.PeriodName.yearly, days, errors);
        assertThat(errors, emptyIterable());
    }

    private void assertValidateDaysPreviousError(String days) {
        final List<ValidationError> errors = new ArrayList<ValidationError>();
        assertThat(rdv.validateDaysPrevious(DAYS_FIELD, ChartFactory.PeriodName.daily, days, errors), Matchers.lessThanOrEqualTo(-1));
        assertThat(errors, Matchers.<ValidationError>iterableWithSize(1));
        assertEquals(DAYS_FIELD, errors.get(0).getField());
    }
}
