package com.atlassian.jira.gadgets.system.util;

import com.google.common.collect.Maps;

import javax.servlet.http.HttpServletRequest;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MockHttpServletRequest {
    public static HttpServletRequest defaultServletRequestStub() {
        // cannot use jira's MockHttpServletRequest as it is not exported in the jar
        final HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        when(mockRequest.getServletPath()).thenReturn("servletPath");
        when(mockRequest.getRequestURL()).thenReturn(new StringBuffer("requestUrl"));
        when(mockRequest.getQueryString()).thenReturn("queryString");
        when(mockRequest.getParameterMap()).thenReturn(Maps.newHashMap());
        when(mockRequest.getScheme()).thenReturn("http");
        when(mockRequest.getServerName()).thenReturn("localhost");
        when(mockRequest.getServerPort()).thenReturn(8090);
        when(mockRequest.getContextPath()).thenReturn("jira");
        return mockRequest;
    }
}
