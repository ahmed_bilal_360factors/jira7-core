package com.atlassian.jira.gadgets.system;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.charts.util.ChartUtils;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.statistics.FilterStatisticsValuesGenerator;
import com.atlassian.jira.issue.statistics.StatisticsMapper;
import com.atlassian.jira.issue.statistics.TwoDimensionalStatsMap;
import com.atlassian.jira.issue.statistics.util.FieldValueToDisplayTransformer;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.rest.api.messages.TextMessage;
import com.atlassian.jira.rest.v1.model.errors.ValidationError;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.velocity.VelocityRequestContext;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.ofbiz.core.entity.GenericValue;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.gadgets.system.util.ResponseMatchers.hasStatus;
import static com.atlassian.jira.gadgets.system.util.ResponseMatchers.sameAs;
import static com.atlassian.jira.rest.v1.util.CacheControl.NO_CACHE;
import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestTwoDimensionalStatsResource {
    @Rule
    public InitMockitoMocks init = new InitMockitoMocks(this);

    @Mock
    StatisticsMapper xMapper;
    @Mock
    StatisticsMapper yMapper;
    @Mock
    I18nHelper mockI18n;
    @Mock
    SearchService mockSearchService;
    @Mock
    JiraAuthenticationContext mockAuthCtx;
    @Mock
    TwoDimensionalStatsMap map;
    @Mock
    StatisticTypesProvider statisticTypesProvider;
    @Mock
    ChartUtils utils;
    @Mock
    FieldValueToDisplayTransformer<StatsMarkup> mockTransformer;
    @Mock
    PermissionManager mockPermissionManager;
    @Mock
    SearchProvider mockSearchProvider;
    @Mock
    VelocityRequestContextFactory velocity;
    @Mock
    VelocityRequestContext velocityRequestContext;
    @Mock
    FilterStatisticsValuesGenerator generator;

    SearchService.IssueSearchParameters issueSearchParameters;

    private final StubSearchRequest request = new StubSearchRequest();
    private final ApplicationUser user = new MockApplicationUser("fooGuy", "Foo Guy", "foo@bar.com");

    private class StubSearchRequest extends SearchRequest {
        private Long id;

        public Long getId() {
            return id;
        }

        public void setId(final Long id) {
            this.id = id;
        }
    }

    @Test
    public void emptyResult() {
        final List<Object> list = Collections.emptyList();

        issueSearchParameters = SearchService.IssueSearchParameters.builder().query(request.getQuery()).build();
        when(mockSearchService.getIssueSearchPath(null, issueSearchParameters)).thenReturn("www.lol.com");
        when(mockAuthCtx.getI18nHelper()).thenReturn(mockI18n);

        final TwoDimensionalStatsResource.TwoDimensionalProperties dimensionalProperties = buildTwoDimensionalProperties(list, list, "assignees", null, null, "Assignee", "Assignee");
        assertTrue(dimensionalProperties.getFilter().isEmpty());
        assertEquals(0, dimensionalProperties.getTotalRows());
    }

    @Test
    public void oneResult() {
        request.setId(1337L);

        when(mockI18n.getText("gadget.twodimensionalfilterstats.total.xaxis")).thenReturn("T:");
        when(mockI18n.getText("gadget.twodimensionalfilterstats.total.yaxis")).thenReturn("Total Unique Issues");

        final List<ApplicationUser> objects = Arrays.asList(user);
        when(mockAuthCtx.getLoggedInUser()).thenReturn(user);
        when(mockAuthCtx.getI18nHelper()).thenReturn(mockI18n);

        when(mockSearchService.getIssueSearchPath(null, issueSearchParameters)).thenReturn("/issues/?theFilterUrl");

        final StatsSearchUrlBuilder builder = mock(StatsSearchUrlBuilder.class);

        when(builder.getSearchUrlForHeaderCell(user, xMapper, request)).thenReturn("?aAxisParam");
        when(builder.getSearchUrlForHeaderCell(user, yMapper, request)).thenReturn("?aAxisParam");
        when(builder.getSearchUrlForCell(user, user, map, request)).thenReturn("?aAxisParam&aAxisParam&combined");

        when(map.getCoordinate(user, user)).thenReturn(3);
        when(map.getYAxisUniqueTotal(user)).thenReturn(3);
        when(map.getXAxisUniqueTotal(user)).thenReturn(3);
        when(map.getYAxisUniqueTotal("assignees")).thenReturn(3);
        when(map.getUniqueTotal()).thenReturn(3L);

        when(mockTransformer.transformFromAssignee("assignees", user, null)).thenReturn(new StatsMarkup("Foo Guy"));

        final TwoDimensionalStatsResource.TwoDimensionalProperties properties = buildTwoDimensionalProperties(objects, objects, "assignees", "assignees", builder, "Assignee", "Assignee");

        assertEquals("http://localhost:8090/JIRA/issues/?filter=1337", properties.getFilter().getFilterUrl());

        final List<TwoDimensionalStatsResource.Cell> firstRowCells = properties.getFirstRow().getCells();
        assertEquals(2, firstRowCells.size());
        assertEquals("Foo Guy", firstRowCells.get(0).getMarkup());
        assertEquals("T:", firstRowCells.get(1).getMarkup());
        assertEquals(2, properties.getRows().size());

        final TwoDimensionalStatsResource.Row nextRow = properties.getRows().get(0);
        final List<TwoDimensionalStatsResource.Cell> nextRowCells = nextRow.getCells();
        assertFalse(properties.getFilter().isEmpty());
        assertEquals(3, nextRowCells.size());
        assertEquals("Foo Guy", nextRowCells.get(0).getMarkup());
        assertEquals("<a href='http://localhost:8090/JIRA/issues/?aAxisParam&aAxisParam&combined'>3</a>", nextRowCells.get(1).getMarkup());
        assertEquals("<a href='http://localhost:8090/JIRA/issues/?aAxisParam'>3</a>", nextRowCells.get(2).getMarkup());

        final TwoDimensionalStatsResource.Row totalsRow = properties.getRows().get(1);
        final List<TwoDimensionalStatsResource.Cell> totalRowsCell = totalsRow.getCells();
        assertEquals(3, totalRowsCell.size());
        assertEquals("Total Unique Issues:", totalRowsCell.get(0).getMarkup());
        assertEquals("<a href='http://localhost:8090/JIRA/issues/?aAxisParam'>3</a>", totalRowsCell.get(1).getMarkup());
        assertEquals("<a href='http://localhost:8090/JIRA/issues/?filter=1337'>3</a>", totalRowsCell.get(2).getMarkup());
        assertEquals(1, properties.getTotalRows());
    }

    @Test
    public void oneResultWithIrrelevants() {
        request.setId(1337L);

        final List<ApplicationUser> objects = Arrays.asList(user);
        when(mockI18n.getText("common.concepts.irrelevant.desc")).thenReturn("The field is not present on some issues");
        when(mockI18n.getText("common.concepts.irrelevant")).thenReturn("Irrelevant");
        when(mockI18n.getText("gadget.twodimensionalfilterstats.total.xaxis")).thenReturn("T:");
        when(mockI18n.getText("gadget.twodimensionalfilterstats.total.yaxis")).thenReturn("Total Unique Issues");

        when(mockAuthCtx.getLoggedInUser()).thenReturn(user);
        when(mockAuthCtx.getI18nHelper()).thenReturn(mockI18n);

        when(mockSearchService.getIssueSearchPath(null, issueSearchParameters)).thenReturn("/issues/?theFilterUrl");

        when(map.getCoordinate(user, user)).thenReturn(3);

        final StatsSearchUrlBuilder builder = mock(StatsSearchUrlBuilder.class);

        when(builder.getSearchUrlForHeaderCell(user, xMapper, request)).thenReturn("?aUrlForUser");
        when(builder.getSearchUrlForHeaderCell(user, yMapper, request)).thenReturn("?aUrlForUser");
        when(builder.getSearchUrlForCell(user, user, map, request)).thenReturn("?aUrlForUser&aUrlForUser");

        when(mockTransformer.transformFromAssignee("assignees", user, null)).thenReturn(new StatsMarkup("Foo Guy"));
        when(map.getXAxisUniqueTotal(user)).thenReturn(1);
        when(map.getXAxisUniqueTotal(FilterStatisticsValuesGenerator.IRRELEVANT)).thenReturn(1);
        when(map.getYAxisUniqueTotal(user)).thenReturn(1);
        when(map.getYAxisUniqueTotal(FilterStatisticsValuesGenerator.IRRELEVANT)).thenReturn(1);
        when(map.getUniqueTotal()).thenReturn(1L);

        final TwoDimensionalStatsResource.TwoDimensionalProperties properties = buildTwoDimensionalPropertiesWithIrrelevant(objects, objects, "assignees", "assignees", builder, "Assignee", "Assignee");

        assertEquals("http://localhost:8090/JIRA/issues/?filter=1337", properties.getFilter().getFilterUrl());

        final List<TwoDimensionalStatsResource.Cell> firstRowCells = properties.getFirstRow().getCells();
        assertEquals(3, firstRowCells.size());

        final TwoDimensionalStatsResource.Cell firstRowCell = firstRowCells.get(0);
        assertEquals("Foo Guy", firstRowCell.getMarkup());
        assertEquals("<span title=\"The field is not present on some issues\">Irrelevant</span>", firstRowCells.get(1).getMarkup());
        assertEquals(3, properties.getRows().size());

        final TwoDimensionalStatsResource.Row nextRow = properties.getRows().get(0);
        final List<TwoDimensionalStatsResource.Cell> nextRowCells = nextRow.getCells();
        assertFalse(properties.getFilter().isEmpty());
        assertEquals(4, nextRowCells.size());
        assertEquals("Foo Guy", nextRowCells.get(0).getMarkup());
        assertEquals("<a href='http://localhost:8090/JIRA/issues/?aUrlForUser&aUrlForUser'>3</a>", nextRowCells.get(1).getMarkup());
        assertEquals("1", nextRowCells.get(2).getMarkup());
        assertEquals(2, properties.getTotalRows());
    }

    @Test
    public void twoResults() {
        /* Test data :

                        Assignee
                        fooGuy      fooGuy2
            project1    1           2       3
            project2    3           4       7
            total       4           6       10
         */

        request.setId(1337L);

        final ApplicationUser userOne = new MockApplicationUser("fooGuy", "Foo Guy", "foo@bar.com");
        final ApplicationUser userTwo = new MockApplicationUser("fooGuy2", "Foo Guy2", "foo@bar.com");

        final GenericValue projectOne = mock(GenericValue.class);
        final GenericValue projectTwo = mock(GenericValue.class);

        when(projectOne.getString("name")).thenReturn("Project1");
        when(projectTwo.getString("name")).thenReturn("Project2");

        final List xObjects = Arrays.asList(userOne, userTwo);
        final List yObjects = Arrays.asList(projectOne, projectTwo);

        when(mockI18n.getText("gadget.twodimensionalfilterstats.total.xaxis")).thenReturn("T:");
        when(mockI18n.getText("gadget.twodimensionalfilterstats.total.yaxis")).thenReturn("Total Unique Issues");

        when(mockAuthCtx.getLoggedInUser()).thenReturn(userOne);
        when(mockAuthCtx.getI18nHelper()).thenReturn(mockI18n);
        //First should be filter url, others should be urls for header, then objects
        when(mockSearchService.getIssueSearchPath(null, issueSearchParameters)).thenReturn("/issues/?theFilterUrl");

        final StatsSearchUrlBuilder builder = mock(StatsSearchUrlBuilder.class);

        when(builder.getSearchUrlForHeaderCell(userOne, xMapper, request)).thenReturn("?urlForUser1");
        when(builder.getSearchUrlForHeaderCell(userOne, yMapper, request)).thenReturn("?urlForUser1");
        when(builder.getSearchUrlForHeaderCell(userTwo, xMapper, request)).thenReturn("?urlForUser2");
        when(builder.getSearchUrlForHeaderCell(userTwo, yMapper, request)).thenReturn("?urlForUser2");
        when(builder.getSearchUrlForHeaderCell(projectOne, yMapper, request)).thenReturn("?urlForProject1");
        when(builder.getSearchUrlForHeaderCell(projectTwo, yMapper, request)).thenReturn("?urlForProject2");
        when(builder.getSearchUrlForHeaderCell(projectOne, xMapper, request)).thenReturn("?urlForProject1");
        when(builder.getSearchUrlForHeaderCell(projectTwo, xMapper, request)).thenReturn("?urlForProject2");

        when(builder.getSearchUrlForCell(userOne, projectOne, map, request)).thenReturn("?urlForUser1InProject1");
        when(builder.getSearchUrlForCell(userTwo, projectOne, map, request)).thenReturn("?urlForUser2InProject1");
        when(builder.getSearchUrlForCell(userOne, projectTwo, map, request)).thenReturn("?urlForUser1InProject2");
        when(builder.getSearchUrlForCell(userTwo, projectTwo, map, request)).thenReturn("?urlForUser2InProject2");

        when(map.getCoordinate(userOne, projectOne)).thenReturn(1);
        when(map.getCoordinate(userTwo, projectOne)).thenReturn(2);
        when(map.getCoordinate(userOne, projectTwo)).thenReturn(3);
        when(map.getCoordinate(userTwo, projectTwo)).thenReturn(4);

        when(map.getYAxisUniqueTotal(projectOne)).thenReturn(3);
        when(map.getYAxisUniqueTotal(projectTwo)).thenReturn(7);
        when(map.getXAxisUniqueTotal(userOne)).thenReturn(4);
        when(map.getXAxisUniqueTotal(userTwo)).thenReturn(6);

        when(map.getUniqueTotal()).thenReturn(10L);

        when(mockTransformer.transformFromAssignee("assignees", userOne, null)).thenReturn(new StatsMarkup("Foo Guy"));
        when(mockTransformer.transformFromAssignee("assignees", userTwo, null)).thenReturn(new StatsMarkup("Foo Guy2"));
        when(mockTransformer.transformFromProject("project", projectOne, null)).thenReturn(new StatsMarkup("Project1"));
        when(mockTransformer.transformFromProject("project", projectTwo, null)).thenReturn(new StatsMarkup("Project2"));

        final TwoDimensionalStatsResource.TwoDimensionalProperties properties = buildTwoDimensionalProperties(xObjects, yObjects, "assignees", "project", builder, "Assignee", "Project");

        assertEquals("http://localhost:8090/JIRA/issues/?filter=1337", properties.getFilter().getFilterUrl());
        final List<TwoDimensionalStatsResource.Cell> firstRowCells = properties.getFirstRow().getCells();
        assertEquals(3, firstRowCells.size());
        assertEquals("Foo Guy", firstRowCells.get(0).getMarkup());
        assertEquals("Foo Guy2", firstRowCells.get(1).getMarkup());
        assertEquals("T:", firstRowCells.get(2).getMarkup());
        assertEquals(3, properties.getRows().size());
        final TwoDimensionalStatsResource.Row nextRow = properties.getRows().get(0);
        final List<TwoDimensionalStatsResource.Cell> nextRowCells = nextRow.getCells();
        assertFalse(properties.getFilter().isEmpty());
        assertEquals(4, nextRowCells.size());
        assertEquals("Project1", nextRowCells.get(0).getMarkup());
        assertEquals("<a href='http://localhost:8090/JIRA/issues/?urlForUser1InProject1'>1</a>", nextRowCells.get(1).getMarkup());
        assertEquals("<a href='http://localhost:8090/JIRA/issues/?urlForUser2InProject1'>2</a>", nextRowCells.get(2).getMarkup());
        assertEquals("<a href='http://localhost:8090/JIRA/issues/?urlForProject1'>3</a>", nextRowCells.get(3).getMarkup());

        final List<TwoDimensionalStatsResource.Cell> thirdRowCells = properties.getRows().get(1).getCells();
        assertEquals("Project2", thirdRowCells.get(0).getMarkup());
        assertEquals("<a href='http://localhost:8090/JIRA/issues/?urlForUser1InProject2'>3</a>", thirdRowCells.get(1).getMarkup());
        assertEquals("<a href='http://localhost:8090/JIRA/issues/?urlForUser2InProject2'>4</a>", thirdRowCells.get(2).getMarkup());
        assertEquals("<a href='http://localhost:8090/JIRA/issues/?urlForProject2'>7</a>", thirdRowCells.get(3).getMarkup());

        final TwoDimensionalStatsResource.Row totalsRow = properties.getRows().get(2);
        final List<TwoDimensionalStatsResource.Cell> totalRowsCell = totalsRow.getCells();
        assertEquals(4, totalRowsCell.size());
        assertEquals("Total Unique Issues:", totalRowsCell.get(0).getMarkup());
        assertEquals("<a href='http://localhost:8090/JIRA/issues/?urlForUser1'>4</a>", totalRowsCell.get(1).getMarkup());
        assertEquals("<a href='http://localhost:8090/JIRA/issues/?urlForUser2'>6</a>", totalRowsCell.get(2).getMarkup());
        assertEquals("<a href='http://localhost:8090/JIRA/issues/?filter=1337'>10</a>", totalRowsCell.get(3).getMarkup());
        assertEquals(2, properties.getTotalRows());
    }

    @Test
    public void resourceReturnsErrorOnInvalidFilter() {
        final MockSearchQueryBackedResource resource = makeResource();

        resource.addErrorsToReturn(new ValidationError("filterId", "invalid filter"));
        resource.setExpectedQueryString("filter-foo");

        final Response response = resource.getStats("filter-foo", "foo", "foo", "foo", "foo", "0");
        assertThat(response, hasStatus(equalTo(400)));
    }

    @Test
    public void validateReturnsErrorOnInvalidFilter() {
        final MockSearchQueryBackedResource resource = makeResource();
        when(statisticTypesProvider.getDisplayName("foo")).thenReturn("foo");
        resource.addErrorsToReturn(new ValidationError("filterId", "invalid filter"));
        resource.setExpectedQueryString("filter-23");

        final Response response = resource.validate("23", "foo", "foo", "10");
        assertThat(response, hasStatus(equalTo(400)));
    }

    @Test
    public void testPassingValidation() {
        final MockSearchQueryBackedResource resource = makeResource();
        when(statisticTypesProvider.getDisplayName("foo")).thenReturn("foo");
        resource.setExpectedQueryString("filter-23");

        final Response actualResponse = resource.validate("23", "foo", "foo", "10");
        assertThat(actualResponse, sameAs(Response.ok(new TextMessage("No input validation errors found.")).cacheControl(NO_CACHE).build()));
    }

    private MockSearchQueryBackedResource makeResource() {
        when(velocity.getJiraVelocityRequestContext()).thenReturn(velocityRequestContext);
        when(velocityRequestContext.getCanonicalBaseUrl()).thenReturn("http://localhost:8090/JIRA");

        return new MockSearchQueryBackedResource(utils, mockAuthCtx, mockSearchService, mockPermissionManager, statisticTypesProvider, mockSearchProvider, velocity, mockTransformer);
    }

    private MockSearchQueryBackedResourceWithIrrelevantData makeResourceWithIrrelevantData() {
        when(velocity.getJiraVelocityRequestContext()).thenReturn(velocityRequestContext);
        when(velocityRequestContext.getCanonicalBaseUrl()).thenReturn("http://localhost:8090/JIRA");

        return new MockSearchQueryBackedResourceWithIrrelevantData(utils, mockAuthCtx, mockSearchService, mockPermissionManager, statisticTypesProvider, mockSearchProvider, velocity, mockTransformer);
    }

    private TwoDimensionalStatsResource.TwoDimensionalProperties buildTwoDimensionalProperties(final List<?> xObjects, final List<?> yObjects, final String xStatsType, final String yStatsType, final StatsSearchUrlBuilder builder, final String xDisplayName, final String yDisplayName) {
        final MockSearchQueryBackedResource resource = makeResource();

        resource.setBuilder(builder);

        resource.setExpectedQueryString("filter-1000");

        when(generator.getStatsMapper(xStatsType)).thenReturn(xMapper);
        when(generator.getStatsMapper(yStatsType)).thenReturn(yMapper);

        when(statisticTypesProvider.getDisplayName(xStatsType)).thenReturn(xDisplayName);
        when(statisticTypesProvider.getDisplayName(yStatsType)).thenReturn(yDisplayName);

        resource.setGenerator(generator);
        resource.setSearchRequest(request);
        resource.setXAxis(xObjects);
        resource.setYAxis(yObjects);

        return resource.buildProperties(request, "ascending", "natural", 0, xStatsType, yStatsType);
    }

    private TwoDimensionalStatsResource.TwoDimensionalProperties buildTwoDimensionalPropertiesWithIrrelevant(final List<?> xObjects, final List<?> yObjects, final String xStatsType, final String yStatsType, final StatsSearchUrlBuilder builder, final String xDisplayName, final String yDisplayName) {
        final MockSearchQueryBackedResourceWithIrrelevantData resource = makeResourceWithIrrelevantData();

        resource.setBuilder(builder);

        resource.setExpectedQueryString("filter-1000");

        final FilterStatisticsValuesGenerator generator = mock(FilterStatisticsValuesGenerator.class);

        when(generator.getStatsMapper(xStatsType)).thenReturn(xMapper);
        when(generator.getStatsMapper(yStatsType)).thenReturn(yMapper);


        when(statisticTypesProvider.getDisplayName(xStatsType)).thenReturn(xDisplayName);
        when(statisticTypesProvider.getDisplayName(yStatsType)).thenReturn(yDisplayName);

        resource.setGenerator(generator);
        resource.setSearchRequest(request);
        resource.setXAxis(xObjects);
        resource.setYAxis(yObjects);

        return resource.buildProperties(request, "ascending", "natural", 0, xStatsType, yStatsType);
    }

    class MockSearchQueryBackedResource extends TwoDimensionalStatsResource {
        protected String expectedQueryString;
        protected Collection<ValidationError> errorsToReturn = new ArrayList<ValidationError>();
        protected SearchRequest searchRequest;
        protected List<?> xAxis;
        protected List<?> yAxis;
        protected StatsSearchUrlBuilder builder;

        public MockSearchQueryBackedResource(final ChartUtils chartUtils, final JiraAuthenticationContext authenticationContext,
                                             final SearchService searchService, final PermissionManager permissionManager,
                                             final StatisticTypesProvider statisticTypesProvider, final SearchProvider searchProvider,
                                             final VelocityRequestContextFactory velocityRequestContextFactory, final FieldValueToDisplayTransformer<StatsMarkup> fieldValueToDisplayTransformer) {
            super(chartUtils, authenticationContext, searchService, permissionManager, statisticTypesProvider, searchProvider, velocityRequestContextFactory, fieldValueToDisplayTransformer, null, null, null);
        }

        public void setBuilder(final StatsSearchUrlBuilder builder) {
            this.builder = builder;
        }

        public void setSearchRequest(final SearchRequest searchRequest) {
            this.searchRequest = searchRequest;
        }

        public void setXAxis(final List<?> xAxis) {
            this.xAxis = xAxis;
        }

        public void setYAxis(final List<?> yAxis) {
            this.yAxis = yAxis;
        }

        public void setExpectedQueryString(final String anExpectedQueryString) {
            this.expectedQueryString = anExpectedQueryString;
        }

        public void addErrorsToReturn(final ValidationError... errorsToAdd) {
            if (errorsToAdd != null) {
                errorsToReturn.addAll(asList(errorsToAdd));
            }
        }

        protected StatsSearchUrlBuilder getStatsSearchUrlBuilder() {
            return builder;
        }

        @Override
        protected SearchRequest getSearchRequestAndValidate(final String queryString, final Collection<ValidationError> errors, final Map<String, Object> params) {
            assertEquals(expectedQueryString, queryString);
            assertNotNull(errors);
            assertNotNull(params);
            errors.addAll(errorsToReturn);
            return searchRequest;
        }

        protected TwoDimensionalStatsMap getAndPopulateTwoDimensionalStatsMap(final StatisticsMapper xAxisMapper, final StatisticsMapper yAxisMapper, final SearchRequest searchRequest) {
            when(map.getXAxis()).thenReturn(xAxis);
            when(map.getYAxis("natural", "ascending")).thenReturn(yAxis);
            when(map.getxAxisMapper()).thenReturn(xAxisMapper);
            when(map.getyAxisMapper()).thenReturn(yAxisMapper);
            when(map.hasIrrelevantYData()).thenReturn(false);
            when(map.hasIrrelevantXData()).thenReturn(false);
            when(map.getBothIrrelevant()).thenReturn(0);
            return map;
        }
    }

    class MockSearchQueryBackedResourceWithIrrelevantData extends MockSearchQueryBackedResource {

        public MockSearchQueryBackedResourceWithIrrelevantData(final ChartUtils chartUtils, final JiraAuthenticationContext authenticationContext, final SearchService searchService, final PermissionManager permissionManager, final StatisticTypesProvider statisticTypesProvider, final SearchProvider searchProvider, final VelocityRequestContextFactory velocityRequestContextFactory, final FieldValueToDisplayTransformer<StatsMarkup> fieldValueToDisplayTransformer) {
            super(chartUtils, authenticationContext, searchService, permissionManager, statisticTypesProvider, searchProvider, velocityRequestContextFactory, fieldValueToDisplayTransformer);
        }

        @Override
        protected TwoDimensionalStatsMap getAndPopulateTwoDimensionalStatsMap(final StatisticsMapper xAxisMapper, final StatisticsMapper yAxisMapper, final SearchRequest searchRequest) {
            when(map.getXAxis()).thenReturn(xAxis);
            when(map.getYAxis("natural", "ascending")).thenReturn(yAxis);
            when(map.getxAxisMapper()).thenReturn(xAxisMapper);
            when(map.getyAxisMapper()).thenReturn(yAxisMapper);
            when(map.hasIrrelevantYData()).thenReturn(true);
            when(map.hasIrrelevantXData()).thenReturn(true);

            for (final Object xAxi : xAxis) {
                when(map.getYAxisIrrelevantTotal(xAxi)).thenReturn(1);
            }
            for (final Object yAxi : yAxis) {
                when(map.getXAxisIrrelevantTotal(yAxi)).thenReturn(1);
            }

            when(map.getBothIrrelevant()).thenReturn(2);
            return map;
        }
    }

}
