package com.atlassian.jira.gadgets.system;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.label.AlphabeticalLabelGroupingService;
import com.atlassian.jira.issue.label.AlphabeticalLabelGroupingSupport;
import com.atlassian.jira.issue.label.AlphabeticalLabelRenderer;
import com.atlassian.jira.issue.label.LabelUtil;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.web.bean.StatisticMapWrapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.ws.rs.core.Response;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * Tests the REST endpoint of {@link com.atlassian.jira.gadgets.system.LabelsResource}
 *
 * @since 7.0
 */
@RunWith(MockitoJUnitRunner.class)
public class TestLabelsResource {
    private static final long PROJECT_ID = 12222L;
    @Mock
    private AlphabeticalLabelGroupingSupport groupingSupport;
    @Mock
    private StatisticMapWrapper statisticMapWrapper;
    @Mock
    private TimeZoneManager timeZoneManager;
    @Mock
    private CustomFieldManager customFieldManager;
    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private ProjectManager projectManager;
    @Mock
    private FieldManager fieldManager;
    @Mock
    private LabelUtil labelUtil;
    @Mock
    private AlphabeticalLabelRenderer alphabeticalLabelRenderer;
    @Mock
    private AlphabeticalLabelGroupingService alphabeticalLabelGroupingService;
    @Mock
    private Field field;

    private ApplicationUser fred = new MockApplicationUser("fred");
    private Project project = new MockProject(10000L, "HSP", "Homosapien");

    private LabelsResource resource;

    @Before
    public void setUp() throws Exception {
        when(field.getName()).thenReturn("fieldName");
        when(authenticationContext.getLoggedInUser()).thenReturn(fred);
        resource = new LabelsResource(
                customFieldManager,
                authenticationContext,
                alphabeticalLabelRenderer,
                projectManager,
                fieldManager,
                labelUtil,
                alphabeticalLabelGroupingService);
    }

    @Test
    public void invalidProjectIdShouldReturnError() {
        final Response response = resource.getLabelsGroupsJSON("invalidProjectId", "labels");

        assertThat(response.getStatus(), is(400));
    }

    @Test
    public void projectDoesNotExistReturnsNotFound() {
        mockProjectAndFieldObject(null, field, "somefield");

        final Response response = resource.getLabelsGroupsJSON("project-12222", "labels");

        assertThat(response.getStatus(), is(404));
    }

    @Test
    public void fieldDoesNotExistReturnsNotFound() {
        mockProjectAndFieldObject(project, null, null);

        final Response response = resource.getLabelsGroupsJSON("project-12222", "labels");

        assertThat(response.getStatus(), is(404));
    }

    @Test
    public void alphabeticallyGroupedLabelsAreConvertedToResponseObjectsCorrectly() {
        mockProjectAndFieldObject(project, field, "labels");
        setupLabelGroupingMockForFieldId("labels");
        setupJQLMockForRegularField("labels");

        final Response response = resource.getLabelsGroupsJSON("project-12222", "labels");

        assertResponseValid(response);
    }

    @Test
    public void alphabeticallyGroupedLabelsForCustomFieldAreConvertedToResponseObjectsCorrectly() {
        mockProjectAndFieldObject(project, field, "customfield_10000");
        setupLabelGroupingMockForFieldId("customfield_10000");
        setupJQLMockForCustomField(10000L);

        final Response response = resource.getLabelsGroupsJSON("project-12222", "customfield_10000");

        assertResponseValid(response);
    }

    private void setupLabelGroupingMockForFieldId(String fieldId) {
        final AlphabeticalLabelGroupingSupport alphaSupport = new AlphabeticalLabelGroupingSupport(newHashSet("abc", "abcde"));
        when(alphabeticalLabelGroupingService.getAlphabeticallyGroupedLabels(fred, PROJECT_ID, fieldId)).thenReturn(alphaSupport);
    }

    private void setupJQLMockForRegularField(String fieldId) {
        when(labelUtil.getLabelSearchPathForProject(fred, PROJECT_ID, "abc")).thenReturn("/issues/?jql=abclabel");
        when(labelUtil.getLabelSearchPathForProject(fred, PROJECT_ID, "abcde")).thenReturn("/issues/?jql=abcdelabel");
    }

    private void setupJQLMockForCustomField(Long fieldId) {
        when(labelUtil.getLabelSearchPathForProject(fred, PROJECT_ID, fieldId, "abc")).thenReturn("/issues/?jql=abclabel");
        when(labelUtil.getLabelSearchPathForProject(fred, PROJECT_ID, fieldId, "abcde")).thenReturn("/issues/?jql=abcdelabel");
    }

    private void assertResponseValid(Response response) {
        final LabelsResource.LabelGroupList actual = (LabelsResource.LabelGroupList) response.getEntity();

        assertThat(response.getStatus(), is(200));
        assertThat(actual, is(getExpectedLabelGroup()));
    }

    private LabelsResource.LabelGroupList getExpectedLabelGroup() {
        final LabelsResource.LabelGroup azLabelGroup = new LabelsResource.LabelGroup("A-Z",
                newArrayList(
                        new LabelsResource.LabelElement("abc", "/issues/?jql=abclabel"),
                        new LabelsResource.LabelElement("abcde", "/issues/?jql=abcdelabel")));
        return new LabelsResource.LabelGroupList("fieldName", "Homosapien", newArrayList(azLabelGroup));
    }

    private void mockProjectAndFieldObject(final Project project, final Field field, final String fieldId) {
        when(fieldManager.getField(fieldId)).thenReturn(field);
        when(projectManager.getProjectObj(PROJECT_ID)).thenReturn(project);
    }
}