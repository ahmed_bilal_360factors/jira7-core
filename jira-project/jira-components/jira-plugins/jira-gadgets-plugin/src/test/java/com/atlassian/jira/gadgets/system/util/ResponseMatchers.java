package com.atlassian.jira.gadgets.system.util;

import com.atlassian.jira.rest.v1.model.errors.ErrorCollection;
import com.atlassian.jira.rest.v1.model.errors.ValidationError;
import com.google.common.collect.Lists;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.hamcrest.Description;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.hamcrest.collection.IsIterableContainingInOrder;

import javax.ws.rs.core.Response;
import java.util.Collection;
import java.util.List;

import static com.atlassian.jira.gadgets.system.util.ErrorCollectionMatchers.hasErrorMessagesThat;
import static com.atlassian.jira.gadgets.system.util.ErrorCollectionMatchers.hasErrorsEqualTo;
import static com.atlassian.jira.gadgets.system.util.ReflectiveMatchers.reflectivelyEquals;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;


public class ResponseMatchers {

    public static Matcher<Response> hasStatus(Matcher<Integer> statusMatcher) {
        return new FeatureMatcher<Response, Integer>(statusMatcher, "Response with status", "status") {
            @Override
            protected Integer featureValueOf(final Response actual) {
                return actual.getStatus();
            }
        };
    }

    public static Matcher<Response> hasSameStatusAs(final Response another) {
        return hasStatus(equalTo(another.getStatus()));
    }

    public static Matcher<Response> statusOk() {
        return hasStatus(equalTo(200));
    }


    private static Matcher<? super Response> hasEntityThat(final Matcher<Object> entityMatcher) {
        return new FeatureMatcher<Response, Object>(entityMatcher, "Response with entity", "entity object") {

            @Override
            protected Object featureValueOf(final Response actual) {
                return actual.getEntity();
            }
        };
    }

    private static Matcher<? super Response> hasErrorCollectionThat(final Matcher<ErrorCollection> entityMatcher) {
        return new FeatureMatcher<Response, ErrorCollection>(entityMatcher, "Response with error collection", "error collection") {

            @Override
            protected ErrorCollection featureValueOf(final Response actual) {
                if (actual.getEntity() instanceof ErrorCollection) {
                    return (ErrorCollection) actual.getEntity();
                } else {
                    throw new ClassCastException("Actual response entity is not an error collection!");
                }
            }
        };
    }

    public static Matcher<? super Response> sameAs(final Response another) {
        if (another.getEntity() instanceof ErrorCollection) {
            return allOf(hasSameStatusAs(another),
                    hasEntityThat(instanceOf(ErrorCollection.class)),
                    hasErrorCollectionThat(hasErrorMessagesThat(equalTo(entityAsErrorCollection(another).getErrorMessages()))),
                    hasErrorCollectionThat(hasErrorsEqualTo(entityAsErrorCollection(another).getErrors())));
        } else {
            return allOf(hasSameStatusAs(another), hasEntityThat(equalTo(another.getEntity())));
        }
    }

    private static ErrorCollection entityAsErrorCollection(final Response another) {
        return (ErrorCollection) another.getEntity();
    }
}

class ErrorCollectionMatchers {
    public static Matcher<ErrorCollection> hasErrorMessagesThat(Matcher<Collection<String>> errorMessagesMatcher) {
        return new FeatureMatcher<ErrorCollection, Collection<String>>(errorMessagesMatcher,
                "error collection with error messages", "error messages") {
            @Override
            protected Collection<String> featureValueOf(final ErrorCollection actual) {
                return actual.getErrorMessages();
            }
        };
    }

    public static Matcher<ErrorCollection> hasErrorsEqualTo(Collection<ValidationError> errors) {
        List<Matcher<? super ValidationError>> matchers = Lists.newArrayListWithCapacity(errors.size());
        for (ValidationError ve : errors) {
            matchers.add(reflectivelyEquals(ve));
        }
        final Matcher<Iterable<? extends ValidationError>> iterableMatcher = IsIterableContainingInOrder.contains(matchers);


        return new FeatureMatcher<ErrorCollection, Collection<ValidationError>>(iterableMatcher,
                "error collection with error messages", "error messages") {
            @Override
            protected Collection<ValidationError> featureValueOf(final ErrorCollection actual) {
                return actual.getErrors();
            }
        };
    }
}

class ReflectiveMatchers {
    private static final ToStringStyle TO_STRING_STYLE = ToStringStyle.SHORT_PREFIX_STYLE;

    public static <X> Matcher<X> reflectivelyEquals(final X other) {
        return new TypeSafeDiagnosingMatcher<X>() {

            @Override
            public void describeTo(final Description description) {
                description.appendText("Objects of the same properties");
            }

            @Override
            protected boolean matchesSafely(final X item, final Description mismatchDescription) {
                final boolean matches = EqualsBuilder.reflectionEquals(item, other);
                if (!matches) {
                    mismatchDescription.appendText("expected:<" + ToStringBuilder.reflectionToString(other, TO_STRING_STYLE) +
                            "> but was:<" + ToStringBuilder.reflectionToString(item, TO_STRING_STYLE) + ">");
                }
                return matches;
            }
        };
    }
}

