package com.atlassian.jira.gadgets.system;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.charts.Chart;
import com.atlassian.jira.charts.ChartFactory;
import com.atlassian.jira.charts.util.ChartUtils;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.rest.api.messages.TextMessage;
import com.atlassian.jira.rest.v1.model.errors.ErrorCollection;
import com.atlassian.jira.rest.v1.model.errors.ValidationError;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.google.common.collect.ImmutableMap;
import org.jfree.chart.urls.XYURLGenerator;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.xy.XYDataset;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.TimeZone;

import static com.atlassian.jira.gadgets.system.util.MockHttpServletRequest.defaultServletRequestStub;
import static com.atlassian.jira.gadgets.system.util.ResponseMatchers.sameAs;
import static com.atlassian.jira.rest.v1.util.CacheControl.NO_CACHE;
import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests the REST endpoint of {@link com.atlassian.jira.gadgets.system.TestCreatedVsResolvedResource}.
 *
 * @since v4.0
 */
public class TestCreatedVsResolvedResource {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    ChartUtils mockChartUtils;
    @Mock
    JiraAuthenticationContext mockAuthCtx;
    @Mock
    SearchService mockSearchService;
    @Mock
    PermissionManager mockPermissionManager;
    @Mock
    ChartFactory mockChartFactory;
    @Mock
    ApplicationProperties mockApplicationProperties;
    @Mock
    TimeZoneManager timeZoneManager;

    private final ApplicationUser mockUser = new MockApplicationUser("fred");
    private MockSearchQueryBackedResource instance;

    @Before
    public void setUp() throws Exception {
        when(timeZoneManager.getLoggedInUserTimeZone()).thenReturn(TimeZone.getDefault());
        instance = new MockSearchQueryBackedResource(mockChartUtils, mockAuthCtx,
                mockSearchService, mockPermissionManager, mockChartFactory, mockApplicationProperties);
    }

    @Test
    public final void validateSucceedsUsingVerbatimQueryWhenContainingDash() {
        final String query = "blah-100";
        final String days = "30";
        final String periodName = "daily";
        final String versionLabel = "major";
        final String expectedQuery = "blah-100";
        testValidateExpectingSuccess(query, days, periodName, versionLabel, expectedQuery);
    }

    @Test
    public final void validateDefaultsToFilterForQueriesNotContainingDash() {
        final String query = "100";
        final String days = "30";
        final String periodName = "daily";
        final String versionLabel = "major";
        final String expectedQuery = "filter-100";
        testValidateExpectingSuccess(query, days, periodName, versionLabel, expectedQuery);
    }

    @Test
    public final void validateFailsForMalformedFilterName() {
        final String query = "bad-query";
        final String days = "10";
        final String periodName = "daily";
        final String versionLabel = "major";
        final ValidationError error = new ValidationError("foo", "bar");

        when(mockApplicationProperties.getDefaultBackedString("jira.chart.days.previous.limit." + periodName))
                .thenReturn(days);

        testValidateExpectingSearchError(query, days, periodName, versionLabel, error);
    }

    @Test
    public final void validateFailsForInvalidPeriod() {
        final String query = "good-query";
        final String days = "10";
        final String periodName = "rarely";
        final String versionLabel = "major";
        final ValidationError error = new ValidationError("periodName", "gadget.common.invalid.period");
        testValidateExpectingError(query, days, periodName, versionLabel, error);
    }

    @Test
    public final void validateFailsForNegativeDays() {
        final String query = "good-query";
        final String days = "-1";
        final String periodName = "daily";
        final String versionLabel = "major";
        final ValidationError error = new ValidationError("daysprevious", "gadget.common.negative.days");
        testValidateExpectingError(query, days, periodName, versionLabel, error);
    }

    @Test
    public final void validateFailsForNonNumericDays() {
        final String query = "good-query";
        final String days = "nan";
        final String periodName = "daily";
        final String versionLabel = "major";
        final ValidationError error = new ValidationError("daysprevious", "gadget.common.days.nan");
        testValidateExpectingError(query, days, periodName, versionLabel, error);
    }

    @Test
    public final void validateFailsForInvalidVersionLabel() {
        final String query = "good-query";
        final String days = "10";
        final String periodName = "daily";
        final String versionLabel = "snapshot";
        final ValidationError error = new ValidationError("versionLabel", "gadget.created.vs.resolved.invalid.version.label");

        when(mockApplicationProperties.getDefaultBackedString("jira.chart.days.previous.limit." + periodName))
                .thenReturn(days);

        testValidateExpectingError(query, days, periodName, versionLabel, error);
    }

    @Test
    public final void generateSucceedsWithNoAvilableData() {
        final String query = "blah-100";
        final int days = 30;
        final String periodName = "daily";
        final String versionLabel = "major";
        final String expectedQuery = "blah-100";
        testGenerateSucceeds(query, days, periodName, versionLabel, false, false, expectedQuery, null);
    }

    @Test
    public final void generateSucceedsAndReturnsData() {
        final String query = "blah-100";
        final int days = 30;
        final String periodName = "daily";
        final String versionLabel = "major";
        final String expectedQuery = "blah-100";
        final CreatedVsResolvedResource.DataRow[] data = new CreatedVsResolvedResource.DataRow[2];
        data[0] = new CreatedVsResolvedResource.DataRow("key1", "createdUrl1", 1, "resolvedUrl1", 11, 21);
        data[1] = new CreatedVsResolvedResource.DataRow("key2", "createdUrl2", 2, "resolvedUrl1", 12, 22);
        testGenerateSucceeds(query, days, periodName, versionLabel, false, true, expectedQuery, data);
    }

    @Test
    public final void generateDefaultsToFilterForQueriesWithoutDash() {
        final String query = "100";
        final int days = 30;
        final String periodName = "daily";
        final String versionLabel = "major";
        final String expectedQuery = "filter-100";
        testGenerateSucceeds(query, days, periodName, versionLabel, false, false, expectedQuery, null);
    }

    @Test
    public final void generateFailsWithMalformedQuery() {
        final String query = "bad-query";
        final String days = "10";
        final String periodName = "daily";
        final String versionLabel = "major";
        final ValidationError error = new ValidationError("foo", "bar");

        when(mockApplicationProperties.getDefaultBackedString("jira.chart.days.previous.limit." + periodName))
                .thenReturn(days);

        testGenerateExpectingSearchError(query, days, periodName, versionLabel, false, false, error);
    }

    @Test
    public final void generateFailsWithInvalidPeriod() {
        final String query = "good-query";
        final String days = "10";
        final String periodName = "rarely";
        final String versionLabel = "major";
        final ValidationError error = new ValidationError("periodName", "gadget.common.invalid.period");
        testGenerateExpectingError(query, days, periodName, versionLabel, false, false, error);
    }

    @Test
    public final void generateFailsWithNegativeDays() {
        final String query = "good-query";
        final String days = "-1";
        final String periodName = "daily";
        final String versionLabel = "major";
        final ValidationError error = new ValidationError("daysprevious", "gadget.common.negative.days");
        testGenerateExpectingError(query, days, periodName, versionLabel, false, false, error);
    }

    @Test
    public final void generateFailsWithNonNumericDays() {
        final String query = "good-query";
        final String days = "nan";
        final String periodName = "daily";
        final String versionLabel = "major";
        final ValidationError error = new ValidationError("daysprevious", "gadget.common.days.nan");
        testGenerateExpectingError(query, days, periodName, versionLabel, false, false, error);
    }

    @Test
    public final void generateFailsWithInvalidVersionLabel() {
        final String query = "good-query";
        final String days = "10";
        final String periodName = "daily";
        final String versionLabel = "snapshot";
        final ValidationError error = new ValidationError("versionLabel", "gadget.created.vs.resolved.invalid.version.label");

        when(mockApplicationProperties.getDefaultBackedString("jira.chart.days.previous.limit." + periodName))
                .thenReturn(days);

        testGenerateExpectingError(query, days, periodName, versionLabel, false, false, error);
    }

    private void testValidateExpectingSuccess(final String query, final String days, final String periodName,
                                              final String versionLabel, final String expectedQuery) {
        instance.setExpectedQueryString(expectedQuery);

        when(mockApplicationProperties.getDefaultBackedString("jira.chart.days.previous.limit." + periodName))
                .thenReturn(days);

        final Response response = instance.validateChart(query, days, periodName, versionLabel);
        assertThat(response, sameAs(Response.ok(new TextMessage("No input validation errors found.")).cacheControl(NO_CACHE).build()));
    }

    private void testValidateExpectingSearchError(final String expectedQuery, final String days, final String periodName,
                                                  final String versionLabel, final ValidationError expectedError) {
        instance.addErrorsToReturn(expectedError);
        testValidateExpectingError(expectedQuery, days, periodName, versionLabel, expectedError);
    }

    private void testValidateExpectingError(final String expectedQuery, final String days, final String periodName,
                                            final String versionLabel, final ValidationError expectedError) {
        instance.setExpectedQueryString(expectedQuery);
        final Response response = instance.validateChart(expectedQuery, days, periodName, versionLabel);
        assertThat(response, sameAs(Response.status(400).entity(ErrorCollection.Builder.newBuilder(expectedError).build()).build()));
    }

    private void testGenerateSucceeds(final String query, final int days, final String periodName,
                                      final String versionLabel, final boolean cumulative, final boolean showUnresolvedTrend,
                                      final String expectedQuery, final CreatedVsResolvedResource.DataRow[] expectedReturnData) {
        when(mockAuthCtx.getLoggedInUser()).thenReturn(mockUser);

        final CategoryDataset dataSet = mock(CategoryDataset.class);
        final XYURLGenerator mockUrlGenerator = mock(XYURLGenerator.class);
        final XYDataset mockXyDataset = mock(XYDataset.class);

        if (expectedReturnData != null) {
            when(dataSet.getColumnCount()).thenReturn(expectedReturnData.length);  //verify

            for (int i = 0; i < expectedReturnData.length; i++) {
                when(dataSet.getColumnKey(i)).thenReturn(expectedReturnData[i].getKey());
                when(dataSet.getValue(0, i)).thenReturn(expectedReturnData[i].getCreatedValue());
                when(mockUrlGenerator.generateURL(mockXyDataset, 0, i)).thenReturn(expectedReturnData[i].getCreatedUrl());
                when(dataSet.getValue(1, i)).thenReturn(expectedReturnData[i].getResolvedValue());
                when(mockUrlGenerator.generateURL(mockXyDataset, 1, i)).thenReturn(expectedReturnData[i].getResolvedUrl());
                if (showUnresolvedTrend) {
                    when(dataSet.getValue(2, i)).thenReturn(expectedReturnData[i].getTrendCount());
                }
            }
        }

        instance.setExpectedQueryString(expectedQuery);

        when(mockApplicationProperties.getDefaultBackedString("jira.chart.days.previous.limit." + periodName))
                .thenReturn(String.valueOf(days));

        when(mockChartFactory.generateCreatedVsResolvedChart(
                isA(ChartFactory.ChartContext.class), eq(days), eq(ChartFactory.PeriodName.daily),
                eq(ChartFactory.VersionLabel.valueOf(versionLabel)), eq(cumulative), eq(showUnresolvedTrend))).
                thenReturn(new Chart("location", "imageMap", "imageMapName",
                        ImmutableMap.<String, Object>builder()
                                .put("numCreatedIssues", 10)
                                .put("numResolvedIssues", 20)
                                .put("completeDataset", dataSet)
                                .put("chartDataset", mockXyDataset)
                                .put("base64Image", "base64Image==")
                                .put("completeDatasetUrlGenerator", mockUrlGenerator).build()));

        final Response response = instance.generateChart(defaultServletRequestStub(), query, String.valueOf(days), periodName, versionLabel, cumulative,
                showUnresolvedTrend, expectedReturnData != null, 400, 250, true);

        assertThat(response, sameAs(Response.ok(
                new CreatedVsResolvedResource.CreatedVsResolvedChart(
                        "location", "filterTitle", "filterUrl", 10, 20, "imageMap", "imageMapName", expectedReturnData, 400, 250, "base64Image==")).build()));
    }

    private void testGenerateExpectingSearchError(final String expectedQuery, final String days, final String periodName,
                                                  final String versionLabel, final boolean cumulative, final boolean showUnresolvedTrend,
                                                  final ValidationError expectedError) {
        instance.addErrorsToReturn(expectedError);
        testGenerateExpectingError(expectedQuery, days, periodName, versionLabel, cumulative, showUnresolvedTrend, expectedError);
    }

    private void testGenerateExpectingError(final String expectedQuery, final String days, final String periodName,
                                            final String versionLabel, final boolean cumulative, final boolean showUnresolvedTrend,
                                            final ValidationError expectedError) {
        instance.setExpectedQueryString(expectedQuery);
        when(mockAuthCtx.getLoggedInUser()).thenReturn(mockUser);

        final Response response = instance.generateChart(defaultServletRequestStub(), expectedQuery, days, periodName, versionLabel, cumulative,
                showUnresolvedTrend, false, 400, 250, true);

        assertThat(response, sameAs(Response.status(400).entity(ErrorCollection.Builder.newBuilder(expectedError).build()).build()));
    }

    class MockSearchQueryBackedResource extends CreatedVsResolvedResource {
        private String expectedQueryString;
        private final Collection<ValidationError> errorsToReturn = new ArrayList<ValidationError>();

        MockSearchQueryBackedResource(final ChartUtils chartUtils, final JiraAuthenticationContext authenticationContext, final SearchService searchService, final PermissionManager permissionManager, final ChartFactory chartFactory, final ApplicationProperties applicationProperties) {
            super(chartFactory, chartUtils, authenticationContext, permissionManager, searchService, null, applicationProperties, timeZoneManager);
        }

        public void setExpectedQueryString(final String anExpectedQueryString) {
            this.expectedQueryString = anExpectedQueryString;
        }

        public void addErrorsToReturn(final ValidationError... errorsToAdd) {
            if (errorsToAdd != null) {
                errorsToReturn.addAll(asList(errorsToAdd));
            }
        }

        @Override
        protected SearchRequest getSearchRequestAndValidate(final String queryString, final Collection<ValidationError> errors, final Map<String, Object> params) {
            assertEquals(expectedQueryString, queryString);
            assertNotNull(errors);
            assertNotNull(params);
            errors.addAll(errorsToReturn);
            return new SearchRequest();
        }

        @Override
        protected String getFilterTitle(final Map<String, Object> params) {
            return "filterTitle";
        }

        @Override
        protected String getFilterUrl(final Map<String, Object> params) {
            return "filterUrl";
        }
    }
}
