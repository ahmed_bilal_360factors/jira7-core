package com.atlassian.jira.gadgets.system.bubblechart;

import com.atlassian.jira.bc.issue.comment.CommentService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.comments.MockComment;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestToIssueBubbleData {
    @Rule
    public MockitoContainer container = MockitoMocksInContainer.rule(this);

    @Mock
    private CommentService commentService;

    private static final long DAY_IN_MS = TimeUnit.DAYS.toMillis(1);
    private static final int DAY_LIMIT_SO_ALL_COMMENTS_ARE_RECENT = 28;
    private static final int DAY_LIMIT_SO_THREE_COMMENTS_ARE_RECENT = 7;
    private static final int DAY_LIMIT_SO_ONE_COMMENT_IS_RECENT = 1;

    private MockApplicationUser defaultUser;
    private MockApplicationUser user1;
    private MockApplicationUser user2;
    private MockApplicationUser user3;
    private MockApplicationUser user4;

    private MockComment comment1;
    private MockComment comment2;
    private MockComment comment3;
    private MockComment comment4;
    private MockComment comment5;
    private MockComment comment6;

    private List<Comment> commentList;
    private Integer commentListLength;
    private long commentListLengthAsLong;

    @Before
    public void setUp() {
        user1 = new MockApplicationUser("user1", "User 1", "user1@email.com");
        user2 = new MockApplicationUser("user2", "User 2", "user2@email.com");
        user3 = new MockApplicationUser("user3", "User 3", "user3@email.com");
        user4 = new MockApplicationUser("user4", "User 4", "user4@email.com");

        defaultUser = user1;

        comment1 = commentCreatedNDaysAgoWithAuthor(11, user1);
        comment2 = commentCreatedNDaysAgoWithAuthor(10, user2);
        comment3 = commentCreatedNDaysAgoWithAuthor(8, user1);
        comment4 = commentCreatedNDaysAgoWithAuthor(2, user4);
        comment5 = commentCreatedNDaysAgoWithAuthor(1, user1);
        comment6 = commentCreatedNDaysAgoWithAuthor(0, user4);

        commentList = Lists.newArrayList(comment1, comment2, comment3, comment4, comment5, comment6);
        commentListLength = commentList.size();
        commentListLengthAsLong = (long) commentListLength;
    }

    @Test
    public void shouldMapAnUnassignedIssueToTheCorrectBubbleDataForParticipantsBubbleType() {
        final MockIssue issue = newIssueWithReporterAndAssignee(user3, null);

        final IssueBubbleData data = toIssueBubbleData("participants", DAY_LIMIT_SO_ALL_COMMENTS_ARE_RECENT).apply(issue);

        assertThatBubbleDataIsCorrect(data, 0, commentListLength, 4, commentListLengthAsLong);
    }

    @Test
    public void shouldNotCountTheSameUserTwiceIfTheyAreInMultipleRoles() {
        // user1 will be a commenter, the reporter, and the assignee, but should only be counted once
        final MockIssue issue = newIssueWithReporterAndAssignee(user1, user1);

        final IssueBubbleData data = toIssueBubbleData("participants", DAY_LIMIT_SO_ALL_COMMENTS_ARE_RECENT).apply(issue);

        assertThatBubbleDataIsCorrect(data, 0, commentListLength, 3, commentListLengthAsLong);
    }

    @Test
    public void shouldMapAnAssignedIssueToTheCorrectBubbleDataForParticipantsBubbleType() {
        final MockIssue issue = newIssueWithReporterAndAssignee(user1, user3);

        final IssueBubbleData data = toIssueBubbleData("participants", DAY_LIMIT_SO_ALL_COMMENTS_ARE_RECENT).apply(issue);

        assertThatBubbleDataIsCorrect(data, 0, commentListLength, 4, commentListLengthAsLong);
    }

    @Test
    public void shouldMapAnAssignedIssueToTheCorrectBubbleDataForVotesBubbleType() {
        final MockIssue issue = newIssueWithReporterAndAssignee(user1, user3);
        issue.setVotes(326L);

        final IssueBubbleData data = toIssueBubbleData("votes", DAY_LIMIT_SO_ALL_COMMENTS_ARE_RECENT).apply(issue);

        assertThatBubbleDataIsCorrect(data, 0, commentListLength, 326, commentListLengthAsLong);
    }

    @Test
    public void shouldFilterCommentsPastRecentCommentsPeriodCutoff() {
        final MockIssue issue = new MockIssue();
        issue.setVotes(100L);
        mockCommentsForIssue(issue);

        final IssueBubbleData allCommentsData = toIssueBubbleData("votes", DAY_LIMIT_SO_ALL_COMMENTS_ARE_RECENT).apply(issue);
        final IssueBubbleData threeCommentsData = toIssueBubbleData("votes", DAY_LIMIT_SO_THREE_COMMENTS_ARE_RECENT).apply(issue);
        final IssueBubbleData twoCommentsData = toIssueBubbleData("votes", DAY_LIMIT_SO_ONE_COMMENT_IS_RECENT).apply(issue);

        assertThat(allCommentsData.getBubbleColorValue(), is(commentListLengthAsLong));
        assertThat(threeCommentsData.getBubbleColorValue(), is(3L));
        assertThat(twoCommentsData.getBubbleColorValue(), is(1L));
    }

    private void assertThatBubbleDataIsCorrect(IssueBubbleData data, int domain, int range, int bubbleRadius, long colorValue) {
        assertThat(data.getBubbleDomain(), is(domain));
        assertThat(data.getBubbleRange(), is(range));
        assertThat(data.getBubbleColorValue(), is(colorValue));
        assertThat(data.getBubbleRadius(), is(bubbleRadius));
    }

    private MockIssue newIssueWithReporterAndAssignee(ApplicationUser reporter, ApplicationUser assignee) {
        final MockIssue issue = new MockIssue();
        issue.setKey("UA-1");
        issue.setReporter(reporter);
        issue.setAssignee(assignee);
        mockCommentsForIssue(issue);

        return issue;
    }

    private ToIssueBubbleData toIssueBubbleData(String bubbleType, Integer recentCommentsDays) {
        return new ToIssueBubbleData(commentService, defaultUser, bubbleType, recentCommentsDays);
    }

    private void mockCommentsForIssue(Issue issue) {
        when(commentService.getCommentsForUser(defaultUser, issue)).thenReturn(commentList);
    }

    private final MockComment commentCreatedNow() {
        return new MockComment("author", "body");
    }

    private final MockComment commentCreatedNDaysAgoWithAuthor(Integer daysAgo, ApplicationUser user) {
        final MockComment comment = commentCreatedNow();
        final Date date = new Date(System.currentTimeMillis() - daysAgo * DAY_IN_MS);
        comment.setCreated(date);
        comment.setAuthor(user);

        return comment;
    }
}