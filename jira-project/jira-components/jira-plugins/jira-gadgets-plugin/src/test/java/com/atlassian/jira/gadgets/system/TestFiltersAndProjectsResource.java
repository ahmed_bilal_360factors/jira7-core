package com.atlassian.jira.gadgets.system;

import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.gadgets.system.FiltersAndProjectsResource.GroupWrapper;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.sharing.type.ShareTypeFactory;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.query.QueryImpl;
import com.google.common.collect.Lists;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Test;

import javax.ws.rs.core.Response;
import java.util.Collection;

import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestFiltersAndProjectsResource {

    @Test
    public void projectAndFilterReturned() {
        JiraAuthenticationContext authenticationContext = mock(JiraAuthenticationContext.class);
        ShareTypeFactory typeFactory = mock(ShareTypeFactory.class);
        PermissionManager permissionManager = mock(PermissionManager.class);
        I18nHelper.BeanFactory i18nHelper = mock(I18nHelper.BeanFactory.class);
        ApplicationUser user = mock(ApplicationUser.class);
        SearchRequestService searchRequestService = mock(SearchRequestService.class);

        when(authenticationContext.getUser()).thenReturn(user);
        Collection<SearchRequest> favouriteSearchRequests = Lists.newArrayList(new SearchRequest(new QueryImpl(), mock(ApplicationUser.class), "name", "desc", 1L, 1L));
        when(searchRequestService.getFavouriteFilters(user)).thenReturn(favouriteSearchRequests);

        FiltersAndProjectsResource resource = new FiltersAndProjectsResource(authenticationContext, searchRequestService, typeFactory, permissionManager, i18nHelper);

        I18nHelper helper = mock(I18nHelper.class);
        when(helper.getText("common.concepts.filters")).thenReturn("Filter");
        when(helper.getText("common.concepts.projects")).thenReturn("projects");
        when(i18nHelper.getInstance(user)).thenReturn(helper);

        when(permissionManager.getProjects(BROWSE_PROJECTS, user)).thenReturn(Lists.<Project>newArrayList(new MockProject(2l, "KEY", "project")));

        Response response = resource.getFilters(true, true);
        FiltersAndProjectsResource.OptionList ol = (FiltersAndProjectsResource.OptionList) response.getEntity();

        assertThat(ol.getOptions(), Matchers.contains(
                optionGroupMatcher(is("Filter"), optionMatcher(is("name"), is("filter-1"))),
                optionGroupMatcher(is("projects"), optionMatcher(is("project"), is("project-2")))
        ));
    }

    private Matcher<GroupWrapper> optionGroupMatcher(final Matcher<String> groupLabelMatcher,
                                                     Matcher<FiltersAndProjectsResource.Option> optionMatcher) {
        return Matchers.hasProperty("group", Matchers.allOf(
                Matchers.hasProperty("label", groupLabelMatcher),
                Matchers.hasProperty("options", Matchers.hasItem(optionMatcher))
        ));
    }

    private Matcher<FiltersAndProjectsResource.Option> optionMatcher(final Matcher<String> labelMatcher, final Matcher<String> valueMatcher) {
        return Matchers.allOf(Matchers.hasProperty("label", labelMatcher),
                Matchers.hasProperty("value", valueMatcher));
    }
}

