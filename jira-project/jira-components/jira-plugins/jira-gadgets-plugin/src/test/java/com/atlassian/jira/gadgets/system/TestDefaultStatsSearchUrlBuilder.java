package com.atlassian.jira.gadgets.system;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.statistics.ProjectStatisticsMapper;
import com.atlassian.jira.issue.statistics.StatisticsMapper;
import com.atlassian.jira.issue.statistics.TwoDimensionalStatsMap;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.project.Project;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestDefaultStatsSearchUrlBuilder {
    @Rule
    public InitMockitoMocks initMocks = new InitMockitoMocks(this);

    @Mock
    SearchService searchService;

    DefaultStatsSearchUrlBuilder builder;

    @Before
    public void setUp() {
        builder = new DefaultStatsSearchUrlBuilder(searchService);
    }

    @Test
    public void testHeaders() {
        StatisticsMapper axisMapper = mock(StatisticsMapper.class);
        SearchRequest request = new SearchRequest();

        SearchRequest result = new SearchRequest();
        when(axisMapper.getSearchUrlSuffix("foo", request)).thenReturn(result);

        when(searchService.getJqlString(result.getQuery())).thenReturn("theUrl");

        String urlForHeaderCell = builder.getSearchUrlForHeaderCell("foo", axisMapper, request);
        assertEquals("?jql=theUrl", urlForHeaderCell);
    }

    @Test
    public void testCells() {
        TwoDimensionalStatsMap twodmap = mock(TwoDimensionalStatsMap.class);
        SearchRequest request = new SearchRequest();

        SearchRequest result = new SearchRequest();
        SearchRequest result2 = new SearchRequest();

        StatisticsMapper xaxisMapper = mock(StatisticsMapper.class);
        StatisticsMapper yaxisMapper = mock(StatisticsMapper.class);

        when(twodmap.getxAxisMapper()).thenReturn(xaxisMapper);
        when(twodmap.getyAxisMapper()).thenReturn(yaxisMapper);
        when(yaxisMapper.getSearchUrlSuffix("bar", request)).thenReturn(result);
        when(xaxisMapper.getSearchUrlSuffix("foo", request)).thenReturn(result2);

        when(searchService.getJqlString(result.getQuery())).thenReturn("thisCoolUrl");

        String urlForHeaderCell = builder.getSearchUrlForCell("foo", "bar", twodmap, request);
        assertEquals("?jql=thisCoolUrl", urlForHeaderCell);
    }

    @Test
    public void testCellsNotFirst() {
        TwoDimensionalStatsMap twodmap = mock(TwoDimensionalStatsMap.class);
        SearchRequest request = new SearchRequest();

        SearchRequest result = new SearchRequest();
        SearchRequest result2 = new SearchRequest();

        StatisticsMapper xaxisMapper = mock(ProjectStatisticsMapper.class);
        StatisticsMapper yaxisMapper = mock(StatisticsMapper.class);

        when(twodmap.getxAxisMapper()).thenReturn(xaxisMapper);
        when(twodmap.getyAxisMapper()).thenReturn(yaxisMapper);
        when(yaxisMapper.getSearchUrlSuffix("bar", request)).thenReturn(result);
        Project project = mock(Project.class);
        when(xaxisMapper.getSearchUrlSuffix(project, request)).thenReturn(result2);

        when(searchService.getJqlString(result.getQuery())).thenReturn("anotherNiceUrl&encodedUrl");

        String urlForHeaderCell = builder.getSearchUrlForCell(project, "bar", twodmap, request);
        assertEquals("?jql=anotherNiceUrl%26encodedUrl", urlForHeaderCell);
    }
}
