package com.atlassian.jira.gadgets.system;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.charts.Chart;
import com.atlassian.jira.charts.ChartFactory;
import com.atlassian.jira.charts.util.ChartUtils;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.rest.api.messages.TextMessage;
import com.atlassian.jira.rest.v1.model.errors.ErrorCollection;
import com.atlassian.jira.rest.v1.model.errors.ValidationError;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.google.common.collect.ImmutableMap;
import org.jfree.chart.urls.XYURLGenerator;
import org.jfree.data.time.TimeSeriesCollection;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import static com.atlassian.jira.gadgets.system.util.MockHttpServletRequest.defaultServletRequestStub;
import static com.atlassian.jira.gadgets.system.util.ResponseMatchers.sameAs;
import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Unit Test for REST endpoint of average age chart resource, {@link com.atlassian.jira.gadgets.system.AverageAgeChartResource}
 *
 * @since v4.0
 */
public class TestAverageAgeChartResource {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    ChartUtils mockChartUtils;
    @Mock
    JiraAuthenticationContext mockAuthCtx;
    @Mock
    SearchService mockSearchService;
    @Mock
    PermissionManager mockPermissionManager;
    @Mock
    ChartFactory mockChartFactory;
    @Mock
    ApplicationProperties mockApplicationProperties;
    @Mock
    TimeZoneManager timeZoneManager;

    private final ApplicationUser mockUser = new MockApplicationUser("fred");


    @InjectMocks
    private MockSearchQueryBackedResource instance;

    @Test
    public final void validateChartTakesQueryVerbatimIfQueryContainsDash() {
        final String query = "blah-100";
        final String days = "30";
        final String periodName = "daily";
        final String expectedQuery = "blah-100";
        testValidateExpectingOkResponse(query, days, periodName, expectedQuery);
    }

    @Test
    public final void validateChartSwitchesToFilterIfQueryDoesNotContainDash() {
        final String query = "100";
        final String days = "30";
        final String periodName = "daily";
        final String expectedQuery = "filter-100";
        testValidateExpectingOkResponse(query, days, periodName, expectedQuery);
    }

    @Test
    public final void validateChartFailsWhenFilterHasDashButIsNotAFilter() {
        final String query = "bad-query";
        final String days = "10";
        final String periodName = "daily";
        final ValidationError error = new ValidationError("foo", "bar");
        testValidateExpectingSearchError(query, days, periodName, error);
    }

    @Test
    public final void validateChartFailsWithWrongPeriodName() {
        final String query = "good-query";
        final String days = "10";
        final String periodName = "rarely";
        final ValidationError error = new ValidationError("periodName", "gadget.common.invalid.period");
        testValidateExpectingError(query, days, periodName, error);
    }

    @Test
    public final void validationFailsWithWrongDaysString() {
        final String query = "good-query";
        final String days = "-1";
        final String periodName = "daily";
        final ValidationError error = new ValidationError("daysprevious", "gadget.common.negative.days");
        testValidateExpectingError(query, days, periodName, error);
    }

    @Test
    public final void validationFailsWithDaysNotANumber() {
        final String query = "good-query";
        final String days = "nan";
        final String periodName = "daily";
        final ValidationError error = new ValidationError("daysprevious", "gadget.common.days.nan");
        testValidateExpectingError(query, days, periodName, error);
    }

    @Test
    public final void generateSucceedsForEmptyData() {
        final String query = "blah-100";
        final int days = 30;
        final String periodName = "daily";
        final String expectedQuery = "blah-100";
        testGenerateExpectingOkResponse(query, days, periodName, expectedQuery, null);
    }

    @Test
    public final void generateSucceedsWithData() {
        final String query = "blah-100";
        final int days = 30;
        final String periodName = "daily";
        final String expectedQuery = "blah-100";
        final TimeChart.TimeDataRow[] data = new TimeChart.TimeDataRow[2];
        data[0] = new TimeChart.TimeDataRow("daily", 3, "issueLink", 50, 3);
        data[1] = new TimeChart.TimeDataRow("monthly", 90, "issueLink", 100, 12);
        testGenerateExpectingOkResponse(query, days, periodName, expectedQuery, data);
    }

    @Test
    public final void generateDefaultsToFilterWithQueryNotContainingDash() {
        final String query = "100";
        final int days = 30;
        final String periodName = "daily";
        final String expectedQuery = "filter-100";
        testGenerateExpectingOkResponse(query, days, periodName, expectedQuery, null);
    }

    @Test
    public final void generateFailsWithMalformedQuery() {
        final String query = "bad-query";
        final String days = "10";
        final String periodName = "daily";
        final ValidationError error = new ValidationError("foo", "bar");

        when(mockApplicationProperties.getDefaultBackedString("jira.chart.days.previous.limit." + periodName))
                .thenReturn(days);

        testGenerateChartExpectingSearchError(query, days, periodName, error);
    }

    @Test
    public final void generateFailsWithWrongPeriodName() {
        final String query = "good-query";
        final String days = "10";
        final String periodName = "rarely";
        final ValidationError error = new ValidationError("periodName", "gadget.common.invalid.period");
        testGenerateChartExpectingError(query, days, periodName, error);
    }

    @Test
    public final void generateFailsWithNegativeDays() {
        final String query = "good-query";
        final String days = "-1";
        final String periodName = "daily";
        final ValidationError error = new ValidationError("daysprevious", "gadget.common.negative.days");
        testGenerateChartExpectingError(query, days, periodName, error);
    }

    @Test
    public final void generateFailsWithNonNumericDays() {
        final String query = "good-query";
        final String days = "nan";
        final String periodName = "daily";
        final ValidationError error = new ValidationError("daysprevious", "gadget.common.days.nan");
        testGenerateChartExpectingError(query, days, periodName, error);
    }

    private void testValidateExpectingOkResponse(final String query, final String days, final String periodName,
                                                 final String expectedQuery) {
        instance.setExpectedQueryString(expectedQuery);

        when(mockApplicationProperties.getDefaultBackedString("jira.chart.days.previous.limit." + periodName))
                .thenReturn(days);

        final Response response = instance.validateChart(query, days, periodName);
        assertThat(response, sameAs(Response.ok(new TextMessage("No input validation errors found.")).build()));
    }

    private void testValidateExpectingSearchError(final String expectedQuery, final String days, final String periodName, final ValidationError expectedError) {
        instance.addErrorsToReturn(expectedError);
        testValidateExpectingError(expectedQuery, days, periodName, expectedError);
    }

    private void testValidateExpectingError(final String expectedQuery, final String days, final String periodName, final ValidationError expectedError) {
        instance.setExpectedQueryString(expectedQuery);
        final Response response = instance.validateChart(expectedQuery, days, periodName);
        assertThat(response, sameAs(Response.status(400).entity(ErrorCollection.Builder.newBuilder(expectedError).build()).build()));
    }

    private void testGenerateChartExpectingSearchError(final String expectedQuery, final String days, final String periodName, final ValidationError expectedError) {
        instance.addErrorsToReturn(expectedError);
        testGenerateChartExpectingError(expectedQuery, days, periodName, expectedError);
    }

    private void testGenerateChartExpectingError(final String expectedQuery, final String days, final String periodName, final ValidationError expectedError) {
        instance.setExpectedQueryString(expectedQuery);
        when(mockAuthCtx.getLoggedInUser()).thenReturn(mockUser);

        final Response response = instance.generateChart(defaultServletRequestStub(), expectedQuery, days, periodName, false, 400, 250, true);
        assertThat(response, sameAs(Response.status(400).entity(ErrorCollection.Builder.newBuilder(expectedError).build()).build()));
    }

    private void testGenerateExpectingOkResponse(final String query, final int days, final String periodName,
                                                 final String expectedQuery, final TimeChart.TimeDataRow[] expectedReturnData) {
        when(mockAuthCtx.getLoggedInUser()).thenReturn(mockUser);

        final TimeSeriesCollection timeSeries = new TimeSeriesCollection();
        final XYURLGenerator mockUrlGenerator = mock(XYURLGenerator.class);
        instance.setExpectedQueryString(expectedQuery);
        instance.setExpectedTimeSeriesCollection(timeSeries);
        instance.setExpectedXyurlGenerator(mockUrlGenerator);
        instance.setTimeChartDataSet(expectedReturnData);

        when(mockApplicationProperties.getDefaultBackedString("jira.chart.days.previous.limit." + periodName))
                .thenReturn(String.valueOf(days));

        when(mockChartFactory.generateAverageAgeChart(
                isA(ChartFactory.ChartContext.class), eq(days), eq(ChartFactory.PeriodName.daily))).
                thenReturn(new Chart("location", "imageMap", "imageMapName",
                        ImmutableMap.of(
                                "completeDataset", timeSeries,
                                "base64Image", "base64Image==",
                                "completeDatasetUrlGenerator", mockUrlGenerator)));

        final Response response = instance.generateChart(defaultServletRequestStub(), query, String.valueOf(days), periodName,
                expectedReturnData != null, 400, 250, true);

        assertThat(response, sameAs(Response.ok(
                new TimeChart("location", "filterTitle", "filterUrl", "imageMap",
                        "imageMapName", expectedReturnData, 400, 250, "base64Image=="))
                .build()));
    }

    static class MockSearchQueryBackedResource extends AverageAgeChartResource {
        private String expectedQueryString;
        private Collection<ValidationError> errorsToReturn = new ArrayList<ValidationError>();

        private TimeSeriesCollection expectedTimeSeriesCollection;
        private XYURLGenerator expectedXyurlGenerator;
        private TimeChart.TimeDataRow[] timeChartDataSet;

        MockSearchQueryBackedResource(final ChartUtils chartUtils, final JiraAuthenticationContext authenticationContext, final SearchService searchService, final PermissionManager permissionManager, final ChartFactory chartFactory, final ApplicationProperties applicationProperties, final TimeZoneManager timeZoneManager) {
            super(chartFactory, chartUtils, authenticationContext, permissionManager, searchService, null, applicationProperties, timeZoneManager);
        }

        public void setExpectedQueryString(final String anExpectedQueryString) {
            this.expectedQueryString = anExpectedQueryString;
        }

        public void addErrorsToReturn(final ValidationError... errorsToAdd) {
            if (errorsToAdd != null) {
                errorsToReturn.addAll(asList(errorsToAdd));
            }
        }

        public void setExpectedTimeSeriesCollection(final TimeSeriesCollection expectedTimeSeriesCollection) {
            this.expectedTimeSeriesCollection = expectedTimeSeriesCollection;
        }

        public void setExpectedXyurlGenerator(final XYURLGenerator expectedXyurlGenerator) {
            this.expectedXyurlGenerator = expectedXyurlGenerator;
        }

        public void setTimeChartDataSet(final TimeChart.TimeDataRow[] data) {
            this.timeChartDataSet = data;
        }

        @Override
        protected SearchRequest getSearchRequestAndValidate(final String queryString, final Collection<ValidationError> errors, final Map<String, Object> params) {
            assertEquals(expectedQueryString, queryString);
            assertNotNull(errors);
            assertNotNull(params);
            errors.addAll(errorsToReturn);
            return new SearchRequest();
        }

        @Override
        protected String getFilterTitle(final Map<String, Object> params) {
            return "filterTitle";
        }

        @Override
        protected String getFilterUrl(final Map<String, Object> params) {
            return "filterUrl";
        }

        @Override
        TimeChart.TimeDataRow[] generateTimeChartDataSet(final TimeSeriesCollection dataSet, final XYURLGenerator urlGenerator) {
            assertSame(expectedTimeSeriesCollection, dataSet);
            assertSame(expectedXyurlGenerator, urlGenerator);
            return timeChartDataSet;
        }
    }
}
