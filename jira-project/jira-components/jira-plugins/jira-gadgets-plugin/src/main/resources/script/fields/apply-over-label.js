define('jira/ajs/gadget/fields/apply-over-label', ['jquery'], function(jQuery) {
    return function(overLabelId){

        jQuery("#" + overLabelId).each(function (){

            var label = jQuery(this)
                .removeClass("overlabel")
                .addClass("overlabel-apply")
                .addClass("show").click(function(){
                    jQuery("#" + jQuery(this).attr("for")).focus();
                });

            var field = jQuery("#" + label.attr("for"))
                .focus(function(){
                    label.removeClass("show").hide();
                }).blur(function(){
                    if (jQuery(this).val() === ""){
                        label.addClass("show").show();
                    }
                });

            if (field.val() !== ""){
                label.removeClass("show").hide();
            }
        });
    };
});

AJS.namespace('AJS.gadget.fields.applyOverLabel', null, require('jira/ajs/gadget/fields/apply-over-label'));
