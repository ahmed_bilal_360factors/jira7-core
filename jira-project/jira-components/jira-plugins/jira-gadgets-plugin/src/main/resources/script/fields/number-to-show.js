define('jira/ajs/gadget/fields/number-to-show', [], function() {
    return function(gadget, userpref){
        return {
            userpref: userpref,
            label: gadget.getMsg("gadget.common.num.label"),
            description:gadget.getMsg("gadget.common.num.description"),
            type: "text",
            value: gadget.getPref(userpref)
        };
    };
});

AJS.namespace('AJS.gadget.fields.numberToShow', null, require('jira/ajs/gadget/fields/number-to-show'));
