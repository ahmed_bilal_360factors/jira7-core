define('jira/ajs/gadget/fields/projects-or-categories-picker', ['jquery'], function(jQuery) {
    return function(gadget, userpref, options) {
        if (!options){
            options = {};
        }
        if (!options.projects || options.projects.length === 0){
            options.projects = [{label:gadget.getMsg("gadget.common.projects.all"), value: "allprojects"}];
        }

        if(options.projects[0].value !== "allprojects"){
            options.projects = jQuery.merge([{label:gadget.getMsg("gadget.common.projects.all"), value: "allprojects"}], options.projects);
        }

        if (!options.categories || options.categories.length === 0){
            return {
                userpref: userpref,
                label: gadget.getMsg("gadget.common.projects.label"),
                description:gadget.getMsg("gadget.common.projects.description"),
                type: "multiselect",
                selected: gadget.getPref(userpref),
                options: options.projects,
                value: gadget.getPrefArray(userpref)
            };
        }

        var setOptions = function(optionList, selected){
            var selectList = jQuery("#proj_cat_picker_" + userpref + " select");
            selectList.empty();
            jQuery(optionList).each(function(){
                selectList.append(
                    jQuery("<option/>").attr("value", this.value).text(this.label)
                );
            });

            selectList.val(selected);
            var selectedOptions = selectList.val();
            if (selectedOptions){
                jQuery("#" + userpref).val(selectedOptions.join("|"));
            } else {
                jQuery("#" + userpref).val("");
            }

        };

        if(options.categories[0].value !== "catallCategories"){
            options.categories = jQuery.merge([{label:gadget.getMsg("gadget.common.categories.all"), value: "catallCategories"}], options.categories);
        }

        return {
            userpref: userpref,
            label: gadget.getMsg("gadget.common.projects.and.categories.label"),
            description:gadget.getMsg("gadget.common.projects.and.categories.description"),
            id: "proj_cat_picker_" + userpref,
            type: "callbackBuilder",
            callback: function(parentDiv){

                parentDiv
                    .append(
                        jQuery('<div class="radio"/>')
                            .append(jQuery(
                            '<input type="radio" id="catOrProj_' + userpref + '_proj" name="catOrProj_' + userpref + '" value="projects" class="radio proj-cat-option" />'
                        ))
                            .append(jQuery('<label for="catOrProj_' + userpref + '_proj" />').text(gadget.getMsg("gadget.common.projects"))))
                    .append(
                        jQuery('<div class="radio"/>')
                            .append(jQuery(
                            '<input type="radio" id="catOrProj_' + userpref + '_cat" name="catOrProj_' + userpref + '" value="categories" class="radio proj-cat-option" />'
                        ))
                            .append(jQuery('<label for="catOrProj_' + userpref + '_cat" />').text(gadget.getMsg("gadget.common.categories"))))
                    .append(jQuery('<select style="margin-top: 5px;" />').attr({multiple:"multiple"}).addClass("select multi-select"))
                    .append(jQuery("<input/>").attr({id: userpref, name: userpref, type:"hidden", value:gadget.getPref(userpref)})
                    );

                var prefs = gadget.getPrefArray(userpref);

                var radioButtons = jQuery("#proj_cat_picker_" + userpref + " input[type='radio']");
                var selectList = jQuery("#proj_cat_picker_" + userpref + " select");
                if (prefs && prefs.length > 0){
                    var first = prefs[0];
                    if (/^cat/.test(first)){
                        radioButtons.val(["categories"]);
                        setOptions(options.categories, prefs);
                    } else {
                        radioButtons.val(["projects"]);
                        setOptions(options.projects, prefs);

                    }
                } else {
                    radioButtons.val(["projects"]);
                    setOptions(options.projects);
                }

                var selectedTypes = {};
                radioButtons.click(function(e){
                    var type = jQuery(this).attr("checked", "checked").val();
                    if (type === "projects"){
                        selectedTypes.categories = selectList.val();
                        setOptions(options.projects, selectedTypes.projects);
                        gadget.resize();
                    } else {
                        selectedTypes.projects = selectList.val();
                        setOptions(options.categories, selectedTypes.categories);
                        gadget.resize();
                    }
                });
                var hiddenPref = jQuery("#" + userpref);
                selectList.change(function(){
                    var selectedOptions = selectList.val();
                    if (selectedOptions){
                        hiddenPref.val(selectedOptions.join("|"));
                    } else {
                        hiddenPref.val("");
                    }
                });

            }
        };


    };
});

AJS.namespace('AJS.gadget.fields.projectsOrCategoriesPicker', null, require('jira/ajs/gadget/fields/projects-or-categories-picker'));
