
define('jira/ajs/gadget/fields/projects-and-categories-picker', ['jquery'], function(jQuery) {
    return function(gadget, userpref, options) {
        if (!options){
            options = {};
        }
        if (!options.projects){
            options.projects = [{label:gadget.getMsg("gadget.common.projects.all"), value: "allprojects"}];
        }

        if(options.projects[0].value !== "allprojects"){
            options.projects = jQuery.merge([{label:gadget.getMsg("gadget.common.projects.all"), value: "allprojects"}], options.projects);
        }

        if (!options.categories || options.categories.length === 0){
            return {
                userpref: userpref,
                label: gadget.getMsg("gadget.common.projects.label"),
                description:gadget.getMsg("gadget.common.projects.description"),
                type: "multiselect",
                selected: gadget.getPref(userpref),
                options: options.projects,
                value: gadget.getPrefArray(userpref)
            };
        }
        var optionList = [
            {
                group :
                {
                    label: gadget.getMsg("gadget.common.projects"),
                    options: options.projects
                }
            },
            {
                group :
                {
                    label: gadget.getMsg("gadget.common.categories"),
                    options: options.categories
                }
            }
        ];

        return {
            userpref: userpref,
            label: gadget.getMsg("gadget.common.projects.and.categories.label"),
            description:gadget.getMsg("gadget.common.projects.and.categories.description"),
            type: "multiselect",
            selected: gadget.getPref(userpref),
            options: optionList,
            value: gadget.getPrefArray(userpref)
        };
    };
});

AJS.namespace('AJS.gadget.fields.projectsAndCategoriesPicker', null, require('jira/ajs/gadget/fields/projects-and-categories-picker'));
