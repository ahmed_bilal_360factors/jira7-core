define('jira/ajs/gadget/fields/period', [], function() {
    return function(gadget, userpref){
        return {
            userpref: userpref,
            label: gadget.getMsg("gadget.common.period.name.label"),
            description:gadget.getMsg("gadget.common.period.name.description"),
            type: "select",
            selected: gadget.getPref(userpref),
            options:[
                {
                    label:gadget.getMsg("gadget.common.period.hourly"),
                    value:"hourly"
                },
                {
                    label:gadget.getMsg("gadget.common.period.daily"),
                    value:"daily"
                },
                {
                    label:gadget.getMsg("gadget.common.period.weekly"),
                    value:"weekly"
                },
                {
                    label:gadget.getMsg("gadget.common.period.monthly"),
                    value:"monthly"
                },
                {
                    label:gadget.getMsg("gadget.common.period.quarterly"),
                    value:"quarterly"
                },
                {
                    label:gadget.getMsg("gadget.common.period.yearly"),
                    value:"yearly"
                }
            ]
        };
    };
});

AJS.namespace('AJS.gadget.fields.period', null, require('jira/ajs/gadget/fields/period'));
