define('jira/ajs/gadget/fields/cumulative', [], function() {
    return function(gadget, userpref){
        return {
            userpref: userpref,
            label: gadget.getMsg("gadget.common.cumulative.label"),
            description:gadget.getMsg("gadget.common.cumulative.description"),
            type: "select",
            selected: gadget.getPref(userpref),
            options:[
                {
                    label:gadget.getMsg("gadget.common.yes"),
                    value:"true"
                },
                {
                    label:gadget.getMsg("gadget.common.no"),
                    value:"false"
                }
            ]
        };
    };
});

AJS.namespace('AJS.gadget.fields.cumulative', null, require('jira/ajs/gadget/fields/cumulative'));
