define('jira/ajs/gadget/fields/project-picker', ['jquery'], function(jQuery) {
    return function(gadget, userpref, options){
        if (!gadget.title){
            gadget.title = gadget.getMsg("gadget.common.project.none.selected");
        }

        if(!jQuery.isArray(options.options)){
            options.options = [options.options];
        }
        return {
            userpref: userpref,
            label: gadget.getMsg("gadget.common.project.label"),
            description:gadget.getMsg("gadget.common.project.description"),
            type: "select",
            selected: gadget.getPref(userpref),
            options: options.options
        };
    };
});

AJS.namespace('AJS.gadget.fields.projectPicker', null, require('jira/ajs/gadget/fields/project-picker'));
