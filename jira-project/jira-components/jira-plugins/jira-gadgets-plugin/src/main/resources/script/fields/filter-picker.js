define('jira/ajs/gadget/fields/filter-picker', ['jquery'], function(jQuery) {
    return function(gadget, userpref){
        if (!gadget.projectOrFilterName){
            gadget.projectOrFilterName = gadget.getMsg("gadget.common.filter.none.selected");
        }

        return {
            userpref: userpref,
            label: gadget.getMsg("gadget.common.filter.label"),
            description:gadget.getMsg("gadget.common.filter.description"),
            id: "proj_filter_picker_" + userpref,
            type: "callbackBuilder",
            callback: function(parentDiv){
                parentDiv.append(
                    jQuery("<input/>").attr({
                        id: "filter_" + userpref + "_id",
                        type: "hidden",
                        name: userpref
                    }).val(gadget.getPref(userpref))
                ).append(
                    jQuery("<span/>").attr({id:"filter_" + userpref + "_name"}).addClass("filterpicker-value-name field-value").text(gadget.projectOrFilterName)
                );
                parentDiv.append(
                    jQuery("<div/>").attr("id", "quickfind-container").append(
                        jQuery("<label/>").addClass("overlabel").attr({
                            "for":"quickfind",
                            id: "quickfind-label"
                        }).text(gadget.getMsg("gadget.common.quick.find"))
                    ).append(
                        jQuery("<input class='text' />").attr("id", "quickfind")
                    ).append(
                        jQuery("<span/>").addClass("inline-error")
                    )
                );

                if (gadget.isLocal()){
                    parentDiv.append(
                        jQuery("<a href='#'/>").addClass("advanced-search").attr({
                            id: "filter_" + userpref + "_advance",
                            title: gadget.getMsg("gadget.common.filterid.edit")
                        }).text(gadget.getMsg("gadget.common.advanced.search")).click(function(e){
                            var url = jQuery.ajaxSettings.baseUrl + "/secure/FilterPickerPopup.jspa?showProjects=false&field=" + userpref;
                            var windowVal = "filter_" + userpref + "_window";
                            var prefs = "width=800, height=500, resizable, scrollbars=yes";

                            var newWindow = window.open(url, windowVal, prefs);
                            newWindow.focus();
                            e.preventDefault();
                        })
                    );
                }


                AJS.gadget.fields.applyOverLabel("quickfind-label");
                AJS.gadget.fields.autocomplete.Filters({
                    fieldID: "quickfind",
                    ajaxData: {},
                    baseUrl: jQuery.ajaxSettings.baseUrl,
                    relatedId: "filter_" + userpref + "_id",
                    relatedDisplayId: "filter_" + userpref + "_name",
                    filtersLabel: gadget.getMsg("gadget.common.filters"),
                    gadget: gadget
                });
            }
        };
    };
});

AJS.namespace('AJS.gadget.fields.filterPicker', null, require('jira/ajs/gadget/fields/filter-picker'));
