define('jira/ajs/gadget/fields/autocomplete/projects-and-filters', ['jira/autocomplete/rest-autocomplete', 'jquery', 'jira/util/objects'], function(RESTAutoComplete, jQuery, objects) {
    /**
     * Project And Filter autocomplete picker
     */
    return function(options) {

        /** @lends # */
        var that = objects.begetObject(RESTAutoComplete);

        that.getAjaxParams = function(){
            return {
                url: options.baseUrl + "/rest/gadget/1.0/pickers/projectsAndFilters",
                data: {
                    fieldName: options.fieldID
                },
                dataType: "json",
                type: "GET",
                global:false,
                error: function(XMLHttpRequest, textStatus, errorThrown){
                    if (XMLHttpRequest.data){
                        var errorCollection = XMLHttpRequest.data.errors;
                        if (errorCollection){
                            jQuery(errorCollection).each(function(){
                                var parent = jQuery("#" + this.field).parent();
                                parent.find("span.inline-error").text(options.gadget.getMsg(this.error));
                            });
                        }
                    }
                }
            };
        };

        that.completeField = function(value) {
            jQuery("#" + options.relatedId).val(value.id);
            jQuery("#" + options.relatedDisplayId).addClass("success").text(value.name);
            jQuery("#" + options.fieldID).val("");

        };

        /**
         * Create html elements from JSON object
         * @param {Object} response - JSON object
         * @returns {Array} Multidimensional array, one column being the html element and the other being its
         * corresponding complete value.
         */
        that.renderSuggestions = function(response) {
            var resultsContainer;
            var suggestionNodes = [];

            this.responseContainer.addClass("aui-list");


            // remove previous results
            this.clearResponseContainer();

            var parent = jQuery("#" + options.fieldID).parent();
            parent.find("span.inline-error").text("");

            if (response && response.projects && response.projects.length > 0) {


                jQuery("<h5/>").text(options.projectsLabel).appendTo(this.responseContainer);
                resultsContainer = jQuery("<ul class='aui-list-section aui-first aui-last'/>").appendTo(this.responseContainer);

                jQuery(response.projects).each(function() {
                    if (!this.isModified){
                        this.isModified = true;
                        this.id = "project-" + this.id;
                    }
                    // add html element and corresponding complete value  to sugestionNodes Array
                    suggestionNodes.push([jQuery("<li class='aui-list-item'/>").attr("id", this.id + "_" + options.fieldID + "_listitem").append(
                        jQuery("<a href='#' class='aui-list-item-link aui-indented-link' />")
                            .click(function (e) {
                                e.preventDefault();
                            })
                            .html(this.html)
                    ).appendTo(resultsContainer), this]);

                });
            }

            if (response && response.filters && response.filters.length > 0) {


                if (resultsContainer) {
                    resultsContainer.removeClass("aui-last");
                }

                jQuery("<h5/>").text(options.filtersLabel).appendTo(this.responseContainer);
                resultsContainer = jQuery("<ul class='aui-list-section aui-first aui-last' />").appendTo(this.responseContainer);

                jQuery(response.filters).each(function() {
                    if (!this.isModified){
                        this.isModified = true;
                        this.id = "filter-" + this.id;
                    }

                    var item = jQuery("<li class='aui-list-item'/>").attr(
                        {
                            id: this.id + "_" + options.fieldID + "_listitem"
                        }
                    );


                    var link = jQuery("<a href='#' class='aui-list-item-link  aui-indented-link' />").append(
                        jQuery("<span/>").addClass("filter-name").html(this.nameHtml)
                        )
                        .click(function (e) {
                            e.preventDefault();
                        })
                        .appendTo(item);

                    if (this.descHtml){
                        link.append(
                            jQuery("<span/>").addClass("filter-desc").html(" - " + this.descHtml)
                        );
                    }

                    item.attr("title", link.text());

                    // add html element and corresponding complete value  to sugestionNodes Array
                    suggestionNodes.push([item.appendTo(resultsContainer), this]);

                });
            }

            if (suggestionNodes.length > 0) {
                this.responseContainer.removeClass("no-results");
                that.addSuggestionControls(suggestionNodes);
            } else {
                this.responseContainer.addClass("no-results");
            }

            return suggestionNodes;
        };

        options.maxHeight = 200;

        // Use autocomplete only once the field has atleast 2 characters
        options.minQueryLength = 1;

        // wait 1/4 of after someone starts typing before going to server
        options.queryDelay = 0.25;

        that.init(options);

        return that;

    };
});

AJS.namespace('AJS.gadget.fields.autocomplete.ProjectsAndFilters', null, require('jira/ajs/gadget/fields/autocomplete/projects-and-filters'));
