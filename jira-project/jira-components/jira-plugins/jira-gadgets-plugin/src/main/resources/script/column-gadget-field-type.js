define('jira/ajs/gadget/column-gadget-field-type', ['require'], function(require) {
    var $ = require('jquery');
    var FilterResultsColumnData = require('jira/ajs/gadget/filter-results-column-data');
    var ColumnPicker = require('jira/ajs/gadget/column-picker');

    function columnGadgetFieldType (gadget, name, availableColumns)
    {
        var dataModel = new FilterResultsColumnData (availableColumns, gadget.getPref(name));
        var columnPicker = new ColumnPicker (gadget, name, dataModel);

        $(document).bind("column-data-item-selected", function () {
            gadget.resize();
        });
        $(document).bind("column-data-item-unselected", function () {
            gadget.resize();
        });

        return {
            id: "columnNames",
            label: gadget.getMsg("gadget.issuetable.common.fields.to.display"),
            type: "callbackBuilder",
            callback: function(parentDiv) {
                columnPicker.initalize(parentDiv);
            }
        };
    }

    return columnGadgetFieldType;
});

AJS.namespace("columnGadgetFieldType", null, require("jira/ajs/gadget/column-gadget-field-type"));
