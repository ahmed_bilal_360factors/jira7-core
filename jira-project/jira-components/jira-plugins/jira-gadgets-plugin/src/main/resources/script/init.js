/**
 * @note we have to bind our event handling immediately and synchronously,
 * because otherwise we can miss the event that ActivityStream fires, which
 * causes {@link https://jira.atlassian.com/browse/JRA-62593}.
 *
 * @note this can be converted to an asynchronous require() block once
 * the {@link external:"ActivityStreams"} global object is converted to
 * an async-friendly module and API.
 *
 * @see {@link https://bitbucket.org/atlassian/atlassian-streams/src/e10ad0b53579dd3e2b6af7aed7bc039a72f3f4b1/aggregator-plugin/src/main/resources/js/activity-stream.gadget.js?at=master&fileviewer=file-view-default#activity-stream.gadget.js-48,47}
 */
(function() {
    'use strict';

    var formatter = require('jira/util/formatter');
    var describeBrowser = require('jira/ajs/browser/describe-browser');
    var moment = require('jira/moment');
    var $ = require('jquery');

    $(function() {
        describeBrowser();
    });

    $(document).bind('preferencesLoaded.streams', function() {

        moment.lang("jira", {
            longDateFormat: {
                LT: JIRA.translateSimpleDateFormat(ActivityStreams.getTimeFormat()),
                L: JIRA.translateSimpleDateFormat(ActivityStreams.getDateFormat()),
                LL: JIRA.translateSimpleDateFormat(ActivityStreams.getDateFormat()),
                LLL: JIRA.translateSimpleDateFormat(ActivityStreams.getDateTimeFormat())
            },
            calendar: {
                sameDay:  ActivityStreams.getDateRelativize() ? formatter.I18n.getText("common.date.relative.day.same", "[", "]", "LT") : "LLL",
                nextDay:  ActivityStreams.getDateRelativize() ? formatter.I18n.getText("common.date.relative.day.next", "[", "]", "LT") : "LLL",
                lastDay:  ActivityStreams.getDateRelativize() ? formatter.I18n.getText("common.date.relative.day.last", "[", "]", "LT") : "LLL",
                nextWeek: "LLL",
                lastWeek: "LLL",
                sameElse: "LLL"
            }
        });
    });
})();
