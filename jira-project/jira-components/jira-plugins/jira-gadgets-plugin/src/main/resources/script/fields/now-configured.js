define('jira/ajs/gadget/fields/now-configured', [], function() {
    return function(){
        return {
            userpref: "isConfigured",
            type: "hidden",
            value: "true"
        };
    };
});

AJS.namespace('AJS.gadget.fields.nowConfigured', null, require('jira/ajs/gadget/fields/now-configured'));
