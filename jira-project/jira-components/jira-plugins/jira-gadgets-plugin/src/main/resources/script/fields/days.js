define('jira/ajs/gadget/fields/days', [], function() {
    return function(gadget, userpref, optMsgKeys){
        return {
            userpref: userpref,
            label: gadget.getMsg(optMsgKeys && optMsgKeys.label ? optMsgKeys.label : "gadget.common.days.label"),
            description: gadget.getMsg(optMsgKeys && optMsgKeys.description ? optMsgKeys.description : "gadget.common.days.description"),
            type: "text",
            value: gadget.getPref(userpref)
        };
    };
});

AJS.namespace('AJS.gadget.fields.days', null, require('jira/ajs/gadget/fields/days'));
