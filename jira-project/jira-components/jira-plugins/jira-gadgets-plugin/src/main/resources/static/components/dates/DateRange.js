/**
 * @name DateRange
 * @global
 *
 * Helper functions used for dealing with date ranges.
 */
define('jira-dashboard-items/components/dates/daterange', ['jira/util/formatter', 'jira/moment'], function(formatter, moment) {

    return {
        /**
         * Given two dates generate a human readable format.
         * @param {number} startDate
         * @param {number} endDate
         * @returns {string}
         */
        rangeToText: function(startDate, endDate) {
            //Default day if same date
            if (startDate === endDate) {
                return moment(startDate).format("Do MMM YYYY");
            }

            startDate = moment(startDate);
            endDate = moment(endDate);

            var differentYear = startDate.year() !== endDate.year();
            if (differentYear) {
                var startString = startDate.format("Do MMM YYYY");
                var endString = endDate.format("Do MMM YYYY");
                return formatter.I18n.getText('popups.daterange.datebetween', startString, endString);
            }

            var differentMonth = startDate.month() !== endDate.month();
            if (differentMonth) {
                startString = startDate.format("Do MMM");
                endString = endDate.format("Do MMM YYYY");
                return formatter.I18n.getText('popups.daterange.datebetween', startString, endString);
            }

            var differentDay = startDate.dayOfYear() !== endDate.dayOfYear();
            if (differentDay) {
                startString = startDate.format("Do");
                endString = endDate.format("Do MMM YYYY");
                return formatter.I18n.getText('popups.daterange.datebetween', startString, endString);
            }

            //Doesn't care about seconds for simplicity.
            var fullDay = ((startDate.get('hour') === 0 && startDate.get('minute') === 0) &&
                           (endDate.get('hour') === 23 && endDate.get('minute') === 59));

            if (fullDay) {
                return moment(startDate).format("Do MMM YYYY");
            }

            startString = moment(startDate).format("HH:mm");
            endString = moment(endDate).format("HH:mm Do MMM YYYY");
            return formatter.I18n.getText('popups.daterange.datebetween', startString, endString);

        }
    };
});
