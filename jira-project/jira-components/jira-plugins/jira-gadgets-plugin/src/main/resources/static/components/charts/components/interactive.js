/**
 * Note: Taken and slightly modified from JIRA Agile control chart code.
 * A component mixin that allows a g svg element to be interacted with by the mouse.
 *
 * @module Interactive
 */
define('jira-dashboard-items/components/charts/components/interactive', [
    'jquery',
    'jira-dashboard-items/lib/c3',
    'underscore'
], function(
    $,
    c3,
    _
) {
    return function() {

        var component = c3.component();

        var cachedOffset;

        /**
         * Update the cached offset at most once every 200ms.
         */
        var updateCachedOffset = _.throttle(function() {
            cachedOffset = $(component.region().selection().node()).offset();
        }, 200);

        /**
         * Test whether or not a mouse event occurred within the bounds
         *
         * @param e
         * @returns {boolean}
         */
        function isWithinBounds(e) {
            var coordinates = getRelativeMouseCoordinates(e);
            var x = coordinates[0];
            var y = coordinates[1];

            component.mouseCoordinates([x, y]);

            return (x > 0 && y > 0) &&
                        (x <= component.region().selection().node().getBBox().width &&
                         y <= component.region().selection().node().getBBox().height);
        }

        /**
         * Calculate the coordinates at which a mouse event occured, relative to the component's selection.
         *
         * @param {jQuery.event} e the mouse event
         * @returns {[number, number]}
         */
        function getRelativeMouseCoordinates(e) {

            updateCachedOffset();

            var x = (e.pageX - cachedOffset.left);
            var y = (e.pageY - cachedOffset.top);

            return [x, y];
        }

        /**
         * Initialise the component:
         *
         *  - Binds to the mousemove event of the domContext to determine when the mouseout/mouseover events occur
         *  - Prevent native behaviour of dragstart event to fix Firefox glitches
         */
        function initialise() {
            var inDom = false;
            component.domContext()
                .on('mousemove', function(e) {
                    var isMouseWithinBounds = isWithinBounds(e);
                    if (isMouseWithinBounds) {
                        if (!inDom) {
                            component.mouseenter(e);
                            inDom = true;
                        }
                        component.mousemove(e);
                    } else {
                        if (inDom) {
                            component.mouseout(e);
                            inDom = false;
                        }
                    }
                })
                .on('mouseleave', function(e) {
                    component.mouseout(e);
                });
        }

        return component
            .extend(c3.withDimensions())
            .extend(_.once(initialise))
            .extend({
                mouseCoordinates: c3.prop(),
                domContext: c3.prop(),
                region: c3.prop(),
                mousedown: c3.event(),
                mouseup: c3.event(),
                mousemove: c3.event(),
                mouseover: c3.event(),
                mouseout: c3.event(),
                mouseenter: c3.event(),
                mouseleave: c3.event()
            });
    };
});
