AJS.test.require([
    'com.atlassian.jira.gadgets:common-test-resources',
    'com.atlassian.jira.gadgets:bubble-chart'
], function () {
        'use strict';

        var $ = require('jquery');
        var Backbone = require('backbone');
        var BubbleChart = require('jira-dashboard-items/bubble-chart');

        var COLORS = ['red', 'blue'];

        var DATA = [
            { bubbleDomain: 1.3, bubbleRange: 1.4, bubbleRadius: 2.3, bubbleColorValue: 4.5, key: 'txt1' },
            { bubbleDomain: 3.1, bubbleRange: 6.9, bubbleRadius: 4.3, bubbleColorValue: 2.7, key: 'txt2' },
            { bubbleDomain: 3.1, bubbleRange: 0, bubbleRadius: 4.3, bubbleColorValue: 3.1, key: 'txt3' },
            { bubbleDomain: 0, bubbleRange: 6.9, bubbleRadius: 1.3, bubbleColorValue: 7.4, key: 'txt4' },
            { bubbleDomain: 123.1, bubbleRange: 111, bubbleRadius: 4.3, bubbleColorValue: 0.7, key: 'txt5' },
            { bubbleDomain: 0, bubbleRange: 6.9, bubbleRadius: 2.9, bubbleColorValue: 5.2, key: 'txt6' }
        ];

        var EXCESSIVE_DATA = [
            { bubbleDomain: 1.3, bubbleRange: 1.4, bubbleRadius: 2.3, bubbleColorValue: 4.5, key: 'txt1' },
            { bubbleDomain: 3.1, bubbleRange: 6.9, bubbleRadius: 4.3, bubbleColorValue: 2.7, key: 'txt2' },
            { bubbleDomain: 3.1, bubbleRange: 0, bubbleRadius: 4.3, bubbleColorValue: 3.1, key: 'txt3' },
            { bubbleDomain: 0, bubbleRange: 6.9, bubbleRadius: 1.3, bubbleColorValue: 7.4, key: 'txt4' },
            { bubbleDomain: 123.1, bubbleRange: 111, bubbleRadius: 4.3, bubbleColorValue: 0.7, key: 'txt5' },
            { bubbleDomain: 0, bubbleRange: 6.9, bubbleRadius: 2.9, bubbleColorValue: 5.2, key: 'txt6' }
        ];

        var aDataItem = EXCESSIVE_DATA[2];
        for (var i = 0; i < 300; i++) {
            EXCESSIVE_DATA.push(aDataItem);
        }

        var BUBBLE_INDEX_HIGHLIGHT = 7;

        module('jira-dashboard-items/bubble-chart', {
            setup: function() {
                this.$el = $('#qunit-fixture');

                this.model = new Backbone.Model({
                    colors: COLORS,
                    bubbleType: 'votes',
                    exceedsIssueLimit: 'false',
                    colorDomain: [1, 2]
                });

                this.chart = new BubbleChart({
                    model: this.model,
                    el: this.$el.get(0),
                    setTitle: function() {},
                    id: 100,
                    resize: function() {},
                    hideLoadingBar: function() {}
                });
                this.chart.render();
            },

            assertLegendIsInformedOfHighlightChangeForDesiredColorBucket: function(colorBucket) {
                this.spy(this.chart.discreteLegendModel, 'set');

                this.model.set('highlightedCircle', {
                    colorBucket: colorBucket
                });

                sinon.assert.calledWith(this.chart.discreteLegendModel.set, 'highlightedSegment', colorBucket);
            }
        });

        test('Should set the gadget title when given a selected filter', function() {
            this.spy(this.chart, 'setTitle');

            this.model.set('filterTitle', 'sometitle');

            sinon.assert.calledWith(this.chart.setTitle, 'bubble-chart.title.filterorproject');
        });

        test('Should tell the legend to highight a segment when a bubble has a mouseover', function() {
            this.assertLegendIsInformedOfHighlightChangeForDesiredColorBucket(BUBBLE_INDEX_HIGHLIGHT);
        });

        test('Should tell the legend to unhighlight a segment when the bubble loses its mouse activity', function() {
            this.assertLegendIsInformedOfHighlightChangeForDesiredColorBucket(undefined);
        });

        test("Should render the bubble chart and legend when the model's data is set", function() {
            this.chart.discreteLegendModel.set('data', [1, 2, 3]);
            this.spy(this.chart, '_createBubbleChart');
            this.spy(this.chart, '_createLegend');

            this.model.set('data', DATA);

            sinon.assert.calledWith(this.chart._createBubbleChart, sinon.match.any, DATA, COLORS);
            sinon.assert.calledOnce(this.chart._createLegend);
        });

        test('Should display an info message when rendering the dashboard item and there is no data in this filter', function() {
            this.model.set('data', []);
            equal(this.$el.find('.aui-message-info').length, 1);
        });

        test('Should display a warning message when rendering the dashboard item and too many issues are returned', function() {
            // Don't try to render the rest of the chart
            var chart = { render: function() {} };
            var legend = { render: function() {} };
            this.stub(this.chart, '_createBubbleChart').returns(chart);
            this.stub(this.chart, '_createLegend').returns(legend);

            this.model.set('exceedsIssueLimit', true);
            this.model.set('bubbleType', 'votes');
            this.model.set('data', EXCESSIVE_DATA);

            equal(this.$el.find('.aui-message-warning').length, 1);
        });
});
