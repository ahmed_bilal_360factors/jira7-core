AJS.test.require([
    "com.atlassian.jira.gadgets:common-test-resources",
    "com.atlassian.jira.gadgets:createdvsresolved-dashboard-item-resources"
], function() {
    var $ = require('jquery');
    var CreatedVsResolvedConfigView = require('jira-dashboard-items/createdvsresolved-config-view');

    function assignProjectFilter($form, name, type, id) {
        $form.find('input[name=name]').val(name);
        $form.find('input[name=type]').val(type);
        $form.find('input[name=id]').val(id);
    }


    var GADGET_ID = '10000';

    module('jira-dashboard-items/createdvsresolved-config-view', {
        setup: function () {
            this.$el = $("<div/>");

            this.dashboardItem = new CreatedVsResolvedConfigView({
                el: this.$el,
                id: GADGET_ID,
                projectFilterDelay: function(callback) { callback(); }
            });


            $("#qunit-fixture").append(this.$el);
            this.server = sinon.fakeServer.create();
        },

        teardown: function () {
            this.server.restore();
            this.$el.remove();
        }
    });

    test("Project/Filter autocomplete selections should be applied into the form", function() {
        this.dashboardItem.render();

        var suggestions = {
            projects: [
                {
                    html: '<b>testProject</b>',
                    id: 10000,
                    key: 'xyz',
                    name: 'test'
                }
            ]
        };

        var projectFilterAutocomplete = $("#" + GADGET_ID + "-project-filter-picker", this.$el);
        projectFilterAutocomplete.val("a");
        projectFilterAutocomplete.trigger($.Event("keyup"));
        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, JSON.stringify(suggestions));

        $(".suggestions .aui-list-item").first().click();

        equal(this.$el.find("input[name=name]").val(), 'test', 'Should have used the correct name');
        equal(this.$el.find("input[name=id]").val(), 10000);
        equal(this.$el.find("input[name=type]").val(), "project");
        equal(projectFilterAutocomplete.val(), "");
    });


    asyncTest("Submitting the form sends a request with the correct values", function() {
        this.dashboardItem.render();


        var assignedValues = {
            name: 'test',
            id: '100',
            type: 'filter',
            periodName: 'weekly',
            range: 7,
            operation: 'count',
            showUnresolvedTrend: true,
            versionLabel: 'major',
            refresh: true
        };


        assignProjectFilter(this.$el, assignedValues.name, assignedValues.type, assignedValues.id);


        this.$el.find("select[name=periodName]").val(assignedValues.periodName);
        this.$el.find("input[name=range]").val(assignedValues.range);
        this.$el.find("select[name=operation]").val(assignedValues.operation);
        this.$el.find("select[name=showUnresolvedTrend]").val('' + assignedValues.showUnresolvedTrend);
        this.$el.find("select[name=versionLabel]").val(assignedValues.versionLabel);
        this.$el.find("input[refresh-interval]").attr('checked', assignedValues.refresh);

        this.dashboardItem.bind('submit', function(fields) {

            equal(fields.name, assignedValues.name, "Should have gotten the name from the form");
            equal(fields.type, assignedValues.type, "Should have gotten the type from the form");
            equal(fields.id, assignedValues.id, "Should have gotten the id from the form");
            equal(fields.operation, assignedValues.operation, "Should have gotten the operation type from the form");
            equal(fields.showUnresolvedTrend, assignedValues.showUnresolvedTrend, "Should have gotten the unresolved type from the form");
            equal(fields.versionLabel, assignedValues.versionLabel, "Should have gotten whether to display version from the form");
            equal(fields.refresh, assignedValues.refresh, "Should have assigned the refresh value of 15");

            QUnit.start();
        });

        $("form", this.$el).submit();
    });

    test("Current preferences populates the form", function() {
        var preferences = {
            name: 'myfilter',
            type: 'filter',
            id: '10000',
            periodName: 'monthly',
            range: 12,
            operation: 'cumulative',
            showUnresolvedTrend: true,
            versionLabel: 'major',
            refresh: true
        };

        this.dashboardItem.render(preferences);

        this.fields = this.dashboardItem.getFields();

        equal(this.fields.name, preferences.name, 'Should have included the name');
        equal(this.fields.type, preferences.type, 'Should have included the type');
        equal(this.fields.id, preferences.id, 'Should have included the id');
        equal(this.fields.periodName, preferences.periodName, 'Should have included the period');
        equal(this.fields.range, preferences.range, 'Should have kept the days previously');
        equal(this.fields.operation, preferences.operation, 'Should have included the operation');
        equal(this.fields.showUnresolvedTrend, preferences.showUnresolvedTrend, 'Should have included the unresolved value');
        equal(this.fields.versionLabel, preferences.versionLabel, 'Should have included the versionLabel');
        equal(this.fields.refresh, preferences.refresh, 'Should have included the refresh value');
    });

    test("No project/filter causes an error", function() {
        this.dashboardItem.render({});

        $("form", this.$el).submit();

        equal(this.$el.find(".projectOrFilter-error").text(), "gadget.common.required.query", "Error message for missing project/filter should be visible");
    });

    test("Blank days previously causes an error", function() {
        this.dashboardItem.render({});

        $("form", this.$el).submit();

        equal(this.$el.find("input[name=range] ~ .error").text(), "portlet.chart.field.date.range.required", "Error message for days should be visible");
    });

    test("Non number inputted into days", function() {
        this.dashboardItem.render({});

        var daysInput = this.$el.find("input[name=range]");
        daysInput.val('asds');
        $("form", this.$el).submit();

        equal(daysInput.siblings(".error").text(), "portlet.chart.field.range.nan", "Inputting non number should show an error");
    });

    test("Negative number inputted into days", function() {
        this.dashboardItem.render({});

        var daysInput = this.$el.find("input[name=range]");
        daysInput.val('-5');
        $("form", this.$el).submit();

        equal(daysInput.siblings(".error").text(), "portlet.chart.field.range.negative", "Inputting a negative number should show an error");
    });


    test("Days overlimit for period", function() {

        var DAYS_LIMIT = {
            hourly: 10,
            daily: 300,
            weekly: 250,
            monthly: 625,
            quarterly: 7500,
            yearly: 100
        };

        this.dashboardItem.render({});

        var periodInput = this.$el.find("input[name=periodName]");
        var daysInput = this.$el.find("input[name=range]");

        periodInput.val('hourly');
        daysInput.val(DAYS_LIMIT.hourly + 1);
        $("form", this.$el).submit();
        equal(daysInput.siblings(".error").text(), "portlet.chart.field.range.maximum", "Inputting a negative number should show an error");
        daysInput.siblings(".error").remove();

        periodInput.val('daily');
        daysInput.val(DAYS_LIMIT.daily + 1);
        $("form", this.$el).submit();
        equal(daysInput.siblings(".error").text(), "portlet.chart.field.range.maximum", "Inputting a negative number should show an error");
        daysInput.siblings(".error").remove();

        periodInput.val('weekly');
        daysInput.val(DAYS_LIMIT.weekly + 1);
        $("form", this.$el).submit();
        equal(daysInput.siblings(".error").text(), "portlet.chart.field.range.maximum", "Inputting a negative number should show an error");
        daysInput.siblings(".error").remove();

        periodInput.val('monthly');
        daysInput.val(DAYS_LIMIT.monthly + 1);
        $("form", this.$el).submit();
        equal(daysInput.siblings(".error").text(), "portlet.chart.field.range.maximum", "Inputting a negative number should show an error");
        daysInput.siblings(".error").remove();

        periodInput.val('quarterly');
        daysInput.val(DAYS_LIMIT.quarterly + 1);
        $("form", this.$el).submit();
        equal(daysInput.siblings(".error").text(), "portlet.chart.field.range.maximum", "Inputting a negative number should show an error");
        daysInput.siblings(".error").remove();

        periodInput.val('yearly');
        daysInput.val(DAYS_LIMIT.yearly + 1);
        $("form", this.$el).submit();
        equal(daysInput.siblings(".error").text(), "portlet.chart.field.range.maximum", "Inputting a negative number should show an error");
        daysInput.siblings(".error").remove();
    });

    test("If configured show cancel button", function() {
        this.dashboardItem.render({
            isConfigured: true
        });

        ok($(".cancel", this.$el).is(":visible"), "Should show cancel button");
    });

    test("If not configured do not show cancel button", function() {
        this.dashboardItem.render({
            isConfigured: false
        });

        ok(!$(".cancel", this.$el).is(":visible"), "Should show cancel button");
    });
});
