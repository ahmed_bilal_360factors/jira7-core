AJS.test.require("com.atlassian.jira.gadgets:linecomparison-graph", function() {

    var d3 = require('jira-dashboard-items/lib/d3');
    var $ = require('jquery');
    var _ = require('underscore');
    var LineComparisonGraph = require('jira-dashboard-items/components/charts/components/linecomparison-graph');
    var DualPlottable = require('jira-dashboard-items/components/charts/components/dualplottable');


    module('Components.Charts.Components.LineComparisonGraph', {
        setup: function() {
            this.$el = $("<svg height='100'/>");
            $("body").append(this.$el);
        },

        teardown: function() {
            this.$el.remove();
        }
    });

    function rangeForData(data) {
        var min = _.min(data, function(d) {
            return d[0];
        });

        var max = _.max(data, function(d) {
            return d[0];
        });

        return max[0] - min[0];
    }

    test("Generated two lines and two area plots", function() {
        var FIRST_LINE_NAME = 'firstSection';
        var SECOND_LINE_NAME = 'secondSection';

        var data = [
            [0, [5, 5]],
            [1, [7, 3]],
            [2, [5, 5]],
            [3, [3, 7]],
            [4, [5, 5]],
            [5, [7, 3]],
            [6, [5, 5]]
        ];

        var GRAPH_X_INCREASOR = 10;

        this.$el.attr('width', rangeForData(data) * GRAPH_X_INCREASOR);

        var plot = LineComparisonGraph({
            firstName: FIRST_LINE_NAME,
            secondName: SECOND_LINE_NAME
        });

        plot.data(data);

        plot.yDomain(DualPlottable().yDomain());

        plot(d3.select(this.$el.get(0)));

        var firstSection = this.$el.find("." + FIRST_LINE_NAME);
        var secondSection = this.$el.find("." + SECOND_LINE_NAME);

        ok(firstSection.length > 0, "Should have found the first section");
        ok(secondSection.length > 0, "Should have found the second section");

        ok(firstSection.find(".line").length > 0, "Should have find a line within the first section");
        ok(firstSection.find(".area").length > 0, "Should have find an area within the first section");


        ok(secondSection.find(".line").length > 0, "Should have find a line within the second section");
        ok(secondSection.find(".area").length > 0, "Should have find an area within the second section");

    });

    test("Lines follow correct path", function() {
        var FIRST_LINE_NAME = 'firstSection';
        var SECOND_LINE_NAME = 'secondSection';

        var data = [
            [0, [5, 5]],
            [1, [10, 0]],
            [2, [5, 5]],
            [3, [0, 10]],
            [4, [5, 5]],
            [5, [10, 0]],
            [6, [5, 5]]
        ];

        var Y_MAX = 10;
        var GRAPH_Y_INCREASOR = 10;
        var HEIGHT = Y_MAX * GRAPH_Y_INCREASOR;
        var GRAPH_X_INCREASOR = 10;

        this.$el.attr('width', rangeForData(data) * GRAPH_X_INCREASOR);
        this.$el.attr('height', HEIGHT);

        var plot = LineComparisonGraph({
            firstName: FIRST_LINE_NAME,
            secondName: SECOND_LINE_NAME
        });

        plot.data(data);

        plot.yDomain(DualPlottable().yDomain());

        plot(d3.select(this.$el.get(0)));

        var firstLine = this.$el.find("." + FIRST_LINE_NAME + " .line path");
        var path = firstLine.attr("d");
        var points = path.split(/[a-zA-Z]/);

        var xPointY = {};

        points.forEach(function(point) {
            var coordinates = point.split(",");
            if (coordinates.length < 2) {
                return;
            }
            var x = parseFloat(coordinates[0]);
            var y = parseFloat(coordinates[1]);

            //fixes problems where d3.svg.area makes it 49.9999 not 50.
            if (("" + x).match(/\d+\.9999/)) {
                x = Math.round(x);
            }

            xPointY[x] = HEIGHT - y;
        });

        data.forEach(function(point) {
            var x = point[0];
            var scaledX = x * GRAPH_X_INCREASOR;
            var expectedScaledY = data[x][1][0] * GRAPH_Y_INCREASOR;
            equal(xPointY[scaledX], expectedScaledY, "The points on the path at position " + x + " does not match the expected number");
        });




        var secondLine = this.$el.find("." + SECOND_LINE_NAME + " .line path");
        path = secondLine.attr("d");
        points = path.split(/[a-zA-Z]/);

        xPointY = {};

        points.forEach(function(point) {
            var coordinates = point.split(",");
            if (coordinates.length < 2) {
                return;
            }
            var x = parseFloat(coordinates[0]);
            var y = parseFloat(coordinates[1]);

            //fixes problems where d3.svg.area makes it 49.9999 not 50.
            if (("" + x).match(/\d+\.9999/)) {
                x = Math.round(x);
            }

            xPointY[x] = HEIGHT - y;
        });

        data.forEach(function(point) {
            var x = point[0];
            var scaledX = x * GRAPH_X_INCREASOR;
            var expectedScaledY = data[x][1][1] * GRAPH_Y_INCREASOR;
            equal(xPointY[scaledX], expectedScaledY, "The points on the path at position " + x + " does not match the expected number");
        });
    });

    //No test for area, assumes that AreaBetweenPlotOneWay-test tests this.
});
