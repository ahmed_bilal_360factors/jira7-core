AJS.test.require('com.atlassian.jira.gadgets:overlap-remover', function() {

    var OverlapRemover = require('jira-dashboard-items/components/overlap-remover/overlap-remover');


    module('Components.OverlapRemover');

    test('Test no overlaps results in all elements remaining', function () {
        var elements = [0, 1, 2];
        var remainingIndices = OverlapRemover.reduce(elements, function() {
            return false;
        });

        equal(remainingIndices.length, elements.length, 'Returned indices should be the same length as the elements');
        deepEqual(remainingIndices, elements, 'All returned indices should be kept');
    });

    test('Reduces to most amount of elements with no overlaps', function() {
        var startingElements = [
            [0, 5],
            [4, 9],
            [8, 13],
            [12, 17],
            [16, 21]
        ];

        var expectedElements = [
            startingElements[0],
            startingElements[2],
            startingElements[4]
        ];

        var remainingIndices = OverlapRemover.reduce(startingElements, function(a, b) {
            return a[1] > b[0];
        });

        deepEqual(remainingIndices, expectedElements, 'Should have reduced to equal distances');
    });


    test('Further reduces to most amount of elements with no overlaps', function() {
        var startingElements = [
            [0, 3],
            [1, 4],
            [2, 5],
            [3, 6],
            [4, 7],
            [5, 8],
            [6, 9],
        ];

        var expectedElements = [
            startingElements[0],
            startingElements[3],
            startingElements[6]
        ];

        var remainingIndices = OverlapRemover.reduce(startingElements, function(a, b) {
            return a[1] > b[0];
        });

        deepEqual(remainingIndices, expectedElements, 'Should have reduced to equal distances');
    });
});
