AJS.test.require([
    'com.atlassian.jira.gadgets:common-test-resources',
    'com.atlassian.jira.gadgets:labels-dashboard-item-resources'
], function () {
    require([
        'jira-dashboard-items/labels',
        'jquery'
    ], function (
        LabelsDashboardItem,
        $
    ) {
        'use strict';

        var STUB_FIELDS = {
            labelFields: [
                {value: 'value1', label: 'label1'},
                {value: 'value2', label: 'label2'},
                {value: 'value3', label: 'label3'}
            ]
        };

        var STUB_PROJECTS = {
            label: 'somelabel',
            options: [
                {label: 'projlabel1', value: 'projvalue1'},
                {label: 'projlabel2', value: 'projvalue2'},
                {label: 'projlabel3', value: 'projvalue3'}
            ]
        };

        var STUB_LABELS_NO_DATA = {
            groups: [
                {
                    key: 'A-Z',
                    labels: []
                }
            ],
            field: 'someField',
            project: 'someProject'
        };

        var STUB_LABELS_DATA_SINGLE = {
            groups: [
                {
                    key: 'A-Z',
                    labels: [
                        { label: 'one', searchUrl: '/some/url/1' },
                        { label: 'two', searchUrl: '/some/url/2' },
                        { label: 'three', searchUrl: '/some/url/3' }
                    ]
                }
            ],
            field: 'someField',
            project: 'someProject'
        };

        var STUB_LABELS_DATA_SPLIT = {
            groups: [
                {
                    key: 'A-L',
                    labels: [
                        { label: 'one', searchUrl: '/some/url/1' },
                        { label: 'two', searchUrl: '/some/url/2' },
                        { label: 'three', searchUrl: '/some/url/3' }
                    ]
                },
                {
                    key: 'M-Z',
                    labels: [
                        { label: 'four', searchUrl: 'another/url/4'},
                        { label: 'five', searchUrl: 'another/url/5'},
                        { label: 'six', searchUrl: 'another/url/6'},
                    ]
                }
            ],
            field: 'someField',
            project: 'someProject'
        };

        module('jira-dashboard-items/labels', {
            setup: function () {
                this.$el = $('<div class="labels-gadget" />');

                var API = {
                    gadget: {
                        id: 1
                    },
                    resize: function () {},
                    getGadgetId: function () {return 1;},
                    hideLoadingBar: function () {},
                    initRefresh: function () {},
                    savePreferences: function () {},
                    setTitle: function () {},
                    showLoadingBar: function () {}
                };

                var gadgetOptions = {
                    delay: function (callback) { callback(); }
                };

                this.dashboardItem = new LabelsDashboardItem(API, gadgetOptions);
                $('#qunit-fixture').append(this.$el);
                this.server = sinon.fakeServer.create();

                // Calling `this.spy` does not work here.
                this.showLoadingSpy = sinon.spy(this.dashboardItem.API, 'showLoadingBar');
                this.hideLoadingSpy = sinon.spy(this.dashboardItem.API, 'hideLoadingBar');
            },

            teardown: function () {
                this.server.restore();
            },

            /**
             * Assert that the loading bar is shown and then hidden
             * @param statusCode The HTTP status code of the network request
             * @param fieldsResponse The response from the server corresponding to the request for fields
             * @param projectsResponse The response from the server corresponding to the request for projects
             */
            assertLoadingBarShowsAndHidesForConfig: function (statusCode, fieldsResponse, projectsResponse) {
                this.dashboardItem.renderEdit(this.$el, {});
                sinon.assert.calledOnce(this.showLoadingSpy);

                // The gadget makes two requests simultaneously, one for the fields and one for projects.
                this.server.requests[0].respond(statusCode, {'Content-Type': 'application/json'}, JSON.stringify(fieldsResponse));
                this.server.requests[1].respond(statusCode, {'Content-Type': 'application/json'}, JSON.stringify(projectsResponse));
                sinon.assert.calledOnce(this.hideLoadingSpy);
            },

            /**
             * Assert that the options for a select dropdown match the expected options
             * @param domOptions The options presented in the DOM
             * @param stubOptions The options we expect to be there. Each option must have a `.value` and a `.label`
             */
            assertSelectOptions: function (domOptions, stubOptions) {
                equal(domOptions.length, stubOptions.length, 'Number of options found should match the number of options given');

                for (var i = 0; i < domOptions.length; i++) {
                    equal($(domOptions[i]).attr('value'), stubOptions[i].value);
                    equal($(domOptions[i]).text(), stubOptions[i].label);
                }
            },

            /**
             * Respond to a fake XHR request with valid stub data
             */
            respondToRequestWithValidData: function () {
                // The gadget makes two requests simultaneously, one for the fields and one for projects
                this.server.requests[0].respond(200, {'Content-Type': 'application/json'}, JSON.stringify(STUB_FIELDS));
                this.server.requests[1].respond(200, {'Content-Type': 'application/json'}, JSON.stringify(STUB_PROJECTS));
            },

            /**
             * Assert that an info message is displayed for scenarios where no labels are found.
             */
            assertNoLabelsInfoMessage: function() {
                equal(this.$el.find('.aui-message.info').length, 1);
                var title = this.$el.find('.aui-message-info > .title').text();
                var description = this.$el.find('.aui-message-info').text();

                // `.text()` appends the text of the child elements to the root text.
                // The description section of the aui info panel doesn't have its own selector.
                // We have to strip the children's text to just contain the root description text.
                description = description.replace('labels.project-tab.nolabels.multiplefields.title', '');

                equal(title, 'labels.project-tab.nolabels.multiplefields.title');
                equal(description, 'labels.project-tab.nolabels.multiplefields.desc');
            }
        });

        /**
         * Test configuration screen
         */
        var SampleSuggestionsData = {
            suggestionType: 'project',
            suggestions: {
                projects: [
                    {
                        html: '<b>testProject</b>',
                        id: 10000,
                        key: 'xyz',
                        name: 'test'
                    }
                ]
            }
        };

        SampleSuggestionsData.suggestionId = SampleSuggestionsData.suggestions.projects[0].id;

        test('Config screen: Should display and then dismiss loading status when requesting config resources and is successful', function () {
            this.assertLoadingBarShowsAndHidesForConfig(200, STUB_FIELDS, STUB_PROJECTS);
        });

        test('Config screen: Should display and then dismiss loading status when requesting config resources, is successful, but the order of fields and projects response is swapped', function () {
            this.assertLoadingBarShowsAndHidesForConfig(200, STUB_PROJECTS, STUB_FIELDS);
        });

        test('Config screen: Should display and then dismiss loading status when requesting config resources and both requests fail', function () {
            this.assertLoadingBarShowsAndHidesForConfig(200, {}, {});
        });

        test('Config screen: Should display and then dismiss loading status when requesting config resources and projects request fails', function () {
            this.assertLoadingBarShowsAndHidesForConfig(500, STUB_FIELDS, {});
        });

        test('Config screen: Should display and then dismiss loading status when requesting config resources and fields request fails', function () {
            this.assertLoadingBarShowsAndHidesForConfig(500, {}, STUB_PROJECTS);
        });

        test('Config screen: Should display requested fields in drop down', function () {
            this.dashboardItem.renderEdit(this.$el, {});

            equal(2, this.server.requests.length, 'Should have a request for fields and projects');

            this.respondToRequestWithValidData();

            // Check that the field selection options are ok
            var fieldOptions = $('#' + this.dashboardItem.API.getGadgetId() + '-labels-field').find('option');
            this.assertSelectOptions(fieldOptions, STUB_FIELDS.labelFields);

            // Check that the project selection options are ok
            var projectSelectId = this.dashboardItem.API.getGadgetId() + '-projects-select';
            var projectOptions = $('#' + projectSelectId).find('option');
            this.assertSelectOptions(projectOptions, STUB_PROJECTS.options);

            // Check the label for the project select picker
            var projectSelectLabel = $('label[for="' + projectSelectId + '"]');
            equal(projectSelectLabel.text(), STUB_PROJECTS.label);
        });

        test('Config screen: Should show error when loading fields fails', function () {
            this.dashboardItem.renderEdit(this.$el, {});
            this.server.requests[0].respond(500, {'Content-Type': 'application/json'}, JSON.stringify({}));
            equal(this.$el.find('.aui-message.aui-message-error').length, 1);
        });

        test('Config screen: Should send the correct values after submitting the form', function () {
            this.dashboardItem.renderEdit(this.$el, {});

            this.respondToRequestWithValidData();

            // Select a project option
            var projectsSelect = $('#' + this.dashboardItem.API.getGadgetId() + '-projects-select', this.$el);
            projectsSelect.val(STUB_PROJECTS.options[1].value);

            // Select a field option
            var fieldSelect = $('#' + this.dashboardItem.API.getGadgetId() + '-labels-field', this.$el);
            fieldSelect.val(STUB_FIELDS.labelFields[1].value);

            // Check the refresh checkbox
            var refreshCheckbox = $('#' + this.dashboardItem.API.getGadgetId() + '-refresh-interval', this.$el);
            refreshCheckbox.attr('checked', true);
            var savePreferencesSpy = this.spy();

            this.stub(this.dashboardItem.API, 'savePreferences', savePreferencesSpy);
            this.dashboardItem.API.getRefreshFieldValue = this.stub().returns(true);
            $('form', this.$el).submit();

            var calledArgument = savePreferencesSpy.args[0][0];
            equal(calledArgument.projectid, projectsSelect.val());
            equal(calledArgument.fieldId, fieldSelect.val());
            equal(calledArgument.refresh, (refreshCheckbox.attr('checked') ? true : false));
        });

        test('Config screen: Should load previously saved preferences into the configuration form', function () {
            this.dashboardItem.renderEdit(this.$el, {
                id: 10000,
                fieldId: STUB_FIELDS.labelFields[1].value,
                projectid: STUB_PROJECTS.options[2].value,
                refresh: true
            });

            this.respondToRequestWithValidData();

            equal(this.$el.find('select[name=projectid]').val(), STUB_PROJECTS.options[2].value, 'Should have saved name');
            equal(this.$el.find('select[name=fieldId]').val(), STUB_FIELDS.labelFields[1].value, 'Should have saved fields');
            equal((this.$el.find('input[name=refresh-interval]').attr('checked') ? true : false), true, 'Expected refresh interval to be saved');
        });

        test('Main screen: Should display an info message when there are no labels', function () {
            this.dashboardItem.render(this.$el, {
                type: 'type',
                id: 10000,
                labelField: STUB_FIELDS.labelFields[0].value,
                isConfigured: true,
                refresh: 15
            });

            equal(this.server.requests.length, 1, 'Should have request for labels');
            this.server.requests[0].respond(200, {'Content-Type': 'text/html'}, JSON.stringify(STUB_LABELS_NO_DATA));
            this.assertNoLabelsInfoMessage();
        });

        test('Main screen: Should display labels in a single section', function () {
            this.dashboardItem.render(this.$el, {
                type: 'type',
                id: 10000,
                labelField: STUB_FIELDS.labelFields[0].value,
                isConfigured: true,
                refresh: 15
            });

            equal(this.server.requests.length, 1, 'Should have request for labels');
            this.server.requests[0].respond(200, {'Content-Type': 'text/html'}, JSON.stringify(STUB_LABELS_DATA_SINGLE));
            equal(this.$el.find('a.aui-label').length, STUB_LABELS_DATA_SINGLE.groups[0].labels.length);
            equal(this.$el.find('h3').length, 1);

            var links = this.$el.find('a.aui-label');
            for (var i = 0; i < links.length; i++) {
                equal($(links[i]).attr('href'), AJS.contextPath() + STUB_LABELS_DATA_SINGLE.groups[0].labels[i].searchUrl);
            }
        });

        test('Main screen: Should display labels split in two sections', function() {
            this.dashboardItem.render(this.$el, {
                type: 'type',
                id: 10000,
                labelField: STUB_FIELDS.labelFields[0].value,
                isConfigured: true,
                refresh: 15
            });

            this.server.requests[0].respond(200, {'Content-Type': 'text/html'}, JSON.stringify(STUB_LABELS_DATA_SPLIT));
            equal(this.$el.find('a.aui-label').length, STUB_LABELS_DATA_SPLIT.groups[0].labels.length + STUB_LABELS_DATA_SPLIT.groups[1].labels.length);
            equal(this.$el.find('h3').length, 2);

            var links = this.$el.find('a.aui-label');

            for (var i = 0; i < links.length; i++) {
                if (i < STUB_LABELS_DATA_SPLIT.groups[0].labels.length) {
                    // First split
                    equal($(links[i]).attr('href'), AJS.contextPath() + STUB_LABELS_DATA_SPLIT.groups[0].labels[i].searchUrl);
                } else {
                    // Second split
                    var firstSplitLength = STUB_LABELS_DATA_SPLIT.groups[0].labels.length;
                    equal($(links[i]).attr('href'), AJS.contextPath() + STUB_LABELS_DATA_SPLIT.groups[1].labels[i - firstSplitLength].searchUrl);
                }
            }

            var headings = this.$el.find('h3');
            equal($(headings[0]).text(), 'A-L');
            equal($(headings[1]).text(), 'M-Z');
        });

        test('Main screen: Should display a \'no labels\' error if the project is not found', function () {
            this.dashboardItem.render(this.$el, {
                type: 'type',
                id: 10000,
                labelField: STUB_FIELDS.labelFields[0].value,
                isConfigured: true,
                refresh: 15
            });

            this.server.requests[0].respond(404, {'Content-Type': 'text/json'}, JSON.stringify({}));
            this.assertNoLabelsInfoMessage();
        });
    });
});
