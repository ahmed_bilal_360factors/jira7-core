/**
 * Extending Colorable indicates that a component can map data to colors.
 *
 * @module Colorable
 */
define('jira-dashboard-items/components/charts/components/colorable', [
    'jira-dashboard-items/lib/c3',
    'jira-dashboard-items/lib/d3'
], function(
    c3,
    d3
) {
    'use strict';

    return function() {
        var DEFAULT_COLOR_VALUE = 4;
        var DEFAULT_COLOR_RANGE = ['red', 'blue'];

        return c3.component('colorable')
            .extend(c3.withData())
            .extend({
                color: function() {
                    var colorScale = this.colorScale();
                    var colorAccessor = this.colorAccessor();

                    return function(d, i) {
                        return colorScale(colorAccessor(d, i));
                    };
                },
                colorAccessor: c3.prop(function() { return DEFAULT_COLOR_VALUE; }),
                colorScaleConstructor: c3.inherit('colorScaleConstructor', d3.scale.linear),
                colorDomain: c3.inherit('colorDomain').onDefault(function() {
                    if (c3.isEmpty(this.data())) return;

                    var min = c3.checkIsNumber(d3.min(this.data(), this.colorAccessor()));
                    var max = c3.checkIsNumber(d3.max(this.data(), this.colorAccessor()));
                    return [min, max];
                }),
                colorRange: c3.inherit('colorRange').onDefault(function() {
                    return DEFAULT_COLOR_RANGE;
                }),
                colorScale: function() {
                    return this.colorScaleConstructor()()
                        .domain(this.colorDomain())
                        .range(this.colorRange());
                }
            });
    };
});
