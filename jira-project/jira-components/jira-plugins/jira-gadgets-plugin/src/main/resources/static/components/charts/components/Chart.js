/**
 * Applies axis and grid lines to a graph to generate a chart.
 * @module Chart
 */
define('jira-dashboard-items/components/charts/components/chart', [
    'jira-dashboard-items/components/charts/components/non-overlapping-horizontal-axis',
    'jira-dashboard-items/lib/d3',
    'jira-dashboard-items/lib/c3'
], function(
    NonOverlappingHorizontalAxis,
    d3,
    c3
) {

    /**
     * @typedef {Object} ChartOptions
     *
     * @property {Function} [xAxisValue] give an x value convert it to a readable format
     * @property {Function} [yAxisValue] give an y value convert it to a readable format
     **/


    /**
     * @param {c3.component} graph to generate axis for.
     * @param {ChartOptions} [options] for the charting.
     */
    return function(graph, options) {
        options = options || {};

        var chart = c3.borderLayout();

        var content = c3.layerable()
            .extend(function() {
                this.selection().classed('graph', true);
            })
            .addLayer('grid', c3.gridLines().orient('left'))
            .addLayer('graph', graph);

        chart.center(content);

        if (options.yAxisValue) {
            var yAxis = c3.labelledAxis()
                .axisConstructor(function () {
                    return d3.svg.axis()
                        .ticks(5)
                        .tickFormat(function (d) {
                            return options.yAxisValue(d);
                        });
                }).extend(function() {
                    this.selection().classed('yAxis', true);
                }).extend({
                    //Override yDomain so that it always has a range of 1.
                    yDomain: function() {
                        var yDomain = this.parent().yDomain();

                        if (yDomain[0] === yDomain[1]) {
                            return [yDomain[0], yDomain[0] + 1];
                        } else {
                            return yDomain;
                        }
                    }
                })
                .width(60)
                .orient('left');

            chart.west(yAxis);
        } else {
            chart.west(c3.withDimensions().width(10));
        }

        if (options.xAxisValue) {
            var xAxis = NonOverlappingHorizontalAxis()
                .axisConstructor(function () {
                    return d3.svg.axis()
                        .tickFormat(function (d) {
                            return options.xAxisValue(d);
                        });
                })
                .extend(function() {
                    this.selection().classed('xAxis', true);
                })
                .height(30)
                .orient('bottom');

            chart.south(xAxis);
        } else {
            chart.south(c3.withDimensions().height(10));
        }

        chart
            .north(c3.withDimensions().height(10))
            .east(c3.withDimensions().width(10));


        return chart;
    };
});
