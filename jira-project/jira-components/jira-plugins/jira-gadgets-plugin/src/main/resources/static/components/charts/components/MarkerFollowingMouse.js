/**
 * @module MarkerFollowingMouse
 */
define('jira-dashboard-items/components/charts/components/marker-following-mouse', [
    'underscore',
    'jira-dashboard-items/components/charts/components/interactive',
    'jira-dashboard-items/components/charts/components/markers',
    'jira-dashboard-items/lib/c3'
], function(
    _,
    Interactive,
    Markers,
    c3
) {

    /**
     * @typedef {Object} MarkerFollowingMouseOptions
     *
     * @property {Number} yTopOffset if we want to extend the marker further to the top (negative means further up)
     * @property {Function} [canShow] whether it is possible to show the marker
     * @property {Node} domContext the mouse events will be bound to.
     * @property {c3.component} region that the mouse enter events, etc want to actually be called on.
     *
     */

    /**
     * @param {MarkerFollowingMouseOptions} options
     *
     * Creates a component which generates a vertical line on the graph and which chases the mouse.
     * It also displays a dialog next to the line.
     */
    return function(options) {


        //Only a single marker
        var data = [[0]];

        var markers = Markers().data(data).xAccessor(function() { return 0;});

        markers.extend(function() {
            this.selection().classed('following-marker', true);
        });

        //Create events that can be bound to.
        markers.extend({
            showCallback: c3.event(),
            refreshCallback: c3.event(),
            hideCallback: c3.event(),
            show: function() {
                if (canShow()) {
                    isVisible = true;
                    markers.elements()
                        .style('visibility', '');

                    markers.showCallback();
                }
            },

            refresh: function() {
                var mouse = region.mouseCoordinates();

                markers.elements()
                    .attr('x1', mouse[0])
                    .attr('x2', mouse[0])
                    .attr('y1', options.yTopOffset || 0);

                if (!canShow() && isVisible) {
                    this.hide();
                }
                else if (!isVisible && canShow()) {
                   this.show();
                }

                markers.refreshCallback(markers.parent().xScale().invert(mouse[0]));
            },

            hide: function() {
                isVisible = false;
                markers.elements()
                    .style('visibility', 'hidden');

                markers.hideCallback();
            }
        });


        //Listen to the regions mouse events to call marker functions.
        var domContext = options.domContext;
        var region = options.region;

        var interactive = Interactive().domContext(domContext).region(region);

        region.extend(interactive);

        region.mouseenter(function() {
           markers.show();
        });

        region.mousemove(function() {
            markers.refresh();
        });

        region.mouseout(function() {
            markers.hide();
        });

        var canShow = options.canShow || _.constant(true);
        var isVisible = false;

        return markers;
    };
});
