/**
 * @name OverlapRemover
 *
 */
define('jira-dashboard-items/components/overlap-remover/overlap-remover', [
    'jira/util/logger',
    'underscore',
    'jira-dashboard-items/components/factors/number-factor-generator'
], function(logger, _, numberFactorGenerator) {

    return {
        /**
         * Given a list of sorted elements from left to right we want to reduce the minimum number of elements to
         * remove overlaps, whilst maintaining the same distance between elements.
         * @param {*[]} elements to be tested for overlaps
         * @param {Function} isOverlapping is given two elements and returns true if they overlap
         * @return {*[]} elements remaining
         */
        reduce: function(elements, isOverlapping) {
            if (_.isUndefined(isOverlapping)) {
                throw new Error("No comparison function provided");
            }
            //Wants the gaps between labels to be even and so finds the factors to do this.
            var factors = numberFactorGenerator.generate(elements.length - 1);

            var nonOverlappingJump = _.find(factors, function(jump) {
                var indices = _.range(0, elements.length, jump);
                var nonOverlap = _.all(_.rest(indices), function(i) {
                    return !isOverlapping(elements[i - jump], elements[i]);
                });
                return nonOverlap;
            });

            //If even the first and last elements overlap, just show those two anyway.
            if (_.isUndefined(nonOverlappingJump)) {
                nonOverlappingJump = _.last(factors);
            }

            logger.log("JUMP: " + nonOverlappingJump);
            var nonOverlappingIndices = _.range(0, elements.length, nonOverlappingJump);
            return _.filter(elements, function(element, index) {
                return _.contains(nonOverlappingIndices, index);
            });
        }
    };
});
