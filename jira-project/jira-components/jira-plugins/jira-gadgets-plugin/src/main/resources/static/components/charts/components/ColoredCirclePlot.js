/**
 * A circle plot component built on c3's circle plot that allows coloring of the circles.
 *
 * @module ColoredCirclePlot
 */
define('jira-dashboard-items/components/charts/components/colored-circle-plot', [
    'jquery',
    'jira-dashboard-items/components/charts/components/colorable',
    'jira-dashboard-items/lib/d3',
    'jira-dashboard-items/lib/c3'
], function(
    $,
    Colorable,
    d3,
    c3
) {
    'use strict';

    return function () {
        var MIN_RADIUS_RANGE = 10;
        var MAX_RADIUS_RANGE = 30;
        var RADIUS_DATA_VALUE = 4;

        return c3.component('colored-circle-plot')
            .extend(c3.circlePlot())
            .extend(Colorable())
            .update(function (event) {
                event.selection
                    .attr('cx', this.x())
                    .attr('cy', this.y())
                    .attr('r', this.radius())
                    .attr('fill', this.color())
                    .attr('stroke', this.strokeColor());
            })
            .extend({
                radius: function () {
                    var radiusScale = this.radiusScale();
                    var radiusAccessor = this.radiusAccessor();

                    return function (d, i) {
                        return radiusScale(radiusAccessor(d, i));
                    };
                },
                radiusAccessor: c3.prop(function () { return RADIUS_DATA_VALUE; }),
                radiusDomain: c3.inherit('radiusDomain').onDefault(function () {
                    if (c3.isEmpty(this.data())) return;

                    var min = c3.checkIsNumber(d3.min(this.data(), this.radiusAccessor()));
                    var max = c3.checkIsNumber(d3.max(this.data(), this.radiusAccessor()));

                    return [min, max];
                }),
                radiusRange: c3.inherit('radiusRange').onDefault(function () {
                    return [MIN_RADIUS_RANGE, MAX_RADIUS_RANGE];
                }),
                radiusScaleConstructor: c3.inherit('radiusScaleConstructor', d3.scale.linear),
                radiusScale: function () {
                    return this.radiusScaleConstructor()()
                        .domain(this.radiusDomain())
                        .range(this.radiusRange());
                },
                strokeColor: function () {
                    var fillColor = this.color();
                    return function (d, i) { return d3.rgb(fillColor(d, i)).darker(); };
                }
            });
    };
});
