AJS.test.require(["com.atlassian.jira.gadgets:area-between-plot-one-way", "com.atlassian.jira.gadgets:c3-testutil"], function() {
    var d3 = require('jira-dashboard-items/lib/d3');
    var $ = require('jquery');
    var AreaBetweenPlotOneWay = require('jira-dashboard-items/components/charts/components/area-between-plot-one-way');
    var c3TestUtil = require('jira-dashboard-items/components/charts/components/c3-test-util');



    module('Components.Charts.Components.AreaBetweenPlotOneWay', {
        setup: function() {
            this.$el = $("<svg height='100'/>");
            $("body").append(this.$el);
        },

        teardown: function() {
            this.$el.remove();
        }
    });

    test("Only displays area one way", function() {
        var SINGLE_POINT = 1;
        var MULTI_POINT = 2;

        var data = [
            [0, [5, 5]],
            [10, [7, 3]],
            [20, [5, 5]],
            [30, [3, 7]],
            [40, [5, 5]],
            [50, [7, 3]],
            [60, [5, 5]]
        ];

        var expectedAreaPoints = [
            [0, SINGLE_POINT],
            [10, MULTI_POINT],
            [20, SINGLE_POINT],
            [30, SINGLE_POINT],
            [40, SINGLE_POINT],
            [50, MULTI_POINT],
            [60, SINGLE_POINT]
        ];

        this.$el.attr('width', c3TestUtil.graphWidth(data));

        var plot = AreaBetweenPlotOneWay.firstAbove();

        plot.data(data);

        plot(d3.select(this.$el.get(0)));

        var path = this.$el.find("path").attr("d");
        var xPointY = c3TestUtil.extractPathPoints(path);

        expectedAreaPoints.forEach(function(point) {
            var x = point[0];
            equal(xPointY[x].length, point[1], "The points on the path at position " + x + " does not match the expected number");
        });
    });



    test("Second above shows opposite to first above ", function() {
        var SINGLE_POINT = 1;
        var MULTI_POINT = 2;

        var data = [
            [0, [5, 5]],
            [10, [7, 3]],
            [20, [5, 5]],
            [30, [3, 7]],
            [40, [5, 5]],
            [50, [7, 3]],
            [60, [5, 5]]
        ];

        var expectedAreaPoints = [
            [0, SINGLE_POINT],
            [10, SINGLE_POINT],
            [20, SINGLE_POINT],
            [30, MULTI_POINT],
            [40, SINGLE_POINT],
            [50, SINGLE_POINT],
            [60, SINGLE_POINT]
        ];

        this.$el.attr('width', c3TestUtil.graphWidth(data));

        var plot = AreaBetweenPlotOneWay.secondAbove();

        plot.data(data);

        plot(d3.select(this.$el.get(0)));

        var path = this.$el.find("path").attr("d");
        var xPointY = c3TestUtil.extractPathPoints(path);

        expectedAreaPoints.forEach(function(point) {
            var x = point[0];
            equal(xPointY[x].length, point[1], "The points on the path at position " + x + " does not match the expected number");
        });
    });

    test("Generates point of intersection", function() {
        var data = [
            [0, [5, 5]],
            [10, [3, 7]],
            [20, [7, 3]],
            [30, [5, 5]]
        ];

        this.$el.attr('width', c3TestUtil.graphWidth(data));

        var plot = AreaBetweenPlotOneWay.firstAbove();

        plot.data(data);

        plot(d3.select(this.$el.get(0)));

        var path = this.$el.find("path").attr("d");
        var xPointY = c3TestUtil.extractPathPoints(path);

        equal(xPointY[15].length, 1, "Expects there to be a data point at point 2 generated from the point of intersection");
    });
});
