AJS.test.require("com.atlassian.jira.gadgets:createdvsresolved-dashboard-item-test-resources", function() {

    var $ = require('jquery');
    var _ = require('underscore');
    var moment = require('jira/moment');
    var DateRange = require('jira-dashboard-items/components/dates/daterange');
    var CreatedVsResolvedChart = require('jira-dashboard-items/components/charts/createdvsresolved-chart');
    var c3TestUtil = require('jira-dashboard-items/components/charts/components/c3-test-util');
    var formatter = require('jira/util/formatter');

    function createData(counts) {
        var data = [];
        counts.forEach(function(count, index) {
            data.push({
                count: count,
                searchUrl: "url/" + index
            });
        });
        return data;
    }

    var dateFormat = "DD-MMM-YYYY";

    var CREATED_DATA = createData([9, 5, 1, 3, 2, 5]);
    var RESOLVED_DATA = createData([5, 8, 5, 3, 4, 3]);
    var VERSIONS_RELEASED = {
        JAN_RELEASE_ONE: moment("05-Jan-2014", dateFormat).valueOf(),
        JAN_RELEASE_TWO: moment("05-Jan-2014", dateFormat).valueOf()
    };
    var DOMAIN = [
        {start: moment("01-Jan-2014", dateFormat).valueOf(), end: moment("01-Feb-2014", dateFormat).valueOf()},
        {start: moment("01-Feb-2014", dateFormat).valueOf(), end: moment("01-Mar-2014", dateFormat).valueOf()},
        {start: moment("01-Mar-2014", dateFormat).valueOf(), end: moment("01-Apr-2014", dateFormat).valueOf()},
        {start: moment("01-Apr-2014", dateFormat).valueOf(), end: moment("01-May-2014", dateFormat).valueOf()},
        {start: moment("01-May-2014", dateFormat).valueOf(), end: moment("01-Jun-2014", dateFormat).valueOf()},
        {start: moment("01-Jun-2014", dateFormat).valueOf(), end: moment("01-Jul-2014", dateFormat).valueOf()}
    ];

    var FILTER_URL = "/someurl?jql=project=10000";


    module('Components.Charts.Components.CreatedVsResolved', {
        setup: function() {
            this.$el = $("<div style='width:255px;'/>");
            $("body").append(this.$el);

            var formattedData = {
                created: CREATED_DATA,
                resolved: RESOLVED_DATA,
                domain: DOMAIN
            };

            this.includeVersionData = {
                versionData : [
                    [
                        VERSIONS_RELEASED.JAN_RELEASE_ONE,
                        {
                            released : true,
                            name : "Version 1",
                            project : {
                                key : "TEST1"
                            },
                            releaseDate : VERSIONS_RELEASED.JAN_RELEASE_ONE
                        }
                    ],
                    [
                        VERSIONS_RELEASED.JAN_RELEASE_TWO,
                        {
                            released : true,
                            name : "Version 2",
                            project : {
                                key : "TEST2"
                            },
                            releaseDate : VERSIONS_RELEASED.JAN_RELEASE_TWO
                        }
                    ]
                ]
            };

            this.DEFAULT_CHART_OPTIONS = {
                id: '1000',
                data: formattedData,
                versionData: null,
                el: this.$el,
                filterUrl: FILTER_URL,
                chartOptions: {
                    includeXAxis: true,
                    includeYAxis: true
                },
                days: 500
            };

            this.clock = sinon.useFakeTimers();

            //Overriding format not AJS.I18n.getText because tests seem to replace that or something.
            this.formatStub = sinon.stub(formatter, "format", function(templateName) {
                if (templateName === 'popups.daterange.datebetween') {
                    return arguments[1] + " and " + arguments[2];
                } else if (templateName === 'portlet.createdvsresolved.created.number') {
                    return arguments[1] + " created";
                } else if (templateName === 'portlet.createdvsresolved.resolved.number') {
                    return arguments[1] + " resolved";
                } else {
                    return templateName;
                }
            });
        },

        teardown: function() {
            $(".aui-inline-dialog").remove();
            this.formatStub.restore();
            this.clock.restore();
            this.$el.remove();
        }
    });

    test("Renders created vs resolved", function() {
        new CreatedVsResolvedChart(this.DEFAULT_CHART_OPTIONS).render();

        ok(this.$el.has("svg.createdvsresolved-chart").length > 0, "Should have rendered the chart into the element");
    });

    test("For each line corner there is a corresponding circle point", function() {
        new CreatedVsResolvedChart(this.DEFAULT_CHART_OPTIONS).render();

        var graph = this.$el.find("svg.createdvsresolved-chart > .graph > .graph");

        var createdPoints = graph.find(".created-points circle");

        var createdLine = graph.find(".created > .line > .line");

        var linePoints = c3TestUtil.extractPathPoints(createdLine.attr("d"));

        for (var x in linePoints) {
            var found = false;
            createdPoints.each(function(index, point) {
                point = $(point);
                if (point.attr("cx") === x &&
                    +point.attr("cy") === linePoints[x][0]) {
                    found = true;
                }
            });
            ok(found, "Did not find circle for line point '" + x + "'");
        }
    });

    test("Creates inline dialog for circle plot click event", function() {

        new CreatedVsResolvedChart(this.DEFAULT_CHART_OPTIONS).render();

        var graph = this.$el.find("svg.createdvsresolved-chart > .graph > .graph");

        var createdPoints = graph.find(".created-points circle");

        ok(!$(".aui-inline-dialog").is(":visible"), "Inline dialog should not be visible");

        var point = 0;
        createdPoints.get(point + 1).dispatchEvent(new MouseEvent('click'));

        this.clock.tick(100);
        //Wait until dialog is rendered.
        var inlineDialog = $(".aui-inline-dialog");
        ok(inlineDialog.is(":visible"), "Inline dialog should now be visible");

        equal(inlineDialog.find("p").text(), CREATED_DATA[point].count + " created common.concepts.view.issuenavigator", "Should have placed the number of created into the dialog");
        equal(inlineDialog.find("a").attr('href'), "url/" + point, "Should have placed the number of created into the dialog");
    });

    test("Creates inline dialog on point click with both data if points overlap", function() {
        new CreatedVsResolvedChart(this.DEFAULT_CHART_OPTIONS).render();
        var graph = this.$el.find("svg.createdvsresolved-chart > .graph > .graph");
        var createdPoints = graph.find(".created-points circle");

        ok(!$(".aui-inline-dialog").is(":visible"), "Inline dialog should not be visible");

        var point = 3;

        createdPoints.get(point + 1).dispatchEvent(new MouseEvent('click'));

        this.clock.tick(100);

        //Wait until dialog is rendered.
        var inlineDialog = $(".aui-inline-dialog");
        ok(inlineDialog.is(":visible"), "Inline dialog should now be visible");

        equal($(inlineDialog.find("p").get(0)).text(), CREATED_DATA[point].count + " created common.concepts.view.issuenavigator", "Should have placed the number of created into the dialog");
        equal($(inlineDialog.find("p").get(1)).text(), RESOLVED_DATA[point].count + " resolved common.concepts.view.issuenavigator", "Should have placed the number of created into the dialog");

        equal($(inlineDialog.find("a").get(0)).attr('href'), "url/" + point, "Should have placed the number of created into the dialog");
        equal($(inlineDialog.find("a").get(1)).attr('href'), "url/" + point, "Should have placed the number of created into the dialog");
    });

    test("Cumulative charts have a correct date range for point click inline dialog", function() {
        var cumulativeChartOptions = _.extend({}, this.DEFAULT_CHART_OPTIONS, {cumulative: true});
        new CreatedVsResolvedChart(cumulativeChartOptions).render();

        var graph = this.$el.find("svg.createdvsresolved-chart > .graph > .graph");

        var createdPoints = graph.find(".created-points circle");

        var point = 2;

        createdPoints.get(point + 1).dispatchEvent(new MouseEvent('click'));

        this.clock.tick(100);

        //Wait until dialog is rendered.
        var inlineDialog = $(".aui-inline-dialog");
        ok(inlineDialog.is(":visible"), "Inline dialog should now be visible");

        var expectedTitle = DateRange.rangeToText(DOMAIN[0].start, DOMAIN[point].end);

        equal($(inlineDialog.find("h3").get(0)).text(), expectedTitle, "Should have created the title from start to point with cumulative");

        var start = moment(DOMAIN[0].start).format("YYYY-MM-DD");

        equal($(inlineDialog.find("a").get(0)).attr('href'), "url/" + point, "View in issue navigator should do from first for cumulative");
    });

    test("Grouped labels working correctly", function(){

        var withVersionDataIncluded = $.extend({}, this.DEFAULT_CHART_OPTIONS, this.includeVersionData);
        var cvr = new CreatedVsResolvedChart(withVersionDataIncluded);
        var elt = this.$el;

        cvr.render();

        //  Only one grouped label is (should) being rendered to the DOM.
        var groupedLabel = elt.find("text.label");
        var groupedLabelIndices = groupedLabel.data('index');

        equal(groupedLabelIndices, "0,1", "Grouped Label has rendered correctly.");
        groupedLabel.trigger("click");

        var groupedLabelInlineDialogSections = $(".aui-inline-dialog-contents .version-information");

        equal(groupedLabelInlineDialogSections.length, _.size(VERSIONS_RELEASED), "Inline dialog for grouped label working.");

    });

});
