/**
 * Creates a set of lines that go down the graph vertically at different points.
 *
 * @module Markers
 */
define('jira-dashboard-items/components/charts/components/markers', [
    'jira-dashboard-items/lib/c3'
], function(c3) {

    return function () {
        return c3.component("marker")
            .extend(c3.drawable())
            .extend(c3.plottable())
            .elementTag('line')
            .elementClass('marker-line')

            .extend({
                y1: c3.prop(0),

                y2: function() {
                    var self = this;
                    return function() {
                        return self.height();
                    };
                }
            })
            .update(function(event) {
                event.selection
                    .attr('x1', this.x())
                    .attr('x2', this.x())
                    .attr('y1', this.y1())
                    .attr('y2', this.y2());
            })
            .extend(function() {
                this.selection().style('pointer-events', 'none');
            });
    };

});
