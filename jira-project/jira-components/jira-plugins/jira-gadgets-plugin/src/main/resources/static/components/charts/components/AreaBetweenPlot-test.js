AJS.test.require(["com.atlassian.jira.gadgets:area-between-plot", "com.atlassian.jira.gadgets:c3-testutil"], function() {
    var d3 = require('jira-dashboard-items/lib/d3');
    var AreaBetweenPlot = require('jira-dashboard-items/components/charts/components/area-between-plot');
    var c3TestUtil = require('jira-dashboard-items/components/charts/components/c3-test-util');


    module('Components.Charts.Components.AreaBetweenPlot', {
        setup: function() {
            this.svg = d3.select('#qunit-fixture').append('svg').attr('height', '100');
        }
    });

    test("Two separate values create an area", function() {
        var SINGLE_POINT = 1;
        var MULTI_POINT = 2;

        var data = [
            [0, [5, 5]],
            [10, [3, 7]],
            [20, [5, 5]],
            [30, [7, 3]],
            [40, [5, 5]]
        ];

        var expectedAreaPoints = [
            [0, SINGLE_POINT],
            [10, MULTI_POINT],
            [20, SINGLE_POINT],
            [30, MULTI_POINT],
            [40, SINGLE_POINT]
        ];

        this.svg.attr('width', c3TestUtil.graphWidth(data));

        var plot = AreaBetweenPlot();

        plot.data(data);

        plot(this.svg);


        var path = d3.select("path").attr("d");
        var xPointY = c3TestUtil.extractPathPoints(path);


        expectedAreaPoints.forEach(function(point) {
            var x = point[0];
            equal(xPointY[x].length, point[1], "The points on the path at position " + x + " does not match the expected number");
        });
    });
});
