define('jira-dashboard-items/labels', [
    'jira/util/formatter',
    'wrm/context-path',
    'jquery',
    'underscore',
    'jira-dashboard-items/components/autocomplete/project-filter-autocomplete',
    'jira-dashboard-items/common-functions'
], function(
    formatter,
    wrmContextPath,
    $,
    _,
    ProjectFilterAutocomplete,
    CommonFunctions) {
    'use strict';

    var DashboardItem = function (API, options) {
        this.API = API;
        this.options = options || {};
    };

    /**
     * Extract data from a form using jQuery.serializeArray. An internal whitelist is used to control data read.
     * Note: 'refresh-interval' is mapped to 'refresh' here
     * @param {Object} [$form]
     * @returns {Object} a map of preferences, where keys are the field names and values are the input values
     * @private
     */
    function getPreferencesObject($form) {
        var formData = $form.serializeArray();
        var paramsWhitelist = ['name', 'type', 'id', 'refresh-interval', 'fieldId', 'projectid'];
        var preferences = {};

        _.each(formData, function (element) {
            if (_.indexOf(paramsWhitelist, element.name) === -1) {
                return;
            }

            if (element.name === 'refresh-interval') {
                preferences.refresh = true;
            }
            else {
                preferences[element.name] = element.value;
            }
        });

        return preferences;
    }

    function ajaxRequestResolved() {
        this.API.resize();
        this.API.hideLoadingBar();
    }

    function displayError($element) {
        $element.html(JIRA.DashboardItem.Labels.Templates.Error());
    }

    DashboardItem.prototype.render = function (element, preferences) {
        this.API.showLoadingBar();
        this.API.initRefresh(preferences, _.bind(this.render, this, element, preferences));

        preferences = CommonFunctions.projectFilterBackwardCompatible(preferences);

        var endPoint = '/rest/gadget/1.0/labels/gadget/' + preferences.projectid + '/' +
            encodeURIComponent(preferences.fieldId);

        $.ajax({
            method: 'GET',
            url: wrmContextPath() + endPoint,
            dataType: 'json'
        }).done(_.bind(displayLabelsDashboardItem, this, $(element))).fail(function(data) {
            if (data.status === 404) {
                $(element).html(JIRA.DashboardItem.Labels.Templates.NoLabels());
            } else {
                displayError($(element));
            }
        }).always(_.bind(ajaxRequestResolved, this));

        function displayLabelsDashboardItem($element, data) {
            // No label data for this field in this project
            if (data.groups.length === 0 || (data.groups.length === 1 && data.groups[0].labels.length === 0)) {
                $element.html(JIRA.DashboardItem.Labels.Templates.NoLabels());
                return;
            }

            // Display the labels
            $element.html(JIRA.DashboardItem.Labels.Templates.Labels({
                groups: data.groups
            }));

            // Set the gadget title
            var field = data.field;
            var project = data.project;
            if (field && project) {
                this.API.setTitle(formatter.format(formatter.I18n.getText('gadget.labels.title'), field, project));
            }
            else {
                this.API.setTitle(formatter.I18n.getText('gadget.labels.dashboard.name'));
            }
        }
    };

    DashboardItem.prototype.renderEdit = function (element, preferences) {
        var $gadgetElement = $(element);

        this.API.showLoadingBar();
        preferences = CommonFunctions.projectFilterBackwardCompatible(preferences);

        var fieldsAjaxRequest = $.ajax(wrmContextPath() + '/rest/gadget/1.0/labels/gadget/fields');
        var projectsAjaxRequest = $.ajax(wrmContextPath() + '/rest/gadget/1.0/filtersAndProjects?showFilters=false');

        // It's not explicit from `$.when` that `fieldsAjaxRequest` will begin or end before `projectsAjaxRequest`.
        // We will enforce this in the `displayConfigurationPanelForMultipleQueries` callback.
        $.when(fieldsAjaxRequest, projectsAjaxRequest)
            .done(_.bind(displayConfigurationPanelForMultipleQueries, this, $gadgetElement))
            .fail(_.partial(displayError, $gadgetElement))
            .always(_.bind(ajaxRequestResolved, this));

        function displayConfigurationPanelForMultipleQueries($gadgetElement, fieldsRequest, projectsRequest) {
            // Enforce that `fieldsRequest` is genuinely for the fields request, and `projectsRequest` is actually for projects.
            // If either of them contain the data for the other, swap the variable names.
            if ((!fieldsRequest[0].labelFields && projectsRequest[0].labelFields) ||
                !projectsRequest[0].label && fieldsRequest[0].label) {
                var temp = projectsRequest;
                projectsRequest = fieldsRequest;
                fieldsRequest = temp;
            }

            if (!fieldsRequest[0].labelFields || !projectsRequest[0].label || !projectsRequest[0].options) {
                // No data retrieved.
                displayError($gadgetElement);
                return;
            }

            // Create a prefix from the gadget's id to prevent collisions with other instances of this dashboard item.
            var prefix = this.API.getGadgetId() + "-";

            // `fieldsRequest` and `projectsRequest` are arrays consisting of the response object,
            // the status, and a jqXHR object.
            // See the last example at http://api.jquery.com/jQuery.when/.
            var fieldsData = fieldsRequest[0].labelFields;
            var projectsSelectionLabel = projectsRequest[0].label;
            var projectsSelectionOptions = projectsRequest[0].options;

            // Load the template, and display the user's projects and custom labels
            var config = JIRA.DashboardItem.Labels.Templates.Configuration({
                prefix: prefix,
                preferences: preferences,
                fields: fieldsData,
                projectsLabel: formatter.I18n.getText(projectsSelectionLabel),
                projects: projectsSelectionOptions
            });

            $gadgetElement.html(config);

            var form = $("form", $gadgetElement);

            var cancelButton = $(".cancel", form);
            if (cancelButton) {
                cancelButton.click(_.bind(function () {
                    this.API.closeEdit();
                }, this));
            }

            form.on("submit", _.bind(function (e) {
                e.preventDefault();

                var $form = $(e.target);
                var preferences = getPreferencesObject($form);

                if (completeValidation(preferences, $form)) {
                    this.API.savePreferences(preferences);
                }
            }, this));
        }

        function completeValidation(preferences, form) {
            if (!preferences.projectid || !preferences.fieldId) {
                $(".projectOrFilter-error", form).text(formatter.I18n.getText('gadget.common.required.query')).show();
                return false;
            }

            return true;
        }
    };

    return DashboardItem;
});
