AJS.test.require("com.atlassian.jira.gadgets:date-range", function() {

    var DateRange = require('jira-dashboard-items/components/dates/daterange');
    var formatter = require('jira/util/formatter');

    var APRIL = 3;
    var MAY = APRIL + 1;

    module('Components.Dates.DateRange', {
        setup: function() {

            //Overriding format not AJS.I18n.getText because tests seem to replace that or something.
           this.formatStub = sinon.stub(formatter, "format", function(templateName, start, end) {
                return start + " and " + end;
            });
        },

        teardown: function() {
            this.formatStub.restore();
        }
    });

    test("Different years", function() {
        var start = new Date(2014, APRIL, 23);
        var end = new Date(2015, APRIL, 23);

        equal(DateRange.rangeToText(start.valueOf(), end.valueOf()), "23 Apr 2014 and 23 Apr 2015");
    });


    test("Different months", function() {
        var start = new Date(2014, APRIL, 23);
        var end = new Date(2014, MAY, 23);

        equal(DateRange.rangeToText(start.valueOf(), end.valueOf()), "23 Apr and 23 May 2014");
    });

    test("Different days in month", function() {
        var start = new Date(2014, APRIL, 16);
        var end = new Date(2014, APRIL, 23);

        equal(DateRange.rangeToText(start.valueOf(), end.valueOf()), "16 and 23 Apr 2014");
    });

    test("Full day range outputs as the single date", function() {
        var start = new Date(2014, APRIL, 23, 0, 0);
        var end = new Date(2014, APRIL, 23, 23, 59);

        equal(DateRange.rangeToText(start.valueOf(), end.valueOf()), "23 Apr 2014");
    });

    test("Different hours in day", function() {
        var start = new Date(2014, APRIL, 16, 12, 34);
        var end = new Date(2014, APRIL, 16, 15, 23);

        equal(DateRange.rangeToText(start.valueOf(), end.valueOf()), "12:34 and 15:23 16 Apr 2014");
    });

    test("Same time defaults to single day", function() {
        var date = new Date(2014, APRIL, 16, 12, 36);

        equal(DateRange.rangeToText(date.valueOf(), date.valueOf()), "16 Apr 2014");
    });

});
