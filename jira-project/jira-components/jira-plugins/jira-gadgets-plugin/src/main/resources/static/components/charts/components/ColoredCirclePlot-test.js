AJS.test.require([
    'com.atlassian.jira.gadgets:common-test-resources',
    'com.atlassian.jira.gadgets:colored-circle-plot'
], function () {

        'use strict';

        var d3 = require('jira-dashboard-items/lib/d3');
        var $ = require('jquery');
        var _ = require('underscore');
        var ColoredCirclePlot = require('jira-dashboard-items/components/charts/components/colored-circle-plot');

        var WIDTH = '300';
        var HEIGHT = '300';
        var DATA = [
            [1, 2, 3, 4],
            [2, 3, 4, 5],
            [3, 4, 5, 6]
        ];

        module('jira-dashboard-items/components/charts/components/colored-circle-plot', {
            setup: function() {
                this.svg = d3.select('#qunit-fixture')
                    .append('svg')
                    .attr('height', HEIGHT)
                    .attr('width', WIDTH);

                this.plot = ColoredCirclePlot();

                this.plotWithAccessors = ColoredCirclePlot()
                    .radiusAccessor(function (d) { return d[2]; })
                    .colorAccessor(function(d) { return d[3]; });
            },

            teardown: function() {}
        });

        test('Should have a default radius accessor', function() {
            var radiusAccessor = this.plot.radiusAccessor;
            ok(_.isFunction(radiusAccessor));
            ok(_.isFunction(radiusAccessor()));

            var defaultRadius = radiusAccessor.call(this.plot)();
            equal(defaultRadius, 4);
        });

        test('Should not have a default radius domain when data is not provided', function() {
            deepEqual(this.plot.radiusDomain(), undefined);
        });

        test('Should have a default radius domain when data is provided', function() {
            this.plot.data([1, 2, 3]);

            deepEqual(this.plot.radiusDomain(), [4, 4]);
        });

        test('Should have a default radius range', function() {
            deepEqual(this.plot.radiusRange(), [10, 30]);
        });

        test('Should have a default radius scale constructor', function() {
            equal(this.plot.radiusScaleConstructor(), d3.scale.linear);
        });

        test('Should have a default radius scale', function() {
            this.plot.data([1, 2, 3]);
            var radiusScale = this.plot.radiusScale;
            var expectedScale = d3.scale.linear().domain([4, 4]).range([10, 30]);
            var actualScale = radiusScale.call(this.plot);

            ok(_.isFunction(radiusScale));
            equal(expectedScale(3), actualScale(3));
            equal(expectedScale(7), actualScale(7));
        });

        test('Should apply a default stroke color', function() {
            this.plotWithAccessors.data(DATA);
            var strokeColor = this.plotWithAccessors.strokeColor;
            var colorConstructor = strokeColor.call(this.plotWithAccessors);

            ok(_.isFunction(strokeColor));
            deepEqual(colorConstructor([1, 2, 3, 4], 0), d3.rgb('178', '0', '0')); // slightly darker red than #ff0000
        });

        test('Should plot circles with the correct x, y, radius, and color values for a simple dataset', function() {
            this.plotWithAccessors.data(DATA);
            this.plotWithAccessors(this.svg);

            var circles = _.map($(this.svg[0]).find('circle'), $);

            equal(circles.length, DATA.length);

            equal(circles[0].attr('cx'), 0);
            equal(circles[1].attr('cx'), WIDTH / 2);
            equal(circles[2].attr('cx'), WIDTH);

            // y range is inverted
            equal(circles[0].attr('cy'), WIDTH);
            equal(circles[1].attr('cy'), WIDTH / 2);
            equal(circles[2].attr('cy'), 0);

            equal(circles[0].attr('r'), 10);
            equal(circles[1].attr('r'), 20);
            equal(circles[2].attr('r'), 30);

            equal(circles[0].attr('fill'), '#ff0000'); // red
            equal(circles[1].attr('fill'), '#800080'); // purple (50%)
            equal(circles[2].attr('fill'), '#0000ff'); // blue
        });
});
