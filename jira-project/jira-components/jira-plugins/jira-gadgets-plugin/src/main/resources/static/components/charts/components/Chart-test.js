AJS.test.require(["com.atlassian.jira.gadgets:chart", "com.atlassian.jira.gadgets:c3-testutil"], function() {

    var d3 = require('jira-dashboard-items/lib/d3');
    var c3 = require('jira-dashboard-items/lib/c3');
    var $ = require('jquery');
    var Chart = require('jira-dashboard-items/components/charts/components/chart');
    var c3TestUtil = require('jira-dashboard-items/components/charts/components/c3-test-util');


    module('Components.Charts.Components.AreaBetweenPlot', {
        setup: function() {
            this.$el = $("<svg height='100'/>");
            //Appended to body instead of #qunit-fixture so you can see it on the page if you breakpoint
            $("body").append(this.$el);
        },

        teardown: function() {
            this.$el.remove();
        }
    });

    test("Including xAxisValue renders correctly", function() {
        var data = [
            [0, 20],
            [100, 10],
            [200, 15],
            [300, 30],
            [400, 20]
        ];

        this.$el.attr('width', c3TestUtil.graphWidth(data));

        var graph = c3.linePlot();

        var chart = Chart(graph, {
            xAxisValue: function(d) {
                return "$" + d;
            }
        }).data(data);

        chart(d3.select(this.$el.get(0)));


        var xAxis = this.$el.children("g").get(1);

        var x = 0;
        var tickIncrementer = 50; //Do not like this.

        $(xAxis).find(".tick").each(function(index, tick) {
            var label = $(tick).find("text");
            var labelValue = label.text();

            equal(labelValue, "$" + x, "Tick " + index + " does not have the correct value");
            x += tickIncrementer;
        });

        var yAxis = this.$el.children("g").get(2);
        ok($(yAxis).is(':empty'), "Should have no ticks in the yAxis");
    });

    test("Including yAxisValue renders correctly", function() {
        var data = [
            [0, 20],
            [100, 10],
            [200, 15],
            [300, 30],
            [400, 20]
        ];

        this.$el.attr('width', c3TestUtil.graphWidth(data));

        var graph = c3.linePlot();

        var chart = Chart(graph, {
            yAxisValue: function(d) {
                return "$" + d;
            }
        }).data(data);

        chart(d3.select(this.$el.get(0)));


        var yAxis = this.$el.children("g").get(2);

        var y = 10;
        var tickIncrementer = 5; //Do not like this.

        $(yAxis).find(".tick").each(function(index, tick) {
            var label = $(tick).find("text");
            var labelValue = label.text();

            equal(labelValue, "$" + y, "Tick " + index + " does not have the correct value");
            y += tickIncrementer;
        });

        var xAxis = this.$el.children("g").get(1);
        ok($(xAxis).is(':empty'), "Should have no ticks in the xAxis");
    });
});
