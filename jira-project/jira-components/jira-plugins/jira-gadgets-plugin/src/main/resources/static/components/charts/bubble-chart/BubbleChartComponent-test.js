AJS.test.require([
    'com.atlassian.jira.gadgets:common-test-resources',
    'com.atlassian.jira.gadgets:bubble-chart-component'
], function () {
    'use strict';

        var BubbleChartComponent = require('jira-dashboard-items/components/charts/bubble-chart/bubble-chart-component');
        var d3 = require('jira-dashboard-items/lib/d3');
        var $ = require('jquery');
        var _ = require('underscore');
        var Backbone = require('backbone');

        var WIDTH = '300';
        var HEIGHT = '300';
        var COLORS = ['red', 'white', 'blue'];
        var DATA = [
            { bubbleDomain: 1, bubbleRange: 2, bubbleRadius: 3, bubbleColorValue: 4, key: 'JC-1'},
            { bubbleDomain: 2, bubbleRange: 3, bubbleRadius: 4, bubbleColorValue: 5, key: 'JC-2'},
            { bubbleDomain: 100, bubbleRange: 4, bubbleRadius: 5, bubbleColorValue: 6, key: 'JC-3'}
        ];

        var LARGE_DATA = [
            {bubbleDomain: 2.75, bubbleRange: 5, bubbleRadius: 14, bubbleColorValue: 5, key: 'JC-2'},
            {bubbleDomain: 2, bubbleRange: 2, bubbleRadius: 1, bubbleColorValue: 6, key: 'JC-3'},
            {bubbleDomain: 3, bubbleRange: 5, bubbleRadius: 7, bubbleColorValue: 1, key: 'JC-4'},
            {bubbleDomain: 4, bubbleRange: 1, bubbleRadius: 5, bubbleColorValue: 2, key: 'JC-5'},
            {bubbleDomain: 5, bubbleRange: 7, bubbleRadius: 4, bubbleColorValue: 3, key: 'JC-6'},
            {bubbleDomain: 121, bubbleRange: 4, bubbleRadius: 7, bubbleColorValue: 5, key: 'JC-7'},
            {bubbleDomain: 2, bubbleRange: 2, bubbleRadius: 3, bubbleColorValue: 2, key: 'JC-8'},
            {bubbleDomain: 3.25, bubbleRange: 5.25, bubbleRadius:  11, bubbleColorValue: 7, key: 'JC-9'},
            {bubbleDomain: 4.1, bubbleRange: 5.7, bubbleRadius:  5.8, bubbleColorValue: 1.7, key: 'JC-20' },
            {bubbleDomain: 3, bubbleRange: 8.1, bubbleRadius:  4.1, bubbleColorValue: 9.1, key: 'JC-21' },
            {bubbleDomain: 1.9, bubbleRange: 3.5, bubbleRadius:  2.3, bubbleColorValue: 4.3, key: 'JC-22' },
            {bubbleDomain: 1.9, bubbleRange: 2.5, bubbleRadius:  3.9, bubbleColorValue: 1.1, key: 'JC-23' },
            {bubbleDomain: 2.8, bubbleRange: 6.5, bubbleRadius:  4.3, bubbleColorValue: 7, key: 'JC-25' },
            {bubbleDomain: 2.2, bubbleRange: 2.5, bubbleRadius:  7.7, bubbleColorValue: 6.7, key: 'JC-26' },
            {bubbleDomain: 3.4, bubbleRange: 5.1, bubbleRadius: 3.2, bubbleColorValue: 8.9, key: 'JC-27' },
            {bubbleDomain: 2.9, bubbleRange: 6.2, bubbleRadius: 4.5, bubbleColorValue: 2.5, key: 'JC-28' },
            {bubbleDomain: 3.6, bubbleRange: 7, bubbleRadius: 4.7, bubbleColorValue: 8.3, key: 'JC-29' },
            {bubbleDomain: 2, bubbleRange: 5.6, bubbleRadius: 4.2, bubbleColorValue: 9.7, key: 'JC-30' },
            {bubbleDomain: 3, bubbleRange: 2.8, bubbleRadius: 9.8, bubbleColorValue: 9.7, key: 'JC-31' },
            {bubbleDomain: 4.7, bubbleRange: 1.8, bubbleRadius: 5.4, bubbleColorValue: 4.1, key: 'JC-32' },
            {bubbleDomain: 21.6, bubbleRange: 4.5, bubbleRadius: 5.1, bubbleColorValue: 9.2, key: 'JC-33' },
            {bubbleDomain: 4.5, bubbleRange: 8, bubbleRadius: 4, bubbleColorValue: 7.8, key: 'JC-34' },
            {bubbleDomain: 2.3, bubbleRange: 6, bubbleRadius: 4.8, bubbleColorValue: 9.1, key: 'JC-35' },
            {bubbleDomain: 2.2, bubbleRange: 3, bubbleRadius: 5.4, bubbleColorValue: 2.4, key: 'JC-36' },
            {bubbleDomain: 3.9, bubbleRange: 4.3, bubbleRadius: 5.5, bubbleColorValue: 6.1, key: 'JC-37' },
            {bubbleDomain: 4.7, bubbleRange: 2.6, bubbleRadius: 4.8, bubbleColorValue: 3.3, key: 'JC-38' },
            {bubbleDomain: 51.7, bubbleRange: 8.3, bubbleRadius: 8.4, bubbleColorValue: 4.8, key: 'JC-39' }
        ];

        var POSITIONING_TEST_DATA = [
            { bubbleDomain: 1, bubbleRange: 5, bubbleRadius: 14, bubbleColorValue: 5, key: 'JC-2'},
            { bubbleDomain: 10, bubbleRange: 25, bubbleRadius:  1, bubbleColorValue: 6, key: 'JC-3'},
            { bubbleDomain: 100, bubbleRange: 75, bubbleRadius:  7, bubbleColorValue: 1, key: 'JC-4'}
        ];

        // Using jQuery's `.click()`, `.mouseover()`, etc. event triggers doesn't work well with d3.js.
        // We need to fire events using regular DOM methods.
        // See: http://stackoverflow.com/questions/9063383/how-to-invoke-click-event-programmaticaly-in-d3.
        $.fn.d3Trigger = function(eventName) {
            this.each(function(index, element) {
                var event = new UIEvent(eventName);
                element.dispatchEvent(event);
            });
        };

        module('jira-dashboard-items/components/charts/bubble-chart/bubble-chart-component', {
            setup: function () {
                this.element = d3.select('#qunit-fixture')
                    .append('div')
                    .attr('width', WIDTH)
                    .attr('height', HEIGHT)
                    [0];
            },

            teardown: function () {
                $('.tipsy').remove();
            },

            renderBubbleChart: function(data, useLogarithmicScale, useRelativeColoring) {
                this.model = new Backbone.Model({
                    xAxisLabel: 'x-axis',
                    yAxisLabel: 'y-axis',
                    useLogarithmicScale: useLogarithmicScale,
                    useRelativeColoring: useRelativeColoring,
                    colors: COLORS,
                    colorDomain: COLORS,
                    data: data
                });

                this.model.getIssueForIssueKey = function() { return {}; };

                this.bubbleChartComponent = new BubbleChartComponent({
                    model: this.model,
                    el: this.element,
                    id: 10000
                });

                this.bubbleChartComponent.render();
            },

            assertAxisIsCorrect: function(axisSelector, axisLabelContents) {
                var gridTicks = $(this.element).find(axisSelector + ' .grid-tick');
                var valueTicks = $(this.element).find(axisSelector + ' .value-tick');
                var valueLabels = $(this.element).find(axisSelector + ' .tick text');
                var valueLabelsContents = _.map(valueLabels, function(el, _) { return el.innerHTML; });

                // BubbleChartComponent should produce 1 label for each grid tick
                // and there should only a value tick for each grid tick (this overrides d3's default behavior).
                equal(gridTicks.length, valueLabels.length);
                equal(gridTicks.length, valueTicks.length);
                deepEqual(valueLabelsContents, axisLabelContents);
            },

            assertXYPositionsOfCirclesAreCorrect: function(data, useLogarithmicScale, useRelativeColoring, expected) {
                this.renderBubbleChart(data, useLogarithmicScale, useRelativeColoring);

                var xyPairs = _.map($(this.element).find('circle'), function(el) {
                    return [Math.round($(el).attr('cx')), Math.round($(el).attr('cy'))];
                });

                deepEqual(xyPairs, expected);
            },

            stubInlineDialog: function() {
                // Don't actually show or hide the dialog
                return this.stub(AJS, 'InlineDialog').returns({
                    show: function() {},
                    hide: function() {}
                });
            }
        });

        test('Should appropriately position circles for a linear scale', function() {
            var expectedPairs = [
                [0, 450],
                [82, 331],
                [905, 35]
            ];

            this.assertXYPositionsOfCirclesAreCorrect(POSITIONING_TEST_DATA, false, true, expectedPairs);
        });

        test('Should appropriately position circles for a logarithmic scale', function() {
            var expectedPairs = [
                [0, 450],
                [453, 203],
                [905, 35]
            ];

            this.assertXYPositionsOfCirclesAreCorrect(POSITIONING_TEST_DATA, true, true, expectedPairs);
        });

        test('Should apply grid lines, value labels, and value ticks to a linear scale bubble chart', function() {
            this.renderBubbleChart(LARGE_DATA, false, true);

            this.assertAxisIsCorrect('.xAxis', ['50', '100']);
            this.assertAxisIsCorrect('.yAxis', ['2', '4', '6', '8']);
        });

        test('Should apply grid lines, value labels, and value ticks to a logarithmic scale bubble chart', function() {
            this.renderBubbleChart(LARGE_DATA, true, false);

            this.assertAxisIsCorrect('.xAxis', ['2', '10', '20', '100']);
            this.assertAxisIsCorrect('.yAxis', ['1', '2', '3', '4', '5', '6']);
        });

        test('Should update the model when the mouse enters a bubble', function() {
            this.renderBubbleChart(DATA, true, false);
            var bubble = $(this.element).find('circle')[0];

            $(bubble).d3Trigger('mouseenter');

            var highlightedCircle = this.model.get('highlightedCircle');
            equal(highlightedCircle.colorBucket, 2);
            equal(highlightedCircle.index, 0);
            equal(highlightedCircle.issueKey, 'JC-1');
        });

        test('Should update the model when the mouse leaves a bubble', function() {
            this.renderBubbleChart(DATA, true, false);
            var bubble = $(this.element).find('circle')[0];

            $(bubble).d3Trigger('mouseleave');

            var highlightedCircle = this.model.get('highlightedCircle');
            equal(highlightedCircle.issueKey, undefined);
            equal(highlightedCircle.colorBucket, undefined);
        });

        test('Should update the model when the user clicks a bubble', function() {
            this.renderBubbleChart(DATA, true, false);
            var bubble = $(this.element).find('circle')[1];
            this.stubInlineDialog();

            $(bubble).d3Trigger('click');

            var selectedCircle = this.model.get('selectedCircle');
            equal(selectedCircle.colorBucket, 2);
            equal(selectedCircle.issueKey, 'JC-2');
        });

        test("Should create a tooltip with the data element's title when the mouse hovers over a bubble", function() {
            var chart = this.renderBubbleChart(DATA, true, false);

            for (var i = 0; i < DATA.length; i++) {
                var bubble = $(this.element).find('circle')[i];
                $(bubble).d3Trigger('mouseenter');
                equal($('.tipsy .tipsy-inner').text(), DATA[i].key);
                $('.tipsy').remove();
            }
        });

        test('Should create an inline dialog when the mouse clicks on a bubble', function() {
            this.renderBubbleChart(DATA, true, false);
            var spy = this.stubInlineDialog();
            var bubbles = $(this.element).find('circle');

            for (var i = 0; i < 3; i++) {
                var bubble = bubbles[i];
                $(bubble).d3Trigger('click');
                deepEqual(spy.args[i][0].get(0), bubble);
                equal(spy.args[i][1], 'bubble-chart-dialog-' + DATA[i].key);
            }

            sinon.assert.calledThrice(spy);
        });

        test('Should draw a backing circle underneath a circle that has been clicked', function() {
            this.renderBubbleChart(DATA, true, true);
            this.stubInlineDialog();
            var bubble = $(this.element).find('circle')[1];
            $(bubble).d3Trigger('click');

            ok(bubble.classList.contains('selected'));
            ok($(bubble).prev().get(0).classList.contains('backing'));

            // If we click another circle...
            var newBubble = $(this.element).find('circle')[0];
            $(newBubble).d3Trigger('click');

            // We can't test that the old circle has its classes removed since that happens when the AUI inline dialog
            // is hidden (but we don't actually create one here).
            // Just check that the new bubble gets properly classed.
            ok(newBubble.classList.contains('selected'));
            ok($(newBubble).prev().get(0).classList.contains('backing'));
        });
});
