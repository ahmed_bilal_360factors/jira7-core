define("jira-dashboard-items/common-functions", [
    'underscore'
], function(
    _
) {

    //TODO: Move this to Atlassian Gadgets plugin.
    /**
     * Given a set of preferences with a projectOrFilterId, creates a type and id preference.
     * @param preferences for the DashboardItem
     * @returns {Object}
     */
    function projectFilterBackwardCompatible(preferences) {
        var newPreferences = _.extend({}, preferences);
        if (newPreferences.projectOrFilterId && (!newPreferences.type || !newPreferences.id)) {
            newPreferences.name = newPreferences.projectOrFilterId;

            var projectOrFilterSplit = newPreferences.projectOrFilterId.split("-");
            if (projectOrFilterSplit.length === 2) {
                newPreferences.type = projectOrFilterSplit[0];
                newPreferences.id = projectOrFilterSplit[1];
            }
        }
        delete newPreferences.projectOrFilterId;
        return newPreferences;
    }


    /**
     * Given a set of preferences in the new project or filter style, revert to the old preferences.
     * @param preferences for the DashboardItem
     * @returns {Object} gadget preferences
     */
    function getSafeLegacyProjectOrFilterId(preferences) {
        var newPreferences = _.extend({}, preferences);
        if (!newPreferences.projectOrFilterId && newPreferences.type && newPreferences.id) {
            newPreferences.projectOrFilterId = newPreferences.type + '-' + newPreferences.id;
        }

        delete newPreferences.id;
        delete newPreferences.type;
        return newPreferences;
    }

    return {
        projectFilterBackwardCompatible: projectFilterBackwardCompatible,
        getSafeLegacyProjectOrFilterId: getSafeLegacyProjectOrFilterId
    };
});
