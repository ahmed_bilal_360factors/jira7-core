AJS.test.require([
    'com.atlassian.jira.gadgets:common-test-resources',
    'com.atlassian.jira.gadgets:relative-distribution'
], function () {
        'use strict';

        var RelativeDistribution = require('jira-dashboard-items/components/charts/components/relative-distribution');

        module('jira-dashboard-items/components/charts/components/relative-distribution');

        test('Should compute the slice of an array for n distinct values', function() {
            var values = [1, 1, 1, 2, 2, 3, 3, 3, 3, 3, 4, 4];
            var result1 = RelativeDistribution.sortedArraySliceForUniqueValueCount(values, 2);
            equal(result1, 4);

            var result2 = RelativeDistribution.sortedArraySliceForUniqueValueCount(values, 1);
            equal(result2, 2);

            var result3 = RelativeDistribution.sortedArraySliceForUniqueValueCount(values, 3);
            equal(result3, 9);

            var result4 = RelativeDistribution.sortedArraySliceForUniqueValueCount(values, 13);
            equal(result4, values.length - 1);
        });

        test('Should distribute values relative to population size', function() {
            var input = [10, 0, 10, 10, 40, 10, 10, 65, 20, 30, 60, 65, 65, 90, 100, 85, 65, 70, 80, 100, 85];
            var output = RelativeDistribution.distribute(input, [0.1, 0.2, 0.4, 0.2, 0.1]);
            var expected = [
                [0, 10, 10, 10, 10, 10],
                [20, 30, 40],
                [60, 65, 65, 65, 65, 70, 80, 85, 85],
                [90, 100, 100],
                []
            ];

            deepEqual(output, expected);
        });

        test('Should be able to distribute a small population', function() {
            var input = [10, 4];
            var output = RelativeDistribution.distribute(input, [0.3, 0.7]);
            var expected = [[4], [10]];
            deepEqual(output, expected);
        });

        test('Should be able to distribute negative numbers', function() {
            var input = [-5, -5, 20, 15, -5, 2];
            var output = RelativeDistribution.distribute(input, [0.3, 0.6, 0.1]);
            var expected = [
                [-5, -5, -5, 2],
                [15, 20],
                []
            ];

            deepEqual(output, expected);
        });

        test('Should be able to distribute all values into one bucket', function() {
            var input = [10, 20, 5, 3, 4, 8, 20];
            var output = RelativeDistribution.distribute(input, [1.0]);
            var expected = [
                [3, 4, 5, 8, 10, 20, 20]
            ];

            deepEqual(output, expected);
        });

        test('Should be able to distribute a single value into the first bucket', function() {
            var input = [5];
            var output = RelativeDistribution.distribute(input, [0.1, 0.3, 0.2, 0.4]);
            var expected = [
                [5],
                [], [], []
            ];

            deepEqual(output, expected);
        });

        test('Should throw an error if the percentage values do not add up to 1', function() {
            var input = [5];
            raises(function() {
                RelativeDistribution.distribute(input, [0.4, 0.1]);
            });
        });
});
