AJS.test.require('com.atlassian.jira.gadgets:number-factor-generator', function() {

    var NumberFactorGenerator = require('jira-dashboard-items/components/factors/number-factor-generator');


    module('Components.Factors.NumberFactorGenerator');

    test('Number smaller than 1 returns an error', function() {

        try {
            NumberFactorGenerator.generate(0);
            ok(false);
        } catch (err) {
            ok(true);
        }
    });

    test('Generating factors for 1 returns only 1 as a factor', function() {
        var results = NumberFactorGenerator.generate(1);
        deepEqual(results, [1], 'Should only return one factor for 1');
    });


    test('Generate only 2 factors for a prime number', function() {
        var results = NumberFactorGenerator.generate(13);
        deepEqual(results, [1, 13]);
    });

    test('Generate correct factors for a non-prime number', function() {
        var results = NumberFactorGenerator.generate(9);
        deepEqual(results, [1, 3, 9], '9 should have 3 factors');

        var results = NumberFactorGenerator.generate(24);
        deepEqual(results, [1, 2, 3, 4, 6, 8, 12, 24], '24 should have 8 factors');
    });
});
