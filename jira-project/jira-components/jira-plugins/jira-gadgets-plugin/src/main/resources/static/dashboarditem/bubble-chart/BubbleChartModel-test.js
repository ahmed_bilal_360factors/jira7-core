AJS.test.require([
    'com.atlassian.jira.gadgets:common-test-resources',
    'com.atlassian.jira.gadgets:bubble-chart-model'
], function () {
    require([
        'jira-dashboard-items/bubble-chart-model'
    ], function (
        BubbleChartModel
    ) {
        'use strict';

        var NETWORK_RESPONSE = {
            data: [
                { key: 'txt1', bubbleDomain: 1.3, bubbleRange: 1.4, bubbleRadius: 2.3, bubbleColorValue: 4.5 },
                { key: 'txt2', bubbleDomain: 3.1, bubbleRange: 6.9, bubbleRadius: 4.3, bubbleColorValue: 2.7 },
                { key: 'txt3', bubbleDomain: 3.1, bubbleRange: 0, bubbleRadius: 4.3, bubbleColorValue: 3.1 },
                { key: 'txt4', bubbleDomain: 0, bubbleRange: 6.9, bubbleRadius: 1.3, bubbleColorValue: 7.4 },
                { key: 'txt5', bubbleDomain: 123.1, bubbleRange: 111, bubbleRadius: 4.3, bubbleColorValue: 0.7 },
                { key: 'txt6', bubbleDomain: 0, bubbleRange: 6.9, bubbleRadius: 2.9, bubbleColorValue: 5.2 }
            ],
            filterTitle: 'some_filter',
            issueCount: 6
        };

        var EXCESSIVE_NETWORK_RESPONSE = {
            data: [
                { key: 'txt1', bubbleDomain: 1.3, bubbleRange: 1.4, bubbleRadius: 2.3, bubbleColorValue: 4.5 },
                { key: 'txt2', bubbleDomain: 3.1, bubbleRange: 6.9, bubbleRadius: 4.3, bubbleColorValue: 2.7 },
                { key: 'txt3', bubbleDomain: 3.1, bubbleRange: 0, bubbleRadius: 4.3, bubbleColorValue: 3.1 },
                { key: 'txt4', bubbleDomain: 0, bubbleRange: 6.9, bubbleRadius: 1.3, bubbleColorValue: 7.4 },
                { key: 'txt5', bubbleDomain: 123.1, bubbleRange: 111, bubbleRadius: 4.3, bubbleColorValue: 0.7 },
                { key: 'txt6', bubbleDomain: 0, bubbleRange: 6.9, bubbleRadius: 2.9, bubbleColorValue: 5.2 }
            ],
            filterTitle: 'some_filter',
            issueCount: 306,
            warning: 'bubble-charts.exceed.issues'
        };

        var aDataItem = EXCESSIVE_NETWORK_RESPONSE.data[2];
        for (var i = 0; i < 300; i++) {
            EXCESSIVE_NETWORK_RESPONSE.data.push(aDataItem);
        }

        module('jira-dashboard-items/bubble-chart', {
            setup: function () {
                this.server = sinon.fakeServer.create();
            },

            teardown: function () {
                this.server.restore();
            },

            assertModelAttributesAreCorrect: function(model, response, sizeAxis) {
                equal(model.get('keyLabel'), 'bubble-chart.key.axis.title.label');
                equal(model.get('xAxisLabel'), 'bubble-chart.x.axis.title.label');
                equal(model.get('yAxisLabel'), 'bubble-chart.y.axis.title.label');
                equal(model.get('sizeAxisLabel'), sizeAxis);
                equal(model.get('colorAxisLabel'), 'bubble-chart.color.axis.title.label');
                equal(model.get('filterTitle'), response.filterTitle);
                equal(model.get('issueCount'), response.data.length);
            },

            assertLegendDataIsCorrect: function(model) {
                deepEqual(model.get('legendData'), [
                    'bubble-chart.legend.low', '', '', '', '', 'bubble-chart.legend.high'
                ]);
            }
        });

        test('Should set the correct attributes for absolute coloring and linear scale', function() {
            var model = new BubbleChartModel({
                projectOrFilterId: 'proj',
                bubbleType: 'votes',
                recentCommentsPeriod: 7,
                useRelativeColoring: false
            });

            model.fetch();
            this.server.requests[0].respond(200, {'Content-Type': 'application/json'}, JSON.stringify(NETWORK_RESPONSE));

            this.assertModelAttributesAreCorrect(model, NETWORK_RESPONSE, 'bubble-chart.votes');
            this.assertLegendDataIsCorrect(model);
            equal(model.get('useRelativeColoring'), false);
            equal(model.get('bubbleType'), 'votes');
            deepEqual(model.get('colorDomain'), [0.7, 7.4]);
            deepEqual(model.get('data'), NETWORK_RESPONSE.data);
        });

        test('Should set the correct attributes for relative coloring and log scale', function() {
            var model = new BubbleChartModel({
                projectOrFilterId: 'proj',
                bubbleType: 'participants',
                recentCommentsPeriod: 28,
                useLogarithmicScale: true,
                useRelativeColoring: true
            });

            model.fetch();
            this.server.requests[0].respond(200, {'Content-Type': 'application/json'}, JSON.stringify(NETWORK_RESPONSE));

            this.assertModelAttributesAreCorrect(model, NETWORK_RESPONSE, 'bubble-chart.participants');
            this.assertLegendDataIsCorrect(model);
            equal(model.get('useRelativeColoring'), true);
            equal(model.get('bubbleType'), 'participants');
            deepEqual(model.get('colorDomain'), [0.7, 2.7, 4.5]);
            deepEqual(model.get('data'), [
                { bubbleDomain: 1.3, bubbleRange: 1.4, bubbleRadius: 2.3, bubbleColorValue: 4.5, key: 'txt1' },
                { bubbleDomain: 3.1, bubbleRange: 6.9, bubbleRadius: 4.3, bubbleColorValue: 2.7, key: 'txt2' },
                { bubbleDomain: 123.1, bubbleRange: 111, bubbleRadius: 4.3, bubbleColorValue: 0.7, key: 'txt5' }
            ]);
        });

        test('Should set the correct flag if the issue limit is exceeded', function() {
            var model = new BubbleChartModel({
                projectOrFilterId: 'proj',
                bubbleType: 'participants',
                recentCommentsPeriod: 28,
                useLogarithmicScale: true,
                useRelativeColoring: true
            });

            model.fetch();
            this.server.requests[0].respond(200, {'Content-Type': 'application/json'}, JSON.stringify(EXCESSIVE_NETWORK_RESPONSE));

            equal(model.get('exceedsIssueLimit'), true);
        });

        test('Should make a request to the correct url', function() {
            var model = new BubbleChartModel({
                projectOrFilterId: 'filter-10101',
                bubbleType: 'participants',
                recentCommentsPeriod: 28
            });

            model.fetch();
            this.server.requests[0].respond(200, {'Content-Type': 'application/json'}, JSON.stringify(NETWORK_RESPONSE));

            ok(this.server.requests[0].url.indexOf(
                AJS.contextPath() + '/rest/gadget/1.0/bubblechart/generate?projectOrFilterId=filter-10101&bubbleType=participants&recentCommentsPeriod=28'
            ) !== -1);
        });

        test('Should be able to determine the correct segment of the domain for a value', function() {
            var model = new BubbleChartModel({
                projectOrFilterId: 'filter-10101',
                bubbleType: 'participants',
                recentCommentsPeriod: 28
            });

            model.fetch();
            this.server.requests[0].respond(200, {'Content-Type': 'application/json'}, JSON.stringify(NETWORK_RESPONSE));

            equal(model.indexInDomainForValue(0.2), 0);
            equal(model.indexInDomainForValue(1.9), 1);
            equal(model.indexInDomainForValue(2.3), 2);
            equal(model.indexInDomainForValue(2.9), 3);
        });
    });
});
