AJS.test.require([
    "com.atlassian.jira.gadgets:common-test-resources",
    "com.atlassian.jira.gadgets:favourite-filters-dashboard-item-resources"
], function() {
    'use strict';

    var $ = require('jquery');
    var _ = require('underscore');
    var FavouriteFiltersDashboardItem = require('jira-dashboard-items/favourite-filters');
    var sampleResponseWithCounts = {"filters":[{"label":"Three","value":10302,"count":22},{"label":"Two","value":10301,"count":27}],"includeCount":true};
    var sampleResponseWithoutCounts = {"filters":[{"label":"Three","value":10302},{"label":"Two","value":10301}],"includeCount":false};

    module('Favourite Filters dashboard item tests', {
        setup: function () {
            this.$el = $("<div/>");
            this.API = $.extend({}, DashboardItem.Mocks.API);
            this.dashboardItem = new FavouriteFiltersDashboardItem(this.API);

            $("#qunit-fixture").append(this.$el);
            this.server = sinon.fakeServer.create();
        },
        teardown: function () {
            this.server.restore();
        },

        assertCorrectFilters: function(filters) {
            var $filterTable = this.$el.find(".filter-list-content table");
            equal($filterTable.find("tr").length, filters.length, "Same number of filters");

            _.each(filters, function(filter, i) {
                var filterRow = $($filterTable.find("tr")[i]);

                equal($(filterRow.find("td")[0]).text(), filter.name, "Filter name matches");
                if(filter.count === null) {
                    equal(filterRow.find("td").length, 1, "Should only be one table column for each filter");
                } else {
                    equal($(filterRow.find("td")[1]).text(), filter.count, "Filter count matches");
                }
            });
        },

        assertOperationsPresent: function() {
            var operationLinks = this.$el.find(".operations-list a");

            equal($(operationLinks[0]).text(), "gadget.favourite.filters.create.new.filter", "Create new link present");
            equal($(operationLinks[1]).text(), "gadget.favourite.filters.manage.filters", "Manage link present");
        }
    });

    test("Server error renders error message in item", function() {
        this.dashboardItem.render(this.$el, {showCounts:true});

        this.server.requests[0].respond(500, { "Content-Type": "application/json" }, JSON.stringify({}));
        equal(this.$el.text(), "dashboard.item.favourite.filters.error", "Should have shown appropriate error on server error");
    });

    test("Empty result renders empty view", function() {
        this.dashboardItem.render(this.$el, {showCounts:true});

        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, JSON.stringify({filters:[]}));
        equal(this.$el.find(".empty-msg").text(), "dashboard.item.favourite.filters.empty", "Empty message is shown");

        var buttons = this.$el.find(".aui-button");
        equal(buttons.length, 2, "Should have 2 buttons to create and manage filters");
        equal($(buttons[0]).text(), "gadget.favourite.filters.create.new.filter", "Create button has correct text");
        equal($(buttons[1]).text(), "gadget.favourite.filters.manage.filters", "Manage button has correct text");
    });

    test("Results with counts are rendered correctly", function() {
        this.dashboardItem.render(this.$el, {showCounts:true});

        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, JSON.stringify(sampleResponseWithCounts));

        this.assertCorrectFilters([{name: "Three", count:22}, { name: "Two", count: 27}]);
        this.assertOperationsPresent();
    });

    test("Results without counts are rendered correctly", function() {
        this.dashboardItem.render(this.$el, {showCounts:false});

        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, JSON.stringify(sampleResponseWithoutCounts));

        this.assertCorrectFilters([{name: "Three", count:null}, { name: "Two", count: null}]);
        this.assertOperationsPresent();
    });

    test("Config prefs are saved correctly", function() {
        var savePreferencesSpy = sinon.spy();
        sinon.stub(this.dashboardItem.API, "savePreferences", savePreferencesSpy);

        this.dashboardItem.renderEdit(this.$el, {});

        $("form", this.$el).submit();

        var prefs = savePreferencesSpy.args[0][0];
        equal(prefs.showCounts, false, "Show counts wasn't checked so should be false");
    });

    test("Config prefs are saved correctly when showCounts is checked", function() {
        var savePreferencesSpy = sinon.spy();
        sinon.stub(this.dashboardItem.API, "savePreferences", savePreferencesSpy);

        this.dashboardItem.renderEdit(this.$el, {});

        this.$el.find("input[name=showCounts]").prop("checked", true);
        $("form", this.$el).submit();

        var prefs = savePreferencesSpy.args[0][0];
        equal(prefs.showCounts, true, "Show counts wasn checked so should be true");
    });
});
