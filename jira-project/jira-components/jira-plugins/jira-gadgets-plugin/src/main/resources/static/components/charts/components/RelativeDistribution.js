define('jira-dashboard-items/components/charts/components/relative-distribution', function() {
    'use strict';

    return {
        /**
         * When called on a sorted array, this method returns the end index of the slice that contains 'x' unique values.
         *
         * For example, [4, 4, 5, 5, 5, 6, 6, 7].sortedArraySliceForUniqueValueCount(2) will return 4, the index
         * of the last 5 in the list.
         *
         * @param array The sorted array to use.
         * @param x The number of unique values to include in the slice.
         * @returns {Integer} The index of the last unique value within the count of 'x'
         */
        sortedArraySliceForUniqueValueCount: function(array, x) {
            var countDistinct = 0;

            for (var i = 0; i < array.length; i++) {
                if (i === 0) {
                    countDistinct++;
                }

                if (i > 0 && array[i] !== array[i - 1]) {
                    countDistinct++;
                }

                if (countDistinct > x) {
                    return i - 1;
                }
            }

            return (array.length - 1);
        },

        /**
         * A distribution of values into population-sized percentage buckets.
         * This takes a collection of values and the percentage of the unique population that will be the size
         * of each partitioned subsegment.
         *
         * A good way to think about this is like a teacher marking exams. The teacher has a list of marks (`values`) and
         * wants to make sure that 10% of kids get an F, 20% get a D, 40% get a C, 20% get a B, and 10% get an A.
         * This module can compute that; in this case, the `percentages` argument should be [0.1, 0.2, 0.4, 0.2, 0.1].
         *
         * An important thing to note is that this module works on distinct values in the `values` array. This means that
         * identical values will be placed into the same bucket, regardless of how much it messes up the distribution
         * of the population in the result.
         *
         * If the percentages produce non-integer size, we _always round up_. This means the top-most bucket
         * will often be empty.
         *
         * @param values The values to be distributed
         * @param percentages The percentage of the total population size that each partition should contain. These should add up to 1.
         * @returns {Array} A 2D array of the values distributed into percentage buckets.
         * @throws {Error} An error if the percentages do not add up to 1.0 (100%)
         */
        distribute: function RelativeDistribution(values, percentages) {
            var total = parseFloat(percentages.reduce(function(a, b) { return a + b; }).toPrecision(3));
            if (total !== 1) {
                throw new Error('Percentages given to RelativeDistribution#distribute do not add up to 1.');
            }

            // Numeric sort on the input data (so that we can place equal items into the same bucket later)
            var sorted = values.sort(function (a, b) { return a - b; });

            // Get a count of how many times each data value occurs
            var unique = {};

            for (var i = 0; i < sorted.length; i++) {
                if (unique[sorted[i]]) {
                    unique[sorted[i]] = unique[sorted[i]]++;
                }
                else {
                    unique[sorted[i]] = 0;
                }
            }

            var numberOfUniqueValues = Object.keys(unique).length;

            // Compute how many items should go into each percentage bucket
            var percentageValueCounts = {};

            for (var i = 0; i < percentages.length; i++) {
                var numberOfUniqueValuesToInclude = percentages[i] * numberOfUniqueValues;
                numberOfUniqueValuesToInclude = Math.ceil(numberOfUniqueValuesToInclude);
                percentageValueCounts[percentages[i]] = numberOfUniqueValuesToInclude;
            }

            // Fill each percentage bucket
            var result = [];
            var valueIndex = 0;

            for (var bucket = 0; bucket < percentages.length; bucket++) {
                var sliceToEnd = sorted.slice(valueIndex);
                var valueCount = percentageValueCounts[percentages[bucket]];
                var endIndex = valueIndex + this.sortedArraySliceForUniqueValueCount(sliceToEnd, valueCount);

                result[bucket] = [];

                sorted.slice(valueIndex, endIndex + 1).forEach(function(v) {
                    result[bucket].push(v);
                });

                valueIndex = endIndex + 1;
            }

            return result;
        }
    };
});
