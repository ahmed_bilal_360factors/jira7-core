AJS.test.require([
    'com.atlassian.jira.gadgets:common-test-resources',
    'com.atlassian.jira.gadgets:colorable'
], function () {

        'use strict';

        var _ = require('underscore');
        var d3 = require('jira-dashboard-items/lib/d3');
        var Colorable = require('jira-dashboard-items/components/charts/components/colorable');

        var DATA = [1, 2, 3, 4, 5];

        module('jira-dashboard-items/components/charts/components/colorable', {
            setup: function() {
                this.colorable = Colorable();

                this.colorableWithAccessorAndData = Colorable()
                    .colorAccessor(function(d) { return d; })
                    .data(DATA);
            }
        });

        test('Should be able to create a colorable component with default domain and range', function() {
            deepEqual(this.colorableWithAccessorAndData.colorDomain(), [1, 5]);
            deepEqual(this.colorableWithAccessorAndData.colorRange(), ['red', 'blue']);
        });

        test('Should compute the correct color with default domain and range', function() {
            equal(this.colorableWithAccessorAndData.color()(1), '#ff0000'); // red
            equal(this.colorableWithAccessorAndData.color()(2), '#bf0040');
            equal(this.colorableWithAccessorAndData.color()(3), '#800080'); // purple (50%)
            equal(this.colorableWithAccessorAndData.color()(4), '#4000bf');
            equal(this.colorableWithAccessorAndData.color()(5), '#0000ff'); // blue
        });

        test('Should provide a default color accessor', function() {
            ok(_.isFunction(this.colorable.colorAccessor));
            ok(_.isFunction(this.colorable.colorAccessor()));
            equal(this.colorable.colorAccessor().call(this.colorable), 4);
        });

        test('Should provide a default color domain if data is provided', function() {
            this.colorable.data([1, 2, 3]);

            deepEqual(this.colorable.colorDomain(), [4, 4]);
        });

        test('Should provide a default color range', function() {
            deepEqual(this.colorable.colorRange(), ['red', 'blue']);
        });

        test('Should provide a default color scale constructor', function() {
            equal(this.colorable.colorScaleConstructor(), d3.scale.linear);
        });

        test('Should provide a default color scale', function() {
            this.colorable.data([1, 2, 3]);
            var expectedScale = d3.scale.linear().domain([4, 4]).range(['red', 'blue']);
            var actualScale = this.colorable.colorScale();

            ok(_.isFunction(this.colorable.colorScale));
            equal(expectedScale(4), actualScale(4));
            equal(expectedScale(1), actualScale(1));
        });
});
