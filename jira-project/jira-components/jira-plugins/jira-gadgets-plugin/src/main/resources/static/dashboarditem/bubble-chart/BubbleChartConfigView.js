define('jira-dashboard-items/bubble-chart-config-view', [
    'jira/util/formatter',
    'jquery',
    'backbone',
    'underscore',
    'jira-dashboard-items/components/autocomplete/project-filter-autocomplete'
], function(formatter, $, Backbone, _, ProjectFilterAutoComplete) {
    'use strict';

    var BubbleChartConfigView = Backbone.View.extend({
        events: {
            'click .cancel': 'cancel',
            'submit': 'submit'
        },

        initialize: function(options) {
            this.id = options.id;
            this.intervals = options.intervals;
            this.types = options.types;
            this.projectFilterDelay = options.projectFilterDelay;

            this.intervalOptions = [
                {value: '1', text: formatter.I18n.getText('bubble-chart.one.day')},
                {value: '7', text: formatter.I18n.getText('bubble-chart.one.week')},
                {value: '14', text: formatter.I18n.getText('bubble-chart.two.weeks')},
                {value: '28', text: formatter.I18n.getText('bubble-chart.four.weeks')},
                {value: '57', text: formatter.I18n.getText('bubble-chart.eight.weeks')},
                {value: '84', text: formatter.I18n.getText('bubble-chart.twelve.weeks')}
            ];

            this.typeOptions = [
                {value: 'participants', text: formatter.I18n.getText('bubble-chart.participants')},
                {value: 'votes', text: formatter.I18n.getText('bubble-chart.votes')}
            ];

        },

        render: function (preferences) {
            var newPreferences = _.clone(preferences || {});
            newPreferences = _.defaults(newPreferences, this._defaultPreferences);
            newPreferences.refresh = newPreferences.refresh === '15';

            this.$el.html(JIRA.DashboardItem.BubbleChart.Templates.Configuration({
                prefix: this.id + '-',
                preferences: newPreferences,
                intervals: this.intervalOptions,
                types: this.typeOptions,
                relativeColoringCheckboxes: this._relativeColoringCheckboxes(newPreferences),
                logarithmicScaleCheckboxes: this._logarithmicScaleCheckboxes(newPreferences)
            }));

            this._bindProjectFilterAutocomplete();
        },

        /**
         * Get the default preferences for the gadget
         * @returns {Object} An object containing the default preferences
         * @private
         */
        _defaultPreferences: {
            isConfigured: false,
            useRelativeColoring: true,
            recentCommentsPeriod: 7,
            refresh: '15',
            useLogarithmicScale: false
        },

        /**
         * Bind the component used to select the project or filter for the gadget
         * @private
         */
        _bindProjectFilterAutocomplete: function () {
            var bubbleChartConfigView = this;

            var projectFilterOptions = {
                field: $('#' + this.id + '-project-filter-picker', this.$el),

                completeField: function (selection) {
                    if (selection) {
                        bubbleChartConfigView.$el.find('input[name=type]').val(selection.type);
                        bubbleChartConfigView.$el.find('input[name=id]').val(selection.id);
                        bubbleChartConfigView.$el.find('input[name=name]').val(selection.name);
                        bubbleChartConfigView.$el.find('.filterpicker-value-name').text(selection.name).addClass('success');
                        this.field.val('');
                        this.field.trigger('change');
                    }
                },

                maxHeight: 140
            };

            // Mostly used for testing where we don't want a delay
            if (this.projectFilterDelay) {
                projectFilterOptions.delay = this.projectFilterDelay;
            }

            // To account for advanced search mechanism
            projectFilterOptions.parentElement = this.$el.find('form');
            projectFilterOptions.fieldID = projectFilterOptions.field.attr('id');

            ProjectFilterAutoComplete(projectFilterOptions);
        },

        /**
         * Called when the form is submitted, this method validates the current
         * fields and submits the form if they are valid.
         * @param event The form submission event.
         */
        submit: function (event) {
            var arrayData = $('form', this.$el).serializeArray();
            event.preventDefault();
            var fields = this._getPreferencesObjectFromSerializedArray(arrayData);

            if (this.validPreferences(fields)) {
                this.trigger('submit', fields);
            }
        },

        /**
         * Called when the form is cancelled.
         */
        cancel: function(event) {
            event.preventDefault();
            this.trigger('cancel');
        },

        _getPreferencesObjectFromSerializedArray: function(serializedArray) {
            var formFieldsObj = serializedArray.reduce(function(fieldsObj, field) {
                fieldsObj[field.name] = field.value;
                return fieldsObj;
            }, {});

            return this._mapFormFieldsToCorrectPreferenceNamesAndValues(formFieldsObj);
        },

        _mapFormFieldsToCorrectPreferenceNamesAndValues: function(formFieldsObj) {
            var preferences = $.extend({}, formFieldsObj);
            preferences.refresh = formFieldsObj['refresh-interval'];
            preferences.useLogarithmicScale = formFieldsObj.useLogarithmicScale === 'on';
            preferences.useRelativeColoring = formFieldsObj.useRelativeColoring === 'on';
            delete preferences['refresh-interval'];
            return preferences;
        },

        /**
         * Validate the configuration and any fields that are not correct apply an error message to the field.
         * @param {Object} preferences The set of preferences. Usually obtained from the form.
         * @returns {boolean} Whether the preferences are valid or not
         */
        validPreferences: function (preferences) {
            var valid = true;
            if (!preferences.id || !preferences.type) {
                $('.projectOrFilter-error', this.$el).text(formatter.I18n.getText('gadget.common.required.query')).show();
                valid = false;
            }
            else {
                $('.projectOrFilter-error', this.$el).hide();
            }

            this.trigger('layoutUpdate');
            return valid;
        },

        _relativeColoringCheckboxes: function(preferences) {
            return [
                {
                    id: 'bubble-chart-relative-color-' + this.id,
                    name: 'useRelativeColoring',
                    labelText: formatter.I18n.getText('bubble-chart.use.relative.coloring.label'),
                    isChecked: preferences.useRelativeColoring
                }
            ];
        },

        _logarithmicScaleCheckboxes: function(preferences) {
            return [
                {
                    id: 'bubble-chart-log-scale-' + this.id,
                    name: 'useLogarithmicScale',
                    labelText: formatter.I18n.getText('bubble-chart.use.logarithmic.scale.label'),
                    descriptionText: formatter.I18n.getText('bubble-chart.use.logarithmic.scale.description'),
                    isChecked: preferences.useLogarithmicScale
                }
            ];
        }
    });

    return BubbleChartConfigView;
});
