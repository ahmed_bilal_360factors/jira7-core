/**
 * Represents a plottable data set that for each x maps to two y values where the y value is [y0, y1].
 * The main role of this component is to provide helper functions to access the y values for a dataset.
 *
 * @module DualPlottable
 */
define('jira-dashboard-items/components/charts/components/dualplottable', [
    'jira-dashboard-items/lib/c3',
    'jira-dashboard-items/lib/d3'
], function(
    c3,
    d3
) {

    return c3.component('dualPlottable')
        .extend(c3.plottable())
        .extend({
            y0: function () {
                var yScale = this.yScale();
                var y0Accessor = this.y0Accessor();
                return function (d, i) {
                    return yScale(y0Accessor(d, i));
                };
            },
            y1: function () {
                var yScale = this.yScale();
                var y1Accessor = this.y1Accessor();
                return function (d, i) {
                    return yScale(y1Accessor(d, i));
                };
            },
            y0Accessor: function () {
                var yAccessor = this.yAccessor();
                return function (d, i) {
                    return yAccessor(d, i)[0];
                };
            },
            y1Accessor: function () {
                var yAccessor = this.yAccessor();
                return function (d, i) {
                    return yAccessor(d, i)[1];
                };
            },
            yDomain: function() {
                if (c3.isEmpty(this.data())) {
                    return;
                }

                var y0Accessor = this.y0Accessor();
                var y1Accessor = this.y1Accessor();

                var min = c3.checkIsNumber(d3.min(this.data(), function(data, index) {
                    var y0 = y0Accessor(data, index);
                    var y1 = y1Accessor(data, index);
                    return d3.min([y0, y1]);
                }));

                var max = c3.checkIsNumber(d3.max(this.data(), function(data, index) {
                    var y0 = y0Accessor(data, index);
                    var y1 = y1Accessor(data, index);
                    return d3.max([y0, y1]);
                }));
                return [min, max];
            }
        });
});
