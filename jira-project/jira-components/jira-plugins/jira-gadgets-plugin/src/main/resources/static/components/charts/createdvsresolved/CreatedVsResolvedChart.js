/**
 * @module CreatedVsResolvedChart
 */
define('jira-dashboard-items/components/charts/createdvsresolved-chart', [
    'jira/util/formatter',
    'jquery',
    'backbone',
    'jira/moment',
    'jira-dashboard-items/lib/d3',
    'jira-dashboard-items/lib/c3',
    'jira-dashboard-items/components/charts/components/linecomparison-graph',
    'jira-dashboard-items/components/charts/components/chart',
    'jira-dashboard-items/components/charts/components/interactive',
    'jira-dashboard-items/components/charts/components/dualplottable',
    'jira-dashboard-items/components/dates/daterange',
    'jira-dashboard-items/components/search/binary-search',
    'jira-dashboard-items/components/charts/components/marker-following-mouse-with-dialog',
    'jira-dashboard-items/components/charts/components/markers',
    'jira-dashboard-items/components/charts/components/grouped-labels',
    'underscore'
], function(
    formatter,
    $,
    Backbone,
    moment,
    d3,
    c3,
    LineComparisonGraph,
    Chart,
    Interactive,
    DualPlottable,
    DateRange,
    BinarySearch,
    MarkerFollowingMouseWithDialog,
    Markers,
    GroupedLabels,
    _) {
    /**
     * @typedef {Object} CreatedVsResolvedData
     * @property {Object[]} created data points
     * @property {Object[]} resolved data points
     * @property {Object[]} domain of the graph
     */

    /**
     * @typedef {Object} CreatedVsResolvedsOptions
     * @property {Number} id unique for this chart
     * @property {Object} [axisOptions]
     * @property {CreatedVsResolvedData} data for the chart
     * @property {Object[]} versionData of any versions for the filter
     * @property {Number} chartHeight of the chart
     */


    var CHART_RIGHT_PADDING = 50;
    var VERSION_TOP_SPACING = 25; //Represents height where the version label will be above the chart.
    var VERSION_HEIGHT = 10; //An estimate for the space needed to render the version label
    var VERSION_LINE_SPACING = 3; //Space below the version text to the line
    var VERSION_GROUP_NUB_HEIGHT = 10; //Height of the nub above the horizontal line joining labels

    //Calculates how high the markers should go above the chart
    var VERSION_MARKER_TOP = VERSION_TOP_SPACING - VERSION_GROUP_NUB_HEIGHT - VERSION_LINE_SPACING;

    var VERSIONS_INLINE_DIALOG_VERT_TOP_OFFSET = -(VERSION_TOP_SPACING + VERSION_HEIGHT);
    var NO_VERSIONS_INLINE_DIALOG_VERT_TOP_OFFSET = -10;

    var CreatedVsResolvedChart = Backbone.View.extend({
        CLASS_NAME: "CreatedVsResolvedChart",

        /**
         * @name LineType
         * @enum {String}
         *
         * The line type of the chart
         */
        LINE_TYPE: {
            CREATED: "created",
            RESOLVED: "resolved"
        },

        INLINE_DIALOG_CLASS: "created-vs-resolved-inline-dialog",

        Templates: JIRA.DashboardItem.CreatedVsResolvedChart.Templates,

        /**
         * @param {CreatedVsResolvedsOptions} options
         */
        initialize: function(options) {
            options = options || {};

            this.parseOptions(options);
        },

        /**
         * Makes sure the options passed into the view are correct.
         * @param {CreatedVsResolvedsOptions} options
         */
        parseOptions: function(options) {
            options = options || {};


            if (!options.id) {
                throw new Error(this.CLASS_NAME + ": Should have supplied an ID");
            }

            if (!options.data) {
                throw new Error(this.CLASS_NAME + ": No data passed into options");
            }

            if (!options.data.created) {
                throw new Error(this.CLASS_NAME + ": No created data passed into options");
            }

            if (!options.data.resolved) {
                throw new Error(this.CLASS_NAME + ": No resolved data passed into options");
            }

            if (!options.data.domain) {
                throw new Error(this.CLASS_NAME + ": No domain data passed into options");
            }

            this.id = options.id;

            this.cumulative = options.cumulative;

            this.days = options.days;

            this.period = options.period;

            this.rawData = options.data;

            this.data = this._formatData(options.data);

            this.hasVersionData = (options.versionData && options.versionData.length > 0);

            this.versionData = options.versionData;

            this.chartHeight = options.chartHeight;

            this.axisOptions = options.axisOptions || {};
        },

        render: function() {
            this.$el.html(this.Templates.Container({
                id: this.id
            }));

            this._chartContainerSelector = "#" + this.id + "-createdvsresolved-chart-wrapper";

            this._generateGraph();

            this._overlayVersionGraphics();

            this._generateMouseFollowingMarker();

            this._generateChart();

            //Specifically sets the top level yDomain function as children like LinePlot in Line Comparison
            //Cannot calculate it, without losing generalisation. This sucks.
            this.chart.yDomain(DualPlottable().yDomain());


            var chartElement = this.$(".createdvsresolved-chart", this.$el);
            chartElement.height(this.chartHeight);

            this.chart(d3.select(chartElement[0]));
        },

        /**
         * Takes domain, created and resolved data and formats it into the desired format
         * @param {Object} data
         * @returns {Object[]} correctly formatted data.
         * @private
         */
        _formatData: function (data) {
            //Assumes at least two elements in domain.
            var gap = data.domain[1].start - data.domain[0].end;
            var length = data.domain[0].end - data.domain[0].start;
            var end = data.domain[0].start - gap;
            var start = end - length;
            var formattedData = [
                [{start: start, end: end}, [0, 0]]
            ];

            data.domain.forEach(function(x, index) {
                formattedData.push([x, [
                    data.created[index].count,
                    data.resolved[index].count
                ]]);
            });
            return formattedData;
        },

        /**
         * Create a circle plot component for a line of data.
         * @param lineType
         * @returns {circlePlot}
         * @private
         */
        _createCirclePlot: function(lineType) {
            var self = this;

            var MIN_RADIUS = 2;
            var MAX_RADIUS = 5;

            var BORDER_WIDTH = 2;

            return c3.circlePlot()
                .radiusAccessor(function() {
                    return MAX_RADIUS;
                })
                .extend({
                    hoverRadiusAccessor: function() {
                        return this.radiusAccessor() * 1.75;
                    }
                })
                .enter(function(event) {
                    var c3Self = this;

                    event.selection.each(function(data, index) {
                        d3.select(this).attr("stroke-width", BORDER_WIDTH);
                    });
                    event.selection.on('click', function(data, index) {
                        if (index > 0) {
                            var dialog = self._createCirclePlotInlineDialog(this, 'circle-plot-' + index, data, index - 1, lineType);
                            dialog.show();
                        }
                    });
                    event.selection.on('mouseover', function() {
                        d3.select(this).transition().duration(100).attr("r", c3Self.hoverRadiusAccessor());
                    });

                    event.selection.on('mouseleave', function() {
                        d3.select(this).transition().duration(100).attr("r", c3Self.radiusAccessor());
                    });
                })

                //Decrease the size of the circles if they are close together...hacky.
                .extend(function() {

                    var circlePoints = this.selection().selectAll("circle");

                    var firstX = d3.select(circlePoints[0][0]).attr('cx');
                    var secondX = d3.select(circlePoints[0][1]).attr('cx');

                    var width = secondX - firstX;


                    var radius = Math.max(MIN_RADIUS, Math.min((width - 1) / 2, MAX_RADIUS));
                    this.radiusAccessor(radius);

                    circlePoints.attr('r', this.radiusAccessor());
                    circlePoints.attr('stroke-width', this.radiusAccessor() / 1.5);
                });
        },

        /**
         * Creates the main graph for the chart. This includes the line, area and circle plots for created and
         * resolved data.
         *
         * @private
         */
        _generateGraph: function() {
            this.graph = LineComparisonGraph({
                firstName: 'created',
                secondName: 'resolved'
            });

            this._modifyLineWidth();

            var createdPlot = this._createCirclePlot(this.LINE_TYPE.CREATED).yAccessor(function(d) {
                return d[1][0];
            });

            var resolvedPlot = this._createCirclePlot(this.LINE_TYPE.RESOLVED).yAccessor(function(d) {
                return d[1][1];
            });

            this.graph
                .addLayer('created-points', createdPlot)
                .addLayer('resolved-points', resolvedPlot);
        },

        /**
         * Change the size of the line depending on how much space there is between points.
         * @private
         */
        _modifyLineWidth: function() {
            function calculateDistanceBetweenPoints(line) {
                var points = line.attr("d").split(/[a-zA-Z]/);
                points.shift();

                if (points.length < 2) {
                    return null;
                }

                var firstX = points[0].split(',')[0];
                var secondX = points[1].split(',')[0];

                return secondX - firstX;
            }

            function modifyLineWidth() {
                var line = this.selection().selectAll("path");

                var width = calculateDistanceBetweenPoints(line);

                if (width) {
                    var lineWidth = Math.max(2, Math.min(width / 2, 5));
                    line.attr('stroke-width', lineWidth);
                } else {
                    line.attr('stroke-width', 5);
                }
            }


            this.graph.extend(modifyLineWidth);
        },

        /**
         * Add a marker that follows the mouse movement onto the graph, with a dialog next to it.
         * This should only show if no element has opened an inline dialog in the chart.
         *
         * @private
         */
        _generateMouseFollowingMarker: function() {
            var self = this;

            this.markerFollowingMouse = MarkerFollowingMouseWithDialog({
                region: this.graph,
                domContextSelector: "#" + this.id + "-createdvsresolved-chart-wrapper",
                canShow: function() {
                    return !$("." + self.INLINE_DIALOG_CLASS).is(":visible");
                },
                yTopOffset: (self.hasVersionData ? VERSIONS_INLINE_DIALOG_VERT_TOP_OFFSET : NO_VERSIONS_INLINE_DIALOG_VERT_TOP_OFFSET),

                generateContent: function(date) {

                    //Given a value calculate what index it is in the data set.
                    var dataIndex = BinarySearch.search(self.data, date, function(goal, test) {
                        var start = test[0].start;
                        var end = test[0].end;

                        if (start <= goal && goal <= end) {
                            return 0;
                        } else if (goal < start) {
                            return -1;
                        } else {
                            return 1;
                        }
                    });

                    if (dataIndex < 0 || dataIndex >= self.data.length) {
                        return null;
                    }

                    var indicatedTime;
                    if (self.cumulative) {
                        indicatedTime = DateRange.rangeToText(self._getFirstDate(), self.data[dataIndex][0].end);
                    } else {
                        indicatedTime = DateRange.rangeToText(self.data[dataIndex][0].start, self.data[dataIndex][0].end);
                    }

                    return self.Templates.renderScrubberDialogContent({
                        indicatedTime: indicatedTime,
                        created: self.data[dataIndex][1][0],
                        resolved: self.data[dataIndex][1][1]
                    });
                }
            });

            this.graph.prependLayer('following-marker', this.markerFollowingMouse);
        },

        /**
         * Apply version lines and labels onto the graph. Allows the user to click a version to get information.
         * @private
         */
        _overlayVersionGraphics: function() {
            var self = this;

            if (this.hasVersionData) {
                var versions = c3.layerable();
                versions.addLayer('lines', Markers().data(this.versionData).xDomain(this.graph.xDomain()).xAccessor(function (d) {
                    return d[0];
                }).extend({
                    y1: function () {
                        return -(VERSION_TOP_SPACING - VERSION_GROUP_NUB_HEIGHT - VERSION_LINE_SPACING);
                    }
                }));

                var labels = GroupedLabels({
                    groupTextGenerator: function(totalGroupedLabels) {
                        return formatter.I18n.getText('portlet.createdvsresolved.versions.number', totalGroupedLabels);
                    }
                }).data(this.versionData).xDomain(this.graph.xDomain()).xAccessor(function (d) {
                    return d[0];
                }).extend({
                    y: function () {
                        return function () {
                            //Want the labels to be placed above the chart by a certain amount
                            return (-VERSION_TOP_SPACING);
                        };
                    },
                    textGenerator: function () {
                        return function (d) {
                            return d[1].name;
                        };
                    }
                }).extend(function() {
                    var labelsSelf = this;
                    var labelData = labelsSelf.data();

                    var dialogCount = 0;
                    this.elements().each(function() {
                        var groupedIds = d3.select(this).attr(GroupedLabels.GROUPING_DATA_ATTR);
                        self._createVersionInlineDialog(this, 'version-info-' + self.id + "-" + (dialogCount++), labelData, groupedIds.split(","));
                    });
                });

                versions.addLayer('labels', labels);

                // TODO: Make the code below not as hacky.
                // create joining lines, should probably be moved to it's own component with some interface to define
                // when it should merge markers...too late to do this now.
                versions.extend(function() {
                    var lineGroup = $(".lines", this.selection().node());
                    var lines = $(".marker-line", lineGroup);
                    var labels = $(".labels .label", this.selection().node());

                    labels.each(function(index) {
                        var label = $(labels.get(index));
                        var lineIndexes = label.attr(GroupedLabels.GROUPING_DATA_ATTR).split(",");

                        var multipleLines = document.createElementNS('http://www.w3.org/2000/svg','g');
                        multipleLines.setAttribute('class','multiple-lines');

                        var start = parseFloat($(lines.get(lineIndexes[0])).attr('x1'));
                        var end = parseFloat($(lines.get(lineIndexes[lineIndexes.length - 1])).attr('x1'));

                        //Need to draw a horizontal line
                        //assumed that they are in x-ordering.
                        var horizontalLine = document.createElementNS('http://www.w3.org/2000/svg','line');
                        horizontalLine.setAttribute('class','horizontal-line');
                        horizontalLine.setAttribute('x1',start);
                        horizontalLine.setAttribute('x2',end);
                        horizontalLine.setAttribute('y1', -VERSION_MARKER_TOP);
                        horizontalLine.setAttribute('y2', -VERSION_MARKER_TOP);
                        multipleLines.appendChild(horizontalLine);

                        var middle = start + ((end - start) / 2);
                        var nubLine = document.createElementNS('http://www.w3.org/2000/svg','line');
                        nubLine.setAttribute('class','nub-line');
                        nubLine.setAttribute('x1', middle);
                        nubLine.setAttribute('x2', middle);
                        nubLine.setAttribute('y1', -(VERSION_MARKER_TOP));
                        nubLine.setAttribute('y2', -(VERSION_MARKER_TOP + VERSION_GROUP_NUB_HEIGHT));
                        multipleLines.appendChild(nubLine);

                        _.each(lineIndexes, function(value) {
                            multipleLines.appendChild($(lines.get(parseInt(value))).remove().get(0));
                        });
                        lineGroup.append(multipleLines);
                    });

                });

                //Highlight lines on label hover
                versions.extend(function() {
                    var versionLines = $(".lines .marker-line", this.selection().node());
                    var groupingLines = $(".lines .horizontal-line", this.selection().node());
                    var nubLines = $(".lines .nub-line", this.selection().node());
                    var labels = $(".labels .label", this.selection().node());

                    labels.mouseenter(function() {
                        var index = labels.index(this);

                        d3.select(groupingLines.get(index)).classed('line-highlighted', true);
                        d3.select(nubLines.get(index)).classed('line-highlighted', true);


                        $(this).attr(GroupedLabels.GROUPING_DATA_ATTR).split(",").forEach(function(value) {
                            d3.select(versionLines.get(value)).classed('line-highlighted', true);
                        });
                    });

                    labels.mouseleave(function() {
                        var index = labels.index(this);

                        d3.select(groupingLines.get(index)).classed('line-highlighted', false);
                        d3.select(nubLines.get(index)).classed('line-highlighted', false);

                        $(this).attr(GroupedLabels.GROUPING_DATA_ATTR).split(",").forEach(function(value) {
                            d3.select(versionLines.get(value)).classed('line-highlighted', false);
                        });
                    });
                });


                this.graph.prependLayer('versions', versions);
            }
        },

        /**
         * Create an inline dialog for the Chart.
         * @param {Node} trigger to click to open dialog
         * @param {String} id of the dialog
         * @param {String} content to place into the dialog.
         * @private
         */
        _createInlineDialog: function(trigger, id, content) {
            var self = this;

            $(trigger).click(function() {
               self.markerFollowingMouse.hide();
            });

            return AJS.InlineDialog($(trigger), id, function(element, trigger, showPopup) {
                element.parent().addClass('created-vs-resolved-inline-dialog');
                element.html(content);


                //this stops closing the dialog if you click anything in it
                //the dialog closes if you click outside it, see JRA-24215
                $(element).click(function(e){
                    e.stopPropagation();
                });


                showPopup();
                return false;
            }, {
                fadeTime: 0,
                cacheContent: false, // don't cache the dialog content
                hideDelay: 60000, // set longer timeout (default is 10 seconds)
                gravity: 'w',
                width: 350
            });
        },

        /**
         * Create an inline dialog for a circle plot.
         * @param {Node} trigger of the circle on the graph
         * @param {String} id to assign to the inline dialog
         * @param {Object} data for the given x axis point
         * @param {Number} index of the data point in the data array
         * @param {String} lineType for the point
         * @private
         */
        _createCirclePlotInlineDialog: function(trigger, id, data, index, lineType) {

            var dialogContent = "";
            if (data[1][0] === data[1][1]) {
                dialogContent = this._createdAndResolvedClickDialogContent(data, index);
            } else if (lineType === this.LINE_TYPE.CREATED) {
                dialogContent = this._createdClickDialogContent(data, index);
            } else if (lineType === this.LINE_TYPE.RESOLVED) {
                dialogContent = this._resolvedClickDialogContent(data, index);
            }

            return this._createInlineDialog(trigger, id, dialogContent);
        },

        /**
         * Given version data and groups of versions generate a version dialog.
         * @param trigger
         * @param id
         * @param data
         * @param groupedIndexes
         * @private
         */
        _createVersionInlineDialog: function(trigger, id, data, groupedIndexes) {
            var firstDate = data[groupedIndexes[0]][0];
            var lastDate = data[groupedIndexes[groupedIndexes.length - 1]][0];
            var dateRangeString = DateRange.rangeToText(firstDate, lastDate);

            var dialogContent = this.Templates.VersionDialog({
                dateRange: dateRangeString,
                data: data,
                groupedIndexes: groupedIndexes
            });

            this._createInlineDialog(trigger, id, dialogContent);
        },


        /**
         * Takes the graph and applies axis' and grids to generate a chart.
         * @private
         */
        _generateChart: function() {
            var self = this;
            var axisOptions = {};

            if (this.axisOptions.includeXAxis) {
                axisOptions.xAxisValue = function(d) {

                    if (self.period === "hourly") {
                        return moment(d).format("ha DD MMM YYYY");
                    } else {
                        return moment(d).format("DD MMM YYYY");
                    }
                };
            }

            if (this.axisOptions.includeYAxis) {
                axisOptions.yAxisValue = function(d) {
                    var noDecimals = d3.format("d")(d);
                    if (noDecimals) {
                        return JIRA.NumberFormatter.format(parseInt(noDecimals));
                    } else {
                        return noDecimals;
                    }
                };
            }

            this.chart = Chart(this.graph, axisOptions)
                .data(this.data).extend({
                    xAccessor: function() {
                        return function(d) {
                            return d[0].end;
                        };
                    }
                });


            //JC-400: Add some more padding on the right so the last label should always be fully visible
            this.chart.east(c3.withDimensions().width(CHART_RIGHT_PADDING));


            //If we are applying version information create padding on the top for the version labels to go into.
            if (this.hasVersionData) {
                this.chart.north(c3.withDimensions().height(VERSION_TOP_SPACING + VERSION_HEIGHT));
            }
        },

        /**
         * Given a type of type and a date range create a query string for JQL.
         * @param {Number} index of the data element
         * @param {LineType} lineType of the data
         *
         * @return {String} of the url for the data
         * @private
         */
        _getSectionUrl: function(index, lineType) {
            return this.rawData[lineType][index].searchUrl;
        },

        /**
         * Create content for the created circle plot inline dialog.
         * @param {Object} data of the point
         * @param {Number} index of the data point
         *
         * @returns {String} HTML representing content
         * @private
         */
        _createdClickDialogContent: function(data, index) {
            return this._createClickDialogContent(data, index, true, false);
        },

        /**
         * Create content for the resolved circle plot inline dialog.
         * @param {Object} data of the point
         * @param {Number} index of the data point
         *
         * @returns {String} HTML representing content
         * @private
         */
        _resolvedClickDialogContent: function(data, index) {
            return this._createClickDialogContent(data, index, false, true);
        },

        /**
         * Create content for the inline dialog which represents both created and resolved.
         * @param {Object} data of the point
         * @returns {String} HTML representing content
         * @private
         */
        _createdAndResolvedClickDialogContent: function(data, index) {
            return this._createClickDialogContent(data, index, true, true);
        },

        /**
         * Generate the content for the point click inline dialog.
         * @param {Object} data for the x coordinate of the point
         * @param {Number} index of the data point
         * @param {Boolean} includeCreated whether to show created info
         * @param {Boolean} includeResolved whether to show resolved info
         * @returns {String} HTML representing content
         * @private
         */
        _createClickDialogContent: function(data, index, includeCreated, includeResolved) {
            var values = [];
            if (includeCreated) {
                values.push({
                    label: formatter.I18n.getText('portlet.createdvsresolved.created.number', data[1][0]),
                    url: this._getSectionUrl(index, this.LINE_TYPE.CREATED),
                    value: data[1][0]
                });
            }

            if (includeResolved) {
                values.push({
                    label: formatter.I18n.getText('portlet.createdvsresolved.resolved.number', data[1][1]),
                    url: this._getSectionUrl(index, this.LINE_TYPE.RESOLVED),
                    value: data[1][1]
                });
            }

            var dialogTitle;
            if (this.cumulative) {
                dialogTitle = DateRange.rangeToText(this._getFirstDate(), data[0].end);
            } else {
                dialogTitle = DateRange.rangeToText(data[0].start, data[0].end);
            }

            return this.Templates.pointDialogMulti({
                title: dialogTitle,
                values: values
            });
        },

        _getFirstDate: function() {
            return this.data[1][0].start;
        }
    });

    return CreatedVsResolvedChart;
});
