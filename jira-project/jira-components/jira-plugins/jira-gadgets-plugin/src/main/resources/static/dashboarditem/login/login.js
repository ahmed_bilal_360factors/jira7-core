define("jira-dashboard-items/login", [
    'jira/util/formatter',
    'jira/captcha',
    'wrm/context-path',
    'underscore',
    'jquery'
], function(formatter, Captcha, wrmContextPath, _, $) {

    function renderTemplate($element, userPreferences, currentError) {
        $element.empty().html(JIRA.DashboardItem.Login.Templates.Login({
            isPublicMode: userPreferences.isPublicMode,
            adminFormOn: userPreferences.isAdminFormOn,
            showForgotPassword: !userPreferences.externalUserManagement && !userPreferences.externalPasswordManagement,
            showRememberMe: userPreferences.allowCookies,
            errorMessage: currentError,
            showCaptcha: userPreferences.isElevatedSecurityCheckShown,
            captchaTimestamp: Date.now()
        }));
    }

    function applyResponsiveLayout($element, $form) {
        if ($element.width() < 490) {
            $form.addClass("top-label");
        } else {
            $form.removeClass("top-label");
        }
    }

    function focusInputUsername($element) {
        setTimeout(function() {
            $element.find("#login-form-username").focus();
        }, 200);
    }

    function onAfterRender() {
        applyResponsiveLayout(this.$element, this.$form);
        focusInputUsername(this.$element);
        Captcha.setup();
        this.API.resize();
    }

    function responseToErrorMessage(data) {
        data = data || {};
        if (data.communicationError) {
            return formatter.I18n.getText("gadget.login.error.communication");
        }
        if (data.loginError) {
            return formatter.I18n.getText("gadget.login.error.misc");
        }
        if (data.loginFailedByPermissions) {
            return formatter.I18n.getText("gadget.login.invalidloginpermissions", data.contactAdminLink);
        }
        if (data.isElevatedSecurityCheckShown) {
            return formatter.I18n.getText("gadget.login.invalidcaptcha");
        }
        return formatter.I18n.getText("gadget.login.invalidlogin");
    }

    function updateTemplate(data, userPreferences) {
        this.currentError = responseToErrorMessage(data);
        this.render(this.$element, userPreferences, true);
        this.API.hideLoadingBar();
        onAfterRender.call(this);
    }

    var DashboardItem = function(API) {
        this.currentError = null;
        this.API = API;
        this.API.on("afterRender", onAfterRender, this);
    };

    DashboardItem.prototype.render = function(element, userPreferences, forceRender) {
        var $element = this.$element = $(element);
        var currentError = this.currentError;

        if (forceRender || $element.find("#loginform").length === 0) {
            renderTemplate($element, userPreferences, currentError);
        }

        var $form = this.$form = $element.find("#loginform");
        $form.submit(_.bind(function(e) {
            e.preventDefault();
            this.API.showLoadingBar();

            $.ajax({
                type: "POST",
                url: wrmContextPath() + "/rest/gadget/1.0/login",
                data: $form.serialize(),
                success: _.bind(function(data) {
                    if (data.loginSucceeded) {
                        window.location = wrmContextPath();
                    } else {
                        // After a few errors, we might need to show the captcha.
                        userPreferences.isElevatedSecurityCheckShown = data.isElevatedSecurityCheckShown;
                        updateTemplate.call(this, data, userPreferences);
                    }
                }, this),
                error: _.bind(function(data) {
                    updateTemplate.call(this, data, userPreferences);
                }, this)
            });
        }, this));
    };

    return DashboardItem;
});
