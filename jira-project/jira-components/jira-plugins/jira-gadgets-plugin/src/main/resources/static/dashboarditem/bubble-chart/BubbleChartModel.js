define('jira-dashboard-items/bubble-chart-model', [
    'jira/util/formatter',
    'wrm/context-path',
    'jquery',
    'underscore',
    'backbone',
    'jira-dashboard-items/components/charts/components/relative-distribution',
    'jira-dashboard-items/lib/d3'
], function(formatter, wrmContextPath, $, _, Backbone, RelativeDistribution, d3) {
    'use strict';

    /**
     * BubbleChartModel manages the data and networking required for the bubble chart dashboard item.
     * Its initializer requires `projectOrFilterId`, `bubbleType`, and `recentCommentsPeriod` to be specified.
     * Optionally, `useLogarithmicScale`, `useRelativeColoring`, and `colors` can be overridden pass in the
     * configuration object to customize the model.
     * Provided that a network request has finished, users can access `keyLabel`, `xAxisLabel`, `yAxisLabel`,
     * `sizeAxisLabel`, `colorAxisLabel`, `filterTitle`, `issueCount`, `data`, `colorDomain`, and `legendData`.
     * @type {BubbleChartModel}
     */
    var BubbleChartModel = Backbone.Model.extend({
        urlRoot: wrmContextPath() + '/rest/gadget/1.0/bubblechart',

        defaults: {
            useLogarithmicScale: false,
            useRelativeColoring: true,
            colors: ['#28892C', '#95B436', '#F5C342','#F1A256', '#F79232', '#D04436'],
            issueLimit: 200,
            sizeAxis: 'participants'
        },

        url: function() {
            return this.urlRoot + '/generate?projectOrFilterId=' + this.get('projectOrFilterId') +
                '&bubbleType=' + this.get('bubbleType') +
                '&recentCommentsPeriod=' + this.get('recentCommentsPeriod');
        },

        parse: function(response) {
            var parsed = {};

            parsed.keyLabel = formatter.I18n.getText('bubble-chart.key.axis.title.label');
            parsed.xAxisLabel = formatter.I18n.getText('bubble-chart.x.axis.title.label');
            parsed.yAxisLabel = formatter.I18n.getText('bubble-chart.y.axis.title.label');
            parsed.sizeAxisLabel = this.get('bubbleType') === 'participants'
                ? formatter.I18n.getText('bubble-chart.participants')
                : formatter.I18n.getText('bubble-chart.votes');
            parsed.colorAxisLabel = formatter.I18n.getText('bubble-chart.color.axis.title.label');
            parsed.filterTitle = response.filterTitle;
            parsed.issueCount = response.issueCount;

            var data = response.data;
            if (this.get('useLogarithmicScale')) {
                data = this._filterZeroesForLogarithmicScale(data);
            }

            if (response.warning && response.issueCount >= this.get('issueLimit')
                || data.length > this.get('issueLimit')) {
                parsed.exceedsIssueLimit = true;
            } else {
                parsed.exceedsIssueLimit = false;
            }

            parsed.colorDomain = this.get('useRelativeColoring')
                ? this._domainForRelativeColorDistribution(data)
                : this._domainForAbsoluteColorDistribution(data);

            parsed.legendData = this.get('useRelativeColoring')
                ? this._legendDataForRelativeColoring(this.get('colors'))
                : this._legendDataForAbsoluteColoring(this.get('colors'));

            parsed.data = data;

            return parsed;
        },

        /**
         * Search for an issue by issue key
         * @param issueKey The issue key to search by--this is the first entry in each array of the network response
         * @returns {Array} An array representing an issue with x, y, radius, and color value as its entries
         */
        getIssueForIssueKey: function(issueKey) {
            // By the time this method would be called, the data will have been reformatted into a d3-suitable format
            // similar to [ [1, 2, 3, 2, 'JC-101'], [2, 34, 1, 2, 'JC-64'], ... ]. We access the last element of each
            // data entry to find based on the issue key. (Otherwise, we would need to maintain two copies of the data
            // in this model--one for the d3-suitable data, and one with the original objects returned from the server.)
            return _.findWhere(this.get('data'), {
                key: issueKey
            });
        },

        /**
         * Returns which segment of the domain a value lives in.
         * @param value
         */
        indexInDomainForValue: function(value) {
            var scale = d3.scale.quantile().domain(this.get('colorDomain')).range(this.get('colors'));
            return this.get('colors').indexOf(scale(value));
        },

        _domainForAbsoluteColorDistribution: function(chartData) {
            var min = d3.min(chartData, function(d) { return d.bubbleColorValue; });
            var max = d3.max(chartData, function(d) { return d.bubbleColorValue; });
            return [min, max];
        },

        _domainForRelativeColorDistribution: function(chartData) {
            var colorValues = chartData.map(function(dataPoint) { return dataPoint.bubbleColorValue; });
            var distribution = RelativeDistribution.distribute(colorValues, [0.1, 0.2, 0.4, 0.2, 0.1]);

            return distribution.map(function(arr) {
                // distribution is a 2D array. Get the first value of each bucket to use as our threshold.
                return arr[0];
            }).filter(Number);
        },

        /**
         * Filter data points that are 0 in the x or y component from the data set
         * @param chartData The data set
         * @returns {Array} The original data set except for anything that had a value of 0 in the x or y (0 or 1) component
         * @private
         */
        _filterZeroesForLogarithmicScale: function(chartData) {
            return chartData.filter(function(dataPoint) {
                return dataPoint.bubbleDomain !== 0 && dataPoint.bubbleRange !== 0;
            });
        },

        // We only want to render 'low' and 'high' labels on the first and last segments--the rest are empty
        _legendDataForRelativeColoring: function(colors) {
            var legendData = colors.map(function(_, i) { return '';});
            legendData[0] = formatter.I18n.getText('bubble-chart.legend.low');
            legendData[legendData.length - 1] = formatter.I18n.getText('bubble-chart.legend.high');
            return legendData;
        },

        _legendDataForAbsoluteColoring: function(colors) {
            // We just use the same labelling as the relative coloring, but we don't want to leak that to the caller
            // since this might change.
            return this._legendDataForRelativeColoring(colors);
        }
    });

    return BubbleChartModel;
});
