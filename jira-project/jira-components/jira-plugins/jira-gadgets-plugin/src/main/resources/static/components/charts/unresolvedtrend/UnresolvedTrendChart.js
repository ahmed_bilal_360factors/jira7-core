/**
 * Creates a c3 component representing the UnresolvedTrendChart.
 *
 * @module UnresolvedTrendChart
 */
define('jira-dashboard-items/components/charts/unresolved-trend-chart', [
    'jquery',
    'backbone',
    'jira-dashboard-items/components/charts/components/chart',
    'jira-dashboard-items/components/charts/components/markers',
    'jira-dashboard-items/components/charts/components/marker-following-mouse-with-dialog',
    'jira-dashboard-items/components/dates/daterange',
    'jira-dashboard-items/components/search/binary-search',
    'jira-dashboard-items/lib/d3',
    'jira-dashboard-items/lib/c3',
    'jira/moment'
], function(
    $,
    Backbone,
    Chart,
    Markers,
    MarkerFollowingMouseWithDialog,
    DateRange,
    BinarySearch,
    d3,
    c3,
    moment
) {

    return Backbone.View.extend({
        Templates: JIRA.DashboardItem.UnresolvedChart.Templates,

        initialize: function(options) {
            options = options || {};

            this._parseOptions(options);
        },

        _parseOptions: function(options) {
            //TODO: Validation for this
            this.id = options.id;
            this.data = options.data;
            this.domain = options.domain;
            this.versionData = options.versionData;
            this.chartHeight = options.chartHeight;
            this.period = options.period;
            this.axisOptions = options.axisOptions || {};
        },

        render: function() {
            var instance = this;

            this.$el.html(instance.Templates.Container({
                id: this.id
            }));

            var svgElement = $("svg.unresolvedtrend-chart", this.$el);

            var unresolvedData = formatAsLineData(this.data, this.domain);

            var graph = c3.layerable();


            graph.addLayer('line', c3.linePlot().elementClass('unresolved-trend'))
                .addLayer('movingMarker', MarkerFollowingMouseWithDialog({
                    region: graph,
                    domContextSelector: "#" + this.id + "-unresolvedtrend-chart-wrapper",
                    canShow: function() {
                        return !$(".created-vs-resolved-inline-dialog").is(":visible");
                    },

                    generateContent: function(date) {

                        //Given a value calculate what index it is in the data set.
                        var dataIndex = BinarySearch.search(unresolvedData, date, function(goal, test) {
                            var start = test[0].start;
                            var end = test[0].end;

                            if (start <= goal && goal <= end) {
                                return 0;
                            } else if (goal < start) {
                                return -1;
                            } else {
                                return 1;
                            }
                        });

                        if (dataIndex < 0 || dataIndex >= unresolvedData.length) {
                            return null;
                        }

                        return instance.Templates.renderScrubberDialogContent({
                            indicatedTime: DateRange.rangeToText(unresolvedData[dataIndex][0].start, unresolvedData[dataIndex][0].end),
                            unresolved: unresolvedData[dataIndex][1]
                        });
                    }
                }));

            if (this.versionData && this.versionData.length > 0) {
                var markers = Markers().data(this.versionData).xDomain(graph.xDomain()).xAccessor(function(d) {
                    return d[0];
                });
                graph.prependLayer('versions', markers);
            }

            var chartOptions = {};

            if (this.axisOptions.includeXAxis) {
                chartOptions.xAxisValue = function(d) {
                    if (this.period === "hourly") {
                        return moment(d).format("ha DD MMM YYYY");
                    } else {
                        return moment(d).format("DD MMM YYYY");
                    }
                };
            }

            if (this.axisOptions.includeYAxis) {
                chartOptions.yAxisValue = function(d) {
                    var noDecimals = d3.format("d")(d);
                    if (noDecimals) {
                        return JIRA.NumberFormatter.format(parseInt(noDecimals));
                    } else {
                        return noDecimals;
                    }
                };
            }

            var chart = Chart(graph, chartOptions).data(unresolvedData).extend({
                xAccessor: function() {
                    return function(d) {
                        return d[0].end;
                    };
                }
            });

            svgElement.height(this.chartHeight);

            chart(d3.select(svgElement.get(0)));
        }
    });

    /**
     * Merge the Unresolved Data and domain into a form usable by the LinePlot
     * @param {Number[]} unresolvedData
     * @param {Object[]} domain
     * @returns {Object[]}
     */
    function formatAsLineData(unresolvedData, domain) {
        var gap = domain[1].start - domain[0].end;
        var length = domain[0].end - domain[0].start;
        var end = domain[0].start - gap;
        var start = end - length;
        var data = [
            [{start: start, end: end}, 0]
        ];

        domain.forEach(function(x, index) {
            data.push([x, unresolvedData[index]]);
        });
        return data;
    }
});
