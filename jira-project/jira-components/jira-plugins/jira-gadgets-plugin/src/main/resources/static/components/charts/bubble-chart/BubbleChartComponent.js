define('jira-dashboard-items/components/charts/bubble-chart/bubble-chart-component', [
    'wrm/context-path',
    'jquery',
    'backbone',
    'jira-dashboard-items/components/charts/components/chart',
    'jira-dashboard-items/components/charts/components/colored-circle-plot',
    'jira-dashboard-items/lib/d3',
    'jira-dashboard-items/lib/c3'
], function(wrmContextPath, $, Backbone, Chart, ColoredCirclePlot, d3, c3) {
    'use strict';

    var BubbleChartComponent = Backbone.View.extend({
        template: JIRA.DashboardItem.BubbleChartComponent.Templates.Container,

        initialize: function(options) {
            this.options = options || {};
            this.colors = options.colors || ['red', 'blue'];
            this.model = options.model;

            this.xAxisTitleLabel = this.model.get('xAxisLabel');
            this.yAxisTitleLabel = this.model.get('yAxisLabel');
            this.xScaleConstructor = (this.model.get('useLogarithmicScale')) ? d3.scale.log : d3.scale.linear;
            this.yScaleConstructor = (this.model.get('useLogarithmicScale')) ? d3.scale.log : d3.scale.linear;

            this.AMOUNT_TO_INCREASE_CHART_TO_PREVENT_CLIPPING = 10;
            this.X_TITLE_LABEL_BUFFER = 25;
            this.HOVER_STATE_CLASS_NAME = 'hovered';
            this.SELECTED_CIRCLE_CLASS = 'selected';
            this.BACKING_CIRCLE_CLASS = 'backing';
            this.TOOLTIP_DELAY_APPEAR_TIME = 0;
            this.TOOLTIP_CLASS = 'bubble-chart-component-tooltip';

            this.inlineDialog = null;
        },

        render: function() {
            var documentClick = this._handleDocumentClick.bind(this);
            var showLayer = this._handleShowLayer.bind(this);
            $(document).on('click', documentClick);
            $(document).on('showLayer', showLayer);

            // Remove event handlers when the gadget is destroyed
            this.$el.on('remove', function() {
                $(document).off('click', documentClick);
                $(document).off('showLayer', showLayer);
            }.bind(this));

            this.listenTo(this.model, {
                'change:highlightedCircle': this._changeHighlight,
                'change:selectedCircle':  this._changeSelection
            });

            this.$el.html(this.template({ id: this.options.id }));

            var bubblePlot = this._createBubblePlot();
            var bubblePlotGraph = c3.layerable()
                .data(this.model.get('data'))
                .addLayer('bubble-plot', bubblePlot);
            var chart = this._createChart(bubblePlotGraph);
            var $chartElement = this.$('.bubble-chart-component-plot');
            this._setInitialHeightOfChart($chartElement);

            var svg = d3.select($chartElement.get(0));
            svg.call(chart);

            this._preventClippingTheTopMostLabelOnYAxis($chartElement);
            this._addGridLinesToChart($chartElement);
            this._removeTicksWithoutText($chartElement);
        },

        /**
         * Get the circle from the DOM corresponding to the given issue key
         * @param issueKey The issue key, e.g. JC-426
         * @returns {DOM Object}
         * @private
         */
        _circleForIssueKey: function(issueKey) {
            return this.$('circle:data("' + issueKey + '")').get(0);
        },

        /**
         * Remove any backing circle currently in the component
         * @private
         */
        _removeAllBackingCircles: function() {
            this.$('circle.' + this.BACKING_CIRCLE_CLASS).remove();
        },

        _handleDocumentClick: function() {
            if ((!this.model.get('highlightedCircle') || !this.model.get('highlightedCircle').issueKey)) {
                if (this.model.get('selectedCircle') && this.model.get('selectedCircle').issueKey) {
                    this._removeSelectedClassFromAllCircles();
                    this.model.unset('selectedCircle');
                }
            }
        },

        _handleShowLayer: function(e, n, target) {
            // We set a custom property on our inline dialogs to tie them to their parent gadget.
            // Then if the dialog that is currently about to show doesn't match this id, we update the selected
            // state for this gadget. This is needed for the scenario where we click from one gadget's circle
            // to another gadget's circle.
            if (target.popup &&
                (target.popup.dashboardItemId !== this.options.id) &&
                this.inlineDialog) {
                this.model.unset('selectedCircle');
            }
        },

        /**
         * This is the handler that is called when the highlighted circle changes
         * @param model The model that changed
         * @param selection The new highlighted selection
         * @private
         */
        _changeHighlight: function(model, selection) {
            if (!selection.issueKey) {
                // Remove highlight styles from all circles
                d3.selectAll('.' + this.HOVER_STATE_CLASS_NAME)
                    .classed(this.HOVER_STATE_CLASS_NAME, false)
                    .attr('r', function() {
                        return Number(d3.select(this).attr('r')) - 1;
                    });

                return;
            }

            var circle = this._circleForIssueKey(selection.issueKey);
            var radius = Number(d3.select(circle).attr('r'));
            d3.select(circle).classed(this.HOVER_STATE_CLASS_NAME, true);
            d3.select(circle).attr('r', radius + 1);
            this._positionToolTipForCircle(circle, selection.index);
        },

        /**
         * This is the handler that is called when the selected circle changes
         * @param model The model that changed
         * @param selection The new selected selection
         * @private
         */
        _changeSelection: function(model, selection) {
            if (this.inlineDialog) {
                this.inlineDialog.hide();
                this.inlineDialog = undefined;
            }

            this._removeAllBackingCircles();

            // If we click from one circle to a circle in a different gadget, `selection` will be undefined.
            // In this case, we don't want to remove the selected class.
            // However, if we click from one circle to anywhere on the body, `selection` will be undefined as well.
            // In this case, we want to remove the selected class. We do this in the document.click handler above.

            if (!selection) {
                return;
            }

            this._removeSelectedClassFromAllCircles();

            var circle = this._circleForIssueKey(selection.issueKey);
            $(circle).tipsy('hide');

            this._addBackingCircleForSelectedCircle(circle);

            model.set('isSelectedCircleInTopColorBucket', selection.isTopColorBucket);

            var issue = model.getIssueForIssueKey(selection.issueKey);
            var inlineDialogContent = this._createInlineDialogContent(
                selection.issueKey,
                issue.bubbleDomain,
                issue.bubbleRange,
                issue.bubbleRadius,
                issue.bubbleColorValue
            );

            var newInlineDialog = this._createInlineDialog(
                $(circle),
                'bubble-chart-dialog-' + selection.issueKey,
                inlineDialogContent
            );

            this.inlineDialog = newInlineDialog;
            this.inlineDialog.show();
        },

        _removeSelectedClassFromAllCircles: function() {
            d3.select(this.el).selectAll('circle.' + this.SELECTED_CIRCLE_CLASS).classed(this.SELECTED_CIRCLE_CLASS, false);
        },

        /**
         * Removes any ticks and their respective grid lines that occur in a group without text.
         * This is likely to occur when using a log scale.
         * @param $chartElement The element that contains the chart
         * @private
         */
        _removeTicksWithoutText: function($chartElement) {
            var tickGroups = $chartElement.find('g.tick');
            tickGroups.each(function(index) {
                var textContent = $('text', this).text();
                if (textContent === '') {
                    $(this).remove();
                }
            });
        },

        /**
         * Tweak the spacing on the chart to ensure that we have enough room at the top for labels at the very top
         * of the y axis to appear without being clipped. This must be done after the graph is already rendered.
         * @param $chartElement
         * @private
         */
        _preventClippingTheTopMostLabelOnYAxis: function($chartElement) {
            var increase = this.AMOUNT_TO_INCREASE_CHART_TO_PREVENT_CLIPPING;

            // Shift each of the sub-groups of the chart (the x and y axes, and the actual plot), and shift them downwards
            // by enough to prevent the top label clipping.
            $chartElement.children('g').each(function(i, el) {
                d3.select(el)
                    .attr('transform', function() {
                        var originalX = d3.transform(d3.select(el).attr('transform')).translate[0];
                        var originalY = d3.transform(d3.select(el).attr('transform')).translate[1];
                        var shiftedY = originalY + increase;

                        return 'translate(' + originalX + ', ' + shiftedY + ')';
                    });
            });

            // We also need to increase the height of the chart element so the bottom axis doesn't clip.
            $chartElement.height('+=' + increase);
        },

        /**
         * Adds horizontal and vertical grid lines to the chart. This must be called after the chart is drawn
         * into the page, since we rely on the fact that we can use the groups created for the ticks, and append
         * our new grid lines in there.
         * @param $chartElement The element that contains the chart
         * @private
         */
        _addGridLinesToChart: function($chartElement) {
            var xAxisTickGroups = $chartElement.find('.xAxis g.tick');
            var graph = d3.select($chartElement.get(0)).select('.graph');
            var xAxis = $chartElement.find('.xAxis').get(0);
            var graphWidth = $chartElement.find('.graph-border').attr('width');
            var graphHeight = $chartElement.find('.graph-border').attr('height');
            var gridTickClass = 'grid-tick';

            var yAxisTickGroups = $chartElement.find('.yAxis g.tick');
            addValueTickClassToGroups(xAxisTickGroups);
            addValueTickClassToGroups(yAxisTickGroups);

            // Add the vertical grid lines, which extend off the x-axis.
            this._addVerticalGridLines(xAxisTickGroups, -graphHeight, gridTickClass);

            // Add the horizontal grid lines, which extend off the y-axis.
            this._addHorizontalGridLines(yAxisTickGroups, graphWidth, gridTickClass);

            // Add the class used to designate a value tick to the given group. Useful for styling
            // the ticks that point at a value differently to styling the grid lines.
            function addValueTickClassToGroups(groups) {
                var valueTickClass = 'value-tick';
                d3.selectAll(groups)
                    .selectAll('line')
                    .classed(valueTickClass, true);
            }
        },

        _addHorizontalGridLines: function(yAxisTickGroups, xAxisWidth, gridTickClass) {
            return d3.selectAll(yAxisTickGroups)
                .append('line')
                .attr('x2', xAxisWidth)
                .classed(gridTickClass, true);
        },

        _addVerticalGridLines: function(xAxisTickGroups, yAxisHeight, gridTickClass) {
            return d3.selectAll(xAxisTickGroups)
                .append('line')
                .attr('y2', yAxisHeight)
                .attr('x2', 0)
                .classed(gridTickClass, true);
        },

        _setInitialHeightOfChart: function($chartElement) {
            var width = this.$el.width();
            $chartElement.width(width);
            $chartElement.height(width * 0.5);
        },

        /**
         * Apply an x- and y-axis to a graph
         * @param graph The graph to apply the axes to
         * @returns {Chart} A chart with a graph and axes
         * @private
         */
        _createChart: function(graph) {
            var chart = c3.borderLayout()
                .xAccessor(function(d) { return d.bubbleDomain; })
                .yAccessor(function(d) { return d.bubbleRange; });
            var content = this._createGraphContentLayer(graph);
            var xAxis = this._createXAxis();
            var yAxis = this._createYAxis();

            chart.center(content);
            chart.west(yAxis);
            chart.south(xAxis);

            return chart.data(this.model.get('data'));
        },

        _createXAxis: function() {
            return c3.labelledAxis()
                .orient('bottom')
                .height(this.X_TITLE_LABEL_BUFFER * 2)
                .extend(function() {
                    this.selection().classed('xAxis', true);
                })
                .xAccessor(function(d) { return d.bubbleDomain; })
                .xScaleConstructor(this.xScaleConstructor)
                .axisConstructor(function() {
                    return d3.svg.axis()
                        .ticks(4, ',d');
                })
                .text(this.xAxisTitleLabel);
        },

        _createYAxis: function() {
            return c3.labelledAxis()
                .orient('left')
                .width(60)
                .extend(function() {
                    this.selection().classed('yAxis', true);
                })
                .yAccessor(function(d) { return d.bubbleRadius; })
                .yScaleConstructor(this.yScaleConstructor)
                .axisConstructor(function() {
                    return d3.svg.axis()
                        .ticks(5, ',d');
                })
                .text(this.yAxisTitleLabel);
        },

        _createGraphContentLayer: function(graph) {
            return c3.layerable()
                .extend(function() {
                    this.selection().classed('graph', true);
                })
                .addLayer('graph', graph);
        },

        /**
         * Get a unique identifier for each tooltip title in the component
         * @param index The index of the respective data element in the data array
         * @returns {string} A string that uniquely identifies each tooltip title
         * @private
         */
        _tooltipTitleSpanIdForIndex: function(index) {
            return 'bubble-chart-' + this.options.id + '-tooltip-' + index;
        },

        /**
         * Add AUI tooltips to each circle in the plot. These will appear on mouseenter.
         * @param data The data being plotted.
         * @param titleAccessor A function for accessing the title for a data element.
         * @param radius A function for computing the radius value for a data element subject to domain and range.
         * @private
         */
        _addTooltipsToCircles: function(data, titleAccessor, radius) {
            var backboneView = this;

            // If we lazily create tooltips--like creating a tooltip for a circle only when the circle is first
            // hovered--the tooltip won't appear until the next hover after we create it, i.e. the tooltip won't appear
            // for the first mouse hover if we don't create all of them up front.

            this.$('circle').each(function(index, element) {
                $(element).tooltip({
                    html: true,
                    title: function() {
                        // Tipsy/AUI tooltip only allows custom arrow positioning via `offset` in one dimension,
                        // but when we use it with SVGs, the tooltip is wrong in both its horizontal and vertical
                        // position.
                        // The tooltip also appears at the top of the DOM, so it's really hard to identify which
                        // circle the tipsy is associated with.
                        // Also, the tipsy doesn't allow us to give it a custom class (and $(element).tooltip
                        // returns the element to which the tipsy will be associated, not the tipsy itself), so
                        // using sensible classes on the tipsy directly via `index` isn't possible.
                        // To work around these limitations, we give tipsy an HTML string as its title (which it
                        // allows), with an id that will be unique to this tipsy. Then we can later access that
                        // tooltip, and use its parent accessors to get the tooltip itself, and thus we can
                        // position it correctly.
                        var spanId = backboneView._tooltipTitleSpanIdForIndex(index);
                        return '<span id="' + spanId + '">' + titleAccessor(data[index]) + '</span>';
                    },
                    trigger: 'manual',
                    gravity: 'w',
                    className: backboneView.TOOLTIP_CLASS // This class hides the tooltips by default. They will
                                                          // be un-hidden after being positioned correctly in
                                                          // each bubble's mouseover callback.
                });
            });
        },

        /**
         * Add a clip path around a certain frame of size and height to hide anything that occurs outside of that frame
         * @param containerSelection The d3 selection to which the clip path will be applied
         * @param width The width of the clip path rect
         * @param height The height of the clip path rect
         * @private
         */
        _addClipToHideCirclesOutsideBorder: function(containerSelection, width, height) {
            var clipId = 'bubble-chart-component-clip-' + this.options.id;

            // Add a clip so that bubbles outside the border do not appear
            d3.select(this.$('g.layer.graph').get(0))
                .insert('defs')
                .append('svg:clipPath')
                    .attr('id', clipId)
                    .append('rect')
                        .attr('width', width)
                        .attr('height', height);

            containerSelection.attr('clip-path', 'url(#' + clipId + ')');
        },

        /**
         * Add a graph border of width and height
         * @param width The width of the bordering frame
         * @param height The height of the bordering frame
         * @private
         */
        _addBorderToFrameOfSize: function(width, height) {
            // Add a border
            d3.select(this.$('g.layer.graph').get(0))
                .insert('rect')
                    .classed('graph-border', true)
                    .attr('width', width)
                    .attr('height', height);
        },

        /**
         * Create a plot of colored circles corresponding to the data given.
         * @returns {ColoredCirclePlot} The plot of the data
         * @private
         */
        _createBubblePlot: function() {
            var backboneView = this;

            return ColoredCirclePlot()
                .extend(function() {
                    backboneView._addTooltipsToCircles(backboneView.model.get('data'), this.titleAccessor(), this.radius());
                    backboneView._addClipToHideCirclesOutsideBorder(this.selection(), this.width(), this.height());
                    backboneView._addBorderToFrameOfSize(this.width(), this.height());
                })
                .xScaleConstructor(this.xScaleConstructor)
                .yScaleConstructor(this.yScaleConstructor)
                .xAccessor(function(d) { return d.bubbleDomain; })
                .yAccessor(function(d) { return d.bubbleRange; })
                .radiusAccessor(function (d) { return d.bubbleRadius; })
                .colorAccessor(function(d) { return d.bubbleColorValue; })
                .update(function(event, d) {
                    var titleAccessor = this.titleAccessor();

                    event.selection
                        .each(function(d, i) {
                            $(this).data(titleAccessor(d), 'true');
                        })
                        .on('click', this.click())
                        .on('mouseenter', this.mouseenter())
                        .on('mouseleave', this.mouseleave());
                })
                .extend({
                    colorScaleConstructor: c3.inherit('colorScaleConstructor', d3.scale.quantile),
                    colorRange: c3.inherit('colorRange').onDefault(function() {
                        return backboneView.model.get('colors');
                    }),
                    colorDomain: c3.inherit('colorDomain').onDefault(function() {
                        return backboneView.model.get('colorDomain');
                    }),
                    titleAccessor: c3.prop(function(data) { return data.key; }),
                    xRange: function() {
                        var width = c3.checkIsNumber(this.width());
                        var largestCircleRadius = c3.checkIsNumber(d3.max(backboneView.model.get('data'), this.radius()));
                        return [0, width - largestCircleRadius - 5];
                    },
                    yRange: function() {
                        var height = c3.checkIsNumber(this.height());
                        var largestCircleRadius = c3.checkIsNumber(d3.max(backboneView.model.get('data'), this.radius()));
                        return [height, largestCircleRadius + 5];
                    },
                    click: function() {
                        var titleAccessor = this.titleAccessor();
                        var colorConstructor = this.color();
                        var colorRange = this.colorRange();

                        return function(data) {
                            var indexInRange = colorRange.indexOf(colorConstructor(data));

                            AJS.trigger('analyticsEvent', {
                                name: 'jira.dashboard.gadgets.bubble-chart.bubble-clicked',
                                data: {
                                    colorBucket: indexInRange
                                }
                            });

                            backboneView.model.set('selectedCircle', {
                                issueKey: titleAccessor(data),
                                colorBucket: indexInRange,
                                isTopColorBucket: indexInRange === colorRange.length - 1
                            });
                        };
                    },
                    mouseenter: function() {
                        var colorRange = this.colorRange();
                        var color = this.color();
                        var titleAccessor = this.titleAccessor();

                        return function(data, index) {
                            var indexInRange = colorRange.indexOf(color(data));

                            $(this).tipsy('show');

                            backboneView.model.set('highlightedCircle', {
                                issueKey: titleAccessor(data),
                                colorBucket: indexInRange,
                                index: index
                            });
                        };
                    },
                    mouseleave: function() {
                        return function() {
                            $(this).tipsy('hide');

                            backboneView.model.set('highlightedCircle', {
                                issueKey: undefined,
                                colorBucket: undefined
                            });
                        };
                    }
                });
        },

        _positionToolTipForCircle: function(circle, index) {
            // Comments in `_addTooltipsToCircles` might be helpful
            // in understanding what we're doing here and why.
            //
            // The tooltip that appears will be in the wrong position so we need to override
            // that, taking into account the radius of the hovered circle.
            //
            // At this stage, the tooltip doesn't exist in the DOM, so we have to execute the
            // resizing after it is rendered (hence the setTimeout). The AUI tooltip/tipsy doesn't provide a callback
            // to indicate when the tooltip has appeared.
            //
            // And also, we need to hide the tooltip before we've finished positioning it--if we
            // don't hide it, there will be a flicker as it moves from its initial position to the
            // one that we provide it.
            //
            // We do it like this:
            //   1. Set up the tooltip in `_addTooltipsToCircles`,
            //      and add the TOOLTIP_CLASS, which sets its display to none so that it's not
            //      visible while incorrectly positioned.
            //   2. After the tooltip has had time to be inserted (but is still invisible), set its
            //      horizontal and vertical position to the correct position.
            //   3. Remove the TOOLTIP_CLASS so that the tooltip becomes visible

            setTimeout(function() {
                // Select the corresponding title span
                var spanId = this._tooltipTitleSpanIdForIndex(index);
                var $tooltip = $('#' + spanId).parent().parent();

                // Override all of the AUI tooltip's positioning.
                var circleRadius = Number($(circle).attr('r'));
                var heightOfTooltip = 20;
                // `getBoundingClientRect` is to the viewport, so doesn't take into account scroll position. We need to add this.
                var circleLeft = circle.getBoundingClientRect().left + window.pageXOffset;
                var circleTop = circle.getBoundingClientRect().top + window.pageYOffset;

                $tooltip.css({
                    'top': circleTop + circleRadius - heightOfTooltip,
                    'left': circleLeft + 2 * circleRadius
                });

                // Remove the class that hides the tooltip initially
                d3.select($tooltip.get(0)).classed(this.TOOLTIP_CLASS, false);
            }.bind(this), this.TOOLTIP_DELAY_APPEAR_TIME + 10);
        },

        /**
         * Construct the HTML to be displayed in the inline dialog when a bubble is clicked
         * @param key The issue key
         * @param x The x value of the issue
         * @param y The y value of the issue
         * @param radius The value that correlates to the radius of the bubble
         * @param color The value that correlates to the color of the bubble
         * @param colorDomain The color domain for the
         * @returns {String} The HTML to display within an inline dialog.
         * @private
         */
        _createInlineDialogContent: function(key, x, y, radius, color) {
            return JIRA.DashboardItem.BubbleChartComponent.Templates.InlineDialog({
                url: wrmContextPath() + '/browse/' + key,
                title: key,
                x: x,
                xLabel: this.model.get('xAxisLabel'),
                y: y,
                yLabel: this.model.get('yAxisLabel'),
                radius: radius,
                radiusLabel: this.model.get('sizeAxisLabel'),
                color: color,
                colorLabel: this.model.get('colorAxisLabel'),
                isTopColorBucket: this.model.get('isSelectedCircleInTopColorBucket')
            });
        },

        /**
         * Create a new inline dialog styled for the bubble chart dashboard item
         * @param trigger The circle that was clicked to trigger the creation of the dialog
         * @param id A unique identifier for the inline dialog
         * @param content The html to be displayed in the dialog
         * @returns {InlineDialog}
         * @private
         */
        _createInlineDialog: function(trigger, id, content) {
            var dialogPadding = 10;
            var dialogWidth = 350;

            var dialog = AJS.InlineDialog($(trigger), id, function(element, trigger, showPopup) {
                element.addClass('bubble-chart-inline-dialog');
                element.css({padding: dialogPadding + 'px'});
                element.html(content);
                showPopup();
                return false;
            }, {
                hideDelay: 60000, // 60 seconds
                gravity: 'w',
                cacheContent: false,
                width: dialogWidth,
                fadeTime: 0
            });

            dialog.dashboardItemId = this.options.id;

            return dialog;
        },

        /**
         * Adds a circle behind another circle in the component. This gives us the double border effect when a circle
         * has been clicked.
         * @param circle The selected circle that needs to have a backing circle applied.
         * @private
         */
        _addBackingCircleForSelectedCircle: function(circle) {
            var backingCircle = circle.cloneNode(true);
            var currentRadius = Number($(backingCircle).attr('r'));

            d3.select(circle).classed(this.SELECTED_CIRCLE_CLASS, true);
            d3.select(backingCircle).classed(this.BACKING_CIRCLE_CLASS, true);
            $(backingCircle).attr('r', currentRadius + 3);
            $(circle).before(backingCircle);
        }
    });

    return BubbleChartComponent;
});
