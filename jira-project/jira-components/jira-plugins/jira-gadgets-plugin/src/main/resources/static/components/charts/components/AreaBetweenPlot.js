define('jira-dashboard-items/components/charts/components/area-between-plot', [
    'jira-dashboard-items/components/charts/components/dualplottable',
    'jira-dashboard-items/lib/c3'
], function(
    DualPlottable,
    c3
) {

    /**
     * Creates an area between two y values. This depends on dual plottable, therefore the yAccessor must return an
     * array of size of at least 2.
     *
     * @exports jira-dashboard-items/components/charts/components/area-between-plot
     * @returns {c3.component}
     */
    return function() {

        return c3.component('areaBetweenPlot')
            .extend(c3.areaPlot())
            .extend(DualPlottable())
            .extend({

                /**
                 * Creates a function to determine the bottom of the area plot at a certain x position.
                 * @returns {Function}
                 */
                areaBottom: function () {
                    var y0 = this.y0();
                    return function (d) {
                        return y0(d);
                    };
                },

                /**
                 * Creates a function to determine the top of the area plot at a certain x position.
                 * @returns {Function}
                 */
                areaTop: function () {
                    var y1 = this.y1();
                    return function (d) {
                        return y1(d);
                    };
                },

                /**
                 * Calculate the area range for the given point.
                 * @param event
                 */
                update: function (event) {
                    var area = this.areaConstructor()()
                        .x(this.x());

                    area.y0(this.areaBottom());

                    area.y1(this.areaTop());
                    event.selection.attr('d', area);
                }
            });
    };
});
