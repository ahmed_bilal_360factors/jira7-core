define("jira-dashboard-items/in-progress", [
    'jira/util/formatter',
    'wrm/context-path',
    'jquery',
    'underscore',
    'jira-dashboard-items/components/search-results-config',
    'jira-dashboard-items/components/search-results'
], function(formatter, wrmContextPath, $, _, SearchResultsConfig, SearchResults) {
    'use strict';

    var DashboardItem = function (API) {
        this.API = API;
    };

    DashboardItem.prototype.render = function (context, preferences) {
        var self = this;

        self.API.showLoadingBar();
        self.API.setTitle(formatter.I18n.getText("gadget.in.progress.title"));

        var displayPrefs = $.extend({}, preferences);
        displayPrefs.jql = "status = 'In Progress' AND resolution is EMPTY AND assignee = currentUser() ORDER BY priority DESC, created ASC";

        this.searchResults = new SearchResults({
            context: context,
            preferences: displayPrefs,
            onContentLoaded: this.API.resize.bind(this.API),
            onEmptyResult: function () {
                context.append(JIRA.DashboardItem.InProgress.Templates.noResults({
                    href: wrmContextPath() + "/issues/?jql=" + encodeURIComponent(displayPrefs.jql)
                }));
                self.API.resize();
            },
            onError: function () {
                context.empty().append(JIRA.DashboardItem.InProgress.Templates.errorResult());
                self.API.resize();
            }
        });

        this.searchResults.render().always(this.API.hideLoadingBar.bind(this.API));
        this.searchResults.on("sorted", function(eventData) {
            preferences.sortBy = eventData.sortBy;
            if(self.API.isEditable()) {
                self.API.savePreferences(preferences);
            } else {
                self.render(context, preferences);
            }
        });
        self.API.initRefresh(displayPrefs, this.searchResults.render);
    };

    DashboardItem.prototype.renderEdit = function (element, preferences) {
        var self = this;

        var searchResultsConfig = new SearchResultsConfig({
            context: element,
            preferences: preferences,
            onContentLoaded: function () {
                self.API.hideLoadingBar();
                self.API.resize();
            },
            onCancel: this.API.closeEdit.bind(this.API),
            onSave: this.API.savePreferences.bind(this.API),
            gadgetAPI: self.API
        });

        var $form = searchResultsConfig.renderConfig();

        this.API.once("afterRender", function () {
            if (element.width() < 350) {
                $form.addClass("top-label");
            }
            self.API.showLoadingBar();
        });
    };

    return DashboardItem;
});
