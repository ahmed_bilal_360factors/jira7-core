/**
 * Defines utility functions for testing lines/areas
 */
define('jira-dashboard-items/components/charts/components/c3-test-util', [
    'underscore'
], function(
    _
) {

        return {


            /**
             * Given a path it returns a mapping of x to multiple y points. e.g. 0 -> 0, 100, 1 -> 30, 50.
             *
             * @param {String} path from a d attr from a path tag
             * @return {Object}
             */
            extractPathPoints: function(path) {
                var points = path.split(/[a-zA-Z]/);

                var xPointY = {};
                var xPointYSet = {};

                points.forEach(function(point) {
                    var coordinates = point.split(",");
                    if (coordinates.length < 2) {
                        return;
                    }
                    var x = parseFloat(coordinates[0]);
                    var y = parseFloat(coordinates[1]);

                    //fixes problems where d3.svg.area makes it 49.9999 not 50.
                    if (("" + x).match(/\d+\.9999/)) {
                        x = Math.round(x);
                    }

                    //fixes problems where d3.svg.area makes it 49.9999 not 50.
                    if (("" + y).match(/\d+\.9999/)) {
                        y = Math.round(y);
                    }



                    if (!xPointY[x]) {
                        xPointY[x] = [];
                        xPointYSet[x] = {};
                    }

                    if (!xPointYSet[x][y]) {
                        xPointY[x].push(y);
                        xPointYSet[x][y] = true;
                    }
                });

                return xPointY;
            },

            graphWidth: function(data) {
                var min = _.min(data, function(d) {
                    return d[0];
                });

                var max = _.max(data, function(d) {
                    return d[0];
                });

                return max[0] - min[0];
            }
        };
    }
);
