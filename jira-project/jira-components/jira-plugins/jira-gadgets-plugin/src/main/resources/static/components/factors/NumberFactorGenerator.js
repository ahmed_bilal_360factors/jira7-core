/**
* @name NumberFactorGenerator
*
* Implements functionality to find the factors of a given number, e.g. 8 would result in [1,2,4,8]
*/
define('jira-dashboard-items/components/factors/number-factor-generator', function() {

    return {
        /**
         * Generates all the factors of a given positive number into a sorted array.
         * @param {Number} n, larger than 1, to find the factors of
         * @return {Number[]}
         *
         */
        generate: function(n) {
            if (n < 1) {
                throw new Error('JIRA.NumberFactorGenerator.generate: number must be >= 1');
            }

            if (n === 1) {
                return [1];
            }

            var factors = [1];

            for (var i = 2; i <= n / 2; ++i) {
                if (n % i === 0) {
                    factors.push(i);
                }
            }

            factors.push(n);

            return factors;
        }
    };
});
