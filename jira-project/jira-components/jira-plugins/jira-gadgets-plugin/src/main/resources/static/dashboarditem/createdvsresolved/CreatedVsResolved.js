define("jira-dashboard-items/createdvsresolved", [
    'jira/util/formatter',
    'wrm/context-path',
    'jquery',
    'jira/moment',
    'jira-dashboard-items/createdvsresolved-config-view',
    'jira-dashboard-items/components/charts/createdvsresolved-chart',
    'jira-dashboard-items/components/charts/unresolved-trend-chart',
    'jira-dashboard-items/common-functions',
    'underscore'
], function(
    formatter,
    wrmContextPath,
    $,
    moment,
    ConfigView,
    CreatedVsResolvedChart,
    UnresolvedTrendChart,
    DashboardHelperFunctions,
    _) {

    var Templates = JIRA.DashboardItem.CreatedVsResolved.Templates;

    var PERIOD_I18N = {
        'hourly': formatter.I18n.getText('gadget.common.period.hourly'),
        'daily': formatter.I18n.getText('gadget.common.period.daily'),
        'weekly': formatter.I18n.getText('gadget.common.period.weekly'),
        'monthly': formatter.I18n.getText('gadget.common.period.monthly'),
        'quarterly': formatter.I18n.getText('gadget.common.period.quarterly'),
        'yearly': formatter.I18n.getText('gadget.common.period.yearly')
    };

    /**
     *
     * @param {InlineGadgetAPI} API
     * @param {Object} [options] for gadget
     * @constructor
     */
    var DashboardItem = function(API, options) {
        this.API = API;
        this.options = options || {};
    };

    /**
     * Render the configuration screen for the created vs resolved gadget
     * @param {Node} element to render into
     * @param {Object} preferences for object.
     */
    DashboardItem.prototype.renderEdit = function(element, preferences) {
        var gadget = this;

        if (gadget.resizeHandler)
        {
            $(element).removeResize(gadget.resizeHandler);
            delete gadget.resizeHandler;
        }


        var dashboardPreferences = gadget._backwardCompatability(preferences);
        var configViewPreferences = gadget._dashboardToConfigViewPreferences(dashboardPreferences);

        $(".createdvsresolved-gadget", element).html(Templates.ConfigContainer());

        gadget.API.setTitle( formatter.I18n.getText('gadget.created.vs.resolved.title') );

        this.configView = new ConfigView({
            id: gadget.API.getGadgetId(),
            el: $(".createdvsresolved-config-container", element),
            projectFilterDelay: this.options.delay
        });

        this.configView.render(configViewPreferences);

        this.configView.bind("cancel", function() {
            gadget.API.closeEdit();
        });

        this.configView.bind("submit", function(gadgetPreferences) {
            gadget.API.resize();

            var dashboardPreferences = gadget._configViewToDashboardPreferences(gadgetPreferences);

            gadget.API.savePreferences(dashboardPreferences).fail(function(message) {
                if (message.status === 401) {
                    gadget.configView.unauthorisedFormSave();
                } else {
                    var errors = parseErrorMessage(message.responseText);
                    gadget.configView.errorSavingForm(errors);
                }
            });
        });

        this.configView.bind("layoutUpdate", function() {
            gadget.API.resize();
        });

        this.API.once("afterRender", function () {
            if (element.width() < 350) {
                $(element).find("form.aui").addClass("top-label");
            }
            gadget.API.resize();
        });

    };


    /**
     * Render the configured created vs resolved gadget.
     * @param {node} element to apply gadget to
     * @param {Object} preferences for gadget that have been configured.
     */
    DashboardItem.prototype.render = function(element, preferences) {
        var gadget = this;

        var gadgetElement = $(".createdvsresolved-gadget", element);
        gadgetElement.html(Templates.RenderChart({showUnresolvedTrend:preferences.showUnresolvedTrend}));

        gadget.API.initRefresh(preferences, _.bind(gadget.render, gadget, element, preferences));
        gadget.API.showLoadingBar();

        preferences = gadget._backwardCompatability(preferences);

        var dateInfoRequest = this._requestDateData(preferences);

        $.when(dateInfoRequest).done(function(dateInfoResponse) {
            gadget.API.hideLoadingBar();

            var formattedData = gadget._generateCreatedVsResolvedData(dateInfoResponse.results);
            var versionData = gadget._generateVersionData(preferences.type, preferences.versionLabel, dateInfoResponse.versions, formattedData.domain);

            gadget.initialRender = true;
            gadget._renderChart($(element).attr('id'), gadgetElement, preferences, dateInfoResponse, formattedData, versionData);
            gadget.resizeHandler = function() {
                if(gadget.initialRender) {
                    gadget.initialRender = false;
                } else {
                    _.bind(gadget._renderChart, gadget, $(element).attr('id'), gadgetElement, preferences, dateInfoResponse, formattedData, versionData)();
                }
            };
            $(element).resize(_.throttle(gadget.resizeHandler, 600));

        }).fail(function(message) {
            if (message.statusText === 'timeout') {
                gadgetElement.html(JIRA.DashboardItem.CreatedVsResolved.Templates.Timeout());
            } else if (message.status === 400) {
                var errors = parseErrorMessage(message.responseText);

                gadgetElement.html(JIRA.DashboardItem.CreatedVsResolved.Templates.Errors({
                    errors: errors
                }));
            } else {
                gadgetElement.html(JIRA.DashboardItem.CreatedVsResolved.Templates.ServerError());
            }
            gadget.API.hideLoadingBar();

            gadget.API.resize();

        });
    };

    /**
     * Get the chart data for the given preferences.
     *
     * @param {Object} preferences for the chart
     * @returns {jQuery.deferred}
     * @private
     */
    DashboardItem.prototype._requestDateData = function(preferences) {
        var dateInfoRequestData = {
            jql: preferences.type + "=" + preferences.id,
            period: preferences.periodName,
            daysprevious: preferences.daysprevious,
            operation: preferences.operation,
            field: ['created', 'resolved'],
            includeVersions: preferences.versionLabel && preferences.versionLabel !== 'none'
        };

        if (preferences.showUnresolvedTrend) {
            dateInfoRequestData.field.push('unresolvedTrend');
        }

        return $.ajax({
            method: "GET",
            url: wrmContextPath() + '/rest/gadget/1.0/dateCountInPeriod',
            dataType: "json",
            data: dateInfoRequestData
        });
    };


    /**
     * This takes the REST rest resource results and puts it into a format that the CreatedVsResolvedChart understands.
     * @param {Object[]} results from REST resource
     * @returns {{domain: Array, created: Array, resolved: Array, unresolvedTrend: Array}}
     */
    DashboardItem.prototype._generateCreatedVsResolvedData = function(results) {
        var domain = [];
        var createdValues = [];
        var resolvedValues = [];
        var unresolvedTrend = [];
        results.forEach(function(result) {
            domain.push({
                start: result.start,
                end: result.end
            });

            createdValues.push(result.data.created);
            resolvedValues.push(result.data.resolved);
            if (result.data.unresolvedTrend) {
                unresolvedTrend.push(result.data.unresolvedTrend.count);
            }
        });

        return {
            domain: domain,
            created: createdValues,
            resolved: resolvedValues,
            unresolvedTrend: unresolvedTrend
        };
    };

    /**
     * Given a version response transform it into a format useful for the charts and filter out unwanted versions.
     * @param {String} type of the issue searcher, e.g. project or filter.
     * @param {String} versionsAllowed states what type of versions we should display
     * @param {Object[]} versions from the server
     * @param {Object[]} domain for the graph
     * @returns {*}
     */
    DashboardItem.prototype._generateVersionData = function(type, versionsAllowed, versions, domain) {
        var dashboardItem = this;
        var start = domain[0].start;
        var end = domain[domain.length - 1].end;
        var data = [];
        versions.forEach(function(version) {
            var hasReleaseDate = !!version.releaseDate;
            var isInRange = (start <= version.releaseDate) && (version.releaseDate <= end);
            var isValidVersionName = dashboardItem._isValidVersionName(versionsAllowed, version.name);

            if (hasReleaseDate && isInRange && isValidVersionName) {
                //generate url

                if (type === "filter") {
                    version.name = version.project.key + ":" + version.name;
                }

                version.releaseDatePretty = moment(version.releaseDate).format("DD-MMM-YYYY");

                data.push([version.releaseDate, version]);
            }
        });

        return _.sortBy(data, function(version) {
            return version[0];
        });
    };


    /**
     * Given a JSON string extract the errors into an array.
     * @param {JSON} errorString to parse
     * @returns {string[]} of errors
     */
    function parseErrorMessage(errorString) {
        var errorArray = [];
        try {
            var errorJSON = JSON.parse(errorString);
        } catch (error) {
            return errorArray;
        }
        for (var key in errorJSON){
            if (errorJSON.hasOwnProperty(key)) {
                errorArray.push({
                    key: key,
                    message: errorJSON[key]
                });
            }
        }
        return errorArray;
    }

    /**
     * @typedef Object GadgetPreferences
     * @global
     *
     * @property {String} [projectFilterID] of a selected filter/project
     * @property {String} [periodName] for the period grouping
     * @property {Number} [daysprevious] for looking back by the period
     * @property {Boolean} [isCumulative] for grouping the issue number
     * @property {Boolean} [showUnresolvedTrend] should include the unresolved trend
     * @property {String} [versionLabel] whether to populate the version
     * @property {Boolean} [refresh] whether to check the refresh value
     **/

    /**
     *
     * @param {GadgetPreferences} gadgetPreferences
     * @returns DashboardPreferences
     * @private
     */
    DashboardItem.prototype._backwardCompatability = function(gadgetPreferences) {
        // lenient check if a value is present
        // https://jira.atlassian.com/browse/JRA-59364
        function isBlank(value) {
            return value === null || value === undefined || value === '';
        }

        if (!_.isUndefined(gadgetPreferences.isCumulative) && isBlank(gadgetPreferences.operation)) {
            if (gadgetPreferences.isCumulative) {
                gadgetPreferences.operation = 'cumulative';
            } else {
                gadgetPreferences.operation = 'count';
            }

            delete gadgetPreferences.isCumulative;
        }

        return DashboardHelperFunctions.projectFilterBackwardCompatible(gadgetPreferences);
    };

    /**
     * @typedef Object DashboardPreferences
     * @global
     *
     * @property {Boolean} [isConfigured] whether the dashboard item is configured
     * @property {String} [name] of a selected filter/project
     * @property {String} [type] of the selected filter/project
     * @property {String} [id] of the selected filter/project
     * @property {String} [periodName] for the period grouping
     * @property {Number} [daysprevious] for looking back by the period
     * @property {String} [operation] for grouping the issue number
     * @property {Boolean} [showUnresolvedTrend] should include the unresolved trend
     * @property {String} [versionLabel] whether to populate the version
     * @property {Boolean} [refresh] whether to check the refresh value
     **/


    /**
     * Convert the dashboard preferences type to the format that the config view expects.
     * @param {DashboardPreferences} dashboardPreferences
     * @returns {CreatedVsResolvedConfigViewPreferences}
     * @private
     */
    DashboardItem.prototype._dashboardToConfigViewPreferences = function(dashboardPreferences) {
        return {
            name: dashboardPreferences.name,
            id: dashboardPreferences.id,
            type: dashboardPreferences.type,
            periodName: dashboardPreferences.periodName,
            range: dashboardPreferences.daysprevious,
            operation: dashboardPreferences.operation,
            showUnresolvedTrend: dashboardPreferences.showUnresolvedTrend,
            versionLabel: dashboardPreferences.versionLabel,
            refresh: dashboardPreferences.refresh,
            isConfigured: dashboardPreferences.isConfigured
        };
    };

    /**
     * Convert the config view preferences format to the expected dashboard item preferences format.
     * @param {CreatedVsResolvedConfigViewPreferences} configViewPreferences
     * @returns DashboardPreferences
     * @private
     */
    DashboardItem.prototype._configViewToDashboardPreferences = function(configViewPreferences) {
        var refresh = false;
        if (configViewPreferences.refresh === true) {
            refresh = '15';
        }

        return {
            name: configViewPreferences.name,
            id: configViewPreferences.id,
            type: configViewPreferences.type,
            periodName: configViewPreferences.periodName,
            daysprevious: configViewPreferences.range,
            operation: configViewPreferences.operation,
            showUnresolvedTrend: configViewPreferences.showUnresolvedTrend,
            versionLabel: configViewPreferences.versionLabel,
            refresh: refresh
        };
    };

    /**
     * If all versions area allowed, any version name is valid. Major versions will filter out any
     * minor versions as defined by the _isMinorVersion function
     *
     * @param {String} versionsAllowed The versions allowed for the chart. Either 'all', 'major' or 'none'
     * @param {String} version the version name
     */
    DashboardItem.prototype._isValidVersionName = function(versionsAllowed, version) {
       return versionsAllowed === "all" || versionsAllowed === "major" && !this._isMinorVersion(version);
    };

    /**
     * A version is minor if it contains more than 1 '.' separators or 'alpha' or 'beta' in the name.
     *
     * This logic may seem a bit strange, however it is what it is from JIRA core from ages back.
     * Changing it would mean changing behaviour of the created vs resolved chart for thousands of
     * users.
     *
     * @param {String} version the version name
     */
    DashboardItem.prototype._isMinorVersion = function(version) {
        if(version) {
            var numberOfVersionSeparators = (version.match(/\./g) || []).length;
            var isAlphaOrBetaVersion = version.indexOf("alpha") >= 0 || version.indexOf("beta") >= 0;
            return numberOfVersionSeparators > 1 || isAlphaOrBetaVersion;
        }
        return false;
    };

    /**
     * Render the chart into the dashboard item.
     * @param {String} id of the dashboard item.
     * @param {Node} gadgetElement to render the chart
     * @param {Object} preferences
     * @param {Object} dateInfoResponse includes the raw response for the data.
     * @param formattedData
     * @param versionData
     * @private
     */
    DashboardItem.prototype._renderChart = function(id, gadgetElement, preferences, dateInfoResponse, formattedData, versionData) {
        var gadget = this;

        var createdVsResolvedChartContainer = gadgetElement.find(".createdvsresolved-chart-container");

        var containerWidth = createdVsResolvedChartContainer.outerWidth();
        var containerHeight = containerWidth * 2 / 3;

        //if no trend is shown account for 20px bottom padding of the trend chart and spacing between charts so charts line up
        var createdVsResolvedChartHeight = preferences.showUnresolvedTrend ? containerHeight * 3 / 4 : containerHeight + 20;
        var trendChartHeight = Math.max(containerHeight / 4, 80);

        var createdVsResolvedOptions = {
            id: id,
            data: formattedData,
            versionData: versionData,
            el: createdVsResolvedChartContainer,
            filterUrl: dateInfoResponse.filterUrl,
            axisOptions: {
                includeXAxis: true,
                includeYAxis: true
            },
            cumulative: preferences.operation === "cumulative",
            days: preferences.daysprevious,
            period: preferences.periodName,
            chartHeight: createdVsResolvedChartHeight
        };

        var createdVsResolvedChart = new CreatedVsResolvedChart(createdVsResolvedOptions);

        createdVsResolvedChart.render();

        if (preferences.showUnresolvedTrend) {

            var unresolvedTrendChart = new UnresolvedTrendChart({
                el: gadgetElement.find(".unresolvedtrend-chart-container").get(0),

                id: id,
                data: formattedData.unresolvedTrend,
                domain: formattedData.domain,
                versionData: versionData,
                period: preferences.periodName,
                chartHeight: trendChartHeight,
                axisOptions: {
                    includeXAxis: true,
                    includeYAxis: true
                }
            });

            unresolvedTrendChart.render();
        }

        var chartInfoContainer = gadgetElement.find(".createdvsresolved-chart-info");

        chartInfoContainer.html(Templates.ChartDetailsContent({
            createdCount: dateInfoResponse.totals.created,
            resolvedCount: dateInfoResponse.totals.resolved,
            dayCount: JIRA.NumberFormatter.format(parseInt(preferences.daysprevious)),
            period: PERIOD_I18N[preferences.periodName],
            filterUrl: dateInfoResponse.filterUrl
        }));

        gadget.API.setTitle( formatter.I18n.getText('gadget.created.vs.resolved.title.specific', dateInfoResponse.filterTitle) );

        gadget.API.resize();
    };

    return DashboardItem;
});
