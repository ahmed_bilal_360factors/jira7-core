AJS.test.require([
    'com.atlassian.jira.gadgets:common-test-resources',
    'com.atlassian.jira.gadgets:discrete-legend'
], function () {

        var DiscreteLegend = require('jira-dashboard-items/components/charts/components/discrete-legend/discrete-legend');
        var d3 = require('jira-dashboard-items/lib/d3');
        var $ = require('jquery');
        var _ = require('underscore');
        var Backbone = require('backbone');

        'use strict';

        var WIDTH = 300;
        var HEIGHT = 300;
        var COLORS = ['red', 'white', 'blue'];
        var DATA = [1, 2, 3];

        module('jira-dashboard-items/components/charts/components/discrete-legend', {
            setup: function () {
                var element = d3.select('#qunit-fixture')
                    .append('div')
                    .attr('width', WIDTH)
                    .attr('height', HEIGHT)
                    [0];

                this.model = new Backbone.Model({
                    data: DATA,
                    colors: COLORS
                });

                this.legend = new DiscreteLegend({
                    el: element,
                    model: this.model
                });

                this.legend.render();
            }
        });

        test('Should render a legend using the provided data and colors', function() {
            var rectangles = $('rect.original-segment', this.legend.$el);
            var legendItemWidth = 93;
            var textBoxes = $('text', this.legend.$el);
            var textDataPairs = _.zip(textBoxes, DATA);
            var rectColorPairs = _.zip(rectangles, COLORS);

            _.each(rectangles, function(e) {
                equal(Math.round($(e).attr('width')), legendItemWidth);
            });

            _.each(textDataPairs, function(pair) {
                var text = $(pair[0]).text();
                var data = pair[1];
                equal(text, data);
            });

            _.each(rectColorPairs, function(pair) {
                var rectColor = $(pair[0]).attr('fill');
                var expectedColor = pair[1];
                equal(rectColor, expectedColor);
            });

            equal($('.segment-group').length, this.model.get('colors').length);
        });

        test('Should highlight and unhighlight a segment of the legend', function() {
            var groups = $('.segment-group', this.legend.$el);

            this.model.trigger('change:highlightedSegment', {}, 1);
            ok(groups[1].classList.contains('highlighted'));
            this.model.trigger('change:highlightedSegment', {}, undefined);
            ok(!groups[1].classList.contains('highlighted'));
        });

        test('Should activate and deactivate a segment of the legend', function() {
            var groups = $('.segment-group', this.legend.$el);

            this.model.trigger('change:selectedSegment', {}, 2);
            ok(groups[2].classList.contains('selected'));
            this.model.trigger('change:selectedSegment', {}, undefined);
            ok(!groups[2].classList.contains('selected'));
        });
});
