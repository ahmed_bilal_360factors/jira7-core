define('jira-dashboard-items/lib/d3', ['atlassian/libs/d3-3.4.13'], function(d3) {
    return d3;
});

AJS.namespace('d3', null, require('jira-dashboard-items/lib/d3'));
