define("jira-dashboard-items/text", [
    "jquery",
    "underscore"
], function(
    $,
    _
) {
    "use strict";

    var DashboardItem = function(API, options) {
        this.API = API;
        this.options = options;
    };

    DashboardItem.prototype.render = function(element, preferences) {
        $(element).html(JIRA.DashboardItem.Text.Templates.Content());
        this.API.setTitle(decodeNonASCII(preferences.title));

        // Write the HTML content in an iframe to maintain backwards compatibility with
        // users who are using the old text gadget, which displays content in an iframe.
        var gadget = this;
        this.API.once("afterRender", function() {
            var iframe = $(".text-dashboard-item-container iframe", $(element)).get(0);

            writeContentToIframe(iframe, _.unescape(preferences.html));

            // Add our page's styles into the iframe, which otherwise would just have default styling.
            applyPageStylesToIframe(iframe);
            updateIframeHeightToFitContent(iframe);

            gadget.API.resize();
        });
    };

    var decodeNonASCII = function (text) {
        return $('<div>').html(text).html();
    };

    DashboardItem.prototype.renderEdit = function(element, preferences) {
        var gadget = this;

        preferences.html = decodeNonASCII(_.unescape(preferences.html));
        preferences.title = _.escape(decodeNonASCII(preferences.title));

        $(element).html(JIRA.DashboardItem.Text.Templates.Configuration({
            preferences: preferences
        }));

        this.API.once("afterRender", this.API.resize);

        var $form = $("form", $(element));

        $(".cancel", $form).click(function() {
            gadget.API.closeEdit();
        });

        $form.submit(function(event) {
            event.preventDefault();

            var preferences = getPreferencesFromForm($form);
            gadget.API.savePreferences(preferences);
        });
    };

    function writeContentToIframe(iframe, content) {
        iframe.contentWindow.document.open();
        iframe.contentWindow.document.write(content);
        iframe.contentWindow.document.close();
    }

    function getPreferencesFromForm($form) {
        var preferencesArray = $form.serializeArray();
        var preferencesObject = {};

        preferencesArray.forEach(function(element) {
            preferencesObject[element.name] = element.value;
        });

        return preferencesObject;
    }

    function applyPageStylesToIframe(iframe) {
        // Insert the desired styles in the iframe's head.
        // We have to do this inline because css files get batched up in production, so we
        // can't get just the styles we want from the head.
        // We could also infer the styles from the containing element, but that isn't really reliable.
        // This way guarantees that we get the styles we want.

        // Minified from AUI's reset css.
        var auiResetStyles = 'html,body,p,div,h1,h2,h3,h4,h5,h6,img,pre,form,fieldset{margin:0;padding:0}' +
            'ul,ol,dl{margin:0}img,fieldset{border:0}img:-moz-broken{font-size:inherit}' +
            'details,main,summary{display:block}audio,canvas,progress,video{display:inline-block;vertical-align:baseline}' +
            'audio:not([controls]){display:none;height:0}[hidden],template{display:none}' +
            'input[type="button"],input[type="submit"],input[type="reset"]{-webkit-appearance:button}';

        // Minified from AUI's page typography css.
        var auiTypographicStyles = '[lang|=en],body{font-family:Arial,sans-serif}' +
            'body{color:#333;font-size:14px;line-height:1.4285714285714}' +
            '[lang|=ja]{font-family:"Hiragino Kaku Gothic Pro","?????? Pro W3","????",Meiryo,"?? ?????",Verdana,Arial,sans-serif}' +
            '.aui-group,.aui-panel,.aui-tabs,blockquote,dl,form.aui,h1,h2,h3,h4,h5,h6,ol,p,pre,table.aui,ul{margin:10px 0 0}' +
            '.aui-group:first-child,.aui-panel:first-child,.aui-tabs:first-child,blockquote:first-child,dl:first-child,form.aui:first-child,h1:first-child,h2:first-child,h3:first-child,h4:first-child,h5:first-child,h6:first-child,ol:first-child,p:first-child,pre:first-child,table.aui:first-child,ul:first-child{margin-top:0}' +
            '.aui-page-header-hero .aui-page-header-main h1,.aui-page-header-hero .aui-page-header-main h2,.aui-page-header-marketing .aui-page-header-main h1,.aui-page-header-marketing .aui-page-header-main h2,h1{color:#333;font-size:32px;font-weight:400;line-height:1.25;text-transform:none;margin:30px 0 0}' +
            'h2{color:#333;font-size:24px;font-weight:400;line-height:1.25;text-transform:none;margin:30px 0 0}' +
            '.aui-page-header-hero .aui-page-header-main p,.aui-page-header-marketing .aui-page-header-main p,h3{color:#333;font-size:20px;font-weight:400;line-height:1.5;text-transform:none;margin:30px 0 0}' +
            'h4,h5,h6{font-weight:700;margin:20px 0 0}' +
            'h6,small{font-size:12px}' +
            'blockquote,h6,q,small{color:#707070}' +
            'h4{color:#333;font-size:16px;line-height:1.25;text-transform:none}' +
            'h5{color:#333;font-size:14px;line-height:1.42857143;text-transform:none}' +
            'h6{line-height:1.66666667;text-transform:uppercase}' +
            'h1:first-child,h2:first-child,h3:first-child,h4:first-child,h5:first-child,h6:first-child{margin-top:0}' +
            'h1+h2,h2+h3,h3+h4,h4+h5,h5+h6{margin-top:10px}' +
            '.aui-group>.aui-item>h1:first-child,.aui-group>.aui-item>h2:first-child,.aui-group>.aui-item>h3:first-child,.aui-group>.aui-item>h4:first-child,.aui-group>.aui-item>h5:first-child,.aui-group>.aui-item>h6:first-child{margin-top:20px}' +
            '.aui-group:first-child>.aui-item>h1:first-child,.aui-group:first-child>.aui-item>h2:first-child,.aui-group:first-child>.aui-item>h3:first-child,.aui-group:first-child>.aui-item>h4:first-child,.aui-group:first-child>.aui-item>h5:first-child,.aui-group:first-child>.aui-item>h6:first-child{margin-top:0}' +
            'small{line-height:1.3333333333333}' +
            'code,kbd{font-family:monospace}' +
            'address,cite,dfn,var{font-style:italic}' +
            'blockquote{border-left:1px solid #ccc;margin-left:19px;padding:10px 20px}' +
            'blockquote>cite{display:block;margin-top:10px}' +
            'q:before{content:open-quote}' +
            'q:after{content:close-quote}' +
            'abbr{border-bottom:1px #707070 dotted;cursor:help}';

        $(iframe.contentWindow.document.head).prepend($('<style></style>').append(auiResetStyles + auiTypographicStyles));
    }

    function updateIframeHeightToFitContent(iframe) {
        $(iframe).css('height', iframe.contentWindow.document.body.scrollHeight);
    }

    return DashboardItem;
});
