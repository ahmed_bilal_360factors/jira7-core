AJS.test.require([
    'com.atlassian.jira.gadgets:common-test-resources',
    'com.atlassian.jira.gadgets:text-dashboard-item-resource'
], function () {
    'use strict';

    require([
        'jira-dashboard-items/text',
        'jquery'
    ], function(
        TextDashboardItem,
        $
    ) {
        module('jira-dashboard-items/text', {
            setup: function() {
                this.dashboardItem = new TextDashboardItem(DashboardItem.Mocks.API);
                this.$el = $('#qunit-fixture');
                this.el = this.$el.get(0);
            },

            titleInputField: function() {
                return $('input[name=title]', this.$el);
            },

            htmlInputField: function() {
                return $('textarea[name=html]', this.$el);
            },

            assertInputFieldsAreCorrect: function(titleContent, htmlContent) {
                equal(this.titleInputField().val(), titleContent);
                equal(this.htmlInputField().val(), htmlContent);
            }
        });


        test('Should display the correct configuration form when there are no saved preferences', function() {
            this.dashboardItem.renderEdit(this.el, {});

            this.assertInputFieldsAreCorrect('', '');
        });

        test('Should display load previously saved preferences into the configuration form', function() {
            var html = '<p><strong>This is some html</strong></p>';
            var title = 'this is a title';

            this.dashboardItem.renderEdit(this.el, {
                html: html,
                title: title
            });

            this.assertInputFieldsAreCorrect(title, html);
        });

        test('Should save the correct preferences when the form is submitted', function() {
            this.spy(this.dashboardItem.API, 'savePreferences');
            var html = '<html><body><div>something</div></body></html>';
            var title = 'some title';

            this.dashboardItem.renderEdit(this.el, {});
            this.titleInputField().val(title);
            this.htmlInputField().val(html);

            $('form', this.$el).submit();

            sinon.assert.calledWith(this.dashboardItem.API.savePreferences, {
                html: html,
                title: title
            });
        });

        test('Should display the html content and title that are saved in preferences', function() {
            this.spy(this.dashboardItem.API, 'setTitle');
            // Should also apply the containing element's style to the iframe's body
            var body = '<body><div>something <a href="http://google.com">with a link</a>.</div></body>';
            var title = 'some title with a link';

            this.dashboardItem.render(this.el, {
                html: body,
                title: title
            });

            this.dashboardItem.API.trigger('afterRender');

            var content = $('.text-dashboard-item-container iframe').contents().find('body').get(0).outerHTML;
            equal(content, body);
            sinon.assert.calledWith(this.dashboardItem.API.setTitle, title);
        });

        test('Should display encoded non-ASCII characters properly', function () {
            var title = '&#x15B;&#x107;&#x144;';
            var body = '<p>&#x15B;&#x107;&#x144;</p>';
            this.dashboardItem.renderEdit(this.el, {
                title: title,
                html: body
            });

            this.assertInputFieldsAreCorrect('śćń', '<p>śćń</p>');
        });
    });
});
