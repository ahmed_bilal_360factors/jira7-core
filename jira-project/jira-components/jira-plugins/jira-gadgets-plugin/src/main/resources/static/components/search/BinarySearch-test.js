AJS.test.require('com.atlassian.jira.gadgets:binary-search', function() {

    var _ = require('underscore');
    var BinarySearch = require('jira-dashboard-items/components/search/binary-search');


    module('Components.Search.BinarySearch');

    test('Can find all elements in an array', function() {

        var testArray = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

        _.each(testArray, function(element, index) {
            var foundIndex = BinarySearch.search(testArray, element);
            equal(foundIndex, index, 'Should have found the element at the correct index for value: ' + element);
        });
    });

    test('Not finding an element returns -1', function() {
        var testArray = [0, 1, 2, 3, 4, 6, 7, 8, 9, 10];

        var foundIndex = BinarySearch.search(testArray, 12);
        equal(foundIndex, -1, 'Should not have been able to find something out of the array');

        foundIndex = BinarySearch.search(testArray, 5);
        equal(foundIndex, -1, 'Should not have been able to find something in the middle of the array');


        foundIndex = BinarySearch.search(testArray, -2);
        equal(foundIndex, -1, 'Should not have been able to find something in the middle of the array');
    });


    test('Passing in your own predicate function works', function() {
        var testArray = [
            [0, 4],
            [5, 6],
            [7, 10],
            [11, 24]
        ];

        function predicate(goal, other) {
            if (other[0] <= goal && goal <= other[1]) {
                return 0;
            } else if (goal < other[0]) {
                return -1;
            } else {
                return 1;
            }
        }

        //Tests first, middle and last value for each element
        _.each(testArray, function(element, index) {

            var first = element[0];
            var last = element[1];
            var mid = first + (last - first) / 2;

            equal(BinarySearch.search(testArray, first, predicate), index, 'Should have found the correct index for value ' + first + ' in element ' + element);
            equal(BinarySearch.search(testArray, mid, predicate), index, 'Should have found the correct index for value ' + mid + ' in element ' + element);
            equal(BinarySearch.search(testArray, last, predicate), index, 'Should have found the correct index for value ' + last + ' in element ' + element);
        });


        //tests out of range elements
        equal(BinarySearch.search(testArray, 6.5, predicate), -1, 'Should have not found the element between elements');
        equal(BinarySearch.search(testArray, -2, predicate), -1, 'Should have not found the element before the elements');
        equal(BinarySearch.search(testArray, 25, predicate), -1, 'Should have not found the element after the elements');
    });
});
