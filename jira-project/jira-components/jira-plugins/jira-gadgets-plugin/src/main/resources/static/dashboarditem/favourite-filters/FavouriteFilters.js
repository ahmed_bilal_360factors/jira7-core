define("jira-dashboard-items/favourite-filters", ['jira/util/formatter', 'wrm/context-path', 'jquery', 'underscore'], function(formatter, wrmContextPath, $, _) {
    var DashboardItem = function(API) {
        this.API = API;
    };

    /**
     * Called to render the view for a fully configured dashboard item.
     *
     * @param context The surrounding <div/> context that this items should render into.
     * @param preferences The user preferences saved for this dashboar item
     */
    DashboardItem.prototype.render = function (context, preferences) {
        var $element = $(context);
        var self = this;
        self.API.showLoadingBar();

        $.ajax({
            method: "GET",
            url: wrmContextPath() + '/rest/gadget/1.0/favfilters',
            dataType: "json",
            data: {
                showCounts : preferences.showCounts
            }
        }).done(function(response) {
            if(response.filters.length > 0) {
                $element.html(JIRA.DashboardItem.FavouriteFilters.Templates.favouriteFilters({
                    filters:response.filters,
                    showCounts: preferences.showCounts
                }));
            } else {
                $element.html(JIRA.DashboardItem.FavouriteFilters.Templates.noFavouriteFilters());
            }
        }).fail(function() {
            $element.html(JIRA.DashboardItem.FavouriteFilters.Templates.errorResult());
        }).always(function() {
            self.API.hideLoadingBar();
            self.API.resize();
        });

        self.API.initRefresh(preferences, _.bind(self.render, self, context, preferences));
    };

    /**
     * Called to render the configuration form for this dashboard item if preferences.isConfigured
     * has not been set yet.
     *
     * @param context The surrounding <div/> context that this items should render into.
     * @param preferences The user preferences saved for this dashboard item
     */
    DashboardItem.prototype.renderEdit = function (context, preferences) {
        var self = this;
        var $context = $(context);

        var prefix = self.API.getGadgetId() + "-";
        $context.html(JIRA.DashboardItem.FavouriteFilters.Templates.configuration({
            prefix: prefix,
            preferences: preferences,
            checkboxes:[{
                id: prefix + "show-counts",
                name: "showCounts",
                labelText: formatter.I18n.getText("gadget.favourite.filters.show.counts.label"),
                descriptionText: formatter.I18n.getText("gadget.favourite.filters.show.counts.description"),
                isChecked: !!preferences.showCounts
            }]
        }));

        var form = $("form", $context);

        form.on("submit", function (e) {
            e.preventDefault();
            self.API.resize();

            var preferences = {
                showCounts: form.find("input[name=showCounts]").is(":checked"),
                refresh: self.API.getRefreshFieldValue()
            };

            self.API.savePreferences(preferences);
        });

        var cancelButton = $(".cancel", form);
        cancelButton.click(function () {
            self.API.closeEdit();
        });

        self.API.resize();
        this.API.once("afterRender", function () {
            self.API.resize();
        });
    };

    return DashboardItem;
});
