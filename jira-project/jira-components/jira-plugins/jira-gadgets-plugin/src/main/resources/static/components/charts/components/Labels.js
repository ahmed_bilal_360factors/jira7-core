/**
 * Creates labels at a point on the chart.
 *
 * @module Labels
 */
define('jira-dashboard-items/components/charts/components/labels', [
    'jquery',
    'underscore',
    'jira-dashboard-items/lib/c3'
], function(
    $,
    _,
    c3
) {

    function horizontallyCenter(x, width) {
        return x - (width / 2);
    }

    /**
     * @returns {Labels}
     */
    var Labels = function () {
        return c3.component("labels")
            .extend(c3.drawable())
            .extend(c3.plottable())
            .elementTag('text')
            .elementClass('label')
            .extend({
                textGenerator:  c3.prop(function(d) {
                    return String(d);
                }),
                mouseenter: c3.event(),
                mouseleave: c3.event(),
                centerXValue: function(x, width) {
                    x = horizontallyCenter(x, width);

                    var boundaryRight = this.xRange()[1];

                    var maximumX = Math.max(0, boundaryRight - width);

                    return Math.max(0, Math.min(x, maximumX));
                },
                sortLabelsByXValue: function() {
                    // As the labels may have different x positions due to centering the text, sort them so they
                    // are in order.
                    $(this.elements()[0]).sort(function(a, b) {
                        var aXCoordinate = parseFloat($(a).attr('x'));
                        var bXCoordinate = parseFloat($(b).attr('x'));
                        return aXCoordinate > bXCoordinate;
                    }).appendTo(this.selection());
                }

            }).update(function(event) {
                var self = this;
                event.selection
                    .text(this.textGenerator())
                    .attr('data-index', function(d, i) {
                        return i;
                    })
                    .attr('x', function(d) {
                        var x = self.x()(d);
                        var width = this.getBBox().width;

                        return self.centerXValue(x, width);
                    })
                    .attr('y', this.y());
            }).extend(self.sortLabelsByXValue);
    };

    Labels.INDEX_ATTR = 'data-index';

    return Labels;
});
