/**
 * Represents a dual line graph with areas between that can be individually styled.
 *
 * @note The data must be in the format [x, [y0, y1]]. This is a huge flaw in re-useability. However, getting it to
 * work like this is too difficult with the time for this project. Tried to be fixed but it is too hard.
 *
 * @module LineComparisonGraph
 */
define('jira-dashboard-items/components/charts/components/linecomparison-graph', [
    'jquery',
    'jira-dashboard-items/components/charts/components/area-between-plot-one-way',
    'jira-dashboard-items/lib/c3'
], function(
    $,
    AreaBetweenPlotOneWay,
    c3
) {

    var LineAndAreaBetweenPlotOneWay = {
        first: function() {
            return c3.layerable()
                .addLayer('area', AreaBetweenPlotOneWay.firstAbove())
                .addLayer('line', c3.linePlot()
                    .yAccessor(function(d) {
                        return d[1][0];
                    })
                );
        },
        second: function() {
            return c3.layerable()
                .addLayer('area', AreaBetweenPlotOneWay.secondAbove())
                .addLayer('line', c3.linePlot()
                    .yAccessor(function(d) {
                        return d[1][1];
                    })
            );
        }
    };

    /**
     * @typedef {Object} LineComparisonGraphOptions
     * @property {String} firstName is the class name of the first line
     * @property {String} secondName is the class name of the second line
     **/


    /**
     * @constructor
     * @param {LineComparisonGraphOptions} options
     */
    return function(options) {
        return c3.layerable()
            .addLayer(options.firstName, LineAndAreaBetweenPlotOneWay.first())
            .addLayer(options.secondName, LineAndAreaBetweenPlotOneWay.second());
    };
});
