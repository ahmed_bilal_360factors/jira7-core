/**
 * Generates labels and any that overlap horizontally are grouped into a single label, with the id's of the labels
 * grouped for future use.
 *
 * @module GroupedLabels
 */
define('jira-dashboard-items/components/charts/components/grouped-labels', [
    'underscore',
    'jira-dashboard-items/components/charts/components/labels',
    'jira-dashboard-items/lib/d3'
], function(
    _,
    Labels,
    d3
) {

    /**
     * @typedef {Object} GroupedLabelOptions
     * @property {Function} [groupTextGenerator] given the number of grouped elements generate a label.
     */

    /**
     * @param {GroupedLabelOptions} [options] for the labels
     * @returns {c3.component}
     * @constructor
     */
    var GroupedLabels = function (options) {

        options = options || {};
        options.groupTextGenerator = options.groupTextGenerator || _.identity;

        return Labels()
            .extend({
                getGroupedIndexes: function(label) {
                    return label.attr(Labels.INDEX_ATTR).split(',');
                },

                getMiddleXCoordinate: function(label) {
                    var indexes = this.getGroupedIndexes(label);

                    var first = 0;
                    var last = indexes.length - 1;

                    var xValue = this.x();
                    var data = this.data();

                    var min = xValue(data[indexes[first]]);
                    var max = xValue(data[indexes[last]]);

                    return (max + min) / 2;
                }
            })

            //Hide overlapping ticks, keeps merging labels while there is still merges occuring.  Cannot just do
            //a single iteration over the labels as we are centering the group label when we merge to keep it in
            //the center of the group which could result in it moving left or right. Therefore, it could move left
            //into another group and so we need to go over and make sure that those groups get merged again.
            .extend(function() {
                var self = this;
                var labelMerged = true;
                while (labelMerged) {
                    labelMerged = false;

                    var labels = this.selection().selectAll(".label");

                    var currentLabelIndex = 0;
                    while (currentLabelIndex < labels.size()) {
                        var label = d3.select(labels[0][currentLabelIndex]);

                        //For each label after it in the DOM.
                        var otherLabelIndex = currentLabelIndex + 1;
                        while (otherLabelIndex < labels.size()) {

                            var otherLabel = d3.select(labels[0][otherLabelIndex]);

                            var overlap = parseFloat(label.attr('x')) + label.node().getBBox().width > parseFloat(otherLabel.attr('x'));

                            if (overlap) {
                                var unionIndexes = _.union(self.getGroupedIndexes(label), self.getGroupedIndexes(otherLabel));

                                labelMerged = true;

                                label.attr(Labels.INDEX_ATTR, unionIndexes.join(','));

                                var middleXCoordinate = self.getMiddleXCoordinate(label);

                                label.text(options.groupTextGenerator(unionIndexes.length));
                                var width = label.node().getBBox().width;

                                label.attr('x', self.centerXValue(middleXCoordinate, width));
                                otherLabel.remove();
                            } else {
                                break;
                            }
                            ++otherLabelIndex;
                        }
                        currentLabelIndex = otherLabelIndex;
                    }
                }
            });
    };

    GroupedLabels.GROUPING_DATA_ATTR = Labels.INDEX_ATTR;

    return GroupedLabels;
});
