define('jira-dashboard-items/bubble-chart', [
    'jira/util/formatter',
    'jquery',
    'underscore',
    'backbone',
    'jira-dashboard-items/components/charts/bubble-chart/bubble-chart-component',
    'jira-dashboard-items/bubble-chart-model',
    'jira-dashboard-items/components/charts/components/discrete-legend/discrete-legend'
], function(
    formatter,
    $,
    _,
    Backbone,
    BubbleChartComponent,
    BubbleChartModel,
    DiscreteLegend) {
    'use strict';

    var BubbleChart = Backbone.View.extend({
        template: JIRA.DashboardItem.BubbleChart.Templates,

        initialize: function(options) {
            this.RESIZE_RATE_LIMIT = 300;
            this.setTitle = options.setTitle;
            this.hideLoadingBar = options.hideLoadingBar;
            this.resize = options.resize;

            this.discreteLegendModel = new Backbone.Model({
                data: options.model.get('legendData'),
                colors: options.model.get('colors')
            });

            this.listenTo(options.model, {
                'change:legendData': function(model, data) { this.discreteLegendModel.set('data', data);},
                'change:colors': function(model, colors) { this.discreteLegendModel.set('colors', colors);}
            });
        },

        render: function() {
            this.listenTo(this.model, {
                'change:data': this._renderBubbleChart,
                'change:filterTitle': function (model, filterTitle) {
                    this.setGadgetTitleForFilter(filterTitle);
                },
                'change:highlightedCircle': function(model, options) {
                    this.discreteLegendModel.set('highlightedSegment', options.colorBucket);
                },
                'change:selectedCircle': function (model, options) {
                    this.discreteLegendModel.set('selectedSegment', options ? options.colorBucket : undefined);
                }
            });

            this.resizeHandler = _.debounce(this._renderBubbleChart.bind(this), this.RESIZE_RATE_LIMIT);
            // We'd prefer to resize when this element resizes rather than the window, however this only has partial browser support.
            // From what I can tell, it works in Chrome and Safari, but not Firefox. The resize event is also
            // only meant to exist for the window, so Firefox is correct. Other dashboard items don't resize in Firefox
            // if they rely on this. This means we don't resize when the dashboard changes width,
            // or when a dashboard item is dragged and dropped to a different column.
            $(window).resize(this.resizeHandler);

            this.$el.on('remove', function() {
                $(window).off('resize', this.resizeHandler);
            }.bind(this));
        },

        _renderBubbleChart: function() {
            this._removeExistingBubbleChartIfItExists();
            this.hideLoadingBar();

            var data = this.model.get('data');

            if (!data) {
                return;
            }

            if (data && data.length === 0) {
                this._displayNoDataInfoMessage();
                return;
            }

            var bubbleType = this.model.get('bubbleType');
            var displayWarning = this.model.get('exceedsIssueLimit');

            // Render the container for the chart with an optional warning if the issue limit has been exceeded
            this.$el.html(this.template.Container({
                type: this.model.get('bubbleType'),
                displayWarning: displayWarning
            }));

            // We use the position of the grid lines to set the legend's width, so we need to render the chart first.
            var $bubbleChartContainer = this.$('.bubble-chart-component-container');
            this.bubbleChart = this._createBubbleChart($bubbleChartContainer, this.model.get('data'), this.model.get('colors'));
            this.bubbleChart.render();

            var $legendElement = this.$('.discrete-legend-component-container');
            var graphWidth = $('.graph-border', $bubbleChartContainer).attr('width');
            var graphHeight = $('.graph-border', $bubbleChartContainer).attr('height');

            $legendElement.attr('width', graphWidth);
            $legendElement.attr('height', graphHeight);

            this.legend = this._createLegend($legendElement.get(0));
            this.legend.render();
            this.resize();
            this.setGadgetTitleForFilter(this.model.get('filterTitle'));
        },

        _removeExistingBubbleChartIfItExists: function() {
            if (this.bubbleChart) {
                this.bubbleChart.stopListening();
                this.bubbleChart.$el.remove();
                this.bubbleChart = null;
            }
        },

        _displayNoDataInfoMessage: function() {
            this.setTitle(formatter.I18n.getText('bubble-chart-title.config'));
            this.$el.html(this.template.NoDataInfo());
            this.resize();
        },

        _createBubbleChart: function(element) {
            return new BubbleChartComponent({
                model: this.model,
                el: element,
                id: this.id
            });
        },

        _createLegend: function(element) {
            return new DiscreteLegend({
                el: element,
                model: this.discreteLegendModel
            });
        },

        /**
         * Set the title of the gadget
         * @param filterTitle The title of the filter that is currently on display
         */
        setGadgetTitleForFilter: function(filterTitle) {
            this.setTitle(formatter.I18n.getText('bubble-chart.title.filterorproject', filterTitle));
        }
    });

    return BubbleChart;
});
