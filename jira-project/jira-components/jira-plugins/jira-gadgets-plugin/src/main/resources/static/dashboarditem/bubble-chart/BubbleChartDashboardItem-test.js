AJS.test.require([
    'com.atlassian.jira.gadgets:common-test-resources',
    'com.atlassian.jira.gadgets:bubble-chart-dashboard-item-resources'
], function () {
    require([
        'jira-dashboard-items/bubble-chart-dashboard-item',
        'jquery'
    ], function (
        BubbleChartDashboardItem,
        $
    ) {
        var NETWORK_RESPONSE = {
            data: [
                { key: 'txt1', bubbleDomain: 1.3, bubbleRange: 1.4, bubbleRadius: 2.3, bubbleColorValue: 4.5 },
                { key: 'txt2', bubbleDomain: 3.1, bubbleRange: 6.9, bubbleRadius: 4.3, bubbleColorValue: 2.7 },
                { key: 'txt3', bubbleDomain: 3.1, bubbleRange: 0, bubbleRadius: 4.3, bubbleColorValue: 3.1 },
                { key: 'txt4', bubbleDomain: 0, bubbleRange: 6.9, bubbleRadius: 1.3, bubbleColorValue: 7.4 },
                { key: 'txt5', bubbleDomain: 123.1, bubbleRange: 111, bubbleRadius: 4.3, bubbleColorValue: 0.7 },
                { key: 'txt6', bubbleDomain: 0, bubbleRange: 6.9, bubbleRadius: 2.9, bubbleColorValue: 5.2 }
            ],
            filterTitle: 'some_filter',
            issueCount: 6
        };

        module('jira-dashboard-items/bubble-chart-dashboard-item', {
            setup: function() {
                this.$el = $('<div class="bubble-chart-gadget" />');

                var API = $.extend({}, DashboardItem.Mocks.API);

                var options = {
                    delay: function(cb) { cb(); }
                };

                this.dashboardItem = new BubbleChartDashboardItem(API, options);

                $('#qunit-fixture').append(this.$el);
                this.server = sinon.fakeServer.create();
            },

            teardown: function() {
                this.server.restore();
            },

            makeRequestReturningErrorWithResponseCode: function(responseCode, errors) {
                this.dashboardItem.render(this.$el, {});
                this.server.requests[0].respond(responseCode, {'Content-Type': 'application/json'}, JSON.stringify({ errors: errors }));
            },

            assertErrorDisplayedForResponseCode: function(responseCode, expectedErrorTitle, errors) {
                this.makeRequestReturningErrorWithResponseCode(responseCode, errors);
                equal($('.aui-message-error .title', this.$el).text(), expectedErrorTitle);
            },

            assertLoadingBarShowsAndHidesWhenRenderingChart: function(statusCode, response) {
                var showLoadingSpy = this.spy(this.dashboardItem.API, 'showLoadingBar');
                var hideLoadingSpy = this.spy(this.dashboardItem.API, 'hideLoadingBar');

                this.dashboardItem.render(this.$el, {});

                sinon.assert.calledOnce(showLoadingSpy);
                this.server.requests[0].respond(statusCode, {'Content-Type': 'application/json'}, JSON.stringify(response));
                sinon.assert.called(hideLoadingSpy);
            }
        });

        test('Should make a network response and set its result on the model', function() {
            this.dashboardItem.render(this.$el, {});

            this.server.requests[0].respond(200, {'Content-Type': 'application/json'}, JSON.stringify(NETWORK_RESPONSE));

            var data = this.dashboardItem.model.get('data');
            deepEqual(data, NETWORK_RESPONSE.data);
        });

        test('Should set the auto refresh property correctly', function() {
            this.spy(this.dashboardItem.API, 'initRefresh');
            var preferences = {
                useLogarithmicScale: true,
                bubbleType: 'votes',
                recentCommentsPeriod: 28,
                useRelativeColoring: false,
                refresh: true
            };

            this.dashboardItem.render(this.$el, preferences);
            this.server.requests[0].respond(200, {'Content-Type': 'application/json'}, JSON.stringify(NETWORK_RESPONSE));

            sinon.assert.calledOnce(this.dashboardItem.API.initRefresh);
            sinon.assert.calledWith(this.dashboardItem.API.initRefresh, preferences);
        });

        test("Should read a user's preferences and set them on the model", function() {
            var preferences = {
                useLogarithmicScale: true,
                bubbleType: 'votes',
                recentCommentsPeriod: 28,
                useRelativeColoring: false
            };

            this.dashboardItem.render(this.$el, preferences);

            equal(this.dashboardItem.model.get('useLogarithmicScale'), preferences.useLogarithmicScale);
            equal(this.dashboardItem.model.get('bubbleType'), preferences.bubbleType);
            equal(this.dashboardItem.model.get('recentCommentsPeriod'), preferences.recentCommentsPeriod);
            equal(this.dashboardItem.model.get('useRelativeColoring'), preferences.useRelativeColoring);
        });

        test('Should create a config view when renderEdit is called', function() {
            this.dashboardItem.renderEdit(this.$el, {});

            deepEqual(this.dashboardItem.configView.intervals, this.dashboardItem.intervalOptions);
            deepEqual(this.dashboardItem.configView.types, this.dashboardItem.typeOptions);
        });

        test('Should set the gadget title when renderEdit is called', function() {
            var setTitleSpy = this.spy(this.dashboardItem.API, 'setTitle');

            this.dashboardItem.renderEdit(this.$el);

            sinon.assert.calledOnce(setTitleSpy);
            ok(setTitleSpy.calledWith('bubble-chart-title.config'));
        });

        test('Should close the edit view when the config view cancel event fires', function() {
            this.dashboardItem.renderEdit(this.$el, {});
            var closeEditSpy = this.spy(this.dashboardItem.API, 'closeEdit');

            this.dashboardItem.configView.trigger('cancel');

            sinon.assert.calledOnce(closeEditSpy);
        });

        test('Should resize when an error message is returned', function() {
            var resizeSpy = this.spy(this.dashboardItem.API, 'resize');

            this.makeRequestReturningErrorWithResponseCode(404);

            sinon.assert.calledOnce(resizeSpy);
        });

        test('Should display an error message when rendering and the network request fails for an unknown reason', function() {
            this.assertErrorDisplayedForResponseCode(400, 'system.error.main.title');
        });

        test('Should display an appropriate error message when the user does not have permission to view the project or filter', function() {
            this.assertErrorDisplayedForResponseCode(400, 'Invalid project or filter', {
                projectOrFilterId: 'Invalid project or filter'
            });
        });

        test('Should display and then dismiss the loading bar when rendering and the network request is successful', function() {
            this.assertLoadingBarShowsAndHidesWhenRenderingChart(200, NETWORK_RESPONSE);
        });

        test('Should display and then dismiss the loading bar when rendering and the network request is not successful', function() {
            this.assertLoadingBarShowsAndHidesWhenRenderingChart(404, {});
        });

        test('Should save the preferences when the config view form is submitted', function() {
            var savePreferencesSpy = this.spy(this.dashboardItem.API, 'savePreferences');
            var preferences = {
                one: 'onePref',
                two: 'twoPref'
            };

            this.dashboardItem.configViewFormDidSubmit(preferences);

            sinon.assert.calledOnce(savePreferencesSpy);
            deepEqual(preferences, savePreferencesSpy.args[0][0]);
        });
    });
});
