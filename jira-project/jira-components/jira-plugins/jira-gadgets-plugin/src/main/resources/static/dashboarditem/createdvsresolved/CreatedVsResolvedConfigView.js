define('jira-dashboard-items/createdvsresolved-config-view', [
    'jira/util/formatter',
    'jquery',
    'backbone',
    'jira-dashboard-items/components/autocomplete/project-filter-autocomplete'
], function(formatter, $, Backbone, ProjectFilterAutoComplete) {

    /**
     * Represents there own preference type so simplify this class as it does not need to know about the old or new
     * gadget preference format.  Therefore, not needing to deal with backwards compatibility.
     * @typedef Object CreatedVsResolvedConfigViewPreferences
     * @global
     *
     * @property {String} [name] of a selected filter/project
     * @property {String} [type] of the selected filter/project
     * @property {String} [id] of the selected filter/project
     * @property {String} [periodName] for the period grouping
     * @property {Number} [range] for looking back by the period
     * @property {String} [operation] for grouping the issue number
     * @property {Boolean} [showUnresolvedTrend] should include the unresolved trend
     * @property {String} [versionLabel] whether to populate the version
     * @property {Boolean} [refresh] whether to check the refresh value
     **/


    /**
     * Represents the view for the configuration screen of the CreatedVsResolved Dashboard Item.
     * @type {Backbone.View} CreatedVsResolvedConfigView
     */
    var CreatedVsResolvedConfigView = Backbone.View.extend({

        templates: JIRA.DashboardItem.CreatedVsResolved.Templates,

        events: {
            "click .cancel": "cancel",
            "submit": "submit"
        },

        initialize: function(options) {
            this.id = options.id;
            this.projectFilterDelay = options.projectFilterDelay;
        },

        /**
         * Render the screen with the current fields populated with the preferences
         * @param {CreatedVsResolvedConfigViewPreferences} preferences
         */
        render: function(preferences) {
            preferences = preferences || {};
            var self = this;

            this.$el.html(this.templates.Configuration({
                prefix: this.id + "-",
                preferences: preferences,
                periodOptions: PERIOD_OPTIONS,
                chartValueOptions: OPERATION_OPTIONS,
                unresolvedOptions: UNRESOLVED_OPTIONS,
                versionsOptions: VERSIONS_OPTIONS
            }));

            this._bindProjectFilterAutocomplete();
        },

        /**
         * Apply the project filter autocomplete to the field in the form.
         * @private
         */
        _bindProjectFilterAutocomplete: function() {
            var self = this;

            var projectFilterOptions = {
                field: $("#" + this.id + "-project-filter-picker", this.$el),

                completeField: function(selection) {
                    if (selection) {
                        self.$el.find("input[name=type]").val(selection.type);
                        self.$el.find("input[name=id]").val(selection.id);
                        self.$el.find("input[name=name]").val(selection.name);
                        self.$el.find(".filterpicker-value-name").text(selection.name).addClass('success');
                        this.field.val('');
                        this.field.trigger("change");
                    }
                },

                maxHeight: 140
            };

            //Mostly used for testing where we don't want a delay;
            if (this.projectFilterDelay) {
                projectFilterOptions.delay = this.projectFilterDelay;
            }

            //To account for advanced search mechanism
            projectFilterOptions.parentElement = this.$el.find("form");
            projectFilterOptions.fieldID = projectFilterOptions.field.attr("id");

            //Instantiate project filter autocomplete
            ProjectFilterAutoComplete(projectFilterOptions);
        },


        submit: function(event) {
            event.preventDefault();

            if (this._validate()) {
                this.trigger("submit", this.getFields());
            }
        },

        cancel: function() {
            this.trigger("cancel");
        },

        /**
         * @returns {CreatedVsResolvedConfigViewPreferences}
         */
        getFields: function() {
            return {
                name: this.$el.find("input[name=name]").val(),
                type: this.$el.find("input[name=type]").val(),
                id: this.$el.find("input[name=id]").val(),
                periodName: this.$el.find("select[name=periodName]").val(),
                range: this.$el.find("input[name=range]").val(),
                operation: this.$el.find("select[name=operation]").val(),
                showUnresolvedTrend: this.$el.find("select[name=showUnresolvedTrend]").val() === 'true',
                versionLabel: this.$el.find("select[name=versionLabel]").val(),
                refresh: this.$el.find("input[name=refresh-interval]").prop('checked')
            };
        },

        /**
         * Validate the configuration and any fields that are not correct apply an error message to the field.
         * @returns {boolean} if it is valid or not
         * @private
         */
        _validate: function() {
            var preferences = this.getFields();
            var valid = true;
            if (!preferences.id || !preferences.type) {
                $(".projectOrFilter-error", this.$el).text(formatter.I18n.getText('gadget.common.required.query')).show();
                valid = false;
            } else {
                $(".projectOrFilter-error", this.$el).hide();
            }

            if (!RANGE_INPUT_LIMIT[preferences.periodName]) {
                throw "Period invalid";
            }

            var range = preferences.range;

            var rangeFieldGroup = $("input[name=range]", this.$el).closest('.field-group');
            if (/^\s*$/.test(preferences.range)) {
                this._addError(rangeFieldGroup, formatter.I18n.getText('portlet.chart.field.date.range.required'));
                valid = false;
            } else if (isNaN(preferences.range)) {
                this._addError(rangeFieldGroup, formatter.I18n.getText('portlet.chart.field.range.nan'));
                valid = false;
            } else if (preferences.range <= 0) {
                this._addError(rangeFieldGroup, formatter.I18n.getText('portlet.chart.field.range.negative'));
                valid = false;
            } else if (!/^\d+$/.test(preferences.range)) {
                this._addError(rangeFieldGroup,formatter.I18n.getText('portlet.chart.field.range.nan'));
                valid = false;
            } else if (range > RANGE_INPUT_LIMIT[preferences.periodName]) {
                this._addError(rangeFieldGroup, formatter.I18n.getText(
                    'portlet.chart.field.range.maximum',
                    RANGE_INPUT_LIMIT[preferences.periodName],
                    PERIOD_LABELS[preferences.periodName]
                ));
                valid = false;
            } else {
                this._removeError(rangeFieldGroup);
            }

            this.trigger("layoutUpdate");

            return valid;
        },

        /**
         * Remove any errors in the field group
         * @param {Node} fieldGroup
         * @private
         */
        _removeError: function(fieldGroup) {
            $(fieldGroup).find('.error').remove();
        },

        /**
         * Add an error to the field group
         * @param {Node} fieldGroup
         * @param {String} errorMessage
         * @private
         */
        _addError: function(fieldGroup, errorMessage) {
            this._removeError(fieldGroup);
            fieldGroup = $(fieldGroup);

            var error = $("<div class='error'/>");
            error.html(errorMessage);
            fieldGroup.append(error);
            error.show();
        },

        /**
         * Trying to save the preferences resulted in a 401.
         */
        unauthorisedFormSave: function() {
            this.removeErrors();
            this.$el.append(this.templates.UnauthorisedFormSave());
            this.trigger("layoutUpdate");
        },

        /**
         * There was an error saving the form.
         */
        errorSavingForm: function(errors) {
            errors = errors || [];
            this.removeErrors();
            this.$el.append(this.templates.ErrorSaving({
                errors: errors
            }));
            this.trigger("layoutUpdate");
        },

        /**
         * Remove any previous messages in the form.
         */
        removeErrors: function() {
            this.$el.find('.aui-message').remove();
            this.trigger("layoutUpdate");
        }
    });

    /**
     * Constants used for population the view options and limits for the fields, etc.
     */

    var RANGE_INPUT_LIMIT = {
        hourly: 10,
        daily: 300,
        weekly: 1750,
        monthly: 7500,
        quarterly: 22500,
        yearly: 36500
    };

    var PERIOD_LABELS = {
        'hourly': formatter.I18n.getText('gadget.common.period.hourly'),
        'daily' : formatter.I18n.getText('gadget.common.period.daily'),
        'weekly' : formatter.I18n.getText('gadget.common.period.weekly'),
        'monthly' : formatter.I18n.getText('gadget.common.period.monthly'),
        'quarterly' : formatter.I18n.getText('gadget.common.period.quarterly'),
        'yearly' : formatter.I18n.getText('gadget.common.period.yearly')
    };

    var PERIOD_OPTIONS = {
        options: [
            {
                value: 'hourly',
                text: PERIOD_LABELS.hourly
            },
            {
                value: 'daily',
                text: PERIOD_LABELS.daily
            },
            {
                value: 'weekly',
                text: PERIOD_LABELS.weekly

            },
            {
                value: 'monthly',
                text: PERIOD_LABELS.monthly

            },
            {
                value: 'quarterly',
                text: PERIOD_LABELS.quarterly
            },
            {
                value: 'yearly',
                text: PERIOD_LABELS.yearly
            }
        ]
    };

    var OPERATION_OPTIONS = {
        options: [
            {
                value: 'count',
                text: formatter.I18n.getText('gadget.common.operation.count')
            },
            {
                value: 'cumulative',
                text: formatter.I18n.getText('gadget.common.operation.cumulative')
            }
        ]
    };

    var UNRESOLVED_OPTIONS = {
        options: [
            {
                value: 'false',
                text: formatter.I18n.getText('gadget.common.no')
            },
            {
                value: 'true',
                text: formatter.I18n.getText('gadget.common.yes')
            }
        ]
    };

    var VERSIONS_OPTIONS = {
        options: [
            {
                value: 'all',
                text: formatter.I18n.getText('gadget.created.vs.resolved.version.all')
            },
            {
                value: 'major',
                text: formatter.I18n.getText('gadget.created.vs.resolved.version.major')
            },
            {
                value: 'none',
                text: formatter.I18n.getText('gadget.created.vs.resolved.version.none')
            }
        ]
    };

    return CreatedVsResolvedConfigView;
});
