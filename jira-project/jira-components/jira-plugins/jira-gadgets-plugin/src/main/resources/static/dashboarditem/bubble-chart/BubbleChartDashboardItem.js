define('jira-dashboard-items/bubble-chart-dashboard-item', [
    'jira/util/formatter',
    'jira-dashboard-items/bubble-chart',
    'jira-dashboard-items/bubble-chart-config-view',
    'jira-dashboard-items/bubble-chart-model',
    'jquery',
    'underscore'
], function(formatter, BubbleChart, BubbleChartConfigView, BubbleChartModel, $, _) {
    'use strict';

    var DashboardItem = function (API, options) {
        this.API = API;
        this.options = options || {};

        this.errors = {
            GENERIC: formatter.I18n.getText('system.error.main.title'),
            UNAUTHORIZED_TITLE: formatter.I18n.getText('bubble-chart.unauthorized.title'),
            UNAUTHORIZED_CONTENT: '',
            NOT_FOUND_TITLE: formatter.I18n.getText('bubble-chart.not-found.title'),
            NOT_FOUND_CONTENT: formatter.I18n.getText('bubble-chart.not-found.content')
        };

        // The user's options will be set in `render`
        this.model = new BubbleChartModel({});
    };

    /**
     * Render the dashboard item
     * @param element The element that will contain the dashboard item
     * @param preferences The dashboard item's preferences
     */
    DashboardItem.prototype.render = function (element, preferences) {
        AJS.trigger('analyticsEvent', {
            name: 'jira.dashboard.gadgets.bubble-chart.loaded',
            data: {
                bubbleType: preferences.bubbleType,
                useLogarithmicScale: preferences.useLogarithmicScale,
                useRelativeColoring: preferences.useRelativeColoring
            }
        });

        this.API.initRefresh(preferences, _.bind(this.render, this, element, preferences));
        this.API.showLoadingBar();

        if (!this.bubbleChart) {
            this.bubbleChart = new BubbleChart({
                model: this.model,
                id: this.API.getGadgetId(),
                setTitle: function(t) { this.API.setTitle(t); }.bind(this),
                hideLoadingBar: function() { this.API.hideLoadingBar(); }.bind(this),
                resize: function() { this.API.resize(); }.bind(this),
                el: element
            });
        }

        this.bubbleChart.render();

        this.model.set({
            data: undefined,
            projectOrFilterId: preferences.type + '-' + preferences.id,
            bubbleType: preferences.bubbleType,
            recentCommentsPeriod: preferences.recentCommentsPeriod,
            useLogarithmicScale: preferences.useLogarithmicScale,
            useRelativeColoring: preferences.useRelativeColoring
        });

        this.model.fetch()
            .always(function () {
                // The `.always` callback will be executed after the this.model.on('change:data') callback that is
                // used in BubbleChart.js. This is bad the loading bar controls the height and size of our container,
                // but we need to be able to do that for the bubble chart. We work around this by passing the
                // `hideLoadingBar` function to the bubble chart in its constructor, so that it can control when
                // the loading bar will hide.
                // We still want this line here for the error case.
                this.API.hideLoadingBar();
            }.bind(this))
            .fail(function (response) {
                this._displayErrorMessage(element, response);
            }.bind(this));
    };

    /**
     * Display a generic error message for the dashboard item
     * @param $element The element in which we will display the error
     * @param response The response from the network request
     */
    DashboardItem.prototype._displayErrorMessage = function($element, response) {
        var options = {};

        try {
            var errors = JSON.parse(response.responseText).errors;
            options.title = errors.projectOrFilterId;
            options.content = '';
        } catch(e) {
            options.title = this.errors.GENERIC;
            options.content = '';
        }

        $element.html(JIRA.DashboardItem.BubbleChart.Templates.Error(options));
        this.API.resize();
    };

    /**
     * Render the edit preferences screen
     * @param element The element that will contain the dashboard item
     * @param preferences The dashboard item's preferences
     */
    DashboardItem.prototype.renderEdit = function(element, preferences) {
        if (this.bubbleChart && this.bubbleChart.resizeHandler) {
            $(window).off('resize', this.bubbleChart.resizeHandler);
        }

        $(element).html(JIRA.DashboardItem.BubbleChart.Templates.ConfigurationContainer());

        this.configView = new BubbleChartConfigView({
            id: this.API.getGadgetId(),
            el: $('.bubble-chart-config-container', element),
            projectFilterDelay: this.options.delay
        });

        this.configView.render(preferences);
        this.configView.bind('cancel', _.bind(function () { this.API.closeEdit(); }, this));
        this.configView.bind('submit', _.bind(function (newPreferences) { this.configViewFormDidSubmit(newPreferences); }, this));

        this.configView.bind('layoutUpdate', function() {
            this.API.resize();
        }.bind(this));

        this.API.once('afterRender', function () {
            this.API.resize();
        }.bind(this));

        this.API.setTitle(formatter.I18n.getText('bubble-chart-title.config'));
    };

    /**
     * Called when the configuration form has been submitted
     * @param gadgetPreferences The preferences of the gadget
     */
    DashboardItem.prototype.configViewFormDidSubmit = function(gadgetPreferences) {
        this.API.resize();
        this.API.savePreferences(gadgetPreferences);
    };

    return DashboardItem;
});
