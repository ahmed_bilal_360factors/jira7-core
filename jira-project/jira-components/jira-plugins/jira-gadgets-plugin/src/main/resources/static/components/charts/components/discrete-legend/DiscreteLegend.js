/**
 * A horizontal segmented bar legend with discrete sub-segments of different colors.
 */
define('jira-dashboard-items/components/charts/components/discrete-legend/discrete-legend', [
    'underscore',
    'jquery',
    'jira-dashboard-items/components/charts/components/colorable',
    'jira-dashboard-items/lib/d3',
    'jira-dashboard-items/lib/c3',
    'backbone'
], function(_, $, Colorable, d3, c3, Backbone) {
    'use strict';

    var discreteLegendComponent = function() {
        return c3.component('discrete-legend')
            .extend(c3.layerable())
            .extend(c3.drawable())
            .extend(Colorable())
            .extend(function() {
                var OFFSET = 20 / this.data().length;
                var EACH_WIDTH = this.width() / this.data().length - OFFSET;
                var EACH_HEIGHT = this.height();

                var svg = this.selection()
                    .append('svg')
                        .attr('class', 'discrete-legend-svg');

                // Render three rects for each segment: the original rect, the backing segment rect,
                // and the selected-style rect. The backing and selected segments are initially hidden,
                // and we show them by adding the `selected` class to the rect segment group.

                // Segment group
                var rectGroup = svg
                    .append('g')
                    .attr('transform', 'translate(2, 2)')
                    .selectAll('rect')
                    .data(this.data())
                    .enter().append('g')
                    .attr('data-segment-id', function(d, i) { return i; })
                    .classed('segment-group', true);

                // Original
                rectGroup.append('rect')
                    .classed('original-segment', true)
                    .attr('width', EACH_WIDTH)
                    .attr('height', EACH_HEIGHT)
                    .attr('rx', 3)
                    .attr('x', function(d, i) { return i * EACH_WIDTH + i * OFFSET; })
                    .attr('y', 0)
                    .attr('fill', this.color());

                // Backing
                rectGroup.append('rect')
                    .classed('backing-segment', true)
                    .attr('width', EACH_WIDTH + 2)
                    .attr('height', EACH_HEIGHT + 2)
                    .attr('x', function(d, i) { return i * EACH_WIDTH + i * OFFSET - 1.25; })
                    .attr('y', -1.25)
                    .attr('fill', this.color())
                    .attr('rx', 3);

                // Selected (inner, slightly smaller than original)
                rectGroup.append('rect')
                    .classed('selected-segment', true)
                    .attr('width', EACH_WIDTH - 2.5)
                    .attr('height', EACH_HEIGHT - 2.5)
                    .attr('x', function(d, i) { return i * EACH_WIDTH + i * OFFSET + 1; })
                    .attr('y', 1)
                    .attr('rx', 1)
                    .attr('fill', this.color());

                // Text labels
                svg
                    .append('g')
                    .selectAll('text')
                        .data(this.data())
                    .enter().append('text')
                        .text(function(d, i) { return d;})
                        .attr('transform', function(d, i) {
                            return 'translate(' + (i * EACH_WIDTH + EACH_WIDTH / 2 + i * OFFSET) + ', 30)';
                        });
            })
            .extend({
                colorAccessor: c3.inherit('colorAccessor', function(d) {
                    return d;
                }),
                colorScaleConstructor: c3.inherit('colorScaleConstructor', d3.scale.quantize)
            });
    };

    /**
     * A segmented legend that is used on the Bubble Chart dashboard item.
     *
     * Each segment has two states: selected and highlighted. These are managed by the `model` object,
     * which should be passed in the constructor.
     *
     * To 'select' a segment, we apply the 'selected' class to that `g.segment-group`, which will add
     * a backing rect, display an inner rect lozenge, and hide the original rect. To 'deselect' a segment,
     * simply remove this class.
     *
     * Highlighting a segment is similar--add the 'highlighted class to the `g.segment-group` corresponding
     * to the segment you want to highlight.
     *
     * @type {DiscreteLegend}
     */
    var DiscreteLegend = Backbone.View.extend({
        initialize: function(options) {
            this.options = options || {};
            this.model = options.model;

            this.listenTo(this.model, 'change:selectedSegment', this._selectedSegmentDidChange);
            this.listenTo(this.model, 'change:highlightedSegment', this._highlightedSegmentDidChange);
        },

        _selectedSegmentDidChange: function(model, index) {
            if (index !== undefined) {
                this._deselectAllSegments();
                this._selectSegment(index);
            } else {
                this._deselectAllSegments();
            }
        },

        _highlightedSegmentDidChange: function(model, index) {
            if (index !== undefined) {
                this._highlightSegment(index);
            } else {
                this._unhighlightAllSegments();
            }
        },

        render: function() {
            var domain = this.model.get('data').map(function(_, i) { return i; });
            var width = this.$el.attr('width');

            var legend = discreteLegendComponent()
                .data(this.model.get('data'))
                .colorScaleConstructor(d3.scale.ordinal)
                .colorDomain(domain)
                .colorRange(this.model.get('colors'))
                .colorAccessor(function(d, i) {
                    return i;
                 })
                .height(10)
                .width(width);

            d3.select(this.el)
                .append('div')
                .attr('class', 'discrete-legend-component')
                .call(legend);
        },

        _selectSegment: function(index) {
            d3.select(this._segmentWithIndex(index)).classed('selected', true);
        },

        _deselectAllSegments: function() {
            d3.select(this.el).selectAll('.segment-group').classed('selected', false);
        },

        _highlightSegment: function(index) {
            d3.select(this._segmentWithIndex(index)).classed('highlighted', true);
        },

        _unhighlightAllSegments: function() {
            d3.select(this.el).selectAll('.segment-group').classed('highlighted', false);
        },

        _segmentWithIndex: function(index) {
            return $('g.segment-group[data-segment-id="' + index + '"]', this.$el).get(0);
        }
    });

    return DiscreteLegend;
});
