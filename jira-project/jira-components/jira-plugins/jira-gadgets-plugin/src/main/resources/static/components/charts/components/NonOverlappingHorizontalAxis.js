/**
 * @name NonOverlappingHorizontalAxis
 *
 * Represents a X axis which removes axis labels that overlap with each other.
 * NOTE: The first and last labels must be shown.
 */
define('jira-dashboard-items/components/charts/components/non-overlapping-horizontal-axis', [
    'jquery',
    'jira-dashboard-items/components/overlap-remover/overlap-remover',
    'jira-dashboard-items/lib/c3',
    'underscore'
], function(
    $,
    overlapRemover,
    c3,
    _
) {

    return function() {

        //minimum gap between the labels to make sure they aren't too close together
        var MIN_LABEL_GAP = 5;

        return c3.labelledAxis()

            .extend(function() {
                var ticks = this.selection().selectAll(".tick")[0];

                var reducedTicks = overlapRemover.reduce(ticks, function(a, b) {
                    var $a = $(a);
                    var $b = $(b);
                    return $a.position().left + $a[0].getBBox().width + MIN_LABEL_GAP > $b.position().left;
                });

                _.each(_.difference(ticks, reducedTicks), function(tick) {
                    $(tick).remove();
                });
            });
    };
});
