/**
 * Only draws the area from one direction, e.g. if the first point is above the second we draw the area but not the
 * other way around.
 *
 * @pre data must be an array with the first value x and y accessor must return an array, where first is y0 and
 * second is y1. TODO: Tried fixing this but to get it working correctly takes too much time and fine tuning.
 *
 * @module jira-dashboard-items/components/charts/components/area-between-plot-one-way
 */
define('jira-dashboard-items/components/charts/components/area-between-plot-one-way', [
    'jira-dashboard-items/components/charts/components/area-between-plot',
    'jira-dashboard-items/lib/c3'
], function(
    AreaBetweenPlot,
    c3
) {

    var AREA_FIRST_ABOVE = 0; //Draw area down to second if first above
    var AREA_SECOND_ABOVE = 1; //Draw area down to first if second above

    /**
     * Used to generate an AreaBetweenPlotOneWay component with a certain direction.
     *
     * @constructor
     * @param {number} direction
     * @returns {c3.component}
     */
    var AreaBetweenPlotOneWay = function(direction) {
        return c3.component('areaBetweenPlotOneWay')
            .extend(AreaBetweenPlot())
            .extend({
                /**
                 * Add the point of intersections into the data so that it can properly generate the area when
                 * the overlap between points.
                 * @returns {Function}
                 */
                dataFilter: function() {
                    var self = this;
                    return function(data) {
                        var modifiedData = includePointsOfIntersection(data, self.xAccessor(), self.yAccessor());
                        return [modifiedData];
                    };
                },

                /**
                 * TODO: Remove this. Creates dependency that data must be in the format [x, [y0, y1]].
                 * Don't even bother trying to fix this. It is hell and you will not succeeed.
                 * @returns {Function}
                 */
                x: function() {
                    var xScale = this.xScale();
                    var xAccessor = function(d) {
                        return d[0];
                    };
                    return function(d) {
                        return xScale(xAccessor(d));
                    };
                },

                /**
                 * @override AreaBetweenPlot.areaBottom
                 * @returns {Function}
                 */
                areaBottom: function() {
                    var self = this;
                    //Put the smallest line value at the bottom
                    return function(d) {
                        var first = self.y0Accessor()(d);
                        var second = self.y1Accessor()(d);
                        if (first > second) {
                            return self.y1()(d);
                        } else {
                            return self.y0()(d);
                        }
                    };
                },

                /**
                 * @override AreaBetweenPlot.areaTop
                 * @returns {Function}
                 */
                areaTop: function() {
                    var self = this;
                    return function(d) {
                        //Put itself as the top
                        if (direction === AREA_FIRST_ABOVE) {
                            return self.y0()(d);
                        } else if (direction === AREA_SECOND_ABOVE) {
                            return self.y1()(d);
                        }
                        throw "SHOULD HAVE A VALID DIRECTION";
                    };
                }
            });
    };


    /**
     * Determine if these values have opposite sign values.
     * @param {number} a
     * @param {number} b
     * @returns {boolean}
     */
    function oppositeSigns(a, b) {
        if (a < 0 && b > 0) {
            return true;
        } else if (a > 0 && b < 0) {
            return true;
        }
        return false;
    }

    /**
     * Given a data set it calculates any points of intersections and places them in between the data slots.
     * @param {Array} data to process
     * @param {Function} xAccessor to grab the x value from the data point.
     * @param {Function} yAccessor to grab the y value from the data point.
     * @returns {Array} including any point of interceptions
     */
    function includePointsOfIntersection(data, xAccessor, yAccessor) {
        var newData = [];

        data.forEach(function(entry, index) {

            var currentX = xAccessor(entry);
            var currentY = yAccessor(entry);

            var notFirst = index > 0;

            if (notFirst) {
                var previous = data[index - 1];
                var previousX = xAccessor(previous);
                var previousY = yAccessor(previous);
                var previousYA = previousY[0];
                var previousYB = previousY[1];

                var currentYA = currentY[0];
                var currentYB = currentY[1];

                var hasFlippedBetween = (oppositeSigns(previousYA - previousYB, currentYA - currentYB));

                if (hasFlippedBetween) {
                    var midX;
                    var midY;

                    var gradientA = (currentYA - previousYA) / (currentX - previousX);
                    var gradientB = (currentYB - previousYB) / (currentX - previousX);

                    var yInterceptA = currentYA - gradientA * currentX;
                    var yInterceptB = currentYB - gradientB * currentX;

                    midX = (yInterceptB - yInterceptA) / (gradientA - gradientB);

                    midY = gradientA * midX + yInterceptA;

                    newData.push([midX, [midY, midY]]);
                }
            }

            newData.push([currentX, currentY]);
        });
        return newData;
    }

    /**
     * @exports jira-dashboard-items/components/charts/components/area-between-plot-one-way
     */
    return {
        firstAbove: function() {
            return AreaBetweenPlotOneWay(AREA_FIRST_ABOVE);
        },
        secondAbove: function() {
            return AreaBetweenPlotOneWay(AREA_SECOND_ABOVE);
        }
    };
});
