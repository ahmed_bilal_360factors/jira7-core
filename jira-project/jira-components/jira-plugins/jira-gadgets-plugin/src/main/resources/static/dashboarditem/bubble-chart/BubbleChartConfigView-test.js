AJS.test.require([
    'com.atlassian.jira.gadgets:common-test-resources',
    'com.atlassian.jira.gadgets:bubble-chart-dashboard-item-resources'
], function () {
    require([
        'jira-dashboard-items/bubble-chart-config-view',
        'jquery'
    ], function (
        BubbleChartConfigView,
        $
    ) {
        'use strict';

        var GADGET_ID = '10000';

        var PREFERENCES = {
            bubbleType: 'votes',
            id: '10000',
            isConfigured: true,
            name: 'some_project',
            recentCommentsPeriod: '28',
            refresh: '15',
            type: 'project',
            useLogarithmicScale: true,
            useRelativeColoring: true
        };

        module('jira-dashboard-items/bubble-chart-config-view', {
            setup: function () {
                this.$el = $('#qunit-fixture');

                this.configView = new BubbleChartConfigView({
                    el: this.$el.get(0),
                    id: GADGET_ID,
                    projectFilterDelay: function(callback) { callback(); }
                });

                this.server = sinon.fakeServer.create();
            },
            teardown: function () {
                this.server.restore();
            },
            filterPickerValueElement: function() {
                return this.configView.$el.find('.filterpicker-value-name.field-value');
            },
            bubbleTypeSelectElement: function() {
                return this.configView.$el.find('select[name=bubbleType]');
            },
            recentCommentsPeriodSelectElement: function() {
                return this.configView.$el.find('select[name=recentCommentsPeriod]');
            },
            refreshIntervalElement: function() {
                return this.configView.$el.find('input[name=refresh-interval]');
            },
            useLogarithmicScaleElement: function() {
                return this.configView.$el.find('input[name=useLogarithmicScale]');
            },
            useRelativeColoringElement: function() {
                return this.configView.$el.find('input[name=useRelativeColoring]');
            },
            assertElementExists: function($el) {
                equal($el.length, 1);
            }
        });

        test('Should load previously saved preferences into the form', function() {
            this.configView.render(PREFERENCES);

            equal(this.filterPickerValueElement().text(), PREFERENCES.name);
            equal(this.bubbleTypeSelectElement().val(), PREFERENCES.bubbleType);
            equal(this.recentCommentsPeriodSelectElement().val(), PREFERENCES.recentCommentsPeriod);
            equal(this.refreshIntervalElement().val(), PREFERENCES.refresh);
            equal(this.useLogarithmicScaleElement().is(':checked'), PREFERENCES.useLogarithmicScale);
            equal(this.useRelativeColoringElement().is(':checked'), PREFERENCES.useRelativeColoring);
        });

        test('Should create a form with the required fields when rendering and pre-fill the defaults', function() {
            this.configView.render({});

            this.assertElementExists(this.filterPickerValueElement());
            this.assertElementExists(this.bubbleTypeSelectElement());
            this.assertElementExists(this.recentCommentsPeriodSelectElement());
            this.assertElementExists(this.refreshIntervalElement());
            this.assertElementExists(this.useLogarithmicScaleElement());
            this.assertElementExists(this.useRelativeColoringElement());

            equal(this.recentCommentsPeriodSelectElement().val(), 7);
            ok(this.refreshIntervalElement().val() === '15');
            ok(!this.useLogarithmicScaleElement().is(':checked'));
            ok(this.useRelativeColoringElement().is(':checked'));
        });

        test('Should return true when validating correct preferences', function() {
            ok(this.configView.validPreferences(PREFERENCES));
        });

        test('Should return false when validating incorrect preferences', function() {
            equal(this.configView.validPreferences({ type: 'project' }), false);
            equal(this.configView.validPreferences({ id: '10000' }), false);
            equal(this.configView.validPreferences({ some: 'other', attribute: 'attr' }), false);
        });

        test('Should send the correct fields when submitting the form', function() {
            var onSubmit = sinon.spy();
            this.configView.render(PREFERENCES);
            this.configView.on('submit', onSubmit);

            this.configView.$el.find('form').trigger('submit');

            ok(onSubmit.calledWith({
                bubbleType: 'votes',
                id: '10000',
                name: 'some_project',
                recentCommentsPeriod: '28',
                refresh: '15',
                type: 'project',
                useLogarithmicScale: true,
                useRelativeColoring: true
            }));
        });

        test("Should emit a cancel event when the form's cancel button is clicked", function() {
            expect(1);
            this.configView.render({ isConfigured: true });

            this.configView.on('cancel', function() {
                ok(true);
            });

            this.configView.$el.find('input.cancel').trigger('click');
        });
    });
});
