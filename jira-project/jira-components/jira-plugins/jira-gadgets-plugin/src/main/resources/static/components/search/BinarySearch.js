/**
 * @name BinarySearch
 * @global
 *
 * Implements Binary search functionality.
 */
define("jira-dashboard-items/components/search/binary-search", [
    'underscore'
], function(
    _
) {

    /**
     * @function
     * @name ComparatorFunction
     * @global
     *
     * @param goal the value you are trying to find
     * @param other the value you are comparing to
     * @returns {Number} < 0 if goal is before other, 0 if equal, > 0 if goal after other
     */
    function defaultComparator(goal, other) {
        return goal - other;
    }

    /**
     * Create a comparator function if non specified
     * @param {*} comparator
     * @returns {ComparatorFunction}
     */
    function generateComparator(comparator) {
        if (!comparator || !_.isFunction(comparator)) {
            return defaultComparator;
        }
        return comparator;
    }


    return {

        /**
         * @param {Array} array to search in
         * @param {*} object that is being searched for
         * @param {ComparatorFunction} comparator is the function that will test whether the object is greater than, equal or less than
         *        a given other object.
         * @returns {Number}
         */
        search: function(array, object, comparator) {
            comparator = generateComparator(comparator);

            var low = 0;
            var high = array.length;
            while (low < high) {
                var middle = (high + low) >> 1;

                var comparison = comparator(object, array[middle]);
                if (comparison === 0) {
                    return middle;
                } else if (comparison < 0) {
                    high = middle;
                } else {
                    low = middle + 1;
                }
            }

            return -1;
        }
    };
});
