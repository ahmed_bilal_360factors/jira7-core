
AJS.test.require("com.atlassian.jira.gadgets:dashboard-item-common-resources", function() {

    var CommonFunctions = require('jira-dashboard-items/common-functions');
    module('jira-dashboard-items/common-resources', {});

    test("preferences backward compatible splits correctly", function() {
        var preferences = {
            projectOrFilterId: 'project-10000'
        };

        var newPreferences = CommonFunctions.projectFilterBackwardCompatible(preferences);

        equal(newPreferences.type, 'project', "Should have extracted the type correctly");
        equal(newPreferences.id, '10000', "Should have extracted the id correctly");
        equal(newPreferences.projectOrFilterId, undefined, "Should have removed the project or filter id");
    });

    test("backward compatible change not made if new format is being used", function() {
        var preferences = {
            projectOrFilterId: 'project-10000',
            type: 'filter',
            id: '10001'
        };

        var newPreferences = CommonFunctions.projectFilterBackwardCompatible(preferences);

        equal(newPreferences.type, 'filter', "Should have extracted the type correctly");
        equal(newPreferences.id, '10001', "Should have extracted the id correctly");
        equal(newPreferences.projectOrFilterId, undefined, "Should have removed the project or filter id");
    });

    test("incorrectly formatted projectFilterId does not crash and create semi complete data", function() {
        var preferences = {
            projectOrFilterId: 'project+10000'
        };

        var newPreferences = CommonFunctions.projectFilterBackwardCompatible(preferences);

        equal(newPreferences.type, undefined, "Should not have been able to extract the type");
        equal(newPreferences.id, undefined, "Should not have been able to extract the id correctly");
        equal(newPreferences.projectOrFilterId, undefined, "Should have removed the ill-formatted project or filter id");
    });

    test("legacy project filter id able to be generated", function() {
        var preferences = {
            type: 'filter',
            id: '10001'
        };

        var newPreferences = CommonFunctions.getSafeLegacyProjectOrFilterId(preferences);
        equal(newPreferences.projectOrFilterId, 'filter-10001', "Should have been able to merge to legacy style");
        equal(newPreferences.type, undefined, "Should not have been able to extract the type");
        equal(newPreferences.id, undefined, "Should not have been able to extract the id correctly");
    });
});
