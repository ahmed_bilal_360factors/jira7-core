AJS.test.require([
    "com.atlassian.jira.gadgets:common-test-resources",
    "com.atlassian.jira.gadgets:createdvsresolved-dashboard-item-resources"
], function() {
    var context = AJS.test.mockableModuleContext();

    var $ = require('jquery');
    var _ = require('underscore');
    var Backbone = require('backbone');
    var moment = require('jira/moment/moment.lib');

    var MockConfigView = Backbone.View.extend({
        render: function() {

        }
    });

    var createdChartRenderSpy = sinon.spy();

    var MockCreatedVsResolvedChart = Backbone.View.extend({
        render: createdChartRenderSpy
    });

    var unresolvedChartRenderSpy = sinon.spy();

    var MockUnresolvedTrendChart = Backbone.View.extend({
        render: unresolvedChartRenderSpy
    });

    context.mock('jira/moment', moment);
    context.mock('jira-dashboard-items/createdvsresolved-config-view', MockConfigView);
    context.mock('jira-dashboard-items/components/charts/createdvsresolved-chart', MockCreatedVsResolvedChart);
    context.mock('jira-dashboard-items/components/charts/unresolved-trend-chart', MockUnresolvedTrendChart);

    //  This must be required after all the above mocks, due to the dependency of
    //  the dashboard item on the above mockings
    var CreatedVsResolvedDashboardItem = context.require('jira-dashboard-items/createdvsresolved');

    module('jira-dashboard-items/createdvsresolved', {
        setup: function () {

            this.$el = $("<div/>");

            var savePreferencesFailSpy = sinon.spy();
            this.savePreferencesSpy = sinon.spy(function() {
                return {
                    fail: savePreferencesFailSpy
                };
            });

            if (CreatedVsResolvedDashboardItem){

                createdChartRenderSpy.reset();
                unresolvedChartRenderSpy.reset();

                this.$el.append(JIRA.DashboardItem.CreatedVsResolved.Templates.Container());

                var API = $.extend({}, DashboardItem.Mocks.API, {
                    savePreferences: this.savePreferencesSpy
                });

                var gadgetOptions = {
                    delay: function(callback) { callback(); }
                };

                this.dashboardItem = new CreatedVsResolvedDashboardItem(API, gadgetOptions);
            }

            $("#qunit-fixture").append(this.$el);

            this.server = sinon.fakeServer.create();
        },

        teardown: function () {
            this.server.restore();
            this.$el.remove();
        }

    });

    var STUB_CONFIG_DATA = {
        name: 'filter',
        type: 'type',
        id: '10000',
        operation: 'count',
        showUnresolvedTrend: true,
        versionLabel: 'all',
        refresh: '15'
    };

    var STUB_VERSION_DATA = {
        versions: [
            {
                id: 10000,
                description: 'test',
                name: '1.1.1',
                archived: false,
                released: true,
                releaseDate: moment("03-Jan-2014").valueOf(),
                project: {
                    id: 10000,
                    name: 'Bulk Move 1',
                    key: 'BULK'
                }
            },
            {
                id: 10001,
                description: 'test',
                name: '1.1.2',
                archived: false,
                released: false,
                releaseDate: moment("03-Jan-2014").valueOf(),
                project: {
                    id: 10000,
                    name: 'Bulk Move 1',
                    key: 'BULK'
                }
            },
            {
                id: 10002,
                description: 'test',
                name: '1.1.2',
                archived: false,
                released: false,
                project: {
                    id: 10000,
                    name: 'Bulk Move 1',
                    key: 'BULK'
                }
            }
        ]
    };

    var STUB_ISSUE_DATA = {
        filterTitle: "title",
        filterUrl: "url",
        totals: {
            resolved: 10,
            created: 20
        },
        results: [
            {
                start: moment("01-Jan-2014").valueOf(),
                end: moment("02-Jan-2014").valueOf() - 1,
                data: {
                    created: {
                        count: 1
                    },
                    resolved: {
                        count: 1
                    },
                    unresolved: {
                        count: 0
                    }
                }
            },
            {
                start: moment("02-Jan-2014").valueOf(),
                end: moment("03-Jan-2014").valueOf() - 1,
                data: {
                    created: {
                        count: 4
                    },
                    resolved: {
                        count: 1
                    },
                    unresolved: {
                        count: 3
                    }
                }
            },
            {
                start: moment("03-Jan-2014").valueOf(),
                end: moment("04-Jan-2014").valueOf() - 1,
                data: {
                    created: {
                        count: 6
                    },
                    resolved: {
                        count: 5
                    },
                    unresolved: {
                        count: 4
                    }
                }
            },
            {
                start: moment("04-Jan-2014").valueOf(),
                end: moment("05-Jan-2014").valueOf() - 1,
                data: {
                    created: {
                        count: 9
                    },
                    resolved: {
                        count: 4
                    },
                    unresolved: {
                        count: 9
                    }
                }
            }
        ],
        versions: STUB_VERSION_DATA.versions
    };

    test("Submitting the form sends a request with the correct values", function () {
        this.dashboardItem.renderEdit(this.$el, {});

        this.$el.find("select[name=period]").val('monthly');
        this.$el.find("input[name=range]").val('25');
        this.$el.find("select[name=operation]").val('count');
        this.$el.find("select[name=unresolved]").val('false');
        this.$el.find("select[name=versions]").val('major');
        this.$el.find("input[refresh-interval]").attr('checked', 'true');

        var configViewPreferences = {
            name: 'myfilter',
            type: 'filter',
            id: '10000',
            periodName: 'monthly',
            range: 25,
            operation: 'cumulative',
            showUnresolvedTrend: true,
            versionLabel: 'major',
            refresh: true
        };

        var expectedDashboardItemPreferences = {
            name: 'myfilter',
            type: 'filter',
            id: '10000',
            periodName: 'monthly',
            daysprevious: 25,
            operation: 'cumulative',
            showUnresolvedTrend: true,
            versionLabel: 'major',
            refresh: '15'
        };

        this.dashboardItem.configView.trigger('submit', configViewPreferences);
        $("form", this.$el).submit();

        ok(this.savePreferencesSpy.calledOnce, "Should have called the save preferences on form submit");
        var calledArg = this.savePreferencesSpy.args[0][0];
        deepEqual(calledArg, expectedDashboardItemPreferences, "Should have saved the preferences returned by the config view");
    });

    test("Rendering configured CreatedVsResolved dashboard item generates chart", function () {
        this.spy(this.dashboardItem.API, 'initRefresh');
        this.dashboardItem.render(this.$el, STUB_CONFIG_DATA);

        this.server.requests[0].respond(200, {"Content-Type": "application/json"}, JSON.stringify(STUB_ISSUE_DATA));

        ok(createdChartRenderSpy.calledOnce, "Should have attempted to render the created vs resolved chart");
        ok(unresolvedChartRenderSpy.calledOnce, "Should have attempted to render the unresolved trend chart");
        sinon.assert.calledOnce(this.dashboardItem.API.initRefresh, "Should have set up auto refresh");
    });

    test("Rendering CreatedVsResolved dashboard item with legacy isCumulative option generates chart with correct operation", function() {
        this.spy(this.dashboardItem.API, 'initRefresh');
        this.dashboardItem.render(this.$el, _.extend({}, STUB_CONFIG_DATA, {
            operation: "",
            isCumulative: true
        }));

        this.server.requests[0].respond(200, {"Content-Type": "application/json"}, JSON.stringify(STUB_ISSUE_DATA));

        ok(this.server.requests[0].url.match(/\boperation=cumulative\b/), "Should have converted legacy isCumulative option to cumulative operation");
        ok(createdChartRenderSpy.calledOnce, "Should have attempted to render the created vs resolved chart");
        ok(unresolvedChartRenderSpy.calledOnce, "Should have attempted to render the unresolved trend chart");
    });

    test("No unresolved trend does not render it", function () {
        this.dashboardItem.render(this.$el, _.extend(STUB_CONFIG_DATA, {showUnresolvedTrend: false}));

        this.server.requests[0].respond(200, {"Content-Type": "application/json"}, JSON.stringify(STUB_ISSUE_DATA));

        ok(createdChartRenderSpy.calledOnce, "Should have attempted to render the created vs resolved chart");
        ok(!unresolvedChartRenderSpy.called, "Should not have attempted to render the unresolved trend chart");
    });

    test("Error retrieving CreatedVsResolved data displays error", function () {
        this.dashboardItem.render(this.$el, STUB_CONFIG_DATA);

        this.server.requests[0].respond(500, {"Content-Type": "application/json"}, JSON.stringify({}));

        ok(this.$el.find(".aui-message.aui-message-error").length === 1, "Should have rendered error message for error retrieving data");
    });

    test("Rendering CreatedVsResolved dashboard item with no versions generates chart and doesn't request version data", function () {
        this.dashboardItem.render(this.$el, _.extend(STUB_CONFIG_DATA, {versionLabel:'none'}));

        this.server.requests[0].respond(200, {"Content-Type": "application/json"}, JSON.stringify(STUB_ISSUE_DATA));

        ok(createdChartRenderSpy.calledOnce, "Should have attempted to render the created vs resolved chart");
        equal(1, this.server.requests.length, "Should only request chart data and not version data, but requested version data as well");
    });

    test("Version name validation only allows valid versions given certain configurations", function() {
        ok(this.dashboardItem._isValidVersionName("all", "version"), "Major version is valid for all");
        ok(this.dashboardItem._isValidVersionName("all", "1.0.0.0"), "Minor version is valid for all");
        ok(this.dashboardItem._isValidVersionName("major", "version"), "Major version is valid for major");
        ok(!this.dashboardItem._isValidVersionName("major", "1.0.0.0"), "Minor version is invalid for major");
        ok(!this.dashboardItem._isValidVersionName("none", "version"), "Major version is invalid for none");
        ok(!this.dashboardItem._isValidVersionName("none", "1.0.0.0"), "Minor version is invalid for none");
    });

    test("Minor version detection 'correctly' identifies minor versions", function() {
        ok(!this.dashboardItem._isMinorVersion(undefined), "Undefined is not a minor version");
        ok(!this.dashboardItem._isMinorVersion(""), "Empty is not a minor version");
        ok(!this.dashboardItem._isMinorVersion("version 1"), "1 is a major version");
        ok(!this.dashboardItem._isMinorVersion("1"), "1 is a major version");
        ok(!this.dashboardItem._isMinorVersion("1.0"), "1.0 is a major version");
        ok(this.dashboardItem._isMinorVersion("1.0.0"), "1.0.0 is a minor version");
        ok(this.dashboardItem._isMinorVersion("Version 0.0.2"), "Version 0.0.2 is a minor version");
        ok(this.dashboardItem._isMinorVersion("1.0.0beta"), "beta is a minor version");
        ok(this.dashboardItem._isMinorVersion("1.0.0alpha"), "alpha is a minor version");
    });
});
