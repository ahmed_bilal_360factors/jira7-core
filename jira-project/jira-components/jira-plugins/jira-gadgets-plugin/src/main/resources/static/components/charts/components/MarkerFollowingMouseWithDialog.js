/**
 * @module MarkerFollowingMouseWithDialog
 */
define('jira-dashboard-items/components/charts/components/marker-following-mouse-with-dialog', [
    'jquery',
    'underscore',
    'jira-dashboard-items/components/charts/components/marker-following-mouse'
], function(
    $,
    _,
    MarkerFollowingMouse
) {

    /**
     * @typedef {Object} MarkerFollowingMouseWithDialogOptions
     *
     * @property {Number} yTopOffset if we want to extend the marker further to the top (negative means further up)
     * @param {Function} generateContent give the data and x coordinate create content for the dialog
     * @param {String} domContextSelector used to select the dom using jQuery.
     * @param {c3.component} region used to bind mouse events to.
     */

    /**
     * @constructor
     *
     * @param {MarkerFollowingMouseWithDialogOptions} options
     */
    return function (options) {
        var generateContent = options.generateContent;

        var dialog;
        function createDialog(trigger, content) {
            dialog = AJS.InlineDialog($(trigger), 'marker-following-mouse', function(element, trigger, showPopup) {
                element.parent().addClass('marker-following-mouse-dialog');
                element.html(content);
                showPopup();
                return false;
            }, {
                fadeTime: 0,
                cacheContent: false, // don't cache the dialog content
                hideDelay: 60000, // set longer timeout (default is 10 seconds)
                gravity: 's',
                persistent: true,
                offsetX: -20
            });
        }

        options.domContext = $(options.domContextSelector);

        var markers = MarkerFollowingMouse(options);

        markers.showCallback(function() {
            createDialog(markers.elements()[0], "");

            dialog.show();
        });

        markers.refreshCallback(function(xCoordinate) {
            var content = generateContent(xCoordinate);

            if (content) {
                if (!dialog) {
                    createDialog(markers.elements()[0], content);
                } else {
                    dialog.find(".contents").first().html(content);
                    dialog.refresh();
                }
            } else {
                markers.hide();
            }
        });

        markers.hideCallback(function() {
            if (dialog) {
                dialog.hide();
                dialog.remove();
                dialog = null;
            }
        });

        return markers;
    };
});
