package com.atlassian.jira.gadgets.system.bubblechart;

import com.atlassian.jira.bc.issue.comment.CommentService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.charts.util.ChartUtils;
import com.atlassian.jira.gadgets.system.SearchQueryBackedResource;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.rest.api.util.ErrorCollection;
import com.atlassian.jira.rest.v1.model.errors.ValidationError;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.Map;

import static com.atlassian.collectors.CollectorsUtil.toImmutableList;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;

/**
 * The REST resource for the bubble chart dashboard item.
 */
@Path("/bubblechart")
@Produces(MediaType.APPLICATION_JSON)
@AnonymousAllowed
public class BubbleChartResource extends SearchQueryBackedResource {
    private static final int MAX_ISSUES = 200;
    private static final String BUBBLE_TYPE = "bubbleType";
    private static final String RECENT_COMMENTS_PERIOD = "recentCommentsPeriod";

    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final CommentService commentService;

    public BubbleChartResource(
            @ComponentImport final ChartUtils chartUtils,
            @ComponentImport final JiraAuthenticationContext jiraAuthenticationContext,
            @ComponentImport final SearchService searchService,
            @ComponentImport final PermissionManager permissionManager,
            @ComponentImport final VelocityRequestContextFactory velocityRequestContextFactory,
            @ComponentImport final CommentService commentService) {
        super(chartUtils, jiraAuthenticationContext, searchService, permissionManager, velocityRequestContextFactory);
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.commentService = commentService;
    }

    @GET
    @Path("/generate")
    @Produces(MediaType.APPLICATION_JSON)
    public Response generateChart(
            @QueryParam(QUERY_STRING) String projectOrFilterId,
            @QueryParam(BUBBLE_TYPE) @DefaultValue("participants") final String bubbleType,
            @QueryParam(RECENT_COMMENTS_PERIOD) @DefaultValue("1") final int recentCommentsDays
    ) {
        if (StringUtils.isNotBlank(projectOrFilterId) && !projectOrFilterId.contains("-")) {
            projectOrFilterId = "filter-" + projectOrFilterId;
        }

        final Map<String, Object> params = Maps.newHashMap();
        final List<ValidationError> errors = Lists.newArrayList();
        final SearchRequest searchRequest = getSearchRequestAndValidate(projectOrFilterId, errors, params);
        if (!errors.isEmpty()) {
            return Response.status(BAD_REQUEST).entity(toErrorCollection(errors)).build();
        }

        final SearchResults search;
        try {
            search = searchService.search(jiraAuthenticationContext.getLoggedInUser(), searchRequest.getQuery(), new PagerFilter(MAX_ISSUES));
        } catch (SearchException e) {
            return Response.status(INTERNAL_SERVER_ERROR).build();
        }

        final int issueCount = search.getTotal();
        final I18nHelper i18n = jiraAuthenticationContext.getI18nHelper();
        final String warning = issueCount > MAX_ISSUES ? i18n.getText("bubble-charts.exceed.issues", Integer.toString(issueCount), Integer.toString(MAX_ISSUES)) : null;
        final List<IssueBubbleData> data = search.getIssues()
                .stream()
                .map(new ToIssueBubbleData(commentService, jiraAuthenticationContext.getLoggedInUser(), bubbleType, recentCommentsDays))
                .collect(toImmutableList());

        final String filterTitle = i18n.getText(getFilterTitle(params));
        return Response.ok(new ResponseContainer(warning, data, filterTitle, issueCount)).build();
    }

    private ErrorCollection toErrorCollection(final List<ValidationError> errors) {
        final I18nHelper i18n = authenticationContext.getI18nHelper();
        final com.atlassian.jira.util.ErrorCollection errorCollection = new SimpleErrorCollection();
        errors.forEach(error -> errorCollection.addError(error.getField(), i18n.getText(error.getError())));
        return ErrorCollection.of(errorCollection);
    }

    @XmlRootElement
    public static class ResponseContainer {
        @XmlElement
        private final String warning;
        @XmlElement
        private final int issueCount;
        @XmlElement
        private final String filterTitle;
        @XmlElement
        private final List<IssueBubbleData> data;

        public ResponseContainer(
                final String warning,
                final List<IssueBubbleData> issueData,
                final String filterTitle,
                final int issueCount) {
            this.warning = warning;
            this.issueCount = issueCount;
            this.filterTitle = filterTitle;
            this.data = issueData;
        }

        public String getWarning() {
            return warning;
        }

        public int getIssueCount() {
            return issueCount;
        }

        public String getFilterTitle() {
            return filterTitle;
        }

        public List<IssueBubbleData> getData() {
            return data;
        }
    }
}
