package com.atlassian.jira.dashboarditem.statistics.service.statistics.beans;

import com.atlassian.jira.dashboarditem.statistics.service.statistics.searcher.beans.StatisticsSearchResultBean;
import com.atlassian.jira.dashboarditem.statistics.service.statistics.searcher.beans.StatisticsSearchResultRowBean;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Represents the generated statistics results returned from the search.
 */
@XmlRootElement
public class OneDimensionalStatisticsResultBean {
    @XmlElement
    private String filterTitle;

    @XmlElement
    private String filterUrl;

    @XmlElement
    private String statType;

    @XmlElement
    private long issueCount;

    @XmlElement
    private List<StatisticsSearchResultRowBean> results;

    public OneDimensionalStatisticsResultBean(final String filterTitle, final String filterUrl, final String statType, final StatisticsSearchResultBean statsBean) {
        this.filterTitle = filterTitle;
        this.filterUrl = filterUrl;
        this.statType = statType;
        this.results = statsBean.getResults();
        this.issueCount = statsBean.getTotal();
    }

    public String getFilterTitle() {
        return filterTitle;
    }

    public String getFilterUrl() {
        return filterUrl;
    }

    public String getStatType() {
        return statType;
    }

    public long getIssueCount() {
        return issueCount;
    }

    public List<StatisticsSearchResultRowBean> getResults() {
        return results;
    }
}
