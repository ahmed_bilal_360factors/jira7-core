package com.atlassian.jira.gadgets.system;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.customfields.statistics.CustomFieldStattable;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.google.common.collect.ImmutableMap;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static com.atlassian.jira.rest.v1.util.CacheControl.NO_CACHE;

/**
 * REST resource for retrieving and validating Statistic Types.
 *
 * @since v4.0
 */
@Path("/statTypes")
@AnonymousAllowed
@Produces({MediaType.APPLICATION_JSON})
public class StatisticTypesResource {
    private final CustomFieldManager customFieldManager;
    private final JiraAuthenticationContext authenticationContext;
    private final PermissionManager permissionManager;
    private final StatisticTypesProvider statisticTypesProvider;

    public StatisticTypesResource(
            @ComponentImport final CustomFieldManager customFieldManager,
            @ComponentImport final JiraAuthenticationContext authenticationContext,
            @ComponentImport final PermissionManager permissionManager,
            final StatisticTypesProvider statisticTypesProvider) {

        this.customFieldManager = customFieldManager;
        this.authenticationContext = authenticationContext;
        this.permissionManager = permissionManager;
        this.statisticTypesProvider = statisticTypesProvider;
    }

    /**
     * Retrieve all available Statistic Types in JIRA.  Both System and Custom Fields.
     *
     * @return A collection key/value pairs {@link MapEntry}
     */
    @GET
    public Response getAxes() {
        final Map<String, String> statisticTypes = getStatisticTypes();
        return Response.ok(new StatTypeCollection(statisticTypes)).cacheControl(NO_CACHE).build();
    }

    private Map<String, String> getStatisticTypes() {

        final Map<String, String> systemStatisticTypes = statisticTypesProvider.getSystemStatisticTypes()
                .entrySet()
                .stream()
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        this::translateValue
                ));

        final Set<Project> visibleProjects =
                new HashSet<>(permissionManager.getProjects(BROWSE_PROJECTS, authenticationContext.getLoggedInUser()));

        final Map<String, String> customFieldStatisticTypes = customFieldManager.getCustomFieldObjects()
                .stream()
                .filter(this::isCustomFieldStattable)
                .filter(cf -> hasCustomFieldAccess(cf, visibleProjects))
                .collect(Collectors.toMap(
                        CustomField::getId,
                        this::translate
                ));

        return ImmutableMap.<String, String>builder()
                .putAll(systemStatisticTypes)
                .putAll(customFieldStatisticTypes)
                .build();
    }

    private String translateValue(final Map.Entry<?, String> entry) {
        return translate(entry.getValue());
    }

    private String translate(final CustomField customField) {
        return translate(customField.getName());
    }

    private String translate(final String key) {
        return authenticationContext.getI18nHelper().getText(key);
    }

    private boolean hasCustomFieldAccess(final CustomField customField, final Set<Project> visibleProjects) {

        if (customField.isAllProjects()) {
            return !visibleProjects.isEmpty();
        }

        return !Collections.disjoint(customField.getAssociatedProjectObjects(), visibleProjects);
    }

    private boolean isCustomFieldStattable(final CustomField customField) {
        return customField.getCustomFieldSearcher() instanceof CustomFieldStattable;
    }

    ///CLOVER:OFF
    @XmlRootElement
    public static class StatTypeCollection {
        @XmlElement
        private Collection<MapEntry> stats;

        @SuppressWarnings({"UnusedDeclaration", "unused"})
        private StatTypeCollection() {
        }

        public StatTypeCollection(final Map<String, String> values) {

            this.stats = convertToMapEntryCollection(values);
        }

        public Collection<MapEntry> getValues() {
            return stats;
        }

        private Collection<MapEntry> convertToMapEntryCollection(final Map<String, String> values) {
            return values.entrySet().stream()
                    .map(MapEntry::fromJavaMapEntry)
                    .collect(Collectors.toList());
        }
    }

    @XmlRootElement
    public static class MapEntry {
        @XmlElement
        private final String value;

        @XmlElement
        private final String label;

        @SuppressWarnings({"UnusedDeclaration", "unused"})
        private MapEntry() {
            value = null;
            label = null;
        }

        public static MapEntry fromJavaMapEntry(final Map.Entry<String, String> entry) {
            return new MapEntry(entry.getKey(), entry.getValue());
        }

        private MapEntry(final String value, final String label) {
            this.value = value;
            this.label = label;
        }

        public String getKey() {
            return value;
        }

        public String getValue() {
            return label;
        }
    }
    ///CLOVER:ON
}
