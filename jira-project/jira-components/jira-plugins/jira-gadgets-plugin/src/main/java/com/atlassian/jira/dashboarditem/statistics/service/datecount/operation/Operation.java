package com.atlassian.jira.dashboarditem.statistics.service.datecount.operation;

public enum Operation {
    count,
    cumulative
}
