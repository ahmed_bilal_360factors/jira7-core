package com.atlassian.jira.dashboarditem.statistics.service.datecount.beans;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class FieldResultBean {
    @XmlElement
    private final Integer count;

    @XmlElement
    private final String searchUrl;

    public FieldResultBean(final Integer count, final String searchUrl) {
        this.count = count;
        this.searchUrl = searchUrl;
    }

    public Integer getCount() {
        return count;
    }

    public String getSearchUrl() {
        return searchUrl;
    }
}
