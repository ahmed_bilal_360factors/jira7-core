package com.atlassian.jira.gadgets.system;

import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.statistics.AssigneeStatisticsMapper;
import com.atlassian.jira.issue.statistics.ComponentStatisticsMapper;
import com.atlassian.jira.issue.statistics.CreatorStatisticsMapper;
import com.atlassian.jira.issue.statistics.FixForVersionStatisticsMapper;
import com.atlassian.jira.issue.statistics.IssueTypeStatisticsMapper;
import com.atlassian.jira.issue.statistics.LabelsStatisticsMapper;
import com.atlassian.jira.issue.statistics.PriorityStatisticsMapper;
import com.atlassian.jira.issue.statistics.ProjectStatisticsMapper;
import com.atlassian.jira.issue.statistics.RaisedInVersionStatisticsMapper;
import com.atlassian.jira.issue.statistics.ReporterStatisticsMapper;
import com.atlassian.jira.issue.statistics.ResolutionStatisticsMapper;
import com.atlassian.jira.issue.statistics.StatisticsMapper;
import com.atlassian.jira.issue.statistics.StatusStatisticsMapper;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
class SystemStatisticTypesFactory {

    static final String COMPONENTS = "components";
    static final String FIXFOR = "fixfor";
    static final String ALLFIXFOR = "allFixfor";
    static final String VERSION = "version";
    static final String ALLVERSION = "allVersion";
    static final String ASSIGNEES = "assignees";
    static final String ISSUETYPE = "issuetype";
    static final String PRIORITIES = "priorities";
    static final String PROJECT = "project";
    static final String REPORTER = "reporter";
    static final String CREATOR = "creator";
    static final String RESOLUTION = "resolution";
    static final String STATUSES = "statuses";
    static final String LABELS = "labels";

    private final UserManager userManager;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final ConstantsManager constantsManager;
    private final VersionManager versionManager;
    private final ProjectManager projectManager;

    @Autowired
    SystemStatisticTypesFactory(
            @ComponentImport final UserManager userManager,
            @ComponentImport final JiraAuthenticationContext jiraAuthenticationContext,
            @ComponentImport final ConstantsManager constantsManager,
            @ComponentImport final VersionManager versionManager,
            @ComponentImport final ProjectManager projectManager
    ) {
        this.userManager = userManager;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.constantsManager = constantsManager;
        this.versionManager = versionManager;
        this.projectManager = projectManager;
    }

    Optional<StatisticsMapper> getSystemMapper(final String statsMapperKey) {

        switch (statsMapperKey) {
            case COMPONENTS:
                return Optional.of(new ComponentStatisticsMapper());

            case ASSIGNEES:
                return Optional.of(new AssigneeStatisticsMapper(userManager, jiraAuthenticationContext));

            case ISSUETYPE:
                return Optional.of(new IssueTypeStatisticsMapper(constantsManager));

            case FIXFOR:
                return Optional.of(new FixForVersionStatisticsMapper(versionManager, false));

            case ALLFIXFOR:
                return Optional.of(new FixForVersionStatisticsMapper(versionManager, true));

            case PRIORITIES:
                return Optional.of(new PriorityStatisticsMapper(constantsManager));

            case PROJECT:
                return Optional.of(new ProjectStatisticsMapper(projectManager));

            case VERSION:
                return Optional.of(new RaisedInVersionStatisticsMapper(versionManager, false));

            case ALLVERSION:
                return Optional.of(new RaisedInVersionStatisticsMapper(versionManager, true));

            case REPORTER:
                return Optional.of(new ReporterStatisticsMapper(userManager, jiraAuthenticationContext));

            case CREATOR:
                return Optional.of(new CreatorStatisticsMapper(userManager, jiraAuthenticationContext));

            case RESOLUTION:
                return Optional.of(new ResolutionStatisticsMapper(constantsManager));

            case STATUSES:
                return Optional.of(new StatusStatisticsMapper(constantsManager));

            case LABELS:
                return Optional.of(new LabelsStatisticsMapper(false));

            default:
                return Optional.empty();
        }
    }
}
