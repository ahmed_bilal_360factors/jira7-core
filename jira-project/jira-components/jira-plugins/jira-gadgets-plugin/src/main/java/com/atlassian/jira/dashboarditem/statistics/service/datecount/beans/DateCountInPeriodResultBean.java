package com.atlassian.jira.dashboarditem.statistics.service.datecount.beans;


import com.atlassian.jira.dashboarditem.statistics.service.versions.beans.ProjectVersionBean;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.Map;

@XmlRootElement
public class DateCountInPeriodResultBean {
    @XmlElement
    private final String filterTitle;

    @XmlElement
    private final String filterUrl;

    @XmlElement
    private final Map<String, Integer> totals;

    @XmlElement
    private final List<TimePeriodResultBean> results;

    @XmlElement
    private final List<ProjectVersionBean> versions;

    public DateCountInPeriodResultBean(final String filterTitle, final String filterUrl, final Map<String, Integer> totals, final List<TimePeriodResultBean> results, final List<ProjectVersionBean> versions) {
        this.filterTitle = filterTitle;
        this.filterUrl = filterUrl;
        this.totals = totals;
        this.results = results;
        this.versions = versions;
    }

    public String getFilterTitle() {
        return filterTitle;
    }

    public String getFilterUrl() {
        return filterUrl;
    }

    public Map<String, Integer> getTotals() {
        return totals;
    }

    public List<TimePeriodResultBean> getResults() {
        return results;
    }

    public List<ProjectVersionBean> getVersions() {
        return versions;
    }
}
