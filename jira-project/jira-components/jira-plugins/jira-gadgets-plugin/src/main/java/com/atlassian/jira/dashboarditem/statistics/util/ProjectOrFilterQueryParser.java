package com.atlassian.jira.dashboarditem.statistics.util;


import com.atlassian.fugue.Option;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.query.Query;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Used to generate and get query information for a given project or filter query.
 *
 * @since v6.4
 */
public interface ProjectOrFilterQueryParser {
    @Nonnull
    public Option<Query> getFilterQuery(@Nullable final ApplicationUser user, @Nonnull final Query query);

    @Nonnull
    QueryInformation getQueryInformation(@Nullable final ApplicationUser user, @Nonnull final Query query);

    public static class QueryInformation {
        private final String name;
        private final String url;

        public QueryInformation(final String name, final String url) {
            this.name = name;
            this.url = url;
        }

        public String getName() {
            return name;
        }

        public String getUrl() {
            return url;
        }
    }
}
