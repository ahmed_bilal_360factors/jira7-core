package com.atlassian.jira.gadgets.system;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.customfields.statistics.CustomFieldStattable;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.statistics.StatisticsMapper;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;

/**
 * Provides available values for stat types used by various gadgets
 */
@Component
public class StatisticTypesProvider {
    public static final String COMPONENTS = getString(SystemStatisticTypesFactory.COMPONENTS);
    public static final String FIXFOR = getString(SystemStatisticTypesFactory.FIXFOR);
    public static final String ALLFIXFOR = getString(SystemStatisticTypesFactory.ALLFIXFOR);
    public static final String VERSION = getString(SystemStatisticTypesFactory.VERSION);
    public static final String ALLVERSION = getString(SystemStatisticTypesFactory.ALLVERSION);
    public static final String ASSIGNEES = getString(SystemStatisticTypesFactory.ASSIGNEES);
    public static final String ISSUETYPE = getString(SystemStatisticTypesFactory.ISSUETYPE);
    public static final String PRIORITIES = getString(SystemStatisticTypesFactory.PRIORITIES);
    public static final String PROJECT = getString(SystemStatisticTypesFactory.PROJECT);
    public static final String REPORTER = getString(SystemStatisticTypesFactory.REPORTER);
    public static final String CREATOR = getString(SystemStatisticTypesFactory.CREATOR);
    public static final String RESOLUTION = getString(SystemStatisticTypesFactory.RESOLUTION);
    public static final String STATUSES = getString(SystemStatisticTypesFactory.STATUSES);
    public static final String LABELS = getString(SystemStatisticTypesFactory.LABELS);

    private static final Map<String, String> systemValues = ImmutableMap.<String, String>builder()
            .put(ASSIGNEES, "gadget.filterstats.field.statistictype.assignees")
            .put(COMPONENTS, "gadget.filterstats.field.statistictype.components")
            .put(ISSUETYPE, "gadget.filterstats.field.statistictype.issuetype")
            .put(FIXFOR, "gadget.filterstats.field.statistictype.fixfor")
            .put(ALLFIXFOR, "gadget.filterstats.field.statistictype.allfixfor")
            .put(PRIORITIES, "gadget.filterstats.field.statistictype.priorities")
            .put(PROJECT, "gadget.filterstats.field.statistictype.project")
            .put(VERSION, "gadget.filterstats.field.statistictype.version")
            .put(ALLVERSION, "gadget.filterstats.field.statistictype.allversion")
            .put(REPORTER, "gadget.filterstats.field.statistictype.reporter")
            .put(CREATOR, "gadget.filterstats.field.statistictype.creator")
            .put(RESOLUTION, "gadget.filterstats.field.statistictype.resolution")
            .put(STATUSES, "gadget.filterstats.field.statistictype.statuses")
            .put(LABELS, "gadget.filterstats.field.statistictype.labels")
            .build();

    private final JiraAuthenticationContext authenticationContext;
    private final CustomFieldManager customFieldManager;
    private final SystemStatisticTypesFactory systemStatisticTypesFactory;

    @Autowired
    public StatisticTypesProvider(
            @ComponentImport final JiraAuthenticationContext authenticationContext,
            @ComponentImport final CustomFieldManager customFieldManager,
            final SystemStatisticTypesFactory systemStatisticTypesFactory
    ) {
        this.authenticationContext = authenticationContext;
        this.customFieldManager = customFieldManager;
        this.systemStatisticTypesFactory = systemStatisticTypesFactory;
    }

    /**
     * Just to prevent constants from being hard inlined.
     */
    private static String getString(final String str) {
        return str;
    }

    /**
     * Get the {@link com.atlassian.jira.issue.statistics.StatisticsMapper} associated with passed key
     *
     * @param statsMapperKey The key for the stats mapper.
     *                       Usually a custom field id, or one of the constants in this class.
     * @return The StatisticsMapper associated with the passed in key.
     */
    public StatisticsMapper getStatsMapper(final String statsMapperKey) {
        return getSystemMapper(statsMapperKey)
                .orElseGet(() -> getCustomFieldMapper(statsMapperKey).orElse(null));
    }

    private Optional<StatisticsMapper> getSystemMapper(final String statsMapperKey) {
        return systemStatisticTypesFactory.getSystemMapper(statsMapperKey);
    }

    private Optional<StatisticsMapper> getCustomFieldMapper(final String statsMapperKey) {
        final CustomField customField = getCustomField(statsMapperKey)
                .orElseThrow(() -> new RuntimeException("No custom field with id " + statsMapperKey));

        if (customField.getCustomFieldSearcher() instanceof CustomFieldStattable) {
            final CustomFieldStattable customFieldStattable = (CustomFieldStattable) customField.getCustomFieldSearcher();
            return Optional.of(customFieldStattable.getStatisticsMapper(customField));
        } else {
            return Optional.empty();
        }
    }

    private Optional<CustomField> getCustomField(final String statsMapperKey) {
        return Optional.ofNullable(customFieldManager.getCustomFieldObject(statsMapperKey));
    }

    /**
     * @return returns a map of all system stat mappers
     */
    public Map<String, String> getSystemStatisticTypes() {
        return systemValues;
    }

    /**
     * Returns the display name for a field
     *
     * @param field The field to get the displayable name for
     * @return A human consumable name for the field
     */
    public String getDisplayName(final String field) {
        final I18nHelper i18n = authenticationContext.getI18nHelper();

        return getSystemStatisticType(field)
                .map(i18n::getText)
                .orElseGet(() -> getCustomField(field).map(CustomField::getName).orElse(""));
    }

    private Optional<String> getSystemStatisticType(final String field) {
        return Optional.ofNullable(systemValues.get(field));
    }
}
