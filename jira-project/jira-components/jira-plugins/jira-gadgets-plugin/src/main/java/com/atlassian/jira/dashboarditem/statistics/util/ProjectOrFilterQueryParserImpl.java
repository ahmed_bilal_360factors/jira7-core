package com.atlassian.jira.dashboarditem.statistics.util;

import com.atlassian.fugue.Option;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.constants.SavedFilterSearchConstants;
import com.atlassian.jira.issue.search.constants.SystemSearchConstants;
import com.atlassian.jira.issue.search.searchers.impl.NamedTerminalClauseCollectingVisitor;
import com.atlassian.jira.issue.search.searchers.util.TerminalClauseCollectingVisitor;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.JiraUrlCodec;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.query.Query;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.SingleValueOperand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;


/**
 * @since v6.4
 */
@Component
public class ProjectOrFilterQueryParserImpl implements ProjectOrFilterQueryParser {
    private final QueryUrlSupplier queryUrlSupplier;
    private final JiraAuthenticationContext authenticationContext;
    private final SearchRequestService searchRequestService;
    private final ProjectService projectService;

    @Autowired
    public ProjectOrFilterQueryParserImpl(
            final QueryUrlSupplier queryUrlSupplier,
            @ComponentImport final JiraAuthenticationContext authenticationContext,
            @ComponentImport final SearchRequestService searchRequestService,
            @ComponentImport final ProjectService projectService
    ) {
        this.queryUrlSupplier = queryUrlSupplier;
        this.authenticationContext = authenticationContext;
        this.searchRequestService = searchRequestService;
        this.projectService = projectService;
    }

    public static final String PROJECT_CLAUSE_NAME = SystemSearchConstants.forProject().getJqlClauseNames().getPrimaryName();
    public static final String FILTER_CLAUSE_NAME = SavedFilterSearchConstants.getInstance().getJqlClauseNames().getPrimaryName();


    @Nonnull
    @Override
    public QueryInformation getQueryInformation(@Nullable final ApplicationUser user, @Nonnull final Query query) {
        String filterName = authenticationContext.getI18nHelper().getText("jira.jql.query");
        String filterUrl = queryUrlSupplier.getUrlForQuery(query);

        if (isFilterOnlyClause(query)) {
            TerminalClause filterClause = getFilterClause(query);

            final Long filterId = ((SingleValueOperand) filterClause.getOperand()).getLongValue();
            final SearchRequest filter = searchRequestService.getFilter(new JiraServiceContextImpl(user, new SimpleErrorCollection()), filterId);
            if (filter != null) {
                filterName = filter.getName();
                filterUrl = queryUrlSupplier.getUrlForFilter(filterId);
            }
        } else if (isProjectOnlyClause(query)) {
            TerminalClause projectClause = getProjectClause(query);
            SingleValueOperand operand = (SingleValueOperand) projectClause.getOperand();
            ProjectService.GetProjectResult projectResult;
            if (operand.getStringValue() != null) {
                projectResult = projectService.getProjectByKey(user, operand.getStringValue());
            } else {
                projectResult = projectService.getProjectById(user, operand.getLongValue());
            }

            if (projectResult.isValid()) {
                filterName = projectResult.getProject().getName();
            }
        }
        return new QueryInformation(filterName, filterUrl);
    }


    @Override
    @Nonnull
    public Option<Query> getFilterQuery(@Nullable final ApplicationUser user, @Nonnull final Query query) {
        final TerminalClause filterClause = getFilterClause(query);
        if (filterClause == null) {
            return Option.none();
        }

        final Long filterId = ((SingleValueOperand) filterClause.getOperand()).getLongValue();
        final SearchRequest filter = searchRequestService.getFilter(new JiraServiceContextImpl(user, new SimpleErrorCollection()), filterId);
        if (filter != null) {
            return Option.some(filter.getQuery());
        } else {
            return Option.none();
        }
    }


    private boolean isProjectOnlyClause(@Nonnull final Query query) {
        return (getNumberClauses(query) == 1 && getProjectClause(query) != null);
    }

    private boolean isFilterOnlyClause(@Nonnull final Query query) {
        return (getNumberClauses(query) == 1 && getFilterClause(query) != null);
    }

    @Nullable
    private TerminalClause getProjectClause(@Nonnull final Query query) {
        return getClause(query, PROJECT_CLAUSE_NAME);
    }


    @Nullable
    private TerminalClause getClause(@Nonnull final Query query, @Nonnull final String clauseName) {
        final NamedTerminalClauseCollectingVisitor projectVisitor = new NamedTerminalClauseCollectingVisitor(clauseName);

        query.getWhereClause().accept(projectVisitor);

        if (projectVisitor.getNamedClauses().size() != 1) {
            return null;
        }

        TerminalClause clause = projectVisitor.getNamedClauses().get(0);

        if (!(clause.getOperand() instanceof SingleValueOperand)) {
            return null;
        }

        return clause;
    }


    private int getNumberClauses(@Nonnull final Query query) {
        final TerminalClauseCollectingVisitor clauseVisitor = new TerminalClauseCollectingVisitor();
        if (query.getWhereClause() != null) {
            query.getWhereClause().accept(clauseVisitor);
        }
        return clauseVisitor.getClauses().size();
    }


    @Nullable
    private TerminalClause getFilterClause(@Nonnull final Query query) {
        return getClause(query, FILTER_CLAUSE_NAME);
    }

    @Nonnull
    private String urlEncode(@Nonnull final String string) {
        return JiraUrlCodec.encode(string);
    }
}
