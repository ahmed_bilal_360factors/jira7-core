package com.atlassian.jira.dashboarditem.statistics.service.datecount.beans;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.Map;

@XmlRootElement
public class TimePeriodResultBean {
    @XmlElement
    final Date start;

    @XmlElement
    final Date end;

    @XmlElement
    final Map<String, FieldResultBean> data;

    public TimePeriodResultBean(final Date start, final Date end, final Map<String, FieldResultBean> data) {
        this.start = start;
        this.end = end;
        this.data = data;
    }

    public Date getStart() {
        return start;
    }

    public Date getEnd() {
        return end;
    }

    public Map<String, FieldResultBean> getData() {
        return data;
    }
}
