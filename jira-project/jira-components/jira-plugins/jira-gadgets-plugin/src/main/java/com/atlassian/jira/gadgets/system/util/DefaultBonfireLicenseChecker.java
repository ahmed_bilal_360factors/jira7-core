package com.atlassian.jira.gadgets.system.util;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DefaultBonfireLicenseChecker implements BonfireLicenseChecker {
    private final PluginAccessor pluginAccessor;

    @Autowired
    public DefaultBonfireLicenseChecker(@ComponentImport final PluginAccessor pluginAccessor) {
        this.pluginAccessor = pluginAccessor;
    }

    @Override
    public boolean bonfireIsActiveAndLicensed() {
        // it's enough that bonfire is enabled
        return pluginAccessor.getEnabledPlugin("com.atlassian.bonfire.plugin") != null;
    }
}
