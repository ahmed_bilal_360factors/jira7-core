package com.atlassian.jira.dashboarditem.quicklinks;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Objects;

/**
 * Corresponds to a website link in the Quick Links dashboard item. It contains a title, URL, and text.
 */
@XmlRootElement
public class Link {
    @XmlElement
    private String title;
    @XmlElement
    private String url;
    @XmlElement
    private String text;

    @SuppressWarnings({"UnusedDeclaration", "unused"})
    private Link() {
    }

    public Link(final String text, final String url, final String title) {
        this.title = title;
        this.url = url;
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public String getText() {
        return text;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final Link link = (Link) o;
        return Objects.equals(title, link.title) &&
                Objects.equals(url, link.url) &&
                Objects.equals(text, link.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, url, text);
    }
}
