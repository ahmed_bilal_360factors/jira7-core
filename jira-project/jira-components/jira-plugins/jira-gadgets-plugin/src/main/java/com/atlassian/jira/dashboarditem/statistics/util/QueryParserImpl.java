package com.atlassian.jira.dashboarditem.statistics.util;


import com.atlassian.fugue.Either;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;


@Component
public class QueryParserImpl implements QueryParser {

    private final SearchService searchService;


    @Autowired
    public QueryParserImpl(
            @ComponentImport final SearchService searchService
    ) {
        this.searchService = searchService;
    }

    @Override
    @Nonnull
    public Either<Query, ErrorCollection> getQuery(@Nullable final ApplicationUser user, @Nonnull final String jql) {
        final ErrorCollection errors = new SimpleErrorCollection();
        final SearchService.ParseResult parseResult = searchService.parseQuery(user, jql);
        if (!parseResult.isValid()) {
            errors.addErrorMessages(parseResult.getErrors().getErrorMessages());
            errors.addReason(ErrorCollection.Reason.VALIDATION_FAILED);
            return Either.right(errors);
        }

        final Query jqlQuery = parseResult.getQuery();
        MessageSet messages = searchService.validateQuery(user, jqlQuery);
        if (messages.hasAnyErrors()) {
            errors.addErrorMessages(messages.getErrorMessages());
            errors.addReason(ErrorCollection.Reason.VALIDATION_FAILED);
            return Either.right(errors);
        }

        return Either.left(jqlQuery);
    }
}
