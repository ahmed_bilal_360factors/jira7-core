package com.atlassian.jira.dashboarditem.statistics.service.datecount;

import com.atlassian.jira.dashboarditem.statistics.service.datecount.field.Field;
import com.atlassian.jira.dashboarditem.statistics.service.datecount.operation.Operation;
import com.atlassian.jira.dashboarditem.statistics.service.datecount.period.TimePeriod;

import java.util.Set;

/**
 * Builder for {@link com.atlassian.jira.dashboarditem.statistics.service.datecount.DateCountInPeriodService.RequestParameters}
 * to pass to the {@link com.atlassian.jira.dashboarditem.statistics.service.datecount.DateCountInPeriodService}.
 */
public class DateCountParametersBuilder {
    private String jql;
    private Set<Field> fields;
    private TimePeriod period;
    private Integer daysPrevious;
    private boolean includeVersions;
    private Operation operation;

    public DateCountParametersBuilder jql(final String jql) {
        this.jql = jql;
        return this;
    }

    public DateCountParametersBuilder fields(final Set<Field> fields) {
        this.fields = fields;
        return this;
    }

    public DateCountParametersBuilder period(final TimePeriod period) {
        this.period = period;
        return this;
    }

    public DateCountParametersBuilder daysPrevious(final Integer daysPrevious) {
        this.daysPrevious = daysPrevious;
        return this;
    }

    public DateCountParametersBuilder includeVersions(final boolean includeVersions) {
        this.includeVersions = includeVersions;
        return this;
    }

    public DateCountParametersBuilder operation(final Operation operation) {
        this.operation = operation;
        return this;
    }

    public DateCountInPeriodService.RequestParameters build() {
        return new DateCountInPeriodService.RequestParameters(jql, fields, period, daysPrevious, includeVersions, operation);
    }
}