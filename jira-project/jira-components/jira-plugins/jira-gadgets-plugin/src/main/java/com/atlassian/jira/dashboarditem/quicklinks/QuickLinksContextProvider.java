package com.atlassian.jira.dashboarditem.quicklinks;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.ContextProvider;
import com.google.common.collect.Maps;

import java.util.Map;

public class QuickLinksContextProvider implements ContextProvider {
    private final QuickLinksProvider quicklinksProvider;

    public QuickLinksContextProvider(QuickLinksProvider quicklinksProvider) {
        this.quicklinksProvider = quicklinksProvider;
    }

    @Override
    public void init(final Map<String, String> params) throws PluginParseException {
    }

    @Override
    public Map<String, Object> getContextMap(final Map<String, Object> context) {
        final Map<String, Object> newContext = Maps.newHashMap(context);
        final LinkLists linkList = quicklinksProvider.getLinks();
        newContext.put("commonLinks", linkList.getCommonLinks());
        newContext.put("navigationLinks", linkList.getNavigationLinks());
        return newContext;
    }
}
