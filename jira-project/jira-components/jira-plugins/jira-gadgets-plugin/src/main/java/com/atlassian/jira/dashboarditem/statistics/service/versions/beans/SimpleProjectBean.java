package com.atlassian.jira.dashboarditem.statistics.service.versions.beans;


import com.atlassian.jira.project.Project;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Represents a simple project bean. The reason that our own is defined is to reduce packet load for information not
 * currently needed, as well as ProjectBeanFactory not being exported by the REST plugin, therefore cannot use the
 * bean anyway.
 * <p>
 * since v6.4
 */
@XmlRootElement
public class SimpleProjectBean {
    @XmlElement
    private Long id;

    @XmlElement
    private String name;

    @XmlElement
    private String key;

    public SimpleProjectBean(final Project project) {
        this.id = project.getId();
        this.name = project.getName();
        this.key = project.getKey();
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getKey() {
        return key;
    }
}
