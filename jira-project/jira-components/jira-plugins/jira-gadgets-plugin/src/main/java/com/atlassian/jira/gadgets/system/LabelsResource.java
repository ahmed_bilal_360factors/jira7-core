package com.atlassian.jira.gadgets.system;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.customfields.CustomFieldUtils;
import com.atlassian.jira.issue.customfields.impl.LabelsCFType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.label.AlphabeticalLabelGroupingService;
import com.atlassian.jira.issue.label.AlphabeticalLabelGroupingSupport;
import com.atlassian.jira.issue.label.AlphabeticalLabelRenderer;
import com.atlassian.jira.issue.label.LabelUtil;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.Predicate;
import com.atlassian.jira.util.collect.CollectionUtil;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import static com.atlassian.gzipfilter.org.apache.commons.lang.StringEscapeUtils.escapeHtml;
import static com.atlassian.jira.issue.customfields.CustomFieldUtils.getCustomFieldId;
import static com.atlassian.jira.rest.v1.util.CacheControl.NO_CACHE;

/**
 * Labels Resource for the label gadget
 *
 * @since v4.2
 */
@Path("labels")
@AnonymousAllowed
@Produces(MediaType.APPLICATION_JSON)
public class LabelsResource {
    private static final Logger log = LoggerFactory.getLogger(LabelsResource.class);
    private static final String PROJECT_ID_PREFIX = "project-";

    private final CustomFieldManager customFieldManager;
    private final JiraAuthenticationContext authenticationContext;
    private AlphabeticalLabelRenderer alphabeticalLabelRenderer;
    private final FieldManager fieldManager;
    private final ProjectManager projectManager;
    private final LabelUtil labelUtil;
    private AlphabeticalLabelGroupingService alphabeticalLabelGroupingService;

    public LabelsResource(
            @ComponentImport final CustomFieldManager customFieldManager,
            @ComponentImport final JiraAuthenticationContext authenticationContext,
            @ComponentImport final AlphabeticalLabelRenderer alphabeticalLabelRenderer,
            @ComponentImport final ProjectManager projectManager,
            @ComponentImport final FieldManager fieldManager,
            @ComponentImport final LabelUtil labelUtil,
            @ComponentImport final AlphabeticalLabelGroupingService alphabeticalLabelGroupingService
    ) {
        this.customFieldManager = customFieldManager;
        this.authenticationContext = authenticationContext;
        this.alphabeticalLabelRenderer = alphabeticalLabelRenderer;
        this.projectManager = projectManager;
        this.fieldManager = fieldManager;
        this.labelUtil = labelUtil;
        this.alphabeticalLabelGroupingService = alphabeticalLabelGroupingService;
    }

    @GET
    @Path("gadget/fields")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLabelFields() {
        @SuppressWarnings("unchecked")
        final Collection<CustomField> labelCfTypes = CollectionUtil.filter(
                customFieldManager.getCustomFieldObjects(), new Predicate<CustomField>() {
                    public boolean evaluate(final CustomField input) {
                        return input.getCustomFieldType() instanceof LabelsCFType;
                    }
                }
        );

        final List<LabelField> labelFields = new ArrayList<LabelField>(labelCfTypes.size() + 1);
        labelFields.add(new LabelField(authenticationContext.getI18nHelper().getText("issue.field.labels"),
                IssueFieldConstants.LABELS));
        for (CustomField labelCf : labelCfTypes) {
            LabelField labelField = new LabelField(labelCf.getName(), labelCf.getId());
            labelFields.add(labelField);
        }

        return Response.ok(new LabelFields(labelFields)).cacheControl(NO_CACHE).build();
    }

    @GET
    @Path("gadget/configuration/validate")
    @Produces(MediaType.APPLICATION_JSON)
    public Response validateLabelGadgetConfiguration() {
        // No point validating -- all fields are select drop downs and the config page won't be shown if fields (or their options) go missing
        return Response.ok().cacheControl(NO_CACHE).build();
    }

    @GET
    @Path("gadget/{project}/{fieldId}/groups")
    @Produces(MediaType.TEXT_HTML)
    public Response getLabelGroups(@PathParam("project") String project, @PathParam("fieldId") String fieldId) {
        final Optional<Long> projectId = parseProjectId(project);
        if (!projectId.isPresent()) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Error parsing project id from '" + escapeHtml(project) + "'").cacheControl(NO_CACHE).build();
        }

        return Response.ok(alphabeticalLabelRenderer.getHtml(authenticationContext.getLoggedInUser(), projectId.get(), fieldId, true)).cacheControl(NO_CACHE).build();
    }

    @GET
    @Path("gadget/{project}/{fieldId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLabelsGroupsJSON(@PathParam("project") String project, @PathParam("fieldId") String fieldId) {
        final Optional<Long> projectId = parseProjectId(project);
        if (!projectId.isPresent()) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Error parsing project id from '" + escapeHtml(project) + "'").cacheControl(NO_CACHE).build();
        }

        final Optional<Field> field = Optional.ofNullable(fieldManager.getField(fieldId));
        final Optional<Project> projectObj = Optional.ofNullable(projectManager.getProjectObj(projectId.get()));
        if (!field.isPresent() || !projectObj.isPresent()) {
            return Response.status(Response.Status.NOT_FOUND).cacheControl(NO_CACHE).build();
        }

        final AlphabeticalLabelGroupingSupport alphaSupport =
                alphabeticalLabelGroupingService.getAlphabeticallyGroupedLabels(authenticationContext.getLoggedInUser(), projectId.get(), fieldId);
        final ApplicationUser user = authenticationContext.getLoggedInUser();
        final List<LabelGroup> groups = Lists.newArrayList();

        for (String key : alphaSupport.getKeys()) {
            final Set<String> labels = alphaSupport.getContents(key);
            List<LabelElement> elements = Lists.newArrayList();
            for (String label : labels) {
                final String issueSearchUrl;
                if (fieldId.startsWith(CustomFieldUtils.CUSTOM_FIELD_PREFIX)) {
                    issueSearchUrl = labelUtil.getLabelSearchPathForProject(user, projectId.get(), getCustomFieldId(fieldId), label);
                } else {
                    issueSearchUrl = labelUtil.getLabelSearchPathForProject(user, projectId.get(), label);
                }
                elements.add(new LabelElement(label, issueSearchUrl));
            }
            groups.add(new LabelGroup(key, elements));
        }

        final LabelGroupList labelGroups = new LabelGroupList(field.get().getName(), projectObj.get().getName(), groups);
        return Response.ok(labelGroups).cacheControl(NO_CACHE).build();
    }

    private Optional<Long> parseProjectId(final String project) {
        try {
            return Optional.of(Long.parseLong(StringUtils.substring(project, PROJECT_ID_PREFIX.length())));
        } catch (NumberFormatException e) {
            return Optional.empty();
        }
    }

    @XmlRootElement
    public static class LabelField {
        @XmlElement
        private String label;

        @XmlElement
        private String value;

        private LabelField() {
        }

        public LabelField(final String label, final String value) {
            this.label = label;
            this.value = value;
        }
    }

    @XmlRootElement
    public static class LabelFields {
        @XmlElement
        private final List<LabelField> labelFields = new ArrayList<LabelField>();

        private LabelFields() {
        }

        public LabelFields(final List<LabelField> labelFields) {
            this.labelFields.addAll(labelFields);
        }
    }

    @XmlRootElement
    public static class LabelElement {
        @XmlElement
        private final String label;

        @XmlElement
        private final String searchUrl;

        public LabelElement(final String label, final String searchUrl) {
            this.label = label;
            this.searchUrl = searchUrl;
        }

        public String getLabel() {
            return label;
        }

        public String getSearchUrl() {
            return searchUrl;
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            final LabelElement that = (LabelElement) o;
            return Objects.equals(label, that.label) &&
                    Objects.equals(searchUrl, that.searchUrl);
        }

        @Override
        public int hashCode() {
            return Objects.hash(label, searchUrl);
        }
    }

    @XmlRootElement
    public static class LabelGroup {
        @XmlElement
        private final String key;

        @XmlElement
        private final List<LabelElement> labels;

        public LabelGroup(final String key, final List<LabelElement> labels) {
            this.key = key;
            this.labels = labels;
        }

        public String getKey() {
            return key;
        }

        public List<LabelElement> getLabels() {
            return labels;
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            final LabelGroup that = (LabelGroup) o;
            return Objects.equals(key, that.key) &&
                    Objects.equals(labels, that.labels);
        }

        @Override
        public int hashCode() {
            return Objects.hash(key, labels);
        }
    }

    @XmlRootElement
    public static class LabelGroupList {
        @XmlElement
        private final List<LabelGroup> groups;

        @XmlElement
        private final String field;

        @XmlElement
        private final String project;

        public LabelGroupList(final String field, final String project, final List<LabelGroup> groups) {
            this.field = field;
            this.project = project;
            this.groups = groups;
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            final LabelGroupList that = (LabelGroupList) o;
            return Objects.equals(groups, that.groups) &&
                    Objects.equals(field, that.field) &&
                    Objects.equals(project, that.project);
        }

        @Override
        public int hashCode() {
            return Objects.hash(groups, field, project);
        }
    }
}
