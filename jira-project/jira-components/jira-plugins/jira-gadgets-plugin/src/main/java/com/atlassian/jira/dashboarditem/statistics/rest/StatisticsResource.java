package com.atlassian.jira.dashboarditem.statistics.rest;

import com.atlassian.fugue.Either;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.dashboarditem.statistics.service.statistics.StatisticsService;
import com.atlassian.jira.dashboarditem.statistics.service.statistics.beans.OneDimensionalStatisticsResultBean;
import com.atlassian.jira.dashboarditem.statistics.service.statistics.searcher.beans.StatisticsSearchResultBean;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Nullable;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Statistic searching REST resource.
 */
@Path("/statistics")
@AnonymousAllowed
public class StatisticsResource {
    private static final String SEARCH_JQL = "jql";
    private static final String STAT_TYPE = "statType";
    public static final String FILTER_ID = "filterId";

    private final StatisticsService statisticsService;
    private final JiraAuthenticationContext authenticationContext;
    private final SearchRequestService searchRequestService;
    private final SearchService searchService;

    public StatisticsResource(
            @ComponentImport final JiraAuthenticationContext authenticationContext,
            @ComponentImport final SearchRequestService searchRequestService,
            @ComponentImport final SearchService searchService,
            final StatisticsService statisticsService
    ) {
        this.statisticsService = statisticsService;
        this.authenticationContext = authenticationContext;
        this.searchRequestService = searchRequestService;
        this.searchService = searchService;
    }

    /**
     * Complete a one dimensional search on a jql search query or a a filter id. The request must provide `jql` or
     * `filterId`. If both are present, `filterId` takes precedence.
     *
     * @param jql      (optional) The jql to use for the query.
     * @param filterId (optional) The filterId to use for the query.
     * @param statType The statistic type to groups search results by.
     * @return HTTP response containing an error or the result of the query
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response oneDimensionalSearch(
            @QueryParam(SEARCH_JQL) @Nullable final String jql,
            @QueryParam(FILTER_ID) @Nullable final Long filterId,
            @QueryParam(STAT_TYPE) @Nullable final String statType) {
        Either<OneDimensionalStatisticsResultBean, ErrorCollection> result;

        ErrorCollection errors = validParameters(jql, filterId, statType);
        if (errors.hasAnyErrors()) {
            result = Either.right(errors);
        } else {
            if (filterId != null) {
                result = oneDimensionalSearchForFilter(filterId, statType);
            } else {
                result = oneDimensionalSearchWithJql(filterId, jql, statType);
            }
        }

        if (result.isRight()) {
            return createErrorResponse(result.right().get());
        } else {
            return Response.ok(result.left().get()).build();
        }
    }

    /**
     * Perform a one dimensional search for a filter with a certain id
     *
     * @param filterId The id of the filter whose results we want.
     * @param statType The statistic type to groups search results by.
     * @return Errors or the result of the jql query. An error will be returned if a filter with this `filterId` cannot
     * be found.
     */
    private Either<OneDimensionalStatisticsResultBean, ErrorCollection> oneDimensionalSearchForFilter(final Long filterId, final String statType) {
        final JiraServiceContextImpl jiraServiceContext = new JiraServiceContextImpl(authenticationContext.getLoggedInUser());
        final SearchRequest filter = searchRequestService.getFilter(jiraServiceContext, filterId);

        if (filter == null) {
            ErrorCollection errors = new SimpleErrorCollection();
            errors.addErrorMessage(authenticationContext.getI18nHelper().getText("rest.statistics.filter.not.found"));
            errors.addReason(ErrorCollection.Reason.NOT_FOUND);
            return Either.right(errors);
        }

        Either<OneDimensionalStatisticsResultBean, ErrorCollection> searchResult = oneDimensionalSearchWithJql(filterId, searchService.getJqlString(filter.getQuery()), statType);

        // Construct a new statistics result so that we can change the filter's name from 'JQL Query' to the actual filter name.
        if (searchResult.isLeft()) {
            final OneDimensionalStatisticsResultBean oldResult = searchResult.left().get();
            final StatisticsSearchResultBean newBean = new StatisticsSearchResultBean(oldResult.getIssueCount(), oldResult.getResults());
            final OneDimensionalStatisticsResultBean newResult = new OneDimensionalStatisticsResultBean(filter.getName(), oldResult.getFilterUrl(), oldResult.getStatType(), newBean);
            return Either.left(newResult);
        }

        return searchResult;
    }

    /**
     * Perform a one dimensional search for any jql query and return the result
     *
     * @param jql      The jql to use in the search.
     * @param statType The statistic type to groups search results by.
     * @return Errors or the result of the jql query
     */
    private Either<OneDimensionalStatisticsResultBean, ErrorCollection> oneDimensionalSearchWithJql(final Long filterId, final String jql, final String statType) {
        return statisticsService.aggregateOneDimensionalStats(jql, filterId, statType);
    }

    /**
     * Validate jql, filterId and statType so they are not blank or missing.
     *
     * @param jql      parameter to check
     * @param filterId parameter to check
     * @param statType parameter to check
     * @return any errors in parameter format
     */
    private ErrorCollection validParameters(@Nullable final String jql, @Nullable Long filterId, @Nullable final String statType) {
        ErrorCollection errors = new SimpleErrorCollection();
        if (jql == null && filterId == null) {
            errors.addErrorMessage(authenticationContext.getI18nHelper().getText("rest.missing.field", SEARCH_JQL));
            errors.addErrorMessage(authenticationContext.getI18nHelper().getText("rest.missing.field", FILTER_ID));
            errors.addReason(ErrorCollection.Reason.VALIDATION_FAILED);
        }

        if (StringUtils.isBlank(statType)) {
            errors.addErrorMessage(authenticationContext.getI18nHelper().getText("rest.missing.field", STAT_TYPE));
            errors.addReason(ErrorCollection.Reason.VALIDATION_FAILED);
        }
        return errors;
    }

    /**
     * Generate a generic error response given errors.
     *
     * @param errors to generate response with
     * @return http response.
     */
    private Response createErrorResponse(final ErrorCollection errors) {
        ErrorCollection.Reason worstReason = ErrorCollection.Reason.getWorstReason(errors.getReasons());
        int errorCode = 500;
        if (worstReason != null) {
            errorCode = worstReason.getHttpStatusCode();
        }
        return createErrorResponse(errors, errorCode);
    }

    /**
     * Generate an error response given errors and an error code.
     *
     * @param errors    to generate response with
     * @param errorCode of the http response
     * @return http response.
     */
    private Response createErrorResponse(final ErrorCollection errors, int errorCode) {
        return Response.status(errorCode).entity(errors.getErrorMessages()).build();
    }
}