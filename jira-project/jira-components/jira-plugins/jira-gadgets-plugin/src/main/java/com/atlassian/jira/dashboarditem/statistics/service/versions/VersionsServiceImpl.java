package com.atlassian.jira.dashboarditem.statistics.service.versions;

import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.dashboarditem.statistics.service.versions.beans.ProjectVersionBean;
import com.atlassian.jira.dashboarditem.statistics.util.ProjectOrFilterQueryParser;
import com.atlassian.jira.dashboarditem.statistics.util.QueryParser;
import com.atlassian.jira.dashboarditem.statistics.util.QueryUrlSupplier;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.search.managers.SearchHandlerManager;
import com.atlassian.jira.issue.search.searchers.IssueSearcher;
import com.atlassian.jira.issue.search.searchers.transformer.ProjectSearchInputTransformer;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.query.Query;
import com.google.common.base.Function;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * @since v6.4
 */
@Component
public class VersionsServiceImpl implements VersionsService {
    private final VersionManager versionManager;
    private final SearchHandlerManager searchHandlerManager;
    private final ProjectService projectService;
    private final JiraAuthenticationContext authenticationContext;
    private final QueryParser queryParser;
    private final ProjectOrFilterQueryParser projectOrFilterQueryParser;
    private final QueryUrlSupplier queryUrlSupplier;


    @Autowired
    public VersionsServiceImpl(
            @ComponentImport final VersionManager versionManager,
            @ComponentImport final SearchHandlerManager searchHandlerManager,
            @ComponentImport final ProjectService projectService,
            @ComponentImport final JiraAuthenticationContext authenticationContext,
            final QueryParser queryParser,
            final ProjectOrFilterQueryParser projectOrFilterQueryParser,
            final QueryUrlSupplier queryUrlSupplier
    ) {
        this.versionManager = versionManager;
        this.searchHandlerManager = searchHandlerManager;
        this.projectService = projectService;
        this.queryParser = queryParser;
        this.authenticationContext = authenticationContext;
        this.projectOrFilterQueryParser = projectOrFilterQueryParser;
        this.queryUrlSupplier = queryUrlSupplier;

    }

    @Override
    public Either<List<ProjectVersionBean>, ErrorCollection> getVersions(@Nonnull final String jql) {
        ApplicationUser user = authenticationContext.getUser();
        Either<Query, ErrorCollection> queryOrError = queryParser.getQuery(user, jql);

        if (queryOrError.isRight()) {
            return Either.right(queryOrError.right().get());
        }

        Query query = queryOrError.left().get();

        Option<Query> filterQuery = projectOrFilterQueryParser.getFilterQuery(user, query);
        if (filterQuery.isDefined()) {
            query = filterQuery.get();
        }

        Collection<Project> projects = getProjectsForQuery(user, query);

        List<ProjectVersionBean> projectVersionBeanList = getVersionsForProjects(query, projects);

        return Either.left(projectVersionBeanList);
    }


    /**
     * Given the set of projects return all the versions that are released or not for them.
     *
     * @param projects to get versions for
     * @return list of version beans
     */
    private List<ProjectVersionBean> getVersionsForProjects(final Query query, final Collection<Project> projects) {
        List<ProjectVersionBean> projectVersionBeanList = new ArrayList<ProjectVersionBean>();
        for (final Project project : projects) {
            List<Version> versions = versionManager.getVersions(project.getId(), false);
            for (final Version version : versions) {
                final String url = queryUrlSupplier.getUrlForQueryWithVersion(query, version);
                projectVersionBeanList.add(new ProjectVersionBean(version, project, url));
            }
        }

        return projectVersionBeanList;
    }


    /**
     * Given a query collect all of the projects that it encompasses
     *
     * @param user  completing the search
     * @param query to be completed
     * @return collection of projects that have issues in this query.
     */
    private Collection<Project> getProjectsForQuery(final ApplicationUser user, final Query query) {
        final IssueSearcher projectSearcher = searchHandlerManager.getSearcher(IssueFieldConstants.PROJECT);
        final ProjectSearchInputTransformer searchInputTransformer = (ProjectSearchInputTransformer) projectSearcher.getSearchInputTransformer();
        final Set<String> projectIds = searchInputTransformer.getIdValuesAsStrings(user, query);

        //Get the projects from the IDs
        final Iterable<Project> projects = Iterables.transform(projectIds, new Function<String, Project>() {
            @Override
            public Project apply(@Nullable final String input) {
                Long id = new Long(input);
                ProjectService.GetProjectResult projectResult = projectService.getProjectById(user, id);
                if (projectResult.isValid()) {
                    return projectResult.getProject();
                }
                return null;
            }
        });

        //Remove any nulls and return
        return Lists.newArrayList(Iterables.filter(projects, Predicates.notNull()));
    }
}
