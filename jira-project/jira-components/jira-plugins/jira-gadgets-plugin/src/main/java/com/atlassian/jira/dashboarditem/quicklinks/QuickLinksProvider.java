package com.atlassian.jira.dashboarditem.quicklinks;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.jql.builder.JqlClauseBuilder;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;

import static com.atlassian.jira.permission.ProjectPermissions.ADMINISTER_PROJECTS;
import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static com.atlassian.jira.permission.ProjectPermissions.CREATE_ISSUES;

/**
 * Provides access to a list of commonly used links
 */
@Component
public class QuickLinksProvider {
    private static final String ISSUE_NAVIGATOR_URL = "/secure/IssueNavigator.jspa";
    private static final String ISSUE_NAVIGATOR_URL_WITH_RESET = ISSUE_NAVIGATOR_URL + "?reset=true";
    private static final String ISSUE_NAVIGATOR_URL_WITH_SHOW = ISSUE_NAVIGATOR_URL + "?mode=show";
    private static final String ISSUE_NAVIGATOR_URL_WITH_RESET_AND_HIDE = ISSUE_NAVIGATOR_URL_WITH_RESET + "&hide=true";
    private static final String BROWSE_PROJECTS_URL = "/secure/BrowseProjects.jspa";
    private static final String CREATE_ISSUE_URL = "/secure/CreateIssue!default.jspa";
    private static final String VIEW_PROJECTS_URL = "/secure/project/ViewProjects.jspa";
    private final JiraAuthenticationContext authenticationContext;
    private final GlobalPermissionManager globalPermissionManager;
    private final PermissionManager permissionManager;
    private final ApplicationProperties applicationProperties;
    private final SearchService searchService;

    @Autowired
    public QuickLinksProvider(
            @ComponentImport final JiraAuthenticationContext authenticationContext,
            @ComponentImport final GlobalPermissionManager globalPermissionManager,
            @ComponentImport final PermissionManager permissionManager,
            @ComponentImport final ApplicationProperties applicationProperties,
            @ComponentImport final SearchService searchService
    ) {
        this.authenticationContext = authenticationContext;
        this.globalPermissionManager = globalPermissionManager;
        this.permissionManager = permissionManager;
        this.applicationProperties = applicationProperties;
        this.searchService = searchService;
    }

    public LinkLists getLinks() {
        final Collection<Link> commons = new ArrayList<Link>();
        final Collection<Link> navigations = new ArrayList<Link>();

        final ApplicationUser user = authenticationContext.getLoggedInUser();

        if (permissionManager.hasProjects(BROWSE_PROJECTS, user)) {
            navigations.add(new Link("gadget.quicklinks.browse.projects", BROWSE_PROJECTS_URL, "gadget.quicklinks.browse.projects.title"));
            navigations.add(new Link("gadget.quicklinks.find.issues", ISSUE_NAVIGATOR_URL_WITH_SHOW, "gadget.quicklinks.find.issues.title"));
        }

        if (permissionManager.hasProjects(CREATE_ISSUES, user)) {
            navigations.add(new Link("gadget.quicklinks.create.issue", CREATE_ISSUE_URL, "gadget.quicklinks.create.issue.title"));
        }

        if (globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user) &&
                permissionManager.hasProjects(ADMINISTER_PROJECTS, user)) {
            navigations.add(new Link("gadget.quicklinks.adminstration", VIEW_PROJECTS_URL, "gadget.quicklinks.administration.title"));
        }

        if (user != null) {
            final Query query = JqlQueryBuilder.newBuilder().where().unresolved().and().reporterIsCurrentUser().buildQuery();
            final String reportedIssuesURL = ISSUE_NAVIGATOR_URL_WITH_RESET_AND_HIDE + searchService.getQueryString(user, query);
            commons.add(new Link("gadget.quicklinks.reported.issues", reportedIssuesURL, "gadget.quicklinks.reported.issues.title"));

            if (applicationProperties.getOption("jira.option.voting")) {
                JqlClauseBuilder builder = JqlQueryBuilder.newClauseBuilder().issueInVotedIssues();

                final String jql = searchService.getQueryString(user, builder.buildQuery());
                final String url = ISSUE_NAVIGATOR_URL_WITH_RESET + jql;

                commons.add(new Link("gadget.quicklinks.voted.issues", url, "gadget.quicklinks.voted.issues.title"));
            }
            if (applicationProperties.getOption("jira.option.watching")) {
                JqlClauseBuilder builder = JqlQueryBuilder.newClauseBuilder().issueInWatchedIssues();

                final String jql = searchService.getQueryString(user, builder.buildQuery());
                final String url = ISSUE_NAVIGATOR_URL_WITH_RESET + jql;
                commons.add(new Link("gadget.quicklinks.watched.issues", url, "gadget.quicklinks.watched.issues.title"));
            }
        }

        return new LinkLists(navigations, commons);
    }
}
