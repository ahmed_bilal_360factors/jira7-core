package com.atlassian.jira.dashboarditem.statistics.service.statistics;

import com.atlassian.fugue.Either;
import com.atlassian.gzipfilter.org.apache.commons.lang.StringUtils;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.dashboarditem.statistics.service.statistics.beans.OneDimensionalStatisticsResultBean;
import com.atlassian.jira.dashboarditem.statistics.service.statistics.searcher.StatisticsSearcher;
import com.atlassian.jira.dashboarditem.statistics.service.statistics.searcher.beans.StatisticsSearchResultBean;
import com.atlassian.jira.dashboarditem.statistics.util.ProjectOrFilterQueryParser;
import com.atlassian.jira.dashboarditem.statistics.util.QueryParser;
import com.atlassian.jira.gadgets.system.StatisticTypesProvider;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * @since v6.4
 */
@Component
public class StatisticsServiceImpl implements StatisticsService {
    private static final String SEARCH_JQL = "jql";
    private static final String STAT_TYPE = "statType";
    private static final String FILTER_ID = "filterId";

    private final JiraAuthenticationContext authenticationContext;
    private final StatisticsSearcher statisticsSearcher;
    private final StatisticTypesProvider statisticTypesProvider;
    private final QueryParser queryParser;
    private final ProjectOrFilterQueryParser projectOrFilterQueryParser;
    private final SearchRequestService searchRequestService;

    @Autowired
    public StatisticsServiceImpl(
            @ComponentImport final JiraAuthenticationContext authenticationContext,
            @ComponentImport final SearchRequestService searchRequestService,
            final StatisticsSearcher statisticsSearcher,
            final StatisticTypesProvider statisticTypesProvider,
            final QueryParser queryParser,
            final ProjectOrFilterQueryParser projectOrFilterQueryParser) {
        this.authenticationContext = authenticationContext;
        this.statisticsSearcher = statisticsSearcher;
        this.statisticTypesProvider = statisticTypesProvider;
        this.queryParser = queryParser;
        this.projectOrFilterQueryParser = projectOrFilterQueryParser;
        this.searchRequestService = searchRequestService;
    }

    @Override
    public Either<OneDimensionalStatisticsResultBean, ErrorCollection> aggregateOneDimensionalStats(
            final String jql,
            final Long filterId,
            @Nonnull final String statType
    ) {
        final ApplicationUser user = authenticationContext.getLoggedInUser();
        ErrorCollection errors = validParameters(jql, filterId, statType);

        if (errors.hasAnyErrors()) {
            return Either.right(errors);
        } else {
            final String jqlQueryString = (filterId != null) ? jqlSearchForFilter(filterId) : jql;

            if (jqlQueryString == null) {
                final ErrorCollection queryStringErrors = new SimpleErrorCollection();
                queryStringErrors.addErrorMessage(authenticationContext.getI18nHelper().getText("rest.error.generating.response", FILTER_ID));
                queryStringErrors.addReason(ErrorCollection.Reason.NOT_FOUND);
                return Either.right(queryStringErrors);
            }

            Either<Query, ErrorCollection> queryOrError = queryParser.getQuery(user, jqlQueryString);

            if (queryOrError.isRight()) {
                return Either.right(queryOrError.right().get());
            }

            final Query jqlQuery = queryOrError.left().get();
            Either<ErrorCollection, String> statOrError = getStatDisplayName(statType);
            if (statOrError.isLeft()) {
                return Either.right(statOrError.left().get());
            }

            final String statTypeDisplayName = statOrError.right().get();

            StatisticsSearchResultBean statBean;
            try {
                statBean = statisticsSearcher.completeOneDimensionalSearch(user, jqlQuery, statType);
            } catch (SearchException e) {
                ErrorCollection searchErrors = new SimpleErrorCollection();
                searchErrors.addErrorMessage(e.getMessage());
                searchErrors.addReason(ErrorCollection.Reason.SERVER_ERROR);
                return Either.right(searchErrors);
            }

            return Either.left(getStatisticsResult(user, statTypeDisplayName, jqlQuery, statBean));
        }
    }


    /**
     * Get the statistic display name for a statistic.
     *
     * @param statType name for the statistic
     * @return either an error or the display name of the stat
     */
    @Nonnull
    private Either<ErrorCollection, String> getStatDisplayName(@Nonnull final String statType) {

        final ErrorCollection errors = new SimpleErrorCollection();
        final String statTypeDisplayName = statisticTypesProvider.getDisplayName(statType);
        if (StringUtils.isBlank(statTypeDisplayName)) {
            errors.addErrorMessage(authenticationContext.getI18nHelper().getText("gadget.common.invalid.stat.type", statType));
            errors.addReason(ErrorCollection.Reason.VALIDATION_FAILED);
            return Either.left(errors);
        }

        return Either.right(statTypeDisplayName);
    }

    /**
     * Generates statistics results from the search completed.
     *
     * @param user                completing the search
     * @param statTypeDisplayName being grouped by
     * @param jqlQuery            that was searched
     * @param statBean            containing the search results
     * @return statistics for the search
     */
    private OneDimensionalStatisticsResultBean getStatisticsResult(final ApplicationUser user, final String statTypeDisplayName, final Query jqlQuery, final StatisticsSearchResultBean statBean) {
        final ProjectOrFilterQueryParser.QueryInformation queryInformation = projectOrFilterQueryParser.getQueryInformation(user, jqlQuery);
        String filterName = queryInformation.getName();
        String filterUrl = queryInformation.getUrl();

        return new OneDimensionalStatisticsResultBean(filterName, filterUrl, statTypeDisplayName, statBean);
    }

    /**
     * Perform a one dimensional search for a filter with a certain id
     *
     * @param filterId The id of the filter whose results we want.
     * @return A String containing the JQL to do the filter search
     */
    private String jqlSearchForFilter(final Long filterId) {
        final JiraServiceContextImpl jiraServiceContext = new JiraServiceContextImpl(authenticationContext.getLoggedInUser());
        SearchRequest filter = searchRequestService.getFilter(jiraServiceContext, filterId);

        if (filter == null) {
            return null;
        }

        return filter.getQuery().getQueryString();
    }

    /**
     * Validate jql, filterId and statType so they are not blank or missing.
     *
     * @param jql      parameter to check
     * @param filterId parameter to check
     * @param statType parameter to check
     * @return any errors in parameter format
     */
    private ErrorCollection validParameters(@Nullable final String jql, @Nullable Long filterId, @Nullable final String statType) {
        ErrorCollection errors = new SimpleErrorCollection();
        if (jql == null && filterId == null) {
            errors.addErrorMessage(authenticationContext.getI18nHelper().getText("rest.missing.field", SEARCH_JQL));
            errors.addErrorMessage(authenticationContext.getI18nHelper().getText("rest.missing.field", FILTER_ID));
            errors.addReason(ErrorCollection.Reason.VALIDATION_FAILED);
        }

        if (com.atlassian.gzipfilter.org.apache.commons.lang.StringUtils.isBlank(statType)) {
            errors.addErrorMessage(authenticationContext.getI18nHelper().getText("rest.missing.field", STAT_TYPE));
            errors.addReason(ErrorCollection.Reason.VALIDATION_FAILED);
        }
        return errors;
    }
}
