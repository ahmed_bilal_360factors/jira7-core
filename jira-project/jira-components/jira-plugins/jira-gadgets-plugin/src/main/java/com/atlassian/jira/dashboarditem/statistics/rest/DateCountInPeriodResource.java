package com.atlassian.jira.dashboarditem.statistics.rest;

import com.atlassian.fugue.Either;
import com.atlassian.jira.dashboarditem.statistics.service.datecount.DateCountInPeriodService;
import com.atlassian.jira.dashboarditem.statistics.service.datecount.DateCountParametersBuilder;
import com.atlassian.jira.dashboarditem.statistics.service.datecount.beans.DateCountInPeriodResultBean;
import com.atlassian.jira.dashboarditem.statistics.service.datecount.field.Field;
import com.atlassian.jira.dashboarditem.statistics.service.datecount.operation.Operation;
import com.atlassian.jira.dashboarditem.statistics.service.datecount.period.TimePeriod;
import com.atlassian.jira.rest.util.ResponseFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;

import javax.annotation.Nullable;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@Path("/dateCountInPeriod")
@AnonymousAllowed
public class DateCountInPeriodResource {
    static final String SEARCH_JQL = "jql";
    static final String FIELD = "field";
    static final String PERIOD = "period";
    static final String DAYS_PREVIOUS = "daysprevious";
    static final String OPERATION = "operation";
    static final String INCLUDE_VERSIONS = "includeVersions";

    private final DateCountInPeriodService dateCountInPeriodService;
    private final JiraAuthenticationContext authenticationContext;
    private final ResponseFactory responseFactory;

    public DateCountInPeriodResource(
            final DateCountInPeriodService dateCountInPeriodService,
            @ComponentImport final JiraAuthenticationContext authenticationContext,
            @ComponentImport final ResponseFactory responseFactory) {
        this.dateCountInPeriodService = dateCountInPeriodService;
        this.authenticationContext = authenticationContext;
        this.responseFactory = responseFactory;
    }

    /**
     * @param jql           to search
     * @param fieldNames    is a list of strings for the fields to group
     * @param periodName    is the name of the period to group by
     * @param daysPrevious  is the number of days to max search backwards
     * @param operationName name of the operation from grouping the issues.
     * @return http response.
     * @response.representation.200.qname DateCountInPeriodResult
     * {@link com.atlassian.jira.dashboarditem.statistics.service.datecount.beans.DateCountInPeriodResultBean}
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc For a given JQL it returns all issues count grouping them by the given period.
     * @response.representation.400 Bad request in that the jql, field, period, daysprevious or operation were not valid.
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response completeCreatedVsResolved(
            @QueryParam(SEARCH_JQL) final String jql,
            @QueryParam(FIELD) final List<String> fieldNames,
            @QueryParam(PERIOD) final String periodName,
            @QueryParam(DAYS_PREVIOUS) final Integer daysPrevious,
            @QueryParam(INCLUDE_VERSIONS) final boolean includeVersions,
            @QueryParam(OPERATION) final String operationName) {
        ErrorCollection errors = validateParameters(fieldNames, periodName, operationName);
        if (errors.hasAnyErrors()) {
            return responseFactory.generateFieldErrorResponse(errors);
        }

        final Set<Field> fields = getFieldSet(fieldNames);
        final TimePeriod period = TimePeriod.valueOf(periodName);
        final Operation operation = Operation.valueOf(operationName);

        final DateCountParametersBuilder builder = new DateCountParametersBuilder()
                .jql(jql)
                .fields(fields)
                .period(period)
                .daysPrevious(daysPrevious)
                .operation(operation)
                .includeVersions(includeVersions);

        final Either<DateCountInPeriodResultBean, ErrorCollection> resultOrError = dateCountInPeriodService.collectData(builder.build());
        if (resultOrError.isRight()) {
            return responseFactory.generateErrorResponse(resultOrError.right().get());
        }

        return Response.ok(resultOrError.left().get()).build();
    }

    private ErrorCollection validateParameters(
            @Nullable final Collection<String> fields,
            @Nullable final String period,
            @Nullable final String operation) {
        final I18nHelper i18nHelper = authenticationContext.getI18nHelper();
        ErrorCollection errors = new SimpleErrorCollection();
        if (fields == null || fields.size() == 0) {
            addValidationError(errors, FIELD, i18nHelper.getText("rest.missing.field", FIELD));
        } else {
            for (final String field : fields) {
                try {
                    Field.valueOf(field);
                } catch (IllegalArgumentException ex) {
                    addValidationError(errors, FIELD, i18nHelper.getText("rest.invalid.field.valid.fields", Arrays.toString(Field.values())));
                }
            }
        }

        if (period == null) {
            addValidationError(errors, PERIOD, i18nHelper.getText("rest.missing.field", PERIOD));
        } else {
            try {
                TimePeriod.valueOf(period);
            } catch (IllegalArgumentException ex) {

                addValidationError(errors, PERIOD, i18nHelper.getText("rest.invalid.field.valid.fields", Arrays.toString(TimePeriod.values())));
            }
        }

        if (operation == null) {
            addValidationError(errors, OPERATION, i18nHelper.getText("rest.missing.field", OPERATION));
        } else {
            try {
                Operation.valueOf(operation);
            } catch (IllegalArgumentException ex) {
                addValidationError(errors, OPERATION, i18nHelper.getText("rest.invalid.field.valid.fields", Arrays.toString(Operation.values())));
            }

        }

        return errors;
    }

    private void addValidationError(final ErrorCollection errors, final String field, final String message) {
        errors.addError(field, message, ErrorCollection.Reason.VALIDATION_FAILED);
    }

    private Set<Field> getFieldSet(final Collection<String> fieldNames) {
        return Sets.immutableEnumSet(Iterables.transform(fieldNames, new Function<String, Field>() {
            @Override
            public Field apply(@Nullable final String input) {
                return Field.valueOf(input);
            }
        }));
    }

}
