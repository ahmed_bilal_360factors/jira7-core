package com.atlassian.jira.dashboarditem.statistics.service.datecount;

import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.atlassian.jira.charts.util.DataUtils;
import com.atlassian.jira.dashboarditem.statistics.service.datecount.beans.DateCountInPeriodResultBean;
import com.atlassian.jira.dashboarditem.statistics.service.datecount.beans.FieldResultBean;
import com.atlassian.jira.dashboarditem.statistics.service.datecount.beans.TimePeriodResultBean;
import com.atlassian.jira.dashboarditem.statistics.service.datecount.field.Field;
import com.atlassian.jira.dashboarditem.statistics.service.datecount.operation.Operation;
import com.atlassian.jira.dashboarditem.statistics.service.datecount.period.TimePeriod;
import com.atlassian.jira.dashboarditem.statistics.service.versions.VersionsService;
import com.atlassian.jira.dashboarditem.statistics.service.versions.beans.ProjectVersionBean;
import com.atlassian.jira.dashboarditem.statistics.util.ProjectOrFilterQueryParser;
import com.atlassian.jira.dashboarditem.statistics.util.QueryParser;
import com.atlassian.jira.dashboarditem.statistics.util.QueryUrlSupplier;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.constants.SystemSearchConstants;
import com.atlassian.jira.issue.statistics.DatePeriodStatisticsMapper;
import com.atlassian.jira.issue.statistics.StatisticsMapper;
import com.atlassian.jira.issue.statistics.util.OneDimensionalObjectHitCollector;
import com.atlassian.jira.jql.ClauseInformation;
import com.atlassian.jira.jql.builder.JqlClauseBuilder;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.query.Query;
import com.atlassian.query.operator.Operator;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Lists;
import org.apache.lucene.search.Collector;
import org.jfree.data.time.RegularTimePeriod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeMap;

import static com.atlassian.fugue.Option.option;


/**
 * @since v6.4
 */
@Component
public class DateCountInPeriodServiceImpl implements DateCountInPeriodService {
    private final QueryParser queryParser;
    private final QueryUrlSupplier queryUrlSupplier;
    private final ProjectOrFilterQueryParser projectOrFilterQueryParser;
    private final VersionsService versionsService;
    private final SearchProvider searchProvider;
    private final TimeZoneManager timeZoneManager;
    private final JiraAuthenticationContext authenticationContext;

    @Autowired
    public DateCountInPeriodServiceImpl(
            final QueryParser queryParser,
            final QueryUrlSupplier queryUrlSupplier,
            final ProjectOrFilterQueryParser projectOrFilterQueryParser,
            final VersionsService versionsService,
            @ComponentImport final SearchProvider searchProvider,
            @ComponentImport final TimeZoneManager timeZoneManager,
            @ComponentImport final JiraAuthenticationContext authenticationContext
    ) {
        this.queryParser = queryParser;
        this.queryUrlSupplier = queryUrlSupplier;
        this.projectOrFilterQueryParser = projectOrFilterQueryParser;
        this.versionsService = versionsService;
        this.searchProvider = searchProvider;
        this.timeZoneManager = timeZoneManager;
        this.authenticationContext = authenticationContext;
    }


    @Override
    public Either<DateCountInPeriodResultBean, ErrorCollection> collectData(@Nonnull final RequestParameters params) {
        ErrorCollection validationErrors = validateParameters(params.getJql(), params.getFields(), params.getPeriod(), params.getDaysPrevious(), params.getOperation());
        if (validationErrors.hasAnyErrors()) {
            return Either.right(validationErrors);
        }

        final ApplicationUser user = authenticationContext.getUser();
        final Either<Query, ErrorCollection> queryOrError = queryParser.getQuery(user, params.getJql());

        if (queryOrError.isRight()) {
            return Either.right(queryOrError.right().get());
        }

        final Query query = queryOrError.left().get();


        final Map<Field, Map<RegularTimePeriod, Number>> dataMaps = new HashMap<Field, Map<RegularTimePeriod, Number>>();
        for (final Field field : params.getFields()) {
            if (field.getDocumentConstant() != null) {
                try {
                    dataMaps.put(field, getIssuesForField(field, query, params.getDaysPrevious(), user, params.getPeriod()));
                } catch (SearchException ex) {
                    ErrorCollection errors = new SimpleErrorCollection();
                    errors.addErrorMessage(ex.getMessage(), ErrorCollection.Reason.SERVER_ERROR);
                    return Either.right(errors);
                }
            }
        }

        normaliseDataMaps(dataMaps, params.getDaysPrevious(), params.getPeriod());


        if (params.getFields().contains(Field.unresolvedTrend) && params.getFields().contains(Field.created) && params.getFields().contains(Field.resolved)) {
            final Map<RegularTimePeriod, Number> unresolvedTrendDataMap = new TreeMap<RegularTimePeriod, Number>();
            final Map<RegularTimePeriod, Number> createdDataMap = dataMaps.get(Field.created);
            final Map<RegularTimePeriod, Number> resolvedDataMap = dataMaps.get(Field.resolved);
            int unresolvedTrend = 0;
            for (RegularTimePeriod key : createdDataMap.keySet()) {
                Integer created = (Integer) createdDataMap.get(key);
                Integer resolved = (Integer) resolvedDataMap.get(key);

                unresolvedTrend = unresolvedTrend + created - resolved;
                unresolvedTrendDataMap.put(key, unresolvedTrend);
            }
            dataMaps.put(Field.unresolvedTrend, unresolvedTrendDataMap);
        }

        List<ProjectVersionBean> versions = Lists.newArrayList();
        if (params.isIncludeVersions()) {
            final Either<List<ProjectVersionBean>, ErrorCollection> versionResult = versionsService.getVersions(params.getJql());
            if (versionResult.isRight()) {
                return Either.right(versionResult.right().get());
            }
            versions = versionResult.left().get();
        }

        return Either.left(generateResult(user, query, params.getDaysPrevious(), params.getOperation(), dataMaps, versions));
    }


    /**
     * Normalise the data so that there is a value for every period for each datamap
     *
     * @param dataMaps     that needs to be normalised
     * @param daysPrevious days to go back and normalise
     * @param period       for what the dates are being grouped in.
     */
    private void normaliseDataMaps(final Map<Field, Map<RegularTimePeriod, Number>> dataMaps, final Integer daysPrevious, final TimePeriod period) {
        if (dataMaps.size() == 0) {
            return;
        }

        final Field firstField = dataMaps.keySet().iterator().next();
        DataUtils.normaliseDateRangeCount(dataMaps.get(firstField), daysPrevious - 1, period.getTimePeriodClass(), timeZoneManager.getLoggedInUserTimeZone()); // only need to do one map as normalising both map keys will fix second

        for (final Field field : dataMaps.keySet()) {
            if (field.equals(firstField)) {
                continue;
            }
            DataUtils.normaliseMapKeys(dataMaps.get(firstField), dataMaps.get(field));
        }
    }

    private Map<RegularTimePeriod, Number> getIssuesForField(
            @Nonnull final Field field,
            @Nonnull final Query query,
            @Nonnull final Integer daysPrevious,
            @Nullable final ApplicationUser user,
            @Nonnull final TimePeriod period) throws SearchException {
        final Query timeBoundedQuery = buildTimeBoundedQuery(field, query, daysPrevious);

        final TimeZone timeZone = timeZoneManager.getLoggedInUserTimeZone();
        final StatisticsMapper fieldMapper = new DatePeriodStatisticsMapper(period.getTimePeriodClass(), field.getDocumentConstant(), timeZone);
        final Map<RegularTimePeriod, Number> result = new TreeMap<RegularTimePeriod, Number>();
        Collector hitCollector = new OneDimensionalObjectHitCollector(fieldMapper, result, true);
        searchProvider.search(timeBoundedQuery, user, hitCollector);
        return result;
    }

    @VisibleForTesting
    Query buildTimeBoundedQuery(final @Nonnull Field field, final @Nonnull Query query, final @Nonnull Integer daysPrevious) {
        final JqlQueryBuilder jqlQueryBuilder = JqlQueryBuilder.newBuilder(query);
        final JqlClauseBuilder whereClauseBuilder = jqlQueryBuilder.where().defaultAnd();

        final Option<ClauseInformation> systemClauseInfo = option(SystemSearchConstants.getClauseInformationById(field.getDocumentConstant()));
        final Option<String> primaryName = option(systemClauseInfo.isDefined() ? systemClauseInfo.get().getJqlClauseNames().getPrimaryName() : field.getDocumentConstant());
        if (primaryName.isDefined()) {
            whereClauseBuilder.addStringCondition(primaryName.get(), Operator.GREATER_THAN_EQUALS, "-" + daysPrevious + "d");
        }

        return whereClauseBuilder.buildQuery();
    }

    /**
     * Convert the collected data to the format we want to return
     *
     * @param query        that the issues were collected from
     * @param daysPrevious how far back the query can go
     * @param isCumulative whether to group the count cumulatively
     * @param dataMaps     from the search, mapping time period to the count/
     * @return list of time period results
     */
    private List<TimePeriodResultBean> createTimePeriodResults(
            final Query query,
            final Integer daysPrevious,
            final Boolean isCumulative,
            final Map<Field, Map<RegularTimePeriod, Number>> dataMaps
    ) {
        //Get all of the time periods
        Set<RegularTimePeriod> timePeriods = getTimePeriodsKeySet(dataMaps);

        List<TimePeriodResultBean> timePeriodResultBeans = new ArrayList<TimePeriodResultBean>();
        for (final RegularTimePeriod timePeriod : timePeriods) {
            Map<String, FieldResultBean> data = new HashMap<String, FieldResultBean>();

            for (final Field field : dataMaps.keySet()) {
                final Integer value = (Integer) dataMaps.get(field).get(timePeriod);

                String url = null;
                //If can generate a modified query for this field, generate the modified queries url.
                if (field.getDocumentConstant() != null) {
                    final Query timePeriodQueryForField = getTimePeriodQueryForField(query, daysPrevious, isCumulative, timePeriod, field);
                    url = queryUrlSupplier.getUrlForQuery(timePeriodQueryForField);
                }


                data.put(field.toString(), new FieldResultBean(value, url));
            }
            timePeriodResultBeans.add(new TimePeriodResultBean(timePeriod.getStart(), timePeriod.getEnd(), data));
        }

        return timePeriodResultBeans;
    }

    /**
     * Given a time period and field generate a modified query to search for these related issues
     *
     * @param query        being used
     * @param daysPrevious maximum days back to look
     * @param isCumulative whether the query include a cumulative date range
     * @param timePeriod   to search within
     * @param field        to make search for
     * @return modified query
     */
    private Query getTimePeriodQueryForField(
            final Query query,
            final Integer daysPrevious,
            final Boolean isCumulative,
            final RegularTimePeriod timePeriod,
            final Field field) {
        //Get a modified query for matching this field and timeperiod
        JqlQueryBuilder jqlQueryBuilder = JqlQueryBuilder.newBuilder(query);
        JqlClauseBuilder whereClauseBuilder = jqlQueryBuilder.where().defaultAnd();
        whereClauseBuilder.addStringCondition(field.getDocumentConstant(), Operator.GREATER_THAN_EQUALS, "-" + daysPrevious + "d");

        //Want in the format < 13:00 not <= 12:59
        Date lastDate = timePeriod.getEnd();
        lastDate.setTime(lastDate.getTime() + 1);

        if (!isCumulative) {
            whereClauseBuilder.addDateCondition(field.getDocumentConstant(), Operator.GREATER_THAN_EQUALS, timePeriod.getStart());
        }

        whereClauseBuilder.addDateCondition(field.getDocumentConstant(), Operator.LESS_THAN, lastDate);

        return whereClauseBuilder.buildQuery();
    }


    /**
     * For the dataset return the time periods. Note: assumes that the time period for each are the same, therefore
     * getting the first is good enough.
     *
     * @param data to get the time periods from
     * @return the set of time periods
     * @throws java.lang.IllegalArgumentException if data keys are empty
     */
    private Set<RegularTimePeriod> getTimePeriodsKeySet(final Map<Field, Map<RegularTimePeriod, Number>> data) {
        if (data.size() == 0) {
            throw new IllegalArgumentException("Expected data keyset to be non-empty");
        }
        final Field aField = data.keySet().iterator().next();

        return data.get(aField).keySet();
    }

    private Map<String, Integer> createTotals(
            final Map<Field, Map<RegularTimePeriod, Number>> dataMaps
    ) {
        final Map<String, Integer> totals = new HashMap<String, Integer>();
        for (final Field field : dataMaps.keySet()) {
            if (!field.equals(Field.unresolvedTrend)) {
                totals.put(field.toString(), DataUtils.getTotalNumber(dataMaps.get(field)));
            }
        }
        return totals;

    }

    private void makeCumulative(final Map<Field, Map<RegularTimePeriod, Number>> dataMaps) {
        for (final Field field : dataMaps.keySet()) {
            if (!field.equals(Field.unresolvedTrend)) {
                DataUtils.makeCumulative(dataMaps.get(field));
            }
        }
    }


    private DateCountInPeriodResultBean generateResult(
            final ApplicationUser user,
            final Query jqlQuery,
            final Integer daysPrevious,
            final Operation operation,
            final Map<Field, Map<RegularTimePeriod, Number>> dataMaps,
            final List<ProjectVersionBean> versions) {
        final Map<String, Integer> totals = createTotals(dataMaps);


        boolean isCumulative = operation.equals(Operation.cumulative);
        if (isCumulative) {
            makeCumulative(dataMaps);
        }

        final List<TimePeriodResultBean> timePeriodResults = createTimePeriodResults(jqlQuery, daysPrevious, isCumulative, dataMaps);

        final ProjectOrFilterQueryParser.QueryInformation queryInformation = projectOrFilterQueryParser.getQueryInformation(user, jqlQuery);

        return new DateCountInPeriodResultBean(queryInformation.getName(), queryInformation.getUrl(), totals, timePeriodResults, versions);
    }


    private ErrorCollection validateParameters(
            @Nullable final String jql,
            @Nullable final Set<Field> fields,
            @Nullable final TimePeriod period,
            @Nullable final Integer daysPrevious,
            @Nullable final Operation operation) {
        final I18nHelper i18nHelper = authenticationContext.getI18nHelper();
        ErrorCollection errors = new SimpleErrorCollection();

        if (jql == null) {
            addValidationErrorMessage(errors, i18nHelper.getText("datecount.service.jql.null"));
        }

        if (fields == null || fields.size() == 0) {
            addValidationErrorMessage(errors, i18nHelper.getText("datecount.service.fields.empty"));
        } else {
            for (final Field field : fields) {
                if (field == null) {
                    addValidationErrorMessage(errors, i18nHelper.getText("datecount.service.fields.hasnull"));
                }
            }
        }


        if (period == null) {
            addValidationErrorMessage(errors, i18nHelper.getText("datecount.service.period.null"));
        }

        if (daysPrevious == null) {
            addValidationErrorMessage(errors, i18nHelper.getText("datecount.service.daysprevious.null"));
        } else if (daysPrevious <= 0) {
            addValidationErrorMessage(errors, i18nHelper.getText("datecount.service.daysprevious.positive"));
        }

        if (operation == null) {
            addValidationErrorMessage(errors, i18nHelper.getText("datecount.service.operation.null"));
        }

        return errors;
    }

    private void addValidationErrorMessage(final ErrorCollection errors, final String message) {
        errors.addErrorMessage(message, ErrorCollection.Reason.VALIDATION_FAILED);
    }
}
