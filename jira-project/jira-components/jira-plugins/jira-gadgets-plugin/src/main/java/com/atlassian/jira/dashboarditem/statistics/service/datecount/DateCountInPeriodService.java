package com.atlassian.jira.dashboarditem.statistics.service.datecount;

import com.atlassian.fugue.Either;
import com.atlassian.jira.dashboarditem.statistics.service.datecount.beans.DateCountInPeriodResultBean;
import com.atlassian.jira.dashboarditem.statistics.service.datecount.field.Field;
import com.atlassian.jira.dashboarditem.statistics.service.datecount.operation.Operation;
import com.atlassian.jira.dashboarditem.statistics.service.datecount.period.TimePeriod;
import com.atlassian.jira.util.ErrorCollection;

import javax.annotation.Nonnull;
import java.util.Set;


/**
 * Service used to collect information about issue counts in date periods.
 *
 * @since v6.4
 */
public interface DateCountInPeriodService {

    /**
     * Given a set of request parameters this service calculates date counts (number of issues) for a given period,
     * search and fields.
     *
     * @return a result bean or error collection
     */
    Either<DateCountInPeriodResultBean, ErrorCollection> collectData(@Nonnull final RequestParameters requestParameters);

    static class RequestParameters {
        @Nonnull
        private final String jql;
        @Nonnull
        private final Set<Field> fields;
        @Nonnull
        private final TimePeriod period;
        @Nonnull
        private final Integer daysPrevious;
        private final boolean includeVersions;
        @Nonnull
        private final Operation operation;

        RequestParameters(
                @Nonnull final String jql,
                @Nonnull final Set<Field> fields,
                @Nonnull final TimePeriod period,
                @Nonnull final Integer daysPrevious,
                final boolean includeVersions,
                @Nonnull final Operation operation) {
            this.jql = jql;
            this.fields = fields;
            this.period = period;
            this.daysPrevious = daysPrevious;
            this.includeVersions = includeVersions;
            this.operation = operation;
        }

        @Nonnull
        public String getJql() {
            return jql;
        }

        @Nonnull
        public Set<Field> getFields() {
            return fields;
        }

        @Nonnull
        public TimePeriod getPeriod() {
            return period;
        }

        @Nonnull
        public Integer getDaysPrevious() {
            return daysPrevious;
        }

        public boolean isIncludeVersions() {
            return includeVersions;
        }

        @Nonnull
        public Operation getOperation() {
            return operation;
        }
    }
}
