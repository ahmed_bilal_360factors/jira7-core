package com.atlassian.jira.dashboarditem.statistics.util;

import com.atlassian.jira.jql.builder.JqlClauseBuilder;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.jql.util.JqlStringSupport;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.util.UrlBuilder;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;

/**
 * @since v6.4
 */
@Component
public class QueryUrlSupplierImpl implements QueryUrlSupplier {
    private final VelocityRequestContextFactory velocityRequestContextFactory;
    private final JqlStringSupport jqlStringSupport;

    @Autowired
    public QueryUrlSupplierImpl(
            @ComponentImport final VelocityRequestContextFactory velocityRequestContextFactory,
            @ComponentImport final JqlStringSupport jqlStringSupport
    ) {
        this.velocityRequestContextFactory = velocityRequestContextFactory;
        this.jqlStringSupport = jqlStringSupport;
    }

    @Nonnull
    @Override
    public String getUrlForQuery(final Query query) {
        UrlBuilder urlBuilder = getIssueUrlBuilder();
        addJqlParameter(urlBuilder, query);
        return urlBuilder.asUrlString();
    }

    @Nonnull
    @Override
    public String getUrlForQueryWithVersion(final Query query, final Version version) {
        JqlQueryBuilder jqlQueryBuilder = JqlQueryBuilder.newBuilder(query);
        JqlClauseBuilder whereClauseBuilder = jqlQueryBuilder.where().defaultAnd();
        whereClauseBuilder.fixVersion(version.getId());

        return getUrlForQuery(whereClauseBuilder.buildQuery());
    }

    @Nonnull
    public String getUrlForFilter(final Long filterId) {
        UrlBuilder urlBuilder = getIssueUrlBuilder();
        addFilterParamter(urlBuilder, filterId);
        return urlBuilder.asUrlString();
    }

    private UrlBuilder getIssueUrlBuilder() {
        final String baseUrl = getBaseUrl();
        return new UrlBuilder(baseUrl + "/issues/");
    }

    private void addFilterParamter(final UrlBuilder urlBuilder, final Long filterId) {
        urlBuilder.addParameter("filter", filterId);
    }

    private void addJqlParameter(final UrlBuilder urlBuilder, final Query query) {
        urlBuilder.addParameter("jql", jqlStringSupport.generateJqlString(query));

    }

    private String getBaseUrl() {
        return velocityRequestContextFactory.getJiraVelocityRequestContext().getBaseUrl();
    }
}
