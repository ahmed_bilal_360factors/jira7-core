package com.atlassian.jira.dashboarditem.quicklinks;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;

/**
 * Provides access to several sets of link types for the Quick Links dashboard item
 */
@XmlRootElement
public class LinkLists {
    @XmlElement
    private Collection<Link> commonLinks;
    @XmlElement
    private Collection<Link> navigationLinks;

    public LinkLists(Collection<Link> navigationLinks, Collection<Link> commonLinks) {
        this.navigationLinks = navigationLinks;
        this.commonLinks = commonLinks;
    }

    public Collection<Link> getCommonLinks() {
        return commonLinks;
    }

    public Collection<Link> getNavigationLinks() {
        return navigationLinks;
    }
}
