package com.atlassian.jira.gadgets.system;

import com.atlassian.jira.dashboarditem.quicklinks.LinkLists;
import com.atlassian.jira.dashboarditem.quicklinks.QuickLinksProvider;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import static com.atlassian.jira.rest.v1.util.CacheControl.NO_CACHE;

/**
 * REST endpoint to retrieve a list of common usage links.
 *
 * @since v4.0
 */
@Path("/quicklinks")
@AnonymousAllowed
@Produces({MediaType.APPLICATION_JSON})
public class QuickLinksResource {
    private final QuickLinksProvider quicklinksProvider;

    public QuickLinksResource(final QuickLinksProvider quicklinksProvider) {
        this.quicklinksProvider = quicklinksProvider;
    }

    @GET
    public Response getQuickLinks() throws Exception {
        LinkLists links = quicklinksProvider.getLinks();
        if (links.getCommonLinks().isEmpty() && links.getNavigationLinks().isEmpty()) {
            return Response.ok(new Warning()).cacheControl(NO_CACHE).build();
        }
        return Response.ok(links).cacheControl(NO_CACHE).build();
    }

    ///CLOVER:OFF
    @XmlRootElement
    public static class Warning {
        @XmlElement
        private boolean noDataAndNoUser = true;

        @SuppressWarnings({"UnusedDeclaration", "unused"})
        private Warning() {
        }

        public boolean isNoDataNoUser() {
            return noDataAndNoUser;
        }
    }

    ///CLOVER:ON
}
