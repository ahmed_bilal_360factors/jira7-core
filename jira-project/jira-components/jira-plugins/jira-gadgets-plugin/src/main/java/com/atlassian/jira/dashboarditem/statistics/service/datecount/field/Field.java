package com.atlassian.jira.dashboarditem.statistics.service.datecount.field;

import com.atlassian.jira.issue.index.DocumentConstants;

public enum Field {
    created(DocumentConstants.ISSUE_CREATED),
    resolved(DocumentConstants.ISSUE_RESOLUTION_DATE),
    unresolvedTrend;

    private final String documentConstant;

    private Field() {
        this.documentConstant = null;
    }


    /**
     * @param documentConstant that can be used for this field in issue searches.
     */
    private Field(final String documentConstant) {
        this.documentConstant = documentConstant;
    }

    /**
     * @return the document constant for this field that can be used for searching.
     */
    public String getDocumentConstant() {
        return documentConstant;
    }
}
