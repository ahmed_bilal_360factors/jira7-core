package com.atlassian.jira.dashboarditem.statistics.service.statistics;

import com.atlassian.fugue.Either;
import com.atlassian.jira.dashboarditem.statistics.service.statistics.beans.OneDimensionalStatisticsResultBean;
import com.atlassian.jira.util.ErrorCollection;

/**
 * Used to generate statistical results for given searches and groupings.
 *
 * @since v6.4
 */
public interface StatisticsService {
    /**
     * Generates statistics for a given search grouped by a statistic type
     *
     * @param jql      to search
     * @param filterId saved filter to search for
     * @param statType to group by  @return Either error if search was invalid or a set of results
     */
    Either<OneDimensionalStatisticsResultBean, ErrorCollection> aggregateOneDimensionalStats(final String jql, final Long filterId, final String statType);
}
