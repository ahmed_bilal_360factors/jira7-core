package com.atlassian.jira.dashboarditem.statistics.service.versions;

import com.atlassian.fugue.Either;
import com.atlassian.jira.dashboarditem.statistics.service.versions.beans.ProjectVersionBean;
import com.atlassian.jira.util.ErrorCollection;

import javax.annotation.Nonnull;
import java.util.List;


/**
 * Service used to collect version information.
 *
 * @since v6.4
 */
public interface VersionsService {
    /**
     * Get the versions for a given jql string
     *
     * @param jql for searching for issues
     * @return versions or errors
     */
    Either<List<ProjectVersionBean>, ErrorCollection> getVersions(@Nonnull final String jql);
}
