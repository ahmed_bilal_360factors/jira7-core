package com.atlassian.jira.dashboarditem.statistics.util;

import com.atlassian.fugue.Either;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.query.Query;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Used to parse jql query strings to determine if it is a valid jql, therefore returning the {@link com.atlassian.query.Query}.
 *
 * @since v6.4
 */
public interface QueryParser {
    /**
     * Get the query for a given jql, otherwise an error collection if error.
     *
     * @param user completing the search
     * @param jql  for the query
     * @return either a query or errors.
     */
    @Nonnull
    Either<Query, ErrorCollection> getQuery(@Nullable final ApplicationUser user, @Nonnull final String jql);
}
