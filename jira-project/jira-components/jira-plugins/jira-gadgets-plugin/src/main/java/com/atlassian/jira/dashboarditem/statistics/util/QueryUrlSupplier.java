package com.atlassian.jira.dashboarditem.statistics.util;

import com.atlassian.jira.project.version.Version;
import com.atlassian.query.Query;

import javax.annotation.Nonnull;

/**
 * Used to generate urls for the issue navigator for a given query and extra parameters.
 *
 * @since v6.4
 */
public interface QueryUrlSupplier {

    @Nonnull
    String getUrlForQuery(final Query query);

    @Nonnull
    String getUrlForQueryWithVersion(final Query query, final Version version);

    @Nonnull
    public String getUrlForFilter(final Long filterId);
}
