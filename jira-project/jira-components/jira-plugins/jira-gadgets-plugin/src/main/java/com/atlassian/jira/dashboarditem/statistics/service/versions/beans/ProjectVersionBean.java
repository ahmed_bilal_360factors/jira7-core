package com.atlassian.jira.dashboarditem.statistics.service.versions.beans;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

/**
 * Represents information for the version and the project.  It also includes a URL for finding all the issues for the
 * given JQL in {@link com.atlassian.jira.dashboarditem.statistics.service.versions.VersionsServiceImpl} with only this
 * version.
 *
 * @since v6.4
 */
@XmlRootElement
public class ProjectVersionBean {
    @XmlElement
    private Long id;

    @XmlElement
    private String description;

    @XmlElement
    private String name;

    @XmlElement
    private String url;

    @XmlElement
    private Boolean archived;

    @XmlElement
    private Boolean released;

    @XmlElement
    private Date startDate;

    @XmlElement
    private Date releaseDate;

    @XmlElement
    private SimpleProjectBean project;

    public ProjectVersionBean(final Version version, final Project project, final String url) {
        this.id = version.getId();
        this.description = version.getDescription();
        this.name = version.getName();
        this.url = url;
        this.archived = version.isArchived();
        this.released = version.isReleased();
        this.startDate = version.getStartDate();
        this.releaseDate = version.getReleaseDate();
        this.project = new SimpleProjectBean(project);
    }

    public Long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public Boolean getArchived() {
        return archived;
    }

    public Boolean getReleased() {
        return released;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public SimpleProjectBean getProject() {
        return project;
    }
}
