package com.atlassian.jira.gadgets.system.bubblechart;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * A data item that is returned in the bubble chart REST resource
 *
 * @since v7.0
 */
@XmlRootElement
public class IssueBubbleData {
    @XmlElement
    private final String key;
    @XmlElement
    private final int bubbleDomain;
    @XmlElement
    private final int bubbleRange;
    @XmlElement
    private final int bubbleRadius;
    @XmlElement
    private final long bubbleColorValue;

    /**
     * @param key              The issue key or a label that is suitable to be displayed
     * @param bubbleDomain     The value for the data item on the x-axis
     * @param bubbleRange      The value for the data item on the y-axis
     * @param bubbleRadius     The value for the data item on the radius axis, i.e. a value indicating the radius of the circle
     * @param bubbleColorValue The value for the data item on the color axis, i.e. a value indicating a suitable fill for the circle
     */
    public IssueBubbleData(final String key, final int bubbleDomain, final int bubbleRange, final int bubbleRadius, final long bubbleColorValue) {
        this.key = key;
        this.bubbleDomain = bubbleDomain;
        this.bubbleRange = bubbleRange;
        this.bubbleRadius = bubbleRadius;
        this.bubbleColorValue = bubbleColorValue;
    }

    public String getKey() {
        return key;
    }

    public int getBubbleDomain() {
        return bubbleDomain;
    }

    public int getBubbleRange() {
        return bubbleRange;
    }

    public int getBubbleRadius() {
        return bubbleRadius;
    }

    public long getBubbleColorValue() {
        return bubbleColorValue;
    }
}
