package com.atlassian.jira.dashboarditem.statistics.service.datecount.period;

import org.jfree.data.time.Day;
import org.jfree.data.time.Hour;
import org.jfree.data.time.Month;
import org.jfree.data.time.Quarter;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Week;
import org.jfree.data.time.Year;


/**
 * Represents a time period that can be used for statistics collection
 *
 * @since 6.4
 */
public enum TimePeriod {
    hourly(Hour.class),
    daily(Day.class),
    weekly(Week.class),
    monthly(Month.class),
    quarterly(Quarter.class),
    yearly(Year.class);

    private Class<? extends RegularTimePeriod> timePeriodClass;

    private TimePeriod(Class<? extends RegularTimePeriod> timePeriodClass) {
        this.timePeriodClass = timePeriodClass;
    }

    public Class<? extends RegularTimePeriod> getTimePeriodClass() {
        return timePeriodClass;
    }
}
