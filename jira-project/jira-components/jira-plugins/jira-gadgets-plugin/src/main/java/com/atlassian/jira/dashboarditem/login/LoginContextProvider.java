package com.atlassian.jira.dashboarditem.login;

import com.atlassian.jira.bc.security.login.LoginProperties;
import com.atlassian.jira.bc.security.login.LoginService;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.help.HelpUrls;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.util.RecoveryMode;
import com.atlassian.jira.web.ExecutingHttpRequest;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.web.ContextProvider;
import com.google.common.collect.Maps;

import java.util.Map;

public class LoginContextProvider implements ContextProvider {
    private final ApplicationProperties applicationProperties;
    private final RecoveryMode recoveryMode;
    private final HelpUrls helpUrls;
    private final JiraAuthenticationContext authenticationContext;
    private final LoginService loginService;

    public LoginContextProvider(
            @ComponentImport final ApplicationProperties applicationProperties,
            @ComponentImport final LoginService loginService,
            @ComponentImport final JiraAuthenticationContext authenticationContext,
            @ComponentImport final RecoveryMode recoveryMode,
            @ComponentImport final HelpUrls helpUrls
    ) {
        this.loginService = loginService;
        this.applicationProperties = applicationProperties;
        this.authenticationContext = authenticationContext;
        this.recoveryMode = recoveryMode;
        this.helpUrls = helpUrls;
    }

    @Override
    public void init(final Map<String, String> params) throws PluginParseException {
    }

    @Override
    public Map<String, Object> getContextMap(final Map<String, Object> context) {
        LoginProperties loginProperties = loginService.getLoginProperties(authenticationContext.getUser(), ExecutingHttpRequest.get());

        final Map<String, Object> newContext = Maps.newHashMap(context);
        newContext.put("showCaptcha", loginProperties.isElevatedSecurityCheckShown());
        newContext.put("isPublicMode", loginProperties.isPublicMode());
        newContext.put("adminFormOn", String.valueOf(applicationProperties.getOption(APKeys.JIRA_SHOW_CONTACT_ADMINISTRATORS_FORM)));
        newContext.put("showForgotPassword", !loginProperties.isExternalPasswordManagement() && !loginProperties.isExternalUserManagement());
        newContext.put("showRememberMe", loginProperties.isAllowCookies());
        newContext.put("errorMessage", "");
        newContext.put("captchaTimestamp", System.currentTimeMillis());
        newContext.put("recoveryModeEnabled", recoveryMode.isRecoveryModeOn());
        newContext.put("recoveryModeUser", recoveryMode.getRecoveryUsername().getOrNull());
        newContext.put("recoveryModeHelpUrl", helpUrls.getUrl("recovery-mode").getUrl());
        return newContext;
    }
}
