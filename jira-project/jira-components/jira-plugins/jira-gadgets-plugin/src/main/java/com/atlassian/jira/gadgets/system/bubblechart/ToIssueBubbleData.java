package com.atlassian.jira.gadgets.system.bubblechart;

import com.atlassian.jira.bc.issue.comment.CommentService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.collect.Sets;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Maps an issue to an object that is suitable to be returned in the bubble chart REST resource.
 *
 * @since v7.0
 */
public class ToIssueBubbleData implements Function<Issue, IssueBubbleData> {
    private static final long ONE_DAY = TimeUnit.DAYS.toMillis(1);
    private static final String PARTICIPANTS = "participants";
    private static final String VOTES = "votes";

    private final CommentService commentService;
    private final ApplicationUser user;
    private final String bubbleType;
    private final long now = System.currentTimeMillis();
    private final long recentCommentPeriodStart;

    public ToIssueBubbleData(final CommentService commentService, final ApplicationUser user, final String bubbleType, final int recentCommentsDays) {
        this.commentService = commentService;
        this.user = user;
        this.bubbleType = bubbleType;
        this.recentCommentPeriodStart = now - recentCommentsDays * ONE_DAY;
    }

    @Override
    public IssueBubbleData apply(final Issue issue) {
        final List<Comment> commentsForUser = commentService.getCommentsForUser(user, issue);

        final Set<Long> participants = getParticipantsForIssue(issue);
        final Set<Long> commenters = getCommentersForComments(commentsForUser);
        participants.addAll(commenters);

        long recentCommentCount = getRecentCommentCount(commentsForUser, recentCommentPeriodStart);
        int daysCreatedAgo = (int) ((now - issue.getCreated().getTime()) / ONE_DAY);

        int bubbleRadius = 0;
        if (bubbleType.equals(PARTICIPANTS)) {
            bubbleRadius = participants.size();
        } else if (bubbleType.equals(VOTES)) {
            bubbleRadius = issue.getVotes().intValue();
        }

        return new IssueBubbleData(issue.getKey(), daysCreatedAgo, commentsForUser.size(), bubbleRadius, recentCommentCount);
    }

    private Set<Long> getParticipantsForIssue(final Issue issue) {
        final Set<Long> participants = Sets.newHashSet();
        if (issue.getReporter() != null) {
            participants.add(issue.getReporter().getId());
        }
        if (issue.getAssignee() != null) {
            participants.add(issue.getAssignee().getId());
        }
        return participants;
    }

    private Set<Long> getCommentersForComments(final Collection<Comment> comments) {
        return comments.stream()
                .map(comment -> comment.getAuthorApplicationUser().getId())
                .collect(Collectors.toSet());
    }

    private long getRecentCommentCount(final List<Comment> commentsForUser, final long recentCommentPeriodStart) {
        return commentsForUser.stream()
                .filter(comment -> comment.getCreated().getTime() > recentCommentPeriodStart)
                .count();
    }
}
