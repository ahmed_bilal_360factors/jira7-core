# JIRA Gadgets Source Code

## History

 Many many years ago there were `<portlet/>`s.  While relatively fast, they weren't very modern, couldn't
 be shared between products easily and were rendered by an exceedingly aged dashboard that made configuration 
 difficult.  Then came Google's opensocial `<gadget/>`s.  These solved some of the problems above and were
 meant to be the holy grail when it came to sharing information securely between products.  Unfortunately this
 gamble didn't pay off, the opensocial spec is more or less dead and Google stopped having an iGoogle home page 
 with gadgets.  Gadgets were also exceedingly slow due to being rendered in iframes and not being able to share
 web-resources.
 
 Thus we enter into the modern age for JIRA's dashboard with the introduction of the `<dashboard-item/>`.  The 
 `<dashboard-item/>` will be rolled out in a phased approach and slowly replace each individual `<gadget/>` out 
 there. Dashboard Items use modern rendering technologies (AMD, Soy, Less...) and are able to share web-resources
 thus greatly improving page load performance by reducing the number of requests and overall size of data to be 
 transferred on a dashboard load.
 
 This developer guide assumes that you'll not be writing outdated `<gadget/>`s any longer but instead provides 
 setup instructions and steps for writing a `<dashboard-item/>`.
 
## Dashboard Item Developer Guide

Dashboard Items can currently be used in 2 different modes:

* Standalone - to write a completely new dashboard item
* Replacement - to replace an existing opensocial gadget.  This means the opensocial gadget definition still needs 
  to be around and the dashboard item will simply replace the opensocial gadget at render time.

Over time, any 'Replacement' dashboard items will gradually be migrated to 'Standalone' dashbaord items.  This requires
an upgrade task though to change the gadget configuration in the database and will remove the ability for remoting 
(putting an opensocial gadget from JIRA on a Confluence page).

### Standalone Dashboard Items

This is what a dashboard item looks like currently:
 
```
<dashboard-item key="login-dashboard-item">
    <definition>
        <title key="gadget.login.title"/>
        <categories>
            <category>JIRA</category>
        </categories>
        <author>
            <name>Atlassian</name>
        </author>
        <thumbnail location="/download/resources/com.atlassian.jira.gadgets:login-dashboard-item-resources/login-thumb.png"/>
    </definition>
    <description key="gadget.login.description"/>
    <resource name="view" type="soy" location=":login-dashboard-item-resources/JIRA.DashboardItem.Login.Templates.Login"/>
    <amd-module>jira-dashboard-items/login</amd-module>
    <context-provider class="com.atlassian.jira.dashboarditem.login.LoginContextProvider"/>
    <condition class="com.atlassian.jira.plugin.webfragment.conditions.IsAnonymousUserCondition"/>
</dashboard-item>
```

The `<definition/>` is what's used to populate an entry for the dashboard item in the gadget directory dialog from which users
pick their gadets.

The `view` resource is optional. It can be used to render a gadget entirely server-side with the help of an optional 
`<context-provider/>` to provide data for rendering.  We use this for the admin, 
introduction and login gadget for example.  A dashboard item can also include an optional `amd-module` param.  
When the dashboard renders it will then call this javascript AMD module and provide it with the dashboard item's
surrounding `<div/>` as context.

Dashboard Items also support `<condition/>`s to optionally show and hide the item on a dashboard and in the directory.

### Dashboard Items to replace existing opensocial gadgets

 Dashboard items in this mode are tied very closely to opensocial `<gadget/>`s.  They can not exist 
 standalone, but can only replace an existing gadget by mapping to its URI via the `<replace-gadget-spec-uri/>` param.
 If a mapped dashboard-item is detected at rendering time by the dashboard it simply renders a `<div/>` inline instead
 of the opensocial iframe.

 Eventually dashboard items will no longer be tied to opensocial gadgets and will be able to exist standalone (see 
 [Connect dashboard items](https://extranet.atlassian.com/x/_gRwi) for more info).

 This is what a dashboard item looks like that replaces an opensocial gadget:
 
```
<dashboard-item key="login-dashboard-item">
     <replace-gadget-spec-uri>rest/gadgets/1.0/g/com.atlassian.jira.gadgets/gadgets/login.xml</replace-gadget-spec-uri>
     <resource name="view" type="soy" location=":login-dashboard-item-resources/JIRA.DashboardItem.Login.Templates.Login"/>
     <amd-module>jira-dashboard-item/login</amd-module>
     <context-provider class="com.atlassian.jira.dashboarditem.login.LoginContextProvider"/>
 </dashboard-item>
```

They work exactly the same as the standalone dashboard items, with the only difference being that the directory is still 
populated from the opensocial gadget definition and what we persist in the database is still an opensocial gadget.  The 
dashboard item only becomes active and is used at *render* time of the dashboard.

### Important do's and don'ts for writing a dashboard item

* Ensure your CSS/LESS is properly namespaced.  Dashboard items are no longer sandboxed in iframes and we therefore 
need to ensure that their styling doesn't interfere with other dashboard items on the dashboard!
* Similarly all javascript needs to be operate within the context of the passed `<div/>` when the dashboard item is 
initalised.  This is to ensure that event handlers for example only get bound within the current dashboard item and 
don't affect other items on the dashboard!
* Under no circumstances should you modify *existing opensocial gadget code*!  The idea is that if something goes 
wrong with the new dashboard item, we can simply turn of its plugin module and the rendering code will fall back to 
the old existing opensocial gadget!
* Server-side rendering should only be used for mostly static information.  Anything that might be expensive should 
**not** be done server side but requested via an ajax request and rendered client-side (e.g. running a JQL query, 
generating data for a chart, etc) as it will otherwise hold up rendering of the entire dashboard.
* All new code should be tested with qunit tests!
* If you are writing a dashboard item to replace an opensocial gadget, remember to test the new dashboard item
  by creating an instance of the old gadget and then enabling your new dashboard item to test that your dashboard item
  is compatible with the preferences of the old gadget
* Introduce shared AMD modules and soy templates where this makes sense for common config form elements (e.g. filter 
pickers)

### Sample Dashboard Item AMD module

This is what a sample dashboard item AMD module for a configurable dashboard item generally looks like:

```
define("jira-dashboard-items/sample-dashboard-item", [
    'underscore'
], function (_) {
    var DashboardItem = function (API) {
        this.API = API;
    };

    /**
     * Called to render the view for a fully configured dashboard item.
     *
     * @param context The surrounding <div/> context that this items should render into.
     * @param preferences The user preferences saved for this dashboard item (e.g. filter id, number of results...)
     */
    DashboardItem.prototype.render = function (context, preferences) {

        //TODO: render the view with the preferences provided

        this.API.once("afterRender", _.bind(function () {
            this.API.showLoadingBar();
        }, this));
    };

    /**
     * Called to render the configuration form for this dashboard item if preferences.isConfigured
     * has not been set yet.
     *
     * @param context The surrounding <div/> context that this items should render into.
     * @param preferences The user preferences saved for this dashboard item 
     */
    DashboardItem.prototype.renderEdit = function (context, preferences) {

        //TODO: render a config form here using provided SOY templates and assign it
        //      to the 'form' variable

        form.on("submit", _.bind(function (e) {
            e.preventDefault();
            if (!validateFields()) {  //TODO: validateFields needs to implemented
                this.API.forceLayoutRefresh();
                return;
            }
            this.API.savePreferences({
                //provide parsed prefs from your config form here
                //to store them for this dashboard item
            });
        }, this));

        form.find("input.button.cancel").on("click", _.bind(function () {
            this.API.closeEdit();
        }, this));
    };

    return DashboardItem;
});
```

## Development Steps

1. Check out JIRA source `git clone ssh://git@stash.atlassian.com:7997/jiraserver/jira.git`
2. Create an issue in the [IG project](http://jdog.atlassian.net/browse/IG) on JDOG for the gadget you want to convert
 and assign it to yourself
3. Create a branch for your gadget prefixed with *issue/*: `git co -b issue/IG-123-convert-twod-stats-gadget`
4. Run JIRA from the root directory: `./jmake debug -sh`
5. Start coding in the `jira-components/jira-plugins/jira-gadgets-plugin` plugin
6. Quickreload is installed and configured for this directory already:
    1. Static files will automatically reload. Just `ALT` + `Tab` to the browser and reload
    2. To reload the plugin if changing Java code simply run `mvn package -DskipTests -DskipSources` in the 
    `jira-gadgets-plugin` directory. Quickreload will then reinstall the plugin for you.
    
*Note*: To get jmake up and running you can follow the steps from the JIRA [Development Handbook](https://extranet.atlassian.com/display/JIRADEV/JIRA+Development+Handbook). For general coding guidelines follow JIRA's [Rules of Engagement](https://extranet.atlassian.com/display/JIRADEV/The+JIRA+development+process).

Once you're ready, push your changes, ensure the builds are green on your branch and submit a pull request to master.
