package com.atlassian.jira.rest.api.util;

import com.atlassian.fugue.Either;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.ErrorCollections;
import com.atlassian.jira.util.I18nHelper;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.asList;

/**
 * Abstract class that provides parsing of expand query parameters.
 *
 * @since v6.5
 */
public abstract class ExpandParameterParser<T extends Enum<T>> {
    private static final Splitter SPLITTER = Splitter.on(',').omitEmptyStrings().trimResults();

    private final I18nHelper i18n;

    private final Map<String, T> index;
    private final List<T> values;

    protected ExpandParameterParser(I18nHelper i18n, Class<T> enumClass) {
        this.i18n = i18n;
        this.index = buildIndex(enumClass);
        this.values = ImmutableList.copyOf(asList(enumClass.getEnumConstants()));
    }

    private Map<String, T> buildIndex(final Class<T> enumClass) {
        ImmutableMap.Builder<String, T> indexBuilder = ImmutableMap.builder();
        for (T constant : enumClass.getEnumConstants()) {
            indexBuilder.put(constant.name(), constant);
        }
        return indexBuilder.build();
    }

    /**
     * parses a query parameter and returns either an error collection in case of failure or a list of parsed elements.
     *
     * @param expandQueryParameter query parameter, may be null
     * @return either error collection or list of {@code T} elements
     */
    public Either<ErrorCollection, List<T>> parseExpandQuery(@Nullable final String expandQueryParameter) {
        ImmutableList.Builder<T> resultBuilder = ImmutableList.builder();
        for (String elementStr : SPLITTER.split(Strings.nullToEmpty(expandQueryParameter))) {
            if (!index.containsKey(elementStr)) {
                return Either.left(ErrorCollections.create(i18n.getText("rest.error.invalid.expand", elementStr, values.toString()), com.atlassian.jira.util.ErrorCollection.Reason.VALIDATION_FAILED));
            }
            resultBuilder.add(index.get(elementStr));
        }
        return Either.right(resultBuilder.build());
    }
}



