package com.atlassian.jira.rest.api.notification;

import org.codehaus.jackson.annotate.JsonProperty;

import javax.annotation.Nullable;

/**
 * Information about the event. This might be either system event or an event configured by JIRA administrator. Only
 * events configured by JIRA administrators have template.
 */
public class NotificationEventBean {
    @JsonProperty
    private final Long id;
    @JsonProperty
    private final String name;
    @JsonProperty
    private final String description;
    @JsonProperty
    @Nullable
    private final NotificationEventBean templateEvent;

    public NotificationEventBean(final Long id, final String name, final String description, final NotificationEventBean templateEvent) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.templateEvent = templateEvent;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public NotificationEventBean getTemplateEvent() {
        return templateEvent;
    }
}
