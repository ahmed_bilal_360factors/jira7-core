package com.atlassian.jira.rest.api.permission;

import com.google.common.collect.ImmutableList;
import org.codehaus.jackson.annotate.JsonAutoDetect;

import java.util.List;

@JsonAutoDetect
public class PermissionGrantsBean {
    private final List<PermissionGrantBean> permissions;
    private final String expand = PermissionSchemeExpandParam.PERMISSION_GRANTS_ENVELOPE_EXPAND_PARAMS;

    public PermissionGrantsBean(final Iterable<PermissionGrantBean> permissions) {
        this.permissions = ImmutableList.copyOf(permissions);
    }

    public List<PermissionGrantBean> getPermissions() {
        return permissions;
    }

    public String getExpand() {
        return expand;
    }
}
