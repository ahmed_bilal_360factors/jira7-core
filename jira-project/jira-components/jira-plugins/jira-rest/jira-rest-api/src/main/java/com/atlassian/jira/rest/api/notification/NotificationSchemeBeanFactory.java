package com.atlassian.jira.rest.api.notification;

import com.atlassian.fugue.Either;
import com.atlassian.jira.notification.NotificationScheme;
import com.atlassian.jira.util.ErrorCollection;

import javax.annotation.Nullable;
import java.util.List;

/**
 * @since v6.5
 */
public interface NotificationSchemeBeanFactory {

    NotificationSchemeBean createNotificationSchemeBean(NotificationScheme notificationScheme, List<NotificationSchemeExpandParam> expand);

    public Either<ErrorCollection, List<NotificationSchemeExpandParam>> parseExpandQuery(@Nullable final String expandQueryParameter);
}
