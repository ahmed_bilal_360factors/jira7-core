package com.atlassian.jira.rest.api.permission;

import com.atlassian.fugue.Either;
import com.atlassian.jira.permission.PermissionGrant;
import com.atlassian.jira.permission.PermissionGrantInput;
import com.atlassian.jira.permission.PermissionScheme;
import com.atlassian.jira.permission.PermissionSchemeInput;
import com.atlassian.jira.util.ErrorCollection;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Collection;
import java.util.List;

/**
 * @since v6.5
 */
@ParametersAreNonnullByDefault
public interface PermissionSchemeBeansFactory {
    PermissionSchemeBean toBean(PermissionScheme scheme, List<PermissionSchemeExpandParam> expands);

    PermissionGrantBean toBean(PermissionGrant permissionGrant, @Nullable Long schemeId, List<PermissionSchemeExpandParam> expands);

    Either<ErrorCollection, PermissionSchemeInput> fromBean(PermissionSchemeBean permissionScheme);

    Either<ErrorCollection, Collection<PermissionGrantInput>> fromBean(List<PermissionGrantBean> permissions);

    Either<ErrorCollection, PermissionGrantInput> fromBean(PermissionGrantBean grantBean);
}
