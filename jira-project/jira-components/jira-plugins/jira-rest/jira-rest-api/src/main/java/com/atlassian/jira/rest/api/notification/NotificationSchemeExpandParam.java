package com.atlassian.jira.rest.api.notification;

import com.google.common.base.Joiner;

import java.util.Arrays;

/**
 * @since v6.5
 */
public enum NotificationSchemeExpandParam {
    notificationSchemeEvents, user, group, projectRole, field, all;

    public static final String NOTIFICATION_SCHEME_EXPAND_PARAMS = Joiner.on(',').join(Arrays.asList(values()));
}
