package com.atlassian.jira.rest.api.notification;

import com.google.common.collect.ImmutableList;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

/**
 * REST representation of {@link com.atlassian.jira.notification.EventNotifications}.
 */
public class NotificationSchemeEventBean {
    @JsonProperty
    private final NotificationEventBean event;
    @JsonProperty
    private final List<AbstractNotificationBean> notifications;

    public NotificationSchemeEventBean(final NotificationEventBean event, final Iterable<AbstractNotificationBean> notifications) {
        this.event = event;
        this.notifications = ImmutableList.copyOf(notifications);
    }

    public NotificationEventBean getEvent() {
        return event;
    }

    public List<AbstractNotificationBean> getNotifications() {
        return notifications;
    }
}
