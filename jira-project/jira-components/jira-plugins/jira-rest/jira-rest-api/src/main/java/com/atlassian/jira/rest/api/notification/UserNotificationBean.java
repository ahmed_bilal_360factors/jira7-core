package com.atlassian.jira.rest.api.notification;

import com.atlassian.jira.issue.fields.rest.json.beans.UserJsonBean;
import com.atlassian.jira.notification.type.NotificationType;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * REST representation of {@link com.atlassian.jira.notification.UserNotification}. Holds information about
 * notification to the configured user.
 *
 * @since 7.0
 */
public class UserNotificationBean extends AbstractNotificationBean {
    @JsonProperty
    private final UserJsonBean user;

    public UserNotificationBean(final Long id,
                                final NotificationType notificationType,
                                final String parameter,
                                final UserJsonBean user) {
        super(id, notificationType, parameter);
        this.user = user;
    }

    public UserJsonBean getUser() {
        return user;
    }

    @Override
    public String getAvailableExpand() {
        return NotificationSchemeExpandParam.user.toString();
    }
}
