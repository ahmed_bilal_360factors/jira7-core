package com.atlassian.jira.rest.api.notification;

import com.atlassian.jira.notification.type.NotificationType;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * REST representation of {@link com.atlassian.jira.notification.EmailNotification}. Holds information about
 * notification to the configured email address.
 *
 * @since 7.0.
 */
public class EmailNotificationBean extends AbstractNotificationBean {
    @JsonProperty
    private final String emailAddress;

    public EmailNotificationBean(final Long id,
                                 final NotificationType notificationType,
                                 final String emailAddress) {
        super(id, notificationType, emailAddress);
        this.emailAddress = emailAddress;
    }

    public String getEmailAddress() {
        return emailAddress;
    }
}
