package com.atlassian.jira.rest.api.notification;

import com.atlassian.jira.notification.type.NotificationType;
import com.atlassian.jira.rest.api.project.ProjectRoleBean;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * REST representation of {@link com.atlassian.jira.notification.ProjectRoleNotification}. Holds information about notification
 * to the selected group.
 */
public class ProjectRoleNotificationBean extends AbstractNotificationBean {
    @JsonProperty
    private final ProjectRoleBean projectRole;

    public ProjectRoleNotificationBean(final Long id,
                                       final NotificationType notificationType,
                                       final String parameter,
                                       final ProjectRoleBean projectRole) {
        super(id, notificationType, parameter);
        this.projectRole = projectRole;
    }

    public ProjectRoleBean getProjectRole() {
        return projectRole;
    }

    @Override
    public String getAvailableExpand() {
        return NotificationSchemeExpandParam.projectRole.toString();
    }
}
