package com.atlassian.jira.rest.api.property;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

import java.io.IOException;
import java.util.Map;

class PropertiesBeanSerializer extends JsonSerializer<PropertiesBean> {
    public void serialize(PropertiesBean value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
        jgen.writeStartObject();
        for (Map.Entry<String, String> e : value.getProperties().entrySet()) {
            jgen.writeFieldName(e.getKey());
            jgen.writeRawValue(e.getValue());
        }
        jgen.writeEndObject();
    }
}
