package com.atlassian.jira.rest.api.notification;

import com.atlassian.jira.notification.type.NotificationType;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.annotation.Nullable;

/**
 * REST representation of {@link com.atlassian.jira.notification.Notification}.
 *
 * @since v7.0
 */
public abstract class AbstractNotificationBean {
    @JsonProperty
    private final Long id;
    @JsonProperty
    private final String notificationType;
    @JsonProperty
    private final String parameter;

    private boolean isInput = false;

    protected AbstractNotificationBean(Long id, NotificationType notificationType, @Nullable String parameter) {
        this.id = id;
        this.notificationType = notificationType.getRestApiName();
        this.parameter = parameter;
    }

    public AbstractNotificationBean input() {
        isInput = true;
        return this;
    }

    @JsonProperty
    public final String getExpand() {
        return isInput ? null : getAvailableExpand();
    }

    protected String getAvailableExpand() {
        return null;
    }

    public Long getId() {
        return id;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public String getParameter() {
        return parameter;
    }
}
