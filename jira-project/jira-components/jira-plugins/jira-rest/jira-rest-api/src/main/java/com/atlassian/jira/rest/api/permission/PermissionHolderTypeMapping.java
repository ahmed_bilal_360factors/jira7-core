package com.atlassian.jira.rest.api.permission;

import com.atlassian.jira.permission.JiraPermissionHolderType;
import com.atlassian.jira.permission.PermissionHolderType;
import com.google.common.base.Objects;
import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableBiMap;

import java.util.Optional;

/**
 * This class contains mapping between JIRA permission holder types and their respective REST names.
 *
 * @since v6.5
 */
public final class PermissionHolderTypeMapping {
    private PermissionHolderTypeMapping() {
    }

    private static final BiMap<String, PermissionHolderType> holderTypesMap = ImmutableBiMap.<String, PermissionHolderType>builder()
            .put("group", JiraPermissionHolderType.GROUP)
            .put("anyone", JiraPermissionHolderType.ANYONE)
            .put("user", JiraPermissionHolderType.USER)
            .put("projectRole", JiraPermissionHolderType.PROJECT_ROLE)
            .put("reporter", JiraPermissionHolderType.REPORTER)
            .put("projectLead", JiraPermissionHolderType.PROJECT_LEAD)
            .put("assignee", JiraPermissionHolderType.ASSIGNEE)
            .put("userCustomField", JiraPermissionHolderType.USER_CUSTOM_FIELD)
            .put("groupCustomField", JiraPermissionHolderType.GROUP_CUSTOM_FIELD)
            .put("reporterWithCreatePermission", JiraPermissionHolderType.REPORTER_WITH_CREATE_PERMISSION)
            .put("assigneeWithAssignablePermission", JiraPermissionHolderType.ASSIGNEE_WITH_ASSIGNABLE_PERMISSION)
            .build();

    public static String toRestType(PermissionHolderType holderType) {
        return Objects.firstNonNull(holderTypesMap.inverse().get(holderType), holderType.getKey());
    }

    public static Optional<PermissionHolderType> fromRestType(String holderType) {
        return Optional.ofNullable(holderTypesMap.get(holderType));
    }
}
