package com.atlassian.jira.rest.api.notification;

import org.codehaus.jackson.annotate.JsonProperty;

import static com.google.common.collect.ImmutableList.copyOf;

public class NotificationSchemeBean {
    @JsonProperty
    private String expand = NotificationSchemeExpandParam.NOTIFICATION_SCHEME_EXPAND_PARAMS;

    @JsonProperty
    private final Long id;
    @JsonProperty
    private final String self;
    @JsonProperty
    private final String name;
    @JsonProperty
    private final String description;
    @JsonProperty
    private final Iterable<NotificationSchemeEventBean> notificationSchemeEvents;

    public NotificationSchemeBean(final Long id,
                                  final String self,
                                  final String name,
                                  final String description,
                                  final Iterable<NotificationSchemeEventBean> notificationSchemeEvents) {
        this.id = id;
        this.self = self;
        this.name = name;
        this.description = description;
        this.notificationSchemeEvents = notificationSchemeEvents != null ? copyOf(notificationSchemeEvents) : null;
    }

    public NotificationSchemeBean input() {
        expand = null;
        return this;
    }

    public Long getId() {
        return id;
    }

    public String getSelf() {
        return self;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Iterable<NotificationSchemeEventBean> getNotificationSchemeEvents() {
        return notificationSchemeEvents;
    }

    public String getExpand() {
        return expand;
    }
}
