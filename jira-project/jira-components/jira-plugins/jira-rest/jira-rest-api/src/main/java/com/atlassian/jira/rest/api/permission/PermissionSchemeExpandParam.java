package com.atlassian.jira.rest.api.permission;

import com.atlassian.jira.permission.JiraPermissionHolderType;
import com.atlassian.jira.permission.PermissionHolderType;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.Optional;
import java.util.Set;

import static com.atlassian.jira.permission.JiraPermissionHolderType.GROUP;
import static com.atlassian.jira.permission.JiraPermissionHolderType.GROUP_CUSTOM_FIELD;
import static com.atlassian.jira.permission.JiraPermissionHolderType.PROJECT_ROLE;
import static com.atlassian.jira.permission.JiraPermissionHolderType.USER;
import static com.atlassian.jira.permission.JiraPermissionHolderType.USER_CUSTOM_FIELD;

public enum PermissionSchemeExpandParam {
    permissions(),
    user(USER),
    group(GROUP),
    projectRole(PROJECT_ROLE),
    field(GROUP_CUSTOM_FIELD, USER_CUSTOM_FIELD),
    all(USER, GROUP, PROJECT_ROLE, GROUP_CUSTOM_FIELD, USER_CUSTOM_FIELD);

    public static final String PERMISSION_SCHEME_EXPAND_PARAMS = Joiner.on(',').join(Arrays.asList(values()));
    public static final String PERMISSION_GRANTS_ENVELOPE_EXPAND_PARAMS = Joiner.on(',').join(ImmutableList.of(user, group, projectRole, field, all));

    private final Set<PermissionHolderType> expandedTypes;

    PermissionSchemeExpandParam(final JiraPermissionHolderType... expandedTypes) {
        this.expandedTypes = ImmutableSet.copyOf(expandedTypes);
    }

    public boolean expandsType(PermissionHolderType type) {
        return expandedTypes.contains(type);
    }

    @Nullable
    public static String expandForType(final String type) {
        Optional<PermissionHolderType> holderType = PermissionHolderTypeMapping.fromRestType(type);
        if (holderType.isPresent()) {
            for (PermissionSchemeExpandParam permissionSchemeExpandParam : values()) {
                if (permissionSchemeExpandParam.expandsType(holderType.get())) {
                    return permissionSchemeExpandParam.toString();
                }
            }
        }
        return null;
    }
}
