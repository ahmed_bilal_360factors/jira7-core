package com.atlassian.jira.rest.api.notification;

import com.atlassian.jira.issue.fields.rest.json.beans.GroupJsonBean;
import com.atlassian.jira.notification.type.NotificationType;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.annotation.Nullable;

/**
 * REST representation of {@link com.atlassian.jira.notification.GroupNotification}. Holds information about notification
 * to the selected group.
 */
public class GroupNotificationBean extends AbstractNotificationBean {
    @JsonProperty
    private final GroupJsonBean group;

    public GroupNotificationBean(final Long id, final NotificationType notificationType, final String parameter, @Nullable final GroupJsonBean group) {
        super(id, notificationType, parameter);
        this.group = group;
    }

    public GroupJsonBean getGroup() {
        return group;
    }

    @Override
    public String getAvailableExpand() {
        return NotificationSchemeExpandParam.group.toString();
    }
}
