package com.atlassian.jira.rest.api.notification;

import com.atlassian.jira.notification.type.NotificationType;
import com.atlassian.jira.rest.api.field.FieldBean;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * REST representation of {@link com.atlassian.jira.notification.CustomFieldValueNotification}. Holds information about
 * notifications to users or groups which are specified in the value of a custom field.
 *
 * @since 7.0
 */
public class CustomFieldValueNotificationBean extends AbstractNotificationBean {
    @JsonProperty
    private final FieldBean field;

    public CustomFieldValueNotificationBean(final Long id,
                                            final NotificationType notificationType,
                                            final String parameter,
                                            final FieldBean field) {
        super(id, notificationType, parameter);
        this.field = field;
    }

    public FieldBean getField() {
        return field;
    }

    @Override
    public String getAvailableExpand() {
        return NotificationSchemeExpandParam.field.toString();
    }
}
