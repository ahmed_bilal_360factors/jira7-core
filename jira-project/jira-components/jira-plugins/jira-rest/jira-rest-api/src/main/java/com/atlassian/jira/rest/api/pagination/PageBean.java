package com.atlassian.jira.rest.api.pagination;

import com.atlassian.jira.util.Page;
import com.atlassian.jira.util.PageRequest;
import org.codehaus.jackson.annotate.JsonAutoDetect;

import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.List;
import java.util.function.Function;

import static java.util.stream.Collectors.toList;

@JsonAutoDetect
public class PageBean<T> {
    private final URI self;
    private final URI nextPage;
    private final int maxResults;
    private final long startAt;
    private final Long total;
    private final Boolean isLast;
    private final List<T> values;

    public PageBean(final URI self, final URI nextPage, final int maxResults, final long startAt, final Long total, final Boolean isLastPage, final List<T> values) {
        this.self = self;
        this.nextPage = nextPage;
        this.maxResults = maxResults;
        this.startAt = startAt;
        this.total = total;
        this.isLast = isLastPage;
        this.values = values;
    }

    public URI getSelf() {
        return self;
    }

    public URI getNextPage() {
        return nextPage;
    }

    public int getMaxResults() {
        return maxResults;
    }

    public long getStartAt() {
        return startAt;
    }

    public Long getTotal() {
        return total;
    }

    public Boolean getIsLast() {
        return isLast;
    }

    public List<T> getValues() {
        return values;
    }

    public static <T> Builder<T> from(PageRequest request, Page<T> response) {
        return new Builder<T>(request, response);
    }

    public static final class Builder<T> {
        private final long startAt;
        private final Long total;
        private final int maxResults;
        private Boolean isLastPage;
        private final List<T> values;
        private URI self;
        private URI nextPage;

        public Builder(final PageRequest request, final Page<T> response) {
            this.maxResults = request.getLimit();
            this.startAt = response.getStart();
            this.total = response.getTotal();
            this.values = response.getValues();
            this.isLastPage = response.isLast();
        }

        public Builder<T> setLinks(String self, int limit) {
            this.self = UriBuilder.fromUri(self).replaceQueryParam("startAt", startAt).replaceQueryParam("maxResults", limit).build();
            if (!isLastPage) {
                this.nextPage = UriBuilder.fromUri(self).replaceQueryParam("startAt", startAt + values.size()).replaceQueryParam("maxResults", limit).build();
            }
            return this;
        }

        public <E> PageBean<E> build(Function<T, E> toBeanFunction) {
            List<E> beans = values.stream().map(toBeanFunction).collect(toList());
            return new PageBean<>(self, nextPage, maxResults, startAt, total, isLastPage, beans);
        }
    }
}
