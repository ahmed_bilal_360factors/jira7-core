package com.atlassian.jira.rest.api.permission;

import com.google.common.collect.ImmutableList;
import org.codehaus.jackson.annotate.JsonAutoDetect;

import java.util.List;

@JsonAutoDetect
public class PermissionSchemesBean {
    private final List<PermissionSchemeBean> permissionSchemes;

    public PermissionSchemesBean(final Iterable<PermissionSchemeBean> permissionSchemes) {
        this.permissionSchemes = ImmutableList.copyOf(permissionSchemes);
    }

    public List<PermissionSchemeBean> getPermissionSchemes() {
        return permissionSchemes;
    }
}
