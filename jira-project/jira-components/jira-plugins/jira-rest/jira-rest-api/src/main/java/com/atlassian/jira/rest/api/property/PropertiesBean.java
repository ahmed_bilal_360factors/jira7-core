package com.atlassian.jira.rest.api.property;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableMap;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.Collections;
import java.util.Map;

@JsonAutoDetect
@JsonSerialize(using = PropertiesBeanSerializer.class)
public class PropertiesBean {
    public static final PropertiesBean EMPTY = PropertiesBean.of(Collections.emptyMap());

    private final Map<String, String> properties;

    public static PropertiesBean of(Map<String, String> properties) {
        return new PropertiesBean(properties);
    }

    private PropertiesBean(Map<String, String> properties) {
        this.properties = properties != null ? ImmutableMap.copyOf(properties) : null;
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PropertiesBean that = (PropertiesBean) o;

        return Objects.equal(this.properties, that.properties);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(properties);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("properties", properties)
                .toString();
    }
}
