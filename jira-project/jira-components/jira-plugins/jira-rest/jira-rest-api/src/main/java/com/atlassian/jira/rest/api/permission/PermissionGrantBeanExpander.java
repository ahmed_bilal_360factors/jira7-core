package com.atlassian.jira.rest.api.permission;

import com.atlassian.fugue.Either;
import com.atlassian.jira.permission.PermissionHolderType;
import com.atlassian.jira.util.ErrorCollection;

import java.util.List;

/**
 * @since 6.5
 */
public interface PermissionGrantBeanExpander {
    Either<ErrorCollection, List<PermissionSchemeExpandParam>> parseExpandQuery(final String expandQueryParameter);

    PermissionHolderBean expand(PermissionHolderBean grantBean, PermissionHolderType holderType, final List<PermissionSchemeExpandParam> expands);
}
