package com.atlassian.jira.rest.api.notification;

import com.atlassian.jira.notification.type.NotificationType;

/**
 * REST representation of {@link com.atlassian.jira.notification.RoleNotification}. Holds information about notification
 * to the configured role, such as assignee, reporter, component lead, etc..
 *
 * @since 7.0
 */
public class RoleNotificationBean extends AbstractNotificationBean {
    public RoleNotificationBean(final Long id, final NotificationType notificationType) {
        super(id, notificationType, null);
    }
}
