package com.atlassian.jira.rest.api.permission;

import com.atlassian.rest.annotation.RestProperty;
import com.google.common.base.Objects;
import org.codehaus.jackson.annotate.JsonProperty;

import java.net.URI;

public final class PermissionGrantBean {
    @RestProperty (scope = RestProperty.Scope.RESPONSE)
    @JsonProperty
    private Long id;
    @JsonProperty
    private URI self;
    @JsonProperty
    private PermissionHolderBean holder;
    @JsonProperty
    private String permission;

    @Deprecated
    public PermissionGrantBean() {
    }

    private PermissionGrantBean(final Long id, final URI self, final PermissionHolderBean holder, final String permission) {
        this.id = id;
        this.self = self;
        this.holder = holder;
        this.permission = permission;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public URI getSelf() {
        return self;
    }

    public void setSelf(final URI self) {
        this.self = self;
    }

    public PermissionHolderBean getHolder() {
        return holder;
    }

    public void setHolder(final PermissionHolderBean type) {
        this.holder = type;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(final String permission) {
        this.permission = permission;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PermissionGrantBean that = (PermissionGrantBean) o;

        return Objects.equal(this.id, that.id) &&
                Objects.equal(this.self, that.self) &&
                Objects.equal(this.holder, that.holder) &&
                Objects.equal(this.permission, that.permission);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, self, holder, permission);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Long id;
        private URI self;
        private PermissionHolderBean holder;
        private String permission;

        public Builder setId(final Long id) {
            this.id = id;
            return this;
        }

        public Builder setSelf(final URI self) {
            this.self = self;
            return this;
        }

        /**
         * generic scheme name: type
         */
        public Builder setHolder(final PermissionHolderBean holder) {
            this.holder = holder;
            return this;
        }

        /**
         * generic scheme name: entityTypeId
         */
        public Builder setPermission(final String permission) {
            this.permission = permission;
            return this;
        }

        public PermissionGrantBean build() {
            return new PermissionGrantBean(id, self, holder, permission);
        }
    }
}
