package com.atlassian.jira.rest.internal.v2.admin.applicationrole;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.application.MockApplicationRole;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.rest.internal.v2.admin.applicationrole.ApplicationRoleResource.GroupBean;
import com.atlassian.jira.rest.matchers.ResponseMatchers;
import com.atlassian.jira.rest.v2.admin.GroupLabelBean;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockGroup;
import com.atlassian.jira.web.component.admin.group.GroupLabelView;
import com.atlassian.jira.web.component.admin.group.GroupLabelsService;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.ws.rs.core.Response;
import java.util.Optional;
import java.util.Set;

import static com.atlassian.jira.web.component.admin.group.GroupLabelView.LabelType.ADMIN;
import static com.atlassian.jira.web.component.admin.group.GroupLabelView.LabelType.MULTIPLE;
import static com.atlassian.jira.web.component.admin.group.GroupLabelView.LabelType.SINGLE;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class ApplicationRoleResourceTest {
    @Rule
    public MockitoRule mocks = MockitoJUnit.rule();

    @Mock
    private ApplicationRoleManager applicationRoleManager;
    @Mock
    private GroupLabelsService groupLabelsService;
    @Mock
    private GlobalPermissionManager permissionManager;
    @Mock
    private JiraAuthenticationContext authenticationContext;

    @Mock
    private ApplicationUser applicationUser;

    @InjectMocks
    private ApplicationRoleResource resource;

    @Before
    public void setUp() {
        //prepare happy path flow
        when(authenticationContext.getLoggedInUser()).thenReturn(applicationUser);
        when(permissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, applicationUser)).thenReturn(true);

        resource = new ApplicationRoleResource(applicationRoleManager, groupLabelsService, permissionManager, authenticationContext, new MockI18nHelper());
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsNotAdmin() {
        when(permissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, applicationUser)).thenReturn(false);

        assertThat(resource.getGroups(), ResponseMatchers.status(Response.Status.FORBIDDEN));
    }

    @Test
    public void shouldReturnGroupBeansForApplicationGroups() {
        when(applicationRoleManager.getRoles()).thenReturn(ImmutableSet.of(
                new MockApplicationRole(ApplicationKey.valueOf("app-a")).groupNames("lions", "monsters"),
                new MockApplicationRole(ApplicationKey.valueOf("app-b")).groupNames("tigers")
        ));

        assertThat(resource.getGroups(), ResponseMatchers.body(Set.class, ImmutableSet.of(
                new GroupBean("lions", ImmutableList.of()),
                new GroupBean("monsters", ImmutableList.of()),
                new GroupBean("tigers", ImmutableList.of())
        )));
    }

    @Test
    public void shouldReturnLabelsForGroups() {
        MockGroup monsters = new MockGroup("monsters");
        when(groupLabelsService.getGroupLabels(monsters, Optional.<Long>empty())).thenReturn(ImmutableList.of(
                new GroupLabelView("admin-text", "admin-title", ADMIN),
                new GroupLabelView("multi-text", "multi-title", MULTIPLE)
        ));

        MockGroup lions = new MockGroup("lions");
        when(groupLabelsService.getGroupLabels(lions, Optional.<Long>empty())).thenReturn(ImmutableList.of(
                new GroupLabelView("single-text", "single-title", SINGLE)
        ));

        when(applicationRoleManager.getRoles()).thenReturn(ImmutableSet.of(
                new MockApplicationRole(ApplicationKey.valueOf("app-a")).groups(monsters),
                new MockApplicationRole(ApplicationKey.valueOf("app-b")).groups(monsters, lions)
        ));

        assertThat(resource.getGroups(), ResponseMatchers.body(Set.class, ImmutableSet.of(
                new GroupBean("monsters", ImmutableList.of(
                        new GroupLabelBean("admin-text", "admin-title", ADMIN),
                        new GroupLabelBean("multi-text", "multi-title", MULTIPLE)
                )),
                new GroupBean("lions", ImmutableList.of(
                        new GroupLabelBean("single-text", "single-title", SINGLE)
                ))
        )));
    }

}