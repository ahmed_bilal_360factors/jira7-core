package com.atlassian.jira.rest.v2.search;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.issue.fields.rest.json.beans.GroupJsonBean;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.rest.v2.issue.project.ProjectBean;
import com.atlassian.jira.scheme.SchemeManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.sharing.type.ShareType;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.util.concurrent.LazyReference;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class UserListResolverTest {
    @Mock
    private UserManager userManager;

    @Mock
    private FeatureManager featureManager;

    @Mock
    private GroupManager groupManager;

    @Mock
    private ProjectManager projectManager;

    @Mock
    private ProjectRoleManager projectRoleManager;

    @Mock
    private SchemeManager schemeManager;

    @Mock
    private JiraAuthenticationContext authContext;

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private UserUtil userUtil;

    @Before
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void testUserCountWithGlobalSharePermission() {
        setUpUserValidPermissionMocks();

        final FilterPermissionBean globalPermission = mock(FilterPermissionBean.class);
        when(globalPermission.getType()).thenReturn(ShareType.Name.GLOBAL.toString());

        final UserListResolver userListResolver = new UserListResolver(authContext, userManager, groupManager, projectManager,
                permissionManager, projectRoleManager, schemeManager, userUtil, featureManager, Arrays.asList(globalPermission));

        final int activeUserCount = Integer.MAX_VALUE;
        when(userUtil.getActiveUserCount()).thenReturn(activeUserCount);

        assertEquals(activeUserCount, userListResolver.getShareCount());
        verify(userManager, never()).getAllUsers();

        when(featureManager.isEnabled(UserListResolver.DO_NOT_COUNT_USERS_ON_SHARED_FILTER_FEATURE)).thenReturn(false);
    }

    @Test
    public void testUserCountWithGroupSharePermission() {
        setUpUserValidPermissionMocks();

        final FilterPermissionBean groupPermission = mock(FilterPermissionBean.class);
        when(groupPermission.getType()).thenReturn(ShareType.Name.GROUP.toString());

        final UserListResolver userListResolver = new UserListResolver(authContext, userManager, groupManager, projectManager,
                permissionManager, projectRoleManager, schemeManager, userUtil, featureManager, Arrays.asList(groupPermission));

        final String groupName = "group";
        final Group group = mock(Group.class);
        final Collection<ApplicationUser> groupUsers = Arrays.asList(mockActiveUser(), mockActiveUser());
        when(groupPermission.getGroup()).thenReturn(new GroupJsonBean(groupName, null));
        when(groupManager.getGroup(groupName)).thenReturn(group);
        when(groupManager.getUsersInGroup(group)).thenReturn(groupUsers);

        assertEquals(groupUsers.size(), userListResolver.getShareCount());
    }

    @Test
    public void testUserCountWithProjectPermission() {
        setUpUserValidPermissionMocks();

        final FilterPermissionBean projectPermission = mock(FilterPermissionBean.class);
        when(projectPermission.getType()).thenReturn(ShareType.Name.PROJECT.toString());

        final UserListResolver userListResolver = new UserListResolver(authContext, userManager, groupManager, projectManager,
                permissionManager, projectRoleManager, schemeManager, userUtil, featureManager, Arrays.asList(projectPermission));

        final String projectName = "project";
        final Project project = mock(Project.class);
        final Collection<ApplicationUser> projectUsers = Arrays.asList(mockActiveUser(), mockActiveUser());
        final ProjectBean projectBean = mock(ProjectBean.class);
        when(projectBean.getName()).thenReturn(projectName);
        when(projectPermission.getProject()).thenReturn(projectBean);
        when(projectManager.getProjectObjByName(projectName)).thenReturn(project);
        when(schemeManager.getUsers((long) Permissions.BROWSE, project)).thenReturn(projectUsers);

        assertEquals(projectUsers.size(), userListResolver.getShareCount());
    }

    @Test(expected = LazyReference.InitializationException.class)
    public void testUserCountWithUnknownPermission() {
        setUpUserValidPermissionMocks();
        final FilterPermissionBean permission = mock(FilterPermissionBean.class);
        when(permission.getType()).thenReturn("nopermission");

        final UserListResolver userListResolver = new UserListResolver(authContext, userManager, groupManager, projectManager,
                permissionManager, projectRoleManager, schemeManager, userUtil, featureManager, Arrays.asList(permission));

        userListResolver.getShareCount();
    }

    @Test
    public void testUserCountShouldReturnZeroWhenTheUserCountIsDisabledByDarkFeature() {
        final UserListResolver userListResolver = new UserListResolver(null, null, null, null, null, null, null, null,
                featureManager, Arrays.asList());

        when(featureManager.isEnabled(UserListResolver.DO_NOT_COUNT_USERS_ON_SHARED_FILTER_FEATURE)).thenReturn(true);

        assertEquals(0, userListResolver.getShareCount());
    }

    private void setUpUserValidPermissionMocks() {
        when(authContext.getUser()).thenReturn(mock(ApplicationUser.class));
        when(permissionManager.hasPermission(eq(Permissions.USER_PICKER), isA(ApplicationUser.class))).thenReturn(Boolean.TRUE);
    }

    private ApplicationUser mockActiveUser() {
        ApplicationUser activeUser = mock(ApplicationUser.class);
        when(activeUser.isActive()).thenReturn(Boolean.TRUE);
        return activeUser;
    }

}