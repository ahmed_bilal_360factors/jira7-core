package com.atlassian.jira.rest.v2.admin;

import com.atlassian.jira.bc.admin.ApplicationPropertiesService;
import com.atlassian.jira.bc.admin.ApplicationProperty;
import com.atlassian.jira.bc.admin.ApplicationPropertyMetadata;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.validation.Success;
import com.atlassian.validation.Validated;
import org.apache.commons.httpclient.HttpStatus;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import javax.ws.rs.core.Response;
import java.util.ArrayList;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * Test for {@link ApplicationPropertiesResource}.
 *
 * @since v4.4
 */
public class ApplicationPropertiesResourceTest {
    @Rule
    public TestRule initMockitoMocks = new InitMockitoMocks(this);

    private static final MockApplicationUser USER = new MockApplicationUser("user");

    @Mock
    private JiraAuthenticationContext authContext;

    @Mock
    private PermissionManager permManager;

    @Mock
    private ApplicationPropertiesService service;

    @InjectMocks
    private ApplicationPropertiesResource resource;

    @Before
    public void setUp() throws Exception {
        when(authContext.getUser()).thenReturn(USER);
        when(authContext.getI18nHelper()).thenReturn(new MockI18nHelper());
    }

    @Test
    public void systemAdminCanGetApplicationProperty() {
        when(permManager.hasPermission(Permissions.SYSTEM_ADMIN, USER)).thenReturn(true);

        ApplicationProperty foobar = applicationProperty("foo", "bar");
        when(service.getApplicationProperty("foo")).thenReturn(foobar);

        Response response = resource.getProperty("foo", "SYSADMIN_ONLY", null);
        assertThat(response.getStatus(), equalTo(HttpStatus.SC_OK));

        ApplicationPropertiesResource.Property property = (ApplicationPropertiesResource.Property) response.getEntity();
        assertThat(property.getKey(), equalTo("foo"));
        assertThat(property.getValue(), equalTo("bar"));
    }

    @Test
    public void systemAdminCanGetAllApplicationProperties() {
        ArrayList<ApplicationProperty> props = new ArrayList<ApplicationProperty>();
        props.add(applicationProperty("foo", "bar"));
        props.add(applicationProperty("fred", "wilma"));
        props.add(applicationProperty("barney", "betty"));

        when(service.hasPermissionForLevel("SYSADMIN_ONLY")).thenReturn(true);
        when(service.getEditableApplicationProperties("SYSADMIN_ONLY", null)).thenReturn(props);

        Response response = resource.getProperty(null, "SYSADMIN_ONLY", null);
        assertThat(response.getStatus(), equalTo(HttpStatus.SC_OK));

        ArrayList<ApplicationPropertiesResource.Property> properties = (ArrayList<ApplicationPropertiesResource.Property>) response.getEntity();
        assertThat(properties.get(0).getKey(), equalTo("foo"));
        assertThat(properties.get(0).getValue(), equalTo("bar"));
        assertThat(properties.get(1).getKey(), equalTo("fred"));
        assertThat(properties.get(1).getValue(), equalTo("wilma"));
        assertThat(properties.get(2).getKey(), equalTo("barney"));
        assertThat(properties.get(2).getValue(), equalTo("betty"));
    }

    @Test
    public void systemAdminCanSetApplicationProperty() {
        when(permManager.hasPermission(Permissions.SYSTEM_ADMIN, USER)).thenReturn(true);

        ApplicationProperty updatedProp = applicationProperty("foo", "baz");
        Validated<ApplicationProperty> updated = new Validated<ApplicationProperty>(new Success("baz"), updatedProp);

        when(service.getApplicationProperty("foo")).thenReturn(updatedProp);
        when(service.setApplicationProperty("foo", "baz")).thenReturn(updated);

        Response response = resource.setProperty("foo", "baz");
        assertThat(response.getStatus(), equalTo(HttpStatus.SC_OK));

        ApplicationPropertiesResource.Property property = (ApplicationPropertiesResource.Property) response.getEntity();
        assertThat(property.getKey(), equalTo("foo"));
        assertThat(property.getValue(), equalTo("baz"));
    }

    private static ApplicationProperty applicationProperty(String key, String value) {
        ApplicationPropertyMetadata md = new ApplicationPropertyMetadata.Builder()
                .key(key)
                .type("string")
                .defaultValue(key + "default")
                .sysAdminEditable(true)
                .requiresRestart(false)
                .build();
        return new ApplicationProperty(md, value);
    }

}
