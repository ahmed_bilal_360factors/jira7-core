package com.atlassian.jira.rest.v2.issue.version;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.bc.project.ProjectAction;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.bc.project.version.VersionBuilder;
import com.atlassian.jira.bc.project.version.VersionService;
import com.atlassian.jira.mock.project.MockVersion;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionCustomFieldStore.CustomFieldUsageImpl;
import com.atlassian.jira.project.version.VersionImpl;
import com.atlassian.jira.rest.exception.NotFoundWebException;
import com.atlassian.jira.rest.v2.issue.VersionResource;
import com.atlassian.jira.rest.v2.issue.version.VersionIssueCountsBean.VersionUsageInCustomFields;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.DateFieldFormat;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.ErrorCollection.Reason;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.google.common.collect.ImmutableList;
import org.apache.commons.beanutils.BeanUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import javax.ws.rs.core.Response;
import java.time.Month;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static com.atlassian.jira.bc.project.version.VersionService.CreateVersionValidationResult.Reason.BAD_NAME;
import static com.atlassian.jira.bc.project.version.VersionService.CreateVersionValidationResult.Reason.FORBIDDEN;
import static com.atlassian.jira.rest.assertions.ResponseAssertions.assertResponseBody;
import static com.atlassian.jira.rest.assertions.ResponseAssertions.assertResponseCacheNever;
import static com.atlassian.jira.rest.assertions.ResponseAssertions.assertStatus;
import static java.util.EnumSet.of;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.4
 */
@RunWith(MockitoJUnitRunner.class)
public class TestVersionResource {
    @Mock
    private EventPublisher eventPublisher;

    @Test
    public void testGetVersionBadId() throws Exception {
        final VersionService versionService = mock(VersionService.class);

        final ProjectService projectService = mock(ProjectService.class);
        final VersionBeanFactory versionBeanFactory = mock(VersionBeanFactory.class);
        final JiraAuthenticationContext context = mock(JiraAuthenticationContext.class);
        final I18nHelper i18n = mock(I18nHelper.class);
        final DateFieldFormat dateFieldFormat = mock(DateFieldFormat.class);

        final String translation = "translation";
        final String badId = "badId";

        when(i18n.getText("admin.errors.version.not.exist.with.id", badId)).thenReturn(translation);

        final VersionResource resource = new VersionResource(versionService, projectService, context, i18n, null, versionBeanFactory, null, null, dateFieldFormat, eventPublisher);
        try {
            resource.getVersion(badId, null);
            fail("Expected exception.");
        } catch (NotFoundWebException e) {
            final Response re = e.getResponse();
            assertResponseCacheNever(re);
            assertResponseBody(com.atlassian.jira.rest.api.util.ErrorCollection.of(translation), re);
        }
    }

    @Test
    public void testGetVersionVersionDoesNotExist() throws Exception {
        final VersionService versionService = mock(VersionService.class);

        final VersionBeanFactory versionBeanFactory = mock(VersionBeanFactory.class);
        final ProjectService projectMgr = mock(ProjectService.class);
        final JiraAuthenticationContext context = mock(JiraAuthenticationContext.class);
        final I18nHelper i18n = mock(I18nHelper.class);
        final DateFieldFormat dateFieldFormat = mock(DateFieldFormat.class);

        final ApplicationUser user = new MockApplicationUser("BBain");
        final String error = "error";
        final long id = 1;

        when(context.getUser()).thenReturn(user);
        when(versionService.getVersionById(user, id)).thenReturn(new VersionService.VersionResult(errors(error)));

        final VersionResource resource = new VersionResource(versionService, projectMgr, context, i18n, null, versionBeanFactory, null, null, dateFieldFormat, eventPublisher);

        try {
            resource.getVersion(String.valueOf(id), null);
            fail("Expected exception.");
        } catch (NotFoundWebException e) {
            final Response re = e.getResponse();
            assertResponseCacheNever(re);
            assertResponseBody(com.atlassian.jira.rest.api.util.ErrorCollection.of(error).reason(Reason.VALIDATION_FAILED), re);
        }
    }

    @Test
    public void testGetVersionGoodVersion() throws Exception {
        final VersionService versionService = mock(VersionService.class);

        final VersionBeanFactory versionBeanFactory = mock(VersionBeanFactory.class);
        final JiraAuthenticationContext context = mock(JiraAuthenticationContext.class);
        final ProjectService projectService = mock(ProjectService.class);
        final I18nHelper i18n = mock(I18nHelper.class);
        final DateFieldFormat dateFieldFormat = mock(DateFieldFormat.class);

        final ApplicationUser user = new MockApplicationUser("BBain");
        final Version version = new MockVersion(1718L, "Version1");
        final long id = 1;

        final VersionBean versionBean = new VersionBean();

        when(context.getUser()).thenReturn(user);
        when(versionService.getVersionById(user, id)).thenReturn(new VersionService.VersionResult(ok(), version));
        when(versionBeanFactory.createVersionBean(version, false, false)).thenReturn(versionBean);

        final VersionResource resource = new VersionResource(versionService, projectService, context, i18n, null, versionBeanFactory, null, null, dateFieldFormat, eventPublisher);

        final Response response = resource.getVersion(String.valueOf(id), null);
        assertResponseCacheNever(response);
        assertResponseBody(versionBean, response);
    }

    @Test
    public void testCreateVersionWithoutProject() throws Exception {
        final VersionService versionService = mock(VersionService.class);

        final VersionBeanFactory versionBeanFactory = mock(VersionBeanFactory.class);
        final JiraAuthenticationContext context = mock(JiraAuthenticationContext.class);
        final ProjectService projectService = mock(ProjectService.class);
        final DateFieldFormat dateFieldFormat = mock(DateFieldFormat.class);

        final VersionResource resource = new VersionResource(versionService, projectService, context, new NoopI18nHelper(), null, versionBeanFactory, null, null, dateFieldFormat, eventPublisher);

        final Response response = resource.createVersion(new VersionBean.Builder().build());
        assertResponseCacheNever(response);
        assertResponseBody(restErrors(NoopI18nHelper.makeTranslation("rest.version.create.no.project")), response);
    }

    @Test
    public void testCreateVersionWithoutTwoReleaseDates() throws Exception {
        final VersionService versionService = mock(VersionService.class);

        final VersionBeanFactory versionBeanFactory = mock(VersionBeanFactory.class);
        final JiraAuthenticationContext context = mock(JiraAuthenticationContext.class);
        final ProjectService projectService = mock(ProjectService.class);
        final DateFieldFormat dateFieldFormat = mock(DateFieldFormat.class);

        final VersionResource resource = new VersionResource(versionService, projectService, context, new NoopI18nHelper(), null, versionBeanFactory, null, null, dateFieldFormat, eventPublisher);

        final VersionBean.Builder builder = new VersionBean.Builder().setProject("brenden").setProjectId(10000l)
                .setReleaseDate(new Date()).setUserReleaseDate("10/12/2001");
        final Response response = resource.createVersion(builder.build());
        assertResponseCacheNever(response);
        assertResponseBody(restErrors(NoopI18nHelper.makeTranslation("rest.version.create.two.release.dates")), response);
    }

    @Test
    public void testCreateVersionBadProject() throws Exception {
        final ApplicationUser user = new MockApplicationUser("BBain");
        final String key = "BJB";

        final VersionService versionService = mock(VersionService.class);

        final VersionBeanFactory versionBeanFactory = mock(VersionBeanFactory.class);
        final JiraAuthenticationContext context = mock(JiraAuthenticationContext.class);
        final ProjectService projectService = mock(ProjectService.class);
        final DateFieldFormat dateFieldFormat = mock(DateFieldFormat.class);

        when(context.getUser()).thenReturn(user);
        when(projectService.getProjectByKeyForAction(user, key, ProjectAction.EDIT_PROJECT_CONFIG))
                .thenReturn(new ProjectService.GetProjectResult(errors("Error"), null));

        final VersionResource resource = new VersionResource(versionService, projectService, context, new NoopI18nHelper(), null, versionBeanFactory, null, null, dateFieldFormat, eventPublisher);

        final VersionBean.Builder builder = new VersionBean.Builder().setProject(key);
        try {
            resource.createVersion(builder.build());
            fail("Expected an exception");
        } catch (NotFoundWebException e) {
            assertResponseCacheNever(e.getResponse());
            assertResponseBody(restErrors(NoopI18nHelper.makeTranslation("rest.version.no.create.permission", key)), e.getResponse());
        }
    }

    @Test
    public void testCreateVersionOkWithDate() throws Exception {
        final ApplicationUser user = new MockApplicationUser("BBain");
        final String key = "BJB";
        final MockProject project = new MockProject(2829L);
        final String name = "name";
        final String desc = "description";
        final Date startDate = new Date();
        final Date releaseDate = new Date();
        final VersionBean versionBean = new VersionBean();
        final boolean isRelease = false;

        final Version version = new MockVersion(171718L, "Test Version");

        final VersionService versionService = mock(VersionService.class);

        final VersionBeanFactory versionBeanFactory = mock(VersionBeanFactory.class);
        final JiraAuthenticationContext context = mock(JiraAuthenticationContext.class);
        final ProjectService projectService = mock(ProjectService.class);
        final DateFieldFormat dateFieldFormat = mock(DateFieldFormat.class);

        final VersionBuilder versionBuilder = mock(VersionBuilder.class);
        when(versionBuilder.projectId(project.getId())).thenReturn(versionBuilder);
        when(versionBuilder.name(name)).thenReturn(versionBuilder);
        when(versionBuilder.description(desc)).thenReturn(versionBuilder);
        when(versionBuilder.startDate(startDate)).thenReturn(versionBuilder);
        when(versionBuilder.releaseDate(releaseDate)).thenReturn(versionBuilder);
        when(versionBuilder.released(isRelease)).thenReturn(versionBuilder);

        final ErrorCollection errorCollection = new SimpleErrorCollection();
        final VersionService.VersionBuilderValidationResult validationResult = mock(VersionService.VersionBuilderValidationResult.class);
        when(validationResult.getErrorCollection()).thenReturn(errorCollection);
        when(validationResult.isValid()).thenReturn(true);

        when(context.getUser()).thenReturn(user);
        when(projectService.getProjectByIdForAction(user, project.getId(), ProjectAction.EDIT_PROJECT_CONFIG))
                .thenReturn(new ProjectService.GetProjectResult(ok(), project));
        when(versionService.newVersionBuilder())
                .thenReturn(versionBuilder);
        when(versionService.validateCreate(user, versionBuilder))
                .thenReturn(validationResult);
        when(versionService.create(user, validationResult))
                .thenReturn(ServiceOutcomeImpl.ok(version));
        when(versionBeanFactory.createVersionBean(version, false, false)).thenReturn(versionBean);

        final VersionResource resource = new VersionResource(versionService, projectService, context, new NoopI18nHelper(), null, versionBeanFactory, null, null, dateFieldFormat, eventPublisher);
        final VersionBean.Builder builder = new VersionBean.Builder().setProject(key).setProjectId(project.getId()).setName(name).setDescription(desc)
                .setStartDate(startDate).setReleaseDate(releaseDate).setReleased(isRelease);

        final Response actualResponse = resource.createVersion(builder.build());
        assertResponseCacheNever(actualResponse);
        assertResponseBody(versionBean, actualResponse);
    }

    @Test
    public void testCreateVersionOkWithUserDate() throws Exception {
        final ApplicationUser user = new MockApplicationUser("BBain");
        final String key = "BJB";
        final MockProject project = new MockProject(2829L);
        final String name = "name";
        final String desc = "description";
        final String startDateStr = "28829291";
        final Date startDate = new Date();
        final String releaseDateStr = "28829292";
        final Date releaseDate = new Date();
        final VersionBean versionBean = new VersionBean();
        final boolean isRelease = false;

        final Version version = new MockVersion(171718L, "Test Version");

        final VersionService versionService = mock(VersionService.class);

        final VersionBeanFactory versionBeanFactory = mock(VersionBeanFactory.class);
        final JiraAuthenticationContext context = mock(JiraAuthenticationContext.class);
        final ProjectService projectService = mock(ProjectService.class);
        final DateFieldFormat dateFieldFormat = mock(DateFieldFormat.class);

        when(dateFieldFormat.parseDatePicker(startDateStr)).thenReturn(startDate);
        when(dateFieldFormat.parseDatePicker(releaseDateStr)).thenReturn(releaseDate);

        VersionBuilder versionBuilder = mock(VersionBuilder.class);
        when(versionBuilder.projectId(project.getId())).thenReturn(versionBuilder);
        when(versionBuilder.name(name)).thenReturn(versionBuilder);
        when(versionBuilder.description(desc)).thenReturn(versionBuilder);
        when(versionBuilder.startDate(startDate)).thenReturn(versionBuilder);
        when(versionBuilder.releaseDate(releaseDate)).thenReturn(versionBuilder);
        when(versionBuilder.released(isRelease)).thenReturn(versionBuilder);

        final ErrorCollection errorCollection = new SimpleErrorCollection();
        final VersionService.VersionBuilderValidationResult validationResult = mock(VersionService.VersionBuilderValidationResult.class);
        when(validationResult.getErrorCollection()).thenReturn(errorCollection);
        when(validationResult.isValid()).thenReturn(true);

        when(context.getUser()).thenReturn(user);
        when(projectService.getProjectByIdForAction(user, project.getId(), ProjectAction.EDIT_PROJECT_CONFIG))
                .thenReturn(new ProjectService.GetProjectResult(ok(), project));
        when(versionService.newVersionBuilder())
                .thenReturn(versionBuilder);
        when(versionService.validateCreate(user, versionBuilder))
                .thenReturn(validationResult);
        when(versionService.create(user, validationResult))
                .thenReturn(ServiceOutcomeImpl.ok(version));
        when(versionBeanFactory.createVersionBean(version, false, false)).thenReturn(versionBean);

        final VersionResource resource = new VersionResource(versionService, projectService, context, new NoopI18nHelper(), null, versionBeanFactory, null, null, dateFieldFormat, eventPublisher);
        final VersionBean.Builder builder = new VersionBean.Builder().setProject(key).setProjectId(project.getId()).setName(name).setDescription(desc)
                .setUserStartDate(startDateStr).setUserReleaseDate(releaseDateStr).setReleased(isRelease);

        final Response actualResponse = resource.createVersion(builder.build());
        assertResponseCacheNever(actualResponse);
        assertResponseBody(versionBean, actualResponse);
    }

    @Test
    public void testCreateVersionValidationAuthErrors() throws Exception {
        final ApplicationUser user = new MockApplicationUser("BBain");
        final String key = "BJB";
        final String name = "name";
        final MockProject project = new MockProject(2829L);
        final boolean isRelease = false;

        final VersionService versionService = mock(VersionService.class);

        final VersionBeanFactory versionBeanFactory = mock(VersionBeanFactory.class);
        final JiraAuthenticationContext context = mock(JiraAuthenticationContext.class);
        final ProjectService projectService = mock(ProjectService.class);
        final DateFieldFormat dateFieldFormat = mock(DateFieldFormat.class);

        final VersionBuilder versionBuilder = mock(VersionBuilder.class);
        when(versionBuilder.projectId(project.getId())).thenReturn(versionBuilder);
        when(versionBuilder.name(name)).thenReturn(versionBuilder);
        when(versionBuilder.description(null)).thenReturn(versionBuilder);
        when(versionBuilder.startDate(null)).thenReturn(versionBuilder);
        when(versionBuilder.releaseDate(null)).thenReturn(versionBuilder);
        when(versionBuilder.released(isRelease)).thenReturn(versionBuilder);

        final ErrorCollection errorCollection = new SimpleErrorCollection();
        final VersionService.VersionBuilderValidationResult validationResult = mock(VersionService.VersionBuilderValidationResult.class);
        when(validationResult.getErrorCollection()).thenReturn(errorCollection);
        when(validationResult.getErrorCollection()).thenReturn(errorCollection);
        when(validationResult.isValid()).thenReturn(false);
        when(validationResult.getSpecificReasons()).thenReturn(of(FORBIDDEN));

        when(context.getUser()).thenReturn(user);
        when(projectService.getProjectByIdForAction(user, project.getId(), ProjectAction.EDIT_PROJECT_CONFIG))
                .thenReturn(new ProjectService.GetProjectResult(ok(), project));
        when(versionService.newVersionBuilder())
                .thenReturn(versionBuilder);
        when(versionService.validateCreate(user, versionBuilder))
                .thenReturn(validationResult);

        final VersionResource resource = new VersionResource(versionService, projectService, context, new NoopI18nHelper(), null, versionBeanFactory, null, null, dateFieldFormat, eventPublisher);
        final VersionBean.Builder builder = new VersionBean.Builder().setProject(key).setProjectId(project.getId()).setName(name).setReleased(isRelease);

        final Response actualResponse = resource.createVersion(builder.build());
        assertResponseCacheNever(actualResponse);
        assertResponseBody(restErrors(NoopI18nHelper.makeTranslation("rest.version.no.create.permission", key)), actualResponse);
        assertStatus(Response.Status.NOT_FOUND, actualResponse);
    }

    @Test
    public void testCreateVersionValidationOtherErrors() throws Exception {
        final ApplicationUser user = new MockApplicationUser("BBain");
        final String errorMessage = "errorMessage";
        final String key = "BJB";
        final String name = "name";
        final MockProject project = new MockProject(2829L);
        final boolean isRelease = false;

        final VersionService versionService = mock(VersionService.class);
        final VersionBeanFactory versionBeanFactory = mock(VersionBeanFactory.class);
        final JiraAuthenticationContext context = mock(JiraAuthenticationContext.class);
        final ProjectService projectService = mock(ProjectService.class);
        final DateFieldFormat dateFieldFormat = mock(DateFieldFormat.class);

        final VersionBuilder versionBuilder = mock(VersionBuilder.class);
        when(versionBuilder.projectId(project.getId())).thenReturn(versionBuilder);
        when(versionBuilder.name(name)).thenReturn(versionBuilder);
        when(versionBuilder.description(null)).thenReturn(versionBuilder);
        when(versionBuilder.startDate(null)).thenReturn(versionBuilder);
        when(versionBuilder.releaseDate(null)).thenReturn(versionBuilder);
        when(versionBuilder.released(isRelease)).thenReturn(versionBuilder);

        final ErrorCollection errorCollection = new SimpleErrorCollection();
        final VersionService.VersionBuilderValidationResult validationResult = mock(VersionService.VersionBuilderValidationResult.class);
        when(validationResult.getErrorCollection()).thenReturn(errorCollection);
        when(validationResult.isValid()).thenReturn(false);
        when(validationResult.getSpecificReasons()).thenReturn(of(BAD_NAME));
        when(validationResult.getErrorCollection()).thenReturn(errors(errorMessage));

        when(context.getUser()).thenReturn(user);
        when(projectService.getProjectByIdForAction(user, project.getId(), ProjectAction.EDIT_PROJECT_CONFIG))
                .thenReturn(new ProjectService.GetProjectResult(ok(), project));
        when(versionService.newVersionBuilder())
                .thenReturn(versionBuilder);
        when(versionService.validateCreate(user, versionBuilder))
                .thenReturn(validationResult);

        final VersionResource resource = new VersionResource(versionService, projectService, context, new NoopI18nHelper(), null, versionBeanFactory, null, null, dateFieldFormat, eventPublisher);
        final VersionBean.Builder builder = new VersionBean.Builder().setProject(key).setProjectId(project.getId()).setName(name).setReleased(isRelease);
        final Response actualResponse = resource.createVersion(builder.build());
        assertResponseCacheNever(actualResponse);
        assertResponseBody(restErrors(Reason.VALIDATION_FAILED, errorMessage), actualResponse);
        assertStatus(Response.Status.BAD_REQUEST, actualResponse);
    }

    @Test
    public void isAbleToCreateAVersionThatHasAlreadyBeenReleasedOnAnSpecifiedReleaseDate() throws Exception {
        final long PROJECT_LONG_ID = 123456L;
        VersionBean inputParamVersionBean = new VersionBean.Builder().setProject("BJB")
                .setProjectId(PROJECT_LONG_ID)
                .setName("Test Version X")
                .setReleased(Boolean.TRUE)
                .setDescription("A test release version")
                .setUserStartDate(java.time.LocalDate.of(2015, Month.OCTOBER, 20).toString())
                .setUserReleaseDate(java.time.LocalDate.of(2015, Month.OCTOBER, 21).toString())
                .build();

        VersionBean expectedVersionBean = (VersionBean) BeanUtils.cloneBean(inputParamVersionBean);

        DateFieldFormat dateFieldFormatMock = mock(DateFieldFormat.class);

        MockProject projectMock = createProjectMock(PROJECT_LONG_ID);
        ApplicationUser applicationUserMock = createApplicationUserMock("BBain");

        MockVersion versionMock = createVersionMock(inputParamVersionBean, projectMock);
        JiraAuthenticationContext jiraAuthContextMock = createJiraAuthenticationContextMock(applicationUserMock);
        ProjectService projectServiceMock = createProjectServiceForEditConfigMock(applicationUserMock, projectMock);
        VersionBuilder versionBuilderMock = createVersionBuilderMock(versionMock);
        VersionService versionServiceMock = createReleaseVersionServiceMock(applicationUserMock, versionMock, versionBuilderMock);

        VersionBeanFactory versionBeanFactoryMock = createVersionBeanFactoryMock(versionMock, expectedVersionBean);

        VersionResource resource = new VersionResource(versionServiceMock, projectServiceMock, jiraAuthContextMock,
                new NoopI18nHelper(), null, versionBeanFactoryMock, null, null,
                dateFieldFormatMock, eventPublisher);

        Response response = resource.createVersion(inputParamVersionBean);

        assertResponseCacheNever(response);
        assertResponseBody(expectedVersionBean, response);
    }

    @Test
    public void shouldReturnVersionUsageForGivenVersion() throws Exception {
        final VersionService versionService = mock(VersionService.class);
        final JiraAuthenticationContext authContext = mock(JiraAuthenticationContext.class);
        final VersionIssueCountsBeanFactory versionIssueCountsBeanFactory = mock(VersionIssueCountsBeanFactory.class);
        final VersionIssueCountsBean expectedVersionIssueCounts = new VersionIssueCountsBean.Builder()
                .issuesFixedCount(1L)
                .issueCountWithCustomFieldsShowingVersion(2L)
                .issuesAffectedCount(3L)
                .customFieldUsage(ImmutableList.of(
                        new VersionUsageInCustomFields(10000, "Field1", 1),
                        new VersionUsageInCustomFields(10001, "Field2", 1)))
                .build();
        final Version version = new VersionImpl(1L, 1L, "Test");
        final ApplicationUser user = new MockApplicationUser("testUser");
        final VersionService.VersionResult versionResult = new VersionService.VersionResult(new SimpleErrorCollection(), version);
        final VersionResource resource = new VersionResource(
                versionService, null, authContext, null, null, null, versionIssueCountsBeanFactory, null, null, null);

        when(versionService.getFixIssuesCount(version)).thenReturn(1L);
        when(versionService.getAffectsIssuesCount(version)).thenReturn(3L);
        when(versionService.getCustomFieldIssuesCount(version)).thenReturn(2L);
        when(versionService.getCustomFieldsUsing(version)).thenReturn(ImmutableList.of(
                new CustomFieldUsageImpl(10000, "Field1", 1),
                new CustomFieldUsageImpl(10001, "Field2", 1)
        ));

        when(authContext.getUser()).thenReturn(user);
        when(versionService.getVersionById(user, 1L)).thenReturn(versionResult);
        when(versionIssueCountsBeanFactory.createVersionBean(
                Mockito.any(Version.class), Mockito.anyLong(), Mockito.anyLong(), Mockito.anyLong(), Mockito.anyList())).then(invocation -> {
            final Object[] arguments = invocation.getArguments();
            return new VersionIssueCountsBean.Builder()
                    .issuesFixedCount((Long) arguments[1])
                    .issuesAffectedCount((Long) arguments[2])
                    .issueCountWithCustomFieldsShowingVersion((Long) arguments[3])
                    .customFieldUsage((List) arguments[4])
                    .build();
        });

        final Response response = resource.getVersionRelatedIssues("1");
        final VersionIssueCountsBean actualVersionIssueCounts = (VersionIssueCountsBean) response.getEntity();

        assertThat(actualVersionIssueCounts, is(equalTo(expectedVersionIssueCounts)));
    }

    private MockProject createProjectMock(long projectId) {
        return new MockProject(projectId);
    }

    private ApplicationUser createApplicationUserMock(String userName) {
        return new MockApplicationUser(userName);
    }

    private JiraAuthenticationContext createJiraAuthenticationContextMock(ApplicationUser mockApplicationUser) {
        JiraAuthenticationContext mockJiraAuthContext = mock(JiraAuthenticationContext.class);
        when(mockJiraAuthContext.getUser()).thenReturn(mockApplicationUser);
        when(mockJiraAuthContext.getLoggedInUser()).thenReturn(mockApplicationUser);
        return mockJiraAuthContext;
    }

    private ProjectService createProjectServiceForEditConfigMock(ApplicationUser applicationUserMock, MockProject projectMock) {
        ProjectService projectServiceMock = mock(ProjectService.class);
        when(projectServiceMock.getProjectByIdForAction(applicationUserMock, projectMock.getId(), ProjectAction.EDIT_PROJECT_CONFIG))
                .thenReturn(new ProjectService.GetProjectResult(ok(), projectMock));
        return projectServiceMock;
    }

    private VersionService.VersionBuilderValidationResult createValidationResultMock() {
        VersionService.VersionBuilderValidationResult validationResult = mock(VersionService.VersionBuilderValidationResult.class);
        when(validationResult.getErrorCollection()).thenReturn(new SimpleErrorCollection());
        when(validationResult.isValid()).thenReturn(true);
        return validationResult;
    }

    private VersionBuilder createVersionBuilderMock(MockVersion versionToBuild) {
        VersionBuilder versionBuilderMock = mock(VersionBuilder.class);

        when(versionBuilderMock.archived(anyBoolean())).thenReturn(versionBuilderMock);
        when(versionBuilderMock.description(anyString())).thenReturn(versionBuilderMock);
        when(versionBuilderMock.name(anyString())).thenReturn(versionBuilderMock);
        when(versionBuilderMock.projectId(anyLong())).thenReturn(versionBuilderMock);
        when(versionBuilderMock.released(anyBoolean())).thenReturn(versionBuilderMock);
        when(versionBuilderMock.releaseDate(anyObject())).thenReturn(versionBuilderMock);
        when(versionBuilderMock.scheduleAfterVersion(anyLong())).thenReturn(versionBuilderMock);
        when(versionBuilderMock.sequence(anyLong())).thenReturn(versionBuilderMock);
        when(versionBuilderMock.startDate(anyObject())).thenReturn(versionBuilderMock);

        when(versionBuilderMock.build()).thenReturn(versionToBuild);

        return versionBuilderMock;
    }

    private VersionService createReleaseVersionServiceMock(ApplicationUser applicationUserMock,
                                                           MockVersion versionMock,
                                                           VersionBuilder versionBuilderMock) {
        VersionService.VersionBuilderValidationResult validationResultMock = createValidationResultMock();

        VersionService versionService = mock(VersionService.class);
        when(versionService.newVersionBuilder()).thenReturn(versionBuilderMock);
        when(versionService.validateCreate(applicationUserMock, versionBuilderMock)).thenReturn(validationResultMock);
        when(versionService.create(applicationUserMock, validationResultMock)).thenReturn(ServiceOutcomeImpl.ok(versionMock));
        return versionService;
    }

    private MockVersion createVersionMock(VersionBean versionBean, MockProject projectMock) {
        MockVersion versionMock = new MockVersion(versionBean.getProjectId(), versionBean.getName());
        versionMock.setProjectObject(projectMock);
        versionMock.setArchived(versionBean.isArchived());
        versionMock.setDescription(versionBean.getDescription());
        versionMock.setReleased(versionBean.isReleased());
        versionMock.setStartDate(versionBean.getStartDate());
        versionMock.setReleaseDate(versionBean.getReleaseDate());
        return versionMock;
    }

    private VersionBeanFactory createVersionBeanFactoryMock(MockVersion versionMock, VersionBean expectedResultBean) {
        VersionBeanFactory versionBeanFactoryMock = mock(VersionBeanFactory.class);
        when(versionBeanFactoryMock.createVersionBean(eq(versionMock), anyBoolean(), anyBoolean())).thenReturn(expectedResultBean);
        return versionBeanFactoryMock;
    }

    private static com.atlassian.jira.rest.api.util.ErrorCollection restErrors(String... errors) {
        return com.atlassian.jira.rest.api.util.ErrorCollection.of(errors);
    }

    private static com.atlassian.jira.rest.api.util.ErrorCollection restErrors(ErrorCollection.Reason reason, String... errors) {
        return com.atlassian.jira.rest.api.util.ErrorCollection.of(errors).reason(reason);
    }

    private static ErrorCollection errors(String... errors) {
        final SimpleErrorCollection collection = new SimpleErrorCollection();
        collection.addErrorMessages(Arrays.asList(errors));
        return collection;
    }

    private static ErrorCollection ok() {
        return new SimpleErrorCollection();
    }
}
