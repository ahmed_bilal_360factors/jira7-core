package com.atlassian.jira.rest.v2.issue;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.impl.AbstractCustomFieldType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldException;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.NavigableField;
import com.atlassian.jira.issue.fields.OrderableField;
import com.atlassian.jira.issue.fields.ProjectSystemField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayout;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import com.atlassian.jira.issue.fields.rest.FieldJsonRepresentation;
import com.atlassian.jira.issue.fields.rest.RestAwareField;
import com.atlassian.jira.issue.fields.rest.json.JsonData;
import com.atlassian.jira.issue.fields.rest.json.JsonType;
import com.atlassian.jira.issue.fields.rest.json.JsonTypeBuilder;
import com.atlassian.jira.issue.fields.rest.json.beans.IssueLinksBeanBuilder;
import com.atlassian.jira.issue.fields.rest.json.beans.IssueLinksBeanBuilderFactory;
import com.atlassian.jira.issue.fields.rest.json.beans.IssueRefJsonBean;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.plugin.customfield.CustomFieldRestSerializer;
import com.atlassian.jira.plugin.customfield.CustomFieldTypeModuleDescriptor;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.rest.api.issue.JsonTypeBean;
import com.atlassian.jira.rest.v2.issue.builder.BeanBuilderFactory;
import com.atlassian.jira.rest.v2.issue.builder.ChangelogBeanBuilder;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.workflow.IssueWorkflowManager;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.opensymphony.workflow.loader.ActionDescriptor;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.rest.v2.issue.IssueBeanBuilder2.FIRST_REPRESENTATION_VERSION;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.withSettings;

/**
 * @since v6.0.3
 */
public class IssueBeanBuilder2Test {
    @Mock
    private FieldLayoutManager fieldLayoutManager;
    @Mock
    private JiraAuthenticationContext authContext;
    @Mock
    private FieldManager fieldManager;
    @Mock
    private ResourceUriBuilder resourceUriBuilder;
    @Mock
    private BeanBuilderFactory beanBuilderFactory;
    @Mock
    private IncludedFields fieldsToInclude;
    @Mock
    private Issue issue;
    @Mock
    private IssueLinksBeanBuilderFactory issueLinkBeanBuilderFactory;
    @Mock
    private IssueWorkflowManager issueWorkflowManager;
    @Mock
    private UriBuilder mock;
    @Mock
    private I18nHelper i18nHelper;

    @Rule
    public InitMockitoMocks initMockitoMocks = new InitMockitoMocks(this);

    private IssueBeanBuilder2 issueBeanBuilder;
    private final String fieldId = "id";
    private final Object fieldData = "data";
    private final String fieldName = "name";

    @Before
    public void setUp() throws Exception {
        issueBeanBuilder = createBuilderWithExpand(null);

        when(issue.getParentId()).thenReturn(null);
        when(fieldsToInclude.included(any(Field.class))).thenReturn(true);
        when(fieldsToInclude.included(anyString(), anyBoolean())).thenReturn(true);

        FieldLayout fieldLayout = mock(FieldLayout.class);
        //noinspection unchecked
        when(fieldLayout.getVisibleLayoutItems(any(Project.class), any(List.class))).thenReturn(Collections.<FieldLayoutItem>emptyList());
        when(fieldLayoutManager.getFieldLayout(issue)).thenReturn(fieldLayout);

        IssueLinksBeanBuilder linksBeanBuilder = mock(IssueLinksBeanBuilder.class);
        when(linksBeanBuilder.buildIssueLinks()).thenReturn(null);
        when(issueLinkBeanBuilderFactory.newIssueLinksBeanBuilder(issue)).thenReturn(linksBeanBuilder);

        when(authContext.getI18nHelper()).thenReturn(i18nHelper);
    }

    @Test
    public void testBuildingIssueData() {
        final Long issueId = 10000L;
        final String issueKey = "issueKey";
        final URI self = URI.create("issue-self");
        when(issue.getId()).thenReturn(issueId);
        when(issue.getKey()).thenReturn(issueKey);
        when(resourceUriBuilder.build(any(UriBuilder.class), eq(IssueResource.class), eq(issueId.toString()))).thenReturn(self);

        final IssueBean bean = issueBeanBuilder.build(issue);

        assertThat(bean.getId(), is(issueId.toString()));
        assertThat(bean.getKey(), is(issueKey));
        assertThat(bean.getSelf(), is(self));
    }

    @Test
    public void testBuildingFields() {
        prepareFieldLayoutManager(createOrderableFieldInLayoutItem(fieldId, fieldName, fieldData));

        final IssueBean bean = issueBeanBuilder.build(issue);

        assertThat(bean.getFields(), isMap(fieldId, fieldData));
    }

    @Test
    public void testBuildingOnlySelectedFields() {
        final String fieldId2 = "id2";
        final FieldLayoutItem fieldInLayoutItem1 = createOrderableFieldInLayoutItem(fieldId, fieldName, fieldData);
        final FieldLayoutItem fieldInLayoutItem2 = createOrderableFieldInLayoutItem(fieldId2, "name2", "data2");

        when(fieldsToInclude.included(fieldInLayoutItem1.getOrderableField())).thenReturn(true);
        when(fieldsToInclude.included(fieldInLayoutItem2.getOrderableField())).thenReturn(false);

        prepareFieldLayoutManager(fieldInLayoutItem1, fieldInLayoutItem2);

        final IssueBean bean = issueBeanBuilder.build(issue);

        assertThat(bean.getFields(), isMap(fieldId, fieldData));
    }

    @Test
    public void testBuildNavigableFields() throws FieldException {
        final NavigableField field = mock(NavigableField.class, withSettings().extraInterfaces(RestAwareField.class));
        prepareField(field, fieldId, fieldName, fieldData, null);
        when(fieldManager.getAvailableNavigableFields(any(ApplicationUser.class))).thenReturn(ImmutableSet.of(field));

        final IssueBean bean = issueBeanBuilder.build(issue);

        assertThat(bean.getFields(), isMap(fieldId, fieldData));
    }

    @Test
    public void testBuildNavigableFieldWhenOrderableIsAlreadySet() throws FieldException {
        final FieldJsonRepresentation navigableJsonRepresentation = new FieldJsonRepresentation(new JsonData("navigable data"));
        final NavigableField field = mock(NavigableField.class, withSettings().extraInterfaces(RestAwareField.class, OrderableField.class));
        final FieldLayoutItem fieldLayoutItem = mock(FieldLayoutItem.class);
        prepareField(field, fieldId, fieldName, fieldData, fieldLayoutItem);
        when(((RestAwareField) field).getJsonFromIssue(issue, false, null)).thenReturn(navigableJsonRepresentation);
        when(fieldLayoutItem.getOrderableField()).thenReturn((OrderableField) field);

        prepareFieldLayoutManager(fieldLayoutItem);
        when(fieldManager.getAvailableNavigableFields(any(ApplicationUser.class))).thenReturn(ImmutableSet.of(field));

        final IssueBean bean = issueBeanBuilder.build(issue);

        assertThat(bean.getFields(), isMap(fieldId, fieldData));
    }

    @Test
    public void testBuildNavigableFieldsWhichIsAlsoOrderable() throws FieldException {
        final NavigableField field = mock(NavigableField.class, withSettings().extraInterfaces(RestAwareField.class, OrderableField.class));
        prepareField(field, fieldId, fieldName, fieldData, null);
        when(fieldManager.getAvailableNavigableFields(any(ApplicationUser.class))).thenReturn(ImmutableSet.of(field));

        final IssueBean bean = issueBeanBuilder.build(issue);

        assertThat(bean.getFields(), nullValue());
    }

    @Test
    public void testBuildSpecialNavigableFields() throws FieldException {
        final ProjectSystemField field = mock(ProjectSystemField.class);
        prepareField(field, fieldId, fieldName, fieldData, null);
        when(fieldManager.getAvailableNavigableFields(any(ApplicationUser.class))).thenReturn(ImmutableSet.of((NavigableField) field));

        final IssueBean bean = issueBeanBuilder.build(issue);

        assertThat(bean.getFields(), isMap(fieldId, fieldData));
    }

    @Test
    public void testBuildingParentLink() {
        final Object parentData = prepareParentLink();

        final IssueBean bean = issueBeanBuilder.build(issue);

        assertThat(bean.getFields(), isMap("parent", parentData));
    }

    @Test
    public void testBuildingRenderedFields() {
        issueBeanBuilder = createBuilderWithExpand("renderedFields");
        final Object renderedData = "rendered data";
        final FieldLayoutItem fieldLayoutItem = createOrderableFieldInLayoutItem(fieldId, fieldName, fieldData);
        final OrderableField field = fieldLayoutItem.getOrderableField();
        final FieldJsonRepresentation fieldJsonRepresentation = new FieldJsonRepresentation(new JsonData(fieldData), new JsonData(renderedData));
        when(((RestAwareField) field).getJsonFromIssue(issue, true, fieldLayoutItem)).thenReturn(fieldJsonRepresentation);
        prepareFieldLayoutManager(fieldLayoutItem);

        final IssueBean bean = issueBeanBuilder.build(issue);

        assertThat(bean.getRenderedFields(), isMap(fieldId, renderedData));
    }

    @Test
    public void testBuildingRenderedFieldsForNullData() {
        issueBeanBuilder = createBuilderWithExpand("renderedFields");
        final Object renderedData = null;
        final FieldLayoutItem fieldLayoutItem = createOrderableFieldInLayoutItem(fieldId, fieldName, fieldData);
        final OrderableField field = fieldLayoutItem.getOrderableField();
        final FieldJsonRepresentation fieldJsonRepresentation = new FieldJsonRepresentation(new JsonData(fieldData), new JsonData(renderedData));
        when(((RestAwareField) field).getJsonFromIssue(issue, true, fieldLayoutItem)).thenReturn(fieldJsonRepresentation);
        prepareFieldLayoutManager(fieldLayoutItem);

        final IssueBean bean = issueBeanBuilder.build(issue);

        final Map<String, Object> expectedData = new HashMap<String, Object>();
        expectedData.put(fieldId, renderedData);
        assertThat(bean.getRenderedFields(), is(expectedData));
    }

    @Test
    public void testBuildingNames() {
        issueBeanBuilder = createBuilderWithExpand("names");
        prepareFieldLayoutManager(createOrderableFieldInLayoutItem(fieldId, fieldName, fieldData));

        final IssueBean bean = issueBeanBuilder.build(issue);

        assertThat(bean.getNames(), isMap(fieldId, fieldName));
    }

    @Test
    public void testBuildingSchema() {
        issueBeanBuilder = createBuilderWithExpand("schema");
        final FieldLayoutItem fieldLayoutItem = createOrderableFieldInLayoutItem(fieldId, fieldName, fieldData);
        prepareFieldLayoutManager(fieldLayoutItem);
        final OrderableField field = fieldLayoutItem.getOrderableField();
        final JsonType jsonType = JsonTypeBuilder.system("type", "system");
        when(((RestAwareField) field).getJsonSchema()).thenReturn(jsonType);

        final IssueBean bean = issueBeanBuilder.build(issue);

        assertThat(bean.getSchema(), isMap(fieldId, new JsonTypeBean(jsonType)));
    }

    @Test
    public void exceptionsThrownFromCustomFieldsShouldBeIgnored() {
        final FieldLayoutItem fieldInLayoutItem = createOrderableFieldInLayoutItem(fieldId, fieldName, fieldData);
        final RestAwareField field = (RestAwareField) fieldInLayoutItem.getOrderableField();
        when((field).getJsonFromIssue(issue, false, fieldInLayoutItem)).thenThrow(new RuntimeException());
        prepareFieldLayoutManager(fieldInLayoutItem);

        try {
            final IssueBean bean = issueBeanBuilder.build(issue);
            assertThat("Exception should be translated to null", bean.getFields(), nullValue());
        } catch (RuntimeException e) {
            fail("Exception should not propagate");
        }
    }

    @Test
    public void testExtraRestRepresentations() throws FieldException {
        issueBeanBuilder = createBuilderWithExpand("versionedRepresentations");
        final Integer version2 = 2;
        final Object data1 = "extra data2";
        final Integer version3 = 3;
        final Object data2 = "extra data3";

        final CustomField field = mock(CustomField.class);
        final CustomFieldRestSerializer restSerializer1 = createRestSerializer(field, data1);
        final CustomFieldRestSerializer restSerializer2 = createRestSerializer(field, data2);
        final FieldLayoutItem fieldLayoutItem = createCustomFieldWithRestSerializer(field, fieldId, fieldName, fieldData,
                ImmutableMap.of(version2, restSerializer1, version3, restSerializer2));
        prepareFieldLayoutManager(fieldLayoutItem);

        final Map<Integer, Object> versionedRepresentations = issueBeanBuilder.build(issue).getVersionedRepresentations().get(fieldId);
        assertThat(versionedRepresentations.size(), is(3));
        assertThat(versionedRepresentations, hasEntry(FIRST_REPRESENTATION_VERSION, fieldData));
        assertThat(versionedRepresentations, hasEntry(version2, data1));
        assertThat(versionedRepresentations, hasEntry(version3, data2));
    }

    @Test
    public void testFieldHaveOriginalRepresentationsInVersionedRepresentations() throws FieldException {
        issueBeanBuilder = createBuilderWithExpand("versionedRepresentations");
        prepareFieldLayoutManager(createOrderableFieldInLayoutItem(fieldId, fieldName, fieldData));

        final Map<Integer, Object> versionedRepresentations = issueBeanBuilder.build(issue).getVersionedRepresentations().get(fieldId);

        assertThat(versionedRepresentations.size(), is(1));
        assertThat(versionedRepresentations, hasEntry(FIRST_REPRESENTATION_VERSION, fieldData));
    }

    @Test
    public void testFieldHaveOriginalRepresentationsInVersionedRepresentationEvenIfItDoesNotHaveData()
            throws FieldException {
        issueBeanBuilder = createBuilderWithExpand("versionedRepresentations");
        prepareFieldLayoutManager(createOrderableFieldInLayoutItem(fieldId, fieldName, null));

        final Map<Integer, Object> versionedRepresentations = issueBeanBuilder.build(issue).getVersionedRepresentations().get(fieldId);

        assertThat(versionedRepresentations.size(), is(1));
        assertThat(versionedRepresentations, hasEntry(FIRST_REPRESENTATION_VERSION, null));
    }

    @Test
    public void testHidingFieldsWhenVersionedRepresentationsAreExpanded() {
        issueBeanBuilder = createBuilderWithExpand("versionedRepresentations");
        prepareFieldLayoutManager(createOrderableFieldInLayoutItem(fieldId, fieldName, fieldData));

        final IssueBean bean = issueBeanBuilder.build(issue);
        final Map<Integer, Object> versionedRepresentations = bean.getVersionedRepresentations().get(fieldId);

        assertThat(bean.getFields(), Matchers.nullValue());
        assertThat(versionedRepresentations.size(), is(1));
        assertThat(versionedRepresentations, hasEntry(FIRST_REPRESENTATION_VERSION, fieldData));
    }

    @Test
    public void exceptionsThrownFromRestSerializerShouldBeIgnored() {
        final CustomField field = mock(CustomField.class);
        final CustomFieldRestSerializer RestSerializer = createRestSerializer(field, new JsonData("extra data2"));
        final FieldLayoutItem fieldLayoutItem = createCustomFieldWithRestSerializer(field, fieldId, fieldName, fieldData,
                ImmutableMap.of(2, RestSerializer));
        when(RestSerializer.getJsonData(field, issue)).thenThrow(new RuntimeException());
        prepareFieldLayoutManager(fieldLayoutItem);

        try {
            final IssueBean bean = issueBeanBuilder.build(issue);
            assertThat("Exception should be translated to null", bean.getFields(), isMap(fieldId, fieldData));
        } catch (RuntimeException e) {
            fail("Exception should not propagate");
        }
    }

    @Test
    public void testBuildingParentLinkSchema() {
        issueBeanBuilder = createBuilderWithExpand("schema");
        prepareParentLink();

        final IssueBean bean = issueBeanBuilder.build(issue);

        final JsonTypeBean schema = new JsonTypeBean(JsonType.ARRAY_TYPE, JsonType.ISSUELINKS_TYPE, "parent", null, null);
        assertThat(bean.getSchema(), isMap("parent", schema));
    }

    @Test
    public void testBuildingParentLinkName() {
        issueBeanBuilder = createBuilderWithExpand("names");
        prepareParentLink();
        when(i18nHelper.getText("issue.field.parent")).thenReturn(fieldName);

        final IssueBean bean = issueBeanBuilder.build(issue);

        assertThat(bean.getNames(), isMap("parent", fieldName));
    }

    @Test
    public void testBuildingTransitions() {
        issueBeanBuilder = createBuilderWithExpand("transitions");

        final List<ActionDescriptor> actionDescriptors = ImmutableList.of(mock(ActionDescriptor.class), mock(ActionDescriptor.class));
        final TransitionMetaBeanBuilder transitionBuilder = mock(TransitionMetaBeanBuilder.class);
        final List<TransitionBean> transitionBeans = ImmutableList.copyOf(Iterables.transform(actionDescriptors, new Function<ActionDescriptor, TransitionBean>() {
            @Override
            public TransitionBean apply(final ActionDescriptor actionDescriptor) {
                return prepareTransitionBeanBuilderForAction(transitionBuilder, actionDescriptor);
            }
        }));
        when(issueWorkflowManager.getSortedAvailableActions(eq(issue), any(ApplicationUser.class))).thenReturn(actionDescriptors);
        when(transitionBuilder.issue(issue)).thenReturn(transitionBuilder);
        when(beanBuilderFactory.newTransitionMetaBeanBuilder()).thenReturn(transitionBuilder);

        final IssueBean bean = issueBeanBuilder.build(issue);

        assertThat(bean.getTransitions(), is(transitionBeans));
    }

    @Test
    public void testBuildingOpsbar() {
        issueBeanBuilder = createBuilderWithExpand("operations");
        final OpsbarBean opsbarBean = mock(OpsbarBean.class);
        final OpsbarBeanBuilder opsbarBeanBuilder = mock(OpsbarBeanBuilder.class);
        when(opsbarBeanBuilder.build()).thenReturn(opsbarBean);
        when(beanBuilderFactory.newOpsbarBeanBuilder(issue)).thenReturn(opsbarBeanBuilder);

        final IssueBean bean = issueBeanBuilder.build(issue);

        assertThat(bean.getOperations(), is(opsbarBean));
    }

    @Test
    public void testBuildingEditMeta() {
        issueBeanBuilder = createBuilderWithExpand("editmeta");
        final EditMetaBean editMetaBean = mock(EditMetaBean.class);
        final EditMetaBeanBuilder editMetaBeanBuilder = mock(EditMetaBeanBuilder.class);
        when(editMetaBeanBuilder.issue(issue)).thenReturn(editMetaBeanBuilder);
        when(editMetaBeanBuilder.fieldsToInclude(fieldsToInclude)).thenReturn(editMetaBeanBuilder);
        when(editMetaBeanBuilder.build()).thenReturn(editMetaBean);
        when(beanBuilderFactory.newEditMetaBeanBuilder()).thenReturn(editMetaBeanBuilder);

        final IssueBean bean = issueBeanBuilder.build(issue);

        assertThat(bean.getEditMeta(), is(editMetaBean));
    }

    @Test
    public void testBuildingChangelog() {
        issueBeanBuilder = createBuilderWithExpand("changelog");
        final ChangelogBean changelogBean = mock(ChangelogBean.class);
        final ChangelogBeanBuilder changelogBeanBuilder = mock(ChangelogBeanBuilder.class);
        when(changelogBeanBuilder.build(issue)).thenReturn(changelogBean);
        when(beanBuilderFactory.newChangelogBeanBuilder()).thenReturn(changelogBeanBuilder);

        final IssueBean bean = issueBeanBuilder.build(issue);

        assertThat(bean.getChangelog(), is(changelogBean));
    }

    @Test
    public void testAddingFieldsToIssueBean() {
        // because issue bean is in API and have methods which allows add new field to them, we have to check that is still mutable.

        issueBeanBuilder = createBuilderWithExpand("names,schema,renderedFields");
        final String fieldKey = "id";
        final String fieldName = "name";
        final Object fieldData = "data";
        final Object renderedData = "rendered data";
        final FieldJsonRepresentation fieldJsonRepresentation = new FieldJsonRepresentation(new JsonData(fieldData), new JsonData(renderedData));
        final JsonType jsonType = JsonTypeBuilder.system("type", "system");

        final FieldLayoutItem fieldLayoutItem = createOrderableFieldInLayoutItem(fieldId, fieldName, fieldData);
        prepareFieldLayoutManager(fieldLayoutItem);
        final OrderableField orderableField = fieldLayoutItem.getOrderableField();
        when(((RestAwareField) orderableField).getJsonSchema()).thenReturn(jsonType);
        when(((RestAwareField) orderableField).getJsonFromIssue(issue, true, fieldLayoutItem)).thenReturn(fieldJsonRepresentation);

        prepareParentLink();
        when(i18nHelper.getText("issue.field.parent")).thenReturn(fieldName);

        final IssueBean bean = issueBeanBuilder.build(issue);

        final Field field = mock(Field.class, withSettings().extraInterfaces(RestAwareField.class));
        when(field.getId()).thenReturn(fieldKey);
        when(field.getName()).thenReturn(fieldName);
        when(((RestAwareField) field).getJsonFromIssue(issue, true, fieldLayoutItem)).thenReturn(fieldJsonRepresentation);
        when(((RestAwareField) field).getJsonSchema()).thenReturn(jsonType);

        bean.addField(field, fieldJsonRepresentation, true);
        assertThat(bean.getFields(), hasEntry(is(fieldKey), is(fieldData)));
        assertThat(bean.getSchema(), hasEntry(is(fieldKey), is(new JsonTypeBean(jsonType))));
        assertThat(bean.getNames(), hasEntry(is(fieldKey), is(fieldName)));
        assertThat(bean.getRenderedFields(), hasEntry(is(fieldKey), is(renderedData)));
    }

    private IssueBeanBuilder2 createBuilderWithExpand(final String expand) {
        return new IssueBeanBuilder2(fieldLayoutManager, authContext, fieldManager,
                resourceUriBuilder, beanBuilderFactory, fieldsToInclude,
                issueLinkBeanBuilderFactory, issueWorkflowManager, mock, expand);
    }

    private FieldLayoutItem createCustomFieldWithRestSerializer(final CustomField field,
                                                                final String id,
                                                                final String name,
                                                                final Object data,
                                                                final Map<Integer, CustomFieldRestSerializer> versionedRepresentations) {
        final AbstractCustomFieldType customFieldType = mock(AbstractCustomFieldType.class, Mockito.CALLS_REAL_METHODS);
        final CustomFieldTypeModuleDescriptor customFieldTypeModuleDescriptor = mock(CustomFieldTypeModuleDescriptor.class);
        when(customFieldTypeModuleDescriptor.getRestSerializers()).thenReturn(versionedRepresentations);
        customFieldType.init(customFieldTypeModuleDescriptor);

        final FieldLayoutItem fieldLayoutItem = mock(FieldLayoutItem.class);
        prepareField(field, id, name, data, fieldLayoutItem);
        when(field.getCustomFieldType()).thenReturn(customFieldType);
        when(fieldLayoutItem.getOrderableField()).thenReturn(field);
        return fieldLayoutItem;
    }

    private CustomFieldRestSerializer createRestSerializer(final CustomField field, final Object data) {
        final CustomFieldRestSerializer serializer = mock(CustomFieldRestSerializer.class);
        when(serializer.getJsonData(field, issue)).thenReturn(new JsonData(data));
        return serializer;
    }

    private void prepareFieldLayoutManager(final FieldLayoutItem... fieldLayoutItems) {
        final FieldLayout fieldLayout = mock(FieldLayout.class);
        //noinspection unchecked
        when(fieldLayout.getVisibleLayoutItems(any(Project.class), any(List.class))).thenReturn(Arrays.asList(fieldLayoutItems));
        when(fieldLayoutManager.getFieldLayout(issue)).thenReturn(fieldLayout);
    }

    private FieldLayoutItem createOrderableFieldInLayoutItem(final String id, final String name, final Object data) {
        final FieldLayoutItem fieldLayoutItem = mock(FieldLayoutItem.class);
        final OrderableField field = mock(OrderableField.class, withSettings().extraInterfaces(RestAwareField.class));
        prepareField(field, id, name, data, fieldLayoutItem);
        when(fieldLayoutItem.getOrderableField()).thenReturn(field);
        return fieldLayoutItem;
    }

    private void prepareField(final Field field, final String id, final String name, final Object data, final FieldLayoutItem fieldLayoutItem) {
        final FieldJsonRepresentation fieldJsonRepresentation = new FieldJsonRepresentation(new JsonData(data));
        when(field.getId()).thenReturn(id);
        when(field.getName()).thenReturn(name);
        when(((RestAwareField) field).getJsonFromIssue(issue, false, fieldLayoutItem)).thenReturn(fieldJsonRepresentation);
    }

    private IssueRefJsonBean prepareParentLink() {
        final long parentId = 10000L;
        when(issue.getParentId()).thenReturn(parentId);

        final IssueRefJsonBean issueRefJsonBean = mock(IssueRefJsonBean.class);
        final IssueLinksBeanBuilder linksBeanBuilder = mock(IssueLinksBeanBuilder.class);
        when(linksBeanBuilder.buildParentLink()).thenReturn(issueRefJsonBean);
        when(issueLinkBeanBuilderFactory.newIssueLinksBeanBuilder(issue)).thenReturn(linksBeanBuilder);

        return issueRefJsonBean;
    }

    private Matcher<? super Map> isMap(final String id, final Object data) {
        return is((Map) ImmutableMap.of(id, data));
    }

    private TransitionBean prepareTransitionBeanBuilderForAction(final TransitionMetaBeanBuilder mainTransitionBuilder, final ActionDescriptor actionDescriptor) {
        final TransitionBean transitionBean = mock(TransitionBean.class);
        final TransitionMetaBeanBuilder actionTransitionBuilder = mock(TransitionMetaBeanBuilder.class);
        when(mainTransitionBuilder.action(actionDescriptor)).thenReturn(actionTransitionBuilder);
        when(actionTransitionBuilder.build()).thenReturn(transitionBean);
        return transitionBean;
    }
}