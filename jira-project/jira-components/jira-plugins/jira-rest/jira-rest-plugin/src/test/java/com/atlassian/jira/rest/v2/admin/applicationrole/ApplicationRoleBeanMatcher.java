package com.atlassian.jira.rest.v2.admin.applicationrole;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.application.ApplicationRole;
import com.google.common.base.Objects;
import com.google.common.collect.Sets;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeDiagnosingMatcher;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import static com.atlassian.extras.common.LicensePropertiesConstants.UNLIMITED_USERS;

/**
 * @since v7.0
 */
class ApplicationRoleBeanMatcher extends TypeSafeDiagnosingMatcher<ApplicationRoleBean> {
    private String name;
    private Set<String> groups;
    private ApplicationKey key;
    private Set<String> defaultGroups;
    private boolean defaultRole;
    private Integer licensedSeats;
    private Boolean defined;
    private Integer currentSeats;
    private Integer remainingSeats;
    private String currentSeatsDescription;

    ApplicationRoleBeanMatcher(ApplicationRole role) {
        this.key = role.getKey();
        this.name = role.getName();
        this.groups = groupNames(role.getGroups());
        this.defaultGroups = groupNames(role.getDefaultGroups());
        this.licensedSeats = role.getNumberOfSeats();
        this.defined = role.isDefined();
        this.defaultRole = role.isSelectedByDefault();
    }

    ApplicationRoleBeanMatcher currentSeats(int seats, String description) {
        this.currentSeats = seats;
        this.currentSeatsDescription = description;
        return this;
    }

    ApplicationRoleBeanMatcher remainingSeats(int seats) {
        this.remainingSeats = seats;
        return this;
    }

    @Override
    protected boolean matchesSafely(final ApplicationRoleBean item, final Description mismatchDescription) {
        if (Objects.equal(name, item.getName())
                && Objects.equal(groups, item.getGroups())
                && Objects.equal(defaultGroups, item.getDefaultGroups())
                && Objects.equal(key, ApplicationKey.valueOf(item.getKey()))
                && Objects.equal(licensedSeats, item.getNumberOfSeats())
                && Objects.equal(defined, item.isDefined())
                && Objects.equal(defaultRole, item.isSelectedByDefault())
                && Objects.equal(currentSeats, item.getUserCount())
                && Objects.equal(remainingSeats, item.getRemainingSeats())
                && Objects.equal(currentSeatsDescription, item.getUserCountDescription())
                && Objects.equal(isUnlimited(), item.isHasUnlimitedSeats())) {
            return true;
        } else {
            final StringWriter writer = new StringWriter();
            final PrintWriter print = new PrintWriter(writer);
            print.printf("[name: %s, ", item.getName());
            print.printf("groups: %s, ", item.getGroups());
            print.printf("key: %s, ", item.getKey());
            print.printf("defaultGroups: %s, ", item.getDefaultGroups());
            print.printf("licensedSeats: %s, ", item.getNumberOfSeats());
            print.printf("defined: %s, ", item.isDefined());
            print.printf("default: %s, ", item.isSelectedByDefault());
            print.printf("currentSeats: %s, ", item.getUserCount());
            print.printf("currentSeatsDescription: %s, ", item.getUserCountDescription());
            print.printf("remainingSeats: %s, ", item.getRemainingSeats());
            print.printf("unlimited: %s]", item.isHasUnlimitedSeats());
            print.close();
            mismatchDescription.appendText(writer.toString());

            return false;
        }
    }

    private Boolean isUnlimited() {
        return remainingSeats == null ? null : remainingSeats == UNLIMITED_USERS;
    }

    @Override
    public void describeTo(final Description description) {
        final StringWriter writer = new StringWriter();
        final PrintWriter print = new PrintWriter(writer);
        print.printf("[name: %s, ", name);
        print.printf("groups: %s, ", groups);
        print.printf("key: %s, ", key);
        print.printf("defaultGroups: %s, ", defaultGroups);
        print.printf("licensedSeats: %s, ", licensedSeats);
        print.printf("defined: %s, ", defined);
        print.printf("default: %s, ", defaultRole);
        print.printf("currentSeats: %s, ", currentSeats);
        print.printf("currentSeatsDescription: %s, ", currentSeatsDescription);
        print.printf("remainingSeats: %s, ", remainingSeats);
        print.printf("unlimited: %s]", isUnlimited());
        description.appendText(writer.toString());
    }

    private static Set<String> groupNames(Collection<Group> groups) {
        Set<String> sets = Sets.newHashSet();
        for (Group group : groups) {
            if (group != null) {
                sets.add(group.getName());
            } else {
                sets.add(null);
            }
        }
        return Collections.unmodifiableSet(sets);
    }
}
