package com.atlassian.jira.rest.v2.admin.applicationrole;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.application.MockApplication;
import com.atlassian.jira.application.MockApplicationManager;
import com.atlassian.jira.application.MockApplicationRole;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static com.atlassian.extras.common.LicensePropertiesConstants.UNLIMITED_USERS;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class ApplicationRoleBeanConverterTest {
    @Rule
    public MockitoRule chain = MockitoJUnit.rule();

    @Mock
    private ApplicationRoleManager roleManager;
    private MockApplicationManager appManager = new MockApplicationManager();

    private ApplicationRoleBeanConverter roleConverter;

    @Before
    public void setUp() {
        roleConverter = new ApplicationRoleBeanConverter(roleManager, appManager);
    }

    @Test
    public void convertsDefinedNonDefaultRoleCorrectly() {
        //given
        final ApplicationKey key = ApplicationKey.valueOf("existing-app");
        ApplicationRole role = new MockApplicationRole(key)
                .groupNames("one", "two", "three")
                .defaultGroupNames("one")
                .name("SomeName")
                .defined(true)
                .numberOfSeats(10);
        when(roleManager.getRemainingSeats(key)).thenReturn(5);
        when(roleManager.getUserCount(key)).thenReturn(5);

        final MockApplication application = appManager.addApplication(key);

        //when
        ApplicationRoleBean bean = roleConverter.roleBean(role);

        //then
        assertThat(bean, new ApplicationRoleBeanMatcher(role)
                .currentSeats(5, application.getUserCountDescription(Option.some(5)))
                .remainingSeats(5));
    }

    @Test
    public void convertsUndefinedDefaultRoleCorrectly() {
        //given
        final ApplicationKey key = ApplicationKey.valueOf("existing-app");
        ApplicationRole role = new MockApplicationRole(key)
                .groupNames("one", "two", "three")
                .defaultGroupNames("one")
                .name("SomeName")
                .defined(false)
                .numberOfSeats(1000);
        when(roleManager.getRemainingSeats(key)).thenReturn(995);
        when(roleManager.getUserCount(key)).thenReturn(5);

        //when
        ApplicationRoleBean bean = roleConverter.roleBean(role);

        //then
        assertThat(bean, new ApplicationRoleBeanMatcher(role)
                .currentSeats(5, appManager.getPlatform().getUserCountDescription(Option.some(5)))
                .remainingSeats(995));
    }

    @Test
    public void convertsRoleWithUnlimitedSeatsCorrectly() {
        //given
        final ApplicationKey key = ApplicationKey.valueOf("existing-app");
        ApplicationRole role = new MockApplicationRole(key)
                .groupNames("one", "two", "three")
                .defaultGroupNames("one")
                .name("SomeName")
                .defined(false)
                .numberOfSeats(UNLIMITED_USERS);
        when(roleManager.getRemainingSeats(key)).thenReturn(UNLIMITED_USERS);
        when(roleManager.getUserCount(key)).thenReturn(5000);

        //when
        ApplicationRoleBean bean = roleConverter.roleBean(role);

        //then
        assertThat(bean, new ApplicationRoleBeanMatcher(role)
                .currentSeats(5000, appManager.getPlatform().getUserCountDescription(Option.some(5000)))
                .remainingSeats(UNLIMITED_USERS));
    }
}
