package com.atlassian.jira.rest.v2.search;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.rest.util.FilterPermissionHelper;
import com.atlassian.jira.sharing.SharePermission;
import com.atlassian.jira.sharing.SharePermissionImpl;
import com.atlassian.jira.sharing.SharedEntity;
import com.atlassian.jira.sharing.type.ShareType;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.query.QueryImpl;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import java.util.HashSet;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.verify;

public class FilterPermissionHelperTest {
    @Rule
    public RuleChain init = MockitoMocksInContainer.forTest(this);

    @Mock
    private SearchRequestService searchRequestService;

    @Mock
    private ApplicationUser user;

    @Mock
    @AvailableInContainer
    private UserManager userManager;

    private FilterPermissionHelper helper;

    @Before
    public void setUp() throws Exception {
        helper = new FilterPermissionHelper(searchRequestService);

    }

    @Test
    public void testAddSharePermissionToFilter() {
        SharePermissionImpl sharePermission = new SharePermissionImpl(ShareType.Name.GROUP, "jira-administrators", null);
        SearchRequest filter1 = new SearchRequest(new QueryImpl(), user, "filter1", "filterter1Desc", 1L, 0L);
        ServiceOutcome<SearchRequest> outcome = helper.addSharePermissionToSavedFilter(user, filter1, sharePermission);

        assertThat(outcome.isValid(), is(true));
        verify(searchRequestService).updateFilter(any(JiraServiceContext.class), argThat(filterWithSharePermissions(sharePermission)));
    }

    @Test
    public void testRemoveSharePermissionToFilter() {
        SharePermissionImpl sharePermission = new SharePermissionImpl(1l, ShareType.Name.GROUP, "jira-administrators", null);

        SearchRequest filterWithPermission = filterWithPermission(sharePermission);

        ServiceOutcome<SearchRequest> outcome = helper.removeSharePermissionFromSavedFilter(user, filterWithPermission, sharePermission.getId());

        assertThat(outcome.isValid(), is(true));
        verify(searchRequestService).updateFilter(any(JiraServiceContext.class), argThat(withoutSharePermissions()));
    }

    private SearchRequest filterWithPermission(SharePermissionImpl sharePermission) {
        SearchRequest filterWithPermission = new SearchRequest(new QueryImpl(), user, "filter1", "filterter1Desc", 1L, 0L);
        HashSet<SharePermission> permissions = new HashSet<>();
        permissions.add(sharePermission);
        filterWithPermission.setPermissions(new SharedEntity.SharePermissions(permissions));
        return filterWithPermission;
    }

    private Matcher<SearchRequest> filterWithSharePermissions(SharePermission... sharePermissions) {
        Matcher<Iterable<? extends SharePermission>> matcher = Matchers.contains(sharePermissions);

        return new TypeSafeMatcher<SearchRequest>() {
            @Override
            protected boolean matchesSafely(SearchRequest item) {
                return matcher.matches(item.getPermissions().getPermissionSet());
            }

            @Override
            public void describeTo(Description description) {
                matcher.describeMismatch(sharePermissions, description);
            }
        };
    }

    private Matcher<SearchRequest> withoutSharePermissions() {
        return new TypeSafeMatcher<SearchRequest>() {
            @Override
            protected boolean matchesSafely(SearchRequest item) {
                return item.getPermissions().isEmpty();
            }

            @Override
            public void describeTo(Description description) {
            }
        };
    }
}