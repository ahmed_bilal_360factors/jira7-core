package com.atlassian.jira.rest.v2.issue;

import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.google.common.base.Supplier;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

/**
 * Tests for UserJsonBeanListWrapper
 */
public class UserJsonBeanListWrapperTest {
    private static final int MAX_RESULTS = 50;

    /**
     * The size of the user collection is provided to the bean separately to the list of users.
     * This test checks that we don't see an IndexOutOfBoundsException if the size doesn't match
     * the actual size of the user collection.
     * <p>
     * Given the size of the user collection has been calculated
     * And users are subsequently deleted
     * When all users are requested with an out of bounds end index
     * Then the list of existing users is returned and no exception is thrown
     */
    @Test
    public void outOfBoundsEndIndexIsHandledGracefully() {
        // We counted 4 users
        int numUsers = 4;
        // But now there are only 2 in the db
        final Supplier<List<ApplicationUser>> userSupplier = () -> listOfUsers(2);
        UserJsonBeanListWrapper wrapper = new UserJsonBeanListWrapper(numUsers, userSupplier, MAX_RESULTS, null, null);

        // Ask for all four users, two should be gracefully returned. No IndexOutOfBoundsException!
        final List<ApplicationUser> orderedList = wrapper.getOrderedList(0, 3);
        assertThat(orderedList, hasSize(2));
    }

    /**
     * Given the size of the user collection has been calculated
     * And users are subsequently deleted
     * When all users are requested an out of bounds start index
     * Then the list of existing users is returned and no exception is thrown
     */
    @Test
    public void outOfBoundsStartIndexIsHandledGracefully() {
        // We counted 4 users
        int numUsers = 4;
        // But now there is only 1 in the db
        final Supplier<List<ApplicationUser>> userSupplier = () -> listOfUsers(1);
        UserJsonBeanListWrapper wrapper = new UserJsonBeanListWrapper(numUsers, userSupplier, MAX_RESULTS, null, null);

        // Ask for the last two users, none should be gracefully returned.
        final List<ApplicationUser> orderedList = wrapper.getOrderedList(2, 3);
        assertThat(orderedList, empty());
    }

    /**
     * Create a list of mock users
     *
     * @param numUsers the number of users
     * @return the list
     */
    private List<ApplicationUser> listOfUsers(final int numUsers) {
        final ApplicationUser[] usersArray = new ApplicationUser[numUsers];
        Arrays.setAll(usersArray, i -> new MockApplicationUser("user" + i));
        return Arrays.asList(usersArray);
    }
}
