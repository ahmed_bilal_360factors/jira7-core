package com.atlassian.jira.rest.v2.admin.applicationrole;

import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.application.MockApplicationRole;
import com.atlassian.jira.application.MockApplicationRoleAdminService;
import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * @since v7.0
 */
public class ApplicationRoleHasherImplTest {
    private MockApplicationRoleAdminService service = new MockApplicationRoleAdminService();
    private final static String EMPTY_COLLECTION_HASH = "0";
    private final ApplicationRoleHasher appRoleHasher = new ApplicationRoleHasherImpl();

    @Test
    public void sameTwoCollectionsReturnSameVersionHash() {
        MockApplicationRole app1 = createApplicationRole("appOne")
                .name("App One")
                .groupNames("one", "two");
        MockApplicationRole app2 = createApplicationRole("appTwo")
                .name("App Two")
                .groupNames("three", "four");
        Collection<ApplicationRole> appCollection1 = Lists.newArrayList(app1, app2);
        Collection<ApplicationRole> appCollection2 = Lists.newArrayList(app1, app2);

        assertEquals(appRoleHasher.getVersionHash(appCollection1), appRoleHasher.getVersionHash(appCollection2));
    }

    @Test
    public void differentCollectionsReturnDifferentVersionHashes() {
        MockApplicationRole app1 = createApplicationRole("appOne")
                .name("App One")
                .groupNames("one", "two");
        MockApplicationRole app2 = createApplicationRole("appTwo")
                .name("App Two")
                .groupNames("three", "four");
        MockApplicationRole app3 = createApplicationRole("appThree")
                .name("App Three")
                .groupNames("five", "six");

        assertNotEquals(appRoleHasher.getVersionHash(Lists.newArrayList(app1, app2)),
                appRoleHasher.getVersionHash(Lists.newArrayList(app2, app3)));
    }

    @Test
    public void collectionsWithAppsInDifferentOrderReturnSameVersionHash() {
        MockApplicationRole app1 = createApplicationRole("appOne")
                .name("App One")
                .groupNames("one", "two");
        MockApplicationRole app2 = createApplicationRole("appTwo")
                .name("App Two")
                .groupNames("three", "four");

        assertEquals(appRoleHasher.getVersionHash(Lists.newArrayList(app1, app2)),
                appRoleHasher.getVersionHash(Lists.newArrayList(app2, app1)));
    }

    @Test
    public void collectionsWithGroupsInDifferentOrderReturnsSameVersionHash() {
        MockApplicationRole app1 = createApplicationRole("appOne")
                .name("App One")
                .groupNames("one", "two");
        MockApplicationRole app2 = createApplicationRole("appTwo")
                .name("App Two")
                .groupNames("three", "four");
        MockApplicationRole app11 = createApplicationRole("appOne")
                .name("App One")
                .groupNames("two", "one");
        MockApplicationRole app22 = createApplicationRole("appTwo")
                .name("App Two")
                .groupNames("four", "three");

        assertEquals(appRoleHasher.getVersionHash(Lists.newArrayList(app1, app2)),
                appRoleHasher.getVersionHash(Lists.newArrayList(app11, app22)));
    }

    @Test
    public void hashingMoreThanOnceOnCollectionReturnsSameVersionHash() {
        MockApplicationRole app1 = createApplicationRole("appOne")
                .name("App One")
                .groupNames("one", "two");
        MockApplicationRole app2 = createApplicationRole("appTwo")
                .name("App Two")
                .groupNames("three", "four");

        Collection<ApplicationRole> appCollection = Lists.newArrayList(app1, app2);
        String versionHashFirst = appRoleHasher.getVersionHash(appCollection);

        assertEquals(versionHashFirst, appRoleHasher.getVersionHash(appCollection));
    }

    @Test
    public void collectionReturnsHashCodeSuccesfullyWhenEmpty() {
        String versionHash = appRoleHasher.getVersionHash(Lists.newArrayList());

        assertEquals(versionHash, EMPTY_COLLECTION_HASH);
    }

    private MockApplicationRole createApplicationRole(String key) {
        return service.addApplicationRole(key);
    }
}
