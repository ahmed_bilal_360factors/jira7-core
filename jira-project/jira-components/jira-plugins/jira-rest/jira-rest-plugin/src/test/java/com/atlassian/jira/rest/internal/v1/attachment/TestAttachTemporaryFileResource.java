package com.atlassian.jira.rest.internal.v1.attachment;

import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.issue.attachment.TemporaryAttachment;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.rest.internal.common.bean.AttachTemporaryFileBadResultBean;
import com.atlassian.jira.rest.internal.common.bean.AttachTemporaryFileGoodResultBean;
import com.atlassian.jira.rest.util.AttachmentHelper;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.xsrf.XsrfTokenGenerator;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.SecureUserTokenManager;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.web.util.AttachmentException;
import com.atlassian.jira.web.util.WebAttachmentManager;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.IOException;
import java.util.Locale;

import static com.atlassian.jira.bc.issue.IssueService.IssueResult;
import static com.atlassian.jira.bc.project.ProjectService.GetProjectResult;
import static com.atlassian.jira.rest.matchers.ResponseMatchers.body;
import static com.atlassian.jira.rest.matchers.ResponseMatchers.noBody;
import static com.atlassian.jira.rest.matchers.ResponseMatchers.noCache;
import static com.atlassian.jira.rest.matchers.ResponseMatchers.status;
import static com.atlassian.jira.rest.util.AttachmentHelper.ValidationError.ATTACHMENT_IO_UNKNOWN;
import static com.atlassian.jira.rest.util.AttachmentHelper.ValidationError.FILENAME_BLANK;
import static com.atlassian.jira.rest.util.AttachmentHelper.ValidationError.XSRF_TOKEN_INVALID;
import static com.atlassian.jira.rest.util.AttachmentHelper.ValidationResult;
import static com.atlassian.jira.user.SecureUserTokenManager.TokenType.SCREENSHOT;
import static com.atlassian.jira.util.NoopI18nHelper.makeTranslation;
import static java.util.Locale.ENGLISH;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.CREATED;
import static javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static org.hamcrest.Matchers.allOf;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.4
 */
public class TestAttachTemporaryFileResource {
    @Rule
    public TestRule initMocks = new InitMockitoMocks(this);

    @Mock
    private WebAttachmentManager webAttachmentManager;
    @Mock
    private IssueService issueService;
    @Mock
    private ProjectService projectService;
    @Mock
    private HttpServletRequest request;
    @Mock
    private ServletInputStream stream;
    @Mock
    private XsrfTokenGenerator xsrfGenerator;
    @Mock
    private AttachmentHelper attachmentHelper;
    @Mock
    private SecureUserTokenManager secureUserTokenManager;

    private MockApplicationUser user;
    private JiraAuthenticationContext authCtx;

    @Before
    public void createDeps() {
        user = new MockApplicationUser("Brenden");
        authCtx = new MockSimpleAuthenticationContext(user, Locale.ENGLISH, new NoopI18nHelper());
    }

    @Test
    public void testBadXsrf() {
        String token = "Toke3423424243n";

        ValidationResult validationResult = mock(ValidationResult.class);
        when(validationResult.isValid()).thenReturn(false);
        when(validationResult.getErrorType()).thenReturn(XSRF_TOKEN_INVALID);
        when(attachmentHelper.validate(request, null, null)).thenReturn(validationResult);
        when(xsrfGenerator.generateToken(request)).thenReturn(token);
        AttachTemporaryFileResource testObj = new AttachTemporaryFileResource(authCtx, webAttachmentManager,
                issueService, projectService, xsrfGenerator, attachmentHelper, secureUserTokenManager);

        Response actualResponse = testObj.addTemporaryAttachment(null, null, 178L, null, null, request);

        final AttachTemporaryFileBadResultBean expectedResponse =
                new AttachTemporaryFileBadResultBean(makeTranslation("attachfile.xsrf.try.again"), token);
        assertThat(actualResponse, Matchers.allOf(
                noCache(),
                body(expectedResponse),
                status(INTERNAL_SERVER_ERROR)
        ));
    }

    @Test
    public void testBadArguments() {
        ValidationResult validationResult = mock(ValidationResult.class);
        when(validationResult.isValid()).thenReturn(false);
        when(validationResult.getErrorType()).thenReturn(FILENAME_BLANK);
        when(validationResult.getErrorMessage()).thenReturn(null);
        when(attachmentHelper.validate(null, null, null)).thenReturn(validationResult);
        when(attachmentHelper.validate(null, "filename", null)).thenReturn(validationResult);
        when(attachmentHelper.validate(null, "    ", null)).thenReturn(validationResult);
        AttachTemporaryFileResource testObj = new AttachTemporaryFileResource(authCtx, webAttachmentManager,
                issueService, projectService, xsrfGenerator, attachmentHelper, secureUserTokenManager);

        assertBadRequest(testObj.addTemporaryAttachment(null, null, 178L, null, null, null));
        assertBadRequest(testObj.addTemporaryAttachment("    ", null, 178L, null, null, null));
        assertBadRequest(testObj.addTemporaryAttachment("filename", null, null, null, null, null));
    }

    @Test
    public void testGoodCreatingIssue() throws IOException, AttachmentException {
        MockProject project = new MockProject(178L);
        String contentType = "application/octet-stream";
        long size = 1L;
        TemporaryAttachment ta = new TemporaryAttachment(273738L, new File("something"), "name", contentType, null);

        ValidationResult validationResult = mock(ValidationResult.class);
        when(validationResult.isValid()).thenReturn(true);
        when(validationResult.getInputStream()).thenReturn(stream);
        when(validationResult.getContentType()).thenReturn(contentType);
        when(validationResult.getSize()).thenReturn(size);
        when(attachmentHelper.validate(request, ta.getFilename(), size)).thenReturn(validationResult);
        when(projectService.getProjectById(user, project.getId())).thenReturn(new GetProjectResult(new SimpleErrorCollection(), project));
        when(webAttachmentManager.createTemporaryAttachment(stream, ta.getFilename(), ta.getContentType(),
                size, null, project, null)).thenReturn(ta);
        AttachTemporaryFileResource testObj = new AttachTemporaryFileResource(authCtx, webAttachmentManager,
                issueService, projectService, xsrfGenerator, attachmentHelper, secureUserTokenManager);

        Response response = testObj.addTemporaryAttachment(ta.getFilename(), project.getId(), null, size, null, request);

        assertThat(response, Matchers.allOf(
                noCache(),
                body(new AttachTemporaryFileGoodResultBean(ta.getId().toString(), ta.getFilename())),
                status(CREATED)
        ));
    }

    @Test
    public void testBadCreatingIssueNoProject() throws IOException, AttachmentException {
        long pid = 178L;
        String errorMsg = "badddd";

        ValidationResult validationResult = mock(ValidationResult.class);
        when(validationResult.isValid()).thenReturn(true);
        when(attachmentHelper.validate(request, "ignored", 1L)).thenReturn(validationResult);
        when(projectService.getProjectById(user, pid)).thenReturn(new GetProjectResult(errors(errorMsg)));
        AttachTemporaryFileResource testObj = new AttachTemporaryFileResource(authCtx, webAttachmentManager,
                issueService, projectService, xsrfGenerator, attachmentHelper, secureUserTokenManager);

        try {
            testObj.addTemporaryAttachment("ignored", pid, null, 1L, null, request);
            fail("Expected error");
        } catch (WebApplicationException e) {
            assertThat(e.getResponse(), Matchers.allOf(
                    noCache(),
                    body(new AttachTemporaryFileBadResultBean(errorMsg)),
                    status(NOT_FOUND)
            ));
        }
    }

    @Test
    public void testGoodpdatingIssueGood() throws IOException, AttachmentException {
        MockIssue issue = new MockIssue(178L);
        MockProject project = new MockProject(74748L);
        int contentLength = 272728;
        String contentType = "text/plain";
        TemporaryAttachment ta = new TemporaryAttachment(273738L, new File("something"), "name", contentType, null);

        ValidationResult validationResult = mock(ValidationResult.class);
        when(validationResult.isValid()).thenReturn(true);
        when(validationResult.getInputStream()).thenReturn(stream);
        when(validationResult.getSize()).thenReturn(272728L);
        when(validationResult.getContentType()).thenReturn(contentType);
        when(attachmentHelper.validate(request, ta.getFilename(), null)).thenReturn(validationResult);
        when(issueService.getIssue(user, issue.getId())).thenReturn(new IssueResult(issue, ok()));
        when(webAttachmentManager.createTemporaryAttachment(stream, ta.getFilename(), ta.getContentType(),
                contentLength, issue, null, null)).thenReturn(ta);
        AttachTemporaryFileResource testObj = new AttachTemporaryFileResource(authCtx, webAttachmentManager,
                issueService, projectService, xsrfGenerator, attachmentHelper, secureUserTokenManager);

        Response response = testObj.addTemporaryAttachment(ta.getFilename(), project.getId(), issue.getId(), null, null, request);

        assertThat(response, allOf(
                noCache(),
                body(new AttachTemporaryFileGoodResultBean(ta.getId().toString(), ta.getFilename())),
                status(CREATED)
        ));
    }

    @Test
    public void testBadUpdatingIssueNoIssue() throws IOException, AttachmentException {
        long pid = 178L;
        long id = 575;
        String errorMsg = "badddd";

        ValidationResult validationResult = mock(ValidationResult.class);
        when(validationResult.isValid()).thenReturn(true);
        when(attachmentHelper.validate(request, "ignored", 1L)).thenReturn(validationResult);
        when(issueService.getIssue(user, id)).thenReturn(new IssueResult(null, errors(errorMsg)));
        AttachTemporaryFileResource testObj = new AttachTemporaryFileResource(authCtx, webAttachmentManager,
                issueService, projectService, xsrfGenerator, attachmentHelper, secureUserTokenManager);

        try {
            testObj.addTemporaryAttachment("ignored", pid, id, 1L, null, request);
            fail("Expected error");
        } catch (WebApplicationException e) {
            assertThat(e.getResponse(), Matchers.allOf(
                    noCache(),
                    body(new AttachTemporaryFileBadResultBean(errorMsg)),
                    status(NOT_FOUND)
            ));
        }
    }

    @Test
    public void testErrorWhileGettingStream() throws IOException, AttachmentException {
        MockIssue issue = new MockIssue(178L);
        MockProject project = new MockProject(74748L);
        String errorMessage = "message";
        String fileName = "name";

        ValidationResult validationResult = mock(ValidationResult.class);
        when(validationResult.isValid()).thenReturn(false);
        when(validationResult.getErrorType()).thenReturn(ATTACHMENT_IO_UNKNOWN);
        when(validationResult.getErrorMessage()).thenReturn(errorMessage);
        when(attachmentHelper.validate(request, fileName, 1L)).thenReturn(validationResult);

        AttachTemporaryFileResource testObj = new AttachTemporaryFileResource(authCtx, webAttachmentManager,
                issueService, projectService, xsrfGenerator, attachmentHelper, secureUserTokenManager);

        Response response = testObj.addTemporaryAttachment(fileName, project.getId(), issue.getId(), 1L, null, request);
        String expectedMessage = makeTranslation("attachfile.error.io.error", fileName, errorMessage);

        assertThat(response, allOf(
                noCache(),
                body(new AttachTemporaryFileBadResultBean(expectedMessage)),
                status(INTERNAL_SERVER_ERROR)
        ));
    }

    @Test
    public void testAttachmentError() throws IOException, AttachmentException {
        long contentLength = 272728;
        String contentType = "text/plain";
        String message = "errorMessae";
        MockIssue issue = new MockIssue(178L);
        MockProject project = new MockProject(74748L);
        TemporaryAttachment ta = new TemporaryAttachment(273738L, new File("something"), "name", contentType, null);

        ValidationResult validationResult = mock(ValidationResult.class);
        when(validationResult.isValid()).thenReturn(true);
        when(validationResult.getInputStream()).thenReturn(stream);
        when(validationResult.getContentType()).thenReturn(contentType);
        when(validationResult.getSize()).thenReturn(272728L);
        when(issueService.getIssue(user, issue.getId())).thenReturn(new IssueResult(issue, ok()));
        when(attachmentHelper.validate(request, ta.getFilename(), 272728L)).thenReturn(validationResult);
        when(webAttachmentManager.createTemporaryAttachment(stream, ta.getFilename(), ta.getContentType(),
                contentLength, issue, null, null))
                .thenThrow(new AttachmentException(message));
        AttachTemporaryFileResource testObj = new AttachTemporaryFileResource(authCtx, webAttachmentManager,
                issueService, projectService, xsrfGenerator, attachmentHelper, secureUserTokenManager);

        Response response = testObj.addTemporaryAttachment(ta.getFilename(), project.getId(), issue.getId(), contentLength, null, request);

        assertThat(response, allOf(
                noCache(),
                body(new AttachTemporaryFileBadResultBean(message)),
                status(INTERNAL_SERVER_ERROR)
        ));
    }

    @Test
    public void testValidSecurityToken() throws IOException, AttachmentException {
        String secureToken = "token";
        int contentLength = 272728;
        String contentType = "text/plain";
        MockIssue issue = new MockIssue(178L);
        MockProject project = new MockProject(74748L);
        TemporaryAttachment ta = new TemporaryAttachment(273738L, new File("something"), "name", contentType, null);
        JiraAuthenticationContext authCtx = new MockSimpleAuthenticationContext((ApplicationUser) null, ENGLISH, new NoopI18nHelper());

        ValidationResult validationResult = mock(ValidationResult.class);
        when(validationResult.isValid()).thenReturn(true);
        when(validationResult.getInputStream()).thenReturn(stream);
        when(validationResult.getSize()).thenReturn(272728L);
        when(validationResult.getContentType()).thenReturn(contentType);
        when(attachmentHelper.validate(request, ta.getFilename(), null)).thenReturn(validationResult);
        when(issueService.getIssue(user, issue.getId())).thenReturn(new IssueResult(issue, ok()));
        when(webAttachmentManager.createTemporaryAttachment(stream, ta.getFilename(), ta.getContentType(),
                contentLength, issue, null, null)).thenReturn(ta);
        when(secureUserTokenManager.useToken(secureToken, SCREENSHOT)).thenReturn(user);
        AttachTemporaryFileResource testObj = new AttachTemporaryFileResource(authCtx, webAttachmentManager,
                issueService, projectService, xsrfGenerator, attachmentHelper, secureUserTokenManager);

        Response response = testObj.addTemporaryAttachment(ta.getFilename(), project.getId(), issue.getId(), null, secureToken, null, request);

        assertThat(response, allOf(
                noCache(),
                body(new AttachTemporaryFileGoodResultBean(ta.getId().toString(), ta.getFilename())),
                status(CREATED)
        ));
    }

    private void assertBadRequest(Response response) {
        assertThat(response, Matchers.allOf(
                noCache(),
                noBody(),
                status(BAD_REQUEST)
        ));
    }

    public SimpleErrorCollection errors(String... errors) {
        SimpleErrorCollection collection = new SimpleErrorCollection();
        for (String error : errors) {
            collection.addErrorMessage(error);
        }
        return collection;
    }

    public SimpleErrorCollection ok() {
        return new SimpleErrorCollection();
    }
}
