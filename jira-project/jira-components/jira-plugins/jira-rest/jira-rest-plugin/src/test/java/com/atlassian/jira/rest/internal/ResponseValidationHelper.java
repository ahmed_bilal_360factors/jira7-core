package com.atlassian.jira.rest.internal;

import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;

public class ResponseValidationHelper {
    public static void assertCreated(final Response response) {
        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
    }

    public static void assertUpdated(final Response response) {
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
    }

    public static void assertNoContent(final Response response) {
        assertEquals(Response.Status.NO_CONTENT.getStatusCode(), response.getStatus());
    }

    public static void assertOk(final Response response) {
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
    }
}
