package com.atlassian.jira.rest.v2.upgrade;

import com.atlassian.jira.cluster.zdu.ClusterUpgradeStateManager;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockComponentContainer;
import com.atlassian.jira.mock.MockPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.upgrade.UpgradeScheduler;
import com.atlassian.jira.upgrade.UpgradeService;
import com.atlassian.scheduler.SchedulerHistoryService;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.status.RunDetails;
import com.atlassian.scheduler.status.RunOutcome;
import org.hamcrest.core.IsNull;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import javax.ws.rs.core.Response;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.doThrow;

@RunWith(MockitoJUnitRunner.class)
public class UpgradeResourceTest {
    @Rule
    public MockComponentContainer container = new MockComponentContainer(this);
    
    @Mock
    private JiraAuthenticationContext mockAuthenticationContext;
    private PermissionManager mockPermissionManager = new MockPermissionManager(true);
    @Mock
    private UpgradeService mockUpgradeService;
    @Mock
    private ClusterUpgradeStateManager mockClusterUpgradeStateManager;
    @Mock
    private SchedulerHistoryService schedulerHistoryService;
    @Mock
    private JiraBaseUrls mockBaseUrls;
    @Mock 
    private SchedulerService schedulerService;
    
    @InjectMocks
    @AvailableInContainer
    private UpgradeScheduler upgradeScheduler;
    
    private UpgradeResource upgradeResource;

    @Before
    public void setUp() {
        when(mockBaseUrls.restApi2BaseUrl()).thenReturn("http://127.0.0.1:8080/jira");
        upgradeResource = new UpgradeResource(mockAuthenticationContext, mockPermissionManager, mockUpgradeService,
                mockClusterUpgradeStateManager, mockBaseUrls, schedulerHistoryService
        );
    }

    @Test
    public void testUpgradeResultNoUpgrades() {
        Response response = upgradeResource.getUpgradeResult();
        assertEquals("Wrong status.", Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }

    @Test
    public void testUpgradeResultWithPendingUpgradesRunning() {
        LocalDate now = new LocalDate();

        RunDetails rundetails = Mockito.mock(RunDetails.class);
        when(rundetails.getStartTime()).thenReturn(now.minus(Period.minutes(1)).toDate());
        when(rundetails.getDurationInMillis()).thenReturn(1234L);
        when(rundetails.getRunOutcome()).thenReturn(RunOutcome.SUCCESS);
        when(rundetails.getMessage()).thenReturn(null);

        when(mockUpgradeService.areUpgradesRunning()).thenReturn(true);
        when(schedulerHistoryService.getLastRunForJob(UpgradeScheduler.JOB_ID)).thenReturn(rundetails);

        Response response = upgradeResource.getUpgradeResult();
        assertThat("Wrong status.", Response.Status.SEE_OTHER.getStatusCode(), is(response.getStatus()));
        assertThat("Should be no content", response.getEntity(), IsNull.nullValue());
    }

    @Test
    public void testUpgradeResultDone() {
        LocalDate now = new LocalDate();

        RunDetails rundetails = Mockito.mock(RunDetails.class);
        when(rundetails.getStartTime()).thenReturn(now.minus(Period.minutes(1)).toDate());
        when(rundetails.getDurationInMillis()).thenReturn(1234L);
        when(rundetails.getRunOutcome()).thenReturn(RunOutcome.SUCCESS);
        when(rundetails.getMessage()).thenReturn(null);

        when(mockUpgradeService.areUpgradesRunning()).thenReturn(false);
        when(schedulerHistoryService.getLastRunForJob(UpgradeScheduler.JOB_ID)).thenReturn(rundetails);

        Response response = upgradeResource.getUpgradeResult();
        assertThat("Wrong status.", Response.Status.OK.getStatusCode(), is(response.getStatus()));
        UpgradeResultBean upgradeResultBean = (UpgradeResultBean) response.getEntity();
        assertThat("No Result", upgradeResultBean, IsNull.notNullValue());
        assertThat("No successful", upgradeResultBean.getOutcome(), is("SUCCESS"));
        assertThat("No successful", upgradeResultBean.getDuration(), is(1234L));
    }

    @Test
    public void testDoUpgradesSuccess() throws Exception {
        Response response = upgradeResource.runUpgradesNow();
        assertEquals("Wrong status.", Response.Status.ACCEPTED.getStatusCode(), response.getStatus());
        verify(schedulerService).scheduleJob(any(), any());
    }

    @Test
    public void testDoUpgradesFail() throws Exception {
        doThrow(new SchedulerServiceException("it failed")).when(schedulerService).scheduleJob(any(), any());
        Response response = upgradeResource.runUpgradesNow();
        assertEquals("Wrong status.", Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());
        verify(schedulerService).scheduleJob(any(), any());
    }
    
    @Test
    public void testErrorDuringZdu() throws Exception {
        when(mockClusterUpgradeStateManager.areDelayedUpgradesHandledByCluster()).thenReturn(true);
        Response response = upgradeResource.runUpgradesNow();
        assertEquals("Wrong status.", Response.Status.CONFLICT.getStatusCode(), response.getStatus());
    }
}
