package com.atlassian.jira.rest.v2.issue;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.rest.json.beans.IssueLinkJsonBean;
import com.atlassian.jira.issue.fields.rest.json.beans.IssueLinksBeanBuilder;
import com.atlassian.jira.issue.fields.rest.json.beans.IssueRefJsonBean;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.link.LinkCollection;
import com.atlassian.jira.rest.v2.issue.context.ContextUriInfo;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.Collections;
import java.util.List;

import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * * Tests for {@link com.atlassian.jira.issue.fields.rest.json.beans.IssueLinksBeanBuilder}.
 *
 * @since v4.2
 */
public class TestIssueLinkBeanBuilder {
    private static final String USER_NAME = "aUser";
    private static final String ISSUE_BASE_URI = "http://localhost:8090/jira/rest/api/2.0/issue/";

    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();
    @Mock
    protected JiraBaseUrls jiraBaseUrls;
    @Mock
    protected Issue issue;
    @Mock
    protected JiraAuthenticationContext authContext;
    @Mock
    protected ApplicationProperties applicationProperties;
    @Mock
    protected IssueLinkManager issueLinkManager;
    @Mock
    protected ResourceUriBuilder uriBuilder;
    @Mock
    protected ContextUriInfo contextUriInfo;
    @Mock
    protected UriInfo uriInfo;

    @Before
    public void setUp() {
        when(jiraBaseUrls.restApi2BaseUrl()).thenReturn("http://localhost/jira");
        when(issue.getSummary()).thenReturn("my issue");
    }

    /**
     * Verifies that any found links are added to the IssueBean.
     *
     * @throws Exception if anything goes wrong
     */
    @Test
    public void buildIssueLinks_linkAdded() throws Exception {
        final ApplicationUser user = new MockApplicationUser(USER_NAME);
        when(authContext.getUser()).thenReturn(user);
        when(applicationProperties.getOption(APKeys.JIRA_OPTION_ISSUELINKING)).thenReturn(true);
        when(issue.getId()).thenReturn(1234L);

        final String linkTypeName = "duplicates";
        final String linkedIssueKey = "LNK-2";
        final Issue linkedIssue = createMockIssue(10001L, linkedIssueKey);
        final IssueLinkType linkType = createMockIssueLinkType(linkTypeName);
        IssueLink issueLink = mock(IssueLink.class);
        when(issueLink.getId()).thenReturn(10014L);

        final LinkCollection linkCollection = mock(LinkCollection.class);
        when(linkCollection.getLinkTypes()).thenReturn(singleton(linkType));
        when(linkCollection.getOutwardIssues(linkTypeName)).thenReturn(singletonList(linkedIssue));
        when(linkCollection.getInwardIssues(linkTypeName)).thenReturn(Collections.<Issue>emptyList());

        when(issueLinkManager.getLinkCollection(issue, user)).thenReturn(linkCollection);
        when(issueLinkManager.getIssueLink(1234L, 10001L, 10000L)).thenReturn(issueLink);
        when(uriBuilder.build(contextUriInfo, IssueResource.class, linkedIssueKey)).thenReturn(new URI(ISSUE_BASE_URI + linkedIssueKey));

        final IssueLinksBeanBuilder builder = createIssueLinkBeanBuilder();
        final List<IssueLinkJsonBean> links = builder.buildIssueLinks();

        // make sure the issue link was added
        assertThat(links, hasSize(1));
        assertThat(links.get(0).outwardIssue().id(), is(linkedIssue.getId().toString()));
        assertThat(links.get(0).outwardIssue().key(), is(linkedIssue.getKey()));
    }

    @Test
    public void buildIssueLinks_noLinkTypes() throws Exception {
        final ApplicationUser user = new MockApplicationUser(USER_NAME);
        when(authContext.getUser()).thenReturn(user);
        when(applicationProperties.getOption(APKeys.JIRA_OPTION_ISSUELINKING)).thenReturn(true);

        final String linkedIssueKey = "LNK-2";

        final LinkCollection linkCollection = mock(LinkCollection.class);
        when(linkCollection.getLinkTypes()).thenReturn(null);

        when(issueLinkManager.getLinkCollection(issue, user)).thenReturn(linkCollection);
        when(uriBuilder.build(uriInfo, IssueResource.class, linkedIssueKey)).thenReturn(new URI(ISSUE_BASE_URI + linkedIssueKey));

        final IssueLinksBeanBuilder builder = createIssueLinkBeanBuilder();
        final List<IssueLinkJsonBean> links = builder.buildIssueLinks();

        // make sure no links were found
        assertThat(links, empty());
    }

    @Test
    public void buildIssueLinks_noOutwardLinksOfType() throws Exception {
        final ApplicationUser user = new MockApplicationUser(USER_NAME);
        when(authContext.getUser()).thenReturn(user);
        when(applicationProperties.getOption(APKeys.JIRA_OPTION_ISSUELINKING)).thenReturn(true);

        final String linkTypeName = "duplicates";
        final String linkedIssueKey = "LNK-2";
        final IssueLinkType linkType = createMockIssueLinkType(linkTypeName);

        final LinkCollection linkCollection = mock(LinkCollection.class);
        when(linkCollection.getLinkTypes()).thenReturn(singleton(linkType));
        when(linkCollection.getOutwardIssues(linkTypeName)).thenReturn(Collections.<Issue>emptyList());
        when(linkCollection.getInwardIssues(linkTypeName)).thenReturn(Collections.<Issue>emptyList());

        when(issueLinkManager.getLinkCollection(issue, user)).thenReturn(linkCollection);
        when(uriBuilder.build(uriInfo, IssueResource.class, linkedIssueKey)).thenReturn(new URI(ISSUE_BASE_URI + linkedIssueKey));

        final IssueLinksBeanBuilder builder = createIssueLinkBeanBuilder();
        final List<IssueLinkJsonBean> links = builder.buildIssueLinks();

        // make sure no links were found
        assertThat(links, empty());
    }

    @Test
    public void buildParentLink() throws Exception {
        final String parentIssueKey = "PAR-1";
        final Issue parent = createMockIssue(10001L, parentIssueKey);

        when(issue.getParentObject()).thenReturn(parent);
        when(uriBuilder.build(contextUriInfo, IssueResource.class, parentIssueKey)).thenReturn(new URI(ISSUE_BASE_URI + parentIssueKey));

        final IssueLinksBeanBuilder builder = createIssueLinkBeanBuilder();
        final IssueRefJsonBean parentLink = builder.buildParentLink();

        assertThat(parentLink.id(), is(parent.getId().toString()));
        assertThat(parentLink.key(), is(parent.getKey()));
    }

    @Test
    public void subtaskLinkAdded() throws Exception {
        final String subtaskIssueKey = "SUB-1";
        final Issue subtask = createMockIssue(10001L, subtaskIssueKey);

        when(issue.getSubTaskObjects()).thenReturn(singletonList(subtask));
        when(uriBuilder.build(contextUriInfo, IssueResource.class, subtaskIssueKey)).thenReturn(new URI(ISSUE_BASE_URI + subtaskIssueKey));

        final IssueLinksBeanBuilder builder = createIssueLinkBeanBuilder();
        final List<IssueRefJsonBean> subtaskLinks = builder.buildSubtaskLinks();

        assertThat(subtaskLinks, hasSize(1));
        assertThat(subtaskLinks.get(0).id(), is(subtask.getId().toString()));
        assertThat(subtaskLinks.get(0).key(), is(subtask.getKey()));
    }

    private IssueLinkType createMockIssueLinkType(final String name) {
        final IssueLinkType mockLinkType = mock(IssueLinkType.class);
        when(mockLinkType.getId()).thenReturn(10000L);
        when(mockLinkType.getName()).thenReturn(name);
        when(mockLinkType.getInward()).thenReturn("my inward");
        when(mockLinkType.getOutward()).thenReturn("my outward");

        return mockLinkType;
    }

    private IssueLinksBeanBuilder createIssueLinkBeanBuilder() {
        return new IssueLinksBeanBuilder(applicationProperties, issueLinkManager, authContext, jiraBaseUrls, issue);
    }

    private Issue createMockIssue(final Long id, final String key) {
        final Issue mockIssue = mock(Issue.class);
        when(mockIssue.getId()).thenReturn(id);
        when(mockIssue.getKey()).thenReturn(key);
        when(mockIssue.getSummary()).thenReturn("issue " + key);

        return mockIssue;
    }
}
