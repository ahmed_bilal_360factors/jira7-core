package com.atlassian.jira.rest.v2.issue;

import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.bc.config.ConstantsService;
import com.atlassian.jira.issue.fields.rest.json.beans.StatusCategoryJsonBean;
import com.atlassian.jira.issue.status.category.StatusCategory;
import com.atlassian.jira.rest.exception.NotFoundWebException;
import com.atlassian.jira.rest.util.StatusCategoryHelper;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;
import org.apache.commons.httpclient.HttpStatus;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Unit test for StatusCategoryResource
 *
 * @since v6.1
 */
public class StatusCategoryResourceTest {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private JiraAuthenticationContext authContext;
    @Mock
    private ConstantsService constantsService;
    @Mock
    private StatusCategoryHelper statusCategoryHelper;

    private StatusCategoryResource statusCategoryResource;
    private MockApplicationUser user;

    @Before
    public void setUp() {
        user = new MockApplicationUser("mockUser");
        when(authContext.getUser()).thenReturn(user);
        statusCategoryResource = new StatusCategoryResource(authContext, constantsService, statusCategoryHelper);
    }

    @Test
    public void testStatusFound() throws Exception {
        final StatusCategory category = mock(StatusCategory.class);
        final UriInfo uriInfo = mock(UriInfo.class);

        when(constantsService.getStatusCategoryById(user, "2")).thenReturn(ServiceOutcomeImpl.ok(category));
        final StatusCategoryJsonBean expectedResponse = new StatusCategoryJsonBean("self", 2L, "key", "color", "name");
        when(statusCategoryHelper.createStatusCategoryBean(category, uriInfo, StatusCategoryResource.class)).thenReturn(
                expectedResponse);

        final Response resp = statusCategoryResource.getStatusCategory("2", mock(Request.class), uriInfo);

        assertEquals(HttpStatus.SC_OK, resp.getStatus());
        assertSame(expectedResponse, resp.getEntity());
    }

    @Test(expected = NotFoundWebException.class)
    public void testStatusCategoryNotFound() throws Exception {
        final String categoryName = "somecategory";

        final ServiceOutcomeImpl<StatusCategory> negativeResponse = new ServiceOutcomeImpl<StatusCategory>(new SimpleErrorCollection("Serious error", ErrorCollection.Reason.SERVER_ERROR), null);
        when(constantsService.getStatusCategoryById(user, categoryName)).thenReturn(negativeResponse);
        when(constantsService.getStatusCategoryByKey(user, categoryName)).thenReturn(negativeResponse);

        statusCategoryResource.getStatusCategory(categoryName, mock(Request.class), mock(UriInfo.class));
    }

}
