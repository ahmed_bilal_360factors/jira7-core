package com.atlassian.jira.rest.v2.issue;

import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.avatar.AvatarPickerHelper;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.bc.project.ProjectAction;
import com.atlassian.jira.bc.project.ProjectCreationData;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.bc.project.version.VersionService;
import com.atlassian.jira.bc.projectroles.ProjectRoleService;
import com.atlassian.jira.blueprint.core.api.CoreProjectConfigurator;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.security.IssueSecuritySchemeManager;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.mock.project.MockVersion;
import com.atlassian.jira.notification.NotificationSchemeManager;
import com.atlassian.jira.permission.PermissionSchemeManager;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectCategoryImpl;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.rest.exception.NotFoundWebException;
import com.atlassian.jira.rest.util.AttachmentHelper;
import com.atlassian.jira.rest.util.ProjectFinder;
import com.atlassian.jira.rest.util.ResponseFactory;
import com.atlassian.jira.rest.util.ResponseFactoryImpl;
import com.atlassian.jira.rest.v2.issue.project.ProjectBean;
import com.atlassian.jira.rest.v2.issue.project.ProjectBeanFactory;
import com.atlassian.jira.rest.v2.issue.project.ProjectBeanFactoryImpl;
import com.atlassian.jira.rest.v2.issue.project.ProjectIdentity;
import com.atlassian.jira.rest.v2.issue.project.ProjectInputBean;
import com.atlassian.jira.rest.v2.issue.project.ProjectRoleBeanFactory;
import com.atlassian.jira.rest.v2.issue.version.VersionBean;
import com.atlassian.jira.rest.v2.issue.version.VersionBeanFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.xsrf.XsrfInvocationChecker;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.UserProjectHistoryManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.ErrorCollection.Reason;
import com.atlassian.jira.util.ErrorCollections;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.jira.util.OrderByRequestParserImpl;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang3.RandomStringUtils;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import javax.annotation.Nullable;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.lang.reflect.Method;
import java.net.URI;
import java.util.Arrays;
import java.util.List;

import static com.atlassian.jira.bc.project.ProjectService.CreateProjectValidationResult;
import static com.atlassian.jira.bc.project.ProjectService.DeleteProjectResult;
import static com.atlassian.jira.bc.project.ProjectService.DeleteProjectValidationResult;
import static com.atlassian.jira.bc.project.ProjectService.GetProjectResult;
import static com.atlassian.jira.bc.project.ProjectService.UpdateProjectSchemesValidationResult;
import static com.atlassian.jira.rest.assertions.ResponseAssertions.assertResponseBody;
import static com.atlassian.jira.rest.assertions.ResponseAssertions.assertResponseCacheNever;
import static com.atlassian.jira.rest.v2.issue.project.ProjectInputBean.CREATE_EXAMPLE;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v4.4
 */
public class TestProjectResource {
    private static final Function<Object, String> GET_ID = new Function<Object, String>() {
        @Override
        public String apply(@Nullable final Object obj) {
            try {
                return Objects.firstNonNull(PropertyUtils.getProperty(obj, "id"), "").toString();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    };

    public static final String LINK_TO_PROJECT = "http://jira/rest/link-to-project";
    public static final Project PROJECT_CREATED_BY_SERVICE = new MockProject(42L, "PR", "mock project");

    @Rule
    public RuleChain chain = MockitoMocksInContainer.forTest(this);

    @Mock
    private JiraAuthenticationContext authContext;

    @Mock
    private ProjectService projectService;

    @Mock
    private VersionService versionService;

    @Mock
    private AvatarService avatarService;

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private PermissionSchemeManager permissionSchemeManager;

    @Mock
    private NotificationSchemeManager notificationSchemeManager;

    @Mock
    private IssueSecuritySchemeManager issueSecuritySchemeManager;

    @Mock
    private ProjectManager projectManager;

    @Mock
    private AvatarManager avatarManager;

    @Mock
    private AvatarPickerHelper avatarPickerHelper;

    @Mock
    private AttachmentHelper attachmentHelper;

    @Mock
    private VersionBeanFactory versionBeanFactory;

    @Mock
    private JiraBaseUrls jiraBaseUrls;

    @Mock
    private ProjectFinder projectFinder;

    @Mock
    private XsrfInvocationChecker xsrfChecker;

    @Mock
    private UriInfo uriInfo;

    @Mock
    private ResourceUriBuilder uriBuilder;

    @Mock
    private ProjectRoleService projectRoleService;

    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;

    @Mock
    private UserManager userManager;

    @Mock
    private ProjectRoleBeanFactory projectRoleBeanFactory;

    @Mock
    private UserProjectHistoryManager projectHistoryManager;

    @Mock
    private I18nHelper i18nHelper;

    @Mock
    private CoreProjectConfigurator coreProjectConfigurator;

    private ResponseFactory responses = new ResponseFactoryImpl(new NoopI18nHelper());

    private ProjectBeanFactory projectBeanFactory;

    private ProjectResource projectResource;

    private final ApplicationUser loggedUser = new MockApplicationUser("Charlie");

    @Before
    public void setUp() {
        when(authContext.getUser()).thenReturn(loggedUser);
        when(authContext.getUser()).thenReturn(loggedUser);
        when(authContext.isLoggedInUser()).thenReturn(Boolean.TRUE);
        when(uriBuilder.build(Mockito.<UriInfo>any(), Mockito.<Class<?>>any(), anyString())).thenReturn(URI.create(LINK_TO_PROJECT));
        when(projectManager.getProjectCategoryObject(CREATE_EXAMPLE.getCategoryId())).thenReturn(new ProjectCategoryImpl(CREATE_EXAMPLE.getCategoryId(), "example", "example"));
        projectBeanFactory = new ProjectBeanFactoryImpl(versionBeanFactory, uriInfo, uriBuilder, projectRoleService,
                jiraAuthenticationContext, userManager, jiraBaseUrls, projectManager, projectRoleBeanFactory);
        projectResource = new ProjectResource(projectService, authContext, null, versionService, null, avatarService, userManager, projectBeanFactory,
                versionBeanFactory, permissionManager, projectManager, avatarManager, avatarPickerHelper,
                attachmentHelper, jiraBaseUrls, null, null, permissionSchemeManager, notificationSchemeManager, issueSecuritySchemeManager,
                null, null, projectFinder, xsrfChecker, projectHistoryManager, responses, i18nHelper, coreProjectConfigurator, new OrderByRequestParserImpl(new MockI18nHelper()));
    }

    @Test
    public void testGetProjectVersionsAnnotations() throws NoSuchMethodException {
        Method method = ProjectResource.class.getMethod("getProjectVersions", String.class, String.class);
        Path path = method.getAnnotation(Path.class);
        assertNotNull(path);
        assertEquals("{projectIdOrKey}/versions", path.value());
    }

    @Test
    public void testGetProjectVersionsBadProject() throws Exception {
        String key = "key";
        String error = "We have a problem";

        when(projectFinder.getGetProjectForActionByIdOrKey(loggedUser, key, ProjectAction.VIEW_PROJECT)).thenReturn(
                new GetProjectResult(errors(Reason.NOT_FOUND, error)));

        final Response response = projectResource.getProjectVersions(key, null);
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
        assertEquals(com.atlassian.jira.rest.api.util.ErrorCollection.of(error).reason(Reason.NOT_FOUND), response.getEntity());
    }

    @Test
    public void testGetProjectVersionsBadVersion() throws Exception {
        String key = "key";
        String error = "We have a problem";
        MockProject project = new MockProject(10101L);

        when(projectFinder.getGetProjectForActionByIdOrKey(loggedUser, key, ProjectAction.VIEW_PROJECT))
                .thenReturn(new GetProjectResult(ok(), project));

        when(versionService.getVersionsByProject(loggedUser, project))
                .thenReturn(new VersionService.VersionsResult(errors(error)));

        try {
            projectResource.getProjectVersions(key, null);
            fail("Should have thrown exceptions.");
        } catch (NotFoundWebException er) {
            assertEquals(com.atlassian.jira.rest.api.util.ErrorCollection.of(error).reason(Reason.VALIDATION_FAILED), er.getResponse().getEntity());
        }
    }

    @Test
    public void testGetProjectVersionsGood() throws Exception {
        String key = "key";
        MockProject project = new MockProject(10101L);
        MockVersion version = new MockVersion(181828L, "verion1");
        List<Version> versions = Lists.<Version>newArrayList(version);
        List<VersionBean> beans = newArrayList(new VersionBean());

        when(projectFinder.getGetProjectForActionByIdOrKey(loggedUser, key, ProjectAction.VIEW_PROJECT))
                .thenReturn(new GetProjectResult(ok(), project));

        when(versionService.getVersionsByProject(loggedUser, project))
                .thenReturn(new VersionService.VersionsResult(ok(), versions));
        when(versionBeanFactory.createVersionBeans(versions, false)).thenReturn(beans);

        Response actualResponse = projectResource.getProjectVersions(key, null);

        assertResponseBody(beans, actualResponse);
        assertResponseCacheNever(actualResponse);
    }

    @Test
    public void testGetProjectsNoExpand() throws Exception {
        MockProject project = mockProject(10101L);
        project.setDescription("My special project");
        List<Project> projects = Lists.<Project>newArrayList(project);

        when(projectService.getAllProjectsForAction(loggedUser, ProjectAction.VIEW_PROJECT))
                .thenReturn(new ServiceOutcomeImpl<>(ok(), projects));

        Response actualResponse = projectResource.getAllProjects(null, null);

        List<?> responseBeans = (List<?>) actualResponse.getEntity();
        assertEquals("Wrong result count.", 1, responseBeans.size());
        ProjectBean responseBean = (ProjectBean) responseBeans.get(0);

        assertEquals("Wrong ID.", String.valueOf(project.getId()), responseBean.getId());
        assertNull("Should not have expanded description.", responseBean.getDescription());

        assertResponseCacheNever(actualResponse);
    }

    @Test
    public void testGetProjectsExpandDescription() throws Exception {
        MockProject project = mockProject(10101L);
        project.setDescription("My special project");
        List<Project> projects = Lists.<Project>newArrayList(project);

        when(projectService.getAllProjectsForAction(loggedUser, ProjectAction.VIEW_PROJECT))
                .thenReturn(new ServiceOutcomeImpl<>(ok(), projects));

        Response actualResponse = projectResource.getAllProjects("description", null);

        List<?> responseBeans = (List<?>) actualResponse.getEntity();
        assertEquals("Wrong result count.", 1, responseBeans.size());
        ProjectBean responseBean = (ProjectBean) responseBeans.get(0);

        assertEquals("Wrong ID.", String.valueOf(project.getId()), responseBean.getId());
        assertEquals("Wrong expanded description.", project.getDescription(), responseBean.getDescription());

        assertResponseCacheNever(actualResponse);
    }

    @Test
    public void testProjectIsCreatedUsingCorrectCallsAndArgumentsInProjectService() {
        ProjectInputBean projectToCreate = CREATE_EXAMPLE;
        ApplicationUser leadUser = new MockApplicationUser(projectToCreate.getLead());
        letProjectServiceValidateEverythingAllRight();
        ProjectCreationData projectCreationData = new ProjectCreationData.Builder()
                .withName(projectToCreate.getName())
                .withKey(projectToCreate.getKey())
                .withDescription(projectToCreate.getDescription())
                .withLead(leadUser)
                .withUrl(projectToCreate.getUrl())
                .withAssigneeType(projectToCreate.getAssigneeType().getId())
                .withAvatarId(projectToCreate.getAvatarId())
                .withType(projectToCreate.getProjectTypeKey())
                .withProjectTemplateKey(projectToCreate.getProjectTemplateKey())
                .build();
        when(userManager.getUserByName(projectToCreate.getLead())).thenReturn(leadUser);

        UpdateProjectSchemesValidationResult schemesValidationResult = schemesValidationResut(projectToCreate.getPermissionScheme(), projectToCreate.getNotificationScheme(), projectToCreate.getIssueSecurityScheme());

        projectResource.createProject(projectToCreate);

        verify(projectService).validateCreateProject(loggedUser, projectCreationData);
        verify(projectService).validateUpdateProjectSchemes(loggedUser, schemesValidationResult.getPermissionSchemeId(),
                schemesValidationResult.getNotificationSchemeId(), schemesValidationResult.getIssueSecuritySchemeId());
        verify(projectService).updateProjectSchemes(schemesValidationResult, PROJECT_CREATED_BY_SERVICE);
        verify(projectService).createProject(Mockito.argThat(Matchers.equalTo(createValidationResult(projectCreationData))));
    }

    @Test
    public void coreProjectConfigurationIsNotAppliedIfAProjectTemplateKeyWasPassedWhenTheProjectIsCreated() {
        ProjectInputBean projectToCreate = ProjectInputBean.builder()
                .setProjectTemplateKey("com.atlassian.jira-legacy-project-templates:jira-blank-item")
                .build();
        letProjectServiceValidateEverythingAllRight();

        projectResource.createProject(projectToCreate);

        assertCoreConfigurationWasNotApplied();
    }

    @Test
    public void coreProjectConfigurationIsAppliedIfAProjectTemplateKeyThatIsEmptyWasPassedWhenTheProjectIsCreated() {
        ProjectInputBean projectToCreate = ProjectInputBean.builder()
                .setProjectTemplateKey("")
                .build();
        letProjectServiceValidateEverythingAllRight();

        projectResource.createProject(projectToCreate);

        assertCoreConfigurationWasApplied();
    }

    @Test
    public void coreProjectConfigurationIsAppliedIfAProjectTemplateKeyWasNotPassedWhenTheProjectIsCreated() {
        ProjectInputBean projectToCreate = ProjectInputBean.builder().build();
        letProjectServiceValidateEverythingAllRight();

        projectResource.createProject(projectToCreate);

        assertCoreConfigurationWasApplied();
    }

    @Test
    public void testLocationAndSelfLinkAreReturnedCorrectly() {
        letProjectServiceValidateEverythingAllRight();
        Response creationResponse = projectResource.createProject(CREATE_EXAMPLE);
        assertThat(creationResponse.getMetadata().getFirst("location").toString(), Matchers.<Object>equalTo(LINK_TO_PROJECT));
        assertThat(((ProjectIdentity) creationResponse.getEntity()).getSelf().toString(), Matchers.equalTo(LINK_TO_PROJECT));
    }

    @Test
    public void testProjectIsDeletedUsingCorrectCallsAndArgumentsInProjectService() {
        when(projectFinder.getGetProjectForActionByIdOrKey(loggedUser, "PR", ProjectAction.EDIT_PROJECT_CONFIG))
                .thenReturn(new GetProjectResult(ok(), new MockProject(42L, "PR")));

        letProjectServiceValidateEverythingAllRight();
        projectResource.deleteProject("PR");
        verify(projectService).validateDeleteProject(loggedUser, "PR");
        verify(projectService).deleteProject(loggedUser, deleteProjectValidationResult("PR"));
    }

    @Test
    public void testRequestedNumberOfProjectsIsReturned() {
        List<Project> lotsOfRecentProjects = ImmutableList.of(project(1L), project(2L), project(3L), project(4L), project(5L), project(6L), project(7L));
        when(projectHistoryManager.getProjectHistoryWithPermissionChecks(ProjectAction.VIEW_PROJECT, loggedUser)).thenReturn(lotsOfRecentProjects);
        for (int i = 0; i < lotsOfRecentProjects.size(); i++) {
            assertThat(getIds(projectResource.getAllProjects("", i).getEntity()), equalTo(getIds(lotsOfRecentProjects.subList(0, i))));
        }
    }

    public List<String> getIds(Object obj) {
        return newArrayList(transform((List<Object>) obj, GET_ID));
    }

    @Test
    public void testOkHttpStatusIsReturnedWhenGettingRecentProjects() {
        when(projectHistoryManager.getProjectHistoryWithPermissionChecks(ProjectAction.VIEW_PROJECT, loggedUser)).thenReturn(singletonList(project(1L)));
        assertThat(projectResource.getAllProjects("", 5).getStatus(), equalTo(Response.Status.OK.getStatusCode()));
    }

    private static Project project(long id) {
        return new MockProject(id, RandomStringUtils.randomAlphabetic(4), new ProjectTypeKey(RandomStringUtils.randomAlphabetic(10)));
    }

    private CreateProjectValidationResult createValidationResult(ProjectCreationData projectCreationData) {
        return new CreateProjectValidationResult(errors(), null, projectCreationData) {
            @Override
            public boolean equals(final Object obj) {
                return EqualsBuilder.reflectionEquals(this, obj);
            }

            @Override
            public int hashCode() {
                return 1;
            }
        };
    }

    private UpdateProjectSchemesValidationResult schemesValidationResut(Long permissionScheme, Long notificationScheme, Long issueSecurityScheme) {
        return new UpdateProjectSchemesValidationResult(errors(), permissionScheme, notificationScheme, issueSecurityScheme) {
            @Override
            public boolean equals(final Object obj) {
                return EqualsBuilder.reflectionEquals(this, obj);
            }

            @Override
            public int hashCode() {
                return 1;
            }
        };
    }

    private DeleteProjectValidationResult deleteProjectValidationResult(final String projectKey) {
        return new DeleteProjectValidationResult(ErrorCollections.empty(), new MockProject(42l, projectKey)) {
            @Override
            public boolean equals(final Object obj) {
                return EqualsBuilder.reflectionEquals(this, obj);
            }

            @Override
            public int hashCode() {
                return 1;
            }
        };
    }

    private void letProjectServiceValidateEverythingAllRight() {
        when(projectService.validateCreateProject(eq(loggedUser), any(ProjectCreationData.class))).then(new Answer<ProjectService.CreateProjectValidationResult>() {
            @Override
            public CreateProjectValidationResult answer(final InvocationOnMock invocationOnMock) throws Throwable {
                Object[] args = invocationOnMock.getArguments();
                return createValidationResult((ProjectCreationData) args[1]);
            }
        });
        Answer<ProjectService.UpdateProjectSchemesValidationResult> schemesValidationAnswer = new Answer<ProjectService.UpdateProjectSchemesValidationResult>() {
            @Override
            public UpdateProjectSchemesValidationResult answer(final InvocationOnMock invocationOnMock)
                    throws Throwable {
                Object[] args = invocationOnMock.getArguments();
                return schemesValidationResut((Long) args[1], (Long) args[2], (Long) args[3]);
            }
        };
        when(projectService.validateUpdateProjectSchemes(eq(loggedUser), anyLong(), anyLong(), anyLong())).then(schemesValidationAnswer);
        when(projectService.validateUpdateProjectSchemes(eq(loggedUser), anyLong(), anyLong(), anyLong())).then(schemesValidationAnswer);
        when(projectService.createProject(Mockito.<CreateProjectValidationResult>any())).then(new Answer<Project>() {
            @Override
            public Project answer(final InvocationOnMock invocationOnMock) throws Throwable {
                CreateProjectValidationResult arg = (CreateProjectValidationResult) invocationOnMock.getArguments()[0];
                return PROJECT_CREATED_BY_SERVICE;
            }
        });

        when(projectService.validateDeleteProject(eq(loggedUser), anyString())).then(new Answer<DeleteProjectValidationResult>() {
            @Override
            public DeleteProjectValidationResult answer(final InvocationOnMock invocation) throws Throwable {
                return deleteProjectValidationResult((String) invocation.getArguments()[1]);
            }
        });

        when(projectService.deleteProject(eq(loggedUser), any(DeleteProjectValidationResult.class))).then(new Answer<DeleteProjectResult>() {
            @Override
            public DeleteProjectResult answer(final InvocationOnMock invocation) throws Throwable {
                return new DeleteProjectResult(ErrorCollections.empty());
            }
        });
    }

    private static ErrorCollection errors(String... errors) {
        return errors(Reason.VALIDATION_FAILED, errors);
    }

    private static ErrorCollection errors(Reason reason, String... errors) {
        SimpleErrorCollection collection = new SimpleErrorCollection();
        collection.addErrorMessages(Arrays.asList(errors));
        collection.addReason(reason);
        return collection;
    }

    private static ErrorCollection ok() {
        return new SimpleErrorCollection();
    }

    private MockProject mockProject(long id) {
        return new MockProject(id, null, new ProjectTypeKey("any-type"));
    }

    private void assertCoreConfigurationWasApplied() {
        verify(coreProjectConfigurator).configure(any(Project.class));
    }

    private void assertCoreConfigurationWasNotApplied() {
        verify(coreProjectConfigurator, never()).configure(any(Project.class));
    }
}
