package com.atlassian.jira.rest.util;

import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.xsrf.XsrfCheckResult;
import com.atlassian.jira.security.xsrf.XsrfInvocationChecker;
import com.atlassian.jira.util.I18nHelper;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * @since v4.4
 */
public class TestAttachmentHelper {
    @Rule
    public InitMockitoMocks mockitoMocks = new InitMockitoMocks(this);

    @Mock
    private HttpServletRequest request;

    @Mock
    private ServletContext servletContext;

    @Mock
    private XsrfInvocationChecker checker;

    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;

    @Mock
    private I18nHelper i18nHelper;

    @InjectMocks
    private AttachmentHelper attachmentHelper;

    @Before
    public void createDeps() {
        when(jiraAuthenticationContext.getI18nHelper()).thenReturn(i18nHelper);
    }

    @Test
    public void validationFailsWhenXsrfIsInvalid() {
        //given
        when(checker.checkWebRequestInvocation(request)).thenReturn(new Result(true, false));

        //when
        AttachmentHelper.ValidationResult validationResult = attachmentHelper.validate(request, "test.jpg", 15430L);

        //then
        assertEquals(validationResult.getErrorType(), AttachmentHelper.ValidationError.XSRF_TOKEN_INVALID);
    }

    @Test
    public void validationFailsWhenFilenameIsEmpty() {
        //given
        when(checker.checkWebRequestInvocation(request)).thenReturn(new Result(true, true));

        //when
        AttachmentHelper.ValidationResult validationResult = attachmentHelper.validate(request, null, 15430L);

        //then
        assertEquals(validationResult.getErrorType(), AttachmentHelper.ValidationError.FILENAME_BLANK);
    }

    @Test
    public void validationFailsWhenInputStreamThrowsIOException() throws IOException {
        //given
        when(checker.checkWebRequestInvocation(request)).thenReturn(new Result(true, true));
        when(request.getInputStream()).thenThrow(new IOException());
        AttachmentHelper attachmentHelper = new AttachmentHelper(checker, jiraAuthenticationContext) {
            @Override
            String getMaxAttachmentSize() {
                return "10000000";
            }
        };

        //when
        AttachmentHelper.ValidationResult validationResult = attachmentHelper.validate(request, "test.jpg", 15430L);

        //then
        assertEquals(validationResult.getErrorType(), AttachmentHelper.ValidationError.ATTACHMENT_IO_UNKNOWN);
    }

    @Test
    public void validationFailsWhenAttachmentSizeIsGreaterThanMax() throws IOException {
        //given
        when(checker.checkWebRequestInvocation(request)).thenReturn(new Result(true, true));
        AttachmentHelper attachmentHelper = new AttachmentHelper(checker, jiraAuthenticationContext) {
            @Override
            String getMaxAttachmentSize() {
                return "100";
            }
        };

        //when
        AttachmentHelper.ValidationResult validationResult = attachmentHelper.validate(request, "test.jpg", 15430L);

        //then
        assertEquals(validationResult.getErrorType(), AttachmentHelper.ValidationError.ATTACHMENT_TO_LARGE);
    }

    @Test
    public void validationFailsWhenAttachmentSizeIsUndefined() {

        //given
        when(checker.checkWebRequestInvocation(request)).thenReturn(new Result(true, true));
        when(request.getContentLength()).thenReturn(-1);
        AttachmentHelper attachmentHelper = new AttachmentHelper(checker, jiraAuthenticationContext);

        //when
        AttachmentHelper.ValidationResult validationResult = attachmentHelper.validate(request, "test.jpg", null);

        //then
        assertEquals(validationResult.getErrorType(), AttachmentHelper.ValidationError.ATTACHMENT_IO_SIZE);
    }

    private static class Result implements XsrfCheckResult {
        private final boolean required;
        private final boolean valid;

        private Result(boolean required, boolean valid) {
            this.required = required;
            this.valid = valid;
        }

        public boolean isRequired() {
            return required;
        }

        public boolean isValid() {
            return valid;
        }

        public boolean isGeneratedForAuthenticatedUser() {
            return false;
        }
    }
}
