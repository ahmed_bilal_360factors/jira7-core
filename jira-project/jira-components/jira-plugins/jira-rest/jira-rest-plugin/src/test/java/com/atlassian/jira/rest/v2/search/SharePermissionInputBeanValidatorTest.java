package com.atlassian.jira.rest.v2.search;


import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.util.I18nHelper;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class SharePermissionInputBeanValidatorTest {

    private SharePermissionInputBeanValidator validator;

    @Before
    public void setUp() throws Exception {
        validator = new SharePermissionInputBeanValidator();
    }

    @Test
    public void validationPassesWithValidInputs() {
        validationPassesWithValidInput(inputBean("group", "jira-users", null, null));
        validationPassesWithValidInput(inputBean("project", null, "2", null));
        validationPassesWithValidInput(inputBean("projectRole", null, "2", "10000"));
        validationPassesWithValidInput(inputBean("global", null, null, null));
    }

    @Test
    public void validationFailsWithInvalidInputs() {
        validationFailsWithMessageKeyAndParam(inputBean("asdf", "jira-users", null, null), "rest.filters.add.permission.invalid.type", "[PROJECT, GROUP, PROJECT_ROLE, GLOBAL, AUTHENTICATED]");
        validationFailsWithMessageKey(inputBean("group", null, null, null), "rest.filters.add.permission.no.group.name.given");
        validationFailsWithMessageKey(inputBean("project", null, null, null), "rest.filters.add.permission.no.project.id.given");
        validationFailsWithMessageKey(inputBean("projectRole", null, null, null), "rest.filters.add.permission.no.project.role.id.given");
    }

    private void validationFailsWithMessageKey(SharePermissionInputBean inputBean, String messageKey) {
        final I18nHelper i18n = mock(I18nHelper.class);
        ServiceResult serviceResult = validator.validateAddPermissionBean(inputBean, i18n);
        assertFalse(serviceResult.isValid());
        verify(i18n).getText(messageKey);
    }

    private void validationFailsWithMessageKeyAndParam(SharePermissionInputBean inputBean, String messageKey, String param) {
        final I18nHelper i18n = mock(I18nHelper.class);
        ServiceResult serviceResult = validator.validateAddPermissionBean(inputBean, i18n);
        assertFalse(serviceResult.isValid());
        verify(i18n).getText(messageKey, param);
    }

    private void validationPassesWithValidInput(SharePermissionInputBean input) {
        ServiceResult serviceResult = validator.validateAddPermissionBean(input, null);
        assertTrue(serviceResult.isValid());
    }

    private SharePermissionInputBean inputBean(String type, String groupname, String projectId, String projectRoleId) {
        SharePermissionInputBean inputBean = new SharePermissionInputBean();
        inputBean.setType(type);
        inputBean.setGroupname(groupname);
        inputBean.setProjectId(projectId);
        inputBean.setProjectRoleId(projectRoleId);
        return inputBean;
    }

}