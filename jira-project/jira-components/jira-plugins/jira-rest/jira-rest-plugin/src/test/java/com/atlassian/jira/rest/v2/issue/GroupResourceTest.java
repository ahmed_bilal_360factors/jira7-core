package com.atlassian.jira.rest.v2.issue;

import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.crowd.exception.runtime.OperationFailedException;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.group.GroupRemoveChildMapper;
import com.atlassian.jira.bc.group.GroupService;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.fields.rest.json.UserBeanFactory;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.rest.api.util.StringList;
import com.atlassian.jira.rest.exception.BadRequestWebException;
import com.atlassian.jira.rest.exception.ForbiddenWebException;
import com.atlassian.jira.rest.exception.NotFoundWebException;
import com.atlassian.jira.rest.internal.PermissionHelper;
import com.atlassian.jira.rest.util.ResponseFactory;
import com.atlassian.jira.rest.util.ResponseFactoryImpl;
import com.atlassian.jira.rest.util.SelfLinkBuilder;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockGroup;
import com.atlassian.jira.user.MockUser;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.plugins.rest.common.expand.EntityCrawler;
import com.atlassian.plugins.rest.common.expand.parameter.DefaultExpandParameter;
import com.atlassian.plugins.rest.common.expand.parameter.ExpandParameter;
import com.atlassian.plugins.rest.common.expand.resolver.EntityExpanderResolver;
import com.atlassian.plugins.rest.common.expand.resolver.ListWrapperEntityExpanderResolver;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.RuleChain;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import static com.atlassian.jira.rest.internal.ResponseValidationHelper.assertCreated;
import static com.atlassian.jira.rest.internal.ResponseValidationHelper.assertOk;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class GroupResourceTest {
    private static final String GROUP_NAME = "jedi";
    private static final String SWAP_GROUP = "sith";
    private static final String USER_NAME = "luke";

    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    @Rule
    public RuleChain chain = MockitoMocksInContainer.forTest(this);

    private GroupResource groupResource;

    private PermissionHelper permissionHelper;

    @Mock
    private GlobalPermissionManager globalPermissionManager;

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private GroupManager groupManager;

    @Mock
    private JiraBaseUrls jiraBaseUrls;

    private SelfLinkBuilder selfLinkBuilder;

    @Mock
    private CrowdService crowdService;

    @Mock
    @AvailableInContainer
    private JiraAuthenticationContext authContext;

    @Mock
    private UserUtil userUtilMock;

    @Mock
    private GroupService groupService;

    @Mock
    @AvailableInContainer
    private ApplicationProperties applicationProperties;

    @Mock
    private UserBeanFactory userBeanFactory;

    private ResponseFactory responses = new ResponseFactoryImpl(new NoopI18nHelper());

    private final MockApplicationUser charlie = new MockApplicationUser("charlie");

    @Before
    public void setUp() throws Exception {
        when(jiraBaseUrls.restApi2BaseUrl()).thenReturn(UriBuilder.fromUri("http://localhost").toString());
        selfLinkBuilder = new SelfLinkBuilder(jiraBaseUrls);

        when(applicationProperties.getEncoding()).thenReturn("UTF-8");

        groupResource = new GroupResource(globalPermissionManager,
                authContext,
                mock(I18nHelper.class),
                groupManager,
                groupService,
                jiraBaseUrls,
                selfLinkBuilder,
                crowdService,
                userBeanFactory,
                responses);

        permissionHelper = new PermissionHelper(permissionManager, globalPermissionManager, userUtilMock, authContext);
        when(authContext.getLoggedInUser()).thenReturn(charlie);
    }

    @Test
    public void testAddGroupAdminHappyPath() throws Exception {
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, charlie)).thenReturn(true);

        when(crowdService.addGroup(Matchers.<Group>any())).thenReturn(new MockGroup(GROUP_NAME));

        final Response response = groupResource.createGroup(createAddGroupRequest(GROUP_NAME));
        assertCreated(response);
    }

    @Test
    public void testAddGroupSysAdminHappyPath() throws Exception {
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.SYSTEM_ADMIN, charlie)).thenReturn(true);

        when(crowdService.addGroup(Matchers.<Group>any())).thenReturn(new MockGroup(GROUP_NAME));

        final Response response = groupResource.createGroup(createAddGroupRequest(GROUP_NAME));
        assertCreated(response);
    }

    @Test
    public void testAddGroupAdminEmptyGroupName() throws Exception {
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, charlie)).thenReturn(true);

        expectedException.expect(BadRequestWebException.class);

        groupResource.createGroup(createAddGroupRequest(StringUtils.EMPTY));
    }

    @Test
    public void testAddGroupWithUserAdminHappyPath() throws Exception {
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.SYSTEM_ADMIN, charlie)).thenReturn(true);

        when(crowdService.getUser(Matchers.eq(USER_NAME))).thenReturn(new MockUser(USER_NAME));
        when(crowdService.addGroup(Matchers.<Group>any())).thenReturn(new MockGroup(GROUP_NAME));

        final Response response = groupResource.createGroup(createAddGroupRequest(GROUP_NAME));
        assertCreated(response);
    }

    @Test
    public void testAddGroupNotAdmin() throws Exception {
        when(crowdService.addGroup(Matchers.<Group>any())).thenReturn(new MockGroup(GROUP_NAME));

        expectedException.expect(ForbiddenWebException.class);
        groupResource.createGroup(createAddGroupRequest(GROUP_NAME));
    }

    @Test
    public void testAddGroupWithUserAdminGroupExists() throws Exception {
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.SYSTEM_ADMIN, charlie)).thenReturn(true);
        when(crowdService.getGroup(Matchers.eq(GROUP_NAME))).thenReturn(new MockGroup(GROUP_NAME));

        expectedException.expect(BadRequestWebException.class);

        groupResource.createGroup(createAddGroupRequest(GROUP_NAME));
    }

    @Test
    public void testAddGroupAdminThrowsOperationNotPermittedException() throws Exception {
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, charlie)).thenReturn(true);
        when(crowdService.addGroup(Matchers.<Group>any())).thenThrow(new OperationNotPermittedException());

        expectedException.expect(ForbiddenWebException.class);

        groupResource.createGroup(createAddGroupRequest(GROUP_NAME));
    }

    @Test
    public void testAddUserToGroupAdminHappyPath() throws Exception {
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, charlie)).thenReturn(true);

        final MockUser user = new MockUser(USER_NAME);
        final MockGroup group = new MockGroup(GROUP_NAME);

        when(crowdService.getUser(Matchers.eq(USER_NAME))).thenReturn(user);
        when(groupManager.getGroup(Matchers.eq(GROUP_NAME))).thenReturn(group);
        when(crowdService.addUserToGroup(Matchers.any(User.class), Matchers.any(Group.class))).thenReturn(true);
        when(groupService.validateAddUserToGroup(Matchers.<JiraServiceContext>any(), Matchers.anyList(), Matchers.eq(USER_NAME))).thenReturn(true);

        final Response response = groupResource.addUserToGroup(GROUP_NAME, createAddUserToGroupRequest(USER_NAME));

        assertCreated(response);
    }

    @Test
    public void testAdminCantAddHimselfToSysadminGroup() throws Exception {
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, charlie)).thenReturn(true);
        when(crowdService.getUser(Matchers.eq(USER_NAME))).thenReturn(new MockUser(USER_NAME));
        when(groupManager.getGroup(Matchers.eq(GROUP_NAME))).thenReturn(new MockGroup(GROUP_NAME));
        when(globalPermissionManager.hasPermission(Matchers.eq(GlobalPermissionKey.SYSTEM_ADMIN), Matchers.<ApplicationUser>any())).thenReturn(false);
        setAddUserValidationError(ErrorCollection.Reason.FORBIDDEN);

        Response response = groupResource.addUserToGroup(GROUP_NAME, createAddUserToGroupRequest(USER_NAME));
        assertThat(response.getStatus(), equalTo(Response.Status.FORBIDDEN.getStatusCode()));
    }

    @Test
    public void testAddUserToGroupAdminUserNotExists() throws Exception {
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, charlie)).thenReturn(true);
        when(crowdService.getUser(Matchers.eq(USER_NAME))).thenReturn(null);
        when(groupManager.getGroup(Matchers.eq(GROUP_NAME))).thenReturn(new MockGroup(GROUP_NAME));

        setAddUserValidationError(ErrorCollection.Reason.NOT_FOUND);

        Response response = groupResource.addUserToGroup(GROUP_NAME, createAddUserToGroupRequest(USER_NAME));
        assertThat(response.getStatus(), equalTo(Response.Status.NOT_FOUND.getStatusCode()));
    }

    @Test
    public void testAddUserToGroupAdminGroupNotExists() throws Exception {
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, charlie)).thenReturn(true);
        when(crowdService.getUser(Matchers.eq(USER_NAME))).thenReturn(new MockUser(USER_NAME));
        when(groupManager.getGroup(Matchers.eq(GROUP_NAME))).thenReturn(null);

        expectedException.expect(NotFoundWebException.class);

        groupResource.addUserToGroup(GROUP_NAME, createAddUserToGroupRequest(USER_NAME));
    }

    @Test
    public void testAddUserToGroupAdminThrowsOperationFailedEx() throws Exception {
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, charlie)).thenReturn(true);
        when(crowdService.getUser(Matchers.eq(USER_NAME))).thenReturn(new MockUser(USER_NAME));
        when(groupManager.getGroup(Matchers.eq(GROUP_NAME))).thenReturn(new MockGroup(GROUP_NAME));

        setAddUserValidationError(ErrorCollection.Reason.SERVER_ERROR);

        doThrow(new OperationFailedException()).when(crowdService).addUserToGroup(Mockito.<User>anyObject(), Mockito.<Group>anyObject());

        Response response = groupResource.addUserToGroup(GROUP_NAME, createAddUserToGroupRequest(USER_NAME));
        assertThat(response.getStatus(), equalTo(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode()));
    }

    @Test
    public void testRemoveGroupWithUserAdminHappyPath() throws Exception {
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.SYSTEM_ADMIN, charlie)).thenReturn(true);
        when(groupManager.getGroup(Matchers.eq(GROUP_NAME))).thenReturn(new MockGroup(GROUP_NAME));
        when(groupService.validateDelete(Matchers.<JiraServiceContext>anyObject(), anyString(), anyString())).thenReturn(true);
        when(groupService.delete(Matchers.<JiraServiceContext>anyObject(), anyString(), anyString())).thenReturn(true);

        final Response response = groupResource.removeGroup(GROUP_NAME, SWAP_GROUP);

        assertOk(response);
    }

    @Test
    public void testRemoveGroupWithUserAdminGroupDoesNotExist() throws Exception {
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.SYSTEM_ADMIN, charlie)).thenReturn(true);
        when(groupManager.getGroup(Matchers.eq(GROUP_NAME))).thenReturn(null);

        expectedException.expect(NotFoundWebException.class);
        groupResource.removeGroup(GROUP_NAME, SWAP_GROUP);
    }

    @Test
    public void testRemoveGroupWithUserAdminValidationError() throws Exception {
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.SYSTEM_ADMIN, charlie)).thenReturn(true);
        when(groupManager.getGroup(Matchers.eq(GROUP_NAME))).thenReturn(new MockGroup(GROUP_NAME));
        setDeleteGroupValidationError(ErrorCollection.Reason.SERVER_ERROR);
        when(groupService.delete(Matchers.<JiraServiceContext>anyObject(), anyString(), anyString())).thenReturn(true);

        Response response = groupResource.removeGroup(GROUP_NAME, SWAP_GROUP);

        assertThat(response.getStatus(), equalTo(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode()));
    }

    @Test
    public void testRemoveGroupWithUserNotAdmin() throws Exception {
        when(groupManager.getGroup(Matchers.eq(GROUP_NAME))).thenReturn(new MockGroup(GROUP_NAME));
        when(groupService.validateDelete(Matchers.<JiraServiceContext>anyObject(), anyString(), anyString())).thenReturn(true);
        when(groupService.delete(Matchers.<JiraServiceContext>anyObject(), anyString(), anyString())).thenReturn(true);

        expectedException.expect(ForbiddenWebException.class);

        groupResource.removeGroup(GROUP_NAME, SWAP_GROUP);
    }

    @Test
    public void testRemoveUserFromGroupAdminHappyPath() throws Exception {
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, charlie)).thenReturn(true);

        when(crowdService.getUser(Matchers.eq(USER_NAME))).thenReturn(new MockUser(USER_NAME));
        when(groupManager.getGroup(Matchers.eq(GROUP_NAME))).thenReturn(new MockGroup(GROUP_NAME));
        when(groupService.validateRemoveUserFromGroups(Matchers.<JiraServiceContext>any(), Mockito.eq(ImmutableList.of(GROUP_NAME)), Mockito.eq(USER_NAME))).thenReturn(true);
        when(groupService.removeUsersFromGroups(Matchers.<JiraServiceContext>any(), Mockito.<GroupRemoveChildMapper>any())).thenReturn(true);

        Response response = groupResource.removeUserFromGroup(GROUP_NAME, USER_NAME);

        assertOk(response);
    }

    @Test
    public void testRemoveUserFromGroupAdminGroupDoesntExist() throws Exception {
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, charlie)).thenReturn(true);

        when(crowdService.getUser(Matchers.eq(USER_NAME))).thenReturn(new MockUser(USER_NAME));
        when(groupManager.getGroup(Matchers.eq(GROUP_NAME))).thenReturn(null);

        expectedException.expect(NotFoundWebException.class);

        groupResource.removeUserFromGroup(GROUP_NAME, USER_NAME);
    }

    @Test
    public void testRemoveUserFromGroupAdminUserDoesntExist() throws Exception {
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, charlie)).thenReturn(true);

        when(crowdService.getUser(Matchers.eq(USER_NAME))).thenReturn(null);
        when(groupManager.getGroup(Matchers.eq(GROUP_NAME))).thenReturn(new MockGroup(GROUP_NAME));

        expectedException.expect(NotFoundWebException.class);
        groupResource.removeUserFromGroup(GROUP_NAME, USER_NAME);
    }

    @Test
    public void testRemoveUserFromGroupAdminValidationError() throws Exception {
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, charlie)).thenReturn(true);

        when(crowdService.getUser(Matchers.eq(USER_NAME))).thenReturn(new MockUser(USER_NAME));
        when(groupManager.getGroup(Matchers.eq(GROUP_NAME))).thenReturn(new MockGroup(GROUP_NAME));
        setRemoveUserValidationError(ErrorCollection.Reason.SERVER_ERROR);
        when(groupService.removeUsersFromGroups(Matchers.<JiraServiceContext>any(), Mockito.<GroupRemoveChildMapper>any())).thenReturn(true);

        Response response = groupResource.removeUserFromGroup(GROUP_NAME, USER_NAME);
        assertThat(response.getStatus(), equalTo(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode()));
    }

    @Test
    public void testRemoveUserFromGroupNotAdmin() throws Exception {
        when(crowdService.getUser(Matchers.eq(USER_NAME))).thenReturn(new MockUser(USER_NAME));
        when(groupManager.getGroup(Matchers.eq(GROUP_NAME))).thenReturn(new MockGroup(GROUP_NAME));
        when(groupService.validateRemoveUserFromGroups(Matchers.<JiraServiceContext>any(), Mockito.eq(ImmutableList.of(GROUP_NAME)), Mockito.eq(USER_NAME))).thenReturn(true);
        when(groupService.removeUsersFromGroups(Matchers.<JiraServiceContext>any(), Mockito.<GroupRemoveChildMapper>any())).thenReturn(true);

        expectedException.expect(ForbiddenWebException.class);
        groupResource.removeUserFromGroup(GROUP_NAME, USER_NAME);
    }

    /**
     * When retrieving a Group without expanding users
     * the users should never be retrieved
     *
     * @throws Exception
     */
    @Test
    public void testGetUsersInGroupNoExpand() throws Exception {
        permissionHelper.configureCurrentLoggedJiraUser("charlie", PermissionHelper.Permission.ADMIN);
        when(crowdService.getUser(Matchers.eq(USER_NAME))).thenReturn(new MockUser(USER_NAME));
        Group mockGroup = new MockGroup(GROUP_NAME);
        when(groupManager.getGroup(Matchers.eq(GROUP_NAME))).thenReturn(mockGroup);
        when(groupManager.getUsersInGroupCount(Matchers.eq(mockGroup))).thenReturn(1);
        final StringList noExpand = new StringList();
        final Response response = groupResource.getGroup(GROUP_NAME, noExpand);
        final GroupBean groupBean = (GroupBean) response.getEntity();
        expandGroupBean(groupBean, noExpand);

        // The users for this group should never have been retrieved, as we didn't expand them
        verify(groupManager, never()).getUsersInGroup(Matchers.<Group>any());
        // size should be initialized regardless
        assertThat(groupBean.getUsers().getSize(), equalTo(1));
    }

    /**
     * When retrieving a Group with expanding users
     * the users should be retrieved
     *
     * @throws Exception
     */
    @Test
    public void testGetUsersInGroupExpand() throws Exception {
        permissionHelper.configureCurrentLoggedJiraUser("charlie", PermissionHelper.Permission.ADMIN);
        when(crowdService.getUser(Matchers.eq(USER_NAME))).thenReturn(new MockUser(USER_NAME));
        Group mockGroup = new MockGroup(GROUP_NAME);
        when(groupManager.getGroup(Matchers.eq(GROUP_NAME))).thenReturn(mockGroup);
        when(groupManager.getUsersInGroupCount(Matchers.eq(mockGroup))).thenReturn(1);

        final StringList expand = new StringList("users[0:50]");
        final Response response = groupResource.getGroup(GROUP_NAME, expand);
        final GroupBean groupBean = (GroupBean) response.getEntity();
        expandGroupBean(groupBean, expand);

        // The users for this group should have been retrieved once
        verify(groupManager, times(1)).getUsersInGroup(Matchers.<Group>any());
        assertThat(groupBean.getUsers().getSize(), equalTo(1));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Expand the expandable members of the GroupBean, depending on the expand parameters in the request.
     * This is what the rest plugin does before serializing the response.
     *
     * @param groupBean        the group bean
     * @param expandParameters the expand parameters
     */
    private void expandGroupBean(final GroupBean groupBean, StringList expandParameters) {
        EntityCrawler entityCrawler = new EntityCrawler();
        ExpandParameter expandParameter = new DefaultExpandParameter(expandParameters.asList());
        EntityExpanderResolver listWrapperExpandResolver = new ListWrapperEntityExpanderResolver();
        entityCrawler.crawl(groupBean, expandParameter, listWrapperExpandResolver);
    }

    private AddGroupBean createAddGroupRequest(final String groupName) {
        final AddGroupBean addGroupBean = new AddGroupBean();
        addGroupBean.setName(groupName);
        return addGroupBean;
    }

    private UpdateUserToGroupBean createAddUserToGroupRequest(final String userName) {
        final UpdateUserToGroupBean updateUserToGroupBean = new UpdateUserToGroupBean();
        updateUserToGroupBean.setName(userName);
        return updateUserToGroupBean;
    }

    private void setAddUserValidationError(final ErrorCollection.Reason reason) {
        when(groupService.validateAddUserToGroup(Matchers.<JiraServiceContext>any(), Matchers.anyList(), Matchers.eq(USER_NAME)))
                .thenAnswer(new SingleErrorAnswer(reason));
    }

    private void setRemoveUserValidationError(final ErrorCollection.Reason reason) {
        when(groupService.validateRemoveUserFromGroups(Matchers.<JiraServiceContext>any(), Mockito.eq(ImmutableList.of(GROUP_NAME)), Mockito.eq(USER_NAME)))
                .thenAnswer(new SingleErrorAnswer(reason));
    }

    private void setDeleteGroupValidationError(final ErrorCollection.Reason reason) {
        when(groupService.validateDelete(Matchers.<JiraServiceContext>anyObject(), anyString(), anyString()))
                .thenAnswer(new SingleErrorAnswer(reason));
    }

    private static class SingleErrorAnswer implements Answer<Object> {
        private final ErrorCollection.Reason reason;

        public SingleErrorAnswer(final ErrorCollection.Reason reason) {
            this.reason = reason;
        }

        @Override
        public Object answer(final InvocationOnMock invocation) throws Throwable {
            ((JiraServiceContext) invocation.getArguments()[0]).getErrorCollection().addError("error", "error", reason);
            return false;
        }
    }
}
