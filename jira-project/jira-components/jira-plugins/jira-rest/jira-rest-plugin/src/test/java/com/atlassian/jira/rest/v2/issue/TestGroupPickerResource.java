package com.atlassian.jira.rest.v2.issue;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.bc.group.search.GroupPickerSearchService;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.rest.v2.admin.GroupLabelBean;
import com.atlassian.jira.rest.v2.issue.groups.GroupPickerResourceHelperImpl;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.jira.web.component.admin.group.GroupLabelsService;
import com.google.common.collect.Lists;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.atlassian.jira.rest.assertions.ResponseAssertions.assertResponseBody;
import static com.atlassian.jira.rest.assertions.ResponseAssertions.assertResponseCacheNever;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests for {@link GroupPickerResource}
 *
 * @since v4.4
 */
@SuppressWarnings("deprecation")
public class TestGroupPickerResource {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private GroupPickerSearchService groupPickerSearchService;

    @Mock
    private JiraAuthenticationContext authenticationContext;

    @Mock
    private ApplicationProperties applicationProperties;

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private GroupLabelsService groupLabelsService;

    @Mock
    private ApplicationRoleManager roleManager;

    @Mock
    private UserManager userManager;

    @InjectMocks
    private GroupPickerResourceHelperImpl groupPickerResourceHelper;

    private I18nHelper i18nHelper = new NoopI18nHelper();

    private List<Group> matchingGroups = Lists.newArrayList();

    private List<GroupLabelBean> labels = Lists.newArrayList();

    @Test
    public void testFindGroupsWithPartialMatch() throws Exception {
        matchingGroups.add(new MockGroup("lalo"));

        final MockApplicationUser mockUser = new MockApplicationUser("mocky");

        when(groupPickerSearchService.findGroups("la")).thenReturn(matchingGroups);
        when(authenticationContext.getUser()).thenReturn(mockUser);
        when(permissionManager.hasPermission(Permissions.USER_PICKER, mockUser)).thenReturn(true);
        when(authenticationContext.getI18nHelper()).thenReturn(i18nHelper);
        when(applicationProperties.getDefaultBackedString(APKeys.JIRA_AJAX_AUTOCOMPLETE_LIMIT)).thenReturn("20");

        GroupPickerResource resource = new GroupPickerResource(groupPickerResourceHelper);

        final List<GroupSuggestionBean> expectedGroups = Lists.newArrayList(new GroupSuggestionBean("lalo", "<b>la</b>lo", labels));

        final GroupSuggestionsBean expectedSuggestions = new GroupSuggestionsBean(1,
                NoopI18nHelper.makeTranslation(GroupPickerResourceHelperImpl.MORE_GROUP_RESULTS_I18N_KEY, "1", "1"), expectedGroups);

        Response response = resource.findGroups("la", null, null, null);
        assertResponseCacheNever(response);
        assertResponseBody(expectedSuggestions, response);
    }

    @Test
    public void testFindGroupsWithNoMatch() throws Exception {
        final MockApplicationUser mockUser = new MockApplicationUser("mocky");

        when(authenticationContext.getUser()).thenReturn(mockUser);
        when(permissionManager.hasPermission(Permissions.USER_PICKER, mockUser)).thenReturn(true);
        when(groupPickerSearchService.findGroups("la")).thenReturn(matchingGroups);
        when(authenticationContext.getI18nHelper()).thenReturn(i18nHelper);
        when(applicationProperties.getDefaultBackedString(APKeys.JIRA_AJAX_AUTOCOMPLETE_LIMIT)).thenReturn("20");

        GroupPickerResource resource = new GroupPickerResource(groupPickerResourceHelper);

        final List<GroupSuggestionBean> expectedGroups =
                Lists.newArrayList();

        final GroupSuggestionsBean expectedSuggestions = new GroupSuggestionsBean(0,
                NoopI18nHelper.makeTranslation(GroupPickerResourceHelperImpl.MORE_GROUP_RESULTS_I18N_KEY, "0", "0"), expectedGroups);

        Response response = resource.findGroups("la", null, null, null);
        assertResponseCacheNever(response);
        assertResponseBody(expectedSuggestions, response);
    }

    @Test
    public void testFindGroupsWithExcessMatch() throws Exception {
        matchingGroups.add(new MockGroup("lalo"));
        matchingGroups.add(new MockGroup("lolo"));

        final MockApplicationUser mockUser = new MockApplicationUser("mocky");

        when(authenticationContext.getUser()).thenReturn(mockUser);
        when(permissionManager.hasPermission(Permissions.USER_PICKER, mockUser)).thenReturn(true);
        when(groupPickerSearchService.findGroups("lo")).thenReturn(matchingGroups);
        when(authenticationContext.getI18nHelper()).thenReturn(i18nHelper);
        when(applicationProperties.getDefaultBackedString(APKeys.JIRA_AJAX_AUTOCOMPLETE_LIMIT)).thenReturn("1");

        GroupPickerResource resource = new GroupPickerResource(groupPickerResourceHelper);

        final List<GroupSuggestionBean> expectedGroups = Lists.newArrayList(new GroupSuggestionBean("lalo", "la<b>lo</b>", labels));

        final GroupSuggestionsBean expectedSuggestions = new GroupSuggestionsBean(2,
                NoopI18nHelper.makeTranslation(GroupPickerResourceHelperImpl.MORE_GROUP_RESULTS_I18N_KEY, "1", "2"), expectedGroups);

        Response response = resource.findGroups("lo", null, null, null);
        assertResponseCacheNever(response);
        assertResponseBody(expectedSuggestions, response);
    }

    @Test
    public void testFindGroupsWithDefaultLimit() throws Exception {
        for (int i = 0; i < GroupPickerResourceHelperImpl.DEFAULT_MAX_RESULTS + 1; ++i) {
            matchingGroups.add(new MockGroup("a" + String.valueOf(i)));
        }

        final MockApplicationUser mockUser = new MockApplicationUser("mocky");

        when(authenticationContext.getUser()).thenReturn(mockUser);
        when(permissionManager.hasPermission(Permissions.USER_PICKER, mockUser)).thenReturn(true);
        when(groupPickerSearchService.findGroups("a")).thenReturn(matchingGroups);
        when(authenticationContext.getI18nHelper()).thenReturn(i18nHelper);
        when(applicationProperties.getDefaultBackedString(APKeys.JIRA_AJAX_AUTOCOMPLETE_LIMIT)).thenReturn(null);

        GroupPickerResource resource = new GroupPickerResource(groupPickerResourceHelper);

        final List<GroupSuggestionBean> expectedGroups = Lists.newArrayList();
        for (int i = 0; i < GroupPickerResourceHelperImpl.DEFAULT_MAX_RESULTS; ++i) {
            expectedGroups.add(new GroupSuggestionBean("a" + String.valueOf(i), "<b>a</b>" + String.valueOf(i), labels));
        }

        final GroupSuggestionsBean expectedSuggestions = new GroupSuggestionsBean(GroupPickerResourceHelperImpl.DEFAULT_MAX_RESULTS + 1,
                NoopI18nHelper.makeTranslation(GroupPickerResourceHelperImpl.MORE_GROUP_RESULTS_I18N_KEY,
                        String.valueOf(GroupPickerResourceHelperImpl.DEFAULT_MAX_RESULTS),
                        String.valueOf(GroupPickerResourceHelperImpl.DEFAULT_MAX_RESULTS + 1)), expectedGroups);

        Response response = resource.findGroups("a", null, null, null);
        assertResponseCacheNever(response);
        assertResponseBody(expectedSuggestions, response);
    }

    @Test
    public void testExcludedGroups() {
        for (int i = 1; i < 4; ++i) {
            matchingGroups.add(new MockGroup("a" + String.valueOf(i)));
        }

        final MockApplicationUser mockUser = new MockApplicationUser("mocky");

        when(authenticationContext.getUser()).thenReturn(mockUser);
        when(permissionManager.hasPermission(Permissions.USER_PICKER, mockUser)).thenReturn(true);
        when(groupPickerSearchService.findGroups("a")).thenReturn(matchingGroups);
        when(authenticationContext.getI18nHelper()).thenReturn(i18nHelper);
        when(applicationProperties.getDefaultBackedString(APKeys.JIRA_AJAX_AUTOCOMPLETE_LIMIT)).thenReturn("20");

        GroupPickerResource resource = new GroupPickerResource(groupPickerResourceHelper);

        final List<GroupSuggestionBean> expectedGroups = Lists.newArrayList();
        expectedGroups.add(new GroupSuggestionBean("a3", "<b>a</b>3", labels));

        final GroupSuggestionsBean expectedSuggestions = new GroupSuggestionsBean(1,
                NoopI18nHelper.makeTranslation(GroupPickerResourceHelperImpl.MORE_GROUP_RESULTS_I18N_KEY, 1, 1), expectedGroups);

        List<String> excluded = new ArrayList<>();
        excluded.add("a1");
        excluded.add("a2");

        Response response = resource.findGroups("a", excluded, null, null);
        assertResponseBody(expectedSuggestions, response);
    }

    @Test
    public void testUserWithNoBrowseUserPermissionsReturnsOnlyExactMatchedGroup() {
        String mockGroupName = "mockGroup";
        final MockApplicationUser mockUser = new MockApplicationUser("mocky");
        final MockGroup mockGroup = new MockGroup(mockGroupName);

        when(authenticationContext.getUser()).thenReturn(mockUser);
        when(permissionManager.hasPermission(Permissions.USER_PICKER, mockUser)).thenReturn(false);
        when(groupPickerSearchService.getGroupByName(mockGroupName)).thenReturn(mockGroup);
        when(authenticationContext.getI18nHelper()).thenReturn(i18nHelper);
        when(applicationProperties.getDefaultBackedString(APKeys.JIRA_AJAX_AUTOCOMPLETE_LIMIT)).thenReturn("20");

        GroupPickerResource resource = new GroupPickerResource(groupPickerResourceHelper);

        final List<GroupSuggestionBean> expectedGroups = Lists.newArrayList();
        expectedGroups.add(new GroupSuggestionBean(mockGroupName, "<b>" + mockGroupName + "</b>", labels));

        final GroupSuggestionsBean expectedSuggestions = new GroupSuggestionsBean(1,
                NoopI18nHelper.makeTranslation(GroupPickerResourceHelperImpl.MORE_GROUP_RESULTS_I18N_KEY, 1, 1), expectedGroups);

        Response response = resource.findGroups(mockGroupName, null, null, null);
        assertResponseBody(expectedSuggestions, response);
    }

    @Test
    public void testUserWithNoBrowseUserPermissionsDoesntReturnNonExactMatchedGroup() {
        String mockGroupName = "mockGroup";
        final MockApplicationUser mockUser = new MockApplicationUser("mocky");
        final MockGroup mockGroup = new MockGroup(mockGroupName);

        when(authenticationContext.getUser()).thenReturn(mockUser);
        when(permissionManager.hasPermission(Permissions.USER_PICKER, mockUser)).thenReturn(false);
        when(groupPickerSearchService.getGroupByName(mockGroupName)).thenReturn(mockGroup);
        when(authenticationContext.getI18nHelper()).thenReturn(i18nHelper);
        when(applicationProperties.getDefaultBackedString(APKeys.JIRA_AJAX_AUTOCOMPLETE_LIMIT)).thenReturn("20");

        GroupPickerResource resource = new GroupPickerResource(groupPickerResourceHelper);

        final List<GroupSuggestionBean> expectedGroups = Lists.newArrayList();

        final GroupSuggestionsBean expectedSuggestions = new GroupSuggestionsBean(0,
                NoopI18nHelper.makeTranslation(GroupPickerResourceHelperImpl.MORE_GROUP_RESULTS_I18N_KEY, 0, 0), expectedGroups);

        Response response = resource.findGroups("mockGr", null, null, null);
        assertResponseBody(expectedSuggestions, response);
    }

    @Test
    public void shouldBeUserAwareForGroupLabels() {
        final MockGroup mockGroup = new MockGroup("lalo");
        final MockApplicationUser mockUser = new MockApplicationUser("mocky");
        final MockApplicationUser mockQueryUser = new MockApplicationUser("qocky", 1000L);

        matchingGroups.add(mockGroup);

        when(groupPickerSearchService.findGroups("la")).thenReturn(matchingGroups);
        when(authenticationContext.getUser()).thenReturn(mockUser);
        when(authenticationContext.getLoggedInUser()).thenReturn(mockUser);
        when(permissionManager.hasPermission(Permissions.USER_PICKER, mockUser)).thenReturn(true);
        when(permissionManager.hasPermission(Permissions.ADMINISTER, mockUser)).thenReturn(true);
        when(authenticationContext.getI18nHelper()).thenReturn(i18nHelper);
        when(applicationProperties.getDefaultBackedString(APKeys.JIRA_AJAX_AUTOCOMPLETE_LIMIT)).thenReturn("20");
        when(userManager.getUserByKeyEvenWhenUnknown("qocky")).thenReturn(mockQueryUser);

        GroupPickerResource resource = new GroupPickerResource(groupPickerResourceHelper);

        final List<GroupSuggestionBean> expectedGroups = Lists.newArrayList(new GroupSuggestionBean("lalo", "<b>la</b>lo", labels));

        final GroupSuggestionsBean expectedSuggestions = new GroupSuggestionsBean(1,
                NoopI18nHelper.makeTranslation(GroupPickerResourceHelperImpl.MORE_GROUP_RESULTS_I18N_KEY, "1", "1"), expectedGroups);

        Response response = resource.findGroups("la", null, null, "qocky");
        assertResponseCacheNever(response);
        assertResponseBody(expectedSuggestions, response);

        verify(groupLabelsService, times(1)).getGroupLabels(mockGroup, Optional.<Long>of(1000L));
    }

    public static class MockGroup implements Group {

        private String name;

        public MockGroup(final String name) {
            this.name = name;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public int compareTo(Group o) {
            return name.compareTo(o.getName());
        }
    }
}
