package com.atlassian.jira.rest.v2.search;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.event.mau.MauEventService;
import com.atlassian.jira.rest.v2.issue.builder.BeanBuilderFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.action.issue.IssueSearchLimits;
import com.atlassian.jira.web.bean.PagerFilter;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class SearchResourceTest {
    @Mock
    SearchService searchService;

    @Mock
    JiraAuthenticationContext jiraAuthenticationContext;

    @Mock
    IssueSearchLimits issueSearchLimits;

    @Mock
    BeanBuilderFactory beanBuildFactory;

    @Mock
    MauEventService mauEventService;

    @Test
    public void testCreateFilterShouldNotCreateUnlimitedFilterWhenMaxResultsIsMinusOne() throws Exception {
        when(issueSearchLimits.getMaxResults()).thenReturn(200);

        SearchResource search = new SearchResource(searchService, jiraAuthenticationContext,
                issueSearchLimits, beanBuildFactory, mauEventService);
        PagerFilter filter = search.createFilter(0, -1);

        assertThat(filter.getMax(), equalTo(200));
    }

    @Test
    public void pagerFilterMaxShouldBeEqualToMaxResultsWhenMaxResultsIsSetAndSearchMaxLimitIsMinusOne() throws Exception {
        when(issueSearchLimits.getMaxResults()).thenReturn(-1);

        SearchResource search = new SearchResource(searchService, jiraAuthenticationContext,
                issueSearchLimits, beanBuildFactory, mauEventService);
        PagerFilter filter = search.createFilter(0, 2);

        assertThat(filter.getMax(), equalTo(2));
    }

    @Test
    public void pagerFilterMaxShouldBeEqualToDefaultIssueReturnedWhenMaxResultsIsNotSet() throws Exception {
        when(issueSearchLimits.getMaxResults()).thenReturn(300);

        SearchResource search = new SearchResource(searchService, jiraAuthenticationContext,
                issueSearchLimits, beanBuildFactory, mauEventService);
        PagerFilter filter = search.createFilter(0, null);

        assertThat(filter.getMax(), equalTo(SearchResource.DEFAULT_ISSUES_RETURNED));
    }

    @Test
    public void pagerFilterMaxShouldEqualToMinOfMaxResultsAndSearchLimitsWhenBothAreSet() throws Exception {
        when(issueSearchLimits.getMaxResults()).thenReturn(100);

        SearchResource search = new SearchResource(searchService, jiraAuthenticationContext,
                issueSearchLimits, beanBuildFactory, mauEventService);
        PagerFilter filter = search.createFilter(0, 101);

        assertThat(filter.getMax(), equalTo(100));
    }

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }
}
