package com.atlassian.jira.rest.v2.issue.version;

import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.rest.v2.issue.VersionResource;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import java.net.URI;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

/**
 * @since v7.0.10
 */
public class TestVersionIssueCountsBeanFactory {
    @Rule
    public final MethodRule mockito = MockitoJUnit.rule();
    @Mock
    private Version version;
    @Mock
    private UriInfo uriInfo;
    @Mock
    private UriBuilder builder;

    @Test
    public void shouldCreateVersionIssueCountsBean() throws Exception {
        final URI uri = new URI("http://localhost:8090/jira");
        final VersionIssueCountsBean expectedCountsBean = new VersionIssueCountsBean(1L, 2L, uri, 3L, null);
        final VersionIssueCountsBeanFactory factory = new VersionIssueCountsBeanFactoryImpl(uriInfo);
        when(version.getId()).thenReturn(12L);
        when(uriInfo.getBaseUriBuilder()).thenReturn(builder);
        when(builder.path(VersionResource.class)).thenReturn(builder);
        when(builder.path(version.getId().toString())).thenReturn(builder);
        when(builder.build()).thenReturn(uri);

        final VersionIssueCountsBean actualCountsBean = factory.createVersionBean(version, 1L, 2L, 3L, null);

        assertThat(actualCountsBean, is(equalTo(expectedCountsBean)));
    }
}
