package com.atlassian.jira.rest.util;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.application.MockApplicationRole;
import com.atlassian.jira.bc.user.UserService;
import com.atlassian.jira.bc.user.UserService.AddUserToApplicationValidationResult;
import com.atlassian.jira.bc.user.UserValidationResultBuilder;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.rest.api.util.ErrorCollection;
import com.atlassian.jira.rest.exception.ForbiddenWebException;
import com.atlassian.jira.rest.matchers.ErrorCollectionMatcher;
import com.atlassian.jira.rest.util.UpdateUserApplicationHelper.ApplicationUpdateResult;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import javax.annotation.Nullable;

import static com.atlassian.labs.mockito.MockitoBooster.whenOption;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v7.0
 */
public class UpdateUserApplicationHelperTest {
    @Rule
    public RuleChain chain = MockitoMocksInContainer.forTest(this);
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private UpdateUserApplicationHelper updateUserApplicationHelper;

    @Mock
    private UserService userServiceMock;

    @Mock
    private UserManager userManagerMock;

    @Mock
    private ApplicationRoleManager applicationRoleManager;

    @Mock
    private GlobalPermissionManager globalPermissionManager;

    @Mock
    private JiraAuthenticationContext authContext;

    @Mock
    private FeatureManager featureManager;

    private UserValidationResultBuilder validationResultBuilder;

    private final String ERROR_TEXT = "erorr.text";

    private MockApplicationUser fredUser;

    private ApplicationRole applicationRole;

    @Before
    public void setUp() {
        updateUserApplicationHelper = new UpdateUserApplicationHelper(
                userServiceMock,
                userManagerMock,
                new MockI18nHelper(),
                applicationRoleManager,
                globalPermissionManager,
                authContext);

        validationResultBuilder = new UserValidationResultBuilder();

        configureCurrentLoggedJiraUser("charlie", GlobalPermissionKey.ADMINISTER);
        fredUser = configureJiraUser("fred", null);
        applicationRole = prepareMockApplicationRole("app-one");
    }

    @Test
    public void shouldNotAddUserToApplicationWhenLoggedUserIsNotAnAdmin() {
        configureCurrentLoggedJiraUser("charlie", null);

        expectedException.expect(ForbiddenWebException.class);
        updateUserApplicationHelper.addUserToApplication("fred", "key");
    }

    @Test
    public void shouldThrowExceptionWhileAddingUserToApplicationWhenUserDoesNotExist() {
        final ApplicationUpdateResult result = updateUserApplicationHelper.addUserToApplication("nobody", "key");

        assertThat(result.getErrorCollection().getReasons(), contains(com.atlassian.jira.util.ErrorCollection.Reason.NOT_FOUND));
    }

    @Test
    public void shouldNotAddUserToApplicationWhenKeyIsInvalid() throws Exception {
        final ApplicationUpdateResult result = updateUserApplicationHelper.addUserToApplication("fred", "invaLi#d ke1");

        assertApplicationUpdateResult(result, false, "rest.user.error.not.valid.application.key [invaLi#d ke1]");
        verify(userServiceMock, never()).addUserToApplication(any(UserService.AddUserToApplicationValidationResult.class));
    }

    @Test
    public void shouldNotAddUserToApplicationWhenValidationFail() throws Exception {
        validationResultBuilder.addErrorMessage(ERROR_TEXT);
        final AddUserToApplicationValidationResult validationResult = validationResultBuilder.buildUserAddToApplicationError();
        when(userServiceMock.validateAddUserToApplication(fredUser, ApplicationKey.valueOf("key"))).thenReturn(validationResult);

        final ApplicationUpdateResult result = updateUserApplicationHelper.addUserToApplication("fred", "key");

        assertApplicationUpdateResult(result, false, ERROR_TEXT);
        verify(userServiceMock, never()).addUserToApplication(any(UserService.AddUserToApplicationValidationResult.class));
    }

    @Test
    public void shouldAddUserToApplicationWhenValidationSuccess() throws Exception {
        final AddUserToApplicationValidationResult validationResult = validationResultBuilder.buildUserAddToApplication(fredUser, applicationRole);
        when(userServiceMock.validateAddUserToApplication(fredUser, ApplicationKey.valueOf("app-one"))).thenReturn(validationResult);

        final ApplicationUpdateResult result = updateUserApplicationHelper.addUserToApplication("fred", "app-one");

        assertApplicationUpdateResult(result, true);
        verify(userServiceMock).addUserToApplication(validationResult);
    }

    @Test
    public void shouldNotRemoveUserFromApplicationWhenLoggedUserIsNotAnAdmin() {
        configureCurrentLoggedJiraUser("charlie", null);

        expectedException.expect(ForbiddenWebException.class);
        updateUserApplicationHelper.removeUserFromApplication("fred", "key");
    }

    @Test
    public void shouldThrowExceptionWhileRemovingUserFromApplicationWhenUserDoesNotExist() {
        final ApplicationUpdateResult result = updateUserApplicationHelper.removeUserFromApplication("nobody", "key");

        assertThat(result.getErrorCollection().getReasons(), contains(com.atlassian.jira.util.ErrorCollection.Reason.NOT_FOUND));
    }

    @Test
    public void shouldNotRemoveUserToApplicationWhenKeyIsInvalid() throws Exception {
        final ApplicationUpdateResult result = updateUserApplicationHelper.removeUserFromApplication("fred", "invaLi#d ke1");

        assertApplicationUpdateResult(result, false, "rest.user.error.not.valid.application.key [invaLi#d ke1]");
        verify(userServiceMock, never()).removeUserFromApplication(any(UserService.RemoveUserFromApplicationValidationResult.class));
    }

    @Test
    public void shouldNotRemoveUserFromApplicationWhenValidationFail() throws Exception {
        validationResultBuilder.addErrorMessage(ERROR_TEXT);
        final UserService.RemoveUserFromApplicationValidationResult validationResult = validationResultBuilder.buildRemoveUserFromApplicationError();

        MockApplicationUser adminUser = configureJiraUser("admin", GlobalPermissionKey.ADMINISTER);
        when(authContext.getLoggedInUser()).thenReturn(adminUser);
        when(userServiceMock.validateRemoveUserFromApplication(adminUser, fredUser, ApplicationKey.valueOf("key"))).thenReturn(validationResult);

        final ApplicationUpdateResult result = updateUserApplicationHelper.removeUserFromApplication("fred", "key");
        assertApplicationUpdateResult(result, false, ERROR_TEXT);
        verify(userServiceMock, never()).removeUserFromApplication(any(UserService.RemoveUserFromApplicationValidationResult.class));
    }

    @Test
    public void shouldRemoveUserFromApplicationWhenValidationSuccess() throws Exception {
        final UserService.RemoveUserFromApplicationValidationResult validationResult = validationResultBuilder.buildRemoveUserFromApplication(fredUser, applicationRole);

        MockApplicationUser adminUser = configureJiraUser("admin", GlobalPermissionKey.ADMINISTER);
        when(authContext.getLoggedInUser()).thenReturn(adminUser);
        when(userServiceMock.validateRemoveUserFromApplication(adminUser, fredUser, ApplicationKey.valueOf("app-one"))).thenReturn(validationResult);

        final ApplicationUpdateResult result = updateUserApplicationHelper.removeUserFromApplication("fred", "app-one");

        assertApplicationUpdateResult(result, true);
        verify(userServiceMock).removeUserFromApplication(validationResult);
    }

    private void assertApplicationUpdateResult(final ApplicationUpdateResult result, boolean isValid, String... errors) {
        assertEquals(isValid, result.isValid());
        if (!isValid) {
            final ErrorCollectionMatcher errorCollectionMatcher = new ErrorCollectionMatcher();
            errorCollectionMatcher.addErrorMessages(ImmutableList.copyOf(errors));
            assertThat(ErrorCollection.of(result.getErrorCollection()), errorCollectionMatcher);
        }
    }

    private MockApplicationRole prepareMockApplicationRole(String key) {
        final MockApplicationRole role = new MockApplicationRole(ApplicationKey.valueOf(key));
        whenOption(applicationRoleManager.getRole(role.getKey())).thenSome(role);
        return role;
    }

    public MockApplicationUser configureCurrentLoggedJiraUser(final String username, @Nullable final GlobalPermissionKey permission) {
        final MockApplicationUser loggedUser = configureJiraUser(username, permission);
        when(authContext.getLoggedInUser()).thenReturn(loggedUser);

        return loggedUser;
    }

    private MockApplicationUser configureJiraUser(final String username, @Nullable final GlobalPermissionKey permission) {
        final String key = "key-" + username;
        final MockApplicationUser user = new MockApplicationUser(key, username);

        final boolean isAdmin = GlobalPermissionKey.ADMINISTER.equals(permission) || GlobalPermissionKey.SYSTEM_ADMIN.equals(permission);
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user)).thenReturn(isAdmin);

        final boolean isSysadmin = GlobalPermissionKey.SYSTEM_ADMIN.equals(permission);
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.SYSTEM_ADMIN, user)).thenReturn(isSysadmin);

        when(userManagerMock.getUserByKey(key)).thenReturn(user);
        when(userManagerMock.getUserByName(username)).thenReturn(user);

        return user;
    }
}
