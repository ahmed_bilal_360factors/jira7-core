package com.atlassian.jira.rest.v2.issue;

import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.MockOrderableField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.fields.screen.FieldScreenRenderTab;
import com.atlassian.jira.issue.fields.screen.FieldScreenRenderer;
import com.atlassian.jira.issue.fields.screen.FieldScreenRendererFactory;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.operation.IssueOperations;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.mock.security.MockAuthenticationContext;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.rest.api.util.StringList;
import com.atlassian.jira.rest.v2.issue.context.ContextUriInfo;
import com.atlassian.jira.rest.v2.issue.version.VersionBeanFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;
import java.util.Map;

import static com.atlassian.jira.issue.IssueFieldConstants.COMMENT;
import static com.atlassian.jira.matchers.ReflectionMatchers.propertiesMatcher;
import static com.atlassian.jira.rest.v2.issue.CreateMetaFieldBeanBuilderTest.getFieldsTab;
import static com.atlassian.jira.rest.v2.issue.IncludedFields.nothingIncludedByDefault;
import static com.google.common.collect.ImmutableList.of;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * Most of the logic of basic class is tested in {@link CreateMetaFieldBeanBuilderTest} already so this
 * class tests its specifics only.
 */
public class EditMetaFieldBeanBuilderTest {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();
    @Mock
    private FieldScreenRendererFactory fieldScreenRendererFactory;
    @Mock
    private FieldLayoutManager fieldLayoutManager;
    @Mock
    private Issue issue;
    @Mock
    private IssueType issueType;
    @Mock
    private Project project;
    @Mock
    private ApplicationUser user;
    @Mock
    private VersionBeanFactory versionBeanFactory;
    @Mock
    private VelocityRequestContextFactory velocityRequestContextFactory;
    @Mock
    private ContextUriInfo contextUriInfo;
    @Mock
    private JiraBaseUrls baseUrls;
    @Mock
    private FieldScreenRenderer fieldScreenRenderer;
    @Mock
    private PermissionManager permissionManager;

    private JiraAuthenticationContext authContext = new MockAuthenticationContext(user, new MockI18nHelper());
    @Mock
    private FieldManager fieldManager;
    @Mock
    private DefaultFieldMetaBeanHelper defaultFieldHelper;
    @Mock
    private IssueManager issueManager;
    @Mock
    @AvailableInContainer
    private AvatarService avatarService;
    private EditMetaFieldBeanBuilder metaFieldBeanBuilder;


    @Before
    public void before() {
        metaFieldBeanBuilder = new EditMetaFieldBeanBuilder(fieldLayoutManager, project, issue, issueType,
                user, versionBeanFactory, velocityRequestContextFactory, contextUriInfo, baseUrls,
                fieldScreenRendererFactory, fieldManager, issueManager);

        when(fieldScreenRendererFactory.getFieldScreenRenderer(issue, IssueOperations.EDIT_ISSUE_OPERATION))
                .thenReturn(fieldScreenRenderer);
        when(fieldManager.getField(COMMENT)).thenReturn(new MockOrderableField(COMMENT,"comment.type.name"));
    }

    @Test
    public void shouldBuildEmptyAndAddComment() throws Exception {
        when(issueManager.isEditable(issue, user)).thenReturn(true);
        when(fieldScreenRenderer.getFieldScreenRenderTabs()).thenReturn(Collections.emptyList());

        final Map<String, FieldMetaBean> fields = metaFieldBeanBuilder.build();

        assertThat("Issue type and project are always required", fields.keySet(), containsInAnyOrder(COMMENT));

        assertThat("Fields should be required", fields.values(),
                hasItem(notRequiredWithName("comment.type.name")));
    }

    @Test
    public void shouldBuildEmptyAndNotAndCommentWhenNotIncluded() throws Exception {
        when(issueManager.isEditable(issue, user)).thenReturn(true);
        when(fieldScreenRenderer.getFieldScreenRenderTabs()).thenReturn(Collections.emptyList());
        metaFieldBeanBuilder.fieldsToInclude(nothingIncludedByDefault(of(StringList.fromList("field1"))));
        final Map<String, FieldMetaBean> fields = metaFieldBeanBuilder.build();

        assertThat("Issue type and project are always required", fields.keySet(), not(containsInAnyOrder(COMMENT)));

        assertThat("Fields should be required", fields.values(),
                not(hasItem(notRequiredWithName("comment.type.name"))));
    }

    @Test
    public void shouldHandleNotRequiredFields() throws Exception {
        when(issueManager.isEditable(issue, user)).thenReturn(true);

        final ImmutableList<FieldScreenRenderTab> fieldScreenRenderTabs = of(getFieldsTab(IssueFieldConstants.ASSIGNEE, false, true));
        when(fieldScreenRenderer.getFieldScreenRenderTabs()).thenReturn(fieldScreenRenderTabs);
        final Map<String, FieldMetaBean> fields = metaFieldBeanBuilder.build();

        assertThat("There should be assignee", fields.keySet(), hasItem(IssueFieldConstants.ASSIGNEE));
        assertThat("Field should not be required", fields.values(), hasItem(notRequiredWithName("assignee_name")));
    }


    private Matcher<? super FieldMetaBean> requiredWithName(String name) {

        return propertiesMatcher(ImmutableMap.of("required", equalTo(true), "name", equalTo(name)));
    }
    private Matcher<? super FieldMetaBean> notRequiredWithName(String name) {

        return propertiesMatcher(ImmutableMap.of("required", equalTo(false), "name", equalTo(name)));
    }
}