package com.atlassian.jira.rest.v2;

import com.atlassian.rest.annotation.ResponseType;
import com.atlassian.rest.annotation.ResponseTypes;
import org.junit.Test;
import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Stream;

import static org.junit.Assert.fail;

public class MetaInformationCoverageTest {

    @Test
    public void allResourceMethodsHaveResponseTypeAnnotations() throws IOException {

        Reflections reflections = analyzeClasspath();

        Set<String> methodsWithoutResponseType = Stream.of(GET.class, PUT.class, POST.class)
                .map(reflections::getMethodsAnnotatedWith)
                .flatMap(Set::stream)
                .filter(this::requiresResponseType)
                .filter(method -> !hasResponseTypeAnnotation(method))
                .map(this::toName)
                .collect(TreeSet::new, Set::add, Set::addAll);

        if (!methodsWithoutResponseType.isEmpty()) {
            failWith(methodsWithoutResponseType);
        }
    }

    private void failWith(Set<String> methodsWithoutResponseType) {
        StringBuilder error = new StringBuilder();
        error.append("\nMethods without response type declared. Annotate them with @ResponseType annotation.\n");
        error.append("If the method does not return anything, annotate with @ResponseType(Void.class)\n\n");
        error.append("Offending methods:\n\n");

        methodsWithoutResponseType.stream().map(name -> "* " + name + "\n").forEach(error::append);

        error.append("\n");

        fail(error.toString());
    }

    private boolean requiresResponseType(Method method) {
        return Response.class.isAssignableFrom(method.getReturnType());
    }

    private boolean hasResponseTypeAnnotation(Method method) {
        return method.getAnnotation(ResponseType.class) != null || method.getAnnotation(ResponseTypes.class) != null;
    }

    private String toName(Method method) {
        return method.getDeclaringClass().getSimpleName() + "." + method.getName();
    }

    private Reflections analyzeClasspath() {
        return new Reflections(new ConfigurationBuilder()
                .setScanners(new MethodAnnotationsScanner())
                .setUrls(ClasspathHelper.forPackage("com.atlassian.jira.rest.v2"))
                .filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix("com.atlassian.jira.rest.v2"))));
    }

}
