package com.atlassian.jira.rest.v2.issue;

import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.rest.v2.issue.project.ProjectBeanFactory;
import com.atlassian.jira.rest.v2.issue.project.ProjectBeanFactoryImpl;
import com.atlassian.jira.rest.v2.issue.project.ProjectIdentity;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import javax.ws.rs.core.UriInfo;
import java.net.URI;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

public class TestProjectBeanFactory {
    @Rule
    public final RuleChain mockitoMocks = MockitoMocksInContainer.forTest(this);

    @Mock
    UriInfo uriInfo;
    @Mock
    ResourceUriBuilder builder;

    private static final Project PROJECT_WITH_IDENTITY = new MockProject(42L, "key");
    private static final Project PROJECT_WITHOUT_ID = new MockProject(null, "key", "but I have a name!");
    private static final Project PROJECT_WITHOUT_KEY = new MockProject(42L);

    private ProjectBeanFactory factory;

    private static URI uriFor(String resourceKey) {
        return URI.create("http://test/jira/rest/api/" + resourceKey);
    }

    @Before
    public void setUp() throws Exception {
        factory = new ProjectBeanFactoryImpl(null, uriInfo, builder, null, null, null, null, null, null);
        when(builder.build(eq(uriInfo), eq(ProjectResource.class), anyString())).then(new Answer<URI>() {
            @Override
            public URI answer(final InvocationOnMock invocationOnMock) throws Throwable {
                return uriFor((String) invocationOnMock.getArguments()[2]);
            }
        });
    }

    @Test
    public void testProjectIdentityIsCreatedUsingTheSuppliedUriBuilderAndInfoWhenProjectHasIdAndKey() {
        ProjectIdentity identity = factory.projectIdentity(PROJECT_WITH_IDENTITY);
        assertThat(identity, equalTo(new ProjectIdentity(uriFor(PROJECT_WITH_IDENTITY.getId().toString()), PROJECT_WITH_IDENTITY.getId(), PROJECT_WITH_IDENTITY.getKey())));
    }

    @Test(expected = NullPointerException.class)
    public void testNpeExceptionIsThrownWhenTryingToCreateIdentityOfProjectWithoutKey() {
        factory.projectIdentity(PROJECT_WITHOUT_KEY);
    }

    @Test(expected = NullPointerException.class)
    public void testNpeExceptionIsThrownWhenTryingToCreateIdentityOfProjectWithoutId() {
        factory.projectIdentity(PROJECT_WITHOUT_ID);
    }
}
