package com.atlassian.jira.rest.v2.issue;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.exception.runtime.OperationFailedException;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.application.MockApplicationRole;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.bc.security.login.DeniedReason;
import com.atlassian.jira.bc.security.login.LoginReason;
import com.atlassian.jira.bc.security.login.LoginResult;
import com.atlassian.jira.bc.security.login.LoginResultImpl;
import com.atlassian.jira.bc.security.login.LoginService;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.plugin.user.PasswordPolicyManager;
import com.atlassian.jira.plugin.user.WebErrorMessage;
import com.atlassian.jira.plugin.user.WebErrorMessageImpl;
import com.atlassian.jira.rest.exception.BadRequestWebException;
import com.atlassian.jira.rest.exception.ForbiddenWebException;
import com.atlassian.jira.rest.exception.NotAuthorisedWebException;
import com.atlassian.jira.rest.exception.ServerErrorWebException;
import com.atlassian.jira.rest.testutils.UserMatchers;
import com.atlassian.jira.rest.v2.admin.applicationrole.ApplicationRoleBean;
import com.atlassian.jira.rest.v2.admin.applicationrole.ApplicationRoleBeanConverter;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.EmailFormatter;
import com.atlassian.jira.util.I18nHelper;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.RuleChain;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.util.Collection;
import java.util.Locale;
import java.util.TimeZone;

import static org.junit.Assert.assertEquals;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v6.1
 */
public class CurrentUserResourceTest {
    @Rule
    public RuleChain chain = MockitoMocksInContainer.forTest(this);
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private static final String CURRENT_PASSWORD = "password";
    private static final String NEW_PASSWORD = "12443";

    private CurrentUserResource currentUserResource;

    @Mock
    private JiraAuthenticationContext jiraAuthenticationContextMock;

    @Mock
    private PermissionManager permissionManagerMock;

    @Mock
    private UserUtil userUtilMock;

    @Mock
    private UserWriteBean userBean;

    @Mock
    private JiraBaseUrls jiraBaseUrls;

    @Mock
    private TimeZoneManager timeZoneManager;

    @Mock
    private UserManager userManager;

    @Mock
    private EventPublisher eventPublisher;

    @Mock
    private I18nHelper.BeanFactory beanFactory;

    @Mock
    private PasswordPolicyManager passwordPolicyManagerMock;

    @Mock
    private LoginService loginService;

    @Mock
    @AvailableInContainer
    private AvatarService avatarService;

    @Mock
    private ApplicationRoleManager applicationRoleManager;

    @Mock
    private ApplicationRoleBeanConverter applicationRoleBeanConverter;

    private LoginResultImpl correctPasswordResult;
    private LoginResultImpl incorrectPasswordResult;

    private MockApplicationUser currentUser;


    @Before
    public void setUp() throws Exception {
        when(jiraBaseUrls.restApi2BaseUrl()).thenReturn(UriBuilder.fromUri("http://localhost").toString());

        currentUserResource = new CurrentUserResource(
                userUtilMock,
                userManager,
                passwordPolicyManagerMock,
                eventPublisher,
                mock(I18nHelper.class),
                mock(EmailFormatter.class),
                jiraAuthenticationContextMock,
                timeZoneManager,
                mock(AvatarService.class),
                jiraBaseUrls,
                loginService,
                beanFactory,
                applicationRoleBeanConverter,
                applicationRoleManager
        );

        currentUser = configureCurrentLoggedJiraUser("charlie", CURRENT_PASSWORD);
        when(applicationRoleBeanConverter.toShortBean()).thenReturn(new Function<ApplicationRole, ApplicationRoleBean>() {
            @Override
            public ApplicationRoleBean apply(final ApplicationRole input) {
                return mock(ApplicationRoleBean.class);
            }
        });

        userBean = createUserBean();
        setupI18NHelper();
    }

    @Test
    public void testUpdateUser() throws Exception {
        final Response response = currentUserResource.updateUser(userBean);
        validateUpdateResponse(response);

        verify(userManager, times(1)).updateUser(
                argThat(new UserMatchers.IsUserWithName(jiraAuthenticationContextMock.getUser().getName())));
    }

    @Test
    public void testUpdateUserWhenNotLoggedIn() throws Exception {
        when(jiraAuthenticationContextMock.getUser()).thenReturn(null);
        expectedException.expect(NotAuthorisedWebException.class);

        currentUserResource.updateUser(userBean);
    }

    @Test
    public void testUpdateUserWithAllFieldsBlank() throws Exception {
        //blank property means this property will not be changed
        final UserWriteBean emptyUser = new UserWriteBean.Builder().toUserBean();
        expectedException.expect(BadRequestWebException.class);

        currentUserResource.updateUser(emptyUser);
    }

    @Test
    public void testUpdateUserWithTooLongEmail() throws Exception {
        expectedException.expect(BadRequestWebException.class);

        final UserWriteBean updateUser = new UserWriteBean.Builder()
                .emailAddress(StringUtils.repeat('X', 256) + "@localhost")
                .toUserBean();

        currentUserResource.updateUser(updateUser);
    }

    @Test
    public void testUpdateUserWithTooLongDisplayName() throws Exception {
        expectedException.expect(BadRequestWebException.class);

        final UserWriteBean updateUser = new UserWriteBean.Builder()
                .displayName(StringUtils.repeat('X', 256))
                .toUserBean();

        currentUserResource.updateUser(updateUser);
    }

    @Test
    public void testUpdateUserWithInvalidEmail() throws Exception {
        expectedException.expect(BadRequestWebException.class);

        final UserWriteBean updateUser = new UserWriteBean.Builder()
                .emailAddress("wrongemailaddress")
                .toUserBean();

        currentUserResource.updateUser(updateUser);
    }

    @Test
    public void testUpdateUserInReadOnlyDirectory() throws Exception {
        when(userManager.canUpdateUser(any(ApplicationUser.class))).thenReturn(false);
        expectedException.expect(ForbiddenWebException.class);

        currentUserResource.updateUser(userBean);
    }

    @Test
    public void testUpdateUserWithNoPassword() throws Exception {
        expectedException.expect(BadRequestWebException.class);

        final UserWriteBean updateUser = new UserWriteBean.Builder()
                .displayName("Charlie")
                .emailAddress("charlie@atlassian.com")
                .toUserBean();
        currentUserResource.updateUser(updateUser);
    }

    @Test
    public void testUpdateUserWithInvalidPassword() throws Exception {
        expectedException.expect(BadRequestWebException.class);

        final UserWriteBean updateUser = new UserWriteBean.Builder()
                .password(CURRENT_PASSWORD + "*")
                .displayName("Charlie")
                .emailAddress("charlie@atlassian.com")
                .toUserBean();
        currentUserResource.updateUser(updateUser);
    }

    @Test
    public void testUpdateUserAuthenticateOperationFailed() throws Exception {
        expectedException.expect(ServerErrorWebException.class);

        final UserWriteBean updateUser = new UserWriteBean.Builder()
                .password(CURRENT_PASSWORD)
                .displayName("Charlie")
                .emailAddress("charlie@atlassian.com")
                .toUserBean();

        LoginService mockLoginService = mock(LoginService.class);
        when(mockLoginService.authenticate(any(ApplicationUser.class), anyString())).thenThrow(OperationFailedException.class);

        CurrentUserResource currentUserResource = new CurrentUserResource(
                userUtilMock,
                userManager,
                passwordPolicyManagerMock,
                eventPublisher,
                mock(I18nHelper.class),
                mock(EmailFormatter.class),
                jiraAuthenticationContextMock,
                timeZoneManager,
                mock(AvatarService.class),
                jiraBaseUrls,
                mockLoginService,
                beanFactory,
                applicationRoleBeanConverter,
                applicationRoleManager);

        currentUserResource.updateUser(updateUser);
    }

    @Test
    public void testChangeMyPasswordWhenNotLoggedIn() throws Exception {
        when(jiraAuthenticationContextMock.getUser()).thenReturn(null);
        expectedException.expect(NotAuthorisedWebException.class);

        final PasswordBean newPass = new PasswordBean(NEW_PASSWORD);
        currentUserResource.changeMyPassword(newPass);
    }

    @Test
    public void testChangeMyPassword() throws Exception {
        final PasswordBean newPass = new PasswordBean(NEW_PASSWORD, CURRENT_PASSWORD);

        final Response response = currentUserResource.changeMyPassword(newPass);
        validateNoContentResponse(response);

        verify(userUtilMock, times(1)).changePassword(Matchers.<ApplicationUser>any(), eq(newPass.getPassword()));
    }

    @Test
    public void testChangeMyPasswordWithBlankPassword() throws Exception {
        final PasswordBean newPass = new PasswordBean(" ");

        expectedException.expect(BadRequestWebException.class);

        currentUserResource.changeMyPassword(newPass);
    }

    @Test
    public void testChangeMyPasswordWithInvalidPassword() throws Exception {
        final PasswordBean newPass = new PasswordBean(NEW_PASSWORD);

        final Collection<WebErrorMessage> errorMessages = ImmutableList.<WebErrorMessage>of(new WebErrorMessageImpl("some error message", null, null));
        when(passwordPolicyManagerMock.checkPolicy(any(ApplicationUser.class), anyString(), anyString())).thenReturn(errorMessages);

        expectedException.expect(BadRequestWebException.class);

        currentUserResource.changeMyPassword(newPass);
    }

    @Test
    public void testChangeMyPasswordWithInvalidCurrentPassword() throws Exception {
        final PasswordBean newPass = new PasswordBean(NEW_PASSWORD, CURRENT_PASSWORD + "-");

        expectedException.expect(BadRequestWebException.class);

        currentUserResource.changeMyPassword(newPass);
    }

    @Test
    public void testChangeMyPasswordThrowsPermissionException() throws Exception {
        final PasswordBean newPass = new PasswordBean(NEW_PASSWORD, CURRENT_PASSWORD);

        throwOnChangePassword(newPass);

        expectedException.expect(ForbiddenWebException.class);

        currentUserResource.changeMyPassword(newPass);
    }

    @Test
    public void testChangeMyPasswordThrowsOperationNotPermittedException() throws Exception {
        final PasswordBean newPass = new PasswordBean(NEW_PASSWORD, CURRENT_PASSWORD);

        doThrow(new OperationNotPermittedException()).when(userUtilMock).changePassword(Matchers.<ApplicationUser>any(), Matchers.eq(newPass.getPassword()));

        expectedException.expect(ForbiddenWebException.class);

        currentUserResource.changeMyPassword(newPass);
    }

    @Test
    public void testChangeMyPasswordThrowsInvalidCredentialException() throws Exception {
        final PasswordBean newPass = new PasswordBean(NEW_PASSWORD, CURRENT_PASSWORD);

        doThrow(new InvalidCredentialException()).when(userUtilMock).changePassword(Matchers.<ApplicationUser>any(), Matchers.eq(newPass.getPassword()));

        expectedException.expect(BadRequestWebException.class);

        currentUserResource.changeMyPassword(newPass);
    }

    @Test
    public void testChangeMyPasswordThrowsUserNotFoundException() throws Exception {
        final PasswordBean newPass = new PasswordBean(NEW_PASSWORD, CURRENT_PASSWORD);

        doThrow(new UserNotFoundException("not found")).when(userUtilMock).changePassword(Matchers.<ApplicationUser>any(), Matchers.eq(newPass.getPassword()));

        expectedException.expect(BadRequestWebException.class);

        currentUserResource.changeMyPassword(newPass);
    }

    @Test
    public void testChangeMyPasswordThrowsNotAuthorisedOnElevatedSecurity() throws Exception {
        final PasswordBean newPass = new PasswordBean(NEW_PASSWORD, CURRENT_PASSWORD);

        correctPasswordResult = new LoginResultImpl(
                // this reason is given if there are more unsuccessful login attempts than allowed
                LoginReason.AUTHENTICATION_DENIED, null, "charlie", ImmutableSet.<DeniedReason>of()
        );

        expectedException.expect(NotAuthorisedWebException.class);

        currentUserResource.changeMyPassword(newPass);
    }

    @Test
    public void testGetUser() throws Exception {
        when(applicationRoleManager.getRolesForUser(currentUser)).thenReturn(ImmutableSet.<ApplicationRole>of(
                new MockApplicationRole(ApplicationKey.valueOf("jira-software")),
                new MockApplicationRole(ApplicationKey.valueOf("jira-core"))
        ));

        setupI18NHelper();
        final Response response = currentUserResource.getUser();
        validateGetResponse(response);
        assertEquals(((UserBean) response.getEntity()).getApplicationRoles().getSize(), 2);
    }

    @Test
    public void testGetUserWhenNotLoggedIn() throws Exception {
        when(jiraAuthenticationContextMock.getUser()).thenReturn(null);
        expectedException.expect(NotAuthorisedWebException.class);

        currentUserResource.getUser();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    private MockApplicationUser configureCurrentLoggedJiraUser(final String username, final String password)
            throws Exception {
        final MockApplicationUser loggedUser = configureJiraUser(username);
        when(jiraAuthenticationContextMock.getUser()).thenReturn(loggedUser);

        ApplicationUser directoryUser = loggedUser;

        correctPasswordResult = new LoginResultImpl(
                LoginReason.OK, null, username, ImmutableSet.<DeniedReason>of()
        );
        when(loginService.authenticate(same(directoryUser), eq(password))).thenAnswer(new Answer<LoginResult>() {
            @Override
            public LoginResult answer(final InvocationOnMock invocation) throws Throwable {
                return correctPasswordResult;
            }
        });

        incorrectPasswordResult = new LoginResultImpl(
                LoginReason.AUTHENTICATED_FAILED, null, username, ImmutableSet.<DeniedReason>of()
        );
        when(loginService.authenticate(same(directoryUser), not(eq(password)))).thenAnswer(new Answer<LoginResult>() {
            @Override
            public LoginResult answer(final InvocationOnMock invocation) throws Throwable {
                return incorrectPasswordResult;
            }
        });

        return loggedUser;
    }

    private MockApplicationUser configureJiraUser(final String username) {
        final String key = "key-" + username;
        final MockApplicationUser user = new MockApplicationUser(key, username);

        when(userManager.canUpdateUser(any(ApplicationUser.class))).thenReturn(true);

        when(timeZoneManager.getTimeZoneforUser(user)).thenReturn(TimeZone.getTimeZone("Europe/Warsaw"));

        when(userUtilMock.getUserByKey(key)).thenReturn(user);
        when(userUtilMock.getUserByName(username)).thenReturn(user);

        return user;
    }

    private void validateUpdateResponse(final Response response) {
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
    }

    private void validateNoContentResponse(final Response response) {
        Assert.assertEquals(Response.Status.NO_CONTENT.getStatusCode(), response.getStatus());
    }

    private void validateGetResponse(final Response response) {
        Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
    }

    private UserWriteBean createUserBean() {
        return new UserWriteBean.Builder().password(CURRENT_PASSWORD).displayName("Charlie").emailAddress("charlie@atlassian.com").toUserBean();
    }

    private void throwOnChangePassword(final PasswordBean newPass)
            throws UserNotFoundException, InvalidCredentialException, OperationNotPermittedException, PermissionException {
        doThrow(new PermissionException()).when(userUtilMock).changePassword(Matchers.<ApplicationUser>any(), Matchers.eq(newPass.getPassword()));
    }

    private void setupI18NHelper() {
        ApplicationUser loggedInUser = jiraAuthenticationContextMock.getUser();
        I18nHelper i18nHelper = mock(I18nHelper.class);
        when(i18nHelper.getLocale()).thenReturn(Locale.CANADA_FRENCH);
        when(beanFactory.getInstance(loggedInUser)).thenReturn(i18nHelper);
    }

}
