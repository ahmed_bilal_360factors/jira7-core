package com.atlassian.jira.rest.v2;

import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.bc.user.UserApplicationHelper;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.rest.internal.v2.ViewUserResource;
import com.atlassian.jira.rest.internal.v2.ViewUserResource.UserApplicationUpdateBean;
import com.atlassian.jira.rest.util.ResponseFactory;
import com.atlassian.jira.rest.util.UpdateUserApplicationHelper;
import com.atlassian.jira.rest.util.UpdateUserApplicationHelper.ApplicationUpdateResult;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.SimpleErrorCollection;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * @since 7.1
 */
public class TestViewUserResource {
    private static final String USER_NAME = "freddie";
    private static final String APP_KEY = "jira-jira";

    @Rule
    public RuleChain mockitoMocksInContainer = MockitoMocksInContainer.forTest(this);

    @Mock
    private UpdateUserApplicationHelper updateUserApplicationHelper;

    @Mock
    private ResponseFactory responseFactory;

    @Mock
    private UserApplicationHelper userApplicationHelper;

    @Mock
    private JiraAuthenticationContext authContext;

    @Mock
    private GlobalPermissionManager permissionManager;

    @Mock
    private UserManager userManager;

    @Mock
    private ApplicationRoleManager applicationRoleManager;

    @Mock
    private ApplicationUpdateResult result;

    @Mock
    private ApplicationUser user;

    @Mock
    private ApplicationUser currentUser;

    private final SimpleErrorCollection errorCollection = new SimpleErrorCollection();

    private ViewUserResource resource;

    @Before
    public void setUp() throws Exception {
        resource = new ViewUserResource(updateUserApplicationHelper, responseFactory, userApplicationHelper,
                authContext, permissionManager, userManager, applicationRoleManager);

        when(authContext.getLoggedInUser()).thenReturn(currentUser);

        when(updateUserApplicationHelper.addUserToApplication(USER_NAME, APP_KEY)).thenReturn(result);
        when(updateUserApplicationHelper.removeUserFromApplication(USER_NAME, APP_KEY)).thenReturn(result);

        when(result.getErrorCollection()).thenReturn(errorCollection);
        when(result.getApplicationUser()).thenReturn(Option.some(user));
    }

    @Test
    public void shouldAddUserCheckGroupUpdatePermission() {
        UserApplicationUpdateBean bean = (UserApplicationUpdateBean) resource.addUserToApplication(USER_NAME, APP_KEY).getEntity();

        assertFalse("User access is not editable", bean.isEditable());
    }

    @Test
    public void shouldRemoveUserCheckGroupUpdatePermission() {
        when(userManager.canUpdateGroupMembershipForUser(user)).thenReturn(true);

        UserApplicationUpdateBean bean = (UserApplicationUpdateBean) resource.removeUserFromApplication("freddie", "jira-jira").getEntity();

        assertTrue("User access is editable", bean.isEditable());
    }

}
