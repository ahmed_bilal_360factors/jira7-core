package com.atlassian.jira.rest.v2.project.type;

import com.atlassian.fugue.Option;
import com.atlassian.jira.project.type.ProjectType;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.project.type.ProjectTypeManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.ws.rs.core.Response;
import java.util.Arrays;

import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestProjectTypeResource {
    private static final ApplicationUser USER = new MockApplicationUser("user");

    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private ProjectTypeManager projectTypeManager;

    private ProjectTypeResource resource;

    @Before
    public void setUp() {
        resource = new ProjectTypeResource(projectTypeManager, authenticationContext);
    }

    @Test
    public void getAllProjectTypesReturnsAllProjectsWhenThereAreSome() {
        havingProjectTypes(
                projectType("type1", "desc1", "icon1", "color1"),
                projectType("type2", "desc2", "icon2", "color2")
        );

        Response response = resource.getAllProjectTypes();

        assertIsOk(response, newArrayList(
                projectTypeBean("type1", "desc1", "icon1", "color1"),
                projectTypeBean("type2", "desc2", "icon2", "color2")
        ));
    }

    @Test
    public void getAllProjectTypesReturnsAnEmptyListIfThereAreNoProjectTypes() {
        havingNoProjectTypes();

        Response response = resource.getAllProjectTypes();

        assertIsOk(response, newArrayList());
    }

    @Test
    public void getProjectTypeByKeyReturnsBeanWhenProjectTypeExists() {
        havingProjectTypeWithKey("business", projectType("business", "desc", "icon", "color"));

        Response response = resource.getProjectTypeByKey("business");

        assertIsOk(response, projectTypeBean("business", "desc", "icon", "color"));
    }

    @Test
    public void getProjectTypeByKeyReturns404WhenProjectTypeDoesNotExist() {
        havingNoProjectTypeWithKey("business");

        Response response = resource.getProjectTypeByKey("business");

        assertIsNotFound(response);
    }

    @Test
    public void getAccessibleProjectTypeByKeyReturns404IfTheUserIsNotLoggedIn() {
        havingNoUserLoggedIn();

        Response response = resource.getAccessibleProjectTypeByKey("business");

        assertIsUnauthorized(response);
    }

    @Test
    public void getAccessibleProjectTypeByKeyReturns404IfTheUserIsLoggedInButHasNoAccessToTheProjectType() {
        havingLoggedInUser(USER);
        havingNoAccessToProjectTypeWithKey(USER, "business");

        Response response = resource.getAccessibleProjectTypeByKey("business");

        assertIsNotFound(response);
    }

    @Test
    public void getAccessibleProjectTypeByKeyReturnsBeanWhenProjectTypeIsAccessibleByLoggedInUser() {
        havingLoggedInUser(USER);
        havingAccessToProjectTypeWithKey(USER, "business", projectType("business", "desc", "icon", "color"));

        Response response = resource.getAccessibleProjectTypeByKey("business");

        assertIsOk(response, projectTypeBean("business", "desc", "icon", "color"));
    }

    private void havingLoggedInUser(ApplicationUser user) {
        when(authenticationContext.isLoggedInUser()).thenReturn(true);
        when(authenticationContext.getLoggedInUser()).thenReturn(user);
    }

    private void havingNoUserLoggedIn() {
        when(authenticationContext.isLoggedInUser()).thenReturn(false);
    }

    private void havingAccessToProjectTypeWithKey(ApplicationUser user, String key, ProjectType projectType) {
        when(projectTypeManager.getAccessibleProjectType(user, new ProjectTypeKey(key))).thenReturn(Option.some(projectType));
    }

    private void havingNoAccessToProjectTypeWithKey(ApplicationUser user, String key) {
        when(projectTypeManager.getAccessibleProjectType(user, new ProjectTypeKey(key)))
                .thenReturn(Option.<ProjectType>none());
    }

    private void havingNoProjectTypeWithKey(String key) {
        when(projectTypeManager.getByKey(new ProjectTypeKey(key)))
                .thenReturn(Option.<ProjectType>none());
    }

    private void havingProjectTypeWithKey(String key, ProjectType projectType) {
        when(projectTypeManager.getByKey(new ProjectTypeKey(key))).thenReturn(Option.some(projectType));
    }

    private void havingNoProjectTypes() {
        havingProjectTypes();
    }

    private void havingProjectTypes(ProjectType... projectTypes) {
        when(projectTypeManager.getAllProjectTypes()).thenReturn(Arrays.asList(projectTypes));
    }

    private ProjectType projectType(String key, String descriptionI18nKey, String icon, String color) {
        return new ProjectType(new ProjectTypeKey(key), descriptionI18nKey, icon, color, 100);
    }

    private ProjectTypeBean projectTypeBean(String key, String descriptionI18nKey, String icon, String color) {
        return new ProjectTypeBean(key, descriptionI18nKey, icon, color);
    }

    private void assertIsNotFound(Response response) {
        assertThat(response.getStatus(), is(404));
        assertThat(response.getEntity(), is(nullValue()));
    }

    private void assertIsUnauthorized(final Response response) {
        assertThat(response.getStatus(), is(401));
        assertThat(response.getEntity(), is(nullValue()));
    }

    private void assertIsOk(Response response, Object expectedEntity) {
        assertThat(response.getStatus(), is(200));
        assertThat(response.getEntity(), is(expectedEntity));
    }
}
