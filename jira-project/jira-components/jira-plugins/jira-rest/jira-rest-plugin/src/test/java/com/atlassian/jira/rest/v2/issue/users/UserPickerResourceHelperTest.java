package com.atlassian.jira.rest.v2.issue.users;

import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.google.common.collect.ImmutableList.copyOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @since v5.2
 */
@RunWith(MockitoJUnitRunner.class)
public class UserPickerResourceHelperTest {
    private static final ApplicationUser USER_A = new MockApplicationUser("a");
    private static final ApplicationUser USER_B = new MockApplicationUser("b");
    private static final ApplicationUser USER_C = new MockApplicationUser("c");

    private static final List<ApplicationUser> USER_LIST = Arrays.asList(USER_A, USER_B, USER_C);

    @Test
    public void testLimitUserSearch() {
        UserPickerResourceHelper userPickerResourceHelper = new UserPickerResourceHelperImpl(null, null, null, null, null);

        List<ApplicationUser> returnedList = userPickerResourceHelper.limitUserSearch(null, null, USER_LIST, null);
        assertEquals(USER_LIST, returnedList);

        returnedList = userPickerResourceHelper.limitUserSearch(-1, -1, USER_LIST, null);
        assertEquals(USER_LIST, returnedList);

        returnedList = userPickerResourceHelper.limitUserSearch(1, null, USER_LIST, null);
        assertTrue(Iterables.elementsEqual(Iterables.skip(USER_LIST, 1), returnedList));

        returnedList = userPickerResourceHelper.limitUserSearch(USER_LIST.size() + 1, null, USER_LIST, null);
        assertTrue(returnedList.isEmpty());

        returnedList = userPickerResourceHelper.limitUserSearch(null, 1, USER_LIST, null);
        assertTrue(Iterables.elementsEqual(Iterables.limit(USER_LIST, 1), returnedList));

        returnedList = userPickerResourceHelper.limitUserSearch(null, 0, USER_LIST, null);
        assertTrue(returnedList.isEmpty());

        returnedList = userPickerResourceHelper.limitUserSearch(null, null, USER_LIST, Collections.<String>emptyList());
        assertEquals(USER_LIST, returnedList);

        returnedList = userPickerResourceHelper.limitUserSearch(null, null, USER_LIST, Arrays.asList(USER_A.getName()));
        assertTrue(Iterables.elementsEqual(copyOf(Iterables.filter(USER_LIST, new Predicate<ApplicationUser>() {
            @Override
            public boolean apply(ApplicationUser input) {
                return input != USER_A;
            }
        })), returnedList));

        returnedList = userPickerResourceHelper.limitUserSearch(1, 1, USER_LIST, null);
        assertTrue(Iterables.elementsEqual(Iterables.limit(Iterables.skip(USER_LIST, 1), 1), returnedList));
    }
}
