package com.atlassian.jira.rest.v1.model.errors;

import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.collect.CollectionBuilder;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class TestErrorCollection {
    @Test
    public void nullArgumentsShouldThrowIllegalArgumentException() {
        try {
            ErrorCollection.Builder.newBuilder().addErrorMessage(null);
            fail("Should have thrown exception");
        } catch (IllegalArgumentException e) {
            //yay
        }
        try {
            ErrorCollection.Builder.newBuilder().addErrorCollection(null);
            fail("Should have thrown exception");
        } catch (IllegalArgumentException e) {
            //yay
        }
        try {
            ErrorCollection.Builder.newBuilder().addError(null, "blah");
            fail("Should have thrown exception");
        } catch (IllegalArgumentException e) {
            //yay
        }
        try {
            ErrorCollection.Builder.newBuilder().addError("blah", null);
            fail("Should have thrown exception");
        } catch (IllegalArgumentException e) {
            //yay
        }
        try {
            ErrorCollection.Builder.newBuilder((ErrorCollection) null);
            fail("Should have thrown exception");
        } catch (IllegalArgumentException e) {
            //yay
        }
        try {
            ErrorCollection.Builder.newBuilder((Collection<ValidationError>) null);
            fail("Should have thrown exception");
        } catch (IllegalArgumentException e) {
            //yay
        }
    }

    @Test
    public void collectionRemainsEmptyWhenEmptyCollectionIsAdded() {
        final ErrorCollection errorCollection = ErrorCollection.Builder.newBuilder().addErrorCollection(new SimpleErrorCollection()).build();
        assertFalse(errorCollection.hasAnyErrors());
    }

    @Test
    public void addSimpleErrorCollection() {
        com.atlassian.jira.util.ErrorCollection errors = new SimpleErrorCollection();
        errors.addError("user", "Bad username");
        errors.addError("field", "Bad field value");
        errors.addErrorMessage("Generic problem");
        errors.addErrorMessage("Another generic issue");
        errors.addErrorMessage("Third generic issue");

        final ErrorCollection errorCollection = ErrorCollection.Builder.newBuilder().addErrorCollection(errors).build();
        assertThat(errorCollection.getErrorMessages(), hasSize(3));
        assertThat(errorCollection.getErrors(), hasSize(2));

        List<String> errorMessages = new ArrayList<>(errorCollection.getErrorMessages());
        assertThat(errorMessages.get(0), equalTo("Generic problem"));
        assertThat(errorMessages.get(1), equalTo("Another generic issue"));
        assertThat(errorMessages.get(2), equalTo("Third generic issue"));

        Collection<ValidationError> fieldErrors = errorCollection.getErrors();
        for (ValidationError fieldError : fieldErrors) {
            if (fieldError.getField().equals("user")) {
                assertThat(fieldError.getError(), equalTo("Bad username"));
                assertNull(fieldError.getParams());
            } else {
                assertThat(fieldError.getError(), equalTo("Bad field value"));
                assertNull(fieldError.getParams());
            }
        }

        final ErrorCollection copiedCollection = ErrorCollection.Builder.newBuilder(errorCollection).build();
        assertNotSame(errorCollection, copiedCollection);

        assertThat(copiedCollection.getErrorMessages(), hasSize(3));
        assertThat(copiedCollection.getErrors(), hasSize(2));

        errorMessages = new ArrayList<>(copiedCollection.getErrorMessages());
        assertThat(errorMessages.get(0), equalTo("Generic problem"));
        assertThat(errorMessages.get(1), equalTo("Another generic issue"));
        assertThat(errorMessages.get(2), equalTo("Third generic issue"));

        fieldErrors = copiedCollection.getErrors();
        for (ValidationError fieldError : fieldErrors) {
            if (fieldError.getField().equals("user")) {
                assertThat(fieldError.getError(), equalTo("Bad username"));
                assertNull(fieldError.getParams());
            } else {
                assertThat(fieldError.getError(), equalTo("Bad field value"));
                assertNull(fieldError.getParams());
            }
        }

    }

    @Test
    public void addErrorMessage() {
        final ErrorCollection errorCollection = ErrorCollection.Builder.newBuilder().addErrorMessage("First message").addErrorMessage("Second message").build();
        assertThat(errorCollection.getErrorMessages(), hasSize(2));

        final List<String> errorMessages = new ArrayList<>(errorCollection.getErrorMessages());
        assertThat(errorMessages.get(0), equalTo("First message"));
        assertThat(errorMessages.get(1), equalTo("Second message"));
    }

    @Test
    public void addErrors() {
        final ErrorCollection errorCollection = ErrorCollection.Builder.newBuilder().addError("field1", "bad error", "param1", "param2").
                addError("field2", "bad error2").build();

        assertThat(errorCollection.getErrors(), hasSize(2));

        final Collection<ValidationError> fieldErrors = errorCollection.getErrors();
        for (ValidationError fieldError : fieldErrors) {
            if (fieldError.getField().equals("field1")) {
                assertThat(fieldError.getError(), equalTo("bad error"));
                assertThat(fieldError.getParams(), hasSize(2));
                final List<String> params = new ArrayList<>(fieldError.getParams());
                assertThat(params.get(0), equalTo("param1"));
                assertThat(params.get(1), equalTo("param2"));
            } else {
                assertThat(fieldError.getError(), equalTo("bad error2"));
                assertNull(fieldError.getParams());
            }
        }

    }

    @Test
    public void addValidationErrors() {
        final ErrorCollection errors = ErrorCollection.Builder.newBuilder(new ValidationError("field1", "error1"), new ValidationError("field2", "error2")).build();
        assertThat(errors.getErrors(), hasSize(2));
        assertThat(errors.getErrorMessages(), hasSize(0));


        final Collection<ValidationError> fieldErrors = errors.getErrors();
        for (ValidationError fieldError : fieldErrors) {
            if (fieldError.getField().equals("field1")) {
                assertThat(fieldError.getError(), equalTo("error1"));
                assertNull(fieldError.getParams());
            } else {
                assertThat(fieldError.getField(), equalTo("field2"));
                assertThat(fieldError.getError(), equalTo("error2"));
                assertNull(fieldError.getParams());
            }
        }
    }

    @Test
    public void addValidationErrorCollection() {
        final List<ValidationError> list = CollectionBuilder.newBuilder(new ValidationError("field1", "error1"), new ValidationError("field2", "error2")).asList();
        final ErrorCollection errors = ErrorCollection.Builder.newBuilder(list).build();
        assertThat(errors.getErrors(), hasSize(2));
        assertThat(errors.getErrorMessages(), hasSize(0));


        final Collection<ValidationError> fieldErrors = errors.getErrors();
        for (ValidationError fieldError : fieldErrors) {
            if (fieldError.getField().equals("field1")) {
                assertThat(fieldError.getError(), equalTo("error1"));
                assertNull(fieldError.getParams());
            } else {
                assertThat(fieldError.getField(), equalTo("field2"));
                assertThat(fieldError.getError(), equalTo("error2"));
                assertNull(fieldError.getParams());
            }
        }
    }

    @Test
    public void hasAnyErrors() {
        final ErrorCollection empty = ErrorCollection.Builder.newBuilder().build();
        assertFalse(empty.hasAnyErrors());

        final ErrorCollection errorCollection = ErrorCollection.Builder.newBuilder().addErrorMessage("blah").build();
        assertTrue(errorCollection.hasAnyErrors());

        final ErrorCollection errorCollection2 = ErrorCollection.Builder.newBuilder().addError("blah", "boo").build();
        assertTrue(errorCollection2.hasAnyErrors());
    }
}
