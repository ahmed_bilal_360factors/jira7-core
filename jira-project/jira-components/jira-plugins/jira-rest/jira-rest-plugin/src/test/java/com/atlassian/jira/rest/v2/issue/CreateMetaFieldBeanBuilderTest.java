package com.atlassian.jira.rest.v2.issue;

import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.MockOrderableField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.fields.screen.FieldScreenRenderLayoutItem;
import com.atlassian.jira.issue.fields.screen.FieldScreenRenderTab;
import com.atlassian.jira.issue.fields.screen.FieldScreenRenderer;
import com.atlassian.jira.issue.fields.screen.FieldScreenRendererFactory;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.operation.IssueOperations;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.mock.security.MockAuthenticationContext;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.rest.api.util.StringList;
import com.atlassian.jira.rest.v2.issue.context.ContextUriInfo;
import com.atlassian.jira.rest.v2.issue.version.VersionBeanFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

import static com.atlassian.jira.issue.IssueFieldConstants.ISSUE_TYPE;
import static com.atlassian.jira.issue.IssueFieldConstants.PROJECT;
import static com.atlassian.jira.matchers.ReflectionMatchers.propertiesMatcher;
import static com.atlassian.jira.rest.v2.issue.IncludedFields.nothingIncludedByDefault;
import static com.google.common.collect.ImmutableList.of;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@SuppressWarnings("unchecked")
public class CreateMetaFieldBeanBuilderTest {
    @Rule
    public TestRule mockitoMocksInContainer = MockitoMocksInContainer.forTest(this);

    @Mock
    private FieldScreenRendererFactory fieldScreenRendererFactory;
    @Mock
    private FieldLayoutManager fieldLayoutManager;
    @Mock
    private Issue issue;
    @Mock
    private IssueType issueType;
    @Mock
    private Project project;
    @Mock
    private ApplicationUser user;
    @Mock
    private VersionBeanFactory versionBeanFactory;
    @Mock
    private VelocityRequestContextFactory velocityRequestContextFactory;
    @Mock
    private ContextUriInfo contextUriInfo;
    @Mock
    private JiraBaseUrls baseUrls;
    @Mock
    private FieldScreenRenderer fieldScreenRenderer;
    @Mock
    private PermissionManager permissionManager;

    private JiraAuthenticationContext authContext = new MockAuthenticationContext(user, new MockI18nHelper());
    @Mock
    private FieldManager fieldManager;
    @Mock
    private DefaultFieldMetaBeanHelper defaultFieldHelper;
    @Mock
    @AvailableInContainer
    private AvatarService avatarService;
    private CreateMetaFieldBeanBuilder metaFieldBeanBuilder;

    @Before
    public void before() {
        metaFieldBeanBuilder = new CreateMetaFieldBeanBuilder(fieldLayoutManager, project, issue, issueType,
                user, versionBeanFactory, velocityRequestContextFactory, contextUriInfo, baseUrls, permissionManager,
                fieldScreenRendererFactory, authContext, fieldManager, defaultFieldHelper);

        when(fieldManager.getOrderableField(ISSUE_TYPE)).thenReturn(new MockOrderableField(ISSUE_TYPE, "issue.type.name"));
        when(fieldScreenRendererFactory.getFieldScreenRenderer(issue, IssueOperations.CREATE_ISSUE_OPERATION))
                .thenReturn(fieldScreenRenderer);
    }

    @Test
    public void shouldBuildEmpty() throws Exception {
        when(permissionManager.hasPermission(Permissions.CREATE_ISSUE, issue, user)).thenReturn(true);
        when(fieldScreenRenderer.getFieldScreenRenderTabs()).thenReturn(Collections.emptyList());

        final Map<String, FieldMetaBean> fields = metaFieldBeanBuilder.build();

        assertThat("Issue type and project are always required", fields.keySet(), containsInAnyOrder(ISSUE_TYPE, PROJECT));

        assertThat("Fields should be required", fields.values(),
                containsInAnyOrder(requiredWithName("issue.field.project"),
                        requiredWithName("issue.type.name")));
    }

    @Test
    public void shouldAddParentForSubTasks() throws Exception {
        when(permissionManager.hasPermission(Permissions.CREATE_ISSUE, issue, user)).thenReturn(true);
        when(issueType.isSubTask()).thenReturn(true);

        final Map<String, FieldMetaBean> fields = metaFieldBeanBuilder.build();

        assertThat("there should be parent field ", fields.keySet(), hasItem("parent"));

        assertThat("Fields should be required", fields.values(), hasItem(requiredWithName("issue.field.parent")));
    }

    @Test
    public void shouldCreateKeysOnly() throws Exception {
        when(permissionManager.hasPermission(Permissions.CREATE_ISSUE, issue, user)).thenReturn(true);
        when(issueType.isSubTask()).thenReturn(true);
        final ImmutableList<FieldScreenRenderTab> fieldScreenRenderTabs = of(getFieldsTab(IssueFieldConstants.ASSIGNEE, false, true));
        when(fieldScreenRenderer.getFieldScreenRenderTabs()).thenReturn(fieldScreenRenderTabs);

        final Set<String> fields = metaFieldBeanBuilder.buildKeys();


        assertThat("Should contain proper fields", fields, Matchers.containsInAnyOrder(
                IssueFieldConstants.ISSUE_TYPE, IssueFieldConstants.PROJECT, "parent", "assignee"));
    }

    @Test
    public void shouldHandleNotRequiredFields() throws Exception {
        when(permissionManager.hasPermission(Permissions.CREATE_ISSUE, issue, user)).thenReturn(true);

        final ImmutableList<FieldScreenRenderTab> fieldScreenRenderTabs = of(getFieldsTab(IssueFieldConstants.ASSIGNEE, false, true));
        when(fieldScreenRenderer.getFieldScreenRenderTabs()).thenReturn(fieldScreenRenderTabs);
        final Map<String, FieldMetaBean> fields = metaFieldBeanBuilder.build();

        assertThat("There should be assignee", fields.keySet(), hasItem(IssueFieldConstants.ASSIGNEE));
        assertThat("Field should not be required", fields.values(), hasItem(notRequiredWithName("assignee_name")));
    }

    @Test
    public void shouldNotIncludeNotVisibleItems() throws Exception {
        when(permissionManager.hasPermission(Permissions.CREATE_ISSUE, issue, user)).thenReturn(true);

        final ImmutableList<FieldScreenRenderTab> fieldScreenRenderTabs = of(getFieldsTab(IssueFieldConstants.ASSIGNEE, false, false));
        when(fieldScreenRenderer.getFieldScreenRenderTabs()).thenReturn(fieldScreenRenderTabs);
        final Map<String, FieldMetaBean> fields = metaFieldBeanBuilder.build();

        assertThat("There should be assignee", fields.keySet(), not(hasItem(IssueFieldConstants.ASSIGNEE)));
        assertThat("Field should not be required", fields.values(), not(hasItem(notRequiredWithName("assignee_name"))));
    }

    @Test
    public void shouldNotAddFieldsWhenUserDoesNotHavePermission() throws Exception {
        when(permissionManager.hasPermission(Permissions.CREATE_ISSUE, issue, user)).thenReturn(false);

        final ImmutableList<FieldScreenRenderTab> fieldScreenRenderTabs = of(getFieldsTab(IssueFieldConstants.ASSIGNEE, true, true));
        when(fieldScreenRenderer.getFieldScreenRenderTabs()).thenReturn(fieldScreenRenderTabs);
        final Map<String, FieldMetaBean> fields = metaFieldBeanBuilder.build();

        assertThat("There should be assignee", fields.keySet(), not(hasItem(IssueFieldConstants.ASSIGNEE)));
        assertThat("Field should not be required", fields.values(), not(hasItem(requiredWithName("assignee_name"))));
    }

    @Test
    public void shouldOnlyUseIncludedFields() throws Exception {
        when(permissionManager.hasPermission(Permissions.CREATE_ISSUE, issue, user)).thenReturn(true);
        metaFieldBeanBuilder.fieldsToInclude(nothingIncludedByDefault(of(StringList.fromList("field1"))));
        final ImmutableList<FieldScreenRenderTab> fieldScreenRenderTabs = of(
                getFieldsTab("field1", true, true),
                getFieldsTab("field2", true, true));

        when(fieldScreenRenderer.getFieldScreenRenderTabs()).thenReturn(fieldScreenRenderTabs);

        final Map<String, FieldMetaBean> fields = metaFieldBeanBuilder.build();

        assertThat("There should be field1", fields.keySet(), hasItem("field1"));
        assertThat("There should be not field2", fields.keySet(), not(hasItem("field2")));
        assertThat("Field should contain field1", fields.values(), hasItem(requiredWithName("field1_name")));
        assertThat("Field should not contain field2", fields.values(), not(hasItem(requiredWithName("field2_name"))));
    }

    static FieldScreenRenderTab getFieldsTab(String fieldId, boolean required, boolean isShow) {

        FieldScreenRenderLayoutItem fieldScreenRenderLayoutItem = mock(FieldScreenRenderLayoutItem.class);
        FieldScreenRenderTab tab = mock(FieldScreenRenderTab.class);
        FieldLayoutItem fieldLayoutItem = mock(FieldLayoutItem.class);
        when(fieldLayoutItem.isRequired()).thenReturn(required);
        when(fieldScreenRenderLayoutItem.getFieldLayoutItem()).thenReturn(fieldLayoutItem);

        when(tab.getFieldScreenRenderLayoutItemsForProcessing()).thenReturn(of(fieldScreenRenderLayoutItem));

        final MockOrderableField value = new MockOrderableField(fieldId, fieldId + "_name");
        value.setShown(isShow);
        when(fieldLayoutItem.getOrderableField()).thenReturn(value);
        return tab;
    }

    private Matcher<? super FieldMetaBean> requiredWithName(String name) {

        return propertiesMatcher(ImmutableMap.of("required", equalTo(true), "name", equalTo(name)));
    }

    private Matcher<? super FieldMetaBean> notRequiredWithName(String name) {

        return propertiesMatcher(ImmutableMap.of("required", equalTo(false), "name", equalTo(name)));
    }
}