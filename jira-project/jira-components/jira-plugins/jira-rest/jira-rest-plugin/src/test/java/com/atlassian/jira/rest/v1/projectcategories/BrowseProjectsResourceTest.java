package com.atlassian.jira.rest.v1.projectcategories;

import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.util.velocity.VelocityRequestContext;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.util.velocity.VelocityRequestSession;
import com.atlassian.jira.web.SessionKeys;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.ws.rs.core.Response;

import static org.apache.http.HttpStatus.SC_OK;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BrowseProjectsResourceTest {
    private BrowseProjectsResource resource;
    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;
    @Mock
    private VelocityRequestContextFactory contextFactory;
    @Mock
    private ProjectManager projectManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private VelocityRequestContext velocityRequestContext;
    @Mock
    private VelocityRequestSession session;

    private static final String BUSINESS = "business";

    @Before
    public void setUp() throws Exception {
        resource = new BrowseProjectsResource(jiraAuthenticationContext, contextFactory, projectManager, permissionManager);
    }

    @Test
    public void shouldSetSelectedProjectTypeInSession() throws Exception {
        when(contextFactory.getJiraVelocityRequestContext()).thenReturn(velocityRequestContext);
        when(velocityRequestContext.getSession()).thenReturn(session);

        final Response response = resource.setSelectedProjectTypeKey(BUSINESS);
        assertThat(response.getStatus(), is(SC_OK));
        verify(session).setAttribute(SessionKeys.BROWSE_PROJECTS_CURRENT_PROJECT_TYPE, BUSINESS);
    }

}