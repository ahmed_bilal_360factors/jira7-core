package com.atlassian.jira.rest.util;

import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.URI;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public final class SelfLinkBuilderTest {
    private static final String BASE_URL = "http://jira.atlassian.com";

    @Mock
    private JiraBaseUrls jiraBaseUrls;

    @InjectMocks
    private SelfLinkBuilder selfLinkBuilder;

    @Before
    public void setUp() {
        assumeRestApiBaseUrlIs(BASE_URL);
    }

    @Test
    public void baseUrlIsEvaluatedEveryTimeSelfLinkIsCreated() {
        assumeRestApiBaseUrlIs("http://rest1");
        assertThat(selfLinkBuilder.path("a").toUri(), equalTo(URI.create("http://rest1/a")));

        assumeRestApiBaseUrlIs("http://rest2");
        assertThat(selfLinkBuilder.path("b").toUri(), equalTo(URI.create("http://rest2/b")));
    }

    @Test
    public void selfLinkIsImmutable() // new link is created each time a method is called
    {
        SelfLinkBuilder.SelfLink basePath = selfLinkBuilder.path("a");

        SelfLinkBuilder.SelfLink link1 = basePath.path("b");
        SelfLinkBuilder.SelfLink link2 = basePath.path("c");
        SelfLinkBuilder.SelfLink link3 = basePath.path("d").queryParam("q", "v");
        SelfLinkBuilder.SelfLink link4 = link3.path("e").queryParam("q2", "v2");

        assertThat(link1.toUri(), equalTo(URI.create(BASE_URL + "/a/b")));
        assertThat(link2.toUri(), equalTo(URI.create(BASE_URL + "/a/c")));
        assertThat(link3.toUri(), equalTo(URI.create(BASE_URL + "/a/d?q=v")));
        assertThat(link4.toUri(), equalTo(URI.create(BASE_URL + "/a/d/e?q=v&q2=v2")));
    }

    @Test
    public void toStringReturnsStringifiedUri() {
        SelfLinkBuilder.SelfLink selfLink = selfLinkBuilder.path("a").path("b c").queryParam("k", "v");
        assertThat(selfLink.toString(), equalTo(selfLink.toUri().toString()));
    }

    @Test
    public void specialCharactersAreEncoded() {
        URI url = selfLinkBuilder.path("a b").toUri();
        assertThat(url, equalTo(URI.create(BASE_URL + "/a%20b")));
    }

    private void assumeRestApiBaseUrlIs(String baseUrl) {
        when(jiraBaseUrls.restApi2BaseUrl()).thenReturn(baseUrl);
    }
}
