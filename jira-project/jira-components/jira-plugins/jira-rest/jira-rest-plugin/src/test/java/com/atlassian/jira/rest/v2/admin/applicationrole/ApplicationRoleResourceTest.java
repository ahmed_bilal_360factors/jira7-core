package com.atlassian.jira.rest.v2.admin.applicationrole;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.application.ApplicationRoleAdminService;
import com.atlassian.jira.application.MockApplicationRole;
import com.atlassian.jira.application.MockApplicationRoleAdminService;
import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.rest.matchers.ErrorCollectionMatcher;
import com.atlassian.jira.security.groups.MockGroupManager;
import com.atlassian.jira.user.MockGroup;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import static com.atlassian.jira.rest.matchers.ResponseMatchers.body;
import static com.atlassian.jira.rest.matchers.ResponseMatchers.errorBody;
import static com.atlassian.jira.rest.matchers.ResponseMatchers.noCache;
import static com.atlassian.jira.rest.matchers.ResponseMatchers.status;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.FORBIDDEN;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.Status.OK;
import static javax.ws.rs.core.Response.Status.PRECONDITION_FAILED;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ApplicationRoleResourceTest {
    @Rule
    public RuleChain chain = MockitoMocksInContainer.forTest(this);

    public I18nHelper helper = new NoopI18nHelper(Locale.ENGLISH);
    private MockApplicationRoleAdminService service = new MockApplicationRoleAdminService();
    private MockGroupManager groupManager = new MockGroupManager();
    private Function<ApplicationRole, ApplicationRoleBean> converter = input ->
    {
        final ApplicationRoleBean roleBean = new ApplicationRoleBean();
        roleBean.setKey(input.getKey().value());
        roleBean.setGroups(groupNames(input.getGroups()));
        roleBean.setDefaultGroups(groupNames(input.getDefaultGroups()));
        roleBean.setName(input.getName());
        roleBean.setNumberOfSeats(input.getNumberOfSeats());
        roleBean.setDefined(input.isDefined());
        roleBean.setSelectedByDefault(input.isSelectedByDefault());

        return roleBean;
    };

    @Mock
    private FeatureManager featureManager;
    @Mock
    private ApplicationRoleHasher applicationRoleHasher;

    private ApplicationRoleResource applicationRoleResource;

    private final Group one = groupManager.createGroup("one");
    private final Group two = groupManager.createGroup("two");
    private final Group three = groupManager.createGroup("three");
    private final Group four = groupManager.createGroup("four");

    @Before
    public void setUp() {
        when(applicationRoleHasher.getVersionHash(anyObject())).thenReturn("1");
        applicationRoleResource = new ApplicationRoleResource(service, helper, groupManager, converter,
                applicationRoleHasher);
    }

    @Test
    public void getAllWorksOnSuccess() {
        final MockApplicationRole role1 = createApplicationRole("id").groups(one, two).name("Name");
        final MockApplicationRole role2 = createApplicationRole("idThree").name("Empty");

        final Response response = applicationRoleResource.getAll();

        assertThat(response, noCache());
        assertThat(response, status(OK));

        @SuppressWarnings("unchecked")
        final List<ApplicationRoleBean> entity = (List<ApplicationRoleBean>) response.getEntity();
        assertThat(entity, Matchers.containsInAnyOrder(fromRole(role1), fromRole(role2)));
    }

    @Test
    public void getAllFailsOnError() {
        final String error1 = "Error";
        final ErrorCollection.Reason forbidden = ErrorCollection.Reason.FORBIDDEN;

        final ServiceOutcomeImpl<Set<ApplicationRole>> error
                = ServiceOutcomeImpl.error(error1, forbidden);

        final ApplicationRoleAdminService service = mock(ApplicationRoleAdminService.class);
        when(service.getRoles()).thenReturn(error);

        final ApplicationRoleResource applicationRoleResource = new ApplicationRoleResource(
                service, helper, groupManager, converter, applicationRoleHasher);
        final Response response = applicationRoleResource.getAll();

        assertThat(response, noCache());
        assertThat(response, status(FORBIDDEN));
        assertThat(response, errorBody(error1));
    }

    @Test
    public void getRoleReturnsRoleWhenItExistsOnSuccess() {
        final MockApplicationRole role1 = createApplicationRole("id").groups(one, two).name("Name");
        createApplicationRole("idThree").name("Empty");

        final Response response = applicationRoleResource.get(role1.getKey().value());

        assertThat(response, noCache());
        assertThat(response, status(OK));
        assertThat(response, body(ApplicationRoleBean.class, fromRole(role1)));
    }

    @Test
    public void getRoleReturns404WhenItDoesNotExist() {
        createApplicationRole("id").groups(one, two).name("Name");
        createApplicationRole("idThree").name("Empty");

        final Response response = applicationRoleResource.get("whatIDontExist");

        assertThat(response, noCache());
        assertThat(response, status(NOT_FOUND));
        assertThat(response, errorBody(MockApplicationRoleAdminService.NOT_FOUND));
    }

    @Test
    public void getRoleReturns400WhenNoRolePassed() {
        createApplicationRole("id").groups(one, two).name("Name");
        createApplicationRole("idThree").name("Empty");

        final Response response = applicationRoleResource.get(null);

        assertBadRequest(response, "null");
    }

    @Test
    public void getRoleReturns400WhenBadRolePassed() {
        final String badKey = "illegal key";
        createApplicationRole("id").groups(one, two).name("Name");
        createApplicationRole("idThree").name("Empty");

        final Response response = applicationRoleResource.get(badKey);
        assertBadRequest(response, badKey);
    }

    @Test
    public void putRoleReturns400WhenNoRolePassed() {
        createApplicationRole("id").groups(one, two).name("Name");
        createApplicationRole("idThree").name("Empty");

        final Response response = applicationRoleResource.put(null, null, new ApplicationRoleBean());

        assertBadRequest(response, "null");
    }

    @Test
    public void putRoleReturns400WhenBadRolePassed() {
        final String key = "this is a bad key";

        createApplicationRole("id").groups(one, two).name("Name");
        createApplicationRole("idThree").name("Empty");

        final Response response = applicationRoleResource.put(key, null, new ApplicationRoleBean());

        assertBadRequest(response, key);
    }

    @Test
    public void putRoleReturns404WhenItDoesNotExist() {
        createApplicationRole("id").groups(one, two).name("Name");
        createApplicationRole("idThree").name("Empty");

        final ApplicationRoleBean applicationRoleBean = new ApplicationRoleBean();
        applicationRoleBean.setGroups(Collections.<String>emptySet());

        final Response response = applicationRoleResource.put("whatIDontExist", null, applicationRoleBean);

        assertThat(response, noCache());
        assertThat(response, status(NOT_FOUND));
        assertThat(response, errorBody(MockApplicationRoleAdminService.NOT_FOUND));
    }

    @Test
    public void putRoleUpdatesAndReturnsRoleWhenSuccessful() {
        final MockApplicationRole role1 = createApplicationRole("id").groups(one, two).name("Name");
        createApplicationRole("idThree").name("Empty");

        final ApplicationRoleBean applicationRoleBean = new ApplicationRoleBean();
        applicationRoleBean.setGroups(groupNames(three, four));

        final Response response = applicationRoleResource.put(role1.getKey().value(), null, applicationRoleBean);

        assertThat(response, noCache());
        assertThat(response, status(OK));
        assertThat(response, body(ApplicationRoleBean.class, fromRole(role1.copy().groups(three, four))));
    }

    @Test
    public void putRoleReturnsRoleWhenNoGroupsPassed() {
        final MockApplicationRole role1 = createApplicationRole("id")
                .groups(one, two)
                .name("Name")
                .defaultGroups(one)
                .copy();
        createApplicationRole("idThree").name("Empty");

        final ApplicationRoleBean applicationRoleBean = new ApplicationRoleBean();

        final Response response = applicationRoleResource.put(role1.getKey().value(), null, applicationRoleBean);

        assertThat(response, noCache());
        assertThat(response, status(OK));
        assertThat(response, body(ApplicationRoleBean.class, fromRole(role1.copy().groups().defaultGroups())));
    }

    @Test
    public void putRoleCanUpdateDefaultsToGroup() {
        final MockApplicationRole role1 = createApplicationRole("id")
                .groups(one, two, three)
                .name("Name")
                .defaultGroups(one, two)
                .copy();

        createApplicationRole("idThree").name("Empty");

        final ApplicationRoleBean applicationRoleBean = new ApplicationRoleBean();
        applicationRoleBean.setGroups(groupNames(one, two, three));
        applicationRoleBean.setDefaultGroups(groupNames(two, three));

        final Response response = applicationRoleResource.put(role1.getKey().value(), null, applicationRoleBean);

        assertThat(response, noCache());
        assertThat(response, status(OK));
        assertThat(response, body(ApplicationRoleBean.class,
                fromRole(role1.copy().defaultGroups(two, three))));
    }

    @Test
    public void putRoleCanUpdateDefaultToNone() {
        final MockApplicationRole role1 = createApplicationRole("id")
                .groups(one, two)
                .name("Name")
                .defaultGroups(one)
                .copy();

        createApplicationRole("idThree").name("Empty");

        final ApplicationRoleBean applicationRoleBean = new ApplicationRoleBean();
        applicationRoleBean.setGroups(groupNames(one, two));
        applicationRoleBean.setDefaultGroups(null);

        final Response response = applicationRoleResource.put(role1.getKey().value(), null, applicationRoleBean);

        final MockApplicationRole expectedRole = role1.copy().defaultGroups(Option.<Group>none());

        assertThat(response, noCache());
        assertThat(response, status(OK));
        assertThat(response, body(ApplicationRoleBean.class, fromRole(expectedRole)));
    }

    @Test
    public void putRoleCantSetInvalidDefaultGroups() {
        final MockApplicationRole role1 = createApplicationRole("id")
                .groups(one, two)
                .name("Name")
                .defaultGroups(one)
                .copy();

        createApplicationRole("idThree").name("Empty");

        final ApplicationRoleBean applicationRoleBean = new ApplicationRoleBean();
        applicationRoleBean.setGroups(groupNames(one));
        applicationRoleBean.setDefaultGroups(groupNames(three));

        final Response response = applicationRoleResource.put(role1.getKey().value(), null, applicationRoleBean);

        final String expectedMessage = NoopI18nHelper.makeTranslation("application.role.rest.default.groups.invalid",
                1, "three");

        assertThat(response, noCache());
        assertThat(response, status(BAD_REQUEST));
        assertThat(response, body(com.atlassian.jira.rest.api.util.ErrorCollection.class,
                new ErrorCollectionMatcher().addError(ApplicationRoleAdminService.ERROR_DEFAULT_GROUPS, expectedMessage)));
    }

    @Test
    public void putRoleCantWithNullGroups() {
        assertNullError(groups(one, null), groups(one), ApplicationRoleAdminService.ERROR_GROUPS, false);
    }

    @Test
    public void putRoleCantWithNullDefaultGroups() {
        assertNullError(groups(one, two), groups(one, null), ApplicationRoleAdminService.ERROR_DEFAULT_GROUPS, false);
    }

    @Test
    public void putFailsWhenInvalidGroupPassed() {
        final MockGroup badGroup = new MockGroup("badGroup");
        assertBadGroupRejected(groups(one, badGroup), groups(one),
                ApplicationRoleAdminService.ERROR_GROUPS, badGroup, false);
    }

    @Test
    public void putFailsWhenInvalidDefaultGroupPassed() {
        final MockGroup badGroup = new MockGroup("badGroup");
        assertBadGroupRejected(groups(one), groups(one, badGroup),
                ApplicationRoleAdminService.ERROR_DEFAULT_GROUPS, badGroup, false);
    }

    @Test
    public void putRoleReturns404WhenNoUpdateRequestedToBadRole() {
        createApplicationRole("id").groups(one, two).name("Name");
        createApplicationRole("idThree").name("Empty");

        final ApplicationRoleBean applicationRoleBean = new ApplicationRoleBean();

        final Response response = applicationRoleResource.put("what", null, applicationRoleBean);

        assertThat(response, noCache());
        assertThat(response, status(NOT_FOUND));
        assertThat(response, errorBody(MockApplicationRoleAdminService.NOT_FOUND));
    }

    @Test
    public void putReturns412WhenDifferentVersionHash() {
        createApplicationRole("id").groups(one, two).name("Name");
        createApplicationRole("idThree").name("Empty");

        final ApplicationRoleBean applicationRoleBean = new ApplicationRoleBean();
        final Response response = applicationRoleResource.put("id", "2", applicationRoleBean);

        assertThat(response, noCache());
        assertThat(response, status(PRECONDITION_FAILED));
    }

    @Test
    public void putBulkRoleReturns400WhenNoRolePassed() {
        createApplicationRole("id").groups(one, two).name("Name");
        createApplicationRole("idThree").name("Empty");

        final Response response = applicationRoleResource.putBulk(
                Lists.newArrayList(new ApplicationRoleBean()), "1");

        assertBadRequest(response, "null");
    }

    @Test
    public void putBulkRoleReturns400WhenBadRolePassed() {
        final String key = "this is a bad key";

        createApplicationRole("id").groups(one, two).name("Name");
        createApplicationRole("idThree").name("Empty");

        ApplicationRoleBean appRoleBean = new ApplicationRoleBean();
        appRoleBean.setKey(key);

        final Response response = applicationRoleResource.putBulk(Lists.newArrayList(appRoleBean), "1");

        assertBadRequest(response, key);
    }

    @Test
    public void putBulkRoleReturns404WhenItDoesNotExist() {
        createApplicationRole("id").groups(one, two).name("Name");
        createApplicationRole("idThree").name("Empty");

        final ApplicationRoleBean appRoleBean = new ApplicationRoleBean();
        appRoleBean.setGroups(Collections.<String>emptySet());
        appRoleBean.setKey("whatIDontExist");

        final Response response = applicationRoleResource.putBulk(Lists.newArrayList(appRoleBean), "1");

        assertThat(response, noCache());
        assertThat(response, status(NOT_FOUND));
        assertThat(response, errorBody(MockApplicationRoleAdminService.NOT_FOUND));
    }

    @Test
    public void putBulkReturns412WhenDifferentVersionHash() {
        createApplicationRole("id").groups(one, two).name("Name");
        createApplicationRole("idThree").name("Empty");

        final ApplicationRoleBean applicationRoleBean = new ApplicationRoleBean();

        final Response response = applicationRoleResource.putBulk(Lists.newArrayList(applicationRoleBean), "2");

        assertThat(response, noCache());
        assertThat(response, status(PRECONDITION_FAILED));
    }


    @Test
    public void webSudoEnabled() {
        final WebSudoRequired sudoRequired = ApplicationRoleResource.class.getAnnotation(WebSudoRequired.class);
        assertThat(sudoRequired, Matchers.notNullValue());
    }

    @Test
    public void xsrfProtectionThroughMediaTypes() {
        final Consumes consumes = ApplicationRoleResource.class.getAnnotation(Consumes.class);
        assertThat(consumes, Matchers.notNullValue());
        assertThat(Arrays.asList(consumes.value()), Matchers.contains(MediaType.APPLICATION_JSON));

        final Produces produces = ApplicationRoleResource.class.getAnnotation(Produces.class);
        assertThat(produces, Matchers.notNullValue());
        assertThat(Arrays.asList(produces.value()), Matchers.contains(MediaType.APPLICATION_JSON));
    }

    private void assertBadRequest(final Response response, final String name) {
        assertThat(response, noCache());
        assertThat(response, status(BAD_REQUEST));
        assertThat(response, errorBody(NoopI18nHelper.makeTranslation("application.role.rest.bad.key", name)));
    }

    private void assertNullError(final Set<Group> groups, final Set<Group> defaultGroups, final String errorKey,
                                 boolean versioned) {
        final MockApplicationRole role1 = service
                .addApplicationRole("id")
                .groups(one, two)
                .name("Name")
                .defaultGroups(one)
                .copy();

        final ApplicationRoleBean applicationRoleBean = new ApplicationRoleBean();
        applicationRoleBean.setGroups(groupNames(groups));
        applicationRoleBean.setDefaultGroups(groupNames(defaultGroups));

        final Response response;

        if (versioned) {
            response = applicationRoleResource.put(role1.getKey().value(), "1", applicationRoleBean);
        } else {
            response = applicationRoleResource.put(role1.getKey().value(), null, applicationRoleBean);
        }

        final String expectedMessage = NoopI18nHelper.makeTranslation("application.role.rest.groups.null");

        assertThat(response, noCache());
        assertThat(response, status(BAD_REQUEST));
        assertThat(response, body(com.atlassian.jira.rest.api.util.ErrorCollection.class,
                new ErrorCollectionMatcher().addError(errorKey, expectedMessage)));
    }

    private void assertBadGroupRejected(Set<Group> groups, Set<Group> defaultGroups, final String expectedKey,
                                        final Group badGroup, final boolean versioned) {
        final MockApplicationRole role1 = service
                .addApplicationRole("id")
                .groups(one, two)
                .name("Name")
                .defaultGroups(one)
                .copy();

        createApplicationRole("idThree").name("Empty");

        final ApplicationRoleBean applicationRoleBean = new ApplicationRoleBean();
        applicationRoleBean.setGroups(groupNames(groups));
        applicationRoleBean.setDefaultGroups(groupNames(defaultGroups));

        String versionHash = versioned ? "1" : null;
        final Response response = applicationRoleResource.put(role1.getKey().value(), versionHash, applicationRoleBean);

        final String expectedMessage = NoopI18nHelper.makeTranslation("application.role.rest.error.bad.group",
                badGroup.getName());

        assertThat(response, noCache());
        assertThat(response, status(BAD_REQUEST));
        assertThat(response, body(com.atlassian.jira.rest.api.util.ErrorCollection.class,
                new ErrorCollectionMatcher().addError(expectedKey, expectedMessage)));
    }

    private static ApplicationRoleBeanMatcher fromRole(ApplicationRole role) {
        return new ApplicationRoleBeanMatcher(role);
    }

    private static Set<Group> groups(Group... groups) {
        return Sets.newHashSet(groups);
    }

    private static Set<String> groupNames(Group... groups) {
        return groupNames(Arrays.asList(groups));
    }

    private static Set<String> groupNames(Collection<Group> groups) {
        Set<String> sets = Sets.newHashSet();
        for (Group group : groups) {
            if (group != null) {
                sets.add(group.getName());
            } else {
                sets.add(null);
            }
        }
        return Collections.unmodifiableSet(sets);
    }

    private MockApplicationRole createApplicationRole(String key) {
        return service.addApplicationRole(key);
    }

}