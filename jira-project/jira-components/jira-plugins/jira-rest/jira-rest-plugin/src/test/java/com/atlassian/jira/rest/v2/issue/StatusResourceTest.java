package com.atlassian.jira.rest.v2.issue;

import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.bc.config.ConstantsService;
import com.atlassian.jira.issue.fields.rest.json.beans.StatusJsonBean;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.rest.exception.NotFoundWebException;
import com.atlassian.jira.rest.util.StatusHelper;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.SimpleErrorCollection;
import org.apache.commons.httpclient.HttpStatus;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Unit test for StatusResource.
 *
 * @since v4.2
 */
public class StatusResourceTest {
    @Rule
    public InitMockitoMocks initMocks = new InitMockitoMocks(this);

    @Mock
    private JiraAuthenticationContext authContext;
    @Mock
    private ConstantsService constantsService;
    @Mock
    private StatusHelper statusHelper;
    @Mock
    private UriInfo uriInfo;
    @Mock
    private Request request;

    private StatusResource statusResource;
    private MockApplicationUser user;

    @Before
    public void setUp() throws Exception {
        user = new MockApplicationUser("mockUser");
        when(authContext.getUser()).thenReturn(user);
        statusResource = new StatusResource(authContext, constantsService, statusHelper);
    }

    @Test
    public void testStatusFound() throws Exception {
        final StatusJsonBean expectedJsonResponse = mock(StatusJsonBean.class);
        final String statusName = "superstatus";

        final Status status = mock(Status.class);

        when(constantsService.getStatusById(user, statusName)).thenReturn(ServiceOutcomeImpl.ok(status));
        when(statusHelper.createStatusBean(status, uriInfo, StatusResource.class)).thenReturn(expectedJsonResponse);

        final Response resp = statusResource.getStatus(statusName, request, uriInfo);

        assertEquals(HttpStatus.SC_OK, resp.getStatus());
        assertSame(expectedJsonResponse, resp.getEntity());
    }

    @Test(expected = NotFoundWebException.class)
    public void testStatusNotFound() throws Exception {
        final String statusName = "superstatus";

        final SimpleErrorCollection errors = new SimpleErrorCollection();
        errors.addError("somefield", "somemessage");

        when(constantsService.getStatusById(user, statusName)).thenReturn(new ServiceOutcomeImpl<Status>(errors, null));
        when(constantsService.getStatusByTranslatedName(user, statusName)).thenReturn(new ServiceOutcomeImpl<Status>(errors, null));

        statusResource.getStatus(statusName, request, uriInfo);
    }

}
