package com.atlassian.jira.rest.v2.issue;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.fields.MockOrderableField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.fields.screen.FieldScreenRenderLayoutItem;
import com.atlassian.jira.issue.fields.screen.FieldScreenRenderTab;
import com.atlassian.jira.issue.fields.screen.FieldScreenRenderer;
import com.atlassian.jira.issue.fields.screen.FieldScreenRendererFactory;
import com.atlassian.jira.rest.v2.issue.context.ContextUriInfo;
import com.atlassian.jira.rest.v2.issue.version.VersionBeanFactory;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.opensymphony.workflow.loader.ActionDescriptor;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;
import java.util.Map;

import static com.atlassian.jira.issue.IssueFieldConstants.ASSIGNEE;
import static com.atlassian.jira.issue.IssueFieldConstants.RESOLUTION;
import static com.atlassian.jira.matchers.ReflectionMatchers.propertiesMatcher;
import static com.google.common.collect.ImmutableList.of;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class TransitionMetaFieldBeanBuilderTest {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();
    @Mock
    private FieldScreenRendererFactory fieldScreenRendererFactory;
    @Mock
    private FieldLayoutManager fieldLayoutManager;
    @Mock
    private ActionDescriptor action;
    @Mock
    private Issue issue;
    @Mock
    private ApplicationUser user;
    @Mock
    private VersionBeanFactory versionBeanFactory;
    @Mock
    private VelocityRequestContextFactory velocityRequestContextFactory;
    @Mock
    private ContextUriInfo contextUriInfo;
    @Mock
    private JiraBaseUrls baseUrls;
    @Mock
    private FieldScreenRenderer fieldScreenRenderer;

    private TransitionMetaFieldBeanBuilder metaFieldBeanBuilder;

    @Before
    public void before() {
        when(fieldScreenRendererFactory.getFieldScreenRenderer(user, issue, action)).thenReturn(fieldScreenRenderer);
        metaFieldBeanBuilder = new TransitionMetaFieldBeanBuilder(fieldScreenRendererFactory, fieldLayoutManager,
                action, issue, user, versionBeanFactory, velocityRequestContextFactory, contextUriInfo, baseUrls);
    }

    @Test
    public void shouldBuildEmpty() throws Exception {
        when(fieldScreenRenderer.getFieldScreenRenderTabs()).thenReturn(Collections.emptyList());

        final Map<String, FieldMetaBean> fields = metaFieldBeanBuilder.build();

        assertThat("There must be no fields if there are not tabs", fields.keySet(), Matchers.empty());

    }

    @Test
    public void shouldMakeResolutionRequired() throws Exception {
        final ImmutableList<FieldScreenRenderTab> fieldScreenRenderTabs = of(getFieldsTab(RESOLUTION, false, true));
        when(fieldScreenRenderer.getFieldScreenRenderTabs()).thenReturn(fieldScreenRenderTabs);

        final Map<String, FieldMetaBean> fields = metaFieldBeanBuilder.build();

        assertThat("There should be one field", fields.keySet(), contains(RESOLUTION));
        assertThat("Resolution must be required", fields.values(), contains(propertiesMatcher(ImmutableMap.of("required", equalTo(true)))));

    }

    @Test
    public void shouldNotMakeOtherFieldsRequired() throws Exception {
        final ImmutableList<FieldScreenRenderTab> fieldScreenRenderTabs = of(getFieldsTab(ASSIGNEE, false, true));
        when(fieldScreenRenderer.getFieldScreenRenderTabs()).thenReturn(fieldScreenRenderTabs);

        final Map<String, FieldMetaBean> fields = metaFieldBeanBuilder.build();

        assertThat("There should be one field", fields.keySet(), contains(ASSIGNEE));
        assertThat("Field should not be required", fields.values(), contains(propertiesMatcher(ImmutableMap.of("required", equalTo(false)))));

    }

    @Test
    public void shouldNotIncludeNotVisibleItems() throws Exception {
        final ImmutableList<FieldScreenRenderTab> fieldScreenRenderTabs = of(
                getFieldsTab("field1", true, true),
                getFieldsTab("field2", false, false));
        when(fieldScreenRenderer.getFieldScreenRenderTabs()).thenReturn(fieldScreenRenderTabs);

        final Map<String, FieldMetaBean> fields = metaFieldBeanBuilder.build();

        assertThat("There should be one field", fields.keySet(), contains("field1"));
        assertThat("", fields.values(), contains(propertiesMatcher(
                ImmutableMap.of(
                "required", equalTo(true),
                "name", equalTo("field1_name")))));

    }

    private FieldScreenRenderTab getFieldsTab(String fieldId, boolean required, boolean isShow) {
        FieldScreenRenderTab tab = mock(FieldScreenRenderTab.class);
        FieldScreenRenderLayoutItem fieldScreenRenderLayoutItem = mock(FieldScreenRenderLayoutItem.class);
        when(fieldScreenRenderLayoutItem.isShow(issue)).thenReturn(isShow);
        FieldLayoutItem fieldLayoutItem = mock(FieldLayoutItem.class);
        when(fieldLayoutItem.isRequired()).thenReturn(required);
        when(fieldScreenRenderLayoutItem.getFieldLayoutItem()).thenReturn(fieldLayoutItem);

        when(tab.getFieldScreenRenderLayoutItemsForProcessing()).thenReturn(of(fieldScreenRenderLayoutItem));

        when(fieldScreenRenderLayoutItem.getOrderableField()).thenReturn(new MockOrderableField(fieldId, fieldId + "_name"));
        return tab;
    }

}