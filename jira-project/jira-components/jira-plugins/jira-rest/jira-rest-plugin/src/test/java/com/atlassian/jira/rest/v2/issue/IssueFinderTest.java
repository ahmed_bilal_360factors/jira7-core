package com.atlassian.jira.rest.v2.issue;

import com.atlassian.jira.event.mau.MauEventService;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.util.ErrorCollection;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class IssueFinderTest {
    private static final String ISSUE_KEY = "TEST-1";

    @Rule
    public final RuleChain mockitoMocks = MockitoMocksInContainer.forTest(this);
    
    @Mock 
    @AvailableInContainer
    private MauEventService mauEventService;
    @Mock
    private com.atlassian.jira.issue.fields.rest.IssueFinder finder;
    @Mock
    private MutableIssue issue;
    @Mock
    private Project project;
    
    @InjectMocks
    private IssueFinder issueFinder;
    

    @Before
    public void setUp() throws Exception {
        when(finder.findIssue(eq(ISSUE_KEY), any(ErrorCollection.class))).thenReturn(issue);
        when(issue.getProjectObject()).thenReturn(project);
    }

    @Test
    public void testGetIssueCallsMauService() throws Exception {
        issueFinder.getIssueObject(ISSUE_KEY);

        verify(mauEventService, times(1)).setApplicationForThreadBasedOnProject(project);
    }
}