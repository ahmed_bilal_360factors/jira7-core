package com.atlassian.jira.rest.v2.issue.project;

import com.google.common.collect.Lists;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Collection;

/**
 * @since v7.0
 */
public class ActorInputBean {
    public static ActorInputBean DOC_EXAMPLE;

    static {
        DOC_EXAMPLE = new ActorInputBean();
        DOC_EXAMPLE.setGroupnames(null);
        DOC_EXAMPLE.setUsernames(Lists.newArrayList("admin"));
    }

    @JsonProperty("user")
    private Collection<String> usernames;

    @JsonProperty("group")
    private Collection<String> groupnames;

    public ActorInputBean() {
    }

    public Collection<String> getGroupnames() {
        return groupnames;
    }

    public Collection<String> getUsernames() {
        return usernames;
    }

    public void setGroupnames(final Collection<String> groupnames) {
        this.groupnames = groupnames;
    }

    public void setUsernames(final Collection<String> usernames) {
        this.usernames = usernames;
    }
}
