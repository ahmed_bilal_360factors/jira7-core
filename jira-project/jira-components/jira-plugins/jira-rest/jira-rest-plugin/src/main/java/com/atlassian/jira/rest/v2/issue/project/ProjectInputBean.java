package com.atlassian.jira.rest.v2.issue.project;


import com.google.common.base.Objects;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @since v7.0
 */
public class ProjectInputBean {
    public static final ProjectInputBean CREATE_EXAMPLE = builder().setName("Example")
            .setKey("EX")
            .setDescription("Example Project description")
            .setLeadName("Charlie")
            .setUrl("http://atlassian.com")
            .setAssigneeType(ProjectBean.AssigneeType.PROJECT_LEAD)
            .setAvatarId(10200L)
            .setIssueSecurityScheme(10001L)
            .setPermissionScheme(10011L)
            .setNotificationScheme(10021L)
            .setCategoryId(10120L)
            .setProjectTypeKey("business")
            .setProjectTemplateKey("com.atlassian.jira-core-project-templates:jira-core-project-management")
            .build();

    @JsonProperty
    private String key;
    @JsonProperty
    private String name;
    @JsonProperty
    private String projectTypeKey;
    @JsonProperty
    private String projectTemplateKey;
    @JsonProperty
    private String description;
    @JsonProperty
    private String lead;
    @JsonProperty
    private String url;
    @JsonProperty
    private ProjectBean.AssigneeType assigneeType;
    @JsonProperty
    private Long avatarId;
    @JsonProperty
    private Long issueSecurityScheme;
    @JsonProperty
    private Long permissionScheme;
    @JsonProperty
    private Long notificationScheme;
    @JsonProperty
    private Long categoryId;

    public ProjectInputBean(
            final String key,
            final String name,
            final String projectTypeKey,
            final String projectTemplateKey,
            final String description,
            final String lead,
            final String url,
            final ProjectBean.AssigneeType assigneeType,
            final Long avatarId,
            final Long issueSecurityScheme,
            final Long permissionScheme,
            final Long notificationScheme,
            final Long categoryId) {
        this.key = key;
        this.name = name;
        this.projectTypeKey = projectTypeKey;
        this.projectTemplateKey = projectTemplateKey;
        this.description = description;
        this.lead = lead;
        this.url = url;
        this.assigneeType = assigneeType;
        this.avatarId = avatarId;
        this.issueSecurityScheme = issueSecurityScheme;
        this.permissionScheme = permissionScheme;
        this.notificationScheme = notificationScheme;
        this.categoryId = categoryId;
    }

    public ProjectInputBean() {
    }

    public String getKey() {
        return key;
    }

    public String getName() {
        return name;
    }

    public String getProjectTypeKey() {
        return projectTypeKey;
    }

    public String getProjectTemplateKey() {
        return projectTemplateKey;
    }

    public String getDescription() {
        return description;
    }

    public String getLead() {
        return lead;
    }

    public String getUrl() {
        return url;
    }

    public ProjectBean.AssigneeType getAssigneeType() {
        return Objects.firstNonNull(assigneeType, ProjectBean.AssigneeType.PROJECT_LEAD);
    }

    @JsonIgnore
    public Long getAssigneeTypeOrNull() {
        return assigneeType != null ? assigneeType.getId() : null;
    }

    public Long getAvatarId() {
        return avatarId;
    }

    public Long getIssueSecurityScheme() {
        return issueSecurityScheme;
    }

    public Long getPermissionScheme() {
        return permissionScheme;
    }

    public Long getNotificationScheme() {
        return notificationScheme;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private String key;
        private String name;
        private String description;
        private String leadName;
        private String url;
        private ProjectBean.AssigneeType defaultAssignee;
        private Long avatarId;
        private Long issueSecurityScheme;
        private Long permissionScheme;
        private Long notificationScheme;
        private Long categoryId;
        private String projectTypeKey;
        private String projectTemplateKey;

        public Builder setKey(final String key) {
            this.key = key;
            return this;
        }

        public Builder setName(final String name) {
            this.name = name;
            return this;
        }

        public Builder setDescription(final String description) {
            this.description = description;
            return this;
        }

        public Builder setLeadName(final String leadName) {
            this.leadName = leadName;
            return this;
        }

        public Builder setUrl(final String url) {
            this.url = url;
            return this;
        }

        public Builder setAssigneeType(final ProjectBean.AssigneeType defaultAssignee) {
            this.defaultAssignee = defaultAssignee;
            return this;
        }

        public Builder setAvatarId(final Long avatarId) {
            this.avatarId = avatarId;
            return this;
        }

        public Builder setIssueSecurityScheme(final Long issueSecurityScheme) {
            this.issueSecurityScheme = issueSecurityScheme;
            return this;
        }

        public Builder setPermissionScheme(final Long permissionScheme) {
            this.permissionScheme = permissionScheme;
            return this;
        }

        public Builder setNotificationScheme(final Long notificationScheme) {
            this.notificationScheme = notificationScheme;
            return this;
        }

        public Builder setCategoryId(final Long categoryId) {
            this.categoryId = categoryId;
            return this;
        }

        public Builder setProjectTypeKey(final String projectTypeKey) {
            this.projectTypeKey = projectTypeKey;
            return this;
        }

        public Builder setProjectTemplateKey(final String projectTemplateKey) {
            this.projectTemplateKey = projectTemplateKey;
            return this;
        }

        public ProjectInputBean build() {
            return new ProjectInputBean(
                    key,
                    name,
                    projectTypeKey,
                    projectTemplateKey,
                    description,
                    leadName,
                    url,
                    defaultAssignee,
                    avatarId,
                    issueSecurityScheme,
                    permissionScheme,
                    notificationScheme,
                    categoryId
            );
        }
    }
}

