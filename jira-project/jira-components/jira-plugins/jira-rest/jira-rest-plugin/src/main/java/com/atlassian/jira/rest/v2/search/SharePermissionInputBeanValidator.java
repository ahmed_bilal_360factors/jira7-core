package com.atlassian.jira.rest.v2.search;

import com.atlassian.gzipfilter.org.apache.commons.lang.StringUtils;
import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.bc.ServiceResultImpl;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Validates a share permission bean used when adding a new share to a filter
 */
@Component
public class SharePermissionInputBeanValidator {
    public ServiceResult validateAddPermissionBean(SharePermissionInputBean inputBean, I18nHelper i18nHelper) {
        ErrorCollection errorCollection = new SimpleErrorCollection();

        String typeString = inputBean.getType();
        Optional<SharePermissionInputBean.Type> maybeType = SharePermissionInputBean.Type.fromValue((typeString));
        if (!maybeType.isPresent()) {
            errorCollection.addErrorMessage(i18nHelper.getText("rest.filters.add.permission.invalid.type", sharePermissionTypesString()));
        } else {
            SharePermissionInputBean.Type type = maybeType.get();
            switch (type) {
                case PROJECT:
                    if (inputBean.getProjectId() == null) {
                        errorCollection.addErrorMessage(i18nHelper.getText("rest.filters.add.permission.no.project.id.given"));
                    }
                    break;
                case GROUP:
                    if (inputBean.getGroupname() == null) {
                        errorCollection.addErrorMessage(i18nHelper.getText("rest.filters.add.permission.no.group.name.given"));
                    }
                    break;
                case PROJECT_ROLE:
                    if (inputBean.getProjectRoleId() == null) {
                        errorCollection.addErrorMessage(i18nHelper.getText("rest.filters.add.permission.no.project.role.id.given"));
                    }
                    break;
            }
        }
        return new ServiceResultImpl(errorCollection);
    }

    private static String sharePermissionTypesString() {
        return "[" + StringUtils.join(SharePermissionInputBean.Type.values(), ", ") + "]";
    }
}
