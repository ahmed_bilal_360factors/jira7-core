package com.atlassian.jira.rest.v2.issue;

import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueInputParameters;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.rest.api.util.ErrorCollection;
import com.atlassian.jira.rest.exception.BadRequestWebException;
import com.atlassian.jira.rest.exception.ForbiddenWebException;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;

import javax.ws.rs.core.Response;

import static javax.ws.rs.core.Response.Status.NO_CONTENT;
import static javax.ws.rs.core.Response.status;

/**
 * Implements the "Edit Issue" use case.
 *
 * @since v5.0
 */
public class UpdateIssueResource {
    private final IssueInputParametersAssembler issueInputParametersAssembler;
    private final IssueService issueService;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final GlobalPermissionManager globalPermissionManager;
    private final PermissionManager permissionManager;
    private final I18nHelper i18n;

    public UpdateIssueResource(
            IssueInputParametersAssembler issueInputParametersAssembler,
            IssueService issueService,
            JiraAuthenticationContext jiraAuthenticationContext,
            GlobalPermissionManager globalPermissionManager,
            PermissionManager permissionManager,
            I18nHelper i18n) {
        this.issueInputParametersAssembler = issueInputParametersAssembler;
        this.issueService = issueService;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.globalPermissionManager = globalPermissionManager;
        this.permissionManager = permissionManager;
        this.i18n = i18n;
    }

    public Response editIssue(Issue issue, IssueUpdateBean updateRequest, boolean notifyUsers) {
        ApplicationUser user = jiraAuthenticationContext.getLoggedInUser();

        if (!notifyUsers && !canDisableUserNotifications(user, issue)) {
            throw new ForbiddenWebException(ErrorCollection.of(i18n.getText("update.issue.resource.not.enough.permissions.for.discarding.user.notification")));
        }

        IssueInputParametersAssembler.Result result = issueInputParametersAssembler.makeUpdateAssembler(updateRequest, issue);
        if (result.getErrors().hasAnyErrors()) {
            throw new BadRequestWebException(ErrorCollection.of(result.getErrors()));
        }

        IssueInputParameters inputParameters = result.getParameters();

        IssueService.UpdateValidationResult validation = issueService.validateUpdate(user, issue.getId(), inputParameters);
        if (!validation.isValid()) {
            throw new BadRequestWebException(ErrorCollection.of(validation.getErrorCollection()));
        }

        IssueService.IssueResult issueResult = issueService.update(user, validation, EventDispatchOption.ISSUE_UPDATED, notifyUsers);
        if (!issueResult.isValid()) {
            throw new BadRequestWebException(ErrorCollection.of(issueResult.getErrorCollection()));
        }

        return status(NO_CONTENT).build();
    }

    public Response transitionIssue(Issue issue, IssueUpdateBean issueUpdateBean) {
        IssueInputParametersAssembler.Result result = issueInputParametersAssembler.makeTransitionAssember(issueUpdateBean, issue);
        if (result.getErrors().hasAnyErrors()) {
            throw new BadRequestWebException(ErrorCollection.of(result.getErrors()));
        }

        IssueInputParameters inputParameters = result.getParameters();
        ApplicationUser user = jiraAuthenticationContext.getUser();

        final IssueService.TransitionValidationResult validationResult = issueService.validateTransition(user, issue.getId(), Integer.valueOf(issueUpdateBean.getTransition().getId()), inputParameters);

        if (!validationResult.isValid()) {
            throw new BadRequestWebException(ErrorCollection.of(validationResult.getErrorCollection()));
        } else {
            IssueService.IssueResult transitionResult = issueService.transition(user, validationResult);
            if (!transitionResult.isValid()) {
                throw new BadRequestWebException(ErrorCollection.of(transitionResult.getErrorCollection()));
            }

            return status(NO_CONTENT).build();
        }
    }

    /**
     * Determines if the current user can disable user notifications for the edit issue operation.
     * Only admins and project admins can disable user notifications.
     */
    public boolean canDisableUserNotifications(ApplicationUser user, Issue issue) {
        return globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user) ||
                permissionManager.hasPermission(ProjectPermissions.ADMINISTER_PROJECTS, issue, user);

    }
}
