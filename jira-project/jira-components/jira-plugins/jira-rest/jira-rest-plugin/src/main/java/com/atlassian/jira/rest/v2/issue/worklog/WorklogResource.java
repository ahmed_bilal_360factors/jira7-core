package com.atlassian.jira.rest.v2.issue.worklog;

import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.issue.worklog.DeletedWorklog;
import com.atlassian.jira.bc.issue.worklog.WorklogChangedSincePage;
import com.atlassian.jira.bc.issue.worklog.WorklogService;
import com.atlassian.jira.entity.WithId;
import com.atlassian.jira.issue.fields.rest.json.WorklogBeanFactory;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.fields.rest.json.beans.WorklogJsonBean;
import com.atlassian.jira.issue.worklog.Worklog;
import com.atlassian.jira.rest.util.ResponseFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.rest.annotation.ResponseType;

import javax.annotation.Nullable;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.google.common.collect.Iterables.getLast;

@Path("worklog")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
public class WorklogResource {
    private final ResponseFactory responseFactory;
    private final WorklogService worklogService;
    private final JiraAuthenticationContext authenticationContext;
    private final WorklogBeanFactory worklogBeanFactory;
    private final JiraBaseUrls jiraBaseUrls;

    public WorklogResource(final ResponseFactory responseFactory,
                           final WorklogService worklogService,
                           final JiraAuthenticationContext authenticationContext,
                           final WorklogBeanFactory worklogBeanFactory,
                           final JiraBaseUrls jiraBaseUrls) {
        this.responseFactory = responseFactory;
        this.worklogService = worklogService;
        this.authenticationContext = authenticationContext;
        this.worklogBeanFactory = worklogBeanFactory;
        this.jiraBaseUrls = jiraBaseUrls;
    }

    /**
     * Returns worklogs id and update time of worklogs that was updated since given time.
     * The returns set of worklogs is limited to 1000 elements.
     * This API will not return worklogs updated during last minute.
     *
     * @param since a date time in unix timestamp format since when updated worklogs will be returned.
     * @return a set of worklogs id and update time.
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc Returns a JSON representation of the worklog changes.
     * @response.representation.200.example {@link WorklogChangedSinceBean#UPDATED_EXAMPLE}
     */
    @GET
    @Path("updated")
    @ResponseType(WorklogChangedSinceBean.class)
    public Response getIdsOfWorklogsModifiedSince(@QueryParam("since") @DefaultValue("0") Long since) {
        final ApplicationUser user = authenticationContext.getLoggedInUser();
        WorklogChangedSincePage<Worklog> worklogsUpdatedSince = worklogService.getWorklogsUpdatedSince(user, since);

        WorklogChangedSinceBean worklogChangedSinceBean = createUpdatedSinceBean(worklogsUpdatedSince);
        return responseFactory.okNoCache(worklogChangedSinceBean);
    }

    /**
     * Returns worklogs id and delete time of worklogs that was deleted since given time.
     * The returns set of worklogs is limited to 1000 elements.
     * This API will not return worklogs deleted during last minute.
     *
     * @param since a date time in unix timestamp format since when deleted worklogs will be returned.
     * @return a set of worklogs id and delete time.
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc Returns a JSON representation of the worklog changes.
     * @response.representation.200.example {@link WorklogChangedSinceBean#DELETED_EXAMPLE}
     */
    @GET
    @Path("deleted")
    @ResponseType(WorklogChangedSinceBean.class)
    public Response getIdsOfWorklogsDeletedSince(@QueryParam("since") @DefaultValue("0") Long since) {
        final WorklogChangedSincePage<DeletedWorklog> worklogsDeletedSince =
                worklogService.getWorklogsDeletedSince(authenticationContext.getLoggedInUser(), since);

        WorklogChangedSinceBean worklogChangedSinceBean = createDeletedSinceBean(worklogsDeletedSince);
        return responseFactory.okNoCache(worklogChangedSinceBean);
    }

    /**
     * Returns worklogs for given worklog ids. Only worklogs to which the calling user has permissions, will be included in the result.
     * The returns set of worklogs is limited to 1000 elements.
     *
     * @param request a JSON object containing ids of worklogs to return
     * @return a set of worklogs for given ids.
     * @request.representation.qname worklogIdsRequest
     * @request.representation.example {@link WorklogIdsRequestBean#EXAMPLE}
     * @response.representation.200.qname worklogs
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc Returns a JSON representation of the search results.
     * @response.representation.200.example {@link WorklogJsonBean#DOC_EXAMPLE}
     * @response.representation.400.doc Returned if the request contains more than 1000 ids or is null
     */
    @POST
    @Path("list")
    @ResponseType(value = List.class, genericTypes = WorklogJsonBean.class)
    public Response getWorklogsForIds(final WorklogIdsRequestBean request) {
        final ApplicationUser loggedInUser = authenticationContext.getLoggedInUser();
        final JiraServiceContextImpl serviceContext = new JiraServiceContextImpl(loggedInUser, new SimpleErrorCollection(), authenticationContext.getI18nHelper());

        if (request.getIds() == null) {
            return responseFactory.badRequest("worklog.rest.error.null.set");
        }

        final ServiceOutcome<Set<Worklog>> worklogsForIds = worklogService.getWorklogsForIds(serviceContext, request.getIds());

        return responseFactory.validateOutcome(worklogsForIds).left().on(new com.google.common.base.Function<Set<Worklog>, Response>() {
            @Nullable
            @Override
            public Response apply(final Set<Worklog> worklogs) {
                return responseFactory.okNoCache(worklogsForIds.get()
                        .stream()
                        .map(new Function<Worklog, WorklogJsonBean>() {
                            @Override
                            public WorklogJsonBean apply(final Worklog worklog) {
                                return worklogBeanFactory.createBean(worklog, loggedInUser);
                            }
                        })
                        .collect(Collectors.toSet()));
            }
        });
    }

    private WorklogChangedSinceBean createUpdatedSinceBean(final WorklogChangedSincePage<Worklog> updatedWorklogs) {
        return createChangeSinceBean("updated", updatedWorklogs, new Function<Worklog, Long>() {
            @Override
            public Long apply(final Worklog worklog) {
                return worklog.getUpdated().getTime();
            }
        });
    }

    private WorklogChangedSinceBean createDeletedSinceBean(final WorklogChangedSincePage<DeletedWorklog> worklogsDeletedSince) {
        return createChangeSinceBean("deleted", worklogsDeletedSince, new Function<DeletedWorklog, Long>() {
            @Override
            public Long apply(final DeletedWorklog deletedWorklog) {
                return deletedWorklog.getDeletionTime().getTime();
            }
        });
    }

    private <T extends WithId> WorklogChangedSinceBean createChangeSinceBean(final String restApiPath,
                                                                             final WorklogChangedSincePage<T> changedSincePage,
                                                                             final Function<T, Long> timeMappingFunction) {
        final Long since = changedSincePage.getSince();
        final Long until = changedSincePage.getChangedSince().isEmpty() ?
                changedSincePage.getSince() : timeMappingFunction.apply(getLast(changedSincePage.getChangedSince()));

        final List<WorklogChangeBean> changeBeans = changedSincePage.getChangedSince()
                .stream()
                .map(new Function<T, WorklogChangeBean>() {

                    @Override
                    public WorklogChangeBean apply(final T t) {
                        return new WorklogChangeBean(t.getId(), timeMappingFunction.apply(t));
                    }
                })
                .collect(Collectors.<WorklogChangeBean>toList());

        final WorklogChangedSinceBean.Builder builder = WorklogChangedSinceBean.builder()
                .setValues(changeBeans)
                .setSince(since)
                .setUntil(until)
                .setIsLastPage(changedSincePage.isLastPage())
                .setSelf(getSelf(restApiPath, since));

        if (!changedSincePage.isLastPage()) {
            return builder.setNextPage(getSelf(restApiPath, until)).build();
        } else {
            return builder.build();
        }
    }

    private URI getSelf(final String restApiPath, final Long since) {
        return UriBuilder.fromPath(jiraBaseUrls.restApi2BaseUrl())
                .path(WorklogResource.class)
                .path(restApiPath)
                .queryParam("since", since)
                .build();
    }
}
