package com.atlassian.jira.rest.v2.issue.context;

import javax.ws.rs.core.UriInfo;

/**
 * Scoped instances of UriInfo that implement this interface are scoped to a REST method invocation.
 *
 * @see javax.ws.rs.core.UriInfo
 * @since v4.2
 */
public interface ContextUriInfo extends UriInfo {
    // empty
}
