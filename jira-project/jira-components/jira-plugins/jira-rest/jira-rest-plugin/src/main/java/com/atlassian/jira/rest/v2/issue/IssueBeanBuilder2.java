package com.atlassian.jira.rest.v2.issue;

import com.atlassian.fugue.ImmutableMaps;
import com.atlassian.fugue.Iterables;
import com.atlassian.fugue.Option;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.customfields.impl.AbstractCustomFieldType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldException;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.NavigableField;
import com.atlassian.jira.issue.fields.OrderableField;
import com.atlassian.jira.issue.fields.ProjectSystemField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayout;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import com.atlassian.jira.issue.fields.rest.FieldJsonRepresentation;
import com.atlassian.jira.issue.fields.rest.RestAwareField;
import com.atlassian.jira.issue.fields.rest.json.JsonData;
import com.atlassian.jira.issue.fields.rest.json.beans.IssueLinksBeanBuilderFactory;
import com.atlassian.jira.issue.fields.rest.json.beans.IssueRefJsonBean;
import com.atlassian.jira.plugin.customfield.CustomFieldRestSerializer;
import com.atlassian.jira.plugin.customfield.CustomFieldTypeModuleDescriptor;
import com.atlassian.jira.rest.api.issue.JsonTypeBean;
import com.atlassian.jira.rest.v2.issue.builder.BeanBuilderFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.jira.workflow.IssueWorkflowManager;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.opensymphony.workflow.loader.ActionDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.transform;

/**
 * Builder for {@link IssueBean} instances.
 *
 * @since v5.0
 */
public class IssueBeanBuilder2 {
    public static final Integer FIRST_REPRESENTATION_VERSION = 1;
    private static final Logger LOG = LoggerFactory.getLogger(IssueBeanBuilder2.class);

    private final FieldLayoutManager fieldLayoutManager;
    private final JiraAuthenticationContext authContext;
    private final FieldManager fieldManager;
    private final ResourceUriBuilder resourceUriBuilder;
    private final BeanBuilderFactory beanBuilderFactory;
    private final IssueLinksBeanBuilderFactory issueLinkBeanBuilderFactory;
    private final IssueWorkflowManager issueWorkflowManager;
    private final Predicate<Field> fieldIncluded;
    private final Cache<Long, IssueRefJsonBean> issueRefsCache = CacheBuilder.newBuilder().build();

    // do not use CacheManager/CacheFactory to create his cache - we want it to be private
    private final LoadingCache<Field, String> fieldNames = CacheBuilder.newBuilder().build(new CacheLoader<Field, String>() {
        @Override
        public String load(@Nonnull final Field field) throws Exception {
            return field.getName();
        }
    });
    /**
     * The UriInfo to use when generating links.
     */
    private final UriBuilder uriBuilder;
    /**
     * the expand query string
     */
    private final String expand;
    private Supplier<String> parentLinkName = Suppliers.memoize(new Supplier<String>() {
        @Override
        public String get() {
            return authContext.getI18nHelper().getText("issue.field.parent");
        }
    });
    private Supplier<List<NavigableField>> includedNavigableFields = Suppliers.memoize(new Supplier<List<NavigableField>>() {
        @Override
        public List<NavigableField> get() {
            try {
                @SuppressWarnings("deprecation")
                final Set<NavigableField> fields = fieldManager.getAvailableNavigableFields(authContext.getLoggedInUser());
                return ImmutableList.copyOf(filter(fields, fieldIncluded));
            } catch (FieldException e) {
                // ignored...display as much as we can.
                return Collections.emptyList();
            }
        }
    });
    /**
     * The list of fields to include in the bean. If null, include all fields.
     */
    private IncludedFields fieldsToInclude;

    public IssueBeanBuilder2(
            final FieldLayoutManager fieldLayoutManager,
            final JiraAuthenticationContext authContext,
            final FieldManager fieldManager,
            final ResourceUriBuilder resourceUriBuilder,
            final BeanBuilderFactory beanBuilderFactory,
            final IncludedFields fieldsToInclude,
            final IssueLinksBeanBuilderFactory issueLinkBeanBuilderFactory,
            final IssueWorkflowManager issueWorkflowManager,
            final UriBuilder uriBuilder, final String expand) {
        this.fieldLayoutManager = fieldLayoutManager;
        this.authContext = authContext;
        this.fieldManager = fieldManager;
        this.resourceUriBuilder = resourceUriBuilder;
        this.beanBuilderFactory = beanBuilderFactory;
        this.fieldsToInclude = fieldsToInclude;
        this.issueLinkBeanBuilderFactory = issueLinkBeanBuilderFactory;
        this.issueWorkflowManager = issueWorkflowManager;
        this.uriBuilder = uriBuilder;
        this.expand = expand;
        this.fieldIncluded = fieldsToInclude == null ? Predicates.<Field>alwaysTrue() : new Predicate<Field>() {
            @Override
            public boolean apply(final Field field) {
                return fieldsToInclude.included(field);
            }
        };
    }

    public IssueBean build(final Issue issue) {
        final Map<String, FieldData> fieldData = createFieldsData(issue);
        final URI selfUri = resourceUriBuilder.build(uriBuilder.clone(), IssueResource.class, String.valueOf(issue.getId()));

        final IssueBean bean = new IssueBean(issue.getId(), issue.getKey(), selfUri);
        bean.setFieldsToInclude(fieldsToInclude);

        Option<IssueRefJsonBean> parentLink = createParentLink(issue);

        if (isExpanded("versionedRepresentations")) {
            bean.addVersionedRepresentations(createVersionedRepresentations(fieldData, issue));
        } else {
            bean.addFields(createFields(fieldData));
        }

        if (parentLink.isDefined()) {
            bean.addParentField(parentLink.get(), parentLinkName.get());
        }

        if (isExpanded("renderedFields")) {
            bean.addRenderedFields(createRenderableFields(fieldData));
        }

        if (isExpanded("names")) {
            bean.addNames(createFieldNames(fieldData));
        }

        if (isExpanded("schema")) {
            bean.addSchema(createFieldSchema(fieldData));
        }

        if (isExpanded("transitions")) {
            bean.addTransitionBeans(createTransitionBeans(issue));
        }

        if (isExpanded("operations")) {
            bean.setOperations(beanBuilderFactory.newOpsbarBeanBuilder(issue).build());
        }

        if (isExpanded("editmeta")) {
            bean.setEditMeta(beanBuilderFactory.newEditMetaBeanBuilder()
                    .issue(issue)
                    .fieldsToInclude(fieldsToInclude)
                    .build());
        }

        if (isExpanded("changelog")) {
            bean.setChangelog(beanBuilderFactory.newChangelogBeanBuilder().build(issue));
        }

        return bean;
    }

    private Map<String, FieldData> createFieldsData(final Issue issue) {
        final boolean renderedFields = isExpanded("renderedFields");
        final Iterable<FieldData> orderableFieldData = createFieldsDataFromOrderable(issue, renderedFields);
        final Set<String> fieldKeys = Sets.newHashSet(transform(orderableFieldData, new Function<FieldData, String>() {
            @Override
            public String apply(final FieldData fieldData) {
                return fieldData.field.getId();
            }
        }));
        final Iterable<FieldData> navigableFieldData = createFieldsDataFromNavigable(issue, fieldKeys, renderedFields);
        return Maps.uniqueIndex(concat(orderableFieldData, navigableFieldData), new Function<FieldData, String>() {
            @Override
            public String apply(final FieldData fieldData) {
                return fieldData.field.getId();
            }
        });
    }

    private Iterable<FieldData> createFieldsDataFromOrderable(final Issue issue, final boolean renderedFields) {
        // iterate over all the visible layout items from the field layout for this issue and attempt to add them to the result
        final FieldLayout layout = fieldLayoutManager.getFieldLayout(issue);
        final List<FieldLayoutItem> fieldLayoutItems = layout.getVisibleLayoutItems(issue.getProjectObject(), CollectionBuilder.list(issue.getIssueTypeId()));
        final Iterable<FieldLayoutItem> includedFieldLayoutItems = filter(fieldLayoutItems, Predicates.compose(fieldIncluded, new Function<FieldLayoutItem, Field>() {
            @Override
            public Field apply(final FieldLayoutItem fieldLayoutItem) {
                return fieldLayoutItem.getOrderableField();
            }
        }));

        return Iterables.collect(includedFieldLayoutItems, new Function<FieldLayoutItem, Option<FieldData>>() {
            @Override
            public Option<FieldData> apply(final FieldLayoutItem fieldLayoutItem) {
                final OrderableField field = fieldLayoutItem.getOrderableField();
                if (field instanceof RestAwareField) {
                    return createFieldData(field, getRepresentationForFields(fieldLayoutItem, field, issue, renderedFields));
                } else {
                    LOG.info(String.format("OrderableField %s not rendered in JSON", field.getId()));
                    return Option.none();
                }
            }
        });
    }

    private Option<FieldJsonRepresentation> getRepresentationForFields(final FieldLayoutItem fieldLayoutItem,
                                                                       final OrderableField field,
                                                                       final Issue issue,
                                                                       final boolean renderedFields) {
        try {
            final FieldJsonRepresentation representation = ((RestAwareField) field).getJsonFromIssue(issue, renderedFields, fieldLayoutItem);
            if (representation == null || representation.getStandardData() == null) {
                return Option.none();
            }
            return Option.some(representation);
        } catch (Exception e) {
            if (LOG.isDebugEnabled()) {
                LOG.debug(String.format("Cannot get value from RestAwareField" + " %s, exception: '%s' ", field.getId(), e.getMessage()), e);
            } else {
                LOG.info(String.format("Cannot get value from RestAwareField" + " %s, exception: '%s' ", field.getId(), e.getMessage()));
            }
            return Option.none();
        }
    }

    private Iterable<FieldData> createFieldsDataFromNavigable(final Issue issue, final Set<String> fieldsInBean, final boolean renderedFields) {
        // Then we try to add "NavigableFields" which aren't "OrderableFields" unless they ae special ones.
        // These aren't included in the Field Layout.
        // This is a bit crap because "getAvailableNavigableFields" doesn't take the issue into account.
        // All it means is the field is not hidden in at least one project the user has BROWSE permission on.
        final Predicate<NavigableField> isNotInBean = new Predicate<NavigableField>() {
            @Override
            public boolean apply(final NavigableField field) {
                return !fieldsInBean.contains(field.getId());
            }
        };
        final Predicate<NavigableField> isNotOrderableExceptIsSpecial = new Predicate<NavigableField>() {
            @Override
            public boolean apply(final NavigableField field) {
                return !(field instanceof OrderableField) || isSpecialField(field);
            }
        };
        final Iterable<NavigableField> navigableFields = filter(includedNavigableFields.get(), Predicates.and(isNotInBean, isNotOrderableExceptIsSpecial));
        final Iterable<RestAwareField> restAwareNavigableFields = filter(navigableFields, RestAwareField.class);

        return Iterables.collect(restAwareNavigableFields, new Function<RestAwareField, Option<FieldData>>() {
            @Override
            public Option<FieldData> apply(final RestAwareField field) {
                return createFieldData((Field) field, Option.option(field.getJsonFromIssue(issue, renderedFields, null)));
            }
        });
    }

    private Option<FieldData> createFieldData(final Field field, final Option<FieldJsonRepresentation> representation) {
        return representation.map(new Function<FieldJsonRepresentation, FieldData>() {
            @Override
            public FieldData apply(final FieldJsonRepresentation representation) {
                return new FieldData(field, representation);
            }
        });
    }

    private Map<String, Object> createFields(final Map<String, FieldData> fields) {
        return Maps.transformValues(fields, new Function<FieldData, Object>() {
            @Override
            public Object apply(final FieldData field) {
                return field.getJsonData().getData();
            }
        });
    }

    private Option<IssueRefJsonBean> createParentLink(final Issue issue) {
        final Long parentId = issue.getParentId();
        if (parentId == null) {
            return Option.none();
        }

        try {
            return Option.some(issueRefsCache.get(parentId, new Callable<IssueRefJsonBean>() {
                @Override
                public IssueRefJsonBean call() throws Exception {
                    return issueLinkBeanBuilderFactory.newIssueLinksBeanBuilder(issue).buildParentLink();
                }
            }));
        } catch (ExecutionException e) {
            throw new RuntimeException("Issue links to parent which does not exist.");
        }
    }

    private Map<String, Object> createRenderableFields(final Map<String, FieldData> fields) {
        return Maps.transformValues(fields, new Function<FieldData, Object>() {
            @Override
            public Object apply(final FieldData field) {
                return field.getRenderedData().map(new Function<JsonData, Object>() {
                    @Override
                    public Object apply(final JsonData json) {
                        return json.getData();
                    }
                }).getOrNull();
            }
        });
    }

    private Map<String, String> createFieldNames(final Map<String, FieldData> fields) {
        return Maps.transformValues(fields, new Function<FieldData, String>() {
            @Override
            public String apply(final FieldData field) {
                return fieldNames.getUnchecked(field.field);
            }
        });
    }

    private Map<String, JsonTypeBean> createFieldSchema(final Map<String, FieldData> fields) {
        return Maps.transformValues(fields, new Function<FieldData, JsonTypeBean>() {
            @Override
            public JsonTypeBean apply(final FieldData field) {
                return field.getJsonTypeBean();
            }
        });
    }

    private List<TransitionBean> createTransitionBeans(final Issue issue) {
        final List<ActionDescriptor> sortedAvailableActions = issueWorkflowManager.getSortedAvailableActions(issue, authContext.getUser());
        return ImmutableList.copyOf(transform(sortedAvailableActions, new Function<ActionDescriptor, TransitionBean>() {
            @Override
            public TransitionBean apply(final ActionDescriptor action) {
                return beanBuilderFactory.newTransitionMetaBeanBuilder()
                        .issue(issue)
                        .action(action)
                        .build();
            }
        }));
    }

    private Map<String, Map<Integer, Object>> createVersionedRepresentations(final Map<String, FieldData> fields, final Issue issue) {
        return ImmutableMaps.transformValue(fields, new Function<FieldData, Map<Integer, Object>>() {
            @Override
            public Map<Integer, Object> apply(final FieldData fieldData) {
                // The HashMap is used here because we need to store null values.
                final Map<Integer, Object> representations = new HashMap<Integer, Object>();
                representations.put(FIRST_REPRESENTATION_VERSION, fieldData.getJsonData().getData());

                if (!(fieldData.field instanceof CustomField)) {
                    return representations;
                }
                final CustomField field = (CustomField) fieldData.field;
                final CustomFieldType customFieldType = field.getCustomFieldType();

                if (!(customFieldType instanceof AbstractCustomFieldType)) {
                    return representations;
                }
                final AbstractCustomFieldType abstractCustomFieldType = (AbstractCustomFieldType) customFieldType;
                final CustomFieldTypeModuleDescriptor descriptor = abstractCustomFieldType.getDescriptor();

                representations.putAll(createVersionedRepresentationsForField(field, descriptor, issue));

                return representations;
            }
        });
    }

    private Map<Integer, Object> createVersionedRepresentationsForField(final CustomField field,
                                                                        final CustomFieldTypeModuleDescriptor descriptor,
                                                                        final Issue issue) {
        return ImmutableMaps.collect(descriptor.getRestSerializers(), new Function<Map.Entry<Integer, CustomFieldRestSerializer>, Option<Map.Entry<Integer, Object>>>() {
            @Override
            public Option<Map.Entry<Integer, Object>> apply(final Map.Entry<Integer, CustomFieldRestSerializer> entry) {
                return getSafelyVersionedRepresentation(entry.getValue(), entry.getKey(), field, issue).map(new Function<JsonData, Map.Entry<Integer, Object>>() {
                    @Override
                    public Map.Entry<Integer, Object> apply(final JsonData jsonData) {
                        return Maps.immutableEntry(entry.getKey(), jsonData.getData());
                    }
                });
            }
        });
    }

    private Option<JsonData> getSafelyVersionedRepresentation(final CustomFieldRestSerializer serializer,
                                                              final Integer version,
                                                              final CustomField field,
                                                              final Issue issue) {
        try {
            return Option.some(serializer.getJsonData(field, issue));
        } catch (Exception e) {
            if (LOG.isDebugEnabled()) {
                LOG.debug(String.format("Cannot get value from rest serializer %s for field %s, exception: '%s' ", version, field.getId(), e.getMessage()), e);
            } else {
                LOG.info(String.format("Cannot get value from rest serializer %s for field %s, exception: '%s' ", version, field.getId(), e.getMessage()));
            }
            return Option.none();
        }
    }

    /**
     * Returns true if this is a special field that cannot appear in the field config for some special reason
     *
     * @param field the field
     * @return true if the field is special
     */
    private boolean isSpecialField(NavigableField field) {
        // At the moment only the Project System Field is special in this respect
        return field instanceof ProjectSystemField;
    }

    private boolean isExpanded(final String element) {
        return expand != null && expand.contains(element);
    }

    // Unfortunately there is no separate method to get jsonData and renderedData so these values are stored in this class.
    static private class FieldData {
        private final Field field;
        private final FieldJsonRepresentation representation;

        public FieldData(final Field field, final FieldJsonRepresentation representation) {
            this.field = field;
            this.representation = representation;
        }

        public JsonData getJsonData() {
            return representation.getStandardData();
        }

        public Option<JsonData> getRenderedData() {
            return Option.option(representation.getRenderedData());
        }

        public RestAwareField getRestAware() {
            return (RestAwareField) field;
        }

        public JsonTypeBean getJsonTypeBean() {
            return new JsonTypeBean(getRestAware().getJsonSchema());
        }
    }
}

