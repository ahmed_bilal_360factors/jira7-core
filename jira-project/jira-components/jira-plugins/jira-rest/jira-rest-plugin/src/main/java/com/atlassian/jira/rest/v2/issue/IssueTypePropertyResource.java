package com.atlassian.jira.rest.v2.issue;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.jira.bc.issuetype.property.IssueTypePropertyService;
import com.atlassian.jira.entity.property.EntityPropertyType;
import com.atlassian.jira.issue.fields.rest.json.beans.EntityPropertyBean;
import com.atlassian.jira.issue.fields.rest.json.beans.EntityPropertyBeanSelfFunctions;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.issuetype.IssueTypeWithID;
import com.atlassian.jira.rest.v2.entity.property.BasePropertyResource;
import com.atlassian.jira.rest.v2.entity.property.EntityPropertiesKeysBean;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.rest.annotation.ResponseType;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * This resource allows to store custom properties for issue types.
 *
 * @since 7.0
 */
@Path("issuetype/{issueTypeId}/properties")
@AnonymousAllowed
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class IssueTypePropertyResource {
    private final BasePropertyResource<IssueTypeWithID> delegate;

    public IssueTypePropertyResource(IssueTypePropertyService issueTypePropertyService, JiraAuthenticationContext authenticationContext,
                                     JiraBaseUrls jiraBaseUrls, I18nHelper i18n) {
        this.delegate = new BasePropertyResource<IssueTypeWithID>(issueTypePropertyService, authenticationContext, jiraBaseUrls, i18n,
                new EntityPropertyBeanSelfFunctions.IssueTypePropertySelfFunction(), EntityPropertyType.ISSUE_TYPE_PROPERTY);
    }

    /**
     * Returns the keys of all properties for the issue type identified by the id.
     *
     * @param issueTypeId the issue type from which the keys will be returned
     * @return a response containing EntityPropertiesKeysBean
     * @response.representation.200.qname issuetype-properties-keys
     * @response.representation.200.doc Returned if the issue type was found.
     * @response.representation.200.mediaType application/json
     * @response.representation.200.example {@link com.atlassian.jira.rest.v2.entity.property.EntityPropertyResourceExamples#GET_PROPERTIES_KEYS_RESPONSE_200}
     * @response.representation.400.doc Returned if the issue type id is invalid.
     * @response.representation.404.doc Returned if the issue type with given id does not exist or if the user does not have permissions to view this issue type.
     */
    @ExperimentalApi
    @GET
    @ResponseType(EntityPropertiesKeysBean.class)
    public Response getPropertyKeys(@PathParam("issueTypeId") final String issueTypeId) {
        return delegate.getPropertiesKeys(issueTypeId);
    }

    /**
     * Sets the value of the specified issue type's property.
     * <p>
     * You can use this resource to store a custom data against an issue type identified by the id. The user
     * who stores the data is required to have permissions to edit an issue type.
     * </p>
     *
     * @param issueTypeId the issue type on which the property will be set
     * @param propertyKey the key of the issue type's property. The maximum length of the key is 255 bytes
     * @param request     the request containing value of the issue type's property. The value has to a valid, non-empty JSON conforming
     *                    to http://tools.ietf.org/html/rfc4627. The maximum length of the property value is 32768 bytes.
     * @response.representation.200.doc Returned if the issue type property is successfully updated.
     * @response.representation.201.doc Returned if the issue type property is successfully created.
     * @response.representation.400.doc Returned if the issue type id is invalid.
     * @response.representation.404.doc Returned if the issue type with given id does not exist or if the user does not have permissions to edit this issue type.
     */
    @ExperimentalApi
    @PUT
    @Path("/{propertyKey}")
    @ResponseType(Void.class)
    public Response setProperty(@PathParam("issueTypeId") final String issueTypeId,
                                @PathParam("propertyKey") final String propertyKey,
                                @Context final HttpServletRequest request) {
        return delegate.setProperty(issueTypeId, propertyKey, request);
    }

    /**
     * Returns the value of the property with a given key from the issue type identified by the id. The user who retrieves
     * the property is required to have permissions to view the issue type.
     *
     * @param issueTypeId the issue type from which the property will be returned
     * @param propertyKey the key of the property to return
     * @return a response containing {@link com.atlassian.jira.issue.fields.rest.json.beans.EntityPropertyBean}
     * @response.representation.200.qname issuetype-property
     * @response.representation.200.doc Returned if the issue type property was found.
     * @response.representation.200.mediaType application/json
     * @response.representation.200.example {@link com.atlassian.jira.rest.v2.entity.property.EntityPropertyResourceExamples#GET_PROPERTY_RESPONSE_200}
     * @response.representation.400.doc Returned if the issue type id is invalid.
     * @response.representation.404.doc Returned if the issue type with given id does not exist or if the user does not have permissions to view this issue type.
     */
    @ExperimentalApi
    @GET
    @Path("/{propertyKey}")
    @ResponseType(EntityPropertyBean.class)
    public Response getProperty(@PathParam("issueTypeId") final String issueTypeId,
                                @PathParam("propertyKey") final String propertyKey) {
        return delegate.getProperty(issueTypeId, propertyKey);
    }

    /**
     * Removes the property from the issue type identified by the id. Ths user removing the property is required
     * to have permissions to edit the issue type.
     *
     * @param issueTypeId the issue type from which the property will be removed
     * @param propertyKey the key of the property to remove
     * @return a 204 HTTP status if everything goes well
     * @response.representation.204.doc Returned if the issue type property was removed successfully.
     * @response.representation.400.doc Returned if the issue type id is invalid.
     * @response.representation.404.doc Returned if the issue type with given id does not exist or if the property with given key is not found.
     */
    @ExperimentalApi
    @DELETE
    @Path("/{propertyKey}")
    public Response deleteProperty(@PathParam("issueTypeId") final String issueTypeId,
                                   @PathParam("propertyKey") final String propertyKey) {
        return delegate.deleteProperty(issueTypeId, propertyKey);
    }
}


