package com.atlassian.jira.rest.v2.admin;

import com.atlassian.jira.rest.util.ResponseFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.rest.annotation.ResponseType;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.atlassian.jira.rest.api.http.CacheControl.never;

/**
 * @since v6.4
 */
@Path("configuration")
@AnonymousAllowed
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class ConfigurationResource {
    private final JiraAuthenticationContext authenticationContext;
    private final ConfigurationBeanFactory configurationBeanFactory;
    private final ResponseFactory responseFactory;

    public ConfigurationResource(JiraAuthenticationContext authenticationContext,
                                 ConfigurationBeanFactory configurationBeanFactory,
                                 ResponseFactory responseFactory) {
        this.authenticationContext = authenticationContext;
        this.configurationBeanFactory = configurationBeanFactory;
        this.responseFactory = responseFactory;
    }

    /**
     * Returns the information if the optional features in JIRA are enabled or disabled. If the time tracking is enabled,
     * it also returns the detailed information about time tracking configuration.
     *
     * @response.representation.200.qname configuration
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc Returned the configuration of optional features in JIRA.
     * @response.representation.200.example {@link com.atlassian.jira.rest.v2.admin.ConfigurationBean#DOC_EXAMPLE}
     * @response.representation.401.doc Returned if the user is not logged in.
     */
    @GET
    @ResponseType(ConfigurationBean.class)
    public Response getConfiguration() {
        final ApplicationUser user = authenticationContext.getLoggedInUser();

        if (user == null) {
            return responseFactory.notLoggedInResponse();
        } else {
            final ConfigurationBean configurationBean = configurationBeanFactory.createConfigurationBean();
            return Response
                    .status(Response.Status.OK)
                    .cacheControl(never())
                    .entity(configurationBean)
                    .build();
        }
    }

}
