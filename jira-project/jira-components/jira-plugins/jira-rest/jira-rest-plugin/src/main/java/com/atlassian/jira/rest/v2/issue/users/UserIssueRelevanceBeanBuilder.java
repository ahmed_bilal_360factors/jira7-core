package com.atlassian.jira.rest.v2.issue.users;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.avatar.AvatarUrls;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.rest.v2.issue.UserResource;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserIssueRelevance;
import com.atlassian.jira.util.EmailFormatter;
import com.atlassian.jira.util.I18nHelper;

import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.stream.Collectors;

import static java.util.Collections.emptyMap;

/**
 * Builder for UserIssueRelevanceBean instances.
 *
 * @since v7.2
 */
public class UserIssueRelevanceBeanBuilder {
    private final EmailFormatter emailFormatter;
    private final I18nHelper i18n;
    private final JiraBaseUrls jiraBaseUrls;
    private final ApplicationUser loggedInUser;
    private final TimeZone timeZone;
    private final AvatarService avatarService;

    public UserIssueRelevanceBeanBuilder(
            final EmailFormatter emailFormatter,
            final I18nHelper i18n,
            final JiraBaseUrls jiraBaseUrls,
            final ApplicationUser loggedInUser,
            final TimeZone timeZone,
            final AvatarService avatarService) {
        this.emailFormatter = emailFormatter;
        this.i18n = i18n;
        this.jiraBaseUrls = jiraBaseUrls;
        this.loggedInUser = loggedInUser;
        this.timeZone = timeZone;
        this.avatarService = avatarService;
    }

    /**
     * Returns a new UserIssueRelevanceBean with all properties set.
     *
     * @return a new UserIssueRelevanceBean
     */
    public UserIssueRelevanceBean build(final UserIssueRelevance userIssueRelevance) {
        final ApplicationUser user = userIssueRelevance.getUser();
        final List<IssueInvolvementBean> issueInvolvements = userIssueRelevance.getIssueInvolvements().stream()
                .map(issueInvolvement -> new IssueInvolvementBean(issueInvolvement.getId(), i18n.getText(issueInvolvement.getI18nKey())))
                .collect(Collectors.toList());
        return new UserIssueRelevanceBean(
                createSelfLink(user),
                user.getKey(),
                user.getName(),
                user.getDisplayName(),
                user.isActive(),
                emailFormatter.formatEmail(user.getEmailAddress(), loggedInUser),
                getAvatarURLs(user),
                timeZone,
                i18n.getLocale(),
                issueInvolvements,
                userIssueRelevance.getHighestIssueInvolvementRank(),
                userIssueRelevance.getLatestCommentCreationTime()
        );
    }

    /**
     * Generate the self link for a given user
     */
    private URI createSelfLink(final ApplicationUser user) {
        return UriBuilder.fromPath(jiraBaseUrls.restApi2BaseUrl())
                .path(UserResource.class)
                .queryParam("username", "{0}") // JRADEV-3622. Workaround for percent encoding problem.
                .build(user.getUsername()); //JRADEV-22338: FIXME: we should start returning key instead of username here!
    }

    /**
     * Generate the avatar urls for a given user
     */
    private Map<String, URI> getAvatarURLs(final ApplicationUser forUser) {
        final Avatar avatar = avatarService.getAvatar(forUser, forUser);
        return avatar != null ? AvatarUrls.getAvatarURLs(forUser, avatar) : emptyMap();
    }
}
