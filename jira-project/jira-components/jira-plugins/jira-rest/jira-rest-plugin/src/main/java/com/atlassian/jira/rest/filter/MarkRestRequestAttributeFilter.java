package com.atlassian.jira.rest.filter;

import com.atlassian.jira.web.ExecutingHttpRequest;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;

import static java.util.Optional.ofNullable;

/**
 * Marks this {@link javax.servlet.http.HttpServletRequest} as a REST request using a request attribute.
 * The attribute name is {@code "is_rest_request"}.
 */
class MarkRestRequestAttributeFilter implements ContainerRequestFilter {

    @Override
    public ContainerRequest filter(final ContainerRequest containerRequest) {
        ofNullable(ExecutingHttpRequest.get()).ifPresent(request -> request.setAttribute("is_rest_request", true));
        return containerRequest;
    }
}
