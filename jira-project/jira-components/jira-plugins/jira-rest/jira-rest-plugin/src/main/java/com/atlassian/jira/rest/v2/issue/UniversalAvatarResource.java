package com.atlassian.jira.rest.v2.issue;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarImageResolver;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.avatar.SystemAndCustomAvatars;
import com.atlassian.jira.icon.IconOwningObjectId;
import com.atlassian.jira.icon.IconType;
import com.atlassian.jira.rest.v2.avatar.AvatarUrls;
import com.atlassian.jira.rest.v2.avatar.TemporaryAvatarHelper;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.rest.common.multipart.FilePart;
import com.atlassian.plugins.rest.common.multipart.MultipartFormParam;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.rest.annotation.ResponseType;
import com.atlassian.rest.annotation.ResponseTypes;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import static com.atlassian.jira.rest.api.http.CacheControl.never;

@Path("universal_avatar")
@AnonymousAllowed
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class UniversalAvatarResource {
    public static final String SYSTEM_AVATARS_FIELD = "system";
    public static final String CUSTOM_AVATARS_FIELD = "custom";

    private final JiraAuthenticationContext authContext;
    private final AvatarUrls avatarUrls;
    private final AvatarResourceHelper avatarResourceHelper;
    private final TemporaryAvatarHelper avatarTemporaryHelper;
    private final AvatarManager avatarManager;
    private final AvatarImageResolver avatarImageResolver;

    public UniversalAvatarResource(final JiraAuthenticationContext authContext,
                                   final AvatarUrls avatarUrls, AvatarResourceHelper avatarResourceHelper,
                                   final TemporaryAvatarHelper avatarTemporaryHelper,
                                   @ComponentImport final AvatarManager avatarManager,
                                   @ComponentImport final AvatarImageResolver avatarImageResolver) {
        this.authContext = authContext;
        this.avatarUrls = avatarUrls;
        this.avatarResourceHelper = avatarResourceHelper;
        this.avatarTemporaryHelper = avatarTemporaryHelper;
        this.avatarManager = avatarManager;
        this.avatarImageResolver = avatarImageResolver;
    }

    /**
     * @since v6.3
     */
    @GET
    @Path("type/{type}/owner/{owningObjectId}")
    @ResponseType(value = Map.class, genericTypes = {String.class, AvatarBean[].class})
    public Response getAvatars(final @PathParam("type") String avatarType, final @PathParam("owningObjectId") String owningObjectId) {
        final IconType iconType = IconType.of(avatarType);
        if (!avatarManager.isValidIconType(iconType)) {
            return Response.status(Response.Status.NOT_FOUND).entity("iconType").build();
        }

        final List<Avatar> systemAvatars = avatarManager.getAllSystemAvatars(iconType);
        final List<Avatar> avatarsForOwner = avatarManager.getCustomAvatarsForOwner(iconType, owningObjectId);
        final SystemAndCustomAvatars allAvatars = new SystemAndCustomAvatars(systemAvatars, avatarsForOwner);

        final ApplicationUser remoteUser = authContext.getUser();

        final List<AvatarBean> systemAvatarBeans = createAvatarBeans(allAvatars.getSystemAvatars(), remoteUser);
        final List<AvatarBean> customAvatarBeans = createAvatarBeans(allAvatars.getCustomAvatars(), remoteUser);

        Map<String, List<AvatarBean>> result = ImmutableMap.<String, List<AvatarBean>>builder().
                put(SYSTEM_AVATARS_FIELD, systemAvatarBeans).
                put(CUSTOM_AVATARS_FIELD, customAvatarBeans).
                build();

        return Response.ok(result).build();
    }

    /**
     * Creates temporary avatar
     *
     * @param avatarType     Type of entity where to change avatar
     * @param owningObjectId Entity id where to change avatar
     * @param filename       name of file being uploaded
     * @param size           size of file
     * @param request        servlet request
     * @return temporary avatar cropping instructions
     * @since v5.0
     */
    @POST
    @Consumes(MediaType.WILDCARD)
    @RequiresXsrfCheck
    @Path("type/{type}/owner/{owningObjectId}/temp")
    @ResponseTypes({@ResponseType(AvatarCroppingBean.class), @ResponseType(AvatarBean.class)})
    public Response storeTemporaryAvatar(final @PathParam("type") String avatarType, final @PathParam("owningObjectId") String owningObjectId, final @QueryParam("filename") String filename,
                                         final @QueryParam("size") Long size, final @Context HttpServletRequest request) {
        try {
            final IconType iconType = IconType.of(avatarType);
            if (!avatarManager.isValidIconType(iconType)) {
                throw new NoSuchElementException("avatarType");
            }

            final ApplicationUser remoteUser = authContext.getUser();

            // get from paramter!!
            Avatar.Size avatarTargetSize = Avatar.Size.LARGE;
            final Response storeTemporaryAvatarResponse = avatarTemporaryHelper.storeTemporaryAvatar(remoteUser, iconType, owningObjectId, avatarTargetSize, filename, size, request);

            return storeTemporaryAvatarResponse;
        } catch (NoSuchElementException x) {
            return Response.status(Response.Status.NOT_FOUND).entity(x.getMessage()).build();
        }
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @RequiresXsrfCheck
    @Path("type/{type}/owner/{owningObjectId}/temp")
    @Produces({MediaType.TEXT_HTML})
    @ResponseTypes({@ResponseType(AvatarCroppingBean.class), @ResponseType(AvatarBean.class)})
    public Response storeTemporaryAvatarUsingMultiPart(final @PathParam("type") String avatarType, final @PathParam("owningObjectId") String owningObjectId,
                                                       final @MultipartFormParam("avatar") FilePart filePart, final @Context HttpServletRequest request) {
        try {
            try {
                final IconType iconType = IconType.of(avatarType);
                if (null == iconType) {
                    throw new NoSuchElementException("avatarType");
                }

                final ApplicationUser remoteUser = authContext.getUser();

                // get from paramter!!
                Avatar.Size avatarTargetSize = Avatar.Size.LARGE;
                final Response storeTemporaryAvatarResponse = avatarTemporaryHelper.storeTemporaryAvatar(remoteUser, iconType, owningObjectId, avatarTargetSize, filePart, request);

                return storeTemporaryAvatarResponse;
            } catch (NoSuchElementException x) {
                return Response.status(Response.Status.NOT_FOUND).entity(x.getMessage()).build();
            }
        } catch (RESTException x) {
            String errorMsgs = x.toString();
            String sep = "";

            return Response.status(Response.Status.OK)
                    .entity("<html><body>"
                            + "<textarea>"
                            + "{"
                            + "\"errorMessages\": [" + errorMsgs + "]"
                            + "}"
                            + "</textarea>"
                            + "</body></html>")
                    .cacheControl(never()).build();
        }
    }

    @POST
    @Path("type/{type}/owner/{owningObjectId}/avatar")
    @ResponseType(AvatarBean.class)
    public Response createAvatarFromTemporary(final @PathParam("type") String avatarType, final @PathParam("owningObjectId") String owningObjectId, final AvatarCroppingBean croppingInstructions) {
        try {
            final IconType iconType = IconType.of(avatarType);
            if (!avatarManager.isValidIconType(iconType)) {
                throw new NoSuchElementException("avatarType");
            }
            final ApplicationUser remoteUser = authContext.getUser();

            final AvatarBean avatarFromTemporary =
                    avatarTemporaryHelper.createAvatarFromTemporary(
                            remoteUser,
                            iconType,
                            owningObjectId,
                            croppingInstructions);

            return Response.status(Response.Status.CREATED).entity(avatarFromTemporary).cacheControl(never()).build();
        } catch (NoSuchElementException x) {
            return Response.status(Response.Status.NOT_FOUND).entity(x.getMessage()).build();
        }
    }


    /**
     * Deletes avatar
     *
     * @param avatarType Project id or project key
     * @param id         database id for avatar
     * @return temporary avatar cropping instructions
     * @since v5.0
     */
    @DELETE
    @Path("type/{type}/owner/{owningObjectId}/avatar/{id}")
    public Response deleteAvatar(final @PathParam("type") String avatarType, final @PathParam("owningObjectId") String owningObjectId, @PathParam("id") final Long id) {
        final IconType iconType = IconType.of(avatarType);
        if (!avatarManager.isValidIconType(iconType)) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        if (!avatarManager.userCanCreateFor(authContext.getLoggedInUser(), iconType, new IconOwningObjectId(owningObjectId))) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        final Response avatarFromTemporaryResponse = avatarResourceHelper.deleteAvatar(id);
        return avatarFromTemporaryResponse;
    }


    private List<AvatarBean> createAvatarBeans(final Iterable<Avatar> avatars, final ApplicationUser remoteUser) {
        List<AvatarBean> beans = Lists.newArrayList();
        for (Avatar avatar : avatars) {
            final AvatarBean bean = new AvatarBean(
                    avatar.getId().toString(),
                    avatar.getOwner(),
                    avatar.isSystemAvatar(),
                    avatarManager.userCanDelete(remoteUser, avatar),
                    avatarUrls.getAvatarURLs(remoteUser, avatar, avatarImageResolver)
            );
            beans.add(bean);
        }

        return beans;
    }

}
