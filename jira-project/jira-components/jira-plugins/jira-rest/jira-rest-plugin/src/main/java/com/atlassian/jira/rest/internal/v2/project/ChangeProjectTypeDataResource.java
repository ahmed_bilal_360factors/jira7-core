package com.atlassian.jira.rest.internal.v2.project;

import com.atlassian.fugue.Either;
import com.atlassian.jira.bc.project.ProjectAction;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.help.HelpUrl;
import com.atlassian.jira.help.HelpUrls;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.type.ProjectType;
import com.atlassian.jira.project.type.ProjectTypeManager;
import com.atlassian.jira.rest.util.ProjectFinder;
import com.atlassian.jira.rest.util.ResponseFactory;
import com.atlassian.jira.rest.v2.issue.project.ProjectBean;
import com.atlassian.jira.rest.v2.issue.project.ProjectBeanFactory;
import com.atlassian.jira.rest.v2.project.type.ProjectTypeBean;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.annotation.Nullable;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("projects")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ChangeProjectTypeDataResource {
    private final JiraAuthenticationContext authContext;
    private final ProjectFinder projectFinder;
    private final ResponseFactory responseFactory;
    private final ProjectTypeManager projectTypeManager;
    private final ProjectBeanFactory projectBeanFactory;
    private final HelpUrls helpUrls;

    public ChangeProjectTypeDataResource(
            @ComponentImport final JiraAuthenticationContext authContext,
            @ComponentImport final ProjectTypeManager projectTypeManager,
            @ComponentImport final HelpUrls helpUrls,
            final ProjectFinder projectFinder,
            final ResponseFactory responseFactory,
            final ProjectBeanFactory projectBeanFactory) {
        this.authContext = authContext;
        this.projectFinder = projectFinder;
        this.responseFactory = responseFactory;
        this.projectTypeManager = projectTypeManager;
        this.projectBeanFactory = projectBeanFactory;
        this.helpUrls = helpUrls;
    }

    /**
     * This method is used to retrieve the information to show the change project type dialog.
     * The actual change project type is located in ProjectResource.
     */
    @GET
    @Path("{projectIdOrKey}/changetypedata/")
    public Response getChangeProjectTypeData(@PathParam("projectIdOrKey") final String projectIdOrKey) {
        final Either<Response, Project> eitherProjectOrErrors = getEitherProjectOrErrors(projectIdOrKey, ProjectAction.EDIT_PROJECT_CONFIG);
        return eitherProjectOrErrors.left().on(new Function<Project, Response>() {
            @Nullable
            @Override
            public Response apply(final Project project) {
                List<ProjectTypeBean> projectTypeBeans = getProjectTypeBeans(projectTypeManager.getAllAccessibleProjectTypes());
                HelpUrl helpLink = helpUrls.getUrl("jira_applications_overview");
                ProjectBean projectBean = projectBeanFactory.fullProject(project, "");
                return Response.ok(new ChangeProjectTypeData(projectBean, helpLink.getUrl(), projectTypeBeans)).build();
            }
        });
    }

    private Either<Response, Project> getEitherProjectOrErrors(final String projectIdOrKey, final ProjectAction action) {
        final ProjectService.GetProjectResult projectResult = projectFinder.getGetProjectForActionByIdOrKey(authContext.getLoggedInUser(),
                projectIdOrKey, action);

        if (projectResult.isValid()) {
            return Either.right(projectResult.getProject());
        }
        return Either.left(responseFactory.errorResponse(projectResult.getErrorCollection()));

    }

    private List<ProjectTypeBean> getProjectTypeBeans(final List<ProjectType> projectTypes) {
        return Lists.newArrayList(Iterables.transform(projectTypes, new Function<ProjectType, ProjectTypeBean>() {
            @Nullable
            @Override
            public ProjectTypeBean apply(@Nullable final ProjectType projectType) {
                return toBean(projectType);
            }
        }));
    }

    private ProjectTypeBean toBean(ProjectType projectType) {
        return new ProjectTypeBean(
                projectType.getKey().getKey(),
                projectType.getDescriptionI18nKey(),
                projectType.getIcon(),
                projectType.getColor()
        );
    }

    public static class ChangeProjectTypeData {
        @JsonProperty
        private ProjectBean project;
        @JsonProperty
        private String helpLink;
        @JsonProperty
        private List<ProjectTypeBean> projectTypes;

        public ChangeProjectTypeData(final ProjectBean project, final String helpLink, final List<ProjectTypeBean> projectTypes) {
            this.project = project;
            this.helpLink = helpLink;
            this.projectTypes = projectTypes;
        }

        public ProjectBean getProject() {
            return project;
        }

        public String getHelpLink() {
            return helpLink;
        }

        public List<ProjectTypeBean> getProjectTypes() {
            return projectTypes;
        }
    }
}
