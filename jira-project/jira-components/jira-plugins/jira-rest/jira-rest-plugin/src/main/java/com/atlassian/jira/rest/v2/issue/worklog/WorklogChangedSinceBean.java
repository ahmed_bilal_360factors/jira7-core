package com.atlassian.jira.rest.v2.issue.worklog;

import com.atlassian.jira.rest.v2.issue.Examples;
import com.google.common.collect.ImmutableList;
import org.codehaus.jackson.annotate.JsonAutoDetect;

import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.List;

@JsonAutoDetect
public class WorklogChangedSinceBean {
    public static final UriBuilder worklogResourcePathBuilder = UriBuilder.fromUri(Examples.jiraURI()).path(WorklogResource.class);
    public static final WorklogChangedSinceBean UPDATED_EXAMPLE = WorklogChangedSinceBean.builder()
            .setValues(ImmutableList.of(
                    new WorklogChangeBean(103L, 1438013671562L),
                    new WorklogChangeBean(104L, 1438013672165L),
                    new WorklogChangeBean(105L, 1438013693136L)
            ))
            .setSince(1438013671562L)
            .setUntil(1438013693136L)
            .setIsLastPage(true)
            .setSelf(worklogResourcePathBuilder.path("updated").queryParam("since", 1438013671136L).build())
            .setNextPage(worklogResourcePathBuilder.path("updated").queryParam("since", 1438013693136L).build())
            .build();

    public static final WorklogChangedSinceBean DELETED_EXAMPLE = WorklogChangedSinceBean.builder()
            .setValues(ImmutableList.of(
                    new WorklogChangeBean(103L, 1438013671562L),
                    new WorklogChangeBean(104L, 1438013672165L),
                    new WorklogChangeBean(105L, 1438013693136L)
            ))
            .setSince(1438013671562L)
            .setUntil(1438013693136L)
            .setIsLastPage(true)
            .setSelf(worklogResourcePathBuilder.path("deleted").queryParam("since", 1438013671136L).build())
            .setNextPage(worklogResourcePathBuilder.path("deleted").queryParam("since", 1438013693136L).build())
            .build();

    private final List<WorklogChangeBean> values;
    private final Long since;
    private final Long until;
    private final boolean isLastPage;
    private final URI self;
    private final URI nextPage;

    public WorklogChangedSinceBean(final URI nextPage,
                                   final boolean isLastPage,
                                   final URI self,
                                   final Long since,
                                   final Long until,
                                   final List<WorklogChangeBean> values) {
        this.nextPage = nextPage;
        this.isLastPage = isLastPage;
        this.self = self;
        this.since = since;
        this.until = until;
        this.values = values;
    }

    public List<WorklogChangeBean> getValues() {
        return values;
    }

    public Long getSince() {
        return since;
    }

    public Long getUntil() {
        return until;
    }

    public boolean isLastPage() {
        return isLastPage;
    }

    public URI getSelf() {
        return self;
    }

    public URI getNextPage() {
        return nextPage;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private List<WorklogChangeBean> values;
        private Long since;
        private Long until;
        private boolean isLastPage;
        private URI self;
        private URI nextPage;

        public Builder setValues(final List<WorklogChangeBean> values) {
            this.values = values;
            return this;
        }

        public Builder setSince(final Long since) {
            this.since = since;
            return this;
        }

        public Builder setUntil(final Long until) {
            this.until = until;
            return this;
        }

        public Builder setIsLastPage(final boolean isLastPage) {
            this.isLastPage = isLastPage;
            return this;
        }

        public Builder setSelf(final URI self) {
            this.self = self;
            return this;
        }

        public Builder setNextPage(final URI nextPage) {
            this.nextPage = nextPage;
            return this;
        }

        public WorklogChangedSinceBean build() {
            return new WorklogChangedSinceBean(nextPage, isLastPage, self, since, until, values);
        }
    }
}
