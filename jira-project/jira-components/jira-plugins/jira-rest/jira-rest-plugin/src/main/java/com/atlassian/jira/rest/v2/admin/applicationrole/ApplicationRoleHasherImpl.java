package com.atlassian.jira.rest.v2.admin.applicationrole;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.application.ApplicationRole;
import com.google.common.base.Charsets;
import com.google.common.hash.Funnel;
import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @since v7.0
 */
public class ApplicationRoleHasherImpl implements ApplicationRoleHasher {
    private final Funnel<ApplicationRole> applicationRoleFunnel = (appRole, primitiveSink) ->
    {
        primitiveSink.putString(appRole.getKey().toString(), Charsets.UTF_8);
        primitiveSink.putBoolean(appRole.isSelectedByDefault());
        for (Group group : appRole.getGroups()) {
            primitiveSink.putString(group.getName(), Charsets.UTF_8);
        }
        for (Group defaultGroup : appRole.getDefaultGroups()) {
            primitiveSink.putString(defaultGroup.getName(), Charsets.UTF_8);
        }
    };

    @Nonnull
    @Override
    public String getVersionHash(@Nonnull final Collection<ApplicationRole> applicationRoles) {
        return calculateVersionHash(applicationRoles);
    }

    private String calculateVersionHash(final Collection<ApplicationRole> applicationRoles) {
        HashFunction hf = Hashing.md5();

        List<ApplicationRole> appRoles = new ArrayList<>(applicationRoles);
        Collections.sort(appRoles, ApplicationRoleNameSorter);

        List<HashCode> hashCodes = appRoles.stream()
                .map(appRole -> hf.newHasher()
                        .putObject(appRole, applicationRoleFunnel).hash())
                .collect(Collectors.toList());

        if (hashCodes.isEmpty()) {
            return "0";
        }

        return Hashing.combineOrdered(hashCodes).toString();
    }

    protected static Comparator<ApplicationRole> ApplicationRoleNameSorter =
            (o1, o2) -> o1.getKey().toString().compareTo(o2.getKey().toString());
}
