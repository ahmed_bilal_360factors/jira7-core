package com.atlassian.jira.rest.v2.issue.attachment.format;

import com.atlassian.core.util.FileSize;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.attachment.Path;
import com.atlassian.jira.plugin.attachment.AttachmentArchive;
import com.atlassian.jira.plugin.attachment.AttachmentArchiveEntry;
import com.google.common.collect.ImmutableList;

import java.util.Collection;
import java.util.List;

/**
 * Output is human-readable and subject to change.
 *
 * @since v6.4
 */
final class ArchiveHumanFormatter implements ArchiveFormatter<HumanReadableArchive> {
    private static final int MAX_ENTRY_LABEL_LENGTH = 40;

    @Override
    public HumanReadableArchive format(final AttachmentArchive archive, final Attachment attachment) {
        final Long id = attachment.getId();
        final List<AttachmentArchiveEntry> entries = archive.getEntries();
        final Collection<HumanReadableArchiveEntry> convertedEntries = convertEntries(entries);
        final String name = attachment.getFilename();
        final int totalEntryCount = archive.getTotalEntryCount();
        final String mediaType = attachment.getMimetype();
        return new HumanReadableArchive(id, name, convertedEntries, totalEntryCount, mediaType);
    }

    private Collection<HumanReadableArchiveEntry> convertEntries(final Iterable<AttachmentArchiveEntry> entries) {
        final ImmutableList.Builder<HumanReadableArchiveEntry> convertedEntries = ImmutableList.builder();
        for (final AttachmentArchiveEntry entry : entries) {
            final HumanReadableArchiveEntry convertedEntry = convertEntry(entry);
            convertedEntries.add(convertedEntry);
        }
        return convertedEntries.build();
    }

    private HumanReadableArchiveEntry convertEntry(final AttachmentArchiveEntry archiveEntry) {
        final Path path = new Path(archiveEntry.getName());
        final long index = archiveEntry.getEntryIndex();
        final String size = FileSize.format(archiveEntry.getSize());
        final String mediaType = archiveEntry.getMediaType();
        final String label = path.abbreviate(MAX_ENTRY_LABEL_LENGTH).toString();
        return new HumanReadableArchiveEntry(path.toString(), index, size, mediaType, label);
    }
}
