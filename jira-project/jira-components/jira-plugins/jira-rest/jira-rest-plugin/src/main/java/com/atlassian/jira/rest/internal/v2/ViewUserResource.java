package com.atlassian.jira.rest.internal.v2;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.bc.user.GroupView;
import com.atlassian.jira.bc.user.UserApplicationHelper;
import com.atlassian.jira.bc.user.UserApplicationHelper.ApplicationSelection;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.rest.util.ResponseFactory;
import com.atlassian.jira.rest.util.UpdateUserApplicationHelper;
import com.atlassian.jira.rest.util.UpdateUserApplicationHelper.ApplicationUpdateResult;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import org.codehaus.jackson.annotate.JsonAutoDetect;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import static com.atlassian.jira.rest.api.http.CacheControl.never;

/**
 * @since v7.0
 */
@Path("viewuser")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
public class ViewUserResource {
    private final UpdateUserApplicationHelper updateUserApplicationHelper;
    private final ResponseFactory responseFactory;
    private final UserApplicationHelper userApplicationHelper;
    private final JiraAuthenticationContext authContext;
    private final GlobalPermissionManager permissionManager;
    private final UserManager userManager;
    private final ApplicationRoleManager applicationRoleManager;

    public ViewUserResource(final UpdateUserApplicationHelper updateUserApplicationHelper,
                            final ResponseFactory responseFactory,
                            @ComponentImport final UserApplicationHelper userApplicationHelper,
                            final JiraAuthenticationContext authContext,
                            final GlobalPermissionManager permissionManager,
                            final UserManager userManager,
                            final ApplicationRoleManager applicationRoleManager) {
        this.updateUserApplicationHelper = updateUserApplicationHelper;
        this.responseFactory = responseFactory;
        this.userApplicationHelper = userApplicationHelper;
        this.authContext = authContext;
        this.permissionManager = permissionManager;
        this.userManager = userManager;
        this.applicationRoleManager = applicationRoleManager;
    }

    /**
     * Remove user from application and return status of applications connected with current user.
     */
    @DELETE
    @Path("application/{applicationKey}")
    public Response removeUserFromApplication(@QueryParam("username") final String username, @PathParam("applicationKey") final String applicationKey) {
        final ApplicationUpdateResult applicationUpdateResult = updateUserApplicationHelper.removeUserFromApplication(username, applicationKey);
        final UserApplicationUpdateBean userApplicationUpdateBean = getUserApplicationUpdateBeanFromUpdateResult(applicationUpdateResult);
        if (applicationUpdateResult.isValid()) {
            return responseFactory.okNoCache(userApplicationUpdateBean);
        } else {
            return Response.status(Response.Status.BAD_REQUEST).cacheControl(never()).entity(userApplicationUpdateBean).build();
        }
    }

    /**
     * Add user to application and return status of applications connected with current user.
     */
    @POST
    @Path("application/{applicationKey}")
    public Response addUserToApplication(@QueryParam("username") final String username, @PathParam("applicationKey") final String applicationKey) {
        final ApplicationUpdateResult applicationUpdateResult = updateUserApplicationHelper.addUserToApplication(username, applicationKey);
        final UserApplicationUpdateBean userApplicationUpdateBean = getUserApplicationUpdateBeanFromUpdateResult(applicationUpdateResult);
        if (applicationUpdateResult.isValid()) {
            return responseFactory.okNoCache(userApplicationUpdateBean);
        } else {
            return Response.status(Response.Status.BAD_REQUEST).cacheControl(never()).entity(userApplicationUpdateBean).build();
        }
    }

    private UserApplicationUpdateBean getUserApplicationUpdateBeanFromUpdateResult(final ApplicationUpdateResult applicationUpdateResult) {
        return new UserApplicationUpdateBean(
                ImmutableList.copyOf(applicationUpdateResult.getErrorCollection().getErrorMessages()),
                isUserAccessEditable(applicationUpdateResult),
                getApplicationForUser(applicationUpdateResult),
                getUserGroups(applicationUpdateResult),
                getUserDisplayName(applicationUpdateResult),
                isShowNoAppsWarning(applicationUpdateResult));
    }

    private List<ApplicationSelection> getApplicationForUser(@Nonnull final ApplicationUpdateResult applicationUpdateResult) {
        final Option<ApplicationUser> applicationUser = applicationUpdateResult.getApplicationUser();
        if (applicationUser.isEmpty()) {
            return ImmutableList.of();
        }

        return userApplicationHelper.getApplicationsForUser(applicationUser.get());
    }

    private List<GroupView> getUserGroups(@Nonnull final ApplicationUpdateResult applicationUpdateResult) {
        if (applicationUpdateResult.getApplicationUser().isEmpty()) {
            return ImmutableList.of();
        }

        return userApplicationHelper.getUserGroups(applicationUpdateResult.getApplicationUser().get());
    }

    private boolean isUserAccessEditable(@Nonnull final ApplicationUpdateResult applicationUpdateResult) {
        final ApplicationUser loggedInUser = authContext.getLoggedInUser();
        final Option<ApplicationUser> userOption = applicationUpdateResult.getApplicationUser();
        final ApplicationUser user = userOption.getOrNull();
        return userOption.isDefined() && loggedInUser != null && userManager.canUpdateGroupMembershipForUser(user) &&
                (permissionManager.hasPermission(GlobalPermissionKey.SYSTEM_ADMIN, loggedInUser) ||
                        !permissionManager.hasPermission(GlobalPermissionKey.SYSTEM_ADMIN, user));
    }

    private String getUserDisplayName(@Nonnull final ApplicationUpdateResult applicationUpdateResult) {
        if (applicationUpdateResult.getApplicationUser().isEmpty()) {
            return "";
        }
        return applicationUpdateResult.getApplicationUser().get().getDisplayName();
    }

    private boolean isShowNoAppsWarning(@Nonnull final ApplicationUpdateResult applicationUpdateResult) {
        return !userApplicationHelper.canUserLogin(applicationUpdateResult.getApplicationUser().getOrNull());
    }

    private final static Function<ApplicationRole, ApplicationKey> TO_APPLICATION_KEY = new Function<ApplicationRole, ApplicationKey>() {
        @Nullable
        @Override
        public ApplicationKey apply(@Nullable final ApplicationRole input) {
            if (input == null) {
                return null;
            }
            return input.getKey();
        }
    };

    @JsonAutoDetect
    public static class UserApplicationUpdateBean {
        private List<String> errorMessages;
        private boolean isEditable;
        private List<ApplicationSelection> selectableApplications;
        private List<GroupView> userGroups;
        private String displayName;
        private boolean showNoAppsWarning;

        public UserApplicationUpdateBean() {
        }

        public UserApplicationUpdateBean(final List<String> errorMessages, final boolean isEditable, final List<ApplicationSelection> selectableApplications,
                                         final List<GroupView> userGroups, final String displayName, final boolean showNoAppsWarning) {
            this.errorMessages = errorMessages;
            this.isEditable = isEditable;
            this.selectableApplications = selectableApplications;
            this.userGroups = userGroups;
            this.displayName = displayName;
            this.showNoAppsWarning = showNoAppsWarning;
        }

        public boolean isShowNoAppsWarning() {
            return showNoAppsWarning;
        }

        public List<String> getErrorMessages() {
            return errorMessages;
        }

        public boolean isEditable() {
            return isEditable;
        }

        public List<ApplicationSelection> getSelectableApplications() {
            return selectableApplications;
        }

        public List<GroupView> getUserGroups() {
            return userGroups;
        }

        public String getDisplayName() {
            return displayName;
        }
    }
}
