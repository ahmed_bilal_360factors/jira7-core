package com.atlassian.jira.rest.v2.issue.project;

import com.atlassian.jira.rest.api.project.RoleActorBean;
import com.atlassian.jira.security.roles.RoleActor;
import com.atlassian.jira.util.Function;
import com.atlassian.jira.util.collect.Transformed;
import com.google.common.collect.ImmutableList;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Collection;
import java.util.Set;

public class ProjectRoleActorsBean {
    private final Collection<RoleActorBean> actors;

    private ProjectRoleActorsBean(final Collection<RoleActorBean> actors) {
        this.actors = actors;
    }

    @JsonProperty
    public Collection<RoleActorBean> getActors() {
        return actors;
    }

    public static ProjectRoleActorsBean from(final Set<RoleActor> roleActors) {
        Collection<RoleActorBean> beans = Transformed.collection(roleActors, new Function<RoleActor, RoleActorBean>() {
            public RoleActorBean get(RoleActor actor) {
                return RoleActorBean.convert(actor);
            }
        });

        return new ProjectRoleActorsBean(beans);
    }

    public static ProjectRoleActorsBean DOC_EXAMPLE;

    static {
        DOC_EXAMPLE = new ProjectRoleActorsBean(ImmutableList.of(RoleActorBean.DOC_EXAMPLE));
    }
}