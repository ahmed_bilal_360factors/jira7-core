package com.atlassian.jira.rest.util;

import com.atlassian.fugue.Either;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.ErrorCollections;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.google.common.base.Function;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;
import java.net.URI;

import static com.atlassian.jira.rest.api.http.CacheControl.never;

@ExportAsService
@Component
public class ResponseFactoryImpl implements ResponseFactory {
    private final I18nHelper i18nBean;

    @Autowired
    public ResponseFactoryImpl(final I18nHelper i18nBean) {
        this.i18nBean = i18nBean;
    }

    @Override
    public Response notLoggedInResponse() {
        return errorResponse(ErrorCollections.create(i18nBean.getText("rest.authentication.no.user.logged.in"), ErrorCollection.Reason.NOT_LOGGED_IN));
    }

    @Override
    public Response errorResponse(final ErrorCollection errorCollection) {
        com.atlassian.jira.rest.api.util.ErrorCollection restErrorCollection = com.atlassian.jira.rest.api.util.ErrorCollection.of(errorCollection);
        return Response.status(restErrorCollection.getStatus()).entity(restErrorCollection).cacheControl(never()).build();
    }

    @Override
    public Response okNoCache(Object entity) {
        return Response.ok(entity).cacheControl(never()).build();
    }

    @Override
    public Response created(final URI self, final Object entity) {
        return Response.created(self).entity(entity).cacheControl(never()).build();
    }

    @Override
    public Response noContent() {
        return Response.noContent().cacheControl(never()).build();
    }

    @Override
    public Response badRequest(final String i18nKey, final String... args) {
        return Response
                .status(Response.Status.BAD_REQUEST)
                .entity(com.atlassian.jira.rest.api.util.ErrorCollection.of(i18nBean.getText(i18nKey, args)))
                .cacheControl(never())
                .build();
    }

    @Override
    public Response forbidden(final String i18nKey, final String... args) {
        return Response
                .status(Response.Status.FORBIDDEN)
                .entity(com.atlassian.jira.rest.api.util.ErrorCollection.of(i18nBean.getText(i18nKey, args)))
                .cacheControl(never())
                .build();
    }

    @Override
    public Response notFound(final String i18nKey, String... args) {
        return Response
                .status(Response.Status.NOT_FOUND)
                .entity(com.atlassian.jira.rest.api.util.ErrorCollection.of(i18nBean.getText(i18nKey, args)))
                .cacheControl(never())
                .build();
    }

    /**
     * Given a list of errors generate a http response
     *
     * @param errors generated
     * @return http response for the worst error
     */
    @Override
    public Response generateFieldErrorResponse(final ErrorCollection errors) {
        ErrorCollection.Reason worstReason = ErrorCollection.Reason.getWorstReason(errors.getReasons());
        int errorCode = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
        if (worstReason != null) {
            errorCode = worstReason.getHttpStatusCode();
        }
        return Response.status(errorCode).entity(errors.getErrors()).build();
    }

    @Override
    public Response generateErrorResponse(final ErrorCollection errors) {
        ErrorCollection.Reason worstReason = ErrorCollection.Reason.getWorstReason(errors.getReasons());
        int errorCode = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
        if (worstReason != null) {
            errorCode = worstReason.getHttpStatusCode();
        }
        return Response.status(errorCode).entity(errors.getErrorMessages()).build();
    }

    @Override
    public <T> Either<Response, T> validateOutcome(ServiceOutcome<T> outcome) {
        if (outcome.isValid()) {
            return Either.right(outcome.get());
        } else {
            return Either.left(errorResponse(outcome.getErrorCollection()));
        }
    }

    @Override
    public Response serviceResultToNoContentResponse(ServiceResult serviceResult) {
        if (serviceResult.isValid()) {
            return noContent();
        } else {
            return errorResponse(serviceResult.getErrorCollection());
        }
    }

    @Override
    public <T> Either<Response, T> toResponse(Either<ErrorCollection, T> either) {
        return either.leftMap(new Function<ErrorCollection, Response>() {
            @Override
            public Response apply(final ErrorCollection errorCollection) {
                return errorResponse(errorCollection);
            }
        });
    }
}
