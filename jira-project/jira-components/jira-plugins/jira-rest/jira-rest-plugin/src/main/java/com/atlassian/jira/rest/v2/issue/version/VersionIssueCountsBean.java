package com.atlassian.jira.rest.v2.issue.version;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.project.version.CustomFieldWithVersionUsage;
import com.atlassian.jira.rest.v2.issue.Examples;
import com.google.common.base.Objects;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.net.URI;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * @since v4.4
 */
@SuppressWarnings({"UnusedDeclaration"})
@XmlRootElement(name = "version")
public class VersionIssueCountsBean {
    /**
     * A version bean instance used for auto-generated documentation.
     */
    static final VersionIssueCountsBean DOC_EXAMPLE;

    static {

        VersionIssueCountsBean bean = new VersionIssueCountsBean();
        bean.self = Examples.restURI("version/10000");
        bean.issuesFixedCount = 23;
        bean.issuesAffectedCount = 101;
        bean.issueCountWithCustomFieldsShowingVersion = 54;
        bean.customFieldUsage = Arrays.asList(
                new VersionUsageInCustomFields(10000, "Field1", 2),
                new VersionUsageInCustomFields(10010, "Field2", 3));

        DOC_EXAMPLE = bean;
    }

    @XmlElement
    private URI self;

    @XmlElement
    private long issuesFixedCount;

    @XmlElement
    private long issuesAffectedCount;

    @XmlElement
    private long issueCountWithCustomFieldsShowingVersion;

    @XmlElement
    private List<VersionUsageInCustomFields> customFieldUsage;

    public long getIssuesFixedCount() {
        return issuesFixedCount;
    }

    public long getIssuesAffectedCount() {
        return issuesAffectedCount;
    }

    public long getIssueCountWithCustomFieldsShowingVersion() {
        return issueCountWithCustomFieldsShowingVersion;
    }

    public List<VersionUsageInCustomFields> getCustomFieldNames() {
        return customFieldUsage;
    }

    public URI getSelf() {
        return self;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final VersionIssueCountsBean that = (VersionIssueCountsBean) o;
        return Objects.equal(issuesFixedCount, that.issuesFixedCount) &&
                Objects.equal(issuesAffectedCount, that.issuesAffectedCount) &&
                Objects.equal(issueCountWithCustomFieldsShowingVersion, that.issueCountWithCustomFieldsShowingVersion) &&
                Objects.equal(self, that.self) &&
                Objects.equal(customFieldUsage, that.customFieldUsage);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(self, issuesFixedCount, issuesAffectedCount, issueCountWithCustomFieldsShowingVersion, customFieldUsage);
    }

    //Needed so that JAXB works.
    public VersionIssueCountsBean() {
    }

    VersionIssueCountsBean(final long issuesFixedCount, final long issuesAffectedCount, final URI self, final long issueCountWithCustomFieldsShowingVersion, final List<VersionUsageInCustomFields> customFieldUsage) {
        this.self = self;
        this.issuesFixedCount = issuesFixedCount;
        this.issuesAffectedCount = issuesAffectedCount;
        this.issueCountWithCustomFieldsShowingVersion = issueCountWithCustomFieldsShowingVersion;
        this.customFieldUsage = customFieldUsage;
    }

    public static class VersionUsageInCustomFields implements CustomFieldWithVersionUsage {
        @XmlElement
        private String fieldName;
        @XmlElement
        private long customFieldId;
        @XmlElement
        private long issueCountWithVersionInCustomField;

        public VersionUsageInCustomFields(long customFieldId, String fieldName, long issueCountWithVersionInCustomField) {
            this.fieldName = fieldName;
            this.customFieldId = customFieldId;
            this.issueCountWithVersionInCustomField = issueCountWithVersionInCustomField;
        }

        @Override
        public long getCustomFieldId() {
            return customFieldId;
        }

        @Nullable
        @Override
        public String getFieldName() {
            return fieldName;
        }

        @Override
        public long getIssueCountWithVersionInCustomField() {
            return issueCountWithVersionInCustomField;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            VersionUsageInCustomFields that = (VersionUsageInCustomFields) o;

            if (customFieldId != that.customFieldId) return false;
            if (issueCountWithVersionInCustomField != that.issueCountWithVersionInCustomField) return false;
            return fieldName.equals(that.fieldName);

        }

        @Override
        public int hashCode() {
            int result = fieldName.hashCode();
            result = 31 * result + (int) (customFieldId ^ (customFieldId >>> 32));
            result = 31 * result + (int) (issueCountWithVersionInCustomField ^ (issueCountWithVersionInCustomField >>> 32));
            return result;
        }
    }

    public static class Builder {
        private URI self;
        private long issuesAffectedCount;
        private long issuesFixedCount;
        private long issueCountWithCustomFieldsShowingVersion;
        private List<VersionUsageInCustomFields> customFieldUsage;

        public URI getSelf() {
            return self;
        }

        public Builder setSelf(final URI self) {
            this.self = self;
            return this;
        }

        public Builder issuesFixedCount(final long issuesFixedCount) {
            this.issuesFixedCount = issuesFixedCount;
            return this;
        }

        public Builder issuesAffectedCount(final long issuesAffectedCount) {
            this.issuesAffectedCount = issuesAffectedCount;
            return this;
        }

        public Builder issueCountWithCustomFieldsShowingVersion(final long issueCountWithCustomFieldsShowingVersion) {
            this.issueCountWithCustomFieldsShowingVersion = issueCountWithCustomFieldsShowingVersion;
            return this;
        }

        public Builder customFieldUsage(final @Nullable Collection<CustomFieldWithVersionUsage> customFieldUsage) {
            this.customFieldUsage = customFieldUsage==null || customFieldUsage.isEmpty() ?
                    null :
                    customFieldUsage.stream()
                            .map(usage -> new VersionUsageInCustomFields(
                                    usage.getCustomFieldId(), usage.getFieldName(),
                                    usage.getIssueCountWithVersionInCustomField()))
                            .collect(CollectorsUtil.toImmutableListWithSizeOf(customFieldUsage));
            return this;
        }

        public VersionIssueCountsBean build() {
            return new VersionIssueCountsBean(issuesFixedCount, issuesAffectedCount, self, issueCountWithCustomFieldsShowingVersion, customFieldUsage);
        }
    }
}
