package com.atlassian.jira.rest.v2.issue;

import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.avatar.AvatarPickerHelper;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.project.ProjectAction;
import com.atlassian.jira.bc.project.ProjectCreationData;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponentService;
import com.atlassian.jira.bc.project.version.VersionService;
import com.atlassian.jira.blueprint.core.api.CoreProjectConfigurator;
import com.atlassian.jira.issue.fields.config.manager.IssueTypeSchemeManager;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.fields.rest.json.beans.StatusJsonBean;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.security.IssueSecuritySchemeManager;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.notification.NotificationSchemeManager;
import com.atlassian.jira.permission.PermissionSchemeManager;
import com.atlassian.jira.icon.IconType;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectCategory;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.template.ProjectTemplateKey;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.rest.api.issue.IssueTypeWithStatusJsonBean;
import com.atlassian.jira.rest.api.pagination.PageBean;
import com.atlassian.jira.rest.api.util.ErrorCollection;
import com.atlassian.jira.rest.exception.BadRequestWebException;
import com.atlassian.jira.rest.exception.NotFoundWebException;
import com.atlassian.jira.rest.util.AttachmentHelper;
import com.atlassian.jira.rest.util.ProjectFinder;
import com.atlassian.jira.rest.util.ResponseFactory;
import com.atlassian.jira.rest.util.StatusHelper;
import com.atlassian.jira.rest.v2.issue.component.ComponentBean;
import com.atlassian.jira.rest.v2.issue.project.ProjectBean;
import com.atlassian.jira.rest.v2.issue.project.ProjectBeanFactory;
import com.atlassian.jira.rest.v2.issue.project.ProjectIdentity;
import com.atlassian.jira.rest.v2.issue.project.ProjectInputBean;
import com.atlassian.jira.rest.v2.issue.version.VersionBean;
import com.atlassian.jira.rest.v2.issue.version.VersionBeanFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.xsrf.XsrfCheckResult;
import com.atlassian.jira.security.xsrf.XsrfInvocationChecker;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserProjectHistoryManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.ErrorCollections;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.NamedWithId;
import com.atlassian.jira.util.OrderByRequest;
import com.atlassian.jira.util.OrderByRequestParser;
import com.atlassian.jira.util.Page;
import com.atlassian.jira.util.PageRequest;
import com.atlassian.jira.util.PageRequests;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.web.ExecutingHttpRequest;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.plugins.rest.common.multipart.FilePart;
import com.atlassian.plugins.rest.common.multipart.MultipartFormParam;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.plugins.rest.common.security.XsrfCheckFailedException;

import com.atlassian.rest.annotation.RequestType;
import com.atlassian.rest.annotation.ResponseType;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.rest.api.http.CacheControl.never;
import static com.atlassian.jira.rest.v1.util.CacheControl.NO_CACHE;
import static com.atlassian.jira.util.ObjectUtils.firstIfNotNull;
import static com.google.common.base.MoreObjects.firstNonNull;
import static com.google.common.base.Strings.nullToEmpty;

/**
 * @since 4.2
 */
@Path("project")
@AnonymousAllowed
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class ProjectResource {
    public static final int MAX_RECENT_PROJECTS = 20;
    private final ProjectService projectService;
    private final UserManager userManager;
    private final JiraAuthenticationContext authContext;
    private final UriInfo uriInfo;
    private final ProjectManager projectManager;
    private final AvatarResourceHelper avatarResourceHelper;
    private final VersionService versionService;
    private final ProjectComponentService projectComponentService;
    private final ProjectBeanFactory projectBeanFactory;
    private final VersionBeanFactory versionBeanFactory;
    private final PermissionManager permissionManager;
    private final AvatarService avatarService;
    private final JiraBaseUrls jiraBaseUrls;
    private final WorkflowManager workflowManager;
    private final IssueTypeSchemeManager issueTypeSchemeManager;
    private final PermissionSchemeManager permissionSchemeManager;
    private final NotificationSchemeManager notificationSchemeManager;
    private final IssueSecuritySchemeManager issueSecuritySchemeManager;
    private final ResourceUriBuilder uriBuilder;
    private final StatusHelper statusHelper;
    private final ProjectFinder projectFinder;
    private final XsrfInvocationChecker xsrfChecker;
    private final ResponseFactory responses;
    private final UserProjectHistoryManager projectHistoryManager;
    private final I18nHelper i18nHelper;
    private final CoreProjectConfigurator coreProjectConfigurator;
    private final OrderByRequestParser orderByRequestParser;

    public ProjectResource(
            final ProjectService projectService,
            final JiraAuthenticationContext authContext,
            final UriInfo uriInfo,
            final VersionService versionService,
            final ProjectComponentService projectComponentService,
            final AvatarService avatarService,
            final UserManager userManager,
            final ProjectBeanFactory projectBeanFactory,
            final VersionBeanFactory versionBeanFactory,
            final PermissionManager permissionManager,
            final ProjectManager projectManager,
            final AvatarManager avatarManager,
            final AvatarPickerHelper avatarPickerHelper,
            final AttachmentHelper attachmentHelper,
            final JiraBaseUrls jiraBaseUrls,
            final WorkflowManager workflowManager,
            final IssueTypeSchemeManager issueTypeSchemeManager,
            final PermissionSchemeManager permissionSchemeManager,
            final NotificationSchemeManager notificationSchemeManager,
            final IssueSecuritySchemeManager issueSecuritySchemeManager,
            final ResourceUriBuilder uriBuilder,
            final StatusHelper statusHelper,
            final ProjectFinder projectFinder,
            final XsrfInvocationChecker xsrfChecker,
            final UserProjectHistoryManager projectHistoryManager,
            final ResponseFactory responses,
            final I18nHelper i18nHelper,
            final CoreProjectConfigurator coreProjectConfigurator,
            final OrderByRequestParser orderByRequestParser) {
        this.workflowManager = workflowManager;
        this.issueTypeSchemeManager = issueTypeSchemeManager;
        this.permissionSchemeManager = permissionSchemeManager;
        this.notificationSchemeManager = notificationSchemeManager;
        this.issueSecuritySchemeManager = issueSecuritySchemeManager;
        this.uriBuilder = uriBuilder;
        this.statusHelper = statusHelper;
        this.projectFinder = projectFinder;
        this.responses = responses;
        this.projectHistoryManager = projectHistoryManager;
        this.i18nHelper = i18nHelper;
        this.orderByRequestParser = orderByRequestParser;
        this.avatarResourceHelper = new AvatarResourceHelper(authContext, avatarManager, avatarService,
                avatarPickerHelper, attachmentHelper, userManager);
        this.permissionManager = permissionManager;
        this.avatarService = avatarService;
        this.projectService = projectService;
        this.authContext = authContext;
        this.versionService = versionService;
        this.projectComponentService = projectComponentService;
        this.userManager = userManager;
        this.projectBeanFactory = projectBeanFactory;
        this.versionBeanFactory = versionBeanFactory;
        this.uriInfo = uriInfo;
        this.projectManager = projectManager;
        this.jiraBaseUrls = jiraBaseUrls;
        this.xsrfChecker = xsrfChecker;
        this.coreProjectConfigurator = coreProjectConfigurator;
    }

    /**
     * Contains a full representation of a project in JSON format.
     * <p>
     * All project keys associated with the project will only be returned if <code>expand=projectKeys</code>.
     * <p>
     *
     * @param projectIdOrKey the project id or project key
     * @param expand         the parameters to expand
     * @return a project
     * @response.representation.200.qname project
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc Returned if the project exists and the user has permission to view it. Contains a full representation
     * of a project in JSON format.
     * @response.representation.200.example {@link com.atlassian.jira.rest.v2.issue.project.ProjectBean#DOC_EXAMPLE}
     * @response.representation.404.doc Returned if the project is not found, or the calling user does not have permission to view it.
     */
    @GET
    @ResponseType (ProjectBean.class)
    @Path("{projectIdOrKey}")
    public Response getProject(@PathParam("projectIdOrKey") final String projectIdOrKey, @QueryParam("expand") final String expand) {
        return getProjectForView(projectIdOrKey).left().on(new Function<Project, Response>() {
            @Override
            public Response apply(final Project project) {
                return Response.ok(projectBeanFactory.fullProject(project, StringUtils.defaultString(expand))).cacheControl(never()).build();
            }
        });
    }

    /**
     * Creates a new project.
     *
     * @param project new project details
     * @return The {@link com.atlassian.jira.rest.v2.issue.project.ProjectIdentity} of newly created project.
     * @response.representation.201.qname project
     * @response.representation.201.mediaType application/json
     * @response.representation.201.doc Returned if the project creation was successful.
     * @response.representation.201.example {@link com.atlassian.jira.rest.v2.issue.project.ProjectIdentity#DOC_EXAMPLE}
     * @response.representation.400.doc Returned if the request is not valid and the project could not be created.
     * @response.representation.401.doc Returned if the user is not logged in.
     * @response.representation.403.doc Returned if the user does not have rights to create projects.
     * @request.representation.example {@link com.atlassian.jira.rest.v2.issue.project.ProjectInputBean#CREATE_EXAMPLE}
     */
    @POST
    @ResponseType(ProjectIdentity.class)
    public Response createProject(final ProjectInputBean project) {
        if (authContext.isLoggedInUser()) {
            ProjectCreationData projectCreationData = toProjectCreationData().apply(project);
            final ProjectService.CreateProjectValidationResult validationResult = projectService.validateCreateProject(
                    authContext.getUser(),
                    projectCreationData
            );

            if (validationResult.isValid()) {

                final ProjectService.UpdateProjectSchemesValidationResult schemesValidationResult = projectService.validateUpdateProjectSchemes(
                        authContext.getUser(), project.getPermissionScheme(), project.getNotificationScheme(), project.getIssueSecurityScheme());

                if (schemesValidationResult.isValid()) {
                    return loadProjectCategory(project.getCategoryId()).left().on(new Function<Option<ProjectCategory>, Response>() {
                        @Override
                        public Response apply(@Nullable final Option<ProjectCategory> maybeCategory) {
                            Project createdProject = projectService.createProject(validationResult);
                            projectService.updateProjectSchemes(schemesValidationResult, createdProject);
                            if (shouldApplyCoreProjectConfiguration(projectCreationData)) {
                                coreProjectConfigurator.configure(createdProject);

                            }
                            ProjectIdentity responseEntity = projectBeanFactory.projectIdentity(createdProject);
                            if (maybeCategory.isDefined()) {
                                projectManager.setProjectCategory(createdProject, maybeCategory.get());
                            }
                            return Response.status(Response.Status.CREATED).location(responseEntity.getSelf()).entity(responseEntity).build();
                        }
                    });
                } else {
                    return responses.errorResponse(schemesValidationResult.getErrorCollection());
                }
            } else {
                return responses.errorResponse(validationResult.getErrorCollection());
            }
        } else {
            return responses.notLoggedInResponse();
        }
    }

    private boolean shouldApplyCoreProjectConfiguration(ProjectCreationData projectCreationData) {
        ProjectTemplateKey projectTemplateKey = projectCreationData.getProjectTemplateKey();
        return projectTemplateKey == null || StringUtils.isEmpty(projectTemplateKey.getKey());
    }

    private Function<ProjectInputBean, ProjectCreationData> toProjectCreationData() {
        return new Function<ProjectInputBean, ProjectCreationData>() {
            @Override
            public ProjectCreationData apply(final ProjectInputBean projectInputBean) {
                ProjectCreationData.Builder builder = new ProjectCreationData.Builder()
                        .withName(projectInputBean.getName())
                        .withKey(projectInputBean.getKey())
                        .withDescription(projectInputBean.getDescription())
                        .withLead(userManager.getUserByName(projectInputBean.getLead()))
                        .withUrl(projectInputBean.getUrl())
                        .withAssigneeType(projectInputBean.getAssigneeType().getId())
                        .withAvatarId(projectInputBean.getAvatarId())
                        .withProjectTemplateKey(projectInputBean.getProjectTemplateKey())
                        .withType(projectInputBean.getProjectTypeKey());
                return builder.build();
            }
        };
    }

    private Either<Response, Option<ProjectCategory>> loadProjectCategory(Long categoryId) {
        if (categoryId != null) {
            ProjectCategory category = projectManager.getProjectCategoryObject(categoryId);
            if (category != null) {
                return Either.right(Option.some(category));
            } else {
                return Either.left(responses.errorResponse(
                        ErrorCollections.validationError("projectCategory", i18nHelper.getText("admin.errors.project.category.does.not.exist"))));
            }
        } else {
            return Either.right(Option.<ProjectCategory>none());
        }
    }

    /**
     * Updates a project.
     * <p>
     * Only non null values sent in JSON will be updated in the project.</p>
     * <p>
     * Values available for the assigneeType field are: "PROJECT_LEAD" and "UNASSIGNED".</p>
     *
     * @param projectIdOrKey identity of the project to update
     * @param expand         the parameters to expand in returned project
     * @param updateData     new values for the project
     * @return updated project details.
     * @response.representation.201.qname project
     * @response.representation.201.mediaType application/json
     * @response.representation.201.doc Returned if the project update was successful.
     * @response.representation.201.example {@link com.atlassian.jira.rest.v2.issue.project.ProjectBean#DOC_EXAMPLE}
     * @response.representation.400.doc Returned if the request is not valid and the project could not be updated.
     * @response.representation.401.doc Returned if the user is not logged in.
     * @response.representation.403.doc Returned if the user does not have rights to update projects.
     * @response.representation.404.doc Returned if the project does not exist.
     * @request.representation.example {@link com.atlassian.jira.rest.v2.issue.project.ProjectInputBean#CREATE_EXAMPLE}
     */
    @PUT
    @Path("{projectIdOrKey}")
    @ResponseType(ProjectBean.class)
    public Response updateProject(@PathParam("projectIdOrKey") final String projectIdOrKey, @QueryParam("expand") final String expand, final ProjectInputBean updateData) {
        if (!authContext.isLoggedInUser()) {
            return responses.notLoggedInResponse();
        }

        return getProjectForEdit(projectIdOrKey).left().on(projectToEdit -> {
            final ProjectService.UpdateProjectValidationResult basicValidationResult = projectService.validateUpdateProject(
                    authContext.getLoggedInUser(),
                    requestProjectUpdate(projectToEdit, updateData)
            );
            if (!basicValidationResult.isValid()) {
                return responses.errorResponse(basicValidationResult.getErrorCollection());
            }
            final ProjectService.UpdateProjectSchemesValidationResult schemesValidationResult = projectService.validateUpdateProjectSchemes(
                    authContext.getUser(),
                    firstNotNullId(updateData.getPermissionScheme(), permissionSchemeManager.getSchemeFor(projectToEdit)),
                    firstNotNullId(updateData.getNotificationScheme(), notificationSchemeManager.getSchemeFor(projectToEdit)),
                    firstNotNullId(updateData.getIssueSecurityScheme(), issueSecuritySchemeManager.getSchemeFor(projectToEdit))
            );
            if (!schemesValidationResult.isValid()) {
                return responses.errorResponse(schemesValidationResult.getErrorCollection());
            }

            return loadProjectCategory(updateData.getCategoryId()).left().on(maybeProjectCategory -> {
                Project project = projectService.updateProject(basicValidationResult);
                if (maybeProjectCategory.isDefined()) {
                    projectManager.setProjectCategory(project, maybeProjectCategory.get());
                }
                projectService.updateProjectSchemes(schemesValidationResult, project);
                return responses.okNoCache(projectBeanFactory.fullProject(getProjectForView(projectIdOrKey).right().get(), nullToEmpty(expand)));
            });
        });
    }

    private static ProjectService.UpdateProjectRequest requestProjectUpdate(Project original, ProjectInputBean updateData) {
        final ProjectService.UpdateProjectRequest request = new ProjectService.UpdateProjectRequest(original);
        request.name(updateData.getName());
        request.key(updateData.getKey());
        request.description(updateData.getDescription());
        request.url(updateData.getUrl());
        request.assigneeType(updateData.getAssigneeTypeOrNull());
        request.avatarId(updateData.getAvatarId());
        request.leadUsername(updateData.getLead());
        return request;
    }

    private static Long firstNotNullId(Long first, NamedWithId second) {
        return first != null ? first : (second != null ? second.getId() : null);
    }

    /**
     * Updates the type of a project.
     *
     * @param projectIdOrKey    identity of the project to update
     * @param newProjectTypeKey The key of the new project type
     * @return A response that will contain the updated project if the operation was successful.
     * @response.representation.200.qname project
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc Returned if the update to the project type was successful.
     * @response.representation.200.example {@link com.atlassian.jira.rest.v2.issue.project.ProjectBean#DOC_EXAMPLE}
     * @response.representation.400.doc Returned if the request is not valid and the project type could not be updated.
     * @response.representation.401.doc Returned if the user is not logged in.
     * @response.representation.403.doc Returned if the user does not have rights to update projects.
     * @response.representation.404.doc Returned if the project does not exist.
     */
    @PUT
    @Path("{projectIdOrKey}/type/{newProjectTypeKey}")
    @ResponseType(ProjectBean.class)
    public Response updateProjectType(@PathParam("projectIdOrKey") final String projectIdOrKey, @PathParam("newProjectTypeKey") final String newProjectTypeKey) {
        if (!authContext.isLoggedInUser()) {
            return responses.notLoggedInResponse();
        }
        return getProjectForEdit(projectIdOrKey).left().on(new Function<Project, Response>() {
            @Override
            public Response apply(@Nullable final Project project) {
                Either<Project, com.atlassian.jira.util.ErrorCollection> updateResult = projectService.updateProjectType(authContext.getLoggedInUser(), project, new ProjectTypeKey(newProjectTypeKey));
                if (updateResult.isLeft()) {
                    return responses.okNoCache(projectBeanFactory.fullProject(updateResult.left().get(), ""));
                }
                return responses.errorResponse(updateResult.right().get());
            }
        });
    }

    /**
     * Deletes a project.
     *
     * @param projectIdOrKey Project id or project key
     * @return no content response
     * @response.representation.204.mediaType application/json
     * @response.representation.204.doc Returned if the project is successfully deleted.
     * @response.representation.401.doc Returned if the user is not logged in.
     * @response.representation.403.doc Returned if the currently authenticated user does not have permission to delete the project.
     * @response.representation.404.doc Returned if the project does not exist.
     */
    @DELETE
    @Path("{projectIdOrKey}")
    public Response deleteProject(@PathParam("projectIdOrKey") final String projectIdOrKey) {
        if (!authContext.isLoggedInUser()) {
            return responses.notLoggedInResponse();
        }

        return getProjectForEdit(projectIdOrKey).left().on(new Function<Project, Response>() {
            @Override
            public Response apply(final Project project) {
                ProjectService.DeleteProjectValidationResult validationResult = projectService.validateDeleteProject(authContext.getUser(), project.getKey());
                if (validationResult.isValid()) {
                    ProjectService.DeleteProjectResult result = projectService.deleteProject(authContext.getUser(), validationResult);
                    return result.isValid() ? responses.noContent() : responses.errorResponse(result.getErrorCollection());
                } else {
                    return responses.errorResponse(validationResult.getErrorCollection());
                }
            }
        });
    }

    /**
     * Contains a full representation of a the specified project's versions.
     *
     * @param projectIdOrKey the project id or project key
     * @param expand         the parameters to expand
     * @return the passed project's versions.
     * @response.representation.200.qname versions
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc Returned if the project exists and the user has permission to view its versions. Contains a full
     * representation of the project's versions in JSON format.
     * @response.representation.200.example {@link com.atlassian.jira.rest.v2.issue.version.VersionBean#DOC_EXAMPLE_LIST}
     * @response.representation.404.doc Returned if the project is not found, or the calling user does not have permission to view it.
     */
    @GET
    @ResponseType (value = List.class, genericTypes = VersionBean.class)
    @Path("{projectIdOrKey}/versions")
    public Response getProjectVersions(@PathParam("projectIdOrKey") final String projectIdOrKey, @QueryParam("expand") final String expand) {
        return getProjectForView(projectIdOrKey).left().on(new Function<Project, Response>() {
            @Override
            public Response apply(final Project project) {
                final VersionService.VersionsResult versionResult = versionService.getVersionsByProject(authContext.getUser(), project);
                if (!versionResult.isValid()) {
                    throw new NotFoundWebException(ErrorCollection.of(versionResult.getErrorCollection()));
                }

                final boolean expandOps = expand != null && expand.contains("operations");
                return responses.okNoCache(versionBeanFactory.createVersionBeans(versionResult.getVersions(), expandOps));
            }
        });
    }

    /**
     * Returns all versions for the specified project. Results are <a href="#pagination">paginated</a>.
     * <p>
     * Results can be ordered by the following fields:
     * <ul>
     * <li>sequence</li>
     * <li>name</li>
     * <li>startDate</li>
     * <li>releaseDate</li>
     * </ul>
     * </p>
     *
     * @param projectIdOrKey the project id or project key
     * @param startAt        the page offset, if not specified then defaults to 0
     * @param maxResults     how many results on the page should be included. Defaults to 50.
     * @param orderBy        ordering of the results.
     * @param expand         the parameters to expand
     * @return the passed project's versions.
     * @response.representation.200.qname versions
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc One page of versions for the project.
     * @response.representation.200.example {@link com.atlassian.jira.rest.v2.issue.version.VersionBean#PAGED_EXAMPLE}
     * @response.representation.404.doc Returned if the project is not found, or the calling user does not have permission to view it.
     */
    @GET
    @Path("{projectIdOrKey}/version")
    @ResponseType(value = PageBean.class, genericTypes = VersionBean.class)
    public Response getProjectVersionsPaginated(
            @PathParam("projectIdOrKey") final String projectIdOrKey,
            @QueryParam("startAt") final Long startAt,
            @QueryParam("maxResults") final Integer maxResults,
            @QueryParam("orderBy") final String orderBy,
            @QueryParam("expand") final String expand) {
        return getProjectForView(projectIdOrKey).left().on(new Function<Project, Response>() {
            @Override
            public Response apply(final Project project) {
                PageRequest pageRequest = PageRequests.request(startAt, firstNonNull(maxResults, 50));
                OrderByRequest<VersionService.VersionExtractableField> orderByRequest = null;
                if (orderBy != null) {
                    Either<com.atlassian.jira.util.ErrorCollection, OrderByRequest<VersionService.VersionExtractableField>> parsedOrderBy = orderByRequestParser.parse(orderBy, VersionService.VersionExtractableField.class);
                    if (parsedOrderBy.isLeft()) {
                        throw new BadRequestWebException(ErrorCollection.of(parsedOrderBy.left().get()));
                    }
                    orderByRequest = parsedOrderBy.right().get();
                }

                ServiceOutcome<Page<Version>> pagedVersions = versionService.getVersionsByProject(authContext.getUser(), project, pageRequest, orderByRequest);

                if (!pagedVersions.isValid()) {
                    throw new RESTException(ErrorCollection.of(pagedVersions.getErrorCollection()));
                }

                final boolean expandOps = expand != null && expand.contains(VersionBean.EXPAND_OPERATIONS);
                final boolean expandRemoteLinks = expand != null && expand.contains(VersionBean.EXPAND_REMOTE_LINKS);

                return Response.ok(PageBean.from(pageRequest, pagedVersions.get())
                        .setLinks(baseSelfForPagedVersions(project.getKey(), orderBy), pageRequest.getLimit())
                        .build(new java.util.function.Function<Version, VersionBean>() {
                            @Override
                            public VersionBean apply(final Version input) {
                                return versionBeanFactory.createVersionBean(input, expandOps, expandRemoteLinks);
                            }
                        }))
                        .cacheControl(never())
                        .build();
            }
        });
    }

    private String baseSelfForPagedVersions(String projectKey, String orderByQuery) {
        UriBuilder withoutOrderBy = UriBuilder.fromPath(jiraBaseUrls.restApi2BaseUrl())
                .path(ProjectResource.class)
                .path(projectKey)
                .path("version");
        if (orderByQuery != null) {
            return withoutOrderBy.queryParam("orderBy", orderByQuery).build().toString();
        }
        return withoutOrderBy.build().toString();
    }

    /**
     * Contains a full representation of a the specified project's components.
     *
     * @param projectIdOrKey the project id or project key
     * @return the passed project's components.
     * @response.representation.200.qname components
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc Returned if the project exists and the user has permission to view its components. Contains a full
     * representation of the project's components in JSON format.
     * @response.representation.200.example {@link com.atlassian.jira.rest.v2.issue.component.ComponentBean#DOC_EXAMPLE_LIST}
     * @response.representation.404.doc Returned if the project is not found, or the calling user does not have permission to view it.
     */
    @GET
    @ResponseType (value = List.class, genericTypes = ComponentBean.class)
    @Path("{projectIdOrKey}/components")
    public Response getProjectComponents(@PathParam("projectIdOrKey") final String projectIdOrKey) {
        return getProjectForView(projectIdOrKey).left().on(new Function<Project, Response>() {
            @Override
            public Response apply(final Project project) {
                final SimpleErrorCollection errorCollection = new SimpleErrorCollection();
                final Collection<ProjectComponent> projectComponents = projectComponentService.findAllForProject(errorCollection, project.getId());
                if (errorCollection.hasAnyErrors()) {
                    throw new NotFoundWebException(ErrorCollection.of(errorCollection));
                }

                return responses.okNoCache(
                        ComponentBean.asFullBeans(project, projectComponents, jiraBaseUrls, userManager, avatarService, permissionManager, projectManager));
            }
        });
    }

    /**
     * Returns all projects which are visible for the currently logged in user. If no user is logged in, it returns the
     * list of projects that are visible when using anonymous access.
     *
     * @param expand the parameters to expand
     * @param recent if this parameter is set then only projects recently accessed by the current user (if not logged in then based on HTTP session) will be returned (maximum count limited to the specified number but no more than 20).
     * @return all projects for which the user has the BROWSE project permission. If no user is logged in,
     * it returns all projects, which are visible when using anonymous access.
     * @response.representation.200.qname projects
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc Returns a list of projects for which the user has the BROWSE, ADMINISTER or PROJECT_ADMIN project permission.
     * @response.representation.200.example {@link com.atlassian.jira.rest.v2.issue.project.ProjectBean#PROJECTS_EXAMPLE}
     * @since v4.3
     */
    @GET
    @ResponseType (value = List.class, genericTypes = ProjectBean.class)
    public Response getAllProjects(@QueryParam("expand") final String expand, @QueryParam("recent") Integer recent) {
        return (recent != null ? getMostRecentProjects(recent) : getAllProjects()).left().on(projectListAsResponse(expand));
    }

    private Either<Response, List<Project>> getAllProjects() {
        return responses.validateOutcome(projectService.getAllProjectsForAction(authContext.getUser(), ProjectAction.VIEW_PROJECT));
    }

    private Either<Response, Iterable<Project>> getMostRecentProjects(Integer count) {
        if (count < 0) {
            return Either.left(responses.errorResponse(ErrorCollections.validationError("recent", i18nHelper.getText("rest.validation.error.negative.number"))));
        } else {
            List<Project> recentProjects = projectHistoryManager.getProjectHistoryWithPermissionChecks(ProjectAction.VIEW_PROJECT, authContext.getUser());
            return Either.right(Iterables.limit(recentProjects, Math.min(count, MAX_RECENT_PROJECTS)));
        }
    }

    private Function<Iterable<Project>, Response> projectListAsResponse(final String expand) {
        return new Function<Iterable<Project>, Response>() {
            @Override
            public Response apply(@Nullable final Iterable<Project> projects) {
                return responses.okNoCache(ImmutableList.copyOf(Iterables.transform(projects, projectBeanFactory.summaryProject(expand))));
            }
        };
    }

    /**
     * Returns all avatars which are visible for the currently logged in user.  The avatars are grouped into
     * system and custom.
     *
     * @param projectIdOrKey project id or project key
     * @return all avatars for which the user has the BROWSE project permission.
     * @response.representation.200.qname avatars
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc Returns a map containing a list of avatars for both custom an system avatars, which the user has the BROWSE project permission.
     * @response.representation.200.example {@link AvatarBean#DOC_EXAMPLE_LIST}
     * @response.representation.404.doc Returned if the currently authenticated user does not have VIEW PROJECT permission.
     * @since v5.0
     */
    @GET
    @Path("{projectIdOrKey}/avatars")
    @ResponseType(value = Map.class, genericTypes = {String.class, AvatarBean[].class})
    public Response getAllAvatars(@PathParam("projectIdOrKey") final String projectIdOrKey) {
        return getProjectForView(projectIdOrKey).left().on(new Function<Project, Response>() {
            @Override
            public Response apply(final Project project) {
                final Avatar selectedAvatar = project.getAvatar();
                final Long selectedAvatarId = selectedAvatar.getId();

                return responses.okNoCache(avatarResourceHelper.getAllAvatars(IconType.PROJECT_ICON_TYPE, project.getId().toString(), selectedAvatarId));
            }
        });
    }

    /**
     * Converts temporary avatar into a real avatar
     *
     * @param projectIdOrKey       project id or project key
     * @param croppingInstructions cropping instructions
     * @return created avatar
     * @request.representation.example {@link com.atlassian.jira.rest.v2.issue.AvatarCroppingBean#DOC_EDIT_EXAMPLE}
     * @response.representation.201.qname avatar
     * @response.representation.201.mediaType application/json
     * @response.representation.201.doc Returns created avatar
     * @response.representation.201.example {@link AvatarBean#DOC_EXAMPLE}
     * @response.representation.400.doc Returned if the cropping coordinates are invalid
     * @response.representation.403.doc Returned if the currently authenticated user does not have permission to pick avatar
     * @response.representation.404.doc Returned if the currently authenticated user does not have EDIT PROJECT permission.
     */
    @POST
    @Path("{projectIdOrKey}/avatar")
    @RequestType (AvatarCroppingBean.class)
    @ResponseType (AvatarBean.class)
    public Response createAvatarFromTemporary(@PathParam("projectIdOrKey") final String projectIdOrKey, final AvatarCroppingBean croppingInstructions) {
        return getProjectForEdit(projectIdOrKey).left().on(new Function<Project, Response>() {
            @Override
            public Response apply(final Project project) {
                return avatarResourceHelper.createAvatarFromTemporary(IconType.PROJECT_ICON_TYPE, project.getId().toString(), croppingInstructions);
            }
        });
    }

    @PUT
    @Path("{projectIdOrKey}/avatar")
    @ResponseType(Void.class)
    public Response updateProjectAvatar(final @PathParam("projectIdOrKey") String projectIdOrKey, final AvatarBean avatarBean) {
        return getProjectForEdit(projectIdOrKey).left().on(new Function<Project, Response>() {
            @Override
            public Response apply(final Project project) {
                final String id = avatarBean.getId();
                Long avatarId;
                try {
                    avatarId = id == null ? null : Long.valueOf(id);
                } catch (NumberFormatException e) {
                    avatarId = null;
                }
                final ProjectService.UpdateProjectValidationResult updateProjectValidationResult =
                        projectService.validateUpdateProject(authContext.getUser(), project.getName(), project.getKey(),
                                project.getDescription(), project.getLeadUserName(), project.getUrl(), project.getAssigneeType(),
                                avatarId);

                if (!updateProjectValidationResult.isValid()) {
                    throwWebException(updateProjectValidationResult.getErrorCollection());
                }

                projectService.updateProject(updateProjectValidationResult);
                return responses.noContent();
            }
        });
    }

    /**
     * Creates temporary avatar
     *
     * @param projectIdOrKey Project id or project key
     * @param filename       name of file being uploaded
     * @param size           size of file
     * @param request        servlet request
     * @return temporary avatar cropping instructions
     * @response.representation.201.qname avatar
     * @response.representation.201.mediaType application/json
     * @response.representation.201.doc temporary avatar cropping instructions
     * @response.representation.201.example {@link com.atlassian.jira.rest.v2.issue.AvatarCroppingBean#DOC_EXAMPLE}
     * @response.representation.403.doc Returned if the request does not contain a valid XSRF token
     * @response.representation.400.doc Validation failed. For example filesize is beyond max attachment size.
     * @response.representation.404.doc Returned if the currently authenticated user does not have EDIT PROJECT permission.
     * @since v5.0
     */
    @POST
    @Consumes(MediaType.WILDCARD)
    @ResponseType (AvatarCroppingBean.class)
    @Path("{projectIdOrKey}/avatar/temporary")
    public Response storeTemporaryAvatar(final @PathParam("projectIdOrKey") String projectIdOrKey, final @QueryParam("filename") String filename,
                                         final @QueryParam("size") Long size, final @Context HttpServletRequest request) {
        final XsrfCheckResult xsrfCheckResult = xsrfChecker.checkWebRequestInvocation(ExecutingHttpRequest.get());
        if (xsrfCheckResult.isRequired() && !xsrfCheckResult.isValid()) {
            throw new XsrfCheckFailedException();
        }

        return getProjectForEdit(projectIdOrKey).left().on(new Function<Project, Response>() {
            @Override
            public Response apply(final Project project) {
                return avatarResourceHelper.storeTemporaryAvatar(IconType.PROJECT_ICON_TYPE, project.getId().toString(), filename, size, request);
            }
        });
    }

    /**
     * Creates temporary avatar using multipart. The response is sent back as JSON stored in a textarea. This is because
     * the client uses remote iframing to submit avatars using multipart. So we must send them a valid HTML page back from
     * which the client parses the JSON.
     *
     * @param projectIdOrKey Project id or project key
     * @param request        servlet request
     * @return temporary avatar cropping instructions
     * @response.representation.201.qname avatar
     * @response.representation.201.mediaType text/html
     * @response.representation.201.doc temporary avatar cropping instructions embeded in HTML page. Error messages will also be embeded in the page.
     * @response.representation.201.example {@link com.atlassian.jira.rest.v2.issue.AvatarCroppingBean#DOC_EXAMPLE}
     * @response.representation.404.doc Returned if the currently authenticated user does not have EDIT PROJECT permission.
     * @since v5.0
     */
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("{projectIdOrKey}/avatar/temporary")
    @Produces({MediaType.TEXT_HTML})
    @ResponseType (AvatarCroppingBean.class)
    public Response storeTemporaryAvatarUsingMultiPart(@PathParam("projectIdOrKey") String projectIdOrKey, final @MultipartFormParam("avatar") FilePart filePart, final @Context HttpServletRequest request) {
        final XsrfCheckResult xsrfCheckResult = xsrfChecker.checkWebRequestInvocation(ExecutingHttpRequest.get());
        if (xsrfCheckResult.isRequired() && !xsrfCheckResult.isValid()) {
            throw new XsrfCheckFailedException();
        }

        return getProjectForEdit(projectIdOrKey).left().on(new Function<Project, Response>() {
            @Override
            public Response apply(final Project project) {
                return avatarResourceHelper.storeTemporaryAvatarUsingMultiPart(IconType.PROJECT_ICON_TYPE, project.getId().toString(), filePart, request);
            }
        });
    }

    /**
     * Deletes avatar
     *
     * @param projectIdOrKey Project id or project key
     * @param id             database id for avatar
     * @return no content response
     * @response.representation.204.mediaType application/json
     * @response.representation.204.doc Returned if the avatar is successfully deleted.
     * @response.representation.403.doc Returned if the currently authenticated user does not have permission to delete the component.
     * @response.representation.404.doc Returned if the avatar does not exist or the currently authenticated user does not have permission to
     * delete it.
     * @since v5.0
     */
    @DELETE
    @Path("{projectIdOrKey}/avatar/{id}")
    public Response deleteAvatar(final @PathParam("projectIdOrKey") String projectIdOrKey, final @PathParam("id") Long id) {
        return getProjectForEdit(projectIdOrKey).left().on(new Function<Project, Response>() {
            @Override
            public Response apply(final Project project) {
                return avatarResourceHelper.deleteAvatar(id);
            }
        });
    }

    /**
     * Get all issue types with valid status values for a project
     *
     * @param projectIdOrKey Project id or project key
     * @return collection of issue types with possi
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc Returned if the project exists and the user has permission to view its components. Contains a full
     * representation of issue types with status values which are valid for each issue type.
     * @response.representation.200.example {@link com.atlassian.jira.rest.v2.issue.IssueTypeWithStatusJsonBeanExample#DOC_EXAMPLE}
     * @response.representation.400.doc Returned if the project is not found, or the calling user does not have permission to view it.
     * @since v6.0
     */
    @GET
    @ResponseType (value = List.class, genericTypes = IssueTypeWithStatusJsonBean.class)
    @Path("{projectIdOrKey}/statuses")
    public Response getAllStatuses(@PathParam("projectIdOrKey") final String projectIdOrKey) {
        return getProjectForView(projectIdOrKey).left().on(new Function<Project, Response>() {
            @Override
            public Response apply(final Project project) {
                final Collection<IssueType> issueTypesForProject = issueTypeSchemeManager.getIssueTypesForProject(project);
                final Collection<IssueTypeWithStatusJsonBean> issueTypesWithStatuses = Lists.newArrayList();

                for (final IssueType issueType : issueTypesForProject) {
                    final JiraWorkflow workflow = workflowManager.getWorkflow(project.getId(), issueType.getId());
                    final ImmutableList<StatusJsonBean> statusJsonBeans = ImmutableList.copyOf(
                            Iterables.transform(workflow.getLinkedStatusObjects(), new Function<Status, StatusJsonBean>() {
                                @Override
                                public StatusJsonBean apply(final Status status) {
                                    return statusHelper.createStatusBean(status, uriInfo, StatusResource.class);
                                }
                            }));

                    issueTypesWithStatuses.add(createIssueTypeWithStatuses(issueType, statusJsonBeans));
                }
                return Response.ok(issueTypesWithStatuses).cacheControl(NO_CACHE).build();
            }
        });
    }

    private void throwWebException(final com.atlassian.jira.util.ErrorCollection errorCollection) {
        throw new RESTException(ErrorCollection.of(errorCollection));
    }

    private IssueTypeWithStatusJsonBean createIssueTypeWithStatuses(final IssueType issueType, final ImmutableList<StatusJsonBean> statusJsonBeans) {
        return new IssueTypeWithStatusJsonBean(
                uriBuilder.build(uriInfo, IssueTypeResource.class, issueType.getId()).toString(),
                issueType.getId(),
                issueType.getName(),
                issueType.isSubTask(),
                statusJsonBeans);
    }

    private Either<Response, Project> getProjectForView(final String projectIdOrKey) {
        return getEitherProjectOrErrors(projectIdOrKey, ProjectAction.VIEW_PROJECT);
    }

    private Either<Response, Project> getProjectForEdit(final String projectIdOrKey) {
        return getEitherProjectOrErrors(projectIdOrKey, ProjectAction.EDIT_PROJECT_CONFIG);
    }

    private Either<Response, Project> getEitherProjectOrErrors(final String projectIdOrKey, final ProjectAction action) {
        final ProjectService.GetProjectResult projectResult = projectFinder.getGetProjectForActionByIdOrKey(authContext.getUser(), projectIdOrKey, action);

        if (!projectResult.isValid()) {
            return Either.left(responses.errorResponse(projectResult.getErrorCollection()));
        }
        return Either.right(projectResult.getProject());
    }
}
