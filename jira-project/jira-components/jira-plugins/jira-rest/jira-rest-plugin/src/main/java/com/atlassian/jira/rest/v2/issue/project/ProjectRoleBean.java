package com.atlassian.jira.rest.v2.issue.project;

import com.atlassian.jira.util.collect.CollectionBuilder;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.net.URI;
import java.util.Collection;

/**
 * @since v4.4
 * @deprecated THIS IS A COPY OF {@link com.atlassian.jira.rest.api.project.ProjectRoleBean This class is deprecated and will be removed with 7.0.
 */
@Deprecated
@SuppressWarnings({"UnusedDeclaration"})
@XmlRootElement(name = "projectRole")
public class ProjectRoleBean {
    @XmlElement
    public URI self;

    @XmlElement
    public String name;

    @XmlElement
    public Long id;

    @XmlElement
    public String description;

    @XmlElement
    public Collection<RoleActorBean> actors;

    public static final ProjectRoleBean DOC_EXAMPLE;

    // This example does not contain URI or actors.
    public static final ProjectRoleBean ROLE_SIMPLE_EXAMPLE;

    static {
        DOC_EXAMPLE = new ProjectRoleBean();
        long id = 10360;
        DOC_EXAMPLE.self = URI.create("http://www.example.com/jira/rest/api/2/project/MKY/role/" + id);
        DOC_EXAMPLE.id = id;
        DOC_EXAMPLE.name = "Developers";
        DOC_EXAMPLE.description = "A project role that represents developers in a project";
        DOC_EXAMPLE.actors = CollectionBuilder.list(RoleActorBean.DOC_EXAMPLE);

        ROLE_SIMPLE_EXAMPLE = new ProjectRoleBean();
        ROLE_SIMPLE_EXAMPLE.id = id;
        ROLE_SIMPLE_EXAMPLE.name = "Developers";
        ROLE_SIMPLE_EXAMPLE.description = "A project role that represents developers in a project";
    }
}
