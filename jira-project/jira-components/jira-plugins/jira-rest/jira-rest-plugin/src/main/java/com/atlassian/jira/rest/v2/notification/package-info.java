@InterceptorChain({RequestScopeInterceptor.class, ExceptionInterceptor.class})
@CorsAllowed package com.atlassian.jira.rest.v2.notification;

import com.atlassian.jira.rest.exception.ExceptionInterceptor;
import com.atlassian.jira.rest.v2.issue.scope.RequestScopeInterceptor;
import com.atlassian.plugins.rest.common.interceptor.InterceptorChain;
import com.atlassian.plugins.rest.common.security.CorsAllowed;
