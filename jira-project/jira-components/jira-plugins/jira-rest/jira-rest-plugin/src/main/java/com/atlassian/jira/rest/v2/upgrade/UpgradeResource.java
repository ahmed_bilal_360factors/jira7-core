package com.atlassian.jira.rest.v2.upgrade;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.cluster.zdu.ClusterUpgradeStateManager;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.upgrade.UpgradeScheduler;
import com.atlassian.jira.upgrade.UpgradeResult;
import com.atlassian.jira.upgrade.UpgradeService;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.rest.annotation.ResponseType;
import com.atlassian.scheduler.SchedulerHistoryService;
import com.atlassian.scheduler.status.RunDetails;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.Optional;

import static com.atlassian.jira.rest.api.http.CacheControl.never;

/**
 * REST resource for executing and querying delayed upgrades.
 *
 * @since 6.4
 */
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@Path("upgrade")
public class UpgradeResource {
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final PermissionManager permissionManager;
    private final UpgradeService upgradeService;
    private final UpgradeScheduler upgradeScheduler;
    private final ClusterUpgradeStateManager clusterUpgradeStateManager;
    private final JiraBaseUrls jiraBaseUrls;
    private final SchedulerHistoryService schedulerHistoryService;

    public UpgradeResource(
            final JiraAuthenticationContext jiraAuthenticationContext,
            final PermissionManager permissionManager,
            final UpgradeService upgradeService,
            final ClusterUpgradeStateManager clusterUpgradeStateManager,
            final JiraBaseUrls jiraBaseUrls,
            final SchedulerHistoryService schedulerHistoryService
    ) {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.permissionManager = permissionManager;
        this.upgradeService = upgradeService;
        this.upgradeScheduler = ComponentAccessor.getComponent(UpgradeScheduler.class);
        this.clusterUpgradeStateManager = clusterUpgradeStateManager;
        this.jiraBaseUrls = jiraBaseUrls;
        this.schedulerHistoryService = schedulerHistoryService;
    }

    /**
     * Runs any pending delayed upgrade tasks.  Need Admin permissions to do this.
     *
     * @return OK response if successful, array of error messages if schedule fails.
     */
    @POST
    @ResponseType(Void.class)
    public Response runUpgradesNow() {
        final ApplicationUser user = jiraAuthenticationContext.getUser();
        if (!permissionManager.hasPermission(Permissions.SYSTEM_ADMIN, user)) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        if (clusterUpgradeStateManager.areDelayedUpgradesHandledByCluster()) {
            return Response.status(Response.Status.CONFLICT).entity("ZDU cluster upgrade in progress").build();
        }

        UpgradeResult status = upgradeScheduler.scheduleUpgrades(0); //0->run immediately
        if (status.successful()) {
            URI location = UriBuilder.fromPath(jiraBaseUrls.restApi2BaseUrl()).path("upgrade").build();
            return Response.status(Response.Status.ACCEPTED).location(location).
                    header("Retry-After", 10).cacheControl(never()).build();
        } else {
            return Response.serverError().cacheControl(never()).entity(status.getErrors()).build();
        }
    }

    /**
     * Returns the result of the last upgrade task.
     *
     * Returns {@link javax.ws.rs.core.Response#seeOther(java.net.URI)} if still running.
     *
     * @return the result of the last upgrade.
     * @response.representation.200.qname
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc Returned if the upgrade is complete
     * @response.representation.200.example {@link com.atlassian.jira.rest.v2.upgrade.UpgradeResultBean#DOC_EXAMPLE}
     * @response.representation.303.doc Returned if the upgrade task is still running.
     * @response.representation.404.doc Returned if no prior upgrade task exists.
     */
    @GET
    @ResponseType(UpgradeResultBean.class)
    public Response getUpgradeResult() {
        final ApplicationUser user = jiraAuthenticationContext.getUser();
        if (!permissionManager.hasPermission(Permissions.ADMINISTER, user)) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        if ((upgradeService.areUpgradesRunning())) {
            URI location = UriBuilder.fromPath(jiraBaseUrls.restApi2BaseUrl()).path("upgrade").build();
            return Response.status(Response.Status.SEE_OTHER).location(location).
                    header("Retry-After", 10).cacheControl(never()).build();
        }

        final Optional<RunDetails> lastUpgradeResult = Optional.ofNullable(
                schedulerHistoryService.getLastRunForJob(UpgradeScheduler.JOB_ID)
        );

        return lastUpgradeResult
                .map(result -> Response.ok(upgradeService.areUpgradesRunning())
                        .entity(UpgradeResultBean.fromRunDetails(result))
                        .cacheControl(never())
                        .build()
                ).orElseGet(() -> Response.status(Response.Status.NOT_FOUND).build());
    }
}
