package com.atlassian.jira.rest.v2.issue.worklog;

import com.google.common.collect.ImmutableSet;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Set;

/**
 * Request to {@link WorklogResource#getWorklogsForIds(WorklogIdsRequestBean)}
 */
public class WorklogIdsRequestBean {
    public static final WorklogIdsRequestBean EXAMPLE = new WorklogIdsRequestBean(ImmutableSet.of(1l, 2l, 5l, 10l));

    @JsonProperty
    private Set<Long> ids;

    public WorklogIdsRequestBean() {
    }

    public WorklogIdsRequestBean(Set<Long> ids) {
        this.ids = ids;
    }

    public Set<Long> getIds() {
        return ids;
    }

    public void setIds(final Set<Long> ids) {
        this.ids = ids;
    }
}
