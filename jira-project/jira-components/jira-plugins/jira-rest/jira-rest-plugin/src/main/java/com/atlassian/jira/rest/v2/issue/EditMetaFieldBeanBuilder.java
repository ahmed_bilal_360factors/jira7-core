package com.atlassian.jira.rest.v2.issue;

import com.atlassian.fugue.Option;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.customfields.OperationContext;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.OrderableField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.fields.screen.FieldScreenRenderTab;
import com.atlassian.jira.issue.fields.screen.FieldScreenRendererFactory;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.operation.IssueOperations;
import com.atlassian.jira.issue.search.constants.SystemSearchConstants;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.rest.v2.issue.context.ContextUriInfo;
import com.atlassian.jira.rest.v2.issue.version.VersionBeanFactory;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;

import java.util.Map;
import java.util.stream.Stream;

import static java.util.stream.StreamSupport.stream;

/**
 * Builder for {@link com.atlassian.jira.rest.v2.issue.FieldMetaBean} instances, in the context of meta data for creating issues.
 *
 * @since v5.0
 */
public class EditMetaFieldBeanBuilder extends AbstractMetaFieldBeanBuilder {
    private final OperationContext operationContext = new EditIssueOperationContext();
    private final FieldScreenRendererFactory fieldScreenRendererFactory;
    private final FieldManager fieldManager;
    private final IssueManager issueManager;

    public EditMetaFieldBeanBuilder(final FieldLayoutManager fieldLayoutManager,
                                    final Project project,
                                    final Issue issue,
                                    final IssueType issueType,
                                    final ApplicationUser user,
                                    final VersionBeanFactory versionBeanFactory,
                                    final VelocityRequestContextFactory velocityRequestContextFactory,
                                    final ContextUriInfo contextUriInfo,
                                    final JiraBaseUrls baseUrls,
                                    final FieldScreenRendererFactory fieldScreenRendererFactory,
                                    final FieldManager fieldManager,
                                    final IssueManager issueManager) {
        super(fieldLayoutManager, project, issue, issueType, user, versionBeanFactory, velocityRequestContextFactory, contextUriInfo, baseUrls, null);
        this.fieldScreenRendererFactory = fieldScreenRendererFactory;
        this.fieldManager = fieldManager;
        this.issueManager = issueManager;
    }

    @Override
    protected void addAdditionalFields(final Map<String, FieldMetaBean> fields) {
        getCommentFieldOption()
                .forEach(commentField -> {
                    final FieldMetaBean fieldMetaBean = getFieldMetaBean(false, commentField);
                    fields.put(commentField.getId(), fieldMetaBean);
                });
    }

    @Override
    protected Stream<String> getAdditionalKeys() {
        return stream(
                getCommentFieldOption()
                        .map(OrderableField::getId).spliterator(),
                false);

    }

    private Option<OrderableField> getCommentFieldOption() {
        OrderableField field = (OrderableField) fieldManager.getField(SystemSearchConstants.forComments().getFieldId());
        if (field.isShown(issue)) {
            if (includeFields == null || includeFields.included(field)) {
                return Option.some(field);
            }
        }
        return Option.none();
    }

    @Override
    public OperationContext getOperationContext() {
        return operationContext;
    }

    @Override
    public boolean hasPermissionToPerformOperation() {
        return issueManager.isEditable(issue, user);
    }

    @Override
    Stream<FieldScreenRenderTab> getFieldScreenRenderTabsStream(Issue issue) {
        return fieldScreenRendererFactory.getFieldScreenRenderer(issue, IssueOperations.EDIT_ISSUE_OPERATION)
                .getFieldScreenRenderTabs()
                .stream();
    }
}
