package com.atlassian.jira.rest.v2.notification;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.notification.NotificationScheme;
import com.atlassian.jira.notification.NotificationSchemeService;
import com.atlassian.jira.rest.api.notification.NotificationSchemeBean;
import com.atlassian.jira.rest.api.notification.NotificationSchemeBeanFactory;
import com.atlassian.jira.rest.api.notification.NotificationSchemeExpandParam;
import com.atlassian.jira.rest.api.pagination.PageBean;
import com.atlassian.jira.rest.util.ResponseFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.Page;
import com.atlassian.jira.util.PageRequest;
import com.atlassian.jira.util.PageRequests;
import com.atlassian.rest.annotation.ResponseType;
import com.google.common.base.Function;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import static com.google.common.base.MoreObjects.firstNonNull;

@Path("notificationscheme")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class NotificationSchemeResource {
    private static final Integer MAX_RESULTS = 50;

    private final NotificationSchemeService notificationSchemeService;
    private final JiraAuthenticationContext authContext;
    private final ResponseFactory responseFactory;
    private final NotificationSchemeBeanFactory notificationSchemeBeanFactory;

    public NotificationSchemeResource(final NotificationSchemeService notificationSchemeService,
                                      final JiraAuthenticationContext authContext,
                                      final ResponseFactory responseFactory,
                                      final NotificationSchemeBeanFactory notificationSchemeBeanFactory) {
        this.notificationSchemeService = notificationSchemeService;
        this.authContext = authContext;
        this.responseFactory = responseFactory;
        this.notificationSchemeBeanFactory = notificationSchemeBeanFactory;
    }

    /**
     * Returns a full representation of the notification scheme for the given id. This resource will return a
     * notification scheme containing a list of events and recipient configured to receive notifications for these events. Consumer
     * should allow events without recipients to appear in response. User accessing
     * the data is required to have permissions to administer at least one project associated with the requested notification scheme.
     * <p>
     * Notification recipients can be:
     * <ul>
     * <li>current assignee - the value of the notificationType is CurrentAssignee</li>
     * <li>issue reporter - the value of the notificationType is Reporter</li>
     * <li>current user - the value of the notificationType is CurrentUser</li>
     * <li>project lead - the value of the notificationType is ProjectLead</li>
     * <li>component lead - the value of the notificationType is ComponentLead</li>
     * <li>all watchers - the value of the notification type is AllWatchers</li>
     * <li>configured user - the value of the notification type is User. Parameter will contain key of the user. Information about the user will be provided
     * if <b>user</b> expand parameter is used. </li>
     * <li>configured group - the value of the notification type is Group. Parameter will contain name of the group. Information about the group will be provided
     * if <b>group</b> expand parameter is used. </li>
     * <li>configured email address - the value of the notification type is EmailAddress, additionally information about the email will be provided.</li>
     * <li>users or users in groups in the configured custom fields - the value of the notification type is UserCustomField or GroupCustomField. Parameter
     * will contain id of the custom field. Information about the field will be provided if <b>field</b> expand parameter is used. </li>
     * <li>configured project role - the value of the notification type is ProjectRole. Parameter will contain project role id. Information about the project role
     * will be provided if <b>projectRole</b> expand parameter is used. </li>
     * </ul>
     * Please see the example for reference.
     * </p>
     * The events can be JIRA system events or events configured by administrator. In case of the system events, data about theirs
     * ids, names and descriptions is provided. In case of custom events, the template event is included as well.
     *
     * @param notificationSchemeId an id of the notification scheme to retrieve
     * @param expand:              optional information to be expanded in the response: group, user, projectRole or field.
     * @return a full representation of the notification scheme with given id
     * @response.representation.200.qname notificationScheme
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc Returned if the notification scheme exists and is visible for the calling user
     * @response.representation.200.example {@link NotificationSchemeBeanExample#DOC_EXAMPLE}
     * @response.representation.404.doc Returned if the notification scheme does not exist, or is not visible to the calling user
     */
    @GET
    @Path("{id}")
    @ResponseType(NotificationSchemeBean.class)
    public Response getNotificationScheme(@PathParam("id") final Long notificationSchemeId, @QueryParam("expand") final String expand) {
        return responseFactory.toResponse(notificationSchemeBeanFactory.parseExpandQuery(expand)).left().on(new Function<List<NotificationSchemeExpandParam>, Response>() {
            @Override
            public Response apply(final List<NotificationSchemeExpandParam> expands) {
                final ServiceOutcome<NotificationScheme> notificationSchemeOutcome = notificationSchemeService.getNotificationScheme(authContext.getUser(), notificationSchemeId);

                return responseFactory.validateOutcome(notificationSchemeOutcome).left().on(new Function<NotificationScheme, Response>() {
                    @Override
                    public Response apply(final NotificationScheme notificationScheme) {
                        return responseFactory.okNoCache(notificationSchemeBeanFactory.createNotificationSchemeBean(notificationScheme, expands));
                    }
                });
            }
        });
    }

    /**
     * Returns a <a href="#pagination">paginated</a> list of notification schemes. In order to access notification scheme, the calling user is
     * required to have permissions to administer at least one project associated with the requested notification scheme. Each scheme contains
     * a list of events and recipient configured to receive notifications for these events. Consumer should allow events without recipients to appear in response.
     * The list is ordered by the scheme's name.
     * Follow the documentation of /notificationscheme/{id} resource for all details about returned value.
     *
     * @param startAt    the index of the first notification scheme to return (0 based).
     * @param maxResults the maximum number of notification schemes to return (max 50).
     * @param expand:    optional information to be expanded in the response: group, user, projectRole or field.
     * @return a paginated list of notification schemes to which the calling user has permissions.
     * @response.representation.200.qname notificationScheme
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc Paginated list of notification schemes to which the user has permissions.
     * @response.representation.200.example {@link NotificationSchemeBeanExample#PAGE_DOC_EXAMPLE}
     */
    @GET
    @ResponseType(value = PageBean.class, genericTypes = NotificationSchemeBean.class)
    public Response getNotificationSchemes(@QueryParam("startAt") final Long startAt, @QueryParam("maxResults") final Integer maxResults, @QueryParam("expand") final String expand) {
        return responseFactory.toResponse(notificationSchemeBeanFactory.parseExpandQuery(expand)).left().on(new Function<List<NotificationSchemeExpandParam>, Response>() {
            @Override
            public Response apply(final List<NotificationSchemeExpandParam> expands) {
                final PageRequest pageRequest = PageRequests.request(startAt, firstNonNull(maxResults, MAX_RESULTS));

                final Page<NotificationScheme> notificationSchemes = notificationSchemeService.getNotificationSchemes(authContext.getLoggedInUser(), pageRequest);

                PageBean<NotificationSchemeBean> pageBean = PageBean.from(pageRequest, notificationSchemes).build(scheme -> notificationSchemeBeanFactory.createNotificationSchemeBean(scheme, expands));

                return responseFactory.okNoCache(pageBean);
            }
        });
    }
}
