package com.atlassian.jira.rest.v2.search;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.issue.fields.rest.json.beans.GroupJsonBean;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.rest.api.project.ProjectRoleBean;
import com.atlassian.jira.rest.v2.issue.project.ProjectBean;
import com.atlassian.jira.scheme.SchemeManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.sharing.type.ShareType;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.util.concurrent.LazyReference;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;

import javax.annotation.Nullable;
import java.util.Collection;

/**
 * Resolves users for given FilterPermissionBeans.
 *
 * @since v6.0
 */
class UserListResolver {
    // visible for testing
    static final String DO_NOT_COUNT_USERS_ON_SHARED_FILTER_FEATURE = "com.atlassian.jira.rest.v2.search.UserListResolver.getShareCount.disabled";

    private final UserManager userManager;
    private final GroupManager groupManager;
    private final ProjectManager projectManager;
    private final ProjectRoleManager projectRoleManager;
    private final SchemeManager schemeManager;
    private final JiraAuthenticationContext authContext;
    private final PermissionManager permissionManager;
    private final UserUtil userUtil;
    private final FeatureManager featureManager;
    final Collection<FilterPermissionBean> permissions;

    //Keeping a lazy ref to avoid doing this more than once during count operations
    private LazyReference<Collection<ApplicationUser>> sharedUsers = new LazyReference<Collection<ApplicationUser>>() {
        @Override
        protected Collection<ApplicationUser> create() throws Exception {
            return getShareUsersInternal();
        }
    };

    public UserListResolver(final JiraAuthenticationContext authContext, final UserManager userManager, final GroupManager groupManager,
                            final ProjectManager projectManager, final PermissionManager permissionManager, final ProjectRoleManager projectRoleManager,
                            final SchemeManager schemeManager, final UserUtil userUtil, final FeatureManager featureManager, final Collection<FilterPermissionBean> permissions) {
        this.authContext = authContext;
        this.userManager = userManager;
        this.groupManager = groupManager;
        this.projectRoleManager = projectRoleManager;
        this.permissionManager = permissionManager;
        this.projectManager = projectManager;
        this.schemeManager = schemeManager;
        this.userUtil = userUtil;
        this.featureManager = featureManager;
        this.permissions = permissions;
    }

    private Collection<ApplicationUser> getShareUsersInternal() {
        final ImmutableSet.Builder<ApplicationUser> sharedUsersBuilder = ImmutableSet.builder();
        // need browse user permission
        if (permissionManager.hasPermission(Permissions.USER_PICKER, authContext.getUser())) {
            for (final FilterPermissionBean sharePermission : permissions) {
                final String type = sharePermission.getType();
                if (type.equals(ShareType.Name.GLOBAL.toString())) {
                    // we've just got all users, they are already a unique set, so just return them.
                    return ImmutableList.copyOf(getAllActiveUsers());
                } else if (type.equals(ShareType.Name.AUTHENTICATED.toString())) {
                    // we've just got all users, they are already a unique set, so just return them.
                    return ImmutableList.copyOf(getAllActiveUsers());
                } else if (type.equals(ShareType.Name.GROUP.toString())) {
                    final GroupJsonBean groupJsonBean = sharePermission.getGroup();
                    final Group group = groupManager.getGroup(groupJsonBean.getName());
                    sharedUsersBuilder.addAll(activeUsers(groupManager.getUsersInGroup(group)));
                } else if (type.equals(ShareType.Name.PROJECT.toString())) {
                    final ProjectBean projectBean = sharePermission.getProject();
                    final Project project = projectManager.getProjectObjByName(projectBean.getName());

                    final ProjectRoleBean roleBean = sharePermission.getRole();
                    if (roleBean == null) {
                        // No role bean means anyone with BROWSE permission in that project
                        sharedUsersBuilder.addAll(
                                activeUsers(schemeManager.getUsers((long) Permissions.BROWSE, project)));
                    } else {
                        // A single role specified so find the users in that role.
                        final ProjectRole projectRole = projectRoleManager.getProjectRole(roleBean.name);
                        sharedUsersBuilder.addAll(activeUsers(projectRoleManager.getProjectRoleActors(projectRole, project).getUsers()));
                    }
                } else if (type.equals(ShareType.Name.PROJECT_UNKNOWN.toString())) {
                    //project unknown should return 0 shared users as to not disclose the amount of users in an invisible project
                } else {
                    throw new IllegalStateException("Unknown share type of: " + type);
                }
            }
        }
        return sharedUsersBuilder.build();
    }

    public Collection<ApplicationUser> getShareUsers() {
        return sharedUsers.get();
    }

    public int getShareCount() {
        final boolean shareUserCountEnabled = !featureManager.isEnabled(DO_NOT_COUNT_USERS_ON_SHARED_FILTER_FEATURE);
        if (shareUserCountEnabled && permissionManager.hasPermission(Permissions.USER_PICKER, authContext.getUser())) {
            // JRA-44236 - We use a cached value from UserUtil#getActiveUserCount to avoid retrieving the user list
            // eagerly to get the active users count if filter permission is global. We still need get the users for other permissions.
            for (final FilterPermissionBean sharePermission : permissions) {
                final String type = sharePermission.getType();
                if (type.equals(ShareType.Name.GLOBAL.toString()) || type.equals(ShareType.Name.AUTHENTICATED.toString())) {
                    return userUtil.getActiveUserCount();
                }
            }
            return getShareUsers().size();
        } else {
            return 0;
        }
    }

    private Iterable<ApplicationUser> getAllActiveUsers() {
        return activeUsers(userManager.getUsers());
    }


    Iterable<ApplicationUser> activeUsers(final Collection<ApplicationUser> users) {
        return Iterables.filter(users, new Predicate<ApplicationUser>() {
            @Override
            public boolean apply(@Nullable ApplicationUser user) {
                return user != null && user.isActive();
            }
        });
    }

}
