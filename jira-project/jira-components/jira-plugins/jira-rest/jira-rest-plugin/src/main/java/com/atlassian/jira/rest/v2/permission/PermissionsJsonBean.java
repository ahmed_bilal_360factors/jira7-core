package com.atlassian.jira.rest.v2.permission;

import com.atlassian.jira.util.collect.MapBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Map;

import static com.atlassian.jira.permission.ProjectPermissions.EDIT_ISSUES;

/**
 * Represents a list of Permissions
 *
 * @since v5.0
 */
public class PermissionsJsonBean {
    @JsonProperty
    private Map<String, ? extends PermissionJsonBean> permissions;

    public PermissionsJsonBean(Map<String, ? extends PermissionJsonBean> permissions) {
        this.permissions = permissions;
    }

    public static final PermissionsJsonBean USER_DOC_EXAMPLE;
    public static final PermissionsJsonBean DOC_EXAMPLE;

    static {
        USER_DOC_EXAMPLE = new PermissionsJsonBean(MapBuilder.<String, PermissionJsonBean>newBuilder()
                .add("EDIT_ISSUE", new UserPermissionJsonBean(EDIT_ISSUES, "Edit Issues", "Ability to edit issues.", true))
                .toHashMap());
        DOC_EXAMPLE = new PermissionsJsonBean(MapBuilder.<String, PermissionJsonBean>newBuilder()
                .add("BULK_CHANGE", new PermissionJsonBean("BULK_CHANGE", "Bulk Change", PermissionJsonBean.PermissionType.GLOBAL,
                        "Ability to modify a collection of issues at once. For example, resolve multiple issues in one step."))
                .toHashMap());
    }
}
