package com.atlassian.jira.rest.v2.issue;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * This bean describes a result of moving task from one position to another.
 *
 * @since v7.2
 */
public class IssueSubTaskMovePositionBean {
    private final long original;
    private final long current;

    @JsonCreator
    public IssueSubTaskMovePositionBean(@JsonProperty("original") long original, @JsonProperty("current") long current) {
        this.original = original;
        this.current = current;
    }

    @JsonProperty("original")
    public long getOriginal() {
        return original;
    }

    @JsonProperty("current")
    public long getCurrent() {
        return current;
    }
}
