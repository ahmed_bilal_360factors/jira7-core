package com.atlassian.jira.rest.v2.search;

import com.atlassian.jira.issue.comparator.UserBestNameComparator;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.rest.api.expand.PagedListWrapper;
import com.atlassian.jira.rest.v2.issue.UserBean;
import com.atlassian.jira.rest.v2.issue.UserBeanBuilder;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.collect.Ordering;

import java.util.List;

/**
 * Wraps a list of Users and pages over them
 *
 * @since v6.0
 */
public class UserBeanListWrapper extends PagedListWrapper<UserBean, ApplicationUser> {
    private final Ordering<ApplicationUser> userOrdering = Ordering.from(new UserBestNameComparator());
    private final JiraBaseUrls jiraBaseUrls;
    // We can't keep just list there, as this will cause EntityCrawler to fail due to existance of two collections in
    // class (see PagedListWrapper.items)
    private final UserListResolver userListResolver;

    @SuppressWarnings("UnusedDeclaration") // necessary for JAXB
    private UserBeanListWrapper() {
        this(null, null, 0);
    }

    public UserBeanListWrapper(final JiraBaseUrls jiraBaseUrls, final UserListResolver userListResolver, final int maxResults) {
        super(userListResolver != null ? userListResolver.getShareCount() : 0, maxResults);
        this.jiraBaseUrls = jiraBaseUrls;
        this.userListResolver = userListResolver;
    }

    public UserBean fromBackedObject(final ApplicationUser user) {
        return new UserBeanBuilder(jiraBaseUrls)
                .user(user)
                .buildShort();
    }

    @Override
    public int getBackingListSize() {
        return userListResolver.getShareCount();
    }

    @Override
    public List<ApplicationUser> getOrderedList(final int startIndex, final int endIndex) {
        final List<ApplicationUser> sortedUsers = userOrdering.leastOf(userListResolver.getShareUsers(), endIndex + 1);
        return sortedUsers.subList(startIndex, endIndex + 1);
    }


}
