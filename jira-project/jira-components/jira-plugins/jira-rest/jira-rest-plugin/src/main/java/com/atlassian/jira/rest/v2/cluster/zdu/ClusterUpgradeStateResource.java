package com.atlassian.jira.rest.v2.cluster.zdu;

import com.atlassian.jira.cluster.zdu.ClusterUpgradeStateManager;
import com.atlassian.jira.cluster.zdu.NodeBuildInfo;
import com.atlassian.jira.cluster.zdu.UpgradeState;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.rest.common.feature.RequiresDarkFeature;
import com.atlassian.rest.annotation.ResponseType;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import static com.atlassian.jira.cluster.zdu.ClusterUpgradeStateManager.CLUSTER_UPGRADE_STATE_DARK_FEATURE;
import static javax.ws.rs.core.Response.Status.CONFLICT;
import static javax.ws.rs.core.Response.Status.FORBIDDEN;

/**
 * @since v7.3
 */
@Path("cluster/zdu")
@RequiresDarkFeature(CLUSTER_UPGRADE_STATE_DARK_FEATURE)
public class ClusterUpgradeStateResource {

    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final GlobalPermissionManager permissionManager;
    private final ClusterUpgradeStateManager clusterUpgradeStateManager;

    public ClusterUpgradeStateResource(@ComponentImport JiraAuthenticationContext jiraAuthenticationContext,
                                       @ComponentImport GlobalPermissionManager permissionManager,
                                       @ComponentImport ClusterUpgradeStateManager clusterUpgradeStateManager) {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.permissionManager = permissionManager;
        this.clusterUpgradeStateManager = clusterUpgradeStateManager;
    }

    @GET
    @Path("state")
    @Produces("application/json")
    @ResponseType(ClusterState.class)
    public Response getState() {
        final ApplicationUser user = jiraAuthenticationContext.getLoggedInUser();
        if (permissionManager.hasPermission(GlobalPermissionKey.SYSTEM_ADMIN, user)) {
            return Response.ok(
                    new ClusterState(
                            clusterUpgradeStateManager.getUpgradeState(),
                            clusterUpgradeStateManager.getClusterBuildInfo()
                    )
            ).build();
        }
        return Response.status(FORBIDDEN).build();
    }

    @POST
    @Path("cancel")
    @Consumes("application/json")
    @Produces("application/json")
    @ResponseType(Void.class)
    public Response cancelUpgrade(@Context UriInfo uri) {
        final ApplicationUser user = jiraAuthenticationContext.getLoggedInUser();
        if (permissionManager.hasPermission(GlobalPermissionKey.SYSTEM_ADMIN, user)) {
            try {
                clusterUpgradeStateManager.cancelUpgrade();
            } catch (IllegalStateException e) {
                return Response.status(CONFLICT)
                        .entity(e.getMessage())
                        .build();
            }

            return Response.created(uri.getRequestUri())
                    .build();
        } else {
            return Response.status(FORBIDDEN).build();
        }
    }

    @POST
    @Path("start")
    @Consumes("application/json")
    @ResponseType(Void.class)
    public Response setReadyToUpgrade(@Context UriInfo uri) {
        final ApplicationUser user = jiraAuthenticationContext.getLoggedInUser();
        if (permissionManager.hasPermission(GlobalPermissionKey.SYSTEM_ADMIN, user)) {
            try {
                clusterUpgradeStateManager.startUpgrade();
            } catch (IllegalStateException e) {
                return Response.status(CONFLICT)
                        .entity(e.getMessage())
                        .build();
            }

            return Response.created(uri.getRequestUri())
                    .build();
        } else {
            return Response.status(FORBIDDEN).build();
        }
    }

    @POST
    @Path("approve")
    @Consumes("application/json")
    @ResponseType(Void.class)
    public Response approveUpgrade() {
        final ApplicationUser user = jiraAuthenticationContext.getLoggedInUser();
        if (permissionManager.hasPermission(GlobalPermissionKey.SYSTEM_ADMIN, user)) {
            try {
                clusterUpgradeStateManager.approveUpgrade();
            } catch (IllegalStateException e) {
                return Response.status(CONFLICT)
                        .entity(e.getMessage())
                        .build();
            }

            return Response.ok().build();
        } else {
            return Response.status(FORBIDDEN).build();
        }
    }

    public static class ClusterState {
        private final UpgradeState state;
        private final NodeBuildInfo buildInfo;

        public ClusterState(UpgradeState state, NodeBuildInfo buildInfo) {
            this.state = state;
            this.buildInfo = buildInfo;
        }

        @JsonProperty("state")
        public UpgradeState getState() {
            return state;
        }

        @JsonProperty("build")
        public NodeBuildInfo getBuildInfo() {
            return buildInfo;
        }
    }
}
