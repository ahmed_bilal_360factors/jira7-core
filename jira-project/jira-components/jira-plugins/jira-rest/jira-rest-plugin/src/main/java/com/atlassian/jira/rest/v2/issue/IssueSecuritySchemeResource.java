package com.atlassian.jira.rest.v2.issue;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.fields.rest.json.beans.SecuritySchemeJsonBean;
import com.atlassian.jira.issue.fields.rest.json.beans.SecuritySchemesJsonBean;
import com.atlassian.jira.issue.security.IssueSecurityLevel;
import com.atlassian.jira.issue.security.IssueSecurityLevelScheme;
import com.atlassian.jira.issue.security.IssueSecuritySchemeService;
import com.atlassian.jira.rest.util.ResponseFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import com.atlassian.rest.annotation.ResponseType;
import com.google.common.base.Function;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * REST resource that allows to view security schemes defined in the product.
 *
 * @since 7.0
 */
@Path("issuesecurityschemes")
@AnonymousAllowed
@Produces({MediaType.APPLICATION_JSON})
public class IssueSecuritySchemeResource {
    private final IssueSecuritySchemeService issueSecuritySchemeService;
    private final ResponseFactory responseFactory;
    private final JiraBaseUrls baseUrls;
    private final JiraAuthenticationContext jiraAuthenticationContext;

    public IssueSecuritySchemeResource(
            IssueSecuritySchemeService issueSecuritySchemeService,
            ResponseFactory responseFactory,
            JiraBaseUrls baseUrls,
            JiraAuthenticationContext jiraAuthenticationContext) {
        this.issueSecuritySchemeService = issueSecuritySchemeService;
        this.responseFactory = responseFactory;
        this.baseUrls = baseUrls;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    /**
     * Returns all issue security schemes that are defined.
     *
     * @return an object containing a collection of issue security schemes
     * @response.representation.200.qname issuesecurityschemes
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc Returned if the user has the administrator permission.
     * @response.representation.200.example {@link com.atlassian.jira.rest.v2.issue.SecuritySchemeExample#DOC_LIST_EXAMPLE}
     * @response.representation.401.doc Returned if the user is not logged in.
     * @response.representation.403.doc Returned if the user does not have the administrator permission.
     */
    @GET
    @ResponseType(SecuritySchemesJsonBean.class)
    public Response getIssueSecuritySchemes() {
        ApplicationUser user = jiraAuthenticationContext.getUser();
        ServiceOutcome<? extends Collection<IssueSecurityLevelScheme>> issueSecurityLevelSchemes = issueSecuritySchemeService.getIssueSecurityLevelSchemes(user);
        return responseFactory.validateOutcome(issueSecurityLevelSchemes).left().on(new Function<Collection<IssueSecurityLevelScheme>, Response>() {
            @Override
            public Response apply(final Collection<IssueSecurityLevelScheme> issueSecurityLevelSchemes) {
                return responseFactory.okNoCache(
                        SecuritySchemesJsonBean.fromList(SecuritySchemeJsonBean.fromIssueSecuritySchemes(new ArrayList<IssueSecurityLevelScheme>(issueSecurityLevelSchemes), baseUrls)));
            }
        });
    }

    /**
     * Returns the issue security scheme along with that are defined.
     *
     * @return an object containing a collection of issue security schemes
     * @response.representation.200.qname issuesecurityscheme
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc Returned if the user has the administrator permission or if the scheme is used in a project in which the user has the administrative permission.
     * @response.representation.200.example {@link com.atlassian.jira.rest.v2.issue.SecuritySchemeExample#DOC_FULL_BEAN_EXAMPLE}
     * @response.representation.401.doc Returned if the user is not logged in.
     * @response.representation.403.doc Returned if the user does not have the administrator permission and the scheme is not used in any project where the user has administrative permission.
     */
    @GET
    @Path("{id}")
    @ResponseType(SecuritySchemeJsonBean.class)
    public Response getIssueSecurityScheme(@PathParam("id") final Long id) {
        final ApplicationUser user = jiraAuthenticationContext.getUser();

        final ServiceOutcome<IssueSecurityLevelScheme> issueSecurityLevelScheme = issueSecuritySchemeService.getIssueSecurityLevelScheme(user, id);
        return responseFactory.validateOutcome(issueSecurityLevelScheme).left().on(new Function<IssueSecurityLevelScheme, Response>() {
            @Override
            public Response apply(final IssueSecurityLevelScheme securityLevelScheme) {
                ServiceOutcome<? extends List<IssueSecurityLevel>> issueSecurityLevels = issueSecuritySchemeService.getIssueSecurityLevels(user, id);
                return responseFactory.validateOutcome(issueSecurityLevels).left().on(new Function<List<IssueSecurityLevel>, Response>() {
                    @Override
                    public Response apply(final List<IssueSecurityLevel> issueSecurityLevels) {
                        return responseFactory.okNoCache(
                                SecuritySchemeJsonBean.fullBean(securityLevelScheme, baseUrls, issueSecurityLevels));
                    }
                });
            }
        });
    }
}
