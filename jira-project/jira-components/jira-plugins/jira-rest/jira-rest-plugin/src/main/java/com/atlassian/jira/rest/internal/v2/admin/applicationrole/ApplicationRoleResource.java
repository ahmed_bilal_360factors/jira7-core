package com.atlassian.jira.rest.internal.v2.admin.applicationrole;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.rest.api.util.ErrorCollection;
import com.atlassian.jira.rest.v2.admin.GroupLabelBean;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.component.admin.group.GroupLabelView;
import com.atlassian.jira.web.component.admin.group.GroupLabelsService;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static com.atlassian.jira.rest.api.http.CacheControl.never;

/**
 * @since v7.0
 */
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@WebSudoRequired
@Path("applicationrole")
public class ApplicationRoleResource {

    private final ApplicationRoleManager applicationRoleManager;
    private final GroupLabelsService groupLabelsService;
    private final GlobalPermissionManager permissionManager;
    private final JiraAuthenticationContext authenticationContext;
    private final I18nHelper i18nHelper;

    public ApplicationRoleResource(
            final ApplicationRoleManager applicationRoleManager,
            final GroupLabelsService groupLabelsService,
            final GlobalPermissionManager permissionManager,
            final JiraAuthenticationContext authenticationContext,
            final I18nHelper i18nHelper) {
        this.applicationRoleManager = applicationRoleManager;
        this.groupLabelsService = groupLabelsService;
        this.permissionManager = permissionManager;
        this.authenticationContext = authenticationContext;
        this.i18nHelper = i18nHelper;
    }

    @Path("groups")
    @GET
    public Response getGroups() {
        if (!permissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, authenticationContext.getLoggedInUser())) {
            return Response.status(Response.Status.FORBIDDEN).entity(ErrorCollection.of()).build();
        }

        final ImmutableSet<GroupBean> groupBeans = applicationRoleManager.getRoles().stream()
                .map(ApplicationRole::getGroups)
                .flatMap(Collection::stream)
                .map(this::toGroupBean)
                .collect(CollectorsUtil.toImmutableSet());

        return Response.ok(groupBeans).cacheControl(never()).build();
    }

    private GroupBean toGroupBean(Group group) {
        final List<GroupLabelView> labels = groupLabelsService.getGroupLabels(group, Optional.<Long>empty());
        return new GroupBean(group.getName(), GroupLabelBean.toLabelsBeansList(labels));
    }

    @XmlRootElement(name = "group")
    public static class GroupBean {
        @XmlElement
        private final String name;

        @XmlElement
        private final List<GroupLabelBean> labels;

        public GroupBean(final String name, final List<GroupLabelBean> labels) {
            this.name = name;
            this.labels = labels;
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            final GroupBean groupBean = (GroupBean) o;

            if (!labels.equals(groupBean.labels)) {
                return false;
            }
            if (!name.equals(groupBean.name)) {
                return false;
            }

            return true;
        }

        @Override
        public int hashCode() {
            int result = name.hashCode();
            result = 31 * result + labels.hashCode();
            return result;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this)
                    .append("name", name)
                    .append("labels", labels)
                    .toString();
        }
    }
}
