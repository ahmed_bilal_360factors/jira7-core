package com.atlassian.jira.rest.v2.permission;

import com.atlassian.jira.permission.GlobalPermissionType;
import com.atlassian.jira.permission.ProjectPermission;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.google.common.base.Objects;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Represents a single JIRA permission
 *
 * @since v7.0
 */
public class PermissionJsonBean extends com.atlassian.jira.issue.fields.rest.json.beans.PermissionJsonBean {

    @JsonProperty
    public String name;

    @JsonProperty
    public PermissionType type;

    @JsonProperty
    public String description;

    /**
     * Indicate whether this is a project or global permission
     */
    static enum PermissionType {
        GLOBAL, PROJECT
    }

    public PermissionJsonBean() {
    }

    public PermissionJsonBean(final String key, final String name, final PermissionType type, final String description) {
        key(key);
        this.name = name;
        this.type = type;
        this.description = description;
    }

    public static PermissionJsonBean fromGlobalPermission(GlobalPermissionType globalPermission, JiraAuthenticationContext authenticationContext) {
        final PermissionJsonBean bean = new PermissionJsonBean();
        bean.key(globalPermission.getKey());
        bean.name = authenticationContext.getI18nHelper().getText(globalPermission.getNameI18nKey());
        bean.description = authenticationContext.getI18nHelper().getText(globalPermission.getDescriptionI18nKey());
        bean.type = PermissionType.GLOBAL;
        return bean;
    }

    public static PermissionJsonBean fromProjectPermission(final ProjectPermission projectPermission, final JiraAuthenticationContext authenticationContext) {
        final PermissionJsonBean bean = new PermissionJsonBean();
        bean.key(projectPermission.getKey());
        bean.name = authenticationContext.getI18nHelper().getText(projectPermission.getNameI18nKey());
        bean.description = authenticationContext.getI18nHelper().getText(projectPermission.getDescriptionI18nKey());
        bean.type = PermissionType.PROJECT;
        return bean;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("id", id())
                .add("key", key())
                .add("name", name)
                .add("type", type)
                .add("description", description)
                .toString();
    }
}
