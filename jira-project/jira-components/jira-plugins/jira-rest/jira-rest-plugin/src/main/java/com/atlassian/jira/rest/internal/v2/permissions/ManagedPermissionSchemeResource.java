package com.atlassian.jira.rest.internal.v2.permissions;

import com.atlassian.fugue.Either;
import com.atlassian.jira.permission.management.ManagedPermissionSchemeHelper;
import com.atlassian.jira.permission.management.beans.PermissionsInputBean;
import com.atlassian.jira.permission.management.beans.ProjectDeleteInstructionsBean;
import com.atlassian.jira.rest.util.ResponseFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.websudo.WebSudoRequired;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("managedpermissionscheme")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
@WebSudoRequired
public class ManagedPermissionSchemeResource {

    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final ResponseFactory responseFactory;

    private final ManagedPermissionSchemeHelper managedPermission;

    public ManagedPermissionSchemeResource(final JiraAuthenticationContext jiraAuthenticationContext,
                                           final @ComponentImport ManagedPermissionSchemeHelper managedPermission,
                                           final ResponseFactory responseFactory) {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.managedPermission = managedPermission;
        this.responseFactory = responseFactory;
    }

    @GET
    @Path("{permissionSchemeId}")
    public Response getManagedPermissionScheme(@PathParam("permissionSchemeId") Long permissionSchemeId) {
        return toResponse(managedPermission.getManagedPermissionScheme(getLoggedInUser(), permissionSchemeId));
    }

    @GET
    @Path("securitytypes")
    public Response getManagedPermissionSchemeAddViewSecurityTypes() {
        return toResponse(managedPermission.getManagedPermissionSchemeAddViewSecurityTypes(getLoggedInUser()));
    }

    @GET
    @Path("{permissionSchemeId}/{permissionKey}")
    public Response getManagedPermissionSchemeAddView(@PathParam("permissionSchemeId") Long permissionSchemeId,
                                                      @PathParam("permissionKey") String permissionKey) {
        return toResponse(managedPermission.getManagedPermissionSchemeAddView(getLoggedInUser(), permissionSchemeId, permissionKey));
    }

    @POST
    @Path("{permissionSchemeId}")
    public Response addManagedPermissionSchemeGrants(@PathParam("permissionSchemeId") Long permissionSchemeId,
                                                     PermissionsInputBean addPermissions) {
        return toResponse(managedPermission.addManagedPermissionSchemeGrants(getLoggedInUser(), permissionSchemeId,
                addPermissions));
    }

    @DELETE
    @Path("{permissionSchemeId}")
    public Response removeManagedPermissionSchemeGrants(@PathParam("permissionSchemeId") Long permissionSchemeId,
                                                        final ProjectDeleteInstructionsBean deleteInstructionsBean) {
        return toResponse(managedPermission.removeManagedPermissionSchemeGrants(getLoggedInUser(), permissionSchemeId,
                deleteInstructionsBean.getGrantsToDelete()));
    }

    private <T> Response toResponse(Either<ErrorCollection, T> result) {
        return responseFactory.toResponse(result).left().on(responseFactory::okNoCache);
    }

    private ApplicationUser getLoggedInUser() {
        return jiraAuthenticationContext.getLoggedInUser();
    }
}
