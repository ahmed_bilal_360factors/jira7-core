package com.atlassian.jira.rest.v2.issue.project;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.rest.api.project.ProjectRoleBean;
import com.atlassian.jira.security.roles.DefaultRoleActors;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleActors;
import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * @since v6.1
 */
public interface ProjectRoleBeanFactory {
    ProjectRoleBean projectRole(@Nonnull Project project, @Nonnull ProjectRole projectRole);

    ProjectRoleBean projectRole(@Nonnull Project project, @Nonnull ProjectRole projectRole, @Nonnull ProjectRoleActors projectRoleActors, @Nullable ApplicationUser loggedInUser);

    ProjectRoleBean projectRole(@Nonnull final ProjectRole projectRole, final DefaultRoleActors actors);

    ProjectRoleBean shortProjectRole(@Nonnull ProjectRole projectRole);

    ProjectRoleBean shortRoleBean(@Nonnull final ProjectRole projectRole);
}
