package com.atlassian.jira.rest.v2.issue.version;

import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.CustomFieldWithVersionUsage;
import com.atlassian.jira.rest.v2.issue.VersionResource;

import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.Collection;

/**
 * Implementation of {@link com.atlassian.jira.rest.v2.issue.version.VersionIssueCountsBeanFactory}.
 *
 * @since v4.4
 */
public class VersionIssueCountsBeanFactoryImpl implements VersionIssueCountsBeanFactory {
    private final UriInfo info;

    public VersionIssueCountsBeanFactoryImpl(final UriInfo info) {
        //these two are proxied to objects from the current request. Be careful we are hunting AOP.
        this.info = info;
    }

    public VersionIssueCountsBean createVersionBean(
            final Version version,
            final long fixIssueCount,
            final long affectsIssueCount,
            final long issueCountWithCustomFieldsShowingVersion,
            final Collection<CustomFieldWithVersionUsage> customFieldUsageCollection) {
        return new VersionIssueCountsBean.Builder()
                .issuesFixedCount(fixIssueCount)
                .issuesAffectedCount(affectsIssueCount)
                .setSelf(createSelfURI(version))
                .issueCountWithCustomFieldsShowingVersion(issueCountWithCustomFieldsShowingVersion)
                .customFieldUsage(customFieldUsageCollection)
                .build();
    }

    private URI createSelfURI(final Version version) {
        return info.getBaseUriBuilder().path(VersionResource.class).path(version.getId().toString()).build();
    }
}
