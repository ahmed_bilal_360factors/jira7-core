package com.atlassian.jira.rest.v2.admin;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.web.component.admin.group.GroupLabelView;
import com.atlassian.jira.web.component.admin.group.GroupLabelView.LabelType;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Represents a {@link com.atlassian.crowd.model.group.Group} label, used in context of {@link com.atlassian.jira.rest.v2.issue.GroupSuggestionBean}
 *
 * @since v7.0
 */
@XmlRootElement(name = "grouplabel")
public class GroupLabelBean {
    @XmlElement
    private String text;

    @XmlElement
    private String title;

    @XmlElement
    private LabelType type;


    public GroupLabelBean() {
    }

    public GroupLabelBean(
            final String text,
            final String title,
            final LabelType type
    ) {
        this.text = text;
        this.title = title;
        this.type = type;
    }

    public String getText() {
        return text;
    }


    public String getTitle() {
        return title;
    }

    public LabelType getType() {
        return type;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("text", text)
                .append("title", title)
                .append("type", type)
                .toString();
    }

    @Override
    public boolean equals(Object rhs) {
        return EqualsBuilder.reflectionEquals(this, rhs);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    public static GroupLabelBean toLabelBean(GroupLabelView label) {
        return new GroupLabelBean(
                label.getText(),
                label.getTitle(),
                label.getType()
        );
    }

    public static List<GroupLabelBean> toLabelsBeansList(List<GroupLabelView> labels) {
        return labels.stream()
                .map(GroupLabelBean::toLabelBean)
                .collect(CollectorsUtil.toImmutableList());
    }
}
