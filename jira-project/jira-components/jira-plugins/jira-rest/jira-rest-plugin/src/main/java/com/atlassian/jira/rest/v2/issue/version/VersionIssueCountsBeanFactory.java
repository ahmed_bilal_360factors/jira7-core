package com.atlassian.jira.rest.v2.issue.version;

import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.CustomFieldWithVersionUsage;

import javax.annotation.Nullable;
import java.util.Collection;

/**
 * Simple factory used to create version issue counts bean from versions and count data.
 *
 * @since v4.4
 */
public interface VersionIssueCountsBeanFactory {
    /**
     * Create a VersionBean given the passed Version.
     *
     * @param version                                  the version to convert.
     * @param fixIssueCount                            the version to convert.
     * @param affectsIssueCount                        the version to convert.
     * @param issueCountWithCustomFieldsShowingVersion number of issues where custom fields shows given version
     * @param customFieldUsageList                         custom fields names affected by version
     * @return the VersionIssueCountsBean from the passed Version.
     */
    VersionIssueCountsBean createVersionBean(Version version,
                                             long fixIssueCount,
                                             long affectsIssueCount,
                                             long issueCountWithCustomFieldsShowingVersion,
                                             @Nullable
                                             Collection<CustomFieldWithVersionUsage> customFieldUsageList);
}
