package com.atlassian.jira.rest.v2.admin.permissionscheme;

import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.atlassian.jira.permission.PermissionGrant;
import com.atlassian.jira.permission.PermissionGrantInput;
import com.atlassian.jira.permission.PermissionHolderType;
import com.atlassian.jira.permission.PermissionScheme;
import com.atlassian.jira.permission.PermissionSchemeInput;
import com.atlassian.jira.rest.api.permission.PermissionGrantBean;
import com.atlassian.jira.rest.api.permission.PermissionGrantBeanExpander;
import com.atlassian.jira.rest.api.permission.PermissionHolderBean;
import com.atlassian.jira.rest.api.permission.PermissionHolderTypeMapping;
import com.atlassian.jira.rest.api.permission.PermissionSchemeBean;
import com.atlassian.jira.rest.api.permission.PermissionSchemeBeansFactory;
import com.atlassian.jira.rest.api.permission.PermissionSchemeExpandParam;
import com.atlassian.jira.rest.util.SelfLinkBuilder;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.ErrorCollections;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.google.common.base.Function;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static com.atlassian.fugue.Either.left;
import static com.atlassian.fugue.Either.right;
import static com.atlassian.fugue.Eithers.sequenceRight;
import static com.atlassian.fugue.Option.option;
import static com.atlassian.jira.permission.PermissionHolder.holder;
import static com.atlassian.jira.permission.data.CustomPermissionHolderType.permissionHolderType;
import static com.atlassian.jira.rest.api.permission.PermissionHolderBean.holderBean;
import static com.atlassian.jira.rest.v2.admin.permissionscheme.PermissionSchemeResource.ENTITY_PATH;
import static com.atlassian.jira.rest.v2.admin.permissionscheme.PermissionSchemeResource.RESOURCE_PATH;
import static com.google.common.base.Objects.firstNonNull;
import static com.google.common.base.Strings.isNullOrEmpty;
import static com.google.common.collect.Iterables.transform;

@ExportAsService
@Component
public final class PermissionSchemeBeansFactoryImpl implements PermissionSchemeBeansFactory {
    private final PermissionGrantBeanExpander expander;
    private final SelfLinkBuilder.SelfLink selfLink;
    private final I18nHelper i18n;

    @Autowired
    public PermissionSchemeBeansFactoryImpl(final SelfLinkBuilder selfLinkBuilder, final PermissionGrantBeanExpander expander, final I18nHelper i18n) {
        this.expander = expander;
        this.i18n = i18n;
        this.selfLink = selfLinkBuilder.path(RESOURCE_PATH);
    }

    @Override
    public PermissionSchemeBean toBean(final PermissionScheme scheme, final List<PermissionSchemeExpandParam> expands) {
        URI self = selfLink.path(scheme.getId().toString()).toUri();

        PermissionSchemeBean.Builder bean = PermissionSchemeBean.builder()
                .setId(scheme.getId())
                .setName(scheme.getName())
                .setSelf(self)
                .setDescription(Strings.emptyToNull(scheme.getDescription()));

        if (!expands.isEmpty()) {
            bean.setPermissions(transform(scheme.getPermissions(), new Function<PermissionGrant, PermissionGrantBean>() {
                @Override
                public PermissionGrantBean apply(final PermissionGrant schemeEntity) {
                    return toBean(schemeEntity, scheme.getId(), expands);
                }
            }));
        }

        return bean.build();
    }

    @Override
    public PermissionGrantBean toBean(final PermissionGrant permissionGrant, Long schemeId, final List<PermissionSchemeExpandParam> expands) {
        URI self = selfLink.path(schemeId.toString(), ENTITY_PATH, permissionGrant.getId().toString()).toUri();

        PermissionHolderType type = permissionGrant.getHolder().getType();
        String holderType = PermissionHolderTypeMapping.toRestType(type);

        PermissionHolderBean holderBean = holderBean(holderType, permissionGrant.getHolder().getParameter().getOrNull());
        return PermissionGrantBean.builder()
                .setId(permissionGrant.getId())
                .setSelf(self)
                .setHolder(expander.expand(holderBean, permissionGrant.getHolder().getType(), expands))
                .setPermission(permissionGrant.getPermission().permissionKey())
                .build();
    }

    @Override
    public Either<ErrorCollection, PermissionSchemeInput> fromBean(final PermissionSchemeBean permissionScheme) {
        return validated(permissionScheme).flatMap(new Function<PermissionSchemeBean, Either<ErrorCollection, PermissionSchemeInput>>() {
            @Override
            public Either<ErrorCollection, PermissionSchemeInput> apply(final PermissionSchemeBean permissionScheme) {
                return fromBean(permissionScheme.getPermissions()).map(new Function<Collection<PermissionGrantInput>, PermissionSchemeInput>() {
                    @Override
                    public PermissionSchemeInput apply(final Collection<PermissionGrantInput> permissionGrants) {
                        return PermissionSchemeInput.builder(permissionScheme.getName())
                                .setDescription(permissionScheme.getDescription())
                                .setPermissions(permissionGrants)
                                .build();
                    }
                });
            }
        });
    }

    @Override
    public Either<ErrorCollection, Collection<PermissionGrantInput>> fromBean(final List<PermissionGrantBean> permissions) {
        return sequenceRight(transform(firstNonNull(permissions, Collections.<PermissionGrantBean>emptyList()), new Function<PermissionGrantBean, Either<ErrorCollection, PermissionGrantInput>>() {
            @Override
            public Either<ErrorCollection, PermissionGrantInput> apply(final PermissionGrantBean grantBean) {
                return fromBean(grantBean);
            }
        })).map(new Function<Iterable<PermissionGrantInput>, Collection<PermissionGrantInput>>() {
            @Override
            public Collection<PermissionGrantInput> apply(final Iterable<PermissionGrantInput> grants) {
                return ImmutableList.copyOf(grants);
            }
        });
    }

    @Override
    public Either<ErrorCollection, PermissionGrantInput> fromBean(final PermissionGrantBean grantBean) {
        return validated(grantBean).map(new Function<PermissionGrantBean, PermissionGrantInput>() {
            @Override
            public PermissionGrantInput apply(final PermissionGrantBean grantBean) {
                return PermissionGrantInput.newGrant(
                        holder(getActorType(grantBean).get(), grantBean.getHolder().getParameter()),
                        new ProjectPermissionKey(grantBean.getPermission()));
            }
        });
    }

    private Either<ErrorCollection, PermissionSchemeBean> validated(final PermissionSchemeBean bean) {
        if (isNullOrEmpty(bean.getName())) {
            return left(ErrorCollections.validationError("name", i18n.getText("rest.missing.field", "name")));
        }
        return right(bean);
    }

    private Either<ErrorCollection, PermissionGrantBean> validated(final PermissionGrantBean bean) {
        Option<? extends PermissionHolderType> actorType = getActorType(bean);
        PermissionHolderBean holder = firstNonNull(bean.getHolder(), new PermissionHolderBean());

        ErrorCollection errors = ErrorCollections.empty();
        if (actorType.isEmpty()) {
            errors.addError("holder.type", i18n.getText("rest.permissionscheme.input.error.unrecognized.holder.type", holder.getType()));
        } else {
            if (actorType.get().requiresParameter() && isNullOrEmpty(holder.getParameter())) {
                errors.addError("holder.parameter", i18n.getText("rest.permissionscheme.input.error.holder.parameter.required", holder.getType()));
            } else if (!actorType.get().requiresParameter() && !isNullOrEmpty(holder.getParameter())) {
                errors.addError("holder.parameter", i18n.getText("rest.permissionscheme.input.error.holder.parameter.unexpected", holder.getType()));
            }
        }
        if (Strings.isNullOrEmpty(bean.getPermission())) {
            errors.addError("permission", i18n.getText("rest.missing.field", "permission"));
        }
        return errors.hasAnyErrors() ? Either.<ErrorCollection, PermissionGrantBean>left(errors) :
                Either.<ErrorCollection, PermissionGrantBean>right(bean);
    }

    private Option<PermissionHolderType> getActorType(final PermissionGrantBean grantBean) {
        return Option.option(grantBean.getHolder()).flatMap(new Function<PermissionHolderBean, Option<PermissionHolderType>>() {
            @Override
            public Option<PermissionHolderType> apply(final PermissionHolderBean holder) {
                return option(holder.getType()).map(new Function<String, PermissionHolderType>() {
                    @Override
                    public PermissionHolderType apply(final String holderType) {
                        return PermissionHolderTypeMapping.fromRestType(holderType).orElse(permissionHolderType(holderType, holder.getParameter()));
                    }
                });
            }
        });
    }
}
