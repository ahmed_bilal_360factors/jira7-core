package com.atlassian.jira.rest.v2.securitylevel;

import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.fields.rest.json.beans.SecurityLevelJsonBean;
import com.atlassian.jira.issue.security.IssueSecurityLevel;
import com.atlassian.jira.issue.security.IssueSecurityLevelManager;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.rest.util.ProjectFinder;
import com.atlassian.jira.rest.util.ResponseFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.rest.annotation.ResponseType;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Provide security level information of the given project for the current user.
 *
 * @since v7.0
 */
@Path("project/{projectKeyOrId}/securitylevel")
@AnonymousAllowed
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class ProjectSecurityLevelResource {
    private final ProjectFinder projectFinder;
    private final IssueSecurityLevelManager issueSecurityLevelManager;
    private final JiraBaseUrls baseUrls;
    private final JiraAuthenticationContext authenticationContext;
    private final PermissionManager permissionManager;
    private final ResponseFactory responseFactory;

    public ProjectSecurityLevelResource(
            ProjectFinder projectFinder,
            IssueSecurityLevelManager issueSecurityLevelManager,
            JiraBaseUrls baseUrls,
            JiraAuthenticationContext authenticationContext,
            PermissionManager permissionManager,
            ResponseFactory responseFactory) {
        this.projectFinder = projectFinder;
        this.issueSecurityLevelManager = issueSecurityLevelManager;
        this.baseUrls = baseUrls;
        this.authenticationContext = authenticationContext;
        this.permissionManager = permissionManager;
        this.responseFactory = responseFactory;
    }

    /**
     * Returns all security levels for the project that the current logged in user has access to.
     * If the user does not have the Set Issue Security permission, the list will be empty.
     *
     * @param projectKeyOrId - key or id of project to list the security levels for
     * @return list of security levels
     * @response.representation.200.qname securityLevels
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc Returns a list of all security levels in a project for which the current user has access.
     * @response.representation.200.example {@link com.atlassian.jira.rest.v2.securitylevel.SecurityListLevelJsonBean#DOC_EXAMPLE}
     * @response.representation.404.doc Returned if the project is not found or the user does not have permissions to browse it.
     */
    @GET
    @ResponseType(SecurityListLevelJsonBean.class)
    public Response getSecurityLevelsForProject(@PathParam("projectKeyOrId") final String projectKeyOrId) {
        ApplicationUser user = authenticationContext.getUser();
        ProjectService.GetProjectResult getProjectResult = projectFinder.getGetProjectByIdOrKey(user, projectKeyOrId);
        if (getProjectResult.isValid()) {
            if (!permissionManager.hasPermission(ProjectPermissions.SET_ISSUE_SECURITY, getProjectResult.getProject(), user)) {
                Collection<SecurityLevelJsonBean> empty = Collections.emptyList();
                return responseFactory.okNoCache(SecurityListLevelJsonBean.of(empty));
            }
            List<IssueSecurityLevel> usersSecurityLevels = issueSecurityLevelManager.getUsersSecurityLevels(getProjectResult.getProject(), user);
            return responseFactory.okNoCache(SecurityListLevelJsonBean.of(SecurityLevelJsonBean.shortBeans(usersSecurityLevels, baseUrls)));
        }
        return responseFactory.errorResponse(getProjectResult.getErrorCollection());
    }
}