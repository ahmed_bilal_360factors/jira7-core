package com.atlassian.jira.rest.v2.issue;

import com.atlassian.jira.issue.fields.rest.json.beans.UserJsonBean;
import com.atlassian.jira.rest.api.pagination.PageBean;
import com.google.common.collect.ImmutableList;

import java.net.URI;
import java.util.HashMap;

public class GroupResourceExample {
    public static final PageBean<UserJsonBean> PAGE_DOC_EXAMPLE;

    static {
        final ImmutableList<UserJsonBean> users = ImmutableList.<UserJsonBean>builder()
                .add(new UserJsonBean("http://example/jira/rest/api/2/user?username=fred", "Fred", "fred", "fred@atlassian.com", new HashMap<>(), "Fred", true, "Australia/Sydney"))
                .add(new UserJsonBean("http://example/jira/rest/api/2/user?username=barney", "Barney", "barney", "barney@atlassian.com", new HashMap<>(), "Barney", false, "Australia/Sydney"))
                .build();

        final URI self = Examples.restURI("group/member?groupname=jira-administrators&includeInactiveUsers=false&startAt=2&maxResults=2");
        final URI nextPage = Examples.restURI("group/member?groupname=jira-administrators&includeInactiveUsers=false&startAt=4&maxResults=2");

        PAGE_DOC_EXAMPLE = new PageBean<>(self, nextPage, 2, 3l, 5l, false, users);
    }
}
