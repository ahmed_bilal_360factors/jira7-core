package com.atlassian.jira.rest.v2.issue;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.fields.rest.json.beans.SecuritySchemeJsonBean;
import com.atlassian.jira.issue.security.IssueSecurityLevel;
import com.atlassian.jira.issue.security.IssueSecurityLevelScheme;
import com.atlassian.jira.issue.security.IssueSecuritySchemeService;
import com.atlassian.jira.rest.util.ResponseFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.rest.annotation.ResponseType;
import com.google.common.base.Function;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Resource for associating permission schemes and projects.
 *
 * @since 7.0
 */
@Path("project/{projectKeyOrId}/issuesecuritylevelscheme")
@Produces({MediaType.APPLICATION_JSON})
public class ProjectIssueSecurityLevelSchemeResource {
    private final IssueSecuritySchemeService issueSecuritySchemeService;
    private final ResponseFactory responseFactory;
    private final JiraBaseUrls baseUrls;
    private final JiraAuthenticationContext jiraAuthenticationContext;

    public ProjectIssueSecurityLevelSchemeResource(final IssueSecuritySchemeService issueSecuritySchemeService,
                                                   final ResponseFactory responseFactory,
                                                   final JiraBaseUrls baseUrls,
                                                   final JiraAuthenticationContext jiraAuthenticationContext) {
        this.issueSecuritySchemeService = issueSecuritySchemeService;
        this.responseFactory = responseFactory;
        this.baseUrls = baseUrls;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    /**
     * Returns the issue security scheme for project.
     *
     * @return an object containing an issue security scheme.
     * @response.representation.200.qname issuesecurityscheme
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc Returned if the user has the administrator permission or if the scheme is used in a project in which the user has the administrative permission.
     * @response.representation.200.example {@link com.atlassian.jira.rest.v2.issue.SecuritySchemeExample#DOC_FULL_BEAN_EXAMPLE}
     * @response.representation.401.doc Returned if the user is not logged in.
     * @response.representation.403.doc Returned if the project is visible for calling user, but the user doesn't have administrative permissions
     * @response.representation.404.doc Returned if the project does not exist, or is not visible to the calling user
     */
    @GET
    @ResponseType(SecuritySchemeJsonBean.class)
    public Response getIssueSecurityScheme(@PathParam("projectKeyOrId") final String projectKeyOrId) {
        final ApplicationUser user = jiraAuthenticationContext.getLoggedInUser();

        final ServiceOutcome<IssueSecurityLevelScheme> issueSecurityLevelScheme = getIssueSecurityLevelScheme(user, projectKeyOrId);
        return responseFactory.validateOutcome(issueSecurityLevelScheme).left().on(new Function<IssueSecurityLevelScheme, Response>() {
            @Override
            public Response apply(final IssueSecurityLevelScheme securityLevelScheme) {
                ServiceOutcome<? extends List<IssueSecurityLevel>> issueSecurityLevels = issueSecuritySchemeService.getIssueSecurityLevels(user, securityLevelScheme.getId());
                return responseFactory.validateOutcome(issueSecurityLevels).left().on(new Function<List<IssueSecurityLevel>, Response>() {
                    @Override
                    public Response apply(final List<IssueSecurityLevel> issueSecurityLevels) {
                        return responseFactory.okNoCache(
                                SecuritySchemeJsonBean.fullBean(securityLevelScheme, baseUrls, issueSecurityLevels));
                    }
                });
            }
        });
    }

    private ServiceOutcome<IssueSecurityLevelScheme> getIssueSecurityLevelScheme(final ApplicationUser user, final String projectKeyOrId) {
        try {
            return issueSecuritySchemeService.getIssueSecurityLevelSchemeForProject(user, Long.parseLong(projectKeyOrId));
        } catch (NumberFormatException e) {
            return issueSecuritySchemeService.getIssueSecurityLevelSchemeForProject(user, projectKeyOrId);
        }
    }
}
