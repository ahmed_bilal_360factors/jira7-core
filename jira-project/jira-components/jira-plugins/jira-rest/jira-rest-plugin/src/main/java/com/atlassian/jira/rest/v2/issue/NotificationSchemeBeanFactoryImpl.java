package com.atlassian.jira.rest.v2.issue;

import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.event.type.EventTypeManager;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.rest.json.UserBeanFactory;
import com.atlassian.jira.issue.fields.rest.json.beans.GroupJsonBean;
import com.atlassian.jira.issue.fields.rest.json.beans.GroupJsonBeanBuilder;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.fields.rest.json.beans.UserJsonBean;
import com.atlassian.jira.issue.search.managers.SearchHandlerManager;
import com.atlassian.jira.notification.CustomFieldValueNotification;
import com.atlassian.jira.notification.EmailNotification;
import com.atlassian.jira.notification.EventNotifications;
import com.atlassian.jira.notification.GroupNotification;
import com.atlassian.jira.notification.Notification;
import com.atlassian.jira.notification.NotificationScheme;
import com.atlassian.jira.notification.NotificationVisitor;
import com.atlassian.jira.notification.ProjectRoleNotification;
import com.atlassian.jira.notification.RoleNotification;
import com.atlassian.jira.notification.UserNotification;
import com.atlassian.jira.rest.api.field.FieldBean;
import com.atlassian.jira.rest.api.notification.AbstractNotificationBean;
import com.atlassian.jira.rest.api.notification.CustomFieldValueNotificationBean;
import com.atlassian.jira.rest.api.notification.EmailNotificationBean;
import com.atlassian.jira.rest.api.notification.GroupNotificationBean;
import com.atlassian.jira.rest.api.notification.NotificationEventBean;
import com.atlassian.jira.rest.api.notification.NotificationSchemeBean;
import com.atlassian.jira.rest.api.notification.NotificationSchemeBeanFactory;
import com.atlassian.jira.rest.api.notification.NotificationSchemeEventBean;
import com.atlassian.jira.rest.api.notification.NotificationSchemeExpandParam;
import com.atlassian.jira.rest.api.notification.ProjectRoleNotificationBean;
import com.atlassian.jira.rest.api.notification.RoleNotificationBean;
import com.atlassian.jira.rest.api.notification.UserNotificationBean;
import com.atlassian.jira.rest.api.project.ProjectRoleBean;
import com.atlassian.jira.rest.api.util.ExpandParameterParser;
import com.atlassian.jira.rest.v2.issue.project.ProjectRoleBeanFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@ExportAsService
@Component
public class NotificationSchemeBeanFactoryImpl extends ExpandParameterParser<NotificationSchemeExpandParam> implements NotificationSchemeBeanFactory {
    private final GroupJsonBeanBuilder groupJsonBeanBuilder;
    private final JiraBaseUrls jiraBaseUrls;
    private final ProjectRoleBeanFactory projectRoleBeanFactory;
    private final SearchHandlerManager searchHandlerManager;
    private final FieldManager fieldManager;
    private final EventTypeManager eventTypeManager;
    private final UserBeanFactory userBeanFactory;
    private final JiraAuthenticationContext authenticationContext;

    @Autowired
    public NotificationSchemeBeanFactoryImpl(
            final I18nHelper i18nHelper,
            final JiraBaseUrls jiraBaseUrls,
            final ProjectRoleBeanFactory projectRoleBeanFactory,
            final SearchHandlerManager searchHandlerManager,
            final FieldManager fieldManager,
            final EventTypeManager eventTypeManager,
            final UserBeanFactory userBeanFactory,
            final JiraAuthenticationContext authenticationContext) {
        super(i18nHelper, NotificationSchemeExpandParam.class);
        this.jiraBaseUrls = jiraBaseUrls;
        this.projectRoleBeanFactory = projectRoleBeanFactory;
        this.searchHandlerManager = searchHandlerManager;
        this.fieldManager = fieldManager;
        this.eventTypeManager = eventTypeManager;
        this.userBeanFactory = userBeanFactory;
        this.authenticationContext = authenticationContext;
        this.groupJsonBeanBuilder = new GroupJsonBeanBuilder(jiraBaseUrls);
    }

    @Override
    public NotificationSchemeBean createNotificationSchemeBean(final NotificationScheme notificationScheme, final List<NotificationSchemeExpandParam> expand) {
        return new NotificationSchemeBean(notificationScheme.getId(),
                getNotificationSchemeSelf(notificationScheme),
                notificationScheme.getName(),
                notificationScheme.getDescription(),
                expand.size() > 0 ? transformNotificationEvents(notificationScheme, expand) : null);
    }

    private Iterable<NotificationSchemeEventBean> transformNotificationEvents(final NotificationScheme notificationScheme, final List<NotificationSchemeExpandParam> expand) {
        final List<EventNotifications> eventNotifications = notificationScheme.getEventNotifications();
        return Iterables.transform(eventNotifications, new Function<EventNotifications, NotificationSchemeEventBean>() {
            @Override
            public NotificationSchemeEventBean apply(final EventNotifications eventNotifications) {
                return new NotificationSchemeEventBean(event(eventNotifications.getEventType()),
                        createNotificationBeans(eventNotifications.getNotifications(), expand));
            }
        });
    }

    private Iterable<AbstractNotificationBean> createNotificationBeans(final Iterable<Notification> notifications,
                                                                       final List<NotificationSchemeExpandParam> expand) {
        return Iterables.transform(notifications, new Function<Notification, AbstractNotificationBean>() {
            @Override
            public AbstractNotificationBean apply(final Notification notification) {
                return notification.accept(new NotificationVisitor<AbstractNotificationBean>() {
                    @Override
                    public AbstractNotificationBean visit(final UserNotification userNotification) {
                        final UserJsonBean userBean = expand(expand, NotificationSchemeExpandParam.user) ?
                                userBeanFactory.createBean(userNotification.getApplicationUser(), authenticationContext.getUser()) : null;
                        return new UserNotificationBean(userNotification.getId(),
                                userNotification.getNotificationType(),
                                userNotification.getParameter(),
                                userBean);
                    }

                    @Override
                    public AbstractNotificationBean visit(final GroupNotification groupNotification) {
                        final GroupJsonBean groupBean = expand(expand, NotificationSchemeExpandParam.group) ?
                                groupJsonBeanBuilder.group(groupNotification.getGroup()).build() : null;
                        return new GroupNotificationBean(groupNotification.getId(),
                                groupNotification.getNotificationType(),
                                groupNotification.getParameter(),
                                groupBean);
                    }

                    @Override
                    public AbstractNotificationBean visit(final ProjectRoleNotification projectRoleNotification) {
                        final ProjectRoleBean projectRoleBean = expand(expand, NotificationSchemeExpandParam.projectRole) ?
                                projectRoleBeanFactory.shortRoleBean(projectRoleNotification.getProjectRole()) : null;
                        return new ProjectRoleNotificationBean(projectRoleNotification.getId(),
                                projectRoleNotification.getNotificationType(),
                                projectRoleNotification.getParameter(),
                                projectRoleBean);
                    }

                    @Override
                    public AbstractNotificationBean visit(final EmailNotification emailNotification) {
                        return new EmailNotificationBean(emailNotification.getId(),
                                emailNotification.getNotificationType(),
                                emailNotification.getEmail());
                    }

                    @Override
                    public AbstractNotificationBean visit(final CustomFieldValueNotification customFieldValueNotification) {
                        final FieldBean fieldBean = expand(expand, NotificationSchemeExpandParam.field) ?
                                FieldBean.shortBean(customFieldValueNotification.getCustomField(), fieldManager, searchHandlerManager) : null;
                        return new CustomFieldValueNotificationBean(customFieldValueNotification.getId(),
                                customFieldValueNotification.getNotificationType(),
                                customFieldValueNotification.getParameter(),
                                fieldBean);
                    }

                    @Override
                    public AbstractNotificationBean visit(final RoleNotification roleNotification) {
                        return new RoleNotificationBean(roleNotification.getId(), roleNotification.getNotificationType());
                    }
                });
            }
        });
    }

    private boolean expand(final List<NotificationSchemeExpandParam> expandQueryParameter, final NotificationSchemeExpandParam expectedExpandParameter) {
        return expandQueryParameter.contains(expectedExpandParameter) || expandQueryParameter.contains(NotificationSchemeExpandParam.all);
    }

    private NotificationEventBean event(final EventType eventType) {
        if (!eventType.isSystemEventType()) {
            final EventType templateEventType = eventTypeManager.getEventType(eventType.getTemplateId());
            final NotificationEventBean templateEventBean =
                    new NotificationEventBean(templateEventType.getId(), templateEventType.getName(), templateEventType.getDescription(), null);

            return new NotificationEventBean(eventType.getId(), eventType.getName(), eventType.getDescription(), templateEventBean);
        } else {
            return new NotificationEventBean(eventType.getId(), eventType.getName(), eventType.getDescription(), null);
        }
    }

    private String getNotificationSchemeSelf(final NotificationScheme notificationScheme) {
        return jiraBaseUrls.restApi2BaseUrl() + "notificationscheme/" + notificationScheme.getId();
    }
}
