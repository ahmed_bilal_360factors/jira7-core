package com.atlassian.jira.rest.util;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationKeys;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.bc.user.UserService;
import com.atlassian.jira.exception.AddException;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.rest.exception.ForbiddenWebException;
import com.atlassian.jira.rest.exception.NotAuthorisedWebException;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.SimpleWarningCollection;
import com.atlassian.jira.util.WarningCollection;
import com.google.common.collect.ImmutableSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @since v7.0
 */
@Component
public class UpdateUserApplicationHelper {
    private final UserService userService;
    private final UserManager userManager;
    private final I18nHelper i18n;
    private final ApplicationRoleManager applicationRoleManager;
    private final GlobalPermissionManager permissionManager;
    private final JiraAuthenticationContext authContext;

    @Autowired
    public UpdateUserApplicationHelper(final UserService userService,
                                       final UserManager userManager,
                                       final I18nHelper i18n,
                                       final ApplicationRoleManager applicationRoleManager,
                                       final GlobalPermissionManager permissionManager,
                                       final JiraAuthenticationContext authContext) {
        this.userService = userService;
        this.userManager = userManager;
        this.i18n = i18n;
        this.applicationRoleManager = applicationRoleManager;
        this.permissionManager = permissionManager;
        this.authContext = authContext;
    }

    public ApplicationUpdateResult addUserToApplication(final String username, final String applicationKey) {
        return doApplicationUpdate(username, applicationKey, new ApplicationUpdateCommand() {
            @Override
            ApplicationUpdateResult execute(final ApplicationUser user, final ApplicationKey appKey) throws PermissionException, RemoveException, AddException {
                final UserService.AddUserToApplicationValidationResult validationResult = userService.validateAddUserToApplication(user, appKey);
                if (!validationResult.isValid()) {
                    final ErrorCollection errorCollection = new SimpleErrorCollection(validationResult.getErrorCollection());
                    errorCollection.setReasons(ImmutableSet.of(ErrorCollection.Reason.VALIDATION_FAILED));
                    return new ApplicationUpdateResult(user, errorCollection);
                }

                userService.addUserToApplication(validationResult);

                return new ApplicationUpdateResult(user);
            }
        });
    }

    public ApplicationUpdateResult removeUserFromApplication(final String username, final String applicationKey) {
        return doApplicationUpdate(username, applicationKey, new ApplicationUpdateCommand() {
            @Override
            ApplicationUpdateResult execute(final ApplicationUser user, final ApplicationKey appKey) throws PermissionException, RemoveException, AddException {
                final UserService.RemoveUserFromApplicationValidationResult validationResult = userService.validateRemoveUserFromApplication(authContext.getLoggedInUser(), user, appKey);
                if (!validationResult.isValid()) {
                    final ErrorCollection errorCollection = new SimpleErrorCollection(validationResult.getErrorCollection());
                    errorCollection.setReasons(ImmutableSet.of(ErrorCollection.Reason.VALIDATION_FAILED));
                    return new ApplicationUpdateResult(user, errorCollection);
                }

                userService.removeUserFromApplication(validationResult);

                return new ApplicationUpdateResult(user);
            }
        });
    }

    private void isCurrentUserLoggedInAndAdmin() {
        final ApplicationUser loggedUser = authContext.getLoggedInUser();
        if (loggedUser == null) {
            throw new NotAuthorisedWebException(com.atlassian.jira.rest.api.util.ErrorCollection.of(i18n.getText("rest.authentication.no.user.logged.in")));
        }

        final boolean isGlobalAdmin = permissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, loggedUser);
        if (!isGlobalAdmin) {
            throw new ForbiddenWebException(com.atlassian.jira.rest.api.util.ErrorCollection.of(i18n.getText("rest.authorization.admin.required")));
        }
    }

    private ApplicationUpdateResult doApplicationUpdate(final String username, final String applicationKey, final ApplicationUpdateCommand command) {
        isCurrentUserLoggedInAndAdmin();

        if (username == null) {
            return updateError(i18n.getText("rest.user.error.no.username.param"), ErrorCollection.Reason.VALIDATION_FAILED);
        }

        final ApplicationUser user = userManager.getUserByName(username);
        if (user == null) {
            return updateError(i18n.getText("rest.user.error.not.found", username), ErrorCollection.Reason.NOT_FOUND);
        }

        final Either<String, ApplicationKey> eitherApplicationKey = ApplicationKeys.TO_APPLICATION_KEY.apply(applicationKey);
        if (eitherApplicationKey == null || eitherApplicationKey.isLeft()) {
            return updateError(i18n.getText("rest.user.error.not.valid.application.key", applicationKey), ErrorCollection.Reason.VALIDATION_FAILED, user);
        }
        final ApplicationKey appKey = eitherApplicationKey.right().get();

        try {
            return command.execute(user, appKey);
        } catch (PermissionException e) {
            return updateError(i18n.getText("error.no-permission"), ErrorCollection.Reason.FORBIDDEN, user);
        } catch (RemoveException | AddException e) {
            return updateError(e.getLocalizedMessage(), ErrorCollection.Reason.CONFLICT, user);
        }
    }

    private ApplicationUpdateResult updateError(String message, ErrorCollection.Reason reason, ApplicationUser user) {
        final ErrorCollection errorCollection = new SimpleErrorCollection();
        errorCollection.addErrorMessage(message, reason);
        return new ApplicationUpdateResult(user, errorCollection);
    }

    private ApplicationUpdateResult updateError(String message, ErrorCollection.Reason reason) {
        return updateError(message, reason, null);
    }

    private abstract static class ApplicationUpdateCommand {
        abstract ApplicationUpdateResult execute(final ApplicationUser user, final ApplicationKey appKey) throws PermissionException, RemoveException, AddException;
    }

    public static class ApplicationUpdateResult implements ServiceResult {
        final ErrorCollection errorCollection;
        final Option<ApplicationUser> applicationUser;
        final WarningCollection warningCollection;


        public ApplicationUpdateResult(final ApplicationUser applicationUser,
                                       final ErrorCollection errorCollection,
                                       final WarningCollection warningCollection) {
            this.errorCollection = errorCollection;
            this.applicationUser = Option.option(applicationUser);
            this.warningCollection = warningCollection;
        }

        public ApplicationUpdateResult(final ApplicationUser applicationUser, final ErrorCollection errorCollection) {
            this(applicationUser, errorCollection, new SimpleWarningCollection());
        }

        public ApplicationUpdateResult(final ApplicationUser applicationUser) {
            this(applicationUser, new SimpleErrorCollection());
        }

        @Override
        public boolean isValid() {
            return !errorCollection.hasAnyErrors();
        }

        @Override
        public ErrorCollection getErrorCollection() {
            return errorCollection;
        }

        @Override
        public WarningCollection getWarningCollection() {
            return warningCollection;
        }

        public Option<ApplicationUser> getApplicationUser() {
            return applicationUser;
        }
    }
}
