package com.atlassian.jira.rest.v2.permission;

import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.bc.project.ProjectAction;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.permission.PermissionScheme;
import com.atlassian.jira.permission.PermissionSchemeService;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.rest.api.permission.PermissionGrantBeanExpander;
import com.atlassian.jira.rest.api.permission.PermissionSchemeBean;
import com.atlassian.jira.rest.api.permission.PermissionSchemeBeansFactory;
import com.atlassian.jira.rest.api.permission.PermissionSchemeExpandParam;
import com.atlassian.jira.rest.util.ProjectFinder;
import com.atlassian.jira.rest.util.ResponseFactory;
import com.atlassian.jira.rest.v2.common.IdBean;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.rest.annotation.ResponseType;
import com.google.common.base.Function;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Resource for associating permission schemes and projects.
 *
 * @since 6.5
 */
@Path("project/{projectKeyOrId}/permissionscheme")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public final class ProjectPermissionSchemeResource {
    private final PermissionSchemeService permissionSchemeService;
    private final ProjectFinder projectFinder;
    private final ResponseFactory responseFactory;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final PermissionSchemeBeansFactory beansFactory;
    private final PermissionGrantBeanExpander expander;

    public ProjectPermissionSchemeResource(final PermissionSchemeService permissionSchemeService,
                                           final ProjectFinder projectFinder,
                                           final ResponseFactory responseFactory,
                                           final JiraAuthenticationContext jiraAuthenticationContext,
                                           final PermissionSchemeBeansFactory beansFactory,
                                           final PermissionGrantBeanExpander expander) {
        this.permissionSchemeService = permissionSchemeService;
        this.projectFinder = projectFinder;
        this.responseFactory = responseFactory;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.beansFactory = beansFactory;
        this.expander = expander;
    }

    /**
     * Assigns a permission scheme with a project.
     *
     * @param projectKeyOrId     key or id of the project
     * @param permissionSchemeId object that contains an id of the scheme
     * @return The newly associated permission scheme if successful, appropriate error otherwise.
     * @response.representation.200.qname permissionScheme
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc Shortened details of the newly associated permission scheme.
     * @response.representation.200.example {@link com.atlassian.jira.rest.v2.admin.permissionscheme.PermissionSchemeBeans#SCHEME_SHORT_EXAMPLE}
     * @request.representation.example {@link com.atlassian.jira.rest.v2.common.IdBean#EXAMPLE}
     * @response.representation.401.doc Returned if the user is not logged in.
     * @response.representation.403.doc Returned if the user does not have permissions to edit project's permission schemes. In practice the user needs to be a JIRA administrator.
     * @response.representation.404.doc Returned if the project or permission scheme is not found.
     */
    @PUT
    @ResponseType(PermissionSchemeBean.class)
    public Response assignPermissionScheme(@PathParam("projectKeyOrId") final String projectKeyOrId, final IdBean permissionSchemeId, @QueryParam("expand") String expand) {

        return responseFactory.toResponse(expander.parseExpandQuery(expand)).left().on(new Function<List<PermissionSchemeExpandParam>, Response>() {
            @Override
            public Response apply(final List<PermissionSchemeExpandParam> expands) {
                final ApplicationUser user = jiraAuthenticationContext.getUser();
                ProjectService.GetProjectResult projectGetResult = projectFinder.getGetProjectForActionByIdOrKey(user, projectKeyOrId, ProjectAction.VIEW_PROJECT);
                return responseFactory.validateOutcome(projectGetResult).left().on(new Function<Project, Response>() {
                    @Override
                    public Response apply(final Project project) {
                        ServiceResult serviceResult = permissionSchemeService.assignPermissionSchemeToProject(user, permissionSchemeId.getId(), project.getId());
                        if (serviceResult.isValid()) {
                            return getProjectPermissionScheme(user, project, expands);
                        } else {
                            return responseFactory.errorResponse(serviceResult.getErrorCollection());
                        }
                    }
                });

            }
        });
    }

    /**
     * Gets a permission scheme assigned with a project.
     *
     * @param projectKeyOrId key or id of the project
     * @return The associated permission scheme if successful, appropriate error otherwise.
     * @response.representation.200.qname permissionScheme
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc The associated permission scheme.
     * @response.representation.200.example {@link com.atlassian.jira.rest.v2.admin.permissionscheme.PermissionSchemeBeans#SCHEME_SHORT_EXAMPLE}
     * @response.representation.401.doc Returned if the user is not logged in.
     * @response.representation.403.doc Returned if the user does not have permissions to view the project's configuration. In practice the user needs to be
     * a JRA administrator or the project's administrator to get the assigned permission scheme.
     * @response.representation.404.doc Returned if the project is not found (or the user does not have permissions to view the project).
     */
    @GET
    @ResponseType(PermissionSchemeBean.class)
    public Response getAssignedPermissionScheme(@PathParam("projectKeyOrId") final String projectKeyOrId, @QueryParam("expand") String expand) {
        return responseFactory.toResponse(expander.parseExpandQuery(expand)).left().on(new Function<List<PermissionSchemeExpandParam>, Response>() {
            @Override
            public Response apply(final List<PermissionSchemeExpandParam> expands) {
                final ApplicationUser user = jiraAuthenticationContext.getUser();
                ProjectService.GetProjectResult projectGetResult = projectFinder.getGetProjectForActionByIdOrKey(user, projectKeyOrId, ProjectAction.VIEW_PROJECT);
                return responseFactory.validateOutcome(projectGetResult).left().on(new Function<Project, Response>() {
                    @Override
                    public Response apply(final Project project) {
                        return getProjectPermissionScheme(user, project, expands);
                    }
                });

            }
        });


    }

    private Response getProjectPermissionScheme(final ApplicationUser user, final Project project, final List<PermissionSchemeExpandParam> expands) {
        return responseFactory.validateOutcome(permissionSchemeService.getSchemeAssignedToProject(user, project.getId())).left().on(new Function<PermissionScheme, Response>() {
            @Override
            public Response apply(final PermissionScheme input) {
                return responseFactory.okNoCache(beansFactory.toBean(input, expands));
            }
        });
    }

}
