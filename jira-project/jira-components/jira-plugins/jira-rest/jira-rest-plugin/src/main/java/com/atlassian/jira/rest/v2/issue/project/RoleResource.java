package com.atlassian.jira.rest.v2.issue.project;

import com.atlassian.fugue.Either;
import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.bc.projectroles.ProjectRoleService;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.rest.api.project.ProjectRoleBean;
import com.atlassian.jira.rest.util.ResponseFactory;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.roles.DefaultRoleActors;
import com.atlassian.jira.security.roles.DefaultRoleActorsImpl;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleActor;
import com.atlassian.jira.security.roles.ProjectRoleImpl;
import com.atlassian.jira.security.roles.RoleActor;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.google.common.base.Function;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.atlassian.rest.annotation.ResponseType;

/**
 * @since 6.4
 */
@Path("role")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@WebSudoRequired
public class RoleResource {

    private final ProjectRoleService projectRoleService;
    private final ProjectRoleBeanFactory projectRoleBeanFactory;
    private final ResponseFactory responses;
    private final I18nHelper i18n;
    private final JiraAuthenticationContext authContext;
    private final GlobalPermissionManager permissionManager;

    public RoleResource(
            final ProjectRoleService projectRoleService,
            final ProjectRoleBeanFactory projectRoleBeanFactory,
            final ResponseFactory responses,
            final I18nHelper i18n,
            final JiraAuthenticationContext authContext,
            final GlobalPermissionManager permissionManager) {
        this.projectRoleService = projectRoleService;
        this.projectRoleBeanFactory = projectRoleBeanFactory;
        this.responses = responses;
        this.i18n = i18n;
        this.authContext = authContext;
        this.permissionManager = permissionManager;
    }

    /**
     * Get all the ProjectRoles available in JIRA. Currently this list is global.
     *
     * @return Returns full details of the roles available in JIRA.
     * @request.representation.200.mediaType application/json
     * @response.representation.200.doc Returned if the user is authenticated
     * @response.representation.200.example {@link com.atlassian.jira.rest.api.project.ProjectRoleBean#ROLE_SIMPLE_EXAMPLE}
     * @response.representation.401.doc Returned if you do not have permissions or you are not logged in.
     * @response.representation.403.doc Returned if the requesting user is not an admin or a sysadmin.
     */
    @GET
    @ResponseType(ProjectRoleBean.class)
    public Response getProjectRoles() {
        final List<ProjectRoleBean> roles = new ArrayList<>();
        final SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        final Collection<ProjectRole> projectRoles = projectRoleService.getProjectRoles(errorCollection);

        if (errorCollection.hasAnyErrors()) {
            return responses.errorResponse(errorCollection);
        }

        for (ProjectRole projectRole : projectRoles) {
            final DefaultRoleActors actors = projectRoleService.getDefaultRoleActors(projectRole, errorCollection);
            if (errorCollection.hasAnyErrors()) {
                return responses.errorResponse(errorCollection);
            }
            roles.add(projectRoleBeanFactory.projectRole(projectRole, actors));
        }

        return Response.ok(roles).build();

    }

    /**
     * Get a specific ProjectRole available in JIRA.
     *
     * @return Returns full details of the role available in JIRA.
     * @response.representation.200.doc Returned if the user is authenticated
     * @response.representation.200.example {@link com.atlassian.jira.rest.api.project.ProjectRoleBean#ROLE_SIMPLE_EXAMPLE}
     * @response.representation.401.doc Returned if you do not have permissions or you are not logged in.
     * @response.representation.403.doc Returned if the requesting user is not an admin or a sysadmin.
     * @response.representation.404.doc Returned if the role with the given id does not exist.
     */
    @GET
    @Path("{id}")
    @ResponseType(ProjectRoleBean.class)
    public Response getProjectRolesById(@PathParam("id") final long roleId) {
        return getProjectRole(roleId).left().on(new com.google.common.base.Function<ProjectRole, Response>() {
            @Override
            public Response apply(final ProjectRole projectRole) {
                return withDefaultRoleActors(projectRole).left().on(new Function<DefaultRoleActors, Response>() {
                    @Nullable
                    @Override
                    public Response apply(@Nullable final DefaultRoleActors defaultRoleActors) {
                        return Response.ok(projectRoleBeanFactory.projectRole(projectRole, defaultRoleActors)).build();
                    }
                });
            }
        });
    }

    /**
     * Creates a new ProjectRole to be available in JIRA.
     * The created role does not have any default actors assigned.
     *
     * @return Returns full details of the created role
     * @request.representation.mediaType application/json
     * @request.representation.example {@link com.atlassian.jira.rest.v2.issue.project.CreateUpdateRoleRequestBean#DOC_EXAMPLE}
     * @response.representation.200.doc Returned if the user is authenticated
     * @response.representation.200.example {@link com.atlassian.jira.rest.api.project.ProjectRoleBean#ROLE_SIMPLE_EXAMPLE}
     * @response.representation.400.doc Returned if the request json does not have a name field or the name field is invalid (empty or starts or ends with whitespace)
     * @response.representation.401.doc Returned if you are not logged in.
     * @response.representation.403.doc Returned if you do not have permissions to create a role.
     * @response.representation.409.doc Returned if a role with given name already exists.
     */
    @POST
    @ResponseType(ProjectRoleBean.class)
    public Response createProjectRole(CreateUpdateRoleRequestBean inputBean) {
        final SimpleErrorCollection errorCollection = new SimpleErrorCollection();

        String description = inputBean.getDescription() == null ? "" : inputBean.getDescription();
        ProjectRole projectRole = projectRoleService.createProjectRole(new ProjectRoleImpl(inputBean.getName(), description), errorCollection);

        if (errorCollection.hasAnyErrors()) {
            return responses.errorResponse(errorCollection);
        }

        final DefaultRoleActors actors = new DefaultRoleActorsImpl(projectRole.getId(), Collections.<RoleActor>emptySet());

        return Response.ok(projectRoleBeanFactory.projectRole(projectRole, actors)).build();
    }

    /**
     * Partially updates a roles name or description.
     *
     * @return Returns updated role.
     * @request.representation.mediaType application/json
     * @request.representation.example {@link com.atlassian.jira.rest.v2.issue.project.CreateUpdateRoleRequestBean#DOC_EXAMPLE}
     * @response.representation.200.doc Returned if the update was successful
     * @response.representation.200.example {@link com.atlassian.jira.rest.api.project.ProjectRoleBean#ROLE_SIMPLE_EXAMPLE}
     * @response.representation.400.doc Returned when both name and description are not given or name field is invalid (empty or starts or ends with whitespace).
     * @response.representation.401.doc Returned if the requesting user is not logged in.
     * @response.representation.403.doc Returned if the requesting user is not an admin or a sysadmin.
     * @response.representation.404.doc Returned if the role with the given id does not exist.
     */
    @POST
    @Path("{id}")
    @ResponseType(ProjectRoleBean.class)
    public Response partialUpdateProjectRole(@PathParam("id") final long roleId, final CreateUpdateRoleRequestBean updateProjectRoleBean) {
        final SimpleErrorCollection errorCollection = new SimpleErrorCollection();

        return getProjectRole(roleId).left().on(new com.google.common.base.Function<ProjectRole, Response>() {
            @Nullable
            @Override
            public Response apply(@Nullable final ProjectRole projectRole) {
                validatePartialUpdate(updateProjectRoleBean, errorCollection);
                if (errorCollection.hasAnyErrors()) {
                    return responses.errorResponse(errorCollection);
                }

                final ProjectRole updatedProjectRole = getPartiallyUpdatedProjectRole(projectRole, updateProjectRoleBean);

                projectRoleService.updateProjectRole(updatedProjectRole, errorCollection);

                if (errorCollection.hasAnyErrors()) {
                    return responses.errorResponse(errorCollection);
                }

                return withDefaultRoleActors(updatedProjectRole).left().on(new Function<DefaultRoleActors, Response>() {
                    @Override
                    public Response apply(final DefaultRoleActors defaultRoleActors) {
                        return Response.ok(projectRoleBeanFactory.projectRole(updatedProjectRole, defaultRoleActors)).build();
                    }
                });
            }
        });
    }

    /**
     * Fully updates a roles. Both name and description must be given.
     *
     * @return Returns updated role.
     * @request.representation.mediaType application/json
     * @request.representation.example {@link com.atlassian.jira.rest.v2.issue.project.CreateUpdateRoleRequestBean#DOC_EXAMPLE}
     * @response.representation.200.doc Returned if the update was successful
     * @response.representation.200.example {@link com.atlassian.jira.rest.api.project.ProjectRoleBean#ROLE_SIMPLE_EXAMPLE}
     * @response.representation.400.doc Returned when name or description is not given or the name field is invalid (empty or starts or ends with whitespace)
     * @response.representation.401.doc Returned if the requesting user is not logged in.
     * @response.representation.403.doc Returned if the requesting user is not an admin or a sysadmin.
     * @response.representation.404.doc Returned if the role with the given id does not exist.
     */
    @PUT
    @Path("{id}")
    @ResponseType(ProjectRoleBean.class)
    public Response fullyUpdateProjectRole(@PathParam("id") final long roleId, final CreateUpdateRoleRequestBean updateProjectRoleBean) {
        final SimpleErrorCollection errorCollection = new SimpleErrorCollection();

        return getProjectRole(roleId).left().on(new com.google.common.base.Function<ProjectRole, Response>() {
            @Nullable
            @Override
            public Response apply(@Nullable final ProjectRole projectRole) {
                validateFullUpdate(updateProjectRoleBean, errorCollection);
                if (errorCollection.hasAnyErrors()) {
                    return responses.errorResponse(errorCollection);
                }
                final ProjectRole updatedProjectRole =
                        ProjectRoleImpl.Builder
                                .from(projectRole)
                                .id(roleId)
                                .name(updateProjectRoleBean.getName())
                                .description(updateProjectRoleBean.getDescription())
                                .build();

                projectRoleService.updateProjectRole(updatedProjectRole, errorCollection);

                if (errorCollection.hasAnyErrors()) {
                    return responses.errorResponse(errorCollection);
                }

                return withDefaultRoleActors(updatedProjectRole).left().on(new Function<DefaultRoleActors, Response>() {
                    @Nullable
                    @Override
                    public Response apply(@Nullable final DefaultRoleActors defaultRoleActors) {
                        return Response.ok(projectRoleBeanFactory.projectRole(updatedProjectRole, defaultRoleActors)).build();
                    }
                });
            }
        });
    }

    /**
     * Deletes a role. May return 403 in the future
     *
     * @param roleIdToSwap if given, removes a role even if it is used in scheme by replacing the role with the given one
     * @response.representation.204.doc Returned if the delete was successful.
     * @response.representation.400.doc Returned if given role with given swap id does not exist.
     * @response.representation.401.doc Returned if the requesting user is not logged in.
     * @response.representation.403.doc Returned if the requesting user is not an admin or a sysadmin.
     * @response.representation.404.doc Returned if the role with the given id does not exist.
     * @response.representation.409.doc Returned if the project role is used in schemes and roleToSwap query parameter is not given.
     */
    @DELETE
    @Path("{id}")
    public Response deleteProjectRole(@PathParam("id") final long roleId, @QueryParam("swap") final Long roleIdToSwap) {
        final SimpleErrorCollection errorCollection = new SimpleErrorCollection();

        return getProjectRole(roleId).left().on(new com.google.common.base.Function<ProjectRole, Response>() {
            @Override
            public Response apply(final ProjectRole projectRole) {
                if (roleIdToSwap == null) {
                    ServiceResult serviceResult = projectRoleService.validateNoRoleUsage(projectRole);

                    if (!serviceResult.isValid()) {
                        return responses.errorResponse(serviceResult.getErrorCollection());
                    }
                } else {
                    final ProjectRole roleToSwap = projectRoleService.getProjectRole(roleIdToSwap, errorCollection);

                    if (roleToSwap == null) {
                        errorCollection.addErrorMessage("rest.swap.role.not.found", ErrorCollection.Reason.VALIDATION_FAILED);
                    }

                    if (errorCollection.hasAnyErrors()) {
                        return responses.errorResponse(errorCollection);
                    }

                    projectRoleService.swapRole(projectRole, roleToSwap);
                }

                projectRoleService.deleteProjectRole(projectRole, errorCollection);

                if (errorCollection.hasAnyErrors()) {
                    return responses.errorResponse(errorCollection);
                }

                return Response.noContent().build();
            }
        });
    }

    /**
     * Gets default actors for the given role.
     *
     * @return Returns actor list.
     * @response.representation.200.doc Returned if the user is authenticated
     * @response.representation.200.example {@link com.atlassian.jira.rest.v2.issue.project.ProjectRoleActorsBean#DOC_EXAMPLE}
     * @response.representation.401.doc Returned if the requesting user is not logged in.
     * @response.representation.403.doc Returned if the requesting user is not an admin or a sysadmin.
     * @response.representation.404.doc Returned if the role with the given id does not exist.
     */
    @GET
    @ResponseType(ProjectRoleBean.class)
    @Path("{id}/actors")
    public Response getProjectRoleActorsForRole(@PathParam("id") final long roleId) {
        return getProjectRole(roleId).left().on(new com.google.common.base.Function<ProjectRole, Response>() {
            @Override
            public Response apply(final ProjectRole projectRole) {
                return withDefaultRoleActors(projectRole).left().on(new Function<DefaultRoleActors, Response>() {
                    @Override
                    public Response apply(final DefaultRoleActors defaultRoleActors) {
                        return Response.ok(ProjectRoleActorsBean.from(defaultRoleActors.getRoleActors())).build();
                    }
                });
            }
        });
    }

    /**
     * Adds default actors to the given role. The request data should contain a list of usernames or a list of groups to add.
     *
     * @return Returns actor list.
     * @request.representation.mediaType application/json
     * @request.representation.example {@link ActorInputBean#DOC_EXAMPLE}
     * @response.representation.200.doc Returned if the update was successful
     * @response.representation.200.example {@link com.atlassian.jira.rest.v2.issue.project.ProjectRoleActorsBean#DOC_EXAMPLE}
     * @response.representation.400.doc Returned if user and group are both given or none are given or group or user in their respective lists does not exist.
     * @response.representation.401.doc Returned if the requesting user is not logged in.
     * @response.representation.403.doc Returned if the requesting user is not an admin or a sysadmin.
     * @response.representation.404.doc Returned if the role with the given id does not exist.
     */
    @POST
    @Path("{id}/actors")
    @ResponseType(ProjectRoleBean.class)
    public Response addProjectRoleActorsToRole(@PathParam("id") final long roleId, final ActorInputBean actorsInput) {
        final SimpleErrorCollection errorCollection = new SimpleErrorCollection();

        return getProjectRole(roleId).left().on(new com.google.common.base.Function<ProjectRole, Response>() {
            @Override
            public Response apply(@Nullable final ProjectRole projectRole) {
                return withValidationOfActorInputBean(actorsInput).left().on(new Function<ValidationActorsResult<Collection<String>>, Response>() {
                    @Override
                    public Response apply(final ValidationActorsResult<Collection<String>> validationResult) {
                        projectRoleService.addDefaultActorsToProjectRole(validationResult.roleEntity, projectRole, validationResult.roleType, errorCollection);
                        if (errorCollection.hasAnyErrors()) {
                            return responses.errorResponse(errorCollection);
                        }
                        return withDefaultRoleActors(projectRole).left().on(new Function<DefaultRoleActors, Response>() {
                            @Override
                            public Response apply(final DefaultRoleActors defaultRoleActors) {
                                return Response.ok(ProjectRoleActorsBean.from(defaultRoleActors.getRoleActors())).build();
                            }
                        });
                    }
                });
            }
        });
    }

    /**
     * Removes default actor from the given role.
     *
     * @param roleId the role id to remove the actors from
     * @param user   if given, removes an actor from given role
     * @param group  if given, removes an actor from given role
     * @return Returns updated actors list.
     * @response.representation.200.doc Returned if the update was successful.
     * @response.representation.200.example {@link com.atlassian.jira.rest.v2.issue.project.ProjectRoleActorsBean#DOC_EXAMPLE}
     * @response.representation.400.doc Returned if user and group are not given, both user and group are given or provided group or user does not exist.
     * @response.representation.401.doc Returned if the requesting user is not logged in.
     * @response.representation.403.doc Returned if the requesting user is not an admin or a sysadmin.
     * @response.representation.404.doc Returned if the role with the given id does not exist.
     */
    @DELETE
    @Path("{id}/actors")
    public Response deleteProjectRoleActorsFromRole(
            @PathParam("id") final long roleId,
            @QueryParam("user") final String user,
            @QueryParam("group") final String group) {
        return getProjectRole(roleId).left().on(new Function<ProjectRole, Response>() {
            @Override
            public Response apply(@Nullable final ProjectRole projectRole) {
                final SimpleErrorCollection errorCollection = new SimpleErrorCollection();

                return withValidationOfInputUserNamesAndGroupNames(user, group).left().on(new Function<ValidationActorsResult<String>, Response>() {
                    @Override
                    public Response apply(final ValidationActorsResult<String> validationResult) {
                        projectRoleService.removeDefaultActorsFromProjectRole(Collections.singleton(validationResult.roleEntity), projectRole, validationResult.roleType, errorCollection);
                        if (errorCollection.hasAnyErrors()) {
                            return responses.errorResponse(errorCollection);
                        }

                        return withDefaultRoleActors(projectRole).left().on(new Function<DefaultRoleActors, Response>() {
                            @Override
                            public Response apply(final DefaultRoleActors defaultRoleActors) {
                                return Response.ok(ProjectRoleActorsBean.from(defaultRoleActors.getRoleActors())).build();
                            }
                        });
                    }
                });
            }
        });
    }

    private void validateFullUpdate(final CreateUpdateRoleRequestBean updateProjectRoleBean, final SimpleErrorCollection errorCollection) {
        if (updateProjectRoleBean.getName() == null || updateProjectRoleBean.getDescription() == null) {
            errorCollection.addErrorMessage(
                    i18n.getText("rest.role.name.and.description.required"),
                    ErrorCollection.Reason.VALIDATION_FAILED);
        }
    }

    private ProjectRole getPartiallyUpdatedProjectRole(final ProjectRole projectRole, final CreateUpdateRoleRequestBean updateProjectRoleBean) {
        ProjectRoleImpl.Builder builder = ProjectRoleImpl.Builder.from(projectRole).id(projectRole.getId());

        if (updateProjectRoleBean.getName() != null) {
            builder.name(updateProjectRoleBean.getName());
        } else {
            builder.description(updateProjectRoleBean.getDescription());
        }
        return builder.build();
    }

    private void validatePartialUpdate(final CreateUpdateRoleRequestBean updateProjectRoleBean, final SimpleErrorCollection errorCollection) {
        if (updateProjectRoleBean.getName() == null && updateProjectRoleBean.getDescription() == null) {
            errorCollection.addErrorMessage(
                    i18n.getText("rest.role.name.or.description.required"),
                    ErrorCollection.Reason.VALIDATION_FAILED);
        }
    }

    @Nonnull
    private Either<Response, ProjectRole> getProjectRole(long roleId) {
        final SimpleErrorCollection errorCollection = new SimpleErrorCollection();

        if (!hasAdminPermission(authContext.getLoggedInUser())) {
            return Either.left(responses.forbidden("rest.authorization.admin.required"));
        }

        final ProjectRole projectRole = projectRoleService.getProjectRole(roleId, errorCollection);

        if (errorCollection.hasAnyErrors()) {
            return Either.left(responses.errorResponse(errorCollection));
        }

        if (projectRole == null) {
            return Either.left(responses.notFound("rest.role.not.found"));
        }

        return Either.right(projectRole);
    }

    @Nonnull
    private Either<Response, DefaultRoleActors> withDefaultRoleActors(ProjectRole projectRole) {
        final SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        final DefaultRoleActors actors = projectRoleService.getDefaultRoleActors(projectRole, errorCollection);

        if (errorCollection.hasAnyErrors()) {
            return Either.left(responses.errorResponse(errorCollection));
        }

        return Either.right(actors);
    }

    private boolean hasAdminPermission(ApplicationUser currentUser) {
        return permissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, currentUser);
    }

    private Either<Response, ValidationActorsResult<Collection<String>>> withValidationOfActorInputBean(final ActorInputBean actorsInput) {
        return withValidationOfInputUserNamesAndGroupNames(actorsInput.getUsernames(), actorsInput.getGroupnames());
    }

    private <T> Either<Response, ValidationActorsResult<T>> withValidationOfInputUserNamesAndGroupNames(final T usernames, final T groupnames) {
        if (usernames != null && groupnames != null) {
            return Either.left(responses.badRequest("rest.role.actors.add.username.or.groupname.both.cannot.be.provided"));
        }
        if (usernames != null) {
            return Either.right(new ValidationActorsResult<>(ProjectRoleActor.USER_ROLE_ACTOR_TYPE, usernames));
        } else if (groupnames != null) {
            return Either.right(new ValidationActorsResult<>(ProjectRoleActor.GROUP_ROLE_ACTOR_TYPE, groupnames));
        } else {
            return Either.left(responses.badRequest("rest.role.actors.delete.username.or.groupname.required"));
        }
    }

    private static class ValidationActorsResult<T> {
        private final String roleType;
        private final T roleEntity;

        private ValidationActorsResult(final String roleType, final T roleEntity) {
            this.roleType = roleType;
            this.roleEntity = roleEntity;
        }
    }

}
