package com.atlassian.jira.rest.v2.admin.applicationrole;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.fugue.Either;
import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.application.ApplicationRoleAdminService;
import com.atlassian.jira.application.ApplicationRoleComparator;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.rest.annotation.ResponseType;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import static com.atlassian.jira.rest.api.http.CacheControl.never;
import static com.atlassian.jira.rest.api.util.ErrorCollection.of;
import static com.atlassian.jira.util.ErrorCollection.Reason;
import static com.atlassian.jira.util.ErrorCollection.Reason.VALIDATION_FAILED;
import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Provides REST access to JIRA's Application Roles.
 *
 * @since v6.3
 */
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@WebSudoRequired
@Path("applicationrole")
public class ApplicationRoleResource {
    private static final String ETAG = "ETag";

    private final ApplicationRoleAdminService service;
    private final I18nHelper helper;
    private final GroupManager groupManager;
    private final Function<ApplicationRole, ApplicationRoleBean> toBean;
    private final ApplicationRoleHasher appRoleHasher;

    @VisibleForTesting
    ApplicationRoleResource(ApplicationRoleAdminService service, final I18nHelper helper,
                            final GroupManager groupManager, final Function<ApplicationRole, ApplicationRoleBean> toBean,
                            final ApplicationRoleHasher appRoleHasher) {
        this.helper = notNull("helper", helper);
        this.groupManager = notNull("groupManager", groupManager);
        this.service = notNull("service", service);
        this.toBean = notNull("toBean", toBean);
        this.appRoleHasher = notNull("appRoleHasher", appRoleHasher);
    }

    @Autowired
    public ApplicationRoleResource(final ApplicationRoleAdminService service, final I18nHelper helper,
                                   GroupManager groupManager, final ApplicationRoleBeanConverter roleBeanConverter,
                                   @Nonnull FeatureManager featureManager, final ApplicationRoleHasher appRoleHasher) {
        this(service, helper, groupManager, (Function<ApplicationRole, ApplicationRoleBean>) roleBeanConverter,
                appRoleHasher);
    }

    /**
     * Returns all ApplicationRoles in the system. Will also return an ETag header containing a version hash of the
     * collection of ApplicationRoles.
     *
     * @return all ApplicationRoles in the system.
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc Returns all ApplicationRoles in the system
     * @response.representation.200.example {@link com.atlassian.jira.rest.v2.admin.applicationrole.ApplicationRoleBeanExamples#DOC_LIST_EXAMPLE}
     * @response.representation.401.doc Returned if the current user is not authenticated.
     * @response.representation.403.doc Returned if the current user is not an administrator.
     */
    @GET
    @ResponseType(value = List.class, genericTypes = ApplicationRoleBean.class)
    public Response getAll() {
        ServiceOutcome<Set<ApplicationRole>> roles = service.getRoles();
        if (!roles.isValid()) {
            return build(responseForIterableOutcome(roles, toBean, applicationRoleComparator()));
        }

        return build(responseForIterableOutcome(roles, toBean, applicationRoleComparator()).header(ETAG,
                appRoleHasher.getVersionHash(roles.get())));
    }

    /**
     * Returns the ApplicationRole with passed key if it exists.
     *
     * @param key the key of the role to use.
     * @return the ApplicationRole if it exists.
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc Returns the ApplicationRole if it exists.
     * @response.representation.200.example {@link com.atlassian.jira.rest.v2.admin.applicationrole.ApplicationRoleBeanExamples#DOC_EXAMPLE}
     * @response.representation.401.doc Returned if the current user is not authenticated.
     * @response.representation.403.doc Returned if the current user is not an administrator.
     * @response.representation.404.doc Returned if the role does not exist.
     */
    @Path("{key}")
    @GET
    @ResponseType(ApplicationRoleBean.class)
    public Response get(@PathParam("key") final String key) {
        return build(withApplicationRole(key, input -> ok(toBean.apply(input))));
    }

    /**
     * Updates the ApplicationRole with the passed data. Only the groups and default groups setting of the
     * role may be updated. Requests to change the key or the name of the role will be silently ignored.
     * <p>
     * Optional: If versionHash is passed through the If-Match header the request will be rejected if not the
     * same as server
     *
     * @param key         the key of the role to update.
     * @param versionHash the hash of the version to update. Optional Param
     * @param bean        the data to update the role with.
     * @return the updated ApplicationRole if the update was successful and a version hash if one was sent in the
     * request.
     * @request.representation.example {@link com.atlassian.jira.rest.v2.admin.applicationrole.ApplicationRoleBeanExamples#UPDATE_EXAMPLE}
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc Returns the updated ApplicationRole if the update was successful.
     * @response.representation.200.example {@link ApplicationRoleBeanExamples#DOC_EXAMPLE}
     * @response.representation.401.doc Returned if the current user does not have permission to edit roles.
     * @response.representation.403.doc Returned if the current user is not an administrator.
     * @response.representation.404.doc Returned if the role does not exist.
     * @response.representation.412.doc Returned if the If-Match header is not null and contains a different version to the server.
     */
    @Path("{key}")
    @PUT
    @ResponseType(ApplicationRoleBean.class)
    public Response put(@PathParam("key") final String key, @HeaderParam("If-Match") final String versionHash,
                        final ApplicationRoleBean bean) {
        return build(putRequestParser(key, bean, versionHash));
    }

    /**
     * Updates the ApplicationRoles with the passed data if the version hash is the same as the server.
     * Only the groups and default groups setting of the role may be updated. Requests to change the key
     * or the name of the role will be silently ignored. It is acceptable to pass only the roles that are updated
     * as roles that are present in the server but not in data to update with, will not be deleted.
     *
     * @param beans the data to update the role with
     * @return the updated ApplicationRoles if the update was successful and the new version hash in an ETag header
     * @request.representation.example {@link com.atlassian.jira.rest.v2.admin.applicationrole.ApplicationRoleBeanExamples#UPDATE_EXAMPLE}
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc Returns the updated ApplicationRole if the update was successful.
     * @response.representation.200.example {@link ApplicationRoleBeanExamples#DOC_LIST_EXAMPLE}
     * @response.representation.401.doc Returned if the current user does not have permission to edit roles.
     * @response.representation.403.doc Returned if the current user is not an administrator.
     * @response.representation.404.doc Returned if the role does not exist.
     * @response.representation.412.doc Returned if the If-Match header is not null and contains a different version to the server.
     */
    @PUT
    @ResponseType(value = List.class, genericTypes = ApplicationRoleBean.class)
    public Response putBulk(final List<ApplicationRoleBean> beans, @HeaderParam("If-Match") final String versionHash) {
        if (versionHash != null) {
            if (!isVersionHashUpToDate(versionHash)) {
                return build(createVersionError(helper.getText("application.role.rest.version.wrong")));
            }
        }

        final List<ApplicationRole> appRoles = new ArrayList<>();
        for (ApplicationRoleBean appRoleBean : beans) {
            final Either<Response.ResponseBuilder, ApplicationRole> eitherErrorOrAppRole = getApplicationRole(appRoleBean.getKey());
            if (eitherErrorOrAppRole.isLeft()) {
                return build(eitherErrorOrAppRole.left().get());
            }

            final Either<Response.ResponseBuilder, ApplicationRole> verifiedAppRoleEither = verifyApplicationRole(
                    eitherErrorOrAppRole.right().get(), appRoleBean);
            if (verifiedAppRoleEither.isLeft()) {
                return build(verifiedAppRoleEither.left().get());
            }

            appRoles.add(verifiedAppRoleEither.right().get());
        }

        ServiceOutcome<Set<ApplicationRole>> outcome = service.setRoles(appRoles);
        if (!outcome.isValid()) {
            return build(responseForIterableOutcome(outcome, toBean, applicationRoleComparator()));
        }

        return build(responseForIterableOutcome(outcome, toBean,
                applicationRoleComparator()).header(ETAG, appRoleHasher.getVersionHash(outcome.get())));
    }

    private Response.ResponseBuilder putRequestParser(final String key, final ApplicationRoleBean bean,
                                                      @Nullable final String versionHash) {
        if (versionHash != null) {
            if (!isVersionHashUpToDate(versionHash)) {
                return createVersionError(helper.getText("application.role.rest.version.wrong"));
            }
        }

        return withApplicationRole(key, role ->
        {
            final Either<Response.ResponseBuilder, ApplicationRole> verifiedAppRole = verifyApplicationRole(role, bean);
            if (verifiedAppRole.isLeft()) {
                return verifiedAppRole.left().get();
            }
            final ApplicationRole newRole = verifiedAppRole.right().get();

            return responseFromOutcome(service.setRole(newRole), toBean);
        });
    }

    private Either<Response.ResponseBuilder, ApplicationRole> verifyApplicationRole(ApplicationRole role, ApplicationRoleBean bean) {
        Set<String> defaultGroups = bean.getDefaultGroups();
        if (defaultGroups == null) {
            defaultGroups = Collections.emptySet();
        }

        Set<String> groups = bean.getGroups();
        if (groups == null) {
            groups = Collections.emptySet();
        }

        Boolean isSelectedByDefault = bean.isSelectedByDefault();
        if (isSelectedByDefault == null) {
            isSelectedByDefault = false;
        }

        if (groups.contains(null)) {
            return Either.left(createGroupError(helper.getText("application.role.rest.groups.null")));
        }
        if (defaultGroups.contains(null)) {
            return Either.left(createDefaultGroupError(helper.getText("application.role.rest.groups.null")));
        }
        final Either<String, Set<Group>> errorMessageOrGroup = toGroups(groups);
        if (errorMessageOrGroup.isLeft()) {
            return Either.left(createGroupError(helper.getText("application.role.rest.error.bad.group",
                    errorMessageOrGroup.left().get())));
        }
        final Either<String, Set<Group>> errorMessageOrDefaultGroup = toGroups(defaultGroups);
        if (errorMessageOrDefaultGroup.isLeft()) {
            return Either.left(createDefaultGroupError(helper.getText("application.role.rest.error.bad.group",
                    errorMessageOrDefaultGroup.left().get())));
        }
        final Set<Group> groupObjects = errorMessageOrGroup.right().get();
        final Set<Group> defaultObjects = errorMessageOrDefaultGroup.right().get();
        if (!groupObjects.containsAll(defaultObjects)) {
            final Set<String> diff = Sets.difference(defaultGroups, groups);
            final String msg = helper.getText("application.role.rest.default.groups.invalid",
                    new Object[]{diff.size(), StringUtils.join(diff, ", ")});

            return Either.left(createDefaultGroupError(msg));
        }

        final ApplicationRole newRole = role
                .withGroups(groupObjects, defaultObjects)
                .withSelectedByDefault(isSelectedByDefault);

        return Either.right(newRole);
    }

    private static Response.ResponseBuilder createGroupError(final String msg) {
        final ErrorCollection collection = new SimpleErrorCollection();
        collection.addError(ApplicationRoleAdminService.ERROR_GROUPS, msg, VALIDATION_FAILED);
        return forCollection(collection);
    }

    private static Response.ResponseBuilder createDefaultGroupError(final String msg) {
        final ErrorCollection collection = new SimpleErrorCollection();
        collection.addError(ApplicationRoleAdminService.ERROR_DEFAULT_GROUPS, msg, VALIDATION_FAILED);
        return forCollection(collection);
    }

    private static Response.ResponseBuilder createVersionError(final String msg) {
        final ErrorCollection collection = new SimpleErrorCollection();
        collection.addError("versionMismatch", msg, Reason.PRECONDITION_FAILED);
        return forCollection(collection);
    }

    private Response.ResponseBuilder withApplicationRole(String key, Function<ApplicationRole, Response.ResponseBuilder> valid) {
        final Either<Response.ResponseBuilder, ApplicationRole> appRoleEither = getApplicationRole(key);
        if (appRoleEither.isLeft()) {
            return appRoleEither.left().get();
        }

        return valid.apply(appRoleEither.right().get());
    }

    /**
     * Gets an ApplicationRole from the provided key
     *
     * @param key the key of the ApplicationRole to retrieve
     * @return an ApplicationRole or a ResponseBuilder containing an error response
     */
    private Either<Response.ResponseBuilder, ApplicationRole> getApplicationRole(String key) {
        if (!ApplicationKey.isValid(key)) {
            return Either.left(forError(helper.getText("application.role.rest.bad.key", key)));
        }

        final ServiceOutcome<ApplicationRole> outcome = service.getRole(ApplicationKey.valueOf(key));
        if (!outcome.isValid()) {
            return Either.left(forCollection(outcome.getErrorCollection()));
        }
        return Either.right(outcome.get());
    }

    private static Response.ResponseBuilder ok(final Object bean) {
        return Response.ok(bean);
    }

    private static <I, O> Response.ResponseBuilder responseFromOutcome(ServiceOutcome<I> outcome,
                                                                       final Function<? super I, ? extends O> transform) {
        if (outcome.isValid()) {
            return ok(transform.apply(outcome.get()));
        } else {
            return forCollection(outcome.getErrorCollection());
        }
    }

    private Comparator<ApplicationRole> applicationRoleComparator() {
        final Locale userLocale = helper.getLocale();
        return new ApplicationRoleComparator(userLocale);
    }

    private static <I, O> Response.ResponseBuilder responseForIterableOutcome(final ServiceOutcome<? extends Iterable<I>> outcome,
                                                                              final Function<? super I, ? extends O> transform,
                                                                              final Comparator<I> sorter) {
        return responseFromOutcome(outcome, input ->
        {
            List<I> inputList = Lists.newArrayList(input);
            inputList.sort(sorter);
            return Lists.newArrayList(Iterables.transform(inputList, transform));
        });
    }

    private static Response.ResponseBuilder forError(String message) {
        return Response.status(Response.Status.BAD_REQUEST).entity(of(message));
    }

    private static Response.ResponseBuilder forCollection(ErrorCollection collection) {
        return forCollection(collection, Reason.SERVER_ERROR);
    }

    private static Response.ResponseBuilder forCollection(ErrorCollection collection, Reason defaultReason) {
        if (!collection.hasAnyErrors()) {
            throw new IllegalArgumentException("collection has no errors.");
        }
        if (defaultReason == null) {
            throw new IllegalArgumentException("defaultReason is null");
        }

        Reason worstReason = Reason.getWorstReason(collection.getReasons());
        if (worstReason == null) {
            worstReason = defaultReason;
        }
        return Response.status(worstReason.getHttpStatusCode()).entity(of(collection));
    }

    /**
     * Gets groups via the group manager
     *
     * @param groupNames the list of groups to get
     * @return the set of groups or an error message
     */
    private Either<String, Set<Group>> toGroups(Iterable<String> groupNames) {
        Set<Group> groups = Sets.newHashSet();
        for (String groupName : groupNames) {
            Group group = groupManager.getGroup(groupName);
            if (group == null) {
                return Either.left(groupName);
            }
            groups.add(group);
        }
        return Either.right(Collections.unmodifiableSet(groups));
    }

    private boolean isVersionHashUpToDate(final String requestVersionHash) {
        ServiceOutcome<Set<ApplicationRole>> appRoles = service.getRoles();
        String serverVersionHash = appRoleHasher.getVersionHash(appRoles.get());

        return requestVersionHash.equals(serverVersionHash);
    }

    private static Response build(Response.ResponseBuilder builder) {
        return builder.cacheControl(never()).build();
    }

}
