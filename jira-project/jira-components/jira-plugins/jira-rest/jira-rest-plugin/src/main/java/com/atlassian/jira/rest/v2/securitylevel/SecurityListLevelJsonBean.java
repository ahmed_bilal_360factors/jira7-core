package com.atlassian.jira.rest.v2.securitylevel;

import com.atlassian.jira.issue.fields.rest.json.beans.SecurityLevelJsonBean;
import com.atlassian.jira.rest.v2.issue.Examples;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Arrays;
import java.util.Collection;

public class SecurityListLevelJsonBean {
    @JsonProperty
    private final Collection<SecurityLevelJsonBean> levels;

    public SecurityListLevelJsonBean(final Collection<SecurityLevelJsonBean> levels) {
        this.levels = levels;
    }

    public static SecurityListLevelJsonBean of(final Collection<SecurityLevelJsonBean> levelBeans) {
        return new SecurityListLevelJsonBean(levelBeans);
    }

    public static final SecurityListLevelJsonBean DOC_EXAMPLE;

    static {
        DOC_EXAMPLE = SecurityListLevelJsonBean.of(
                Arrays.asList(
                        new SecurityLevelJsonBean(SecurityLevelJsonBean.getSelf(Examples.REST_BASE_URL + "/", "100000"), "100000", "security description", "securityLevelName"),
                        new SecurityLevelJsonBean(SecurityLevelJsonBean.getSelf(Examples.REST_BASE_URL + "/", "100001"), "100001", "another security description", "secret")));
    }
}