package com.atlassian.jira.rest.v2.issue.users;

import com.atlassian.jira.rest.v2.issue.Examples;
import com.atlassian.jira.rest.v2.issue.UserBean;
import com.atlassian.jira.util.collect.MapBuilder;
import com.google.common.collect.ImmutableList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.TimeZone;

import static java.util.Objects.requireNonNull;

/**
 * A user who can be mentioned with respect to a particular issue, usually
 * because they have been involved in it in some way.
 *
 * Similar to {@link UserBean} except for that the entity has extra information
 * around about how this user has been involved, and how relevant this user is
 * to the issue (which is expected to be used by clients to inform the
 * presentation of results).
 *
 * @since v7.2
 */
@XmlRootElement(name = "user")
public class UserIssueRelevanceBean {
    @XmlElement
    private URI self;

    @XmlElement
    private String key;

    @XmlElement
    private String name;

    @XmlElement
    private String emailAddress;

    @XmlElement
    private Map<String, URI> avatarUrls;

    @XmlElement
    private String displayName;

    @XmlElement
    private boolean active;

    @XmlElement
    private String timeZone;

    @XmlElement
    private String locale;

    @XmlElement
    private List<IssueInvolvementBean> issueInvolvements;

    @XmlElement
    private Integer highestIssueInvolvementRank;

    @XmlElement
    private Long latestCommentCreationTime;

    UserIssueRelevanceBean() {
        // empty
    }

    public UserIssueRelevanceBean(URI self,
                                  String key,
                                  String name,
                                  String displayName,
                                  boolean active,
                                  String emailAddress,
                                  Map<String, URI> avatarUrls,
                                  TimeZone timeZone,
                                  Locale locale,
                                  List<IssueInvolvementBean> issueInvolvements,
                                  Optional<Integer> highestIssueInvolvementRank,
                                  Optional<Date> latestCommentCreationTime) {
        this.self = requireNonNull(self);
        this.key = requireNonNull(key);
        this.name = requireNonNull(name);
        this.displayName = requireNonNull(displayName);
        this.active = active;
        this.emailAddress = emailAddress;
        this.avatarUrls = requireNonNull(avatarUrls);
        this.timeZone = timeZone != null ? timeZone.getID() : null;
        this.locale = locale != null ? locale.toString() : null;
        this.issueInvolvements = ImmutableList.copyOf(issueInvolvements);
        this.highestIssueInvolvementRank = highestIssueInvolvementRank.orElse(null);
        this.latestCommentCreationTime = latestCommentCreationTime.map(date -> date.getTime()).orElse(null);
    }

    public static final UserIssueRelevanceBean DOC_EXAMPLE = new UserIssueRelevanceBean();

    static {
        DOC_EXAMPLE.self = Examples.restURI("user?username=fred");
        DOC_EXAMPLE.name = "fred";
        DOC_EXAMPLE.emailAddress = "fred@example.com";
        DOC_EXAMPLE.displayName = "Fred F. User";
        DOC_EXAMPLE.active = true;
        DOC_EXAMPLE.avatarUrls = MapBuilder.<String, URI>newBuilder()
                .add("16x16", Examples.jiraURI("secure/useravatar?size=xsmall&ownerId=fred"))
                .add("24x24", Examples.jiraURI("secure/useravatar?size=small&ownerId=fred"))
                .add("32x32", Examples.jiraURI("secure/useravatar?size=medium&ownerId=fred"))
                .add("48x48", Examples.jiraURI("secure/useravatar?size=large&ownerId=fred"))
                .toMap();
        DOC_EXAMPLE.timeZone = "Australia/Sydney";

        DOC_EXAMPLE.issueInvolvements = ImmutableList.of(IssueInvolvementBean.DOC_EXAMPLE);
        DOC_EXAMPLE.highestIssueInvolvementRank = 0;
        DOC_EXAMPLE.latestCommentCreationTime = 1458688056272L;
    }

    public URI getSelf() {
        return self;
    }

    public String getKey() {
        return key;
    }

    public String getName() {
        return name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Map<String, URI> getAvatarUrls() {
        return avatarUrls;
    }

    public List <IssueInvolvementBean> getIssueInvolvements() {
        return issueInvolvements;
    }

    public Integer getHighestIssueInvolvementRank() {
        return highestIssueInvolvementRank;
    }

    public Long getLatestCommentCreationTime() {
        return latestCommentCreationTime;
    }
}

