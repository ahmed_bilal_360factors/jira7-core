package com.atlassian.jira.rest.v2.admin.applicationrole;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Set;

/**
 * Representation of a {@link com.atlassian.jira.application.ApplicationRole} in REST.
 *
 * @since v6.3
 */
public class ApplicationRoleBean {
    @JsonProperty
    private String key;

    @JsonProperty
    private Set<String> groups;

    @JsonProperty
    private String name;

    @JsonProperty
    private Set<String> defaultGroups;

    @JsonProperty
    private Boolean selectedByDefault;

    //Things above here are required. Below are optional.
    @JsonProperty
    private Boolean defined;

    @JsonProperty
    private Integer numberOfSeats;

    @JsonProperty
    private Integer remainingSeats;

    @JsonProperty
    private Integer userCount;

    @JsonProperty
    private String userCountDescription;

    @JsonProperty
    private Boolean hasUnlimitedSeats;

    @JsonProperty
    private Boolean platform;

    //Needed for Jackson.
    public ApplicationRoleBean() {
    }

    ApplicationRoleBean(
            final String key,
            final String name,
            final Set<String> groups,
            final Set<String> defaultGroups,
            final Boolean selectedByDefault) {
        this.key = key;
        this.groups = groups;
        this.name = name;
        this.defaultGroups = defaultGroups;
        this.selectedByDefault = selectedByDefault;
    }

    ApplicationRoleBean(
            final String key,
            final String name,
            final Set<String> groups,
            final Set<String> defaultGroups,
            final Boolean selectedByDefault,
            final Boolean defined,
            final int numberOfSeats,
            final int remainingSeats,
            final int userCount,
            final String userCountDescription,
            final Boolean hasUnlimitedSeats,
            final Boolean platform) {
        this.key = key;
        this.groups = groups;
        this.name = name;
        this.defaultGroups = defaultGroups;
        this.selectedByDefault = selectedByDefault;
        this.defined = defined;
        this.numberOfSeats = numberOfSeats;
        this.remainingSeats = remainingSeats;
        this.userCount = userCount;
        this.userCountDescription = userCountDescription;
        this.hasUnlimitedSeats = hasUnlimitedSeats;
        this.platform = platform;
    }

    public void setDefaultGroups(final Set<String> defaultGroups) {
        this.defaultGroups = defaultGroups;
    }

    public Set<String> getDefaultGroups() {
        return defaultGroups;
    }

    public String getKey() {
        return key;
    }

    public Set<String> getGroups() {
        return groups;
    }

    public String getName() {
        return name;
    }

    public Boolean isSelectedByDefault() {
        return selectedByDefault;
    }

    public Boolean isDefined() {
        return defined;
    }

    public Integer getNumberOfSeats() {
        return numberOfSeats;
    }

    public Integer getRemainingSeats() {
        return remainingSeats;
    }

    public Integer getUserCount() {
        return userCount;
    }

    public String getUserCountDescription() {
        return userCountDescription;
    }

    public Boolean isHasUnlimitedSeats() {
        return hasUnlimitedSeats;
    }

    public Boolean isPlatform() {
        return platform;
    }

    public void setKey(final String key) {
        this.key = key;
    }

    public void setGroups(final Set<String> groups) {
        this.groups = groups;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setHasUnlimitedSeats(final boolean hasUnlimitedSeats) {
        this.hasUnlimitedSeats = hasUnlimitedSeats;
    }

    public void setSelectedByDefault(final boolean isSelectedByDefault) {
        this.selectedByDefault = isSelectedByDefault;
    }

    public void setDefined(final boolean defined) {
        this.defined = defined;
    }

    public void setNumberOfSeats(final Integer numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public void setRemainingSeats(final Integer remainingSeats) {
        this.remainingSeats = remainingSeats;
    }

    public void setUserCount(final Integer userCount) {
        this.userCount = userCount;
    }

    public void setUserCountDescription(final String userCountDescription) {
        this.userCountDescription = userCountDescription;
    }

    public void setPlatform(final boolean platform) {
        this.platform = platform;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE)
                .append("key", key)
                .append("groups", groups)
                .append("name", name)
                .append("defaultGroups", defaultGroups)
                .append("selectedByDefault", selectedByDefault)
                .append("defined", defined)
                .append("numberOfSeats", numberOfSeats)
                .append("remainingSeats", remainingSeats)
                .append("userCount", userCount)
                .append("userCountDescription", userCountDescription)
                .append("hasUnlimitedSeats", hasUnlimitedSeats)
                .append("platform", platform)
                .toString();
    }

    public static ApplicationRoleBean BuildDocExample(final String key, final String name) {
        return new ApplicationRoleBean(key, name, null, null, null);
    }
}
