package com.atlassian.jira.rest.v2.search;

import com.atlassian.jira.bc.filter.FilterSubscriptionService;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.jql.util.JqlStringSupport;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.rest.v2.issue.UserBean;
import com.atlassian.jira.rest.v2.issue.UserBeanBuilder;
import com.atlassian.jira.scheme.SchemeManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.user.util.UserUtil;

import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * A builder utility to create a {@link FilterBean}
 *
 * @since v5.2
 */
public class FilterBeanBuilder {
    private final JiraAuthenticationContext authContext;
    private final ProjectManager projectManager;
    private final PermissionManager permissionManager;
    private final ProjectRoleManager projectRoleManager;
    private final UserManager userManager;
    private final JqlStringSupport jqlStringSupport;
    private final GroupManager groupManager;
    private final SchemeManager schemeManager;
    private final FilterSubscriptionService filterSubscriptionService;
    private final JiraBaseUrls jiraBaseUrls;
    private final UserUtil userUtil;
    private final FeatureManager featureManager;
    private final FilterPermissionBeanFactory filterPermissionBeanFactory;

    private SearchRequest filter;
    private UriInfo context;
    private String canoncialBaseUrl;
    private ApplicationUser owner = null;
    private boolean favourite = false;

    public FilterBeanBuilder(JiraAuthenticationContext authContext, ProjectManager projectManager, PermissionManager permissionManager,
                             ProjectRoleManager projectRoleManager,
                             UserManager userManager, JqlStringSupport jqlStringSupport,
                             GroupManager groupManager, SchemeManager schemeManager, FilterSubscriptionService filterSubscriptionService,
                             JiraBaseUrls jiraBaseUrls,
                             UserUtil userUtil, final FeatureManager featureManager, FilterPermissionBeanFactory filterPermissionBeanFactory) {
        this.authContext = authContext;
        this.projectManager = projectManager;
        this.permissionManager = permissionManager;
        this.projectRoleManager = projectRoleManager;
        this.userManager = userManager;
        this.jqlStringSupport = jqlStringSupport;
        this.groupManager = groupManager;
        this.schemeManager = schemeManager;
        this.filterSubscriptionService = filterSubscriptionService;
        this.jiraBaseUrls = jiraBaseUrls;
        this.userUtil = userUtil;
        this.featureManager = featureManager;
        this.filterPermissionBeanFactory = filterPermissionBeanFactory;
    }

    /**
     * Sets the filter
     *
     * @param filter a filter
     * @return this
     */
    public FilterBeanBuilder filter(final SearchRequest filter) {
        this.filter = filter;
        return this;
    }

    /**
     * Sets the context.
     *
     * @param context          a UriInfo
     * @param canoncialBaseUrl the baseurl of this instance
     * @return this
     */
    public FilterBeanBuilder context(UriInfo context, final String canoncialBaseUrl) {
        this.context = context;
        this.canoncialBaseUrl = canoncialBaseUrl;
        return this;
    }


    public FilterBeanBuilder owner(ApplicationUser owner) {
        this.owner = owner;
        return this;
    }

    public FilterBeanBuilder favourite(boolean favourite) {
        this.favourite = favourite;
        return this;
    }


    public FilterBean build() {
        if (filter != null) {
            if (context == null || canoncialBaseUrl == null) {
                throw new IllegalStateException("No context set.");
            }

            final UserBean owner = new UserBeanBuilder(jiraBaseUrls).user(this.owner).buildShort();
            final URI issueNavUri = URI.create(canoncialBaseUrl +
                    "/issues/?filter=" + filter.getId());

            final URI self = context.getBaseUriBuilder().path(FilterResource.class).
                    path(Long.toString(filter.getId())).build();

            String JQL = jqlStringSupport.generateJqlString(filter.getQuery());
            URI searchUri = context.getBaseUriBuilder().path(SearchResource.class)
                    .queryParam("jql", "{0}").build(JQL);

            Collection<FilterPermissionBean> sharePermissions =
                    StreamSupport.stream(filter.getPermissions().spliterator(), false)
                            .map((sharePermission) -> filterPermissionBeanFactory.buildPermissionBean(authContext.getLoggedInUser(), sharePermission))
                            .collect(Collectors.toList());

            final FilterSubscriptionBeanListWrapper filterSubscribtionBeanListWrapper = new FilterSubscriptionBeanListWrapper(
                    filterSubscriptionService, userManager, authContext.getUser(), filter, jiraBaseUrls);

            final UserListResolver userListResolver = new UserListResolver(authContext, userManager,
                    groupManager, projectManager, permissionManager,
                    projectRoleManager, schemeManager, userUtil, featureManager, sharePermissions);

            final UserBeanListWrapper userBeanListWrapper = new UserBeanListWrapper(jiraBaseUrls, userListResolver, FilterBean.MAX_USER_LIMIT);

            return new FilterBean(self, Long.toString(this.filter.getId()),
                    this.filter.getName(), this.filter.getDescription(), owner, JQL,
                    issueNavUri, searchUri, favourite, sharePermissions,
                    filterSubscribtionBeanListWrapper, userBeanListWrapper);
        }
        return null;
    }
}
