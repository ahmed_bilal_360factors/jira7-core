package com.atlassian.jira.rest.v2.issue;

import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.config.SubTaskService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.rest.json.beans.IssueLinksBeanBuilder;
import com.atlassian.jira.issue.fields.rest.json.beans.IssueLinksBeanBuilderFactory;
import com.atlassian.jira.issue.fields.rest.json.beans.IssueRefJsonBean;
import com.atlassian.jira.rest.util.ResponseFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.rest.annotation.ResponseType;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.function.Function;

import static com.atlassian.jira.rest.api.http.CacheControl.never;

/**
 *
 */
@Path("issue/{issueIdOrKey}/subtask")
@Produces({MediaType.APPLICATION_JSON})
public class IssueSubTaskResource {

    private final SubTaskService subTaskService;
    private final IssueFinder issueFinder;
    private final JiraAuthenticationContext authenticationContext;
    private final IssueLinksBeanBuilderFactory issueLinksBeanBuilderFactory;
    private final ResponseFactory responseFactory;

    public IssueSubTaskResource(
            SubTaskService subTaskService,
            IssueFinder issueFinder,
            JiraAuthenticationContext authenticationContext,
            IssueLinksBeanBuilderFactory issueLinksBeanBuilderFactory,
            ResponseFactory responseFactory) {
        this.subTaskService = subTaskService;
        this.issueFinder = issueFinder;
        this.authenticationContext = authenticationContext;
        this.issueLinksBeanBuilderFactory = issueLinksBeanBuilderFactory;
        this.responseFactory = responseFactory;
    }

    /**
     * Returns an issue's subtask list
     *
     * @param issueIdOrKey The parent issue's key or id
     *
     * @request.representation.mediaType application/json
     * @response.representation.200.doc
     * @response.representation.403.doc Returned if the user is not allowed to edit the issue
     * @response.representation.404.doc Returned if the issue doesn't exist
     */
    @GET
    @ResponseType(Response.class)
    public Response getSubTasks(@PathParam("issueIdOrKey") String issueIdOrKey) {
        return withIssue(issueIdOrKey, (issue) -> {
            final IssueLinksBeanBuilder issueLinkBeanBuilder = issueLinksBeanBuilderFactory.newIssueLinksBeanBuilder(issue);
            final List<IssueRefJsonBean> subtaskLinks = issueLinkBeanBuilder.buildSubtaskLinks();
            return responseFactory.okNoCache(subtaskLinks);
        });
    }

    @GET
    @Path("move")
    @ResponseType(Response.class)
    public Response canMoveSubTask(@PathParam("issueIdOrKey") String issueIdOrKey) {
        return withIssue(issueIdOrKey, issue -> Response
                .ok(subTaskService.canMoveSubtask(authenticationContext.getLoggedInUser(), issue))
                .build());
    }

    /**
     * Reorders an issue's subtasks by moving the subtask at index "from"
     * to index "to".
     *
     * @param issueIdOrKey The parent issue's key or id
     * @param position the description of previous and current position of subtask in the sequence.
     *
     * @request.representation.mediaType application/json
     * @response.representation.204.doc Returned if the request was successful
     * @response.representation.400.doc Returned if the from or to parameters are out of bounds
     * @response.representation.403.doc Returned if the user is not allowed to edit the issue
     * @response.representation.404.doc Returned if the parent issue doesn't exist
     */
    @POST
    @Path("move")
    @Consumes("application/json")
    @ResponseType(Response.class)
    public Response moveSubTasks(@PathParam("issueIdOrKey") String issueIdOrKey, IssueSubTaskMovePositionBean position) {
        return withIssue(issueIdOrKey, (issue) -> {
            final ApplicationUser user = authenticationContext.getLoggedInUser();
            final ServiceResult result = subTaskService.moveSubTask(user, issue, position.getOriginal(), position.getCurrent());
            if (!result.isValid()) {
                return responseFactory.errorResponse(result.getErrorCollection());
            }
            return Response.noContent().cacheControl(never()).build();
        });
    }

    private Response withIssue(String issueIdOrKey, Function<Issue, Response> fn) {
        final Issue issue = issueFinder.getIssueObject(issueIdOrKey);
        if (issue != null) {
            return fn.apply(issue);
        }
        return responseFactory.notFound("");
    }
}
