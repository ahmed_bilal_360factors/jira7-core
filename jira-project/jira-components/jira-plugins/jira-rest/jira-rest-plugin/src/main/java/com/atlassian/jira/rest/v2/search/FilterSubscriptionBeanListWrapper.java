package com.atlassian.jira.rest.v2.search;

import com.atlassian.jira.bc.filter.FilterSubscriptionService;
import com.atlassian.jira.entity.WithId;
import com.atlassian.jira.issue.fields.rest.json.beans.GroupJsonBean;
import com.atlassian.jira.issue.fields.rest.json.beans.GroupJsonBeanBuilder;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.subscription.FilterSubscription;
import com.atlassian.jira.rest.api.expand.PagedListWrapper;
import com.atlassian.jira.rest.v2.issue.UserBean;
import com.atlassian.jira.rest.v2.issue.UserBeanBuilder;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.util.concurrent.LazyReference;
import com.google.common.collect.Ordering;
import com.opensymphony.util.TextUtils;

import java.util.Collection;
import java.util.List;

/**
 * Wraps a list of FilterSubscriptions and pages over them
 *
 * @since v6.0
 */
public class FilterSubscriptionBeanListWrapper extends PagedListWrapper<FilterSubscriptionBean, FilterSubscription> {

    private FilterSubscriptionService filterSubscriptionService;
    private SearchRequest filter;
    private ApplicationUser user;
    private UserManager userManager;
    private JiraBaseUrls jiraBaseUrls;

    private LazyReference<Collection<FilterSubscription>> visibleSubscriptionsReference = new LazyReference<Collection<FilterSubscription>>() {
        @Override
        protected Collection<FilterSubscription> create() throws Exception {
            return filterSubscriptionService.getVisibleFilterSubscriptions(user, filter);
        }
    };

    public FilterSubscriptionBeanListWrapper(FilterSubscriptionService filterSubscriptionService,
                                             UserManager userManager, ApplicationUser user, SearchRequest filter, JiraBaseUrls jiraBaseUrls) {
        super(0, FilterBean.MAX_USER_LIMIT);
        this.filter = filter;
        this.filterSubscriptionService = filterSubscriptionService;
        this.user = user;
        this.userManager = userManager;
        this.jiraBaseUrls = jiraBaseUrls;
    }

    private FilterSubscriptionBeanListWrapper() {
        super(0, 0);
    }

    /**
     * Returns an empty FilterSubscriptionBeanListWrapper.
     *
     * @return an empty FilterSubscriptionBeanListWrapper
     */
    public static FilterSubscriptionBeanListWrapper empty() {
        return new FilterSubscriptionBeanListWrapper(null, null, null, null, null);
    }

    @Override
    public FilterSubscriptionBean fromBackedObject(FilterSubscription filterSubscription) {
        return new FilterSubscriptionBeanBuilder().subscription(filterSubscription).build();
    }

    @Override
    public int getBackingListSize() {
        return visibleSubscriptionsReference.get().size();
    }

    @Override
    public List<FilterSubscription> getOrderedList(int startIndex, int endIndex) {
        List<FilterSubscription> subscriptions = Ordering.from(WithId.ID_COMPARATOR).immutableSortedCopy(visibleSubscriptionsReference.get());
        return subscriptions.subList(startIndex, endIndex + 1);
    }


    class FilterSubscriptionBeanBuilder {
        private FilterSubscription subscription;

        public FilterSubscriptionBeanBuilder subscription(FilterSubscription subscription) {
            this.subscription = subscription;
            return this;
        }

        private UserBean buildUserBean() {
            // Mismatch due to legacy column names from before users were renameable
            final String userKey = subscription.getUserKey();
            return new UserBeanBuilder(jiraBaseUrls).user(userManager.getUserByKey(userKey)).buildShort();
        }

        private GroupJsonBean buildGroupBean() {
            final String groupName = subscription.getGroupName();
            return validGroupName(groupName) ? new GroupJsonBeanBuilder(jiraBaseUrls).name(groupName).build() : null;
        }

        private boolean validGroupName(String groupName) {
            return TextUtils.stringSet(groupName) && userManager.getGroup(groupName) != null;
        }

        public FilterSubscriptionBean build() {
            return new FilterSubscriptionBean(subscription.getId(), buildUserBean(), buildGroupBean());
        }
    }

}
