package com.atlassian.jira.rest.v2.notification;

import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.issue.fields.rest.json.beans.GroupJsonBean;
import com.atlassian.jira.issue.fields.rest.json.beans.UserJsonBean;
import com.atlassian.jira.notification.type.NotificationType;
import com.atlassian.jira.rest.api.field.FieldBean;
import com.atlassian.jira.rest.api.notification.AbstractNotificationBean;
import com.atlassian.jira.rest.api.notification.CustomFieldValueNotificationBean;
import com.atlassian.jira.rest.api.notification.EmailNotificationBean;
import com.atlassian.jira.rest.api.notification.GroupNotificationBean;
import com.atlassian.jira.rest.api.notification.NotificationEventBean;
import com.atlassian.jira.rest.api.notification.NotificationSchemeBean;
import com.atlassian.jira.rest.api.notification.NotificationSchemeEventBean;
import com.atlassian.jira.rest.api.notification.ProjectRoleNotificationBean;
import com.atlassian.jira.rest.api.notification.RoleNotificationBean;
import com.atlassian.jira.rest.api.notification.UserNotificationBean;
import com.atlassian.jira.rest.api.pagination.PageBean;
import com.atlassian.jira.rest.api.project.ProjectRoleBean;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

public class NotificationSchemeBeanExample {
    public static final NotificationSchemeBean DOC_EXAMPLE;
    public static final NotificationEventBean EVENT_DOC_EXAMPLE;
    public static final NotificationEventBean CUSTOM_EVENT_DOC_EXAMPLE;
    public static final PageBean<NotificationSchemeBean> PAGE_DOC_EXAMPLE;

    static {
        EVENT_DOC_EXAMPLE = new NotificationEventBean(EventType.ISSUE_CREATED_ID, "Issue created", "Event published when issue is created", null);
        CUSTOM_EVENT_DOC_EXAMPLE = new NotificationEventBean(20l, "Custom event", "Custom event which is published together with issue created event", EVENT_DOC_EXAMPLE);
        final Iterable<AbstractNotificationBean> notifications = ImmutableList.<AbstractNotificationBean>builder()
                .add(new GroupNotificationBean(1l, NotificationType.GROUP, GroupJsonBean.DOC_EXAMPLE.getName(), GroupJsonBean.DOC_EXAMPLE))
                .add(new RoleNotificationBean(2l, NotificationType.CURRENT_ASSIGNEE))
                .add(new ProjectRoleNotificationBean(3l, NotificationType.PROJECT_ROLE, ProjectRoleBean.DOC_EXAMPLE.id.toString(), ProjectRoleBean.DOC_EXAMPLE))
                .add(new EmailNotificationBean(4l, NotificationType.SINGLE_EMAIL_ADDRESS, "rest-developer@atlassian.com"))
                .add(new UserNotificationBean(5l, NotificationType.SINGLE_USER, UserJsonBean.USER_SHORT_DOC_EXAMPLE.getKey(), UserJsonBean.USER_SHORT_DOC_EXAMPLE))
                .add(new CustomFieldValueNotificationBean(6l, NotificationType.GROUP_CUSTOM_FIELD_VALUE, FieldBean.DOC_EXAMPLE_CF.getId(), FieldBean.DOC_EXAMPLE_CF))
                .build();
        final Iterable<NotificationSchemeEventBean> notificationSchemeEvents = ImmutableList.<NotificationSchemeEventBean>builder()
                .add(new NotificationSchemeEventBean(EVENT_DOC_EXAMPLE, notifications))
                .add(new NotificationSchemeEventBean(CUSTOM_EVENT_DOC_EXAMPLE, notifications))
                .build();
        DOC_EXAMPLE = new NotificationSchemeBean(
                10100l,
                "http://example.com/jira/rest/api/2/notificationscheme/10010",
                "notification scheme name",
                "description",
                notificationSchemeEvents
        );

        PAGE_DOC_EXAMPLE = new PageBean<NotificationSchemeBean>(null, null, 6, 1, 5L, false, Lists.newArrayList(DOC_EXAMPLE));
    }
}
