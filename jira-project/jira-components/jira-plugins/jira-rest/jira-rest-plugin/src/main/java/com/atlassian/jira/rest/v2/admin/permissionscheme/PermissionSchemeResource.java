package com.atlassian.jira.rest.v2.admin.permissionscheme;

import com.atlassian.fugue.Either;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.PermissionGrant;
import com.atlassian.jira.permission.PermissionGrantInput;
import com.atlassian.jira.permission.PermissionScheme;
import com.atlassian.jira.permission.PermissionSchemeInput;
import com.atlassian.jira.permission.PermissionSchemeService;
import com.atlassian.jira.permission.data.PermissionGrantAsPureData;
import com.atlassian.jira.rest.api.permission.PermissionGrantBean;
import com.atlassian.jira.rest.api.permission.PermissionGrantBeanExpander;
import com.atlassian.jira.rest.api.permission.PermissionGrantsBean;
import com.atlassian.jira.rest.api.permission.PermissionSchemeBean;
import com.atlassian.jira.rest.api.permission.PermissionSchemeBeansFactory;
import com.atlassian.jira.rest.api.permission.PermissionSchemeExpandParam;
import com.atlassian.jira.rest.api.permission.PermissionSchemesBean;
import com.atlassian.jira.rest.util.ResponseFactory;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.ErrorCollections;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.rest.annotation.RequestType;
import com.atlassian.rest.annotation.ResponseType;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import static com.atlassian.jira.permission.data.PermissionGrantAsPureData.TO_PURE_DATA;
import static com.atlassian.jira.permission.data.PermissionGrantAsPureData.equalToModuloId;
import static com.atlassian.jira.util.ErrorCollection.Reason.VALIDATION_FAILED;
import static com.google.common.collect.Iterables.contains;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.transform;

/**
 * Resource for managing permission schemes.
 *
 * <p>
 *     Permission scheme is a collection of permission grants. Each grant holds information about a permission granted to a group of users. These groups of users
 *     are called <b>holders</b> and are defined by two values: <b>type</b> and <b>parameter</b>. Type can be for example "group", or "user" and <b>parameter</b> is
 *     an additional specification. In case of groups the parameter will hold the group name, and in case of users: user id.
 * </p>
 *
 * <p>
 *     Types can be extended by plugins, but here is a list of all built-in types (expected parameter contents are given in parenthesis):
 *
 *     <dl>
 *         <dt><b>anyone</b></dt><dd>Grant for anonymous users.</dd>
 *         <dt><b>group</b> (group name)</dt><dd>Grant for the specified group</dd>
 *         <dt><b>user</b> (user id)</dt><dd>Grant for the specified user</dd>
 *         <dt><b>projectRole</b> (project role id)</dt><dd>Grant for the specified project role</dd>
 *         <dt><b>reporter</b></dt><dd>Grant for an issue reported</dd>
 *         <dt><b>projectLead</b></dt><dd>Grant for a project lead</dd>
 *         <dt><b>assignee</b></dt><dd>Grant for a user assigned to an issue</dd>
 *         <dt><b>userCustomField</b> (custom field id)</dt><dd>Grant for a user selected in the specified custom field</dd>
 *         <dt><b>groupCustomField</b> (custom field id)</dt><dd>Grant for a user selected in the specified custom field</dd>
 *     </dl>
 * </p>
 *
 * <p>
 *     There are also two "hidden" holder types, which are not available in on-demand but can be used in enterprise instances:
 *
 *     <dl>
 *         <dt><b>reporterWithCreatePermission</b></dt><dd>This type can be used only with BROWSE_PROJECTS permission to show only projects where the user has create permission and issues within that where they are the reporter.</dd>
 *         <dt><b>assigneeWithAssignablePermission</b></dt><dd>This type can be used only with BROWSE_PROJECTS permission to show only projects where the user has the assignable permission and issues within that where they are the assignee.</dd>
 *     </dl>
 * </p>
 *
 * <p>
 *     In addition to specifying the permission holder, a permission must be selected. That way a pair of (holder, permission) is created
 *     and it represents a single permission grant.
 * </p>
 *
 * <p>
 *     Custom permissions can be added by plugins, but below we present a set of built-in JIRA permissions.
 *
 *     <ul>
 *         <li>ADMINISTER_PROJECTS</li>
 *         <li>BROWSE_PROJECTS</li>
 *         <li>VIEW_DEV_TOOLS</li>
 *         <li>VIEW_READONLY_WORKFLOW</li>
 *         <li>CREATE_ISSUES</li>
 *         <li>EDIT_ISSUES</li>
 *         <li>TRANSITION_ISSUES</li>
 *         <li>SCHEDULE_ISSUES</li>
 *         <li>MOVE_ISSUES</li>
 *         <li>ASSIGN_ISSUES</li>
 *         <li>ASSIGNABLE_USER</li>
 *         <li>RESOLVE_ISSUES</li>
 *         <li>CLOSE_ISSUES</li>
 *         <li>MODIFY_REPORTER</li>
 *         <li>DELETE_ISSUES</li>
 *         <li>LINK_ISSUES</li>
 *         <li>SET_ISSUE_SECURITY</li>
 *         <li>VIEW_VOTERS_AND_WATCHERS</li>
 *         <li>MANAGE_WATCHERS</li>
 *         <li>ADD_COMMENTS</li>
 *         <li>EDIT_ALL_COMMENTS</li>
 *         <li>EDIT_OWN_COMMENTS</li>
 *         <li>DELETE_ALL_COMMENTS</li>
 *         <li>DELETE_OWN_COMMENTS</li>
 *         <li>CREATE_ATTACHMENTS</li>
 *         <li>DELETE_ALL_ATTACHMENTS</li>
 *         <li>DELETE_OWN_ATTACHMENTS</li>
 *         <li>WORK_ON_ISSUES</li>
 *         <li>EDIT_OWN_WORKLOGS</li>
 *         <li>EDIT_ALL_WORKLOGS</li>
 *         <li>DELETE_OWN_WORKLOGS</li>
 *         <li>DELETE_ALL_WORKLOGS</li>
 *     </ul>
 *
 * </p>
 */
@Path(PermissionSchemeResource.RESOURCE_PATH)
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@WebSudoRequired
public final class PermissionSchemeResource {
    public static final String RESOURCE_PATH = "permissionscheme";
    public static final String ENTITY_PATH = "permission";

    private final JiraAuthenticationContext authenticationContext;
    private final PermissionSchemeBeansFactory beansFactory;
    private final PermissionGrantBeanExpander permissionGrantBeanExpander;
    private final PermissionSchemeService permissionSchemeService;
    private final I18nHelper i18n;
    private final ResponseFactory responseFactory;
    private final GlobalPermissionManager globalPermissionManager;

    public PermissionSchemeResource(
            final JiraAuthenticationContext authenticationContext,
            final PermissionSchemeBeansFactory beansFactory,
            final PermissionGrantBeanExpander permissionGrantBeanExpander,
            final PermissionSchemeService permissionSchemeService,
            final I18nHelper i18n,
            final ResponseFactory responseFactory,
            final GlobalPermissionManager globalPermissionManager) {
        this.authenticationContext = authenticationContext;
        this.beansFactory = beansFactory;
        this.permissionGrantBeanExpander = permissionGrantBeanExpander;
        this.permissionSchemeService = permissionSchemeService;
        this.i18n = i18n;
        this.responseFactory = responseFactory;
        this.globalPermissionManager = globalPermissionManager;
    }

    /**
     * Returns a list of all permission schemes.
     * <p>
     * By default only shortened beans are returned. If you want to include permissions of all the schemes,
     * then specify the <b>permissions</b> expand parameter. Permissions will be included also if you specify
     * any other expand parameter.
     * </p>
     *
     * @return a list of all permission schemes.
     * @response.representation.200.mediaType application/json
     * @response.representation.200.qname permissionSchemes
     * @response.representation.200.doc Returned if successful.
     * @response.representation.401.doc Returned if user is not logged in.
     * @response.representation.403.doc Returned if user is not allowed to view permission schemes.
     * @response.representation.200.example {@link PermissionSchemeBeans#SCHEME_LIST_EXAMPLE}
     */
    @GET
    @ResponseType(PermissionSchemesBean.class)
    public Response getPermissionSchemes(@QueryParam("expand") String expand) {
        return withParsedExpandParameter(expand).left().on(new Function<List<PermissionSchemeExpandParam>, Response>() {
            @Override
            public Response apply(final List<PermissionSchemeExpandParam> expands) {
                ServiceOutcome<? extends List<PermissionScheme>> permissionSchemes = permissionSchemeService.getPermissionSchemes(getUser());
                return responseFactory.validateOutcome(permissionSchemes).left().on(new Function<List<PermissionScheme>, Response>() {
                    @Override
                    public Response apply(final List<PermissionScheme> permissionSchemes) {
                        List<PermissionSchemeBean> result = ImmutableList.copyOf(transform(permissionSchemes, new Function<PermissionScheme, PermissionSchemeBean>() {
                            @Override
                            public PermissionSchemeBean apply(final PermissionScheme scheme) {
                                return beansFactory.toBean(scheme, expands);
                            }
                        }));
                        return responseFactory.okNoCache(new PermissionSchemesBean(result));
                    }
                });
            }
        });
    }

    /**
     * Returns a permission scheme identified by the given id.
     *
     * @return a permission scheme.
     * @response.representation.200.mediaType application/json
     * @response.representation.200.qname permissionScheme
     * @response.representation.200.doc Returned if successful.
     * @response.representation.401.doc Returned if user is not logged in.
     * @response.representation.403.doc Returned if user is not allowed to view permission schemes.
     * @response.representation.404.doc Returned if the scheme doesn't exist.
     * @response.representation.200.example {@link PermissionSchemeBeans#SCHEME_EXAMPLE}
     */
    @GET
    @ResponseType(PermissionSchemeBean.class)
    @Path("{schemeId}")
    public Response getPermissionScheme(@PathParam("schemeId") final Long id, @QueryParam("expand") String expand) {
        return withParsedExpandParameter(expand).left().on(new Function<List<PermissionSchemeExpandParam>, Response>() {
            @Override
            public Response apply(final List<PermissionSchemeExpandParam> expands) {
                return responseFactory.validateOutcome(permissionSchemeService.getPermissionScheme(getUser(), id)).left().on(new Function<PermissionScheme, Response>() {
                    @Override
                    public Response apply(final PermissionScheme scheme) {
                        return responseFactory.okNoCache(beansFactory.toBean(scheme, expands));
                    }
                });
            }
        });
    }

    /**
     * Deletes a permission scheme identified by the given id.
     *
     * @response.representation.204.mediaType application/json
     * @response.representation.204.doc Returned if the permission scheme is successfully deleted.
     * @response.representation.401.doc Returned if user is not logged in.
     * @response.representation.403.doc Returned if user is not allowed to delete permission schemes.
     */
    @DELETE
    @Path("{schemeId}")
    public Response deletePermissionScheme(@PathParam("schemeId") final Long id) {
        return responseFactory.serviceResultToNoContentResponse(permissionSchemeService.deletePermissionScheme(getUser(), id));
    }

    /**
     * Create a new permission scheme.
     * This method can create schemes with a defined permission set, or without.
     *
     * @return a newly created scheme
     * @response.representation.201.mediaType application/json
     * @response.representation.201.qname permissionScheme
     * @response.representation.201.doc Returned if the scheme is created successfully.
     * @response.representation.201.example {@link PermissionSchemeBeans#SCHEME_EXAMPLE}
     * @request.representation.example {@link PermissionSchemeBeans#SCHEME_CREATE_EXAMPLE}
     * @response.representation.401.doc Returned if the user is not logged.
     * @response.representation.403.doc Returned if the user is not allowed to create permission schemes.
     */
    @POST
    @RequestType(PermissionSchemeBean.class)
    @ResponseType(PermissionSchemeBean.class)
    public Response createPermissionScheme(final PermissionSchemeBean permissionScheme, @QueryParam("expand") String expand) {
        return withParsedExpandParameter(expand).left().on(new Function<List<PermissionSchemeExpandParam>, Response>() {
            @Override
            public Response apply(final List<PermissionSchemeExpandParam> expands) {
                return toResponse(beansFactory.fromBean(permissionScheme)).left().on(new Function<PermissionSchemeInput, Response>() {
                    @Override
                    public Response apply(final PermissionSchemeInput input) {
                        ServiceOutcome<PermissionScheme> createdScheme = permissionSchemeService.createPermissionScheme(getUser(), input);
                        return responseFactory.validateOutcome(createdScheme).left().on(new Function<PermissionScheme, Response>() {
                            @Override
                            public Response apply(final PermissionScheme scheme) {
                                PermissionSchemeBean created = beansFactory.toBean(scheme, expands);
                                return responseFactory.created(created.getSelf(), created);
                            }
                        });
                    }
                });
            }
        });
    }

    /**
     * Updates a permission scheme.
     * <p>
     * If the permissions list is present then it will be set in the permission scheme, which basically means it will overwrite any permission grants that
     * existed in the permission scheme. Sending an empty list will remove all permission grants from the permission scheme.
     * </p>
     * <p>
     * To update just the name and description, do not send permissions list at all.
     * </p>
     * <p>
     * To add or remove a single permission grant instead of updating the whole list at once use the <b>{schemeId}/permission/</b> resource.
     * </p>
     *
     * @return a newly updated scheme
     * @response.representation.201.mediaType application/json
     * @response.representation.201.qname permissionScheme
     * @response.representation.201.doc Returned if the scheme is updated successfully.
     * @response.representation.201.example {@link PermissionSchemeBeans#SCHEME_EXAMPLE}
     * @request.representation.example {@link PermissionSchemeBeans#SCHEME_CREATE_EXAMPLE}
     * @response.representation.401.doc Returned if the user is not logged.
     * @response.representation.403.doc Returned if the user is not allowed to edit permission schemes.
     * @response.representation.404.doc Returned if the permission is not found.
     */
    @PUT
    @Path("{schemeId}")
    @RequestType(PermissionSchemeBean.class)
    @ResponseType(PermissionSchemeBean.class)
    public Response updatePermissionScheme(@PathParam("schemeId") final Long schemeId, final PermissionSchemeBean permissionScheme, @QueryParam("expand") String expand) {

        return withParsedExpandParameter(expand).left().on(new Function<List<PermissionSchemeExpandParam>, Response>() {
            @Override
            public Response apply(final List<PermissionSchemeExpandParam> expands) {
                return updatePermissionScheme(schemeId, oldScheme -> beansFactory.fromBean(permissionScheme).map(new Function<PermissionSchemeInput, PermissionSchemeInput>() {
                    @Override
                    public PermissionSchemeInput apply(final PermissionSchemeInput newScheme) {
                        if (permissionScheme.getPermissions() == null) {
                            return PermissionSchemeInput
                                    .builder(oldScheme)
                                    .setName(newScheme.getName())
                                    .setDescription(newScheme.getDescription().getOrNull())
                                    .build();
                        }
                        return newScheme;
                    }
                }), updatedScheme -> responseFactory.okNoCache(beansFactory.toBean(updatedScheme, expands)));
            }
        });
    }

    /**
     * Returns all permission grants of the given permission scheme.
     *
     * @return a permission scheme entities list.
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc Returned if successful.
     * @response.representation.200.qname permissionGrants
     * @response.representation.401.doc Returned if user is not logged in.
     * @response.representation.403.doc Returned if user is not allowed to view permission schemes.
     * @response.representation.200.example {@link PermissionSchemeBeans#GRANT_LIST_EXAMPLE}
     */
    @GET
    @ResponseType(PermissionGrantsBean.class)
    @Path("{schemeId}/permission")
    public Response getPermissionSchemeGrants(@PathParam("schemeId") final Long schemeId, @QueryParam("expand") String expand) {
        return withParsedExpandParameter(expand).left().on(new Function<List<PermissionSchemeExpandParam>, Response>() {
            @Override
            public Response apply(final List<PermissionSchemeExpandParam> expands) {
                return responseFactory.validateOutcome(permissionSchemeService.getPermissionScheme(getUser(), schemeId)).left().on(new Function<PermissionScheme, Response>() {
                    @Override
                    public Response apply(final PermissionScheme scheme) {
                        return responseFactory.okNoCache(new PermissionGrantsBean(ImmutableList.copyOf(transform(scheme.getPermissions(), new Function<PermissionGrant, PermissionGrantBean>() {
                            @Override
                            public PermissionGrantBean apply(final PermissionGrant grant) {
                                return beansFactory.toBean(grant, schemeId, expands);
                            }
                        }))));
                    }
                });
            }
        });
    }

    /**
     * Creates a permission grant in a permission scheme.
     *
     * @return a newly created permission
     * @response.representation.201.mediaType application/json
     * @response.representation.201.qname permissionGrant
     * @response.representation.201.doc Returned if the scheme permission is created successfully.
     * @response.representation.201.example {@link PermissionSchemeBeans#GRANT_EXAMPLE}
     * @request.representation.example {@link PermissionSchemeBeans#GRANT_CREATE_EXAMPLE}
     * @response.representation.401.doc Returned if the user is not logged.
     * @response.representation.403.doc Returned if the user is not allowed to edit permission schemes.
     */
    @POST
    @RequestType(PermissionGrantBean.class)
    @ResponseType(PermissionGrantBean.class)
    @Path("{schemeId}/permission")
    public Response createPermissionGrant(@PathParam("schemeId") final Long schemeId, final PermissionGrantBean grantBean, @QueryParam("expand") String expand) {
        return withParsedExpandParameter(expand).left().on(new Function<List<PermissionSchemeExpandParam>, Response>() {
            @Override
            public Response apply(final List<PermissionSchemeExpandParam> expands) {
                return asAdmin(() -> toResponse(beansFactory.fromBean(grantBean)).left().on(new Function<PermissionGrantInput, Response>() {
                    @Override
                    public Response apply(final PermissionGrantInput newPermission) {
                        return updatePermissionScheme(schemeId, oldScheme -> validateThatNewPermissionDoesNotAlreadyExist(oldScheme, newPermission).map(new Function<PermissionGrantInput, PermissionSchemeInput>() {
                            @Override
                            public PermissionSchemeInput apply(final PermissionGrantInput input) {
                                return PermissionSchemeInput.builder(oldScheme).addPermission(input).build();
                            }
                        }), newScheme -> {
                            PermissionGrant createdPermission = Iterables.find(newScheme.getPermissions(), equalToModuloId(newPermission));
                            PermissionGrantBean createdPermissionBean = beansFactory.toBean(createdPermission, schemeId, expands);
                            return responseFactory.created(createdPermissionBean.getSelf(), createdPermissionBean);
                        });
                    }
                }));
            }
        });
    }

    /**
     * Deletes a permission grant from a permission scheme.
     *
     * @return no content response
     * @response.representation.204.mediaType application/json
     * @response.representation.204.doc Returned if the permission grant is deleted successfully.
     * @response.representation.401.doc Returned if the user is not logged.
     * @response.representation.403.doc Returned if the user is not allowed to edit permission schemes.
     */
    @DELETE
    @Path("{schemeId}/permission/{permissionId}")
    public Response deletePermissionSchemeEntity(@PathParam("schemeId") final Long schemeId, @PathParam("permissionId") final Long permissionId) {
        return asAdmin(() -> updatePermissionScheme(schemeId, scheme -> {
            if (!findPermissionGrant(scheme, permissionId).isPresent()) {
                return Either.left(ErrorCollections.create(i18n.getText("rest.permissionscheme.permission.grant.does.not.exist", permissionId.toString()), VALIDATION_FAILED));
            }

            Iterable<PermissionGrant> newPermissionSet = filter(scheme.getPermissions(), new Predicate<PermissionGrant>() {
                @Override
                public boolean apply(final PermissionGrant grant) {
                    return !grant.getId().equals(permissionId);
                }
            });
            return Either.right(PermissionSchemeInput.builder(scheme).setOriginalPermissions(newPermissionSet).build());
        }, input -> responseFactory.noContent()));
    }

    private Response updatePermissionScheme(final Long schemeId, final Function<PermissionScheme, Either<ErrorCollection, PermissionSchemeInput>> oldToNew, final Function<PermissionScheme, Response> newSchemeToResponse) {
        return responseFactory.validateOutcome(permissionSchemeService.getPermissionScheme(getUser(), schemeId)).left().on(new Function<PermissionScheme, Response>() {
            @Override
            public Response apply(final PermissionScheme scheme) {
                return toResponse(oldToNew.apply(scheme)).left().on(new Function<PermissionSchemeInput, Response>() {
                    @Override
                    public Response apply(final PermissionSchemeInput newScheme) {
                        return responseFactory.validateOutcome(permissionSchemeService.updatePermissionScheme(getUser(), schemeId, newScheme)).left().on(new Function<PermissionScheme, Response>() {
                            @Override
                            public Response apply(final PermissionScheme createdScheme) {
                                return newSchemeToResponse.apply(createdScheme);
                            }
                        });
                    }
                });
            }
        });
    }

    /**
     * Returns a permission grant identified by the given id.
     *
     * @return the permission grant
     * @response.representation.200.mediaType application/json
     * @response.representation.200.qname permissionGrant
     * @response.representation.200.doc Returned if successful.
     * @response.representation.401.doc Returned if user is not logged in.
     * @response.representation.403.doc Returned if user is not allowed to view permission schemes.
     * @response.representation.200.example {@link PermissionSchemeBeans#GRANT_EXAMPLE}
     */
    @GET
    @ResponseType(PermissionGrantBean.class)
    @Path("{schemeId}/permission/{permissionId}")
    public Response getPermissionSchemeGrant(@PathParam("schemeId") final Long schemeId, @PathParam("permissionId") final Long permissionId, @QueryParam("expand") String expand) {
        return withParsedExpandParameter(expand).left().on(new Function<List<PermissionSchemeExpandParam>, Response>() {
            @Override
            public Response apply(final List<PermissionSchemeExpandParam> expands) {
                return responseFactory.validateOutcome(permissionSchemeService.getPermissionScheme(getUser(), schemeId)).left().on(new Function<PermissionScheme, Response>() {
                    @Override
                    public Response apply(final PermissionScheme permissionScheme) {
                        Optional<PermissionGrant> maybeEntity = findPermissionGrant(permissionScheme, permissionId);
                        if (maybeEntity.isPresent()) {
                            return responseFactory.okNoCache(beansFactory.toBean(maybeEntity.get(), schemeId, expands));
                        } else {
                            return responseFactory.errorResponse(ErrorCollections.create(i18n.getText("rest.permissionscheme.permission.grant.does.not.exist", permissionId.toString()), ErrorCollection.Reason.NOT_FOUND));
                        }
                    }
                });
            }
        });
    }

    private Optional<PermissionGrant> findPermissionGrant(final PermissionScheme permissionScheme, final Long permissionId) {
        return Iterables.tryFind(permissionScheme.getPermissions(), new Predicate<PermissionGrant>() {
            @Override
            public boolean apply(final PermissionGrant grant) {
                return grant.getId().equals(permissionId);
            }
        });
    }

    private ApplicationUser getUser() {
        return authenticationContext.getUser();
    }

    private Either<Response, List<PermissionSchemeExpandParam>> withParsedExpandParameter(String parameter) {
        return toResponse(permissionGrantBeanExpander.parseExpandQuery(parameter));
    }

    private <T> Either<Response, T> toResponse(Either<ErrorCollection, T> either) {
        return responseFactory.toResponse(either);
    }

    private Either<ErrorCollection, PermissionGrantInput> validateThatNewPermissionDoesNotAlreadyExist(final PermissionScheme oldScheme, final PermissionGrantInput newPermission) {
        if (contains(transform(oldScheme.getPermissions(), TO_PURE_DATA), PermissionGrantAsPureData.of(newPermission))) {
            return Either.left(ErrorCollections.create(i18n.getText("rest.permissionscheme.permission.already.exists",
                    newPermission.getPermission().permissionKey(),
                    newPermission.getHolder().getType().toString(),
                    newPermission.getHolder().getParameter().getOrNull(),
                    oldScheme.getId().toString()), VALIDATION_FAILED));
        } else {
            return Either.right(newPermission);
        }
    }

    public Response asAdmin(RestAction action) {
        if (globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, getUser())) {
            return action.perform();
        } else {
            return responseFactory.errorResponse(ErrorCollections.create(i18n.getText("rest.permissionscheme.forbidden"), ErrorCollection.Reason.FORBIDDEN));
        }
    }

    private static interface RestAction {
        Response perform();
    }
}
