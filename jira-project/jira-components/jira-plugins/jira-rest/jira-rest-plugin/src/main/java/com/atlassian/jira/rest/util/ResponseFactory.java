package com.atlassian.jira.rest.util;

import com.atlassian.fugue.Either;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.util.ErrorCollection;

import javax.ws.rs.core.Response;
import java.net.URI;

/**
 * A component for building commonly used types of REST responses
 *
 * @since v6.1
 */
public interface ResponseFactory {
    /**
     * Creates a response that tells the user they can't use a feature when they are not logged in
     *
     * @return 401 HTTP response with appropriate message.
     */
    Response notLoggedInResponse();

    /**
     * Creates an appropriate REST error response out of error collection.
     *
     * @param errorCollection errors that will be sent back to the user and from which the HTTP response status will be
     *                        figured out
     * @return REST response with JSON-ized error collection and appropriate status
     */
    Response errorResponse(ErrorCollection errorCollection);

    /**
     * Constructs OK response with cache control set to <b>never</b>
     *
     * @param entity object that will be sent in the response
     * @return OK response holding the {@code entity}
     */
    Response okNoCache(Object entity);

    /**
     * Constructs CREATED response with cache control set to <b>never</b>
     *
     * @param self   URI to the newly created entity
     * @param entity object that will be sent in the response
     * @return CREATED response holding the {@code entity}
     */
    Response created(URI self, Object entity);

    /**
     * Constructs "no content" (204) HTTP response with cache control set to <b>never</b>
     *
     * @return no content HTTP response
     */
    Response noContent();

    /**
     * Constructs a "bad request" (400) HTTP reposne without caching.
     *
     * @param i18nKey key of the i18n message which is included in the response.
     * @param args    arguments to be added to the internationalised message.
     * @return bad request HTTP response with translated message.
     */
    Response badRequest(String i18nKey, String... args);

    /**
     * Constructs "not found" (404) HTTP response without caching.
     *
     * @param i18nKey key of the i18n message which is included in the response.
     * @return not found HTTP response.
     */
    Response notFound(String i18nKey, String... args);

    /**
     * Constructs "forbidden" (403) HTTP response without caching.
     *
     * @param i18nKey key of the i18n message which is included in the response.
     * @return forbidden HTTP response.
     */
    Response forbidden(String i18nKey, String... args);

    /**
     * Given a list of errors generate a http response
     *
     * @param errors generated
     * @return http response for the worst error
     */
    Response generateFieldErrorResponse(ErrorCollection errors);

    /**
     * Given a list of errors generate a http response
     *
     * @param errors generated
     * @return http response for the worst error
     */
    Response generateErrorResponse(ErrorCollection errors);

    /**
     * Returns a proper error response if the outcome is invalid, or the outcome value otherwise.
     *
     * @param outcome service outcome
     * @param <T>     service outcome value type
     * @return either error response or the service outcome value
     */
    <T> Either<Response, T> validateOutcome(ServiceOutcome<T> outcome);

    /**
     * Transforms a service result to "no content" (204) HTTP response with cache control set to <b>never</b>
     * if the result is valid, or error response is invalid.
     *
     * @param serviceResult service result to validate
     * @return either an error response corresponding the the service result error, or 204.
     */
    Response serviceResultToNoContentResponse(ServiceResult serviceResult);

    /**
     * Maps {@code ErrorCollection} in an {@code Either} to a {@code Response}.
     *
     * @param either either to map the left value in
     * @param <T>    type of right value
     * @return mapped either
     */
    <T> Either<Response, T> toResponse(Either<ErrorCollection, T> either);

}
