package com.atlassian.jira.rest.v2.project.type;

import com.atlassian.fugue.Option;
import com.atlassian.jira.project.type.ProjectType;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.project.type.ProjectTypeManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.rest.annotation.ResponseType;
import com.google.common.base.Function;

import javax.annotation.Nullable;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Lists.newArrayList;

/**
 * @since 7.0
 */
@Path("project/type")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@AnonymousAllowed
public class ProjectTypeResource {
    private final ProjectTypeManager projectTypeManager;
    private final JiraAuthenticationContext authenticationContext;

    public ProjectTypeResource(ProjectTypeManager projectTypeManager, JiraAuthenticationContext authenticationContext) {
        this.projectTypeManager = checkNotNull(projectTypeManager);
        this.authenticationContext = checkNotNull(authenticationContext);
    }

    /**
     * Returns all the project types defined on the JIRA instance, not taking into account whether
     * the license to use those project types is valid or not.
     *
     * @return All project types defined on the JIRA instance.
     * @response.representation.200.qname projectTypes
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc Returns a list with all the project types defined on the JIRA instance.
     * @response.representation.200.example {@link com.atlassian.jira.rest.v2.project.type.ProjectTypeBean#TYPES_EXAMPLE}
     * @since 7.0
     */
    @GET
    @ResponseType(value = List.class, genericTypes = ProjectTypeBean.class)
    public Response getAllProjectTypes() {
        return Response.ok(newArrayList(transform(projectTypeManager.getAllProjectTypes(), new Function<ProjectType, ProjectTypeBean>() {
            @Override
            public ProjectTypeBean apply(@Nullable final ProjectType projectType) {
                return toBean(projectType);
            }
        }))).build();
    }

    /**
     * Returns the project type with the given key.
     *
     * @return The project type with the given key.
     * @response.representation.200.qname projectType
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc Returns a representation of the project type with the given id
     * @response.representation.200.example {@link com.atlassian.jira.rest.v2.project.type.ProjectTypeBean#DOC_EXAMPLE1}
     * @since 7.0
     */
    @GET
    @Path("{projectTypeKey}")
    @ResponseType(ProjectTypeBean.class)
    public Response getProjectTypeByKey(@PathParam("projectTypeKey") String projectTypeKey) {
        Option<ProjectType> projectType = projectTypeManager.getByKey(new ProjectTypeKey(projectTypeKey));
        if (projectType.isDefined()) {
            return Response.ok(toBean(projectType.get())).build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    /**
     * Returns the project type with the given key, if it is accessible to the logged in user.
     * This takes into account whether the user is licensed on the Application that defines the project type.
     *
     * @return The project type with the given key.
     * @response.representation.200.qname projectType
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc Returns a representation of the project type with the given id
     * @response.representation.401.doc A response status of 401 indicates that there is not a logged in user and therefore this operation can't be performed
     * @response.representation.404.doc A response status of 404 indicates that the project type is not accessible for the logged in user
     * @response.representation.200.example {@link com.atlassian.jira.rest.v2.project.type.ProjectTypeBean#DOC_EXAMPLE1}
     * @since 7.0
     */
    @GET
    @Path("{projectTypeKey}/accessible")
    @ResponseType(ProjectTypeBean.class)
    public Response getAccessibleProjectTypeByKey(@PathParam("projectTypeKey") String projectTypeKey) {
        if (authenticationContext.isLoggedInUser()) {
            Option<ProjectType> projectType = projectTypeManager.getAccessibleProjectType(authenticationContext.getLoggedInUser(), new ProjectTypeKey(projectTypeKey));
            if (projectType.isDefined()) {
                return Response.ok(toBean(projectType.get())).build();
            } else {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
        }
        return Response.status(Response.Status.UNAUTHORIZED).build();
    }

    private ProjectTypeBean toBean(ProjectType projectType) {
        return new ProjectTypeBean(
                projectType.getKey().getKey(),
                projectType.getDescriptionI18nKey(),
                projectType.getIcon(),
                projectType.getColor()
        );
    }
}
