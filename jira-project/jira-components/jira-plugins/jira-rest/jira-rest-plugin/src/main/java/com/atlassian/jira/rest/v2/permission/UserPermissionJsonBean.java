package com.atlassian.jira.rest.v2.permission;

import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.GlobalPermissionType;
import com.atlassian.jira.permission.ProjectPermission;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.google.common.base.Objects;
import org.codehaus.jackson.annotate.JsonProperty;

import static com.atlassian.jira.permission.LegacyProjectPermissionKeyMapping.getId;

/**
 * Represents one permission and whether the caller has it
 *
 * @since v5.0
 */
public class UserPermissionJsonBean extends PermissionJsonBean {
    @JsonProperty
    public boolean havePermission;

    @JsonProperty
    public Boolean deprecatedKey;

    public UserPermissionJsonBean() {
    }

    /**
     * Construct a bean using a Global Permission - rather than the Permission enum
     */
    public UserPermissionJsonBean(GlobalPermissionType globalPermissionType, boolean havePermission,
                                  JiraAuthenticationContext authenticationContext) {
        Integer id = GlobalPermissionKey.GLOBAL_PERMISSION_ID_TRANSLATION.inverse().get(globalPermissionType.getGlobalPermissionKey());
        id((id == null) ? "-1" : id.toString());
        key(globalPermissionType.getKey());
        this.type = PermissionType.GLOBAL;
        this.name = authenticationContext.getI18nHelper().getText(globalPermissionType.getNameI18nKey());
        this.description = authenticationContext.getI18nHelper().getText(globalPermissionType.getDescriptionI18nKey());
        this.havePermission = havePermission;
    }

    /**
     * Construct a bean using a Project Permission
     */
    public UserPermissionJsonBean(ProjectPermission permission, boolean havePermission,
                                  JiraAuthenticationContext authenticationContext) {
        Integer id = getId(permission.getProjectPermissionKey());
        id((id == null) ? "-1" : id.toString());
        key(permission.getKey());
        this.type = PermissionType.PROJECT;
        this.name = authenticationContext.getI18nHelper().getText(permission.getNameI18nKey());
        this.description = authenticationContext.getI18nHelper().getText(permission.getDescriptionI18nKey());
        this.havePermission = havePermission;
    }

    public UserPermissionJsonBean(Permissions.Permission permission, boolean havePermission,
                                  JiraAuthenticationContext authenticationContext) {
        id(String.valueOf(permission.getId()));
        key(permission.name());
        this.type = PermissionType.PROJECT;
        this.name = authenticationContext.getI18nHelper().getText(permission.getNameKey());
        this.description = authenticationContext.getI18nHelper().getText(permission.getDescriptionKey());
        this.havePermission = havePermission;
        this.deprecatedKey = true;
    }

    // please dont use this one...
    public UserPermissionJsonBean(ProjectPermissionKey permissionKey, String name, String description,
                                  boolean havePermission) {
        Integer id = getId(permissionKey);
        id((id == null) ? "-1" : id.toString());
        key(permissionKey.permissionKey());
        this.type = PermissionType.PROJECT;
        this.name = name;
        this.description = description;
        this.havePermission = havePermission;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("id", id())
                .add("key", key())
                .add("name", name)
                .add("type", type)
                .add("description", description)
                .add("deprecatedKey", deprecatedKey)
                .add("havePermission", havePermission)
                .toString();
    }
}
