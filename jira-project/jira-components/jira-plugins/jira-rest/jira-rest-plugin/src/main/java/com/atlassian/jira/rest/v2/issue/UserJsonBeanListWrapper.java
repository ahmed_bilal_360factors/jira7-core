package com.atlassian.jira.rest.v2.issue;

import com.atlassian.jira.issue.comparator.UserBestNameComparator;
import com.atlassian.jira.issue.fields.rest.json.UserBeanFactory;
import com.atlassian.jira.issue.fields.rest.json.beans.UserJsonBean;
import com.atlassian.jira.rest.api.expand.PagedListWrapper;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.base.Supplier;
import com.google.common.collect.Ordering;

import java.util.List;

/**
 * Wraps a list of Users and pages over them
 *
 * @since v6.0
 */
public class UserJsonBeanListWrapper extends PagedListWrapper<UserJsonBean, ApplicationUser> {
    private final Ordering<ApplicationUser> userOrdering = Ordering.from(new UserBestNameComparator());
    private Supplier<List<ApplicationUser>> usersSupplier;
    private ApplicationUser loggedInUser;
    private UserBeanFactory userBeanFactory;

    // For JAXB
    public UserJsonBeanListWrapper() {
    }

    public UserJsonBeanListWrapper(final int userCount, Supplier<List<ApplicationUser>> usersSupplier, final int maxResults, final ApplicationUser loggedInUser, final UserBeanFactory userBeanFactory) {
        super(userCount, maxResults);
        this.userBeanFactory = userBeanFactory;
        this.usersSupplier = usersSupplier;
        this.loggedInUser = loggedInUser;
    }

    public UserJsonBean fromBackedObject(final ApplicationUser user) {
        return userBeanFactory.createBean(user, loggedInUser);
    }

    @Override
    public int getBackingListSize() {
        return size;
    }

    @Override
    public List<ApplicationUser> getOrderedList(final int startIndex, final int endIndex) {
        final List<ApplicationUser> sortedUsers = userOrdering.leastOf(usersSupplier.get(), endIndex + 1);
        // Make sure the indexes cannot be out of bounds, in case the collection of users has changed since we calculated the size
        final int safeStartIndexInclusive = Math.min(startIndex, sortedUsers.size());
        final int safeEndIndexExclusive = Math.min(endIndex + 1, sortedUsers.size());
        return sortedUsers.subList(safeStartIndexInclusive, safeEndIndexExclusive);
    }
}
