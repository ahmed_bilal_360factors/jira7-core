package com.atlassian.jira.rest.v2.entity.property;

import com.atlassian.fugue.Function2;
import com.atlassian.fugue.Option;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.entity.WithId;
import com.atlassian.jira.entity.property.EntityProperty;
import com.atlassian.jira.entity.property.EntityPropertyService;
import com.atlassian.jira.entity.property.EntityPropertyType;
import com.atlassian.jira.entity.property.JsonEntityPropertyManagerImpl;
import com.atlassian.jira.issue.fields.rest.json.beans.EntityPropertyBean;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.rest.api.util.ErrorCollection;
import com.atlassian.jira.rest.exception.BadRequestWebException;
import com.atlassian.jira.rest.util.RestStringUtils;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.google.common.base.Function;
import com.google.common.base.Supplier;
import com.google.common.io.ByteStreams;
import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import static com.atlassian.jira.entity.property.EntityPropertyService.DeletePropertyValidationResult;
import static com.atlassian.jira.entity.property.EntityPropertyService.PropertyInput;
import static com.atlassian.jira.entity.property.EntityPropertyService.PropertyKeys;
import static com.atlassian.jira.entity.property.EntityPropertyService.PropertyResult;
import static com.atlassian.jira.entity.property.EntityPropertyService.SetPropertyValidationResult;
import static com.atlassian.jira.rest.api.http.CacheControl.never;
import static com.atlassian.jira.util.ErrorCollection.Reason;
import static com.google.common.base.Preconditions.checkNotNull;
import static javax.ws.rs.core.Response.status;

/**
 * @since v6.2
 */
public class BasePropertyResource<E extends WithId> {
    private final EntityPropertyService<E> entityPropertyService;
    private final JiraAuthenticationContext authContext;
    private final JiraBaseUrls jiraBaseUrls;
    private final I18nHelper i18n;
    private final EntityPropertyType entityPropertyType;
    private final Function2<Long, String, String> entityIdAndPropertyKeyToSelfFunction;
    private final ObjectMapper minimalistObjectMapper;

    public BasePropertyResource(EntityPropertyService<E> entityPropertyService, JiraAuthenticationContext authContext,
                                JiraBaseUrls jiraBaseUrls, I18nHelper i18n, Function2<Long, String, String> entityIdAndPropertyKeyToSelfFunction,
                                EntityPropertyType entityPropertyType) {
        this.entityPropertyService = entityPropertyService;
        this.authContext = authContext;
        this.jiraBaseUrls = jiraBaseUrls;
        this.i18n = i18n;
        this.entityPropertyType = checkNotNull(entityPropertyType);
        this.entityIdAndPropertyKeyToSelfFunction = checkNotNull(entityIdAndPropertyKeyToSelfFunction);

        // mappers are expensive so we create a singleton to perform the normalisation of stored strings to a minimalist json string
        minimalistObjectMapper = new ObjectMapper();
        minimalistObjectMapper.disable(SerializationConfig.Feature.INDENT_OUTPUT);
    }

    /**
     * Returns a new instance of this class which behaves in the same way as this
     * but uses a newly supplied function to calculate self links of properties.
     *
     * @param selfFunction function that will be used by the returned instance to calculate self links
     * @return a new instance
     */
    public BasePropertyResource<E> withSelfFunction(Function2<Long, String, String> selfFunction) {
        return new BasePropertyResource<>(
                entityPropertyService,
                authContext,
                jiraBaseUrls,
                i18n,
                selfFunction,
                entityPropertyType);
    }

    /**
     * Returns the keys of all properties for the entity identified by the id.
     *
     * @param id the entity from which keys will be returned.
     * @return a response containing EntityPropertiesKeysBean.
     */
    public Response getPropertiesKeys(final String id) {
        final ApplicationUser user = authContext.getLoggedInUser();
        PropertyKeys<E> propertyKeys = getPropertiesKeys(user, id);
        if (propertyKeys.isValid()) {
            EntityPropertiesKeysBean entity = EntityPropertiesKeysBean.build(jiraBaseUrls,
                    propertyKeys.getEntity().getId(), propertyKeys.getKeys(), entityIdAndPropertyKeyToSelfFunction);
            return status(Response.Status.OK).entity(entity)
                    .cacheControl(never())
                    .build();
        } else {
            return error(propertyKeys.getErrorCollection());
        }
    }

    /**
     * Sets the value of the specified entity's property.
     * <p>
     * This method can used to store a custom data against the entity identified by the key or by the id. The user
     * who stores the data is required to have permissions to edit the entity.
     * </p>
     *
     * @param id          the entity's id on which the property will be set.
     * @param propertyKey the key of the entity's property. The maximum length of the key is 255 bytes.
     * @param request     the request containing value of the entity's property. The value has to a valid, non-empty JSON conforming
     *                    to http://tools.ietf.org/html/rfc4627. The maximum length of the property value is 32768 bytes.
     */
    public Response setProperty(final String id, final String propertyKey, final HttpServletRequest request) {
        final ApplicationUser user = authContext.getLoggedInUser();

        SetPropertyValidationResult setValidationResult =
                validateSetProperty(user, new PropertyInput(propertyValue(request), propertyKey), id);

        if (setValidationResult.isValid()) {
            PropertyResult property = getProperty(user, propertyKey, id);
            PropertyResult propertyResult = entityPropertyService.setProperty(user, setValidationResult);
            if (!propertyResult.isValid()) {
                return error(propertyResult.getErrorCollection());
            }
            return property.getEntityProperty().fold(
                    () -> status(Response.Status.CREATED).cacheControl(never()).build(),
                    entityProperty -> status(Response.Status.OK).cacheControl(never()).build()
            );
        } else {
            return error(setValidationResult.getErrorCollection());
        }
    }

    /**
     * Returns the value of the property with a given key from the entity identified by the the id. The user who retrieves
     * the property is required to have permissions to read the entity.
     *
     * @param entityId    the id of the entity from which the property will be returned.
     * @param propertyKey the key of the property to returned.
     * @return a response containing {@link EntityPropertyBean}.
     */
    public Response getProperty(final String entityId, final String propertyKey) {
        final ApplicationUser user = authContext.getLoggedInUser();

        PropertyResult propertyResult = getProperty(user, propertyKey, entityId);

        if (propertyResult.isValid()) {
            Option<EntityProperty> property = propertyResult.getEntityProperty();

            return property.fold(propertyDoesNotExist(propertyKey), propertyBean());
        } else {
            return error(propertyResult.getErrorCollection());
        }
    }

    private Supplier<Response> propertyDoesNotExist(String propertyKey) {
        return () -> {
            final com.atlassian.jira.util.ErrorCollection errorCollection = new SimpleErrorCollection();
            errorCollection.addErrorMessage(i18n.getText("jira.properties.service.property.does.not.exist", propertyKey), Reason.NOT_FOUND);
            return error(errorCollection);
        };
    }

    private Function<EntityProperty, Response> propertyBean() {
        return entityProperty -> {
            EntityPropertyBean entityPropertyBean = EntityPropertyBean.builder(jiraBaseUrls, entityIdAndPropertyKeyToSelfFunction)
                    .value(jacksonSafeJsonValue(entityProperty.getValue()))
                    .key(entityProperty.getKey())
                    .build(entityProperty.getEntityId());
            return status(Response.Status.OK).entity(entityPropertyBean).cacheControl(never()).build();
        };
    }

    private String jacksonSafeJsonValue(String storedJSON) {
        String serialisedJSON = storedJSON;
        try {
            //
            // jackson also has a problem where it can't convert newlines in JSON objects to JsonRawValue : JRA-47582
            // so we normalize it to no indentation. Its also strange that Jackson can handle this (with Unicode characters)
            // but not later when its tries to Object serialise the result but....there you go.
            JsonNode jsonNode = minimalistObjectMapper.readTree(serialisedJSON);
            serialisedJSON = minimalistObjectMapper.writeValueAsString(jsonNode);

            // escape unicode, because for some characters(i.e. pile of poo) jackson may throw exception
            // during processing of JsonRawValue, which entity property value is in this case
            serialisedJSON = RestStringUtils.escapeUnicode(serialisedJSON);
            return serialisedJSON;
        } catch (IOException e) {
            throw new IllegalStateException(String.format("The JSON stored against an entity is, unexpectedly, not valid JSON - '%s'", storedJSON), e);
        }
    }

    public Response deleteProperty(final String id, String propertyKey) {
        final ApplicationUser user = authContext.getLoggedInUser();

        DeletePropertyValidationResult deleteValidationResult = validateDeleteProperty(user, propertyKey, id);

        if (deleteValidationResult.isValid()) {
            entityPropertyService.deleteProperty(user, deleteValidationResult);
            return status(Response.Status.NO_CONTENT).cacheControl(never()).build();
        } else {
            return error(deleteValidationResult.getErrorCollection());
        }
    }

    protected PropertyKeys<E> getPropertiesKeys(final ApplicationUser user, final String id) {
        return withIdValidation(id, entityId -> entityPropertyService.getPropertiesKeys(user, entityId));
    }

    protected PropertyResult getProperty(final ApplicationUser user, final String propertyKey, final String id) {
        return withIdValidation(id, entityId -> entityPropertyService.getProperty(user, entityId, propertyKey));
    }

    protected DeletePropertyValidationResult validateDeleteProperty(final ApplicationUser user, final String propertyKey, final String id) {
        return withIdValidation(id, id1 -> entityPropertyService.validateDeleteProperty(user, id1, propertyKey));
    }

    protected SetPropertyValidationResult validateSetProperty(final ApplicationUser user, final PropertyInput propertyInput,
                                                              final String id) {
        return withIdValidation(id, entityId -> entityPropertyService.validateSetProperty(user, entityId, propertyInput));
    }

    protected <T> T withIdValidation(final String id, final Function<Long, T> idFunction) {
        return idFunction.apply(getLongOrBadRequest(id));
    }

    private Response error(final com.atlassian.jira.util.ErrorCollection errorCollection) {
        Reason reason = Option.option(Reason.getWorstReason(errorCollection.getReasons())).getOrElse(Reason.SERVER_ERROR);
        return status(reason.getHttpStatusCode()).entity(ErrorCollection.of(errorCollection)).cacheControl(never()).build();
    }

    private Long getLongOrBadRequest(final String idOrKey) {
        try {
            return Long.parseLong(idOrKey);
        } catch (NumberFormatException e) {
            SimpleErrorCollection errorCollection = new SimpleErrorCollection();
            errorCollection.addErrorMessage(i18n.getText("jira.properties.service.property.invalid.entity", i18n.getText(entityPropertyType.getI18nKeyForEntityName())));
            throw new BadRequestWebException(ErrorCollection.of(errorCollection));
        }
    }

    private String propertyValue(final HttpServletRequest request) {
        try {
            InputStream limitInputStream = ByteStreams.limit(request.getInputStream(), JsonEntityPropertyManagerImpl.MAXIMUM_VALUE_LENGTH + 1);
            byte[] bytes = IOUtils.toByteArray(limitInputStream);
            if (bytes.length > JsonEntityPropertyManagerImpl.MAXIMUM_VALUE_LENGTH) {
                throw new BadRequestWebException(
                        ErrorCollection.of(i18n.getText("jira.properties.service.length.unknown", JsonEntityPropertyManagerImpl.MAXIMUM_VALUE_LENGTH)));
            }
            return new String(bytes, Charset.forName(ComponentAccessor.getApplicationProperties().getEncoding()));
        } catch (IOException e) {
            throw new BadRequestWebException(ErrorCollection.of(e.getMessage()));
        }
    }

}
