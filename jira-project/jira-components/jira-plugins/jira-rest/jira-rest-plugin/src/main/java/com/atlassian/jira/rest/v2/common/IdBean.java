package com.atlassian.jira.rest.v2.common;

import com.google.common.base.Objects;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Simple bean that contains only one field: id.
 */
public final class IdBean {
    public static final IdBean EXAMPLE = new IdBean(10000L);

    @JsonProperty
    private Long id;

    @Deprecated
    public IdBean() {
    }

    public IdBean(final Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IdBean that = (IdBean) o;

        return Objects.equal(this.id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("id", id)
                .toString();
    }
}
