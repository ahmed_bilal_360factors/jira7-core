package com.atlassian.jira.rest.v2.issue.project;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.rest.api.project.ProjectRoleBean;
import com.atlassian.jira.rest.api.project.RoleActorBean;
import com.atlassian.jira.security.roles.DefaultRoleActors;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleActors;
import com.atlassian.jira.security.roles.RoleActor;
import com.atlassian.jira.security.roles.RoleActorComparator;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.Function;
import com.atlassian.jira.util.collect.Transformed;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import java.util.Collection;
import java.util.SortedSet;
import java.util.TreeSet;

import static org.apache.commons.collections.CollectionUtils.isNotEmpty;

/**
 * @since v6.1
 */
public class ProjectRoleBeanFactoryImpl implements ProjectRoleBeanFactory {
    private final JiraBaseUrls jiraBaseUrls;
    private final AvatarService avatarService;
    private final UriInfo uriInfo;

    public ProjectRoleBeanFactoryImpl(final JiraBaseUrls jiraBaseUrls, final AvatarService avatarService, final UriInfo uriInfo) {
        this.jiraBaseUrls = jiraBaseUrls;
        this.avatarService = avatarService;
        this.uriInfo = uriInfo;
    }

    @Override
    public ProjectRoleBean projectRole(@Nonnull Project project, @Nonnull ProjectRole projectRole) {
        final ProjectRoleBean projectRoleBean = projectRole(projectRole, null);

        projectRoleBean.self = uriInfo.getBaseUriBuilder().path(ProjectRoleResource.class).path(projectRoleBean.id.toString()).build(project.getId());
        return projectRoleBean;
    }

    @Override
    public ProjectRoleBean projectRole(@Nonnull final Project project, @Nonnull final ProjectRole projectRole, @Nonnull final ProjectRoleActors projectRoleActors, @Nullable final ApplicationUser loggedInUser) {

        // Sort the actors by name, as opposed to parameter
        final SortedSet<RoleActor> sortedActors = new TreeSet<RoleActor>(RoleActorComparator.COMPARATOR);
        sortedActors.addAll(projectRoleActors.getRoleActors());

        final Collection<RoleActorBean> actors = Transformed.collection(sortedActors, new Function<RoleActor, RoleActorBean>() {
            public RoleActorBean get(RoleActor actor) {
                final RoleActorBean bean = RoleActorBean.convert(actor);
                bean.setAvatarUrl(avatarService.getAvatarURL(loggedInUser, bean.getName(), Avatar.Size.SMALL));
                return bean;
            }
        });
        final ProjectRoleBean projectRoleBean = projectRole(project, projectRole);
        projectRoleBean.actors = actors;
        return projectRoleBean;
    }

    @Override
    public ProjectRoleBean projectRole(@Nonnull final ProjectRole projectRole, final DefaultRoleActors actors) {
        final ProjectRoleBean projectRoleBean = new ProjectRoleBean();
        projectRoleBean.name = projectRole.getName();
        projectRoleBean.id = projectRole.getId();
        projectRoleBean.description = projectRole.getDescription();
        projectRoleBean.self = uriInfo.getBaseUriBuilder().path(RoleResource.class).path(projectRoleBean.id.toString()).build();

        if (actors != null && isNotEmpty(actors.getRoleActors())) {
            projectRoleBean.actors = Transformed.collection(actors.getRoleActors(), new Function<RoleActor, RoleActorBean>() {
                public RoleActorBean get(RoleActor actor) {
                    return RoleActorBean.convert(actor);
                }
            });
        }

        return projectRoleBean;
    }

    @Override
    public ProjectRoleBean shortProjectRole(@Nonnull final ProjectRole projectRole) {
        return projectRole(projectRole, null);
    }

    @Override
    public ProjectRoleBean shortRoleBean(@Nonnull final ProjectRole projectRole) {
        final ProjectRoleBean projectRoleBean = new ProjectRoleBean();
        projectRoleBean.name = projectRole.getName();
        projectRoleBean.id = projectRole.getId();
        projectRoleBean.description = projectRole.getDescription();
        projectRoleBean.self = UriBuilder.fromPath(jiraBaseUrls.restApi2BaseUrl())
                .path("role")
                .path(projectRoleBean.id.toString())
                .build();

        return projectRoleBean;
    }
}
