package com.atlassian.jira.rest.v2.issue;

import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.embedded.impl.ImmutableGroup;
import com.atlassian.crowd.embedded.impl.ImmutableUser;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.crowd.exception.embedded.InvalidGroupException;
import com.atlassian.crowd.exception.runtime.OperationFailedException;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.group.GroupService;
import com.atlassian.jira.issue.fields.rest.json.UserBeanFactory;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.fields.rest.json.beans.UserJsonBean;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.rest.api.pagination.PageBean;
import com.atlassian.jira.rest.api.util.ErrorCollection;
import com.atlassian.jira.rest.api.util.StringList;
import com.atlassian.jira.rest.exception.BadRequestWebException;
import com.atlassian.jira.rest.exception.ForbiddenWebException;
import com.atlassian.jira.rest.exception.NotFoundWebException;
import com.atlassian.jira.rest.exception.ServerErrorWebException;
import com.atlassian.jira.rest.util.ResponseFactory;
import com.atlassian.jira.rest.util.SelfLinkBuilder;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.Page;
import com.atlassian.jira.util.PageRequest;
import com.atlassian.jira.util.PageRequests;
import com.atlassian.rest.annotation.RequestType;
import com.atlassian.rest.annotation.ResponseType;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang.StringUtils;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.function.Function;

import static com.atlassian.jira.rest.api.http.CacheControl.never;
import static com.google.common.base.MoreObjects.firstNonNull;
import static com.google.common.collect.Iterables.filter;
import static java.lang.Math.min;

/**
 * @since v6.0
 */
@Path("group")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class GroupResource {
    static final int MAX_USERS_COUNT = 50;

    private final GlobalPermissionManager permissionManager;
    private final JiraAuthenticationContext authContext;
    private final I18nHelper i18n;
    private final GroupManager groupManager;
    private final GroupService groupService;
    private final JiraBaseUrls jiraBaseUrls;
    private final SelfLinkBuilder.SelfLink groupSelfLink;
    private final CrowdService crowdService;
    private final UserBeanFactory userBeanFactory;
    private final ResponseFactory responses;

    public GroupResource(GlobalPermissionManager permissionManager,
                         JiraAuthenticationContext authContext,
                         I18nHelper i18n,
                         GroupManager groupManager,
                         GroupService groupService,
                         JiraBaseUrls jiraBaseUrls,
                         SelfLinkBuilder selfLinkBuilder,
                         CrowdService crowdService,
                         UserBeanFactory userBeanFactory,
                         ResponseFactory responses) {
        this.permissionManager = permissionManager;
        this.authContext = authContext;
        this.i18n = i18n;
        this.groupManager = groupManager;
        this.groupService = groupService;
        this.jiraBaseUrls = jiraBaseUrls;
        this.crowdService = crowdService;
        this.userBeanFactory = userBeanFactory;
        this.responses = responses;

        this.groupSelfLink = selfLinkBuilder.path("group");
    }

    /**
     * Returns REST representation for the requested group. Allows to get list of active users belonging to the
     * specified group and its subgroups if "users" expand option is provided. You can page through users list by using
     * indexes in expand param. For example to get users from index 10 to index 15 use "users[10:15]" expand value. This
     * will return 6 users (if there are at least 16 users in this group). Indexes are 0-based and inclusive.
     * <p>
     * This resource is deprecated, please use group/member API instead.
     *
     * @param groupName A name of requested group.
     * @param expand    List of fields to expand. Currently only available expand is "users".
     * @return REST representation of a group
     * @since 6.0
     */
    @Deprecated
    @GET
    @ResponseType(GroupBean.class)
    public Response getGroup(@QueryParam("groupname") final String groupName, @QueryParam("expand") StringList expand) {
        ensureCanManageGroups();
        Group group = ensureGroupExists(groupName);
        final GroupBean groupBean = buildGroupBean(group);
        return Response.ok(groupBean).build();
    }

    private boolean hasGroupManagementPermission(final ApplicationUser remoteUser) {
        return permissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, remoteUser)
                || permissionManager.hasPermission(GlobalPermissionKey.SYSTEM_ADMIN, remoteUser);
    }

    /**
     * This resource returns a <a href="#pagination">paginated</a> list of users who are members of the specified group and its subgroups.
     * Users in the page are ordered by user names. User of this resource is required to have sysadmin or admin permissions.
     *
     * @param startAt              the index of the first user in group to return (0 based).
     * @param maxResults           the maximum number of users to return (max 50).
     * @param groupName            a name of the group for which members will be returned.
     * @param includeInactiveUsers inactive users will be included in the response if set to true.
     * @return a paginated list of users in the group.
     * @response.representation.200.qname usersInGroup
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc Paginated list of users in the group.
     * @response.representation.200.example {@link com.atlassian.jira.rest.v2.issue.GroupResourceExample#PAGE_DOC_EXAMPLE}
     * @response.representation.400.doc Returned if the name of the provided group is empty
     * @response.representation.401.doc Returned if the user is not logged in.
     * @response.representation.403.doc Returned if the calling user is not admin or sysadmin
     * @response.representation.404.doc Returned if the specified group does not exist
     */
    @Path("member")
    @GET
    @ResponseType(GroupBean.class)
    public Response getUsersFromGroup(@QueryParam("groupname") final String groupName,
                                      @QueryParam("includeInactiveUsers") @DefaultValue("false") boolean includeInactiveUsers,
                                      @QueryParam("startAt") @DefaultValue("0") final Long startAt,
                                      @QueryParam("maxResults") @DefaultValue("50") final Integer maxResults) {
        ensureCanManageGroups();
        Group group = ensureGroupExists(groupName);

        final Integer limit = firstNonNull(maxResults, MAX_USERS_COUNT);
        final PageRequest pageRequest = PageRequests.request(startAt, min(limit, MAX_USERS_COUNT));

        final Page<ApplicationUser> usersInGroup = groupManager.getUsersInGroup(group.getName(), includeInactiveUsers, pageRequest);

        return responses.okNoCache(PageBean.from(pageRequest, usersInGroup)
                .setLinks(buildSelfForPagedUsers(groupName, includeInactiveUsers, startAt, maxResults), pageRequest.getLimit())
                .build(new Function<ApplicationUser, UserJsonBean>() {
                    @Override
                    public UserJsonBean apply(final ApplicationUser user) {
                        return userBeanFactory.createBean(user, authContext.getLoggedInUser());
                    }
                }));
    }

    private String buildSelfForPagedUsers(String groupName, Boolean includeInactiveUsers, Long startAt, Integer maxResults) {
        final SelfLinkBuilder.SelfLink selfLink = groupSelfLink.path("member").queryParam("groupname", groupName).queryParam("includeInactiveUsers", includeInactiveUsers.toString())
                .queryParam("startAt", startAt.toString()).queryParam("maxResults", maxResults.toString());

        return selfLink.toString();
    }

    /**
     * Creates a group by given group parameter
     * <p>
     * Returns REST representation for the requested group.
     *
     * @param groupBean a group to add
     * @return REST representation of a group
     * @response.representation.201.qname group
     * @response.representation.201.mediaType application/json
     * @response.representation.201.doc Returns full representation of a JIRA group in JSON format.
     * @response.representation.201.example {@link com.atlassian.jira.rest.v2.issue.GroupBean#DOC_EXAMPLE_WITH_USERS}
     * @response.representation.400.doc Returned if user requested an empty group name or group already exists
     * @response.representation.401.doc Returned if the current user is not authenticated.
     * @response.representation.403.doc Returned if the current user does not have administrator permissions.
     * @since 6.1
     */
    @POST
    @WebSudoRequired
    @RequestType(AddGroupBean.class)
    @ResponseType(GroupBean.class)
    public Response createGroup(final AddGroupBean groupBean) {
        final String groupName = groupBean.getName();

        return doGroupUpdate(new GroupUpdateCommand() {
            @Override
            public Response execute() throws OperationNotPermittedException, InvalidGroupException {
                validateGroupName(groupName);

                if (crowdService.getGroup(groupName) != null) {
                    // Group exists
                    throw new BadRequestWebException(ErrorCollection.of(i18n.getText("groupbrowser.error.group.exists")));
                }

                final ImmutableGroup immutableGroup = new ImmutableGroup(groupName);
                crowdService.addGroup(immutableGroup);

                final GroupBean responseGroupBean = buildGroupBean(immutableGroup);
                return Response.status(Response.Status.CREATED)
                        .location(responseGroupBean.getSelf()).entity(responseGroupBean)
                        .cacheControl(never()).build();
            }
        });
    }

    /**
     * Deletes a group by given group parameter.
     * <p>
     * Returns no content
     *
     * @param groupName (mandatory) The name of the group to delete.
     * @param swapGroup If you delete a group and content is restricted to that group, the content will be hidden from all users. 
     * To prevent this, use this parameter to specify a different group to transfer the restrictions (comments and worklogs only) to.
     * @return no content
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc Returned if the group was deleted.
     * @response.representation.400.doc Returned if user requested an group that does not exist.
     * @response.representation.401.doc Returned if the current user is not authenticated.
     * @response.representation.403.doc Returned if the current user does not have administrator permissions.
     * @response.representation.404.doc Returned if the requested group was not found.
     * @since 6.1
     */
    @DELETE
    @WebSudoRequired
    public Response removeGroup(@QueryParam("groupname") final String groupName, @QueryParam("swapGroup") final String swapGroup) {
        return doGroupUpdate(new GroupUpdateCommand() {
            @Override
            public Response execute() throws OperationNotPermittedException {
                ensureGroupExists(groupName);

                final JiraServiceContextImpl validateContext = new JiraServiceContextImpl(authContext.getLoggedInUser());
                if (!groupService.validateDelete(validateContext, groupName, swapGroup)) {
                    return responses.errorResponse(validateContext.getErrorCollection());
                }

                final JiraServiceContextImpl deleteContext = new JiraServiceContextImpl(authContext.getLoggedInUser());
                if (!groupService.delete(deleteContext, groupName, swapGroup)) {
                    return responses.errorResponse(deleteContext.getErrorCollection());
                }

                return Response.ok().cacheControl(never()).build();
            }
        });
    }

    /**
     * Adds given user to a group.
     * <p>
     * Returns the current state of the group.
     *
     * @param groupName A name of requested group.
     * @param userBean  User to add to a group
     * @return REST representation of a group     
     * @response.representation.201.qname group
     * @response.representation.201.mediaType application/json
     * @response.representation.201.doc Returns full representation of a JIRA group in JSON format.
     * @request.representation.example {@link UpdateUserToGroupBean#DOC_EXAMPLE}
     * @response.representation.400.doc Returned if user requested an empty group name or the user already belongs to the group.
     * @response.representation.401.doc Returned if the current user is not authenticated.
     * @response.representation.403.doc Returned if the current user does not have administrator permissions.
     * @response.representation.404.doc Returned if the requested group was not found or requested user was not found.
     * @since 6.1
     */
    @POST
    @WebSudoRequired
    @Path("user")
    @ResponseType(GroupBean.class)
    public Response addUserToGroup(@QueryParam("groupname") final String groupName, final UpdateUserToGroupBean userBean) {

        return doGroupUpdate(new GroupUpdateCommand() {
            @Override
            public Response execute() throws OperationNotPermittedException {
                ensureGroupExists(groupName);
                final String username = userBean.getName();

                final JiraServiceContextImpl serviceContext = new JiraServiceContextImpl(authContext.getLoggedInUser());
                if (!groupService.validateAddUserToGroup(serviceContext, ImmutableList.of(groupName), username)) {
                    return responses.errorResponse(serviceContext.getErrorCollection());
                }

                final User crowdUser = getUser(username);

                final Group immutableGroup = new ImmutableGroup(groupName);
                final User immutableUser = ImmutableUser.newUser(crowdUser).toUser();
                if (crowdService.addUserToGroup(immutableUser, immutableGroup)) {
                    final GroupBean responseGroupBean = buildGroupBean(immutableGroup);
                    return Response.status(Response.Status.CREATED)
                            .location(responseGroupBean.getSelf()).entity(responseGroupBean)
                            .cacheControl(never()).build();
                } else {
                    return Response.status(Response.Status.BAD_REQUEST)
                            .entity(i18n.getText("rest.group.user.already.exists.in.group", username, groupName))
                            .cacheControl(never()).build();
                }
            }
        });
    }

    /**
     * Removes given user from a group.
     * <p>
     * Returns no content
     *
     * @param groupName A name of requested group.
     * @param username  User to remove from a group
     * @return REST representation of a group
     * @response.representation.200.qname group
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc If the user was removed from the group.
     * @response.representation.400.doc Returned if user requested an empty group name
     * @response.representation.401.doc Returned if the current user is not authenticated.
     * @response.representation.403.doc Returned if the current user does not have administrator permissions.
     * @response.representation.404.doc Returned if the requested group was not found or the requested user wan not found
     * @since 6.1
     */
    @WebSudoRequired
    @DELETE
    @Path("user")
    public Response removeUserFromGroup(@QueryParam("groupname") final String groupName, @QueryParam("username") final String username) {
        return doGroupUpdate(new GroupUpdateCommand() {
            @Override
            public Response execute() throws OperationNotPermittedException {
                ensureGroupExists(groupName);

                // Get user before group op. validation to get 404 code if user does not exist
                final User crowdUser = getUser(username);

                final JiraServiceContextImpl serviceContext = new JiraServiceContextImpl(authContext.getLoggedInUser());
                if (!groupService.validateRemoveUserFromGroups(serviceContext, ImmutableList.of(groupName), username)) {
                    return responses.errorResponse(serviceContext.getErrorCollection());
                }


                final ImmutableGroup immutableGroup = new ImmutableGroup(groupName);
                final User immutableUser = ImmutableUser.newUser(crowdUser).toUser();
                crowdService.removeUserFromGroup(immutableUser, immutableGroup);
                return Response.ok().cacheControl(never()).build();
            }
        });
    }

    private Response doGroupUpdate(final GroupUpdateCommand command) {
        ensureCanManageGroups();

        try {
            return command.execute();
        } catch (OperationNotPermittedException e) {
            throw new ForbiddenWebException(ErrorCollection.of(i18n.getText("generic.error", e.getLocalizedMessage())));
        } catch (InvalidGroupException e) {
            throw new BadRequestWebException(ErrorCollection.of(i18n.getText("generic.error", e.getLocalizedMessage())));
        } catch (OperationFailedException e) {
            throw new ServerErrorWebException(ErrorCollection.of(i18n.getText("generic.error", e.getLocalizedMessage())));
        }
    }

    /**
     * Ensure the group exists
     *
     * @param groupName the group name
     * @return the group
     * @throws BadRequestWebException if a group name is not provided
     * @throws NotFoundWebException   if the group does not exist
     */
    private Group ensureGroupExists(final String groupName) {
        validateGroupName(groupName);
        final Group group = groupManager.getGroup(groupName.trim());
        if (group == null) {
            throw new NotFoundWebException(ErrorCollection.of(i18n.getText("rest.group.error.not.found", groupName)));
        }
        return group;
    }

    private User getUser(final String username) {
        User crowdUser = null;
        if (username != null) {
            crowdUser = crowdService.getUser(username);
            if (crowdUser == null) {
                throw new NotFoundWebException(ErrorCollection.of(i18n.getText("admin.errors.user.does.not.exist", username)));
            }
        }
        return crowdUser;
    }

    private void ensureCanManageGroups() {
        final ApplicationUser remoteUser = authContext.getLoggedInUser();
        if (!(hasGroupManagementPermission(remoteUser))) {
            throw new ForbiddenWebException(ErrorCollection.of(i18n.getText("rest.authorization.admin.required")));
        }
    }

    private void validateGroupName(final String groupName) {
        if (StringUtils.isEmpty(groupName)) {
            throw new BadRequestWebException(ErrorCollection.of(i18n.getText("rest.group.error.empty")));
        }
    }

    private GroupBean buildGroupBean(final Group group) {
        // Load the users lazily, only if they are to be expanded
        Supplier<List<ApplicationUser>> userSupplier = () -> ImmutableList.copyOf(filter(groupManager.getUsersInGroup(group), user -> (user != null) && user.isActive()));
        int activeUserCount = groupManager.getUsersInGroupCount(group);
        final UserJsonBeanListWrapper userJsonBeanListWrapper = new UserJsonBeanListWrapper(activeUserCount, userSupplier, MAX_USERS_COUNT, authContext.getLoggedInUser(), userBeanFactory);
        return new GroupBeanBuilder(jiraBaseUrls, group.getName()).users(userJsonBeanListWrapper).build();
    }

    interface GroupUpdateCommand {
        Response execute() throws OperationNotPermittedException, InvalidGroupException;
    }
}
