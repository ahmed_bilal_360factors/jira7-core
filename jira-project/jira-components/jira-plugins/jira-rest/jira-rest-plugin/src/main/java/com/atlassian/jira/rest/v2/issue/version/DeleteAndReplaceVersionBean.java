package com.atlassian.jira.rest.v2.issue.version;

import java.util.List;
import java.util.Optional;

/**
 * REST call parameters.
 *
 * @see com.atlassian.jira.rest.v2.issue.VersionResource#delete(String, DeleteAndReplaceVersionBean)
 * @since v7.0.10
 */
public class DeleteAndReplaceVersionBean {
    private Long moveFixIssuesTo;
    private Long moveAffectedIssuesTo;
    private List<CustomFieldReplacement> customFieldReplacementList;

    /**
     * Version that will replace version in field 'fix version', if empty version will be deleted from field.
     */
    public Long getMoveFixIssuesTo() {
        return moveFixIssuesTo;
    }

    /**
     * Version that will replace version in field 'affected version', if empty version will be deleted from field.
     */
    public Long getMoveAffectedIssuesTo() {
        return moveAffectedIssuesTo;
    }

    /**
     * List of version what will replace version in custom fields. Empty means that version
     * will be removed from all custom fields.
     */
    public List<CustomFieldReplacement> getCustomFieldReplacementList() {
        return customFieldReplacementList;
    }

    /**
     * Information about version replacement in custom field.
     */
    public static class CustomFieldReplacement {
        private Long customFieldId;
        private Long moveTo;

        /**
         * Custom field id.
         */
        public Long getCustomFieldId() {
            return customFieldId;
        }

        /**
         * What version will replace version in given custom field. Empty means
         * that version will be removed.
         */
        public Long getMoveTo() {
            return moveTo;
        }
    }
}
