package com.atlassian.jira.rest.v2.search;

import com.atlassian.jira.issue.fields.rest.json.beans.GroupJsonBean;
import com.atlassian.jira.issue.fields.rest.json.beans.GroupJsonBeanBuilder;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.rest.api.project.ProjectRoleBean;
import com.atlassian.jira.rest.v2.issue.project.ProjectBean;
import com.atlassian.jira.rest.v2.issue.project.ProjectBeanFactory;
import com.atlassian.jira.rest.v2.issue.project.ProjectRoleBeanFactory;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.sharing.SharePermission;
import com.atlassian.jira.sharing.type.ShareType;
import com.atlassian.jira.sharing.type.ShareTypeFactory;
import com.atlassian.jira.sharing.type.ShareTypePermissionChecker;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.function.BiFunction;

import static com.atlassian.jira.sharing.type.ShareType.Name.GLOBAL;
import static com.atlassian.jira.sharing.type.ShareType.Name.AUTHENTICATED;
import static com.atlassian.jira.sharing.type.ShareType.Name.GROUP;
import static com.atlassian.jira.sharing.type.ShareType.Name.PROJECT;
import static com.atlassian.jira.sharing.type.ShareType.Name.PROJECT_UNKNOWN;

@Component
public class FilterPermissionBeanFactory {
    private final ProjectBeanFactory projectBeanFactory;
    private final ProjectManager projectManager;
    private final ProjectRoleManager projectRoleManager;
    private final ProjectRoleBeanFactory projectRoleBeanFactory;
    private final JiraBaseUrls jiraBaseUrls;
    private final ShareTypeFactory shareTypeFactory;

    private final ImmutableMap<ShareType.Name, BiFunction<ApplicationUser, SharePermission, FilterPermissionBean>> mapFunction;

    @Autowired
    public FilterPermissionBeanFactory(final ProjectBeanFactory projectBeanFactory, final ProjectManager projectManager, final ProjectRoleManager projectRoleManager, final ProjectRoleBeanFactory projectRoleBeanFactory, final JiraBaseUrls jiraBaseUrls, ShareTypeFactory shareTypeFactory) {
        this.projectBeanFactory = projectBeanFactory;
        this.projectManager = projectManager;
        this.projectRoleManager = projectRoleManager;
        this.projectRoleBeanFactory = projectRoleBeanFactory;
        this.jiraBaseUrls = jiraBaseUrls;
        this.shareTypeFactory = shareTypeFactory;

        this.mapFunction = ImmutableMap.of(
                GLOBAL, this::fromGlobal,
                AUTHENTICATED, this::fromAuthenticated,
                GROUP, this::fromGroup,
                PROJECT, this::fromProject);
    }

    public FilterPermissionBean buildPermissionBean(ApplicationUser user, SharePermission sharePermission) {
        return mapFunction.get(sharePermission.getType()).apply(user, sharePermission);
    }

    private FilterPermissionBean fromGlobal(ApplicationUser user, SharePermission sharePermission) {
        return new FilterPermissionBean(sharePermission.getId(), GLOBAL.get(), null, null, null);
    }

    private FilterPermissionBean fromAuthenticated(ApplicationUser user, SharePermission sharePermission) {
        return new FilterPermissionBean(sharePermission.getId(), AUTHENTICATED.get(), null, null, null);
    }

    private FilterPermissionBean fromGroup(ApplicationUser user, SharePermission sharePermission) {
        String groupKey = sharePermission.getParam1();
        GroupJsonBean group = new GroupJsonBeanBuilder(jiraBaseUrls).name(groupKey).build();
        return new FilterPermissionBean(sharePermission.getId(), GROUP.get(), null, null, group);
    }

    private FilterPermissionBean fromProject(ApplicationUser applicationUser, SharePermission sharePermission) {
        final ShareTypePermissionChecker permissionChecker = shareTypeFactory.getShareType(sharePermission.getType()).getPermissionsChecker();
        if (!permissionChecker.hasPermission(applicationUser, sharePermission)) {
            return new FilterPermissionBean(sharePermission.getId(), PROJECT_UNKNOWN.get(), null, null, null);
        }
        Long projectId = Long.valueOf(sharePermission.getParam1());
        Project project = projectManager.getProjectObj(projectId);
        ProjectBean projectBean = projectBeanFactory.shortProject(project);

        String roleKey = sharePermission.getParam2();
        if (null != roleKey) {
            Long id = Long.valueOf(roleKey);
            final ProjectRole projectRole = projectRoleManager.getProjectRole(id);
            final ProjectRoleBean projectRoleBean = projectRoleBeanFactory.projectRole(project, projectRole);

            return new FilterPermissionBean(sharePermission.getId(), PROJECT.get(), projectBean, projectRoleBean, null);

        } else {
            return new FilterPermissionBean(sharePermission.getId(), PROJECT.get(), projectBean, null, null);
        }
    }
}
