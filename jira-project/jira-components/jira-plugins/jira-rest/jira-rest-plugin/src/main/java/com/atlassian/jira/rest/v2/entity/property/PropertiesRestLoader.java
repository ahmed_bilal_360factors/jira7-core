package com.atlassian.jira.rest.v2.entity.property;

import com.atlassian.jira.entity.WithId;
import com.atlassian.jira.entity.property.EntityProperty;
import com.atlassian.jira.entity.property.EntityPropertyService;
import com.atlassian.jira.rest.api.property.PropertiesBean;
import com.atlassian.jira.rest.api.util.StringList;
import com.atlassian.jira.rest.v2.issue.IncludedFields;
import com.atlassian.jira.user.ApplicationUser;

import java.util.List;

import static com.atlassian.collectors.CollectorsUtil.toImmutableList;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.stream.Collectors.toMap;

public final class PropertiesRestLoader<T extends WithId> {
    private final EntityPropertyService<T> service;

    public static <T extends WithId> PropertiesRestLoader<T> withService(EntityPropertyService<T> service) {
        return new PropertiesRestLoader<>(service);
    }

    private PropertiesRestLoader(final EntityPropertyService<T> service) {
        this.service = service;
    }

    public PropertiesBean getProperties(ApplicationUser user, T entity, List<StringList> queryParam) {
        if (queryParam == null || queryParam.isEmpty()) {
            return null;
        }

        IncludedFields includedProperties = IncludedFields.nothingIncludedByDefault(queryParam);

        List<EntityProperty> properties = includedProperties.includeAll() ?
                service.getProperties(user, entity.getId()).stream()
                        .filter(prop -> includedProperties.included(prop.getKey()))
                        .collect(toImmutableList()) :
                service.getProperties(user, entity.getId(), newArrayList(includedProperties.getIncluded()));

        return PropertiesBean.of(properties.stream().collect(toMap(EntityProperty::getKey, EntityProperty::getValue)));
    }
}
