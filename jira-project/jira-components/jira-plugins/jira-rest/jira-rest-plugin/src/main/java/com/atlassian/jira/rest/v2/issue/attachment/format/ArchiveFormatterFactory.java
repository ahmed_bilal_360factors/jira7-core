package com.atlassian.jira.rest.v2.issue.attachment.format;

import com.atlassian.jira.plugin.attachment.AttachmentArchive;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import org.springframework.stereotype.Component;

/**
 * Creates {@link com.atlassian.jira.rest.v2.issue.attachment.format.ArchiveFormatter} instances.
 *
 * @since v6.4
 */
@ExportAsService
@Component
public class ArchiveFormatterFactory {
    @SuppressWarnings("unused")
    private ArchiveFormatterFactory() {

    }

    public ArchiveFormatter<HumanReadableArchive> human() {
        return new ArchiveHumanFormatter();
    }

    public ArchiveFormatter<AttachmentArchive> raw() {
        return new ArchiveRawFormatter();
    }
}
