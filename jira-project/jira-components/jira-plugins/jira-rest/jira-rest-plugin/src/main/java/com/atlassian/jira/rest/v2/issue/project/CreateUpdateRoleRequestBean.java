package com.atlassian.jira.rest.v2.issue.project;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

public class CreateUpdateRoleRequestBean {
    private final String name;
    private final String description;

    @JsonCreator
    public CreateUpdateRoleRequestBean(@JsonProperty("name") String name, @JsonProperty("description") String description) {
        this.name = name;
        this.description = description;
    }

    @JsonProperty
    public String getName() {
        return name;
    }

    @JsonProperty
    public String getDescription() {
        return description;
    }

    public static final CreateUpdateRoleRequestBean DOC_EXAMPLE;

    static {
        DOC_EXAMPLE = new CreateUpdateRoleRequestBean("MyRole", "role description");
    }
}
