package com.atlassian.jira.rest.util;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.sharing.SharePermission;
import com.atlassian.jira.sharing.SharedEntity;
import com.atlassian.jira.sharing.type.ShareType;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.collect.ImmutableSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class FilterPermissionHelper {

    private final SearchRequestService searchRequestService;

    @Autowired
    public FilterPermissionHelper(SearchRequestService searchRequestService) {
        this.searchRequestService = searchRequestService;
    }

    public ServiceOutcome<SearchRequest> addSharePermissionToSavedFilter(ApplicationUser user, SearchRequest filter, SharePermission sharePermission) {
        Set<SharePermission> shares =  sharePermission.getType().equals(ShareType.Name.GLOBAL) || sharePermission.getType().equals(ShareType.Name.AUTHENTICATED) ?
                Collections.singleton(sharePermission) :
                ImmutableSet.<SharePermission>builder().addAll(filter.getPermissions().getPermissionSet()).add(sharePermission).build();

        return updatePermissions(user, filter, shares);
    }

    public ServiceOutcome<SearchRequest> removeSharePermissionFromSavedFilter(ApplicationUser user, SearchRequest filter, Long permissionId) {
        Set<SharePermission> newPermissionSet = filter.getPermissions().getPermissionSet()
                .stream()
                .filter((sharePermission) -> !sharePermission.getId().equals(permissionId))
                .collect(Collectors.toSet());

        return updatePermissions(user, filter, newPermissionSet);
    }

    private ServiceOutcome<SearchRequest> updatePermissions(ApplicationUser user, SearchRequest filter, Set<SharePermission> permissions) {
        JiraServiceContext context = new JiraServiceContextImpl(user);
        SearchRequest filterCopy = new SearchRequest(filter);

        filterCopy.setPermissions(new SharedEntity.SharePermissions(permissions));

        SearchRequest returnedValue = searchRequestService.updateFilter(context, filterCopy);
        if (context.getErrorCollection().hasAnyErrors()) {
            return ServiceOutcomeImpl.from(context.getErrorCollection());
        }
        return ServiceOutcomeImpl.ok(returnedValue);
    }
}
