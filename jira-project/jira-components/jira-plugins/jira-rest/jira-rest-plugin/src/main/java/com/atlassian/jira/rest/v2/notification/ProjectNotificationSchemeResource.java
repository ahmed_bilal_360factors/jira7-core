package com.atlassian.jira.rest.v2.notification;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.notification.NotificationScheme;
import com.atlassian.jira.notification.NotificationSchemeService;
import com.atlassian.jira.rest.api.notification.NotificationSchemeBean;
import com.atlassian.jira.rest.api.notification.NotificationSchemeBeanFactory;
import com.atlassian.jira.rest.api.notification.NotificationSchemeExpandParam;
import com.atlassian.jira.rest.util.ResponseFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.rest.annotation.ResponseType;
import com.google.common.base.Function;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Resource for associating notification schemes and projects.
 *
 * @since 6.5
 */
@Path("project/{projectKeyOrId}/notificationscheme")
@Produces({MediaType.APPLICATION_JSON})
public class ProjectNotificationSchemeResource {
    private final ResponseFactory responseFactory;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final NotificationSchemeService notificationSchemeService;
    private final NotificationSchemeBeanFactory notificationSchemeBeanFactory;

    public ProjectNotificationSchemeResource(final ResponseFactory responseFactory,
                                             final JiraAuthenticationContext jiraAuthenticationContext,
                                             final NotificationSchemeService notificationSchemeService,
                                             final NotificationSchemeBeanFactory notificationSchemeBeanFactory) {
        this.responseFactory = responseFactory;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.notificationSchemeService = notificationSchemeService;
        this.notificationSchemeBeanFactory = notificationSchemeBeanFactory;
    }

    /**
     * Gets a notification scheme associated with the project.
     * Follow the documentation of /notificationscheme/{id} resource for all details about returned value.
     *
     * @param projectKeyOrId key or id of the project
     * @param expand:        optional information to be expanded in the response: group, user, projectRole or field
     * @return The associated notification scheme if successful, appropriate error otherwise.
     * @response.representation.200.qname notificationScheme
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc Returned if the notification scheme exists and is visible for the calling user
     * @response.representation.200.example {@link NotificationSchemeBeanExample#DOC_EXAMPLE}
     * @response.representation.401.doc Returned if the user is not logged in.
     * @response.representation.403.doc Returned if the user is allowed to access the project, but is not an administrator of the project or JIRA and therefore
     * can't see the notification scheme of the project.
     * @response.representation.404.doc Returned if the notification scheme does not exist, or is not visible to the calling user
     */
    @GET
    @ResponseType(NotificationSchemeBean.class)
    public Response getNotificationScheme(@PathParam("projectKeyOrId") final String projectKeyOrId,
                                          @QueryParam("expand") final String expand) {
        return responseFactory.toResponse(notificationSchemeBeanFactory.parseExpandQuery(expand)).left().on(new Function<List<NotificationSchemeExpandParam>, Response>() {
            @Override
            public Response apply(final List<NotificationSchemeExpandParam> expands) {
                final ApplicationUser user = jiraAuthenticationContext.getUser();

                final ServiceOutcome<NotificationScheme> schemeForProject = getNotificationSchemeForProject(user, projectKeyOrId);

                return responseFactory.validateOutcome(schemeForProject).left().on(new Function<NotificationScheme, Response>() {
                    @Override
                    public Response apply(final NotificationScheme notificationScheme) {
                        return responseFactory.okNoCache(notificationSchemeBeanFactory.createNotificationSchemeBean(notificationScheme, expands));
                    }
                });

            }
        });
    }

    private ServiceOutcome<NotificationScheme> getNotificationSchemeForProject(final ApplicationUser user, final String projectKeyOrId) {
        try {
            return notificationSchemeService.getNotificationSchemeForProject(user, Long.parseLong(projectKeyOrId));
        } catch (NumberFormatException e) {
            return notificationSchemeService.getNotificationSchemeForProject(user, projectKeyOrId);
        }
    }
}
