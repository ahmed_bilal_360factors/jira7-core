package com.atlassian.jira.rest.v2.issue;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.crowd.embedded.impl.ImmutableUser;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.bc.security.login.LoginReason;
import com.atlassian.jira.bc.security.login.LoginResult;
import com.atlassian.jira.bc.security.login.LoginService;
import com.atlassian.jira.event.user.UserProfileUpdatedEvent;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.plugin.user.PasswordPolicyManager;
import com.atlassian.jira.plugin.user.WebErrorMessage;
import com.atlassian.jira.rest.api.util.ErrorCollection;
import com.atlassian.jira.rest.exception.BadRequestWebException;
import com.atlassian.jira.rest.exception.ForbiddenWebException;
import com.atlassian.jira.rest.exception.NotAuthorisedWebException;
import com.atlassian.jira.rest.exception.NotFoundWebException;
import com.atlassian.jira.rest.exception.ServerErrorWebException;
import com.atlassian.jira.rest.v2.admin.applicationrole.ApplicationRoleBeanConverter;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.DelegatingApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.EmailFormatter;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.rest.annotation.RequestType;
import com.atlassian.rest.annotation.ResponseType;
import com.google.common.collect.ImmutableList;
import com.opensymphony.util.TextUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collection;

import static com.atlassian.jira.rest.api.http.CacheControl.never;


/**
 * Currently logged user resource
 *
 * @since 6.1
 */
@Path("myself")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@ExperimentalApi
public class CurrentUserResource {
    private static final int MAX_LENGTH = 255;
    private final Logger log = LoggerFactory.getLogger(CurrentUserResource.class);

    private final UserUtil userUtil;
    private final UserManager userManager;
    private final PasswordPolicyManager passwordPolicyManager;
    private final EventPublisher eventPublisher;
    private final I18nHelper i18n;
    private final EmailFormatter emailFormatter;
    private final JiraAuthenticationContext authContext;
    private final TimeZoneManager timeZoneManager;
    private final AvatarService avatarService;
    private final JiraBaseUrls jiraBaseUrls;
    private final LoginService loginService;
    private final I18nHelper.BeanFactory beanFactory;
    private final ApplicationRoleManager applicationRoleManager;
    private final ApplicationRoleBeanConverter applicationRoleBeanConverter;

    public CurrentUserResource(final UserUtil userUtil, final UserManager userManager,
                               final PasswordPolicyManager passwordPolicyManager, final EventPublisher eventPublisher,
                               final I18nHelper i18n, final EmailFormatter emailFormatter, final JiraAuthenticationContext authContext,
                               final TimeZoneManager timeZoneManager, final AvatarService avatarService, final JiraBaseUrls jiraBaseUrls,
                               final LoginService loginService, final I18nHelper.BeanFactory beanFactory,
                               final ApplicationRoleBeanConverter applicationRoleBeanConverter, final ApplicationRoleManager applicationRoleManager
    ) {
        this.userManager = userManager;
        this.passwordPolicyManager = passwordPolicyManager;
        this.eventPublisher = eventPublisher;
        this.jiraBaseUrls = jiraBaseUrls;
        this.userUtil = userUtil;
        this.i18n = i18n;
        this.emailFormatter = emailFormatter;
        this.authContext = authContext;
        this.timeZoneManager = timeZoneManager;
        this.avatarService = avatarService;
        this.beanFactory = beanFactory;
        this.loginService = loginService;
        this.applicationRoleManager = applicationRoleManager;
        this.applicationRoleBeanConverter = applicationRoleBeanConverter;
    }

    /**
     * Returns currently logged user. This resource cannot be accessed anonymously.
     *
     * @response.representation.200.qname user
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc Returns a full representation of a JIRA user in JSON format.
     * @response.representation.200.example {@link UserBean#DOC_EXAMPLE}
     * @response.representation.401.doc Returned if the current user is not authenticated.
     */
    @GET
    @ResponseType(UserBean.class)
    public Response getUser() {
        final ApplicationUser currentUser = authContext.getUser();
        if (currentUser == null) {
            throw new NotAuthorisedWebException(ErrorCollection.of(i18n.getText("rest.authentication.no.user.logged.in")));
        }

        return createUserResponse(currentUser);
    }

    /**
     * Modify currently logged user. The "value" fields present will override the existing value.
     * Fields skipped in request will not be changed. Only email and display name can be change that way.
     * Requires user password.
     *
     * @return a user
     * @request.representation.mediaType application/json
     * @response.representation.200.mediaType application/json
     * @response.representation.200.qname user
     * @response.representation.200.doc Returned if the user exists and the caller has permission to edit it.
     * @request.representation.example {@link UserWriteBean#DOC_EXAMPLE_UPDATE_MYSELF}
     * @response.representation.200.example {@link UserWriteBean#DOC_EXAMPLE_UPDATED_MYSELF}
     * @response.representation.400.doc Returned if the request is invalid including incorrect password.
     * @response.representation.401.doc Returned if the user is not authenticated.
     * @response.representation.403.doc Returned if the directory is read-only.
     * @response.representation.404.doc Returned if the the user could not be found.
     */
    @PUT
    @RequestType(UserWriteBean.class)
    @ResponseType(UserBean.class)
    public Response updateUser(final UserWriteBean userBean) {
        final ApplicationUser currentUser = authContext.getUser();
        if (currentUser == null) {
            throw new NotAuthorisedWebException(ErrorCollection.of(i18n.getText("rest.authentication.no.user.logged.in")));
        }
        final String password = userBean.getPassword();

        if (StringUtils.isBlank(password)) {
            throw new BadRequestWebException(ErrorCollection.of(i18n.getText("rest.myself.error.password.cannot.be.empty")));
        }

        if (StringUtils.isBlank(userBean.getEmailAddress()) &&
                StringUtils.isBlank(userBean.getDisplayName())) {
            throw new BadRequestWebException(ErrorCollection.of(i18n.getText("rest.myself.error.no.value.found.to.be.changed")));
        }

        if (StringUtils.length(userBean.getDisplayName()) > MAX_LENGTH) {
            throw new BadRequestWebException(ErrorCollection.of(i18n.getText("rest.myself.error.field.too.long", "displayName", Integer.toString(MAX_LENGTH))));
        }

        if (StringUtils.length(userBean.getEmailAddress()) > MAX_LENGTH) {
            throw new BadRequestWebException(ErrorCollection.of(i18n.getText("rest.myself.error.field.too.long", "emailAddress", Integer.toString(MAX_LENGTH))));
        }

        if (StringUtils.isNotBlank(userBean.getEmailAddress()) &&
                !TextUtils.verifyEmail(userBean.getEmailAddress())) {
            throw new BadRequestWebException(ErrorCollection.of(i18n.getText("admin.errors.invalid.email")));
        }

        // Is the directory writable?
        if (!userManager.canUpdateUser(currentUser)) {
            throw new ForbiddenWebException(ErrorCollection.of(i18n.getText("admin.errors.cannot.edit.user.directory.read.only")));
        }
        checkAuthentication(currentUser, password);

        final ImmutableUser.Builder userBuilder = ImmutableUser.newUser(currentUser.getDirectoryUser());
        userBuilder.emailAddress(StringUtils.defaultIfBlank(userBean.getEmailAddress(), currentUser.getEmailAddress()));
        userBuilder.displayName(StringUtils.defaultIfBlank(userBean.getDisplayName(), currentUser.getDisplayName()));

        userManager.updateUser(new DelegatingApplicationUser(currentUser.getId(), currentUser.getKey(), userBuilder.toUser()));

        final String key = currentUser.getKey();
        final ApplicationUser changedUser = userUtil.getUserByKey(key);
        if (changedUser == null) {
            throw new NotFoundWebException(ErrorCollection.of(i18n.getText("rest.user.error.not.found.with.key", key)));
        }

        eventPublisher.publish(new UserProfileUpdatedEvent(changedUser, changedUser));
        return createUserResponse(changedUser);
    }

    /**
     * Checks if provided password is valid for given user.
     *
     * @param user     user
     * @param password password
     */
    private void checkAuthentication(final ApplicationUser user, final String password) {
        switch (getLoginResultReason(user, password)) {
            case OK:
                break;
            case AUTHENTICATION_DENIED:
                // the login service will not even acquire information whether the login is correct or not,
                // if the elevated security check is triggered.
                throw new NotAuthorisedWebException(ErrorCollection.of(i18n.getText("changepassword.elevated.authorisation.required")));

            case AUTHORISATION_FAILED:
            case AUTHENTICATED_FAILED:

                throw new BadRequestWebException(ErrorCollection.of(i18n.getText("changepassword.could.not.find.user")));
        }
    }

    private LoginReason getLoginResultReason(final ApplicationUser user, final String password) {
        final LoginResult loginResult;
        try {
            loginResult = loginService.authenticate(user, password);
        } catch (Exception e) {
            log.debug("An error occurred while authenticating user {}: {}", user.getName(), e.getMessage());
            throw new ServerErrorWebException(ErrorCollection.of(i18n.getText("rest.error.internal")));
        }

        if (loginResult == null) {
            log.debug("An error occurred while authenticating user {}: Could not authenticate user.", user.getName());
            throw new ServerErrorWebException(ErrorCollection.of(i18n.getText("rest.error.internal")));
        }

        final LoginReason reason = loginResult.getReason();
        if (reason == null) {
            log.debug("An error occurred while authenticating user {}: Missing authorisation operation reason.", user.getName());
            throw new ServerErrorWebException(ErrorCollection.of(i18n.getText("rest.error.internal")));
        }

        return reason;
    }


    /**
     * Modify caller password.
     *
     * @request.representation.mediaType application/json
     * @request.representation.example {@link PasswordBean#OLD_PASSWORD_DOC_EXAMPLE}
     * @response.representation.204.doc Returned if the user exists and the caller has permission to edit it.
     * @response.representation.400.doc Returned if new password is incorrect/missing or current password is incorrect.
     * @response.representation.401.doc Returned if the user is not authenticated.
     * @response.representation.403.doc Returned if the directory is read-only.
     * @response.representation.404.doc Returned if the caller does have permission to change user password but the user does not exist.
     */
    @PUT
    @Path("password")
    @ResponseType(Void.class)
    public Response changeMyPassword(final PasswordBean passwordBean) {
        final ApplicationUser currentUser = authContext.getUser();
        if (currentUser == null) {
            throw new NotAuthorisedWebException(ErrorCollection.of(i18n.getText("rest.authentication.no.user.logged.in")));
        }

        final String password = passwordBean.getPassword();
        final String currentPassword = passwordBean.getCurrentPassword();

        if (StringUtils.isBlank(password)) {
            throw new BadRequestWebException(ErrorCollection.of(i18n.getText("changepassword.new.password.required")));
        }

        final Collection<WebErrorMessage> messages = passwordPolicyManager.checkPolicy(currentUser, currentPassword, password);
        if (!messages.isEmpty()) {
            throw new BadRequestWebException(ErrorCollection.of(i18n.getText("changepassword.new.password.rejected")));
        }

        checkAuthentication(currentUser, currentPassword);

        try {
            userUtil.changePassword(currentUser, password);
        } catch (UserNotFoundException e) {
            throw new BadRequestWebException(ErrorCollection.of(i18n.getText("changepassword.could.not.find.user")));
        } catch (InvalidCredentialException e) {
            throw new BadRequestWebException(ErrorCollection.of(i18n.getText("changepassword.new.password.rejected")));
        } catch (OperationNotPermittedException e) {
            throw new ForbiddenWebException(ErrorCollection.of(i18n.getText("admin.errors.cannot.edit.user.directory.read.only")));
        } catch (PermissionException e) {
            throw new ForbiddenWebException(ErrorCollection.of(i18n.getText("admin.errors.cannot.edit.user.directory.read.only")));
        }

        return Response.noContent().cacheControl(never()).build();
    }

    private Response createUserResponse(final ApplicationUser currentUser) {
        final UserBeanBuilder builder = new UserBeanBuilder(jiraBaseUrls).user(currentUser)
                .groups(ImmutableList.copyOf(userUtil.getGroupNamesForUser(currentUser.getUsername())))
                .loggedInUser(currentUser)
                .emailFormatter(emailFormatter)
                .timeZone(timeZoneManager.getTimeZoneforUser(currentUser))
                .avatarService(avatarService)
                .i18nBeanFactory(beanFactory)
                .applicationRoles(applicationRoleManager.getRolesForUser(currentUser));

        return Response.ok(builder.buildFull(applicationRoleBeanConverter)).cacheControl(never()).build();
    }
}
