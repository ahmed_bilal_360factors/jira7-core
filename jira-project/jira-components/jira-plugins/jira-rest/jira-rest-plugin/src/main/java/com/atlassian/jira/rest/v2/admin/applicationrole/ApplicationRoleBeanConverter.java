package com.atlassian.jira.rest.v2.admin.applicationrole;

import com.atlassian.application.api.Application;
import com.atlassian.application.api.ApplicationKey;
import com.atlassian.application.api.ApplicationManager;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.base.Function;
import com.google.common.base.Supplier;
import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Set;

import static com.atlassian.extras.common.LicensePropertiesConstants.UNLIMITED_USERS;

/**
 * @since v7.0
 */
@Component
public class ApplicationRoleBeanConverter implements Function<ApplicationRole, ApplicationRoleBean> {
    private final ApplicationRoleManager roleManager;
    private final ApplicationManager appManager;
    private final Supplier<Application> platform = new Supplier<Application>() {
        @Override
        public Application get() {
            return appManager.getPlatform();
        }
    };

    @Autowired
    public ApplicationRoleBeanConverter(@ComponentImport final ApplicationRoleManager roleManager,
                                        @ComponentImport final ApplicationManager appManager) {
        this.roleManager = roleManager;
        this.appManager = appManager;
    }

    public ApplicationRoleBean roleBean(ApplicationRole role) {
        final ApplicationKey key = role.getKey();
        final int userCount = roleManager.getUserCount(key);
        final Application application = appManager.getApplication(key).getOrElse(platform);
        final String userCountDescription = application.getUserCountDescription(Option.some(userCount));
        final int remainingSeats = roleManager.getRemainingSeats(key);
        return new ApplicationRoleBean(
                key.value(),
                role.getName(),
                toNames(role.getGroups()),
                toNames(role.getDefaultGroups()),
                role.isSelectedByDefault(),
                role.isDefined(),
                role.getNumberOfSeats(),
                remainingSeats,
                userCount,
                userCountDescription,
                remainingSeats == UNLIMITED_USERS,
                role.isPlatform());
    }

    public ApplicationRoleBean shortBean(ApplicationRole role) {
        return new ApplicationRoleBean(role.getKey().value(), role.getName(), null, null, null);
    }

    public Function<ApplicationRole, ApplicationRoleBean> toShortBean() {
        return new Function<ApplicationRole, ApplicationRoleBean>() {
            @Override
            public ApplicationRoleBean apply(final ApplicationRole input) {
                return shortBean(input);
            }
        };
    }

    private static Set<String> toNames(Iterable<Group> groups) {
        Set<String> names = Sets.newHashSet();
        for (Group group : groups) {
            names.add(group.getName());
        }
        return Collections.unmodifiableSet(names);
    }

    @Override
    public ApplicationRoleBean apply(final ApplicationRole input) {
        return roleBean(input);
    }
}
