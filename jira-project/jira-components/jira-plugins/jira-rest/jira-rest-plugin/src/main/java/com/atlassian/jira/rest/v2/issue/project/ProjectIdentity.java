package com.atlassian.jira.rest.v2.issue.project;

import com.google.common.base.Objects;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.validation.constraints.NotNull;
import java.net.URI;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Class containing just the project identity, i.e. ID, key and link to self
 *
 * @since v7.0
 */
public class ProjectIdentity {
    public static final ProjectIdentity DOC_EXAMPLE = new ProjectIdentity(URI.create("http://example/jira/rest/api/2/project/10042"), 10010L, "EX");

    @JsonProperty
    private URI self;
    @JsonProperty
    private Long id;
    @JsonProperty
    private String key;

    public ProjectIdentity() {
    }

    public ProjectIdentity(@NotNull final URI self, @NotNull final Long id, @NotNull final String key) {
        this.self = checkNotNull(self);
        this.id = checkNotNull(id);
        this.key = checkNotNull(key);
    }

    public URI getSelf() {
        return self;
    }

    public Long getId() {
        return id;
    }

    public String getKey() {
        return key;
    }


    @Override
    public int hashCode() {
        return Objects.hashCode(self, id, key);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final ProjectIdentity other = (ProjectIdentity) obj;
        return Objects.equal(this.self, other.self) && Objects.equal(this.id, other.id) && Objects.equal(this.key, other.key);
    }

    @Override
    public String toString() {
        return "ProjectIdentity{" +
                "self=" + self +
                ", id=" + id +
                ", key='" + key + '\'' +
                '}';
    }
}
