package com.atlassian.jira.rest.v2.admin.permissionscheme;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.fugue.Option;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.rest.json.UserBeanFactory;
import com.atlassian.jira.issue.fields.rest.json.beans.GroupJsonBean;
import com.atlassian.jira.issue.fields.rest.json.beans.GroupJsonBeanBuilder;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.fields.rest.json.beans.UserJsonBean;
import com.atlassian.jira.issue.search.managers.SearchHandlerManager;
import com.atlassian.jira.permission.JiraPermissionHolderType;
import com.atlassian.jira.permission.PermissionHolderType;
import com.atlassian.jira.rest.api.field.FieldBean;
import com.atlassian.jira.rest.api.permission.PermissionGrantBeanExpander;
import com.atlassian.jira.rest.api.permission.PermissionHolderBean;
import com.atlassian.jira.rest.api.permission.PermissionSchemeExpandParam;
import com.atlassian.jira.rest.api.project.ProjectRoleBean;
import com.atlassian.jira.rest.api.util.ExpandParameterParser;
import com.atlassian.jira.rest.v2.issue.project.ProjectRoleBeanFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;
import static com.atlassian.jira.permission.JiraPermissionHolderType.GROUP;
import static com.atlassian.jira.permission.JiraPermissionHolderType.GROUP_CUSTOM_FIELD;
import static com.atlassian.jira.permission.JiraPermissionHolderType.PROJECT_ROLE;
import static com.atlassian.jira.permission.JiraPermissionHolderType.USER;
import static com.atlassian.jira.permission.JiraPermissionHolderType.USER_CUSTOM_FIELD;

@ExportAsService
@Component
public final class PermissionGrantBeanExpanderImpl extends ExpandParameterParser<PermissionSchemeExpandParam> implements PermissionGrantBeanExpander {
    private static final Splitter SPLITTER = Splitter.on(',').omitEmptyStrings().trimResults();

    private interface Expander {
        PermissionHolderBean.Builder expand(String parameter, PermissionHolderBean.Builder holderBean);
    }

    private final Map<JiraPermissionHolderType, Expander> expanders;

    @Autowired
    public PermissionGrantBeanExpanderImpl(
            final JiraAuthenticationContext authenticationContext,
            final JiraBaseUrls jiraBaseUrls,
            final I18nHelper i18n,
            final UserBeanFactory userBeanFactory,
            final UserManager userManager,
            final ProjectRoleBeanFactory projectRoleBeanFactory,
            final GroupManager groupManager,
            final ProjectRoleManager projectRoleManager,
            final CustomFieldManager customFieldManager,
            final FieldManager fieldManager,
            final SearchHandlerManager searchHandlerManager) {
        super(i18n, PermissionSchemeExpandParam.class);

        final GroupJsonBeanBuilder groupJsonBeanBuilder = new GroupJsonBeanBuilder(jiraBaseUrls);

        final Expander customFieldExpander = new Expander() {
            @Override
            public PermissionHolderBean.Builder expand(final String parameter, final PermissionHolderBean.Builder holderBean) {
                CustomField customField = customFieldManager.getCustomFieldObject(parameter);
                FieldBean customFieldBean = FieldBean.shortBean(customField, fieldManager, searchHandlerManager);
                return holderBean.setField(customFieldBean);
            }
        };

        expanders = ImmutableMap.of(
                USER, new Expander() {
                    @Override
                    public PermissionHolderBean.Builder expand(final String parameter, final PermissionHolderBean.Builder holderBean) {
                        ApplicationUser user = userManager.getUserByKey(parameter);
                        UserJsonBean userBean = userBeanFactory.createBean(user, authenticationContext.getUser());
                        return holderBean.setUser(userBean);
                    }
                },
                PROJECT_ROLE, new Expander() {
                    @Override
                    public PermissionHolderBean.Builder expand(final String parameter, final PermissionHolderBean.Builder holderBean) {
                        ProjectRole projectRole = projectRoleManager.getProjectRole(Long.valueOf(parameter));
                        ProjectRoleBean projectRoleBean = projectRoleBeanFactory.shortProjectRole(projectRole);
                        return holderBean.setProjectRole(projectRoleBean);
                    }
                },
                GROUP, new Expander() {
                    @Override
                    public PermissionHolderBean.Builder expand(final String parameter, final PermissionHolderBean.Builder holderBean) {
                        Group group = groupManager.getGroup(parameter);
                        GroupJsonBean groupJsonBean = groupJsonBeanBuilder.group(group).build();
                        return holderBean.setGroup(groupJsonBean);
                    }
                },
                GROUP_CUSTOM_FIELD, customFieldExpander,
                USER_CUSTOM_FIELD, customFieldExpander);
    }

    public PermissionHolderBean expand(PermissionHolderBean grantBean, PermissionHolderType holderType, final List<PermissionSchemeExpandParam> expands) {
        Option<JiraPermissionHolderType> jiraHolderType = JiraPermissionHolderType.fromKey(holderType.getKey(), grantBean.getParameter());
        if (jiraHolderType.isDefined()) {
            Option<Expander> expander = getExpander(jiraHolderType.get(), expands);
            if (expander.isDefined()) {
                return expander.get().expand(grantBean.getParameter(), PermissionHolderBean.builder(grantBean)).build();
            }
        }
        return grantBean;
    }

    private Option<Expander> getExpander(JiraPermissionHolderType permissionHolderType, List<PermissionSchemeExpandParam> expands) {
        for (PermissionSchemeExpandParam expand : expands) {
            if (expand.expandsType(permissionHolderType) && expanders.containsKey(permissionHolderType)) {
                return some(expanders.get(permissionHolderType));
            }
        }

        return none();
    }
}
