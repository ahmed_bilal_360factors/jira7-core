package com.atlassian.jira.rest.v2.issue.users;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Used to give the list of possible involvements to the front-end, along with an i18n key for displaying.
 *
 * @see UserIssueRelevanceBean
 * @since v7.2
 */
@XmlRootElement(name = "user")
public class IssueInvolvementBean {
    @XmlElement
    private String id;

    @XmlElement
    private String label;

    IssueInvolvementBean() {}

    public IssueInvolvementBean(final String id, final String label) {
        this.id = id;
        this.label = label;
    }

    public static final IssueInvolvementBean DOC_EXAMPLE = new IssueInvolvementBean();

    static {
        DOC_EXAMPLE.id = "assignee";
        DOC_EXAMPLE.label = "Assignee";
    }

    public String getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }
}

