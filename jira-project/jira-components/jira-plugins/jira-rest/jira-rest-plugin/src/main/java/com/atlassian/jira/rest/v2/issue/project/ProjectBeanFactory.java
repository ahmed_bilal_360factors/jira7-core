package com.atlassian.jira.rest.v2.issue.project;

import com.atlassian.jira.project.Project;
import com.google.common.base.Function;

import javax.annotation.Nonnull;

/**
 * Class for creating project from projects.
 *
 * @since v4.4
 */
public interface ProjectBeanFactory {
    ProjectBean fullProject(@Nonnull Project project, @Nonnull String expand);

    ProjectBean shortProject(@Nonnull Project project);

    Function<Project, ProjectBean> summaryProject(String expand);

    /**
     * Returns just the identity of the project, i.e. an object containing its ID, ket and link to self.
     *
     * @param project project which identity we want to create
     * @return project identity (an instance of {@link com.atlassian.jira.rest.v2.issue.project.ProjectIdentity})
     * @throws java.lang.NullPointerException when project id or key is null
     */
    ProjectIdentity projectIdentity(@Nonnull Project project);
}
