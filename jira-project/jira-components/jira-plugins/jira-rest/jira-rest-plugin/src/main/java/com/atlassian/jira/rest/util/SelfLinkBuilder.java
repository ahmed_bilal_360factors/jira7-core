package com.atlassian.jira.rest.util;

import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Component
public final class SelfLinkBuilder {
    public final class SelfLink {
        private final List<String> paths;
        private final Map<String, String> queryParams;

        private SelfLink(final List<String> path, Map<String, String> queryParams) {
            this.paths = path;
            this.queryParams = queryParams;
        }

        public SelfLink path(String pathSegment, String... pathSegments) {
            return new SelfLink(ImmutableList.<String>builder().addAll(paths).add(pathSegment).add(pathSegments).build(), queryParams);
        }

        public SelfLink queryParam(String key, String value) {
            return new SelfLink(paths,
                    ImmutableMap.<String, String>builder().putAll(queryParams).put(key, value).build());
        }

        public URI toUri() {
            UriBuilder builder = UriBuilder.fromPath(jiraBaseUrls.restApi2BaseUrl());
            for (String path : paths) {
                builder.path(path);
            }
            for (Map.Entry<String, String> queryParam : queryParams.entrySet()) {
                builder.queryParam(queryParam.getKey(), queryParam.getValue());
            }
            return builder.build();
        }

        public String toString() {
            return toUri().toString();
        }
    }

    private final JiraBaseUrls jiraBaseUrls;

    @Autowired
    public SelfLinkBuilder(final JiraBaseUrls jiraBaseUrls) {
        this.jiraBaseUrls = jiraBaseUrls;
    }

    public SelfLink path(String path) {
        return new SelfLink(Collections.singletonList(path), Collections.<String, String>emptyMap());
    }
}
