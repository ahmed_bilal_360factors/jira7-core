package com.atlassian.jira.rest.v2.admin.applicationrole;

import com.atlassian.jira.application.ApplicationRole;

import javax.annotation.Nonnull;
import java.util.Collection;

/**
 * Version hash generator for a collection of {@link ApplicationRole}
 *
 * @since v7.0
 */
public interface ApplicationRoleHasher {
    /**
     * Get the hash version based on the state of the passed application roles
     *
     * @return a hash indicating a version of the data stored
     */
    @Nonnull
    String getVersionHash(@Nonnull Collection<ApplicationRole> applicationRoles);
}
