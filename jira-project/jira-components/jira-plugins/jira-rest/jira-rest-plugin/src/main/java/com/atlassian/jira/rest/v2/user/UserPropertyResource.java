package com.atlassian.jira.rest.v2.user;

import com.atlassian.fugue.Either;
import com.atlassian.fugue.Function2;
import com.atlassian.jira.bc.user.UserPropertyService;
import com.atlassian.jira.entity.property.EntityPropertyType;
import com.atlassian.jira.issue.fields.rest.json.beans.EntityPropertyBean;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.rest.util.ResponseFactory;
import com.atlassian.jira.rest.util.SelfLinkBuilder;
import com.atlassian.jira.rest.v2.entity.property.BasePropertyResource;
import com.atlassian.jira.rest.v2.entity.property.EntityPropertiesKeysBean;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserIdentity;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.ErrorCollection.Reason;
import com.atlassian.jira.util.ErrorCollections;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.rest.annotation.ResponseType;
import com.google.common.base.Function;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Optional;

/**
 * @since v7.0
 */
@Path("user/properties/")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class UserPropertyResource {
    private final BasePropertyResource<ApplicationUser> delegate;
    private final I18nHelper i18n;
    private final ResponseFactory responseFactory;
    private final UserManager userManager;
    private final SelfLinkBuilder.SelfLink selfLink;

    protected UserPropertyResource(
            UserPropertyService userPropertyService,
            JiraAuthenticationContext authenticationContext,
            JiraBaseUrls jiraBaseUrls,
            I18nHelper i18n,
            ResponseFactory responseFactory,
            UserManager userManager,
            SelfLinkBuilder selfLinkBuilder) {
        this.responseFactory = responseFactory;
        this.userManager = userManager;
        this.selfLink = selfLinkBuilder.path("user/properties/");
        this.delegate = new BasePropertyResource<ApplicationUser>(userPropertyService, authenticationContext, jiraBaseUrls, i18n,
                new Function2<Long, String, String>() {
                    @Override
                    public String apply(final Long userId, final String propertyKey) {
                        return getSelf(userId, propertyKey);
                    }
                }, EntityPropertyType.COMMENT_PROPERTY);
        this.i18n = i18n;
    }

    private String getSelf(final Long userId, final String propertyKey) {
        return selfLink.path(propertyKey).queryParam("userKey", userManager.getUserIdentityById(userId).get().getKey()).toString();
    }

    /**
     * Returns the keys of all properties for the user identified by the key or by the id.
     *
     * @param username username of the user whose properties are to be returned
     * @param userKey  key of the user whose properties are to be returned
     * @return a response containing EntityPropertiesKeysBean.
     * @response.representation.200.qname user-properties-keys
     * @response.representation.200.doc Returned if the user was found.
     * @response.representation.200.mediaType application/json
     * @response.representation.200.example {@link com.atlassian.jira.rest.v2.entity.property.EntityPropertyResourceExamples#GET_PROPERTIES_KEYS_RESPONSE_200}
     * @response.representation.400.doc Returned if the user key or id is invalid.
     * @response.representation.401.doc Returned if the calling user is not authenticated.
     * @response.representation.403.doc Returned if the calling user does not have permission to browse the user.
     * @response.representation.404.doc Returned if the user with given key or id does not exist or if the property with given key is not found.
     */
    @GET
    @ResponseType(EntityPropertiesKeysBean.class)
    public Response getPropertiesKeys(@QueryParam("userKey") String userKey, @QueryParam("username") String username) {
        return withUserId(userKey, username).left().on(new Function<String, Response>() {
            @Override
            public Response apply(final String input) {
                return delegate.getPropertiesKeys(input);
            }
        });
    }

    /**
     * Sets the value of the specified user's property.
     * <p>
     * You can use this resource to store a custom data against the user identified by the key or by the id. The user
     * who stores the data is required to have permissions to administer the user.
     * </p>
     *
     * @param username    username of the user whose property is to be set
     * @param userKey     key of the user whose property is to be set
     * @param propertyKey the key of the user's property. The maximum length of the key is 255 bytes.
     * @param request     the request containing value of the user's property. The value has to a valid, non-empty JSON conforming
     *                    to http://tools.ietf.org/html/rfc4627. The maximum length of the property value is 32768 bytes.
     * @response.representation.200.doc Returned if the user property is successfully updated.
     * @response.representation.201.doc Returned if the user property is successfully created.
     * @response.representation.400.doc Returned if the user key or id is invalid.
     * @response.representation.401.doc Returned if the calling user is not authenticated.
     * @response.representation.403.doc Returned if the calling user does not have permission to administer the user.
     * @response.representation.404.doc Returned if the user with given key or id does not exist.
     */
    @PUT
    @Path("/{propertyKey}")
    @ResponseType(Void.class)
    public Response setProperty(@QueryParam("userKey") String userKey, @QueryParam("username") String username, @PathParam("propertyKey") final String propertyKey, @Context final HttpServletRequest request) {
        return withUserId(userKey, username).left().on(new Function<String, Response>() {
            @Override
            public Response apply(final String input) {
                return delegate.setProperty(input, propertyKey, request);
            }
        });
    }

    /**
     * Returns the value of the property with a given key from the user identified by the key or by the id. The user who retrieves
     * the property is required to have permissions to read the user.
     *
     * @param username username of the user whose property is to be returned
     * @param userKey  key of the user whose property is to be returned
     * @return a response containing {@link com.atlassian.jira.issue.fields.rest.json.beans.EntityPropertyBean}.
     * @response.representation.200.qname user-property
     * @response.representation.200.doc Returned if the user property was found.
     * @response.representation.200.mediaType application/json
     * @response.representation.200.example {@link com.atlassian.jira.rest.v2.entity.property.EntityPropertyResourceExamples#GET_PROPERTY_RESPONSE_200}
     * @response.representation.400.doc Returned if the user key or id is invalid.
     * @response.representation.401.doc Returned if the calling user is not authenticated.
     * @response.representation.403.doc Returned if the calling user does not have permission to browse the user.
     * @response.representation.404.doc Returned if the user with given key or id does not exist or if the property with given key is not found.
     */
    @GET
    @Path("/{propertyKey}")
    @ResponseType(EntityPropertyBean.class)
    public Response getProperty(@QueryParam("userKey") String userKey, @QueryParam("username") String username, @PathParam("propertyKey") final String propertyKey) {
        return withUserId(userKey, username).left().on(new Function<String, Response>() {
            @Override
            public Response apply(final String input) {
                return delegate.getProperty(input, propertyKey);
            }
        });
    }

    /**
     * Removes the property from the user identified by the key or by the id. Ths user removing the property is required
     * to have permissions to administer the user.
     *
     * @param username username of the user whose property is to be removed
     * @param userKey  key of the user whose property is to be removed
     * @return a 204 HTTP status if everything goes well.
     * @response.representation.204.doc Returned if the user property was removed successfully.
     * @response.representation.400.doc Returned if the user key or id is invalid.
     * @response.representation.401.doc Returned if the calling user is not authenticated.
     * @response.representation.403.doc Returned if the calling user does not have permission to edit the user.
     * @response.representation.404.doc Returned if the user with given key or id does not exist or if the property with given key is not found.
     */
    @DELETE
    @Path("/{propertyKey}")
    public Response deleteProperty(@QueryParam("userKey") String userKey, @QueryParam("username") String username, @PathParam("propertyKey") final String propertyKey) {
        return withUserId(userKey, username).left().on(new Function<String, Response>() {
            @Override
            public Response apply(final String input) {
                return delegate.deleteProperty(input, propertyKey);
            }
        });
    }

    private Either<Response, String> withUserId(final String userKey, final String username) {
        if ((username == null) == (userKey == null)) {
            return Either.left(responseFactory.errorResponse(ErrorCollections.create(i18n.getText("rest.user.properties.id.not.specified"), Reason.VALIDATION_FAILED)));
        }

        final Optional<UserIdentity> userIdentity = username != null ? userManager.getUserIdentityByUsername(username) : userManager.getUserIdentityByKey(userKey);

        return userIdentity.isPresent() ? Either.<Response, String>right(userIdentity.get().getId().toString()) :
                Either.<Response, String>left(responseFactory.errorResponse(ErrorCollections.create(i18n.getText("rest.user.error.not.found.general"), Reason.NOT_FOUND)));
    }
}

