package com.atlassian.jira.rest.v2.search;

import com.atlassian.jira.sharing.SharePermission;
import com.atlassian.jira.sharing.SharePermissionImpl;
import com.atlassian.jira.sharing.type.ShareType;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Optional;
import java.util.function.Function;

/**
 * @since v7.0
 */
public class SharePermissionInputBean {
    public static SharePermissionInputBean DOC_EXAMPLE;

    static {
        DOC_EXAMPLE = new SharePermissionInputBean();
        DOC_EXAMPLE.setType("group");
        DOC_EXAMPLE.setGroupname("jira-administrators");
    }

    enum Type {
        PROJECT("project", inputBean -> new SharePermissionImpl(ShareType.Name.PROJECT, inputBean.projectId, null)),
        GROUP("group", inputBean -> new SharePermissionImpl(ShareType.Name.GROUP, inputBean.groupname, null)),
        PROJECT_ROLE("projectRole", inputBean -> new SharePermissionImpl(ShareType.Name.PROJECT, inputBean.projectId, inputBean.projectRoleId)),
        GLOBAL("global", inputBean -> new SharePermissionImpl(ShareType.Name.GLOBAL, null, null)),
        AUTHENTICATED("authenticated", inputBean -> new SharePermissionImpl(ShareType.Name.AUTHENTICATED, null, null));

        private final String value;
        private final Function<SharePermissionInputBean, SharePermission> mapFunction;

        Type(String value, Function<SharePermissionInputBean, SharePermission> mapFunction) {
            this.value = value;
            this.mapFunction = mapFunction;
        }

        public String getValue() {
            return value;
        }

        public static Optional<Type> fromValue(String type) {
            for (Type value : Type.values()) {
                if (value.value.equals(type)) {
                    return Optional.of(value);
                }
            }
            return Optional.empty();
        }

        public SharePermission buildSharePermission(SharePermissionInputBean inputBean) {
            return mapFunction.apply(inputBean);
        }
    }

    @JsonProperty("type")
    private String type;

    @JsonProperty("projectId")
    private String projectId;

    @JsonProperty("groupname")
    private String groupname;

    @JsonProperty("projectRoleId")
    private String projectRoleId;

    public SharePermissionInputBean() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(final String groupname) {
        this.groupname = groupname;
    }

    public String getProjectRoleId() {
        return projectRoleId;
    }

    public void setProjectRoleId(String projectRoleId) {
        this.projectRoleId = projectRoleId;
    }
}