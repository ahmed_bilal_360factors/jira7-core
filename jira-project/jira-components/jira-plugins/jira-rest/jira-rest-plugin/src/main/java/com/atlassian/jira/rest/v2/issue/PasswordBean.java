package com.atlassian.jira.rest.v2.issue;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Used to change user password
 *
 * @since v6.1
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PasswordBean {
    /**
     * Password bean example used in auto-generated documentation.
     */
    public static final PasswordBean DOC_EXAMPLE = new PasswordBean("new password");
    public static final PasswordBean OLD_PASSWORD_DOC_EXAMPLE = new PasswordBean("new password", "current password");

    @JsonProperty
    private String password;

    @JsonProperty
    private String currentPassword;

    private PasswordBean() {
    }

    public PasswordBean(final String password) {
        this(password, null);
    }

    public PasswordBean(final String password, final String currentPassword) {
        this.password = password;
        this.currentPassword = currentPassword;
    }

    public String getPassword() {
        return password;
    }

    public String getCurrentPassword() {
        return currentPassword;
    }
}
