package com.atlassian.jira.rest.v2.search;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.sharing.SharePermission;
import com.atlassian.jira.util.I18nHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class SharePermissionFactory {
    private final SharePermissionInputBeanValidator sharePermissionInputBeanValidator;

    @Autowired
    public SharePermissionFactory(SharePermissionInputBeanValidator sharePermissionInputBeanValidator) {
        this.sharePermissionInputBeanValidator = sharePermissionInputBeanValidator;
    }

    public ServiceOutcome<SharePermission> fromBean(SharePermissionInputBean inputBean, I18nHelper i18nHelper) {
        ServiceResult serviceResult = sharePermissionInputBeanValidator.validateAddPermissionBean(inputBean, i18nHelper);
        if (serviceResult.isValid()) {
            Optional<SharePermissionInputBean.Type> type = SharePermissionInputBean.Type.fromValue(inputBean.getType());
            if (type.isPresent()) {
                return ServiceOutcomeImpl.ok(type.get().buildSharePermission(inputBean));
            }
        }
        return ServiceOutcomeImpl.from(serviceResult.getErrorCollection());
    }
}
