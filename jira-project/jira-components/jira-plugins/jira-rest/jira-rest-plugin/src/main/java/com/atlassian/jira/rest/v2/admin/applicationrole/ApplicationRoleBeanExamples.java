package com.atlassian.jira.rest.v2.admin.applicationrole;

import com.google.common.collect.Sets;

import java.util.Arrays;
import java.util.List;

/**
 * Examples of the {@link ApplicationRoleBean} for documentation.
 */
public class ApplicationRoleBeanExamples {
    public static final ApplicationRoleBean DOC_EXAMPLE =
            new ApplicationRoleBean("jira-software", "JIRA Software", Sets.newHashSet("jira-software-users", "jira-testers"),
                    Sets.newHashSet("jira-software-users"), false, false, 10, 5, 5, "5 developers", false, false);

    public static final ApplicationRoleBean DOC_EXAMPLE2 =
            new ApplicationRoleBean("jira-core", "JIRA Core", Sets.newHashSet("jira-core-users"), Sets.newHashSet("jira-core-users"),
                    false, false, 1, 1, 0, "0 users", false, true);

    public static List<ApplicationRoleBean> DOC_LIST_EXAMPLE = Arrays.asList(DOC_EXAMPLE, DOC_EXAMPLE2);

    public static final ApplicationRoleBean UPDATE_EXAMPLE =
            new ApplicationRoleBean("jira-software", "JIRA Software", Sets.newHashSet("jira-software-users", "jira-testers"),
                    Sets.newHashSet("jira-software-users"), true);
}
