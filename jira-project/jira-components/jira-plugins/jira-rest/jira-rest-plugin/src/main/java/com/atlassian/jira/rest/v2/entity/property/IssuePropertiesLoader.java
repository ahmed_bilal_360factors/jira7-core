package com.atlassian.jira.rest.v2.entity.property;

import com.atlassian.jira.bc.issue.properties.IssuePropertyService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.rest.api.property.PropertiesBean;
import com.atlassian.jira.rest.api.util.StringList;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * A component used to load properties based on a query param
 */
@Component
public class IssuePropertiesLoader {
    private final IssuePropertyService issuePropertyService;

    @Autowired
    public IssuePropertiesLoader(@ComponentImport final IssuePropertyService issuePropertyService) {
        this.issuePropertyService = issuePropertyService;
    }

    public PropertiesBean getProperties(ApplicationUser user, Issue issue, List<StringList> queryParam) {
        return PropertiesRestLoader.withService(issuePropertyService).getProperties(user, issue, queryParam);
    }
}
