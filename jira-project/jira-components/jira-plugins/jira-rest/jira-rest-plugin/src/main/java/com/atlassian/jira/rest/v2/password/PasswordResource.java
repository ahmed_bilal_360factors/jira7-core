package com.atlassian.jira.rest.v2.password;

import com.atlassian.jira.plugin.user.PasswordPolicyManager;
import com.atlassian.jira.plugin.user.WebErrorMessage;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.rest.annotation.RequestType;
import com.atlassian.rest.annotation.ResponseType;
import com.google.common.collect.ImmutableList;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collection;
import java.util.List;

import static org.apache.commons.lang3.StringUtils.isEmpty;

/**
 * REST resource for operations related to passwords and the password policy.
 *
 * @since v6.1
 */
@Path("password")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class PasswordResource {
    private final PasswordPolicyManager passwordPolicyManager;
    private final UserManager userManager;

    public PasswordResource(PasswordPolicyManager passwordPolicyManager, UserManager userManager) {
        this.passwordPolicyManager = passwordPolicyManager;
        this.userManager = userManager;
    }

    /**
     * Returns the list of requirements for the current password policy. For example, "The password must have at least 10 characters.",
     * "The password must not be similar to the user's name or email address.", etc.
     *
     * @param hasOldPassword whether or not the user will be required to enter their current password.  Use
     *                       {@code false} (the default) if this is a new user or if an administrator is forcibly changing
     *                       another user's password.
     * @return a response containing a JSON array of the user-facing messages.  If no policy is set, then
     * this will be an empty list.
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc Returns an array of message strings.
     */
    @GET
    @Path("policy")
    @ResponseType(value = List.class, genericTypes = String.class)
    public Response getPasswordPolicy(@QueryParam("hasOldPassword") @DefaultValue("false") boolean hasOldPassword) {
        return Response.ok(passwordPolicyManager.getPolicyDescription(hasOldPassword)).build();
    }

    /**
     * Returns a list of statements explaining why the password policy would disallow a proposed password for a new user.
     * <p>
     * You can use this method to test the password policy validation. This could be done prior to an action 
     * where a new user and related password are created, using methods like the ones in 
     * <a href="https://docs.atlassian.com/jira/latest/com/atlassian/jira/bc/user/UserService.html">UserService</a>.      
     * For example, you could use this to validate a password in a create user form in the user interface, as the user enters it.<br/>
     * The username and new password must be not empty to perform the validation.<br/>
     * Note, this method will help you validate against the policy only. It won't check any other validations that might be performed 
     * when creating a new user, e.g. checking whether a user with the same name already exists.
     * </p>
     *
     * @param bean a representation of the intended parameters for the user that would be created.
     * @return a response containing a JSON array of the user-facing messages.  If no policy is set, then
     * this will be an empty list.
     * @request.representation.mediaType application/json
     * @request.representation.example {@link com.atlassian.jira.rest.v2.password.PasswordPolicyCreateUserBean#DOC_EXAMPLE}
     * The username and new password must be specified.  The old password should be specified for
     * updates where the user would be required to enter it and omitted for those like a password
     * reset or forced change by the administrator where the old password would not be known.
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc Returns an array of message strings.
     * @response.representation.400.doc Returned if the request is invalid, such as if the username or password is left unspecified.
     */
    @POST
    @Path("policy/createUser")
    @RequestType(PasswordPolicyCreateUserBean.class)
    @ResponseType(value = List.class, genericTypes = String.class)
    public Response policyCheckCreateUser(PasswordPolicyCreateUserBean bean) {
        if (bean == null || isEmpty(bean.getUsername()) || isEmpty(bean.getPassword())) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        return Response.ok(snippets(passwordPolicyManager.checkPolicy(bean.getUsername(),
                bean.getDisplayName(), bean.getEmailAddress(), bean.getPassword()))).build();
    }

    /**
     * Returns a list of statements explaining why the password policy would disallow a proposed new password for a user with an existing password.
     * <p>
     * You can use this method to test the password policy validation. This could be done prior to an action where the password 
     * is actually updated, using methods like <a href="https://docs.atlassian.com/jira/latest/com/atlassian/jira/web/action/user/ChangePassword.html">ChangePassword</a>      
     * or <a href="https://docs.atlassian.com/jira/latest/com/atlassian/jira/web/action/user/ResetPassword.html">ResetPassword</a>. 
     * For example, you could use this to validate a password in a change password form in the user interface, as the user enters it.<br/>
     * The user must exist and the username and new password must be not empty, to perform the validation.<br/>
     * Note, this method will help you validate against the policy only. It won't check any other validations that might be performed 
     * when submitting a password change/reset request, e.g. verifying whether the old password is valid.
     * </p>
     *
     * @param bean a representation of the intended parameters for the update that would be performed
     * @return a response containing a JSON array of the user-facing messages.  If no policy is set, then
     * this will be an empty list.
     * @request.representation.mediaType application/json
     * @request.representation.example {@link com.atlassian.jira.rest.v2.password.PasswordPolicyUpdateUserBean#DOC_EXAMPLE}
     * The username and new password must be specified.  The old password should be specified for
     * updates where the user would be required to enter it and omitted for those like a password
     * reset or forced change by the administrator where the old password would not be known.
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc Returns an array of message strings.
     * @response.representation.400.doc Returned if the request is invalid, such as if the username or new password is left unspecified.
     * @response.representation.404.doc Returned if the username does not correspond to any existing user.
     */
    @POST
    @Path("policy/updateUser")
    @RequestType(PasswordPolicyUpdateUserBean.class)
    @ResponseType(value = List.class, genericTypes = String.class)
    public Response policyCheckUpdateUser(PasswordPolicyUpdateUserBean bean) {
        if (bean == null || isEmpty(bean.getUsername()) || isEmpty(bean.getNewPassword())) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        final ApplicationUser existing = userManager.getUserByName(bean.getUsername());
        if (existing == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        return Response.ok(snippets(passwordPolicyManager.checkPolicy(existing, bean.getOldPassword(),
                bean.getNewPassword()))).build();
    }

    private static List<String> snippets(Collection<WebErrorMessage> messages) {
        final ImmutableList.Builder<String> snippets = ImmutableList.builder();
        for (WebErrorMessage message : messages) {
            snippets.add(message.getSnippet());
        }
        return snippets.build();
    }
}
