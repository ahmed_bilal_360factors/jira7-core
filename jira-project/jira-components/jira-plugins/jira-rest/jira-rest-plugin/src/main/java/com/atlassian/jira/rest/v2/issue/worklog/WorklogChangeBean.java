package com.atlassian.jira.rest.v2.issue.worklog;

import org.codehaus.jackson.annotate.JsonAutoDetect;

/**
 * Bean with information about worklog update.
 *
 * @since 7.0
 */
@JsonAutoDetect
public class WorklogChangeBean {
    private final Long worklogId;
    private final Long updatedTime;

    public WorklogChangeBean(final Long worklogId, final Long updatedTime) {
        this.worklogId = worklogId;
        this.updatedTime = updatedTime;
    }

    public Long getWorklogId() {
        return worklogId;
    }

    public Long getUpdatedTime() {
        return updatedTime;
    }
}
