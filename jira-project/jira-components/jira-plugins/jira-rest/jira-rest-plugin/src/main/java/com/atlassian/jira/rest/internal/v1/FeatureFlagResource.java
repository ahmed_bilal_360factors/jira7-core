package com.atlassian.jira.rest.internal.v1;

import com.atlassian.fugue.Option;
import com.atlassian.jira.config.FeatureFlag;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.rest.exception.ForbiddenWebException;
import com.atlassian.jira.util.dbc.Assertions;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Map;
import java.util.Set;

import static com.atlassian.jira.rest.v1.util.CacheControl.NO_CACHE;

/**
 * REST resource for managing feature flags.
 *
 * @since 7.1
 */
@Path("featureFlag")
@Produces({MediaType.APPLICATION_JSON})
public class FeatureFlagResource {
    private final FeatureManager featureManager;

    public FeatureFlagResource(final FeatureManager featureManager) {
        this.featureManager = Assertions.notNull("featureManager", featureManager);
    }

    /**
     * Gets all managed features
     *
     * @response.representation.200.doc Returned if the currently authenticated user can view dark features.
     * @response.representation.403.doc Returned if the currently authenticated user does not have permission to view dark features.
     */
    @GET
    public Response get() {
        if (!featureManager.hasSiteEditPermission()) {
            throw new ForbiddenWebException();
        }

        Map<String, DarkFeaturePropertyBean> features = getFeatureFlags();
        return Response.ok(features).cacheControl(NO_CACHE).build();
    }

    /**
     * Gets whether feature flag is enabled or disabled.
     *
     * @response.representation.200.doc Returned if the key is defined.
     * @response.representation.404.doc Returned if the feature key is not defined.
     * @response.representation.403.doc Returned if the currently authenticated user does not have permission to view dark features.
     */
    @GET
    @Path("/{featureKey}")
    public Response get(@PathParam("featureKey") final String featureKey) {
        if (!featureManager.hasSiteEditPermission()) {
            throw new ForbiddenWebException();
        }

        if (StringUtils.isNotBlank(featureKey)) {
            Option<FeatureFlag> key = featureManager.getFeatureFlag(featureKey);
            if (key.isDefined()) {
                boolean enabled = featureManager.isEnabled(key.get());
                return Response.ok(new DarkFeaturePropertyBean(enabled)).cacheControl(NO_CACHE).build();
            } else {
                return Response.status(Response.Status.NOT_FOUND).cacheControl(NO_CACHE).build();
            }
        } else {
            return Response.status(Response.Status.BAD_REQUEST).cacheControl(NO_CACHE).build();
        }
    }

    /**
     * Enable or disable a feature flag via PUT.
     *
     * @request.representation.mediaType application/json
     * @response.representation.204.doc Returned if the currently authenticated user can enable dark features.
     * @response.representation.404.doc Returned if the feature key is not defined.
     * @response.representation.403.doc Returned if the currently authenticated user does not have permission to edit dark features.
     */
    @PUT
    @Path("/{featureKey}")
    @Consumes({MediaType.APPLICATION_JSON})
    public Response put(@PathParam("featureKey") final String featureKey, final DarkFeaturePropertyBean darkFeaturePropertyBean) {
        if (!featureManager.hasSiteEditPermission()) {
            throw new ForbiddenWebException();
        }

        if (StringUtils.isNotBlank(featureKey)) {
            Option<FeatureFlag> key = featureManager.getFeatureFlag(featureKey);
            if (key.isDefined()) {
                setFeatureFlag(key.get(), darkFeaturePropertyBean.isEnabled());
                return Response.noContent().cacheControl(NO_CACHE).build();
            } else {
                return Response.status(Response.Status.NOT_FOUND).cacheControl(NO_CACHE).build();
            }
        } else {
            return Response.status(Response.Status.BAD_REQUEST).cacheControl(NO_CACHE).build();
        }
    }

    /**
     * Enabled or disables <code>feature</code> depending on the value of <code>enabled</code>.
     *
     * @param featureFlag a String containing the feature name
     * @param enabled     a Boolean indicating whether to enable or disable the feature
     */
    private void setFeatureFlag(FeatureFlag featureFlag, boolean enabled) {

        if (featureFlag.isOnByDefault()) {
            setSiteFeature(featureFlag.disabledFeatureKey(), !enabled);
        } else {
            setSiteFeature(featureFlag.enabledFeatureKey(), enabled);
        }
    }

    private void setSiteFeature(String darkFeatureKey, boolean enabled) {
        if (enabled) {
            featureManager.enableSiteDarkFeature(darkFeatureKey);
        } else {
            featureManager.disableSiteDarkFeature(darkFeatureKey);
        }
    }

    /**
     * Returns a Map containing all feature flags.
     *
     * @return a Map containing all feature flags.
     */
    private Map<String, DarkFeaturePropertyBean> getFeatureFlags() {
        Set<FeatureFlag> featureFlags = featureManager.getRegisteredFlags();
        Map<String, DarkFeaturePropertyBean> result = Maps.newHashMap();
        for (FeatureFlag featureFlag : featureFlags) {
            result.put(featureFlag.featureKey(), new DarkFeaturePropertyBean(featureManager.isEnabled(featureFlag)));
        }
        return result;
    }
}
