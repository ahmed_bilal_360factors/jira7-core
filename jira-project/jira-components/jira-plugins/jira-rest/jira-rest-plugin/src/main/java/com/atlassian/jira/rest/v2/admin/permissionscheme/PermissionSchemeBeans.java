package com.atlassian.jira.rest.v2.admin.permissionscheme;

import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.rest.api.permission.PermissionGrantBean;
import com.atlassian.jira.rest.api.permission.PermissionGrantsBean;
import com.atlassian.jira.rest.api.permission.PermissionHolderBean;
import com.atlassian.jira.rest.api.permission.PermissionSchemeBean;
import com.atlassian.jira.rest.api.permission.PermissionSchemesBean;
import com.atlassian.jira.rest.v2.issue.Examples;
import com.google.common.collect.ImmutableList;

import java.util.Collections;

public class PermissionSchemeBeans {
    public static final PermissionGrantBean GRANT_CREATE_EXAMPLE = PermissionGrantBean.builder()
            .setHolder(PermissionHolderBean.holderBean("group", "jira-developers").input())
            .setPermission(ProjectPermissions.ADMINISTER_PROJECTS.permissionKey())
            .build();

    public static PermissionSchemeBean SCHEME_CREATE_EXAMPLE = PermissionSchemeBean.builder()
            .setName("Example permission scheme")
            .setDescription("description")
            .setPermissions(ImmutableList.of(GRANT_CREATE_EXAMPLE))
            .build()
            .input();

    public static final PermissionGrantBean GRANT_EXAMPLE = PermissionGrantBean.builder()
            .setId(10000L)
            .setSelf(Examples.restURI(PermissionSchemeResource.RESOURCE_PATH, PermissionSchemeResource.ENTITY_PATH, "10000"))
            .setHolder(PermissionHolderBean.holderBean("group", "jira-developers"))
            .setPermission(ProjectPermissions.ADMINISTER_PROJECTS.permissionKey())
            .build();

    public static final ImmutableList<PermissionGrantBean> GRANT_BEANS = ImmutableList.of(GRANT_EXAMPLE);

    public static PermissionSchemeBean SCHEME_EXAMPLE = PermissionSchemeBean.builder()
            .setId(10000L)
            .setSelf(Examples.restURI(PermissionSchemeResource.RESOURCE_PATH, "10000"))
            .setName("Example permission scheme")
            .setDescription("description")
            .setPermissions(GRANT_BEANS)
            .build()
            .input();

    public static final PermissionGrantsBean GRANT_LIST_EXAMPLE = new PermissionGrantsBean(GRANT_BEANS);

    public static PermissionSchemeBean SCHEME_SHORT_EXAMPLE = PermissionSchemeBean.builder()
            .setId(10000L)
            .setSelf(Examples.restURI(PermissionSchemeResource.RESOURCE_PATH, "10000"))
            .setName("Example permission scheme")
            .setDescription("description")
            .build()
            .input();

    public static PermissionSchemesBean SCHEME_LIST_EXAMPLE = new PermissionSchemesBean(Collections.singletonList(SCHEME_SHORT_EXAMPLE));
}
