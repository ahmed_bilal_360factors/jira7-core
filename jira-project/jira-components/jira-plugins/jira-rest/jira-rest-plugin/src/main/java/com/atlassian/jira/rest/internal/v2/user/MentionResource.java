package com.atlassian.jira.rest.internal.v2.user;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.rest.IssueFinder;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.mention.MentionableUserSearcher;
import com.atlassian.jira.rest.v2.issue.users.UserIssueRelevanceBean;
import com.atlassian.jira.rest.v2.issue.users.UserIssueRelevanceBeanBuilder;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserIssueRelevance;
import com.atlassian.jira.util.EmailFormatter;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.rest.annotation.ResponseType;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import static com.atlassian.jira.rest.api.http.CacheControl.never;

/**
 * Internal resources backing @mentions.
 *
 * @since v7.2
 */
@Path("user/mention")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class MentionResource {
    private static final int MIN_USERS_RETURNED = 1;
    private static final int DEFAULT_USERS_RETURNED = 50;
    private static final int MAX_USERS_RETURNED = 1000;

    private final EmailFormatter emailFormatter;
    private final I18nHelper.BeanFactory beanFactory;
    private final IssueFinder issueFinder;
    private final JiraAuthenticationContext authContext;
    private final JiraBaseUrls jiraBaseUrls;
    private final MentionableUserSearcher mentionableUserSearch;
    private final TimeZoneManager timeZoneManager;
    private final AvatarService avatarService;

    public MentionResource(final EmailFormatter emailFormatter,
                           final I18nHelper.BeanFactory beanFactory,
                           final IssueFinder issueFinder,
                           final JiraAuthenticationContext authContext,
                           final JiraBaseUrls jiraBaseUrls,
                           @ComponentImport final MentionableUserSearcher mentionableUserSearch,
                           final TimeZoneManager timeZoneManager,
                           final AvatarService avatarService) {
        this.emailFormatter = emailFormatter;
        this.beanFactory = beanFactory;
        this.issueFinder = issueFinder;
        this.authContext = authContext;
        this.jiraBaseUrls = jiraBaseUrls;
        this.mentionableUserSearch = mentionableUserSearch;
        this.timeZoneManager = timeZoneManager;
        this.avatarService = avatarService;
    }

    /**
     * Returns a list of users who are relevant to the issue and match the query
     * string. The users are sorted by 'relevance' to the issue, with most
     * relevant appearing first in the list.
     *
     * @param query      The username filter, no users returned if left blank
     * @param issueKey   The issue key for the issue being edited we need to find viewable users for.
     * @param maxResults The maximum number of users to return (defaults to 50). The maximum allowed value is 1000.
     * @return a Response with the users matching the query
     * @response.representation.200.qname List of users
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc Returns a full representation of a JIRA user with relevancy information in JSON format.
     * @response.representation.200.example {@link com.atlassian.jira.rest.v2.issue.users.UserIssueRelevanceBean#DOC_EXAMPLE}
     * @response.representation.401.doc Returned if the current user is not authenticated.
     */
    @GET
    @ResponseType(value = List.class, genericTypes = UserIssueRelevanceBean.class)
    @Path("search")
    public Response search(@QueryParam("query") final String query,
                           @QueryParam("issueKey") final String issueKey,
                           @QueryParam("maxResults") Integer maxResults) {
        maxResults = maxResults == null ? DEFAULT_USERS_RETURNED : clamp(maxResults, MIN_USERS_RETURNED, MAX_USERS_RETURNED);

        final ApplicationUser loggedInUser = authContext.getLoggedInUser();
        final Issue issue = issueFinder.findIssue(issueKey, new SimpleErrorCollection());
        final List<UserIssueRelevance> involvedUsers = mentionableUserSearch.findRelatedUsersToMention(query, issue, loggedInUser, maxResults);

        final UserIssueRelevanceBeanBuilder userIssueRelevanceBeanBuilder = new UserIssueRelevanceBeanBuilder(
                emailFormatter,
                beanFactory.getInstance(loggedInUser),
                jiraBaseUrls,
                loggedInUser,
                timeZoneManager.getTimeZoneforUser(loggedInUser),
                avatarService
        );
        final List<UserIssueRelevanceBean> userBeans = involvedUsers.stream()
                .map(userIssueRelevanceBeanBuilder::build)
                .collect(CollectorsUtil.toImmutableList());

        return Response.ok(userBeans).cacheControl(never()).build();
    }

    private static int clamp(final int n, final int min, final int max) {
        return n < min ? min : (n > max ? max : n);
    }
}
