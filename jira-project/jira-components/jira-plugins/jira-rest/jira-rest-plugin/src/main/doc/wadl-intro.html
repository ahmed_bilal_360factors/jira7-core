<p>
    Welcome to the JIRA Server platform REST API reference. You can use this REST API to build add-ons for JIRA, develop integrations between JIRA and other applications, or script interactions with JIRA. This page documents the REST resources available in JIRA Server platform, along with expected HTTP response codes and sample requests. <br/>
    Looking for the REST API reference for a different JIRA version? Follow the links below.
</p>
<ul>
    <li><a href="https://docs.atlassian.com/jira/REST/cloud/">JIRA Cloud platform REST API</a></li>
    <li><a href="https://docs.atlassian.com/jira/REST/">List of all JIRA REST APIs</a></li>    
</ul>

<h2 id="gettingstarted">Getting started</h2>

<p>
    If you haven't integrated with JIRA Server before, read the <a href="https://developer.atlassian.com/display/JIRADEV/Getting+started+with+Plugins2">Getting started guide</a> in the <a href="https://developer.atlassian.com/display/JIRADEV">JIRA Server developer documentation</a>. You may also want to read our <a href="https://developer.atlassian.com/display/JIRADEV/JIRA+REST+APIs">JIRA REST API overview</a>, which describes how the JIRA REST APIs work, including a simple example of a REST call.
</p>

<h2 id="authentication">Authentication</h2>

<p>
    The preferred authentication methods for the JIRA REST APIs are <a href="https://developer.atlassian.com/display/JIRADEV/JIRA+REST+API+Example+-+OAuth+authentication">OAuth</a> and <a href="https://developer.atlassian.com/display/JIRADEV/JIRA+REST+API+Example+-+Basic+Authentication">HTTP basic authentication</a> (when using SSL).
</p>

<p>
    JIRA itself uses cookie-based authentication in the browser, so you can call REST from JavaScript on the page and rely on the authentication that the browser has established. To reproduce the behavior of the JIRA log-in page (for example, to display authentication error messages to users) can <code>POST</code> to the <code>/auth/1/session</code> <a href="#auth/1/session"> resource</a>.
</p>

<h2><a name="JIRA4.2RESTAPIdiscussionpoints-StructureoftheRESTURIs"></a>URI Structure</h2>

<p>
    JIRA's REST APIs provide access to resources (data entities) via URI paths. To use a REST API, your application will
    make an HTTP request and parse the response. The JIRA REST API uses
    <a href="http://en.wikipedia.org/wiki/JSON">JSON</a> as its communication format, and the standard HTTP methods like
    <code>GET</code>, <code>PUT</code>, <code>POST</code> and <code>DELETE</code> (see API descriptions below for which
    methods are available for each resource). URIs for JIRA's REST API resource have the following structure:
</p>

<pre>
    <code>http://host:port/context/rest/api-name/api-version/resource-name</code>
</pre>

<p>
    Currently there are two API names available, which will be discussed further below:
</p>
<ul>
    <li><code>auth</code> - for authentication-related operations, and</li>
    <li><code>api</code> - for everything else.</li>
</ul>

<p>
    The current API version is <code>2</code>. However, there is also a symbolic version, called <code>latest</code>,
    which resolves to the latest version supported by the given JIRA instance. As an example, if you wanted to retrieve
    the JSON representation of issue <a href="https://jira.atlassian.com/browse/JRA-9">JRA-9</a> from Atlassian's public
    issue tracker, you would access:
</p>

<pre>
    <code><a href="https://jira.atlassian.com/rest/api/latest/issue/JRA-9" class="external-link" rel="nofollow">https://jira.atlassian.com/rest/api/latest/issue/JRA-9</a></code>
</pre>

<p>
    There is a <a href="http://en.wikipedia.org/wiki/Web_Application_Description_Language">WADL</a> document that
    contains the documentation for each resource in the JIRA REST API. It is available
    <a href="jira-rest-plugin.wadl">here</a>.
</p>

<h2 id="expansion">Expansion</h2>

<p>
    In order to simplify API responses, the JIRA REST API uses resource expansion. This means the API will only 
    return parts of the resource when explicitly requested.
</p>

<p>
    You can use the <code>expand</code> query parameter to specify a comma-separated list of entities that you want
    expanded, identifying each of them by name. For example, appending <code>?expand=names,renderedFields</code> to an
    issue's URI requests the inclusion of the translated field names and the HTML-rendered field values in the response.
    Continuing with our example above, we would use the following URL to get that information for JRA-9:
</p>

<pre>
    <code><a href="https://jira.atlassian.com/rest/api/latest/issue/JRA-9?expand=names,renderedFields" class="external-link" rel="nofollow">https://jira.atlassian.com/rest/api/latest/issue/JRA-9?expand=names,renderedFields</a></code>
</pre>

<p>
    To discover the identifiers for each entity, look at the <code>expand</code> property in the parent object. In the
    JSON example below, the resource declares widgets as being expandable.
</p>

<pre>
    <code class="json">{"expand":"widgets", "self":"http://www.example.com/jira/rest/api/resource/KEY-1", "widgets":{"widgets":[],"size":5}}</code>
</pre>

<p>
    You can use the dot notation to specify expansion of entities within another entity. For example
    <code>?expand=widgets.fringels</code> would expand the widgets collection and also the fringel property on each
    widget.
</p>

<h2 id="pagination">Pagination</h2>
<p>
    JIRA uses pagination to limit the response size for resources that return a potentially large collection
    of items. A request to a paged API will result in a values array wrapped in a JSON object with some paging metadata, for example:
</p>
<pre>
    {
        "startAt" : 0,
        "maxResults" : 10,
        "total": 200,
        "values": [
            { /* result 0 */ },
            { /* result 1 */ },
            { /* result 2 */ }
        ]
    }
</pre>
<p>
    Clients can use the "startAt" and "maxResults" parameters to retrieve the desired numbers of results.
</p>
<p>
    The "maxResults" parameter indicates how many results to return per page. Each API may have a different limit for
    number of items returned.
</p>
<p>
    The "startAt" parameter indicates which item should be used as the first item in the page of results.
</p>
<p>
    <strong>Important:</strong> The response contains a "total" field which denotes the total number of entities contained in all pages.
    This number <strong><em>may change</em></strong> as the client requests the subsequent pages. A client should always assume
    that the requested page can be empty. REST API consumers should also consider the field to be optional. In cases, when calculating this
    value is too expensive we may not include this in response.
</p>

<h2 id="ordering">Ordering</h2>
<p>
    Some resources support ordering by a specific field. Ordering request is provided in the <strong>orderBy</strong> query parameter.
    See the docs for specific methods to see which fields they support and if they support ordering at all.
</p>

<p>
    Ordering can be ascending or descending. By default it's ascending. To specify the ordering use "-" or "+" sign. Examples:
    <dl>
        <dt><strong>?orderBy=name</strong></dt><dd>Order by "name" ascending</dd>
        <dt><strong>?orderBy=+name</strong></dt><dd>Order by "name" ascending</dd>
        <dt><strong>?orderBy=-name</strong></dt><dd>Order by "name" descending</dd>
    </dl>
</p>


<h2 id="experimental">Experimental methods</h2>

<p>
    Methods marked as experimental may change without an earlier notice. We are looking for your feedback for these methods.
</p>

<h2 id="special-request-headers">Special request and response headers</h2>

<p>
<ul>
    <li>
        <strong>X-AUSERNAME</strong> - Response header which contains either username of the authenticated user or 'anonymous'.
    </li>
    <li>
        <strong>X-Atlassian-Token</strong> - methods which accept multipart/form-data will only process requests with 'X-Atlassian-Token: no-check' header.
    </li>
</ul>
</p>

<h2 id="error-responses">Error responses</h2>

Most resources will return a response body in addition to the status code. Usually, the JSON schema of the entity returned is the following:

<div class="representation-doc-block">
    <p><pre><code>{"id":"https://docs.atlassian.com/jira/REST/schema/error-collection#","title":"Error Collection","type":"object","properties":{"errorMessages":{"type":"array","items":{"type":"string"}},"errors":{"type":"object","patternProperties":{".+":{"type":"string"}},"additionalProperties":false},"status":{"type":"integer"}},"additionalProperties":false}</code></pre></p>
</div>