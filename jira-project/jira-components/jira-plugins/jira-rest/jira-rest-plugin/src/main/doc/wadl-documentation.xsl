<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE cd_page [
<!ENTITY intro SYSTEM "wadl-intro.html">
]>
<!--
  wadl_documentation.xsl (2008-12-09)

  An XSLT stylesheet for generating HTML documentation from WADL,
  by Mark Nottingham <mnot@yahoo-inc.com>.

  Copyright (c) 2006-2008 Yahoo! Inc.

  This work is licensed under the Creative Commons Attribution-ShareAlike 2.5
  License. To view a copy of this license, visit
    http://creativecommons.org/licenses/by-sa/2.5/
  or send a letter to
    Creative Commons
    543 Howard Street, 5th Floor
    San Francisco, California, 94105, USA
-->
<!--
 * FIXME
    - Doesn't inherit query/header params from resource/@type
    - XML schema import, include, redefine don't import
-->
<!--
  * TODO
    - forms
    - link to or include non-schema variable type defs (as a separate list?)
    - @href error handling
-->

<!--suppress XmlUnusedNamespaceDeclaration -->
<xsl:stylesheet
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
        xmlns:wadl="http://wadl.dev.java.net/2009/02"
        xmlns:xs="http://www.w3.org/2001/XMLSchema"
        xmlns:html="http://www.w3.org/1999/xhtml"
        xmlns:exsl="http://exslt.org/common"
        xmlns:ns="urn:namespace"
        extension-element-prefixes="exsl"
        xmlns="http://www.w3.org/1999/xhtml"
        exclude-result-prefixes="xsl wadl xs html ns"
        >

    <xsl:output
        method="html"
        encoding="UTF-8"
        indent="yes"
        doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
        doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
    />

    <xsl:variable name="wadl-ns">http://wadl.dev.java.net/2009/02</xsl:variable>
    <xsl:variable name="rest-uri-prefix">/rest</xsl:variable>
    <xsl:variable name="hard-space"><xsl:text>&#160;</xsl:text></xsl:variable>

    <xsl:template match="code">
        <code class="json"/>
    </xsl:template>

    <!-- expand @hrefs, @types into a full tree -->

    <xsl:variable name="resources">
        <xsl:apply-templates select="/wadl:application/wadl:resources" mode="expand"/>
    </xsl:variable>

    <xsl:template match="wadl:resources" mode="expand">
        <xsl:variable name="base">
            <xsl:choose>
                <xsl:when test="substring(@base, string-length(@base), 1) = '/'">
                    <xsl:value-of select="substring(@base, 1, string-length(@base) - 1)"/>
                </xsl:when>
                <xsl:otherwise><xsl:value-of select="@base"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:element name="resources" namespace="{$wadl-ns}">
            <xsl:for-each select="namespace::*">
                <xsl:variable name="prefix" select="name(.)"/>
                <xsl:if test="$prefix">
                    <xsl:attribute name="ns:{$prefix}"><xsl:value-of select="."/></xsl:attribute>
                </xsl:if>
            </xsl:for-each>
            <xsl:apply-templates select="@*|node()" mode="expand">
                <xsl:with-param name="base" select="$base"/>
            </xsl:apply-templates>
        </xsl:element>
    </xsl:template>

    <xsl:template match="wadl:resource[@type]" mode="expand" priority="1">
        <xsl:param name="base"></xsl:param>
        <xsl:variable name="uri" select="substring-before(@type, '#')"/>
        <xsl:variable name="id" select="substring-after(@type, '#')"/>
        <xsl:element name="resource" namespace="{$wadl-ns}">
			<xsl:attribute name="path"><xsl:value-of select="@path"/></xsl:attribute>
            <xsl:choose>
                <xsl:when test="$uri">
                    <xsl:variable name="included" select="document($uri, /)"/>
                    <xsl:copy-of select="$included/descendant::wadl:resource_type[@id=$id]/@*"/>
                    <xsl:attribute name="id"><xsl:value-of select="@type"/>#<xsl:value-of select="@path"/></xsl:attribute>
                    <xsl:apply-templates select="$included/descendant::wadl:resource_type[@id=$id]/*" mode="expand">
                        <xsl:with-param name="base" select="$uri"/>
                    </xsl:apply-templates>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:copy-of select="//resource_type[@id=$id]/@*"/>
                    <xsl:attribute name="id"><xsl:value-of select="$base"/>#<xsl:value-of select="@type"/>#<xsl:value-of select="@path"/></xsl:attribute>
                    <xsl:apply-templates select="//wadl:resource_type[@id=$id]/*" mode="expand">
                        <xsl:with-param name="base" select="$base"/>
                    </xsl:apply-templates>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:apply-templates select="node()" mode="expand">
                <xsl:with-param name="base" select="$base"/>
            </xsl:apply-templates>
        </xsl:element>
    </xsl:template>

    <xsl:template match="wadl:*[@href]" mode="expand">
        <xsl:param name="base"></xsl:param>
        <xsl:variable name="uri" select="substring-before(@href, '#')"/>
        <xsl:variable name="id" select="substring-after(@href, '#')"/>
        <xsl:element name="{local-name()}" namespace="{$wadl-ns}">
            <xsl:copy-of select="@*"/>
            <xsl:choose>
                <xsl:when test="$uri">
                    <xsl:attribute name="id"><xsl:value-of select="@href"/></xsl:attribute>
                    <xsl:variable name="included" select="document($uri, /)"/>
                    <xsl:apply-templates select="$included/descendant::wadl:*[@id=$id]/*" mode="expand">
                        <xsl:with-param name="base" select="$uri"/>
                    </xsl:apply-templates>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="id"><xsl:value-of select="$base"/>#<xsl:value-of select="$id"/></xsl:attribute>
                    <!-- xsl:attribute name="id"><xsl:value-of select="generate-id()"/></xsl:attribute -->
                    <xsl:attribute name="element"><xsl:value-of select="//wadl:*[@id=$id]/@element"/></xsl:attribute>
                    <xsl:attribute name="mediaType"><xsl:value-of select="//wadl:*[@id=$id]/@mediaType"/></xsl:attribute>
                    <xsl:attribute name="status"><xsl:value-of select="//wadl:*[@id=$id]/../@status"/></xsl:attribute>
                    <xsl:attribute name="name"><xsl:value-of select="//wadl:*[@id=$id]/@name"/></xsl:attribute>
                    <xsl:apply-templates select="//wadl:*[@id=$id]/*" mode="expand">
                        <xsl:with-param name="base" select="$base"/>
                    </xsl:apply-templates>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:element>
    </xsl:template>

    <xsl:template match="node()[@id]" mode="expand">
        <xsl:param name="base"></xsl:param>
        <xsl:element name="{local-name()}" namespace="{$wadl-ns}">
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates select="node()" mode="expand">
                <xsl:with-param name="base" select="$base"/>
            </xsl:apply-templates>
        </xsl:element>
    </xsl:template>

    <xsl:template match="@*|node()" mode="expand">
        <xsl:param name="base"></xsl:param>
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" mode="expand">
                <xsl:with-param name="base" select="$base"/>
            </xsl:apply-templates>
        </xsl:copy>
    </xsl:template>

<!-- debug $resources
    <xsl:template match="/">
    <xsl:copy-of select="$resources"/>
    </xsl:template>
-->

    <!-- collect grammars (TODO: walk over $resources instead) -->

    <xsl:variable name="grammars">
        <xsl:copy-of select="/wadl:application/wadl:grammars/*[not(namespace-uri()=$wadl-ns)]"/>
        <xsl:apply-templates select="/wadl:application/wadl:grammars/wadl:include[@href]" mode="include-grammar"/>
        <xsl:apply-templates select="/wadl:application/wadl:resources/descendant::wadl:resource[@type]" mode="include-href"/>
        <xsl:apply-templates select="exsl:node-set($resources)/descendant::wadl:*[@href]" mode="include-href"/>
    </xsl:variable>

    <xsl:template match="wadl:include[@href]" mode="include-grammar">
        <xsl:variable name="included" select="document(@href, /)/*"></xsl:variable>
        <xsl:element name="wadl:include">
            <xsl:attribute name="href"><xsl:value-of select="@href"/></xsl:attribute>
            <xsl:copy-of select="$included"/> <!-- FIXME: xml-schema includes, etc -->
        </xsl:element>
    </xsl:template>

    <xsl:template match="wadl:*[@href]" mode="include-href">
        <xsl:variable name="uri" select="substring-before(@href, '#')"/>
        <xsl:if test="$uri">
            <xsl:variable name="included" select="document($uri, /)"/>
            <xsl:copy-of select="$included/wadl:application/wadl:grammars/*[not(namespace-uri()=$wadl-ns)]"/>
            <xsl:apply-templates select="$included/descendant::wadl:include[@href]" mode="include-grammar"/>
            <xsl:apply-templates select="$included/wadl:application/wadl:resources/descendant::wadl:resource[@type]" mode="include-href"/>
            <xsl:apply-templates select="$included/wadl:application/wadl:resources/descendant::wadl:*[@href]" mode="include-href"/>
        </xsl:if>
    </xsl:template>

    <xsl:template match="wadl:resource[@type]" mode="include-href">
        <xsl:variable name="uri" select="substring-before(@type, '#')"/>
        <xsl:if test="$uri">
            <xsl:variable name="included" select="document($uri, /)"/>
            <xsl:copy-of select="$included/wadl:application/wadl:grammars/*[not(namespace-uri()=$wadl-ns)]"/>
            <xsl:apply-templates select="$included/descendant::wadl:include[@href]" mode="include-grammar"/>
            <xsl:apply-templates select="$included/wadl:application/wadl:resources/descendant::wadl:resource[@type]" mode="include-href"/>
            <xsl:apply-templates select="$included/wadl:application/wadl:resources/descendant::wadl:*[@href]" mode="include-href"/>
        </xsl:if>
    </xsl:template>

    <!-- main template -->

    <xsl:template match="/wadl:application">
        <html>
            <head>
                <title>
                    <xsl:choose>
                        <xsl:when test="wadl:doc[@title]">
                            <xsl:value-of select="wadl:doc[@title][1]/@title"/>
                        </xsl:when>
                        <xsl:otherwise>My Web Application</xsl:otherwise>
                    </xsl:choose>
                </title>

                <xsl:text disable-output-escaping="yes"><![CDATA[
                <link rel="stylesheet" href="https://aui-cdn.atlassian.com/aui-adg/5.8.12/css/aui.css" media="all">
                <!--[if lt IE 9]><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/aui/5.2/css/aui-ie.css" media="all"><![endif]-->
                <!--[if IE 9]><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/aui/5.2/css/aui-ie9.css" media="all"><![endif]-->

                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/aui/5.2/js/aui.js"></script>
                <!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/aui/5.2/js/aui-ie.js"></script><![endif]-->

                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/aui/5.2/css/aui-experimental.css" media="all">
                <script src="https://cdnjs.cloudflare.com/ajax/libs/aui/5.2/js/aui-experimental.js"></script>

                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/8.5/styles/default.min.css">
                <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/8.5/highlight.min.js"></script>

                <link rel="stylesheet" href="style.css" media="all">
                <link rel="icon" type="image/png" href="images/favicon.png">
                <script type="text/javascript" src="app.js"></script>
                ]]></xsl:text>

            </head>
            <body class="aui-layout aui-theme-default aui-page-fixed">
                <header id="header" role="banner">
                    <nav role="navigation">
                        <div class="aui-header-inner">
                            <div class="aui-header-primary">
                                <h1 id="logo" class="aui-header-logo aui-header-logo-design">
                                    <a href="https://developer.atlassian.com">
                                        <span class="aui-header-logo-device">Atlassian Developers</span>
                                    </a>
                                </h1>
                            </div>
                        </div>
                    </nav>
                </header>
                <section id="content" role="main">
                    <header class="aui-page-header">
                        <div class="aui-page-header-inner">
                            <div class="aui-page-header-main">
                                <h1 class="hero">
                                    JIRA Server platform REST API reference
                                </h1>
                                <p class="hero">
                                    <xsl:choose>
                                        <xsl:when test="wadl:doc[@title]">
                                            <xsl:value-of select="wadl:doc[@title][1]/@title"/>
                                        </xsl:when>
                                        <xsl:otherwise>My Web Application</xsl:otherwise>
                                    </xsl:choose>
                                </p>
                            </div>
                        </div>
                    </header>
                    <div class="aui-page-panel">
                        <div class="aui-page-panel-inner">
                            <section class="aui-page-panel-content">
                                <!--
								   - This is where the introductory documentation gets inserted. This basically includes any
								   - documentation that isn't automatically generated. See wadl-intro.html.
								   -->
                                &intro;

                                <h2 id="resources">Resources</h2><a class="expand-methods" id="expand-all"/>

                                <xsl:apply-templates select="exsl:node-set($resources)" mode="list"/>
                                <xsl:if test="exsl:node-set($resources)/descendant::wadl:fault">
                                    <h2 id="faults">Faults</h2>
                                    <xsl:apply-templates select="exsl:node-set($resources)/descendant::wadl:fault" mode="list"/>
                                </xsl:if>
                            </section>
                        </div>
                    </div>
                </section>
                <footer id="footer" role="contentinfo">
                    <section class="footer-body">
                        <div id="footer-logo"><a href="http://www.atlassian.com/" target="_blank">Atlassian</a></div>
                    </section>
                </footer>
                <!-- Analytics Tracking Script -->
                <script src="https://ace-cdn.atlassian.com/stp/current/analytics/js/atl-analytics.min.js"></script>
                <script type="text/javascript">ace.analytics.Initializer.initWithPageAnalytics('BpJ1LB9DeVf9cx42UDsc5VCqZvJQ60dC');</script>
            </body>
        </html>
    </xsl:template>

    <!-- Listings -->

    <xsl:template match="wadl:resources" mode="list">
        <xsl:apply-templates select="wadl:resource" mode="list">
            <xsl:sort select="@path"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="wadl:resource" mode="list">
        <xsl:variable name="context"><xsl:call-template name="trimSlashes"><xsl:with-param select="ancestor::wadl:resource/@path" name="path"/></xsl:call-template></xsl:variable>
        <xsl:variable name="href-id" select="@id"/>
        <xsl:variable name="path"><xsl:call-template name="trimSlashes"><xsl:with-param name="path" select="@path"/></xsl:call-template></xsl:variable>
        <xsl:variable name="full-path">
            <xsl:value-of select="$rest-uri-prefix"/>
            <xsl:text>/</xsl:text>
            <xsl:if test="string-length($context)">
                <xsl:value-of select="$context"/>
                <xsl:text>/</xsl:text>
            </xsl:if>
            <xsl:value-of select="$path"/>
        </xsl:variable>
        <!--currently path is a name but it may change-->
        <xsl:variable name="name"><xsl:value-of select="$path"/></xsl:variable>

        <xsl:variable name="full-name">
            <xsl:value-of select="href"/>
        </xsl:variable>
        <div class="resource">
            <h3 id="{$name}">
                <a href="#{$name}">
                    <xsl:choose>
                        <xsl:when test="wadl:doc[@title]">
                            <xsl:value-of select="wadl:doc[@title][1]/@title"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:copy-of select="$name"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </a><a class="expand-methods"/>
            </h3>
            <xsl:apply-templates select="wadl:doc">
                <xsl:with-param name="expandable" select="1"/>
            </xsl:apply-templates>
            <div class="methods">
                <xsl:apply-templates select="wadl:method">
                    <xsl:with-param name="parent-path">
                        <xsl:value-of select="$full-path"/>
                    </xsl:with-param>
                    <xsl:with-param name="parent-name">
                        <xsl:value-of select="$name"/>
                    </xsl:with-param>
                </xsl:apply-templates>
                <xsl:apply-templates select="wadl:resource" mode="list-methods">
                    <xsl:with-param name="parent-name">
                        <xsl:value-of select="$name"/>
                    </xsl:with-param>
                    <xsl:sort select="@path"/>
                </xsl:apply-templates>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="wadl:resource" mode="list-methods">
        <xsl:param name="parent-name"/>
        <xsl:variable name="context"><xsl:call-template name="trimSlashes"><xsl:with-param select="ancestor::wadl:resource/@path" name="path"/></xsl:call-template></xsl:variable>
        <xsl:variable name="path"><xsl:call-template name="trimSlashes"><xsl:with-param name="path" select="@path"/></xsl:call-template></xsl:variable>
        <xsl:variable name="full-path">
            <xsl:value-of select="$rest-uri-prefix"/>
            <xsl:text>/</xsl:text>
            <xsl:if test="string-length($context)">
                <xsl:value-of select="$context"/>
                <xsl:text>/</xsl:text>
            </xsl:if>
            <xsl:value-of select="$path"/>
        </xsl:variable>
        <xsl:apply-templates select="wadl:method">
            <xsl:with-param name="parent-path"><xsl:value-of select="$full-path"/></xsl:with-param>
            <xsl:with-param name="parent-name"><xsl:value-of select="$parent-name"/></xsl:with-param>
        </xsl:apply-templates>
        <xsl:apply-templates select="wadl:resource" mode="list-methods">
            <xsl:with-param name="parent-name"><xsl:value-of select="$parent-name"/></xsl:with-param>
            <xsl:sort select="@path"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="wadl:method">
        <xsl:param name="parent-path"/>
        <xsl:param name="parent-name"/>
        <xsl:variable name="href"><xsl:value-of select="concat($parent-name, '-', @id)"/></xsl:variable>
        <div class="method">
            <h4 id="{$href}" class="expandable">
                <span class="left">
                    <button class="expand"/>
                    <a href="#{$href}">
                        <xsl:call-template name="method-name-from-java">
                            <xsl:with-param name="text"><xsl:value-of select="@id"/></xsl:with-param>
                        </xsl:call-template>
                    </a>
                    <xsl:if test="@experimental != '' and @experimental = 'true'">
                        &#160;<span class="aui-lozenge aui-lozenge-subtle aui-lozenge-current">experimental</span>
                    </xsl:if>
                    <xsl:if test="@deprecated != '' and @deprecated = 'true'">
                        &#160;<span class="aui-lozenge">deprecated</span>
                    </xsl:if>
                </span>
                <code>
                    <xsl:value-of select="concat(@name, $hard-space, $parent-path)" />
                </code>
            </h4>
            <div class="method-body">
                <xsl:apply-templates select="wadl:doc"/>
                <xsl:apply-templates select="wadl:request" />
                <xsl:if test="wadl:response">
                    <h5>Responses</h5>
                    <xsl:apply-templates select="wadl:response"/>
                </xsl:if>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="wadl:request">
        <h5>Request</h5>
        <xsl:apply-templates select="." mode="param-group">
            <xsl:with-param name="style">query</xsl:with-param>
        </xsl:apply-templates>
        <xsl:apply-templates select="." mode="param-group">
            <xsl:with-param name="style">header</xsl:with-param>
        </xsl:apply-templates>
        <xsl:if test="wadl:representation">
            <xsl:apply-templates select="wadl:representation" mode="list"/>
        </xsl:if>
    </xsl:template>

    <xsl:template match="wadl:response">
        <xsl:apply-templates select="." mode="param-group">
            <xsl:with-param name="style">header</xsl:with-param>
        </xsl:apply-templates>
        <xsl:if test="wadl:representation">
            <ul>
                <xsl:apply-templates select="wadl:representation">
                   <xsl:sort select="../@status"/>
                </xsl:apply-templates>
            </ul>
        </xsl:if>
        <xsl:if test="wadl:fault">
            <p><em>potential faults:</em></p>
            <ul>
                <xsl:apply-templates select="wadl:fault"/>
            </ul>
        </xsl:if>
    </xsl:template>

    <xsl:template match="wadl:representation|wadl:fault">
        <li class="representation">
            <xsl:call-template name="representation-name"/>
            <xsl:apply-templates select="." mode="list"/>
        </li>
    </xsl:template>

    <xsl:template match="wadl:representation|wadl:fault" mode="list">
        <div class="representation-doc">
            <xsl:apply-templates select="wadl:doc">
                <xsl:with-param name="inline" select="1"/>
            </xsl:apply-templates>
            <xsl:if test="@element or wadl:param">
                <div class="representation">
                    <xsl:apply-templates select="." mode="param-group">
                        <xsl:with-param name="style">plain</xsl:with-param>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="." mode="param-group">
                        <xsl:with-param name="style">header</xsl:with-param>
                    </xsl:apply-templates>
                </div>
            </xsl:if>
        </div>
    </xsl:template>

    <xsl:template match="wadl:*" mode="param-group">
        <xsl:param name="style"/>
        <xsl:if test="ancestor-or-self::wadl:*/wadl:param[@style=$style]">
        <h6><xsl:value-of select="$style"/> parameters</h6>
        <table class="aui">
            <tr>
                <th>parameter</th>
                <th>type</th>
                <th>description</th>
           </tr>
            <xsl:apply-templates select="ancestor-or-self::wadl:*/wadl:param[@style=$style]"/>
        </table>
        </xsl:if>
    </xsl:template>

    <xsl:template match="wadl:param">
        <tr>
            <td>
                <code><xsl:value-of select="@name"/></code>
            </td>
            <td>
                <em><xsl:call-template name="local-qname"><xsl:with-param name="qname" select="@type"/></xsl:call-template></em>
                    <xsl:if test="@required='true'"> <small> (required)</small></xsl:if>
                    <xsl:if test="@repeating='true'"> <small> (repeating)</small></xsl:if>
                <xsl:choose>
                    <xsl:when test="wadl:option">
                        <p><em>One of:</em></p>
                        <ul>
                            <xsl:apply-templates select="wadl:option"/>
                        </ul>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:if test="@default"><p>Default: <tt><xsl:value-of select="@default"/></tt></p></xsl:if>
                        <xsl:if test="@fixed"><p>Fixed: <tt><xsl:value-of select="@fixed"/></tt></p></xsl:if>
                    </xsl:otherwise>
                </xsl:choose>
            </td>
            <td>
                <xsl:apply-templates select="wadl:doc"/>
                <xsl:if test="wadl:option[wadl:doc]">
                    <dl>
                        <xsl:apply-templates select="wadl:option" mode="option-doc"/>
                    </dl>
                </xsl:if>
                <xsl:if test="@path">
                    <ul>
                        <li>XPath to value: <tt><xsl:value-of select="@path"/></tt></li>
                        <xsl:apply-templates select="wadl:link"/>
                    </ul>
                </xsl:if>
            </td>
        </tr>
    </xsl:template>

    <xsl:template match="wadl:link">
        <li>
            Link: <a href="#{@resource_type}"><xsl:value-of select="@rel"/></a>
        </li>
    </xsl:template>

    <xsl:template match="wadl:option">
        <li>
            <tt><xsl:value-of select="@value"/></tt>
            <xsl:if test="ancestor::wadl:param[1]/@default=@value"> <small> (default)</small></xsl:if>
        </li>
    </xsl:template>

    <xsl:template match="wadl:option" mode="option-doc">
            <dt>
                <tt><xsl:value-of select="@value"/></tt>
                <xsl:if test="ancestor::wadl:param[1]/@default=@value"> <small> (default)</small></xsl:if>
            </dt>
            <dd>
                <xsl:apply-templates select="wadl:doc"/>
            </dd>
    </xsl:template>

    <xsl:template match="wadl:doc">
        <xsl:param name="inline">0</xsl:param>
        <xsl:param name="expandable">0</xsl:param>
        <xsl:choose>
            <xsl:when test="node()[1]=text() and $expandable=1 and string-length(node()[1])>100">
                <div id="{generate-id()}" class="aui-expander-content" style="min-height: 1.5em;">
                    <xsl:value-of select="node()" disable-output-escaping="yes" />
                    <a id="reveal-text-trigger" data-replace-text="Show less" data-replace-selector=".reveal-text-trigger-text" class="aui-expander-trigger aui-expander-reveal-text" aria-controls="{generate-id()}">
                        <span class="reveal-text-trigger-text">Show more</span>
                    </a>
                </div>
            </xsl:when>
            <xsl:when test="node()[1]=text() and $inline=0">
                <p>
                    <xsl:value-of select="node()" disable-output-escaping="yes" />
                    <!--<xsl:apply-templates select="node()" mode="copy"/>-->
                </p>
            </xsl:when>
            <xsl:when test="node()[1]=text() and $inline=1">
                <xsl:value-of select="node()" disable-output-escaping="yes" />
            </xsl:when>
            <xsl:otherwise>
                <div class="representation-doc-block">
                    <xsl:apply-templates select="node()" mode="copy"/>
                </div>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- utilities -->

    <xsl:template name="trimSlashes">
        <xsl:param name="path"/>

        <xsl:variable name="tmp">
            <xsl:choose>
                <xsl:when test="substring($path, string-length($path), 1) = '/'">
                    <xsl:value-of select="substring($path, 1, string-length($path) - 1)"/>
                </xsl:when>
                <xsl:otherwise><xsl:value-of select="$path"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:choose>
            <xsl:when test="substring($tmp, 1, 1) = '/'">
                <xsl:value-of select="substring($tmp, 2, string-length($tmp) - 1)"/>
            </xsl:when>
            <xsl:otherwise><xsl:value-of select="$tmp"/></xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="get-namespace-uri">
        <xsl:param name="context" select="."/>
        <xsl:param name="qname"/>
        <xsl:variable name="prefix" select="substring-before($qname,':')"/>
        <xsl:variable name="qname-ns-uri" select="$context/namespace::*[name()=$prefix]"/>
        <!-- nasty hack to get around libxsl's refusal to copy all namespace nodes when pushing nodesets around -->
        <xsl:choose>
            <xsl:when test="$qname-ns-uri">
                <xsl:value-of select="$qname-ns-uri"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="exsl:node-set($resources)/*[1]/attribute::*[namespace-uri()='urn:namespace' and local-name()=$prefix]"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="get-element">
        <xsl:param name="context" select="."/>
        <xsl:param name="qname"/>
        <xsl:variable name="localname">
            <xsl:choose>
                <xsl:when test="contains($qname, ':')">
                    <xsl:value-of select="substring-after($qname, ':')"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$qname"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:call-template name="print-type-definition">
            <xsl:with-param name="localname" select="$localname"/>
        </xsl:call-template>
    </xsl:template>

    <xsl:template name="print-type-definition">
        <xsl:param name="localname"/>
        <xsl:choose>
            <xsl:when test="exsl:node-set($grammars)/wadl:include/xs:schema/xs:element[@name=$localname]">
                <xsl:call-template name="print-source-link">
                  <xsl:with-param name="node" select="exsl:node-set($grammars)/wadl:include/xs:schema/xs:element[@name=$localname]"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="print-source-link">
                <xsl:with-param name="node" select="exsl:node-set($grammars)/wadl:include/xs:schema/xs:complexType[@name=$localname]"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="print-source-link">
        <xsl:param name="node"/>
        <xsl:variable name='source' select="$node/ancestor-or-self::wadl:include[1]/@href"/>
        <p><em>Source: <a href="{$source}"><xsl:value-of select="$source"/></a></em></p>
        <pre><xsl:apply-templates select="$node" mode="encode"/></pre>
    </xsl:template>

    <xsl:template name="local-qname">
        <xsl:param name="qname"/>
        <xsl:value-of select="substring-after($qname, ':')"/>
    </xsl:template>

    <xsl:template name="expand-qname">
        <xsl:param name="context" select="."/>
        <xsl:param name="qname"/>
        <xsl:variable name="ns-uri">
            <xsl:call-template name="get-namespace-uri">
                <xsl:with-param name="context" select="$context"/>
                <xsl:with-param name="qname" select="$qname"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:text>{</xsl:text>
        <xsl:value-of select="$ns-uri"/>
        <xsl:text>} </xsl:text>
        <xsl:value-of select="substring-after($qname, ':')"/>
    </xsl:template>

    <xsl:template name="representation-name">
        <span class="representation-name">
            <xsl:variable name="expanded-name">
                <xsl:call-template name="expand-qname">
                    <xsl:with-param select="@element" name="qname"/>
                </xsl:call-template>
            </xsl:variable>
            <xsl:if test="../@status">
                <xsl:choose>
                    <xsl:when test="../@status &lt; 400">
                        <b>
                            <span class="aui-lozenge aui-lozenge-success">Status
                                <xsl:value-of select="../@status"/>
                            </span>
                        </b>
                    </xsl:when>
                    <xsl:otherwise>
                        <b>
                            <span class="aui-lozenge aui-lozenge-error">Status
                                <xsl:value-of select="../@status"/>
                            </span>
                        </b>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:if>
            <xsl:if test="../@status and @mediaType"> - </xsl:if>
            <xsl:if test="@mediaType" >
                <i><xsl:value-of select="@mediaType"/></i>
            </xsl:if>
        </span>
    </xsl:template>

    <xsl:variable name="upper">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
    <xsl:variable name="lower">abcdefghijklmnopqrstuvwxyz</xsl:variable>

    <xsl:template name="de-camel-case">
        <xsl:param name="text" />
        <xsl:if test="$text != ''">
            <xsl:variable name="letter" select="substring($text, 1, 1)" />
            <xsl:choose>
                <xsl:when test="contains($upper, $letter)">
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="translate($letter, $upper, $lower)" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$letter"/>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:call-template name="de-camel-case">
                <xsl:with-param name="text" select="substring-after($text, $letter)" />
            </xsl:call-template>
        </xsl:if>
    </xsl:template>

    <xsl:template name="method-name-from-java">
        <xsl:param name="text" />
        <xsl:variable name="name">
            <xsl:call-template name="de-camel-case">
                <xsl:with-param name="text" select="$text"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:value-of select="concat(translate(substring($name,1,1), $lower, $upper), substring($name, 2))"/>
    </xsl:template>

    <!-- entity-encode markup for display -->

    <xsl:template match="*" mode="encode">
        <xsl:text>&lt;</xsl:text>
        <xsl:value-of select="name()"/><xsl:apply-templates select="attribute::*" mode="encode"/>
        <xsl:choose>
            <xsl:when test="*|text()">
                <xsl:text>&gt;</xsl:text>
                <xsl:apply-templates select="*|text()" mode="encode" xml:space="preserve"/>
                <xsl:text>&lt;/</xsl:text><xsl:value-of select="name()"/><xsl:text>&gt;</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>/&gt;</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="@*" mode="encode">
        <xsl:text> </xsl:text><xsl:value-of select="name()"/><xsl:text>="</xsl:text><xsl:value-of select="."/><xsl:text>"</xsl:text>
    </xsl:template>

    <xsl:template match="text()" mode="encode">
        <xsl:value-of select="." xml:space="preserve"/>
    </xsl:template>

    <!-- copy HTML for display -->

    <xsl:template match="html:*" mode="copy">
        <!-- remove the prefix on HTML elements -->
        <xsl:element name="{local-name()}">
            <xsl:for-each select="@*">
                <xsl:attribute name="{local-name()}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <xsl:apply-templates select="node()" mode="copy"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="@*|node()[namespace-uri()!='http://www.w3.org/1999/xhtml']" mode="copy">
        <!-- everything else goes straight through -->
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" mode="copy"/>
        </xsl:copy>
    </xsl:template>

</xsl:stylesheet>
