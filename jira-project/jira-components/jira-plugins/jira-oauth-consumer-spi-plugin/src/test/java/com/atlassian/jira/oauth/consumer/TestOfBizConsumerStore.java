package com.atlassian.jira.oauth.consumer;

import com.atlassian.jira.matchers.MapMatchers;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.consumer.core.ConsumerServiceStore;
import com.atlassian.oauth.util.RSAKeys;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.util.List;

import static com.atlassian.oauth.Consumer.SignatureMethod.RSA_SHA1;
import static com.atlassian.oauth.Consumer.key;
import static com.atlassian.oauth.util.RSAKeys.generateKeyPair;
import static com.atlassian.oauth.util.RSAKeys.toPemEncoding;
import static com.google.common.collect.ImmutableMap.of;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestOfBizConsumerStore {
    public static final String OAUTH_CONSUMER_TABLE_NAME = "OAuthConsumer";
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testGet() {
        final OfBizDelegator mockDelegator = mock(OfBizDelegator.class);

        final GenericValue consumerGv = new MockGenericValue(OAUTH_CONSUMER_TABLE_NAME,
                of
                        (
                                "service", "google",
                                "consumerKey", "iGoogle", "signatureMethod", "HMAC_SHA1",
                                "sharedSecret", "shhhh...",
                                "name", "myGoogle"
                        ));

        final List<GenericValue> gvList = ImmutableList.of(consumerGv);
        when(mockDelegator.findByAnd(OAUTH_CONSUMER_TABLE_NAME, of("service", "google"))).thenReturn(gvList);
        when(mockDelegator.findByAnd(OAUTH_CONSUMER_TABLE_NAME, of("service", "dontexist"))).thenReturn(ImmutableList.<GenericValue>of());

        OfBizConsumerStore store = new OfBizConsumerStore(mockDelegator);
        ConsumerServiceStore.ConsumerAndSecret cas = store.get("google");
        //should only contain one consumer.  The host conumser should not be part of this list.
        assertEquals("google", cas.getServiceName());
        assertEquals("iGoogle", cas.getConsumer().getKey());
        assertEquals("shhhh...", cas.getSharedSecret());
        assertEquals("myGoogle", cas.getConsumer().getName());

        ConsumerServiceStore.ConsumerAndSecret casnonexistant = store.get("dontexist");
        assertNull(casnonexistant);
    }

    @Test
    public void testGetByKey() {
        final OfBizDelegator mockDelegator = mock(OfBizDelegator.class);

        final GenericValue consumerGv = new MockGenericValue(OAUTH_CONSUMER_TABLE_NAME,
                of
                        (
                                "service", "google",
                                "consumerKey", "iGoogle",
                                "signatureMethod", "HMAC_SHA1",
                                "sharedSecret", "shhhh...",
                                "name", "myGoogle"
                        ));

        final List<GenericValue> gvList = ImmutableList.of(consumerGv);
        when(mockDelegator.findByAnd(OAUTH_CONSUMER_TABLE_NAME, of("consumerKey", "iGoogle"))).thenReturn(gvList);
        when(mockDelegator.findByAnd(OAUTH_CONSUMER_TABLE_NAME, of("consumerKey", "dontexist"))).thenReturn(ImmutableList.<GenericValue>of());

        OfBizConsumerStore store = new OfBizConsumerStore(mockDelegator);
        ConsumerServiceStore.ConsumerAndSecret cas = store.getByKey("iGoogle");
        //should only contain one consumer.  The host conumser should not be part of this list.
        assertEquals("google", cas.getServiceName());
        assertEquals("iGoogle", cas.getConsumer().getKey());
        assertEquals("shhhh...", cas.getSharedSecret());
        assertEquals("myGoogle", cas.getConsumer().getName());

        ConsumerServiceStore.ConsumerAndSecret casnonexistant = store.getByKey("dontexist");
        assertNull(casnonexistant);
    }

    @Test
    public void testPutInvalid() throws NoSuchAlgorithmException {
        OfBizConsumerStore store = new OfBizConsumerStore(null);

        final KeyPair pair = RSAKeys.generateKeyPair();
        final Consumer consumer = Consumer.key("iGoogle").name("myGoogle").description("The googles").
                signatureMethod(Consumer.SignatureMethod.RSA_SHA1).publicKey(pair.getPublic()).build();
        ConsumerServiceStore.ConsumerAndSecret cas = new ConsumerServiceStore.ConsumerAndSecret("blah", consumer, pair.getPrivate());

        exception.expect(Exception.class);
        store.put(null, cas);
    }

    @Test
    public void testPutInvalidConsumerAndSecret() {
        OfBizConsumerStore store = new OfBizConsumerStore(null);

        exception.expect(Exception.class);
        store.put("myservice", null);
    }

    @Test
    public void testPutNew() throws NoSuchAlgorithmException {
        final KeyPair pair = generateKeyPair();
        final Consumer consumer = key("iGoogle").name("myGoogle").description("The googles").
                signatureMethod(RSA_SHA1).publicKey(pair.getPublic()).build();
        final ConsumerServiceStore.ConsumerAndSecret cas = new ConsumerServiceStore.ConsumerAndSecret("google", consumer, pair.getPrivate());
        final OfBizDelegator ofBizDelegator = mock(OfBizDelegator.class);

        when(ofBizDelegator.findByAnd(OAUTH_CONSUMER_TABLE_NAME, of("service", "google"))).thenReturn(ImmutableList.<GenericValue>of());
        final GenericValue consumerGv = new MockGenericValue(OAUTH_CONSUMER_TABLE_NAME,
                of
                        (
                                "service", "google",
                                "consumerKey", "iGoogle",
                                "signatureMethod", "HMAC_SHA1",
                                "sharedSecret", "shhhh...",
                                "name", "myGoogle"
                        ));

        when(ofBizDelegator.createValue(eq(OAUTH_CONSUMER_TABLE_NAME),
                argThat(MapMatchers.hasEntries(
                        ImmutableMap.<String, Object>builder()
                                .put("service", "google")
                                .put("consumerKey", "iGoogle")
                                .put("signatureMethod", "RSA_SHA1")
                                .put("name", "myGoogle")
                                .put("publicKey", toPemEncoding(pair.getPublic()))
                                .put("privateKey", toPemEncoding(pair.getPrivate()))
                                .put("description", "The googles")
                                .build())))).thenReturn(consumerGv);

        OfBizConsumerStore store = new OfBizConsumerStore(ofBizDelegator);
        store.put("google", cas);
    }

    @Test
    public void testPutExisting() throws NoSuchAlgorithmException, GenericEntityException {
        final KeyPair pair = generateKeyPair();
        final Consumer consumer = key("iGoogle2").name("myGoogle2").description("The googles2").
                signatureMethod(RSA_SHA1).publicKey(pair.getPublic()).build();
        final ConsumerServiceStore.ConsumerAndSecret cas = new ConsumerServiceStore.ConsumerAndSecret("google", consumer, pair.getPrivate());
        final OfBizDelegator ofBizDelegator = new MockOfBizDelegator();
        final GenericValue consumerGv = ofBizDelegator.createValue(OAUTH_CONSUMER_TABLE_NAME,
                ImmutableMap.<String, Object>of(
                        "service", "google",
                        "consumerKey", "iGoogle",
                        "signatureMethod", "HMAC_SHA1",
                        "sharedSecret", "shhhh...",
                        "name", "myGoogle"
                )
        );

        OfBizConsumerStore store = new OfBizConsumerStore(ofBizDelegator);
        store.put("google", cas);
        final List<GenericValue> values = ofBizDelegator.findByAnd(OAUTH_CONSUMER_TABLE_NAME, ImmutableMap.of("service", "google"));

        assertThat(values, contains(consumerGv));
    }

    @Test
    public void testGetAllServiceProviders() {
        final OfBizDelegator mockDelegator = mock(OfBizDelegator.class);

        final GenericValue hostGv = new MockGenericValue(OAUTH_CONSUMER_TABLE_NAME,
                of("service", "__HOST_SERVICE__"));
        final GenericValue consumerGv = new MockGenericValue(OAUTH_CONSUMER_TABLE_NAME,
                of
                        (
                                "service", "google",
                                "consumerKey", "iGoogle",
                                "signatureMethod", "HMAC_SHA1",
                                "sharedSecret", "shhhh...",
                                "name", "myGoogle"
                        )
        );

        final List<GenericValue> gvList = ImmutableList.of(hostGv, consumerGv);
        when(mockDelegator.findAll(OAUTH_CONSUMER_TABLE_NAME)).thenReturn(gvList);

        OfBizConsumerStore store = new OfBizConsumerStore(mockDelegator);
        Iterable<Consumer> allServiceProviders = store.getAllServiceProviders();
        //should only contain one consumer.  The host conumser should not be part of this list.
        Iterator<Consumer> iterator = allServiceProviders.iterator();
        assertEquals("myGoogle", iterator.next().getName());
        assertFalse(iterator.hasNext());
    }

}
