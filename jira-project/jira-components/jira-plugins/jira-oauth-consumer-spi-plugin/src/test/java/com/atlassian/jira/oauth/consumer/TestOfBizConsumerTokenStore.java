package com.atlassian.jira.oauth.consumer;

import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.propertyset.JiraPropertySetFactory;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.oauth.consumer.ConsumerToken;
import com.atlassian.oauth.consumer.ConsumerTokenStore;
import com.opensymphony.module.propertyset.PropertySet;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.junit.MockitoJUnit;
import org.ofbiz.core.entity.GenericValue;

import java.security.PublicKey;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.3
 */
public class TestOfBizConsumerTokenStore {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Test
    public void getConsumerTokens() throws Exception {
        final OfBizDelegator mockDelegator = mock(OfBizDelegator.class);
        final ConsumerService consumerService = mock(ConsumerService.class);

        final Consumer consumer = Consumer.key("iGoogle").
                signatureMethod(Consumer.SignatureMethod.RSA_SHA1).
                name("Google").
                publicKey(mock(PublicKey.class)).
                build();
        when(consumerService.getConsumerByKey("iGoogle")).thenReturn(consumer);

        final GenericValue consumerGv1 = new MockGenericValue("OAuthConsumer",
                MapBuilder.<String, Object>newBuilder()
                        .add("tokenKey", "12343")
                        .add("tokenType", "ACCESS")
                        .add("token", "ALKLJDLKKLHKDJHLKJDHLKJFHLKDJHFJKL")
                        .add("tokenSecret", "iuywoiuhkjskljhslkfjh")
                        .add("consumerKey", "iGoogle")
                        .add("id", 789L)
                        .toMap());

        final GenericValue consumerGv2 = new com.atlassian.jira.mock.ofbiz.MockGenericValue("OAuthConsumer",
                MapBuilder.<String, Object>newBuilder()
                        .add("tokenKey", "456789")
                        .add("tokenType", "ACCESS")
                        .add("token", "KIOUEOUYOIEUYROIUEYROIUJKHDKJHFJKH")
                        .add("tokenSecret", "iuywoiuhkjskljhslkfjh")
                        .add("consumerKey", "iGoogle")
                        .add("id", 123L)
                        .toMap());

        final List<GenericValue> gvList = CollectionBuilder.newBuilder(consumerGv1, consumerGv2).asList();

        when(mockDelegator.findByAnd("OAuthConsumerToken", MapBuilder.<String, Object>newBuilder()
                .add("consumerKey", "iGoogle").toMap())).thenReturn(gvList);

        final JiraPropertySetFactory jiraPropertySetFactory = mock(JiraPropertySetFactory.class);
        PropertySet propertySet = mock(PropertySet.class);
        when(propertySet.getKeys()).thenReturn(Collections.emptyList());
        when(jiraPropertySetFactory.buildCachingPropertySet("OAuthConsumerToken", 789l, true)).thenReturn(propertySet);
        when(jiraPropertySetFactory.buildCachingPropertySet("OAuthConsumerToken", 123l, true)).thenReturn(propertySet);

        OfBizConsumerTokenStore ofBizConsumerTokenStore = new OfBizConsumerTokenStore(mockDelegator, jiraPropertySetFactory) {
            @Override
            protected ConsumerService getConsumerService() {
                return consumerService;
            }
        };
        final Map<ConsumerTokenStore.Key, ConsumerToken> tokenMap = ofBizConsumerTokenStore.getConsumerTokens("iGoogle");
        assertThat(tokenMap.keySet(), contains(new ConsumerTokenStore.Key("456789"), new ConsumerTokenStore.Key("12343")));

        final ConsumerToken token2 = tokenMap.get(new ConsumerTokenStore.Key("456789"));
        assertThat(token2.getToken(), is("KIOUEOUYOIEUYROIUEYROIUJKHDKJHFJKH"));
        assertThat(token2.isAccessToken(), is(true));
        assertThat(token2.getTokenSecret(), is("iuywoiuhkjskljhslkfjh"));
        assertThat(token2.getConsumer().getKey(), is("iGoogle"));

        final ConsumerToken token1 = tokenMap.get(new ConsumerTokenStore.Key("12343"));
        assertThat(token1.getToken(), is("ALKLJDLKKLHKDJHLKJDHLKJFHLKDJHFJKL"));
        assertThat(token1.isAccessToken(), is(true));
        assertThat(token1.getTokenSecret(), is("iuywoiuhkjskljhslkfjh"));
        assertThat(token1.getConsumer().getKey(), is("iGoogle"));
    }
}
