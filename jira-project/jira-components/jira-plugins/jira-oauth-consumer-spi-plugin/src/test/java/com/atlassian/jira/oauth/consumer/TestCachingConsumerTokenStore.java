package com.atlassian.jira.oauth.consumer;

import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.Token;
import com.atlassian.oauth.consumer.ConsumerToken;
import com.atlassian.oauth.consumer.ConsumerTokenStore;
import com.google.common.collect.ImmutableMap;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.security.Principal;
import java.util.Map;

import static com.atlassian.oauth.Consumer.SignatureMethod.HMAC_SHA1;
import static com.atlassian.oauth.Consumer.key;
import static com.atlassian.oauth.consumer.ConsumerToken.newAccessToken;
import static com.atlassian.oauth.consumer.ConsumerToken.newRequestToken;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestCachingConsumerTokenStore {
    @Rule
    public ExpectedException exception = ExpectedException.none();
    @Mock
    private EventPublisher mockEventPublisher;

    @Test
    public void testGetTokenWithInvalidArguments() {
        final ConsumerTokenStore delegateStore = mock(ConsumerTokenStore.class);
        Consumer consumer = Consumer.key("www.google.com").name("iGoogle").signatureMethod(Consumer.SignatureMethod.HMAC_SHA1).build();
        final Map<String, String> props = MapBuilder.<String, String>newBuilder().add("prop1", "val1").add("prop2", "val2").toMap();

        ConsumerToken requestToken = ConsumerToken.newRequestToken("mytoken")
                .tokenSecret("ssh...it's secret")
                .consumer(consumer)
                .properties(props)
                .build();

        when(delegateStore.get(new ConsumerTokenStore.Key("nonexistentkey"))).thenReturn(null);
        when(delegateStore.get(new ConsumerTokenStore.Key("mytoken"))).thenReturn(requestToken);

        CachingConsumerTokenStore store = new CachingConsumerTokenStore(delegateStore, mockEventPublisher, new MemoryCacheManager());

        exception.expect(IllegalArgumentException.class);
        store.get(null);
    }

    @Test
    public void testRetrievingConsumerTokenFromCache() {
        final ConsumerTokenStore delegateStore = mock(ConsumerTokenStore.class);
        Consumer consumer = Consumer.key("www.google.com").name("iGoogle").signatureMethod(Consumer.SignatureMethod.HMAC_SHA1).build();

        final Map<String, String> props = ImmutableMap.of("prop1", "val1", "prop2", "val2");

        ConsumerToken requestToken = ConsumerToken.newRequestToken("mytoken")
                .tokenSecret("ssh...it's secret")
                .consumer(consumer)
                .properties(props)
                .build();

        when(delegateStore.get(new ConsumerTokenStore.Key("nonexistentkey"))).thenReturn(null);
        final ConsumerTokenStore.Key tokenStoreKey = new ConsumerTokenStore.Key("mytoken");
        when(delegateStore.get(tokenStoreKey)).thenReturn(requestToken);

        CachingConsumerTokenStore store = new CachingConsumerTokenStore(delegateStore, mockEventPublisher, new MemoryCacheManager());

        final Token token = store.get(new ConsumerTokenStore.Key("nonexistentkey"));
        assertNull(token);

        Token goodToken = store.get(tokenStoreKey);
        assertTokenEquals(requestToken, goodToken);
        // make sure the token came from DB as it was not cached before
        verify(delegateStore).get(tokenStoreKey);

        // make sure there are no futther interactions with the database
        reset(delegateStore);
        assertTokenEquals(requestToken, store.get(tokenStoreKey));
        verify(delegateStore, times(0)).get(tokenStoreKey);
    }

    @Test
    public void testPut() {
        final ConsumerTokenStore delegateStore = mock(ConsumerTokenStore.class);
        Consumer consumer = key("www.google.com").name("iGoogle").signatureMethod(HMAC_SHA1).build();
        Principal user = new Principal() {
            public String getName() {
                return "admin";
            }
        };
        final Map<String, String> props = ImmutableMap.of("prop1", "val1", "prop2", "val2");

        ConsumerToken requestToken = newRequestToken("mytoken")
                .tokenSecret("ssh...it's secret")
                .consumer(consumer)
                .properties(props)
                .build();
        ConsumerToken accessToken = newAccessToken("mytoken")
                .tokenSecret("ssh...it's secret")
                .consumer(consumer)
                .properties(props)
                .build();

        when(delegateStore.put(new ConsumerTokenStore.Key("mytoken"), requestToken)).thenReturn(requestToken);
        when(delegateStore.get(new ConsumerTokenStore.Key("mytoken"))).thenReturn(requestToken);
        when(delegateStore.put(new ConsumerTokenStore.Key("mytoken"), accessToken)).thenReturn(accessToken);
        when(delegateStore.get(new ConsumerTokenStore.Key("mytoken"))).thenReturn(accessToken);

        CachingConsumerTokenStore store = new CachingConsumerTokenStore(delegateStore, mockEventPublisher, new MemoryCacheManager());

        try {
            store.put(null, null);
            fail("Should have thrown exception!");
        } catch (RuntimeException e) {
            //yay!
        }

        store.put(new ConsumerTokenStore.Key("mytoken"), requestToken);

        //now try to get the token a couple of times.  Should only hit the bcvking store once
        Token token = store.get(new ConsumerTokenStore.Key("mytoken"));
        assertTokenEquals(requestToken, token);
        token = store.get(new ConsumerTokenStore.Key("mytoken"));
        assertTokenEquals(requestToken, token);
        token = store.get(new ConsumerTokenStore.Key("mytoken"));
        assertTokenEquals(requestToken, token);

        //lets update the token.. This should clear the cached entry
        store.put(new ConsumerTokenStore.Key("mytoken"), accessToken);
        token = store.get(new ConsumerTokenStore.Key("mytoken"));
        assertTokenEquals(requestToken, token);
        token = store.get(new ConsumerTokenStore.Key("mytoken"));
        assertTokenEquals(requestToken, token);
    }

    @Test
    public void testRemove() {
        final ConsumerTokenStore delegateStore = mock(ConsumerTokenStore.class);
        Consumer consumer = key("www.google.com").name("iGoogle").signatureMethod(HMAC_SHA1).build();
        Principal user = new Principal() {
            public String getName() {
                return "admin";
            }
        };
        final Map<String, String> props = ImmutableMap.of("prop1", "val1", "prop2", "val2");

        ConsumerToken requestToken = newRequestToken("mytoken")
                .tokenSecret("ssh...it's secret")
                .consumer(consumer)
                .properties(props)
                .build();

        delegateStore.remove(new ConsumerTokenStore.Key("mytoken"));
        when(delegateStore.get(new ConsumerTokenStore.Key("mytoken"))).thenReturn(null);

        CachingConsumerTokenStore store = new CachingConsumerTokenStore(delegateStore, mockEventPublisher, new MemoryCacheManager());

        try {
            store.remove(null);
            fail("Should have thrown exception!");
        } catch (RuntimeException e) {
            //yay!
        }

        store.remove(new ConsumerTokenStore.Key("mytoken"));

        //now try to get the token a couple of times.  Should only hit the delegate once.
        Token token = store.get(new ConsumerTokenStore.Key("mytoken"));
        assertNull(token);
        token = store.get(new ConsumerTokenStore.Key("mytoken"));
        assertNull(token);
    }

    private void assertTokenEquals(final Token expected, final Token resultToken) {
        assertThat(resultToken, Matchers.allOf(
                Matchers.hasProperty("token", is(expected.getToken())),
                Matchers.hasProperty("tokenSecret", is(expected.getTokenSecret())),
                Matchers.hasProperty("consumer", is(expected.getConsumer())),
                Matchers.hasProperty("properties", is(expected.getProperties()))
        ));
    }
}
