# jira-analytics-whitelist-plugin

This plugin provides an Atlassian Analytics whitelist for JIRA core (i.e. non-plugin) components. The only whitelist
entries that should be added to this plugin are those which originate from non-plugins. Any analytics events triggered
from a plugin should be declared within that plugin itself.
