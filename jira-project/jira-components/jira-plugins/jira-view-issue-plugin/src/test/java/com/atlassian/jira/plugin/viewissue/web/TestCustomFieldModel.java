package com.atlassian.jira.plugin.viewissue.web;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.FieldRenderingContext;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.screen.FieldScreenRenderLayoutItem;
import com.atlassian.jira.plugin.customfield.CustomFieldTypeModuleDescriptor;
import com.atlassian.jira.util.collect.MapBuilder;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import webwork.action.Action;

import java.util.Map;

import static com.atlassian.jira.plugin.viewissue.web.FieldModel.FieldModelType.CUSTOM;
import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNull.nullValue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@RunWith(MockitoJUnitRunner.class)
public class TestCustomFieldModel {
    private static final String LENGTH_260 = StringUtils.repeat('a', 260);
    private static final String CF_ID = "CF_ID";
    private static final String CF_ELEMENT_ID = "CF_ID-val";
    private static final Object CF_VALUE = newArrayList("CF_VALUE");
    private static final String CF_KEY = "number-field";
    private static final String CF_COMPLETE_KEY = "CF_COMPLETE_KEY";
    private static final String CF_HTML = "<div></div>";
    private static final String CF_NAME = "CF_NAME";
    
    @Mock
    private CustomField customField;
    @Mock
    private Issue issue;
    @Mock
    private Action action;
    @Mock
    private CustomFieldType customFieldType;
    @Mock
    private CustomFieldTypeModuleDescriptor moduleDescriptor;
    @Mock
    private FieldScreenRenderLayoutItem layoutItem;
    @Mock
    private FieldLayoutItem fieldLayoutItem;

    @Before
    public void setUp() {
        when(customField.getId()).thenReturn(CF_ID);
        when(customField.getCustomFieldType()).thenReturn(customFieldType);
        when(customField.getValue(eq(issue))).thenReturn(CF_VALUE);
        when(customField.getName()).thenReturn(CF_NAME);
        when(customField.getViewHtml(eq(null), eq(action), eq(issue),
                eq(MapBuilder.build(FieldRenderingContext.ISSUE_VIEW, true)))).thenReturn(CF_HTML);
        when(customField.getViewHtml(eq(fieldLayoutItem), eq(action), eq(issue),
                eq(MapBuilder.build(FieldRenderingContext.ISSUE_VIEW, true)))).thenReturn(CF_HTML);
        when(layoutItem.getFieldLayoutItem()).thenReturn(fieldLayoutItem);
        when(fieldLayoutItem.getOrderableField()).thenReturn(customField);
        when(customFieldType.getDescriptor()).thenReturn(moduleDescriptor);

        when(moduleDescriptor.isViewTemplateExists()).thenReturn(true);
        when(moduleDescriptor.getKey()).thenReturn(CF_KEY);
        when(moduleDescriptor.getCompleteKey()).thenReturn(CF_COMPLETE_KEY);
    }

    @Test
    public void testGetters() {
        CustomFieldModel customFieldModel = new CustomFieldModel(fieldLayoutItem, issue, action);

        assertThat(customFieldModel.hasValue(), is(true));
        assertThat(customFieldModel.hasView(), is(true));
        assertThat(customFieldModel.getId(), is(CF_ID));
        assertThat(customFieldModel.getElementId(), is(CF_ELEMENT_ID));
        assertThat(customFieldModel.getFieldModelType(), is(CUSTOM));
        assertThat(customFieldModel.getFieldTypeCompleteKey(), is(CF_COMPLETE_KEY));
        assertThat(customFieldModel.getFieldType(), is(CF_KEY));
        assertThat(customFieldModel.getFieldHtml(), is(CF_HTML));
        assertThat(customFieldModel.getStyleClass(), is("type-number-field"));
        assertThat(customFieldModel.getName(), is(CF_NAME));
    }

    @Test
    public void testStyleClassOnShortTextAreaCF() {

        when(customField.getValue(eq(issue))).thenReturn("short text value");
        when(moduleDescriptor.getKey()).thenReturn("textarea");

        CustomFieldModel customFieldModel = new CustomFieldModel(fieldLayoutItem, issue, action);

        assertThat(customFieldModel.getStyleClass(), is("type-textarea"));
    }

    @Test
    public void testStyleClassOnLongTextAreaCF() {

        when(customField.getValue(eq(issue))).thenReturn(LENGTH_260);
        when(moduleDescriptor.getKey()).thenReturn("textarea");

        CustomFieldModel customFieldModel = new CustomFieldModel(fieldLayoutItem, issue, action);

        assertThat(customFieldModel.getStyleClass(), is("type-textarea twixified"));
    }

    @Test
    public void testViewNotCalculatedWhenNoViewAvailable() {
        when(moduleDescriptor.isViewTemplateExists()).thenReturn(false);

        CustomFieldModel customFieldModel = new CustomFieldModel(fieldLayoutItem, issue, action);
        assertThat(customFieldModel.getFieldHtml(), nullValue());

        verify(customField, never()).getViewHtml(any(FieldLayoutItem.class), any(Action.class), any(Issue.class), any(Map.class));
    }

    @Test
    public void testFieldLayoutItemConstructor() {
        CustomFieldModel customFieldModel = new CustomFieldModel(fieldLayoutItem, issue, action);
        assertThat(customFieldModel.getFieldHtml(), is(CF_HTML));

        verify(customField, times(1)).getViewHtml(eq(fieldLayoutItem), eq(action), eq(issue),
                eq(MapBuilder.build(FieldRenderingContext.ISSUE_VIEW, true)));
    }
}