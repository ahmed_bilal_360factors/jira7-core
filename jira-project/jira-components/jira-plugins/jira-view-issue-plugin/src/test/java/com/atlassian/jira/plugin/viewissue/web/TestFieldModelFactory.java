package com.atlassian.jira.plugin.viewissue.web;

import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.OrderableField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayout;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import com.atlassian.jira.plugin.customfield.CustomFieldTypeModuleDescriptor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import webwork.action.Action;

import java.util.List;
import java.util.Map;

import static com.atlassian.jira.plugin.viewissue.web.FieldModelFactory.COMPOSED_BY_FIELDS;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestFieldModelFactory {
    private static final String ID_1 = "ID_1";
    private static final String ID_2 = "ID_2";
    private static final String CF_ID = "CF_ID";
    
    @InjectMocks
    private FieldModelFactory fieldModelFactory;

    @Mock
    private Field field1;
    @Mock
    private Field field2;
    @Mock
    private CustomField customField;
    @Mock
    private Issue issue;
    @Mock
    private Action action;
    @Mock
    private CustomFieldType customFieldType;
    @Mock
    private DateFieldModelFactory dateFieldModelFactory;
    @Mock
    private CustomFieldTypeModuleDescriptor descriptor;
    @Mock
    private FieldLayoutManager fieldLayoutManager;
    @Mock
    private FieldLayout fieldLayout;

    private Map<String, Object> context;

    @Before
    public void setUp() throws Exception {
        when(field1.getId()).thenReturn(ID_1);
        when(field2.getId()).thenReturn(ID_2);
        when(customField.getId()).thenReturn(CF_ID);
        when(customField.getCustomFieldType()).thenReturn(customFieldType);
        when(customFieldType.getDescriptor()).thenReturn(descriptor);

        when(fieldLayoutManager.getFieldLayout(eq(issue))).thenReturn(fieldLayout);
        
        when(fieldLayout.getFieldLayoutItem(any(OrderableField.class))).thenAnswer(invocationOnMock -> {
            OrderableField field = invocationOnMock.getArgumentAt(0, OrderableField.class);
            FieldLayoutItem layoutItem = mock(FieldLayoutItem.class);
            when(layoutItem.getOrderableField()).thenReturn(field);
            return layoutItem;
        });

        when(dateFieldModelFactory.isDateSystemField(any(Field.class))).thenReturn(false);

        List<Field> fieldOrder = newArrayList(field1, field2);
        context = newHashMap();
        context.put(COMPOSED_BY_FIELDS, fieldOrder);
        context.put("issue", issue);
        context.put("action", action);
    }

    @Test
    public void testHasFieldOrder() throws Exception {
        boolean result = fieldModelFactory.hasRequestedFields(context);

        assertThat(result, is(true));
    }

    @Test
    public void testHasNoFieldOrder() throws Exception {
        context.clear();
        boolean result = fieldModelFactory.hasRequestedFields(context);

        assertThat(result, is(false));
    }

    @Test
    public void testHasFieldOrderEvenWithEmptyList() throws Exception {
        ((List) context.get(COMPOSED_BY_FIELDS)).clear();

        boolean result = fieldModelFactory.hasRequestedFields(context);

        assertThat(result, is(true));
    }

    @Test
    public void testCanHandleNoIssueAndActionInContext() throws Exception {
        context.put(COMPOSED_BY_FIELDS, newArrayList(customField, field1, field2));
        context.remove("issue");
        context.remove("action");
        when(fieldLayoutManager.getFieldLayout()).thenReturn(fieldLayout);

        List<FieldModel> fieldModels = fieldModelFactory.getFieldModels(context);
        assertThat(fieldModels, hasSize(3));
    }

    @Test
    public void testGetFieldModels() throws Exception {
        List<FieldModel> fieldModels = fieldModelFactory.getFieldModels(context);
        assertThat(fieldModels, hasSize(2));

        FieldModel model1 = fieldModels.get(0);
        FieldModel model2 = fieldModels.get(1);
        assertThat(model1.getId(), is(ID_1));
        assertThat(model2.getId(), is(ID_2));
    }

    @Test
    public void testGetCustomAndSystemFieldModels() throws Exception {
        context.put(COMPOSED_BY_FIELDS, newArrayList(customField, field1, field2));

        List<FieldModel> fieldModels = fieldModelFactory.getFieldModels(context);

        assertThat(fieldModels, hasSize(3));

        FieldModel cfmodel = fieldModels.get(0);
        FieldModel model1 = fieldModels.get(1);
        FieldModel model2 = fieldModels.get(2);

        assertThat(cfmodel.getId(), is(CF_ID));
        assertThat(model1.getId(), is(ID_1));
        assertThat(model2.getId(), is(ID_2));

        assertThat(model1, instanceOf(SystemFieldModel.class));
        assertThat(cfmodel, instanceOf(CustomFieldModel.class));
    }

    @Test
    public void testDateFields() throws Exception {
        DateFieldModel dateModel = mock(DateFieldModel.class);
        when(dateFieldModelFactory.isDateSystemField(eq(field2))).thenReturn(true);
        when(dateFieldModelFactory.toModel(eq(field2), eq(issue))).thenReturn(dateModel);

        when(field2.getId()).thenReturn(IssueFieldConstants.DUE_DATE);
        
        List<FieldModel> fieldModels = fieldModelFactory.getFieldModels(context);
        
        assertThat(fieldModels, hasSize(2));
        FieldModel model1 = fieldModels.get(0);
        FieldModel model2 = fieldModels.get(1);
        
        assertThat(model1.getId(), is(ID_1));
        assertThat(model2, is(dateModel));
    }
}