package com.atlassian.jira.plugin.viewissue;

import com.atlassian.jira.bean.SubTaskBean;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugin.viewissue.subtasks.SubTaskViewOptionsFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.velocity.VelocityRequestContext;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.util.velocity.VelocityRequestSession;
import com.atlassian.jira.web.SessionKeys;
import com.atlassian.plugin.web.api.WebItem;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import org.hamcrest.collection.IsIterableWithSize;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestSubTaskViewOptionsFactory {
    @Mock
    VelocityRequestContextFactory requestContextFactory;

    @Mock
    JiraAuthenticationContext authenticationContext;

    @Mock
    VelocityRequestContext requestContext;

    @Mock
    VelocityRequestSession velocityRequestSession;

    @Mock
    I18nHelper i18n;

    @Mock
    Issue issue;

    Map<String, Object> context;

    final String issueKey = "issueKey";
    final String baseUrl = "baseUrl";

    Collection<String> links = ImmutableList.of(
            "/browse/(.+)\\?subTaskView=all#issuetable",
            "/browse/(.+)\\?subTaskView=unresolved#issuetable",
            "/issue/bulkedit/BulkEdit1!default.jspa\\?reset=true&searchParent=(.+)",
            "/issues/\\?jql=parent%3D(.+)"
    );

    Collection<String> linksUnresolved = ImmutableList.of(
            "/browse/(.+)\\?subTaskView=all#issuetable",
            "/browse/(.+)\\?subTaskView=unresolved#issuetable",
            "/issue/bulkedit/BulkEdit1!default.jspa\\?reset=true&searchParent=(.+)&searchMode=unresolved",
            "/issues/\\?jql=parent%3D(.+)\\+AND\\+resolution%3DUnresolved"
    );

    SubTaskViewOptionsFactory subTaskViewOptionsFactory;

    @Before
    public void setUp() {
        subTaskViewOptionsFactory = new SubTaskViewOptionsFactory(requestContextFactory, authenticationContext);
        when(requestContextFactory.getJiraVelocityRequestContext()).thenReturn(requestContext);
        when(authenticationContext.getI18nHelper()).thenReturn(i18n);
        when(issue.getKey()).thenReturn(issueKey);
        when(i18n.getText(Matchers.anyString())).thenAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            return (String) args[0];
        });
        when(requestContext.getBaseUrl()).thenReturn(baseUrl);
        when(requestContext.getSession()).thenReturn(velocityRequestSession);
        when(velocityRequestSession.getAttribute(SessionKeys.SUB_TASK_VIEW)).thenReturn("");
        this.context = ImmutableMap.of("issue", issue);
    }

    @Test
    public void checkProperLinkNumber() {
        Integer count = 0;
        Iterable<WebItem> items = subTaskViewOptionsFactory.getItems(context);
        for (WebItem wi : items) {
            count++;
        }
        assertThat("Number of returned links should be " + links.size(), count, org.hamcrest.Matchers.is(links.size()));
    }

    //check for proper links
    @Test
    public void checkProperUrl() {
        final Iterable<WebItem> items = subTaskViewOptionsFactory.getItems(context);
        for (WebItem wi : items) {
            String urlUT = wi.getUrl();
            //try to match
            boolean matched = false;
            for (String urlRegexp : links) {
                if (urlUT.matches(baseUrl + urlRegexp)) {
                    matched = true;
                    break;
                }
            }
            if (!matched) {
                fail(String.format("Unknown url %s", urlUT));
            }
        }
    }

    @Test
    public void checkDifferentWeights() {
        Set<Integer> weights = new HashSet<Integer>();
        final Iterable<WebItem> items = subTaskViewOptionsFactory.getItems(context);
        items.forEach(wi -> weights.add(wi.getWeight()));

        assertThat("Each link should have different weight", weights.size(), org.hamcrest.Matchers.is(links.size()));
    }

    @Test
    public void checkDifferentUrls() {
        final Set<String> links = new HashSet<String>();
        final Iterable<WebItem> items = subTaskViewOptionsFactory.getItems(context);
        items.forEach(wi -> links.add(wi.getUrl()));

        assertThat("Each link should have different url", links.size(), org.hamcrest.Matchers.is(this.links.size()));
    }

    @Test
    public void checkDifferentId() {
        final Set<String> ids = new HashSet<String>();
        final Iterable<WebItem> items = subTaskViewOptionsFactory.getItems(context);
        items.forEach(wi -> ids.add(wi.getId()));

        assertThat("Each link should have different id", ids.size(), org.hamcrest.Matchers.is(this.links.size()));
    }

    @Test
    public void checkOnlyOneCheckedOptionInViewAll() {
        when(velocityRequestSession.getAttribute(SessionKeys.SUB_TASK_VIEW)).thenReturn(SubTaskBean.SUB_TASK_VIEW_ALL);

        final Iterable<WebItem> items = subTaskViewOptionsFactory.getItems(context);

        assertThat(items, IsIterableWithSize.iterableWithSize(links.size()));
        final Iterable<WebItem> checked = Iterables.filter(items, (item) -> {
            return Arrays.asList(item.getStyleClass().split(" ")).contains("aui-checked");
        });
        assertThat("Only one item should be checked", Iterables.size(checked), equalTo(1));
    }

    @Test
    public void checkOnlyOneCheckedOptionInViewUnresolved() {
        when(velocityRequestSession.getAttribute(SessionKeys.SUB_TASK_VIEW)).thenReturn(SubTaskBean.SUB_TASK_VIEW_UNRESOLVED);

        final Iterable<WebItem> items = subTaskViewOptionsFactory.getItems(context);

        assertThat(items, IsIterableWithSize.iterableWithSize(links.size()));
        final Iterable<WebItem> checked = Iterables.filter(items, (item) -> {
            return Arrays.asList(item.getStyleClass().split(" ")).contains("aui-checked");
        });
        assertThat("Only one item should be checked", Iterables.size(checked), equalTo(1));
    }

    @Test
    public void checkProperUrlInViewUnresolved() {
        when(velocityRequestSession.getAttribute(SessionKeys.SUB_TASK_VIEW)).thenReturn(SubTaskBean.SUB_TASK_VIEW_UNRESOLVED);

        final Iterable<WebItem> items = subTaskViewOptionsFactory.getItems(context);
        for (WebItem wi : items) {
            String urlUT = wi.getUrl();
            //try to match
            boolean matched = false;
            for (String urlRegexp : linksUnresolved) {
                if (urlUT.matches(baseUrl + urlRegexp)) {
                    matched = true;
                    break;
                }
            }
            if (!matched) {
                fail(String.format("Unknown url %s", urlUT));
            }
        }
    }

}
