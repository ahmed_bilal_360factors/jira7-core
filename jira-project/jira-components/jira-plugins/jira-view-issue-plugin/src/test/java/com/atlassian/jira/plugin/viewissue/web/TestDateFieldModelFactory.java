package com.atlassian.jira.plugin.viewissue.web;

import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.DateFieldFormat;
import com.atlassian.jira.util.I18nHelper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.Timestamp;

import static com.atlassian.jira.datetime.DateTimeStyle.COMPLETE;
import static com.atlassian.jira.datetime.DateTimeStyle.ISO_8601_DATE;
import static com.atlassian.jira.datetime.DateTimeStyle.ISO_8601_DATE_TIME;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestDateFieldModelFactory {
    // Field types:
    private static final String DATETIME = "datetime";
    private static final String DATEPICKER = "datepicker";

    // Default style class:
    private static final String USER_TZ = "user-tz";

    private static final String DUE_I18N_NAME = "issue.field.due";
    private static final String DUE_NAME = "Due";
    private static final String FIELD_NAME = "Field Name";

    private static final String DATE_STRING = "1/2/3";

    @InjectMocks
    private DateFieldModelFactory dateFieldModelFactory;

    @Mock
    private Field field;
    @Mock
    private Issue issue;
    @Mock
    private Timestamp RESOLUTION_DATE;
    @Mock
    private Timestamp CREATED_DATE;
    @Mock
    private Timestamp UPDATED_DATE;
    @Mock
    private Timestamp DUE_DATE;
    @Mock
    private DateTimeFormatter dateTimeFormatter;
    @Mock
    private DateTimeFormatterFactory dateTimeFormatterFactory;
    @Mock
    private DateFieldFormat dateFieldFormat;
    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;
    @Mock
    private I18nHelper i18nHelper;

    @Before
    public void setUp() {
        when(issue.getResolutionDate()).thenReturn(RESOLUTION_DATE);
        when(issue.getCreated()).thenReturn(CREATED_DATE);
        when(issue.getUpdated()).thenReturn(UPDATED_DATE);
        when(issue.getDueDate()).thenReturn(DUE_DATE);

        DateTimeFormatter defaultDateTimeFormatter = mock(DateTimeFormatter.class);
        when(dateTimeFormatterFactory.formatter()).thenReturn(defaultDateTimeFormatter);
        when(defaultDateTimeFormatter.forLoggedInUser()).thenReturn(dateTimeFormatter);

        when(dateTimeFormatter.withStyle(argThat(anyOf(is(ISO_8601_DATE_TIME), is(ISO_8601_DATE), is(COMPLETE)))))
                .thenReturn(dateTimeFormatter);
        when(dateTimeFormatter.withSystemZone()).thenReturn(dateTimeFormatter);

        when(field.getName()).thenReturn(FIELD_NAME);

        when(jiraAuthenticationContext.getI18nHelper()).thenReturn(i18nHelper);
        when(i18nHelper.getText(eq(DUE_I18N_NAME))).thenReturn(DUE_NAME);
    }

    @Test
    public void testWhenDateSystemField() {
        when(field.getId()).thenReturn(IssueFieldConstants.UPDATED);

        boolean result = dateFieldModelFactory.isDateSystemField(field);

        assertThat(result, is(true));
    }

    @Test
    public void testWhenNotSystemDateField() {
        when(field.getId()).thenReturn(IssueFieldConstants.ASSIGNEE);

        boolean result = dateFieldModelFactory.isDateSystemField(field);

        assertThat(result, is(false));
    }

    @Test
    public void testWhenFieldIsCustomField() {
        when(field.getId()).thenReturn("customfield_12345");

        boolean result = dateFieldModelFactory.isDateSystemField(field);

        assertThat(result, is(false));
    }

    @Test
    public void testUpdatedFieldToModel() {
        when(field.getId()).thenReturn(IssueFieldConstants.UPDATED);
        when(dateTimeFormatter.format(UPDATED_DATE)).thenReturn(DATE_STRING);

        DateFieldModel dateFieldModel = dateFieldModelFactory.toModel(field, issue);

        assertDateTimeFieldsCorrect(dateFieldModel, IssueFieldConstants.UPDATED);
    }

    @Test
    public void testResolvedDateFieldToModel() {
        when(field.getId()).thenReturn(IssueFieldConstants.RESOLUTION_DATE);
        when(dateTimeFormatter.format(RESOLUTION_DATE)).thenReturn(DATE_STRING);

        DateFieldModel dateFieldModel = dateFieldModelFactory.toModel(field, issue);

        assertDateTimeFieldsCorrect(dateFieldModel, IssueFieldConstants.RESOLUTION_DATE);
    }

    @Test
    public void testCreatedDateFieldToModel() {
        when(field.getId()).thenReturn(IssueFieldConstants.CREATED);
        when(dateTimeFormatter.format(CREATED_DATE)).thenReturn(DATE_STRING);

        DateFieldModel dateFieldModel = dateFieldModelFactory.toModel(field, issue);

        assertDateTimeFieldsCorrect(dateFieldModel, IssueFieldConstants.CREATED);
    }

    @Test
    public void testHasValue() {
        when(field.getId()).thenReturn(IssueFieldConstants.RESOLUTION_DATE);
        when(issue.getResolutionDate()).thenReturn(null);
        when(dateTimeFormatter.format(eq(null))).thenReturn(DATE_STRING);

        DateFieldModel dateFieldModel = dateFieldModelFactory.toModel(field, issue);

        assertThat(dateFieldModel.hasValue(), is(false));
    }

    @Test
    public void testHasValueForDueDate() {
        when(field.getId()).thenReturn(IssueFieldConstants.DUE_DATE);
        when(issue.getDueDate()).thenReturn(null);
        when(dateFieldFormat.format(null)).thenReturn(DATE_STRING);
        when(dateTimeFormatter.format(null)).thenReturn(DATE_STRING);

        DateFieldModel dateFieldModel = dateFieldModelFactory.toModel(field, issue);

        assertThat(dateFieldModel.hasValue(), is(false));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testThrowsExceptionForNonDateSystemField() {
        when(field.getId()).thenReturn(IssueFieldConstants.ASSIGNEE);

        dateFieldModelFactory.toModel(field, issue);
    }
    
    @Test
    public void testDueDateFieldToModel() {
        when(field.getId()).thenReturn(IssueFieldConstants.DUE_DATE);
        when(dateFieldFormat.format(DUE_DATE)).thenReturn(DATE_STRING);
        when(dateTimeFormatter.format(DUE_DATE)).thenReturn(DATE_STRING);

        DateFieldModel dateFieldModel = dateFieldModelFactory.toModel(field, issue);

        assertThat(dateFieldModel, instanceOf(DueDateFieldModel.class));
        assertThat(dateFieldModel.getFieldHtml(), is(DATE_STRING));
        assertThat(dateFieldModel.getName(), is(DUE_NAME));
        assertThat(dateFieldModel.getTitle(), is(DATE_STRING));
        assertThat(dateFieldModel.getIso8601(), is(DATE_STRING));
        assertThat(dateFieldModel.getFieldType(), is(DATEPICKER));
        assertThat(dateFieldModel.getElementId(), is("due-date"));
        assertThat(dateFieldModel.hasValue(), is(true));
    }

    private void assertDateTimeFieldsCorrect(DateFieldModel dateFieldModel, String fieldId) {
        assertThat(dateFieldModel.getFieldHtml(), is(DATE_STRING));
        assertThat(dateFieldModel.getName(), is(FIELD_NAME));
        assertThat(dateFieldModel.getStyleClass(), is(USER_TZ));
        assertThat(dateFieldModel.getFieldType(), is(DATETIME));
        assertThat(dateFieldModel.getElementId(), is(fieldId + "-val"));
        assertThat(dateFieldModel.hasValue(), is(true));
    }
}