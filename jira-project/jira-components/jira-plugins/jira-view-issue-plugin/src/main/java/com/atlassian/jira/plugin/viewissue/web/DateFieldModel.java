package com.atlassian.jira.plugin.viewissue.web;

import com.atlassian.jira.issue.fields.Field;

/**
 * Field model to render date time system fields.
 */
public class DateFieldModel extends SystemFieldModel {
    protected String fieldType;
    protected String iso8601;
    protected String title;
    protected String fieldHtml;
    protected String styleClass;
    protected String key;

    DateFieldModel(Field dateTimeField, String fieldHtml, String iso8601, String title) {
        super(dateTimeField);
        this.fieldHtml = fieldHtml;
        this.iso8601 = iso8601;
        this.title = title;
        this.styleClass = "user-tz";
        this.fieldType = "datetime";
    }

    public String getIso8601() {
        return iso8601;
    }

    public String getFieldType() {
        return fieldType;
    }

    public String getTitle() {
        return title;
    }

    public String getFieldHtml() {
        return fieldHtml;
    }

    public String getStyleClass() {
        return styleClass;
    }

    public boolean hasValue() {
        return fieldHtml != null;
    }

    public String getKey() {
        return key;
    }
}
