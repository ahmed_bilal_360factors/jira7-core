package com.atlassian.jira.plugin.viewissue.web;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.screen.FieldScreenRenderLayoutItem;
import com.atlassian.jira.issue.fields.screen.FieldScreenRenderTab;
import webwork.action.Action;

import java.util.List;
import java.util.stream.Collectors;

public class SimpleTab {
    final private String name;
    final private List<CustomFieldModel> fields;

    public SimpleTab(FieldScreenRenderTab tab, Issue issue, Action action) {
        name = tab.getName();

        final List<FieldScreenRenderLayoutItem> layoutItems = tab.getFieldScreenRenderLayoutItems();
        fields = layoutItems.stream()
                .map(layoutItem -> layoutItemToModel(layoutItem, issue, action))
                .collect(Collectors.toList());
    }

    public String getName() {
        return name;
    }

    public List<CustomFieldModel> getFields() {
        return fields;
    }

    private CustomFieldModel layoutItemToModel(FieldScreenRenderLayoutItem layoutItem, Issue issue, Action action) {
        return new CustomFieldModel(layoutItem.getFieldLayoutItem(), issue, action);
    }
}
