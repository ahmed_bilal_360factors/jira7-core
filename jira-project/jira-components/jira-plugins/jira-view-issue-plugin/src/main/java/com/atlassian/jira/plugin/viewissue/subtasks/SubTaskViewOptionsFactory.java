package com.atlassian.jira.plugin.viewissue.subtasks;

import com.atlassian.jira.bean.SubTaskBean;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.jira.util.velocity.VelocityRequestContext;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.util.velocity.VelocityRequestSession;
import com.atlassian.jira.web.SessionKeys;
import com.atlassian.plugin.web.api.WebItem;
import com.atlassian.plugin.web.api.model.WebFragmentBuilder;
import com.atlassian.plugin.web.api.provider.WebItemProvider;
import org.apache.commons.lang.StringUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

/**
 * Factory to return the options for the different views for subtask list (All, Unresolved)
 *
 * @since v4.4
 */
public class SubTaskViewOptionsFactory implements WebItemProvider {
    private static final String ITEM_SECTION = "com.atlassian.jira.jira-view-issue-plugin:view-subtasks/drop/subtask-view-options";

    private final VelocityRequestContextFactory requestContextFactory;
    private final JiraAuthenticationContext authenticationContext;

    public SubTaskViewOptionsFactory(VelocityRequestContextFactory requestContextFactory, JiraAuthenticationContext authenticationContext) {
        this.requestContextFactory = requestContextFactory;
        this.authenticationContext = authenticationContext;
    }

    @Override
    public Iterable<WebItem> getItems(final Map<String, Object> context) {
        final VelocityRequestContext requestContext = requestContextFactory.getJiraVelocityRequestContext();
        final I18nHelper i18n = authenticationContext.getI18nHelper();
        final Issue issue = (Issue) context.get("issue");


        final VelocityRequestSession session = requestContext.getSession();
        final String baseUrl = requestContext.getBaseUrl();
        final String subTaskView = (String) session.getAttribute(SessionKeys.SUB_TASK_VIEW);
        boolean showingAll = SubTaskBean.SUB_TASK_VIEW_DEFAULT.equals(SubTaskBean.SUB_TASK_VIEW_ALL);
        if (StringUtils.isNotBlank(subTaskView)) {
            showingAll = subTaskView.equals(SubTaskBean.SUB_TASK_VIEW_ALL);
        }

        final WebItem allLink = new WebFragmentBuilder(10).
                id("subtasks-show-all").
                label(i18n.getText("viewissue.subtasks.tab.show.all.subtasks")).
                title(i18n.getText("viewissue.subtasks.tab.show.all.subtasks")).
                styleClass(showingAll ? "aui-list-checked aui-checked" : "aui-list-checked").
                webItem(ITEM_SECTION).
                url(baseUrl + "/browse/" + issue.getKey() + "?subTaskView=all#issuetable").build();
        final WebItem openLink = new WebFragmentBuilder(20).
                id("subtasks-show-open").
                label(i18n.getText("viewissue.subtasks.tab.show.open.subtasks")).
                title(i18n.getText("viewissue.subtasks.tab.show.open.subtasks")).
                styleClass(!showingAll ? "aui-list-checked aui-checked" : "aui-list-checked").
                webItem(ITEM_SECTION).
                url(baseUrl + "/browse/" + issue.getKey() + "?subTaskView=unresolved#issuetable").build();

        final String urlParams = showingAll ? "" : "&searchMode=unresolved";
        final WebItem bulkOperationLink = new WebFragmentBuilder(30).
                id("subtasks-bulk-operation").
                label(i18n.getText("viewissue.subtasks.tab.bulk.operation")).
                title(i18n.getText("viewissue.subtasks.tab.bulk.operation")).
                styleClass("aui-list-checked").
                webItem(ITEM_SECTION).
                url(baseUrl + "/issue/bulkedit/BulkEdit1!default.jspa?reset=true&searchParent=" + issue.getKey() + urlParams).build();

        final String jql = showingAll ? "parent=" + issue.getKey() : "parent=" + issue.getKey() + " AND resolution=Unresolved";
        final WebItem openInIssueNavigator = new WebFragmentBuilder(40).
                id("subtasks-open-issue-navigator").
                label(i18n.getText("viewissue.subtasks.tab.open.issue.navigator")).
                title(i18n.getText("viewissue.subtasks.tab.open.issue.navigator")).
                styleClass("aui-list-checked").
                webItem(ITEM_SECTION).
                url(baseUrl + "/issues/?jql=" + encode(jql)).build();


        return CollectionBuilder.list(allLink, openLink, bulkOperationLink, openInIssueNavigator);
    }

    private String encode(String data) {
        try {
            return URLEncoder.encode(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
}
