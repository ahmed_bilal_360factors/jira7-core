package com.atlassian.jira.plugin.viewissue.web;

import com.atlassian.jira.issue.fields.Field;

public abstract class FieldModel {
    public static enum FieldModelType {
        SYSTEM,
        CUSTOM
    }

    protected String id;
    protected String name;

    protected FieldModel(Field field) {
        this.id = field.getId();
        this.name = field.getName();
    }

    public abstract FieldModelType getFieldModelType();

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getElementId() {
        return getId() + "-val";
    }
}

