package com.atlassian.jira.plugin.viewissue.subtasks;

import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.webresource.api.data.WebResourceDataProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

/**
 * Data provider to supply different properties for subtasks client-side scripts
 *
 * @since v7.2
 */
public class SubTasksWebDataProvider implements WebResourceDataProvider {
    private final static Logger log = LoggerFactory.getLogger(SubTasksWebDataProvider.class);

    @Override
    public Jsonable get() {
        return writer -> {
            try {
                getData().write(writer);
            } catch (JSONException e) {
                log.error("Unable to prepare data for subtasks panel client-side controller", e);
                throw new RuntimeException(e);
            }
        };
    }

    private JSONObject getData() {
        return new JSONObject(new HashMap<String, Object>(){{
            put("url", "/rest/api/2/issue/{issueId}/subtask/move");
        }});
    }
}
