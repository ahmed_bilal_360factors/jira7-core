package com.atlassian.jira.plugin.viewissue;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.screen.FieldScreenRenderLayoutItem;
import com.atlassian.jira.issue.fields.screen.FieldScreenRenderer;
import com.atlassian.jira.issue.fields.screen.FieldScreenRendererFactory;
import com.atlassian.jira.issue.fields.util.FieldPredicates;
import com.atlassian.jira.issue.operation.IssueOperations;
import com.atlassian.jira.issue.vote.VoteManager;
import com.atlassian.jira.issue.watchers.WatcherManager;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.plugin.profile.UserFormatManager;
import com.atlassian.jira.plugin.viewissue.web.CustomFieldModel;
import com.atlassian.jira.plugin.viewissue.web.FieldModel;
import com.atlassian.jira.plugin.viewissue.web.FieldModelFactory;
import com.atlassian.jira.plugin.viewissue.web.SystemFieldModel;
import com.atlassian.jira.plugin.webfragment.CacheableContextProvider;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.web.FieldVisibilityManager;
import com.atlassian.plugin.PluginParseException;
import org.apache.commons.lang.StringUtils;
import webwork.action.Action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static java.util.stream.Collectors.toList;

/**
 * Context Provider for the People Block pnn view issue
 *
 * @since v4.4
 */
public class PeopleBlockContextProvider implements CacheableContextProvider {
    private static final String COMPOSED_BY_FIELDS = "composedByFields";
    private final ApplicationProperties applicationProperties;
    private final AvatarService avatarService;
    private final JiraAuthenticationContext authenticationContext;
    private final FieldScreenRendererFactory fieldScreenRendererFactory;
    private final FieldVisibilityManager fieldVisibilityManager;
    private final IssueManager issueManager;
    private final PermissionManager permissionManager;
    private final UserFormatManager userFormatManager;
    private final VoteManager voteManager;
    private final WatcherManager watcherManager;
    private FieldModelFactory fieldModelFactory;

    public PeopleBlockContextProvider(
            final ApplicationProperties applicationProperties,
            final AvatarService avatarService,
            final JiraAuthenticationContext authenticationContext,
            final FieldScreenRendererFactory fieldScreenRendererFactory,
            final FieldVisibilityManager fieldVisibilityManager,
            final IssueManager issueManager,
            final PermissionManager permissionManager,
            final UserFormatManager userFormatManager,
            final VoteManager voteManager,
            final WatcherManager watcherManager,
            final FieldModelFactory fieldModelFactory) {
        this.applicationProperties = applicationProperties;
        this.avatarService = avatarService;
        this.authenticationContext = authenticationContext;
        this.fieldScreenRendererFactory = fieldScreenRendererFactory;
        this.fieldVisibilityManager = fieldVisibilityManager;
        this.issueManager = issueManager;
        this.permissionManager = permissionManager;
        this.userFormatManager = userFormatManager;
        this.voteManager = voteManager;
        this.watcherManager = watcherManager;
        this.fieldModelFactory = fieldModelFactory;
    }

    @Override
    public void init(Map<String, String> params) throws PluginParseException {
    }

    @Override
    public Map<String, Object> getContextMap(Map<String, Object> context) {
        final Issue issue = (Issue) context.get("issue");
        final ApplicationUser user = authenticationContext.getLoggedInUser();
        final Action action = (Action) context.get("action");

        final MapBuilder<String, Object> paramsBuilder = MapBuilder.newBuilder(context);

        paramsBuilder.add("issue", issue);
        paramsBuilder.add("user", user);
        paramsBuilder.add("i18n", authenticationContext.getI18nHelper());
        paramsBuilder.add("peopleComponent", this);
        paramsBuilder.add("assigneeVisible", isAssigneeVisible(issue));
        paramsBuilder.add("reporterVisible", isReporterVisible(issue));
        paramsBuilder.add("showAssignToMe", showAssignToMe(user, issue));
        List<String> watchers = watcherManager.getCurrentWatcherUsernames(issue);
        paramsBuilder.add("watchers", watchers);
        paramsBuilder.add("watching", user != null && watchers.contains(user.getUsername()));
        paramsBuilder.add("voting", voteManager.hasVoted(user, issue));
        paramsBuilder.add("isResolved", issue.getResolution() != null);
        final String reporterId = issue.getReporterId();
        paramsBuilder.add("isCurrentUserReporter", StringUtils.isNotBlank(reporterId) && user != null && reporterId.equals(user.getKey()));
        //need to be logged in in order to toggle watching/voting for an issue
        paramsBuilder.add("isLoggedIn", user != null);
        paramsBuilder.add("votingEnabled", applicationProperties.getOption(APKeys.JIRA_OPTION_VOTING));
        paramsBuilder.add("watchingEnabled", applicationProperties.getOption(APKeys.JIRA_OPTION_WATCHING));
        paramsBuilder.add("canManageWatcherList", permissionManager.hasPermission(ProjectPermissions.MANAGE_WATCHERS, issue, user));
        paramsBuilder.add("canViewVotersAndWatchers", permissionManager.hasPermission(ProjectPermissions.VIEW_VOTERS_AND_WATCHERS, issue, user));

        if (fieldModelFactory.hasRequestedFields(context)) {
            paramsBuilder.add("fields", fieldModelFactory.getFieldModels(context));
        } else {
            paramsBuilder.add("userCustomFields", createCustomFieldModels(issue, action));
        }

        return paramsBuilder.toMap();
    }

    private boolean showAssignToMe(ApplicationUser currentUser, Issue issue) {
        return currentUser != null
                && !currentUser.getKey().equals(issue.getAssigneeId())
                && permissionManager.hasPermission(ProjectPermissions.ASSIGN_ISSUES, issue, currentUser)
                && permissionManager.hasPermission(ProjectPermissions.ASSIGNABLE_USER, issue, currentUser)
                && issueManager.isEditable(issue);
    }

    @Override
    public String getUniqueContextKey(Map<String, Object> context) {
        final Issue issue = (Issue) context.get("issue");
        final ApplicationUser user = authenticationContext.getLoggedInUser();
        return issue.getId() + '/' + (user == null ? "" : user.getUsername());
    }

    private List<CustomFieldModel> createCustomFieldModels(final Issue issue, final Action action) {
        final FieldScreenRenderer screenRenderer = fieldScreenRendererFactory.getFieldScreenRenderer(issue, IssueOperations.VIEW_ISSUE_OPERATION, FieldPredicates.isCustomUserField());
        final List<FieldScreenRenderLayoutItem> fieldScreenRenderLayoutItems = screenRenderer.getAllScreenRenderItems();
        
        return fieldScreenRenderLayoutItems.stream()
                .map(layoutItem -> new CustomFieldModel(layoutItem.getFieldLayoutItem(), issue, action))
                .collect(toList());
    }

    private boolean isAssigneeVisible(Issue issue) {
        return isFieldVisible(issue, IssueFieldConstants.ASSIGNEE);
    }

    private boolean isReporterVisible(Issue issue) {
        return isFieldVisible(issue, IssueFieldConstants.REPORTER);
    }

    private boolean isFieldVisible(Issue issue, String field) {
        return !fieldVisibilityManager.isFieldHidden(issue.getProjectId(), field, issue.getIssueTypeId());
    }

    /**
     * Construct an HTML string to display an issue's assignee.
     *
     * @param issue The issue whose assignee is to be rendered.
     * @return an HTML string that displays {@code issue}'s assignee.
     */
    public String getAssigneeDisplayHtml(Issue issue) {
        if (issue == null) {
            return "";
        }

        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("avatarURL", getAvatarURL(issue.getAssignee()));
        parameters.put("defaultFullName", authenticationContext.getI18nHelper().getText("common.status.unassigned"));

        return userFormatManager.formatUserkey(issue.getAssigneeId(), "avatarFullNameHover", "issue_summary_assignee", parameters);
    }

    /**
     * Construct an HTML string to display an issue's reporter.
     *
     * @param issue The issue whose reporter is to be rendered.
     * @return an HTML string that displays {@code issue}'s reporter.
     */
    public String getReporterDisplayHtml(Issue issue) {
        if (issue == null) {
            return "";
        }

        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("avatarURL", getAvatarURL(issue.getReporter()));

        return userFormatManager.formatUserkey(issue.getReporterId(), "avatarFullNameHover", "issue_summary_reporter", parameters);
    }

    private String getAvatarURL(final ApplicationUser user) {
        return avatarService.getAvatarURL(authenticationContext.getLoggedInUser(), user, Avatar.Size.NORMAL).toString();
    }
}