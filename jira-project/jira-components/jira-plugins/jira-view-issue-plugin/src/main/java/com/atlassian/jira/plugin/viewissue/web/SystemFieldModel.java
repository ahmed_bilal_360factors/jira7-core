package com.atlassian.jira.plugin.viewissue.web;

import com.atlassian.jira.issue.fields.Field;

public class SystemFieldModel extends FieldModel {
    public SystemFieldModel(Field field) {
        super(field);
    }

    @Override
    public FieldModelType getFieldModelType() {
        return FieldModelType.SYSTEM;
    }
}

