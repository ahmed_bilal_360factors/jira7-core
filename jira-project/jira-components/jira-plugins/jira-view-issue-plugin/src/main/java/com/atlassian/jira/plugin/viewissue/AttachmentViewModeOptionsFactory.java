package com.atlassian.jira.plugin.viewissue;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.jira.util.velocity.VelocityRequestContext;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.plugin.web.api.WebItem;
import com.atlassian.plugin.web.api.model.WebFragmentBuilder;
import com.atlassian.plugin.web.api.provider.WebItemProvider;

import java.util.Map;

/**
 * Factory to return the options for the different view mode options
 *
 * @since 6.4
 */
public class AttachmentViewModeOptionsFactory implements WebItemProvider {
    private static final String ITEM_SECTION = "com.atlassian.jira.jira-view-issue-plugin:attachmentmodule/drop/attachment-view-mode-options";
    private final VelocityRequestContextFactory requestContextFactory;
    private final JiraAuthenticationContext authenticationContext;
    private final ApplicationProperties applicationProperties;
    private final AttachmentBlockContextHelper helper;

    public AttachmentViewModeOptionsFactory(VelocityRequestContextFactory requestContextFactory, JiraAuthenticationContext authenticationContext, final ApplicationProperties applicationProperties, final AttachmentBlockContextHelper helper) {
        this.requestContextFactory = requestContextFactory;
        this.authenticationContext = authenticationContext;
        this.applicationProperties = applicationProperties;
        this.helper = helper;
    }

    @Override
    public Iterable<WebItem> getItems(final Map<String, Object> context) {
        if (!applicationProperties.getOption(APKeys.JIRA_OPTION_ALLOWTHUMBNAILS)) {
            return CollectionBuilder.list();
        }

        final VelocityRequestContext requestContext = requestContextFactory.getJiraVelocityRequestContext();
        final I18nHelper i18n = authenticationContext.getI18nHelper();
        final Issue issue = (Issue) context.get("issue");


        final String baseUrl = requestContext.getBaseUrl();
        final String viewMode = helper.getAttachmentViewMode();

        boolean viewModeList = AttachmentBlockContextHelper.VIEWMODE_LIST.equals(viewMode);

        final WebItem galleryLink = new WebFragmentBuilder(10).
                id("attachment-view-mode-gallery").
                label(i18n.getText("viewissue.attachments.view.mode.gallery")).
                title(i18n.getText("viewissue.attachments.view.mode.gallery")).
                styleClass(!viewModeList ? "aui-list-checked aui-checked" : "aui-list-checked").
                webItem(ITEM_SECTION).
                url(baseUrl + "/browse/" + issue.getKey() + "?attachmentViewMode=gallery#attachmentmodule").build();

        final WebItem listLink = new WebFragmentBuilder(20).
                id("attachment-view-mode-list").
                label(i18n.getText("viewissue.attachments.view.mode.list")).
                title(i18n.getText("viewissue.attachments.view.mode.list")).
                styleClass(viewModeList ? "aui-list-checked aui-checked" : "aui-list-checked").
                webItem(ITEM_SECTION).
                url(baseUrl + "/browse/" + issue.getKey() + "?attachmentViewMode=list#attachmentmodule").build();


        return CollectionBuilder.list(listLink, galleryLink);
    }
}
