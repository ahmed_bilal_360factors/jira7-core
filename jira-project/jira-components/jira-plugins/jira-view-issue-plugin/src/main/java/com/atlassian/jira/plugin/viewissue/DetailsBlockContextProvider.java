package com.atlassian.jira.plugin.viewissue;

import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.atlassian.jira.event.mau.MauEventService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.OrderableField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.screen.FieldScreenRenderTab;
import com.atlassian.jira.issue.fields.screen.FieldScreenRenderer;
import com.atlassian.jira.issue.fields.screen.FieldScreenRendererFactory;
import com.atlassian.jira.issue.fields.util.FieldPredicates;
import com.atlassian.jira.issue.label.LabelUtil;
import com.atlassian.jira.issue.operation.IssueOperations;
import com.atlassian.jira.issue.views.SearchLinkGenerator;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.plugin.issueview.IssueViewModuleDescriptor;
import com.atlassian.jira.plugin.viewissue.web.FieldModelFactory;
import com.atlassian.jira.plugin.viewissue.web.SimpleTab;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.JiraVelocityUtils;
import com.atlassian.jira.util.UrlBuilder;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.web.FieldVisibilityManager;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.ContextProvider;
import com.opensymphony.workflow.loader.StepDescriptor;
import org.apache.commons.lang.StringUtils;
import webwork.action.Action;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import static com.google.common.collect.Maps.newHashMap;
import static java.util.stream.Collectors.toList;

/**
 * Context provider for the details block
 * If a List of Fields is provided in the map under the key COMPOSED_BY_FIELDS,
 * this context provider will create the view models for each of the fields given
 * which can be rendered in order by detailsblock-composed.vm.
 * 
 * Otherwise this will consult the FieldVisibilityManager to determine whether
 * each field should display for the view issue page, and in some cases
 * it will not create a model if the field has no value.
 *  
 * @since v5.0
 */
public class DetailsBlockContextProvider implements ContextProvider {

    private final PluginAccessor pluginAccessor;
    private final JiraAuthenticationContext authenticationContext;
    private final ProjectComponentManager projectComponentManager;
    private final FieldVisibilityManager fieldVisibilityManager;
    private final LabelUtil labelUtil;
    private final FieldManager fieldManager;
    private final FieldScreenRendererFactory fieldScreenRendererFactory;
    private final IssueManager issueManager;
    private final PermissionManager permissionManager;
    private final WorkflowManager workflowManager;
    private final SearchLinkGenerator searchLinkGenerator;
    private final MauEventService mauEventService;
    private VersionManager versionManager;
    private FieldModelFactory fieldModelFactory;

    public DetailsBlockContextProvider(PluginAccessor pluginAccessor,
                                       JiraAuthenticationContext authenticationContext, ProjectComponentManager projectComponentManager,
                                       FieldVisibilityManager fieldVisibilityManager, LabelUtil labelUtil,
                                       final FieldManager fieldManager, FieldScreenRendererFactory fieldScreenRendererFactory,
                                       final IssueManager issueManager, final PermissionManager permissionManager,
                                       final WorkflowManager workflowManager, final SearchLinkGenerator searchLinkGenerator,
                                       final VersionManager versionManager, final MauEventService mauEventService, FieldModelFactory fieldModelFactory) {
        this.pluginAccessor = pluginAccessor;
        this.authenticationContext = authenticationContext;
        this.projectComponentManager = projectComponentManager;
        this.fieldVisibilityManager = fieldVisibilityManager;
        this.labelUtil = labelUtil;
        this.fieldManager = fieldManager;
        this.fieldScreenRendererFactory = fieldScreenRendererFactory;
        this.issueManager = issueManager;
        this.permissionManager = permissionManager;
        this.workflowManager = workflowManager;
        this.searchLinkGenerator = searchLinkGenerator;
        this.versionManager = versionManager;
        this.mauEventService = mauEventService;
        this.fieldModelFactory = fieldModelFactory;
    }

    @Override
    public void init(Map<String, String> params) throws PluginParseException {
    }

    @Override
    public Map<String, Object> getContextMap(Map<String, Object> context) {
        final MapBuilder<String, Object> paramsBuilder = MapBuilder.newBuilder(JiraVelocityUtils.getDefaultVelocityParams(context, authenticationContext));

        final Issue issue = (Issue) context.get("issue");
        mauEventService.setApplicationForThreadBasedOnProject(issue.getProjectObject());
        final Action action = (Action) context.get("action");
        final ApplicationUser user = authenticationContext.getLoggedInUser();
        paramsBuilder.add("i18n", authenticationContext.getI18nHelper());
        paramsBuilder.add("issue", issue);
        paramsBuilder.add("summaryComponent", this);
        paramsBuilder.add("fieldVisibility", fieldVisibilityManager);
        paramsBuilder.add("issueViews", getIssueViews());
        paramsBuilder.add("canEdit", issueManager.isEditable(issue, user));

        paramsBuilder.add("hasViewWorkflowPermission", permissionManager.hasPermission(ProjectPermissions.VIEW_READONLY_WORKFLOW, issue, user));
        final JiraWorkflow workflow = workflowManager.getWorkflow(issue);
        if (workflow != null) {
            paramsBuilder.add("workflowName", workflow.getName());
            paramsBuilder.add("workflowUrl", getWorkflowUrl(workflow, issue));
        }

        final Long projectId = issue.getProjectId();
        paramsBuilder.add("searchLinkGenerator", searchLinkGenerator);
        paramsBuilder.add("projectHasComponents", !projectComponentManager.findAllForProject(projectId).isEmpty());
        paramsBuilder.add("projectHasVersions", !versionManager.getVersions(projectId).isEmpty());

        paramsBuilder.add("labelUtil", labelUtil);
        paramsBuilder.add("renderedEnvironmentHtml", getRenderedEnvironmentFieldValue(issue, action));
        
        if (fieldModelFactory.hasRequestedFields(context)) {
            paramsBuilder.add("fields", fieldModelFactory.getFieldModels(context));
        } else {
            paramsBuilder.addAll(createCFTabs(issue, action));
        }
        
        return paramsBuilder.toMap();
    }

    private Map<String, Object> createCFTabs(Issue issue, Action action) {
        Map<String, Object> map = newHashMap();

        final FieldScreenRenderer fieldScreenRenderer = fieldScreenRendererFactory.getFieldScreenRenderer(issue, IssueOperations.VIEW_ISSUE_OPERATION, FieldPredicates.isStandardViewIssueCustomField());
        final List<FieldScreenRenderTab> fieldScreenRenderTabs = fieldScreenRenderer.getFieldScreenRenderTabs();

        List<SimpleTab> tabModels = fieldScreenRenderTabs.stream()
                .map(tab -> new SimpleTab(tab, issue, action))
                .collect(toList());
        
        map.put("tabs", tabModels);
        return map;
    }

    private Collection<IssueViewModuleDescriptor> getIssueViews() {
        return pluginAccessor.getEnabledModuleDescriptorsByClass(IssueViewModuleDescriptor.class);
    }
    
    /**
     * Gets the HTML that shows the environment field. This includes divs and a javascript enabled hide/show toggle
     * button.
     *
     * @param issue  what issue they want to do it to
     * @param action what they want to do
     * @return the HTML that shows the environment field.
     */
    private String getRenderedEnvironmentFieldValue(final Issue issue, final Action action) {
        if (StringUtils.isNotBlank(issue.getEnvironment())) {
            final OrderableField environmentField = fieldManager.getOrderableField(IssueFieldConstants.ENVIRONMENT);
            final FieldScreenRenderer renderer = fieldScreenRendererFactory.getFieldScreenRenderer(issue, IssueOperations.VIEW_ISSUE_OPERATION);
            final FieldLayoutItem fieldLayoutItem = renderer.getFieldScreenRenderLayoutItem(environmentField).getFieldLayoutItem();

            // JRA-16224 Cannot call getViewHtml() on FieldScreenRenderLayoutItem, because it will return "" if Environment is not included in the Screen Layout.
            return environmentField.getViewHtml(fieldLayoutItem, action, issue);
        }
        return null;
    }

    private String getWorkflowUrl(JiraWorkflow workflow, Issue issue) {
        if (workflow != null) {
            UrlBuilder url = new UrlBuilder(false).
                    addPath("/").addPath("browse").addPath(issue.getKey()).
                    addParameter("workflowName", workflow.getName());

            StepDescriptor step = workflow.getLinkedStep(issue.getStatusObject());
            if (step != null) { 
                url.addParameter("stepId", Integer.toString(step.getId()));
            }

            return url.asUrlString();
        }
        return "";
    }
}
