package com.atlassian.jira.plugin.viewissue.events;

import com.atlassian.analytics.api.annotations.EventName;

@EventName("jira.viewissue.tab.loaded")
public class IssueTabRenderedEvent {
    private final String tab;
    private final int tabPosition;
    private final long issueId;

    public IssueTabRenderedEvent(final String tab, final int tabPosition, final long issueId) {
        this.tab = tab;
        this.tabPosition = tabPosition;
        this.issueId = issueId;
    }

    public String getTab() {
        return tab;
    }

    public int getTabPosition() {
        return tabPosition;
    }

    public long getIssueId() {
        return issueId;
    }
}
