package com.atlassian.jira.plugin.viewissue.web;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.FieldRenderingContext;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.plugin.customfield.CustomFieldTypeModuleDescriptor;
import com.atlassian.jira.util.collect.MapBuilder;
import webwork.action.Action;

/**
 * View model for the view issue page representing a custom field. 
 */
public class CustomFieldModel extends FieldModel {
    private final Object value;
    private String fieldType = null;
    private String fieldTypeCompleteKey = null;
    private String styleClass = null;
    private boolean hasView = false;
    private String fieldHtml = null;

    public CustomFieldModel(FieldLayoutItem layoutItem, Issue issue, Action action) {
        super(layoutItem.getOrderableField());
        CustomField customField = (CustomField) layoutItem.getOrderableField();
        CustomFieldTypeModuleDescriptor descriptor = customField.getCustomFieldType().getDescriptor();
        value = customField.getValue(issue);
        hasView = descriptor.isViewTemplateExists();

        if (hasView) {
            initialiseField(customField, layoutItem, issue, action, descriptor, value);
        }
    }

    private void initialiseField(CustomField customField, FieldLayoutItem layoutItem, Issue issue, Action action, CustomFieldTypeModuleDescriptor descriptor, Object value) {
        fieldType = descriptor.getKey();
        fieldTypeCompleteKey = descriptor.getCompleteKey();
        styleClass = "type-" + fieldType;
        if ("textarea".equals(fieldType) && value != null && ((String) value).length() > 255) {
            styleClass += " twixified";
        }

        fieldHtml = customField.getViewHtml(layoutItem, action, issue, MapBuilder.build(FieldRenderingContext.ISSUE_VIEW, Boolean.TRUE));
    }

    public String getStyleClass() {
        return styleClass;
    }

    public boolean hasView() {
        return hasView;
    }

    public boolean hasValue() {
        return value != null;
    }

    public String getFieldHtml() {
        return fieldHtml;
    }

    public String getFieldType() {
        return fieldType;
    }

    @Override
    public FieldModelType getFieldModelType() {
        return FieldModelType.CUSTOM;
    }

    public String getFieldTypeCompleteKey() {
        return fieldTypeCompleteKey;
    }
}