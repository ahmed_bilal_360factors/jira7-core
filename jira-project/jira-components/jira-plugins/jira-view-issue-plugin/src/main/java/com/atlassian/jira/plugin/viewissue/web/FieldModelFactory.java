package com.atlassian.jira.plugin.viewissue.web;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.OrderableField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import webwork.action.Action;

import java.util.List;
import java.util.Map;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.stream.Collectors.toList;


public class FieldModelFactory {
    public static final String COMPOSED_BY_FIELDS = "composedByFields";

    private final DateFieldModelFactory dateFieldModelFactory;
    private final FieldLayoutManager fieldLayoutManager;

    public FieldModelFactory(FieldLayoutManager fieldLayoutManager,
                             DateFieldModelFactory dateFieldModelFactory) {
        this.fieldLayoutManager = fieldLayoutManager;
        this.dateFieldModelFactory = dateFieldModelFactory;
    }

    /**
     * Gets the view models for the fields requested in order from the given context.
     * @param context context containing a permutation of fields under the key FieldModelFactory.COMPOSED_BY_FIELDS
     * @return
     */
    public List<FieldModel> getFieldModels(Map<String, Object> context) {
        final Issue issue = (Issue) context.get("issue");
        final Action action = (Action) context.get("action");

        List<Field> fields = (List<Field>) context.getOrDefault(COMPOSED_BY_FIELDS, newArrayList());

        return fields.stream()
                .map(field -> toModel(field, issue, action))
                .collect(toList());
    }

    /**
     * Determines if the given context map has in it a requested permutation of fields. 
     * @param contextMap
     * @return true if the context contains an ordering.
     */
    public boolean hasRequestedFields(Map<String, Object> contextMap) {
        return contextMap.containsKey(COMPOSED_BY_FIELDS);
    }

    private FieldModel toModel(Field field, Issue issue, Action action) {
        if (dateFieldModelFactory.isDateSystemField(field)) {
            return dateFieldModelFactory.toModel(field, issue);
        } else if (field instanceof CustomField) {
            FieldLayoutItem fieldLayoutItem = getFieldLayoutItem((CustomField) field, issue);
            return new CustomFieldModel(fieldLayoutItem, issue, action);
        } else {
            return new SystemFieldModel(field);
        }
    }

    private FieldLayoutItem getFieldLayoutItem(OrderableField field, Issue issue) {
        if (issue != null) {
            return fieldLayoutManager.getFieldLayout(issue).getFieldLayoutItem(field);
        } else {
            return fieldLayoutManager.getFieldLayout().getFieldLayoutItem(field);
        }
    }
}
