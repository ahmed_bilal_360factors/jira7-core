package com.atlassian.jira.plugin.viewissue.web;

import com.atlassian.jira.JiraDataType;
import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.DateFieldFormat;

import java.sql.Timestamp;

import static com.atlassian.jira.JiraDataTypes.DATE;
import static com.atlassian.jira.datetime.DateTimeStyle.COMPLETE;
import static com.atlassian.jira.datetime.DateTimeStyle.ISO_8601_DATE;
import static com.atlassian.jira.datetime.DateTimeStyle.ISO_8601_DATE_TIME;
import static com.atlassian.jira.issue.IssueFieldConstants.CREATED;
import static com.atlassian.jira.issue.IssueFieldConstants.DUE_DATE;
import static com.atlassian.jira.issue.IssueFieldConstants.RESOLUTION_DATE;
import static com.atlassian.jira.issue.IssueFieldConstants.UPDATED;
import static org.apache.commons.lang.StringEscapeUtils.escapeHtml;

public class DateFieldModelFactory {
    private final DateFieldFormat dateFieldFormat;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final DateTimeFormatterFactory dateTimeFormatterFactory;

    public DateFieldModelFactory(DateTimeFormatterFactory dateTimeFormaterFactory, DateFieldFormat dateFieldFormat, JiraAuthenticationContext jiraAuthenticationContext) {
        this.dateTimeFormatterFactory = dateTimeFormaterFactory;
        this.dateFieldFormat = dateFieldFormat;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    /**
     * Checks if the given field is a date system field.
     *
     * @param field
     * @return
     */
    public boolean isDateSystemField(Field field) {
        JiraDataType fieldType = JiraDataTypes.getFieldType(field.getId());
        return fieldType != null && fieldType.matches(DATE);
    }

    /**
     * Converts the given date field into a DateFieldModel. The field must be a date system field.
     * Check using {@link com.atlassian.jira.plugin.viewissue.web.DateFieldModelFactory#isDateSystemField}
     */
    public DateFieldModel toModel(Field field, Issue issue) {
        Timestamp date = getTimestampFromIssue(issue, field);
        DateTimeFormatter dateTimeFormatter = dateTimeFormatterFactory.formatter().forLoggedInUser();

        if (DUE_DATE.equals(field.getId())) {
            return createDueDateFieldModel(dateTimeFormatter, field, date);
        } else {
            return createDateTimeFieldModel(dateTimeFormatter, field, date);
        }
    }

    private DateFieldModel createDateTimeFieldModel(DateTimeFormatter dateTimeFormatter, Field field, Timestamp dateTime) {
        String fieldHtml = dateTime != null ? escapeHtml(dateTimeFormatter.format(dateTime)) : null;
        String iso8601 = escapeHtml(dateTimeFormatter.withStyle(ISO_8601_DATE_TIME).format(dateTime));
        String title = escapeHtml(dateTimeFormatter.withStyle(COMPLETE).format(dateTime));

        return new DateFieldModel(field, fieldHtml, iso8601, title);
    }

    private DueDateFieldModel createDueDateFieldModel(DateTimeFormatter dateTimeFormatter, Field field, Timestamp dueDate) {
        String fieldHtml = dueDate != null ? escapeHtml(dateFieldFormat.format(dueDate)) : null;
        String iso8601 = escapeHtml(dateTimeFormatter.withSystemZone().withStyle(ISO_8601_DATE).format(dueDate));
        String title = fieldHtml;

        DueDateFieldModel model = new DueDateFieldModel(field, fieldHtml, iso8601, title);

        // The name of the field is 'Due Date', however this field insists on displaying a different name in the view issue ('Due').
        // Probably to be consistent with the 'Updated', 'Resolved' and 'Created' fields it sits next to.
        model.name = jiraAuthenticationContext.getI18nHelper().getText("issue.field.due");
        return model;
    }
    
    private Timestamp getTimestampFromIssue(Issue issue, Field dateField) {
        String id = dateField.getId();
        if (DUE_DATE.equals(id)) {
            return issue.getDueDate();
        } else if (CREATED.equals(id)) {
            return issue.getCreated();
        } else if (RESOLUTION_DATE.equals(id)) {
            return issue.getResolutionDate();
        } else if (UPDATED.equals(id)) {
            return issue.getUpdated();
        } else {
            throw new IllegalArgumentException("Unsupported field: " + id);
        }
    }
}
