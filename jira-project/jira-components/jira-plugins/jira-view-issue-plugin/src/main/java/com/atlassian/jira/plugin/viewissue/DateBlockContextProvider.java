package com.atlassian.jira.plugin.viewissue;

import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.screen.FieldScreenRenderLayoutItem;
import com.atlassian.jira.issue.fields.screen.FieldScreenRenderer;
import com.atlassian.jira.issue.fields.screen.FieldScreenRendererFactory;
import com.atlassian.jira.issue.fields.util.FieldPredicates;
import com.atlassian.jira.issue.operation.IssueOperations;
import com.atlassian.jira.plugin.viewissue.web.CustomFieldModel;
import com.atlassian.jira.plugin.viewissue.web.DateFieldModel;
import com.atlassian.jira.plugin.viewissue.web.DateFieldModelFactory;
import com.atlassian.jira.plugin.viewissue.web.FieldModel;
import com.atlassian.jira.plugin.viewissue.web.FieldModelFactory;
import com.atlassian.jira.plugin.webfragment.CacheableContextProvider;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.web.FieldVisibilityManager;
import com.atlassian.plugin.PluginParseException;
import webwork.action.Action;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;

/**
 * Context Provider for Date Block on View Issue page.
 *
 * @since v4.4
 */
public class DateBlockContextProvider implements CacheableContextProvider {
    private final FieldVisibilityManager fieldVisibilityManager;
    private final JiraAuthenticationContext authenticationContext;
    private final FieldScreenRendererFactory fieldScreenRendererFactory;
    private final FieldManager fieldManager;
    private final DateFieldModelFactory dateFieldModelFactory;
    private final FieldModelFactory fieldModelFactory;

    public DateBlockContextProvider(FieldVisibilityManager fieldVisibilityManager, JiraAuthenticationContext authenticationContext,
                                    FieldScreenRendererFactory fieldScreenRendererFactory, FieldManager fieldManager,
                                    DateFieldModelFactory dateFieldModelFactory, FieldModelFactory fieldModelFactory) {
        this.fieldVisibilityManager = fieldVisibilityManager;
        this.authenticationContext = authenticationContext;
        this.fieldScreenRendererFactory = fieldScreenRendererFactory;
        this.fieldManager = fieldManager;
        this.dateFieldModelFactory = dateFieldModelFactory;
        this.fieldModelFactory = fieldModelFactory;
    }

    @Override
    public void init(Map<String, String> params) throws PluginParseException {
    }

    @Override
    public Map<String, Object> getContextMap(Map<String, Object> context) {
        final Issue issue = (Issue) context.get("issue");
        final Action action = (Action) context.get("action");
        context.put("i18n", authenticationContext.getI18nHelper());

        final MapBuilder<String, Object> paramsBuilder = MapBuilder.newBuilder(context);

        final List<FieldModel> dates = newArrayList();
        
        if (fieldModelFactory.hasRequestedFields(context)) {
            dates.addAll(fieldModelFactory.getFieldModels(context));
        } else {
            List<Field> systemDateFields = newArrayList();
            if (issue.getDueDate() != null && !fieldVisibilityManager.isFieldHidden(IssueFieldConstants.DUE_DATE, issue)) {
                systemDateFields.add(fieldManager.getField(IssueFieldConstants.DUE_DATE));
            }

            systemDateFields.add(fieldManager.getField(IssueFieldConstants.CREATED));
            systemDateFields.add(fieldManager.getField(IssueFieldConstants.UPDATED));

            if (issue.getResolutionDate() != null) {
                systemDateFields.add(fieldManager.getField(IssueFieldConstants.RESOLUTION_DATE));
            }
            
            dates.addAll(getDateSystemFieldModels(systemDateFields, issue));
            dates.addAll(getDateCustomFieldModels(issue, action));
        }

        paramsBuilder.add("dates", dates);

        return paramsBuilder.toMap();
    }

    @Override
    public String getUniqueContextKey(Map<String, Object> context) {
        final Issue issue = (Issue) context.get("issue");
        final ApplicationUser user = authenticationContext.getLoggedInUser();

        return issue.getId() + "/" + (user == null ? "" : user.getName());
    }

    private List<CustomFieldModel> getDateCustomFieldModels(Issue issue, Action action) {
        FieldScreenRenderer screenRenderer = fieldScreenRendererFactory.getFieldScreenRenderer(issue, IssueOperations.VIEW_ISSUE_OPERATION, FieldPredicates.isCustomDateField());
        final List<FieldScreenRenderLayoutItem> dateRenderItems = screenRenderer.getAllScreenRenderItems();

        return dateRenderItems.stream()
                .map(layoutItem -> new CustomFieldModel(layoutItem.getFieldLayoutItem(), issue, action))
                .collect(Collectors.toList());
    }

    private List<DateFieldModel> getDateSystemFieldModels(List<Field> systemFields, Issue issue) {
        return systemFields.stream()
                .filter(dateFieldModelFactory::isDateSystemField)
                .map(field -> dateFieldModelFactory.toModel(field, issue))
                .collect(Collectors.toList());
    }
}
