package com.atlassian.jira.plugin.viewissue.web;

import com.atlassian.jira.issue.fields.Field;

/**
 * Field Model to render the Due Date system field
 */
public class DueDateFieldModel extends DateFieldModel {
    DueDateFieldModel(Field dateField, String fieldHtml, String iso8601, String title) {
        super(dateField, fieldHtml, iso8601, title);
        this.styleClass = null;
        this.fieldType = "datepicker";
    }

    /**
     * Unfortunately when view issue was originally written, this field was rendered on the DOM with the arbitrary id of 'due-date'
     * Then issue nav plugin came along and added a special case for the DueDate field which now looks for this id.
     *
     * @return the correct id to render the due date field with so that issue nav will apply the inline editor.
     */
    @Override
    public String getElementId() {
        return "due-date";
    }
}
