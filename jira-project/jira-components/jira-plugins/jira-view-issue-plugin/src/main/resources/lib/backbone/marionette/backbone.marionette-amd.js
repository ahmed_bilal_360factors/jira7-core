define('jira/view-issue-plugin/lib/marionette', [
    'backbone',
    'underscore',
    'atlassian/libs/factories/marionette-2.1.0'
], function defineMarionette(Backbone, _, MarionetteFactory) {
    return MarionetteFactory(_, Backbone);
});
