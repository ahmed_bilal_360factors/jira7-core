AJS.test.require('com.atlassian.jira.jira-view-issue-plugin:controller-subtasks', function () {

    var $ = require('jquery');
    var _ = require('underscore');

    var SubtasksHelper = require('viewissue/subtasks/subtasks-helper');

    module("viewissue/subtasks/subtasks-helper", {
        setup: function(){
            var fixture = $("#qunit-fixture");
            $("<issuetable-web-component>" +
                "<table id='issuetable'>" +
                "   <tbody>" +
                "       <tr><td class='stsequence'><div rel='0'></div></td></tr>" +
                "       <tr><td class='stsequence'><div rel='5'></div></td></tr>" +
                "       <tr><td class='stsequence'><div rel='3'></div></td></tr>" +
                "       <tr><td class='stsequence'><div rel='7'></div></td></tr>" +
                "   </tbody></table></issuetable-web-component>")
                .appendTo(fixture);
        },
        tearDown: function(){}
    });

    test("Knows if issue was moved", function(){
        equal(SubtasksHelper.moved({
            position: {
                original : 100500,
                current : 100500
            }
        }), false, "Must answer 'false' if subtask not moved");
    });

    test("Creates proper context", function(){
        var context = SubtasksHelper.create({
            position: {
                original: 2,
                current: 1
            },
            table: $("#issuetable")[0],
            row: $("#issuetable tbody tr")[1]
        });

        equal($("#issuetable").find("tbody tr .stsequence div").length, context.allRows.length, "Must find all rows in changed table");
        equal(true, context.up, "Must properly calculate if subtask was moved up or down");
        equal(1, context.movedRows.length, "Must find all rows which sequence numbers need to be updated");
        equal(5, context.originalSequence, "Must properly calculate original sequence number of subtask");
        equal(3, context.currentSequence, "Must properly calculate new sequence number of subtask");

    });
});
