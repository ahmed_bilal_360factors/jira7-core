/**
 * Reports bulk move events to analytics.
 */
define('jira/view-issue-plugin/analytics', ['jquery'], function ($) {
    var $doc = $(document);

    $doc.on('click', '#subtasks-bulk-operation', function () {
        AJS.trigger('analyticsEvent', {name: "jira.bulk.move.click.subtasks-bulk-operation"});
    });
    $doc.on('click', '#subtasks-open-issue-navigator', function () {
        AJS.trigger('analyticsEvent', {name: "jira.bulk.move.click.subtasks-open-issue-navigator"});
    });
});
