/**
 * Reports bulk move events to analytics.
 */
(function () {
    var $ = require('jquery');

    $(function () {
        $("body").on("click", "#subtasks-bulk-operation", function () {
            AJS.trigger('analyticsEvent', {name: "jira.bulk.move.click.subtasks-bulk-operation"});
        });
        $("body").on("click", "#subtasks-open-issue-navigator", function () {
            AJS.trigger('analyticsEvent', {name: "jira.bulk.move.click.subtasks-open-issue-navigator"});
        });
    });
})();
