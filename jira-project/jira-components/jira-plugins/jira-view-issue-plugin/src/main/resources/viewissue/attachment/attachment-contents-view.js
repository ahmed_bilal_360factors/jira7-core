/**
 * @module jira/view-issue-plugin/attachment/attachment-contents-view
 * @requires module:jira/view-issue-plugin/lib/marionette
 */
define('jira/view-issue-plugin/attachment/attachment-contents-view', [
    'jira/view-issue-plugin/lib/marionette'
], function defineAttachmentContentsView(Marionette) {
    'use strict';

    return Marionette.ItemView.extend({
        template: JIRA.Templates.ViewIssue.attachmentContents,

        initialize: function initialize(options) {
            this.attachment = options.attachment;
            this.issueId = options.issueId;
            this.baseUrl = options.baseUrl;
        },

        serializeData: function serializeData() {
            return {
                attachment: this.attachment,
                issueId: this.issueId,
                baseUrl: this.baseUrl
            };
        }

    });
});

/**
 * @module jira/attachment/attachment-contents-view
 * @deprecated use {@link module:jira/view-issue-plugin/attachment/attachment-contents-view}
 */
define('jira/attachment/attachment-contents-view', ['jira/view-issue-plugin/attachment/attachment-contents-view'], function(View) { return View; });
