require([
    'jira/view-issue-plugin/attachment/attachment-element',
    'jira/view-issue-plugin/init-draggable-attachment',
    'jira/view-issue-plugin/links/links-container',
    'jira/view-issue-plugin/analytics'
]);
