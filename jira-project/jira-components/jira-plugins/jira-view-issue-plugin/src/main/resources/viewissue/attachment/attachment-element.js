/**
 * @module jira/view-issue-plugin/attachment/attachment-element
 *
 * @requires module:jira/attachment/attachment-contents-view
 * @requires module:jira/viewissue/slideshow/slideshow-configuration
 * @requires module:jira/view-issue-plugin/lib/marionette
 * @requires module:jira/skate
 * @requires external:jQuery
 */
define('jira/view-issue-plugin/attachment/attachment-element', [
    'jira/util/logger',
    'wrm/context-path',
    'jquery',
    'jira/skate',
    'jira/viewissue/slideshow/slideshow-configuration',
    'jira/view-issue-plugin/lib/marionette',
    'jira/attachment/attachment-contents-view'
], function(
    logger,
    wrmContextPath,
    $,
    skate,
    galleryOpts,
    Marionette,
    AttachmentContentsView) {
    'use strict';

    /**
     * @skate js-file-attachment
     */
    return skate('js-file-attachment', {
        type: skate.type.CLASSNAME,
        attached: function elementAttachedHandler(element) {
            $(element).find('.expander').one('expandBlock', function elementExpandedHandler() {
                expandArchive(element);
            });
            logger.trace('jira.plugins.viewissue.expansion.wired');
        },
        events: {
            'click .attachment-title': function elementClickedHandler(element, e) {
                if (e.which !== 1) {
                    return;
                }
                var $thumb = element.getThumbnailImage();
                if ($thumb.length) {
                    $thumb.click();
                    e.preventDefault();
                }
            }
        },
        prototype: {
            getThumbnailImage: function() {
                var element = this;
                return $('.gallery', element);
            }
        }
    });

    function expandArchive(element) {
        var $attachmentRow = $(element);
        var attachmentId = $attachmentRow.data('attachment-id');
        var issueId = $attachmentRow.data('issue-id');
        var expansionResource = wrmContextPath() + "/rest/api/2/attachment/" + attachmentId + "/expand/human";
        var promise = $.get(expansionResource);
        promise.done(function renderAttachmentContents(data) {
            var region = new Marionette.Region({
                el: $attachmentRow.find('.zip-contents').first()
            });
            var view = new AttachmentContentsView({
                attachment: data,
                issueId: issueId,
                baseUrl: window.location.protocol + "//" + window.location.host + wrmContextPath()
            });
            region.show(view);
        });
        promise.error(function handleAttachmentExpansionError(data) {
            logger.error(data);
        });
    }

});
