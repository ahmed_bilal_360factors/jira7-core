/**
 * @module jira/view-issue-plugin/links/links-container
 *
 * @requires module:jira/util/formatter
 * @requires module:wrm/context-path
 * @requires module:jira/ajs/ajax/smart-ajax
 * @requires module:jira/skate
 * @requires external:jQuery
 */
define('jira/view-issue-plugin/links/links-container', [
    'jira/util/formatter',
    'wrm/context-path',
    'jira/ajs/ajax/smart-ajax',
    'jira/skate',
    'jquery'
], function(
    formatter,
    wrmContextPath,
    SmartAjax,
    skate,
    $
) {
    "use strict";

    /**
     * @skate links-container
     */
    return skate("links-container", {
        type: skate.type.CLASSNAME,
        attached: function(element) {
            $(element).find('dd:not(.collapsed-link)').each(function(k, link) {
                updateLink($(link));
            });
        },
        events: {
            'click #show-more-links-link': function(element, e, trigger) {
                e.preventDefault();

                var $element = $(element);
                // Hide the trigger and display hidden links
                $element.find(trigger).parent().hide();
                $element.find(".collapsed-links-list").removeClass("collapsed-links-list");

                // Loads previously hidden links that require async loading
                $element.find('dd.collapsed-link').each(function(k, link) {
                    var $link = $(link);
                    updateLink($link);
                    $link.removeClass("collapsed-link");
                });
            }
        }
    });

    function updateLink($link) {
        var remoteLinkId = $link.data('remote-link-id');
        if(remoteLinkId && $link.data('requires-async-loading')) {
            updateRemoteLink(remoteLinkId, $link.attr('id'));
        }
    }

    function updateRemoteLink(id, htmlElementId) {
        SmartAjax.makeRequest({
            error: function() {
                var $htmlElement = $(document.getElementById(htmlElementId));
                var defaultIconUrl = $htmlElement.parents("*[data-default-link-icon]").data("default-link-icon");
                $htmlElement.find(".link-loading")
                    .removeClass("link-loading")
                    .addClass("link-loading-failed")
                    .html(formatter.I18n.getText("common.concepts.loading.failed"));
                $htmlElement.find(".link-throbber")
                    .append($("<img width='16' height='16' title='' alt='' />").attr("src", defaultIconUrl));
            },
            success: function(data) {
                $(document.getElementById(htmlElementId)).find(".link-content").html(data);
            },
            url: wrmContextPath() + "/rest/viewIssue/1/remoteIssueLink/render/" + id
        });
    }
});
