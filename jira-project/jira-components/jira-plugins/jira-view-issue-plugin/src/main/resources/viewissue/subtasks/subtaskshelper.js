define("viewissue/subtasks/subtasks-helper", [
    'jquery'
], function ($) {
    return {
        moved: function (event) {
            return event.position.original !== event.position.current;
        },
        create: function (event) {
            return (function (api) {
                api.allRows = $(event.table).find("tbody tr .stsequence div");

                api.up = event.position.current < event.position.original ? true : false;

                // Whenever rows being dragged, we need adjust their sequence number.
                // But not for all rows, only for rows which a in dragging window.
                // Dragging window is rows which are between original location of row being dragged
                // and its new location.

                // New sequence number depends on in which direction row being dragged.
                // Whenever row dragged up - sequence numbers of rows in window should be increased.
                // Whenever row dragged down - sequence number of rows in window should be decresed.
                api.movedRows = api.up
                    ? api.allRows.slice(event.position.current + 1, event.position.original + 1)
                    : api.allRows.slice(event.position.original, event.position.current);

                api.originalSequence = parseInt($(event.row).find(".stsequence div").attr("rel"));
                api.currentSequence = parseInt($(api.movedRows[api.up ? 0 : api.movedRows.length - 1]).attr("rel"));


                api.updateSequence = function () {
                    $(event.row).find(".stsequence div").attr("rel", api.currentSequence);

                    $(api.movedRows).each(function (idx, div) {
                        var $div = $(div);

                        $div.attr("rel", api.up
                            ? parseInt($div.attr("rel")) + 1
                            : parseInt($div.attr("rel")) - 1);
                    });
                };

                return api;
            })({});
        }
    };
});
