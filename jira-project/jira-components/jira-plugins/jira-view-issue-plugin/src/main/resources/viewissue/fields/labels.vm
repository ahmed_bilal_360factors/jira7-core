#* @vtlvariable name="issue" type="com.atlassian.jira.issue.Issue" *#
#**
 * The lozenges for the labels system and custom fields (with an optional edit link). Also see labelsLozenges.jsp
 * which returns the same HTML for the in-line updating of labels fields.
 *
 * @param i18n i18n helper
 * @param user the user issuing the server request
 * @param labelUtil labels helper
 * @param issueId the ID of the issue; may be null for bulk edit preview
 * @param fieldId the labels field name (e.g., 'labels' or 'customfield_10000')
 * @param customFieldIdAsLong custom field (numeric) ID or false if it is the labels system field
 * @param labels the labels to render
 * @param canEdit true if the edit labels link should be rendered, false otherwise; note that the issue navigator
 *     currently doesn't allow editing, so if this changes, then the DOM IDs rendered here need to be modified.
 * @param readOnly true if the labels are read only, false otherwise
 * @param baseurl the JIRA instance's base URL
 * @param searchLinkGenerator tool for easier creation of search links for particular field value
 *#
#macro(labelsLozenges $i18n $user $labelUtil $issueId $fieldId $customFieldIdAsLong $labels $canEdit $noLink $readOnly $baseUrl $prefix)
<div class="labels-wrap value">
    #if ($labels && $labels.size() > 0)
        <ul class="labels" #if ($issueId)id="#if ($prefix)${prefix}#end${fieldId}-${issueId}-value"#end>
            #foreach($label in $labels)
                #if ($noLink)
                    <li><a class="lozenge nolink" name="$fieldId" title="${textutils.htmlEncode($!label.label)}"><span>${textutils.htmlEncode($!label.label)}</span></a></li>
                #else
                    #if ($customFieldIdAsLong)
                        <li><a class="lozenge" href="${baseUrl}${labelUtil.getLabelSearchPath($user, $customFieldIdAsLong, $!label.label)}" title="${textutils.htmlEncode($!label.label)}"><span>${textutils.htmlEncode($!label.label)}</span></a></li>
                    #else
                        <li><a class="lozenge" href="${baseUrl}${labelUtil.getLabelSearchPath($user, $!label.label)}" title="${textutils.htmlEncode($!label.label)}"><span>${textutils.htmlEncode($!label.label)}</span></a></li>
                    #end
                #end
            #end
            #if ($canEdit && $readOnly == false)
            ## Edit links don't appear in issue tables, so id's of the form edit-labels-(labels|customfield_10000) are good enough.
                <li><a class="aui-icon aui-icon-small aui-iconfont-edit edit-labels" id="edit-labels-${issueId}-${fieldId}" href="${baseUrl}/secure/EditLabels!default.jspa?id=${issueId}&noLink=${noLink}#if ($customFieldIdAsLong)&customFieldId=${customFieldIdAsLong}#end"><span>${i18n.getText('label.edit.title')}</span></a></li>
            #end
        </ul>
    #else
        <span class="labels" #if ($issueId)id="#if ($prefix)${prefix}#end${fieldId}-${issueId}-value"#end>${i18n.getText('common.words.none')}</span>
        #if ($canEdit && $readOnly == false)
        ## Edit links don't appear in issue tables, so id's of the form edit-labels-(labels|customfield_10000) are good enough.
            <a class="aui-icon aui-icon-small aui-iconfont-edit edit-labels" id="edit-labels-${issueId}-${fieldId}" href="${baseUrl}/secure/EditLabels!default.jspa?id=${issueId}&noLink=${noLink}#if ($customFieldIdAsLong)&customFieldId=${customFieldIdAsLong}#end"><span>${i18n.getText('label.edit.title')}</span></a>
        #end
    #end
</div>
#end

<li class="item full-width">
    <div class="wrap" id="wrap-labels">
        <strong class="name">$i18n.getText("issue.field.labels"):</strong>
        #set($labels = $issue.labels)
        #labelsLozenges($i18n $user $labelUtil $issue.id 'labels' false $labels $canEdit false false $baseurl '')
    </div>
</li>