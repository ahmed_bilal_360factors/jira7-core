require([
    'jquery',
    'wrm/data',
    'wrm/context-path',
    'jira/issuetable',
    'viewissue/subtasks/subtasks-helper'
], function ($, WrmData, wrmContextPath, IssueTable, SubtasksHelper) {
    WrmData.claim("com.atlassian.jira.jira-view-issue-plugin:controller-subtasks.controller.subtasks.parameters", function (config) {
        var ready = false;
        IssueTable.on(IssueTable.Events.ON_INIT, function(){
            if(!ready) {
                ready = true;

                var url = wrmContextPath() + config.url.replace("{issueId}", JIRA.Issue.getIssueId());
                $.get(url).success(function (canDragAndDrop) {
                    if (canDragAndDrop) {
                        IssueTable.on(IssueTable.Events.ON_ROW_DRAG, function (event) {
                            if (SubtasksHelper.moved(event)) {
                                var subtasks = SubtasksHelper.create(event);

                                $.ajax({
                                    url: wrmContextPath() + config.url.replace("{issueId}", JIRA.Issue.getIssueId()),
                                    type: "POST", // for jquery less then 1.9.0
                                    method: "POST", // for jquery since 1.9.0
                                    accept: "application/json",
                                    contentType: "application/json",
                                    data: JSON.stringify({
                                        original: subtasks.originalSequence,
                                        current: subtasks.currentSequence
                                    }),
                                    dataType: "json"
                                }).success(function () {
                                    subtasks.updateSequence();

                                    subtasks.allRows.each(function (idx, div) {
                                        $(div).html(idx + 1 + ".");
                                    });
                                }).fail(function () {
                                    IssueTable.cancelDragging();
                                });

                                AJS.trigger("analyticsEvent", {
                                    name: "com.atlassian.jira.view-issue-plugin.subtasks.drag.and.drop",
                                    data: {
                                        direction: subtasks.up ? "up" : "down",
                                        rowsCount: subtasks.allRows.length,
                                        rowsUpdated: subtasks.movedRows.length
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }, function () {
        //nothing to do in case of failure
        console.warn("Unable to read data from WRM");
    });
});
