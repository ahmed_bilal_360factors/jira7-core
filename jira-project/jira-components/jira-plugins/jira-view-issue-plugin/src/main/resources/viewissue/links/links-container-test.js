AJS.test.require('com.atlassian.jira.jira-view-issue-plugin:linkingmodule-js', function () {
    var $ = require('jquery');
    var skate = require('jira/skate');

    function createLinkHtml(id, isRemoteLink, isCollapsed) {
        var attributes = [];
        if(isRemoteLink) {
            attributes.push('data-remote-link-id="' + id + '"');
            attributes.push('data-requires-async-loading="data-requires-async-loading"');
        }
        if(isCollapsed) {
            attributes.push('class="collapsed-link"');
        }

        return '<dd id="' + id + '"' + attributes.join(' ') + '>' +
                '<div class="link-content">' +
                    '<div class="link-loading"></div>' +
                    '<div class="link-throbber"></div>' +
                '</div>' +
                '<div class="delete-link" id="delete_' + id + '"></div>' +
            '</dd>';
    }

    // Skate doesn't like to be context.required more than once, hence this is moved up here from module.setup
    var sandbox = sinon.sandbox.create();
    sandbox.useFakeServer();
    var SmartAjax = {makeRequest: sandbox.stub().returns(new jQuery.Deferred().resolve())};
    var context = AJS.test.mockableModuleContext();
    context.mock("jira/ajs/ajax/smart-ajax", SmartAjax);
    context.mock("wrm/context-path", sandbox.stub().returns(""));
    context.require('jira/view-issue-plugin/links/links-container');

    module("viewissue/links/links-container", {
        setup: function () {
            var $fixture = $("#qunit-fixture");
            this.$linksContainer = $('<div class="links-container" data-default-link-icon="/foo/bar/icon.jpg">' +
                    '<dl class="links-list">' +
                        '<dt title="visible">Visible</dt>' +
                        createLinkHtml(1, false, false) +
                        createLinkHtml(2, false, false) +
                        createLinkHtml(3, true, false) +
                        '</dl>' +
                        '<dl class="links-list collapsed-links-list">' +
                        '<dt title="collapsed">Collapsed</dt>' +
                        createLinkHtml(4, false, true) +
                        createLinkHtml(5, true, true) +
                    '</dl>' +
                    '<div id="show-more-links">' +
                        '<a id="show-more-links-link" href="#">Show more</a> <span></span>' +
                    '</div>' +
                '</div>'
            );
            this.$linksContainer.appendTo($fixture);
            this.$remoteIssue3 = this.$linksContainer.find('#3');

            skate.init(this.$linksContainer.get(0));
        },
        teardown: function () {
            SmartAjax.makeRequest.reset();
        }
    });

    test("When attached, visible remote link summaries are requested", function() {
        sinon.assert.calledOnce(SmartAjax.makeRequest);
        sinon.assert.calledWith(SmartAjax.makeRequest, {
            url: "/rest/viewIssue/1/remoteIssueLink/render/3",
            success: sinon.match.func,
            error: sinon.match.func
        });
    });

    test("When the 'show more links' link is clicked, the collapsed remote link summaries are requested", function() {
        // trigger a native click event
        this.$linksContainer.find('#show-more-links-link').get(0).click();

        sinon.assert.calledWith(SmartAjax.makeRequest, {
            url: "/rest/viewIssue/1/remoteIssueLink/render/5",
            success: sinon.match.func,
            error: sinon.match.func
        });
    });

    test("When the 'show more links' link is clicked, the collapsed links become visible", function() {
        // trigger a native click event
        this.$linksContainer.find('#show-more-links-link').get(0).click();

        equal(this.$linksContainer.find('.collapsed-links-list').length, 0);
        equal(this.$linksContainer.find('.collapsed-link').length, 0);
    });

    test("When the remote link request succeeds, the link content is updated", function() {
        SmartAjax.makeRequest.firstCall.args[0].success("foo");

        equal(this.$remoteIssue3.text(), "foo");
    });

    test("When the remote link request fails, a message is shown", function() {
        SmartAjax.makeRequest.firstCall.args[0].error();

        equal(this.$remoteIssue3.text(), "common.concepts.loading.failed");
    });

    test("When the remote link request fails, the default icon is shown", function() {
        SmartAjax.makeRequest.firstCall.args[0].error();

        equal(this.$remoteIssue3.find('img').attr('src'), "/foo/bar/icon.jpg");
    });
});
