package com.atlassian.jira.dev.resources;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestInstrumentStylesheetFilter {
    private static final String MODULE_NAME1 = "com.atlassian.auiplugin:layer";
    private static final String MODULE_NAME2 = "com.atlassian.auiplugin:aui-reset";
    private static final String MODULE_NAME3 = "com.atlassian.auiplugin:aui-page-layout";

    private final static String RESOURCE_URL = "http://localhost:8090/jira/s/cf68c932ec46dcad7a69c4c6a6ccaa49-CDN/en_AU-3ppa56/70102/b6b48b2829824b869586ac216d119363/5.7.31/_/download/resources/" + MODULE_NAME1 + "/layer.css";
    private final static String RESOURCE_CONTENT = "body { width: 100% }\nh1, b { height: 100% }";

    private final static String RESOURCE_CONTENT_TEMPLATE = "body, instrumented-resource[key='%s'] {\n\twidth: 100.0%%;\n}\n" +
            "h1, b, instrumented-resource[key='%s'] {\n\theight: 100.0%%;\n}\n";

    private static final String RESOURCE_KEY1 = MODULE_NAME1 + "/layer.css";
    private static final String RESOURCE_KEY2 = MODULE_NAME2 + "/less/aui-reset.less";
    private static final String RESOURCE_KEY3 = MODULE_NAME3 + "/less/adg-page-layout.less";

    private final static String RESOURCE_CONTENT_INSTRUMENTED = String.format(RESOURCE_CONTENT_TEMPLATE, RESOURCE_KEY1, RESOURCE_KEY1);

    private final static String BATCH_URL = "/jira/s/a99f18eaa90ff3c38f11924758e20d59-CDN/en_AU-3ppa56/70102/b6b48b2829824b869586ac216d119363/6f43e8a0e527941a0155d0cd244c61a3/_/download/contextbatch/js/_super/batch.js?atlassian.aui.raphael.disabled=true&locale=en-AU";
    private final static String BATCH_CONTENT = "/* module-key = '" + MODULE_NAME2 + "', location = '/less/aui-reset.less' */\n" +
            RESOURCE_CONTENT + "\n" +
            "/* module-key = '" + MODULE_NAME3 + "', location = '/less/adg-page-layout.less' */\n" +
            RESOURCE_CONTENT;
    private final static String BATCH_CONTENT_INSTRUMENTED = "/* module-key = '" + MODULE_NAME2 + "', location = '/less/aui-reset.less' */\n" +
            String.format(RESOURCE_CONTENT_TEMPLATE, RESOURCE_KEY2, RESOURCE_KEY2) + "\n" +
            "/* module-key = '" + MODULE_NAME3 + "', location = '/less/adg-page-layout.less' */\n" +
            String.format(RESOURCE_CONTENT_TEMPLATE, RESOURCE_KEY3, RESOURCE_KEY3) + "\n";

    @Mock
    private PluginAccessor pluginAccessor;

    private InstrumentStylesheetsFilter filter;

    @Before
    public void setUp() throws Exception {
        filter = new InstrumentStylesheetsFilter();
        filter.pluginAccessor = pluginAccessor;

        when(pluginAccessor.getEnabledPluginModule(MODULE_NAME1)).thenReturn(mockDescriptor(MODULE_NAME1));
        when(pluginAccessor.getEnabledPluginModule(MODULE_NAME2)).thenReturn(mockDescriptor(MODULE_NAME2));
        when(pluginAccessor.getEnabledPluginModule(MODULE_NAME3)).thenReturn(mockDescriptor(MODULE_NAME3));
    }

    @Test
    public void testShouldInstrumentDownloadResource() throws Exception {
        String result = filter.processResource(RESOURCE_URL, RESOURCE_CONTENT);
        assertEquals(RESOURCE_CONTENT_INSTRUMENTED, result);
    }

    @Test
    public void testShouldInstrumentBatchResource() throws Exception {
        String result = filter.processResource(BATCH_URL, BATCH_CONTENT);
        assertEquals(BATCH_CONTENT_INSTRUMENTED, result);
    }

    private ModuleDescriptor mockDescriptor(final String resourceKey) {
        return mock(WebResourceModuleDescriptor.class);
    }
}
