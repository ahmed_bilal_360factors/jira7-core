package com.atlassian.jira.dev.resources;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestInstrumentScriptFilter {
    private static final String MODULE_NAME_DND = "com.atlassian.jira.plugins.jira-dnd-attachment-plugin:dnd-amd";
    private static final String MODULE_NAME_WIKI = "com.atlassian.jira.plugins.jira-wiki-editor:wiki-editor-resources";
    private static final String MODULE_NAME_PK = "com.atlassian.pocketknife.featureflags-plugin:pocketknife-feature-flags";

    private static final String RESOURCE_URL = "/jira/s/441db44e64255fa065ed48517dff9d91-CDN/en_AUf6t4wa/70102/b6b48b2829824b869586ac216d119363/1.9.0/_/download/resources/" + MODULE_NAME_WIKI + "/WikiEditorUtil.js?locale=en-AU";

    private static final String RESOURCE_CONTENT = "function() { log('foo', 0xD512); }";
    private static final String RESOURCE_CONTENT_INSTRUMENTED = ";_ijs=typeof _ijs=='undefined'?{}:_ijs;\n"
            + ";_ijs['" + MODULE_NAME_WIKI + "/WikiEditorUtil.js?locale=en-AU']=0;\n"
            + "function() {\n"
            + "  ;_ijs['" + MODULE_NAME_WIKI + "/WikiEditorUtil.js?locale=en-AU']++;\n"
            + "  log('foo', 0xD512);\n"
            + "}\n";
    private static final String BATCH_URL = "/jira/s/d41d8cd98f00b204e9800998ecf8427e-CDN/en_AU-92sgf5/70102/b6b48b2829824b869586ac216d119363/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js";
    private static final String BATCH_CONTENT = ";\n"
            + "/* module-key = '" + MODULE_NAME_PK + "', location = '/featureflags/feature-manager.js' */\n"
            + RESOURCE_CONTENT + "\n"
            + "/* module-key = '" + MODULE_NAME_DND + "', location = 'js/jira-amd.js' */\n"
            + RESOURCE_CONTENT;

    private static final String BATCH_CONTENT_INSTRUMENTED = "/* module-key = '" + MODULE_NAME_PK + "', location = '/featureflags/feature-manager.js' */\n"
            + ";_ijs=typeof _ijs=='undefined'?{}:_ijs;\n"
            + ";_ijs['" + MODULE_NAME_PK + "/featureflags/feature-manager.js']=0;\n"
            + "function() {\n"
            + "  ;_ijs['" + MODULE_NAME_PK + "/featureflags/feature-manager.js']++;\n"
            + "  log('foo', 0xD512);\n"
            + "}\n"
            + "\n"
            + "/* module-key = '" + MODULE_NAME_DND + "', location = 'js/jira-amd.js' */\n"
            + ";_ijs=typeof _ijs=='undefined'?{}:_ijs;\n"
            + ";_ijs['" + MODULE_NAME_DND + "/js/jira-amd.js']=0;\n"
            + "function() {\n"
            + "  ;_ijs['" + MODULE_NAME_DND + "/js/jira-amd.js']++;\n"
            + "  log('foo', 0xD512);\n"
            + "}\n"
            + "\n";

    private InstrumentScriptFilter filter;

    @Mock
    private PluginAccessor pluginAccessor;

    @Before
    public void setUp() throws Exception {
        filter = new InstrumentScriptFilter();
        filter.pluginAccessor = pluginAccessor;

        when(pluginAccessor.getEnabledPluginModule(MODULE_NAME_WIKI)).thenReturn(mockDescriptor(MODULE_NAME_WIKI));
        when(pluginAccessor.getEnabledPluginModule(MODULE_NAME_DND)).thenReturn(mockDescriptor(MODULE_NAME_DND));
    }

    @Test
    public void shouldInstrumentDownloadResource() throws Exception {
        String result = filter.processResource(RESOURCE_URL, RESOURCE_CONTENT);
        assertEquals(RESOURCE_CONTENT_INSTRUMENTED, result);
    }

    @Test
    public void shouldInstrumentBatchContent() throws Exception {
        String result = filter.processResource(BATCH_URL, BATCH_CONTENT);
        assertEquals(BATCH_CONTENT_INSTRUMENTED, result);
    }

    private ModuleDescriptor mockDescriptor(final String resourceKey) {
        return mock(WebResourceModuleDescriptor.class);
    }
}
