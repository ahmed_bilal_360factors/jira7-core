require(["internal/browser-metrics", "jira/util/logger"], function defineBrowserMetricsTracer(metrics, logger) {
    "use strict";
    metrics.subscribe(function traceBrowserMetrics(beacon) {
        var eventKey = beacon.report.key;
        logger.trace("jira.browsermetric.event", beacon.report);
        logger.trace("jira.browsermetric.event." + eventKey, beacon.report);
    });
});
