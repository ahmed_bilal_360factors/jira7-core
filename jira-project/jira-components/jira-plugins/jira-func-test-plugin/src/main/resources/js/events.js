define('jira/func/event',
    ['jquery', 'underscore', 'backbone', 'marionette', 'wrm/context-path'],
    function($, _, Backbone, Marionette, wrmContextPath) {
        "use strict";
        var contextPath = wrmContextPath();

        var View = Marionette.ItemView.extend({
            tagName: "div",
            template: JIRA.Func.Event.Templates.viewEvents,
            ui: {
                start: "#event-start",
                stop: "#event-stop",
                log: "#event-log",
                clear: "#event-clear",
                mark: "#event-mark"
            },
            events: {
                "click #event-start": "_start",
                "click #event-stop": "_stop",
                "click #event-clear": "_clear",
                "click #event-mark": "_mark"
            },
            _start: function () {
                this.ui.start.prop("disabled", true);
                this.ui.stop.prop("disabled", false);
                this.trigger("start");
            },
            _stop: function () {
                this.ui.start.prop("disabled", false);
                this.ui.stop.prop("disabled", true);
                this.trigger("stop");
            },
            _clear: function () {
                this.ui.log.empty();
            },
            _mark: function() {
                this.addMessage("Marked at " + new Date().toLocaleString());
            },
            addEvent: function (event) {
                var $event = $(JIRA.Func.Event.Templates.viewEvent({"event": event}));
                $event.appendTo(this.ui.log);
            },
            addMessage: function (msg) {
                this.addEvent(msg);
            }
        });

        var REST = contextPath + "/rest/func-test/1.0/event";

        var Controller = function (options) {
            this.view = options.view;
            this.listenTo(this.view, "start", this._start.bind(this));
            this.listenTo(this.view, "stop", this._stop.bind(this));
            this.running = false;
        };

        _.extend(Controller.prototype, Backbone.Events, {
            _start: function () {
                this.view.addMessage("Started Polling for Events");
                this.running = true;
                this._poll(true);
            },
            _get: function(position) {
                return $.ajax({
                    url: REST,
                    context: this,
                    type: "GET",
                    contentType: "application/json",
                    dataType: "json",
                    data: {
                        position: position
                    }
                });
            },
            _poll: function(start) {
                var pos = start ? -1 : this.position || 0;
                this._get(pos).done(function (data) {

                    if (!this.running) {
                        return;
                    }

                    if (!start || data.position < pos) {
                        if (data.position > pos) {
                            //Dropped some events.
                            this.view.addEvent("<Dropped " + (data.position - pos) + " Events>");
                        }

                        _.each(data.events, function (e) {
                            this.view.addEvent(e);
                        }, this);

                        this.position = data.position + data.events.length;
                        this.timer = window.setTimeout(this._poll.bind(this, false), 1000);
                    } else {
                        this.position = data.position;
                        this.timer = window.setTimeout(this._poll.bind(this, false), 1000);
                    }
                });
            },
            _stop: function() {

                delete this.position;
                this.running = false;

                if (this.timer) {
                    window.clearTimeout(this.timer);
                    delete this.timer;
                }

                $.ajax({
                    url: REST,
                    type: "DELETE",
                    context: this,
                    contentType: "application/json",
                    dataType: "json"
                }).done(function () {
                    this.view.addMessage("Stopped Polling for Events");
                });
            }
        });

        return function ($el) {
            var view = new View();
            $el = $($el);
            $el.append(view.render().$el);
            return new Controller({
                view: view
            });
        };
    }
);

require(['jquery', 'jira/func/event'], function($, event) {
    "use strict";

    $(function () {
        var $container = $("#events-container");
        event($container);
    });
});

