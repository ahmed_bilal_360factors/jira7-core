require(['jira/util/logger', 'jquery'], function(logger, $) {
    'use strict';

    var nop = function() {};
    nop.$noAlert = true;

    var nopOut = function(name) {
        logger.log("***");
        logger.log("*** Replacing window." + name + " with a NOOP");
        logger.log("***");
        window[name] = nop;
    };

    // this needs to run after all other onbeforeunload hackery
    $(function() {
        setTimeout(function() {
            nopOut('alert');
            nopOut('onbeforeunload');
        }, 0);
    });
});
