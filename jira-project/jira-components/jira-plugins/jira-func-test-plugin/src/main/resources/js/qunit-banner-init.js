require(['jquery', 'wrm/data', 'wrm/context-path'], function($, wrmData, wrmContextPath) {
    'use strict';

    var showBanner = wrmData.claim('com.atlassian.jira.dev.func-test-plugin:qunit-locale-banner.show-qunit-banner');

    if(showBanner) {
        var $bannerContainer = $('<div id="qunit-banner-container"></div>');
        var warningMsg = aui.message.warning({
            content: '<p>Looks like you were running some qunit tests.  Time to <a href="#" id="reset-lnk">reset your locale</a>?</p>'
        });
        $bannerContainer.append(warningMsg);
        $bannerContainer.on("click", "#reset-lnk", function(e) {
            e.preventDefault();
            $.ajax({
                type: "PUT",
                url: wrmContextPath() + "/rest/func-test/1.0/qunittranslation",
                complete: function() {
                    $bannerContainer.hide();
                    window.location.reload();
                }
            });
        });

        $(function() {
            $("#header").prepend($bannerContainer);
        });
    }
});
