(function($) {
    'use strict';

    var list = [];

    /**
     * first argument is the key of this trace (usually a dot-qualified identifier)
     * the remaining arguments are arbitrary, but preserved.
     */
    var exports = function(id) {
        if (exports.callback) {
            exports.callback.apply(this, arguments);
        }
        var rest = Array.prototype.slice.call(arguments);
        rest.shift();
        list.push({
            ts: (new Date).getTime(),
            id: id,
            args: rest
        });
    };

    exports.drain = function() {
        var r = list;
        list = [];
        return r;
    };

    exports.inspect = function() {
        return list;
    };

    $(document).ajaxComplete(function(event, request, settings) {
        exports('AJS.$.ajaxComplete', settings.url);
    });

    $(document).ajaxSuccess(function(event, request, settings) {
        exports('AJS.$.ajaxSuccess', settings.url);
    });

    $(document).ajaxError(function(event, request, settings) {
        exports('AJS.$.ajaxError', settings.url);
    });

    // export trace as a global for legacy + debugging.
    if ('JIRA' in window) {
        window.JIRA.trace = exports;
    }
    // the jira/util/logger implementation should pick this up.
    window['__tracer'] = exports;
})(AJS.$);
