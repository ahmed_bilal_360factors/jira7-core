/**
 * @module CriticalCSS
 *
 * Use this module to find out which stylesheets are used on current page. This is an utility module,
 * can be used manually and also via Java class(CriticalCSS) in web driver tests.
 *
 * @since 7.1
 */
define('jira/func/critical-css',
    ['underscore', 'jquery'],
    function(_, $) {
        "use strict";

        var matchResource = function(href) {
            return (href.match(/resources\/(.*)/) || [])[1] || href;
        };

        var matchInstrumentation = function(selectorText) {
            return selectorText && (selectorText.match(/instrumented-resource\[key=("|')([^'^"]+)('|")\]/) || [])[2];
        };

        var getCritical = function(scanFullPage) {
            // fallback for Firefox
            if (!window.getMatchedCSSRules) {
                return getAll().map(function(resourceName) {
                    return getUsage(resourceName, scanFullPage);
                }).filter(function(usage) {
                    return usage.percentage > 0;
                }).map(function(usage) {
                    return usage.resource;
                });
            }

            var styles = [];

            var walker = document.createTreeWalker(document, NodeFilter.SHOW_ELEMENT, function(node) {
                return NodeFilter.FILTER_ACCEPT;
            }, true);

            while(walker.nextNode()) {
                var node = walker.currentNode;
                if($(node).is(':visible') || scanFullPage) {
                    var rules = window.getMatchedCSSRules(node);
                    _.each(rules, function(rule) {
                        styles.push(matchInstrumentation(rule.selectorText));
                    });
                }
            }

            return _.uniq(styles).map(matchResource);
        };

        var getAll = function() {
            var resourceNames = _.toArray(document.styleSheets)
                .reduce(function (r, style) {
                    _.each(style.cssRules, function (rule) {
                        var resourceName = matchInstrumentation(rule.selectorText);
                        if (resourceName) {
                            r[resourceName] = true;
                        }
                    });
                    return r;
                }, {});
            return Object.keys(resourceNames);
        };

        var sumRuleWeight = function(total, rule) {
            return total + rule.cssText.length;
        };

        var getSelector = function (rule) {
            return rule.selectorText;
        };

        /**
         * Get fake style object using CSS instrumentation.
         *
         * @param resourceName
         * @returns {{cssRules: *}}
         */
        var getStyle = function(resourceName) {
            var cssRules = _.toArray(document.styleSheets).reduce(function (r, style) {
                return r.concat(_.filter(style.cssRules, function (rule) {
                    return rule.selectorText && rule.selectorText.indexOf(resourceName) > 0;
                }));
            }, []);

            if (cssRules.length === 0) {
                return null;
            }

            return {
                cssRules: cssRules
            };
        };

        var getUsage = function(resourceName, scanFullPage) {
            var style = _.toArray(document.styleSheets).filter(function(style) {
                return style.href && style.href.indexOf("resources/" + resourceName) >= 0;
            })[0];

            if (!style) {
                style = getStyle(resourceName);
            }

            if (!style) {
                throw "Stylesheet not found: " + resourceName;
            }

            var allRules = _.toArray(style.cssRules);
            var usedRules = allRules.filter(function(rule) {
                var $nodes = $(rule.selectorText);
                return scanFullPage && $nodes.length > 0 || $nodes.is(':visible');
            });

            var usedLength = usedRules.reduce(sumRuleWeight, 0);
            var totalLength = allRules.reduce(sumRuleWeight, 0);
            var percentage = totalLength > 0 ? usedLength / totalLength : 0;

            return {
                resource: resourceName,
                href: style.href,
                percentage: percentage,
                usedLength: usedLength,
                totalLength: totalLength,
                used: usedRules,
                usedSelectors: usedRules.map(getSelector),
                all: allRules,
                allSelectors: allRules.map(getSelector),
                styleSheet: style
            };
        };

        return {
            getCritical: getCritical,
            getAll: getAll,
            getUsage: getUsage,
            getStyle: getStyle
        };
});
