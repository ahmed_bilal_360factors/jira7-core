/**
 * @module CriticalJS
 *
 * Create reports from JS instrumentation.
 *
 * @since 7.1
 */
define('jira/func/critical-js', [
    'jquery'
], function(
    $
) {
    "use strict";

    /**
     * Return an object which contains number of calls for given resource key.
     * @returns {Object}
     */
    function getInstrumentLog() {
        return window._ijs || {};
    }

    /**
     * Get usage information for given resource key.
     * @param resourceName
     * @returns {{resource: *, calls: *}}
     */
    function getUsage(resourceName) {
        var script = getInstrumentLog(resourceName);

        if (!script) {
            throw "Script not found: " + resourceName;
        }

        return {
            resource: resourceName,
            calls: getInstrumentLog()[resourceName]
        };
    }

    /**
     * Get resource keys for all loaded javascript resources.
     * @returns {Array}
     */
    function getAll() {
        return Object.keys(getInstrumentLog());
    }

    /**
     * Get resource keys for all javascript resources that were called more than once.
     * @returns {Array.<T>}
     */
    function getCritical() {
        var instrumentLog = getInstrumentLog();
        return Object.keys(instrumentLog).filter(function(resourceName){
            return instrumentLog[resourceName] > 1;
        });
    }

    return {
        getCritical: getCritical,
        getAll: getAll,
        getUsage: getUsage,
        getInstrumentLog: getInstrumentLog
    };
});
