(function() {
    'use strict';

    var list = [];

    window.onerror = function(message, url, lineNumber) {
        list.push({
            message: message,
            url: url,
            lineNumber: lineNumber
        });
        return false;
    };

    // export error list as a global for legacy + debugging.
    if ('JIRA' in window) {
        if (!window.JIRA.DevMode) { window.JIRA.DevMode = {}; }
        if (!window.JIRA.DevMode.Errors) { window.JIRA.DevMode.Errors = {}; }
        window.JIRA.DevMode.Errors = list;
    }

    // export as AMD
    define('jira/func/error-tracker', ['exports'], function(exports) {
        exports.inspect = function() {
            return list;
        };
    });
})();
