package com.atlassian.jira.dev.resources;

import javax.servlet.ServletResponse;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Instrument javascript files, so their usage can be measured.
 */
public class InstrumentScriptFilter extends AbstractInstrumentResourceFilter {
    private static final Pattern RESOURCE_PATTERN = Pattern.compile("^(?:.*)download/resources/(.*)$");
    private static final Pattern BATCH_PATTERN = Pattern.compile("^(.*)download/(contextbatch|batch)/(.*)$");

    private InstrumentScript instrumentScript = new InstrumentScript();

    protected boolean acceptResource(final ServletResponse response) {
        return response.getContentType() != null && response.getContentType().contains("application/javascript");
    }

    protected AbstractInstrumentResource getInstrumentResource() {
        return instrumentScript;
    }

    protected Matcher matchResource(final String requestUrl) {
        return RESOURCE_PATTERN.matcher(requestUrl);
    }

    protected Matcher matchBatch(final String requestUrl) {
        return BATCH_PATTERN.matcher(requestUrl);
    }

    protected boolean instrumentEnabled() {
        return jiraProperties.getBoolean("instrument-js");
    }
}
