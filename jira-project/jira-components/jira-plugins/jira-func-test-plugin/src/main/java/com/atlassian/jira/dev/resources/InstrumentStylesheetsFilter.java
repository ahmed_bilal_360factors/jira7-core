package com.atlassian.jira.dev.resources;

import javax.servlet.ServletResponse;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Instrument stylesheet files, their usage can be measured.
 *
 * @since v7.1
 */
public class InstrumentStylesheetsFilter extends AbstractInstrumentResourceFilter {
    private static final Pattern RESOURCE_PATTERN = Pattern.compile("^(?:.*)download/resources/(.*)$");
    private static final Pattern BATCH_PATTERN = Pattern.compile("^(.*)download/(contextbatch|batch)/(.*)$");

    private InstrumentStylesheet instrumentStylesheet = new InstrumentStylesheet();

    protected boolean acceptResource(final ServletResponse response) {
        final String contentType = response.getContentType();
        return contentType != null && contentType.contains("text/css");
    }

    protected AbstractInstrumentResource getInstrumentResource() {
        return instrumentStylesheet;
    }

    protected Matcher matchResource(final String requestUrl) {
        return RESOURCE_PATTERN.matcher(requestUrl);
    }

    protected Matcher matchBatch(final String requestUrl) {
        return BATCH_PATTERN.matcher(requestUrl);
    }

    protected boolean instrumentEnabled() {
        return jiraProperties.getBoolean("instrument-css");
    }
}
