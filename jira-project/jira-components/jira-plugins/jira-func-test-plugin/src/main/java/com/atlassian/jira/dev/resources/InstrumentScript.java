package com.atlassian.jira.dev.resources;

import org.mozilla.javascript.CompilerEnvirons;
import org.mozilla.javascript.IRFactory;
import org.mozilla.javascript.RhinoException;
import org.mozilla.javascript.Token;
import org.mozilla.javascript.ast.AstNode;
import org.mozilla.javascript.ast.AstRoot;
import org.mozilla.javascript.ast.Block;
import org.mozilla.javascript.ast.Comment;
import org.mozilla.javascript.ast.FunctionNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Utility class, which can instrument provided javascript source, instrumentation format is compatible with CriticalJS javascript module.
 *
 * @since 7.1
 */
public class InstrumentScript extends AbstractInstrumentResource {
    private static final Logger log = LoggerFactory.getLogger(InstrumentScript.class);

    private static final int LANGUAGE_VERSION = 180;

    private final CompilerEnvirons env;

    public InstrumentScript() {
        env = new CompilerEnvirons();
        env.setRecoverFromErrors(true);
        env.setLanguageVersion(LANGUAGE_VERSION);
    }

    public String instrumentResource(String source, String resourceName) throws IOException {
        final String instrumentPrologue = ";_ijs=typeof _ijs=='undefined'?{}:_ijs;\n"
                + String.format(";_ijs['%s']=0;\n", resourceName);
        final String instrumentFragment = String.format(";_ijs['%s']++;\n", resourceName);

        // traverse AST and inject instrumentation fragment
        final AstRoot root;
        try {
            root = parse(source, resourceName);
        } catch (RhinoException e) {
            log.error(String.format("Unable to parse resource: %s", resourceName), e);
            return source;
        }

        final List<FunctionNode> functions = new ArrayList<>();
        root.visitAll(node -> {
            if (node instanceof FunctionNode) {
                AstNode block = ((FunctionNode) node).getBody();
                if (block instanceof Block) {
                    block.addChildToFront(new Comment(0, 10, Token.CommentType.BLOCK_COMMENT, instrumentFragment));
                    functions.add(((FunctionNode) node));
                }
            }

            return true;
        });

        // no function nodes, let's assume that it is actually executing some code
        if (functions.isEmpty()) {
            return instrumentPrologue + source;
        }

        try {
            // make sure that result can be parsed, Mozilla Rhino is not free of bugs
            final String result = instrumentPrologue + root.toSource();
            parse(result, resourceName);
            return result;
        } catch (RhinoException e) {
            log.error(String.format("Exception parsing instrumented resource: %s", resourceName), e);
            // in case of any problems, leave information in instrumentation log
            return instrumentPrologue + source + String.format("\n;_ijs['%s']=-1;\n", resourceName);
        }
    }

    public AstRoot parse(final String source, final String resourceName) throws IOException {
        StringReader strReader = new StringReader(source);
        return getParser().parse(strReader, resourceName, 0);
    }

    private IRFactory getParser() {
        return new IRFactory(env);
    }
}
