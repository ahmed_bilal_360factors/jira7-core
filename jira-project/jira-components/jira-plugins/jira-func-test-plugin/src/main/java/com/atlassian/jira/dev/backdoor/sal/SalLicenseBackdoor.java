package com.atlassian.jira.dev.backdoor.sal;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.sal.api.license.LicenseHandler;
import com.atlassian.sal.api.license.MultiProductLicenseDetails;
import com.atlassian.sal.api.license.ProductLicense;
import com.atlassian.sal.api.validate.ValidationResult;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import static com.atlassian.jira.dev.backdoor.util.CacheControl.never;

/**
 * @since v7.0
 */
@Path("sal/license")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
@AnonymousAllowed
public class SalLicenseBackdoor {
    private final com.atlassian.sal.api.license.LicenseHandler licenseHandler;

    public SalLicenseBackdoor(final LicenseHandler licenseHandler) {
        this.licenseHandler = licenseHandler;
    }

    @POST
    @Path("products")
    public Response setProductLicense(ProductLicenseTO productLicense) {
        try {
            licenseHandler.addProductLicense(productLicense.productKey, productLicense.licenseKey);
            return Response.ok()
                    .cacheControl(never()).build();
        } catch (Exception invalidLicense) {
            return Response.serverError()
                    .cacheControl(never())
                    .build();
        }
    }

    @DELETE
    @Path("products/{productKey}")
    public Response deleteProductLicense(@PathParam("productKey") String productKey) {
        try {
            licenseHandler.removeProductLicense(productKey);
            return createOkResponseWithNoCache(Boolean.TRUE);
        } catch (Exception unableToDeleteOnlyLicense) {
            return createOkResponseWithNoCache(Boolean.FALSE);
        }
    }

    @GET
    @Path("products/{productKey}")
    public Response getProductLicense(@PathParam("productKey") String productKey) {
        return createOkResponseWithNoCache(licenseHandler.getRawProductLicense(productKey));
    }

    @GET
    @Path("products")
    public Response getAllProductKeys() {
        return createOkResponseWithNoCache(licenseHandler.getProductKeys());
    }

    @GET
    @Path("product/details/{productKey}")
    public Response getProductLicenseDetails(@PathParam("productKey") String productKey) {
        return createOkResponseWithNoCache(new SingleProductLicenseDetailsViewTO(licenseHandler.getProductLicenseDetails(productKey)));
    }

    @GET
    @Path("product/details")
    public Response getAllProductLicenseDetails() {
        List<MultiProductLicenseDetailsTO> multiProductLicenseDetailsTOs = new ArrayList<MultiProductLicenseDetailsTO>();
        for (MultiProductLicenseDetails multiProductLicenseDetails : licenseHandler.getAllProductLicenses()) {
            MultiProductLicenseDetailsTO multiProductLicenseDetailsTO = new MultiProductLicenseDetailsTO(multiProductLicenseDetails);
            List<ProductLicenseTO> productsTOs = new ArrayList<ProductLicenseTO>();
            for (ProductLicense productLicense : multiProductLicenseDetails.getProductLicenses()) {
                productsTOs.add(new ProductLicenseTO(productLicense));
            }
            multiProductLicenseDetailsTO.productLicenses = productsTOs;
            multiProductLicenseDetailsTOs.add(multiProductLicenseDetailsTO);
        }
        return createOkResponseWithNoCache(multiProductLicenseDetailsTOs);
    }

    @POST
    @Path("product/validate")
    public Response validateLicenseString(ProductLicenseTO licenseResponse) {
        final ValidationResult validationResult = licenseHandler.validateProductLicense(licenseResponse.productKey, licenseResponse.licenseKey, licenseResponse.locale);
        return createOkResponseWithNoCache(new ValidationResultTO(validationResult));
    }

    @POST
    @Path("product/details/decode")
    public Response decodeLicense(ProductLicenseTO licenseResponse) {
        List<ProductLicenseTO> productsTOs = new ArrayList<ProductLicenseTO>();
        final MultiProductLicenseDetails multiProductLicenseDetails = licenseHandler.decodeLicenseDetails(licenseResponse.licenseKey);
        MultiProductLicenseDetailsTO multiProductLicenseDetailsTO = new MultiProductLicenseDetailsTO(multiProductLicenseDetails);
        for (ProductLicense productLicense : multiProductLicenseDetails.getProductLicenses()) {
            productsTOs.add(new ProductLicenseTO(productLicense));
        }
        multiProductLicenseDetailsTO.productLicenses = productsTOs;
        return createOkResponseWithNoCache(multiProductLicenseDetailsTO);
    }

    @GET
    @Path("serverid")
    public Response getServerId() {
        return createOkResponseWithNoCache(licenseHandler.getServerId());
    }

    @GET
    @Path("sen")
    public Response getSupportEntitlementNumber() {
        return createOkResponseWithNoCache(licenseHandler.getSupportEntitlementNumber());
    }

    @GET
    @Path("host/allowsCustomProducts")
    public Response hostAllowsCustomProducts() {
        return createOkResponseWithNoCache(licenseHandler.hostAllowsCustomProducts());
    }

    @GET
    @Path("host/allowsMultipleLicenses")
    public Response hostAllowsMultipleLicenses() {
        return createOkResponseWithNoCache(licenseHandler.hostAllowsMultipleLicenses());
    }

    @GET
    @Path("ping")
    public Response ping() {
        return createOkResponseWithNoCache("pong");
    }

    private Response createOkResponseWithNoCache(Object entity) {
        return Response.ok(entity).cacheControl(never()).build();
    }

    public static class MultiProductLicenseDetailsTO extends BaseLicenseDetailsTO {
        @JsonProperty
        private List<ProductLicenseTO> productLicenses;

        public MultiProductLicenseDetailsTO(final MultiProductLicenseDetails multiProductLicenseDetails) {
            this.isEvaluationLicense = multiProductLicenseDetails.isEvaluationLicense();
            this.licenseTypeName = multiProductLicenseDetails.getLicenseTypeName();
            this.organisationName = multiProductLicenseDetails.getOrganisationName();
            this.isEnterpriseLicensingAgreement = multiProductLicenseDetails.isEnterpriseLicensingAgreement();
            this.isDataCenter = multiProductLicenseDetails.isDataCenter();
            this.maintenanceExpiryDate = multiProductLicenseDetails.getMaintenanceExpiryDate();
            this.licenseExpiryDate = multiProductLicenseDetails.getLicenseExpiryDate();
            this.isPerpetualLicense = multiProductLicenseDetails.isPerpetualLicense();
            this.serverId = multiProductLicenseDetails.getServerId();
            this.description = multiProductLicenseDetails.getDescription();
            this.supportEntitlementNumber = multiProductLicenseDetails.getSupportEntitlementNumber();
        }
    }

    public static class ProductLicenseTO {
        @JsonProperty
        private String licenseKey;
        @JsonProperty
        String productKey;
        @JsonProperty
        boolean isUnlimitedNumberOfUsers;
        @JsonProperty
        int numberOfUsers;
        @JsonProperty
        String productDisplayName;
        @JsonProperty
        Locale locale;

        public ProductLicenseTO() {
        }

        public ProductLicenseTO(ProductLicense productLicense) {
            this.productKey = productLicense.getProductKey();
            this.isUnlimitedNumberOfUsers = productLicense.isUnlimitedNumberOfUsers();
            this.productDisplayName = productLicense.getProductDisplayName();
            this.numberOfUsers = productLicense.getNumberOfUsers();
        }
    }

    public static class BaseLicenseDetailsTO {
        @JsonProperty
        protected boolean isEvaluationLicense;
        @JsonProperty
        protected String licenseTypeName;
        @JsonProperty
        protected String organisationName;
        @JsonProperty
        protected String supportEntitlementNumber;
        @JsonProperty
        protected String description;
        @JsonProperty
        protected String serverId;
        @JsonProperty
        protected boolean isPerpetualLicense;
        @JsonProperty
        protected Date licenseExpiryDate;
        @JsonProperty
        protected Date maintenanceExpiryDate;
        @JsonProperty
        protected boolean isDataCenter;
        @JsonProperty
        protected boolean isEnterpriseLicensingAgreement;
    }

    public static class ValidationResultTO {
        @JsonProperty
        protected Set<String> errorMessages;
        @JsonProperty
        protected Set<String> warningMessages;

        public ValidationResultTO() {
            this.errorMessages = Sets.newHashSet();
            this.warningMessages = Sets.newHashSet();
        }

        public ValidationResultTO(ValidationResult validationResult) {
            this.errorMessages = ImmutableSet.copyOf(validationResult.getErrorMessages());
            this.warningMessages = ImmutableSet.copyOf(validationResult.getWarningMessages());
        }
    }
}