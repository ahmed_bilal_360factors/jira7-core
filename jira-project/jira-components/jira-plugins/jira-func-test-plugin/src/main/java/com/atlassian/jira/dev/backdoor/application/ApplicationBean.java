package com.atlassian.jira.dev.backdoor.application;

import com.atlassian.application.api.Application;
import com.atlassian.application.api.PlatformApplication;
import com.atlassian.application.api.PluginApplication;
import com.atlassian.jira.dev.backdoor.sal.SingleProductLicenseDetailsViewTO;
import com.atlassian.sal.api.license.SingleProductLicenseDetailsView;
import com.google.common.base.Function;
import com.google.common.base.Functions;
import org.codehaus.jackson.annotate.JsonProperty;
import org.joda.time.DateTime;

import static com.atlassian.sal.api.license.ProductLicense.UNLIMITED_USER_COUNT;

/**
 * @since v7.0
 */
public abstract class ApplicationBean {
    private final Application application;

    protected ApplicationBean(final Application application) {
        this.application = application;
    }

    @JsonProperty
    public String getKey() {
        return application.getKey().toString();
    }

    @JsonProperty
    public String getName() {
        return application.getName();
    }

    @JsonProperty
    public String getDescription() {
        return application.getDescription();
    }

    @JsonProperty
    public String getDefaultGroup() {
        return application.getDefaultGroup();
    }

    @JsonProperty
    public String getConfigurationURI() {
        return application.getConfigurationURI().map(Functions.toStringFunction()).getOrNull();
    }

    @JsonProperty
    public String getPostInstallURI() {
        return application.getPostInstallURI().map(Functions.toStringFunction()).getOrNull();
    }

    @JsonProperty
    public String getPostUpdateURI() {
        return application.getPostUpdateURI().map(Functions.toStringFunction()).getOrNull();
    }

    @JsonProperty
    public String getProductHelpServerSpaceURI() {
        return application.getProductHelpServerSpaceURI().map(Functions.toStringFunction()).getOrNull();
    }

    @JsonProperty
    public String getProductHelpCloudSpaceURI() {
        return application.getProductHelpCloudSpaceURI().map(Functions.toStringFunction()).getOrNull();
    }

    @JsonProperty
    public DateTime getBuildDate() {
        return application.buildDate();
    }

    @JsonProperty
    public SingleProductLicenseDetailsViewTO getLicense() {
        return application.getLicense().map(new Function<SingleProductLicenseDetailsView, SingleProductLicenseDetailsViewTO>() {
            @Override
            public SingleProductLicenseDetailsViewTO apply(final SingleProductLicenseDetailsView input) {
                return new SingleProductLicenseDetailsViewTO(input);
            }
        }).getOrNull();
    }

    @JsonProperty
    public int getMaxUserCount() {
        return application.getAccess().getMaximumUserCount().getOrElse(UNLIMITED_USER_COUNT);
    }

    @JsonProperty
    public int getCurrentUserCount() {
        return application.getAccess().getActiveUserCount();
    }

    @JsonProperty
    public String getAccessURI() {
        return application.getAccess().getManagementPage().toString();
    }

    @JsonProperty
    public String getVersion() {
        return application.getVersion();
    }

    @JsonProperty
    public abstract String getType();

    static ApplicationBean toBean(Application application) {
        if (application instanceof PlatformApplication) {
            return new PlatformApplicationBean((PlatformApplication) application);
        } else if (application instanceof PluginApplication) {
            return new PluginApplicationBean((PluginApplication) application);
        } else {
            throw new IllegalArgumentException("Don't know how to convert " + application.getClass() + " instances.");
        }
    }
}
