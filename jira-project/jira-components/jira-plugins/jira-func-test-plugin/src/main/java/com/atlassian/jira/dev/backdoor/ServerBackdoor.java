package com.atlassian.jira.dev.backdoor;

import com.atlassian.jira.util.collect.EnumerationIterator;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import org.apache.log4j.Appender;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.SimpleLayout;
import org.apache.log4j.WriterAppender;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.util.Enumeration;
import java.util.Optional;
import java.util.stream.Stream;

@AnonymousAllowed
@Path("server")
public class ServerBackdoor {

    private volatile BackdoorAppender appender;

    //There will never be an actual response to this one
    @Path("kill")
    @POST
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response kill() {
        //Loggers have buffers that would need flushing, System.err is unbuffered so we use that
        System.err.println("Server was killed via backdoor");

        Runtime.getRuntime().halt(66);

        return Response.ok().build();
    }

    @Path("dumpThreads")
    @POST
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response dumpThreads() {
        ThreadMXBean threading = ManagementFactory.getThreadMXBean();
        ThreadInfo[] threads = ManagementFactory.getThreadMXBean().dumpAllThreads(
                threading.isObjectMonitorUsageSupported(),
                threading.isSynchronizerUsageSupported());

        Stream.of(threads)
                .map(ThreadInfo::toString)
                .forEach(System.err::print);

        return Response.ok().build();
    }

    @POST
    @Path("enable-logging")
    public synchronized void enable() {
        if (appender == null) {
            appender = new BackdoorAppender(new StringWriter());
            Enumeration<Logger> allLoggers = (Enumeration<Logger>) LogManager.getCurrentLoggers();
            EnumerationIterator.fromEnumeration(allLoggers).forEachRemaining(logger -> addAppender(logger, appender));
        }
    }

    @POST
    @Path("disable-logging")
    public synchronized void disable(){
        Enumeration<Logger> allLoggers = (Enumeration<Logger>) LogManager.getCurrentLoggers();
        EnumerationIterator.fromEnumeration(allLoggers).forEachRemaining(logger -> logger.removeAppender(BackdoorAppender.NAME));
        appender = null;
    }

    @GET
    @Path("get-logs")
    public synchronized String get() {
        return Optional.ofNullable(appender).map(BackdoorAppender::getContents).orElse(null);
    }

    private static void addAppender(Logger logger, Appender appender) {
        if (logger.getAppender(BackdoorAppender.NAME) == null) {
            logger.addAppender(appender);
        }
    }

    private static class BackdoorAppender extends WriterAppender {

        private static final String NAME = BackdoorAppender.class.getName();

        private final Writer writer;

        BackdoorAppender(StringWriter writer) {
            super(new SimpleLayout(), writer);
            this.name = NAME;
            this.writer = writer;
        }

        String getContents() {
            return writer.toString();
        }
    }
}
