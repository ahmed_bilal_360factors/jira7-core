package com.atlassian.jira.dev.backdoor;

import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.statistics.ComponentStatisticsManager;
import com.atlassian.jira.issue.statistics.ProjectStatisticsManager;
import com.atlassian.jira.jql.parser.JqlParseException;
import com.atlassian.jira.jql.parser.JqlQueryParser;
import com.atlassian.jira.project.Project;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.query.Query;

import com.google.common.collect.Maps;

@Path("/statistics")
@AnonymousAllowed
public class StatisticsServiceBackdoor {

    private final JqlQueryParser jqlParser;
    private final ProjectStatisticsManager projectStatisticsManager;
    private final ComponentStatisticsManager componentStatisticsManager;

    public StatisticsServiceBackdoor(JqlQueryParser jqlParser) {
        this.projectStatisticsManager = ComponentAccessor.getComponent(ProjectStatisticsManager.class);
        this.componentStatisticsManager = ComponentAccessor.getComponent(ComponentStatisticsManager.class);
        this.jqlParser = jqlParser;
    }

    @GET
    @Produces (MediaType.APPLICATION_JSON)
    @Path ("/projects")
    public Response getProjectsResultingFrom(@QueryParam ("jql") String jqlQuery) {
        Optional<Query> query = Optional.ofNullable(jqlQuery).map(this::parseStringToQuery);
        Set<String> projects = projectStatisticsManager.getProjectsResultingFrom(query).stream()
                .map(Project::getKey)
                .collect(Collectors.toSet());
        return Response.ok(projects).build();
    }

    @GET
    @Produces (MediaType.APPLICATION_JSON)
    @Path ("/components")
    public Response getProjectWithComponentResultingFrom(@QueryParam ("jql") String jqlQuery) {
        Optional<Query> query = Optional.ofNullable(jqlQuery).map(this::parseStringToQuery);
        Map<Project, Map<ProjectComponent, Integer>> result = componentStatisticsManager.getProjectsWithComponentsWithIssueCount(query);
        Map<String, Map<String, Integer>> resultAsMap = toMap(result);
        return Response.ok(resultAsMap).build();
    }

    private Query parseStringToQuery(final String string) {
        try {
            return jqlParser.parseQuery(string);
        } catch (JqlParseException e) {
            throw new RuntimeException(e);
        }
    }

    private Map<String, Map<String, Integer>> toMap(Map<Project, Map<ProjectComponent, Integer>> param) {
        final Map<String, Map<String, Integer>> result = Maps.newHashMap();
        
        for (Map.Entry<Project, Map<ProjectComponent, Integer>> entry : param.entrySet()) {
            String projectKey = entry.getKey().getKey();

            Map<String, Integer> inner = entry.getValue().entrySet().stream()
                    .collect(Collectors.toMap(e -> e.getKey().getName(), Map.Entry::getValue));
            
            result.put(projectKey,  inner);
        }
        return result;
    }
}