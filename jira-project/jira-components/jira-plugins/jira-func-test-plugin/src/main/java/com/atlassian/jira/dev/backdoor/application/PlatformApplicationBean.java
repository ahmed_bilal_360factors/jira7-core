package com.atlassian.jira.dev.backdoor.application;

import com.atlassian.application.api.PlatformApplication;

/**
 * @since v7.0
 */
public class PlatformApplicationBean extends ApplicationBean {
    public PlatformApplicationBean(final PlatformApplication application) {
        super(application);
    }

    @Override
    public String getType() {
        return "platform";
    }
}
