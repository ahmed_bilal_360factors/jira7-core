package com.atlassian.jira.dev.backdoor.application;

import com.atlassian.application.api.PluginApplication;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

/**
 * @since v7.0
 */
public class PluginApplicationBean extends ApplicationBean {
    private final PluginApplication application;

    protected PluginApplicationBean(final PluginApplication application) {
        super(application);
        this.application = application;
    }

    @JsonProperty
    public String getDefinitionModuleKey() {
        return application.getDefinitionModuleKey();
    }

    @JsonProperty
    public List<ApplicationPluginBean> getPlugins() {
        return ImmutableList.copyOf(Iterables.transform(application.getPlugins(), ApplicationPluginBean.TO_BEAN));
    }

    @Override
    public String getType() {
        return "plugin";
    }
}
