package com.atlassian.jira.dev.backdoor;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * A JSON serialised description of how many users are active in a role.
 *
 * @since v6.4
 */
@SuppressWarnings({"UnusedDeclaration"})
@XmlRootElement
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class UsersInLicense {
    @JsonProperty
    private String applicationKey;

    @JsonProperty
    private Integer activeUsers;

    @JsonCreator
    public UsersInLicense(String applicationKey, int activeUsers) {
        this.applicationKey = applicationKey;
        this.activeUsers = activeUsers;
    }

    public Integer getActiveUsers() {
        return activeUsers;
    }

    public void setActiveUsers(final Integer activeUsers) {
        this.activeUsers = activeUsers;
    }

    public String getApplicationKey() {
        return applicationKey;
    }

    public void setApplicationKey(final String applicationKey) {
        this.applicationKey = applicationKey;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final UsersInLicense that = (UsersInLicense) o;

        if (activeUsers != null ? !activeUsers.equals(that.activeUsers) : that.activeUsers != null) {
            return false;
        }
        if (applicationKey != null ? !applicationKey.equals(that.applicationKey) : that.applicationKey != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = applicationKey != null ? applicationKey.hashCode() : 0;
        result = 31 * result + (activeUsers != null ? activeUsers.hashCode() : 0);
        return result;
    }
}
