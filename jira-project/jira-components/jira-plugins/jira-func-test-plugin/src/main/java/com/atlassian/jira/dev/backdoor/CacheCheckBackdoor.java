package com.atlassian.jira.dev.backdoor;

import com.atlassian.jira.cache.serialcheck.SerializationChecker;
import com.atlassian.jira.dev.backdoor.util.CacheControl;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.google.common.base.Throwables;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Backdoor for manipulating barriers remotely.
 *
 * @since v7.2.0
 */
@AnonymousAllowed
@Path("/cachecheck")
public class CacheCheckBackdoor {
    private final SerializationChecker checker;

    public CacheCheckBackdoor(SerializationChecker checker) {
        this.checker = checker;
    }

    @GET
    @Path("serialization")
    public Response checkSerialization() {

        Collection<? extends Exception> errors = checker.getErrors();
        checker.reset();

        if (errors.isEmpty()) {
            return Response.ok()
                    .cacheControl(CacheControl.never()).build();
        }

        String errorString = errors.stream()
                .map(Throwables::getStackTraceAsString)
                .collect(Collectors.joining("\n\n"));

        return Response.status(Response.Status.PRECONDITION_FAILED)
                .entity(errorString)
                .cacheControl(CacheControl.never())
                .build();

    }
}
