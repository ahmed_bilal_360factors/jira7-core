package com.atlassian.jira.dev.events;

import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.webresource.api.assembler.PageBuilderService;

/**
 * @since v7.0
 */
public class ViewEventAction extends JiraWebActionSupport {
    private final PageBuilderService pageBuilder;

    public ViewEventAction(final PageBuilderService pageBuilder) {
        this.pageBuilder = pageBuilder;
    }

    public String doExecute() {
        pageBuilder.assembler()
                .resources()
                .requireWebResource("com.atlassian.jira.dev.func-test-plugin:events-resource");
        return SUCCESS;
    }
}
