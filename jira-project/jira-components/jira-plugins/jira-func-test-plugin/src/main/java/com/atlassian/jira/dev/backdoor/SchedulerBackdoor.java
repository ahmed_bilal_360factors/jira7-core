package com.atlassian.jira.dev.backdoor;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.scheduler.SchedulerHistoryService;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.config.CronScheduleInfo;
import com.atlassian.scheduler.config.IntervalScheduleInfo;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.Schedule;
import com.atlassian.scheduler.status.JobDetails;
import com.atlassian.scheduler.status.RunDetails;
import com.google.common.collect.Lists;

import javax.annotation.Nullable;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Backdoor for querying the {@link com.atlassian.scheduler.SchedulerService}.
 *
 * @since v7.0.0
 */
@Path("scheduler")
@AnonymousAllowed
@Consumes({"application/json"})
@Produces({"application/json"})
public class SchedulerBackdoor {
    private final SchedulerService schedulerService;
    private final SchedulerHistoryService schedulerHistoryService;

    public SchedulerBackdoor(SchedulerService schedulerService, SchedulerHistoryService schedulerHistoryService) {
        this.schedulerService = schedulerService;
        this.schedulerHistoryService = schedulerHistoryService;
    }

    @GET
    @Path("jobRunnerKeys")
    public List<String> getJobRunnerKeys() {
        final Set<JobRunnerKey> jobRunnerKeys = schedulerService.getJobRunnerKeysForAllScheduledJobs();
        return jobRunnerKeys.stream()
                .map(JobRunnerKey::toString)
                .sorted()
                .collect(Collectors.toList());
    }

    @GET
    @Nullable
    @Path("job/{jobId}")
    public JobBean getJob(@PathParam("jobId") String jobId) {
        final JobDetails jobDetails = schedulerService.getJobDetails(JobId.of(jobId));
        return (jobDetails != null) ? new JobBean(jobDetails) : null;
    }

    @GET
    @Nullable
    @Path("job/{jobId}/last")
    public JobRunBean getLastJobRun(@PathParam("jobId") String jobId) {
        final RunDetails runDetails = schedulerHistoryService.getLastRunForJob(JobId.of(jobId));
        return (runDetails != null) ? new JobRunBean(runDetails) : null;
    }

    @GET
    @Nullable
    @Path("job/{jobId}/lastSuccessful")
    public JobRunBean getLastSuccessfulJobRun(@PathParam("jobId") String jobId) {
        final RunDetails runDetails = schedulerHistoryService.getLastSuccessfulRunForJob(JobId.of(jobId));
        return (runDetails != null) ? new JobRunBean(runDetails) : null;
    }

    @GET
    @Path("jobs/{jobRunnerKey}")
    public List<JobBean> getJobsByJobRunnerKey(@PathParam("jobRunnerKey") String jobRunnerKey) {
        final List<JobDetails> jobs = schedulerService.getJobsByJobRunnerKey(JobRunnerKey.of(jobRunnerKey));
        return jobs.stream()
                .map(JobBean::new)
                .collect(Collectors.toList());
    }

    @GET
    @Path("jobs")
    public List<JobBean> getAllJobs() {
        final Set<JobRunnerKey> jobRunnerKeys = schedulerService.getJobRunnerKeysForAllScheduledJobs();
        return jobRunnerKeys.stream()
                .map(schedulerService::getJobsByJobRunnerKey)
                .map(jobs -> Lists.transform(jobs, JobBean::new))
                .reduce(new ArrayList<>(), (list, beans) -> {
                    list.addAll(beans);
                    return list;
                });
    }

    @Nullable
    private static Long dateToLong(Date date) {
        return (date != null) ? date.getTime() : null;
    }

    public static class JobRunBean {
        public String message;
        public Date startTime;
        public long durationInMillis;
        public String runOutcome;

        public JobRunBean(final RunDetails runDetails) {
            message = runDetails.getMessage();
            startTime = runDetails.getStartTime();
            durationInMillis = runDetails.getDurationInMillis();
            runOutcome = runDetails.getRunOutcome().toString();
        }
    }

    public static class JobBean {
        public String jobId;
        public String jobRunnerKey;
        public Long nextRunTime;
        public String runMode;
        public String scheduleType;
        public Long intervalInMillis;
        public Long firstRunTime;
        public String cronExpression;
        public String timeZoneId;
        public boolean runnable;

        JobBean(JobDetails jobDetails) {
            jobId = jobDetails.getJobId().toString();
            jobRunnerKey = jobDetails.getJobRunnerKey().toString();
            nextRunTime = dateToLong(jobDetails.getNextRunTime());
            runMode = jobDetails.getRunMode().toString();
            runnable = jobDetails.isRunnable();

            final Schedule schedule = jobDetails.getSchedule();
            scheduleType = schedule.getType().toString();
            switch (schedule.getType()) {
                case INTERVAL:
                    final IntervalScheduleInfo intervalInfo = schedule.getIntervalScheduleInfo();
                    intervalInMillis = intervalInfo.getIntervalInMillis();
                    firstRunTime = dateToLong(intervalInfo.getFirstRunTime());
                    break;

                case CRON_EXPRESSION:
                    final CronScheduleInfo cronInfo = schedule.getCronScheduleInfo();
                    cronExpression = cronInfo.getCronExpression();
                    timeZoneId = (cronInfo.getTimeZone() != null) ? cronInfo.getTimeZone().getID() : null;
                    break;
            }
        }
    }

}
