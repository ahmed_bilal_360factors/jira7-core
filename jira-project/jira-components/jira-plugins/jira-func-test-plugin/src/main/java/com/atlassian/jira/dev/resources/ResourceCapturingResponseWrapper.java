package com.atlassian.jira.dev.resources;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

/**
 * Response wrapper used to capture underlying stream and modify final response.
 *
 * @since 7.1
 */
public class ResourceCapturingResponseWrapper extends HttpServletResponseWrapper {
    private final ByteArrayOutputStream outputStream;
    private ServletOutputStream servletOutputStream;
    private PrintWriter writer;

    public ResourceCapturingResponseWrapper(final HttpServletResponse httpServletResponse) throws IOException {
        super(httpServletResponse);

        this.outputStream = new ByteArrayOutputStream(httpServletResponse.getBufferSize());
    }

    public byte[] getBytes() {
        return outputStream.toByteArray();
    }

    public String getString() throws IOException {
        return new String(getBytes(), getCharacterEncoding());
    }

    @Override
    public ServletOutputStream getOutputStream() throws IOException {
        if (servletOutputStream == null) {
            servletOutputStream = new CapturingServletOutputStream();
        }

        return servletOutputStream;
    }

    @Override
    public PrintWriter getWriter() throws IOException {
        if (writer == null) {
            writer = new PrintWriter(new OutputStreamWriter(outputStream, getCharacterEncoding()));
        }

        return writer;
    }

    private class CapturingServletOutputStream extends ServletOutputStream {
        @Override
        public void write(final int b) throws IOException {
            outputStream.write(b);
        }

        @Override
        public void write(final byte[] b) throws IOException {
            outputStream.write(b);
        }

        @Override
        public void write(final byte[] b, final int off, final int len) throws IOException {
            outputStream.write(b, off, len);
        }
    }
}