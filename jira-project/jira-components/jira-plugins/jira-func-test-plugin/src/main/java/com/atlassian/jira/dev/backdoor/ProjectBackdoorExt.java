package com.atlassian.jira.dev.backdoor;

import com.atlassian.jira.bc.project.ProjectCreationData;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectCategory;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @since v6.1
 */
@AnonymousAllowed
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
@Path("project/{projectKey}")
public class ProjectBackdoorExt {
    private final ProjectManager projectManager;
    private final ProjectService projectService;
    private final UserManager userManager;
    private final JiraAuthenticationContext authenticationContext;

    public ProjectBackdoorExt(
            ProjectManager projectManager,
            ProjectService projectService,
            UserManager userManager,
            JiraAuthenticationContext authenticationContext) {
        this.projectManager = projectManager;
        this.projectService = projectService;
        this.userManager = userManager;
        this.authenticationContext = authenticationContext;
    }

    @GET
    @Path("id")
    public Response getProjectId(@PathParam("projectKey") String key) {
        Project project = projectManager.getProjectObjByKey(key);
        if (project == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(project.getId()).build();
        }
    }

    @GET
    @Path("category/name")
    public Response getProjectCategoryName(@PathParam("projectKey") String key) {
        Project project = projectManager.getProjectObjByKey(key);
        if (project == null) {
            return Response.status(Response.Status.NOT_FOUND).entity("project with specified key cannot be found").build();
        }
        ProjectCategory category = project.getProjectCategoryObject();
        if (category == null) {
            return Response.status(Response.Status.NOT_FOUND).entity("project specified does not have a category").build();
        }
        return Response.ok(category.getName()).build();
    }

    @POST
    public Response createProjectBasedOnExistingProject(@PathParam("projectKey") String key, ProjectInputBean input) {
        ProjectCreationData data = new ProjectCreationData.Builder()
                .withKey(input.getKey())
                .withName(input.getName())
                .withLead(userManager.getUserByName(input.getLead()))
                .build();
        Project existingProject = projectManager.getProjectByCurrentKey(key);

        ProjectService.CreateProjectValidationResult result = projectService.validateCreateProjectBasedOnExistingProject(
                authenticationContext.getLoggedInUser(), existingProject.getId(), data);
        if (result.isValid()) {
            final Project project = projectService.createProject(result);
            return Response.ok(project.getKey()).build();
        }
        return Response.serverError().build();
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class ProjectInputBean {
        @JsonProperty
        private String key;
        @JsonProperty
        private String name;
        @JsonProperty
        private String lead;

        public ProjectInputBean(
                final String key,
                final String name,
                final String lead) {
            this.key = key;
            this.name = name;
            this.lead = lead;
        }

        public ProjectInputBean() {
        }

        public String getKey() {
            return key;
        }

        public String getName() {
            return name;
        }

        public String getLead() {
            return lead;
        }
    }
}
