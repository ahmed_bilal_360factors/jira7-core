package com.atlassian.jira.dev.backdoor;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventListenerRegistrar;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.mail.event.SendMessageEvent;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.google.common.collect.ImmutableList;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.atlassian.jira.dev.backdoor.util.CacheControl.never;

@AnonymousAllowed
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@Path("/outgoingmail")
public class OutgoingMailBackdoor {
    private static final Logger log = Logger.getLogger(OutgoingMailBackdoor.class);
    // While email encoding is configurable in JIRA, for test purposes we are fine with this assumption
    private static final String DEFAULT_ENCODING = "UTF-8";
    private static final Pattern CID_PATTERN = Pattern.compile("src=[\"']cid:(.*?)[\"']");

    private final JiraBaseUrls jiraBaseUrls;
    private final EventListenerRegistrar eventListenerRegistrar;

    private Collection<MimeMessage> messages;
    private AtomicBoolean enabled = new AtomicBoolean(false);

    public OutgoingMailBackdoor(final JiraBaseUrls jiraBaseUrls, final EventListenerRegistrar eventListenerRegistrar) {
        this.jiraBaseUrls = jiraBaseUrls;
        this.eventListenerRegistrar = eventListenerRegistrar;
        this.messages = new ConcurrentLinkedQueue<>();
    }

    @PostConstruct
    public void init() throws Exception {
        eventListenerRegistrar.register(this);
    }

    @PreDestroy
    public void cleanUp() throws Exception {
        eventListenerRegistrar.unregister(this);
    }

    @EventListener
    public void sendMessage(SendMessageEvent event) {
        if (enabled.get()) {
            this.messages.add(event.getMessage());
        }
    }

    @POST
    @Path("enable")
    public Response enable() {
        enabled.set(true);
        return Response.ok().cacheControl(never()).build();
    }

    @POST
    @Path("disable")
    public Response disable() {
        enabled.set(false);
        return Response.ok().cacheControl(never()).build();
    }

    @POST
    @Path("clear-messages")
    public Response clearMessages() {
        messages.clear();
        return Response.ok().cacheControl(never()).build();
    }

    @GET
    @Path("mails")
    public Response getMails() {
        final ImmutableList<String> rawMessages = messages.stream()
                .map(OutgoingMailBackdoor::messageToString)
                .collect(CollectorsUtil.toImmutableList());
        return Response.ok(rawMessages).cacheControl(never()).build();
    }

    @GET
    @Path("preview")
    public Response previewMessage(@QueryParam("messageId") String messageId) {

        final MimeMessage mimeMessage = getMimeMessage(messageId);

        if (mimeMessage != null) {
            try {
                return Response.ok(getBody(mimeMessage), MediaType.TEXT_HTML_TYPE).cacheControl(never()).build();
            } catch (Exception e) {
                log.error("Error reading message body", e);
                return Response.serverError().entity("Error reading message body").build();
            }
        } else {
            return Response.status(Response.Status.NOT_FOUND).cacheControl(never()).build();
        }
    }

    @GET
    @Path("part")
    public Response previewMessage(@QueryParam("messageId") String messageId, @QueryParam("cid") String CID) {

        final MimeMessage mimeMessage = getMimeMessage(messageId);

        if (mimeMessage != null) {
            try {
                final BodyPart part = getPart(mimeMessage, CID);
                if (part != null) {
                    return Response.ok(IOUtils.toByteArray(part.getInputStream()), part.getContentType()).cacheControl(never()).build();
                } else {
                    return Response.status(Response.Status.NOT_FOUND).cacheControl(never()).build();
                }
            } catch (Exception e) {
                log.error("Error reading part body", e);
                return Response.serverError().entity("Error reading part body").build();
            }
        } else {
            return Response.status(Response.Status.NOT_FOUND).cacheControl(never()).build();
        }
    }

    private MimeMessage getMimeMessage(String messageId) {
        return messages.stream()
                .filter(message -> matchMimeMessage(messageId, message))
                .findFirst().orElse(null);
    }

    private String getBody(MimeMessage message) throws IOException, MessagingException {
        if (message.getContent() instanceof MimeMultipart) {
            MimeMultipart content = (MimeMultipart) message.getContent();
            // Message body is always the first as it is inserted there by MessageCreator in atlassian-mail
            final String body = content.getBodyPart(0).getContent().toString();
            return prepareMessageBody(message.getMessageID(), body);
        }

        return getBody(message);
    }

    private String prepareMessageBody(final String messageID, final String body) throws MessagingException {
        // this hackery is required, because Neko parser in HTTPUnit does not like xhtml doctype that we use in emails
        final String preparedBody = StringUtils
                .replacePattern(body, "<!DOCTYPE ([^>]+)>", "<!DOCTYPE html>")
                .replace("<html xmlns=\"http://www.w3.org/1999/xhtml\">", "<html lang=\"en\">")
                .replaceFirst("<base href=\"([^\"]+)\">", "")
                .replace("</head>", "<script type=\"text/javascript\" src=\"" + jiraBaseUrls.baseUrl() + "/download/resources/com.atlassian.plugins.jquery:jquery/jquery.js\"></script></head>");

        // replace part references with URIs
        Matcher matcher = CID_PATTERN.matcher(preparedBody);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            String url = String.format("src=\"part?messageId=%s&cid=<%s>\"", messageID, matcher.group(1));
            matcher.appendReplacement(sb, url);
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    private static BodyPart getPart(MimeMessage message, String CID) throws IOException, MessagingException {
        if (message.getContent() instanceof MimeMultipart) {
            MimeMultipart content = (MimeMultipart) message.getContent();
            return content.getBodyPart(CID);
        } else {
            return null;
        }
    }

    private static String messageToString(MimeMessage message) {
        final ByteArrayOutputStream os = new ByteArrayOutputStream();
        try {
            message.writeTo(os);
            return new String(os.toByteArray(), DEFAULT_ENCODING);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    private static boolean matchMimeMessage(final String messageId, final MimeMessage message) {
        try {
            return message.getMessageID().equals(messageId);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    private static String getBody(Part msg) {
        String all = getWholeMessage(msg);
        int i = all.indexOf("\r\n\r\n");
        return all.substring(i + 4, all.length());
    }

    private static String getWholeMessage(Part msg) {
        try {
            ByteArrayOutputStream bodyOut = new ByteArrayOutputStream();
            msg.writeTo(bodyOut);
            return bodyOut.toString("US-ASCII").trim();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
