package com.atlassian.jira.dev.backdoor.util;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.osgi.factory.OsgiPlugin;
import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;

import java.util.Optional;

/**
 * Util class for AO
 */
public class AoUtil {

    public static String getAoCompatiblePrefix(String pluginKey, PluginAccessor pluginAccessor) {
        Plugin plugin = Optional.ofNullable(pluginAccessor.getPlugin(pluginKey))
                .orElseThrow(() -> new IllegalStateException("Plugin with key [" + pluginKey + "] not found"));

        if (!(plugin instanceof OsgiPlugin)) {
            throw new IllegalArgumentException("Plugin " + plugin + " must be an OsgiPlugin");
        }

        String bundleSymbolicName = ((OsgiPlugin) plugin).getBundle().getSymbolicName();
        String hash = Hashing.md5().hashString(bundleSymbolicName, Charsets.UTF_8).toString();
        return "AO_" + hash.substring(hash.length() - 6, hash.length()).toUpperCase();
    }
}
