package com.atlassian.jira.dev.backdoor;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.InstanceFeatureManager;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@AnonymousAllowed
@Path("/instanceFeatures")
public class InstanceFeaturesBackdoor {
    private final InstanceFeatureManager featureManager;
    private final EventPublisher eventPublisher;
    private final JiraProperties jiraProperties;

    public InstanceFeaturesBackdoor(InstanceFeatureManager featureManager, EventPublisher eventPublisher,
                                    JiraProperties jiraProperties) {
        this.featureManager = featureManager;
        this.eventPublisher = eventPublisher;
        this.jiraProperties = jiraProperties;
    }

    @GET
    @Path("/{featureKey}")
    @Produces({MediaType.TEXT_PLAIN})
    public Response getInstanceFeature(@PathParam("featureKey") String featureKey) {
        return Response.ok(Boolean.toString(featureManager.isInstanceFeatureEnabled(featureKey))).build();
    }

    @GET
    @Path("/{featureKey}/set")
    @Produces({MediaType.TEXT_PLAIN})
    public Response enableInstanceFeature(@PathParam("featureKey") String featureKey) {
        jiraProperties.setProperty(FeatureManager.SYSTEM_PROPERTY_PREFIX + featureKey, "true");
        eventPublisher.publish(ClearCacheEvent.INSTANCE);
        return Response.ok(Boolean.toString(featureManager.isInstanceFeatureEnabled(featureKey))).build();
    }

    @GET
    @Path("/{featureKey}/unset")
    @Produces({MediaType.TEXT_PLAIN})
    public Response clearInstanceFeature(@PathParam("featureKey") String featureKey) {
        jiraProperties.unsetProperty(FeatureManager.SYSTEM_PROPERTY_PREFIX + featureKey);
        eventPublisher.publish(ClearCacheEvent.INSTANCE);
        return Response.ok(Boolean.toString(featureManager.isInstanceFeatureEnabled(featureKey))).build();
    }

}
