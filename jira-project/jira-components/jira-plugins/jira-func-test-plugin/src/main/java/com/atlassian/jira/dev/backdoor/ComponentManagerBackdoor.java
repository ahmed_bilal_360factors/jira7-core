package com.atlassian.jira.dev.backdoor;

import com.atlassian.jira.ComponentManager;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.atlassian.jira.dev.backdoor.util.CacheControl.never;

/**
 * Backdoor to access component manager state
 *
 * @since v7.2
 */
@AnonymousAllowed
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@Path("/componentManager")
public class ComponentManagerBackdoor {

    @GET
    @Path("state")
    public Response getState() {
        return Response.ok(ComponentManagerState.from(ComponentManager.getInstance())).cacheControl(never()).build();
    }

    @JsonAutoDetect
    private static class ComponentManagerState {
        @JsonProperty
        private final ComponentManager.State state;

        @JsonProperty
        private final ComponentManager.PluginSystemState pluginSystemState;

        private ComponentManagerState(ComponentManager.State state, ComponentManager.PluginSystemState pluginSystemState) {
            this.state = state;
            this.pluginSystemState = pluginSystemState;
        }

        private static ComponentManagerState from(ComponentManager componentManager) {
            return new ComponentManagerState(componentManager.getState(), componentManager.getPluginSystemState());
        }
    }
}
