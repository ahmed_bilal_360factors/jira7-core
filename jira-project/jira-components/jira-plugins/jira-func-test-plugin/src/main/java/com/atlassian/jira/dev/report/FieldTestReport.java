package com.atlassian.jira.dev.report;

import com.atlassian.gzipfilter.org.apache.commons.lang.StringUtils;
import com.atlassian.jira.plugin.report.impl.AbstractReport;
import com.atlassian.jira.web.action.ProjectActionSupport;

import java.util.HashMap;
import java.util.Map;

public class FieldTestReport extends AbstractReport {

    public String generateReportHtml(ProjectActionSupport action, Map reqParams) throws Exception {
        final Map<String, Object> params = new HashMap<>();
        params.put("report", this);
        params.put("action", action);

        params.put("aString", reqParams.get("aString"));
        params.put("aLong", reqParams.get("aLong"));
        params.put("aSelect", reqParams.get("aSelect"));
        params.put("aHidden", reqParams.get("aHidden"));
        params.put("aDate", reqParams.get("aDate"));
        params.put("aUser", reqParams.get("aUser"));
        params.put("aGroup", reqParams.get("aGroup"));
        params.put("aText", reqParams.get("aText"));
        params.put("aMultiSelect", reqParams.get("aMultiSelect"));
        params.put("aCheckbox", reqParams.get("aCheckbox"));
        params.put("aCascade", reqParams.get("aCascade"));
        params.put("aFilter", reqParams.get("aFilter"));
        params.put("aFilterOrProject", reqParams.get("aFilterOrProject"));

        // Retrieve the portlet parameters
        return descriptor.getHtml("view", params);
    }

    @Override
    public void validate(ProjectActionSupport action, Map params) {
        super.validate(action, params);

        //Special case for testing validation error messages for fields
        //Make on validation error message for each field
        if ("fail".equals(params.get("aString"))) {
            action.addError("aString", "This string field has an error");
            action.addError("aLong", "This long field has an error");
            action.addError("aSelect", "This select field has an error");
            action.addError("aHidden", "This hidden field has an error");
            action.addError("aDate", "This date field has an error");
            action.addError("aUser", "This user field has an error");
            action.addError("aGroup", "This group field has an error");
            action.addError("aText", "This text field has an error");
            action.addError("aMultiSelect", "This multi select field has an error");
            action.addError("aCheckbox", "This checkbox field has an error");
            action.addError("aCascade", "This cascade field has an error");
            action.addError("aFilter", "This filter field has an error");
            action.addError("aFilterOrProject", "This filter/project field has an error");
            return;
        }
    }
}
