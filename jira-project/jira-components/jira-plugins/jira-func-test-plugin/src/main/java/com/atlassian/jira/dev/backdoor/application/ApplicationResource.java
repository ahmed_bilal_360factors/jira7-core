package com.atlassian.jira.dev.backdoor.application;

import com.atlassian.application.api.Application;
import com.atlassian.application.api.ApplicationAccess;
import com.atlassian.application.api.ApplicationKey;
import com.atlassian.application.api.ApplicationManager;
import com.atlassian.fugue.Option;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.sal.api.user.UserKey;
import com.google.common.base.Function;
import com.google.common.base.Supplier;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.atlassian.jira.dev.backdoor.util.CacheControl.never;
import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Iterables.transform;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;

@Path("application")
@AnonymousAllowed
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
/**
 * This resource deals with installed applications, not licensed applications. It's possible to have an application
 * installed but no license for that application, and it's also possible to have a license installed for an application
 * but not have that application installed. To deal with licensed applications use the production REST resources (for
 * example ApplicationRoleResource).
 */
public class ApplicationResource {
    private static final Supplier<Response> NOT_FOUND_REPONSE = () -> Response.status(NOT_FOUND).cacheControl(never()).build();
    private static final Function<Object, Response> OK_RESPONSE = input -> Response.ok(input).cacheControl(never()).build();

    private final ApplicationManager applicationManager;

    public ApplicationResource(final ApplicationManager applicationManager) {
        this.applicationManager = applicationManager;
    }

    @GET
    public Response getAll() {
        return Response
                .ok(copyOf(transform(applicationManager.getApplications(), input -> ApplicationBean.toBean(input))))
                .cacheControl(never())
                .build();
    }

    @GET
    @Path("{key}")
    public Response getKey(@PathParam("key") final String key) {
        return applicationManager
                .getApplication(ApplicationKey.valueOf(key))
                .map(input -> ApplicationBean.toBean(input))
                .map(OK_RESPONSE)
                .getOrElse(NOT_FOUND_REPONSE);
    }

    @GET
    @Path("{key}/count")
    public Response getCount(@PathParam("key") final String key, @QueryParam("count") final Integer count) {
        return applicationManager
                .getApplication(ApplicationKey.valueOf(key))
                .map(new Function<Application, String>() {
                    @Override
                    public String apply(final Application input) {
                        return input.getUserCountDescription(Option.option(count));
                    }
                })
                .map(OK_RESPONSE)
                .getOrElse(NOT_FOUND_REPONSE);
    }

    @GET
    @Path("{key}/access")
    public Response getAccess(@PathParam("key") final String key, final @QueryParam("userKey") String userKey) {
        return applicationManager
                .getAccess(ApplicationKey.valueOf(key))
                .map(new Function<ApplicationAccess, String>() {
                    @Override
                    public String apply(final ApplicationAccess input) {
                        final Option<ApplicationAccess.AccessError> option =
                                input.getAccessError(userKey == null ? null : new UserKey(userKey));
                        return option.isEmpty() ? "OK" : option.get().name();
                    }
                })
                .map(OK_RESPONSE)
                .getOrElse(NOT_FOUND_REPONSE);
    }

    @PUT
    @Path("{key}/clearConfiguration")
    public Response clearConfiguration(@PathParam("key") final String key) {
        return applicationManager
                .getApplication(ApplicationKey.valueOf(key))
                .map(application -> {
                    application.clearConfiguration();
                    return null;
                })
                .map(OK_RESPONSE)
                .getOrElse(NOT_FOUND_REPONSE);
    }
}
