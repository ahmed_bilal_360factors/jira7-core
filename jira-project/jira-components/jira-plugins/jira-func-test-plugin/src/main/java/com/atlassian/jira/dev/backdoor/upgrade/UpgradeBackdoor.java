package com.atlassian.jira.dev.backdoor.upgrade;

import com.atlassian.jira.dev.backdoor.util.AoUtil;
import com.atlassian.jira.upgrade.LegacyUpgradeTaskBuildNumberStorageImpl;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.upgrade.api.UpgradeTaskService;
import com.atlassian.jira.component.ComponentAccessor;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * This backdoor is used to manipulate the upgrades for the system for testing. It is not available in production.
 */
@Path("upgrade")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class UpgradeBackdoor {

    private final UpgradeTaskService upgradeTaskService;
    private final LegacyUpgradeTaskBuildNumberStorageImpl legacyUpgradeTaskBuildNumberStorage;
    private final PluginAccessor pluginAccessor;

    public UpgradeBackdoor(UpgradeTaskService upgradeTaskService) {
        this.pluginAccessor = ComponentAccessor.getPluginAccessor();
        this.upgradeTaskService = upgradeTaskService;
        this.legacyUpgradeTaskBuildNumberStorage = ComponentAccessor.getComponent(LegacyUpgradeTaskBuildNumberStorageImpl.class);
    }

    /**
     * Get the host upgrade details. Currently only returns the host's upgrade build number.
     *
     * @return the host upgrade details.
     */
    @GET
    @Path("hostDetails")
    public Response getHostDetails() {
        int hostBuildNumber = upgradeTaskService.getDatabaseBuildNumberForHost();

        HostUpgradeDetailsJsonBean hostUpgradeDetailsJsonBean = new HostUpgradeDetailsJsonBean();
        hostUpgradeDetailsJsonBean.setBuildNumber(hostBuildNumber);

        return Response.ok(hostUpgradeDetailsJsonBean).build();
    }

    /**
     * Get the plugin upgrade details. Currently only returns the plugin's upgrade build number.
     *
     * @return the plugin upgrade details.
     */
    @GET
    @Path("pluginDetails")
    public Response getPluginDetails(@QueryParam("pluginKey") final String pluginKey) {
        int pluginBuildNumber = upgradeTaskService.getDatabaseBuildNumberForPlugin(pluginKey);
        int salBuildNumber = legacyUpgradeTaskBuildNumberStorage.getSalBuildNumber(pluginKey);
        int aoBuildNumber = legacyUpgradeTaskBuildNumberStorage.getAoBuildNumber(AoUtil.getAoCompatiblePrefix(pluginKey, pluginAccessor));

        PluginUpgradeDetailsJsonBean pluginUpgradeDetailsJsonBean = new PluginUpgradeDetailsJsonBean(pluginBuildNumber, salBuildNumber, aoBuildNumber);
        return Response.ok(pluginUpgradeDetailsJsonBean).build();
    }
}