package com.atlassian.jira.dev.resources;

import cz.vutbr.web.css.CSSException;
import cz.vutbr.web.css.CSSFactory;
import cz.vutbr.web.css.CombinedSelector;
import cz.vutbr.web.css.RuleBlock;
import cz.vutbr.web.css.RuleFactory;
import cz.vutbr.web.css.Selector;
import cz.vutbr.web.css.StyleSheet;
import cz.vutbr.web.csskit.RuleFactoryImpl;
import cz.vutbr.web.csskit.RuleSetImpl;

import java.io.IOException;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Utility class, which can instrument provided stylesheet, instrumentation format is compatible with CriticalCS javascript module.
 *
 * @since 7.1
 */
public class InstrumentStylesheet extends AbstractInstrumentResource {
    private final RuleFactory rf;

    public InstrumentStylesheet() {
        this.rf = RuleFactoryImpl.getInstance();
    }

    @Override
    public String instrumentResource(final String source, final String resourceName) throws IOException {
        try {
            final StyleSheet styleSheet = CSSFactory.parse(source);

            return StreamSupport.stream(styleSheet.spliterator(), false)
                    .map(rule -> instrumentRule(resourceName, rule))
                    .collect(Collectors.joining());
        } catch (CSSException e) {
            return source;
        }
    }

    private String instrumentRule(final String resourceName, final RuleBlock<?> rule) {
        // we can't instrument other types of rules, yet.
        // this method will fail on @font-face, but luckily there is only one of such.
        if (!(rule instanceof RuleSetImpl)) {
            return rule.toString();
        }

        final Selector attrSelector = (Selector) rf.createSelector().unlock();
        attrSelector.add(rf.createAttribute(resourceName, true, Selector.Operator.EQUALS, "key"));

        final Selector nameSelector = (Selector) rf.createSelector().unlock();
        nameSelector.add(rf.createElement("instrumented-resource"));

        final CombinedSelector combinedSelector = (CombinedSelector) rf.createCombinedSelector().unlock();
        combinedSelector.add(nameSelector);
        combinedSelector.add(attrSelector);

        ((RuleSetImpl) rule).getSelectors().add(combinedSelector);

        return rule.toString();
    }
}
