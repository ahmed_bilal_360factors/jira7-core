package com.atlassian.jira.dev.resources;

import com.atlassian.fugue.Option;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;
import java.util.regex.Matcher;

/**
 * Abstract class for resource instrumentation filters.
 *
 * @since 7.1
 */
public abstract class AbstractInstrumentResourceFilter implements Filter {
    protected JiraProperties jiraProperties;
    protected PluginAccessor pluginAccessor;

    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
        this.jiraProperties = ComponentAccessor.getComponent(JiraProperties.class);
        this.pluginAccessor = ComponentAccessor.getPluginAccessor();
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {
        if (!instrumentEnabled()) {
            filterChain.doFilter(request, response);
            return;
        }


        final HttpServletResponse httpResponse = (HttpServletResponse) response;
        final ResourceCapturingResponseWrapper responseWrapper = new ResourceCapturingResponseWrapper(httpResponse);
        filterChain.doFilter(request, responseWrapper);

        final String requestUrl = ((HttpServletRequest) request).getRequestURL().toString();
        final String content = responseWrapper.getString();

        if (acceptResource(response)) {
            // do not cache those resources, because we want them to be reloaded so that it does not break succeeding tests.
            httpResponse.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // http 1.1
            httpResponse.setHeader("Pragma", "no-cache"); // http 1.0
            httpResponse.setDateHeader("Expires", 0); // prevent proxy caching

            response.getWriter().write(processResource(requestUrl, content));
        } else {
            response.getWriter().write(content);
        }
    }

    protected String processResource(final String requestUrl, final String content) throws IOException {
        final Matcher resourceMatcher = matchResource(requestUrl);
        if (resourceMatcher.matches()) {
            final String resourceName = resourceMatcher.group(1);
            final String resourceLocation = getResourceLocation(resourceName);

            return getInstrumentResource().instrumentResource(content, resourceLocation);
        } else if (matchBatch(requestUrl).matches()) {
            return getInstrumentResource().instrumentBatch(content, requestUrl);
        } else {
            return content;
        }
    }

    /**
     * Map given resource name that includes module key and resource path to complete key with module name and resource location.
     *
     * @param resourceName
     * @return complete key with module name nad resource location.
     */
    protected String getResourceLocation(final String resourceName) {
        final String moduleName = resourceName.split("/")[0];
        final String name = resourceName.substring(resourceName.indexOf('/') + 1);

        return getDescriptor(moduleName)
                .map(descriptor -> Optional.ofNullable(descriptor.getResourceDescriptor("download", name))
                                .map(ResourceDescriptor::getLocation)
                                .map(location -> getInstrumentResource().getResourceName(moduleName, location))
                                .orElse(resourceName)
                ).getOrElse(resourceName);
    }

    private Option<WebResourceModuleDescriptor> getDescriptor(String moduleCompleteKey) {
        ModuleDescriptor<?> module = pluginAccessor.getEnabledPluginModule(moduleCompleteKey);
        return module instanceof WebResourceModuleDescriptor ?
                Option.some((WebResourceModuleDescriptor) module) :
                Option.none();
    }

    /**
     * Returns true when given response contains resource that should be instrumented by filter implementation.
     *
     * @param response
     * @return true when given response contains resource that should be instrumented by filter implementation.
     */
    protected abstract boolean acceptResource(final ServletResponse response);

    protected abstract Matcher matchResource(final String requestUrl);

    protected abstract Matcher matchBatch(final String requestUrl);

    protected abstract boolean instrumentEnabled();

    protected abstract AbstractInstrumentResource getInstrumentResource();

    @Override
    public void destroy() {

    }
}
