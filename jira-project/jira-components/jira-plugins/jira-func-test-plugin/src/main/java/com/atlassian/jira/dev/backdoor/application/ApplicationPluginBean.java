package com.atlassian.jira.dev.backdoor.application;

import com.atlassian.application.api.ApplicationPlugin;
import com.google.common.base.Function;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @since v7.0
 */
public class ApplicationPluginBean {
    static final Function<ApplicationPlugin, ApplicationPluginBean> TO_BEAN = new Function<ApplicationPlugin, ApplicationPluginBean>() {
        @Override
        public ApplicationPluginBean apply(final ApplicationPlugin input) {
            return new ApplicationPluginBean(input);
        }
    };

    private final ApplicationPlugin plugin;

    ApplicationPluginBean(final ApplicationPlugin plugin) {
        this.plugin = plugin;
    }

    @JsonProperty
    public String getKey() {
        return plugin.getPluginKey();
    }

    @JsonProperty
    public String getType() {
        return plugin.getType().name();
    }
}
