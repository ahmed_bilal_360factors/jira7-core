package com.atlassian.jira.dev.rest;

import com.atlassian.plugin.web.api.DynamicWebInterfaceManager;
import com.atlassian.plugin.web.descriptors.WebPanelModuleDescriptor;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

import static java.util.Collections.emptyMap;

@AnonymousAllowed
@Produces({MediaType.TEXT_HTML})
@Path("bigpipe")
public class BigPipeRestResource {

    private final DynamicWebInterfaceManager dynamicWebInterfaceManager;

    public BigPipeRestResource(DynamicWebInterfaceManager dynamicWebInterfaceManager) {
        this.dynamicWebInterfaceManager = dynamicWebInterfaceManager;
    }

    @GET
    public Response render(){
        try (StringWriter writer = new StringWriter()) {
            List<WebPanelModuleDescriptor> displayableWebPanelDescriptors = dynamicWebInterfaceManager.getDisplayableWebPanelDescriptors("com.atlassian.jira.dev.func-test-plugin:bigpipe-panel", emptyMap());
            for (WebPanelModuleDescriptor displayableWebPanelDescriptor : displayableWebPanelDescriptors) {
                displayableWebPanelDescriptor.getModule().writeHtml(writer, emptyMap());
            }
            return Response.ok(writer.toString()).build();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
