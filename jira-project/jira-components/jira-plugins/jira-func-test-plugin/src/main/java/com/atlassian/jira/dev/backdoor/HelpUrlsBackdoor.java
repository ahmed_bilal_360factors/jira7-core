package com.atlassian.jira.dev.backdoor;

import com.atlassian.jira.help.HelpUrl;
import com.atlassian.jira.help.HelpUrls;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Backdoor for querying help urls.
 *
 * @since v7.0
 */
@AnonymousAllowed
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@Path("/help")
public class HelpUrlsBackdoor {
    private final HelpUrls helpUrls;

    public HelpUrlsBackdoor(HelpUrls helpUrls) {
        this.helpUrls = helpUrls;
    }

    @GET
    @Path("{key}")
    public Response getHelpUrl(@PathParam("key") String key) {
        HelpUrl url = helpUrls.getUrl(key);
        return Response.ok(url.getUrl()).build();
    }

}
