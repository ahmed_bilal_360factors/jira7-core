package com.atlassian.jira.dev.backdoor;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.embedded.impl.ImmutableUser;
import com.atlassian.jira.bc.user.search.UserSearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.crowd.embedded.ofbiz.ExtendedUserDao;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.DelegatingApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.google.common.collect.ImmutableList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

/**
 * Backdoor for testing user manager and user searching.
 *
 * @since v7.0
 */
@AnonymousAllowed
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@Path("/userManager")
public class UserManagerBackdoor {
    private final UserManager userManager;
    private final UserSearchService userSearchService;
    private final ExtendedUserDao ofBizUserDao;

    public UserManagerBackdoor(UserSearchService userSearchService, UserManager userManager) {
        this.userSearchService = userSearchService;
        this.userManager = userManager;

        //ExtendedUserDao is internal so need to use ComponentAccessor to get this - for testing only
        this.ofBizUserDao = ComponentAccessor.getComponent(ExtendedUserDao.class);
    }

    @GET
    @Path("keysByFullName")
    public List<String> findUserKeysByFullName(@QueryParam("fullName") String fullName) {
        return ImmutableList.copyOf(userSearchService.findUserKeysByFullName(fullName));
    }

    @GET
    @Path("keysByEmail")
    public List<String> findUserKeysByEmail(@QueryParam("email") String email) {
        return ImmutableList.copyOf(userSearchService.findUserKeysByEmail(email));
    }

    @GET
    @Path("userNamesStreamed")
    public List<String> getUserNamesStreamed() {
        List<String> users = new ArrayList<>();
        ofBizUserDao.processUsers((User user) -> users.add(user.getName()));

        return users;
    }

    @PUT
    @Path("active")
    public void setActive(@QueryParam("username") final String username, @QueryParam("active") final String active) {
        final ApplicationUser user = userManager.getUserByName(username);
        final ImmutableUser.Builder newUserBuilder = new ImmutableUser.Builder()
                .directoryId(user.getDirectoryId())
                .name(user.getName())
                .displayName(user.getDisplayName())
                .emailAddress(user.getEmailAddress())
                .active(Boolean.valueOf(active));
        final ApplicationUser newUser = new DelegatingApplicationUser(user.getId(), user.getKey(), newUserBuilder.toUser());
        userManager.updateUser(newUser);
    }
}
