package com.atlassian.jira.dev.backdoor.sal;

import com.atlassian.sal.api.license.BaseLicenseDetails;
import com.atlassian.sal.api.license.SingleProductLicenseDetailsView;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @since v7.0
 */
public class SingleProductLicenseDetailsViewTO extends SalLicenseBackdoor.BaseLicenseDetailsTO {
    @JsonProperty
    String productKey;
    @JsonProperty
    boolean isUnlimitedNumberOfUsers;
    @JsonProperty
    int numberOfUsers;
    @JsonProperty
    String productDisplayName;

    public SingleProductLicenseDetailsViewTO() {
    }

    public SingleProductLicenseDetailsViewTO(BaseLicenseDetails baseLicenseDetails) {
        this.isEvaluationLicense = baseLicenseDetails.isEvaluationLicense();
        this.licenseTypeName = baseLicenseDetails.getLicenseTypeName();
        this.organisationName = baseLicenseDetails.getOrganisationName();
        this.isEnterpriseLicensingAgreement = baseLicenseDetails.isEnterpriseLicensingAgreement();
        this.isDataCenter = baseLicenseDetails.isDataCenter();
        this.maintenanceExpiryDate = baseLicenseDetails.getMaintenanceExpiryDate();
        this.licenseExpiryDate = baseLicenseDetails.getLicenseExpiryDate();
        this.isPerpetualLicense = baseLicenseDetails.isPerpetualLicense();
        this.serverId = baseLicenseDetails.getServerId();
        this.description = baseLicenseDetails.getDescription();
        this.supportEntitlementNumber = baseLicenseDetails.getSupportEntitlementNumber();
    }

    public SingleProductLicenseDetailsViewTO(SingleProductLicenseDetailsView singleProductLicenseDetailsView) {
        this.isEvaluationLicense = singleProductLicenseDetailsView.isEvaluationLicense();
        this.licenseTypeName = singleProductLicenseDetailsView.getLicenseTypeName();
        this.organisationName = singleProductLicenseDetailsView.getOrganisationName();
        this.isEnterpriseLicensingAgreement = singleProductLicenseDetailsView.isEnterpriseLicensingAgreement();
        this.isDataCenter = singleProductLicenseDetailsView.isDataCenter();
        this.maintenanceExpiryDate = singleProductLicenseDetailsView.getMaintenanceExpiryDate();
        this.licenseExpiryDate = singleProductLicenseDetailsView.getLicenseExpiryDate();
        this.isPerpetualLicense = singleProductLicenseDetailsView.isPerpetualLicense();
        this.serverId = singleProductLicenseDetailsView.getServerId();
        this.description = singleProductLicenseDetailsView.getDescription();
        this.supportEntitlementNumber = singleProductLicenseDetailsView.getSupportEntitlementNumber();
        this.productKey = singleProductLicenseDetailsView.getProductKey();
        this.isUnlimitedNumberOfUsers = singleProductLicenseDetailsView.isUnlimitedNumberOfUsers();
        this.productDisplayName = singleProductLicenseDetailsView.getProductDisplayName();
        this.numberOfUsers = singleProductLicenseDetailsView.getNumberOfUsers();
    }


    public void setSingleProductLicenseDetailsView(SingleProductLicenseDetailsView singleProductLicenseDetailsView) {
        this.productKey = singleProductLicenseDetailsView.getProductKey();
        this.isUnlimitedNumberOfUsers = singleProductLicenseDetailsView.isUnlimitedNumberOfUsers();
        this.productDisplayName = singleProductLicenseDetailsView.getProductDisplayName();
        this.numberOfUsers = singleProductLicenseDetailsView.getNumberOfUsers();
    }
}
