package com.atlassian.jira.dev.events;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventListenerRegistrar;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Iterables;
import com.google.common.collect.ImmutableList;
import org.codehaus.jackson.annotate.JsonProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.util.ArrayDeque;
import java.util.Deque;

/**
 * @since v7.0
 */
@Component
public class EventRecorder {
    private static final int NUM_ELEMENTS = 200;

    private final EventListenerRegistrar eventManager;
    private final Deque<String> events = new ArrayDeque<>(NUM_ELEMENTS);
    private int headPosition = 0;
    private boolean recording = false;

    @Autowired
    public EventRecorder(EventPublisher eventManager) {
        this.eventManager = eventManager;
    }

    @PreDestroy
    public void preDestory() {
        stopRecording();
    }

    public synchronized Result getCurrentPosition() {
        ensureRecording();

        return Result.empty(headPosition + events.size());
    }

    public synchronized Result get(final int position) {
        ensureRecording();

        final int safePosition = Math.max(position, headPosition);
        final int size = events.size();
        final int drop = Math.min(safePosition - headPosition, size);
        if (drop < size) {
            return new Result(Iterables.drop(drop, events), safePosition);
        } else {
            return Result.empty(headPosition + size);
        }
    }

    public synchronized void stopRecording() {
        if (recording) {
            eventManager.unregister(this);
            recording = false;
            headPosition = headPosition + events.size();
            events.clear();
        }
    }

    private synchronized void ensureRecording() {
        if (!recording) {
            eventManager.register(this);
            recording = true;
        }
    }

    @EventListener
    public synchronized void listen(Object event) {
        if (events.size() >= NUM_ELEMENTS) {
            headPosition++;
            events.removeFirst();

        }
        events.addLast(event.getClass().getName());
    }

    public static class Result {
        @JsonProperty
        private final Iterable<Object> events;

        @JsonProperty
        private final int position;

        public Result(final Iterable<?> events, final int position) {
            this.events = ImmutableList.copyOf(events);
            this.position = position;
        }

        public Iterable<Object> getEvents() {
            return events;
        }

        public int getPosition() {
            return position;
        }

        private static Result empty(int position) {
            return new Result(ImmutableList.of(), position);
        }
    }
}
