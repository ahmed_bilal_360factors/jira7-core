package com.atlassian.jira.dev.backdoor;

import com.atlassian.jira.application.ApplicationAuthorizationService;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.atlassian.application.api.ApplicationKey.valueOf;

/**
 * Backdoor JSON interface to access the active users for a specific application role.
 *
 * @since 6.4
 */
@Path("roleCount")
@AnonymousAllowed
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class RoleCount {
    private final ApplicationAuthorizationService applicationAuthorizationService;

    public RoleCount(final ApplicationAuthorizationService applicationAuthorizationService) {
        this.applicationAuthorizationService = applicationAuthorizationService;
    }

    @GET
    @Path("{applicationKey}")
    public Response getActiveUsers(@PathParam("applicationKey") String applicationKey) {
        final int userCount = applicationAuthorizationService.getUserCount(valueOf(applicationKey));
        return Response.ok(new UsersInLicense(applicationKey, userCount)).build();
    }
}
