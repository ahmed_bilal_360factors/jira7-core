package com.atlassian.jira.dev.backdoor.upgrade;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PluginUpgradeDetailsJsonBean {

    @JsonProperty
    private Integer buildNumber;

    @JsonProperty
    private Integer salBuildNumber;

    @JsonProperty
    private Integer aoBuildNumber;

    public PluginUpgradeDetailsJsonBean(final int buildnumber, final int salBuildNumber, final int aoBuildNumber) {
        this.buildNumber = buildnumber;
        this.salBuildNumber = salBuildNumber;
        this.aoBuildNumber = aoBuildNumber;
    }
}
