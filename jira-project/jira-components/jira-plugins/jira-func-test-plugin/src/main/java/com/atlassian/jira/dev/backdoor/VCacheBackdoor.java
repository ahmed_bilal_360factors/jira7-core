package com.atlassian.jira.dev.backdoor;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.vcache.DirectExternalCache;
import com.atlassian.vcache.ExternalCacheSettingsBuilder;
import com.atlassian.vcache.PutPolicy;
import com.atlassian.vcache.RequestCache;
import com.atlassian.vcache.VCacheFactory;
import com.atlassian.vcache.VCacheUtils;
import com.atlassian.vcache.marshallers.MarshallerFactory;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

@AnonymousAllowed
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@Path("/vcache")
public class VCacheBackdoor {
    private final VCacheFactory vCacheFactory;

    public VCacheBackdoor(VCacheFactory vCacheFactory) {
        this.vCacheFactory = vCacheFactory;
    }

    @POST
    @Path("/direct/{name}/{key}")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces({MediaType.TEXT_PLAIN})
    public Response putValueToDirectCache(@PathParam("name") String cacheName, @PathParam("key") String key,
                                          @DefaultValue("PUT_ALWAYS") @QueryParam("putPolicy") PutPolicy putPolicy, String value) {
        final DirectExternalCache<String> directExternalCache = directExternalCache(cacheName);

        final CompletionStage<String> result = directExternalCache.put(key, value, putPolicy)
                .thenApply(Object::toString);

        return Response.ok(VCacheUtils.join(result)).build();
    }

    @GET
    @Path("/direct/{name}/{key}")
    @Produces({MediaType.TEXT_PLAIN})
    public Response getValueFromDirectCache(@PathParam("name") String cacheName, @PathParam("key") String key) {
        final DirectExternalCache<String> directExternalCache = directExternalCache(cacheName);

        final CompletionStage<Optional<String>> result = directExternalCache.get(key);
        return Response.ok(VCacheUtils.join(result).orElse("")).build();
    }

    @POST
    @Path("/request/{name}/{key}")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces({MediaType.TEXT_PLAIN})
    public Response putValueToRequestCache(@PathParam("name") String cacheName, @PathParam("key") String key,
                                           String value) {
        final RequestCache<String, String> requestCache = vCacheFactory.getRequestCache(cacheName);

        requestCache.put(key, value);

        return Response.ok().build();
    }

    @GET
    @Path("/request/{name}/{key}")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces({MediaType.TEXT_PLAIN})
    public Response getValueFromRequestCache(@PathParam("name") String cacheName, @PathParam("key") String key) {
        final RequestCache<String, String> requestCache = vCacheFactory.getRequestCache(cacheName);

        return Response.ok(requestCache.get(key).orElse("")).build();
    }

    private DirectExternalCache<String> directExternalCache(String cacheName) {
        return vCacheFactory.getDirectExternalCache(cacheName, MarshallerFactory.stringMarshaller(),
                new ExternalCacheSettingsBuilder().build());
    }
}
