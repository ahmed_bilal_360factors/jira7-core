package com.atlassian.jira.dev.backdoor;

import com.atlassian.jira.dev.events.EventRecorder;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.atlassian.jira.dev.backdoor.util.CacheControl.never;
import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Provides the ability to monitor the events that trigger on the server via REST endpoint.
 *
 * @since v7.0
 */
@Path("/event")
@AnonymousAllowed
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class EventRecorderBackdoor {
    private final EventRecorder recorder;

    public EventRecorderBackdoor(final EventRecorder recorder) {
        this.recorder = notNull("recorder", recorder);
    }

    @GET
    public Response getEvents(@QueryParam("position") final int position) {
        final EventRecorder.Result result = position == -1 ? recorder.getCurrentPosition() : recorder.get(position);
        return Response.ok(result)
                .cacheControl(never())
                .build();
    }

    @DELETE
    public Response deleteRecorder() {
        recorder.stopRecording();
        return Response.ok()
                .cacheControl(never())
                .build();
    }
}
