package com.atlassian.jira.dev.backdoor;

import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.Collection;
import java.util.List;

/**
 * Backdoor for testing group manager and user filtering.
 *
 * @since v7.0
 */
@AnonymousAllowed
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@Path("/groupManager")
public class GroupManagerBackdoor {
    private final GroupManager groupManager;

    public GroupManagerBackdoor(GroupManager groupManager) {
        this.groupManager = groupManager;
    }

    @GET
    @Path("directMembers")
    public Collection<String> getNamesOfDirectMembersOfGroups(@QueryParam("groupNames") List<String> groupNames, @QueryParam("limit") Integer limit) {
        return groupManager.getNamesOfDirectMembersOfGroups(groupNames, limit == null ? 0 : limit);
    }

    @GET
    @Path("filteredUsers")
    public Collection<String> filterUsersInAllGroupsDirect(@QueryParam("userNames") List<String> userNames, @QueryParam("groupNames") List<String> groupNames) {
        return groupManager.filterUsersInAllGroupsDirect(userNames, groupNames);
    }
}
