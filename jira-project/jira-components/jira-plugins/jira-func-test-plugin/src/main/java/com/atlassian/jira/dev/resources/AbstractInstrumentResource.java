package com.atlassian.jira.dev.resources;

import com.google.common.base.Splitter;

import java.io.IOException;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Abstract class for resource instrumentation implementations.
 *
 * @since 7.1
 */
public abstract class AbstractInstrumentResource {
    public abstract String instrumentResource(String source, String resourceName) throws IOException;

    public String instrumentBatch(final String content, final String requestUrl) throws IOException {
        Pattern resourcePattern = Pattern.compile("/\\* module-key = '([^']+)', location = '([^']+)' \\*/");

        final StringBuffer resultContent = new StringBuffer();

        final Iterator<String> lines = Splitter.on("\n").split(content).iterator();
        final StringBuffer resourceContent = new StringBuffer();
        String resourceName = null;
        String resourceComment = null;

        while (lines.hasNext()) {
            final String line = lines.next();
            final Matcher matcher = resourcePattern.matcher(line);
            final boolean isResourceHeader = matcher.matches();

            if (!isResourceHeader) {
                resourceContent.append(line).append("\n");
            }

            if ((!lines.hasNext() || isResourceHeader) && (resourceContent.length() > 0 && resourceName != null)) {
                resultContent.append(resourceComment)
                        .append("\n")
                        .append(instrumentResource(resourceContent.toString(), resourceName))
                        .append("\n");
            }

            if (isResourceHeader) {
                resourceComment = line;
                resourceName = getResourceName(matcher.group(1), matcher.group(2));
                resourceContent.setLength(0);
            }
        }

        return resultContent.toString();
    }

    public String getResourceName(String moduleName, String resourceLocation) {
        return moduleName + (resourceLocation.startsWith("/") ? "" : "/") + resourceLocation;
    }

}
