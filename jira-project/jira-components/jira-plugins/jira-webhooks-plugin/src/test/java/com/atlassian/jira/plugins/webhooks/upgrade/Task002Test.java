package com.atlassian.jira.plugins.webhooks.upgrade;

import com.atlassian.activeobjects.test.TestActiveObjects;
import com.atlassian.jira.plugins.webhooks.ao.WebhookDao;
import net.java.ao.EntityManager;
import net.java.ao.test.converters.NameConverters;
import net.java.ao.test.jdbc.Data;
import net.java.ao.test.junit.ActiveObjectsJUnitRunner;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.jira.plugins.webhooks.upgrade.WebHookUpgradeTaskUtil.assertEventsEqual;
import static com.atlassian.jira.plugins.webhooks.upgrade.WebHookUpgradeTaskUtil.createWebhook;
import static org.junit.Assert.assertThat;

@RunWith(ActiveObjectsJUnitRunner.class)
@Data(Task001Test.Data.class)
@NameConverters
public class Task002Test {
    // injected by ActiveObjectsJunitRunner
    private EntityManager entityManager;
    private final String[] allEvents = new String[]{"jira:issue_created", "jira:issue_deleted", "jira:issue_updated", "jira:worklog_updated"};
    private TestActiveObjects activeObjects;

    @Before
    public void setUp() throws Exception {
        activeObjects = new TestActiveObjects(entityManager);
    }

    @Test
    public void testChangingAllEventsIntoActualEventNames() throws Exception {
        WebhookDao webhook = createWebhook(entityManager, "webhook name", "events:all_events");
        new Task002(activeObjects).doUpgrade();
        WebhookDao webHookAfterUpgrade = activeObjects.get(WebhookDao.class, webhook.getID());
        assertEventsEqual(webHookAfterUpgrade.getEvents(), allEvents);
    }

    @Test
    public void testChangingNoEventsIntoEmptyArray() throws Exception {
        WebhookDao webhook = createWebhook(entityManager, "webhook name", "events:no_events");
        new Task002(activeObjects).doUpgrade();
        WebhookDao webHookAfterUpgrade = activeObjects.get(WebhookDao.class, webhook.getID());
        assertThat(webHookAfterUpgrade.getEvents(), CoreMatchers.is(new String[]{}));
    }

    @Test
    public void testProperEventNamesAreNotChanged() throws Exception {
        WebhookDao webhook = createWebhook(entityManager, "webhook name", "jira:issue_updated");
        new Task002(activeObjects).doUpgrade();
        WebhookDao webHookAfterUpgrade = activeObjects.get(WebhookDao.class, webhook.getID());
        assertEventsEqual(webHookAfterUpgrade.getEvents(), "jira:issue_updated");
    }
}
