package com.atlassian.jira.plugins.webhooks.serializer;

import com.atlassian.jira.event.project.VersionDeleteEvent;
import com.atlassian.jira.plugins.webhooks.registration.RegisteredWebHookEventFactory;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.rest.v2.issue.version.VersionBean;
import com.google.common.collect.ImmutableMap;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class VersionDeletedEventSerializerTest {
    @Mock
    private Version versionToDelete;
    @Mock
    private RegisteredWebHookEventFactory factory;
    @Mock
    private SimpleVersionBeanFactory versionBeanFactory;

    private VersionDeletedEventSerializer serializer;

    @Before
    public void setUp() throws Exception {
        when(versionBeanFactory.createBean(Mockito.any(Version.class))).thenReturn(mock(VersionBean.class));
        this.serializer = new VersionDeletedEventSerializer(factory, versionBeanFactory);
    }

    @Test
    public void testMergedVersionSet() {
        final Version mergedToVersion = mock(Version.class);
        final VersionDeleteEvent event = new VersionDeleteEvent.VersionDeleteEventBuilder(versionToDelete)
                .setMergedTo(mergedToVersion)
                .createEvent();

        final ImmutableMap<String, Object> serializedVersion = serializer.putFields(event, ImmutableMap.<String, Object>builder()).build();

        assertThat(serializedVersion, Matchers.hasKey("mergedTo"));
    }


    @Test
    public void testFixVersionSwappedToSet() {
        final Version affectsVersionSwappedTo = mock(Version.class);
        final VersionDeleteEvent event = new VersionDeleteEvent.VersionDeleteEventBuilder(versionToDelete)
                .affectsVersionSwappedTo(affectsVersionSwappedTo)
                .createEvent();

        final ImmutableMap<String, Object> serializedVersion = serializer.putFields(event, ImmutableMap.<String, Object>builder()).build();

        assertThat(serializedVersion, Matchers.hasKey("affectsVersionSwappedTo"));
    }

    @Test
    public void testVersionMergedToSet() {
        final Version fixVersionSwappedTo = mock(Version.class);
        final VersionDeleteEvent event = new VersionDeleteEvent.VersionDeleteEventBuilder(versionToDelete)
                .fixVersionSwappedTo(fixVersionSwappedTo)
                .createEvent();

        final ImmutableMap<String, Object> serializedVersion = serializer.putFields(event, ImmutableMap.<String, Object>builder()).build();

        assertThat(serializedVersion, Matchers.hasKey("fixVersionSwappedTo"));
    }

}