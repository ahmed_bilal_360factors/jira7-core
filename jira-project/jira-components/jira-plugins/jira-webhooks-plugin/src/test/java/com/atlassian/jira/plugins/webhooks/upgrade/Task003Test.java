package com.atlassian.jira.plugins.webhooks.upgrade;

import com.atlassian.activeobjects.test.TestActiveObjects;
import com.atlassian.jira.plugins.webhooks.ao.WebhookDao;
import com.google.common.collect.Lists;
import net.java.ao.EntityManager;
import net.java.ao.test.converters.NameConverters;
import net.java.ao.test.jdbc.Data;
import net.java.ao.test.jdbc.DatabaseUpdater;
import net.java.ao.test.junit.ActiveObjectsJUnitRunner;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@RunWith(ActiveObjectsJUnitRunner.class)
@Data(Task003Test.Data.class)
@NameConverters
public class Task003Test {
    // injected by ActiveObjectsJunitRunner
    private EntityManager entityManager;
    private TestActiveObjects activeObjects;

    @Before
    public void setUp() throws Exception {
        activeObjects = new TestActiveObjects(entityManager);
    }

    @Test
    public void unrecognisedEvent() throws Exception {
        assertSingleEventUnchanged("aklsdfjaklsfjaklsdfjasdlfksdjfklasdf");
    }

    @Test
    public void unmodifiedEventWithJiraPrefix() throws Exception {
        assertSingleEventUnchanged("jira:issue_created");
    }

    @Test
    public void noWebhooksDefined() throws Exception {
        performUpgrade();
        // There are still no webhooks
        assertEquals(0, entityManager.count(WebhookDao.class));
    }

    @Test
    public void changed() throws Exception {
        String[] events = {
                "jira:version_created",
                "jira:version_released",
                "jira:version_unreleased",
                "jira:version_merged",
                "jira:version_deleted",
                "jira:version_updated",
                "jira:version_moved"};

        String[] newEvents = {
                "version_created",
                "version_released",
                "version_unreleased",
                "version_merged",
                "version_deleted",
                "version_updated",
                "version_moved"};

        final WebhookDao webhook = WebHookUpgradeTaskUtil.createWebhook(entityManager, "testing", events);

        performUpgrade();
        WebhookDao webHookAfterUpgrade = activeObjects.get(WebhookDao.class, webhook.getID());
        assertThat(webHookAfterUpgrade, isRegisteredForEvents(newEvents));
    }

    @Test
    public void bothPrefixedAndNotEvents() throws Exception {
        String[] events = {
                "jira:version_created",
                "jira:issue_created",
                "user_deleted"
        };
        String[] newEvents = {
                "version_created",
                "jira:issue_created",
                "user_deleted"
        };
        final WebhookDao webhook = WebHookUpgradeTaskUtil.createWebhook(entityManager, "testing", events);

        performUpgrade();
        WebhookDao webHookAfterUpgrade = activeObjects.get(WebhookDao.class, webhook.getID());
        assertThat(webHookAfterUpgrade, isRegisteredForEvents(newEvents));
    }

    @Test
    public void emptyEvents() throws Exception {
        final WebhookDao webhook = WebHookUpgradeTaskUtil.createWebhook(entityManager, "testing", new String[0]);

        performUpgrade();
        assertThat(webhook, isRegisteredForEvents(new String[0]));
    }

    @Test(expected = java.lang.reflect.UndeclaredThrowableException.class)
    public void nullEvents() throws Exception {
        final WebhookDao webhook = WebHookUpgradeTaskUtil.createWebhook(entityManager, "testing", null);
    }

    private void performUpgrade() throws Exception {
        new Task003(activeObjects).doUpgrade();
    }

    private void assertSingleEventUnchanged(final String event) throws Exception {
        final WebhookDao webhook = WebHookUpgradeTaskUtil.createWebhook(entityManager, "webhook name", event);
        performUpgrade();
        assertThat(webhook, isRegisteredForEvents(event));
    }

    @SuppressWarnings("unchecked")
    public static final class Data implements DatabaseUpdater {
        @Override
        public void update(final EntityManager entityManager) throws Exception {
            entityManager.migrate(WebhookDao.class);
        }
    }

    private Matcher<WebhookDao> isRegisteredForEvents(final String... expectedEvents) {
        return new TypeSafeMatcher<WebhookDao>() {
            @Override
            protected boolean matchesSafely(final WebhookDao webhook) {
                final List<String> actual = Lists.newArrayList(webhook.getEvents());
                final List<String> expected = Lists.newArrayList(expectedEvents);

                Collections.sort(actual);
                Collections.sort(expected);

                return actual.equals(expected);
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("WebHook registered for events " + Arrays.toString(expectedEvents));
            }
        };
    }
}
