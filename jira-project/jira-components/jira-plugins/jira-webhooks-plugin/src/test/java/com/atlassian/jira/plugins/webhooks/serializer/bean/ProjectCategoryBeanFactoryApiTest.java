package com.atlassian.jira.plugins.webhooks.serializer.bean;

import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.project.ProjectCategory;
import com.atlassian.jira.project.ProjectCategoryImpl;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProjectCategoryBeanFactoryApiTest {
    private static final long ID = 10000L;
    private static final String NAME = "Name";
    private static final String DESCRIPTION = "Description";

    @Mock
    private JiraBaseUrls urls;

    private ProjectCategoryBeanFactory factory;

    @Before
    public void setUp() throws Exception {
        when(urls.restApi2BaseUrl()).thenReturn("/rest/");

        factory = new ProjectCategoryBeanFactory(urls);
    }

    @Test
    public void testCreateBean() throws Exception {
        final ProjectCategory projectCategory = new ProjectCategoryImpl(ID, NAME, DESCRIPTION);
        final ProjectCategoryBean bean = factory.createBean(projectCategory);

        assertThat(bean, projectCategoryBeanMatcher(ID, NAME, DESCRIPTION));
    }

    private Matcher<ProjectCategoryBean> projectCategoryBeanMatcher(final Long id, final String name, final String description) {
        // noinspection unchecked
        return Matchers.<ProjectCategoryBean>allOf(
                hasProperty("self", equalTo("/rest/projectCategory/" + id)),
                hasProperty("id", equalTo(id)),
                hasProperty("name", equalTo(name)),
                hasProperty("description", equalTo(description))
        );
    }
}