package com.atlassian.jira.plugins.webhooks.upgrade;

import com.atlassian.jira.plugins.webhooks.ao.WebhookDao;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import net.java.ao.EntityManager;

import java.sql.SQLException;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class WebHookUpgradeTaskUtil {
    static void assertEventsEqual(final String[] actualEvents, final String... expectedEvents) {
        final List<String> actual = Lists.newArrayList(actualEvents);
        final List<String> expected = Lists.newArrayList(expectedEvents);

        Collections.sort(actual);
        Collections.sort(expected);

        assertEquals(expected, actual);
    }

    static WebhookDao createWebhook(final EntityManager entityManager, final String name, final String... events) throws SQLException {
        final WebhookDao dao = entityManager.create(WebhookDao.class, ImmutableMap.<String, Object>of(
                "LAST_UPDATED_USER", "admin",
                "URL", "http://www.example.com",
                "LAST_UPDATED", new Date(),
                "NAME", name,
                "REGISTRATION_METHOD", "testing"
        ));
        dao.setEvents(events);
        dao.save();
        return dao;
    }
}
