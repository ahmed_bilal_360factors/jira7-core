package com.atlassian.jira.plugins.webhooks.serializer.bean;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.project.AssigneeTypes;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectCategory;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import java.net.URI;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ProjectBeanFactoryApiTest {
    @Rule
    public RuleChain chain = MockitoMocksInContainer.forTest(this);

    private static final long PROJECT_ID = 10000L;
    private static final String PROJECT_KEY = "KEY";
    private static final String PROJECT_NAME = "Name";
    private static final String LEAD_KEY = "USER";

    @Mock
    @AvailableInContainer
    private AvatarService avatarService;
    @Mock
    private JiraBaseUrls urls;
    @Mock
    private UserManager userManager;
    @Mock
    private UserBeanFactory userBeanFactory;
    @Mock
    private ProjectCategoryBeanFactory projectCategoryBeanFactory;

    @Mock
    private ProjectCategory projectCategory;
    @Mock
    private Avatar avatar;
    @Mock
    private ProjectCategoryBean projectCategoryBean;
    @Mock
    private UserBean userBean;

    private ProjectBeanFactory factory;

    @Before
    public void setUp() throws Exception {
        final ApplicationUser lead = mock(ApplicationUser.class);
        when(userManager.getUserByKeyEvenWhenUnknown(eq(LEAD_KEY))).thenReturn(lead);
        when(userBeanFactory.createBean(eq(lead))).thenReturn(userBean);
        when(projectCategoryBeanFactory.createBean(eq(projectCategory))).thenReturn(projectCategoryBean);
        when(urls.restApi2BaseUrl()).thenReturn("/rest/");
        when(avatarService.getProjectAvatarAbsoluteURL(any(Project.class), any(Avatar.Size.class))).thenReturn(new URI("uri"));

        factory = new ProjectBeanFactory(userManager, urls, userBeanFactory, projectCategoryBeanFactory);
    }

    @Test
    public void testCreateBean() throws Exception {
        final ProjectBean bean = factory.createBean(getProjectMock());

        assertThat(bean, projectBeanMatcher(PROJECT_ID, PROJECT_KEY, PROJECT_NAME, projectCategoryBean, userBean, AssigneeTypes.PRETTY_PROJECT_LEAD));
    }

    private Project getProjectMock() {
        Project project = mock(Project.class);
        when(project.getId()).thenReturn(PROJECT_ID);
        when(project.getKey()).thenReturn(PROJECT_KEY);
        when(project.getName()).thenReturn(PROJECT_NAME);
        when(project.getAvatar()).thenReturn(avatar);
        when(project.getProjectCategoryObject()).thenReturn(projectCategory);
        when(project.getLeadUserKey()).thenReturn(LEAD_KEY);
        when(project.getAssigneeType()).thenReturn(AssigneeTypes.PROJECT_LEAD);

        return project;
    }

    private Matcher<ProjectBean> projectBeanMatcher(final Long id, final String key, final String name, final ProjectCategoryBean projectCategory, final UserBean projectLead, final String assigneeType) {
        // noinspection unchecked
        return allOf(
                hasProperty("self", equalTo("/rest/project/" + id)),
                hasProperty("id", equalTo(id)),
                hasProperty("key", equalTo(key)),
                hasProperty("name", equalTo(name)),
                hasProperty("avatarUrls"),
                hasProperty("projectCategory", equalTo(projectCategory)),
                hasProperty("projectLead", equalTo(projectLead)),
                hasProperty("assigneeType", equalTo(assigneeType))
        );
    }
}