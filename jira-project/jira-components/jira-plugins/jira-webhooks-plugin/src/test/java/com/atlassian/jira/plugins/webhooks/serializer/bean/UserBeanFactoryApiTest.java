package com.atlassian.jira.plugins.webhooks.serializer.bean;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.user.ApplicationUser;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserBeanFactoryApiTest {
    @Rule
    public RuleChain chain = MockitoMocksInContainer.forTest(this);

    private static final String NAME = "Name";
    private static final String KEY = "KEY";
    private static final String EMAIL = "user@example.com";
    private static final String DISPLAY_NAME = "John Smith";

    @Mock
    @AvailableInContainer
    private ApplicationProperties applicationProperties;
    @Mock
    private JiraBaseUrls urls;

    private UserBeanFactory factory;

    @Before
    public void setUp() throws Exception {
        when(applicationProperties.getEncoding()).thenReturn("UTF-8");
        when(urls.restApi2BaseUrl()).thenReturn("/rest/");

        factory = new UserBeanFactory(urls);
    }

    @Test
    public void testCreateBean() throws Exception {
        final UserBean bean = factory.createBean(getUserMock());

        assertThat(bean, projectCategoryBeanMatcher(NAME, KEY, EMAIL, DISPLAY_NAME));
    }

    private ApplicationUser getUserMock() {
        ApplicationUser user = mock(ApplicationUser.class);
        when(user.getName()).thenReturn(NAME);
        when(user.getKey()).thenReturn(KEY);
        when(user.getEmailAddress()).thenReturn(EMAIL);
        when(user.getDisplayName()).thenReturn(DISPLAY_NAME);

        return user;
    }

    private Matcher<UserBean> projectCategoryBeanMatcher(final String name, final String key, final String emailAddress, final String displayName) {
        //noinspection unchecked
        return Matchers.<UserBean>allOf(
                hasProperty("self", equalTo("/rest/user?key=" + key)),
                hasProperty("name", equalTo(name)),
                hasProperty("key", equalTo(key)),
                hasProperty("emailAddress", equalTo(emailAddress)),
                hasProperty("displayName", equalTo(displayName))
        );
    }
}