package com.atlassian.jira.plugins.webhooks.workflow;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.rest.json.UserBeanFactory;
import com.atlassian.jira.issue.fields.rest.json.beans.UserJsonBean;
import com.atlassian.jira.plugins.webhooks.serializer.IssueBeanFactory;
import com.atlassian.jira.rest.v2.issue.IssueBean;
import com.atlassian.jira.user.ApplicationUser;
import com.opensymphony.workflow.StoreException;
import com.opensymphony.workflow.loader.ActionDescriptor;
import com.opensymphony.workflow.loader.StepDescriptor;
import com.opensymphony.workflow.loader.WorkflowDescriptor;
import com.opensymphony.workflow.spi.Step;
import com.opensymphony.workflow.spi.WorkflowEntry;
import com.opensymphony.workflow.spi.WorkflowStore;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.atlassian.jira.plugins.webhooks.workflow.TransitionMapper.ACTION_ID;
import static com.atlassian.jira.plugins.webhooks.workflow.TransitionMapper.CREATED_STEP;
import static com.atlassian.jira.plugins.webhooks.workflow.TransitionMapper.ENTRY;
import static com.atlassian.jira.plugins.webhooks.workflow.TransitionMapper.FROM_STATUS;
import static com.atlassian.jira.plugins.webhooks.workflow.TransitionMapper.TO_STATUS;
import static com.atlassian.jira.plugins.webhooks.workflow.TransitionMapper.TRANSITION_CONTEXT;
import static com.atlassian.jira.plugins.webhooks.workflow.TransitionMapper.WORKFLOW_DESCRIPTOR;
import static com.atlassian.jira.plugins.webhooks.workflow.TransitionMapper.WORKFLOW_STORE;
import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasEntry;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestTransitionMapper {
    private static int WORKFLOW_ACTION_ID = 1;

    @Mock
    private IssueBeanFactory issueBeanFactory;
    @Mock
    private UserBeanFactory userBeanFactory;
    @Mock
    private ApplicationUser user;
    @Mock
    private MutableIssue mutableIssue;

    private TransitionMapper transitionMapper;
    private Map<String, Object> inputContext;

    @Before
    public void setup() {
        setupClassesToPopulateUserAndIssueInfo();
        populateInitialInputContext();

        transitionMapper = new TransitionMapper(issueBeanFactory, userBeanFactory);
    }

    @Test
    public void fromStatusAndToStatusReturnsExpectedStepNameInTheTransitionContextIfTheWorkflowContainsOnlyTwoSteps()
            throws StoreException {
        Step originStep = stepWithId(1);
        Step destinationStep = stepWithIdAndPreviousIds(2, new long[]{1L});
        inputContext.put(CREATED_STEP, destinationStep);
        inputContext.put(WORKFLOW_STORE, workflowStoreWithHistorySteps(originStep, destinationStep));

        WorkflowDescriptor workflowDescriptor = workflowDescriptorWithSteps(originStep, destinationStep);
        inputContext.put(WORKFLOW_DESCRIPTOR, workflowDescriptor);

        Map<String, Object> transitionContext = (Map<String, Object>) transitionMapper.toMap(inputContext, user, mutableIssue).get(TRANSITION_CONTEXT);

        assertThat(transitionContext, hasEntry(FROM_STATUS, (Object) stepNameFor(originStep)));
        assertThat(transitionContext, hasEntry(TO_STATUS, (Object) stepNameFor(destinationStep)));
    }

    @Test
    public void fromStatusReturnsAnEmptyStringInTheTransitionContextIfThereAreNoPreviousStepsForTheCurrentStep()
            throws StoreException {
        Step currentStep = stepWithId(1);
        inputContext.put(CREATED_STEP, currentStep);
        inputContext.put(WORKFLOW_STORE, workflowStoreWithHistorySteps(currentStep));

        WorkflowDescriptor workflowDescriptor = workflowDescriptorWithSteps(currentStep);
        inputContext.put(WORKFLOW_DESCRIPTOR, workflowDescriptor);

        Map<String, Object> transitionContext = (Map<String, Object>) transitionMapper.toMap(inputContext, user, mutableIssue).get(TRANSITION_CONTEXT);

        assertThat(transitionContext, hasEntry(FROM_STATUS, (Object) ""));
    }

    @Test
    public void fromStatusReturnsAnEmptyStringInTheTransitionContextIfThePreviousStepCannotBeFound()
            throws StoreException {
        Step currentStep = stepWithIdAndPreviousIds(1, new long[]{1L});
        inputContext.put(CREATED_STEP, currentStep);
        inputContext.put(WORKFLOW_STORE, workflowStoreWithHistorySteps());

        WorkflowDescriptor workflowDescriptor = workflowDescriptorWithSteps(currentStep);
        inputContext.put(WORKFLOW_DESCRIPTOR, workflowDescriptor);

        Map<String, Object> transitionContext = (Map<String, Object>) transitionMapper.toMap(inputContext, user, mutableIssue).get(TRANSITION_CONTEXT);

        assertThat(transitionContext, hasEntry(FROM_STATUS, (Object) ""));
    }

    @Test
    public void fromStatusReturnsAnEmptyStringInTheTransitionContextIfAStoreExceptionIsThrownWhileTryingToRetrieveTheFromStatus()
            throws StoreException {
        Step currentStep = stepWithIdAndPreviousIds(1, new long[]{1L});
        inputContext.put(CREATED_STEP, currentStep);
        inputContext.put(WORKFLOW_STORE, workflowStoreThatThrowsAnException());

        WorkflowDescriptor workflowDescriptor = workflowDescriptorWithSteps(currentStep);
        inputContext.put(WORKFLOW_DESCRIPTOR, workflowDescriptor);

        Map<String, Object> transitionContext = (Map<String, Object>) transitionMapper.toMap(inputContext, user, mutableIssue).get(TRANSITION_CONTEXT);

        assertThat(transitionContext, hasEntry(FROM_STATUS, (Object) ""));
    }

    @Test
    public void toStatusReturnsAnEmptyStringInTheTransitionContextIfTheWorkflowDescriptorCannotFindTheStepDescriptorForTheDestinationStep()
            throws StoreException {
        Step originStep = stepWithId(1);
        Step destinationStep = stepWithIdAndPreviousIds(2, new long[]{1L});
        inputContext.put(CREATED_STEP, destinationStep);
        inputContext.put(WORKFLOW_STORE, workflowStoreWithHistorySteps(originStep, destinationStep));

        WorkflowDescriptor workflowDescriptor = workflowDescriptorWithSteps(originStep);
        inputContext.put(WORKFLOW_DESCRIPTOR, workflowDescriptor);

        Map<String, Object> transitionContext = (Map<String, Object>) transitionMapper.toMap(inputContext, user, mutableIssue).get(TRANSITION_CONTEXT);

        assertThat(transitionContext, hasEntry(TO_STATUS, (Object) ""));
    }

    private void setupClassesToPopulateUserAndIssueInfo() {
        IssueBean issueBean = mock(IssueBean.class);
        UserJsonBean userJsonBean = mock(UserJsonBean.class);

        when(issueBeanFactory.createBean(any(Issue.class))).thenReturn(issueBean);
        when(userBeanFactory.createBean(any(ApplicationUser.class))).thenReturn(userJsonBean);
    }

    private void populateInitialInputContext() {
        inputContext = new HashMap<String, Object>();
        inputContext.put(ENTRY, someWorkflow());
        inputContext.put(ACTION_ID, WORKFLOW_ACTION_ID);
    }

    private WorkflowEntry someWorkflow() {
        WorkflowEntry workflowEntry = mock(WorkflowEntry.class);
        when(workflowEntry.getId()).thenReturn((long) WORKFLOW_ACTION_ID);
        when(workflowEntry.getWorkflowName()).thenReturn("workflow");

        return workflowEntry;
    }

    private void someActionDescriptorFor(WorkflowDescriptor workflowDescriptor) {
        ActionDescriptor actionDescriptor = mock(ActionDescriptor.class);
        when(workflowDescriptor.getAction(WORKFLOW_ACTION_ID)).thenReturn(actionDescriptor);
        when(actionDescriptor.getName()).thenReturn("action-descriptor");
    }

    private Step stepWithIdAndPreviousIds(int stepId, long[] precedingStepIds) {
        Step step = mock(Step.class);
        when(step.getStepId()).thenReturn(stepId);
        when(step.getId()).thenReturn((long) stepId);
        when(step.getPreviousStepIds()).thenReturn(precedingStepIds);
        when(step.getStartDate()).thenReturn(new Date());

        return step;
    }

    private Step stepWithId(int stepId) {
        return stepWithIdAndPreviousIds(stepId, new long[]{});
    }

    private WorkflowStore workflowStoreWithHistorySteps(Step... steps) throws StoreException {
        WorkflowStore workflowStore = mock(WorkflowStore.class);
        when(workflowStore.findHistorySteps((long) WORKFLOW_ACTION_ID)).thenReturn(newArrayList(steps));

        return workflowStore;
    }

    private WorkflowStore workflowStoreThatThrowsAnException() throws StoreException {
        WorkflowStore workflowStore = mock(WorkflowStore.class);
        when(workflowStore.findHistorySteps(WORKFLOW_ACTION_ID)).thenThrow(StoreException.class);

        return workflowStore;
    }

    private WorkflowDescriptor workflowDescriptorWithSteps(Step... steps) {
        WorkflowDescriptor workflowDescriptor = mock(WorkflowDescriptor.class);
        for (Step step : steps) {
            StepDescriptor stepDescriptor = stepDescriptorFromStep(step);
            when(workflowDescriptor.getStep(step.getStepId())).thenReturn(stepDescriptor);
        }

        someActionDescriptorFor(workflowDescriptor);

        return workflowDescriptor;
    }

    private StepDescriptor stepDescriptorFromStep(Step step) {
        String stepName = stepNameFor(step);

        StepDescriptor stepDescriptor = mock(StepDescriptor.class);
        when(stepDescriptor.getName()).thenReturn(stepName);

        return stepDescriptor;
    }

    private String stepNameFor(Step step) {
        return Integer.toString(step.getStepId());
    }
}
