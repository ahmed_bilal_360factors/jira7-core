package com.atlassian.jira.plugins.webhooks.upgrade;

import com.atlassian.activeobjects.test.TestActiveObjects;
import com.atlassian.jira.plugins.webhooks.ao.WebhookDao;
import net.java.ao.EntityManager;
import net.java.ao.test.converters.NameConverters;
import net.java.ao.test.jdbc.Data;
import net.java.ao.test.jdbc.DatabaseUpdater;
import net.java.ao.test.junit.ActiveObjectsJUnitRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(ActiveObjectsJUnitRunner.class)
@Data(Task001Test.Data.class)
@NameConverters
public class Task001Test {
    // injected by ActiveObjectsJunitRunner
    private EntityManager entityManager;
    private TestActiveObjects activeObjects;

    @Before
    public void setUp() throws Exception {
        activeObjects = new TestActiveObjects(entityManager);
    }

    @Test
    public void allEventsUnchanged() throws Exception {
        assertSingleEventUnchanged("events:all_events");
    }

    @Test
    public void noEventsUnchaged() throws Exception {
        assertSingleEventUnchanged("events:no_events");
    }

    @Test
    public void unrecognisedEvent() throws Exception {
        assertSingleEventUnchanged("aklsdfjaklsfjaklsdfjasdlfksdjfklasdf");
    }

    @Test
    public void noWebhooksDefined() throws Exception {
        new Task001(activeObjects).doUpgrade();
        // There are still no webhooks
        assertEquals(0, entityManager.count(WebhookDao.class));
    }

    @Test
    public void noDuplicated() throws Exception {
        // Multiple old events that map to a single new event
        WebhookDao webhook = WebHookUpgradeTaskUtil.createWebhook(entityManager, "webhook name", "issue_assigned", "issue_updated", "issue_moved");
        new Task001(activeObjects).doUpgrade();
        WebhookDao webHookAfterUpgrade = activeObjects.get(WebhookDao.class, webhook.getID());
        WebHookUpgradeTaskUtil.assertEventsEqual(webHookAfterUpgrade.getEvents(), "jira:issue_updated");
    }

    @Test
    public void everything() throws Exception {
        final WebhookDao webhook = WebHookUpgradeTaskUtil.createWebhook(entityManager, "testing",
                "issue_created",
                "issue_deleted",

                "issue_work_logged",
                "issue_worklog_deleted",
                "issue_worklog_updated",

                "issue_assigned",
                "issue_closed",
                "issue_reopened",
                "issue_resolved",
                "issue_comment_edited",
                "issue_commented",
                "issue_moved",
                "issue_updated",
                "issue_work_started",
                "issue_work_stopped",
                "issue_generic_event");

        new Task001(activeObjects).doUpgrade();
        WebhookDao webHookAfterUpgrade = activeObjects.get(WebhookDao.class, webhook.getID());
        WebHookUpgradeTaskUtil.assertEventsEqual(webHookAfterUpgrade.getEvents(), "jira:issue_updated", "jira:issue_created", "jira:issue_deleted", "jira:worklog_updated");
    }

    private void assertSingleEventUnchanged(final String event) throws Exception {
        final WebhookDao webhook = WebHookUpgradeTaskUtil.createWebhook(entityManager, "webhook name", event);
        new Task001(activeObjects).doUpgrade();
        WebHookUpgradeTaskUtil.assertEventsEqual(webhook.getEvents(), event);
    }

    @SuppressWarnings("unchecked")
    public static final class Data implements DatabaseUpdater {
        @Override
        public void update(final EntityManager entityManager) throws Exception {
            entityManager.migrate(WebhookDao.class);
        }
    }
}
