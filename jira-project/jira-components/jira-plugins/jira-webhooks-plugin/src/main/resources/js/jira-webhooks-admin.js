(function ($) {
    var TEMPLATES = atl.plugins.webhooks.admin.templates;

    var TransitionModel = Backbone.Model.extend({
    });

    var TransitionCollection = Backbone.Collection.extend({
        initialize: function (options) {
            this.selectionModel = options.selectionModel;
        },
        url: function () {
            var webhookId = this.selectionModel.getSelected().get("self").split("/").splice(-1)[0];
            return contextPath + "/rest/jira-webhook/1.0/jira-webhook/" + webhookId + "/transitions";
        },
        model: TransitionModel
    });

    var TransitionView = Backbone.View.extend({
        tagName: 'li',
        render: function () {
            var unescapedWorkflowName = this.model.get("workflow");
            var workflowName = AJS.escapeHtml(unescapedWorkflowName);
            var transitionName = AJS.escapeHtml(this.model.get("transition"));
            var workflowLink = AJS.$("<a></a>")
                    .attr("href", contextPath + "/secure/admin/workflows/ViewWorkflowSteps.jspa?workflowMode=live&workflowName=" + unescapedWorkflowName)
                    .html(workflowName)
                    .wrap("<div></div>").parent().html();
            this.$el.append(AJS.I18n.getText("webhooks.transition", transitionName, workflowLink));
        }
    });

    var TransitionsView = Backbone.View.extend({
        el: "#webhook-transitions",
        initialize: function () {
            this.model.selectionModel.onSelectionChange(this.selectionChanged, this);
        },
        render: function () {
            var that = this;
            this.$el.empty();
            if (this.model.length === 0) {
                var noTransitionsMessage = AJS.$("<span></span>").text(AJS.I18n.getText("webhooks.transitions.notransitions"));
                this.$el.append(noTransitionsMessage);
            }

            this.model.each(function (model) {
                var transitionView = new TransitionView({model: model});
                transitionView.render();
                that.$el.append(transitionView.$el);
            });
        },
        selectionChanged: function (selectionModel, selectedModel) {
            if (selectedModel && selectedModel.get("self") != undefined) {
                var that = this;
                this.model.fetch({
                    success: function (model) { that.render(); }
                });
            } else {
                this.model.reset();
                this.render();
            }
        }
    });

    var $jql = null;
    var $jqlDisplay = null;
    var $jqlOverlabel = null;

    webhooks.onInit(function (selectionModel) {
        $jql = $("#webhook-jql").expandOnInput();
        $jqlOverlabel = $("#webhook-jql-overlabel").overlabel();
        $jqlDisplay = $("#webhook-jql-display");
        var transitionCollection = new TransitionCollection({"selectionModel": selectionModel});
        new TransitionsView({model: transitionCollection});
    });

    webhooks.addFilter("issue-related-events-section", function () {
        return $jql.val();
    }, function (filter) {
        $jql.val(filter);
        var trimmedJql = AJS.$.trim(filter);
        $jqlDisplay.text(trimmedJql || AJS.I18n.getText('webhooks.admin.jql.empty'));
        $jqlOverlabel.toggleClass("hidden", !!trimmedJql);
    });

    webhooks.setDateFormatter(function (date) {
        return moment(date).format(JIRA.translateSimpleDateFormat(AJS.Meta.get("date-complete")));
    });
})(AJS.$);
