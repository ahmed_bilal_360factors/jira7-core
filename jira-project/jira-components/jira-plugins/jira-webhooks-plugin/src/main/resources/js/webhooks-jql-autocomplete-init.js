AJS.$(function($) {
	if (!AJS.params.autocompleteEnabled) return;
	var $advSearch = $("#webhook-jql");
	var jqlFieldNames = JSON.parse($("#jqlFieldz").text());
    var jqlFunctionNames = JSON.parse($("#jqlFunctionNamez").text());
    var jqlReservedWords = JSON.parse($("#jqlReservedWordz").text());

	var isAutoSelectFirstEnabled = function() {
		var FeatureFlagManager = require("jira/featureflags/feature-manager");
		return FeatureFlagManager.isFeatureEnabled("jira.jql.autoselectfirst");
	};

    var jqlAutoComplete = JIRA.JQLAutoComplete({
		fieldID: $advSearch.attr("id"),
		parser: JIRA.JQLAutoComplete.MyParser(jqlReservedWords),
		queryDelay: .65,
		jqlFieldNames: jqlFieldNames,
		jqlFunctionNames: jqlFunctionNames,
		minQueryLength: 0,
		allowArrowCarousel: true,
		autoSelectFirst: isAutoSelectFirstEnabled(),
		errorID: 'jqlerrormsg'
	});
    $advSearch.expandOnInput();

	jqlAutoComplete.buildResponseContainer();
	var detachedSuggestionContainer = $('<div class="atlassian-autocomplete webhooks-detached-suggestion-container"/>')
			.append($(jqlAutoComplete.responseContainer).detach())
			.appendTo("body");
	jqlAutoComplete.parse($advSearch.text());
	jqlAutoComplete.updateColumnLineCount();

	$advSearch.keypress(function(event) {
		if (jqlAutoComplete.dropdownController === null || !jqlAutoComplete.dropdownController.displayed || jqlAutoComplete.selectedIndex < 0) {
			if (event.keyCode === 13 && !event.ctrlKey && !event.shiftKey) {
				event.preventDefault();
				jqlAutoComplete.dropdownController.hideDropdown();
			}
		}
	}).bind('expandedOnInput', function() {
		detachedSuggestionContainer.css($advSearch.offset());

		jqlAutoComplete.positionResponseContainer();
	}).click(function() {
		jqlAutoComplete.dropdownController.hideDropdown();
	}).bind('webhooks.valueChanged', function() {
		jqlAutoComplete.parse($advSearch.val());
	});
});
