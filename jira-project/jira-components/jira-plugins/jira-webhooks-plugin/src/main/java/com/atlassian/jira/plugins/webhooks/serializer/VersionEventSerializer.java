package com.atlassian.jira.plugins.webhooks.serializer;

import com.atlassian.jira.event.project.AbstractVersionEvent;
import com.atlassian.jira.plugins.webhooks.registration.RegisteredWebHookEventFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public final class VersionEventSerializer extends AbstractVersionEventSerializer<AbstractVersionEvent> {
    @Autowired
    public VersionEventSerializer(final RegisteredWebHookEventFactory registeredWebHookEventFactory, final SimpleVersionBeanFactory versionBeanFactory) {
        super(registeredWebHookEventFactory, versionBeanFactory);
    }
}
