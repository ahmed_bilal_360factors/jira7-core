package com.atlassian.jira.plugins.webhooks.url;

import com.atlassian.jira.event.AbstractProjectEvent;
import com.atlassian.jira.project.Project;
import com.atlassian.webhooks.api.document.ProvidesUrlVariables;
import com.atlassian.webhooks.spi.UriVariablesProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class ProjectEventVariablesProvider implements UriVariablesProvider<AbstractProjectEvent> {
    private final ProjectContextSerializer contextSerializer;

    @Autowired
    public ProjectEventVariablesProvider(final ProjectContextSerializer contextSerializer) {
        this.contextSerializer = contextSerializer;
    }

    @ProvidesUrlVariables({"project.key", "project.id"})
    @Override
    public Map<String, Object> uriVariables(final AbstractProjectEvent projectEvent) {
        final Project project = projectEvent.getProject();
        return contextSerializer.getContext(project);
    }
}
