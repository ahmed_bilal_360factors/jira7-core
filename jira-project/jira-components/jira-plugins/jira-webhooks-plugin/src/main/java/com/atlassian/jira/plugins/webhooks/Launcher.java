package com.atlassian.jira.plugins.webhooks;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.extension.JiraStartedEvent;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.webhooks.api.events.WebHookClearListenerCacheEvent;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * This handles all of the "startup" stuff for the webhooks plugin.
 */
public class Launcher implements LifecycleAware {
    private final EventPublisher eventPublisher;

    public Launcher(EventPublisher eventPublisher) {
        this.eventPublisher = checkNotNull(eventPublisher);
    }

    /**
     * First event in the lifecycle: Spring context comes up
     */
    @PostConstruct
    private void onSpringContextStarted() {
        // here is where we place any startup code needed for the plugin but NOT anything AO since its not fully up
        //
        // yet
        //
        eventPublisher.register(this);
    }

    /**
     * Second event in the lifecycle: System runs (this comes after the plugin framework events)
     */
    @Override
    public final void onStart() {
    }

    @Override
    public void onStop() {
    }

    @PreDestroy
    private void onSpringContextStopped() {
        eventPublisher.unregister(this);
    }

    @SuppressWarnings("unused")
    @EventListener
    public void onClearCacheEvent(final ClearCacheEvent clearCacheEvent) {
        eventPublisher.publish(new WebHookClearListenerCacheEvent());
    }

    @SuppressWarnings("unused")
    @EventListener
    public void onJiraStartedEvent(final JiraStartedEvent jiraStartedEvent) {
        eventPublisher.publish(new WebHookClearListenerCacheEvent());
    }

}
