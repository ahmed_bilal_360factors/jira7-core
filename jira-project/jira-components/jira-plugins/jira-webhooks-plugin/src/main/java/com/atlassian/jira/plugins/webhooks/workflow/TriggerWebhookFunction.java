package com.atlassian.jira.plugins.webhooks.workflow;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Option;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.plugins.webhooks.listener.JqlFilterStoreUtils;
import com.atlassian.jira.plugins.webhooks.matcher.JqlEventMatcher;
import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.api.register.listener.WebHookListenerService;
import com.google.common.base.Optional;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Map;

/**
 * DO NOT rename!
 */
public class TriggerWebhookFunction extends AbstractJiraFunctionProvider {
    private final I18nResolver i18n;
    private final TransitionMapper transitionMapper;
    private final EventPublisher eventPublisher;
    private final JqlEventMatcher jqlEventMatcher;
    private final WebHookListenerService webHookListenerService;

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    public TriggerWebhookFunction(I18nResolver i18n, TransitionMapper transitionMapper,
                                  EventPublisher eventPublisher, JqlEventMatcher jqlEventMatcher, WebHookListenerService webHookListenerService) {
        this.i18n = i18n;
        this.transitionMapper = transitionMapper;
        this.eventPublisher = eventPublisher;
        this.jqlEventMatcher = jqlEventMatcher;
        this.webHookListenerService = webHookListenerService;
    }

    @Override
    public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
        final String webHookId = (String) args.get(TriggerWebHookFunctionPluginFactory.TARGET_FIELD_ID);
        final Option<PersistentWebHookListener> webHookOption = webHookListenerService.getWebHookListener(Integer.parseInt(webHookId));

        if (webHookOption.isEmpty()) {
            logger.error(i18n.getText("webhooks.postfunction.deleted"));
            return;
        }

        final PersistentWebHookListener listener = webHookOption.get();
        final MutableIssue issue = getIssue(transientVars);

        if (issue == null || !issue.isCreated()) {
            logger.error("Transition cannot be sent to webhook as an issue has not been created yet.");
            return;
        }

        final Optional<String> filter = Optional.fromNullable(JqlFilterStoreUtils.getJqlFilter(listener));
        if (listener.isEnabled() && jqlEventMatcher.matchJql(filter.orNull(), issue)) {
            final Map<String, Object> data = transitionMapper.toMap(transientVars, getCaller(transientVars, args), issue);
            try {
                boolean excludeIssueDetails = listener.isExcludeBody();
                final String message = excludeIssueDetails ? "" : new TransitionSerializer(data).getJson();
                eventPublisher.publish(new WebHookPostFunctionEvent(listener.getId().get(), message, issue));
            } catch (IOException e) {
                logger.error("Error serializing post-function context", e);
            }
        }
    }

}
