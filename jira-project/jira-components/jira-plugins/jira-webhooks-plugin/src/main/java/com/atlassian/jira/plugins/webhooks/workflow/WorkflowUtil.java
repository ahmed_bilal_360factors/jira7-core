package com.atlassian.jira.plugins.webhooks.workflow;


import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import com.opensymphony.workflow.loader.ActionDescriptor;
import com.opensymphony.workflow.loader.FunctionDescriptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

@Component
public class WorkflowUtil {
    private final WorkflowManager workflowManager;

    @Autowired
    public WorkflowUtil(WorkflowManager workflowManager) {
        this.workflowManager = workflowManager;
    }

    public Set<Map<String, String>> getTransitionLinkedToWebHookListener(final int id) {
        final Set<Map<String, String>> transitions = Sets.newHashSet();
        final Collection<JiraWorkflow> workflows = workflowManager.getWorkflows();
        for (JiraWorkflow workflow : workflows) {
            transitions.addAll(getTransitions(id, workflow));
        }
        return transitions;
    }

    private Set<Map<String, String>> getTransitions(final int webHookListenerId, JiraWorkflow workflow) {
        Set<Map<String, String>> transitions = Sets.newHashSet();
        final Map<ActionDescriptor, Collection<FunctionDescriptor>> actionToPostfunctionsMap = workflowManager.getPostFunctionsForWorkflow(workflow);
        for (Map.Entry<ActionDescriptor, Collection<FunctionDescriptor>> entry : actionToPostfunctionsMap.entrySet()) {
            if (isWebHookLinked(webHookListenerId, entry.getValue())) {
                transitions.add(ImmutableMap.of("workflow", workflow.getName(), "transition", entry.getKey().getName()));
            }
        }
        return transitions;
    }

    private boolean isWebHookLinked(final int id, Collection<FunctionDescriptor> functions) {
        return Iterables.any(functions, new Predicate<FunctionDescriptor>() {
            @Override
            public boolean apply(FunctionDescriptor functionDescriptor) {
                return isTriggerWebHookFunction(functionDescriptor) && isWebHookLinkedTo(functionDescriptor);
            }

            private boolean isWebHookLinkedTo(FunctionDescriptor functionDescriptor) {
                return functionDescriptor.getArgs()
                        .get(TriggerWebHookFunctionPluginFactory.TARGET_FIELD_ID).equals(String.valueOf(id));
            }

            private boolean isTriggerWebHookFunction(FunctionDescriptor functionDescriptor) {
                return TriggerWebhookFunction.class.getName().equals(functionDescriptor.getArgs().get("class.name"));
            }
        });
    }
}
