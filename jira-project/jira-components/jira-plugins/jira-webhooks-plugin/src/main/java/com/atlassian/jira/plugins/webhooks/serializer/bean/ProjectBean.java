package com.atlassian.jira.plugins.webhooks.serializer.bean;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Map;

public class ProjectBean {
    @JsonProperty
    private String self;

    @JsonProperty
    private Long id;

    @JsonProperty
    private String key;

    @JsonProperty
    private String name;

    @JsonProperty
    private Map<String, String> avatarUrls;

    @JsonProperty
    private ProjectCategoryBean projectCategory;

    @JsonProperty
    private UserBean projectLead;

    @JsonProperty
    private String assigneeType;

    public ProjectBean(final String self, final Long id, final String key, final String name, final Map<String, String> avatarUrls, final ProjectCategoryBean projectCategory, final UserBean projectLead, final String assigneeType) {
        this.self = self;
        this.id = id;
        this.key = key;
        this.name = name;
        this.avatarUrls = avatarUrls;
        this.projectCategory = projectCategory;
        this.projectLead = projectLead;
        this.assigneeType = assigneeType;
    }

    public String getSelf() {
        return self;
    }

    public Long getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public String getName() {
        return name;
    }

    public Map<String, String> getAvatarUrls() {
        return avatarUrls;
    }

    public ProjectCategoryBean getProjectCategory() {
        return projectCategory;
    }

    public UserBean getProjectLead() {
        return projectLead;
    }

    public String getAssigneeType() {
        return assigneeType;
    }
}
