package com.atlassian.jira.plugins.webhooks.serializer.comment;

import com.atlassian.jira.event.comment.CommentCreatedEvent;
import com.atlassian.jira.plugins.webhooks.registration.RegisteredWebHookEventFactory;
import com.atlassian.jira.plugins.webhooks.serializer.bean.CommentBeanFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CommentCreatedSerializer extends AbstractCommentSerializer<CommentCreatedEvent> {

    @Autowired
    public CommentCreatedSerializer(final RegisteredWebHookEventFactory registeredWebHookEventFactory,
            final CommentBeanFactory commentBeanFactory) {
        super(registeredWebHookEventFactory, commentBeanFactory);
    }

    @Override
    protected Long getTimestamp(final CommentCreatedEvent event) {
        return event.getComment().getCreated().getTime();
    }
}
