package com.atlassian.jira.plugins.webhooks.workflow;

import com.atlassian.fugue.Option;
import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginFunctionFactory;
import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.api.register.listener.WebHookListenerService;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.FunctionDescriptor;
import org.apache.log4j.Logger;

import java.util.Map;

/**
 *
 */
public class TriggerWebHookFunctionPluginFactory extends AbstractWorkflowPluginFactory
        implements WorkflowPluginFunctionFactory {

    public static final String PARAM_FIELD_ID = "webhookId";
    public static final String TARGET_FIELD_ID = "field.webhookId";
    public static final String WEBHOOKS = "webhooks";
    public static final String SELECTED_WEBHOOK = "selectedWebhook";
    public static final String WEBHOOK_NAME = "webhookName";
    private static final Logger LOG = Logger.getLogger(TriggerWebHookFunctionPluginFactory.class.getName());

    private final WebHookListenerService webHookCustomerService;


    public TriggerWebHookFunctionPluginFactory(final WebHookListenerService webHookCustomerService) {
        this.webHookCustomerService = webHookCustomerService;
    }

    @Override
    protected void getVelocityParamsForInput(Map<String, Object> velocityParams) {
        velocityParams.put(WEBHOOKS, Lists.newArrayList(webHookCustomerService.getAllWebHookListeners()));
    }

    @Override
    protected void getVelocityParamsForEdit(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {
        getVelocityParamsForInput(velocityParams);
        FunctionDescriptor functionDescriptor = (FunctionDescriptor) descriptor;
        final String webHookId = (String) functionDescriptor.getArgs().get(TARGET_FIELD_ID);
        try {
            velocityParams.put(PARAM_FIELD_ID, Integer.parseInt(webHookId));
        } catch (NumberFormatException e) {
            LOG.debug("Webhook id is not a valid integer.", e);
            velocityParams.put(PARAM_FIELD_ID, -1);
        }
    }

    @Override
    protected void getVelocityParamsForView(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {
        FunctionDescriptor functionDescriptor = (FunctionDescriptor) descriptor;
        String webHookId = (String) functionDescriptor.getArgs().get(TARGET_FIELD_ID);
        try {
            final Option<PersistentWebHookListener> webhook = webHookCustomerService.getWebHookListener(Integer.parseInt(webHookId));
            if (webhook.isDefined()) {
                velocityParams.put(WEBHOOK_NAME, webhook.get().getName());
            }
        } catch (NumberFormatException e) {
            LOG.debug("Webhook id is not a valid integer.", e);
            velocityParams.put(WEBHOOK_NAME, "Webhook not found");
        }
    }

    @Override
    public Map<String, ?> getDescriptorParams(Map<String, Object> formParams) {
        final String webhookId = extractSingleParam(formParams, SELECTED_WEBHOOK);
        return ImmutableMap.of(TARGET_FIELD_ID, webhookId);
    }
}
