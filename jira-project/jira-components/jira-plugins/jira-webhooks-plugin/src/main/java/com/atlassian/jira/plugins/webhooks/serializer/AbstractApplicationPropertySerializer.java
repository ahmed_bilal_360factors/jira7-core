package com.atlassian.jira.plugins.webhooks.serializer;

import com.atlassian.jira.event.property.AbstractApplicationPropertySetEvent;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.plugins.webhooks.registration.RegisteredWebHookEventFactory;
import com.atlassian.jira.plugins.webhooks.serializer.bean.ApplicationPropertyBean;
import com.google.common.collect.ImmutableMap;

public abstract class AbstractApplicationPropertySerializer<T extends AbstractApplicationPropertySetEvent> extends AbstractJiraEventSerializer<T> {
    private final JiraBaseUrls urls;

    protected AbstractApplicationPropertySerializer(final RegisteredWebHookEventFactory registeredWebHookEventFactory, final JiraBaseUrls urls) {
        super(registeredWebHookEventFactory);
        this.urls = urls;
    }

    @Override
    protected Long getTimestamp(final T event) {
        return event.getTime().getTime();
    }

    @Override
    protected ImmutableMap.Builder<String, Object> putFields(final T event, final ImmutableMap.Builder<String, Object> defaultJson) {
        String url = urls.restApi2BaseUrl() + "configuration";

        ApplicationPropertyBean propertyBean = new ApplicationPropertyBean(url, event.getPropertyKey(), getEventValue(event));
        defaultJson.put("property", propertyBean);

        return defaultJson;
    }

    protected abstract String getEventValue(final T event);

}
