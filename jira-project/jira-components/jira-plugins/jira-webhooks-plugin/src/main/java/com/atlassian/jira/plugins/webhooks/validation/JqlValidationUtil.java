package com.atlassian.jira.plugins.webhooks.validation;


import com.atlassian.fugue.Either;
import com.atlassian.jira.jql.parser.JqlParseException;
import com.atlassian.jira.jql.parser.JqlQueryParser;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.message.Message;
import com.atlassian.webhooks.api.util.ErrorMessage;
import com.atlassian.webhooks.api.util.MessageCollection;
import com.google.common.base.Optional;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class JqlValidationUtil {
    private final JqlQueryParser jqlQueryParser;
    private final I18nResolver i18n;

    @Autowired
    public JqlValidationUtil(final JqlQueryParser jqlQueryParser, final I18nResolver i18n) {
        this.jqlQueryParser = jqlQueryParser;
        this.i18n = i18n;
    }

    public void validateFilter(final MessageCollection.Builder errorCollectionBuilder, final Optional<String> filter) {
        if (filter.isPresent() && StringUtils.isNotBlank(filter.get())) {
            final String jql = filter.get();
            final Either<JqlParseException, String> jqlValid = isJqlValid(jql);
            if (!jqlValid.isRight()) {
                final Message errorMessage = new ErrorMessage("jql", i18n.getText("webhooks.invalid.jql", jql, jqlValid.left().get()));
                errorCollectionBuilder.addMessage(errorMessage, MessageCollection.Reason.VALIDATION_FAILED);
            }
        }
    }

    public Either<JqlParseException, String> isJqlValid(final String jql) {
        try {
            jqlQueryParser.parseQuery(jql);
            return Either.right(jql);
        } catch (JqlParseException e) {
            return Either.left(e);
        }
    }
}
