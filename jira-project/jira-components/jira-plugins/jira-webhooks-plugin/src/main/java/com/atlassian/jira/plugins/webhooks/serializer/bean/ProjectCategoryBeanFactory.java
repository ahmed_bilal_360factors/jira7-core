package com.atlassian.jira.plugins.webhooks.serializer.bean;

import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.project.ProjectCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;

@Component
public class ProjectCategoryBeanFactory {
    private final JiraBaseUrls urls;

    @Autowired
    public ProjectCategoryBeanFactory(final JiraBaseUrls urls) {
        this.urls = urls;
    }

    public ProjectCategoryBean createBean(@Nullable final ProjectCategory projectCategory) {
        if (projectCategory == null) {
            return null;
        }

        return new ProjectCategoryBean(createSelfLink(projectCategory),
                projectCategory.getId(),
                projectCategory.getName(),
                projectCategory.getDescription()
        );
    }

    private String createSelfLink(ProjectCategory projectCategory) {
        return urls.restApi2BaseUrl() + "projectCategory/" + projectCategory.getId().toString();
    }
}