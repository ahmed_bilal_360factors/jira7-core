package com.atlassian.jira.plugins.webhooks.serializer;

import com.atlassian.jira.event.AbstractProjectEvent;
import com.atlassian.jira.plugins.webhooks.registration.RegisteredWebHookEventFactory;
import com.atlassian.jira.plugins.webhooks.serializer.bean.ProjectBean;
import com.atlassian.jira.plugins.webhooks.serializer.bean.ProjectBeanFactory;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProjectEventSerializer extends AbstractJiraEventSerializer<AbstractProjectEvent> {
    private final ProjectBeanFactory projectBeanFactory;

    @Autowired
    public ProjectEventSerializer(final RegisteredWebHookEventFactory registeredWebHookEventFactory, final ProjectBeanFactory projectBeanFactory) {
        super(registeredWebHookEventFactory);
        this.projectBeanFactory = projectBeanFactory;
    }

    @Override
    protected Long getTimestamp(AbstractProjectEvent event) {
        return event.getTime().getTime();
    }

    @Override
    protected ImmutableMap.Builder<String, Object> putFields(AbstractProjectEvent event, ImmutableMap.Builder<String, Object> defaultJson) {
        ProjectBean projectBean = projectBeanFactory.createBean(event.getProject());
        defaultJson.put("project", projectBean);
        return defaultJson;
    }
}
