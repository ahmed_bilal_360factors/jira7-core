package com.atlassian.jira.plugins.webhooks.registration;

import com.atlassian.crowd.event.user.UserCreatedEvent;
import com.atlassian.crowd.event.user.UserDeletedEvent;
import com.atlassian.crowd.event.user.UserUpdatedEvent;
import com.atlassian.jira.event.AbstractProjectEvent;
import com.atlassian.jira.event.comment.CommentCreatedEvent;
import com.atlassian.jira.event.comment.CommentDeletedEvent;
import com.atlassian.jira.event.comment.CommentEvent;
import com.atlassian.jira.event.comment.CommentUpdatedEvent;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.issue.IssuePreDeleteEvent;
import com.atlassian.jira.event.issue.IssueRelatedEvent;
import com.atlassian.jira.event.project.AbstractVersionEvent;
import com.atlassian.jira.event.project.VersionDeleteEvent;
import com.atlassian.jira.event.project.VersionMergeEvent;
import com.atlassian.jira.event.property.BooleanApplicationPropertySetEvent;
import com.atlassian.jira.event.property.StringApplicationPropertySetEvent;
import com.atlassian.jira.event.worklog.WorklogCreatedEvent;
import com.atlassian.jira.event.worklog.WorklogDeletedEvent;
import com.atlassian.jira.event.worklog.WorklogUpdatedEvent;
import com.atlassian.jira.plugins.webhooks.serializer.BooleanApplicationPropertySerializer;
import com.atlassian.jira.plugins.webhooks.serializer.IssueEventSerializer;
import com.atlassian.jira.plugins.webhooks.serializer.IssuePreDeleteEventSerializer;
import com.atlassian.jira.plugins.webhooks.serializer.ProjectEventSerializer;
import com.atlassian.jira.plugins.webhooks.serializer.StringApplicationPropertySerializer;
import com.atlassian.jira.plugins.webhooks.serializer.UserCreatedEventSerializer;
import com.atlassian.jira.plugins.webhooks.serializer.UserDeletedEventSerializer;
import com.atlassian.jira.plugins.webhooks.serializer.UserUpdatedEventSerializer;
import com.atlassian.jira.plugins.webhooks.serializer.VersionDeletedEventSerializer;
import com.atlassian.jira.plugins.webhooks.serializer.VersionEventSerializer;
import com.atlassian.jira.plugins.webhooks.serializer.VersionMergeEventSerializer;
import com.atlassian.jira.plugins.webhooks.serializer.comment.CommentCreatedSerializer;
import com.atlassian.jira.plugins.webhooks.serializer.comment.CommentDeletedSerializer;
import com.atlassian.jira.plugins.webhooks.serializer.comment.CommentUpdatedSerializer;
import com.atlassian.jira.plugins.webhooks.serializer.worklog.WorklogCreatedSerializer;
import com.atlassian.jira.plugins.webhooks.serializer.worklog.WorklogDeletedSerializer;
import com.atlassian.jira.plugins.webhooks.serializer.worklog.WorklogUpdatedSerializer;
import com.atlassian.jira.plugins.webhooks.ui.JiraTransitionsPanel;
import com.atlassian.jira.plugins.webhooks.url.CommentEventVariablesProvider;
import com.atlassian.jira.plugins.webhooks.url.IssueRelatedEventVariablesProvider;
import com.atlassian.jira.plugins.webhooks.url.ProjectEventVariablesProvider;
import com.atlassian.jira.plugins.webhooks.url.UserCreatedEventVariablesProvider;
import com.atlassian.jira.plugins.webhooks.url.UserDeletedEventVariablesProvider;
import com.atlassian.jira.plugins.webhooks.url.UserUpdatedEventVariablesProvider;
import com.atlassian.jira.plugins.webhooks.url.VersionEventVariablesProvider;
import com.atlassian.jira.plugins.webhooks.url.VersionMergeEventVariablesProvider;
import com.atlassian.jira.plugins.webhooks.url.WebHookPostFunctionEventVariablesProvider;
import com.atlassian.jira.plugins.webhooks.validation.JiraWebHookListenerActionValidator;
import com.atlassian.jira.plugins.webhooks.workflow.WebHookPostFunctionEvent;
import com.atlassian.webhooks.api.register.WebHookEventSection;
import com.atlassian.webhooks.api.register.WebHookPluginRegistration;
import com.atlassian.webhooks.api.register.WebHookPluginRegistrationBuilder;
import com.atlassian.webhooks.spi.WebHookPluginRegistrationFactory;

public class JiraWebHookRegistrationFactory implements WebHookPluginRegistrationFactory {
    private final IssueRelatedEventVariablesProvider issueRelatedEventVariablesProvider;
    private final ProjectEventVariablesProvider projectEventVariablesProvider;
    private final WebHookPostFunctionEventVariablesProvider webHookPostFunctionEventVariablesProvider;
    private final JiraCloudCondition jiraCloudCondition;
    private final IssueEventSerializer issueEventSerializer;
    private final IssuePreDeleteEventSerializer preDeleteEventSerializer;
    private final VersionEventSerializer versionEventSerializer;
    private final ProjectEventSerializer projectEventSerializer;
    private final BooleanApplicationPropertySerializer booleanApplicationPropertySerializer;
    private final StringApplicationPropertySerializer stringApplicationPropertySerializer;
    private final RegisteredWebHookEventFactory registeredWebHookEventFactory;
    private final JiraTransitionsPanel jiraTransitionsPanel;
    private final UserCreatedEventSerializer userCreatedEventSerializer;
    private final UserDeletedEventSerializer userDeletedEventSerializer;
    private final UserUpdatedEventSerializer userUpdatedEventSerializer;
    private final UserCreatedEventVariablesProvider userCreatedEventVariablesProvider;
    private final UserDeletedEventVariablesProvider userDeletedEventVariablesProvider;
    private final UserUpdatedEventVariablesProvider userUpdatedEventVariablesProvider;
    private final JiraWebHookListenerActionValidator actionValidator;
    private final VersionMergeEventSerializer versionMergedEventSerializer;
    private final VersionDeletedEventSerializer versionDeletedEventSerializer;
    private final VersionEventVariablesProvider versionEventVariablesProvider;
    private final VersionMergeEventVariablesProvider versionMergeEventVariablesProvider;
    private final CommentEventVariablesProvider commentEventVariablesProvider;
    private final WorklogCreatedSerializer worklogCreatedSerializer;
    private final WorklogUpdatedSerializer worklogUpdatedSerializer;
    private final WorklogDeletedSerializer worklogDeletedSerializer;
    private final CommentCreatedSerializer commentCreatedSerializer;
    private final CommentDeletedSerializer commentDeletedSerializer;
    private final CommentUpdatedSerializer commentUpdatedSerializer;

    public JiraWebHookRegistrationFactory(final IssueRelatedEventVariablesProvider issueRelatedEventVariablesProvider,
            final ProjectEventVariablesProvider projectEventVariablesProvider,
            final WebHookPostFunctionEventVariablesProvider webHookPostFunctionEventVariablesProvider,
            final JiraCloudCondition jiraCloudCondition,
            final IssueEventSerializer issueEventSerializer,
            final IssuePreDeleteEventSerializer preDeleteEventSerializer,
            final VersionEventSerializer versionEventSerializer,
            final ProjectEventSerializer projectEventSerializer,
            final BooleanApplicationPropertySerializer booleanApplicationPropertySerializer,
            final StringApplicationPropertySerializer stringApplicationPropertySerializer,
            final RegisteredWebHookEventFactory registeredWebHookEventFactory,
            final JiraTransitionsPanel jiraTransitionsPanel,
            final UserCreatedEventSerializer userCreatedEventSerializer,
            final UserDeletedEventSerializer userDeletedEventSerializer,
            final UserUpdatedEventSerializer userUpdatedEventSerializer,
            final UserCreatedEventVariablesProvider userCreatedEventVariablesProvider,
            final UserDeletedEventVariablesProvider userDeletedEventVariablesProvider,
            final UserUpdatedEventVariablesProvider userUpdatedEventVariablesProvider,
            final JiraWebHookListenerActionValidator actionValidator,
            final VersionMergeEventSerializer versionMergedEventSerializer,
            final VersionDeletedEventSerializer versionDeletedEventSerializer,
            final VersionEventVariablesProvider versionEventVariablesProvider,
            final VersionMergeEventVariablesProvider versionMergeEventVariablesProvider,
            final CommentEventVariablesProvider commentEventVariablesProvider,
            final WorklogCreatedSerializer worklogCreatedSerializer,
            final WorklogUpdatedSerializer worklogUpdatedSerializer,
            final WorklogDeletedSerializer worklogDeletedSerializer,
            final CommentCreatedSerializer commentCreatedSerializer,
            final CommentDeletedSerializer commentDeletedSerializer,
            final CommentUpdatedSerializer commentUpdatedSerializer) {
        this.issueRelatedEventVariablesProvider = issueRelatedEventVariablesProvider;
        this.projectEventVariablesProvider = projectEventVariablesProvider;
        this.webHookPostFunctionEventVariablesProvider = webHookPostFunctionEventVariablesProvider;
        this.jiraCloudCondition = jiraCloudCondition;
        this.issueEventSerializer = issueEventSerializer;
        this.preDeleteEventSerializer = preDeleteEventSerializer;
        this.versionEventSerializer = versionEventSerializer;
        this.projectEventSerializer = projectEventSerializer;
        this.booleanApplicationPropertySerializer = booleanApplicationPropertySerializer;
        this.stringApplicationPropertySerializer = stringApplicationPropertySerializer;
        this.registeredWebHookEventFactory = registeredWebHookEventFactory;
        this.jiraTransitionsPanel = jiraTransitionsPanel;
        this.userCreatedEventSerializer = userCreatedEventSerializer;
        this.userDeletedEventSerializer = userDeletedEventSerializer;
        this.userUpdatedEventSerializer = userUpdatedEventSerializer;
        this.userCreatedEventVariablesProvider = userCreatedEventVariablesProvider;
        this.userDeletedEventVariablesProvider = userDeletedEventVariablesProvider;
        this.userUpdatedEventVariablesProvider = userUpdatedEventVariablesProvider;
        this.actionValidator = actionValidator;
        this.versionMergedEventSerializer = versionMergedEventSerializer;
        this.versionDeletedEventSerializer = versionDeletedEventSerializer;
        this.versionEventVariablesProvider = versionEventVariablesProvider;
        this.versionMergeEventVariablesProvider = versionMergeEventVariablesProvider;
        this.commentEventVariablesProvider = commentEventVariablesProvider;
        this.worklogCreatedSerializer = worklogCreatedSerializer;
        this.worklogUpdatedSerializer = worklogUpdatedSerializer;
        this.worklogDeletedSerializer = worklogDeletedSerializer;
        this.commentCreatedSerializer = commentCreatedSerializer;
        this.commentDeletedSerializer = commentDeletedSerializer;
        this.commentUpdatedSerializer = commentUpdatedSerializer;
    }

    @Override
    public WebHookPluginRegistration createPluginRegistration() {
        WebHookPluginRegistrationBuilder registration = WebHookPluginRegistration.builder()
                .variablesProvider(IssueRelatedEvent.class, issueRelatedEventVariablesProvider)
                .variablesProvider(AbstractProjectEvent.class, projectEventVariablesProvider)
                .variablesProvider(WebHookPostFunctionEvent.class, webHookPostFunctionEventVariablesProvider)
                .variablesProvider(UserCreatedEvent.class, userCreatedEventVariablesProvider)
                .variablesProvider(UserDeletedEvent.class, userDeletedEventVariablesProvider)
                .variablesProvider(UserUpdatedEvent.class, userUpdatedEventVariablesProvider)
                .variablesProvider(AbstractVersionEvent.class, versionEventVariablesProvider)
                .variablesProvider(VersionMergeEvent.class, versionMergeEventVariablesProvider)
                .variablesProvider(CommentEvent.class, commentEventVariablesProvider)
                .eventSerializer(IssueEvent.class, issueEventSerializer)
                .eventSerializer(IssuePreDeleteEvent.class, preDeleteEventSerializer)
                .eventSerializer(AbstractVersionEvent.class, versionEventSerializer)
                .eventSerializer(VersionMergeEvent.class, versionMergedEventSerializer)
                .eventSerializer(VersionDeleteEvent.class, versionDeletedEventSerializer)
                .eventSerializer(AbstractProjectEvent.class, projectEventSerializer)
                .eventSerializer(WebHookPostFunctionEvent.class, new WebHookPostFunctionEvent.FunctionEventSerializerFactory())
                .eventSerializer(UserCreatedEvent.class, userCreatedEventSerializer)
                .eventSerializer(UserDeletedEvent.class, userDeletedEventSerializer)
                .eventSerializer(UserUpdatedEvent.class, userUpdatedEventSerializer)
                .eventSerializer(BooleanApplicationPropertySetEvent.class, booleanApplicationPropertySerializer)
                .eventSerializer(StringApplicationPropertySetEvent.class, stringApplicationPropertySerializer)
                // in case you wonder, what is the difference between these 3, they return different timestamps.
                // WorklogCreatedSerializer returns WorklogCreated time, WorklogUpdated returns updated time (different clumns in db)
                // and WorklogDeletedSerializer returns current time.
                .eventSerializer(WorklogCreatedEvent.class, worklogCreatedSerializer)
                .eventSerializer(WorklogUpdatedEvent.class, worklogUpdatedSerializer)
                .eventSerializer(WorklogDeletedEvent.class, worklogDeletedSerializer)
                // The same pattern as with Worklogs is applied to comments.
                .eventSerializer(CommentCreatedEvent.class, commentCreatedSerializer)
                .eventSerializer(CommentDeletedEvent.class, commentDeletedSerializer)
                .eventSerializer(CommentUpdatedEvent.class, commentUpdatedSerializer)
                .cloudCondition(jiraCloudCondition)
                .addValidator(actionValidator)
                .addHtmlPanel(jiraTransitionsPanel);

        for (final WebHookEventSection section : registeredWebHookEventFactory.getWebHookEventSections()) {
            registration.addWebHookSection(section);
        }

        return registration.build();
    }
}
