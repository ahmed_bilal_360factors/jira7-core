package com.atlassian.jira.plugins.webhooks.url;

import com.atlassian.jira.event.issue.IssueRelatedEvent;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.project.Project;
import com.atlassian.webhooks.api.document.ProvidesUrlVariables;
import com.atlassian.webhooks.spi.UriVariablesProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Resolves the context for issue related events.
 */
@Component
public class IssueRelatedEventVariablesProvider implements UriVariablesProvider<IssueRelatedEvent> {
    private final IssueAndProjectContextSerializer contextSerializer;

    @Autowired
    public IssueRelatedEventVariablesProvider(final IssueAndProjectContextSerializer contextSerializer) {
        this.contextSerializer = contextSerializer;
    }

    @ProvidesUrlVariables({"project.key", "project.id", "issue.key", "issue.id"})
    @Override
    public Map<String, Object> uriVariables(final IssueRelatedEvent issueRelatedEvent) {
        final Issue issue = issueRelatedEvent.getIssue();
        final Project project = issue.getProjectObject();
        return contextSerializer.getContext(project, issue);
    }
}
