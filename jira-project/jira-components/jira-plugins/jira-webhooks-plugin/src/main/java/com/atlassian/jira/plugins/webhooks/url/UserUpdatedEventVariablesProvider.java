package com.atlassian.jira.plugins.webhooks.url;

import com.atlassian.crowd.event.user.UserUpdatedEvent;
import com.atlassian.jira.plugins.webhooks.serializer.bean.UserBean;
import com.atlassian.jira.plugins.webhooks.serializer.bean.UserBeanFactory;
import com.atlassian.jira.user.ApplicationUsers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public final class UserUpdatedEventVariablesProvider extends UserEventVariablesProvider<UserUpdatedEvent> {
    private final UserBeanFactory userBeanFactory;

    @Autowired
    public UserUpdatedEventVariablesProvider(final UserBeanFactory userBeanFactory) {
        this.userBeanFactory = userBeanFactory;
    }

    @Override
    protected UserVariables getUserVariables(final UserUpdatedEvent event) {
        UserBean user = userBeanFactory.createBean(ApplicationUsers.from(event.getUser()));
        return new UserVariables(user.getName(), user.getKey());
    }
}
