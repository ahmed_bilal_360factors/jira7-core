package com.atlassian.jira.plugins.webhooks.url;

import com.atlassian.jira.project.Project;
import com.google.common.collect.ImmutableMap;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class ProjectContextSerializer {
    public Map<String, Object> getContext(final Project project) {
        return ImmutableMap.<String, Object>of("project", ImmutableMap.of(
                "key", project.getKey(), "id", project.getId()
        ));
    }
}
