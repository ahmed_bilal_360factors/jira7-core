package com.atlassian.jira.plugins.webhooks.ao;

import net.java.ao.Entity;
import net.java.ao.Implementation;
import net.java.ao.schema.Ignore;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.StringLength;

import java.util.Date;

/**
 * Definition of WebHookListener stored as AO.
 */
@SuppressWarnings("UnusedDeclaration")
@Implementation(WebhookDaoImpl.class)
public interface WebhookDao extends Entity {
    @NotNull
    String getLastUpdatedUser();

    void setLastUpdatedUser(String username);

    @NotNull
    @StringLength(StringLength.UNLIMITED)
    String getUrl();

    void setUrl(String url);

    @NotNull
    Date getLastUpdated();

    void setLastUpdated(Date updated);

    @NotNull
    @StringLength(StringLength.UNLIMITED)
    String getName();

    void setName(String name);

    @StringLength(StringLength.UNLIMITED)
    String getFilter();

    void setFilter(String filter);

    // Was this created via REST, the UI, or something else?
    @NotNull
    String getRegistrationMethod();

    void setRegistrationMethod(String method);

    boolean getExcludeIssueDetails();

    void setExcludeIssueDetails(boolean excludeIssueDetails);

    /* Unfortunately, according to https://answers.atlassian.com/questions/45149/activeobjects-jira
     * ActiveObjects can do one-to-many mappings of primitives, so we need to wrap it in another entity.
     * That seems like overkill. On top of that, attempting to do that resulted in exceptions from ActiveObject's
     * TypeMatter suggesting some sort of regression.
     *
     * Instead we will handle this ourselves.
     */
    @StringLength(StringLength.UNLIMITED)
    String getEncodedEvents();

    void setEncodedEvents(String events);

    @StringLength(StringLength.UNLIMITED)
    String getParameters();

    void setParameters(String events);

    boolean isEnabled();

    void setEnabled(boolean enabled);

    @Ignore
    String[] getEvents();

    @Ignore
    void setEvents(String... events);
}