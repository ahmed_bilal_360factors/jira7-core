package com.atlassian.jira.plugins.webhooks.url;

import java.util.Map;

import com.atlassian.jira.event.comment.CommentEvent;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.webhooks.api.document.ProvidesUrlVariables;
import com.atlassian.webhooks.spi.UriVariablesProvider;

import com.google.common.collect.ImmutableMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CommentEventVariablesProvider implements UriVariablesProvider<CommentEvent> {

    private final IssueAndProjectContextSerializer issueAndProjectContextSerializer;

    @Autowired
    public CommentEventVariablesProvider(final IssueAndProjectContextSerializer issueAndProjectContextSerializer) {
        this.issueAndProjectContextSerializer = issueAndProjectContextSerializer;
    }

    @Override
    @ProvidesUrlVariables ({"project.key", "project.id", "issue.id", "issue.key", "comment.id"})
    public Map<String, Object> uriVariables(final CommentEvent commentEvent) {
        final Comment comment = commentEvent.getComment();
        final Map<String, Object> issueAndProjectContextSerializerContext =
                issueAndProjectContextSerializer.getContext(comment.getIssue().getProjectObject(), comment.getIssue());
        return ImmutableMap.<String, Object>builder()
                .putAll(issueAndProjectContextSerializerContext)
                .put("comment", ImmutableMap.of("id", comment.getId()))
                .build();
    }
}
