package com.atlassian.jira.plugins.webhooks.registration;

import com.atlassian.crowd.event.user.AutoUserUpdatedEvent;
import com.atlassian.crowd.event.user.UserCreatedEvent;
import com.atlassian.crowd.event.user.UserDeletedEvent;
import com.atlassian.crowd.event.user.UserEditedEvent;
import com.atlassian.crowd.event.user.UserUpdatedEvent;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.event.ProjectCreatedEvent;
import com.atlassian.jira.event.ProjectDeletedEvent;
import com.atlassian.jira.event.ProjectUpdatedEvent;
import com.atlassian.jira.event.comment.CommentCreatedEvent;
import com.atlassian.jira.event.comment.CommentDeletedEvent;
import com.atlassian.jira.event.comment.CommentUpdatedEvent;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.issue.IssuePreDeleteEvent;
import com.atlassian.jira.event.issue.IssueRelatedEvent;
import com.atlassian.jira.event.project.VersionCreateEvent;
import com.atlassian.jira.event.project.VersionDeleteEvent;
import com.atlassian.jira.event.project.VersionMergeEvent;
import com.atlassian.jira.event.project.VersionMoveEvent;
import com.atlassian.jira.event.project.VersionReleaseEvent;
import com.atlassian.jira.event.project.VersionUnreleaseEvent;
import com.atlassian.jira.event.project.VersionUpdatedEvent;
import com.atlassian.jira.event.property.AbstractApplicationPropertySetEvent;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.event.worklog.WorklogCreatedEvent;
import com.atlassian.jira.event.worklog.WorklogDeletedEvent;
import com.atlassian.jira.event.worklog.WorklogUpdatedEvent;
import com.atlassian.jira.plugins.webhooks.matcher.ApplicationPropertyEventMatcher;
import com.atlassian.jira.plugins.webhooks.matcher.IssueEventIdMatcher;
import com.atlassian.jira.plugins.webhooks.matcher.JqlEventMatcher;
import com.atlassian.jira.plugins.webhooks.ui.JiraJQLPanel;
import com.atlassian.jira.plugins.webhooks.workflow.WebHookPostFunctionEvent;
import com.atlassian.webhooks.api.register.RegisteredWebHookEvent;
import com.atlassian.webhooks.api.register.WebHookEventGroup;
import com.atlassian.webhooks.api.register.WebHookEventSection;
import com.atlassian.webhooks.api.util.EventMatchers;
import com.atlassian.webhooks.spi.EventMatcher;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Set;

import static com.atlassian.jira.plugins.webhooks.matcher.EventTypeMatcher.narrowToSubclasses;
import static com.atlassian.webhooks.api.util.EventMatchers.and;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.transform;

@Component
public class RegisteredWebHookEventFactory {
    private final Iterable<RegisteredWebHookEvent> allEvents;
    private final Iterable<WebHookEventSection> webHookEventSections;

    private final JqlEventMatcher jqlEventMatcher;

    @Autowired
    public RegisteredWebHookEventFactory(final JiraJQLPanel jiraJQLPanel, JqlEventMatcher jqlEventMatcher) {
        this.jqlEventMatcher = jqlEventMatcher;

        WebHookEventGroup issueGroup = WebHookEventGroup.builder()
                .nameI18nKey("webhook.group.issue")
                .addEvent(event("jira:issue_created", "webhook.group.issue.created", IssueEvent.class, Sets.newHashSet(EventType.ISSUE_CREATED_ID)))
                .addEvent(event("jira:issue_updated", "webhook.group.issue.updated", IssueEvent.class, Sets.newHashSet(EventType.ISSUE_ASSIGNED_ID,
                        EventType.ISSUE_CLOSED_ID, EventType.ISSUE_REOPENED_ID, EventType.ISSUE_RESOLVED_ID,
                        EventType.ISSUE_COMMENT_EDITED_ID, EventType.ISSUE_COMMENTED_ID, EventType.ISSUE_MOVED_ID,
                        EventType.ISSUE_UPDATED_ID, EventType.ISSUE_WORKSTARTED_ID, EventType.ISSUE_WORKSTOPPED_ID,
                        EventType.ISSUE_GENERICEVENT_ID, EventType.ISSUE_COMMENT_DELETED_ID)))
                .addEvent(event("jira:issue_deleted", "webhook.group.issue.deleted", IssuePreDeleteEvent.class, Collections.<Long>emptySet()))
                .addEvent(event("jira:worklog_updated", "webhook.group.issue.worklog.changed", IssueEvent.class, Sets.newHashSet(EventType.ISSUE_WORKLOG_UPDATED_ID,
                        EventType.ISSUE_WORKLOG_DELETED_ID, EventType.ISSUE_WORKLOGGED_ID)))
                .build();

        WebHookEventGroup commentGroup = WebHookEventGroup.builder()
                .nameI18nKey("webhook.group.comment")
                .addEvent(event("comment_created", "webhook.group.comment.created", CommentCreatedEvent.class))
                .addEvent(event("comment_updated", "webhook.group.comment.updated", CommentUpdatedEvent.class))
                .addEvent(event("comment_deleted", "webhook.group.comment.deleted", CommentDeletedEvent.class))
                .build();

        WebHookEventGroup worklogGroup = WebHookEventGroup.builder()
                .nameI18nKey("webhook.group.worklog")
                .addEvent(event("worklog_created", "webhook.group.worklog.created", WorklogCreatedEvent.class))
                .addEvent(event("worklog_updated", "webhook.group.worklog.updated", WorklogUpdatedEvent.class))
                .addEvent(event("worklog_deleted", "webhook.group.worklog.deleted", WorklogDeletedEvent.class))
                .build();

        WebHookEventGroup versionGroup = WebHookEventGroup.builder()
                .nameI18nKey("webhook.group.version")
                .addEvent(event("jira:version_released", "webhook.group.version.released", VersionReleaseEvent.class))
                .addEvent(event("jira:version_unreleased", "webhook.group.version.unreleased", VersionUnreleaseEvent.class))
                .addEvent(event("jira:version_created", "webhook.group.version.created", VersionCreateEvent.class))
                .addEvent(event("jira:version_moved", "webhook.group.version.moved", VersionMoveEvent.class))
                .addEvent(event("jira:version_updated", "webhook.group.version.updated", VersionUpdatedEvent.class))
                .addEvent(event("jira:version_merged", "webhook.group.version.merged", VersionMergeEvent.class))
                .addEvent(event("jira:version_deleted", "webhook.group.version.deleted", VersionDeleteEvent.class))
                .build();

        WebHookEventGroup projectGroup = WebHookEventGroup.builder()
                .nameI18nKey("webhook.group.project")
                .addEvent(event("project_created", "webhook.group.project.created", ProjectCreatedEvent.class))
                .addEvent(event("project_updated", "webhook.group.project.updated", ProjectUpdatedEvent.class))
                .addEvent(event("project_deleted", "webhook.group.project.deleted", ProjectDeletedEvent.class))
                .build();

        WebHookEventGroup systemWebHooksGroup = WebHookEventGroup.builder()
                .addEvent(RegisteredWebHookEvent.withId("jira-webhook-post-function")
                        .firedWhen(WebHookPostFunctionEvent.class)
                        .isMatchedBy(new WebHookPostFunctionEvent.FunctionEventIdMatcher()))
                .build();

        WebHookEventSection issueRelatedSection = WebHookEventSection.section("issue-related-events-section")
                .nameI18nKey("webhook.section.issue-related")
                .descriptionI18nKey("webhook.section.issue-related.description")
                .addGroup(issueGroup)
                .addGroup(worklogGroup)
                .addGroup(commentGroup)
                .panel(jiraJQLPanel)
                .build();

        WebHookEventSection projectSection = WebHookEventSection.section("project-related-events-section")
                .nameI18nKey("webhook.section.project-related")
                .descriptionI18nKey("webhook.section.project-related.description")
                .addGroup(projectGroup)
                .addGroup(versionGroup)
                .build();

        WebHookEventSection userSection = WebHookEventSection.section("user-related-events-section")
                .nameI18nKey("webhook.section.user-related")
                .addGroup(WebHookEventGroup
                                .builder()
                                .nameI18nKey("webhook.group.user")
                                .addEvent(event("user_created", "webhook.group.user.created", UserCreatedEvent.class))
                                .addEvent(event("user_deleted", "webhook.group.user.deleted", UserDeletedEvent.class))
                                .addEvent(event("user_updated", "webhook.group.user.updated", UserUpdatedEvent.class, narrowToSubclasses(UserEditedEvent.class, AutoUserUpdatedEvent.class)))
                                .build()
                ).build();

        WebHookEventSection propertiesSection = WebHookEventSection.section("application-property-section")
                .nameI18nKey("webhook.section.application-properties")
                .addGroup(WebHookEventGroup
                        .builder()
                        .nameI18nKey("webhook.jira.features.status")
                        .addEvent(applicationPropertyEvent("option_voting_changed", "webhook.voting.option.changed", APKeys.JIRA_OPTION_VOTING))
                        .addEvent(applicationPropertyEvent("option_watching_changed", "webhook.watching.option.changed", APKeys.JIRA_OPTION_WATCHING))
                        .addEvent(applicationPropertyEvent("option_unassigned_issues_changed", "webhook.unassigned.issues.option.changed", APKeys.JIRA_OPTION_ALLOWUNASSIGNED))
                        .addEvent(applicationPropertyEvent("option_subtasks_changed", "webhook.subtasks.option.changed", APKeys.JIRA_OPTION_ALLOWSUBTASKS))
                        .addEvent(applicationPropertyEvent("option_attachments_changed", "webhook.attachments.option.changed", APKeys.JIRA_OPTION_ALLOWATTACHMENTS))
                        .addEvent(applicationPropertyEvent("option_issuelinks_changed", "webhook.issuelinks.option.changed", APKeys.JIRA_OPTION_ISSUELINKING))
                        .addEvent(applicationPropertyEvent("option_timetracking_changed", "webhook.timetracking.option.changed", APKeys.JIRA_OPTION_TIMETRACKING))
                        .build())
                .build();

        WebHookEventSection systemSection = WebHookEventSection.section("system-section")
                .addGroup(systemWebHooksGroup)
                .build();

        webHookEventSections = ImmutableList.<WebHookEventSection>builder()
                .add(issueRelatedSection)
                .add(projectSection)
                .add(userSection)
                .add(propertiesSection)
                .add(systemSection)
                .build();

        final Iterable<WebHookEventGroup> groups = concat(transform(webHookEventSections, new Function<WebHookEventSection, Set<WebHookEventGroup>>() {
            @Override
            public Set<WebHookEventGroup> apply(final WebHookEventSection webHookEventSection) {
                return webHookEventSection.getGroups();
            }
        }));
        allEvents = concat(transform(groups, new Function<WebHookEventGroup, Iterable<RegisteredWebHookEvent>>() {
            @Override
            public Iterable<RegisteredWebHookEvent> apply(final WebHookEventGroup group) {
                return group.getEvents();
            }
        }));

    }

    public Iterable<RegisteredWebHookEvent> getAllEvents() {
        return allEvents;
    }

    public Iterable<WebHookEventSection> getWebHookEventSections() {
        return webHookEventSections;
    }

    private <T extends IssueRelatedEvent> RegisteredWebHookEvent event(final String id, final String namei18nKey, final Class<T> eventClass, final Set<Long> issueEventIds) {
        return RegisteredWebHookEvent
                .withId(id)
                .andDisplayName(namei18nKey)
                .firedWhen(eventClass)
                .isMatchedBy((!issueEventIds.isEmpty()) ? and(new IssueEventIdMatcher(issueEventIds), jqlEventMatcher) : jqlEventMatcher);
    }

    private RegisteredWebHookEvent event(final String id, final String namei18nKey, final Class<?> eventClass) {
        return RegisteredWebHookEvent
                .withId(id)
                .andDisplayName(namei18nKey)
                .firedWhen(eventClass)
                .isMatchedBy(EventMatchers.ALWAYS_TRUE);
    }

    private <T> RegisteredWebHookEvent event(String id, String displayName, Class<T> eventClass, EventMatcher<? super T> matcher) {
        return RegisteredWebHookEvent
                .withId(id)
                .andDisplayName(displayName)
                .firedWhen(eventClass)
                .isMatchedBy(matcher);
    }

    private RegisteredWebHookEvent applicationPropertyEvent(String id, String displayName, String key) {
        return RegisteredWebHookEvent
                .withId(id)
                .andDisplayName(displayName)
                .firedWhen(AbstractApplicationPropertySetEvent.class)
                .isMatchedBy(new ApplicationPropertyEventMatcher(key));
    }

}
