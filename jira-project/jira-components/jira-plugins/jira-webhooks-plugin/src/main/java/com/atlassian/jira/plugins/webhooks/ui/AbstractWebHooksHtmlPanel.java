package com.atlassian.jira.plugins.webhooks.ui;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.web.component.jql.AutoCompleteJsonGenerator;
import com.atlassian.sal.api.message.HelpPath;
import com.atlassian.sal.api.message.HelpPathResolver;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.atlassian.velocity.htmlsafe.HtmlSafe;
import com.atlassian.webhooks.spi.WebHooksHtmlPanel;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.opensymphony.util.TextUtils;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Locale;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

class AbstractWebHooksHtmlPanel implements WebHooksHtmlPanel {
    private final TemplateRenderer templateRenderer;
    private final AutoCompleteJsonGenerator autoCompleteJsonGenerator;
    private final ApplicationProperties applicationProperties;
    private final HelpPathResolver helpPathResolver;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final String templatePath;

    public AbstractWebHooksHtmlPanel(String templateName, final TemplateRenderer templateRenderer,
                                     final AutoCompleteJsonGenerator autoCompleteJsonGenerator,
                                     final ApplicationProperties applicationProperties,
                                     final HelpPathResolver helpPathResolver,
                                     final JiraAuthenticationContext jiraAuthenticationContext) {
        this.templatePath = "/templates/" + templateName;
        this.templateRenderer = checkNotNull(templateRenderer);
        this.autoCompleteJsonGenerator = checkNotNull(autoCompleteJsonGenerator);
        this.applicationProperties = checkNotNull(applicationProperties);
        this.helpPathResolver = checkNotNull(helpPathResolver);
        this.jiraAuthenticationContext = checkNotNull(jiraAuthenticationContext);
    }

    @Override
    public String getHtml() {
        try {
            Map<String, Object> builder = Maps.newHashMap();
            builder.put("action", this);
            builder.put("textutils", new TextUtils());

            final StringWriter stringWriter = new StringWriter();
            templateRenderer.render(templatePath, ImmutableMap.copyOf(builder), stringWriter);
            return stringWriter.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private ApplicationUser getUser() {
        //return this.jiraAuthenticationContext.getUser();
        return this.jiraAuthenticationContext.getLoggedInUser();
    }

    private Locale getLocale() {
        return this.jiraAuthenticationContext.getLocale();
    }

    @HtmlSafe
    public String getVisibleFieldNamesJson() throws JSONException {
        return autoCompleteJsonGenerator.getVisibleFieldNamesJson(getUser(), getLocale());
    }

    @HtmlSafe
    public String getVisibleFunctionNamesJson() throws JSONException {
        return autoCompleteJsonGenerator.getVisibleFunctionNamesJson(getUser(), getLocale());
    }

    @HtmlSafe
    public String getJqlReservedWordsJson() throws JSONException {
        return autoCompleteJsonGenerator.getJqlReservedWordsJson();
    }

    public boolean isAutocompleteEnabled() {
        return !applicationProperties.getOption(APKeys.JIRA_JQL_AUTOCOMPLETE_DISABLED);
    }

    public HelpPath getHelpPath(final String path) {
        return helpPathResolver.getHelpPath(path);
    }
}
