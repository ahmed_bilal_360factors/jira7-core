package com.atlassian.jira.plugins.webhooks.serializer;

import com.atlassian.crowd.event.user.UserDeletedEvent;
import com.atlassian.jira.plugins.webhooks.registration.RegisteredWebHookEventFactory;
import com.atlassian.jira.user.UserKeyService;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public final class UserDeletedEventSerializer extends CrowdEventSerializer<UserDeletedEvent> {
    private final UserKeyService userKeyService;

    @Autowired
    public UserDeletedEventSerializer(final RegisteredWebHookEventFactory registeredWebHookEventFactory, final UserKeyService userKeyService) {
        super(registeredWebHookEventFactory);
        this.userKeyService = userKeyService;
    }

    @Override
    protected ImmutableMap.Builder<String, Object> putFields(final UserDeletedEvent event, final ImmutableMap.Builder<String, Object> defaultJson) {
        return defaultJson.put("user", ImmutableMap.of(
                "name", event.getUsername(),
                "key", userKeyService.getKeyForUsername(event.getUsername())));
    }

}
