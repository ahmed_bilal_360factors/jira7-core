package com.atlassian.jira.plugins.webhooks.serializer;

import com.atlassian.jira.event.issue.IssueRelatedEvent;
import com.atlassian.jira.plugins.webhooks.registration.RegisteredWebHookEventFactory;

abstract class AbstractIssueEventSerializer<T extends IssueRelatedEvent> extends AbstractJiraEventSerializer<T> {
    protected AbstractIssueEventSerializer(final RegisteredWebHookEventFactory registeredWebHookEventFactory) {
        super(registeredWebHookEventFactory);
    }

    @Override
    protected final Long getTimestamp(T event) {
        return event.getTime().getTime();
    }
}
