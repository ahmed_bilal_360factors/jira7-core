package com.atlassian.jira.plugins.webhooks.serializer;

import com.atlassian.jira.event.project.VersionDeleteEvent;
import com.atlassian.jira.plugins.webhooks.registration.RegisteredWebHookEventFactory;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public final class VersionDeletedEventSerializer extends AbstractVersionEventSerializer<VersionDeleteEvent> {
    @Autowired
    public VersionDeletedEventSerializer(final RegisteredWebHookEventFactory registeredWebHookEventFactory, final SimpleVersionBeanFactory versionBeanFactory) {
        super(registeredWebHookEventFactory, versionBeanFactory);
    }

    @Override
    protected ImmutableMap.Builder<String, Object> putFields(final VersionDeleteEvent event, final ImmutableMap.Builder<String, Object> defaultJson) {
        ImmutableMap.Builder<String, Object> result = super.putFields(event, defaultJson);
        if (event.getMergedTo().isDefined()) {
            result.put("mergedTo", versionBeanFactory.createBean(event.getMergedTo().get()));
        }
        if (event.getAffectsVersionSwappedTo().isDefined()) {
            result.put("affectsVersionSwappedTo", versionBeanFactory.createBean(event.getAffectsVersionSwappedTo().get()));
        }
        if (event.getFixVersionSwappedTo().isDefined()) {
            result.put("fixVersionSwappedTo", versionBeanFactory.createBean(event.getFixVersionSwappedTo().get()));
        }
        return result;
    }
}
