package com.atlassian.jira.plugins.webhooks.serializer;

import com.atlassian.crowd.event.user.UserUpdatedEvent;
import com.atlassian.jira.plugins.webhooks.registration.RegisteredWebHookEventFactory;
import com.atlassian.jira.plugins.webhooks.serializer.bean.UserBeanFactory;
import com.atlassian.jira.user.ApplicationUsers;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public final class UserUpdatedEventSerializer extends CrowdEventSerializer<UserUpdatedEvent> {
    private final UserBeanFactory userBeanFactory;

    @Autowired
    public UserUpdatedEventSerializer(final RegisteredWebHookEventFactory registeredWebHookEventFactory, final UserBeanFactory userBeanFactory) {
        super(registeredWebHookEventFactory);
        this.userBeanFactory = userBeanFactory;
    }

    @Override
    protected ImmutableMap.Builder<String, Object> putFields(final UserUpdatedEvent event, final ImmutableMap.Builder<String, Object> defaultJson) {
        return defaultJson.put("user", userBeanFactory.createBean(ApplicationUsers.from(event.getUser())));
    }
}
