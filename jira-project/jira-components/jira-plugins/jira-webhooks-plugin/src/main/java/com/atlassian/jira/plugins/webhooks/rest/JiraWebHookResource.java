package com.atlassian.jira.plugins.webhooks.rest;

import com.atlassian.jira.plugins.webhooks.workflow.WorkflowUtil;
import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.api.register.listener.WebHookListenerService;
import com.google.common.base.Function;
import com.google.common.base.Supplier;

import javax.annotation.Nullable;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import static com.google.common.base.Preconditions.checkNotNull;
import static javax.ws.rs.core.Response.ok;
import static javax.ws.rs.core.Response.status;

@Path("jira-webhook")
public class JiraWebHookResource {
    private final WebHookListenerService webHookListenerService;
    private final WorkflowUtil workflowUtil;

    public JiraWebHookResource(WebHookListenerService webHookListenerService, WorkflowUtil workflowUtil) {
        this.workflowUtil = checkNotNull(workflowUtil);
        this.webHookListenerService = checkNotNull(webHookListenerService);
    }

    @GET
    @Path("{id}/transitions")
    public Response getTransitions(@PathParam("id") final int id) {
        return webHookListenerService.getWebHookListener(id).fold(new Supplier<Response>() {
            @Override
            public Response get() {
                return status(Response.Status.NOT_FOUND).build();
            }
        }, new Function<PersistentWebHookListener, Response>() {
            @Override
            public Response apply(@Nullable final PersistentWebHookListener listener) {
                return ok(workflowUtil.getTransitionLinkedToWebHookListener(id)).build();
            }
        });
    }
}
