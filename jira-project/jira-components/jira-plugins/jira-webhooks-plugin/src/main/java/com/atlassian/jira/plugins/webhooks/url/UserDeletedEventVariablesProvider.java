package com.atlassian.jira.plugins.webhooks.url;

import com.atlassian.crowd.event.user.UserDeletedEvent;
import com.atlassian.jira.user.UserKeyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public final class UserDeletedEventVariablesProvider extends UserEventVariablesProvider<UserDeletedEvent> {
    private final UserKeyService userKeyService;

    @Autowired
    public UserDeletedEventVariablesProvider(final UserKeyService userKeyService) {
        this.userKeyService = userKeyService;
    }

    @Override
    protected UserVariables getUserVariables(final UserDeletedEvent event) {
        return new UserVariables(event.getUsername(), userKeyService.getKeyForUsername(event.getUsername()));
    }
}
