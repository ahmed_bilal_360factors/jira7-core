package com.atlassian.jira.plugins.webhooks.matcher;

import com.atlassian.webhooks.api.register.listener.WebHookListener;
import com.atlassian.webhooks.spi.EventMatcher;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;

import javax.annotation.concurrent.Immutable;
import java.util.Set;

@Immutable
public final class EventTypeMatcher<T> implements EventMatcher<T> {
    private final Set<Class<? extends T>> acceptedEvents;

    private EventTypeMatcher(final Iterable<Class<? extends T>> acceptedEvents) {
        this.acceptedEvents = ImmutableSet.copyOf(acceptedEvents);
    }

    public static <T> EventTypeMatcher<T> narrowToSubclasses(Class<? extends T>... acceptedEvents) {
        return new EventTypeMatcher<T>(Lists.newArrayList(acceptedEvents));
    }

    @Override
    public boolean matches(final T event, final WebHookListener listener) {
        for (Class<? extends T> eventClass : acceptedEvents) {
            if (eventClass.isInstance(event)) {
                return true;
            }
        }
        return false;
    }
}
