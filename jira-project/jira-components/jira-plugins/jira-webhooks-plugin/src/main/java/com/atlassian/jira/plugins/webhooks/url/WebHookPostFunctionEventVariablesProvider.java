package com.atlassian.jira.plugins.webhooks.url;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugins.webhooks.workflow.WebHookPostFunctionEvent;
import com.atlassian.jira.project.Project;
import com.atlassian.webhooks.api.document.ProvidesUrlVariables;
import com.atlassian.webhooks.spi.UriVariablesProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class WebHookPostFunctionEventVariablesProvider implements UriVariablesProvider<WebHookPostFunctionEvent> {
    private final IssueAndProjectContextSerializer contextSerializer;

    @Autowired
    public WebHookPostFunctionEventVariablesProvider(final IssueAndProjectContextSerializer contextSerializer) {
        this.contextSerializer = contextSerializer;
    }

    @ProvidesUrlVariables({"project.key", "project.id", "issue.key", "issue.id"})
    @Override
    public Map<String, Object> uriVariables(final WebHookPostFunctionEvent webHookPostFunctionEvent) {
        final Issue issue = webHookPostFunctionEvent.getIssue();
        final Project project = issue.getProjectObject();
        return contextSerializer.getContext(project, issue);
    }
}
