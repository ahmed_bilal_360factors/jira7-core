package com.atlassian.jira.plugins.webhooks.url;

final class UserUrlVariables {
    public static final String NAME = "modifiedUser.name";
    public static final String KEY = "modifiedUser.key";
    public static final String OLD_NAME = "modifiedUser.oldName";
}
