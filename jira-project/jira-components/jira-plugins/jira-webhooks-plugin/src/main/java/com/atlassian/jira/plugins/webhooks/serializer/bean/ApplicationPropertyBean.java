package com.atlassian.jira.plugins.webhooks.serializer.bean;

import org.codehaus.jackson.annotate.JsonProperty;

public class ApplicationPropertyBean {
    @JsonProperty
    private String self;

    @JsonProperty
    private String key;

    @JsonProperty
    private String value;

    public ApplicationPropertyBean(final String self, final String key, final String value) {
        this.self = self;
        this.key = key;
        this.value = value;
    }

    public String getSelf() {
        return self;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}
