package com.atlassian.jira.plugins.webhooks.upgrade;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.plugins.webhooks.PluginProperties;
import com.atlassian.jira.plugins.webhooks.ao.WebhookDao;
import com.atlassian.sal.api.message.Message;
import com.google.common.collect.ImmutableMap;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Upgrade to deal with JRADEV-14988 where we renamed events.
 */
public class Task001 implements com.atlassian.sal.api.upgrade.PluginUpgradeTask {
    private ActiveObjects ao;
    private final ImmutableMap<String, String> eventRenameMap;

    public Task001(final ActiveObjects ao) {
        this.ao = ao;

        final ImmutableMap.Builder<String, String> builder = ImmutableMap.builder();

        builder.put("issue_created", "jira:issue_created");
        builder.put("issue_deleted", "jira:issue_deleted");

        builder.put("issue_work_logged", "jira:worklog_updated");
        builder.put("issue_worklog_deleted", "jira:worklog_updated");
        builder.put("issue_worklog_updated", "jira:worklog_updated");

        builder.put("issue_assigned", "jira:issue_updated");
        builder.put("issue_closed", "jira:issue_updated");
        builder.put("issue_reopened", "jira:issue_updated");
        builder.put("issue_resolved", "jira:issue_updated");
        builder.put("issue_comment_edited", "jira:issue_updated");
        builder.put("issue_commented", "jira:issue_updated");
        builder.put("issue_moved", "jira:issue_updated");
        builder.put("issue_updated", "jira:issue_updated");
        builder.put("issue_work_started", "jira:issue_updated");
        builder.put("issue_work_stopped", "jira:issue_updated");
        builder.put("issue_generic_event", "jira:issue_updated");

        eventRenameMap = builder.build();
    }

    @Override
    public int getBuildNumber() {
        return 1;
    }

    @Override
    public String getShortDescription() {
        return "This upgrade tasks consolidates the old LOTS OF EVENTS into a smaller, more understandable number.";
    }

    @Override
    public Collection<Message> doUpgrade() throws Exception {
        final WebhookDao[] webhookDaos = ao.find(WebhookDao.class);
        for (WebhookDao webhookDao : webhookDaos) {
            final Set<String> eventSet = new HashSet<String>();
            final String[] events = webhookDao.getEvents();
            for (String event : events) {
                if (eventRenameMap.containsKey(event)) {
                    eventSet.add(eventRenameMap.get(event));
                } else {
                    eventSet.add(event);
                }
            }
            webhookDao.setEvents(eventSet.toArray(new String[eventSet.size()]));
            webhookDao.save();
        }

        return Collections.emptyList();
    }

    @Override
    public String getPluginKey() {
        return PluginProperties.PLUGIN_KEY;
    }
}
