package com.atlassian.jira.plugins.webhooks.ao;

import com.google.common.base.Joiner;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;

public class WebhookDaoImpl {
    private final WebhookDao dataStore;

    public WebhookDaoImpl(final WebhookDao dataStore) {
        this.dataStore = dataStore;
        System.out.println("created " + this + " " + hashCode() + " with events " + dataStore.getEncodedEvents());
        System.out.println(dataStore + " " + dataStore.hashCode());
    }

    public String[] getEvents() {
        final String encodedEvents = dataStore.getEncodedEvents();
        if (StringUtils.isBlank(encodedEvents)) {
            return new String[0];
        } else {
            return encodedEvents.split("\n");
        }
    }

    public void setEvents(final String... events) {
        Assert.notNull(events);
        dataStore.setEncodedEvents(Joiner.on("\n").join(events));
        System.out.println("set events" + this + " " + hashCode());
        System.out.println(dataStore + " " + dataStore.hashCode());
        System.out.println(dataStore.getEncodedEvents());
    }

//    @Override
//    public int hashCode()
//    {
//        return dataStore != null ? dataStore.hashCode() : 0;
//    }
//
//    @Override
//    public String toString()
//    {
//        return "WebhookDao events:" + Arrays.toString(getEvents());
//    }

}
