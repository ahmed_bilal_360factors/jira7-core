package com.atlassian.jira.plugins.webhooks.serializer;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.rest.v2.issue.IncludedFields;
import com.atlassian.jira.rest.v2.issue.IssueBean;
import com.atlassian.jira.rest.v2.issue.IssueBeanBuilder;
import com.atlassian.jira.rest.v2.issue.builder.BeanBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.UriBuilder;

/**
 * This is a class that can produce JSON-friendly representations of issues.
 */
@Component
public class IssueBeanFactory {
    private BeanBuilderFactory restBeanBuilderFactory;

    @Autowired
    public IssueBeanFactory(BeanBuilderFactory restBeanBuilderFactory) {
        this.restBeanBuilderFactory = restBeanBuilderFactory;
    }

    /**
     * Takes a business issue object and returns a bean that can be serialized to the JSON representation used in our REST API.
     *
     * @param issue JIRA issue
     * @return issue object that can be serialized to JSON
     */
    public IssueBean createBean(Issue issue) {
        final IssueBeanBuilder issueBeanBuilder = restBeanBuilderFactory.newIssueBeanBuilder(issue, IncludedFields.includeAllByDefault(null));
        final JiraBaseUrls jiraBaseUrls = ComponentAccessor.getComponent(JiraBaseUrls.class);
        final UriBuilder uriBuilder = UriBuilder.fromPath(jiraBaseUrls.restApi2BaseUrl());
        return issueBeanBuilder.uriBuilder(uriBuilder).build();
    }
}
