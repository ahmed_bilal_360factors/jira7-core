package com.atlassian.jira.plugins.webhooks.url;

import com.atlassian.crowd.event.user.UserCreatedEvent;
import com.atlassian.jira.plugins.webhooks.serializer.bean.UserBean;
import com.atlassian.jira.plugins.webhooks.serializer.bean.UserBeanFactory;
import com.atlassian.jira.user.ApplicationUsers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserCreatedEventVariablesProvider extends UserEventVariablesProvider<UserCreatedEvent> {
    private final UserBeanFactory userBeanFactory;

    @Autowired
    public UserCreatedEventVariablesProvider(final UserBeanFactory userBeanFactory) {
        this.userBeanFactory = userBeanFactory;
    }

    @Override
    protected UserVariables getUserVariables(final UserCreatedEvent event) {
        UserBean user = userBeanFactory.createBean(ApplicationUsers.from(event.getUser()));
        return new UserVariables(user.getName(), user.getKey());
    }
}
