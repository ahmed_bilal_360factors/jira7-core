package com.atlassian.jira.plugins.webhooks.serializer.worklog;

import com.atlassian.jira.event.worklog.WorklogEvent;
import com.atlassian.jira.plugins.webhooks.registration.RegisteredWebHookEventFactory;
import com.atlassian.jira.plugins.webhooks.serializer.bean.WorklogBeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WorklogCreatedSerializer extends AbstractWorklogSerializer {
    @Autowired
    public WorklogCreatedSerializer(RegisteredWebHookEventFactory registeredWebHookEventFactory, final WorklogBeanFactory worklogBeanFactory) {
        super(registeredWebHookEventFactory, worklogBeanFactory);
    }

    @Override
    protected Long getTimestamp(final WorklogEvent event) {
        return event.getWorklog().getCreated().getTime();
    }
}
