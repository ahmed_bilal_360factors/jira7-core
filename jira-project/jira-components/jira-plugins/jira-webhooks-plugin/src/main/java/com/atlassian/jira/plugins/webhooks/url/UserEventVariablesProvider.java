package com.atlassian.jira.plugins.webhooks.url;

import com.atlassian.crowd.event.DirectoryEvent;
import com.atlassian.webhooks.spi.DocumentedUriVariablesProvider;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import java.util.Collection;
import java.util.Map;

import static com.atlassian.jira.plugins.webhooks.url.UserUrlVariables.KEY;
import static com.atlassian.jira.plugins.webhooks.url.UserUrlVariables.NAME;

abstract class UserEventVariablesProvider<T extends DirectoryEvent> implements DocumentedUriVariablesProvider<T> {

    protected static class UserVariables {
        private final String name;
        private final String key;

        public UserVariables(final String name, final String key) {
            this.name = name;
            this.key = key;
        }
    }

    @Override
    public final Collection<String> providedVariables() {
        return ImmutableList.of(NAME, KEY);
    }

    @Override
    public final Map<String, Object> uriVariables(final T event) {
        UserVariables variables = getUserVariables(event);
        return ImmutableMap.<String, Object>of(
                NAME, variables.name,
                KEY, variables.key
        );
    }

    protected abstract UserVariables getUserVariables(final T event);
}
