package com.atlassian.jira.plugins.webhooks.upgrade;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.plugins.webhooks.PluginProperties;
import com.atlassian.jira.plugins.webhooks.ao.WebhookDao;
import com.atlassian.sal.api.message.Message;
import com.google.common.collect.ImmutableMap;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


public class Task003 implements com.atlassian.sal.api.upgrade.PluginUpgradeTask {
    private final ActiveObjects ao;
    private final Map<String, String> eventRenameMap;

    public Task003(final ActiveObjects ao) {
        this.ao = ao;
        eventRenameMap = buildVersionRenameMap();
    }

    private ImmutableMap<String, String> buildVersionRenameMap() {
        final ImmutableMap.Builder<String, String> builder = ImmutableMap.builder();
        builder.put("jira:version_created", "version_created");
        builder.put("jira:version_released", "version_released");
        builder.put("jira:version_unreleased", "version_unreleased");
        builder.put("jira:version_merged", "version_merged");
        builder.put("jira:version_deleted", "version_deleted");
        builder.put("jira:version_updated", "version_updated");
        builder.put("jira:version_moved", "version_moved");
        return builder.build();
    }

    @Override
    public int getBuildNumber() {
        return 3;
    }

    @Override
    public String getShortDescription() {
        return "This task strips jira: prefix from version webhook ids";
    }

    @Override
    public Collection<Message> doUpgrade() throws Exception {
        final WebhookDao[] webhookDaos = ao.find(WebhookDao.class);
        for (WebhookDao webhookDao : webhookDaos) {
            String[] oldEventList = webhookDao.getEvents();
            String[] newEvents = stripJiraPrefixFromVersionEvents(oldEventList);
            webhookDao.setEvents(newEvents);
            webhookDao.save();
        }
        return Collections.emptyList();
    }

    private String[] stripJiraPrefixFromVersionEvents(final String[] oldEventList) {
        final Set<String> eventSet = new HashSet<String>();
        for (String oldEvent : oldEventList) {
            if (eventRenameMap.containsKey(oldEvent)) {
                eventSet.add(eventRenameMap.get(oldEvent));
            } else {
                eventSet.add(oldEvent);
            }
        }
        return eventSet.toArray(new String[eventSet.size()]);
    }

    @Override
    public String getPluginKey() {
        return PluginProperties.PLUGIN_KEY;
    }
}
