package com.atlassian.jira.plugins.webhooks.listener;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.google.common.base.Strings.nullToEmpty;

public class JiraWebHookListenerParameters {
    public static Map<String, String> jsonToMap(String jsonMap) {
        try {
            final JSONObject jsonObject = new JSONObject(nullToEmpty(jsonMap).isEmpty() ? "{}" : jsonMap);
            final Map<String, String> map = new HashMap<String, String>();
            if (jsonObject.length() > 0) {
                for (String key : JSONObject.getNames(jsonObject)) {
                    map.put(key, jsonObject.getString(key));
                }
            }
            return map;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
}
