package com.atlassian.jira.plugins.webhooks.url;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class VersionAndProjectContextSerializer {
    private final VersionContextSerializer versionContextSerializer;
    private final ProjectContextSerializer projectContextSerializer;

    @Autowired
    public VersionAndProjectContextSerializer(final VersionContextSerializer versionContextSerializer, final ProjectContextSerializer projectContextSerializer) {
        this.versionContextSerializer = versionContextSerializer;
        this.projectContextSerializer = projectContextSerializer;
    }

    public Map<String, Object> getContext(final Project project, final Version version) {
        final ImmutableMap.Builder<String, Object> builder = ImmutableMap.builder();

        builder.putAll(versionContextSerializer.getContext(version));
        builder.putAll(projectContextSerializer.getContext(project));

        return builder.build();

    }
}
