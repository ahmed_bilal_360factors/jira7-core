package com.atlassian.jira.plugins.webhooks.url;

import com.atlassian.jira.event.project.VersionMergeEvent;
import com.atlassian.webhooks.api.document.ProvidesUrlVariables;
import com.atlassian.webhooks.spi.UriVariablesProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class VersionMergeEventVariablesProvider implements UriVariablesProvider<VersionMergeEvent> {
    private final VersionContextSerializer contextSerializer;

    @Autowired
    public VersionMergeEventVariablesProvider(final VersionContextSerializer contextSerializer) {
        this.contextSerializer = contextSerializer;
    }

    @ProvidesUrlVariables({"mergedVersion.id"})
    @Override
    public Map<String, Object> uriVariables(VersionMergeEvent event) {
        return contextSerializer.getContext(event.getMergedVersion(), "mergedVersion");
    }
}
