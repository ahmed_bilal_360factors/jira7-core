package com.atlassian.jira.plugins.webhooks.serializer;

import com.atlassian.jira.event.property.StringApplicationPropertySetEvent;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.plugins.webhooks.registration.RegisteredWebHookEventFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StringApplicationPropertySerializer extends AbstractApplicationPropertySerializer<StringApplicationPropertySetEvent> {
    @Autowired
    public StringApplicationPropertySerializer(final RegisteredWebHookEventFactory registeredWebHookEventFactory, final JiraBaseUrls urls) {
        super(registeredWebHookEventFactory, urls);
    }

    @Override
    protected String getEventValue(final StringApplicationPropertySetEvent event) {
        return event.getPropertyValue();
    }
}
