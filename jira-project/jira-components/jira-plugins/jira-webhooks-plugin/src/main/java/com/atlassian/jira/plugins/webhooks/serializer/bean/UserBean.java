package com.atlassian.jira.plugins.webhooks.serializer.bean;

import org.codehaus.jackson.annotate.JsonProperty;

public class UserBean {
    @JsonProperty
    private String self;

    @JsonProperty
    private String name;

    @JsonProperty
    private String key;

    @JsonProperty
    private String emailAddress;

    @JsonProperty
    private String displayName;

    public UserBean(final String self, final String name, final String key, final String emailAddress, final String displayName) {
        this.self = self;
        this.name = name;
        this.key = key;
        this.emailAddress = emailAddress;
        this.displayName = displayName;
    }

    public String getSelf() {
        return self;
    }

    public String getName() {
        return name;
    }

    public String getKey() {
        return key;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getDisplayName() {
        return displayName;
    }
}
