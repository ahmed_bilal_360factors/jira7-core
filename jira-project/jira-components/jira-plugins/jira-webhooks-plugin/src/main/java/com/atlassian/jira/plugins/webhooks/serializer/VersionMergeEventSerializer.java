package com.atlassian.jira.plugins.webhooks.serializer;

import com.atlassian.jira.event.project.VersionMergeEvent;
import com.atlassian.jira.plugins.webhooks.registration.RegisteredWebHookEventFactory;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class VersionMergeEventSerializer extends AbstractVersionEventSerializer<VersionMergeEvent> {

    @Autowired
    public VersionMergeEventSerializer(final RegisteredWebHookEventFactory registeredWebHookEventFactory, final SimpleVersionBeanFactory versionBeanFactory) {
        super(registeredWebHookEventFactory, versionBeanFactory);
    }

    @Override
    protected ImmutableMap.Builder<String, Object> putFields(final VersionMergeEvent event, final ImmutableMap.Builder<String, Object> defaultJson) {
        return super.putFields(event, defaultJson)
                .put("mergedVersion", versionBeanFactory.createBean(event.getMergedVersion()));
    }
}
