package com.atlassian.jira.plugins.webhooks.workflow;

import com.atlassian.fugue.Option;
import com.atlassian.jira.issue.Issue;
import com.atlassian.webhooks.api.register.listener.WebHookListener;
import com.atlassian.webhooks.api.register.listener.WebHookListenerRegistrationDetails;
import com.atlassian.webhooks.api.util.ConsiderAllListeners;
import com.atlassian.webhooks.spi.EventMatcher;
import com.atlassian.webhooks.spi.EventSerializer;

public class WebHookPostFunctionEvent {
    private final int webHookConsumerId;
    private final String message;
    private final Issue issue;

    public WebHookPostFunctionEvent(int webHookConsumerId, String message, final Issue issue) {
        this.webHookConsumerId = webHookConsumerId;
        this.message = message;
        this.issue = issue;
    }

    @ConsiderAllListeners
    public static final class FunctionEventIdMatcher implements EventMatcher<WebHookPostFunctionEvent> {
        @Override
        public boolean matches(final WebHookPostFunctionEvent event, final WebHookListener listener) {
            Option<WebHookListenerRegistrationDetails.PersistentStoreRegistrationDetails> persistentStoreDetails = listener.getRegistrationDetails().getPersistentStoreDetails();
            return persistentStoreDetails.isDefined() && Integer.valueOf(event.webHookConsumerId).equals(persistentStoreDetails.get().getId());
        }
    }

    public static final class FunctionEventSerializerFactory implements EventSerializer<WebHookPostFunctionEvent> {
        @Override
        public String serialize(WebHookPostFunctionEvent event) {
            return event.message;
        }
    }

    public Issue getIssue() {
        return issue;
    }
}
