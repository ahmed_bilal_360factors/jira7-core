package com.atlassian.jira.plugins.webhooks.serializer;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.rest.v2.issue.VersionResource;
import com.atlassian.jira.rest.v2.issue.version.VersionBean;
import com.atlassian.jira.util.DateFieldFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.Date;

/**
 * Simplified version of VersionBeanFactoryImpl that provides only basic version information.
 */
@Component
public class SimpleVersionBeanFactory {
    private VersionManager versionManager;
    private final DateFieldFormat dateFieldFormat;

    @Autowired
    public SimpleVersionBeanFactory(final VersionManager versionManager,
                                    final DateFieldFormat dateFieldFormat) {
        this.versionManager = versionManager;
        this.dateFieldFormat = dateFieldFormat;
    }

    public VersionBean createBean(Version version) {
        final Date startDate = version.getStartDate();
        final Date releaseDate = version.getReleaseDate();

        String prettyStartDate = null;
        if (startDate != null) {
            prettyStartDate = dateFieldFormat.format(startDate);
        }

        String prettyReleaseDate = null;
        if (releaseDate != null) {
            prettyReleaseDate = dateFieldFormat.format(releaseDate);
        }

        final VersionBean.Builder beanBuilder = new VersionBean.Builder()
                .setArchived(version.isArchived())
                .setDescription(version.getDescription())
                .setExpand(null)
                .setId(version.getId())
                .setName(version.getName())
                .setOverdue(versionManager.isVersionOverDue(version))
                .setProjectId(version.getProjectId())
                .setReleased(version.isReleased())
                .setUserReleaseDate(prettyReleaseDate)
                .setUserStartDate(prettyStartDate)
                .setSelf(createSelfURI(version));

        return beanBuilder.build();
    }

    private URI createSelfURI(Version version) {
        final JiraBaseUrls jiraBaseUrls = ComponentAccessor.getComponent(JiraBaseUrls.class);
        final UriBuilder uriBuilder = UriBuilder.fromPath(jiraBaseUrls.restApi2BaseUrl());
        return uriBuilder.path(VersionResource.class).path(version.getId().toString()).build();
    }
}
