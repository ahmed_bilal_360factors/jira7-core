package com.atlassian.jira.plugins.webhooks.serializer.worklog;

import com.atlassian.jira.event.worklog.WorklogEvent;
import com.atlassian.jira.plugins.webhooks.registration.RegisteredWebHookEventFactory;
import com.atlassian.jira.plugins.webhooks.serializer.AbstractJiraEventSerializer;
import com.atlassian.jira.plugins.webhooks.serializer.bean.WorklogBeanFactory;
import com.google.common.collect.ImmutableMap;

abstract class AbstractWorklogSerializer extends AbstractJiraEventSerializer<WorklogEvent> {
    private final WorklogBeanFactory worklogBeanFactory;

    public AbstractWorklogSerializer(RegisteredWebHookEventFactory registeredWebHookEventFactory, final WorklogBeanFactory worklogBeanFactory) {
        super(registeredWebHookEventFactory);
        this.worklogBeanFactory = worklogBeanFactory;
    }

    @Override
    protected ImmutableMap.Builder<String, Object> putFields(final WorklogEvent event, final ImmutableMap.Builder<String, Object> defaultJson) {
        return defaultJson.put("worklog", worklogBeanFactory.createBean(event.getWorklog()));
    }
}
