package com.atlassian.jira.plugins.webhooks.serializer.comment;

import com.atlassian.jira.event.comment.CommentEvent;
import com.atlassian.jira.plugins.webhooks.registration.RegisteredWebHookEventFactory;
import com.atlassian.jira.plugins.webhooks.serializer.AbstractJiraEventSerializer;
import com.atlassian.jira.plugins.webhooks.serializer.bean.CommentBeanFactory;

import com.google.common.collect.ImmutableMap;

/**
 * Serializes implementations of {@link CommentEvent}. Comments are also
 * serialized during issue serialization and on issue related events.
 */
abstract class AbstractCommentSerializer<T extends CommentEvent> extends AbstractJiraEventSerializer<T> {

    private final CommentBeanFactory commentBeanFactory;

    protected AbstractCommentSerializer(final RegisteredWebHookEventFactory registeredWebHookEventFactory,
            final CommentBeanFactory commentBeanFactory) {
        super(registeredWebHookEventFactory);
        this.commentBeanFactory = commentBeanFactory;
    }

    @Override
    protected ImmutableMap.Builder<String, Object> putFields(final T event,
            final ImmutableMap.Builder<String, Object> defaultJson) {
        return defaultJson.put("comment", commentBeanFactory.createBean(event.getComment()));
    }
}
