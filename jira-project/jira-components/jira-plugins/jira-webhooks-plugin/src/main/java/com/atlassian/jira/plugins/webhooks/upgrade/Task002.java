package com.atlassian.jira.plugins.webhooks.upgrade;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.plugins.webhooks.PluginProperties;
import com.atlassian.jira.plugins.webhooks.ao.WebhookDao;
import com.atlassian.sal.api.message.Message;
import com.google.common.collect.Sets;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Removes NO_EVENTS and ALL_EVENTS from active objects.
 */
public class Task002 implements com.atlassian.sal.api.upgrade.PluginUpgradeTask {
    private enum Event {
        ALL_EVENTS("events:all_events"),
        NO_EVENTS("events:no_events");

        private String value;

        public String getValue() {
            return value;
        }

        Event(final String value) {
            this.value = value;
        }
    }

    private final ActiveObjects ao;
    private final Set<String> allEvents;

    public Task002(final ActiveObjects ao) {
        this.ao = checkNotNull(ao);
        this.allEvents = Sets.newHashSet("jira:issue_created", "jira:issue_deleted", "jira:issue_updated", "jira:worklog_updated");
    }

    @Override
    public int getBuildNumber() {
        return 2;
    }

    @Override
    public String getShortDescription() {
        return "This task translates all events into the proper event list";
    }

    @Override
    public Collection<Message> doUpgrade() throws Exception {
        final WebhookDao[] webhookDaos = ao.find(WebhookDao.class);
        for (WebhookDao webhookDao : webhookDaos) {
            final String[] events = webhookDao.getEvents();
            if (events != null && events.length == 1) {
                if (events[0].equals(Event.ALL_EVENTS.getValue())) {
                    webhookDao.setEvents(allEvents.toArray(new String[allEvents.size()]));
                } else if (events[0].equals(Event.NO_EVENTS.getValue())) {
                    webhookDao.setEvents(new String[]{});
                }
            }
            webhookDao.save();
        }

        return Collections.emptyList();
    }

    @Override
    public String getPluginKey() {
        return PluginProperties.PLUGIN_KEY;
    }
}
