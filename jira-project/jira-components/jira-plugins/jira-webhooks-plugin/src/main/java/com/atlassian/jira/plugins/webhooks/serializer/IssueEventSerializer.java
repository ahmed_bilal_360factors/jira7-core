package com.atlassian.jira.plugins.webhooks.serializer;

import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.issue.IssueEventVisitor;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.issue.fields.rest.json.CommentBeanFactory;
import com.atlassian.jira.issue.fields.rest.json.UserBeanFactory;
import com.atlassian.jira.issue.fields.rest.json.beans.UserJsonBean;
import com.atlassian.jira.plugins.webhooks.registration.RegisteredWebHookEventFactory;
import com.atlassian.jira.rest.v2.issue.ChangelogBean;
import com.atlassian.jira.rest.v2.issue.builder.BeanBuilderFactory;
import com.atlassian.jira.rest.v2.issue.builder.ChangelogBeanBuilder;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Component
public class IssueEventSerializer extends AbstractIssueEventSerializer<IssueEvent> {
    private final CommentBeanFactory commentBeanFactory;
    private final UserBeanFactory userBeanFactory;
    private final BeanBuilderFactory restBeanBuilderFactory;
    private final IssueBeanFactory issueBeanFactory;

    private final Collection<Long> noChangeLogEventTypes = Lists.newArrayList(
            EventType.ISSUE_COMMENTED_ID,
            EventType.ISSUE_COMMENT_DELETED_ID,
            EventType.ISSUE_COMMENT_EDITED_ID,
            EventType.ISSUE_DELETED_ID
    );

    @Autowired
    public IssueEventSerializer(final RegisteredWebHookEventFactory registeredWebHookEventFactory,
                                final CommentBeanFactory commentBeanFactory,
                                final UserBeanFactory userBeanFactory,
                                final BeanBuilderFactory restBeanBuilderFactory,
                                final IssueBeanFactory issueBeanFactory) {
        super(registeredWebHookEventFactory);
        this.commentBeanFactory = commentBeanFactory;
        this.userBeanFactory = userBeanFactory;
        this.restBeanBuilderFactory = restBeanBuilderFactory;
        this.issueBeanFactory = issueBeanFactory;
    }

    @Override
    protected ImmutableMap.Builder<String, Object> putFields(IssueEvent event, ImmutableMap.Builder<String, Object> builder) {
        builder.put("issue_event_type_name", mapEventToName(event));
        if (event.getUser() != null) {
            builder.put("user", userBeanFactory.createBean(event.getUser()));
        }
        builder.put("issue", issueBeanFactory.createBean(event.getIssue()));
        Optional<? extends ChangelogBean.ChangeHistoryBean> changelog = getChangelog(event);
        if (changelog.isPresent()) {
            builder.put("changelog", changelog.get());
        }
        if (event.getComment() != null) {
            builder.put("comment", commentBeanFactory.createBean(event.getComment()));
        }
        return builder;
    }


    /**
     * Try to find the ChangeHistoryBean corresponding to this IssueEvent. Our search strategy is:
     * 1. If the IssueEvent had a changelog with it, then we can match based on changelog id
     * 2. If not, then we take the most recent changelog by the user who triggered the event
     * 3. Final fall back (should never happen?), is to just use the most recent changelog
     *
     * @param issueEvent the event whose change history bean you want to find
     * @return the ChangeHistoryBean corresponding to this IssueEvent
     */
    private Optional<? extends ChangelogBean.ChangeHistoryBean> getChangelog(final IssueEvent issueEvent) {
        final ChangelogBeanBuilder changelogBeanBuilder = restBeanBuilderFactory.newChangelogBeanBuilder();
        List<ChangelogBean.ChangeHistoryBean> allChanges = changelogBeanBuilder.build(issueEvent.getIssue()).getHistories();

        if (noChangeLogEventTypes.contains(issueEvent.getEventTypeId())) {
            return Optional.absent();
        }

        if (allChanges.size() == 0) {
            return Optional.absent();
        }

        final List<ChangelogBean.ChangeHistoryBean> histories = Lists.reverse(allChanges);

        ChangelogBean.ChangeHistoryBean changeHistoryBean = Iterables.find(histories, getPredicate(issueEvent), histories.get(0));
        return Optional.of(new NoUserCHB(changeHistoryBean));
    }

    private String mapEventToName(IssueEvent event) {
        return event.accept(new IssueEventVisitor<String>() {
            @Override
            public String onIssueCreated(IssueEvent e) {
                return "issue_created";
            }

            @Override
            public String onIssueUpdated(IssueEvent e) {
                return "issue_updated";
            }

            @Override
            public String onIssueAssigned(IssueEvent e) {
                return "issue_assigned";
            }

            @Override
            public String onIssueResolved(IssueEvent e) {
                return "issue_resolved";
            }

            @Override
            public String onIssueCommented(IssueEvent e) {
                return "issue_commented";
            }

            @Override
            public String onIssueCommentEdited(IssueEvent e) {
                return "issue_comment_edited";
            }

            @Override
            public String onIssueCommentDeleted(IssueEvent e) {
                return "issue_comment_deleted";
            }

            @Override
            public String onIssueClosed(IssueEvent e) {
                return "issue_closed";
            }

            @Override
            public String onIssueReopened(IssueEvent e) {
                return "issue_reopened";
            }

            @Override
            public String onIssueDeleted(IssueEvent e) {
                return "issue_deleted";
            }

            @Override
            public String onIssueMoved(IssueEvent e) {
                return "issue_moved";
            }

            @Override
            public String onIssueWorkLogged(IssueEvent e) {
                return "issue_work_logged";
            }

            @Override
            public String onIssueWorkStarted(IssueEvent e) {
                return "issue_work_started";
            }

            @Override
            public String onIssueWorkStopped(IssueEvent e) {
                return "issue_work_stopped";
            }

            @Override
            public String onIssueWorklogUpdated(IssueEvent e) {
                return "issue_worklog_updated";
            }

            @Override
            public String onIssueWorklogDeleted(IssueEvent e) {
                return "issue_worklog_deleted";
            }

            @Override
            public String onIssueGenericEvent(IssueEvent e) {
                return "issue_generic";
            }

            @Override
            public String onCustomEvent(IssueEvent e) {
                return "custom";
            }
        });
    }

    /**
     * Determine what predicate to use when looking through the changehistory beans. If the issue event
     * had a changelog provided then we can just match on changelog id. If there's no changelog in the event
     * itself then we look for the most recent changelog by the user the triggered the event.
     *
     * @param issueEvent the issue event whose changelog we want
     * @return a predicate to find a matching changelog
     */
    private static Predicate<ChangelogBean.ChangeHistoryBean> getPredicate(final IssueEvent issueEvent) {
        if (issueEvent.getChangeLog() != null && issueEvent.getChangeLog().get("id") != null) {
            return changeHistoryBean -> {
                if (changeHistoryBean == null) {
                    return false;
                }
                Long changeLogId = (Long) issueEvent.getChangeLog().get("id");
                return Long.valueOf(changeHistoryBean.getId()).equals(changeLogId);
            };
        } else {
            return changeHistoryBean -> changeHistoryBean != null &&
                    StringUtils.equals(issueEvent.getUser().getName(), changeHistoryBean.getAuthor().getName());
        }
    }

    static class NoUserCHB extends ChangelogBean.ChangeHistoryBean {
        private ChangelogBean.ChangeHistoryBean changeHistory;

        NoUserCHB(ChangelogBean.ChangeHistoryBean chb) {
            this.changeHistory = chb;
            this.setId(chb.getId());
            this.setItems(chb.getItems());
        }

        @Override
        @JsonIgnore
        public UserJsonBean getAuthor() {
            return changeHistory.getAuthor();
        }

        @Override
        @JsonIgnore
        public Date getCreated() {
            return changeHistory.getCreated();
        }
    }
}
