package com.atlassian.jira.plugins.webhooks.workflow;

import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.rest.json.UserBeanFactory;
import com.atlassian.jira.plugins.webhooks.serializer.IssueBeanFactory;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.opensymphony.workflow.StoreException;
import com.opensymphony.workflow.loader.ActionDescriptor;
import com.opensymphony.workflow.loader.StepDescriptor;
import com.opensymphony.workflow.loader.WorkflowDescriptor;
import com.opensymphony.workflow.spi.Step;
import com.opensymphony.workflow.spi.WorkflowEntry;
import com.opensymphony.workflow.spi.WorkflowStore;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

import static com.atlassian.fugue.Option.option;

@Component
public class TransitionMapper {
    private static final Logger LOG = Logger.getLogger(TransitionMapper.class.getName());

    static final String TRANSITION_CONTEXT = "transition";
    static final String FROM_STATUS = "from_status";
    static final String TO_STATUS = "to_status";
    static final String WORKFLOW_STORE = "store";
    static final String CREATED_STEP = "createdStep";
    static final String WORKFLOW_DESCRIPTOR = "descriptor";
    static final String WORKFLOW_ID = "workflowId";
    static final String WORKFLOW_NAME = "workflowName";
    static final String TRANSITION_ID = "transitionId";
    static final String TRANSITION_NAME = "transitionName";
    static final String COMMENT = "comment";
    static final String USER = "user";
    static final String ISSUE = "issue";
    static final String TIMESTAMP = "timestamp";
    static final String ENTRY = "entry";
    static final String ACTION_ID = "actionId";

    private final IssueBeanFactory issueBeanFactory;
    private final UserBeanFactory userBeanFactory;

    @Autowired
    public TransitionMapper(final IssueBeanFactory issueBeanFactory, final UserBeanFactory userBeanFactory) {
        this.issueBeanFactory = issueBeanFactory;
        this.userBeanFactory = userBeanFactory;
    }

    public Map<String, Object> toMap(Map transientVars, ApplicationUser user, MutableIssue issue) {
        final WorkflowEntry entry = (WorkflowEntry) transientVars.get(ENTRY);
        final ImmutableMap.Builder<String, Object> transition = ImmutableMap.builder();
        final Integer actionId = (Integer) transientVars.get(ACTION_ID);
        final WorkflowDescriptor workflowDescriptor = (WorkflowDescriptor) transientVars.get(WORKFLOW_DESCRIPTOR);
        final ActionDescriptor actionDescriptor = workflowDescriptor.getAction(actionId);
        final Step createdStep = (Step) transientVars.get(CREATED_STEP);
        final WorkflowStore workflowStore = (WorkflowStore) transientVars.get(WORKFLOW_STORE);

        transition.put(WORKFLOW_ID, entry.getId())
                .put(WORKFLOW_NAME, entry.getWorkflowName())
                .put(TRANSITION_ID, actionId)
                .put(TRANSITION_NAME, actionDescriptor.getName())
                .put(FROM_STATUS, findPreviousStatus(createdStep, entry, workflowStore, workflowDescriptor))
                .put(TO_STATUS, getToStatus(workflowDescriptor, createdStep));

        final ImmutableMap.Builder<String, Object> transitionContext = ImmutableMap.builder();
        transitionContext.put(TRANSITION_CONTEXT, transition.build())
                .put(COMMENT, transientVars.get(COMMENT) != null ? transientVars.get(COMMENT) : "")
                .put(USER, user != null ? userBeanFactory.createBean(user) : "");

        transitionContext.put(ISSUE, issueBeanFactory.createBean(issue));
        transitionContext.put(TIMESTAMP, createdStep.getStartDate().getTime());

        return transitionContext.build();
    }

    private String findPreviousStatus(Step currentStep, WorkflowEntry entry, WorkflowStore workflowStore, WorkflowDescriptor workflowDescriptor) {
        if (currentStep.getPreviousStepIds().length == 0) {
            return "";
        }
        final long previousStepId = currentStep.getPreviousStepIds()[0];
        try {
            final List<Step> historySteps = workflowStore.findHistorySteps(entry.getId());
            final Step previousStep = Iterables.find(historySteps, new Predicate<Step>() {
                @Override
                public boolean apply(Step step) {
                    return step.getId() == previousStepId;
                }
            }, null);

            if (previousStep == null) {
                LOG.error("Transition from cannot be found. It seems that workflow data is inconsistent.");
                return "";
            }

            return workflowDescriptor.getStep(previousStep.getStepId()).getName();
        } catch (StoreException e) {
            return "";
        }
    }

    private String getToStatus(WorkflowDescriptor workflowDescriptor, Step createdStep) {
        return option(workflowDescriptor.getStep(createdStep.getStepId())).map(new Function<StepDescriptor, String>() {
            @Override
            public String apply(final StepDescriptor input) {
                return input.getName();
            }
        }).getOrElse("");
    }
}
