package com.atlassian.jira.plugins.webhooks.url;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.project.Project;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class IssueAndProjectContextSerializer {
    private final IssueContextSerializer issueContextSerializer;
    private final ProjectContextSerializer projectContextSerializer;

    @Autowired
    public IssueAndProjectContextSerializer(final IssueContextSerializer issueContextSerializer, final ProjectContextSerializer projectContextSerializer) {
        this.issueContextSerializer = issueContextSerializer;
        this.projectContextSerializer = projectContextSerializer;
    }

    public Map<String, Object> getContext(final Project project, final Issue issue) {
        final ImmutableMap.Builder<String, Object> builder = ImmutableMap.builder();

        builder.putAll(issueContextSerializer.getContext(issue));
        builder.putAll(projectContextSerializer.getContext(project));

        return builder.build();

    }
}
