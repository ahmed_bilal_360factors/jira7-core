package com.atlassian.jira.plugins.webhooks.registration;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.webhooks.spi.WebHookPluginRegistrationFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class JiraCloudCondition implements WebHookPluginRegistrationFactory.CloudCondition {
    private final FeatureManager featureManager;

    @Autowired
    public JiraCloudCondition(final FeatureManager featureManager) {
        this.featureManager = featureManager;
    }

    @Override
    @Deprecated
    public boolean isCloud() {
        return false;
    }
}
