package com.atlassian.jira.plugins.webhooks.url;

import com.atlassian.jira.project.version.Version;
import com.google.common.collect.ImmutableMap;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class VersionContextSerializer {
    public Map<String, Object> getContext(final Version version) {
        return getContext(version, "version");
    }

    public Map<String, Object> getContext(final Version version, String keyName) {
        return ImmutableMap.<String, Object>of(keyName, ImmutableMap.of(
                "id", version.getId()
        ));
    }
}
