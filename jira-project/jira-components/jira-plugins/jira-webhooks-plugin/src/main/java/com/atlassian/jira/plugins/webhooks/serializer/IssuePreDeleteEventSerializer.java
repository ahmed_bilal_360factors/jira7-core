package com.atlassian.jira.plugins.webhooks.serializer;

import com.atlassian.jira.event.issue.IssuePreDeleteEvent;
import com.atlassian.jira.issue.fields.rest.json.UserBeanFactory;
import com.atlassian.jira.plugins.webhooks.registration.RegisteredWebHookEventFactory;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class IssuePreDeleteEventSerializer extends AbstractIssueEventSerializer<IssuePreDeleteEvent> {

    private final UserBeanFactory userBeanFactory;
    private final IssueBeanFactory issueBeanFactory;

    @Autowired
    public IssuePreDeleteEventSerializer(final RegisteredWebHookEventFactory registeredWebHookEventFactory,
                                         final UserBeanFactory userBeanFactory,
                                         final IssueBeanFactory issueBeanFactory) {
        super(registeredWebHookEventFactory);
        this.userBeanFactory = userBeanFactory;
        this.issueBeanFactory = issueBeanFactory;
    }

    @Override
    protected ImmutableMap.Builder<String, Object> putFields(IssuePreDeleteEvent preDeleteEvent, ImmutableMap.Builder<String, Object> builder) {
        if (preDeleteEvent.getUser() != null) {
            builder.put("user", userBeanFactory.createBean(preDeleteEvent.getUser()));
        }
        builder.put("issue", issueBeanFactory.createBean(preDeleteEvent.getIssue()));
        return builder;
    }
}
