package com.atlassian.jira.plugins.webhooks.serializer;


import com.atlassian.jira.plugins.webhooks.registration.RegisteredWebHookEventFactory;
import com.atlassian.webhooks.api.register.RegisteredWebHookEvent;
import com.atlassian.webhooks.api.register.listener.WebHookListener;
import com.atlassian.webhooks.spi.EventSerializer;
import com.google.common.collect.ImmutableMap;

public abstract class AbstractJiraEventSerializer<T> implements EventSerializer<T> {
    private final RegisteredWebHookEventFactory registeredWebHookEventFactory;

    protected AbstractJiraEventSerializer(final RegisteredWebHookEventFactory registeredWebHookEventFactory) {
        this.registeredWebHookEventFactory = registeredWebHookEventFactory;
    }

    protected ImmutableMap.Builder<String, Object> defaultFields(T event) {
        ImmutableMap.Builder<String, Object> builder = ImmutableMap.<String, Object>builder();
        builder.put("timestamp", getTimestamp(event));
        builder.put("webhookEvent", resolveEventName(event));
        return builder;
    }

    protected abstract Long getTimestamp(T event);

    @Override
    public final String serialize(T event) {
        return JacksonSerializer.serialize(putFields(event, defaultFields(event)).build());
    }

    protected abstract ImmutableMap.Builder<String, Object> putFields(T event, ImmutableMap.Builder<String, Object> defaultJson);

    private String resolveEventName(T event) {
        return getJiraEventMapping(event).getId();
    }

    private RegisteredWebHookEvent getJiraEventMapping(final T e) {
        for (RegisteredWebHookEvent registeredWebHookEvent : registeredWebHookEventFactory.getAllEvents()) {
            if (registeredWebHookEvent.getEventClass().isInstance(e) &&
                    registeredWebHookEvent.getEventMatcher().matches(e, WebHookListener.fromPersistentStore(42, "name").to("/").build())) {
                return registeredWebHookEvent;
            }
        }
        throw new IllegalStateException("No matching WebHookJiraEvent found for event " + e);
    }
}
