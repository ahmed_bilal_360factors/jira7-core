package com.atlassian.jira.plugins.webhooks.validation;

import com.atlassian.jira.plugins.webhooks.listener.JqlFilterStoreUtils;
import com.atlassian.jira.plugins.webhooks.workflow.WorkflowUtil;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.api.util.ErrorMessage;
import com.atlassian.webhooks.api.util.MessageCollection;
import com.atlassian.webhooks.spi.WebHookListenerActionValidator;
import com.google.common.base.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Set;

@Component
public class JiraWebHookListenerActionValidator implements WebHookListenerActionValidator {
    private final JqlValidationUtil jqlValidationUtil;
    private final I18nResolver i18n;
    private final WorkflowUtil workflowUtil;

    @Autowired
    public JiraWebHookListenerActionValidator(JqlValidationUtil jqlValidationUtil,
                                              I18nResolver i18n,
                                              WorkflowUtil workflowUtil) {
        this.jqlValidationUtil = jqlValidationUtil;
        this.i18n = i18n;
        this.workflowUtil = workflowUtil;
    }

    @Override
    public MessageCollection validateWebHookRegistration(final PersistentWebHookListener listener) {
        return validateAdditionAndUpdate(listener);
    }

    @Override
    public MessageCollection validateWebHookUpdate(PersistentWebHookListener listener) {
        return validateAdditionAndUpdate(listener);
    }

    private MessageCollection validateAdditionAndUpdate(final PersistentWebHookListener listener) {
        final MessageCollection.Builder builder = MessageCollection.builder();
        final Optional<String> filter = Optional.fromNullable(JqlFilterStoreUtils.getJqlFilter(listener));

        jqlValidationUtil.validateFilter(builder, filter);

        return builder.build();
    }

    @Override
    public MessageCollection validateWebHookRemoval(PersistentWebHookListener webHookListenerParameters) {
        final Set<Map<String, String>> transitions = workflowUtil.getTransitionLinkedToWebHookListener(webHookListenerParameters.getId().get());
        if (!transitions.isEmpty()) {
            final MessageCollection.Builder errorCollectionBuilder = MessageCollection.builder();
            final ErrorMessage conflictErrorMsg = new ErrorMessage("title", i18n.getText("webhooks.delete.error.linkedtransitions", webHookListenerParameters.getName()));
            final ErrorMessage transitionsErrorMsg = new ErrorMessage("transitions", transitions.toString());
            errorCollectionBuilder.addMessage(conflictErrorMsg, MessageCollection.Reason.CONFLICT);
            errorCollectionBuilder.addMessage(transitionsErrorMsg, MessageCollection.Reason.CONFLICT);
            return errorCollectionBuilder.build();
        }
        return MessageCollection.empty();
    }

}
