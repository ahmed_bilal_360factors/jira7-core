package com.atlassian.jira.plugins.webhooks.serializer.comment;

import com.atlassian.jira.event.comment.CommentUpdatedEvent;
import com.atlassian.jira.plugins.webhooks.registration.RegisteredWebHookEventFactory;
import com.atlassian.jira.plugins.webhooks.serializer.bean.CommentBeanFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CommentUpdatedSerializer extends AbstractCommentSerializer<CommentUpdatedEvent> {
    @Autowired
    public CommentUpdatedSerializer(final RegisteredWebHookEventFactory registeredWebHookEventFactory,
            final CommentBeanFactory commentBeanFactory) {
        super(registeredWebHookEventFactory, commentBeanFactory);
    }

    @Override
    protected Long getTimestamp(final CommentUpdatedEvent event) {
        return event.getComment().getUpdated().getTime();
    }
}
