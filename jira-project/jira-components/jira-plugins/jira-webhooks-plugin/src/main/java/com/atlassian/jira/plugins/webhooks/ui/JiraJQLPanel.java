package com.atlassian.jira.plugins.webhooks.ui;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.component.jql.AutoCompleteJsonGenerator;
import com.atlassian.sal.api.message.HelpPathResolver;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Renders the panel with JQL Field.
 */
@Component
public class JiraJQLPanel extends AbstractWebHooksHtmlPanel {

    @Autowired
    public JiraJQLPanel(final TemplateRenderer templateRenderer, final AutoCompleteJsonGenerator autoCompleteJsonGenerator, final ApplicationProperties applicationProperties, final HelpPathResolver helpPathResolver, final JiraAuthenticationContext jiraAuthenticationContext) {
        super("webhooks-jql.vm", templateRenderer, autoCompleteJsonGenerator, applicationProperties, helpPathResolver, jiraAuthenticationContext);
    }
}
