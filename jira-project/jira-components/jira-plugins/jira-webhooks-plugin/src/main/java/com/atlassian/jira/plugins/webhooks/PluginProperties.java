package com.atlassian.jira.plugins.webhooks;

public final class PluginProperties {
    private PluginProperties() {
    }

    public static final String PLUGIN_KEY = "com.atlassian.jira.plugins.webhooks.jira-webhooks-plugin";
}
