package com.atlassian.jira.plugins.webhooks.serializer;

import com.atlassian.crowd.event.user.UserCreatedEvent;
import com.atlassian.jira.plugins.webhooks.registration.RegisteredWebHookEventFactory;
import com.atlassian.jira.plugins.webhooks.serializer.bean.UserBeanFactory;
import com.atlassian.jira.user.ApplicationUsers;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserCreatedEventSerializer extends CrowdEventSerializer<UserCreatedEvent> {
    private final UserBeanFactory userBeanFactory;

    @Autowired
    protected UserCreatedEventSerializer(RegisteredWebHookEventFactory registeredWebHookEventFactory, UserBeanFactory userBeanFactory) {
        super(registeredWebHookEventFactory);
        this.userBeanFactory = userBeanFactory;
    }

    @Override
    protected ImmutableMap.Builder<String, Object> putFields(UserCreatedEvent event, ImmutableMap.Builder<String, Object> defaultJson) {
        return defaultJson.put("user", userBeanFactory.createBean(ApplicationUsers.from(event.getUser())));
    }
}
