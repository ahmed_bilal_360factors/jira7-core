package com.atlassian.jira.plugins.webhooks.url;

import com.atlassian.jira.issue.Issue;
import com.google.common.collect.ImmutableMap;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class IssueContextSerializer {
    public Map<String, Object> getContext(final Issue issue) {
        return ImmutableMap.<String, Object>of("issue", ImmutableMap.of(
                "key", issue.getKey(), "id", issue.getId()
        ));
    }
}
