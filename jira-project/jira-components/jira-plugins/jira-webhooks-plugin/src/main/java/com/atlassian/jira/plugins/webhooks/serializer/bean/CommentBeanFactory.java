package com.atlassian.jira.plugins.webhooks.serializer.bean;

import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.fields.rest.json.beans.CommentJsonBean;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.util.EmailFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("webhooksCommentBeanFactory")
public class CommentBeanFactory {

    private final JiraBaseUrls jiraBaseUrls;
    private final EmailFormatter emailFormatter;
    private final ProjectRoleManager projectRoleManager;

    @Autowired
    public CommentBeanFactory(final JiraBaseUrls jiraBaseUrls,
            final EmailFormatter emailFormatter,
            final ProjectRoleManager projectRoleManager) {
        this.jiraBaseUrls = jiraBaseUrls;
        this.emailFormatter = emailFormatter;
        this.projectRoleManager = projectRoleManager;
    }

    public CommentJsonBean createBean(final Comment comment) {
        // ApplicationUser is set to null so author's email address will not be included in the bean
        // if email visibility is not set public in JIRA.
        return CommentJsonBean.shortBean(comment, jiraBaseUrls, projectRoleManager, null, emailFormatter);
    }
}
