package com.atlassian.jira.plugins.webhooks.ao.upgrade;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.activeobjects.external.ActiveObjectsUpgradeTask;
import com.atlassian.activeobjects.external.ModelVersion;
import net.java.ao.Entity;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.StringLength;
import net.java.ao.schema.Table;

import java.util.Date;

public class UpgradeTask_v1 implements ActiveObjectsUpgradeTask {
    @Override
    public ModelVersion getModelVersion() {
        return ModelVersion.valueOf("1");
    }

    @Override
    public void upgrade(ModelVersion currentVersion, ActiveObjects ao) {
        ao.migrate(IntermediateWebhookDao.class);
        for (IntermediateWebhookDao webhookDao : ao.find(IntermediateWebhookDao.class)) {
            webhookDao.setFilter(webhookDao.getJql());
            webhookDao.save();
        }
    }

    @Table("WebhookDao")
    interface IntermediateWebhookDao extends Entity {
        String getJql();

        void setJql(String jql);

        @NotNull
        String getLastUpdatedUser();

        void setLastUpdatedUser(String username);

        @NotNull
        @StringLength(StringLength.UNLIMITED)
        String getUrl();

        void setUrl(String url);

        @NotNull
        Date getLastUpdated();

        void setLastUpdated(Date updated);

        @NotNull
        @StringLength(StringLength.UNLIMITED)
        String getName();

        void setName(String name);

        @StringLength(StringLength.UNLIMITED)
        String getFilter();

        void setFilter(String filter);

        // Was this created via REST, the UI, or something else?
        @NotNull
        String getRegistrationMethod();

        void setRegistrationMethod(String method);

        /* Unfortunately, according to https://answers.atlassian.com/questions/45149/activeobjects-jira
             * ActiveObjects can do one-to-many mappings of primitives, so we need to wrap it in another entity.
             * That seems like overkill. On top of that, attempting to do that resulted in exceptions from ActiveObject's
             * TypeMatter suggesting some sort of regression.
             *
             * Instead we will handle this ourselves.
             */
        @StringLength(StringLength.UNLIMITED)
        String getEncodedEvents();

        void setEncodedEvents(String events);

        boolean isEnabled();

        void setEnabled(boolean enabled);

    }
}