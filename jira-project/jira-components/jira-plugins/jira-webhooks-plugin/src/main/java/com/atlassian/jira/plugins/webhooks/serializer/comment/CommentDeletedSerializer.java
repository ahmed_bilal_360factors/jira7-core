package com.atlassian.jira.plugins.webhooks.serializer.comment;

import java.time.Instant;
import java.util.Date;

import com.atlassian.jira.event.comment.CommentDeletedEvent;
import com.atlassian.jira.plugins.webhooks.registration.RegisteredWebHookEventFactory;
import com.atlassian.jira.plugins.webhooks.serializer.bean.CommentBeanFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CommentDeletedSerializer extends AbstractCommentSerializer<CommentDeletedEvent> {

    @Autowired
    public CommentDeletedSerializer(final RegisteredWebHookEventFactory registeredWebHookEventFactory,
            final CommentBeanFactory commentBeanFactory) {
        super(registeredWebHookEventFactory, commentBeanFactory);
    }

    @Override
    protected Long getTimestamp(final CommentDeletedEvent event) {
        return Instant.now().toEpochMilli();
    }
}
