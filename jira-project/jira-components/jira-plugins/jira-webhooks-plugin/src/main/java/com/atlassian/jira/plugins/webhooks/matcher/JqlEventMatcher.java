package com.atlassian.jira.plugins.webhooks.matcher;

import com.atlassian.jira.event.issue.IssueRelatedEvent;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.jql.parser.JqlParseException;
import com.atlassian.jira.jql.parser.JqlQueryParser;
import com.atlassian.jira.plugins.webhooks.validation.JqlValidationUtil;
import com.atlassian.query.Query;
import com.atlassian.webhooks.api.register.listener.WebHookListener;
import com.atlassian.webhooks.spi.EventMatcher;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Matches event's underlying issue with given JQL.
 */
@Component
public class JqlEventMatcher<T extends IssueRelatedEvent> implements EventMatcher<T> {
    private final SearchProvider searchProvider;
    private final JqlQueryParser jqlQueryParser;
    private final JqlValidationUtil jqlValidationUtil;

    private static final Logger slowLog = Logger.getLogger(JqlEventMatcher.class.getName() + "_SLOW");
    private static final Logger log = Logger.getLogger(JqlEventMatcher.class.getName());

    @Autowired
    public JqlEventMatcher(SearchProvider searchProvider, JqlQueryParser jqlQueryParser, JqlValidationUtil jqlValidationUtil) {
        this.searchProvider = searchProvider;
        this.jqlQueryParser = jqlQueryParser;
        this.jqlValidationUtil = jqlValidationUtil;
    }

    /*
     * Filtering is used for Events as well as for Transitions so the method accepts both IssueRelatedEvent and Issue.
     */
    @Override
    public boolean matches(final T event, final WebHookListener listener) {
        return matchJql(listener.getParameters().getFilter(), event.getIssue());
    }

    public boolean matchJql(final String filter, final Issue issue) {
        if (StringUtils.isBlank(filter)) {
            return true;
        }
        if (!jqlValidationUtil.isJqlValid(filter).isRight()) {
            log.error(String.format("The following JQL query '%-1.800s' defined for filtering webhook events is incorrect. Filtering has been disabled for webhook.", filter));
            return true;
        }
        try {
            final Query query = getQuery(issue.getKey(), filter);
            final long startTime = System.currentTimeMillis();

            final boolean matches = searchProvider.searchCountOverrideSecurity(query, null) > 0;
            long queryTime = System.currentTimeMillis() - startTime;

            if (queryTime > 50) {
                String logMessage = String.format("JQL query '%-1.800s' produced lucene query and took '%d' ms to run.", query.toString(), queryTime);
                slowLog.info(logMessage);
            }
            return matches;
        } catch (SearchException e) {
            return false;
        } catch (JqlParseException e) {
            log.error("Query could not be parsed", e);
            return false;
        }
    }

    private Query getQuery(String issueKey, final String filter) throws JqlParseException {
        final JqlQueryBuilder queryBuilder = JqlQueryBuilder.newBuilder(jqlQueryParser.parseQuery(filter));
        return queryBuilder.where().and().issue(issueKey).buildQuery();
    }

}
