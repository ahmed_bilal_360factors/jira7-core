package com.atlassian.jira.plugins.webhooks.url;

import com.atlassian.jira.event.project.AbstractVersionEvent;
import com.atlassian.jira.project.Project;
import com.atlassian.webhooks.api.document.ProvidesUrlVariables;
import com.atlassian.webhooks.spi.UriVariablesProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class VersionEventVariablesProvider implements UriVariablesProvider<AbstractVersionEvent> {
    private final VersionAndProjectContextSerializer contextSerializer;

    @Autowired
    public VersionEventVariablesProvider(final VersionAndProjectContextSerializer contextSerializer) {
        this.contextSerializer = contextSerializer;
    }

    @ProvidesUrlVariables({"project.key", "project.id", "version.id"})
    @Override
    public Map<String, Object> uriVariables(AbstractVersionEvent event) {
        final Project project = event.getVersion().getProjectObject();
        return contextSerializer.getContext(project, event.getVersion());
    }
}
