package com.atlassian.jira.plugins.webhooks.listener;

import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.api.util.SectionKey;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Date;

/**
 * This class contains useful methods for retrieving and storing JQL filters.
 * <p>
 * <p>
 * Filters in atlassian-webhooks are handled differently then in jira-webhooks on the store level so
 * we need to maintain a kind of compatibility layer between internal jira store representation
 * and {@link com.atlassian.webhooks.api.register.listener.PersistentWebHookListener} objects.
 * That's what this class does.
 * </p>
 * <p>
 * <p>
 * The idea is that all operations concerning retrieving and storing JQL filters
 * are encapsulated here so that they are easy to track and reason about.
 * </p>
 */
public final class JqlFilterStoreUtils {
    private static SectionKey ISSUES_SECTION = new SectionKey("issue-related-events-section");

    public static final class JqlAwarePersistentListenerBuilder {
        private final PersistentWebHookListener.Builder builder;

        private JqlAwarePersistentListenerBuilder(final PersistentWebHookListener.Builder builder) {
            this.builder = builder;
        }

        public JqlAwarePersistentListenerBuilder setListenerName(final String listenerName) {
            builder.setListenerName(listenerName);
            return this;
        }

        public JqlAwarePersistentListenerBuilder setLastUpdated(final Date lastUpdated) {
            builder.setLastUpdated(lastUpdated);
            return this;
        }

        public JqlAwarePersistentListenerBuilder addWebHookIds(final Collection<String> webHookIds) {
            builder.addWebHookIds(webHookIds);
            return this;
        }

        public JqlAwarePersistentListenerBuilder setLastUpdatedByUser(final String lastUpdatedByUser) {
            builder.setLastUpdatedByUser(lastUpdatedByUser);
            return this;
        }

        public JqlAwarePersistentListenerBuilder setUrl(final String url) {
            builder.setUrl(url);
            return this;
        }

        public JqlAwarePersistentListenerBuilder setExcludeBody(final boolean excludeBody) {
            builder.setExcludeBody(excludeBody);
            return this;
        }

        public JqlAwarePersistentListenerBuilder setEnabled(final boolean enabled) {
            builder.setEnabled(enabled);
            return this;
        }

        public JqlAwarePersistentListenerBuilder setDescription(final String description) {
            builder.setDescription(description);
            return this;
        }

        public JqlAwarePersistentListenerBuilder setJqlFilter(String filter) {
            builder.addFilter(ISSUES_SECTION.getValue(), filter);
            return this;
        }

        public PersistentWebHookListener build() {
            return builder.build();
        }
    }

    /**
     * Create a builder for a new {@link com.atlassian.webhooks.api.register.listener.PersistentWebHookListener} object.
     * The builder differs from the default one in that it has a method to set JQL filter.
     *
     * @param id id of listener existing in the jira listener store
     * @return a JQL filter aware builder for {@link com.atlassian.webhooks.api.register.listener.PersistentWebHookListener} objects
     */
    public static JqlAwarePersistentListenerBuilder persistentListenerBuilder(@Nonnull Integer id) {
        return new JqlAwarePersistentListenerBuilder(PersistentWebHookListener.existing(id));
    }

    /**
     * Retreves a JQL filter from the listener.
     *
     * @param listener a listener
     * @return JQL filter or empty string if not defined
     */
    public static String getJqlFilter(PersistentWebHookListener listener) {
        return listener.getFilterFor(ISSUES_SECTION);
    }
}
