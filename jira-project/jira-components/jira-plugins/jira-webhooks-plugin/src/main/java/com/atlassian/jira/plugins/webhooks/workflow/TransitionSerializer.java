package com.atlassian.jira.plugins.webhooks.workflow;

import com.atlassian.plugins.rest.common.json.JacksonJsonProviderFactory;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.codehaus.jackson.map.ObjectMapper;

import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.Map;

/**
 *
 */
public class TransitionSerializer {
    private final JacksonJsonProvider jsonProvider;
    private final Map<String, Object> data;

    public TransitionSerializer(Map<String, Object> data) {
        this.data = data;
        jsonProvider = new JacksonJsonProviderFactory().create();
    }

    public String getJson() throws IOException {
        final ObjectMapper objectMapper = jsonProvider.locateMapper(data.getClass(), MediaType.APPLICATION_JSON_TYPE);

        return objectMapper.writeValueAsString(data);
    }
}

