package com.atlassian.jira.plugins.webhooks.serializer.bean;

import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.fields.rest.json.beans.ProjectJsonBean;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectAssigneeTypes;
import com.atlassian.jira.project.ProjectCategory;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProjectBeanFactory {
    private final JiraBaseUrls urls;
    private final UserManager userManager;
    private final UserBeanFactory userBeanFactory;
    private final ProjectCategoryBeanFactory projectCategoryBeanFactory;

    @Autowired
    public ProjectBeanFactory(final UserManager userManager,
                              final JiraBaseUrls urls,
                              final UserBeanFactory userBeanFactory,
                              final ProjectCategoryBeanFactory projectCategoryBeanFactory) {
        this.userManager = userManager;
        this.urls = urls;
        this.userBeanFactory = userBeanFactory;
        this.projectCategoryBeanFactory = projectCategoryBeanFactory;
    }

    public ProjectBean createBean(final Project project) {
        return new ProjectBean(createSelfLink(project),
                project.getId(),
                project.getKey(),
                project.getName(),
                ProjectJsonBean.getAvatarUrls(project),
                createProjectCategoryBean(project.getProjectCategoryObject()),
                createLeadBean(project),
                ProjectAssigneeTypes.getPrettyAssigneeType(project.getAssigneeType())
        );
    }

    private String createSelfLink(Project project) {
        return urls.restApi2BaseUrl() + "project/" + project.getId().toString();
    }

    private ProjectCategoryBean createProjectCategoryBean(final ProjectCategory projectCategory) {
        return projectCategoryBeanFactory.createBean(projectCategory);
    }

    private UserBean createLeadBean(Project project) {
        final ApplicationUser user = userManager.getUserByKeyEvenWhenUnknown(project.getLeadUserKey());
        return userBeanFactory.createBean(user);
    }

}