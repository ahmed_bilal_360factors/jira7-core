package com.atlassian.jira.plugins.webhooks.serializer;

import com.atlassian.crowd.event.DirectoryEvent;
import com.atlassian.jira.plugins.webhooks.registration.RegisteredWebHookEventFactory;
import org.springframework.beans.factory.annotation.Autowired;

abstract class CrowdEventSerializer<T extends DirectoryEvent> extends AbstractJiraEventSerializer<T> {
    @Autowired
    protected CrowdEventSerializer(RegisteredWebHookEventFactory registeredWebHookEventFactory) {
        super(registeredWebHookEventFactory);
    }

    @Override
    protected final Long getTimestamp(T event) {
        return event.getTimestamp();
    }
}
