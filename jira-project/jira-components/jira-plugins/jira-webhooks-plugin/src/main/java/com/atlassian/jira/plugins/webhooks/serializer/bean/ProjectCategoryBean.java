package com.atlassian.jira.plugins.webhooks.serializer.bean;

import org.codehaus.jackson.annotate.JsonProperty;

public class ProjectCategoryBean {
    @JsonProperty
    private String self;

    @JsonProperty
    private Long id;

    @JsonProperty
    private String name;

    @JsonProperty
    private String description;

    public ProjectCategoryBean(final String self, final Long id, final String name, final String description) {
        this.self = self;
        this.id = id;
        this.description = description;
        this.name = name;
    }

    public String getSelf() {
        return self;
    }

    public Long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

}