package com.atlassian.jira.plugins.webhooks.serializer.bean;

import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.JiraUrlCodec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("WebHookUserBeanFactory")
public class UserBeanFactory {
    private final JiraBaseUrls jiraBaseUrls;

    @Autowired
    public UserBeanFactory(final JiraBaseUrls jiraBaseUrls) {
        this.jiraBaseUrls = jiraBaseUrls;
    }

    public UserBean createBean(final ApplicationUser user) {
        return new UserBean(createSelfLink(user),
                user.getName(),
                user.getKey(),
                user.getEmailAddress(),
                user.getDisplayName()
        );
    }

    private String createSelfLink(ApplicationUser user) {
        return jiraBaseUrls.restApi2BaseUrl() + "user?key=" + JiraUrlCodec.encode(user.getKey());
    }
}