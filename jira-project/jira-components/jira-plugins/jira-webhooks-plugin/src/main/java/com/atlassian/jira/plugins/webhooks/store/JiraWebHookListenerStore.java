package com.atlassian.jira.plugins.webhooks.store;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.fugue.Option;
import com.atlassian.jira.plugins.webhooks.ao.WebhookDao;
import com.atlassian.jira.plugins.webhooks.listener.JqlFilterStoreUtils;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.api.register.listener.RegistrationMethod;
import com.atlassian.webhooks.spi.WebHookListenerStore;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import net.java.ao.DBParam;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Lists.newArrayList;

public class JiraWebHookListenerStore implements WebHookListenerStore {
    private static final Logger log = Logger.getLogger(JiraWebHookListenerStore.class.getName());

    private final ActiveObjects ao;
    private final UserManager userManager;
    private final I18nResolver i18n;

    public JiraWebHookListenerStore(final ActiveObjects ao, final UserManager userManager, final I18nResolver i18n) {
        this.i18n = checkNotNull(i18n);
        this.ao = checkNotNull(ao);
        this.userManager = checkNotNull(userManager);
    }

    @Override
    public PersistentWebHookListener addWebHook(final PersistentWebHookListener listener, final RegistrationMethod registrationMethod) {
        final WebhookDao webhookDao = ao.executeInTransaction(new TransactionCallback<WebhookDao>() {
            @Override
            public WebhookDao doInTransaction() {
                final WebhookDao webHookDao = ao.create(WebhookDao.class,
                        new DBParam("LAST_UPDATED_USER", userManager.getRemoteUsername()),
                        new DBParam("URL", listener.getUrl()),
                        new DBParam("LAST_UPDATED", new Date()),
                        new DBParam("NAME", listener.getName()),
                        new DBParam("FILTER", JqlFilterStoreUtils.getJqlFilter(listener)),
                        new DBParam("REGISTRATION_METHOD", registrationMethod.toString()),
                        new DBParam("ENABLED", true),
                        new DBParam("EXCLUDE_ISSUE_DETAILS", listener.isExcludeBody())
                );
                webHookDao.setEvents(Iterables.toArray(listener.getEvents(), String.class));
                webHookDao.save();
                return webHookDao;
            }
        });
        return createWebHookListener(webhookDao);
    }

    @Override
    public PersistentWebHookListener updateWebHook(final PersistentWebHookListener listener)
            throws IllegalArgumentException {
        return createWebHookListener(ao.executeInTransaction(new TransactionCallback<WebhookDao>() {
            @Override
            public WebhookDao doInTransaction() {
                final WebhookDao webHookDao = ao.get(WebhookDao.class, listener.getId().get());
                if (webHookDao == null) {
                    throw new IllegalArgumentException(i18n.getText("webhooks.invalid.webhook.id"));
                }

                webHookDao.setName(listener.getName());
                webHookDao.setUrl(listener.getUrl());
                webHookDao.setEvents(listener.getEvents().toArray(new String[listener.getEvents().size()]));
                webHookDao.setFilter(JqlFilterStoreUtils.getJqlFilter(listener));
                webHookDao.setExcludeIssueDetails(listener.isExcludeBody());
                webHookDao.setLastUpdatedUser(userManager.getRemoteUsername());
                webHookDao.setLastUpdated(new Date());
                webHookDao.setEnabled(listener.isEnabled());

                webHookDao.save();

                return webHookDao;
            }
        }));
    }

    @Override
    public Option<PersistentWebHookListener> getWebHook(final int id) {
        return ao.executeInTransaction(new TransactionCallback<Option<PersistentWebHookListener>>() {
            @Override
            public Option<PersistentWebHookListener> doInTransaction() {
                return Option.option(createWebHookListener(ao.get(WebhookDao.class, id)));
            }
        });
    }

    @Override
    public void removeWebHook(final int id) throws IllegalArgumentException {
        ao.executeInTransaction(new TransactionCallback<Object>() {
            @Override
            public Object doInTransaction() {
                final WebhookDao webHookDao = ao.get(WebhookDao.class, id);
                if (webHookDao == null) {
                    throw new IllegalArgumentException(i18n.getText("webhooks.invalid.webhook.id"));
                }
                ao.delete(webHookDao);
                return webHookDao;
            }
        });
    }

    @Override
    public Collection<PersistentWebHookListener> getAllWebHooks() {
        return newArrayList(transform(ao.executeInTransaction(new TransactionCallback<Collection<WebhookDao>>() {
                    @Override
                    public Collection<WebhookDao> doInTransaction() {
                        return Arrays.asList(ao.find(WebhookDao.class));
                    }
                }), new Function<WebhookDao, PersistentWebHookListener>() {
                    @Override
                    public PersistentWebHookListener apply(final WebhookDao webhookDao) {
                        return createWebHookListener(webhookDao);
                    }
                }
        ));
    }

    private PersistentWebHookListener createWebHookListener(final WebhookDao webhookDao) {
        int id = webhookDao.getID();

        String url = ensureStringNotNull(webhookDao.getUrl(), id, "url");
        String name = ensureStringNotNull(webhookDao.getName(), id, "name");
        String filter = ensureStringNotNull(webhookDao.getFilter(), id, "filter");
        String[] events = ensureNotNull(webhookDao.getEvents(), id, "events", new String[0]);
        String lastUpdatedUser = ensureStringNotNull(webhookDao.getLastUpdatedUser(), id, "lastUpdatedUser");
        Date lastUpdated = ensureNotNull(webhookDao.getLastUpdated(), id, "lastUpdated", new Date());

        return JqlFilterStoreUtils.persistentListenerBuilder(id)
                .setUrl(url)
                .setEnabled(webhookDao.isEnabled())
                .addWebHookIds(Arrays.asList(events))
                .setLastUpdated(lastUpdated)
                .setLastUpdatedByUser(lastUpdatedUser)
                .setListenerName(name)
                .setExcludeBody(webhookDao.getExcludeIssueDetails())
                .setJqlFilter(filter)
                .build();
    }

    private String ensureStringNotNull(String value, int id, String valueName) {
        return ensureNotNull(value, id, valueName, "");
    }

    private <T> T ensureNotNull(T value, int id, String valueName, T emptyValue) {
        if (value == null) {
            log.warn(String.format("%s was NULL for webhook listener %d but shouldn't have been, defaulting to '%s'", valueName, id, emptyValue));
            return emptyValue;
        } else {
            return value;
        }
    }
}
