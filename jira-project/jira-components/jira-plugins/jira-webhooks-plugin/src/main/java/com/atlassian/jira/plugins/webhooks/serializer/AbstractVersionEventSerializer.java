package com.atlassian.jira.plugins.webhooks.serializer;

import com.atlassian.jira.event.project.AbstractVersionEvent;
import com.atlassian.jira.plugins.webhooks.registration.RegisteredWebHookEventFactory;
import com.atlassian.jira.rest.v2.issue.version.VersionBean;
import com.google.common.collect.ImmutableMap;

import java.util.Date;

abstract class AbstractVersionEventSerializer<T extends AbstractVersionEvent> extends AbstractJiraEventSerializer<T> {
    protected final SimpleVersionBeanFactory versionBeanFactory;

    public AbstractVersionEventSerializer(final RegisteredWebHookEventFactory registeredWebHookEventFactory, final SimpleVersionBeanFactory versionBeanFactory) {
        super(registeredWebHookEventFactory);
        this.versionBeanFactory = versionBeanFactory;
    }

    @Override
    protected Long getTimestamp(AbstractVersionEvent event) {
        return new Date().getTime();
    }

    @Override
    protected ImmutableMap.Builder<String, Object> putFields(T event, ImmutableMap.Builder<String, Object> defaultJson) {
        VersionBean versionBean = versionBeanFactory.createBean(event.getVersion());
        defaultJson.put("version", versionBean);
        return defaultJson;
    }
}
