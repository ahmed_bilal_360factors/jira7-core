package com.atlassian.jira.plugins.webhooks.serializer;

import com.atlassian.plugins.rest.common.json.JacksonJsonProviderFactory;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;

import javax.ws.rs.core.MediaType;
import java.io.IOException;

/**
 * Use Jackson's ObjectMapper to serialise java objects to JSON strings.
 */
public class JacksonSerializer {
    private static final JacksonJsonProvider JACKSON_JSON_PROVIDER = new JacksonJsonProviderFactory().create();

    public static String serialize(Object object) {
        try {
            return JACKSON_JSON_PROVIDER.locateMapper(object.getClass(), MediaType.APPLICATION_JSON_TYPE).writeValueAsString(object);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}