package com.atlassian.jira.plugins.webhooks.matcher;

import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.webhooks.api.register.listener.WebHookListener;
import com.atlassian.webhooks.spi.EventMatcher;

import javax.annotation.concurrent.Immutable;
import java.util.Set;

/**
 * Matcher for IssueEvent. Matches for event class and for event ID.
 *
 * @since v1.1
 */
@Immutable
public final class IssueEventIdMatcher implements EventMatcher<IssueEvent> {
    private final Set<Long> eventIds;

    public IssueEventIdMatcher(final Set<Long> eventIds) {
        this.eventIds = eventIds;
    }

    @Override
    public boolean matches(final IssueEvent issueEvent, final WebHookListener consumerParams) {
        return eventIds.contains(issueEvent.getEventTypeId());
    }
}
