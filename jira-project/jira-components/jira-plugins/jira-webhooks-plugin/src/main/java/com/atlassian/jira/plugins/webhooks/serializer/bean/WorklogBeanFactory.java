package com.atlassian.jira.plugins.webhooks.serializer.bean;

import com.atlassian.jira.bc.issue.worklog.TimeTrackingConfiguration;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.fields.rest.json.beans.WorklogJsonBean;
import com.atlassian.jira.issue.worklog.Worklog;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.EmailFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WorklogBeanFactory {
    private final JiraBaseUrls jiraBaseUrls;
    private final UserManager userManager;
    private final TimeTrackingConfiguration timeTrackingConfiguration;
    private final EmailFormatter emailFormatter;

    @Autowired
    public WorklogBeanFactory(final JiraBaseUrls jiraBaseUrls,
                              final UserManager userManager,
                              final TimeTrackingConfiguration timeTrackingConfiguration,
                              final EmailFormatter emailFormatter) {
        this.jiraBaseUrls = jiraBaseUrls;
        this.userManager = userManager;
        this.timeTrackingConfiguration = timeTrackingConfiguration;
        this.emailFormatter = emailFormatter;
    }

    public WorklogJsonBean createBean(final Worklog worklog) {
        // ApplicationUser is set to null so author's email address will not be included in the bean
        // if email visibility is not set public in JIRA.

        return WorklogJsonBean.getWorklog(worklog, jiraBaseUrls, userManager, timeTrackingConfiguration, null, emailFormatter);
    }
}
