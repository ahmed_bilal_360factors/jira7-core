package com.atlassian.jira.plugins.webhooks.matcher;

import com.atlassian.jira.event.property.AbstractApplicationPropertySetEvent;
import com.atlassian.webhooks.api.register.listener.WebHookListener;
import com.atlassian.webhooks.spi.EventMatcher;

import javax.annotation.concurrent.Immutable;

@Immutable
public final class ApplicationPropertyEventMatcher implements EventMatcher<AbstractApplicationPropertySetEvent> {
    private final String propertyKey;

    public ApplicationPropertyEventMatcher(final String key) {
        this.propertyKey = key;
    }

    @Override
    public boolean matches(final AbstractApplicationPropertySetEvent abstractApplicationPropertySetEvent,
                           final WebHookListener webHookListener) {
        return abstractApplicationPropertySetEvent.getPropertyKey().equals(propertyKey);
    }
}
