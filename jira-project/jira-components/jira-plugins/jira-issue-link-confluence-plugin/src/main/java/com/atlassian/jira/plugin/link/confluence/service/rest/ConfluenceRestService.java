package com.atlassian.jira.plugin.link.confluence.service.rest;

import com.atlassian.jira.plugin.link.confluence.service.ConfluenceRemoteService;

/**
 * Interact with a remote Confluence instance via its REST API.
 *
 * @since v7.3.2
 */
public interface ConfluenceRestService extends ConfluenceRemoteService {
}
