package com.atlassian.jira.plugin.link.confluence.service.rest;

import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Extends the normal REST response handler with functionality
 * appropriate for parsing the format of responses for collections of data.
 *
 * @since v7.3.3
 */
public abstract class AbstractConfluenceRestCollectionResponseHandler<T>
        extends AbstractConfluenceRestResponseHandler<List<T>> {
    private static final Logger log = LoggerFactory.getLogger(AbstractConfluenceRestCollectionResponseHandler.class);

    private static final Map<Integer, String> errorsForStatuses = MapBuilder.<Integer, String>newBuilder()
            .add(400, "searchconfluence.error.bad.request")
            .add(401, "searchconfluence.error.forbidden")
            .add(403, "searchconfluence.error.forbidden")
            .add(404, "searchconfluence.error.not.found")
            .add(500, "addconfluencelink.search.servererror")
            .add(503, "addconfluencelink.search.servererror")
            .add(0, "addconfluencelink.search.commserror")
            .toHashMap();

    @Override
    List<T> parseJsonResponse(JSONObject responseObj, ErrorCollection errors) throws JSONException {
        final Integer statusCode = responseObj.has("statusCode") ? responseObj.getInt("statusCode") : 200;
        final String message = responseObj.has("message") ? responseObj.getString("message") : null;
        List<T> results = null;

        if (errorsForStatuses.containsKey(statusCode)) {
            errors.addErrorMessage(i18n(errorsForStatuses.get(statusCode), message));
        } else {
            if (StringUtils.isNotBlank(message)) {
                log.debug("Confluence REST API returned a non-empty message for a 'successful' response: ", message);
            } else {
                log.debug("Confluence REST API returned something we can use.");
            }
            results = processResults(responseObj, errors);
        }

        return results;
    }

    List<T> processResults(JSONObject responseObj, ErrorCollection errors) throws JSONException {
        final List<T> results = Lists.newArrayList();

        final Integer size = responseObj.has("size") ? responseObj.getInt("size") : null;
        if (size == null || size <= 0) {
            log.debug("Confluence returned zero results");
        } else {
            final List<JSONObject> jsonSearchObjects = convertToList(responseObj.getJSONArray("results"));
            final List<T> confSearchObjects = jsonSearchObjects.stream()
                    .map(this::processResult)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
            final int resultsInError = jsonSearchObjects.size() - confSearchObjects.size();
            if (resultsInError > 0) {
                errors.addErrorMessage(i18n("searchconfluence.error.partial.result.parsing", resultsInError));
            }
            results.addAll(confSearchObjects);
        }
        return results;
    }

    @VisibleForTesting
    abstract T processResult(JSONObject object);

    static List<JSONObject> convertToList(@Nonnull JSONArray results) {
        final List<JSONObject> resultsList = Lists.newArrayList();
        for (int i = 0; i < results.length(); i++) {
            resultsList.add(results.optJSONObject(i));
        }
        return resultsList;
    }
}
