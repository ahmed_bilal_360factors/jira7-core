package com.atlassian.jira.plugin.link.confluence.rest;

import javax.xml.bind.annotation.XmlElement;
import java.util.List;

/**
 * Bean to represent Confluence search results.
 *
 * @since v5.0
 */
@SuppressWarnings({"UnusedDeclaration", "FieldCanBeLocal"})
public class ConfluenceSearchResponseBean {
    @XmlElement
    private List<Result> result;

    public ConfluenceSearchResponseBean(final List<Result> result) {
        this.result = result;
    }

    public static class Result {
        @XmlElement
        private String id;

        @XmlElement
        private String type;

        /**
         * Sanitised, text-only title from the Confluence content.
         */
        @XmlElement
        private String title;

        /**
         * The Confluence content's excerpt. May contain HTML (e.g., marks for the matching characters).
         */
        @XmlElement
        private String titleContent;

        /**
         * Sanitised, text-only excerpt from the Confluence content.
         */
        @XmlElement
        private String excerpt;

        /**
         * The Confluence content's excerpt. May contain HTML (e.g., marks for the matching characters).
         */
        @XmlElement
        private String excerptContent;

        @XmlElement
        private String url;

        public Result(final String id, final String type, final String title, final String excerpt, final String url) {
            this(id, type, title, title, excerpt, excerpt, url);
        }

        public Result(
                final String id,
                final String type,
                final String title,
                final String titleContent,
                final String excerpt,
                final String excerptContent,
                final String url
        ) {
            this.id = id;
            this.type = type;
            this.title = title;
            this.titleContent = titleContent;
            this.excerpt = excerpt;
            this.excerptContent = excerptContent;
            this.url = url;
        }
    }
}
