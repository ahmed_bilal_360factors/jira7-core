package com.atlassian.jira.plugin.link.confluence.cql;

import com.atlassian.jira.plugin.link.confluence.Builder;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

/**
 * Helps with the construction of valid CQL queries to ask Confluence for information.
 *
 * @since v7.3.3
 */
public class ConfluenceCqlBuilder implements Builder<String> {
    private String textQuery;
    private String spaceKey;
    private final List<String> excludedContentTypes = Lists.newArrayList();

    public ConfluenceCqlBuilder textQuery(String textQuery) {
        this.textQuery = textQuery;
        return this;
    }

    public ConfluenceCqlBuilder spaceKey(String spaceKey) {
        this.spaceKey = spaceKey;
        return this;
    }

    public ConfluenceCqlBuilder excludeContentType(ConfluenceContentType contentType) {
        excludedContentTypes.add(contentType.getType());
        return this;
    }

    @Override
    public String build() {
        return constructCqlQuery(textQuery, spaceKey, excludedContentTypes);
    }

    @Override
    public void clear() {
        textQuery = null;
        spaceKey = null;
        excludedContentTypes.clear();
    }

    /**
     *
     * @param textQuery
     * @param spaceKey
     * @param excludeContentTypes
     * @return
     */
    private String constructCqlQuery(String textQuery, @Nullable String spaceKey, @Nonnull List<String> excludeContentTypes) {
        final List<String> cqlTerms = Lists.newArrayList();
        cqlTerms.add(String.format("text ~ \"%s\"", esc(textQuery)));
        if (StringUtils.isNotBlank(spaceKey)) {
            cqlTerms.add(String.format("space = \"%s\"", esc(spaceKey)));
        }
        if (!excludeContentTypes.isEmpty()) {
            cqlTerms.add(String.format("type NOT IN (%s)", String.join(",", excludeContentTypes)));
        }
        return String.join(" AND ", cqlTerms);
    }

    private String esc(@Nonnull String val) {
        return StringUtils.replace(val, "\"", "\\\"");
    }
}
