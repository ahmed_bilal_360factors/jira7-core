package com.atlassian.jira.plugin.link.confluence.service.rest;

import com.atlassian.annotations.Internal;
import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.event.ApplicationLinksIDChangedEvent;
import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.event.api.EventListener;
import com.atlassian.jira.CachingComponent;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.plugin.link.applinks.RemoteResponse;
import com.atlassian.jira.plugin.link.confluence.ConfluencePage;
import com.atlassian.jira.plugin.link.confluence.ConfluenceSearchResult;
import com.atlassian.jira.plugin.link.confluence.ConfluenceSpace;
import com.atlassian.jira.plugin.link.confluence.cql.ConfluenceContentType;
import com.atlassian.jira.plugin.link.confluence.cql.ConfluenceCqlBuilder;
import com.atlassian.jira.plugin.link.confluence.service.rpc.ConfluenceRpcService;
import com.atlassian.jira.util.UrlBuilder;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.ResponseException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Component
@ExportAsService
public class ConfluenceRestServiceImpl implements ConfluenceRestService, CachingComponent {
    private static final Logger log = LoggerFactory.getLogger(ConfluenceRestServiceImpl.class);
    private static final String CONTENT_URL = "rest/api/content/%s";

    private final ConfluenceRpcService confluenceRpcService;
    private final Cache<ApplicationId, Boolean> restSearchSupported;

    @Autowired
    public ConfluenceRestServiceImpl(ConfluenceRpcService confluenceRpcService,
                                     @ComponentImport final CacheManager cacheManager) {
        this.confluenceRpcService = confluenceRpcService;
        restSearchSupported = cacheManager.getCache(ConfluenceRestServiceImpl.class.getName() + ".restSearchSupported",
                null,
                new CacheSettingsBuilder().expireAfterAccess(30, TimeUnit.MINUTES).maxEntries(50).build());
    }

    @EventListener
    public void onClearCacheEvent(final ClearCacheEvent e) {
        log.debug("Clearing rest support cache because clear cache requested", e);
        clearCache();
    }

    @EventListener
    public void onAppLinkIdChangeEvent(final ApplicationLinksIDChangedEvent e) {
        log.debug("Clearing rest support cache because applink ID has changed", e);
        clearCache();
    }

    @Override
    @Internal
    public void clearCache() {
        restSearchSupported.removeAll();
    }

    @Override
    public RemoteResponse<ConfluencePage> getPage(ApplicationLink applicationLink, String pageId) throws CredentialsRequiredException, ResponseException {
        final ApplicationLinkRequestFactory requestFactory = applicationLink.createAuthenticatedRequestFactory();
        final ApplicationLinkRequest request = requestFactory.createRequest(Request.MethodType.GET, String.format(CONTENT_URL, pageId));
        return request.execute(new ConfluenceRestContentResponseHandler());
    }

    @Override
    public RemoteResponse<List<ConfluenceSpace>> getSpaces(ApplicationLink applicationLink) throws CredentialsRequiredException, ResponseException {
        return confluenceRpcService.getSpaces(applicationLink);
    }

    @Override
    public RemoteResponse<List<ConfluenceSearchResult>> search(ApplicationLink applicationLink, String query, int maxResults, @Nullable String spaceKey) throws CredentialsRequiredException, ResponseException {
        Boolean isSupported = restSearchSupported.get(applicationLink.getId(), () -> true);
        if (isSupported) {
            final RemoteResponse<List<ConfluenceSearchResult>> conf59searchResults = attemptSearchForConfluence_5_9_up(applicationLink, query, maxResults, spaceKey);
            // We want to trust the REST endpoint in all cases except for a 404;
            // the 404 is the only means we have of telling whether the instance supported the REST request or not.
            // Before you try and refactor this check and the cache away, be advised that there is no way
            // of retrieving the remote applink version unless you are an admin *and* inside the core applinks plugin.
            // So, there's no chance of checking the Confluence version here.
            if (conf59searchResults.getStatusCode() != 404) {
                return conf59searchResults;
            }
            log.warn(String.format("Remote Confluence instance '%s' does not support REST endpoint for search", applicationLink.getId()));
            restSearchSupported.put(applicationLink.getId(), false);
        }
        // Older confluence versions still have the XMLRPC API, so use that instead.
        return confluenceRpcService.search(applicationLink, query, maxResults, spaceKey);
    }

    /**
     * Confluence's REST API only included a generic search endpoint from 5.9 onwards.
     * Attempt the search
     * @return a RemoteResponse, either filled with results if the request worked, or with an error if the response 404'd.
     */
    private RemoteResponse<List<ConfluenceSearchResult>> attemptSearchForConfluence_5_9_up(ApplicationLink applicationLink, String query, int maxResults, @Nullable String spaceKey) throws CredentialsRequiredException, ResponseException {
        ConfluenceCqlBuilder cqlBuilder = new ConfluenceCqlBuilder();
        String cqlQuery = cqlBuilder
                .excludeContentType(ConfluenceContentType.ATTACHMENT)
                .excludeContentType(ConfluenceContentType.COMMENT)
                .textQuery(query)
                .spaceKey(spaceKey)
                .build();
        UrlBuilder builder = new UrlBuilder("rest/api/search");
        builder.addParameter("cql", cqlQuery);
        builder.addParameter("limit", maxResults);
        builder.addParameter("expand", "body");
        if (StringUtils.isNotBlank(spaceKey)) {
            final Map<String, Object> cqlContextMap = MapBuilder.build("spaceKey", spaceKey);
            final JSONObject cqlContext = new JSONObject(cqlContextMap);
            builder.addParameter("cqlcontext", cqlContext.toString());
        }

        final ApplicationLinkRequestFactory requestFactory = applicationLink.createAuthenticatedRequestFactory();
        final ApplicationLinkRequest request = requestFactory.createRequest(Request.MethodType.GET, builder.asUrlString());
        return request.execute(new ConfluenceRestSearchResponseHandler());
    }
}
