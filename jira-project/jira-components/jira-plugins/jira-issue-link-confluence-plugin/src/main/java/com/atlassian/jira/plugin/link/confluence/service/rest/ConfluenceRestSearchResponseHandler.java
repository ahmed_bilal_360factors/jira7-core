package com.atlassian.jira.plugin.link.confluence.service.rest;

import com.atlassian.jira.plugin.link.confluence.ConfluenceSearchResult;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Handles the JSON responses from Confluence's /rest/api/search endpoint,
 * converting them in to {@link ConfluenceSearchResult} objects.
 *
 * @since 7.3.3
 */
public class ConfluenceRestSearchResponseHandler
        extends AbstractConfluenceRestCollectionResponseHandler<ConfluenceSearchResult> {
    private static final Logger log = LoggerFactory.getLogger(ConfluenceRestSearchResponseHandler.class);

    @Override
    ConfluenceSearchResult processResult(JSONObject jsonObject) {
        final ConfluenceSearchResult.ConfluenceSearchResultBuilder builder = new ConfluenceSearchResult.ConfluenceSearchResultBuilder();
        try {
            if (jsonObject.has("entityType")) {
                // Confluence 5.9+
                final String entityType = jsonObject.getString("entityType");
                final JSONObject content = jsonObject.getJSONObject(entityType);
                builder.id(content.getString("id"));
                builder.type(content.getString("type"));

                final String url = fullConfluenceUrl(content.getJSONObject("_links"));
                builder.url(url);

                final String titleContent = mark(jsonObject.getString("title"));
                builder.titleContent(titleContent);
                builder.title(stripHtml(titleContent));

                final String excerptContent = mark(jsonObject.getString("excerpt"));
                builder.excerptContent(excerptContent);
                builder.excerpt(stripHtml(excerptContent));
            }
        } catch (JSONException e) {
            log.debug("Error processing JSON object for search result", jsonObject, e);
        }

        return builder.build();
    }

    private static String mark(String content) {
        if (StringUtils.isNotBlank(content)) {
            return Jsoup.clean(content, Whitelist.simpleText())
                    .replace("@@@endhl@@@", "</mark>")
                    .replace("@@@hl@@@", "<mark>");
        }
        return "";
    }

    private static String stripHtml(String content) {
        return Jsoup.parseBodyFragment(content).text();
    }
}
