package com.atlassian.jira.plugin.link.confluence.service.rest;

import com.atlassian.jira.plugin.link.confluence.ConfluencePage;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConfluenceRestContentResponseHandler extends AbstractConfluenceRestResponseHandler<ConfluencePage> {

    private static final Logger log = LoggerFactory.getLogger(ConfluenceRestContentResponseHandler.class);

    public ConfluencePage parseJsonResponse(JSONObject responseObj, ErrorCollection errors) throws JSONException {
        final Integer statusCode = responseObj.has("statusCode") ? responseObj.getInt("statusCode") : 200;
        final String message = responseObj.has("message") ? responseObj.getString("message") : null;

        if (statusCode == 404) {
            log.debug("Confluence REST API returned a 404 response; the content link is either invalid or the user has no permission to view it.");
            errors.addErrorMessage(i18n("searchconfluence.error.not.found"));
            return null;
        } else {
            if (StringUtils.isNotBlank(message)) {
                log.debug("Confluence REST API returned a non-empty message for a 'successful' response: ", message);
            } else {
                log.debug("Confluence REST API returned something we can use.");
            }
            final String fullUrl = fullConfluenceUrl(responseObj.getJSONObject("_links"));
            return new ConfluencePage(
                    responseObj.getString("id"),
                    responseObj.getString("title"),
                    fullUrl
            );
        }
    }
}
