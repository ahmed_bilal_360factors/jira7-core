package com.atlassian.jira.plugin.link.confluence.service.rest;

import com.atlassian.applinks.api.ApplicationLinkResponseHandler;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.plugin.link.applinks.RemoteResponse;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.UrlBuilder;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Convert HTTP Response objects in to POJOs representing Confluence data.
 *
 * @since 7.3.2
 */
public abstract class AbstractConfluenceRestResponseHandler<T> implements ApplicationLinkResponseHandler<RemoteResponse<T>> {
    private static final Logger log = LoggerFactory.getLogger(AbstractConfluenceRestResponseHandler.class);

    private String baseUrl = null;

    @Override
    public RemoteResponse<T> credentialsRequired(Response response) throws ResponseException {
        return RemoteResponse.credentialsRequired(response);
    }

    @Override
    public RemoteResponse<T> handle(Response response) throws ResponseException {
        final String body = response.getResponseBodyAsString();
        final ErrorCollection errors = new SimpleErrorCollection();
        T result = null;
        try {
            final JSONObject responseObj = new JSONObject(body);
            baseUrl = getBaseUrl(responseObj);
            result = parseJsonResponse(responseObj, errors);
        } catch (JSONException e) {
            log.debug("Encountered a JSON parsing problem for the Confluence REST API response...", e);
            errors.addErrorMessage(i18n("addconfluencelink.search.error"));
        }

        if (errors.hasAnyErrors()) {
            return new RemoteResponse<>(result, errors, response);
        } else {
            return new RemoteResponse<>(result, response);
        }
    }

    String i18n(final String i18nKey, Object... args) {
        final I18nHelper i18n = ComponentAccessor.getJiraAuthenticationContext().getI18nHelper();
        return i18n.getText(i18nKey, args);
    }

    protected String fullConfluenceUrl(final JSONObject links) throws JSONException {
        return new UrlBuilder(baseUrl)
                .addPathUnsafe(links.getString("webui"))
                .asUrlString();
    }

    protected String getBaseUrl(final JSONObject responseObj) throws JSONException {
        if (responseObj.has("_links")) {
            return responseObj.getJSONObject("_links").getString("base");
        }
        return null;
    }

    abstract T parseJsonResponse(JSONObject object, ErrorCollection errorCollection) throws JSONException;
}
