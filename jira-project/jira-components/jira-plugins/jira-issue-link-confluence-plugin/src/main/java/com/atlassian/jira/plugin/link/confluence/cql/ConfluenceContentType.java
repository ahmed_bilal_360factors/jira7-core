package com.atlassian.jira.plugin.link.confluence.cql;

/**
 * A list of the known Confluence content types as at Confluence 5.9.
 * Because of custom content types, the list is not finite. However, CQL
 * may only recognise a strict subset.
 *
 * Because of custom content types, the recommended usage of these values
 * is to blacklist specific ones in a CQL search, as opposed to searching
 * exclusively for a specific type.
 *
 * @since v7.3.3
 */
public enum ConfluenceContentType {
    ATTACHMENT("attachment"),
    COMMENT("comment"),
    PAGE("page"),
    BLOG_POST("blogpost");

    private final String type;

    ConfluenceContentType(final String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
