package com.atlassian.jira.plugin.link.confluence.service;

import com.atlassian.jira.config.FeatureFlag;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.plugin.link.confluence.service.rest.ConfluenceRestService;
import com.atlassian.jira.plugin.link.confluence.service.rpc.ConfluenceRpcService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.annotations.VisibleForTesting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.function.Supplier;

/**
 * Choose between using Confluence's XMLRPC API or its REST API.
 * It really is a factory that chooses the appropriate strategy for
 * connecting to a remote confluence instance as a service.
 * Thanks, gang of four!
 *
 * @since v7.3.2
 */
@Component
public class ConfluenceRemoteServiceStrategyFactory implements Supplier<ConfluenceRemoteService> {
    private static final String FEATURE_FLAG_KEY = "confluence.issuelinks.use.rest.api";
    @VisibleForTesting
    static final FeatureFlag FEATURE_FLAG = FeatureFlag.featureFlag(FEATURE_FLAG_KEY).defaultedTo(true);

    private final FeatureManager featureManager;
    private final ConfluenceRestService restService;
    private final ConfluenceRpcService rpcService;

    @Autowired
    public ConfluenceRemoteServiceStrategyFactory(
            final ConfluenceRestService restService,
            final ConfluenceRpcService rpcService,
            @ComponentImport final FeatureManager featureManager) {
        this.featureManager = featureManager;
        this.restService = restService;
        this.rpcService = rpcService;
    }

    @Override
    public ConfluenceRemoteService get() {
        if (shouldUseREST()) {
            return getRestService();
        }
        return getRpcService();
    }

    private boolean shouldUseREST() {
        return featureManager.isEnabled(FEATURE_FLAG);
    }

    private ConfluenceRestService getRestService() {
        return restService;
    }

    private ConfluenceRpcService getRpcService() {
        return rpcService;
    }
}
