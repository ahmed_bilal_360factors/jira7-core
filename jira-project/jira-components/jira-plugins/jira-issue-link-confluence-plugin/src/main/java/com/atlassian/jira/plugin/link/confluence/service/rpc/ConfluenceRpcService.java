package com.atlassian.jira.plugin.link.confluence.service.rpc;

import com.atlassian.jira.plugin.link.confluence.service.ConfluenceRemoteService;

/**
 * Helper class for making XMLRPC calls to Confluence servers.
 *
 * @since v5.0
 */
public interface ConfluenceRpcService extends ConfluenceRemoteService {
}
