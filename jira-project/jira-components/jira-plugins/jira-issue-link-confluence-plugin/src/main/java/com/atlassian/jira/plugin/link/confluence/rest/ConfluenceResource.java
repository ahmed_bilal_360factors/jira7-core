package com.atlassian.jira.plugin.link.confluence.rest;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.application.confluence.ConfluenceApplicationType;
import com.atlassian.fugue.Either;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.plugin.link.applinks.RemoteResponse;
import com.atlassian.jira.plugin.link.confluence.ConfluenceSearchResult;
import com.atlassian.jira.plugin.link.confluence.ConfluenceSearchResult.ConfluenceSearchResultBuilder;
import com.atlassian.jira.plugin.link.confluence.ConfluenceSpace;
import com.atlassian.jira.plugin.link.confluence.rest.ConfluenceSearchResponseBean.Result;
import com.atlassian.jira.plugin.link.confluence.rest.ConfluenceSpaceResponseBean.Space;
import com.atlassian.jira.plugin.link.confluence.service.ConfluenceRemoteServiceStrategyFactory;
import com.atlassian.jira.rest.api.pagination.PageBean;
import com.atlassian.jira.rest.api.util.ErrorCollection;
import com.atlassian.jira.util.BaseUrlSwapper;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.Page;
import com.atlassian.jira.util.PageRequest;
import com.atlassian.jira.util.PageRequests;
import com.atlassian.jira.util.Pages;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.sal.api.net.ResponseException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import java.util.List;

import static com.atlassian.jira.rest.api.http.CacheControl.never;
import static com.google.common.base.MoreObjects.firstNonNull;

/**
 * A REST resource to access Confluence's remote API. This is needed to perform authenticated requests.
 *
 * @since v5.0
 */
@AnonymousAllowed
@Path("confluence")
public class ConfluenceResource {
    /** The default maximum number of results a Confluence REST endpoint returns */
    static final int CONF_DEFAULT_MAX = 25;

    private final ConfluenceRemoteServiceStrategyFactory strategyFactory;
    private final ApplicationLinkService applicationLinkService;
    private final JiraBaseUrls jiraBaseUrls;
    private final I18nHelper i18nBean;

    @Autowired
    public ConfluenceResource(
            final ConfluenceRemoteServiceStrategyFactory confluenceRemoteServiceStrategyFactory,
            @ComponentImport final ApplicationLinkService applicationLinkService,
            @ComponentImport final JiraBaseUrls jiraBaseUrls,
            @ComponentImport final I18nHelper i18nBean) {
        this.strategyFactory = confluenceRemoteServiceStrategyFactory;
        this.applicationLinkService = applicationLinkService;
        this.jiraBaseUrls = jiraBaseUrls;
        this.i18nBean = i18nBean;
    }

    /**
     * Returns all spaces from a remote Confluence instance. Results are <a href="#pagination">paginated</a>.
     *
     * @param query          a string of text to search for in Confluence Space titles and descriptions.
     * @param appId          the applink id for the remote Confluence instance.
     * @param startAt        the page offset, if not specified then defaults to 0.
     * @param maxResults     how many results on the page should be included. Defaults to {@link #CONF_DEFAULT_MAX}.
     * @return the relevant Confluence Spaces.
     */
    @GET
    @Path("/space")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getSpaces(
            @QueryParam("query") final String query,
            @QueryParam("startAt") final Long startAt,
            @QueryParam("maxResults") final Integer maxResults,
            @QueryParam("appId") final String appId) {
        return getApplink(appId).left().on(appLink -> {
            try {
                final PageRequest pageRequest = PageRequests.request(startAt, firstNonNull(maxResults, CONF_DEFAULT_MAX));
                final RemoteResponse<List<ConfluenceSpace>> response = strategyFactory.get().getSpaces(appLink);
                if (!response.isSuccessful()) {
                    return handleUnsuccessfulResponse(response);
                }

                final Page<ConfluenceSpace> results = Pages.toPage(response.getEntity(), pageRequest);

                final String spaceSelfLink = getSpaceSelfLink(query, pageRequest, appId);
                final PageBean<Space> bean = PageBean.from(pageRequest, results)
                        .setLinks(spaceSelfLink, pageRequest.getLimit())
                        .build(this::makeSpace);
                return Response.ok(bean).cacheControl(never()).build();
            } catch (CredentialsRequiredException e) {
                return Response.status(Status.UNAUTHORIZED).cacheControl(never()).build();
            } catch (ResponseException e) {
                return Response.status(Status.INTERNAL_SERVER_ERROR).cacheControl(never()).build();
            }
        });
    }

    /**
     * Returns all matching content from a remote Confluence instance. Results are <a href="#pagination">paginated</a>.
     *
     * @param query          a string of text to search for anywhere in a piece of Confluence content.
     * @param spaceKey       if provided, will restrict the search to a single Space in Confluence.
     * @param appId          the applink id for the remote Confluence instance.
     * @param startAt        the page offset, if not specified then defaults to 0.
     * @param maxResults     how many results on the page should be included. Defaults to {@link #CONF_DEFAULT_MAX}.
     * @return Confluence content that matched the search query and was in the search's scope.
     */
    @GET
    @Path("/search")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getSiteSearch(
            @QueryParam("query") final String query,
            @QueryParam("spaceKey") final String spaceKey,
            @QueryParam("startAt") final Long startAt,
            @QueryParam("maxResults") final Integer maxResults,
            @QueryParam("appId") final String appId) {
        return getApplink(appId).left().on(appLink -> {
            try {
                final PageRequest pageRequest = PageRequests.request(startAt, firstNonNull(maxResults, CONF_DEFAULT_MAX));
                final RemoteResponse<List<ConfluenceSearchResult>> response = strategyFactory.get().search(appLink, query, maxResults, spaceKey);
                if (!response.isSuccessful()) {
                    return handleUnsuccessfulResponse(response);
                }

                final Page<ConfluenceSearchResult> results = Pages.toPage(response.getEntity(), pageRequest, result -> true, result -> {
                    return new ConfluenceSearchResultBuilder(result)
                            .url(BaseUrlSwapper.swapRpcUrlToDisplayUrl(result.getUrl(), appLink))
                            .build();
                });

                final String searchSelfLink = getSearchResultSelfLink(query, spaceKey, pageRequest, appId);
                final PageBean<Result> bean = PageBean.from(pageRequest, results)
                        .setLinks(searchSelfLink, pageRequest.getLimit())
                        .build(this::makeResult);
                return Response.ok(bean).cacheControl(never()).build();
            } catch (CredentialsRequiredException e) {
                return Response.status(Status.UNAUTHORIZED).cacheControl(never()).build();
            } catch (ResponseException e) {
                return Response.status(Status.INTERNAL_SERVER_ERROR).cacheControl(never()).build();
            }
        });
    }

    @GET
    @Path("/applink")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getApplicationLinks() {
        final ConfluenceApplicationLinksBean bean = new ConfluenceApplicationLinksBean(getConfluenceAppLinks());
        return Response.ok(bean).cacheControl(never()).build();
    }

    private Either<Response, ApplicationLink> getApplink(final String appId) {
        final ApplicationLink appLink = getConfluenceAppLink(appId);
        if (appLink == null) {
            ErrorCollection errors = ErrorCollection.of(i18nBean.getText("addconfluencelink.search.applink.not.found"));
            return Either.left(Response.status(Status.BAD_REQUEST).entity(errors).cacheControl(never()).build());
        }
        return Either.right(appLink);
    }

    private ApplicationLink getConfluenceAppLink(final String appId) {
        for (final ApplicationLink appLink : getConfluenceAppLinks()) {
            if (appLink.getId().get().equals(appId)) {
                return appLink;
            }
        }

        return null;
    }

    private Iterable<ApplicationLink> getConfluenceAppLinks() {
        return applicationLinkService.getApplicationLinks(ConfluenceApplicationType.class);
    }

    private Space makeSpace(ConfluenceSpace space) {
        return new Space(space.getKey(), space.getName(), space.getType(), space.getUrl());
    }

    private Result makeResult(ConfluenceSearchResult result) {
        return new Result(result.getId(), result.getType(),
                result.getTitle(), result.getTitleContent(),
                result.getExcerpt(), result.getExcerptContent(),
                result.getUrl());
    }

    private String getSpaceSelfLink(String query, PageRequest pageRequest, String appId) {
        UriBuilder uri = UriBuilder.fromPath(jiraBaseUrls.restApi2BaseUrl())
                .path(ConfluenceResource.class)
                .path("space");
        if (StringUtils.isNotBlank(query)) {
            uri.queryParam("query", query);
        }
        uri.queryParam("startAt", String.valueOf(pageRequest.getStart()));
        uri.queryParam("maxResults", String.valueOf(pageRequest.getLimit()));
        uri.queryParam("appId", appId);

        return uri.build().toString();
    }

    private String getSearchResultSelfLink(String query, String spaceKey, PageRequest pageRequest, String appId) {
        UriBuilder uri = UriBuilder.fromPath(jiraBaseUrls.restApi2BaseUrl())
                .path(ConfluenceResource.class)
                .path("search");
        if (StringUtils.isNotBlank(query)) {
            uri.queryParam("query", query);
        }
        if (StringUtils.isNotBlank(spaceKey)) {
            uri.queryParam("spaceKey", spaceKey);
        }
        uri.queryParam("startAt", String.valueOf(pageRequest.getStart()));
        uri.queryParam("maxResults", String.valueOf(pageRequest.getLimit()));
        uri.queryParam("appId", appId);

        return uri.build().toString();
    }

    private Response handleUnsuccessfulResponse(final RemoteResponse<?> response) {
        final ErrorCollection errors = ErrorCollection.of(response.getStatusText());
        if (response.hasErrors()) {
            errors.addErrorCollection(response.getErrors());
        }
        return Response.status(response.getStatusCode()).entity(errors).cacheControl(never()).build();
    }
}
