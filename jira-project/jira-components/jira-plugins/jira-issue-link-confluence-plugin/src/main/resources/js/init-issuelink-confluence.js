require([
    'jira-issue-link/confluence/init-confluence-page-search-dialog',
    'jira-issue-link/confluence/init-confluence-issuelink',
    'jquery'
], function(PageSearchDialog, IssueLink, $) {
    'use strict';

    IssueLink.init();

    $(function() {
        PageSearchDialog.init();
    });
});
