/**
 * Initialises OAuth authentication for Confluence Application Links. Requires the following elements:
 * <div class="issue-link-applinks-authentication-message"></div>
 */
define('jira-issue-link/confluence/init-confluence-issuelink', ['require'], function (require) {
    'use strict';

    var jQuery = require('jquery');
    var IssueLinkAppLinks = require('jira-issue-link/applinks/common');
    var Types = require('jira/util/events/types');
    var Events = require('jira/util/events');

    var settings = {
        getCurrentAppId: function (context) {
            return jQuery("#issue-link-confluence-app-id", context).val();
        },
        shouldExecute: function (context) {
            return jQuery("#confluence-page-link", context).length !== 0;
        },
        getIssueId: function (context) {
            return jQuery("input[name=id]", context).val();
        }
    };

    function init() {
        Events.bind(Types.NEW_PAGE_ADDED, function (e, context) {
            IssueLinkAppLinks.init(settings, context);
        });
    }

    return {
        init: init
    };
});
