package com.atlassian.jira.plugin.link.confluence.service.rpc;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.ApplicationLinkResponseHandler;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.jira.plugin.link.applinks.RemoteResponse;
import com.atlassian.jira.plugin.link.confluence.ConfluencePage;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * This is a unit-test level description of what we expect Confluence's RPC calls to do.
 * It does not defend us against the contract of the XMLRPC API changing!
 *
 * As such, this should be complemented by an acceptance test with Confluence.
 */
@RunWith(Parameterized.class)
public class TestConfluenceRpcServiceImpl {

    @Rule
    public MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private ApplicationLinkRequestFactory requestFactory;

    @Mock
    private ApplicationLink applicationLink;

    @Mock
    private ApplicationLinkRequest request;

    private ConfluenceRpcService confluenceRpcService;
    private ConfVersion version;
    private String lastRequestBody;

    private static final String getPageRequest = "<methodName>confluence2.getPage</methodName>";
    private static final String getBlogRequest = "<methodName>confluence2.getBlogEntry</methodName>";

    private static final ConfluencePage RESTRICTED_PAGE_ENTITY = new ConfluencePage("restricted-id", "some-restricted-page", "http://example.com/confluence/display/SECRET/some+restricted+page");
    private static final ConfluencePage PAGE_ENTITY = new ConfluencePage("page-id", "some-page", "http://example.com/confluence/display/KB/some+page");
    private static final ConfluencePage BLOG_ENTITY = new ConfluencePage("blog-id", "some-blog", "http://example.com/confluence/display/KB/2017/01/25/some+blog");

    private enum ConfVersion {
        CONF56X,
        CONF59X
    }

    private static final Object[][] testcases = new Object[][]{
            {ConfVersion.CONF56X},
            {ConfVersion.CONF59X}
    };

    public TestConfluenceRpcServiceImpl(ConfVersion version) {
        this.version = version;
    }

    @Parameterized.Parameters(name = "JIRA when integrated with Confluence {0}")
    public static Collection input() {
        return Arrays.asList(testcases);
    }

    @Before
    public void setUp() throws CredentialsRequiredException, ResponseException {
        lastRequestBody = "";
        confluenceRpcService = new ConfluenceRpcServiceImpl();
        when(applicationLink.createAuthenticatedRequestFactory()).thenReturn(requestFactory);
        when(requestFactory.createRequest(any(Request.MethodType.class), anyString()))
                .thenReturn(request);
        when(request.setRequestBody(anyString(), anyString())).thenAnswer(
                args -> {
                    lastRequestBody = args.getArgumentAt(0, String.class);
                    return request;
                }
        );
        // Mock the XMLRPC responses according to what the Confluence versions are known to do.
        switch (version) {
            case CONF56X:
                mockConfluence561RpcBehaviour();
                break;
            case CONF59X:
                mockConfluence591RpcBehaviour();
                break;
        }
    }

    @Test
    public void testGetPage() throws CredentialsRequiredException, ResponseException {
        RemoteResponse<ConfluencePage> response = confluenceRpcService.getPage(applicationLink, "page-id");
        final ConfluencePage entity = response.getEntity();
        assertNotNull(entity);
        assertEquals("some-page", entity.getTitle());
        assertEquals("Should be the full URL including the remote instance's base URL",
                "http://example.com/confluence/display/KB/some+page", entity.getUrl());
    }

    @Test
    public void testGetBlogInsteadOfPage() throws CredentialsRequiredException, ResponseException {
        RemoteResponse<ConfluencePage> response = confluenceRpcService.getPage(applicationLink, "blog-id");
        final ConfluencePage entity = response.getEntity();
        assertNotNull(entity);
        assertEquals("some-blog", entity.getTitle());
        assertEquals("Should be the full URL including the remote instance's base URL",
                "http://example.com/confluence/display/KB/2017/01/25/some+blog", entity.getUrl());
    }

    @Test
    public void testGetANonExistentPage() throws CredentialsRequiredException, ResponseException {
        RemoteResponse<ConfluencePage> response = confluenceRpcService.getPage(applicationLink, "nonexistent");
        assertThat(response.hasErrors(), Matchers.is(true));
        assertThat(response.getErrors().getErrorMessages(), Matchers.hasItem("Not found"));
    }

    @Test
    public void testGetPageWithNoPermissions() throws CredentialsRequiredException, ResponseException {
        RemoteResponse<ConfluencePage> response = confluenceRpcService.getPage(applicationLink, "restricted-id");
        assertThat(response.hasErrors(), Matchers.is(true));
        assertThat(response.getErrors().getErrorMessages(), Matchers.hasItem("RemoteException: You're not allowed to view that..."));
    }

    /**
     * From observation, the behaviour of the Confluence XMLRPC API in Confluence <b>5.6.1</b> is:
     * <ul>
     *   <li>Requesting a page as a page returns a 200.</li>
     *   <li>Requesting a blogpost as a page returns a 200 with a ClassCastException error.</li>
     *   <li>Requesting a page as a blogpost returns a 200 with a ClassCastException error.</li>
     * </ul>
     */
    private void mockConfluence561RpcBehaviour() throws ResponseException {
        final Map<ConfluencePage,RemoteResponse<ConfluencePage>> blogResponses = MapBuilder.build(
                RESTRICTED_PAGE_ENTITY, error("RemoteException: You're not allowed to view that..."), // e.g., "java.lang.Exception: com.atlassian.confluence.rpc.RemoteException: You're not allowed to view that page, or it does not exist."
                PAGE_ENTITY, error("ClassCastException"), // e.g., "java.lang.ClassCastException: com.atlassian.confluence.pages.Page cannot be cast to com.atlassian.confluence.pages.BlogPost"
                BLOG_ENTITY, response(BLOG_ENTITY)
        );
        final Map<ConfluencePage,RemoteResponse<ConfluencePage>> pageResponses = MapBuilder.build(
                RESTRICTED_PAGE_ENTITY, error("RemoteException: You're not allowed to view that..."), // e.g., "java.lang.Exception: com.atlassian.confluence.rpc.RemoteException: You're not allowed to view that page, or it does not exist."
                BLOG_ENTITY, error("ClassCastException"), // e.g., "java.lang.ClassCastException: com.atlassian.confluence.pages.BlogPost cannot be cast to com.atlassian.confluence.pages.Page"
                PAGE_ENTITY, response(PAGE_ENTITY)
        );
        setUpTheMockResponses(MapBuilder.build(getBlogRequest, blogResponses, getPageRequest, pageResponses));
    }

    /**
     * From observation, the behaviour of the Confluence XMLRPC API as at Confluence <b>5.9.1</b> is:
     * <ul>
     *   <li>Requesting a page as a page returns a 200.</li>
     *   <li>Requesting a blogpost as a page returns a 200 with a RemoteException error stating "ID1234 is not a page?".</li>
     *   <li>Requesting a page as a blogpost returns a 200 with a ClassCastException error.</li>
     * </ul>
     */
    private void mockConfluence591RpcBehaviour() throws ResponseException {
        final Map<ConfluencePage,RemoteResponse<ConfluencePage>> blogResponses = MapBuilder.build(
                RESTRICTED_PAGE_ENTITY, error("RemoteException: You're not allowed to view that..."), // e.g., "java.lang.Exception: com.atlassian.confluence.rpc.RemoteException: You're not allowed to view that page, or it does not exist."
                PAGE_ENTITY, error("ClassCastException"), // e.g., "java.lang.ClassCastException: com.atlassian.confluence.pages.BlogPost cannot be cast to com.atlassian.confluence.pages.Page"
                BLOG_ENTITY, response(BLOG_ENTITY)
        );
        final Map<ConfluencePage,RemoteResponse<ConfluencePage>> pageResponses = MapBuilder.build(
                RESTRICTED_PAGE_ENTITY, error("RemoteException: You're not allowed to view that..."), // e.g., "java.lang.Exception: com.atlassian.confluence.rpc.RemoteException: You're not allowed to view that page, or it does not exist."
                BLOG_ENTITY, error("RemoteException"), // e.g., "java.lang.Exception: com.atlassian.confluence.rpc.RemoteException: 753666 is not a page?"
                PAGE_ENTITY, response(PAGE_ENTITY)
        );
        setUpTheMockResponses(MapBuilder.build(getBlogRequest, blogResponses, getPageRequest, pageResponses));
    }

    /**
     * If this looks complicated to you, that's because it is.
     * It's complicated because the class under test is violating the single responsibility principle,
     * and should be split in to two (i.e., extract the nested classes and test the req-res stuff separately).
     * If you want to fix this test code for a deprecated class, I won't stop you, but consider
     * whether your time is better spent outside or with your loved ones first.
     */
    private void setUpTheMockResponses(Map<String,Map<ConfluencePage,RemoteResponse<ConfluencePage>>> requestToResponses) throws ResponseException {
        when(request.execute(any(ApplicationLinkResponseHandler.class))).thenAnswer(
                args -> {
                    final Map<ConfluencePage,RemoteResponse<ConfluencePage>> responses = requestToResponses.entrySet().stream()
                            .filter(reqResEntry -> lastRequestBody.contains(reqResEntry.getKey()))
                            .map(Map.Entry::getValue)
                            .findFirst()
                            .get();
                    final Optional<RemoteResponse<ConfluencePage>> returnVal = responses.entrySet().stream()
                            .filter(entry -> lastRequestBody.contains(entry.getKey().getPageId()))
                            .map(Map.Entry::getValue)
                            .findFirst();
                    return returnVal.orElse(error("Not found"));
                }
        );
    }

    private static RemoteResponse<ConfluencePage> response(ConfluencePage page) {
        return new RemoteResponse<>(page, mock(Response.class));
    }

    private static RemoteResponse<ConfluencePage> error(String message) {
        ErrorCollection errors = new SimpleErrorCollection();
        errors.addErrorMessage(message);
        return new RemoteResponse<>(null, errors, mock(Response.class));
    }
}
