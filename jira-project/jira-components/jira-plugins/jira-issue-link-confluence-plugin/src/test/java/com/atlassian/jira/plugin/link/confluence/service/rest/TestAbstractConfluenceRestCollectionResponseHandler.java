package com.atlassian.jira.plugin.link.confluence.service.rest;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.plugin.link.applinks.RemoteResponse;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.json.JSONObject;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.List;

import static com.atlassian.jira.plugin.link.confluence.service.rest.ConfluenceRestResponseHandlerTestUtils.mockResponseFor;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.emptyCollectionOf;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestAbstractConfluenceRestCollectionResponseHandler {
    private static final String NEEDED_CQL_PARAM = "{\"statusCode\":400,\"data\":{\"authorized\":false,\"valid\":true,\"errors\":[]},\"message\":\"cql query parameter is required\"}";
    private static final String INVALID_CQL_PARAM = "{\"statusCode\":400,\"data\":{\"authorized\":false,\"valid\":true,\"errors\":[]},\"message\":\"Expecting operator for field 'text', supported operators are : ~, !~\"}";
    private static final String BAD_CQLCONTEXT_PARAM = "{\"statusCode\":400,\"data\":{\"authorized\":false,\"valid\":true,\"errors\":[]},\"message\":\"Could not parse Search Context from cql context param {invalidJSONlol}\"}";
    private static final String ANONYMOUS_USER = "{\"statusCode\":403,\"data\":{\"authorized\":false,\"valid\":true,\"errors\":[]},\"message\":\"Not permitted to use confluence : null\"}";
    private static final String FOUR_OH_FOUR = "{\"statusCode\":404,\"data\":{\"authorized\":false,\"valid\":true,\"errors\":[]},\"message\":\"No content found with id: ContentId{id=12345}\"}";
    private static final String OH_NOES = "{\"statusCode\":500,\"message\":\"Confluence went bye-bye :(\"}";
    private static final String REASONABLE_NO_RESULTS = "{\"results\":[], \"size\": 0}";
    private static final String REASONABLE_ONE_RESULT = "{\"results\":[{\"title\":\"Whee!\"}], \"size\": 1}";

    @Rule
    public RuleChain init = MockitoMocksInContainer.forTest(this);
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();
    @Mock
    @AvailableInContainer
    private JiraAuthenticationContext mockAuthenticationContext;

    private I18nHelper mockI18nHelper = new MockI18nHelper();

    private Object SUCCESS = mock(Object.class);

    private MockCollectionResponseHandler handler;

    @Before
    public void setup() {
        when(ComponentAccessor.getComponent(JiraAuthenticationContext.class).getI18nHelper()).thenReturn(mockI18nHelper);
        handler = new MockCollectionResponseHandler();
    }

    @Test
    public void parseJsonResponseFor404() throws Exception {
        final RemoteResponse<List<Object>> result = handler.handle(mockResponseFor(FOUR_OH_FOUR));
        assertThat(result.hasErrors(), is(true));
        assertThat(result.getErrors().getErrorMessages(), hasItem("searchconfluence.error.not.found [No content found with id: ContentId{id=12345}]"));
    }

    @Test
    public void parseJsonResponseFor403() throws Exception {
        final RemoteResponse<List<Object>> result = handler.handle(mockResponseFor(ANONYMOUS_USER));
        assertThat(result.hasErrors(), is(true));
        assertThat(result.getErrors().getErrorMessages(), hasItem("searchconfluence.error.forbidden [Not permitted to use confluence : null]"));
    }

    @Test
    public void parseJsonResponseFor400() throws Exception {
        final RemoteResponse<List<Object>> result = handler.handle(mockResponseFor(NEEDED_CQL_PARAM));
        assertThat(result.hasErrors(), is(true));
        assertThat(result.getErrors().getErrorMessages(), hasItem("searchconfluence.error.bad.request [cql query parameter is required]"));
    }

    @Test
    public void parseJsonResponseFor500() throws Exception {
        final RemoteResponse<List<Object>> result = handler.handle(mockResponseFor(OH_NOES));
        assertThat(result.hasErrors(), is(true));
        assertThat(result.getErrors().getErrorMessages(), hasItem("addconfluencelink.search.servererror [Confluence went bye-bye :(]"));
    }

    @Test
    public void parseJsonResponseForSuccessfulNoResults() throws Exception {
        final RemoteResponse<List<Object>> result = handler.handle(mockResponseFor(REASONABLE_NO_RESULTS));
        assertThat(result.hasErrors(), is(false));
        assertThat(result.getEntity(), emptyCollectionOf(Object.class));
    }

    @Test
    public void parseJsonResponseForSuccessfulOneResult() throws Exception {
        final RemoteResponse<List<Object>> result = handler.handle(mockResponseFor(REASONABLE_ONE_RESULT));
        assertThat(result.hasErrors(), is(false));
        assertThat(result.getEntity(), hasItem(SUCCESS));
    }

    private class MockCollectionResponseHandler extends AbstractConfluenceRestCollectionResponseHandler<Object> {
        @Override
        Object processResult(JSONObject object) {
            return SUCCESS;
        }
    }
}
