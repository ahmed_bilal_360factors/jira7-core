package com.atlassian.jira.plugin.link.confluence.service;

import com.atlassian.jira.config.FeatureFlag;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.plugin.link.confluence.service.rest.ConfluenceRestService;
import com.atlassian.jira.plugin.link.confluence.service.rest.ConfluenceRestServiceImpl;
import com.atlassian.jira.plugin.link.confluence.service.rpc.ConfluenceRpcService;
import com.atlassian.jira.plugin.link.confluence.service.rpc.ConfluenceRpcServiceImpl;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

public class TestConfluenceRemoteServiceStrategyFactory {
    @Rule public MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock private FeatureManager featureManager;
    @Mock private ConfluenceRestServiceImpl mockRestImpl;
    @Mock private ConfluenceRpcServiceImpl mockRpcImpl;

    private FeatureFlag featureFlag = ConfluenceRemoteServiceStrategyFactory.FEATURE_FLAG;
    private ConfluenceRemoteServiceStrategyFactory strategyFactory;

    @Before
    public void setup() throws Exception {
        strategyFactory = new ConfluenceRemoteServiceStrategyFactory(mockRestImpl, mockRpcImpl, featureManager);
    }

    @Test
    public void shouldUseRestApiByDefault() {
        when(featureManager.isEnabled(featureFlag)).thenReturn(true);
        ConfluenceRemoteService service = strategyFactory.get();
        assertThat(service, instanceOf(ConfluenceRestService.class));
    }

    @Test
    public void shouldUseRpcApiIfDarkFeatureFlagSaysSo() {
        when(featureManager.isEnabled(featureFlag)).thenReturn(false);
        ConfluenceRemoteService service = strategyFactory.get();
        assertThat(service, instanceOf(ConfluenceRpcService.class));
    }
}
