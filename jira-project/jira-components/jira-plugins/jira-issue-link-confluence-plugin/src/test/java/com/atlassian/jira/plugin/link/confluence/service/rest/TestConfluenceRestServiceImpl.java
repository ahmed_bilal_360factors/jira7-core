package com.atlassian.jira.plugin.link.confluence.service.rest;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.ApplicationLinkResponseHandler;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.event.ApplicationLinksIDChangedEvent;
import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.jira.plugin.link.applinks.RemoteResponse;
import com.atlassian.jira.plugin.link.confluence.ConfluencePage;
import com.atlassian.jira.plugin.link.confluence.ConfluenceSearchResult;
import com.atlassian.jira.plugin.link.confluence.ConfluenceSpace;
import com.atlassian.jira.plugin.link.confluence.service.rpc.ConfluenceRpcService;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.google.common.collect.Lists;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.net.URLEncoder;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class TestConfluenceRestServiceImpl {
    @Rule public MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock private ApplicationLinkRequestFactory requestFactory;
    @Mock private ApplicationLinkRequest request;
    @Mock private ConfluenceRpcService confluenceRpcService;
    @Mock private ApplicationLink modernConfluenceInstance;
    @Mock private ApplicationLink olderThanConf59Instance;
    private final ApplicationId conf5_9_id = new ApplicationId(UUID.randomUUID().toString());
    private final ApplicationId conf5_6_id = new ApplicationId(UUID.randomUUID().toString());
    private final MemoryCacheManager cacheManager = new MemoryCacheManager();

    private ConfluenceRestServiceImpl confluenceRestService;

    private static final ConfluencePage PRIVATE_PAGE = new ConfluencePage("restricted-id", "secret-page", "http://example.com/confluence/display/SECRET/super+secret+page");
    private static final ConfluencePage PAGE_ENTITY = new ConfluencePage("page-id", "some-page", "http://example.com/confluence/display/KB/some+page");
    private static final ConfluencePage BLOG_ENTITY = new ConfluencePage("blog-id", "some-blog", "http://example.com/confluence/display/KB/2017/01/25/some+blog");

    private static final List<ConfluenceSpace> FILLED_SPACE_LIST = Lists.newArrayList(
            new ConfluenceSpace("DEMO", "Demo space", "global", "/display/DEMO"),
            new ConfluenceSpace("EXAMPLE", "<An> example <script>space", "global", "display/EXAMPLE"),
            new ConfluenceSpace("~nihao", "\u4f60\u597d", "personal", "display/~nihao")
    );

    private static final String FILLED_SEARCH_QUERY = "some";
    private static final String FILLED_SPACE_KEY = "fsk";
    private static final List<ConfluenceSearchResult> FILLED_SEARCH_RESULTS = Lists.newArrayList(
            new ConfluenceSearchResult(PAGE_ENTITY.getPageId(), "page", PAGE_ENTITY.getTitle(), "this is @@@hl@@@some@@@endhl@@@ kind of page...", PAGE_ENTITY.getUrl()),
            new ConfluenceSearchResult("12345", "page", "Another page", "@@@hl@@@some@@@endhl@@@ other page", "http://example.com/confluence/display/viewpage.action?pageId=12345")
    );

    @Before
    public void setup() throws CredentialsRequiredException {
        confluenceRestService = new ConfluenceRestServiceImpl(confluenceRpcService, cacheManager);
        when(modernConfluenceInstance.getId()).thenReturn(conf5_9_id);
        when(modernConfluenceInstance.createAuthenticatedRequestFactory()).thenReturn(requestFactory);
        when(olderThanConf59Instance.getId()).thenReturn(conf5_6_id);
        when(olderThanConf59Instance.createAuthenticatedRequestFactory()).thenReturn(requestFactory);
        when(requestFactory.createRequest(any(Request.MethodType.class), anyString()))
                .thenReturn(request);
    }

    @After
    public void teardown() {
        confluenceRestService.clearCache();
    }

    @Test
    public void getNormalPage() throws Exception {
        requestReturns(validResponseOf(PAGE_ENTITY));
        RemoteResponse<ConfluencePage> response = confluenceRestService.getPage(modernConfluenceInstance, PAGE_ENTITY.getPageId());
        assertThat(response.hasErrors(), is(false));
        assertThat(response.getEntity(), is(PAGE_ENTITY));
    }

    @Test
    public void getBlogPost() throws Exception {
        requestReturns(validResponseOf(BLOG_ENTITY));
        RemoteResponse<ConfluencePage> response = confluenceRestService.getPage(modernConfluenceInstance, BLOG_ENTITY.getPageId());
        assertThat(response.hasErrors(), is(false));
        assertThat(response.getEntity(), is(BLOG_ENTITY));
    }

    @Test
    public void getNonExistentPage() throws Exception {
        requestReturns(error("Not found"));
        RemoteResponse<ConfluencePage> response = confluenceRestService.getPage(modernConfluenceInstance, "nonexistent");
        assertThat(response.getEntity(), nullValue(ConfluencePage.class));
        assertThat(response.hasErrors(), is(true));
        assertThat(response.getErrors().getErrorMessages(), hasItem("Not found"));
    }

    @Test
    @Ignore("RAID-351 - It currently delegates to the XMLRPC API, which isn't stubbed here")
    public void getSpaces() throws Exception {
        requestReturns(validResponseOf(FILLED_SPACE_LIST));
        RemoteResponse<List<ConfluenceSpace>> response = confluenceRestService.getSpaces(modernConfluenceInstance);
        assertThat(response.hasErrors(), is(false));
        assertThat(response.getEntity(), is(not(nullValue())));
        assertThat(response.getEntity(), is(FILLED_SPACE_LIST));
    }

    @Test
    // TODO RAID-351: 9/2/17 remove when getSpaces is un-ignored
    public void getSpacesDelegatesToXmlRpc() throws Exception {
        confluenceRestService.getSpaces(olderThanConf59Instance);
        verify(confluenceRpcService).getSpaces(olderThanConf59Instance);
    }

    @Test
    public void searchConstructsAppropriateQueryURL() throws Exception {
        requestReturns(validResponseOf(FILLED_SEARCH_RESULTS));
        confluenceRestService.search(modernConfluenceInstance, FILLED_SEARCH_QUERY, 12, null);
        final String expectedUrl = String.format("rest/api/search?cql=%s&limit=12&expand=body", cqlFor(FILLED_SEARCH_QUERY));
        verify(requestFactory).createRequest(Request.MethodType.GET, expectedUrl);
    }

    @Test
    public void searchWithinSpaceConstructsAppropriateQueryURL() throws Exception {
        requestReturns(validResponseOf(FILLED_SEARCH_RESULTS));
        confluenceRestService.search(modernConfluenceInstance, FILLED_SEARCH_QUERY, 5, FILLED_SPACE_KEY);
        final String expectedUrl = String.format("rest/api/search?cql=%s&limit=5&expand=body&cqlcontext=%s",
                cqlFor(FILLED_SEARCH_QUERY, FILLED_SPACE_KEY),
                URLEncoder.encode("{\"spaceKey\":\"" + FILLED_SPACE_KEY + "\"}", "UTF-8"));
        verify(requestFactory).createRequest(Request.MethodType.GET, expectedUrl);
    }

    @Test
    public void searchWithInvalidSpaceValueConstructsAppropriateQueryURL() throws Exception {
        requestReturns(validResponseOf(FILLED_SEARCH_RESULTS));
        final String query = "a test of empty space key";
        final String expectedUrl = String.format("rest/api/search?cql=%s&limit=5&expand=body", cqlFor(query));
        confluenceRestService.search(modernConfluenceInstance, query, 5, "    ");
        verify(requestFactory).createRequest(Request.MethodType.GET, expectedUrl);
    }

    @Test
    public void searchReturnsResultsForValidConf_5_9() throws Exception {
        requestReturns(validResponseOf(FILLED_SEARCH_RESULTS));
        RemoteResponse<List<ConfluenceSearchResult>> response = confluenceRestService.search(modernConfluenceInstance, FILLED_SEARCH_QUERY, 5, null);
        assertThat(response.hasErrors(), is(false));
        assertThat(response.getEntity(), is(not(nullValue())));
        assertThat(response.getEntity(), is(FILLED_SEARCH_RESULTS));
    }

    @Test
    public void searchDelegatesToRpcForOlderConfVersions() throws Exception {
        final Response mockResponse = mock(Response.class);
        when(mockResponse.getStatusCode()).thenReturn(404);
        requestReturns(error(mockResponse, "The requested Confluence content could not be found"));
        confluenceRestService.search(olderThanConf59Instance, FILLED_SEARCH_QUERY, 5, "spacekey");
        verify(confluenceRpcService).search(olderThanConf59Instance, FILLED_SEARCH_QUERY, 5, "spacekey");
    }

    @Test
    public void searchWillNotAttemptSubsequentRestCallsOnConfVersionsThatDontHaveRest() throws Exception {
        final Response mockResponse = mock(Response.class);
        when(mockResponse.getStatusCode()).thenReturn(404);
        requestReturns(error(mockResponse, "The requested Confluence content could not be found"));
        confluenceRestService.search(olderThanConf59Instance, FILLED_SEARCH_QUERY, 5, null);
        verify(request, times(1)).execute(any(ConfluenceRestSearchResponseHandler.class));

        confluenceRestService.search(olderThanConf59Instance, FILLED_SEARCH_QUERY, 5, null);
        verifyNoMoreInteractions(request);
    }

    @Test
    public void searchWillAttemptSubsequentRestCallsOnConfVersionsIfCachesAreCleared() throws Exception {
        final Response mockResponse = mock(Response.class);
        when(mockResponse.getStatusCode()).thenReturn(404);
        requestReturns(error(mockResponse, "The requested Confluence content could not be found"));
        confluenceRestService.search(olderThanConf59Instance, FILLED_SEARCH_QUERY, 5, null);
        verify(request, times(1)).execute(any(ConfluenceRestSearchResponseHandler.class));

        // The confluence instance gets updated, maybe changes the ID, maybe doesn't.
        confluenceRestService.onAppLinkIdChangeEvent(new ApplicationLinksIDChangedEvent(olderThanConf59Instance, conf5_6_id));
        confluenceRestService.search(olderThanConf59Instance, FILLED_SEARCH_QUERY, 5, null);
        verify(request, times(2)).execute(any(ConfluenceRestSearchResponseHandler.class));
    }

    @Test
    public void searchWillAttemptRestCallsOnDifferentConfVersions() throws Exception {
        final Response olderConfResponse = mock(Response.class);
        when(olderConfResponse.getStatusCode()).thenReturn(404);
        requestReturns(error(olderConfResponse, "The requested Confluence content could not be found"));
        confluenceRestService.search(olderThanConf59Instance, FILLED_SEARCH_QUERY, 5, null);
        verify(request, times(1)).execute(any(ConfluenceRestSearchResponseHandler.class));

        requestReturns(validResponseOf(FILLED_SEARCH_RESULTS));
        confluenceRestService.search(modernConfluenceInstance, FILLED_SEARCH_QUERY, 5, null);
        verify(request, times(2)).execute(any(ConfluenceRestSearchResponseHandler.class));
    }


    //
    // Test helpers
    //

    private void requestReturns(Object response) throws ResponseException {
        when(request.execute(any(ApplicationLinkResponseHandler.class))).thenReturn(response);
    }

    private Object validResponseOf(ConfluencePage page) {
        return new RemoteResponse<>(page, mock(Response.class));
    }

    private Object validResponseOf(List things) {
        return new RemoteResponse<>(things, mock(Response.class));
    }

    private Object error(String message) {
        return error(mock(Response.class), message);
    }

    private Object error(Response response, String message) {
        final SimpleErrorCollection errors = new SimpleErrorCollection();
        errors.addErrorMessage(message);
        return new RemoteResponse<ConfluencePage>(null, errors, response);
    }

    private String cqlFor(String textQuery) throws Exception {
        return cqlWithDefaultIgnores(String.format("text ~ \"%s\"", textQuery));
    }

    private String cqlFor(String textQuery, String spaceKey) throws Exception {
        return cqlWithDefaultIgnores(String.format("text ~ \"%s\" AND space = \"%s\"", textQuery, spaceKey));
    }

    private String cqlWithDefaultIgnores(String cqlQuery) throws Exception {
        return URLEncoder.encode(cqlQuery + " AND type NOT IN (attachment,comment)", "UTF-8");
    }
}
