package com.atlassian.jira.plugin.link.confluence.service.rest;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.plugin.link.applinks.RemoteResponse;
import com.atlassian.jira.plugin.link.confluence.ConfluencePage;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static com.atlassian.jira.plugin.link.confluence.service.rest.ConfluenceRestResponseHandlerTestUtils.mockResponseFor;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Mockito.when;

public class TestConfluenceRestContentResponseHandler {
    private static final String JSON_FOR_PAGE = "{\"id\":\"852011\",\"type\":\"page\",\"title\":\"example's Home\",\"space\":{\"id\":983041,\"key\":\"EXAMPLE\",\"name\":\"example\",\"type\":\"global\",\"_links\":{\"self\":\"http://example.com/confluence/rest/api/space/EXAMPLE\"},\"_expandable\":{\"icon\":\"\",\"description\":\"\",\"homepage\":\"/rest/api/content/852011\"}},\"history\":{\"latest\":true,\"createdBy\":{\"type\":\"known\",\"profilePicture\":{\"path\":\"/confluence/s/en_GB/5635/NOCACHE1/_/images/icons/profilepics/default.png\",\"width\":48,\"height\":48,\"isDefault\":true},\"username\":\"admin\",\"displayName\":\"admin\",\"userKey\":\"ff80808159d5fc260159d5fc2e010003\"},\"createdDate\":\"2017-01-25T15:38:03.524+0100\",\"_links\":{\"self\":\"http://example.com/confluence/rest/api/content/852011/history\"},\"_expandable\":{\"lastUpdated\":\"\"}},\"version\":{\"by\":{\"type\":\"known\",\"profilePicture\":{\"path\":\"/confluence/s/en_GB/5635/NOCACHE1/_/images/icons/profilepics/default.png\",\"width\":48,\"height\":48,\"isDefault\":true},\"username\":\"admin\",\"displayName\":\"admin\",\"userKey\":\"ff80808159d5fc260159d5fc2e010003\"},\"when\":\"2017-01-25T15:38:03.524+0100\",\"message\":\"\",\"number\":1,\"minorEdit\":false},\"_links\":{\"webui\":\"/display/EXAMPLE/example%27s+Home\",\"tinyui\":\"/x/KwAN\",\"collection\":\"/rest/api/content\",\"base\":\"http://example.com/confluence\",\"context\":\"/confluence\",\"self\":\"http://example.com/confluence/rest/api/content/852011\"},\"_expandable\":{\"container\":\"\",\"metadata\":\"\",\"children\":\"/rest/api/content/852011/child\",\"ancestors\":\"\",\"body\":\"\",\"descendants\":\"/rest/api/content/852011/descendant\"}}";
    private static final String JSON_FOR_BLOG = "{\"id\":\"753666\",\"type\":\"blogpost\",\"status\":\"current\",\"title\":\"demo blog\",\"space\":{\"id\":65537,\"key\":\"ds\",\"name\":\"Demonstration Space\",\"type\":\"global\",\"_links\":{\"self\":\"http://example.com/confluence/rest/api/space/ds\"},\"_expandable\":{\"icon\":\"\",\"description\":\"\",\"homepage\":\"/rest/api/content/98310\"}},\"history\":{\"latest\":true,\"createdBy\":{\"type\":\"known\",\"profilePicture\":{\"path\":\"/confluence/s/en_GB/6207/NOCACHE1/_/images/icons/profilepics/default.png\",\"width\":48,\"height\":48,\"isDefault\":true},\"username\":\"admin\",\"displayName\":\"admin\",\"userKey\":\"2c9682714db22c7c014db22f51970002\"},\"createdDate\":\"2017-01-30T12:12:49.133+01:00\",\"_links\":{\"self\":\"http://example.com/confluence/rest/api/content/753666/history\"},\"_expandable\":{\"lastUpdated\":\"\",\"previousVersion\":\"\",\"nextVersion\":\"\"}},\"version\":{\"by\":{\"type\":\"known\",\"profilePicture\":{\"path\":\"/confluence/s/en_GB/6207/NOCACHE1/_/images/icons/profilepics/default.png\",\"width\":48,\"height\":48,\"isDefault\":true},\"username\":\"admin\",\"displayName\":\"admin\",\"userKey\":\"2c9682714db22c7c014db22f51970002\"},\"when\":\"2017-01-30T12:12:49.140+01:00\",\"message\":\"\",\"number\":1,\"minorEdit\":false},\"_links\":{\"webui\":\"/display/ds/2017/01/30/demo+blog\",\"tinyui\":\"/x/AoAL\",\"collection\":\"/rest/api/content\",\"base\":\"http://example.com/confluence\",\"context\":\"/confluence\",\"self\":\"http://example.com/confluence/rest/api/content/753666\"},\"_expandable\":{\"container\":\"/rest/api/space/ds\",\"metadata\":\"\",\"operations\":\"\",\"children\":\"/rest/api/content/753666/child\",\"ancestors\":\"\",\"body\":\"\",\"descendants\":\"/rest/api/content/753666/descendant\"}}";
    private static final String JSON_FOR_PRIVATE_BLOG = "{\"id\":\"1507334\",\"type\":\"blogpost\",\"title\":\"fred's private blog\",\"space\":{\"id\":1605633,\"key\":\"~fred\",\"name\":\"fred\",\"type\":\"global\",\"_links\":{\"self\":\"http://example.com/confluence/rest/api/space/~fred\"},\"_expandable\":{\"icon\":\"\",\"description\":\"\",\"homepage\":\"/rest/api/content/1507331\"}},\"history\":{\"latest\":true,\"createdBy\":{\"type\":\"known\",\"profilePicture\":{\"path\":\"/confluence/s/en_GB/5635/NOCACHE1/_/images/icons/profilepics/default.png\",\"width\":48,\"height\":48,\"isDefault\":true},\"username\":\"fred\",\"displayName\":\"fred\",\"userKey\":\"ff80808159efba9b0159f4e4ddcf0002\"},\"createdDate\":\"2017-01-31T15:21:30.321+0100\",\"_links\":{\"self\":\"http://example.com/confluence/rest/api/content/1507334/history\"},\"_expandable\":{\"lastUpdated\":\"\"}},\"version\":{\"by\":{\"type\":\"known\",\"profilePicture\":{\"path\":\"/confluence/s/en_GB/5635/NOCACHE1/_/images/icons/profilepics/default.png\",\"width\":48,\"height\":48,\"isDefault\":true},\"username\":\"fred\",\"displayName\":\"fred\",\"userKey\":\"ff80808159efba9b0159f4e4ddcf0002\"},\"when\":\"2017-01-31T17:08:25.813+0100\",\"number\":2,\"minorEdit\":false},\"_links\":{\"webui\":\"/display/~fred/2017/01/31/fred%27s+private+blog\",\"tinyui\":\"/x/BgAX\",\"collection\":\"/rest/api/content\",\"base\":\"http://example.com/confluence\",\"context\":\"/confluence\",\"self\":\"http://example.com/confluence/rest/api/content/1507334\"},\"_expandable\":{\"container\":\"\",\"metadata\":\"\",\"children\":\"/rest/api/content/1507334/child\",\"ancestors\":\"\",\"body\":\"\",\"descendants\":\"/rest/api/content/1507334/descendant\"}}";
    private static final String JSON_FOR_HTTP_404 = "{\"statusCode\":404,\"data\":{\"authorized\":false,\"valid\":true,\"errors\":[]},\"message\":\"No content found with id: ContentId{id=12345}\"}";
    private static final String HTML_FOR_HTTP_404 = "<!DOCTYPE html>\n<html>\n<head>\n                        <title>Page Not Found - Confluence</title>\n    \n</head><body>        <div id=\"main\" class=\" aui-page-panel\"><meta name=\"confluence-request-uri\" content=\"/confluence/fourohfour.action\">        <meta name=\"confluence-query-string\" content=\"\">        \n\t\t<p>The page you were trying to reach could not be found. This could be because:</p>\n\t\t<ul>\n\t\t    <li>The page does not exist.</li>\n\t\t    <li>The page exists, but you do not have permission to view it.</li>\n\t\t</ul>\n</div></body></html>";

    @Rule
    public RuleChain init = MockitoMocksInContainer.forTest(this);
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    @AvailableInContainer
    private JiraAuthenticationContext mockAuthenticationContext;

    private I18nHelper mockI18nHelper = new MockI18nHelper();

    private ConfluenceRestContentResponseHandler responseHandler;

    @Before
    public void setup() {
        responseHandler = new ConfluenceRestContentResponseHandler();
        when(ComponentAccessor.getComponent(JiraAuthenticationContext.class).getI18nHelper()).thenReturn(mockI18nHelper);
    }

    @Test
    public void handleNormalPageResponse() throws Exception {
        final RemoteResponse<ConfluencePage> response = responseHandler.handle(mockResponseFor(JSON_FOR_PAGE));
        assertThat(response.getErrors(), nullValue(ErrorCollection.class));
        final ConfluencePage entity = response.getEntity();
        assertThat(entity, is(instanceOf(ConfluencePage.class)));
        assertThat(entity.getPageId(), is("852011"));
        assertThat(entity.getTitle(), is("example's Home"));
        assertThat(entity.getUrl(), is("http://example.com/confluence/display/EXAMPLE/example%27s+Home"));
    }

    @Test
    public void handleNormalBlogResponse() throws Exception {
        RemoteResponse<ConfluencePage> response = responseHandler.handle(mockResponseFor(JSON_FOR_BLOG));
        assertThat(response.getErrors(), nullValue(ErrorCollection.class));
        final ConfluencePage entity = response.getEntity();
        assertThat(entity, is(instanceOf(ConfluencePage.class)));
        assertThat(entity.getPageId(), is("753666"));
        assertThat(entity.getTitle(), is("demo blog"));
        assertThat(entity.getUrl(), is("http://example.com/confluence/display/ds/2017/01/30/demo+blog"));
    }

    @Test
    public void handleUnauthorisedOrNonexistentPageResponse() throws Exception {
        RemoteResponse<ConfluencePage> response = responseHandler.handle(mockResponseFor(JSON_FOR_HTTP_404));
        assertThat(response.getEntity(), Matchers.nullValue(ConfluencePage.class));
        assertThat(response.hasErrors(), is(true));
        assertThat(response.getErrors().getErrorMessages(), hasItem("searchconfluence.error.not.found"));
    }

    @Test
    public void handleResponseForInvalidRestApiSearch() throws Exception {
        RemoteResponse<ConfluencePage> response = responseHandler.handle(mockResponseFor(HTML_FOR_HTTP_404));
        assertThat(response.getEntity(), Matchers.nullValue());
        assertThat(response.hasErrors(), is(true));
        assertThat(response.getErrors().getErrorMessages(), hasItem("addconfluencelink.search.error"));
    }

}
