package com.atlassian.jira.plugin.link.confluence.service.rest;

import com.atlassian.jira.plugin.link.applinks.RemoteResponse;
import com.atlassian.jira.plugin.link.confluence.ConfluenceSearchResult;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.util.json.JSONObject;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.plugin.link.confluence.service.rest.ConfluenceRestResponseHandlerTestUtils.mockResponseFor;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.startsWith;

public class TestConfluenceRestSearchResponseHandler {
    private ConfluenceRestSearchResponseHandler handler;

    @Before
    public void setup() {
        handler = new ConfluenceRestSearchResponseHandler();
    }

    @Test
    public void handleSuccessfulConfluence_5_9Search() throws Exception {
        final RemoteResponse<List<ConfluenceSearchResult>> response = handler.handle(mockResponseFor(CONF59_FILLED_SEARCH_REULTS));
        final List<ConfluenceSearchResult> entity = response.getEntity();
        assertThat(entity, is(not(nullValue())));
        assertThat(entity, hasSize(3));
        assertThat(response.hasErrors(), is(false));
    }

    @Test
    public void resultsGetFullConfluenceUrl() throws Exception {
        final RemoteResponse<List<ConfluenceSearchResult>> response = handler.handle(mockResponseFor(CONF59_FILLED_SEARCH_REULTS));
        final List<ConfluenceSearchResult> entity = response.getEntity();
        assertThat(entity.get(0).getUrl(), equalTo("http://example.com/confluence/display/ds/demo"));
        assertThat(entity.get(1).getUrl(), equalTo("http://example.com/confluence/display/ds/demo+page"));
        assertThat(entity.get(2).getUrl(), equalTo("http://example.com/confluence/display/ds/2017/01/30/A+demo+of+HTML+content"));
    }

    @Test
    public void resultsHaveSanitisedAndHtmlContentMarkedWithSearchTerms() throws Exception {
        final RemoteResponse<List<ConfluenceSearchResult>> response = handler.handle(mockResponseFor(CONF59_FILLED_SEARCH_REULTS));
        final List<ConfluenceSearchResult> entity = response.getEntity();
        assertThat(entity.get(0).getTitle(), equalTo("demo"));
        assertThat(entity.get(0).getTitleContent(), equalTo("<mark>demo</mark>"));
        assertThat(entity.get(0).getExcerpt(), startsWith("Link demo! http://demo! Date Jan 30, 2017 demo!"));
        assertThat(entity.get(0).getExcerptContent(), startsWith("Link <mark>demo</mark>! http://demo! Date Jan 30, 2017 <mark>demo</mark>!"));

        assertThat(entity.get(2).getExcerptContent(), not(anyOf(
                containsString("<script"),
                containsString("script>"),
                containsString("<img")
        )));
    }

    @Test
    public void resultsAreExplicitlyCleanedOfBadHtmlTags() throws Exception {
        final String title1 = "A <b>bold <u>title</u></b> with a <script>alert(1)</script> script";
        final Map<String,Object> entity1 = MapBuilder.build(
                "id", "12345",
                "title", title1,
                "type", "page",
                "_links", MapBuilder.build("webui", "/x/xss"));
        final Map<String,Object> result1 = MapBuilder.build(
                "title", title1,
                "excerpt", "Some <script>alert(2)</script> tags, <strong>and</strong> <img onload='alert(3)' src=javascript:x /> tags",
                "entityType", "content",
                "content", entity1);
        final String payload = makeResultsFor(Lists.newArrayList(new JSONObject(result1).toString()));
        final RemoteResponse<List<ConfluenceSearchResult>> response = handler.handle(mockResponseFor(payload));
        final List<ConfluenceSearchResult> entity = response.getEntity();
        assertThat(entity.get(0).getTitle(), equalTo("A bold title with a script"));
        assertThat(entity.get(0).getTitleContent(), equalTo("A \n<b>bold <u>title</u></b> with a  script"));
        assertThat(entity.get(0).getExcerpt(), equalTo("Some tags, and tags"));
        assertThat(entity.get(0).getExcerptContent(), equalTo("Some  tags, \n<strong>and</strong>  tags"));
    }

    private static String makeResultsFor(@Nonnull List<String> searchResults) {
        final String baseResult = "{" +
                "   \"results\":%s," +
                "   \"start\":0," +
                "   \"limit\":25," +
                "   \"size\":%d," +
                "   \"totalSize\":%d," +
                "   \"cqlQuery\":\"text ~ demo\"," +
                "   \"searchDuration\":97," +
                "   \"_links\":{\"base\":\"http://example.com/confluence\",\"context\":\"/confluence\"}" +
                "}";
        return String.format(baseResult, searchResults, searchResults.size(), searchResults.size());
    }

    private static final String CONF59_SEARCH_TEXT = "demo";
    private static final String CONF59_SEARCHRESULT_A = "{" +
            "    \"content\":{" +
            "        \"id\":\"753670\"," +
            "        \"type\":\"page\"," +
            "        \"title\":\"demo\"," +
            "        \"_links\":{\"webui\":\"/display/ds/demo\",\"tinyui\":\"/x/BoAL\",\"self\":\"http://example.com/confluence/rest/api/content/753670\"}," +
            "        \"_expandable\":{\"container\":\"\",\"metadata\":\"\",\"extensions\":\"\",\"operations\":\"\",\"children\":\"\",\"history\":\"/rest/api/content/753670/history\",\"ancestors\":\"\",\"body\":\"\",\"version\":\"\",\"descendants\":\"\",\"space\":\"/rest/api/space/ds\"}" +
            "    }," +
            "    \"title\":\"@@@hl@@@demo@@@endhl@@@\"," +
            "    \"excerpt\":\"Link @@@hl@@@demo@@@endhl@@@! http://demo! Date Jan 30, 2017 @@@hl@@@demo@@@endhl@@@! http://demo! No link preview available. Please open the link for details. Open\"," +
            "    \"url\":\"/display/ds/demo\"," +
            "    \"resultGlobalContainer\":{\"title\":\"Demonstration Space\",\"displayUrl\":\"/display/ds\"}," +
            "    \"entityType\":\"content\"," +
            "    \"iconCssClass\":\"aui-iconfont-page-default\"," +
            "    \"lastModified\":\"2017-01-30T12:13:22.735+01:00\"," +
            "    \"friendlyLastModified\":\"Jan 30, 2017\"" +
            "}";
    private static final String CONF59_SEARCHRESULT_B = "{" +
            "    \"content\":{" +
            "       \"id\":\"753668\"," +
            "       \"type\":\"page\"," +
            "       \"title\":\"demo page\"," +
            "       \"_links\":{\"webui\":\"/display/ds/demo+page\",\"tinyui\":\"/x/BIAL\",\"self\":\"http://example.com/confluence/rest/api/content/753668\"}," +
            "       \"_expandable\":{\"container\":\"\",\"metadata\":\"\",\"extensions\":\"\",\"operations\":\"\",\"children\":\"\",\"history\":\"/rest/api/content/753668/history\",\"ancestors\":\"\",\"body\":\"\",\"version\":\"\",\"descendants\":\"\",\"space\":\"/rest/api/space/ds\"}" +
            "   }," +
            "   \"title\":\"@@@hl@@@demo@@@endhl@@@ page\"," +
            "   \"excerpt\":\"page!\"," +
            "   \"url\":\"/display/ds/demo+page\"," +
            "   \"resultGlobalContainer\":{\"title\":\"Demonstration Space\",\"displayUrl\":\"/display/ds\"}," +
            "   \"entityType\":\"content\"," +
            "   \"iconCssClass\":\"aui-iconfont-page-default\"," +
            "   \"lastModified\":\"2017-01-30T12:13:04.204+01:00\"," +
            "   \"friendlyLastModified\":\"Jan 30, 2017\"" +
            "}";
    private static final String CONF59_SEARCHRESULT_C = "{" +
            "    \"content\":{" +
            "       \"id\":\"753666\"," +
            "       \"type\":\"blogpost\"," +
            "       \"title\":\"demo blog\"," +
            "       \"_links\":{\"webui\":\"/display/ds/2017/01/30/A+demo+of+HTML+content\",\"tinyui\":\"/x/AoAL\",\"self\":\"http://example.com/confluence/rest/api/content/753666\"}," +
            "       \"_expandable\":{\"container\":\"\",\"metadata\":\"\",\"extensions\":\"\",\"operations\":\"\",\"children\":\"\",\"history\":\"/rest/api/content/753666/history\",\"ancestors\":\"\",\"body\":\"\",\"version\":\"\",\"descendants\":\"\",\"space\":\"/rest/api/space/ds\"}" +
            "    }," +
            "    \"title\":\"A @@@hl@@@demo@@@endhl@@@ of HTML content\"," +
            "    \"excerpt\":\"We have: a Confluence page link to create a page. an existing page an external link: https://docs.atlassian.com/atlassianconfluence/REST/5.9.1/d3e964 https://docs.atlassian.com/atlassianconfluence/REST/5.9.1/d3e964 Some bad string @@@hl@@@demos@@@endhl@@@: &lt;script&gt;alert(&#39;123&#39;);&lt;/script&gt; &lt;img src=x onerror=alert(123) /&gt; &lt;svg&gt;&lt;script&gt;123\"," +
            "    \"url\":\"/display/ds/2017/01/30/A+demo+of+HTML+content\"," +
            "    \"resultGlobalContainer\":{\"title\":\"Demonstration Space\",\"displayUrl\":\"/display/ds\"}," +
            "    \"entityType\":\"content\"," +
            "    \"iconCssClass\":\"aui-iconfont-page-blogpost\"," +
            "    \"lastModified\":\"2017-01-30T12:12:49.140+01:00\"," +
            "    \"friendlyLastModified\":\"Jan 30, 2017\"" +
            "}";
    private static final String CONF59_FILLED_SEARCH_REULTS = makeResultsFor(Lists.newArrayList(CONF59_SEARCHRESULT_A, CONF59_SEARCHRESULT_B, CONF59_SEARCHRESULT_C));
}
