package com.atlassian.jira.plugin.link.confluence.service.rest;

import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import org.hsqldb.lib.StringInputStream;

import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ConfluenceRestResponseHandlerTestUtils {
    static Response mockResponseFor(final String body) throws ResponseException {
        final Map<String, String> headers = MapBuilder.<String, String>newBuilder()
                .add("Content-type", "application/json")
                .toHashMap();
        final Response response = mock(Response.class);
        when(response.getResponseBodyAsString()).thenReturn(body);
        when(response.getResponseBodyAsStream()).thenReturn(new StringInputStream(body));
        when(response.getHeaders()).thenReturn(headers);
        return response;
    }
}
