package com.atlassian.jira.plugins.ha.testapi.client;

import com.atlassian.jira.testkit.client.BackdoorControl;
import com.atlassian.jira.testkit.client.JIRAEnvironmentData;

/**
 * Simple backdoor control for activating and deactivating a cluster member.
 *
 * @since v1.0
 */
public class ClusterControl<T extends BackdoorControl<T>> extends BackdoorControl<T> {
    private static final String HA_PLUGIN_REST_PATH = "ha";

    /**
     * Creates a new BackdoorControl.
     *
     * @param environmentData a JIRAEnvironmentData
     */
    public ClusterControl(final JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    @Override
    protected String getRestModulePath() {
        return HA_PLUGIN_REST_PATH;
    }

    public void activate() {
        createResource().path("nodes").path("current").put("true");
    }

    public void passivate() {
        createResource().path("nodes").path("current").put("false");
    }
}
