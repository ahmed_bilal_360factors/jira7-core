package com.atlassian.jira.plugins.ha.testapi.test;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.openqa.selenium.WebDriver;

import static com.atlassian.collectors.CollectorsUtil.toImmutableMap;
import static java.util.function.Function.identity;

/**
 * For ha web driver tests, we use a separate browser for each node. We keep these browsers around between tests to
 * prevent us from creating/destroying browsers needlessly. New browser windows can also be created _by_ a test (e.g.
 * if they click a link which targets a new tab or window). We want to make sure that any _extra_ web driver instances
 * created during a test are _closed_ at the end of a test, but we want to keep our original per-node browsers. This
 * class remembers the active window handler (the original starting browser) of each node at the beginning of a test,
 * and let's us close any extra windows at the end. This is managed automatically for any ha web driver test through
 * the {@link WebDriverInstanceMonitorRule}, which is included in the default set of web rules by
 * {@link JiraHaWebTestRules#forWebDriverTest(Object)}.
 *
 * @see WebDriverInstanceMonitorRule
 * @see JiraHaWebTestRules#forWebDriverTest(Object)
 * @since v7.3
 */
class WebDriverInstanceMonitor {
    private Map<String, JiraCluster.Node> originalNodeHandles;

    public WebDriverInstanceMonitor(List<JiraCluster.Node> nodes) {
        this.originalNodeHandles = nodes.stream().collect(toImmutableMap(this::getWindowHandle, identity()));
    }

    public void closeExtraWindows() {
        originalNodeHandles.entrySet().stream().forEach(entry -> {
            final String originalHandle = entry.getKey();
            final JiraCluster.Node node = entry.getValue();

            getDriver(node).getWindowHandles().stream()
                    .filter(handle -> !Objects.equals(originalHandle, handle))
                    .forEach(otherHandle -> {
                        switchToHandle(node, otherHandle);
                        closeCurrentDriver(node);
                    });

            switchToHandle(node, originalHandle);
        });
    }

    private void switchToHandle(JiraCluster.Node node, String otherHandle) {
        getDriver(node).switchTo().window(otherHandle);
    }

    private void closeCurrentDriver(JiraCluster.Node node) {
        getDriver(node).close();
    }

    private String getWindowHandle(JiraCluster.Node node) {
        return getDriver(node).getWindowHandle();
    }

    private WebDriver getDriver(JiraCluster.Node node) {
        return node.product().getTester().getDriver().getDriver();
    }
}
