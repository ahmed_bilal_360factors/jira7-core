package com.atlassian.jira.plugins.ha.testapi.test.driver;

import com.atlassian.jira.pageobjects.JiraTestedProduct;

/**
 * Creates JiraTestedProduct instances for a given node for use in functional tests.
 *
 * @since v7.3
 */
public interface JiraTestedProductFactory {
    JiraTestedProduct create(final String nodeName);
}
