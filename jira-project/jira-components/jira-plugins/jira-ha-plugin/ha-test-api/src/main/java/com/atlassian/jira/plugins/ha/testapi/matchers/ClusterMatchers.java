package com.atlassian.jira.plugins.ha.testapi.matchers;

import com.atlassian.jira.plugins.ha.testapi.test.JiraCluster;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.util.Collection;

import static com.atlassian.collectors.CollectorsUtil.toImmutableList;
import static org.hamcrest.core.Every.everyItem;

public class ClusterMatchers {
    public static Matcher<JiraCluster> allNodes(Matcher<JiraCluster.Node> nodeMatcher) {
        return new AllNodesInClusterMatcher(nodeMatcher);
    }

    /**
     * The preferred matcher for asserting over nodes in a cluster is {@link ClusterMatchers#allNodes(Matcher)}, but
     * this API is sometimes needed when a test asserts on a subset of nodes (i.e. make an assertion on 2 of 3 nodes
     * after an unclean shutdown).
     */
    public static Matcher<Collection<? extends JiraCluster.Node>> everyNode(Matcher<JiraCluster.Node> nodeMatcher) {
        return new AllNodesInCollectionMatcher(nodeMatcher);
    }

    static class AllNodesInClusterMatcher extends TypeSafeMatcher<JiraCluster> {
        private final AllNodesInCollectionMatcher delegateMatcher;

        public AllNodesInClusterMatcher(Matcher<JiraCluster.Node> nodeMatcher) {
            this.delegateMatcher = new AllNodesInCollectionMatcher(nodeMatcher);
        }

        @Override
        protected boolean matchesSafely(JiraCluster jiraCluster) {
            return delegateMatcher.matchesSafely(jiraCluster.nodes());
        }

        @Override
        public void describeTo(Description description) {
            delegateMatcher.describeTo(description);
        }

        @Override
        protected void describeMismatchSafely(JiraCluster jiraCluster, Description mismatchDescription) {
            delegateMatcher.describeMismatchSafely(jiraCluster.nodes(), mismatchDescription);
        }
    }

    static class AllNodesInCollectionMatcher extends TypeSafeMatcher<Collection<? extends JiraCluster.Node>> {
        private final Matcher<JiraCluster.Node> nodeMatcher;

        public AllNodesInCollectionMatcher(Matcher<JiraCluster.Node> nodeMatcher) {
            this.nodeMatcher = nodeMatcher;
        }

        @Override
        protected boolean matchesSafely(Collection<? extends JiraCluster.Node> nodes) {
            return everyItem(nodeMatcher).matches(nodes);
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("All nodes in collection to match ")
                    .appendValue(nodeMatcher);
        }

        @Override
        protected void describeMismatchSafely(Collection<? extends JiraCluster.Node> nodes, Description mismatchDescription) {
            Collection nonMatchingNodes = nodes.stream()
                    .filter(node -> !nodeMatcher.matches(node))
                    .collect(toImmutableList());

            mismatchDescription.appendValue("Nodes ")
                    .appendValue(nonMatchingNodes)
                    .appendText(" do not match ")
                    .appendValue(nodeMatcher)
                    .appendText(String.format(" (%d/%d successful matches)",
                            nodes.size() - nonMatchingNodes.size(), nodes.size()));
        }
    }
}
