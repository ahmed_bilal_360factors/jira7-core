package com.atlassian.jira.plugins.ha.testapi.matchers;

import com.atlassian.jira.functest.framework.backdoor.ZeroDowntimeControl;
import com.atlassian.jira.plugins.ha.testapi.test.JiraCluster;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.util.Objects;

import static com.atlassian.jira.plugins.ha.testapi.matchers.DelegatingMatcher.matchWithDelegate;

public class ClusterStateMatchers {
    public static Matcher<JiraCluster.Node> isInUpgradeState(ZeroDowntimeControl.UpgradeState state) {
        return new UpgradeStateMatcher(state);
    }

    public static Matcher<JiraCluster.Node> upgradeState(Matcher<ZeroDowntimeControl.UpgradeState> delegateMatcher) {
        return matchWithDelegate(node -> node.backdoor().zdu().currentState().getState(), delegateMatcher);
    }

    static class UpgradeStateMatcher extends TypeSafeMatcher<JiraCluster.Node> {
        private final ZeroDowntimeControl.UpgradeState expectedUpgradeState;

        UpgradeStateMatcher(ZeroDowntimeControl.UpgradeState expectedUpgradeState) {
            this.expectedUpgradeState = expectedUpgradeState;
        }

        @Override
        protected boolean matchesSafely(JiraCluster.Node node) {
            return Objects.equals(expectedUpgradeState, node.backdoor().zdu().currentState().getState());
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("Node to have upgrade state ")
                    .appendValue(expectedUpgradeState);
        }

        @Override
        protected void describeMismatchSafely(JiraCluster.Node node, Description mismatchDescription) {
            mismatchDescription.appendText("Node has upgrade state ")
                    .appendValue(node.backdoor().zdu().currentState().getState());
        }
    }
}
