package com.atlassian.jira.plugins.ha.testapi.test.driver;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.pageobjects.TestedProductFactory;

/**
 * Creates JiraTestedProduct instances for use in functional tests (not web drivers). Every tested product created with
 * this factory will share _the same_ web driver. This should be be used for tests where we _don't_ need to instrument
 * multiple browsers across multiple nodes (i.e. integration tests where we can get by with just each node's backdoor).
 *
 * @since v7.3
 */
public class JiraFuncTestedProductFactory implements JiraTestedProductFactory {
    @Override
    public JiraTestedProduct create(String nodeName) {
        return TestedProductFactory.create(JiraTestedProduct.class, nodeName, null);
    }
}
