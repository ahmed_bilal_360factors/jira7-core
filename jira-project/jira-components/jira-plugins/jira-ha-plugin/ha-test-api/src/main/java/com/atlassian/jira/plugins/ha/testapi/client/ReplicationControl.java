package com.atlassian.jira.plugins.ha.testapi.client;

import java.util.concurrent.TimeUnit;

import javax.annotation.Nonnull;

import com.atlassian.jira.testkit.client.BackdoorControl;
import com.atlassian.jira.testkit.client.JIRAEnvironmentData;

/**
 * Backdoor for controlling clustering replication.
 *
 * @since v1.0
 */
public class ReplicationControl extends BackdoorControl<ReplicationControl> {
    private static final String HA_PLUGIN_REST_PATH = "ha";

    /**
     * Creates a new BackdoorControl.
     *
     * @param environmentData a JIRAEnvironmentData
     */
    public ReplicationControl(final JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    @Override
    protected String getRestModulePath() {
        return HA_PLUGIN_REST_PATH;
    }

    public void waitForAllReplicationOperations(long time, @Nonnull TimeUnit timeUnit) {
        long timeMillis = timeUnit.toMillis(time);
        createResource().path("replication").path("waitForAllReplicationOperations")
                .queryParam("maxTimeToWaitMs", String.valueOf(timeMillis))
                .get(String.class);
    }

    public void waitForClusterNodes(int nodeCount, long time, @Nonnull TimeUnit timeUnit) {
        long timeMillis = timeUnit.toMillis(time);
        createResource().path("replication").path("waitForClusterNodes")
                .queryParam("nodeCount", String.valueOf(nodeCount))
                .queryParam("maxTimeToWaitMs", String.valueOf(timeMillis))
                .get(String.class);
    }

    public int getPendingOperationCount() {
        return Integer.parseInt(createResource().path("replication").path("pendingOperationCount")
                .get(String.class));
    }

}
