package com.atlassian.jira.plugins.ha.testapi.client;

import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.plugins.ha.testapi.test.JiraCluster;
import com.atlassian.jira.testkit.beans.WorkflowSchemeData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

public class ClusterCleaner {

    private static final Logger log = LoggerFactory.getLogger(ClusterCleaner.class);

    private final JiraCluster cluster;

    /**
     * Any node in the cluster, it doesn't matter which one.
     */
    private final JiraCluster.Node node;

    public ClusterCleaner(JiraCluster cluster) {
        this.cluster = cluster;
        this.node = cluster.anyNode();
    }

    private Backdoor backdoor() {
        return node.backdoor();
    }

    public void safelyDeleteCustomField(final String fieldId) {
        if (fieldId != null) {
            try {
                backdoor().customFields().deleteCustomField(fieldId);
            } catch (final Throwable e) {
                log.debug("Couldn't delete custom field.", e);
            }
        }
    }

    public void safelyDeleteProject(String projectKey) {
        if (projectKey != null) {
            try {
                backdoor().project().deleteProject(projectKey);
            } catch (final Throwable e) {
                log.debug("Couldn't delete project.", e);
            }
        }
    }

    public void safelyDeleteIssueSecurityScheme(String issueSecuritySchemeName) {
        if (issueSecuritySchemeName != null) {
            try {
                node.admin().issueSecuritySchemes().deleteScheme(issueSecuritySchemeName);
            } catch (final Throwable e) {
                log.debug("Couldn't delete issue security scheme.", e);
            }
        }
    }

    /**
     * Deletes the group with the given name via the node 1 back door.
     *
     * @param name ignored if blank
     */
    public final void safelyDeleteGroup(final String name) {
        if (isNotBlank(name)) {
            try {
                backdoor().usersAndGroups().deleteGroup(name);
            } catch (final Throwable e) {
                log.debug("Couldn't delete group.", e);
            }
        }
    }

    /**
     * Deletes the user with the given username via the node 1 back door.
     *
     * @param username ignored if blank
     */
    public final void safelyDeleteUser(final String username) {
        if (isNotBlank(username)) {
            try {
                backdoor().usersAndGroups().deleteUser(username);
            } catch (final Throwable e) {
                log.debug("Couldn't delete user.", e);
            }
        }
    }

    public void safelyDeleteWorkflow(String workflowName) {
        if (workflowName != null) {
            try {
                node.admin().workflows().goTo().delete(workflowName);
            } catch (Throwable e) {
                log.debug("Couldn't delete workflow.", e);
            }
        }
    }

    public void safelyDeleteWorkflowScheme(String workflowSchemeName) {
        if (workflowSchemeName != null) {
            try {
                WorkflowSchemeData workflowSchemeData =
                        backdoor().workflowSchemes().getWorkflowSchemeByName(workflowSchemeName);
                backdoor().workflowSchemes().deleteScheme(workflowSchemeData.getId());
            } catch (Throwable e) {
                log.debug("Couldn't delete workflow.", e);
            }
        }
    }

    public void safelyDeletePermissionScheme(Long schemeId) {
        if (schemeId != null) {
            try {
                backdoor().permissionSchemes().deleteScheme(schemeId);
            } catch (Exception e) {
                log.debug("Couldn't delete permission scheme.", e);
            }
        }
    }
}
