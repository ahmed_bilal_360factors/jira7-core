package com.atlassian.jira.plugins.ha.testapi.test.driver;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.pageobjects.TestedProductFactory;
import java.util.HashMap;
import java.util.Map;

/**
 * Creates JiraTestedProduct instances for use in functional tests (not web drivers). Every tested product created with
 * this factory will share _the same_ web driver. This should be be used for tests where we _don't_ need to instrument
 * multiple browsers across multiple nodes (i.e. integration tests where we can get by with just each node's backdoor).
 *
 * @since v7.3
 */
public class JiraWebDriverTestedProductFactory implements JiraTestedProductFactory {
    private final WebDriverTesterFactory webDriverTesterFactory = new WebDriverTesterFactory();
    /**
     * A NodeName -> JiraTestedProduct map to cache tested products that have already been created for a given node. We
     * don't want to create more web drivers than we have nodes - if a node already has a tested product created for it,
     * then we shouldn't create a new one.
     */
    private final Map<String, JiraTestedProduct> nodeProducts = new HashMap<>();

    @Override
    public JiraTestedProduct create(final String nodeName) {
        return nodeProducts.computeIfAbsent(nodeName, this::createNewTestedProductForNode);
    }

    private JiraTestedProduct createNewTestedProductForNode(String nodeName) {
        return TestedProductFactory.create(JiraTestedProduct.class, nodeName, webDriverTesterFactory);
    }
}
