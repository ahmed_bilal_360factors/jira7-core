package com.atlassian.jira.plugins.ha.testapi.test;

import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.config.properties.JiraSystemProperties;
import com.atlassian.jira.functest.rule.StatementDecorator;
import com.atlassian.jira.pageobjects.config.junit4.rule.DirtyWarningTerminatorRule;
import com.atlassian.jira.pageobjects.config.junit4.rule.LandingPageRedirectRule;
import com.atlassian.jira.pageobjects.config.junit4.rule.RuleChainBuilder;
import com.atlassian.jira.pageobjects.config.junit4.rule.SetupJiraRule;
import com.atlassian.jira.pageobjects.setup.JiraWebDriverScreenshotRule;
import com.atlassian.jira.pageobjects.setup.JiraWebTestLogger;
import com.atlassian.jira.plugins.ha.testapi.client.ClusterCleaner;
import com.atlassian.jira.plugins.ha.testapi.test.driver.JiraFuncTestedProductFactory;
import com.atlassian.jira.plugins.ha.testapi.test.driver.JiraTestedProductFactory;
import com.atlassian.jira.plugins.ha.testapi.test.driver.JiraWebDriverTestedProductFactory;
import com.atlassian.jira.plugins.ha.testapi.test.driver.SplitWindowSizeRuleFactory;
import com.atlassian.webdriver.testing.rule.LogPageSourceRule;
import com.atlassian.webdriver.testing.rule.SessionCleanupRule;
import com.atlassian.webdriver.testing.rule.WindowSizeRule;
import com.google.inject.Binder;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.Provider;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;
import org.junit.rules.RuleChain;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.collectors.CollectorsUtil.toImmutableList;
import static org.junit.rules.RuleChain.emptyRuleChain;

/**
 * A set of JUnit Rules for HA tests.
 * Make sure all rules in this chain are cluster-safe.
 */
public final class JiraHaWebTestRules {
    private static final Logger log = LoggerFactory.getLogger(JiraHaWebTestRules.class);

    // We want these to be shared across tests so that we don't create more browser instances than we need (i.e. more
    // than one per node).
    private static final JiraTestedProductFactory webDriverTestedProductFactory = new JiraWebDriverTestedProductFactory();
    private static final JiraTestedProductFactory funcTestedProductFactory = new JiraFuncTestedProductFactory();

    /**
     * private constructor
     */
    private JiraHaWebTestRules() {
        // do nothing
    }

    /**
     * To make it easier to run cluster tests from local machines with cluster nodes running from JMake, default the
     * system properties if none have been explicitly set to those that JMake would use.
     * If left blank, every node will have exactly the same URL which is <i>never</i> correct.
     */
    private static void setUpDefaultSystemPropertiesIfNeeded(int nodeCount) {
        boolean propertyDefaultsUsed = false;
        for (int i = 0; i <= nodeCount; i++) {

            String nodeName = "node" + (i + 1);
            int nodeDefaultPort = 8090 + i;

            //Properties that need to be set:
            //baseurl.nodeX, http.nodeX.port, context.nodeX.path

            propertyDefaultsUsed |= setPropertyIfNotAlreadySet("baseurl." + nodeName, "http://localhost:" + nodeDefaultPort + "/jira");
            propertyDefaultsUsed |= setPropertyIfNotAlreadySet("http." + nodeName + ".port", String.valueOf(nodeDefaultPort));
            propertyDefaultsUsed |= setPropertyIfNotAlreadySet("context." + nodeName + ".path", "/jira");
        }

        if (propertyDefaultsUsed) {
            log.info("JMake defaults were used for cluster configuration for running tests.");
        }
    }

    private static boolean setPropertyIfNotAlreadySet(String name, String value) {
        JiraProperties props = JiraSystemProperties.getInstance();
        if (props.getProperty(name) == null) {
            props.setProperty(name, value);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Initialise the base rules we need for func tests. This should be used when we don't actually need to instrument
     * browsers for each node (i.e. we can get by with the backdoors for each node).
     * <p/>
     * See ha-tests/it.com.atlassian.jira.plugins.ha.func for examples.
     */
    public static RuleChain forFuncTest(Object test) {
        // Create the cluster here so that we can inject it into a test if needed. These nodes share a WebDriver tester
        // and other resources, so we perform the application and browser setup no a single node (it doesn't matter
        // which one).
        final JiraCluster cluster = new JiraCluster(createNodesForFuncTest());
        final JiraCluster.Node someNode = cluster.anyNode();
        final Injector injector = Guice.createInjector(new ClusterModule(cluster));

        return RuleChainBuilder.forProduct(someNode.product())
                .around(WindowSizeRule.class)
                .around(SessionCleanupRule.class)
                .around(JiraWebDriverScreenshotRule.class)
                .around(DirtyWarningTerminatorRule.class)
                .around(LandingPageRedirectRule.class)
                .around(SetupJiraRule.class)
                .around(new LogPageSourceRule(someNode.product().getTester().getDriver(), JiraWebTestLogger.LOGGER))
                .around(new InjectFieldsRule(injector, test))
                .around(new WaitForClusterStartupRule(cluster))
                .build();
    }

    /**
     * Initialise the base rules we need for web driver tests. This should be used when want to instrument, at the same
     * time, multiple browsers targeting different nodes. If browser instrumentation is not needed, then
     * JiraHaWebTestRules#forFuncTest should be used.
     * <p/>
     * See ha-tests/it.com.atlassian.jira.plugins.ha.webdriver for examples.
     */
    public static RuleChain forWebDriverTest(Object test) {
        final List<JiraCluster.Node> nodes = createNodesForWebDriverTest();
        final JiraCluster cluster = new JiraCluster(nodes);
        final JiraCluster.Node anyNode = cluster.anyNode();
        final Injector injector = Guice.createInjector(new ClusterModule(cluster));

        // These rules define behaviour on the application as a whole, and so only need to be defined on one node (it
        // doesn't matter which node).
        final RuleChain clusterRules = RuleChainBuilder.forProduct(anyNode.product())
                .around(LandingPageRedirectRule.class)
                .around(SetupJiraRule.class)
                .around(new InjectFieldsRule(injector, test))
                .around(new WaitForClusterStartupRule(cluster))
                .build();

        // These rules define behaviour on the web browser, and so need to be defined on _each_ node. Use the same
        // WindowSizeFactory for each node so that we can keep track of each window's position (we want them to be
        // side-by-side so that we can see everything from the Bamboo captures).
        final SplitWindowSizeRuleFactory splitWindowSizeRuleFactory = new SplitWindowSizeRuleFactory();
        final RuleChain nodeRules = nodes.stream()
                .map(JiraCluster.Node::product)
                .map(product -> RuleChainBuilder.forProduct(product)
                        .around(splitWindowSizeRuleFactory.create(product.getTester().getDriver()))
                        .around(SessionCleanupRule.class)
                        .around(new WebDriverInstanceMonitorRule(nodes))
                        .around(JiraWebDriverScreenshotRule.class)
                        .around(DirtyWarningTerminatorRule.class)
                        .around(new LogPageSourceRule(product.getTester().getDriver(), JiraWebTestLogger.LOGGER))
                        .build())
                .reduce(emptyRuleChain(), (accumulatorChain, nextChain) -> nextChain.around(accumulatorChain));

        return nodeRules.around(clusterRules);
    }

    private static List<JiraCluster.Node> createNodesForFuncTest() {
        return createNodesForTest(funcTestedProductFactory);
    }

    private static List<JiraCluster.Node> createNodesForWebDriverTest() {
        return createNodesForTest(webDriverTestedProductFactory);
    }

    private static List<JiraCluster.Node> createNodesForTest(JiraTestedProductFactory testedProductFactory) {
        // The number of nodes for these tests is currently restricted to 2 - but that might be expanded to n in the future.
        final int numberOfNodes = 2;
        setUpDefaultSystemPropertiesIfNeeded(numberOfNodes);
        return IntStream.rangeClosed(1, numberOfNodes)
                .mapToObj(i -> "node" + i)
                .map(testedProductFactory::create)
                .map(JiraCluster.Node::new)
                .collect(toImmutableList());
    }

    /**
     * Can't reuse the one in jira-func-tests because it changes incompatibly between 7.1 and 7.2.
     */
    private static class InjectFieldsRule implements TestRule {
        private final Injector injector;
        private final Object testInstance;

        public InjectFieldsRule(final Injector injector, final Object testInstance) {
            this.injector = injector;
            this.testInstance = testInstance;
        }

        @Override
        public Statement apply(final Statement base, final Description description) {
            return new StatementDecorator(base, () -> injector.injectMembers(testInstance));
        }
    }

    /**
     * Before each test runs, ensure all nodes in the cluster have fully started up.
     */
    private static class WaitForClusterStartupRule extends TestWatcher {

        private final JiraCluster cluster;

        public WaitForClusterStartupRule(JiraCluster cluster) {
            this.cluster = cluster;
        }

        @Override
        protected void starting(Description description) {
            cluster.waitForStartup();
        }
    }

    /**
     * Injects cluster test objects using Guice.
     */
    private static class ClusterModule implements Module {

        private final JiraCluster cluster;

        public ClusterModule(JiraCluster cluster) {
            this.cluster = cluster;
        }

        @Override
        public void configure(Binder binder) {
            binder.bind(JiraCluster.class).toInstance(cluster);
            binder.bind(JiraCluster.Node.class).toProvider(new AlwaysDifferentNodeProvider(cluster));
            binder.bind(ClusterCleaner.class).toInstance(cluster.cleaner());
        }
    }

    /**
     * A provider implementation that will provide every node in the cluster one-by-one, but will fail with an error
     * when asking for more nodes than there are.
     */
    private static class AlwaysDifferentNodeProvider implements Provider<JiraCluster.Node> {
        private final List<JiraCluster.Node> availableNodes;
        private final int totalNodeCount;

        public AlwaysDifferentNodeProvider(JiraCluster cluster) {
            availableNodes = new ArrayList<>(cluster.nodes());
            totalNodeCount = availableNodes.size();
        }

        @Override
        public JiraCluster.Node get() {
            if (availableNodes.isEmpty()) {
                throw new IndexOutOfBoundsException("Attempting to inject more nodes than are available: " + totalNodeCount);
            }
            return availableNodes.remove(0);
        }
    }
}
