package com.atlassian.jira.plugins.ha.testapi.client;

import com.atlassian.jira.testkit.client.BackdoorControl;
import com.atlassian.jira.testkit.client.JIRAEnvironmentData;

/**
 * Backdoor for creating orphan issues.
 *
 * @since v1.0
 */
public class OrphanIssueControl<T extends BackdoorControl<T>> extends BackdoorControl<T> {
    private static final String HA_PLUGIN_REST_PATH = "ha";

    public OrphanIssueControl(final JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    @Override
    protected String getRestModulePath() {
        return HA_PLUGIN_REST_PATH;
    }

    public String createOrphanIssue(Long issueId, Long nonExistingProjectId) {
        return createResource().path("orphanIssue").path("create")
                .queryParam("issueId", String.valueOf(issueId))
                .queryParam("nonExistingProjectId", String.valueOf(nonExistingProjectId))
                .get(String.class);
    }

    public void deleteOrphanIssue(Long issueId) {
        createResource().path("orphanIssue").path("delete")
                .queryParam("issueId", String.valueOf(issueId))
                .get(String.class);
    }
}
