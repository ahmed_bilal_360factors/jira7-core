package com.atlassian.jira.plugins.ha.testapi.test.driver;

import com.atlassian.browsers.BrowserConfig;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.webdriver.AtlassianWebDriver;
import com.atlassian.webdriver.WebDriverFactory;
import com.atlassian.webdriver.browsers.AutoInstallConfiguration;
import com.atlassian.webdriver.pageobjects.DefaultWebDriverTester;
import com.atlassian.webdriver.pageobjects.WebDriverTester;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Unlike the default atlassian-selemium factory which caches singleton web drivers to prevent us creating more browsers
 * than we typically need, this factory naively creates a new web driver every time we ask. This lets us instrument
 * multiple browsers at the same time in the same test (ultimately allowing us to instrument two nodes at the same
 * time).
 * <p/>
 * We do cache web drivers by node (no need to create more browsers than we have nodes), but that logic needs to happen
 * at a higher level in ({@link JiraWebDriverTestedProductFactory} specifically, where we have some scope over the nodes
 * themselves).
 * <p/>
 * This does copy/recreate _some_ logic from {@link com.atlassian.webdriver.LifecycleAwareWebDriverGrid} (the default
 * factory), but since our use case here is fairly unique, I can't think of a more appropriate place for this code to live.
 *
 * @see JiraWebDriverTestedProductFactory
 * @see com.atlassian.webdriver.LifecycleAwareWebDriverGrid
 * @since v7.3
 */
class WebDriverTesterFactory implements TestedProductFactory.TesterFactory<WebDriverTester> {
    private static final Logger Log = LoggerFactory.getLogger(WebDriverTesterFactory.class);

    @Override
    public WebDriverTester create() {
        final BrowserConfig config = AutoInstallConfiguration.setupBrowser();
        final AtlassianWebDriver driver = WebDriverFactory.getDriver(config);
        addShutdownHook(driver);
        return new DefaultWebDriverTester(driver);
    }

    private static void addShutdownHook(final WebDriver driver) {
        Log.debug("Adding shutdown hook for Web Driver: {}", driver);
        Runtime.getRuntime().addShutdownHook(new WebDriverQuitter(driver));
    }

    private static class WebDriverQuitter extends Thread {
        private final WebDriver driver;

        WebDriverQuitter(WebDriver driver) {
            this.driver = driver;
        }

        @Override
        public void run() {
            Log.debug("Shutting down Web Driver: {}", driver);
            try {
                driver.quit();
            } catch (NullPointerException | WebDriverException e) {
                Log.warn("Exception when trying to quit Web Driver {}: {}", driver, e.getMessage());
            }
        }
    }
}
