package com.atlassian.jira.plugins.ha.testapi.test.driver;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;

import static com.atlassian.jira.plugins.ha.testapi.test.driver.SplitWindowSizeRuleFactory.Positions.LEFT_SPLIT;
import static com.atlassian.jira.plugins.ha.testapi.test.driver.SplitWindowSizeRuleFactory.Positions.RIGHT_SPLIT;

/**
 * Generates window sizing rules when two browsers are running in parallel (i.e. for ZDU web driver tests). The first
 * time a size rule is requested, the window will fill half-width on the _left_ side of the screen. The second time a
 * size rule is requested, the window will fill half-width on the _right_ side of the screen. Successive requests will
 * throw {@link IllegalStateException}s.
 *
 * @see com.atlassian.jira.plugins.ha.test.JiraHaWebTestRules#forWebDriverTest(Object)
 * @since v7.3
 */
public class SplitWindowSizeRuleFactory {
    enum Positions {
        LEFT_SPLIT,
        RIGHT_SPLIT;

        public Point toPoint(final int windowWidth) {
            switch (this) {
                case RIGHT_SPLIT:
                    return new Point(windowWidth / 2, 0);
                case LEFT_SPLIT:
                default:
                    return new Point(0, 0);
            }
        }
    }

    /**
     * Queue up available positions (start with the left side of the screen, then the right).
     */
    private final Queue<Positions> windowPositions;

    public SplitWindowSizeRuleFactory() {
        windowPositions = new LinkedList<>(Arrays.asList(LEFT_SPLIT, RIGHT_SPLIT));
    }

    public TestWatcher create(WebDriver webDriver) {
        return new SplitWindowSizeRule(webDriver);
    }

    class SplitWindowSizeRule extends TestWatcher {
        private final WebDriver webDriver;
        private final Dimension maxWindowSize;

        SplitWindowSizeRule(WebDriver webDriver) {
            // Maximise the window first -> so that we can figure out the maximum width -> so that we can figure out
            // the mid-point of the host window -> so that we can display two windows at half width each.
            webDriver.manage().window().maximize();
            Dimension maxWindowSize = webDriver.manage().window().getSize();
            this.maxWindowSize = new Dimension(maxWindowSize.getWidth(), maxWindowSize.getHeight());
            this.webDriver = webDriver;
        }

        @Override
        protected void starting(Description description) {
            if (windowPositions.isEmpty()) {
                throw new IllegalStateException("This sizing strategies only supports up to 2 windows - you have requested too many!");
            } else {
                setWindowSize(this.maxWindowSize.getWidth() / 2, this.maxWindowSize.getHeight());
                setWindowPosition(windowPositions.remove().toPoint(this.maxWindowSize.getWidth()));
            }
        }

        private void setWindowSize(final int width, final int height) {
            webDriver.manage().window().setSize(new Dimension(width, height));
        }

        private void setWindowPosition(Point position) {
            webDriver.manage().window().setPosition(position);
        }
    }
}
