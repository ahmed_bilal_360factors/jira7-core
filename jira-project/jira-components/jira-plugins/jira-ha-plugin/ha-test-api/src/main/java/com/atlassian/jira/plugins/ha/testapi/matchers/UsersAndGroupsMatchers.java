package com.atlassian.jira.plugins.ha.testapi.matchers;

import com.atlassian.jira.plugins.ha.testapi.test.JiraCluster;
import org.hamcrest.Matcher;

import static com.atlassian.jira.plugins.ha.testapi.matchers.DelegatingMatcher.matchWithDelegate;

public class UsersAndGroupsMatchers {
    public static Matcher<JiraCluster.Node> userCount(Matcher<Integer> userCountMatcher) {
        return matchWithDelegate(node -> node.backdoor().usersAndGroups().getAllUsers().size(), userCountMatcher);
    }
}
