package com.atlassian.jira.plugins.ha.testapi.matchers;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.util.function.Function;


/**
 * Generic helper class to compose custom matchers.
 * <p>
 * Given a matcher that accepts a type U, constructs a matcher that accepts a type T where T is some type that can
 * access a U.
 * <p>
 * For example, given that U is a type UpgradeState, and T is a type ClusterNode, we can create a matcher that accepts
 * a ClusterNode by providing 1) a matcher that accepts an UpgradeState; and 2) a function that maps a ClusterNode on to
 * an Upgrade State.
 * <p>
 *
 * @param <T> The subject type of the new matcher.
 * @param <U> The subject type of the original delegate matcher.
 * @see ClusterStateMatchers
 */
public class DelegatingMatcher<T, U> extends TypeSafeMatcher<T> {
    private final Matcher<U> delegateMatcher;
    private final Function<T, U> valueSupplier;

    public static final <T, U> TypeSafeMatcher<T> matchWithDelegate(Function<T, U> valueSupplier, Matcher<U> delegateMatcher) {
        return new DelegatingMatcher<>(valueSupplier, delegateMatcher);
    }

    private DelegatingMatcher(Function<T, U> valueSupplier, Matcher<U> delegateMatcher) {
        this.delegateMatcher = delegateMatcher;
        this.valueSupplier = valueSupplier;
    }

    @Override
    protected boolean matchesSafely(T subject) {
        return delegateMatcher.matches(valueSupplier.apply(subject));
    }

    @Override
    public void describeTo(Description description) {
        delegateMatcher.describeTo(description);
    }

    @Override
    protected void describeMismatchSafely(T item, Description mismatchDescription) {
        delegateMatcher.describeMismatch(item, mismatchDescription);
    }
}
