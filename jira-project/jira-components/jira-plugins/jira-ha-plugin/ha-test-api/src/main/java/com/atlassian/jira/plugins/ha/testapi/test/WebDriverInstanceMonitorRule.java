package com.atlassian.jira.plugins.ha.testapi.test;

import java.util.List;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

/**
 * Rule-wrapper around {@link WebDriverInstanceMonitor} that, at the end of each test, closes any extra web driver
 * instances/windows created during that test.
 *
 * @see WebDriverInstanceMonitor
 * @see JiraHaWebTestRules#forWebDriverTest(Object)
 * @since v7.3
 */
class WebDriverInstanceMonitorRule extends TestWatcher {
    private WebDriverInstanceMonitor webDriverInstanceMonitor;

    public WebDriverInstanceMonitorRule(List<JiraCluster.Node> nodes) {
        this.webDriverInstanceMonitor = new WebDriverInstanceMonitor(nodes);
    }

    @Override
    protected void finished(Description description) {
        webDriverInstanceMonitor.closeExtraWindows();
    }
}
