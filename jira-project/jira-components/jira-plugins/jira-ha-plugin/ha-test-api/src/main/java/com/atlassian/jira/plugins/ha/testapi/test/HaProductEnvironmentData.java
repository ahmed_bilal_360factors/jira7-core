package com.atlassian.jira.plugins.ha.testapi.test;

import com.atlassian.jira.pageobjects.config.ProductInstanceBasedEnvironmentData;
import com.atlassian.pageobjects.ProductInstance;

import java.io.File;
import javax.annotation.Nonnull;

/**
 * An extension of ProductInstanceBasedEnvironmentData that allows us to set the location of the XML data.
 *
 * @since v1.0
 */
public class HaProductEnvironmentData extends ProductInstanceBasedEnvironmentData {
    private final File xmlDataLocation;

    public HaProductEnvironmentData(@Nonnull final ProductInstance productInstance, final String xmlLocation) {
        super(productInstance);
        xmlDataLocation = new File(xmlLocation);
    }

    @Override
    public File getXMLDataLocation() {
        return xmlDataLocation;
    }
}
