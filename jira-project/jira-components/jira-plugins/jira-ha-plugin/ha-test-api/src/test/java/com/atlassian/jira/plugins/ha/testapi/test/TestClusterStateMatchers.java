package com.atlassian.jira.plugins.ha.testapi.test;

import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.functest.framework.backdoor.ZeroDowntimeControl;
import org.junit.Test;

import static com.atlassian.jira.functest.framework.backdoor.ZeroDowntimeControl.UpgradeState.MIXED;
import static com.atlassian.jira.functest.framework.backdoor.ZeroDowntimeControl.UpgradeState.RUNNING_UPGRADE_TASKS;
import static com.atlassian.jira.functest.framework.backdoor.ZeroDowntimeControl.UpgradeState.STABLE;
import static com.atlassian.jira.plugins.ha.testapi.matchers.ClusterStateMatchers.isInUpgradeState;
import static com.atlassian.jira.plugins.ha.testapi.matchers.ClusterStateMatchers.upgradeState;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.isOneOf;
import static org.hamcrest.Matchers.not;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestClusterStateMatchers {
    @Test
    public void nodeHasStableState() {
        JiraCluster.Node node = mockNodeWithUpgradeState(STABLE);

        assertThat(node, isInUpgradeState(STABLE));
    }

    @Test
    public void nodeIsNotStable() {
        JiraCluster.Node node = mockNodeWithUpgradeState(MIXED);

        assertThat(node, not(isInUpgradeState(STABLE)));
    }

    @Test
    public void stateIsOneOf() {
        JiraCluster.Node node = mockNodeWithUpgradeState(STABLE);

        assertThat(node, upgradeState(isOneOf(MIXED, STABLE)));
    }

    @Test
    public void stateIsNoneOf() {
        JiraCluster.Node node = mockNodeWithUpgradeState(RUNNING_UPGRADE_TASKS);

        assertThat(node, upgradeState(not(isOneOf(MIXED, STABLE))));
    }

    private JiraCluster.Node mockNodeWithUpgradeState(ZeroDowntimeControl.UpgradeState upgradeState) {
        JiraCluster.Node node = mock(JiraCluster.Node.class);
        Backdoor backdoor = mock(Backdoor.class);
        ZeroDowntimeControl zeroDowntimeControl = mock(ZeroDowntimeControl.class);
        ZeroDowntimeControl.ClusterState clusterState = mock(ZeroDowntimeControl.ClusterState.class);

        when(node.backdoor()).thenReturn(backdoor);
        when(backdoor.zdu()).thenReturn(zeroDowntimeControl);
        when(zeroDowntimeControl.currentState()).thenReturn(clusterState);
        when(clusterState.getState()).thenReturn(upgradeState);

        return node;
    }
}
