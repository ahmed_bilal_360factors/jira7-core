package com.atlassian.jira.plugins.ha.testapi.test;

import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.testkit.beans.UserDTO;
import com.atlassian.jira.testkit.client.UsersAndGroupsControl;
import org.junit.Test;

import java.util.List;
import java.util.stream.IntStream;

import static com.atlassian.collectors.CollectorsUtil.toImmutableList;
import static com.atlassian.jira.plugins.ha.testapi.matchers.UsersAndGroupsMatchers.userCount;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestUsersAndGroupsMatchers {
    @Test
    public void userCountEquality() {
        JiraCluster.Node node = mockNodeWithUsersListOfSize(2);

        assertThat(node, userCount(is(2)));
    }

    @Test
    public void userCountComparison() {
        JiraCluster.Node node = mockNodeWithUsersListOfSize(2);

        assertThat(node, userCount(greaterThan(1)));
    }

    private JiraCluster.Node mockNodeWithUsersListOfSize(final int amount) {
        JiraCluster.Node node = mock(JiraCluster.Node.class);
        Backdoor backdoor = mock(Backdoor.class);
        UsersAndGroupsControl usersAndGroupsControl = mock(UsersAndGroupsControl.class);

        when(node.backdoor()).thenReturn(backdoor);
        when(backdoor.usersAndGroups()).thenReturn(usersAndGroupsControl);

        final List<UserDTO> users = IntStream.range(0, amount)
                .mapToObj(i -> mock(UserDTO.class))
                .collect(toImmutableList());

        when(usersAndGroupsControl.getAllUsers()).thenReturn(users);

        return node;
    }
}
