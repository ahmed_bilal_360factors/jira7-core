package com.atlassian.jira.plugins.ha.testapi.test;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Test;

import java.util.Collection;
import java.util.Objects;
import java.util.stream.Stream;

import static com.atlassian.collectors.CollectorsUtil.toImmutableList;
import static com.atlassian.jira.plugins.ha.testapi.matchers.ClusterMatchers.allNodes;
import static com.atlassian.jira.plugins.ha.testapi.matchers.ClusterMatchers.everyNode;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.not;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestClusterMatchers {
    @Test
    public void allNodesMatch() {
        final JiraCluster jiraCluster = mockClusterWithNodeToStrings("node", "node", "node");


        assertThat(jiraCluster, allNodes(toStringEquals("node")));
    }

    @Test
    public void someNodesMatch() {
        final JiraCluster jiraCluster = mockClusterWithNodeToStrings("node", "node", "not node");

        assertThat(jiraCluster, not(allNodes(toStringEquals("node"))));
    }

    @Test
    public void noNodesMatch() {
        final JiraCluster jiraCluster = mockClusterWithNodeToStrings("not node", "not node", "not node");

        assertThat(jiraCluster, not(allNodes(toStringEquals("node"))));
    }

    @Test
    public void assertOnNodesDirectly() {
        final JiraCluster jiraCluster = mockClusterWithNodeToStrings("node", "node", "node");
        final Collection<? extends JiraCluster.Node> nodes = jiraCluster.nodes();

        assertThat(nodes, everyNode(toStringEquals("node")));
    }

    private JiraCluster mockClusterWithNodeToStrings(final String... toStrings) {
        final Collection<? extends JiraCluster.Node> nodes = Stream.of(toStrings)
                .map(toStringValue -> {
                    JiraCluster.Node node = mock(JiraCluster.Node.class);
                    when(node.toString()).thenReturn(toStringValue);
                    return node;
                })
                .collect(toImmutableList());


        JiraCluster jiraCluster = mock(JiraCluster.class);
        doReturn(nodes).when(jiraCluster).nodes();

        return jiraCluster;
    }

    static Matcher<JiraCluster.Node> toStringEquals(final String toString) {
        return new NodeToStringMatcher(toString);
    }

    /**
     * Trivial matcher for testing that matches on a node's <code>toString</code> method.
     */
    static class NodeToStringMatcher extends TypeSafeMatcher<JiraCluster.Node> {
        private final String toStringValue;

        public NodeToStringMatcher(String toStringValue) {
            this.toStringValue = toStringValue;
        }

        @Override
        protected boolean matchesSafely(JiraCluster.Node node) {
            return Objects.equals(node.toString(), toStringValue);
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("Node::toString to equal ")
                    .appendValue(toStringValue);
        }
    }
}
