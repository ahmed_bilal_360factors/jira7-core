package it.com.atlassian.jira.plugins.ha.webdriver;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.pages.clustered.ViewUpgradesPage;
import com.atlassian.jira.plugins.ha.testapi.test.JiraCluster;
import com.atlassian.jira.plugins.ha.testapi.test.JiraHaWebTestRules;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.openqa.selenium.WebDriver;

import javax.inject.Inject;
import java.util.Objects;

import static com.atlassian.jira.matchers.RegexMatchers.regexMatchesPattern;
import static java.util.regex.Pattern.CASE_INSENSITIVE;
import static java.util.regex.Pattern.compile;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.StringContains.containsString;

/**
 * @since v7.3
 */
@WebTest({Category.WEBDRIVER_TEST, Category.CLUSTER})
public class JiraUpgradesTest {
    @Rule
    public TestRule rules = JiraHaWebTestRules.forWebDriverTest(this);

    @Inject
    private JiraCluster.Node node;

    private ViewUpgradesPage viewUpgrades;

    @Before
    public void setUp() {
        node.login();
        this.viewUpgrades = node.product().goTo(ViewUpgradesPage.class);
    }

    @Test
    public void followIntroDocumentationLink() {
        viewUpgrades.clickIntroDocumentationLink();
        switchFocusToNextWindow();

        assertThat("node is on documentation page", currentUrl(), regexMatchesPattern(compile("managing.zero.downtime.upgrades", CASE_INSENSITIVE)));
    }

    @Test
    public void followHealthCheckLink() {
        viewUpgrades.clickViewHealthCheckLink();

        assertThat("node is on support tools page", currentUrl(), containsString("servlet/stp"));
    }

    @Test
    public void followCheckAddonsLink() {
        viewUpgrades.clickCheckAddonsLink();

        assertThat("node is on upm page", currentUrl(), containsString("servlet/upm"));
    }

    @Test
    public void followDownloadLatestLink() {
        viewUpgrades.clickDownloadLatestLink();
        switchFocusToNextWindow();

        assertThat("node is on WAC", currentUrl(), containsString("atlassian.com/download"));
    }

    private void switchFocusToNextWindow() {
        final WebDriver driver = driver();
        final String currentWindow = driver.getWindowHandle();
        driver.getWindowHandles().stream()
                .filter(handle -> !Objects.equals(handle, currentWindow))
                .findFirst()
                .map(driver.switchTo()::window)
                .orElseThrow(() -> new RuntimeException("Couldn't change focused web driver window"));
    }

    private WebDriver driver() {
        return node.product().getTester().getDriver().getDriver();
    }

    private String currentUrl() {
        return driver().getCurrentUrl();
    }
}
