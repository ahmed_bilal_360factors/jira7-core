package it.com.atlassian.jira.plugins.ha.zdu;

import com.atlassian.jira.functest.framework.backdoor.ZeroDowntimeControl;
import com.atlassian.jira.plugins.ha.testapi.test.JiraCluster;
import com.atlassian.jira.plugins.ha.testapi.test.JiraHaWebTestRules;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import javax.inject.Inject;

import static org.junit.Assert.assertEquals;

/**
 * This test makes sure that tests will go red
 * if jira is not in MIXED state
 *
 * @since v7.4
 */
public class ClusterStateTest {

    @Rule
    public TestRule rules = JiraHaWebTestRules.forFuncTest(this);

    @Inject
    private JiraCluster.Node node1;

    @Inject
    private JiraCluster.Node node2;

    @Test
    public void ensureNode1KnowsThatClusterIsInMixedMode() throws Exception {
        assertEquals(ZeroDowntimeControl.UpgradeState.MIXED, node1.backdoor().zdu().currentState().getState());
    }

    @Test
    public void ensureNode2KnowsThatClusterIsInMixedMode() throws Exception {
        assertEquals(ZeroDowntimeControl.UpgradeState.MIXED, node2.backdoor().zdu().currentState().getState());
    }
}
