package it.com.atlassian.jira.plugins.ha.func;

import com.atlassian.jira.plugins.ha.testapi.client.ClusterCleaner;
import com.atlassian.jira.plugins.ha.testapi.test.JiraCluster;
import com.atlassian.jira.plugins.ha.testapi.test.JiraHaWebTestRules;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.rest.api.issue.IssueFields;
import com.atlassian.jira.rest.api.issue.IssueUpdateRequest;
import com.atlassian.jira.rest.api.issue.ResourceRef;
import com.atlassian.jira.testkit.beans.Resolution;
import com.atlassian.jira.testkit.client.restclient.SearchRequest;
import com.atlassian.jira.testkit.client.restclient.SearchResult;
import com.atlassian.jira.testkit.client.restclient.TransitionsClient;
import java.util.List;
import javax.inject.Inject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import static org.hamcrest.core.Is.is;

/**
 * This is a small test to test the JIRA HA testing framework is up
 *
 * @since v6.0
 */
public class ResolutionsClusterTest {
    private static final String PROJECT_KEY = "HSP";
    private TransitionsClient transitionsClient1;
    private TransitionsClient transitionsClient2;

    @Rule
    public TestRule rules = JiraHaWebTestRules.forFuncTest(this);

    @Inject
    private JiraCluster.Node node1;

    @Inject
    private JiraCluster.Node node2;

    @Inject
    private ClusterCleaner cleaner;

    @Before
    public void setup() {
        transitionsClient1 = new TransitionsClient(node1.environment());
        transitionsClient2 = new TransitionsClient(node2.environment());
    }

    @Test
    public void testAddResolution() throws InterruptedException {
        Resolution resolution = null;
        try {
            node1.login();
            node1.backdoor().project().addProject("Homosapiens", PROJECT_KEY, "admin");
            // Add a new resolution
            resolution = node1.backdoor().getTestkit().resolutions().createResolution("Abandoned", "Just For testing");
            IssueCreateResponse issueResp = node1.backdoor().issues().createIssue(PROJECT_KEY, "New HA Issue");

            node2.login();

            // Edit issu on node 2 to ensure resolution visible
            final IssueUpdateRequest issueUpdateRequest = new IssueUpdateRequest();
            issueUpdateRequest.fields(new IssueFields());
            issueUpdateRequest.transition(ResourceRef.withId("2")); // id for "Close Issue"
            issueUpdateRequest.fields().resolution(new ResourceRef().name("Abandoned"));

            transitionsClient2.postResponse(issueResp.key, issueUpdateRequest);
            SearchResult response = node2.backdoor().search().getSearch(new SearchRequest().jql("resolution = abandoned"));
            Assert.assertThat("Should get 1 issue", response.issues.size(), is(1));
        } finally {
            // Cleanup our stuff.
            cleaner.safelyDeleteProject(PROJECT_KEY);
            safelyDeleteResolution(resolution);
        }
    }

    @Test
    public void testUpdateResolution() throws InterruptedException {
        Resolution resolution = null;
        try {
            node1.login();
            node1.backdoor().project().addProject("Homosapiens", PROJECT_KEY, "admin");
            // Add a new resolution
            resolution = node1.backdoor().getTestkit().resolutions().createResolution("Abandoned", "Just For testing");
            IssueCreateResponse issueResp = node1.backdoor().issues().createIssue(PROJECT_KEY, "New HA Issue");

            node2.login();

            // Edit issue on node 2 to ensure resolution visible
            final IssueUpdateRequest issueUpdateRequest = new IssueUpdateRequest();
            issueUpdateRequest.fields(new IssueFields());
            issueUpdateRequest.transition(ResourceRef.withId("2")); // id for "Close Issue"
            issueUpdateRequest.fields().resolution(new ResourceRef().name("Abandoned"));

            transitionsClient2.postResponse(issueResp.key, issueUpdateRequest);
            SearchResult response = node2.backdoor().search().getSearch(new SearchRequest().jql("resolution = abandoned"));
            Assert.assertThat("Should get 1 issue", response.issues.size(), is(1));

            // Edit the resolution on node 1 and search for results using the new name on node 2
            resolution = node1.backdoor().getTestkit().resolutions().updateResolution(Long.valueOf(resolution.getId()), "Reclaimed", "Just For testing2");
            SearchResult response2 = node2.backdoor().search().getSearch(new SearchRequest().jql("resolution = reclaimed"));
            Assert.assertThat("Should get 1 issue", response2.issues.size(), is(1));

        } finally {
            // Cleanup our stuff.
            cleaner.safelyDeleteProject(PROJECT_KEY);
            safelyDeleteResolution(resolution);
        }
    }


    @Test
    public void testDeleteResolution() throws InterruptedException {
        Resolution resolution = null;
        try {
            node1.login();
            node2.login();

            // Add a new resolution
            final List<Resolution> originalResolutions = node2.backdoor().getTestkit().resolutions().getResolutions();
            resolution = node1.backdoor().getTestkit().resolutions().createResolution("Abandoned", "Just For testing");
            int newResolutionCount = node2.backdoor().getTestkit().resolutions().getResolutions().size();
            Assert.assertThat("Should get 1 more resolution now", newResolutionCount, is(originalResolutions.size() + 1));

            // Delete from node2
            node2.backdoor().getTestkit().resolutions().deleteResolution(Long.valueOf(resolution.getId()));
            final List<Resolution> finalResolutions = node1.backdoor().getTestkit().resolutions().getResolutions();
            Assert.assertThat("Should get original resolution now", finalResolutions.size(), is(originalResolutions.size()));
            for (int i = 0; i < finalResolutions.size(); i++) {
                Assert.assertThat("Should get original resolution now", finalResolutions.get(i).getId(), is(originalResolutions.get(i).getId()));
            }
        } finally {
            // Cleanup our stuff.
            safelyDeleteResolution(resolution);
        }
    }

    protected void safelyDeleteResolution(Resolution resolution) {
        if (resolution != null) {
            try {
                node1.backdoor().getTestkit().resolutions().deleteResolution(Long.valueOf(resolution.getId()));
            } catch (Exception e) {
            }
        }
    }

}
