package it.com.atlassian.jira.plugins.ha.func;

import com.atlassian.jira.functest.framework.backdoor.ProjectControlExt;
import com.atlassian.jira.plugins.ha.testapi.client.ClusterCleaner;
import com.atlassian.jira.plugins.ha.testapi.test.JiraCluster;
import com.atlassian.jira.plugins.ha.testapi.test.JiraHaWebTestRules;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.testkit.client.restclient.Issue;
import java.util.Arrays;
import java.util.List;
import javax.inject.Inject;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static org.hamcrest.core.IsNot.not;

/**
 * Integration test of projects in a cluster.
 *
 * @since v6.3
 */
@RunWith(Parameterized.class)
@Ignore("Flaky test, @natashbar is investigating...")
public class ProjectsTest {
    private static final Logger log = LoggerFactory.getLogger(ProjectsTest.class);

    private static final String PROJECT_KEY_1 = "AMF";
    private static final String PROJECT_KEY_2 = "BLA";
    private static final String PROJECT_KEY_3 = "TST";

    // Run operations on different nodes in random order, repeat to make sure that we test different orders.
    private static final int TEST_REPEAT_COUNT = 4;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    // In order to run tests many times
    @Parameterized.Parameters
    public static List<Object[]> data() {
        return Arrays.asList(new Object[TEST_REPEAT_COUNT][0]);
    }

    @Rule
    public TestRule rules = JiraHaWebTestRules.forFuncTest(this);

    @Inject
    private JiraCluster cluster;

    @Inject
    private JiraCluster.Node node1;

    @Inject
    private JiraCluster.Node node2;

    @Inject
    private ClusterCleaner cleaner;

    @Test
    public void testNewProjectVisibleOnAnotherNode() throws InterruptedException {
        try {
            log.debug("testNewProjectVisibleOnAllNodes");
            final long projectId = node1.backdoor().project().addProject("Test new project", PROJECT_KEY_1, cluster.adminUsername());

            final long projectId1 = node2.backdoor().project().getProjectId(PROJECT_KEY_1);
            log.debug("Project ID from a random but different node: " + projectId1);
            assertThat("Should see the project in all nodes.", projectId1, is(projectId));
        } finally {
            cleaner.safelyDeleteProject(PROJECT_KEY_1);
            cleaner.safelyDeleteProject(PROJECT_KEY_2);
            cleaner.safelyDeleteProject(PROJECT_KEY_3);
        }
    }

    @Test
    public void testKeyCannotBeUsedAgain() {
        try {
            final long projectId = project().addProject("Test key reuse", PROJECT_KEY_1, cluster.adminUsername());
            assertThat(project().getProjectKeys(projectId), hasItem(PROJECT_KEY_1));
            project().addProjectKey(projectId, PROJECT_KEY_3);
            log.info("Keys: " + project().getProjectKeys(projectId));

            final long blahId = project().addProject("Blah", PROJECT_KEY_2, cluster.adminUsername());
            exception.expect(Exception.class);
            project().editProjectKey(blahId, PROJECT_KEY_3);
        } finally {
            cleaner.safelyDeleteProject(PROJECT_KEY_1);
            cleaner.safelyDeleteProject(PROJECT_KEY_2);
            cleaner.safelyDeleteProject(PROJECT_KEY_3);
        }
    }

    @Test
    public void testProjectDeleteRemovesKeys() {
        try {
            final long projectId = project().addProject("Test edit", PROJECT_KEY_1, cluster.adminUsername());
            project().addProjectKey(projectId, PROJECT_KEY_2);
            project().addProjectKey(projectId, PROJECT_KEY_3);

            project().deleteProject(PROJECT_KEY_1);
            final long blahId = project().addProject("Blah", PROJECT_KEY_2, cluster.adminUsername());
            assertThat(project().getProjectKeys(blahId), hasItem(PROJECT_KEY_2));
        } finally {
            cleaner.safelyDeleteProject(PROJECT_KEY_1);
            cleaner.safelyDeleteProject(PROJECT_KEY_2);
            cleaner.safelyDeleteProject(PROJECT_KEY_3);
        }
    }

    @Test
    public void testProjectEditIsvisibleOnAnotherNode() {
        try {
            final long projectId1 = node1.backdoor().project().addProject("Test edit", PROJECT_KEY_1, cluster.adminUsername());
            node1.admin().project()
                    .editProject(projectId1, "New name", "Description can change.", "http://no.where.com/no/url");

            final long prjectId2 = node2.backdoor().project().getProjectId(PROJECT_KEY_1);
            log.debug("Project ID from another random but different node: " + prjectId2);
            String name = node2.backdoor().project().getProjectName(prjectId2);
            assertThat("Should see project modifications in all nodes.", name, is("New name"));
        } finally {
            cleaner.safelyDeleteProject(PROJECT_KEY_1);
            cleaner.safelyDeleteProject(PROJECT_KEY_2);
            cleaner.safelyDeleteProject(PROJECT_KEY_3);
        }
    }

    @Test
    public void testKeyRenamedWhileBackgroundReindexing() throws Exception {
        try {
            final long projectId = project().addProject("Test rename while background reindexing", PROJECT_KEY_1,
                    cluster.adminUsername());
            final IssueCreateResponse response = node1.backdoor().issues().createIssue(PROJECT_KEY_1, "Test issue");
            final String issueId = response.id();
            final String issueKey = response.key();

            node1.backdoor().barrier().raiseBarrierAndRun("backgroundReindex", new Runnable() {
                @Override
                public void run() {
                    node1.backdoor().indexing().startInBackground();
                    // If we used editProjectKey() here, re-indexing will go on forever.
                    project().editProjectKeyNoWaitForReindex(projectId, PROJECT_KEY_2);
                }
            });
            node1.backdoor().indexing().getInBackgroundProgress().waitForCompletion();
            cluster.waitForSync();
            Issue issue = node2.backdoor().issues().getIssue(issueKey);
            assertThat(issue.id, is(issueId));
            assertThat(issue.key, not(is(issueKey)));
        } finally {
            cleaner.safelyDeleteProject(PROJECT_KEY_1);
            cleaner.safelyDeleteProject(PROJECT_KEY_2);
            cleaner.safelyDeleteProject(PROJECT_KEY_3);
        }
    }

    protected ProjectControlExt project() {
        return node1.backdoor().project();
    }
}
