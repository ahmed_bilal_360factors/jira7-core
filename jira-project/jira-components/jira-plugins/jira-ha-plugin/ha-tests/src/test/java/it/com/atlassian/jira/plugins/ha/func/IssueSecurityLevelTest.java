package it.com.atlassian.jira.plugins.ha.func;

import com.atlassian.jira.functest.framework.admin.IssueSecurityLevel;
import com.atlassian.jira.functest.framework.admin.IssueSecuritySchemes;
import com.atlassian.jira.plugins.ha.testapi.client.ClusterCleaner;
import com.atlassian.jira.plugins.ha.testapi.test.JiraCluster;
import com.atlassian.jira.plugins.ha.testapi.test.JiraHaWebTestRules;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.rest.api.issue.IssueFields;
import com.atlassian.jira.rest.api.issue.ResourceRef;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.testkit.client.restclient.SearchRequest;
import com.atlassian.jira.testkit.client.restclient.SearchResult;
import javax.inject.Inject;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;

/**
 * Integration test of issue security level in a cluster.
 *
 * @since v6.3
 */
public class IssueSecurityLevelTest {
    private static final String PROJECT_KEY_1 = "AMF";
    private static final String PROJECT_NAME = "Amphibians";
    private static final String ISSUE_SECURITY_SCHEME_NAME = "Test_Scheme";
    private static final String ISSUE_SECURITY_LEVEL_NAME = "Test_Security_Level";
    private static final String USERNAME_JACK = "Jack";
    private static final String PASSWORD_JACK = "Jack";
    private static final String USERNAME_JILL = "Jill";
    private static final String PASSWORD_JILL = "Jill";

    @Rule
    public TestRule rules = JiraHaWebTestRules.forFuncTest(this);

    @Inject
    private JiraCluster cluster;

    @Inject
    private JiraCluster.Node node1;

    @Inject
    private JiraCluster.Node node2;

    @Inject
    private ClusterCleaner cleaner;

    @Test
    public void testSecuritySchemeIsVisibleOnOtherNodes() {
        try {
            node1.admin().issueSecuritySchemes()
                    .newScheme(ISSUE_SECURITY_SCHEME_NAME, "Test issue security scheme");
            final IssueSecuritySchemes.IssueSecurityScheme scheme =
                    node2.admin().issueSecuritySchemes()
                            .getScheme(ISSUE_SECURITY_SCHEME_NAME);
            assertThat("Security scheme is not visible on another node.", scheme, notNullValue());
        } finally {
            cleaner.safelyDeleteProject(PROJECT_KEY_1);
            cleaner.safelyDeleteIssueSecurityScheme(ISSUE_SECURITY_SCHEME_NAME);
        }
    }

    @Test
    public void testIssueIsVisibleToTheRightPeople() {
        try {
            node1.admin().usersAndGroups()
                    .addUser(USERNAME_JACK, PASSWORD_JACK, USERNAME_JACK.toUpperCase(), USERNAME_JACK + "@example.com");
            node1.admin().usersAndGroups()
                    .addUser(USERNAME_JILL, PASSWORD_JILL, USERNAME_JILL.toUpperCase(), USERNAME_JILL + "@example.com");

            node1.admin().issueSecuritySchemes()
                    .newScheme(ISSUE_SECURITY_SCHEME_NAME, "Test issue security scheme");
            final IssueSecuritySchemes.IssueSecurityScheme scheme =
                    node1.admin().issueSecuritySchemes().getScheme(ISSUE_SECURITY_SCHEME_NAME);
            IssueSecurityLevel level = scheme.newLevel(ISSUE_SECURITY_LEVEL_NAME, "Test issue security scheme level");
            level.addIssueSecurity(IssueSecurityLevel.IssueSecurity.USER, cluster.adminUsername());
            level.addIssueSecurity(IssueSecurityLevel.IssueSecurity.USER, USERNAME_JACK);

            node1.admin().permissionSchemes().defaultScheme().grantPermissionToSingleUser(
                    Permissions.Permission.SET_ISSUE_SECURITY.getId(), cluster.adminUsername());
            final long projectId = node1.backdoor().project().addProject(PROJECT_NAME, PROJECT_KEY_1, cluster.adminUsername());
            node1.admin().project()
                    .associateIssueLevelSecurityScheme(PROJECT_NAME, ISSUE_SECURITY_SCHEME_NAME);
            IssueCreateResponse restrictedIssue =
                    node1.backdoor().issues().createIssue(projectId, "restricted issue", cluster.adminUsername());
            node1.backdoor().issues().setIssueFields(restrictedIssue.key,
                    new IssueFields().securityLevel(ResourceRef.withName(ISSUE_SECURITY_LEVEL_NAME)));
            IssueCreateResponse visibleIssue =
                    node1.backdoor().issues().createIssue(projectId, "visible issue", cluster.adminUsername());

            node2.login(USERNAME_JILL, PASSWORD_JILL);
            node2.backdoor().search().loginAs(USERNAME_JILL, PASSWORD_JILL);
            SearchResult response = node2.backdoor().search().getSearch(new SearchRequest().jql("project = " + PROJECT_KEY_1));
            assertThat("Jill should see only one issue.", response.issues.size(), is(1));
            node2.backdoor().search().loginAs(USERNAME_JACK, PASSWORD_JACK);
            response = node2.backdoor().search().getSearch(new SearchRequest().jql("project = " + PROJECT_KEY_1));
            assertThat("Jack should see both issues.", response.issues.size(), is(2));
            node2.backdoor().search().loginAs(cluster.adminUsername(), cluster.adminPassword());
        } finally {
            cleaner.safelyDeleteUser(USERNAME_JACK);
            cleaner.safelyDeleteUser(USERNAME_JILL);
            cleaner.safelyDeleteProject(PROJECT_KEY_1);
            cleaner.safelyDeleteIssueSecurityScheme(ISSUE_SECURITY_SCHEME_NAME);
        }
    }
}
