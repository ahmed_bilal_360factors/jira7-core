package it.com.atlassian.jira.plugins.ha.func;

import com.atlassian.jira.plugins.ha.testapi.client.ClusterCleaner;
import com.atlassian.jira.plugins.ha.testapi.test.JiraCluster;
import com.atlassian.jira.plugins.ha.testapi.test.JiraHaWebTestRules;
import com.atlassian.jira.testkit.beans.UserDTO;
import com.atlassian.jira.testkit.client.UsersAndGroupsControl;
import javax.inject.Inject;
import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * Integration test of user management in a cluster.
 *
 * @since 6.3
 */
public class UserManagementTest {
    private static final String TEST_GROUP = "quilters";
    private static final String TEST_USER = "jbloggs";

    @Rule
    public TestRule rules = JiraHaWebTestRules.forFuncTest(this);

    @Inject
    private JiraCluster.Node node1;

    @Inject
    private JiraCluster.Node node2;

    @Inject
    private ClusterCleaner cleaner;

    @Test
    public void userAddedOnOneNodeShouldBeVisibleOnAnother() {
        // Set up
        assertUserPresence(TEST_USER, node1, false);
        assertUserPresence(TEST_USER, node2, false);
        node1.backdoor().usersAndGroups().addUser(TEST_USER);

        // Invoke and check
        assertUserPresence(TEST_USER, node2, true);
    }

    @Test
    public void userDeletedOnOneNodeShouldNotBeVisibleOnAnother() {
        // Set up
        node1.backdoor().usersAndGroups().addUser(TEST_USER);
        assertUserPresence(TEST_USER, node1, true);
        assertUserPresence(TEST_USER, node2, true);

        // Invoke
        node1.backdoor().usersAndGroups().deleteUser(TEST_USER);

        // Check
        assertUserPresence(TEST_USER, node1, false);
        assertUserPresence(TEST_USER, node2, false);
    }

    @Test
    public void userAddedToGroupOnOneNodeShouldBeInThatGroupOnAnother() {
        // Set up
        node1.backdoor().usersAndGroups().addUser(TEST_USER);
        node1.backdoor().usersAndGroups().addGroup(TEST_GROUP);

        // Invoke
        node1.backdoor().usersAndGroups().addUserToGroup(TEST_USER, TEST_GROUP);

        // Check
        assertTestUserInTestGroup(node2, true);
    }

    private void assertTestUserInTestGroup(final JiraCluster.Node node, final boolean expectedValue) {
        assertThat(node.backdoor().usersAndGroups().isUserInGroup(TEST_USER, TEST_GROUP), is(expectedValue));
    }

    @Test
    public void userRemovedFromGroupOnOneNodeShouldNotBeInThatGroupOnAnotherNode() {
        // Set up
        node1.backdoor().usersAndGroups().addUser(TEST_USER);
        node1.backdoor().usersAndGroups().addGroup(TEST_GROUP);
        node1.backdoor().usersAndGroups().addUserToGroup(TEST_USER, TEST_GROUP);
        assertTestUserInTestGroup(node2, true);

        // Invoke
        node1.backdoor().usersAndGroups().removeUserFromGroup(TEST_USER, TEST_GROUP);

        // Check
        assertTestUserInTestGroup(node2, false);
    }

    @Test
    public void emailAddressUpdatedOnOneNodeShouldBeVisibleOnAnother() {
        // Set up
        node1.backdoor().usersAndGroups().addUser(TEST_USER);
        final UserDTO testUser = node1.backdoor().usersAndGroups().getUserByName(TEST_USER);
        final String newEmail = testUser.getEmail();

        // Invoke
        node1.backdoor().usersAndGroups().updateUser(testUser);

        // Check
        final UserDTO userOnNode2 = node2.backdoor().usersAndGroups().getUserByName(TEST_USER);
        assertThat(userOnNode2.getEmail(), is(newEmail));
    }

    private void assertUserPresence(final String username, final JiraCluster.Node node, final boolean expectedPresence) {
        final UsersAndGroupsControl users = node.backdoor().usersAndGroups();
        assertThat(users.userExists(username), is(expectedPresence));
    }

    @After
    public void deleteTestUser() {
        cleaner.safelyDeleteUser(TEST_USER);
        cleaner.safelyDeleteGroup(TEST_GROUP);
    }
}
