package it.com.atlassian.jira.plugins.ha.func;

import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.pageobjects.pages.JiraLoginPage;
import com.atlassian.jira.plugins.ha.testapi.client.ClusterCleaner;
import com.atlassian.jira.plugins.ha.testapi.test.JiraCluster;
import com.atlassian.jira.plugins.ha.testapi.test.JiraHaWebTestRules;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.rest.api.issue.IssueFields;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.test.util.lic.ApplicationLicenseConstants;
import com.atlassian.jira.testkit.client.PermissionsControl;
import com.atlassian.jira.testkit.client.UsersAndGroupsControl;
import com.atlassian.jira.testkit.client.restclient.Issue;
import com.atlassian.jira.testkit.client.restclient.SearchRequest;
import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import net.sourceforge.jwebunit.WebTester;
import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import static com.atlassian.jira.functest.framework.FunctTestConstants.BUILT_IN_CUSTOM_FIELD_KEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TEXT_SEARCHER;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_TEXTFIELD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.JIRA_USERS_ROLE_ID;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * This is a small test to test the JIRA HA testing framework is up.
 *
 * @since v6.0
 */
public class PermissionChangesTest {
    private static final String CUSTOMFIELD_ = "customfield_";
    private static final String PROJECT_KEY = "HSP";
    private static final String PROJECT_KEY2 = "MKY";

    @Rule
    public TestRule rules = JiraHaWebTestRules.forFuncTest(this);

    @Inject
    private JiraCluster cluster;

    @Inject
    private JiraCluster.Node node1;

    @Inject
    private JiraCluster.Node node2;

    @Inject
    private ClusterCleaner cleaner;

    /**
     * Custom field ID
     */
    private String customFieldId;

    /**
     * Permission scheme ID
     */
    private Long permissionSchemeId;

    /**
     * Name of user to delete on teardown
     */
    private String userToDelete;

    /**
     * Name of group to delete on teardown
     */
    private String groupToDelete;

    /**
     * Additional teardown operations
     */
    private List<Runnable> additionalTearDown = new ArrayList<>();

    @After
    public void teardown() {
        cluster.waitForSync();
        cleaner.safelyDeleteProject(PROJECT_KEY);
        cleaner.safelyDeleteProject(PROJECT_KEY2);
        cleaner.safelyDeleteCustomField(customFieldId);
        cleaner.safelyDeletePermissionScheme(permissionSchemeId);
        cleaner.safelyDeleteUser(userToDelete);
        cleaner.safelyDeleteGroup(groupToDelete);
        additionalTearDown.forEach(Runnable::run);
        cluster.waitForSync();
    }

    @Test
    public void testUserDeleted() {
        String user = "barney";
        node1.backdoor().usersAndGroups().addUser(user, user, "Barney of Bermuda", "barney@example.com");
        userToDelete = user;
        node2.login(user, user);
        node2.logout();

        node1.backdoor().usersAndGroups().deleteUser(user);
        userToDelete = null;
        node2.product().gotoLoginPage().performLoginSteps(user, user, true);
        assertTrue("User remains on login screen after failed login attempt", node2.product().isAt(JiraLoginPage.class));
    }

    @Test
    public void testUserNotInUsersGroup() {
        String user = "barney";
        node1.backdoor().usersAndGroups().addUser(user, user, "Barney of Bermuda", "barney@example.com");
        userToDelete = user;
        node2.login(user, user);
        node2.logout();

        node1.backdoor().usersAndGroups().removeUserFromGroup(user, "jira-users");
        node2.product().gotoLoginPage().performLoginSteps(user, user, true);
        assertTrue("User remains on login screen after failed login attempt", node2.product().isAt(JiraLoginPage.class));
    }

    @Test
    public void testGroupDeleted() {
        String user = "barney";
        String group = "jira-peeps";

        UsersAndGroupsControl users = node1.backdoor().usersAndGroups();
        users.addGroup(group);
        groupToDelete = group;
        node1.backdoor().applicationRoles().putRoleWithDefaultsSelectedByDefault(
                ApplicationLicenseConstants.SOFTWARE_KEY, true,
                ImmutableList.of("jira-administrators", "jira-users", group),
                ImmutableList.of("jira-users", group));
        additionalTearDown.add(() -> node1.backdoor().applicationRoles().putRoleWithDefaultsSelectedByDefault(
                ApplicationLicenseConstants.SOFTWARE_KEY, true,
                ImmutableList.of("jira-administrators", "jira-users"),
                ImmutableList.of("jira-users")));

        users.addUser(user, user, "Barney of Bermuda", "barney@example.com");
        userToDelete = user;
        users.removeUserFromGroup(user, "jira-users");
        users.addUserToGroup(user, group);

        node2.login(user, user);
        node2.logout();

        users.deleteGroup(group);
        groupToDelete = null;
        node2.product().gotoLoginPage().performLoginSteps(user, user, true);
        assertTrue("User remains on login screen after failed login attempt", node2.product().isAt(JiraLoginPage.class));
    }

    @Test
    public void testGroupDoesNotHaveUserPermission() {
        PermissionsControl permissions = node1.backdoor().permissions();
        String user = "barney";
        String group = "jira-peeps";

        UsersAndGroupsControl users = node1.backdoor().usersAndGroups();
        users.addGroup(group);
        groupToDelete = group;
        node1.backdoor().applicationRoles().putRoleWithDefaultsSelectedByDefault(
                ApplicationLicenseConstants.SOFTWARE_KEY, true,
                ImmutableList.of("jira-administrators", "jira-users", group),
                ImmutableList.of("jira-users", group));
        additionalTearDown.add(() -> node1.backdoor().applicationRoles().putRoleWithDefaultsSelectedByDefault(
                ApplicationLicenseConstants.SOFTWARE_KEY, true,
                ImmutableList.of("jira-administrators", "jira-users"),
                ImmutableList.of("jira-users"))
        );

        users.addUser(user, user, "Barney of Bermuda", "barney@example.com");
        userToDelete = user;
        users.removeUserFromGroup(user, "jira-users");
        users.addUserToGroup(user, group);

        node2.login(user, user);
        node2.logout();

        node1.backdoor().applicationRoles().putRoleWithDefaultsSelectedByDefault(
                ApplicationLicenseConstants.SOFTWARE_KEY, true,
                ImmutableList.of("jira-administrators", "jira-users"),
                ImmutableList.of("jira-users"));

        node2.product().gotoLoginPage().performLoginSteps(user, user, true);
        assertTrue("User remains on login screen after failed login attempt", node2.product().isAt(JiraLoginPage.class));
    }

    @Test
    public void testPermissionSchemeAdded() {
        permissionSchemeId = node1.backdoor().permissionSchemes().createScheme(
                "Test Permission Scheme", "A test permission scheme");

        WebTester tester = node2.tester();
        tester.gotoPage("secure/admin/ViewPermissionSchemes.jspa");
        tester.assertTextPresent("Test Permission Scheme");
        tester.assertTextPresent("A test permission scheme");
    }

    @Test
    public void testPermissionSchemeDeleted() {
        permissionSchemeId = node1.backdoor().permissionSchemes().createScheme(
                "Test Permission Scheme", "A test permission scheme");

        // Load the scheme before deleting just to make sure any caches, etc are accounted for
        WebTester tester = node2.tester();
        tester.gotoPage("secure/admin/ViewPermissionSchemes.jspa");
        tester.assertTextPresent("Test Permission Scheme");
        tester.assertTextPresent("A test permission scheme");

        node1.backdoor().permissionSchemes().deleteScheme(permissionSchemeId);
        permissionSchemeId = null;

        tester.gotoPage("secure/admin/ViewPermissionSchemes.jspa");
        tester.assertTextNotPresent("Test Permission Scheme");
        tester.assertTextNotPresent("A test permission scheme");
    }

    @Test
    public void testPermissionSchemeRenamed() {
        permissionSchemeId = node1.backdoor().permissionSchemes().createScheme(
                "Test Permission Scheme", "A test permission scheme");

        WebTester tester1 = node1.tester();
        tester1.gotoPage("secure/admin/EditPermissionScheme!default.jspa?schemeId=" + permissionSchemeId);
        tester1.setFormElement("name", "Updated Test Permission Scheme");
        tester1.setFormElement("description", "An updated test permission scheme");
        tester1.submit();

        WebTester tester = node2.tester();
        tester.gotoPage("secure/admin/ViewPermissionSchemes.jspa");
        tester.assertTextPresent("Updated Test Permission Scheme");
        tester.assertTextPresent("An updated test permission scheme");
    }

    @Test
    public void testCustomFieldConfig() throws InterruptedException {
        safelyAddUserFred();

        // Add a project with a custom field and an issue
        long hspId = node1.backdoor().project().addProject("Homosapiens", PROJECT_KEY, "admin");
        customFieldId = node1.backdoor().customFields().createCustomField("CF1", "Test CF 1",
                BUILT_IN_CUSTOM_FIELD_KEY + ":" + CUSTOM_FIELD_TYPE_TEXTFIELD,
                BUILT_IN_CUSTOM_FIELD_KEY + ":" + CUSTOM_FIELD_TEXT_SEARCHER);
        node1.backdoor().screens().addFieldToScreen("Default Screen", "CF1");
        node1.backdoor().fieldConfiguration().associateCustomFieldWithProject(customFieldId, "Homosapiens");
        IssueCreateResponse issueResp = node1.backdoor().issues().createIssue(PROJECT_KEY, "New HA Issue");
        Long customFieldId = Long.valueOf(this.customFieldId.substring(CUSTOMFIELD_.length()));
        node1.backdoor().issues().setIssueFields(issueResp.key, new IssueFields().customField(customFieldId, "Walter was here"));

        // Add a second project with issue as a control
        node1.backdoor().project().addProject("Monkey", PROJECT_KEY2, "admin");
        node1.backdoor().issues().createIssue(PROJECT_KEY2, "New MKY Issue");
        cluster.waitForSync();

        // Custom field search should be available on node2
        assertThat("Should get 1 issue", getSearch(node2.backdoor(), "CF1 ~ Walter"), hasSize(1));

        // Now set the permissions on the project so Fred can't see it
        permissionSchemeId = node1.backdoor().permissionSchemes().copyDefaultScheme("HSP Scheme");
        node1.backdoor().project().setPermissionScheme(hspId, permissionSchemeId);
        node1.backdoor().permissionSchemes().removeProjectRolePermission(permissionSchemeId, Permissions.BROWSE, JIRA_USERS_ROLE_ID);

        // Fred can't see the Homosapiens project issue
        node2.loginAsUser();
        assertThat("Should not find the issue", getSearch(node2.backdoor(), "summary ~ HA"), hasSize(0));

        // But can find the Monkey project issue
        assertThat("Should get 1 issue", getSearch(node2.backdoor(), "summary ~ MKY"), hasSize(1));

        // Restore permission
        node1.backdoor().permissionSchemes().addProjectRolePermission(permissionSchemeId, Permissions.BROWSE, JIRA_USERS_ROLE_ID);

        // Fred can see the Homosapiens project issue again
        assertThat("Should get 1 issue", getSearch(node2.backdoor(), "summary ~ HA"), hasSize(1));
    }

    private List<Issue> getSearch(Backdoor backdoor, String jql) {
        return backdoor.search().getSearch(new SearchRequest().jql(jql)).issues;
    }

    private void safelyAddUserFred() {
        // Add user fred
        if (!node1.backdoor().usersAndGroups().userExists("fred")) {
            node1.backdoor().usersAndGroups().addUser("fred", "fred", "Fred of Denmark", "fred@example.com");
        }
    }
}
