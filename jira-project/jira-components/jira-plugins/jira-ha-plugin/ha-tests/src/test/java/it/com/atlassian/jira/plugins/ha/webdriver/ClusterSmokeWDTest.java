package it.com.atlassian.jira.plugins.ha.webdriver;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.plugins.ha.testapi.test.JiraCluster;
import com.atlassian.jira.plugins.ha.testapi.test.JiraHaWebTestRules;
import javax.inject.Inject;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

/**
 * This test doesn't assert anything useful, it just does performs a bare minimum set of actions in two browsers across
 * two nodes to validate that our cluster tests are setup properly.
 *
 * @since v7.3
 */
@WebTest({Category.WEBDRIVER_TEST, Category.CLUSTER})
public class ClusterSmokeWDTest {
    @Rule
    public TestRule rules = JiraHaWebTestRules.forWebDriverTest(this);

    @Inject
    private JiraCluster.Node node1;

    @Inject
    private JiraCluster.Node node2;

    @Test
    public void webDriverWorksForClusters() {
        node1.login();
        node2.login();
    }
}
