package it.com.atlassian.jira.plugins.ha.func;

import com.atlassian.jira.functest.framework.backdoor.WorkflowsControlExt.Workflow;
import com.atlassian.jira.plugins.ha.testapi.client.ClusterCleaner;
import com.atlassian.jira.plugins.ha.testapi.test.JiraCluster;
import com.atlassian.jira.plugins.ha.testapi.test.JiraHaWebTestRules;
import com.atlassian.jira.testkit.beans.WorkflowSchemeData;
import java.util.Arrays;
import java.util.List;
import javax.inject.Inject;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;

/**
 * Integration test of workflow designer.
 *
 * @since v6.3
 */
@RunWith(Parameterized.class)
public class WorkflowTest {
    private static final String JIRA_WORKFLOW_NAME = "jira";
    private static final String WORKFLOW_NAME = "Test Workflow";
    private static final String WORKFLOW_NAME_NEW = "Another Workflow";
    private static final String WORKFLOW_DESC = "Desc";
    private static final String WORKFLOW_SCHEME_NAME = "Test Workflow Scheme";
    private static final String PROJECT_KEY = "REP";

    // Run operations on different nodes in random order, repeat to make sure that we test different orders.
    private static final int TEST_REPEAT_COUNT = 4;

    @Rule
    public TestRule rules = JiraHaWebTestRules.forFuncTest(this);

    @Inject
    private JiraCluster.Node node;

    @Inject
    private JiraCluster cluster;

    @Inject
    private ClusterCleaner cleaner;

    // In order to run tests many times
    @Parameterized.Parameters
    public static List<Object[]> data() {
        return Arrays.asList(new Object[TEST_REPEAT_COUNT][0]);
    }

    @Test
    public void testPublishedWorkflowVisibleOnOtherNode() {
        try {
            // Create a workflow, add a step, and rename.
            node.admin().workflows().goTo().addWorkflow(WORKFLOW_NAME, WORKFLOW_DESC);
            node.admin().workflows().goTo().workflowSteps(WORKFLOW_NAME).add("Step 1", null);
            node.admin().workflows().goTo().edit(WORKFLOW_NAME).rename().setNameTo(WORKFLOW_NAME_NEW).submit();
            // Assert the details, and add a new step to it.
            Workflow workflow2 = node.backdoor().workflow().getWorkflowDetailed(WORKFLOW_NAME_NEW);
            assertThat("Workflow description must be correct.", workflow2.getDescription(), is(WORKFLOW_DESC));
            node.admin().workflows().goTo().workflowSteps(WORKFLOW_NAME_NEW).add("Step 2", null);
        } finally {
            cleaner.safelyDeleteWorkflow(WORKFLOW_NAME);
            cleaner.safelyDeleteWorkflow(WORKFLOW_NAME_NEW);
        }
    }

    @Test
    public void testDraftWorkflowVisibleOnOtherNode() {
        try {
            // Create a workflow.
            node.admin().workflows().goTo().addWorkflow(WORKFLOW_NAME, WORKFLOW_DESC);
            node.admin().workflows().goTo().workflowSteps(WORKFLOW_NAME).add("Step 1", null);
            node.admin().workflows().goTo().workflowSteps(WORKFLOW_NAME).add("Step 2", null);
            // Create a project and assign this workflow to it.
            node.backdoor().project().addProject("Test Project", PROJECT_KEY, cluster.adminUsername());
            WorkflowSchemeData workflowScheme = node.backdoor().workflowSchemes().createScheme(
                    new WorkflowSchemeData().setName(WORKFLOW_SCHEME_NAME).setDefaultWorkflow(WORKFLOW_NAME));
            node.backdoor().project().setWorkflowScheme(PROJECT_KEY, workflowScheme.getId());
            // Create a draft of workflow.
            node.admin().workflows().goTo().createDraft(WORKFLOW_NAME).add("Step 3", null);
            Workflow workflow = node.backdoor().workflow().getWorkflowDetailed(WORKFLOW_NAME);
            assertThat("Workflow should have a draft.", workflow.isHasDraft(), is(true));
            // Create a draft of workflow scheme.
            WorkflowSchemeData draftWorkflowScheme = node.backdoor().workflowSchemes().createDraft(workflowScheme);
            node.backdoor().workflowSchemes()
                    .updateDraftScheme(workflowScheme.getId(),
                            new WorkflowSchemeData().setDefaultWorkflow(JIRA_WORKFLOW_NAME));
            assertThat("Workflow scheme should have a draft.", draftWorkflowScheme.isDraft(), is(true));
            // Migrate project to new scheme and check the draft.
            WorkflowSchemeData classicWorkflowScheme = node.backdoor().workflowSchemes().getWorkflowSchemeByName(
                    "classic");
            node.backdoor().project().setWorkflowScheme(PROJECT_KEY, classicWorkflowScheme.getId());
            // Make sure the workflow scheme draft lives on its own now.
            draftWorkflowScheme =
                    node.backdoor().workflowSchemes().getWorkflowSchemeByName("Copy of " + WORKFLOW_SCHEME_NAME);
            assertThat("Workflow scheme draft has been made its own scheme?", draftWorkflowScheme.isDraft(), is(false));
            // Make sure the workflow draft lives on its own now.
            Workflow draftWorkflow = node.backdoor().workflow().getWorkflowDetailed("Copy of " + WORKFLOW_NAME);
            assertThat("Workflow draft has been made its own scheme?", draftWorkflow, is(notNullValue()));
        } finally {
            cleaner.safelyDeleteProject(PROJECT_KEY);
            cleaner.safelyDeleteWorkflowScheme("Copy of " + WORKFLOW_SCHEME_NAME);
            cleaner.safelyDeleteWorkflowScheme(WORKFLOW_SCHEME_NAME);
            cleaner.safelyDeleteWorkflow("Copy of " + WORKFLOW_NAME);
            cleaner.safelyDeleteWorkflow(WORKFLOW_NAME);
        }
    }
}

