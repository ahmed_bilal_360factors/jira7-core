package it.com.atlassian.jira.plugins.ha.func;

import com.atlassian.jira.functest.framework.backdoor.IndexingControl;
import com.atlassian.jira.plugins.ha.testapi.client.ClusterCleaner;
import com.atlassian.jira.plugins.ha.testapi.test.JiraCluster;
import com.atlassian.jira.plugins.ha.testapi.test.JiraHaWebTestRules;
import com.atlassian.jira.testkit.client.restclient.Issue;
import com.atlassian.jira.testkit.client.restclient.SearchRequest;
import com.atlassian.jira.testkit.client.restclient.SearchResult;
import java.util.List;
import javax.inject.Inject;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Concurrency tests for reindexing.
 * Test reindexing on one node while making modifications on another. Indexes on both nodes should be correct.
 */
public class ReindexConcurrentModificationsTest {

    /**
     * A project key
     */
    private static final String PROJECT_KEY = "HSP";

    /**
     * A modified project key
     */
    private static final String MODIFIED_PROJECT_KEY = "NEW";

    /**
     * The config property that sets the size of the batches used when reindexing
     */
    private static final String BATCH_SIZE_KEY = "jira.index.background.batch.size";

    @Rule
    public TestRule rules = JiraHaWebTestRules.forFuncTest(this);

    /**
     * The original index batch size before this test modified it
     */
    private String originalBatchSize;

    /**
     * The node we'll run a stop-the-world indexing on
     */
    @Inject
    private JiraCluster.Node reindexNode;

    /**
     * The node that will replicate the index
     */
    @Inject
    private JiraCluster.Node replicationNode;

    @Inject
    private JiraCluster cluster;

    @Inject
    private ClusterCleaner cleaner;

    @Before
    public void setup() {
        reindexNode.backdoor().project().addProject("Homosapiens", PROJECT_KEY, "admin");

        // Force batch size to 1 so that issues are retrieved from the db one at a time
        originalBatchSize = reindexNode.backdoor().applicationProperties().getString(BATCH_SIZE_KEY);
        reindexNode.backdoor().applicationProperties().setString(BATCH_SIZE_KEY, "1");
    }

    @After
    public void teardown() {
        cleaner.safelyDeleteProject(PROJECT_KEY);
        cleaner.safelyDeleteProject(MODIFIED_PROJECT_KEY);
        reindexNode.backdoor().applicationProperties().setString(BATCH_SIZE_KEY, originalBatchSize);
    }

    /**
     * Given a project key is modified on node 1
     * And node 1 is in the process of performing the project re-index
     * When node 2 concurrently runs its scheduled re-index
     * Then node 2 re-indexes the project with its new key
     */
    @Test
    public void projectKeyChangeIsReindexedOnBothNodes() throws Exception {
        // Create a project and issue
        final String issueKey = reindexNode.backdoor().issues().createIssue(PROJECT_KEY, "An Issue").key();
        final Long projectId = reindexNode.backdoor().project().getProjectId(PROJECT_KEY);
        cluster.waitForSync();

        // Raise a barrier to block indexing on both nodes so we can control the timing
        reindexNode.backdoor().barrier().raiseBarrierAndRun("backgroundReindex", () -> {
            replicationNode.backdoor().barrier().raiseBarrierAndRun("backgroundReindex", () -> {
                // Change the project key on node 1. This will initiate a project re-index on node 1,
                // but the re-index is blocked by node 1's barrier
                reindexNode.backdoor().project().editProjectKeyNoWaitForReindex(projectId, MODIFIED_PROJECT_KEY);
                reindexNode.backdoor().indexing().getProjectIndexingProgress(projectId).waitForIndexingStarted();

                // lower the barrier on node 2
            });
            // Ensure node 2 has completed re-indexing of the project via its scheduled re-index
            cluster.waitForSync();
            replicationNode.backdoor().indexing().getProjectIndexingProgress(projectId).waitForCompletion();

            // lower the barrier on node 1
        });
        // Wait for node 1 to finish re-indexing the project
        reindexNode.backdoor().indexing().getProjectIndexingProgress(projectId).waitForCompletion();

        final String modifiedIssueKey = issueKey.replace(PROJECT_KEY, MODIFIED_PROJECT_KEY);

        // Check node 1 indexes
        final List<Issue> issuesOnNode1 = searchByProjectKey(reindexNode, MODIFIED_PROJECT_KEY);
        assertThat("Modified project key is indexed on node 1", issuesOnNode1, hasSize(1));
        assertThat("Issue has modified key on node 1", issuesOnNode1.get(0).key, is(modifiedIssueKey));

        // Check node 2 indexes
        final List<Issue> issuesOnNode2 = searchByProjectKey(replicationNode, MODIFIED_PROJECT_KEY);
        assertThat("Modified project key is indexed on node 2", issuesOnNode2, hasSize(1));
        assertThat("Issue has modified key on node 2", issuesOnNode2.get(0).key, is(modifiedIssueKey));
    }

    /**
     * Given a full stop-the-world reindex is running on node 1,
     * When concurrent changes are made to some issues on node 2,
     * And the index is then replicated to node 2,
     * Then the concurrently modified issues are correctly reindexed.
     */
    @Test
    public void concurrentModificationBetweenBatches() throws Exception {

        // Create two issues
        final String issue1Summary = "Issue 1";
        final String issue1Key = reindexNode.backdoor().issues().createIssue(PROJECT_KEY, issue1Summary).key();
        final String issue2Summary = "Issue 2";
        final String issue2Key = reindexNode.backdoor().issues().createIssue(PROJECT_KEY, issue2Summary).key();

        final String issue1SummaryModified = issue1Summary + " modified";
        final String issue2SummaryModified = issue2Summary + " modified";

        // Pause the scheduled reindexing on node1.
        // As it runs on a schedule, it may interfere with the indexing order unpredictably
        withScheduledReindexingPaused(reindexNode, () -> {
            reindexNode.backdoor().barrier().raiseBarrierAndRun("backgroundReindex", () -> {
                IndexingControl.IndexingProgress indexingProgress = reindexNode.backdoor().indexing().startStopTheWorldReIndex();
                indexingProgress.waitForIndexingStarted();
                // At this point, indexing is blocked at the barrier.
                // issue2 has been read from the database already as it's in batch 1, but it has not been processed yet.

                // Modify both issues on node 2
                replicationNode.backdoor().issues().setSummary(issue2Key, issue2SummaryModified);
                replicationNode.backdoor().issues().setSummary(issue1Key, issue1SummaryModified);

                // Resume indexing
            });

            // Wait for the index to complete
            reindexNode.backdoor().indexing().getInBackgroundProgress().waitForCompletion();
        });

        waitForRecoveryToComplete();

        // Verify modified issues are correctly indexed on node 2
        assertThat("Updates for issue 1 are indexed on node 2", searchBySummary(replicationNode, issue1SummaryModified), hasSize(1));
        assertThat("Updates for issue 2 are indexed on node 2", searchBySummary(replicationNode, issue2SummaryModified), hasSize(1));

        // Verify modified issues are correctly indexed on node 1
        assertThat("Updates for issue 1 are indexed on node 1", searchBySummary(reindexNode, issue1SummaryModified), hasSize(1));
        assertThat("Updates for issue 2 are indexed on node 1", searchBySummary(reindexNode, issue2SummaryModified), hasSize(1));
    }

    /**
     * Search for issues on the node containing this summary text, within the project
     *
     * @return the list of issues found
     */
    private List<Issue> searchBySummary(JiraCluster.Node node, String summary) {
        SearchResult searchResult = node.backdoor().search().getSearch(new SearchRequest().jql("summary ~ \"" + summary + "\" AND project = " + PROJECT_KEY));
        return searchResult.issues;
    }

    /**
     * Search for all issues in the given project
     *
     * @return the list of issues found
     */
    private List<Issue> searchByProjectKey(JiraCluster.Node node, String projectKey) {

        SearchResult searchResult = node.backdoor().search().getSearch(new SearchRequest().jql("project = " + projectKey));
        return searchResult.issues;
    }

    /**
     * Run the provided task while the scheduled reindex is paused on this node
     *
     * @param node the node
     * @param task the task to run
     */
    private void withScheduledReindexingPaused(JiraCluster.Node node, Runnable task) {
        node.backdoor().indexing().pauseScheduledReindex();
        try {
            task.run();
        } finally {
            node.backdoor().indexing().startScheduledReindex();
        }
    }

    /**
     * Wait for the recovery process to complete
     */
    private void waitForRecoveryToComplete() {
        cluster.waitForSync();
        // The replication operations are reset in the db before the recovery process completes. At this point
        // we know the recovery process has started, but it may not have completed.
        // Make another arbitrary change that needs replicating, then waitForSync() again.
        reindexNode.backdoor().issues().createIssue(PROJECT_KEY, "dummyIssueToWaitForRecoveryToComplete").key();
        cluster.waitForSync();
    }
}
