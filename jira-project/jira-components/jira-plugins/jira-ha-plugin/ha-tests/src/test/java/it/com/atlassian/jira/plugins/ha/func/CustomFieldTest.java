package it.com.atlassian.jira.plugins.ha.func;

import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.plugins.ha.testapi.client.ClusterCleaner;
import com.atlassian.jira.plugins.ha.testapi.test.JiraCluster;
import com.atlassian.jira.plugins.ha.testapi.test.JiraHaWebTestRules;
import com.atlassian.jira.testkit.beans.CustomFieldResponse;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;
import javax.inject.Inject;
import net.sourceforge.jwebunit.WebTester;
import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.hamcrest.collection.IsIterableContainingInAnyOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import static com.atlassian.jira.functest.framework.FunctTestConstants.BUILT_IN_CUSTOM_FIELD_KEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TEXT_SEARCHER;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_TEXTFIELD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_FIELD_SCREEN_NAME;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

public class CustomFieldTest {
    private static final String PROJECT_KEY = "HSP";
    private static final CustomFieldComparator FIELD_COMPARATOR = new CustomFieldComparator();
    private static final Pattern PATTERN_ID = Pattern.compile("id=(\\d+)");

    @Rule
    public TestRule rules = JiraHaWebTestRules.forFuncTest(this);

    @Inject
    private JiraCluster cluster;

    @Inject
    private ClusterCleaner cleaner;

    /**
     * Used to create, update, modify fields.
     */
    @Inject
    private JiraCluster.Node dataNode;

    /**
     * The other node used to verify results, sometimes used as a second data node.
     */
    @Inject
    private JiraCluster.Node verifyNode;

    private static class CustomField {
        public String id;
        public String name;

        public static final AtomicInteger counter = new AtomicInteger((int) System.currentTimeMillis());

        public CustomField(String id, String name) {
            this.id = id;
            this.name = name;
        }

        public static CustomField generate() {
            int num = counter.incrementAndGet();
            return (new CustomField("ACF" + num, "Test ACF " + num));
        }
    }

    @Test
    public void testCustomFieldAdded() {
        String fieldId = null;
        try {
            // Add a project with a custom field
            long hspId = dataNode.backdoor().project().addProject("Homosapiens", PROJECT_KEY, "admin");
            fieldId = dataNode.backdoor().customFields().createCustomField("ACF1", "Test ACF 1",
                    BUILT_IN_CUSTOM_FIELD_KEY + ":" + CUSTOM_FIELD_TYPE_TEXTFIELD,
                    BUILT_IN_CUSTOM_FIELD_KEY + ":" + CUSTOM_FIELD_TEXT_SEARCHER);

            dataNode.backdoor().screens().addFieldToScreen(DEFAULT_FIELD_SCREEN_NAME, "ACF1");
            dataNode.backdoor().fieldConfiguration().associateCustomFieldWithProject(fieldId, "Homosapiens");

            // Ensure list of custom fields is the same on each node
            assertCustomFieldsAreEqual();

            cluster.waitForSync();

            // Create an issue and check that the custom field appears on the expected screens
            WebTester tester = verifyNode.tester();
            tester.gotoPage("secure/CreateIssue!default.jspa");
            selectProjectAndIssueTypeOptions(tester, "Homosapiens", hspId);
            tester.submit();

            tester.setFormElement("summary", "Something's wrong");

            tester.assertFormElementPresentWithLabel("Test ACF 1");
            tester.setFormElement(fieldId, "Test Value");
            tester.submit();

            tester.assertTextInElement(fieldId + "-val", "Test Value"); // Verify that the field is visible and contains the expected value
            String path = getPath(tester);
            tester.clickLink("action_id_5"); // Go to Resolve issue screen
            tester.assertFormElementNotPresentWithLabel("Test ACF 1"); // Verify the field is not in Resolve Screen as not added to screen

            // Verify that the value of the field is visible on the original node
            tester = dataNode.tester();
            tester.gotoPage(path);
            tester.assertTextInElement(fieldId + "-val", "Test Value");
        } finally {
            cluster.waitForSync();
            cleaner.safelyDeleteProject(PROJECT_KEY);
            cleaner.safelyDeleteCustomField(fieldId);
            cluster.waitForSync();
        }
    }

    @Test
    public void testSameCustomFieldAddedManyTimes() {
        //Repeat this many times to try and hit weird caching condition
        for (int i = 0; i < 20; i++) {
            CustomField cf = CustomField.generate();
            System.err.println("Using CF: " + cf.name);

            //GProject p = GProject.generate();
            //System.err.println("Using P: " + p.key + ": " + p.name);

            System.err.println("Run #" + (i + 1));
            String fieldId = null;
            try {
                // Add a project with a custom field
                long hspId = dataNode.backdoor().project().addProject("Homosapiens", PROJECT_KEY, "admin");
                fieldId = dataNode.backdoor().customFields().createCustomField(cf.id, cf.name,
                        BUILT_IN_CUSTOM_FIELD_KEY + ":" + CUSTOM_FIELD_TYPE_TEXTFIELD,
                        BUILT_IN_CUSTOM_FIELD_KEY + ":" + CUSTOM_FIELD_TEXT_SEARCHER);

                dataNode.backdoor().screens().addFieldToScreen(DEFAULT_FIELD_SCREEN_NAME, cf.id);
                dataNode.backdoor().fieldConfiguration().associateCustomFieldWithProject(fieldId, "Homosapiens");

                // Ensure list of custom fields is the same on each node
                assertCustomFieldsAreEqual();

                cluster.waitForSync();

                // Create an issue and check that the custom field appears on the expected screens
                WebTester tester = verifyNode.tester();
                tester.gotoPage("secure/CreateIssue!default.jspa");
                selectProjectAndIssueTypeOptions(tester, "Homosapiens", hspId);
                tester.submit();

                tester.setFormElement("summary", "Something's wrong");

                tester.assertFormElementPresentWithLabel(cf.name);
                tester.setFormElement(fieldId, "Test Value");
                tester.submit();

                tester.assertTextInElement(fieldId + "-val", "Test Value"); // Verify that the field is visible and contains the expected value
                String path = getPath(tester);
                tester.clickLink("action_id_5"); // Go to Resolve issue screen
                tester.assertFormElementNotPresentWithLabel(cf.name); // Verify the field is not in Resolve Screen as not added to screen

                // Verify that the value of the field is visible on the original node
                tester = dataNode.tester();
                tester.gotoPage(path);
                tester.assertTextInElement(fieldId + "-val", "Test Value");
            } finally {
                cluster.waitForSync();
                cleaner.safelyDeleteProject(PROJECT_KEY);
                cleaner.safelyDeleteCustomField(fieldId);
                cluster.waitForSync();
            }
        }
    }

    private void selectProjectAndIssueTypeOptions(WebTester tester, String projectName, long hspId) {
        try {
            tester.selectOption("pid", projectName);
        } catch (RuntimeException e) {
            //Fallback to trying to set value for later JIRA versions
            tester.setFormElement("pid", String.valueOf(hspId));
        }

        try {
            tester.selectOption("issuetype", "Bug");
        } catch (RuntimeException e) {
            //Fallback for trying to set issuetype for later JIRA versions
            tester.setFormElement("issuetype", "1"); //bug
        }
    }

    @Test
    public void testCustomFieldDeleted() {
        String fieldId = null;
        try {
            // Add a project with a custom field
            long hspId = dataNode.backdoor().project().addProject("Homosapiens", PROJECT_KEY, "admin");
            fieldId = dataNode.backdoor().customFields().createCustomField("CF1", "Test CF 1",
                    BUILT_IN_CUSTOM_FIELD_KEY + ":" + CUSTOM_FIELD_TYPE_TEXTFIELD,
                    BUILT_IN_CUSTOM_FIELD_KEY + ":" + CUSTOM_FIELD_TEXT_SEARCHER);

            dataNode.backdoor().screens().addFieldToScreen(DEFAULT_FIELD_SCREEN_NAME, "CF1");
            dataNode.backdoor().fieldConfiguration().associateCustomFieldWithProject(fieldId, "Homosapiens");

            cluster.waitForSync();

            // Commence creating an issue
            WebTester tester = verifyNode.tester();
            tester.gotoPage("secure/CreateIssue!default.jspa?pid=" + hspId);
            selectProjectAndIssueTypeOptions(tester, "Homosapiens", hspId);
            tester.submit();

            // On the other node, delete the custom field
            dataNode.backdoor().customFields().deleteCustomField(fieldId);

            assertCustomFieldsAreEqual();

            // Finish creating the issue
            tester.setFormElement("summary", "Something's wrong");
            tester.assertFormElementPresentWithLabel("Test CF 1");
            tester.setFormElement(fieldId, "Test Value");
            tester.submit();

            // Verify the custom field is not present as it has been deleted
            tester.assertElementNotPresent(fieldId + "-val");

            // Create another issue and verify the field no longer appears on the create issue screen
            tester.gotoPage("secure/CreateIssue!default.jspa?pid=" + hspId);
            selectProjectAndIssueTypeOptions(tester, "Homosapiens", hspId);
            tester.submit();

            tester.setFormElement("summary", "Something's wrong again");
            tester.assertFormElementNotPresentWithLabel("Test CF 1");
            tester.submit();

            tester.assertElementNotPresent(fieldId + "-val");
        } finally {
            cluster.waitForSync();
            cleaner.safelyDeleteProject(PROJECT_KEY);
            cleaner.safelyDeleteCustomField(fieldId);
            cluster.waitForSync();
        }
    }

    @Test
    public void testCustomFieldUpdated() {
        String fieldId = null;
        try {
            // Add a project with a custom field
            long hspId = dataNode.backdoor().project().addProject("Homosapiens", PROJECT_KEY, "admin");
            fieldId = dataNode.backdoor().customFields().createCustomField("CF1", "Test CF 1",
                    BUILT_IN_CUSTOM_FIELD_KEY + ":" + CUSTOM_FIELD_TYPE_TEXTFIELD,
                    BUILT_IN_CUSTOM_FIELD_KEY + ":" + CUSTOM_FIELD_TEXT_SEARCHER);

            dataNode.backdoor().screens().addFieldToScreen(DEFAULT_FIELD_SCREEN_NAME, "CF1");
            dataNode.backdoor().fieldConfiguration().associateCustomFieldWithProject(fieldId, "Homosapiens");

            cluster.waitForSync();

            // Commence creating an issue
            WebTester tester = verifyNode.tester();
            tester.gotoPage("secure/CreateIssue!default.jspa?pid=" + hspId);
            selectProjectAndIssueTypeOptions(tester, "Homosapiens", hspId);
            tester.submit();

            // On the other node, update the custom field
            dataNode.backdoor().customFields().updateCustomField(fieldId, "CF2", "Test CF 2",
                    BUILT_IN_CUSTOM_FIELD_KEY + ":" + CUSTOM_FIELD_TEXT_SEARCHER);

            assertCustomFieldsAreEqual();

            // Finish creating the issue
            tester.setFormElement("summary", "Something's wrong");
            tester.assertFormElementPresentWithLabel("Test CF 1");
            tester.setFormElement(fieldId, "Test Value");
            tester.submit();

            // Verify the custom field has the updated values
            tester.assertTextInElement(fieldId + "-val", "Test Value");

            // Create another issue and verify the updated field appears on the create issue screen
            tester.gotoPage("secure/CreateIssue!default.jspa?pid=" + hspId);
            selectProjectAndIssueTypeOptions(tester, "Homosapiens", hspId);
            tester.submit();

            tester.setFormElement("summary", "Something's wrong again");
            tester.assertFormElementPresentWithLabel("Test CF 2");
            tester.setFormElement(fieldId, "Test Value 2");
            tester.submit();

            tester.assertTextInElement(fieldId + "-val", "Test Value 2");
        } finally {
            cluster.waitForSync();
            cleaner.safelyDeleteProject(PROJECT_KEY);
            cleaner.safelyDeleteCustomField(fieldId);
            cluster.waitForSync();
        }
    }

    @Test
    public void testCustomFieldAddedToScreen() {
        String fieldId = null;
        try {
            // Add a project with a custom field and an issue
            long hspId = dataNode.backdoor().project().addProject("Homosapiens", PROJECT_KEY, "admin");
            fieldId = dataNode.backdoor().customFields().createCustomField("CF1", "Test CF 1",
                    BUILT_IN_CUSTOM_FIELD_KEY + ":" + CUSTOM_FIELD_TYPE_TEXTFIELD,
                    BUILT_IN_CUSTOM_FIELD_KEY + ":" + CUSTOM_FIELD_TEXT_SEARCHER);

            dataNode.backdoor().screens().addFieldToScreen(DEFAULT_FIELD_SCREEN_NAME, "CF1");
            dataNode.backdoor().fieldConfiguration().associateCustomFieldWithProject(fieldId, "Homosapiens");

            cluster.waitForSync();

            WebTester tester = verifyNode.tester();
            tester.gotoPage("secure/CreateIssue!default.jspa?pid=" + hspId);
            selectProjectAndIssueTypeOptions(tester, "Homosapiens", hspId);
            tester.submit();

            tester.setFormElement("summary", "Something's wrong");
            tester.assertFormElementPresentWithLabel("Test CF 1");
            tester.setFormElement(fieldId, "Test Value");
            tester.submit();

            // Verify the custom field does not appear in the Resolve screen
            tester.assertTextInElement(fieldId + "-val", "Test Value");
            tester.clickLink("action_id_5"); // Resolve issue screen
            tester.assertFormElementNotPresentWithLabel("Test CF 1"); // Not in Resolve Screen
            tester.clickLink("issue-workflow-transition-cancel");

            // Add the field to the Resolve screen and verify it now shows
            dataNode.backdoor().screens().addFieldToScreen(FunctTestConstants.RESOLVE_FIELD_SCREEN_NAME, "CF1");

            tester.clickLink("action_id_5"); // Resolve issue screen
            tester.assertFormElementPresentWithLabel("Test CF 1"); // Now in Resolve Screen
        } finally {
            cluster.waitForSync();
            cleaner.safelyDeleteProject(PROJECT_KEY);
            cleaner.safelyDeleteCustomField(fieldId);
            cluster.waitForSync();
        }
    }

    @Test
    public void testCustomFieldRemovedFromScreen() {
        String fieldId = null;
        try {
            // Add a project with a custom field
            long hspId = dataNode.backdoor().project().addProject("Homosapiens", PROJECT_KEY, "admin");
            fieldId = dataNode.backdoor().customFields().createCustomField("CF1", "Test CF 1",
                    BUILT_IN_CUSTOM_FIELD_KEY + ":" + CUSTOM_FIELD_TYPE_TEXTFIELD,
                    BUILT_IN_CUSTOM_FIELD_KEY + ":" + CUSTOM_FIELD_TEXT_SEARCHER);

            dataNode.backdoor().screens().addFieldToScreen(DEFAULT_FIELD_SCREEN_NAME, "CF1");
            dataNode.backdoor().fieldConfiguration().associateCustomFieldWithProject(fieldId, "Homosapiens");

            cluster.waitForSync();

            // Commence creating an issue
            WebTester tester = verifyNode.tester();
            tester.gotoPage("secure/CreateIssue!default.jspa?pid=" + hspId);
            selectProjectAndIssueTypeOptions(tester, "Homosapiens", hspId);
            tester.submit();

            // Remove custom field from screen
            dataNode.backdoor().screens().removeFieldFromScreen(DEFAULT_FIELD_SCREEN_NAME, "CF1");

            // Finish creating the issue
            tester.setFormElement("summary", "Something's wrong");
            tester.assertFormElementPresentWithLabel("Test CF 1");
            tester.setFormElement(fieldId, "Test Value");
            tester.submit();

            // Verify the custom field is not present as it has been deleted
            tester.assertElementNotPresent(fieldId + "-val");

            // Create another issue and verify the field no longer appears on the create issue screen
            tester.gotoPage("secure/CreateIssue!default.jspa?pid=" + hspId);
            selectProjectAndIssueTypeOptions(tester, "Homosapiens", hspId);
            tester.submit();

            tester.setFormElement("summary", "Something's wrong again");
            tester.assertFormElementNotPresentWithLabel("Test CF 1");
            tester.submit();

            tester.assertElementNotPresent(fieldId + "-val");
        } finally {
            cluster.waitForSync();
            cleaner.safelyDeleteProject(PROJECT_KEY);
            cleaner.safelyDeleteCustomField(fieldId);
            cluster.waitForSync();
        }
    }

    @Test
    public void testIssueOperationMappings() {
        String fieldId = null;
        String fieldScreenId = null;
        String fieldScreenSchemeId = null;
        String issueTypeScreenSchemeId = null;
        try {
            // Add a screen, field screen, and issue type screen scheme
            WebTester tester = dataNode.tester();
            tester.gotoPage("secure/admin/AddNewFieldScreen.jspa");
            tester.setFormElement("fieldScreenName", "Custom Field Screen");
            tester.setFormElement("fieldScreenDescription", "A screen to put a custom field in");
            tester.submit();
            fieldScreenId = getId(tester);

            tester.gotoPage("secure/admin/AddNewFieldScreenScheme.jspa");
            tester.setFormElement("fieldScreenSchemeName", "Custom Field Screen Scheme");
            tester.setFormElement("fieldScreenSchemeDescription", "A screen scheme to put a custom screen in");
            tester.selectOption("fieldScreenId", "Custom Field Screen");
            tester.submit();
            fieldScreenSchemeId = getId(tester);

            tester.gotoPage("secure/admin/AddIssueTypeScreenScheme!default.jspa");
            tester.setFormElement("schemeName", "Custom Issue Type Screen Scheme");
            tester.setFormElement("schemeDescription", "An issue type screen scheme to put a custom screen in");
            tester.selectOption("fieldScreenSchemeId", "Custom Field Screen Scheme");
            tester.submit();
            issueTypeScreenSchemeId = getId(tester);

            // Create a project that uses issue type screen scheme
            long hspId = dataNode.backdoor().project().addProject("Homosapiens", PROJECT_KEY, "admin");
            dataNode.backdoor().project().setIssueTypeScreenScheme(hspId, Long.parseLong(issueTypeScreenSchemeId));
            fieldId = dataNode.backdoor().customFields().createCustomField("CF1", "Test CF 1",
                    BUILT_IN_CUSTOM_FIELD_KEY + ":" + CUSTOM_FIELD_TYPE_TEXTFIELD,
                    BUILT_IN_CUSTOM_FIELD_KEY + ":" + CUSTOM_FIELD_TEXT_SEARCHER);
            dataNode.backdoor().screens().addFieldToScreen("Custom Field Screen", "CF1");

            cluster.waitForSync();

            // Create issue on other node and verify field exists
            tester = verifyNode.tester();
            tester.gotoPage("secure/CreateIssue!default.jspa?pid=" + hspId);
            selectProjectAndIssueTypeOptions(tester, "Homosapiens", hspId);
            tester.submit();

            tester.assertFormElementPresentWithLabel("Test CF 1");
            tester.setFormElement(fieldId, "Test Value");
            tester.submit();
        } finally {
            cluster.waitForSync();
            cleaner.safelyDeleteProject(PROJECT_KEY);
            cleaner.safelyDeleteCustomField(fieldId);
            safelyDeleteIssueTypeScreenScheme(issueTypeScreenSchemeId);
            safelyDeleteScreenScheme(fieldScreenSchemeId);
            safelyDeleteScreen(fieldScreenId);
            cluster.waitForSync();
        }
    }

    public void safelyDeleteScreen(String id) {
        safelyDeleteFromPage("secure/admin/ViewDeleteFieldScreen.jspa?id=", id);
    }

    public void safelyDeleteScreenScheme(String id) {
        safelyDeleteFromPage("secure/admin/ViewDeleteFieldScreenScheme.jspa?id=", id);
    }

    private void safelyDeleteIssueTypeScreenScheme(String id) {
        safelyDeleteFromPage("secure/admin/ViewDeleteIssueTypeScreenScheme.jspa?id=", id);
    }

    private void safelyDeleteFromPage(String page, String id) {
        if (id != null) {
            try {
                WebTester tester = dataNode.tester();
                tester.gotoPage(page + id);
                tester.submit("Delete");
            } catch (Exception e) {
            }
        }
    }

    private String getPath(WebTester tester) {
        String path = tester.getTestContext().getWebClient().getCurrentPage().getURL().getPath();
        return path.substring(path.indexOf("/", 1) + 1);
    }

    private String getId(WebTester tester) {
        String path = tester.getTestContext().getWebClient().getCurrentPage().getURL().getQuery();
        assertThat(path, notNullValue());
        java.util.regex.Matcher matcher = PATTERN_ID.matcher(path);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }

    private void assertCustomFieldsAreEqual() {
        // Get the backdoor using a selection policy of 'DIFFERENT'
        // In a two node cluster calling this method will leave the existing selection intact
        Collection<CustomFieldResponse> node1CustomFields = dataNode.backdoor().customFields().getCustomFields();
        Collection<CustomFieldResponse> node2CustomFields = verifyNode.backdoor().customFields().getCustomFields();

        assertThat(node2CustomFields, containsInAnyOrder(FIELD_COMPARATOR, node1CustomFields));
    }

    @Factory
    public static <T> Matcher<Iterable<? extends T>> containsInAnyOrder(Comparator<? super T> comparator, Iterable<T> items) {
        Collection<Matcher<? super T>> matchers = new ArrayList<Matcher<? super T>>();
        for (T item : items) {
            matchers.add(new ComparatorMatcher<T>(comparator, item));
        }
        return new IsIterableContainingInAnyOrder<T>(matchers);
    }

    private static class ComparatorMatcher<T> extends TypeSafeDiagnosingMatcher<T> {

        private final Comparator<? super T> comparator;
        private final T expected;

        private ComparatorMatcher(Comparator<? super T> comparator, T expected) {
            this.comparator = comparator;
            this.expected = expected;
        }

        @Override
        protected boolean matchesSafely(T actual, Description mismatchDescription) {
            return comparator.compare(actual, expected) == 0;
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("compares equal to ")
                    .appendValue(expected).appendText(" with comparator ")
                    .appendValue(comparator);
        }
    }

    private static class CustomFieldComparator implements Comparator<CustomFieldResponse> {
        @Override
        public int compare(CustomFieldResponse actual, CustomFieldResponse expected) {
            int result = expected.id.compareTo(actual.id);
            if (result == 0) {
                result = expected.name.compareTo(actual.name);
            }
            return result;
        }

        @Override
        public String toString() {
            return "CustomField id & name";
        }
    }
}
