package it.com.atlassian.jira.plugins.ha.func;

import com.atlassian.jira.plugins.ha.testapi.client.ClusterCleaner;
import com.atlassian.jira.plugins.ha.testapi.test.JiraCluster;
import com.atlassian.jira.plugins.ha.testapi.test.JiraHaWebTestRules;
import com.atlassian.jira.testkit.client.restclient.SearchRequest;
import com.atlassian.jira.testkit.client.restclient.SearchResult;
import javax.inject.Inject;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * This is a small test to test the JIRA HA testing framework is up
 *
 * @since v6.0
 */
public class SmokeTest {
    private static final String PROJECT_KEY = "HSP";

    @Rule
    public TestRule rules = JiraHaWebTestRules.forFuncTest(this);

    @Inject
    private JiraCluster.Node node1;

    @Inject
    private JiraCluster.Node node2;

    @Inject
    private JiraCluster cluster;

    @Inject
    private ClusterCleaner cleaner;

    @Test
    public void testIndexing() throws InterruptedException {
        try {
            node1.login();
            node1.backdoor().project().addProject("Homosapiens", PROJECT_KEY, "admin");
            node1.backdoor().issues().createIssue(PROJECT_KEY, "New HA Issue");
            cluster.waitForSync();
            node2.login();
            SearchResult response = node2.backdoor().search().getSearch(new SearchRequest().jql("summary ~ HA"));
            assertThat("Should get 1 issue", response.issues.size(), is(1));
        } finally {
            // Cleanup our stuff.
            cleaner.safelyDeleteProject(PROJECT_KEY);
        }
    }
}
