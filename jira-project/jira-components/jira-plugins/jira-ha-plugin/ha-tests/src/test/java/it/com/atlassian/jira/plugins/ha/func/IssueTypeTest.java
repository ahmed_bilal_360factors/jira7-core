package it.com.atlassian.jira.plugins.ha.func;

import com.atlassian.jira.plugins.ha.testapi.test.JiraCluster;
import com.atlassian.jira.plugins.ha.testapi.test.JiraHaWebTestRules;
import com.atlassian.jira.testkit.client.IssueTypeControl;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import java.util.List;
import javax.inject.Inject;
import net.sourceforge.jwebunit.WebTester;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

/**
 * Integration test of issue types in a cluster.
 *
 * @since 6.3
 */
public class IssueTypeTest {
    private static final String DEFAULT_ISSUE_TYPE = "Item";
    private IssueTypeControl issueTypesOnNode1;
    private IssueTypeControl issueTypesOnNode2;

    @Rule
    public TestRule rules = JiraHaWebTestRules.forFuncTest(this);

    @Inject
    private JiraCluster.Node node1;

    @Inject
    private JiraCluster.Node node2;

    @Before
    public void setUp() {
        issueTypesOnNode1 = node1.backdoor().issueType();
        issueTypesOnNode2 = node2.backdoor().issueType();
    }

    @Test
    public void issueTypeAddedOnOneNodeShouldBeVisibleToOthers() {
        IssueTypeControl.IssueType issueType = null;
        try {
            issueType = issueTypesOnNode1.createIssueType("Item");
            List<IssueTypeControl.IssueType> issueTypes = issueTypesOnNode2.getIssueTypes();
            assertThat(issueTypes, hasItem(new IssueTypeMatcher(issueType)));
        } finally {
            safelyDeleteIssueType(issueType);
        }
    }

    @Test
    public void sameIssueTypeCannotBeAddedOnBothNodes() {
        IssueTypeControl.IssueType issueType = null;
        try {
            issueType = issueTypesOnNode1.createIssueType("Item");
            try {
                issueTypesOnNode2.createIssueType("Item");
            } catch (UniformInterfaceException e) {
                assertThat(e.getResponse().getClientResponseStatus(), is(ClientResponse.Status.BAD_REQUEST));
            }
        } finally {
            safelyDeleteIssueType(issueType);
        }
    }

    @Test
    public void issueTypeDeletedOnOneNodeShouldNotBeVisibleToOthers() {
        IssueTypeControl.IssueType issueType = null;
        try {
            issueType = issueTypesOnNode1.createIssueType("Item");
            issueTypesOnNode1.deleteIssueType(Long.valueOf(issueType.getId()));
            List<IssueTypeControl.IssueType> issueTypes = issueTypesOnNode2.getIssueTypes();
            assertThat(issueTypes, not(hasItem(new IssueTypeMatcher(issueType))));
            issueType = null;
        } finally {
            safelyDeleteIssueType(issueType);
        }
    }

    @Test
    public void issueTypeChangedOnOneNodeShouldBeVisibleToOthers() {
        String newType = "Object";
        WebTester tester = node2.tester();
        IssueTypeControl.IssueType issueType = null;
        try {
            issueType = issueTypesOnNode1.createIssueType(DEFAULT_ISSUE_TYPE);
            issueType.setName(newType);

            tester.gotoPage("secure/admin/EditIssueType!default.jspa?id=" + issueType.getId());
            tester.setFormElement("name", newType);
            tester.submit();

            List<IssueTypeControl.IssueType> issueTypes = issueTypesOnNode1.getIssueTypes();
            assertThat(issueTypes, hasItem(new IssueTypeMatcher(issueType)));
        } finally {
            safelyDeleteIssueType(issueType);
        }
    }

    private void safelyDeleteIssueType(IssueTypeControl.IssueType issueType) {
        if (issueType != null) {
            try {
                issueTypesOnNode1.deleteIssueType(Long.valueOf(issueType.getId()));
            } catch (Exception e) {
            }
        }
    }

    private static class IssueTypeMatcher extends BaseMatcher<Object> {
        private final IssueTypeControl.IssueType expected;

        private IssueTypeMatcher(IssueTypeControl.IssueType expected) {
            this.expected = expected;
        }

        @Override
        public boolean matches(Object o) {
            return o instanceof IssueTypeControl.IssueType &&
                    expected.getName().equals(((IssueTypeControl.IssueType) o).getName()) &&
                    expected.getId().equals(((IssueTypeControl.IssueType) o).getId());
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("an issue type of " + expected.getName());
        }
    }


}
