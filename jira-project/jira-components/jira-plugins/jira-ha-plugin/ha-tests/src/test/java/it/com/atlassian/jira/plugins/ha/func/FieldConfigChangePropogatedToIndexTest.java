package it.com.atlassian.jira.plugins.ha.func;

import com.atlassian.jira.plugins.ha.testapi.client.ClusterCleaner;
import com.atlassian.jira.plugins.ha.testapi.test.JiraCluster;
import com.atlassian.jira.plugins.ha.testapi.test.JiraHaWebTestRules;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.rest.api.issue.IssueFields;
import com.atlassian.jira.testkit.client.restclient.SearchRequest;
import com.atlassian.jira.testkit.client.restclient.SearchResult;
import javax.inject.Inject;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import static com.atlassian.jira.functest.framework.FunctTestConstants.BUILT_IN_CUSTOM_FIELD_KEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TEXT_SEARCHER;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_TEXTFIELD;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * This is a small test to test the JIRA HA testing framework is up.
 *
 * @since v6.0
 */
public class FieldConfigChangePropogatedToIndexTest {
    private static final String CUSTOMFIELD_ = "customfield_";
    private static final String PROJECT_KEY = "HSP";
    private static final String PROJECT_KEY2 = "MKY";

    @Rule
    public TestRule rules = JiraHaWebTestRules.forFuncTest(this);

    @Inject
    private JiraCluster cluster;

    @Inject
    private ClusterCleaner cleaner;

    /**
     * Used to create, update, modify fields.
     */
    @Inject
    private JiraCluster.Node dataNode;

    /**
     * The other node used to verify results, sometimes used as a second data node.
     */
    @Inject
    private JiraCluster.Node verifyNode;

    @Test
    public void testCustomFieldConfig() throws InterruptedException {
        String fieldId = null;
        try {
            // Add a field with a custom field
            dataNode.backdoor().project().addProject("Homosapiens", PROJECT_KEY, "admin");
            dataNode.backdoor().project().addProject("Monkey", PROJECT_KEY2, "admin");
            fieldId = dataNode.backdoor().customFields().createCustomField("CF1", "Test CF 1", BUILT_IN_CUSTOM_FIELD_KEY +
                    ":" + CUSTOM_FIELD_TYPE_TEXTFIELD, BUILT_IN_CUSTOM_FIELD_KEY + ":" + CUSTOM_FIELD_TEXT_SEARCHER);
            Long customFieldId = Long.valueOf(fieldId.substring(CUSTOMFIELD_.length()));
            dataNode.backdoor().screens().addFieldToScreen("Default Screen", "CF1");
            dataNode.backdoor().fieldConfiguration().associateCustomFieldWithProject(fieldId, "Homosapiens");

            IssueCreateResponse issueResp = dataNode.backdoor().issues().createIssue(PROJECT_KEY, "New HA Issue");
            dataNode.backdoor().issues().setIssueFields(issueResp.key, new IssueFields().customField(customFieldId, "Walter was here"));
            cluster.waitForSync();
            // This should be available on the node2
            SearchResult response = verifyNode.backdoor().search().getSearch(new SearchRequest().jql("CF1 ~ Walter"));
            assertThat("Should get 1 issue", response.issues.size(), is(1));

            // Now if we hide the field and reindex the issue, it should not be available on the far side
            dataNode.backdoor().fieldConfiguration().associateCustomFieldWithProject(fieldId, "Monkey");
            IssueCreateResponse issue2Resp = dataNode.backdoor().issues().createIssue(PROJECT_KEY2, "New MKY Issue");
            dataNode.backdoor().issues().setIssueFields(issue2Resp.key, new IssueFields().customField(customFieldId, "Monkey was here"));
            dataNode.backdoor().issues().setIssueFields(issueResp.key, new IssueFields().description("Updated"));
            cluster.waitForSync();

            // We should not be able to find the issue by this custom field on the node2
            response = verifyNode.backdoor().search().getSearch(new SearchRequest().jql("CF1 ~ Walter"));
            assertThat("Should not find the issue", response.issues.size(), is(0));
            // But can find by summary
            response = verifyNode.backdoor().search().getSearch(new SearchRequest().jql("summary ~ HA"));
            assertThat("Should get 1 issue", response.issues.size(), is(1));
        } finally {
            // Clean up our stuff
            cleaner.safelyDeleteProject(PROJECT_KEY);
            cleaner.safelyDeleteProject(PROJECT_KEY2);
            cleaner.safelyDeleteCustomField(fieldId);
        }
    }

}
