package it.com.atlassian.jira.plugins.ha.func;

import com.atlassian.jira.plugins.ha.testapi.client.ClusterCleaner;
import com.atlassian.jira.plugins.ha.testapi.client.OrphanIssueControl;
import com.atlassian.jira.plugins.ha.testapi.test.JiraCluster;
import com.atlassian.jira.plugins.ha.testapi.test.JiraHaWebTestRules;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import javax.inject.Inject;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Test resilience of the indexer.  Need to make sure that error conditions do not cripple a node.
 */
public class IndexerResilienceTest {
    private static final Logger log = LoggerFactory.getLogger(IndexerResilienceTest.class);

    private static final String PROJECT_KEY_1 = "GLA";
    private static final long PROJECT_ID_THAT_DOES_NOT_EXIST = 97597597L;

    @Rule
    public TestRule rules = JiraHaWebTestRules.forFuncTest(this);

    @Inject
    private JiraCluster cluster;

    @Inject
    private JiraCluster.Node node;

    @Inject
    private ClusterCleaner cleaner;

    /**
     * Intentionally create an issue that has a missing project, wait for the nodes to attempt to index it, then
     * see if the indexer recovers.  It is expected to see an error in the logs of the nodes but it should still
     * recover and continue to function normally.
     * <p/>
     * <p/>
     * What used to happen was that the indexer would get stuck continually looping attempting to index the same issue.
     * The fix should allow the node to work its way past this, even if that issue didn't index properly.
     */
    @Test
    public void testCreateOrphanIssue() {
        Long issueId = null;
        try {
            node.backdoor().project().addProject("Test new project Galah", PROJECT_KEY_1, cluster.adminUsername());
            final IssueCreateResponse issueCreateResponse = node.backdoor().issues().createIssue(PROJECT_KEY_1, "This is my new issue");
            issueId = Long.valueOf(issueCreateResponse.id());

            final OrphanIssueControl orphanIssueControl = new OrphanIssueControl(node.environment());
            orphanIssueControl.createOrphanIssue(issueId, PROJECT_ID_THAT_DOES_NOT_EXIST);

            cluster.waitForSync();

            //If we get here everything is fine, but if something goes wrong this call will time out and fail
        } finally {
            safelyDeleteIssue(issueId);
            cleaner.safelyDeleteProject(PROJECT_KEY_1);
        }
    }

    private void safelyDeleteIssue(Long issueId) {
        if (issueId != null) {
            try {
                OrphanIssueControl issueControl = new OrphanIssueControl(node.environment());
                issueControl.deleteOrphanIssue(issueId);
            } catch (final Exception e) {
                log.debug("Couldn't delete issue.", e);
            }
        }
    }
}
