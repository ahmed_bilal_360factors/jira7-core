package it.com.atlassian.jira.plugins.ha.func;

import com.atlassian.jira.plugins.ha.testapi.test.JiraCluster;
import com.atlassian.jira.plugins.ha.testapi.test.JiraHaWebTestRules;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.fail;

/**
 * This is a small test to test the JIRA HA testing framework is up
 *
 * @since v6.0
 */
public class PluginStateChangesTest {
    private static final Logger log = LoggerFactory.getLogger(PluginStateChangesTest.class);

    @Rule
    public TestRule rules = JiraHaWebTestRules.forFuncTest(this);

    @Inject
    private JiraCluster cluster;

    @Inject
    private JiraCluster.Node node1;

    @Inject
    private JiraCluster.Node node2;

    @Test
    public void testPluginDisable() throws InterruptedException {
        try {
            node1.backdoor().plugins().enablePlugin("com.atlassian.jira.jira-my-home-plugin");
            cluster.waitForSync();
            waitForPluginState("ENABLED", "com.atlassian.jira.jira-my-home-plugin", node2);

            node1.backdoor().plugins().disablePlugin("com.atlassian.jira.jira-my-home-plugin");
            cluster.waitForSync();
            waitForPluginState("DISABLED", "com.atlassian.jira.jira-my-home-plugin", node2);

            node2.backdoor().plugins().disablePlugin("com.atlassian.jira.jira-my-home-plugin");
            cluster.waitForSync();
            // Should not happen on the node2
            waitForPluginState("DISABLED", "com.atlassian.jira.jira-my-home-plugin", node2);
            cluster.waitForSync();
        } finally {
            safelyEnablePlugin("com.atlassian.jira.jira-my-home-plugin");
        }
    }

    /**
     * Enable a plugin, but don't fail if the operation fails.  Use this for test cleanup operations.
     *
     * @param pluginKey the key of the plugin to enable.
     */
    private void safelyEnablePlugin(String pluginKey) {
        cluster.waitForSync();
        node1.backdoor().plugins().enablePlugin(pluginKey);
        try {
            waitForPluginState("ENABLED", pluginKey, node1);
            waitForPluginState("ENABLED", pluginKey, node2);
        } catch (AssertionError e) {
            //We're in a finally block so don't fail the test here - we're just resetting state
            log.warn("Error waiting for plugin state to be reset to enabled: " + e, e);
        }
        cluster.waitForSync();
    }

    private void waitForPluginState(String expectedState, String pluginKey, JiraCluster.Node node, long time, TimeUnit timeUnit) {
        try {
            String state;
            long totalTimeMillis = 0L;
            long maxTimeMillis = timeUnit.toMillis(time);
            do {
                state = node.backdoor().plugins().getPluginState(pluginKey);
                if (!expectedState.equals(state)) {
                    Thread.sleep(200L);
                    totalTimeMillis += 200L;
                }
                if (totalTimeMillis > maxTimeMillis) {
                    fail("Plugin '" + pluginKey + "' did not transition to state '" + expectedState + "', current state is '" + state + "'.");
                }
            }
            while (!expectedState.equals(state));
        } catch (InterruptedException e) {
            throw new AssertionError("Interrupted waiting for plugin state.", e);
        }
    }

    private void waitForPluginState(String state, String pluginKey, JiraCluster.Node node) {
        waitForPluginState(state, pluginKey, node, 60, TimeUnit.SECONDS);
    }
}
