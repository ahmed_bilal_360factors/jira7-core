package it.com.atlassian.jira.plugins.ha.func;

import com.atlassian.jira.plugins.ha.testapi.test.JiraCluster;
import com.atlassian.jira.plugins.ha.testapi.test.JiraHaWebTestRules;
import com.atlassian.jira.webtests.ztests.ao.TestActiveObjects;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.client.apache.ApacheHttpClient;
import com.sun.jersey.client.apache.config.ApacheHttpClientConfig;
import com.sun.jersey.client.apache.config.DefaultApacheHttpClientConfig;
import java.util.Collections;
import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider;
import org.hamcrest.collection.IsEmptyCollection;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import static com.atlassian.jira.webtests.ztests.ao.TestActiveObjects.Blog;
import static com.atlassian.jira.webtests.ztests.ao.TestActiveObjects.Comment;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;

/**
 * Integration test of projects in a cluster.
 *
 * @since v6.3
 */

public class AoTest {
    @Rule
    public TestRule rules = JiraHaWebTestRules.forFuncTest(this);

    @Inject
    private JiraCluster.Node node1;

    @Inject
    private JiraCluster.Node node2;

    private Client client;
    private static final GenericType<List<Blog>> BLOG_LIST = new GenericType<List<Blog>>() {
    };
    private WebResource resource1;
    private WebResource resource2;

    @Before
    public void setUp() {
        ApacheHttpClientConfig config = new DefaultApacheHttpClientConfig();
        config.getProperties().put(ApacheHttpClientConfig.PROPERTY_PREEMPTIVE_AUTHENTICATION, Boolean.TRUE);
        config.getState().setCredentials(null, null, -1, "admin", "admin");
        config.getClasses().add(JacksonJaxbJsonProvider.class);

        client = ApacheHttpClient.create(config);

        resource1 = createResource(node1);
        resource2 = createResource(node2);

        deleteAll(resource1);
    }

    @After
    public void tearDownTest() {
        deleteAll(resource1);
        client.destroy();
    }

    @Test
    public void testCreate() throws Exception {
        final String AUTHOR = "bride";
        final String TEXT = "You an I have unfinished business!";

        Blog newBlog = new Blog();
        newBlog.setAuthor(AUTHOR);
        newBlog.setText(TEXT);
        newBlog.setComments(Collections.<Comment>emptyList());

        ClientResponse response = resource1.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class, newBlog);
        assertThat(response.getStatus(), is(Response.Status.CREATED.getStatusCode()));

        List<Blog> blogs = resource1.get(BLOG_LIST);
        assertThat(blogs.size(), is(1));

        ClientResponse response2 = resource2.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class, newBlog);
        assertThat(response2.getStatus(), is(Response.Status.CREATED.getStatusCode()));

        blogs = resource1.get(BLOG_LIST);
        assertThat(blogs.size(), is(2));

        Blog savedBlog = blogs.get(0);
        assertThat(savedBlog.getAuthor(), is(AUTHOR));
        assertThat(savedBlog.getText(), is(TEXT));
        assertThat(savedBlog.getId(), notNullValue());
        savedBlog = blogs.get(1);
        assertThat(savedBlog.getAuthor(), is(AUTHOR));
        assertThat(savedBlog.getText(), is(TEXT));
        assertThat(savedBlog.getId(), notNullValue());
    }

    @Test
    public void testUpdate() throws Exception {
        String author = "bride";
        String text = "You an I have unfinished business!";

        Blog newBlog = new Blog();
        newBlog.setAuthor(author);
        newBlog.setText(text);
        newBlog.setComments(Collections.<TestActiveObjects.Comment>emptyList());


        ClientResponse response = resource1.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class, newBlog);
        assertThat(response.getStatus(), is(Response.Status.CREATED.getStatusCode()));

        List<Blog> blogs = resource1.get(BLOG_LIST);
        assertThat(blogs.size(), is(1));
        Blog savedBlog = blogs.get(0);

        author = "groom";
        text = "I am leaving on a jet-plane!";

        savedBlog.setAuthor(author);
        savedBlog.setText(text);
//        savedBlog.setComments(ImmutableList.of(Comment.build("Me", "Rubbish comment 1", new Date())));

        response = resource2.type(MediaType.APPLICATION_JSON_TYPE).put(ClientResponse.class, savedBlog);
        assertThat(response.getStatus(), is(Response.Status.OK.getStatusCode()));

        blogs = resource1.get(BLOG_LIST);
        Blog updatedBlog = blogs.get(0);
        assertThat(updatedBlog.getAuthor(), is(author));
        assertThat(updatedBlog.getText(), is(text));
        assertThat(updatedBlog.getId(), notNullValue());
    }

    @Test
    public void testDelete() throws Exception {
        String author = "bride";
        String text = "You an I have unfinished business!";

        Blog newBlog = new Blog();
        newBlog.setAuthor(author);
        newBlog.setText(text);
        newBlog.setComments(Collections.<TestActiveObjects.Comment>emptyList());

        ClientResponse response = resource1.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class, newBlog);
        assertThat(response.getStatus(), is(Response.Status.CREATED.getStatusCode()));

        List<Blog> blogs = resource2.get(BLOG_LIST);
        assertThat(blogs.size(), is(1));
        Blog savedBlog = blogs.get(0);

        response = resource2.path(savedBlog.getId().toString()).type(MediaType.APPLICATION_JSON_TYPE).delete(ClientResponse.class);
        assertThat(response.getStatus(), is(Response.Status.NO_CONTENT.getStatusCode()));

        blogs = resource2.get(BLOG_LIST);
        assertThat(blogs, IsEmptyCollection.<Blog>empty());
        blogs = resource1.get(BLOG_LIST);
        assertThat(blogs, IsEmptyCollection.<Blog>empty());
    }

    private void deleteAll(WebResource resource) {
        final ClientResponse deleteResponse = resource.delete(ClientResponse.class);
        assertThat(deleteResponse.getStatus(), is(Response.Status.NO_CONTENT.getStatusCode()));
    }

    private WebResource createResource(JiraCluster.Node node) {

        return client.resource(node.baseUri()).path("rest")
                .path("func-test").path("latest").path("blog");
    }
}
