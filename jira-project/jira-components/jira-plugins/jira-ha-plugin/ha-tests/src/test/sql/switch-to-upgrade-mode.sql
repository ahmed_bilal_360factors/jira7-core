insert into clusterupgradestate (
  id,
  database_time,
  cluster_version,
  cluster_build_number,
  state,
  order_number
) VALUES (
  9999,
  1475555165690,
  '${jira.stable.version}',
  ${jira.stable.build.number},
  'READY_TO_UPGRADE',
  99999
)