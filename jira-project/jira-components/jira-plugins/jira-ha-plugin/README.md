JIRA High Availability Plugin
=============================

Features
--------

* Allows the system administrator to activate and passivate the current node, by PUTting "true" and "false" respectively to the following resource:

    /rest/ha/1.0/nodes/current?os_username=sysadmin_user&os_password=sysadmin_password

* Prevents any requests other than the above from reaching the passive node.

Sub Modules
-----------

- `ha-plugin`: The actual JIRA plugin that supports activating/passivating nodes and instrumenting the cluster for testing.
- `ha-test-api`: The test rules and backdoors used to write tests against clustered JIRA. This can be used by other plugins to write their own ha tests.
- `ha-tests`: The set of tests to run against clustered JIRA. This is exported so that other plugins can run it as part of their own plans if they want to.

Testing Instructions
--------------------

On CI, all setup and tests can be run with `mvn clean verify -P setup-db` from the `ha-tests` directory. Func and Webdriver tests can also be run separately by specifying either `-DtestGroups=ha-func` or `-DtestGroups=ha-webdriver` (all groups are run by default).

We're differentiating between func and webdriver tests based on browser interaction - if we actually care about what's happening in the browser (i.e we want to instrument user actions and assert DOM state), then it's categorised under **web driver**. If we can get by with backdoors and don't care about the browser, it's **func**.

Because we are testing with 2 Running JIRAs, things get a bit complicated for local testing.

The pom.xml in the `ha-tests` module specifies 2 "products"; "`node1`" and "`node2`"

### Testing in IDEA

There are two options for testing locally:
1. Use the cluster defined in the `ha-tests` pom; or,
2. Start a cluster the usual way with `jmake clustered ...`.

In either case, make sure multicast is running on the local machine: try `# ifconfig lo multicast`.

#### `ha-tests` cluster

- Make sure Postgres is running (should be default ports etc - check the properties defined in the pom if something seems off).
- Set up a local database - From the `ha-tests` directory, run `mvn clean install -Plocal-db`. This should give you:

      - database = jiraha
      - schema = public
      - user = postgres
      - password = postgres

- Start both instances from the `ha-tests` directory:
    - `mvn jira:run -Dproduct=node1`
    - `mvn jira:run -Dproduct=node2`

(Start up one, wait for it to get going, then start the second).

Add the following to your default JUnit run configuration

```
-Dbaseurl.node1=http://localhost:5991/node1
-Dhttp.node1.port=5991
-Dcontext.node1.path=/node1
-Dbaseurl.node2=http://localhost:5992/node2
-Dhttp.node2.port=5992
-Dcontext.node2.path=/node2
```

This will allow the func tests to find the running applications.

#### `jmake` cluster

- Start up two DC nodes through `jmake`:

    ```
    # Start the first node
    jmake clustered debug
    # wait for first JIRA to start...
    # Start the second node
    jmake clustered debug quickstart
    ```

- Make sure **none** of the parameters described in the previous section (`baseurl.node`, `context.node2.path`, etc) are given to JUnit through IDEA (if no values are overridden by these parameters, then the ha tests will fallback to `jmake`'s defaults).
