package com.atlassian.jira.plugins.ha.container.warhack;

import com.atlassian.jira.upgrade.UpgradeTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathException;
import javax.xml.xpath.XPathFactory;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.jar.JarEntry;

/**
 * Adds an upgrade task to upgrades.xml file in JIRA mapped to <code>(latest build number in XML file) + 1</code>.
 */
public class AddUpgradeTaskOperation implements WarOperation {

    private static final Logger log = LoggerFactory.getLogger(AddUpgradeTaskOperation.class);

    private final Class<? extends UpgradeTask> upgradeTaskClass;
    private String generatedBuildNumber;

    public AddUpgradeTaskOperation(Class<? extends UpgradeTask> upgradeTaskClass) {
        this.upgradeTaskClass = upgradeTaskClass;
    }

    @Override
    public InputStream processEntry(JarEntry entry, InputStream is) throws IOException {
        if ("WEB-INF/classes/upgrades.xml".equals(entry.getName())) {
            return updateUpgradesXml(is);
        } else {
            return is;
        }
    }

    @Override
    public void preprocessEntry(JarEntry entry, ExceptionalSupplier<? extends InputStream, IOException> is)
    throws IOException {
        //No preprocessing needed
    }

    private Document parseUpgradesXmlIntoDocument(InputStream upgradesXmlStream)
    throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        return dbf.newDocumentBuilder().parse(upgradesXmlStream, "upgrades.xml");
    }

    private long readLastBuildNumber(Document upgradesXmlDoc, XPath xPath)
    throws XPathException {
        Element lastUpgradeElement = (Element)xPath.evaluate("/upgrades/upgrade[last()]", upgradesXmlDoc, XPathConstants.NODE);
        String lastBuildNumberStr = lastUpgradeElement.getAttribute("build");
        return Long.parseLong(lastBuildNumberStr);
    }

    private long nextBuildNumber(Document upgradesXmlDoc, XPath xPath)
    throws XPathException {
        long lastBuildNumber = readLastBuildNumber(upgradesXmlDoc, xPath);
        return lastBuildNumber + 1;
    }

    private void writeNewUpgradeElement(Document upgradesXmlDoc, String nextBuildNumberStr, XPath xPath)
    throws XPathException {

        Element upgradesElement = (Element)xPath.evaluate("/upgrades", upgradesXmlDoc, XPathConstants.NODE);
        Element newUpgradeElement = upgradesXmlDoc.createElementNS(upgradesElement.getNamespaceURI(), "upgrade");
        upgradesElement.appendChild(newUpgradeElement);
        newUpgradeElement.setAttribute("build", nextBuildNumberStr);
        Element newUpgradeClassElement = upgradesXmlDoc.createElementNS(upgradesElement.getNamespaceURI(), "class");
        newUpgradeElement.appendChild(newUpgradeClassElement);
        newUpgradeClassElement.setTextContent(upgradeTaskClass.getName());
    }

    private InputStream generateNewXmlFile(Document upgradesXmlDoc)
    throws TransformerException {
        TransformerFactory tf = TransformerFactory.newInstance();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        tf.newTransformer().transform(new DOMSource(upgradesXmlDoc), new StreamResult(bos));
        return new ByteArrayInputStream(bos.toByteArray());
    }

    private InputStream updateUpgradesXml(InputStream upgradesXmlStream) throws IOException {
        try {
            XPath xPath = XPathFactory.newInstance().newXPath();

            Document doc = parseUpgradesXmlIntoDocument(upgradesXmlStream);
            long nextBuildNumber = nextBuildNumber(doc, xPath);
            String nextBuildNumberStr = String.valueOf(nextBuildNumber);
            writeNewUpgradeElement(doc, nextBuildNumberStr, xPath);
            InputStream newXmlFileStream = generateNewXmlFile(doc);
            log.info("Added upgrade task " + upgradeTaskClass.getName() + " for build " + nextBuildNumberStr);
            generatedBuildNumber = nextBuildNumberStr;
            return newXmlFileStream;

        } catch (XPathException | ParserConfigurationException | SAXException | TransformerException e) {
            throw new IOException("Error generating new upgrades.xml file: " + e, e);
        }
    }

    @Override
    public Collection<? extends FileEntry> additionalFiles() throws IOException {
        //Add the class file to the WAR
        URL classResource = upgradeTaskClass.getResource(upgradeTaskClass.getSimpleName() + ".class");
        if (classResource == null) {
            throw new FileNotFoundException("Could not read bytes for class " + upgradeTaskClass.getName());
        }
        return Collections.singletonList(new SimpleFileEntry("WEB-INF/classes/" + classToResourceName(upgradeTaskClass),
                                                classResource::openStream));
    }

    private static String classToResourceName(Class<?> clazz) {
        return clazz.getName().replace('.', '/') + ".class";
    }

    /**
     * @return the build number generated after processing upgrades.xml.
     */
    protected String getGeneratedBuildNumber() {
        return generatedBuildNumber;
    }
}
