package com.atlassian.jira.plugins.ha.container;

import java.io.IOException;

/**
 * @since v7.3
 */
public interface ContainerSetBuilder<T extends ContainerSet<?>> {
    T build() throws IOException;

    ContainerSetBuilder<T> numNodes(int numNodes);

    ContainerSetBuilder<T> name(String name);

    ContainerSetBuilder<T> sharedHome(String sharedHome);

    ContainerSetBuilder<T> dependencyDir(String dependencyDir);

    ContainerSetBuilder<T> dbconfig(String dbconfig);

    ContainerSetBuilder<T> systemProperty(String property, String value);

    ContainerSetBuilder<T> systemProperty(String property);
}
