package com.atlassian.jira.plugins.ha.container.warhack;

import java.io.IOException;
import java.io.InputStream;

/**
 * A file that is written to a WAR file.
 */
public interface FileEntry {
    /**
     * @return name of the entry in the WAR file.  e.g. <code>WEB-INF/classes/META-INF/myfile.txt</code>
     */
    String getName();

    /**
     * @return an input stream for reading the data of the file to put into the WAR.
     *
     * @throws IOException if an I/O error occurs.
     */
    InputStream getInputStream() throws IOException;
}
