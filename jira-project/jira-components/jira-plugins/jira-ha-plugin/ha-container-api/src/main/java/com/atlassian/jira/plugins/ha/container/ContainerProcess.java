package com.atlassian.jira.plugins.ha.container;

import java.io.IOException;

/**
 * Control for a web container's executable process.
 * 
 * @since 7.3.5
 */
public interface ContainerProcess {
    /**
     * @return true if the process is currently running, false if the process does not exist (such as if it has terminated).
     * @throws IOException if an error occurs reading process information.
     */
    boolean isRunning()
    throws IOException;
}
