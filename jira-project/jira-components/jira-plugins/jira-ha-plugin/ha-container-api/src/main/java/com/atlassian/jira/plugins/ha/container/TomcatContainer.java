package com.atlassian.jira.plugins.ha.container;

import com.atlassian.jira.cluster.ClusterNodeProperties;
import com.atlassian.jira.cluster.ClusterNodePropertiesImpl;
import com.atlassian.jira.config.util.AbstractJiraHome;
import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.plugins.ha.testapi.client.ReplicationControl;
import com.atlassian.jira.plugins.ha.testapi.test.JiraCluster;
import com.atlassian.jira.util.Supplier;
import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableMap;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.net.ServerSocket;
import java.nio.file.Path;
import java.time.Duration;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import javax.annotation.Nonnull;
import org.codehaus.cargo.container.ContainerException;
import org.codehaus.cargo.container.deployable.Deployable;
import org.codehaus.cargo.container.deployable.WAR;
import org.codehaus.cargo.container.property.GeneralPropertySet;
import org.codehaus.cargo.container.tomcat.Tomcat8xInstalledLocalContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TomcatContainer implements Container {
    private static final Logger log = LoggerFactory.getLogger(TomcatContainer.class);

    /**
     * Max time to wait from Tomcat sending stop command to the Tomcat process actually dying.
     */
    private static final Duration CONTAINER_DEATH_TIMEOUT = Duration.ofMinutes(5);

    private final Tomcat8xInstalledLocalContainer cargoContainer;
    private final Supplier<JiraCluster.Node> nodeMaker;
    private JiraCluster.Node jiraNode;
    private final JiraTestedProduct product;
    private final Map<JiraType, Path> warArchiveMap;
    private ReplicationControl replication;
    private final Path jiraHomeDir;

    private final ProcessControlProvider processControlProvider = new AttachProcessControlProvider();

    TomcatContainer(Tomcat8xInstalledLocalContainer cargoContainer, Supplier<JiraCluster.Node> nodeMaker,
                           final JiraTestedProduct product, Map<JiraType, ? extends Path> warArchiveMap, final Path jiraHomeDir) {
        this.cargoContainer = cargoContainer;
        this.nodeMaker = nodeMaker;
        this.product = product;
        this.warArchiveMap = ImmutableMap.copyOf(warArchiveMap);
        this.jiraHomeDir = jiraHomeDir;
        
        processControlProvider.configureContainer(this);
    }

    @Override
    public void start() {
        waitForKnownContainerPortsToBeFree();
        cargoContainer.start();
    }

    @Override
    public void stop() {
        try {
            ContainerProcess process = processControlProvider.containerProcess(this);
            
            cargoContainer.stop();
            
            //Wait for the process to die, fail if it doesn't
            if (process == null) {
                log.warn("Process for Tomcat container not found, will not be able to wait for process to die.");
            } else {
                log.info("Waiting for Tomcat process to terminate...");
                Stopwatch timer = Stopwatch.createStarted();
                waitForContainerProcessToDie(process);
                log.info("Tomcat container process took " + timer.toString() + " to terminate");
            }
            
            waitForKnownContainerPortsToBeFree();
            
        } catch (ContainerException | InterruptedException | IOException | TimeoutException e) {
            log.warn(String.format("Problem stopping tomcat container for node %s: '%s', dumping threads to System.err",
                    jiraNode(), e.getMessage()));
            
            generateThreadDump();
        } catch (Throwable t) {
            log.error("Error occurred stopping Tomcat container for node " + jiraNode() + ": " + t, t);
            throw t;
        }
    }
    
    private void waitForKnownContainerPortsToBeFree() {
        //Wait for any container-related ports to be freed
        //These are RMI ports and HTTP ports
        Set<Integer> ports = new HashSet<>();
        getClusterProperties().getPropertySafely("ehcache.listener.port").map(Integer::parseInt).ifPresent(ports::add);
        getClusterProperties().getPropertySafely("ehcache.object.port").map(Integer::parseInt).ifPresent(ports::add);
        int portOffset = Integer.parseInt(getCargoContainer().getConfiguration().getPropertyValue(GeneralPropertySet.PORT_OFFSET));
        ports.add(8080 + portOffset);
        waitForPortsToBeFree(ports);
    }
    
    private void waitForPortsToBeFree(Collection<Integer> ports) {
        log.info("Waiting for ports " + ports + " to be freed...");
        Stopwatch timer = Stopwatch.createStarted();
        ports.forEach(this::waitForPortToBeFree);
        log.info("Ports " + ports + " freed in " + timer);
    }
    
    private void waitForPortToBeFree(int port) {
        long maxTime = System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(60);
        try {
            do {
                try (ServerSocket socket = new ServerSocket(port)) {
                    log.debug("Port " + port + " is free");
                    return;
                } catch (IOException e) {
                    log.debug("Server socket port " + port + " could not be opened yet.", e);
                }
                Thread.sleep(200L);
            } while (System.currentTimeMillis() < maxTime);
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted waiting for ports to be freed.", e);
        }
        
        throw new RuntimeException("Timed out waiting for port " + port + " to become free");
    }
    
    private void waitForContainerProcessToDie(ContainerProcess process) throws IOException, InterruptedException, TimeoutException {
        
        long maxWaitTime = System.currentTimeMillis() + CONTAINER_DEATH_TIMEOUT.toMillis();
        
        while (process.isRunning() && System.currentTimeMillis() < maxWaitTime) {
            Thread.sleep(1000L);
            log.info("Waiting for Tomcat process to die...");
        }
        
        if (process.isRunning()) {
            throw new TimeoutException("Timed out waiting for Tomcat process " + process + " to die.");
        }
    }

    public synchronized ReplicationControl replication() {
        if (replication == null) {
            replication = new ReplicationControl(jiraNode().environment());
        }
        return replication;
    }

    @Override
    public synchronized JiraCluster.Node jiraNode() {
        if (jiraNode == null) {
            jiraNode = nodeMaker.get();
        }

        return jiraNode;
    }

    /**
     * Access the underlying cargo container.  Can be useful for fine-grained customization.
     *
     * @return cargo container hosting the JIRA instance.
     */
    public Tomcat8xInstalledLocalContainer getCargoContainer() {
        return cargoContainer;
    }

    @Override
    public void installWar(Path warFile, String context) {

        //Remove existing JIRA if it exists
        for (Iterator<Deployable> i = cargoContainer.getConfiguration().getDeployables().iterator(); i.hasNext(); ) {
            Deployable dep = i.next();
            if (dep instanceof WAR) {
                WAR depWar = (WAR) dep;
                if (context.equals(depWar.getContext()))
                    i.remove();
            }
        }

        WAR war = new WAR(warFile.toAbsolutePath().toString());
        war.setContext(context);
        cargoContainer.getConfiguration().addDeployable(war);
    }

    @Override
    public void installJira(JiraType warType) {
        Objects.requireNonNull(warType, "warType == null");
        Path warArchive = warArchiveMap.get(warType);
        if (warArchive == null) {
            throw new RuntimeException("No JIRA WAR archive of type " + warType + " registered.");
        }

        installWar(warArchive, "jira");
    }

    @Override
    public Path getLocalHome() {
        return jiraHomeDir;
    }

    public ClusterNodeProperties getClusterProperties() {
        final JiraHome jiraHome = resolveJiraHome();
        return new ClusterNodePropertiesImpl(jiraHome);
    }

    private AbstractJiraHome resolveJiraHome() {
        return new AbstractJiraHome() {
            @Nonnull
            @Override
            public File getLocalHome() {
                return jiraHomeDir.toFile();
            }
        };
    }

    @Override
    public Map<String, String> getSystemProperties() {
        return getCargoContainer().getSystemProperties();
    }

    @Override
    public void generateThreadDump() {
        StringWriter buf = new StringWriter();
        try {
            processControlProvider.generateThreadDump(this, buf);
            log.info("Thread dump for " + jiraNode().product().getProductInstance().getInstanceId() + ":" + buf.toString());
        } catch (IOException e) {
            log.error("Thread dump failed for " + jiraNode().product().getProductInstance().getInstanceId() + ":" + e, e);
        }
    }

    public JiraTestedProduct getProduct() {
        return product;
    }
}
