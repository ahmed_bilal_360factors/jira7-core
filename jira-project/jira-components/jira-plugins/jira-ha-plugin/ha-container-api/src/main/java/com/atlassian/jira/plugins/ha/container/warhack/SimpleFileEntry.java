package com.atlassian.jira.plugins.ha.container.warhack;

import java.io.IOException;
import java.io.InputStream;

public class SimpleFileEntry implements FileEntry {

    private final String name;
    private final ExceptionalSupplier<? extends InputStream, ? extends IOException> streamSupplier;

    public SimpleFileEntry(String name, ExceptionalSupplier<? extends InputStream, ? extends IOException> streamSupplier) {
        this.name = name;
        this.streamSupplier = streamSupplier;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return streamSupplier.get();
    }
}
