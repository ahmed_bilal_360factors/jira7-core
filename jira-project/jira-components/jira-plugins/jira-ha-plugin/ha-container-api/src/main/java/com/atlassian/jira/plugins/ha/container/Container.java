package com.atlassian.jira.plugins.ha.container;

import com.atlassian.jira.cluster.ClusterNodeProperties;
import com.atlassian.jira.plugins.ha.testapi.test.JiraCluster;

import java.nio.file.Path;
import java.util.Map;

/**
 * A single webapp container that can hold a running instance of JIRA.
 */
public interface Container {
    /**
     * Start the instance.
     */
    void start();

    /**
     * Cleanly stop the instance.
     */
    void stop();

    /**
     * @return the JIRA cluster node the instance is running.  Use this for accessing REST endpoints, etc.
     */
    JiraCluster.Node jiraNode();

    /**
     * Installs a WAR file intothis container.
     *
     * @param warFile the WAR file to install.
     * @param context the context path.  Do not include any leading or trailing '/' characters.
     */
    void installWar(Path warFile, String context);

    /**
     * Installs a predefined JIRA WAR into this container.
     *
     * @param warType the JIRA version to install.
     */
    void installJira(JiraType warType);

    /**
     * @return a path to the local home directory of JIRA
     */
    Path getLocalHome();

    /**
     * @return The wrapped property file for this node (cluster.properties from jirahome).
     */
    ClusterNodeProperties getClusterProperties();

    /**
     * @return the system properties for the container.  Can be modified.
     * 
     * @since 7.3.5
     */
    Map<String, String> getSystemProperties();

    /**
     * Generate a thread dump of this process to the logs.
     * 
     * @since 7.3.5
     */
    void generateThreadDump();
}
