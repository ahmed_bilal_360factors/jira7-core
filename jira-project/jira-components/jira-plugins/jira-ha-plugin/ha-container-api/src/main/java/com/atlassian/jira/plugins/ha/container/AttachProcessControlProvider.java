package com.atlassian.jira.plugins.ha.container;

import com.google.common.io.CharStreams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.tools.ToolProvider;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

/**
 * Use Java Attach API to identify container processes.
 * 
 * @since 7.3.5
 */
/*
 * Implementation note: this is using reflection to access all of the attach API.  This is sucky, but less sucky than
 * fighting for the ability to either get the ability to put tools.jar at system scope in the POM without breaking all
 * of our validation rules in Nexus or arguing against those rules and losing.
 */
public class AttachProcessControlProvider implements ProcessControlProvider {
    
    private static final Logger log = LoggerFactory.getLogger(AttachProcessControlProvider.class);

    /**
     * The name of the system property in the Tomcat container process that stores globally unique ID used to identify
     * a specific node's Java process.  The Attach API is used to read system properties from each Java process and uses
     * the value of this key to know which one is owned by the specific node.
     */
    private static final String PROCESS_IDENTIFIER_PROPERTY_KEY = AttachProcessControlProvider.class.getName() + ".id";
    
    private final Class<?> attachVirtualMachineClass;
    private final Class<?> attachVirtualMachineDescriptorClass;
    private final Class<?> hotspotVirtualMachineClass;
    
    public AttachProcessControlProvider() {
        ClassLoader toolsLoader = ToolProvider.getSystemToolClassLoader();
        try {
            attachVirtualMachineClass = Class.forName("com.sun.tools.attach.VirtualMachine", true, toolsLoader);
            attachVirtualMachineDescriptorClass = Class.forName("com.sun.tools.attach.VirtualMachineDescriptor", true, toolsLoader);
            hotspotVirtualMachineClass = Class.forName("sun.tools.attach.HotSpotVirtualMachine", true, toolsLoader);
        } catch (ClassNotFoundException e) {
            //If running under JDK, tools.jar should always be available
            throw new RuntimeException("tools.jar not available, attach API not found", e);
        }
    }
    
    @Override
    public void configureContainer(Container container) {
        //UUID should be globally unique, even if there are multiple tests runs on the same machine for some reason
        container.getSystemProperties().put(PROCESS_IDENTIFIER_PROPERTY_KEY, UUID.randomUUID().toString());
    }

    @Override
    public ContainerProcess containerProcess(Container container) 
    throws IOException {
        String containerId = container.getSystemProperties().get(PROCESS_IDENTIFIER_PROPERTY_KEY);
        if (containerId == null) {
            throw new IllegalStateException("Container not configured by process control provider.");
        }

        for (Object vmd : virtualMachineList()) {
            try {
                Object vm = virtualMachineAttach(vmd);
                try {
                    String vmId = virtualMachineGetSystemProperties(vm).getProperty(PROCESS_IDENTIFIER_PROPERTY_KEY);
                    if (containerId.equals(vmId)) {
                        return new JvmAttachProcess(vmd);
                    }
                } finally {
                    virtualMachineDetach(vm);
                }
            } catch (IOException e) {
                throw e;
            } catch (Exception e) { //AttachNotSupportedException
                log.warn("Failed to attach to " + vmd + " so cannot scan whether this is a container.");
            }
        }
        
        return null;
    }

    /**
     * <a href="http://docs.oracle.com/javase/8/docs/jdk/api/attach/spec/com/sun/tools/attach/VirtualMachine.html#getSystemProperties--">VirtualMachine.getSystemProperties()</a>
     */
    private Properties virtualMachineGetSystemProperties(Object virtualMachine) throws IOException {
        try {
            return (Properties)attachVirtualMachineClass.getMethod("getSystemProperties").invoke(virtualMachine);
        } catch (NoSuchMethodException e) {
            NoSuchMethodError err = new NoSuchMethodError(e.getMessage());
            err.initCause(e);
            throw err;
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * <a href="http://docs.oracle.com/javase/8/docs/jdk/api/attach/spec/com/sun/tools/attach/VirtualMachine.html#detach--">VirtualMachine.detach()</a>
     */
    private void virtualMachineDetach(Object virtualMachine) {
        try {
            attachVirtualMachineClass.getMethod("detach").invoke(virtualMachine);
        } catch (NoSuchMethodException e) {
            NoSuchMethodError err = new NoSuchMethodError(e.getMessage());
            err.initCause(e);
            throw err;
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * <a href="http://docs.oracle.com/javase/8/docs/jdk/api/attach/spec/com/sun/tools/attach/VirtualMachine.html#attach-com.sun.tools.attach.VirtualMachineDescriptor-">VirtualMachine.attach(VirtualMachineDescriptor)</a>
     */
    private Object virtualMachineAttach(Object virtualMachineDescriptor) {
        try {
            return attachVirtualMachineClass.getMethod("attach", attachVirtualMachineDescriptorClass).invoke(null, virtualMachineDescriptor);
        } catch (NoSuchMethodException e) {
            NoSuchMethodError err = new NoSuchMethodError(e.getMessage());
            err.initCause(e);
            throw err;
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * <a href="http://docs.oracle.com/javase/8/docs/jdk/api/attach/spec/com/sun/tools/attach/VirtualMachine.html#list--">VirtualMachine.list()</a>
     */
    private List<?> virtualMachineList() {
        try {
            return (List<?>) attachVirtualMachineClass.getMethod("list").invoke(null);
        } catch (NoSuchMethodException e) {
            NoSuchMethodError err = new NoSuchMethodError(e.getMessage());
            err.initCause(e);
            throw err;
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void generateThreadDump(Container container, Writer writer) throws IOException {
        String containerId = container.getSystemProperties().get(PROCESS_IDENTIFIER_PROPERTY_KEY);
        if (containerId == null) {
            throw new IllegalStateException("Container not configured by process control provider.");
        }

        for (Object vmd : virtualMachineList()) {
            try {
                Object vm = virtualMachineAttach(vmd);
                try {
                    String vmId = virtualMachineGetSystemProperties(vm).getProperty(PROCESS_IDENTIFIER_PROPERTY_KEY);
                    if (containerId.equals(vmId)) {
                        
                        Object param1 = new Object[0];
                        try (Reader reader = new InputStreamReader((InputStream)hotspotVirtualMachineClass.getMethod("remoteDataDump", Object[].class).invoke(vm, param1), StandardCharsets.UTF_8)) {
                            CharStreams.copy(reader, writer);
                        }
                    }
                } finally {
                    virtualMachineDetach(vm);
                }
            } catch (IOException e) {
                throw e;
                
            } catch (Exception e) { //AttachNotSupportedException
                log.warn("Failed to attach to " + vmd + " so cannot scan whether this is a container.", e);
            }
        }
    }

    private class JvmAttachProcess implements ContainerProcess {
        private final Object vmDescriptor;

        public JvmAttachProcess(Object vmDescriptor) {
            this.vmDescriptor = vmDescriptor;
        }

        @Override
        public boolean isRunning() throws IOException {
            return virtualMachineList().contains(vmDescriptor);
        }

        @Override
        public String toString() {
            return vmDescriptor.toString();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            JvmAttachProcess that = (JvmAttachProcess) o;

            return vmDescriptor.equals(that.vmDescriptor);
        }

        @Override
        public int hashCode() {
            return vmDescriptor.hashCode();
        }
    }
}
