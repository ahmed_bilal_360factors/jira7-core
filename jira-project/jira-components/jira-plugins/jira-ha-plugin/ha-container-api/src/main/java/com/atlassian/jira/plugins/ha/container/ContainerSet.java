package com.atlassian.jira.plugins.ha.container;

import com.atlassian.jira.functest.framework.backdoor.ZeroDowntimeControl;
import com.atlassian.jira.plugins.ha.testapi.test.JiraCluster;

import java.nio.file.Path;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * A set of webapp containers that can potentially run a JIRA cluster.
 *
 * @param <T> the container type.
 */
public interface ContainerSet<T extends Container> {

    /**
     * Start all containers in the set.
     */
    void startAll();

    /**
     * Cleanly stop all containers in the set.
     */
    void stopAll();

    /**
     * @return all containers in the set.
     */
    List<? extends T> getContainers();

    /**
     * Access an object that can perform operations on the JIRA cluster.  Access REST endpoints through this.
     *
     * @return object that can perform JIRA cluster operations.
     */
    JiraCluster cluster();

    /**
     * Installs a predefined JIRA WAR into all containers in the set.
     *
     * @param warType the JIRA version to install.
     */
    void installJira(JiraType warType);

    /**
     * Return the path of a predefined JIRA WAR file.  File may not actually exist.
     *
     * @param warType JIRA version.
     *
     * @return the path of the WAR file.
     */
    Path jiraWarFile(JiraType warType);

    /**
     * Installs a WAR file into all containers in the set.
     *
     * @param warFile the WAR file to install.
     * @param context the context path.  Do not include any leading or trailing '/' characters.
     */
    void installWar(Path warFile, String context);

    /**
     * Installs any plugins to shared home that are required for testing.  These plugins are the func-test plugin, ha
     * plugin, etc.
     *
     * @param jiraType JIRA version to target.
     */
    void installTestingPlugins(JiraType jiraType);

    /**
     * Installs a plugin into shared home.
     *
     * @param pluginFile the plugin JAR/OBR file to install.
     */
    void installSharedPlugin(Path pluginFile);

    /**
     * Reset the database of the instance, returning it to an initial state.
     */
    void resetDatabase();

    Path getSharedHome();

    void waitForUpgradeState(ZeroDowntimeControl.UpgradeState targetState, Duration timeout);

    /**
     * Waits at most the specified amount of time for all containers to finish starting up and join the cluster.
     *
     * @param timeout the maximum amount of time to wait.
     */
    void waitForStartupOfActiveContainers(Duration timeout);

    /**
     * Waits for a default maximum amount of time for all containers that are starting to complete startup and join the
     * cluster.
     */
    void waitForStartupOfActiveContainers();
}
