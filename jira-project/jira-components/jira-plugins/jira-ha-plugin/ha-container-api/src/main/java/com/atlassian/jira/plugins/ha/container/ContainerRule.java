package com.atlassian.jira.plugins.ha.container;

import org.junit.rules.TestRule;

public interface ContainerRule<T extends Container> extends TestRule, ContainerSet<T> {
}
