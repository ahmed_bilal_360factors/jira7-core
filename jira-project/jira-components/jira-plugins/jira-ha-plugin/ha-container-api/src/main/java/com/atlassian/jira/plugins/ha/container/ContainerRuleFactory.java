package com.atlassian.jira.plugins.ha.container;

import com.atlassian.jira.config.properties.JiraSystemProperties;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.config.junit4.rule.DirtyWarningTerminatorRule;
import com.atlassian.jira.pageobjects.config.junit4.rule.RuleChainBuilder;
import com.atlassian.jira.pageobjects.setup.JavaScriptErrorsRuleFactory;
import com.atlassian.jira.pageobjects.setup.JiraWebDriverScreenshotRule;
import com.atlassian.jira.pageobjects.setup.JiraWebTestLogger;
import com.atlassian.webdriver.testing.rule.LogPageSourceRule;
import com.atlassian.webdriver.testing.rule.SessionCleanupRule;
import com.atlassian.webdriver.testing.rule.WindowSizeRule;
import net.sf.cglib.proxy.Mixin;
import org.junit.rules.ExternalResource;
import org.junit.rules.RuleChain;
import org.junit.rules.TestRule;

import java.io.IOException;
import java.util.List;
import java.util.function.Function;

import static java.util.stream.Collectors.toList;

/**
 * Provides various functionality to set up a JIRA cluster based on Apache Tomcat using the Cargo API.
 * <p></p>
 * This is the main class you have to use to write container tests.
 * <p></p>
 * Example usage:
 * <pre><code>
 *     public class Test {
 *         &#064;Rule
 *         public ContainerRule&lt;?&gt; containerSet = ContainerRuleFactory.create(builder -> builder.numNodes(2));
 *
 *         &#064;Test
 *         public void testCase() {
 *             containerSet.resetDatabase();
 *             containerSet.installJira(JiraType.BASE);
 *             containerSet.installTestingPlugins(JiraType.BASE);
 *
 *             Container node1 = containerSet.getContainers().get(0);
 *             Container node2 = containerSet.getContainers().get(1);
 *
 *             containerSet.startAll();
 *             containerSet.cluster().waitForStartup();
 *         }
 *     }
 * </code></pre>
 */
public class ContainerRuleFactory {
    static {
        JiraSystemProperties.getInstance().setProperty("atlassian.test.target.dir", "target");
    }

    public static ContainerRule<?> create(Function<TomcatContainerSetBuilder, TomcatContainerSetBuilder> builder) {
        try {
            final ContainerSet<TomcatContainer> containerSet = builder.apply(new TomcatContainerSetBuilder()).build();
            final TestRule cleanupRule = new ExternalResource() {
                @Override
                protected void after() {
                    containerSet.stopAll();
                }
            };

            final List<TestRule> productRules = containerSet.getContainers().stream().map(container -> {
                final JiraTestedProduct product = container.getProduct();
                return RuleChainBuilder.forProduct(product)
                        .around(WindowSizeRule.class)
                        .around(SessionCleanupRule.class)
                        .around(JiraWebDriverScreenshotRule.class)
                        .around(DirtyWarningTerminatorRule.class)
                        .around(JavaScriptErrorsRuleFactory.create(() -> product.getTester().getDriver()))
                        .around(new LogPageSourceRule(product.getTester().getDriver(), JiraWebTestLogger.LOGGER))
                        .build();
            }).collect(toList());

            RuleChain ruleChain = RuleChain.outerRule(cleanupRule);
            for (TestRule rule : productRules) {
                ruleChain = ruleChain.around(rule);
            }

            return (ContainerRule<?>) Mixin.create(new Class[]{ ContainerSet.class, TestRule.class, ContainerRule.class}, new Object[]{ containerSet, ruleChain });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
