package com.atlassian.jira.plugins.ha.container.warhack;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Collections;
import java.util.jar.JarEntry;

/**
 * Exclude files from target WAR by name.
 */
public class ExcludeFileOperation implements WarOperation {

    private static final Logger log = LoggerFactory.getLogger(ExcludeFileOperation.class);

    private final String name;

    public ExcludeFileOperation(String name) {
        this.name = name;
    }

    @Override
    public InputStream processEntry(JarEntry entry, InputStream is) throws IOException {
        if (entry.getName().equals(name)) {
            log.info("Excluding file " + name + " from WAR");
            return null;
        } else {
            return is;
        }
    }

    @Override
    public void preprocessEntry(JarEntry entry, ExceptionalSupplier<? extends InputStream, IOException> is) throws IOException {
        //No-op
    }

    @Override
    public Collection<? extends FileEntry> additionalFiles() throws IOException {
        return Collections.emptyList();
    }
}
