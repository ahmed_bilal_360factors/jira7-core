package com.atlassian.jira.plugins.ha.container;

import com.atlassian.core.util.FileUtils;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.config.properties.JiraSystemProperties;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.plugins.ha.container.warhack.WarHacker;
import com.atlassian.jira.plugins.ha.testapi.test.JiraCluster;
import com.atlassian.jira.plugins.ha.testapi.test.driver.JiraTestedProductFactory;
import com.atlassian.jira.plugins.ha.testapi.test.driver.JiraWebDriverTestedProductFactory;
import com.atlassian.plugin.util.PluginUtils;
import com.google.common.collect.Maps;
import org.apache.commons.io.FilenameUtils;
import org.codehaus.cargo.container.property.GeneralPropertySet;
import org.codehaus.cargo.container.tomcat.Tomcat8xInstalledLocalContainer;
import org.codehaus.cargo.container.tomcat.Tomcat8xStandaloneLocalConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * One special function that is hidden in this class is to specify debugging for each node using a system property.
 * It works like this:
 * <pre><code>
 *     -DcontainerDebug=jira-1:5005,jira-2:5006
 * </code></pre>
 * This will start up <code>node-1</code> with debugging enabled on port <code>5005</code> and <code>jira-2</code> with
 * debugging enabled on port <code>5006</code>.
 */
public class TomcatContainerSetBuilder implements ContainerSetBuilder<TomcatContainerSet> {
    private static final Logger log = LoggerFactory.getLogger(TomcatContainerSetBuilder.class);

    private static Set<Integer> previouslyAssignedPorts = new TreeSet<>();

    private int numNodes = 1;
    private String name = "jira";
    private String sharedHome = "shared-home";
    private String dependencyDir = "dependency";
    private String dbconfig = "dbconfig.xml";
    private Map<String, String> systemProperties = Maps.newLinkedHashMap();

    TomcatContainerSetBuilder() {
    }

    @Override
    public TomcatContainerSet build() throws IOException {
        Path targetDirectory = Paths.get("target");

        Path tomcatDirectory = findTomcat(targetDirectory);
        Path sharedHomePath = targetDirectory.resolve(sharedHome);
        Path dependencyDirPath = targetDirectory.resolve(dependencyDir);
        ClassPath classpathAdditions = ClassPath.fromDirectoryOfJars(dependencyDirPath);
        Path dbConfigFile = targetDirectory.resolve(dbconfig);
        Map<JiraType, ? extends Path> warMap = readJiraWars(dependencyDirPath, classpathAdditions);

        Files.createDirectories(sharedHomePath);
        JiraWebDriverTestedProductFactory productFactory = new JiraWebDriverTestedProductFactory();

        List<TomcatContainer> containers = new ArrayList<>(numNodes);
        for (int i = 0; i < numNodes; i++) {
            containers.add(createContainer(tomcatDirectory,
                    targetDirectory,
                    name + "-" + (i + 1),
                    i,
                    dbConfigFile,
                    sharedHomePath,
                    classpathAdditions,
                    productFactory,
                    warMap,
                    systemProperties));
        }

        return new TomcatContainerSet(containers, () -> new JiraCluster(containers.stream().map(TomcatContainer::jiraNode).collect(Collectors.toList())), sharedHomePath, targetDirectory, warMap, dbConfigFile);
    }

    @Override
    public TomcatContainerSetBuilder numNodes(final int numNodes) {
        this.numNodes = numNodes;
        return this;
    }

    @Override
    public TomcatContainerSetBuilder name(final String name) {
        this.name = name;
        return this;
    }

    @Override
    public TomcatContainerSetBuilder sharedHome(final String sharedHome) {
        this.sharedHome = sharedHome;
        return this;
    }

    @Override
    public TomcatContainerSetBuilder dependencyDir(final String dependencyDir) {
        this.dependencyDir = dependencyDir;
        return this;
    }

    @Override
    public TomcatContainerSetBuilder dbconfig(final String dbconfig) {
        this.dbconfig = dbconfig;
        return this;
    }

    @Override
    public TomcatContainerSetBuilder systemProperty(final String property, final String value) {
        this.systemProperties.put(property, value);
        return this;
    }

    @Override
    public TomcatContainerSetBuilder systemProperty(final String property) {
        this.systemProperties.put(property, "true");
        return this;
    }

    private Path findTomcat(Path targetDirectory) throws IOException {
        Path tomcatBase = targetDirectory.resolve("tomcat");
        return Files.list(tomcatBase)
                .filter(Files::isDirectory)
                .sorted(Comparator.reverseOrder()) //latest version
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Could not find any Tomcat downloads"));
    }

    private TomcatContainer createContainer(Path tomcatDirectory, Path baseDirectory, String containerName,
                                            int nodeIndex, Path dbConfigFile, Path sharedHome,
                                            ClassPath tomcatClasspathAdditions,
                                            JiraTestedProductFactory productFactory,
                                            Map<JiraType, ? extends Path> warArchiveMap,
                                            final Map<String, String> systemProperties)
            throws IOException {

        Path configDir = baseDirectory.resolve(containerName).toAbsolutePath();
        Path jiraHomeDir = baseDirectory.resolve(containerName + "-home").toAbsolutePath();
        FileUtils.deleteDir(configDir.toFile());
        Files.createDirectories(configDir);
        Files.createDirectories(jiraHomeDir);

        Tomcat8xStandaloneLocalConfiguration config = new Tomcat8xStandaloneLocalConfiguration(configDir.toString());
        config.setProperty(GeneralPropertySet.PORT_OFFSET, String.valueOf(nodeIndex));

        Tomcat8xInstalledLocalContainer container = new Tomcat8xInstalledLocalContainer(config);

        long startupTimeout = Long.parseLong(System.getProperty("ha.container.startupTimeout", "600000"));
        container.setTimeout(startupTimeout);

        container.getSystemProperties().put("jira.home", jiraHomeDir.toString());
        container.getSystemProperties().put("com.atlassian.jira.startup.LauncherContextListener.SYNCHRONOUS", "true");
        container.getSystemProperties().put("atlassian.cluster.scale", "true");
        container.getSystemProperties().put("atlassian.darkfeature.jira.zdu.cluster-upgrade-state", "true");
        container.getSystemProperties().put("atlassian.darkfeature.jira.onboarding.feature.disabled", "true");
        container.getSystemProperties().put("jira.startup.warnings.disable", "true");
        container.getSystemProperties().put(PluginUtils.ATLASSIAN_PLUGINS_ENABLE_WAIT, "300"); //default of 60 seconds can be too low on Bamboo
        container.getSystemProperties().putAll(systemProperties);
        container.setHome(tomcatDirectory.toString());
        Optional.ofNullable(System.getProperties().get("containerDebug"))
                .map(String::valueOf)
                .map(s -> s.split(","))
                .map(Stream::of)
                .ifPresent(stringStream -> stringStream
                        .map(s -> s.split(":"))
                        .filter(strings -> Objects.equals(strings[0], containerName))
                        .forEach(strings -> {
                            container.getConfiguration().getProperties().put(GeneralPropertySet.JVMARGS, "-agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=" + strings[1]);
                        })
                );
        container.setLogger(new LabelledLogger(containerName));

        //Configure classpath
        tomcatClasspathAdditions.getJars().stream()
                .map(Path::toAbsolutePath)
                .map(Path::toString)
                .forEach(container::addSharedClasspath);

        //Configure dbconfig.xml
        Path targetDbConfigFile = jiraHomeDir.resolve("dbconfig.xml");
        Files.copy(dbConfigFile, targetDbConfigFile, StandardCopyOption.REPLACE_EXISTING);

        generateClusterProperties(containerName, jiraHomeDir, sharedHome);
        configureAtlassianUrlSystemProperties(nodeIndex, containerName);

        JiraTestedProduct product = productFactory.create(containerName);

        return new TomcatContainer(container, () -> new JiraCluster.Node(product), product, warArchiveMap, jiraHomeDir);
    }

    private void configureAtlassianUrlSystemProperties(int nodeIndex, String nodeName) {
        JiraProperties systemProperties = JiraSystemProperties.getInstance();

        int nodeDefaultPort = 8080 + nodeIndex;

        final String hostname;
        // if using Docker PBC Selenium is actually running on a different host, give it our address
        if (systemProperties.getBoolean("ha.docker.host")) {
            try {
                hostname = InetAddress.getLocalHost().getHostAddress();
            } catch (UnknownHostException e) {
                throw new RuntimeException(e);
            }
        } else {
            hostname = systemProperties.getProperty("test.host", "localhost");
        }

        //Properties that need to be set:
        //baseurl.nodeX, http.nodeX.port, context.nodeX.path
        setPropertyIfNotAlreadySet("baseurl." + nodeName, "http://" + hostname + ":" + nodeDefaultPort + "/jira");
        setPropertyIfNotAlreadySet("http." + nodeName + ".port", String.valueOf(nodeDefaultPort));
        setPropertyIfNotAlreadySet("context." + nodeName + ".path", "/jira");
    }

    private boolean setPropertyIfNotAlreadySet(String name, String value) {
        if (System.getProperty(name) == null) {
            System.setProperty(name, value);
            return true;
        } else {
            return false;
        }
    }

    private void generateClusterProperties(String instanceName, Path localHomeDirectory, Path sharedHomeDirectory)
            throws IOException {
        Path clusterPropertiesFile = localHomeDirectory.resolve("cluster.properties");

        Properties props = new Properties();
        props.setProperty("jira.shared.home", sharedHomeDirectory.toAbsolutePath().toString());
        props.setProperty("ehcache.listener.port", String.valueOf(findRandomAvailablePort()));
        props.setProperty("ehcache.object.port", String.valueOf(findRandomAvailablePort()));
        props.setProperty("ehcache.listener.hostName", "localhost");
        props.setProperty("ehcache.peer.discovery", "default");
        props.setProperty("jira.node.id", instanceName);

        try (OutputStream os = Files.newOutputStream(clusterPropertiesFile)) {
            props.store(os, "JIRA Cluster properties file");
        }
    }

    private Map<JiraType, ? extends Path> readJiraWars(Path directory, ClassPath tomcatClasspathAdditions)
            throws IOException {
        Map<JiraType, Path> warMap = new EnumMap<>(JiraType.class);

        for (JiraType warType : JiraType.values()) {
            String fileName = "jira-" + warType.name().toLowerCase(Locale.ROOT) + ".war";
            Path warFile = directory.resolve(fileName);
            warMap.put(warType, warFile);
        }

        fixLibsInWars(warMap, tomcatClasspathAdditions);

        //Special case for upgrade WAR - if it does not exist generate one from the base WAR
        if (Files.notExists(warMap.get(JiraType.BASE))) {
            throw new FileNotFoundException("Base WAR file " + warMap.get(JiraType.BASE) + " does not exist.");
        }
        Path upgradeWarFile = warMap.get(JiraType.UPGRADE);
        if (Files.notExists(upgradeWarFile)) {
            //Change the file name for the upgrade WAR so we know it was generated
            //This also allows us to avoid useless fixLibsInWars() for ones generated from known fixed base WARs
            upgradeWarFile = upgradeWarFile.resolveSibling(FilenameUtils.getBaseName(upgradeWarFile.getFileName().toString()) + "-generated.war");
            warMap.put(JiraType.UPGRADE, upgradeWarFile);
            generateUpgradeWar(upgradeWarFile, warMap.get(JiraType.BASE));
        }

        return warMap;
    }

    /**
     * Any WEB-INF/lib JARs present in Tomcat's lib directory are removed from each WAR in the map, a new
     * WAR is generated with a '-fixed' suffix and its path replaced in the map.  If the '-fixed' WAR already
     * exists then use that one instead of regenerating another from scratch.
     *
     * @param warMap                   a map with WAR file values.  Each of these is processed and potentially replaced.
     * @param tomcatClasspathAdditions JARs present on Tomcat's main lib classpath.
     * @throws IOException if an I/O error occurs.
     */
    private void fixLibsInWars(Map<?, Path> warMap, ClassPath tomcatClasspathAdditions)
            throws IOException {
        //Fix each WAR in the map by cleaning up libs
        for (Map.Entry<?, Path> entry : warMap.entrySet()) {
            Path warFile = entry.getValue();
            Path fixedWarFile = warFile.resolveSibling(FilenameUtils.getBaseName(warFile.toString()) + "-fixed.war");
            if (Files.exists(warFile) && Files.notExists(fixedWarFile)) {
                WarHacker warHacker = new WarHacker(warFile);

                warHacker.excludeLibs(tomcatClasspathAdditions.getJars().stream()
                        .map(Path::getFileName)
                        .map(Path::toString)
                        .toArray(String[]::new));

                warHacker.writeWarFile(fixedWarFile);
                entry.setValue(fixedWarFile);
            }
        }
    }

    private void generateUpgradeWar(Path upgradeWarFile, Path baseWarFile) throws IOException {

        log.info("Generating upgrade WAR file from base file");

        WarHacker warHacker = new WarHacker(baseWarFile);
        warHacker.incrementBuildNumber();
        warHacker.writeWarFile(upgradeWarFile);

        log.info("Upgrade WAR generated");
    }

    /**
     * @return A free port that we are safe to give to a container to use for eh cache listeners (we need to declare
     * this in the properties file of a node ahead of time). This should take into account both:
     * - Which nodes are actually free on the system at the current point in time; and also,
     * - Which nodes have already been "promised" to other nodes (but still free on the system because no node has
     * started up).
     */
    private int findRandomAvailablePort() {
        final Optional<Integer> maybeFreePort = findFreeSystemPort();

        if (!maybeFreePort.isPresent()) {
            throw new RuntimeException("Couldn't find enough ports for node");
        }

        // If we haven't already assigned this port to a properties file in a another node - we're OK to use it here.
        final Integer freePort = maybeFreePort.get();
        if (!previouslyAssignedPorts.contains(freePort)) {
            previouslyAssignedPorts.add(freePort);
            return freePort;
        }

        // If the system gave us a free port, but it's already been assigned to a properties file on another node, then
        // check the next 15 sequentially. This is me trying to find a balance between finding a free port, but also
        // not looping forever (we can't just keep asking the system for ports because it might just keep giving back
        // the same thing).
        for (int i = 0; i < 15; i++) {
            final int nextPort = freePort + i;
            if (!previouslyAssignedPorts.contains(nextPort)) {
                previouslyAssignedPorts.add(nextPort);
                return nextPort;
            }
        }

        // Not much more we can do except fail the test.
        throw new RuntimeException("Couldn't find enough ports for node");
    }

    private Optional<Integer> findFreeSystemPort() {
        try (ServerSocket socket = new ServerSocket(0)) {
            return Optional.of(socket.getLocalPort());
        } catch (IOException e) {
            log.warn("Couldn't find any free system ports for this node to use!");
            return Optional.empty();
        }
    }


}
