package com.atlassian.jira.plugins.ha.container;

import java.io.IOException;
import java.io.Writer;

/**
 * Provides the ability to monitor the process of a container.
 * 
 * @since 7.3.5
 */
public interface ProcessControlProvider {
    /**
     * Configure a container for process monitoring.  This could, for example, set a unique identifier.
     * 
     * @param container container to configure.
     */
    void configureContainer(Container container);

    /**
     * Returns the process of a container if it exists.
     * 
     * @param container the container to retrieve the process for.
     *                  
     * @return the container process, or null if it does not exist.
     * 
     * @throws IOException if an error occurs reading the container's process.
     */
    ContainerProcess containerProcess(Container container)
    throws IOException;

    /**
     * Generate a thread dump of the process to the specified writer.
     * 
     * @throws IOException if an I/O error occurs generating the thread dump or writing to <code>writer</code>.
     */
    void generateThreadDump(Container container, Writer writer)
    throws IOException;
}
