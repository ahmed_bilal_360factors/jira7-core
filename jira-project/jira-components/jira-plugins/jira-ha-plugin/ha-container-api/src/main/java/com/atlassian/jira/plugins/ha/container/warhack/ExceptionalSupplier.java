package com.atlassian.jira.plugins.ha.container.warhack;

/**
 * Supplier that can throw a checked exception.
 */
public interface ExceptionalSupplier<T, E extends Exception> {
    T get() throws E;
}