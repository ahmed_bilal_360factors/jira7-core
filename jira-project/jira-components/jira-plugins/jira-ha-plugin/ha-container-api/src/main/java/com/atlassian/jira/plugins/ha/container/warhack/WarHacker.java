package com.atlassian.jira.plugins.ha.container.warhack;

import com.atlassian.jira.upgrade.UpgradeTask;
import org.apache.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;
import java.util.jar.JarOutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Write modified JIRA WAR files for testing purposes.  Original WAR files are not modified.
 */
public class WarHacker {
    private static final Pattern LAST_NUMBER_PATTERN = Pattern.compile("(\\d+)(?!.*\\d)");

    private final Path warFile;
    private final List<WarOperation> operations = new ArrayList<>();

    /**
     * Creates a WAR Hacker with a base JIRA WAR file.
     *
     * @param warFile the JIRA WAR file to use as the base.
     */
    public WarHacker(Path warFile) {
        this.warFile = warFile;
    }

    public void addOperation(WarOperation operation) {
        operations.add(operation);
    }

    /**
     * Registers a change to modify the build number of JIRA.
     *
     * @param buildNumberTransform how to generate new build number from the old one.
     */
    public void overrideBuildNumber(Function<String, String> buildNumberTransform) {
        addOperation(new FilterBuildUtilsOperation(FilterBuildUtilsOperation.Field.BUILD_NUMBER, buildNumberTransform));
    }

    /**
     * Registers a change to modify the build number of JIRA.
     *
     * @param versionTransform how to generate new build number from the old one.
     */
    public void overrideVersion(Function<String, String> versionTransform) {
        addOperation(new FilterBuildUtilsOperation(FilterBuildUtilsOperation.Field.VERSION_STRING, versionTransform));
    }

    /**
     * Registers a change to increment JIRA's build number by one.
     */
    public void incrementBuildNumber() {
        overrideBuildNumber(bn -> String.valueOf(Integer.parseInt(bn) + 1));
    }

    /**
     * Registers a change to increment JIRA's version by one.
     */
    public void incrementVersionString() {
        overrideVersion(version -> {
            final Matcher matcher = LAST_NUMBER_PATTERN.matcher(version);
            if (!matcher.find()) {
                throw new IllegalArgumentException("no number found in version string");
            }

            final StringBuffer buf = new StringBuffer();

            matcher.appendReplacement(buf, String.valueOf(Integer.valueOf(matcher.group(1)) + 1));
            matcher.appendTail(buf);

            return buf.toString();
        });
    }

    /**
     * Exclude JAR files from WEB-INF/lib in the target WAR by name.
     *
     * @param fileNames the names of the files in WEB-INF/lib to exclude.  Do not include path.
     */
    public void excludeLibs(String... fileNames) {
        for (String fileName : fileNames) {
            addOperation(new ExcludeFileOperation("WEB-INF/lib/" + fileName));
        }
    }

    /**
     * Adds an upgrade task to the WAR and configures it with the next build number.
     *
     * @param upgradeTaskClass the upgrade task class.
     */
    public void addUpgradeTask(Class<? extends UpgradeTask> upgradeTaskClass) {
        addOperation(new AddUpgradeTaskOperation(upgradeTaskClass));
    }

    private void preprocessWar() throws IOException {
        //Pre-processing
        try (JarInputStream warIs = new JarInputStream(Files.newInputStream(warFile))) {
            JarEntry entry;
            do {
                entry = warIs.getNextJarEntry();
                if (entry != null) {
                    //Multiple operations might need the data so the first time it's asked for we read into
                    //ByteArrayInputStream and supply that stream to all the operations that need it
                    final AtomicReference<byte[]> dataHolder = new AtomicReference<>();
                    for (WarOperation operation : operations) {
                        operation.preprocessEntry(entry, () -> {
                            if (dataHolder.get() == null) {
                                dataHolder.set(IOUtils.toByteArray(warIs));
                            }
                            return new ByteArrayInputStream(dataHolder.get());
                        });
                    }
                }
            } while (entry != null);
        }
    }

    private void processWar(Path destinationFile) throws IOException {
        //Processing
        try (JarInputStream warIs = new JarInputStream(Files.newInputStream(warFile));
             JarOutputStream warOs = new JarOutputStream(Files.newOutputStream(destinationFile))) {

            JarEntry entry;
            do {
                entry = warIs.getNextJarEntry();

                if (entry != null) {
                    JarEntry outEntry = new JarEntry(entry.getName());
                    if (entry.getComment() != null) {
                        outEntry.setComment(entry.getComment());
                    }
                    if (entry.getCreationTime() != null) {
                        outEntry.setCreationTime(entry.getCreationTime());
                    }
                    if (entry.getLastAccessTime() != null) {
                        outEntry.setLastAccessTime(entry.getLastAccessTime());
                    }
                    if (entry.getLastModifiedTime() != null) {
                        outEntry.setLastModifiedTime(entry.getLastModifiedTime());
                    }

                    InputStream is = new UncloseableInputStream(warIs);
                    for (WarOperation operation : operations) {
                        if (is != null) {
                            is = operation.processEntry(entry, is);
                        }
                    }

                    if (is != null) {
                        warOs.putNextEntry(outEntry);
                        IOUtils.copy(is, warOs);
                        warOs.closeEntry();
                    }
                }
            } while (entry != null);

            //Any extra entries from operations
            for (WarOperation operation : operations) {
                for (FileEntry additionalFileEntry : operation.additionalFiles()) {
                    JarEntry outEntry = new JarEntry(additionalFileEntry.getName());
                    warOs.putNextEntry(outEntry);
                    try (InputStream is = additionalFileEntry.getInputStream()) {
                        IOUtils.copy(is, warOs);
                    }
                    warOs.closeEntry();
                }
            }
        }
    }

    /**
     * Write a new WAR file with all registered modifications.
     *
     * @param destinationFile the new WAR file to generate.
     *
     * @throws IOException if an I/O error occurs reading the original WAR or writing the modified WAR.
     */
    public void writeWarFile(Path destinationFile) throws IOException {
        preprocessWar();
        processWar(destinationFile);
    }

    private static class UncloseableInputStream extends FilterInputStream {
        public UncloseableInputStream(InputStream is) {
            super(is);
        }

        @Override
        public void close() throws IOException {
            //Do not close underlying stream
        }
    }
}
