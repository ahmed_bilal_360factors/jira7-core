package com.atlassian.jira.plugins.ha.container;

import org.codehaus.cargo.util.log.LogLevel;
import org.codehaus.cargo.util.log.SimpleLogger;

public class LabelledLogger extends SimpleLogger {

    private final String label;

    public LabelledLogger(String label) {
        this.label = label;
    }

    @Override
    protected void doLog(LogLevel level, String message, String category) {
        super.doLog(level, message, label);
    }
}
