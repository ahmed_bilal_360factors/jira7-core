package com.atlassian.jira.plugins.ha.container.warhack;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.jar.JarEntry;

/**
 * An operation does something to the WAR file by modifying or adding entries.
 */
public interface WarOperation {

    /**
     * Process a file entry in a WAR file.
     *
     * @param entry the entry in the WAR file.
     * @param is the input stream to the original data.
     *
     * @return either <code>is</code> to leave entry unmodified or a new input stream that filters original data.
     *          Return <code>null</code> to not write the entry into the destination WAR file.
     *
     * @throws IOException if an I/O error occurs.
     */
    InputStream processEntry(JarEntry entry, InputStream is)
    throws IOException;

    /**
     * Pre-processes a file entry in a WAR file.  All entries in a WAR are pre-processed before they are processed.
     * It allows other file contents to be read.
     *
     * @param entry the entry in the WAR file to read.
     * @param is provides access to the input stream of the entry.  Only consume if needed.
     *
     * @throws IOException if an I/O error occurs reading an entry.
     */
    void preprocessEntry(JarEntry entry, ExceptionalSupplier<? extends InputStream, IOException> is)
    throws IOException;

    /**
     * @return a list of additional files that should be added to the WAR byt his operation, or empty collection
     *          if nothing should be added.
     *
     * @throws IOException if an error occurs gathering additional files.
     */
    Collection<? extends FileEntry> additionalFiles()
    throws IOException;
}
