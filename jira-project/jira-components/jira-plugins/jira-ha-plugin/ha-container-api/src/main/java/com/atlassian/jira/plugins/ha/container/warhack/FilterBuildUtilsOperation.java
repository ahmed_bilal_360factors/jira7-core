package com.atlassian.jira.plugins.ha.container.warhack;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtField;
import javassist.CtMethod;
import javassist.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Collections;
import java.util.function.Function;
import java.util.jar.JarEntry;

/**
 * Modifies the <code>BuildUtils</code> class of JIRA, allowing JIRA's build number to be overridden.
 */
public class FilterBuildUtilsOperation implements WarOperation {

    private static final Logger log = LoggerFactory.getLogger(FilterBuildUtilsOperation.class);

    public enum Field {
        BUILD_NUMBER("BUILD_NUMBER", "getCurrentBuildNumber"),
        VERSION_STRING("VERSION", "getVersion");

        final String fieldName;
        final String getterMethod;

        Field(final String fieldName, final String getterMethod) {
            this.fieldName = fieldName;
            this.getterMethod = getterMethod;
        }
    }

    private final Function<String, String> valueTransformer;
    private final Field field;

    public FilterBuildUtilsOperation(final Field field, Function<String, String> valueTransformer) {
        this.valueTransformer = valueTransformer;
        this.field = field;
    }

    @Override
    public InputStream processEntry(JarEntry entry, InputStream is) throws IOException {
        if (entry.getName().equals("WEB-INF/classes/com/atlassian/jira/util/BuildUtils.class")) {
            return processBuildUtilsClass(is);
        } else {
            return is;
        }
    }

    private InputStream processBuildUtilsClass(InputStream buildUtilsClassStream) throws IOException {
        ClassPool pool = new ClassPool(true);
        CtClass buildUtilsClass = pool.makeClass(buildUtilsClassStream);

        try {
            modifyBuildUtilsClass(buildUtilsClass, valueTransformer);
            return new ByteArrayInputStream(buildUtilsClass.toBytecode());
        } catch (CannotCompileException | NotFoundException e) {
            throw new IOException("Failed to build new BuildUtils class: " + e, e);
        }
    }

    private void modifyBuildUtilsClass(CtClass buildUtilsClass, final Function<String, String> valueTransformer) throws CannotCompileException, NotFoundException {
        final String fieldName = this.field.fieldName;
        final String getterMethod = this.field.getterMethod;

        //Hack the constant
        CtField field = buildUtilsClass.getDeclaredField(fieldName);
        String existingValue = field.getConstantValue().toString();
        String newValue = valueTransformer.apply(existingValue);
        log.info("Replacing " + fieldName + " '" + existingValue + "' with '" + newValue + "' in BuildUtils.");
        buildUtilsClass.removeField(field);
        buildUtilsClass.addField(field, CtField.Initializer.constant(newValue));

        //Hack getCurrentBuildNumber() - constants are expanded at compile time so this needs to be modified as well
        CtMethod getterMethodObject = buildUtilsClass.getDeclaredMethod(getterMethod);
        getterMethodObject.setBody("return " + fieldName + ";");
    }

    @Override
    public void preprocessEntry(JarEntry entry, ExceptionalSupplier<? extends InputStream, IOException> is) throws IOException {
        //Nothing needed
    }

    @Override
    public Collection<? extends FileEntry> additionalFiles() throws IOException {
        return Collections.emptyList();
    }
}
