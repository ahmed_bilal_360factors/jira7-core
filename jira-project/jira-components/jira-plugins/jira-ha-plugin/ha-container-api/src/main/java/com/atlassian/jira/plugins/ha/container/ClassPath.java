package com.atlassian.jira.plugins.ha.container;

import com.google.common.collect.ImmutableList;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ClassPath {

    private final List<? extends Path> jars;

    public ClassPath(List<? extends Path> jars) {
        this.jars = ImmutableList.copyOf(jars);
    }

    public ClassPath(Path... jars) {
        this(Arrays.asList(jars));
    }

    public static ClassPath fromDirectoryOfJars(Path directory) throws IOException {
        return new ClassPath(Files.list(directory)
                                  .filter(file -> file.getFileName().toString().endsWith(".jar"))
                                  .collect(Collectors.toList()));
    }

    public List<? extends Path> getJars() {
        return jars;
    }

    @Override
    public String toString() {
        return jars.toString();
    }
}
