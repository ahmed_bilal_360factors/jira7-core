UPDATE propertystring set propertyvalue = '73000' where id = (select id from propertyentry where property_key = 'jira.version.patched');
if object_id('clusterupgradestate', 'U') is not null drop table clusterupgradestate;
if object_id('clusternode', 'U') is not null drop table clusternode;
if object_id('clusternodeheartbeat', 'U') is not null drop table clusternodeheartbeat;
if object_id('clustermessage', 'U') is not null drop table clustermessage;
if object_id('upgradetaskhistory', 'U') is not null drop table upgradetaskhistory;
