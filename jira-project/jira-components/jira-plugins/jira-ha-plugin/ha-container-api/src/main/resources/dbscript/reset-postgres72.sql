UPDATE propertystring set propertyvalue = '73000' where id = (select id from propertyentry where property_key = 'jira.version.patched');
drop table if exists clusterupgradestate;
drop table if exists clusternode;
drop table if exists clusternodeheartbeat;
drop table if exists clustermessage;
drop table if exists upgradetaskhistory;
