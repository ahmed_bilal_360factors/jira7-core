package com.atlassian.jira.plugins.ha.container.warhack;

import com.google.common.collect.ImmutableList;
import org.apache.commons.io.IOUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.contains;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.any;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

@RunWith(MockitoJUnitRunner.class)
public class TestWarHacker {

    @Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();

    @Mock
    private WarOperation operation;

    private void generateSimpleWarFile(Path warFile) throws IOException {
        try (JarOutputStream os = new JarOutputStream(Files.newOutputStream(warFile))) {
            os.putNextEntry(new JarEntry("one.txt"));
            os.write("This is the first file".getBytes(StandardCharsets.UTF_8));
            os.putNextEntry(new JarEntry("two.txt"));
            os.write("This is the second file".getBytes(StandardCharsets.UTF_8));
        }
    }

    @Test
    public void testNoModifyingOperations() throws Exception {

        Path tempWarFile = tempFolder.newFile().toPath();
        Path outFile = tempFolder.newFile().toPath();

        generateSimpleWarFile(tempWarFile);

        when(operation.processEntry(any(), any())).thenAnswer(inv -> inv.getArguments()[1]);

        WarHacker wh = new WarHacker(tempWarFile);
        wh.addOperation(operation);
        wh.writeWarFile(outFile);

        //Validate WAR is written
        try (JarFile w = new JarFile(outFile.toFile())) {
            List<JarEntry> entries = w.stream().collect(Collectors.toList());
            assertThat(entries, hasSize(2));

            String content1 = IOUtils.toString(w.getInputStream(w.getJarEntry("one.txt")), "UTF-8");
            assertThat(content1, is("This is the first file"));
            String content2 = IOUtils.toString(w.getInputStream(w.getJarEntry("two.txt")), "UTF-8");
            assertThat(content2, is("This is the second file"));
        }
    }

    @Test
    public void operationCanModifyEntryContent() throws Exception {

        Path tempWarFile = tempFolder.newFile().toPath();
        Path outFile = tempFolder.newFile().toPath();

        generateSimpleWarFile(tempWarFile);

        when(operation.processEntry(any(), any())).thenAnswer(inv -> generateNewFileContent((JarEntry)inv.getArguments()[0], (InputStream)inv.getArguments()[1]));

        WarHacker wh = new WarHacker(tempWarFile);
        wh.addOperation(operation);
        wh.writeWarFile(outFile);

        //Validate WAR is written
        try (JarFile w = new JarFile(outFile.toFile())) {
            List<JarEntry> entries = w.stream().collect(Collectors.toList());
            assertThat(entries, hasSize(2));

            String content1 = IOUtils.toString(w.getInputStream(w.getJarEntry("one.txt")), "UTF-8");
            assertThat(content1, is("[one.txt modified] This is the first file"));
            String content2 = IOUtils.toString(w.getInputStream(w.getJarEntry("two.txt")), "UTF-8");
            assertThat(content2, is("[two.txt modified] This is the second file"));
        }
    }

    @Test
    public void operationCanAddEntries() throws Exception {

        Path tempWarFile = tempFolder.newFile().toPath();
        Path outFile = tempFolder.newFile().toPath();

        generateSimpleWarFile(tempWarFile);

        when(operation.processEntry(any(), any())).thenAnswer(inv -> inv.getArguments()[1]);
        Collection<FileEntry> fileEntries = ImmutableList.of(
                new SimpleFileEntry("three.txt", () -> new ByteArrayInputStream("Third file".getBytes(StandardCharsets.UTF_8))),
                new SimpleFileEntry("four.txt", () -> new ByteArrayInputStream("Fourth file".getBytes(StandardCharsets.UTF_8))));
        doReturn(fileEntries).when(operation).additionalFiles();

        WarHacker wh = new WarHacker(tempWarFile);
        wh.addOperation(operation);
        wh.writeWarFile(outFile);

        //Validate WAR is written
        try (JarFile w = new JarFile(outFile.toFile())) {
            List<JarEntry> entries = w.stream().collect(Collectors.toList());
            assertThat(entries, hasSize(4));

            String content1 = IOUtils.toString(w.getInputStream(w.getJarEntry("one.txt")), "UTF-8");
            assertThat(content1, is("This is the first file"));
            String content2 = IOUtils.toString(w.getInputStream(w.getJarEntry("two.txt")), "UTF-8");
            assertThat(content2, is("This is the second file"));
            String content3 = IOUtils.toString(w.getInputStream(w.getJarEntry("three.txt")), "UTF-8");
            assertThat(content3, is("Third file"));
            String content4 = IOUtils.toString(w.getInputStream(w.getJarEntry("four.txt")), "UTF-8");
            assertThat(content4, is("Fourth file"));
        }
    }

    @Test
    public void operationPreprocessSeesFiles() throws Exception {

        Path tempWarFile = tempFolder.newFile().toPath();
        Path outFile = tempFolder.newFile().toPath();

        generateSimpleWarFile(tempWarFile);

        Map<String, String> preprocessorFileContentMap = new LinkedHashMap<>();
        Mockito.doAnswer(inv -> {
            readFileIntoMap(preprocessorFileContentMap, (JarEntry)inv.getArguments()[0], (ExceptionalSupplier<InputStream, IOException>)inv.getArguments()[1]);
            return null; })
                .when(operation).preprocessEntry(any(), any());
        when(operation.processEntry(any(), any())).thenAnswer(inv -> inv.getArguments()[1]);

        WarHacker wh = new WarHacker(tempWarFile);
        wh.addOperation(operation);
        wh.writeWarFile(outFile);

        assertThat(preprocessorFileContentMap.keySet(), contains("one.txt", "two.txt"));
        assertThat(preprocessorFileContentMap.get("one.txt"), is("This is the first file"));
        assertThat(preprocessorFileContentMap.get("two.txt"), is("This is the second file"));
    }

    private void readFileIntoMap(Map<? super String, ? super String> map, JarEntry entry,
                                                       ExceptionalSupplier<? extends InputStream, ? extends IOException> streamSupplier)
    throws IOException {
        map.put(entry.getName(), IOUtils.toString(streamSupplier.get(), "UTF-8"));
    }

    private InputStream generateNewFileContent(JarEntry entry, InputStream originalContent) throws IOException {
        String originalString = IOUtils.toString(originalContent, "UTF-8");
        String newString = "[" + entry.getName() + " modified] " + originalString;
        return new ByteArrayInputStream(newString.getBytes(StandardCharsets.UTF_8));
    }
}
