package com.atlassian.jira.plugins.ha.container.warhack;

import com.atlassian.jira.util.BuildUtils;
import com.atlassian.jira.util.IOUtil;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.jar.JarEntry;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class TestFilterBuildUtilsOperation {

    @Test
    public void nonBuildUtilsClassEntriesAreUntouched() throws Exception {
        FilterBuildUtilsOperation op = new FilterBuildUtilsOperation(FilterBuildUtilsOperation.Field.BUILD_NUMBER, buildNumber -> {
            //Verify that the operation sees the existing build number from the 'old' class file
            fail("Should not be able to perform operation on non-BuildUtils class file entry.");
            return null;
        });

        try (InputStream originalIs = new ByteArrayInputStream(new byte[10])) {
            InputStream is = op.processEntry(new JarEntry("galahfile.txt"), originalIs);
            assertThat("Input stream should not be modified.", originalIs, sameInstance(is));
        }
    }

    /**
     * Build a new class file from the existing one, load it and verify it works and has a modified build number.
     */
    private void verifyBuildUtilsClassModified(final FilterBuildUtilsOperation.Field fieldType, final String existingValue) throws Exception {

        //Read existing build number from test classpath's version of BuildUtils
        String newValue = "galah-build";

        //Build a new BuildUtils class file and store into byte array
        byte[] buildUtilsClassData;
        try (InputStream is = BuildUtils.class.getResource("BuildUtils.class").openStream()) {
            FilterBuildUtilsOperation op = new FilterBuildUtilsOperation(fieldType, value -> {
                //Verify that the operation sees the existing build number from the 'old' class file
                assertThat(value, Matchers.is(existingValue));
                return "galah-build";
            });
            JarEntry entry = new JarEntry("WEB-INF/classes/com/atlassian/jira/util/BuildUtils.class");
            try (InputStream changeIs = op.processEntry(entry, is)) {
                buildUtilsClassData = IOUtil.toByteArray(changeIs);
            }
        }

        //Load the new classfile in our custom classloader
        String buildUtilsClassName = BuildUtils.class.getName();
        ClassLoader loader = new ClassLoader(TestFilterBuildUtilsOperation.class.getClassLoader()) {
            @Override
            protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
                if (buildUtilsClassName.equals(name)) {
                    return defineClass(name, buildUtilsClassData, 0, buildUtilsClassData.length);
                }
                return super.loadClass(name, resolve);
            }
        };

        //Use reflection to query our new modified class file and verify build number is changed by the operation
        Class<?> buildUtilsClass = Class.forName(BuildUtils.class.getName(), true, loader);
        String buildNumber = (String)buildUtilsClass.getMethod(fieldType.getterMethod).invoke(null);

        assertThat(buildNumber, is(newValue));
    }

    @Test
    public void buildUtilsClassBuildNumberModifiedCorrectly() throws Exception {
        verifyBuildUtilsClassModified(FilterBuildUtilsOperation.Field.BUILD_NUMBER, BuildUtils.getCurrentBuildNumber());
    }

    @Test
    public void buildUtilsClassVersionStringModifiedCorrectly() throws Exception {
        verifyBuildUtilsClassModified(FilterBuildUtilsOperation.Field.VERSION_STRING, BuildUtils.getVersion());
    }
}
