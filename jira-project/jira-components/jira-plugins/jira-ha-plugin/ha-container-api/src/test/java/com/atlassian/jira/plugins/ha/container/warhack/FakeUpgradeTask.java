package com.atlassian.jira.plugins.ha.container.warhack;

import com.atlassian.jira.upgrade.UpgradeTask;

/**
 * Not a real upgrade task, just used for testing upgrade task operations.
 */
public abstract class FakeUpgradeTask implements UpgradeTask {
}
