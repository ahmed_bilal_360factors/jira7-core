package com.atlassian.jira.plugins.ha.container.warhack;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.jar.JarEntry;

import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertThat;
import static org.xmlunit.matchers.CompareMatcher.isIdenticalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

public class TestAddUpgradeTaskOperation {

    @Test
    public void testUpgradeXmlGeneration() throws Exception {
        AddUpgradeTaskOperation op = new AddUpgradeTaskOperation(FakeUpgradeTask.class);

        JarEntry entry = new JarEntry("WEB-INF/classes/upgrades.xml");
        String upgradesXml =
                "<upgrades>" +
                "  <upgrade build='73000'>" +
                "    <class>com.atlassian.jira.upgrades.UpgradeTask73000</class>" +
                "  </upgrade>" +
                "  <upgrade build='73001'>" +
                "    <class>com.atlassian.jira.upgrades.UpgradeTask73001</class>" +
                "  </upgrade>" +
                "</upgrades>";

        try (InputStream resultIs = op.processEntry(entry, new ByteArrayInputStream(upgradesXml.getBytes(StandardCharsets.UTF_8)))) {
            String expected =
                    "<upgrades>" +
                    "  <upgrade build='73000'>" +
                    "    <class>com.atlassian.jira.upgrades.UpgradeTask73000</class>" +
                    "  </upgrade>" +
                    "  <upgrade build='73001'>" +
                    "    <class>com.atlassian.jira.upgrades.UpgradeTask73001</class>" +
                    "  </upgrade>" +
                    "  <upgrade build='73002'>" +
                    "    <class>com.atlassian.jira.plugins.ha.container.warhack.FakeUpgradeTask</class>" +
                    "  </upgrade>" +
                    "</upgrades>";
            assertThat(resultIs, isIdenticalTo(expected).throwComparisonFailure().ignoreComments().ignoreWhitespace());
        }
    }

    @Test
    public void upgradeClassFileIsAdded() throws Exception {
        AddUpgradeTaskOperation op = new AddUpgradeTaskOperation(FakeUpgradeTask.class);
        Collection<? extends FileEntry> additionalFiles = op.additionalFiles();

        assertThat(additionalFiles, hasSize(1));

        FileEntry additionalFile = additionalFiles.iterator().next();
        assertThat(additionalFile.getName(), is("WEB-INF/classes/com/atlassian/jira/plugins/ha/container/warhack/FakeUpgradeTask.class"));

        byte[] classData = IOUtils.toByteArray(additionalFile.getInputStream());
        assertThat("Class file in WAR should have a non-zero size", classData.length, greaterThan(0));
    }
}
