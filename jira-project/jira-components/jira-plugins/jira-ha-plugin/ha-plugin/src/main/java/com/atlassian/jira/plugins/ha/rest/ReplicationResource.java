package com.atlassian.jira.plugins.ha.rest;

import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.cluster.Node;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.ofbiz.core.entity.EntityCondition;
import org.ofbiz.core.entity.EntityExpr;
import org.ofbiz.core.entity.EntityFieldMap;
import org.ofbiz.core.entity.EntityOperator;
import org.ofbiz.core.entity.GenericValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.ofbiz.core.entity.EntityOperator.EQUALS;

/**
 * The web resource for replication management.
 *
 * @since 6.3
 */
@Path("/replication")
public class ReplicationResource {
    private static final Logger log = LoggerFactory.getLogger(ReplicationResource.class);

    private static final long POLL_TIME_MS = 200;

    private final ClusterManager clusterManager;
    private final OfBizDelegator ofBizDelegator;

    public ReplicationResource(@ComponentImport final ClusterManager clusterManager, @ComponentImport final OfBizDelegator ofBizDelegator) {
        this.clusterManager = requireNonNull(clusterManager);
        this.ofBizDelegator = requireNonNull(ofBizDelegator);
    }

    /**
     * Waits for all replication operations between nodes in the cluster to complete.
     *
     * @param maxTimeToWaitMs the maximum amount of time to wait in milliseconds.  If nodes are not replicated
     *                        after this time an error is produced.
     * @return the actual amount of time waited.
     */
    @GET
    @Path("waitForAllReplicationOperations")
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    public String waitForAllReplicationOperations(@QueryParam("maxTimeToWaitMs") final long maxTimeToWaitMs) {
        final long startTime = System.currentTimeMillis();

        try {
            while (System.currentTimeMillis() - startTime < maxTimeToWaitMs) {
                final String pendingOperations = getPendingOperationCount();
                if (pendingOperations.equals("0")) {
                    return (String.valueOf(System.currentTimeMillis() - startTime));
                }

                Thread.sleep(POLL_TIME_MS);
            }
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted polling.", e);
        }

        throw new RuntimeException("Timeout waiting for replication.");
    }

    /**
     * Waits until the specified number of nodes become active in the cluster.
     *
     * @param nodeCount       the number of nodes the cluster must have.
     * @param maxTimeToWaitMs the maximum amount of time to wait in milliseconds.  An error is returned if
     *                        the required number of nodes are not active after this time.
     * @return the amount of time waited.
     */
    @GET
    @Path("waitForClusterNodes")
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    public String waitForClusterNodes(@QueryParam("nodeCount") int nodeCount,
                                      @QueryParam("maxTimeToWaitMs") long maxTimeToWaitMs) {
        final long startTime = System.currentTimeMillis();

        try {
            while (System.currentTimeMillis() - startTime < maxTimeToWaitMs) {
                int numActiveNodes = clusterManager.findLiveNodes().size();
                if (numActiveNodes >= nodeCount) {
                    return (String.valueOf(System.currentTimeMillis() - startTime));
                }
                Thread.sleep(POLL_TIME_MS);
            }
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted polling.", e);
        }

        throw new RuntimeException("Timeout waiting for nodes.");
    }

    /**
     * Returns the number of pending replication operations.
     *
     * @return the number of pending replication operations.
     */
    @GET
    @Path("pendingOperationCount")
    @Produces(APPLICATION_JSON)
    public String getPendingOperationCount() {
        // Map of node id to max index operation ID on that node
        final Map<String, String> nodeMaxIds = new HashMap<>();

        final Collection<String> liveNodeIds =
                clusterManager.findLiveNodes().stream().map(Node::getNodeId).collect(toList());

        for (final String nodeId : liveNodeIds) {
            getMaxIndexOperationId(nodeId).ifPresent(maxOpId -> nodeMaxIds.put(nodeId, maxOpId));
        }

        final Set<String> nodesThatAreBehind = new HashSet<>();

        // Check every node is up to date with every other node
        for (final String receivingNodeId : liveNodeIds) {
            // Compare this node with all other nodes
            final List<String> otherNodeIds = liveNodeIds.stream().filter(nodeId -> !nodeId.equals(receivingNodeId)).collect(toList());
            for (final String sendingNodeId : otherNodeIds) {
                // Find the NodeIndexCounter row for this pair of nodes
                final String expectedMaxId = nodeMaxIds.get(sendingNodeId);
                if (expectedMaxId != null) {
                    final Optional<String> indexOperationId = getIndexOperationId(receivingNodeId, sendingNodeId);
                    if (indexOperationId.isPresent()) {
                        final boolean behind = !expectedMaxId.equals(indexOperationId.get());
                        log.info("Replication check " + receivingNodeId + " from " + sendingNodeId + ": " + (behind ? "behind" : "ok"));
                        if (behind) {
                            nodesThatAreBehind.add(receivingNodeId);
                        }
                    } else {
                        // No replication has ever taken place from sending node to receiving node, and the sending node does have index operations.
                        // The receiving node is behind
                        log.info("Replication check " + receivingNodeId + " from " + sendingNodeId + ": behind (no replications yet)");
                        nodesThatAreBehind.add(receivingNodeId);
                    }
                } else {
                    log.info("Replication check " + receivingNodeId + " from " + sendingNodeId + ": ok (sending node has no index operations)");
                }
            }
        }

        return String.valueOf(nodesThatAreBehind.size());
    }

    /**
     * Get the row of the NodeIndexCounter table that records the latest index operations between a sending
     * and receiving node. May return null if no index operations have been replicated between these nodes.
     *
     * @param receivingNodeId the receiving node ID
     * @param sendingNodeId   the sending node ID
     * @return the row
     */
    private Optional<String> getIndexOperationId(final String receivingNodeId, final String sendingNodeId) {
        final ImmutableList<EntityCondition> entityConditions = ImmutableList.of(
                new EntityExpr("nodeId", EQUALS, receivingNodeId),
                new EntityExpr("sendingNodeId", EQUALS, sendingNodeId));
        final List<GenericValue> gvs = ofBizDelegator.findByAnd("NodeIndexCounter", entityConditions);
        return gvs.stream().findFirst().map(gv -> gv.getString("indexOperationId"));
    }

    /**
     * Get the maximum index operation ID for the given node.
     *
     * @param nodeId the node ID
     * @return the index operation id, if any
     */
    private Optional<String> getMaxIndexOperationId(final String nodeId) {
        final EntityFieldMap condition = new EntityFieldMap(ImmutableMap.of("nodeId", nodeId), EntityOperator.EQUALS);
        final List<GenericValue> results = ofBizDelegator.findByCondition(
                "ReplicatedIndexOperation", condition, ImmutableSet.of("id"), ImmutableList.of("id desc"));
        return results.stream().findFirst().map(gv -> gv.getString("id"));
    }
}
