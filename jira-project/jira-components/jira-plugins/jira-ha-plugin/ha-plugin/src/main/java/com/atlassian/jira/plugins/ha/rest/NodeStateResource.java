package com.atlassian.jira.plugins.ha.rest;

import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.cluster.NodeStateService;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import static com.atlassian.jira.util.ErrorCollection.Reason;
import static com.atlassian.jira.util.ErrorCollection.Reason.getWorstReason;
import static java.util.Objects.requireNonNull;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.ok;
import static javax.ws.rs.core.Response.status;

/**
 * The web resource for the current node's state within the JIRA cluster.
 *
 * @since 6.1
 */
@Path("/nodes")
public class NodeStateResource {
    // Constants
    private static final Logger LOGGER = LoggerFactory.getLogger(NodeStateResource.class);

    // Fields
    private final JiraAuthenticationContext authenticationContext;
    private final NodeStateService nodeStateService;

    public NodeStateResource(@ComponentImport final JiraAuthenticationContext authenticationContext,
                             @ComponentImport final NodeStateService nodeStateService) {
        this.authenticationContext = requireNonNull(authenticationContext);
        this.nodeStateService = requireNonNull(nodeStateService);
    }

    /**
     * Gets the state of this JIRA node.
     *
     * request.representation.mediaType application/json
     *
     * request.representation.doc
     * A flag indicating whether this JIRA instance should be active.
     *
     * request.representation.example
     * true
     *
     * response.representation.400.doc
     * Returned if the specified base URL is not valid.
     *
     * @return the desired state of this instance
     */
    @GET
    @Path("current")
    @Produces(APPLICATION_JSON)
    public boolean getState() {
        return getState(authenticationContext.getLoggedInUser());
    }

    /**
     * Sets the state of this JIRA node.
     *
     * request.representation.mediaType application/json
     *
     * request.representation.doc
     * A flag indicating whether this JIRA instance should be active.
     *
     * request.representation.example
     * true
     *
     * response.representation.400.doc
     * Returned if the specified base URL is not valid.
     *
     * @param active the desired state of this instance
     * @return ok if state successfully set, else contains error collection
     */
    @PUT
    @Path("current")
    @Produces(APPLICATION_JSON)
    public Response setState(final boolean active) {
        final ServiceResult serviceResult = setState(active, authenticationContext.getLoggedInUser());
        if (serviceResult.isValid()) {
            return ok().build();
        }
        final ErrorCollection errorCollection = serviceResult.getErrorCollection();
        final Reason reason = getWorstReason(errorCollection.getReasons());
        return status(reason.getHttpStatusCode()).entity(errorCollection.getErrorMessages()).build();
    }

    private ServiceResult setState(final boolean active, final ApplicationUser user) {
        LOGGER.debug("User {} setting current JIRA node to active = {}", user, active);
        if (active) {
            return nodeStateService.activate(user);
        }
        return nodeStateService.deactivate(user);
    }

    private boolean getState(final ApplicationUser user) {
        LOGGER.debug("User {} getting the current JIRA node state", user);
        return nodeStateService.isActive();
    }
}
