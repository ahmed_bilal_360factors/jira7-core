package com.atlassian.jira.plugins.ha.rest;

import com.atlassian.jira.cluster.NodeStateService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.JiraSystemProperties;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static javax.servlet.http.HttpServletResponse.SC_SERVICE_UNAVAILABLE;

/**
 * Rejects normal (non-HA-related) requests to a JIRA node that is passive within a cluster.
 *
 * @since 6.1
 */
public class PassiveNodeFilter implements Filter {
    // Constants
    private static final Logger LOGGER = LoggerFactory.getLogger(PassiveNodeFilter.class);

    static final String ERROR_CONTENT_TYPE = "text/plain";
    static final String HA_REST_PATH_PREFIX = "/rest/ha/";
    static final String PASSIVE_NODE_ERROR = "This JIRA node is passive and does not handle normal user requests.";
    static final String RESPONSE_ENCODING = "UTF-8";

    // Fields
    private NodeStateService nodeStateService;

    // Only used for testing
    private boolean disabledForTesting;

    /**
     * Constructor that expects the {@link NodeStateService} to be obtained statically at request time.
     */
    @SuppressWarnings("unused")
    public PassiveNodeFilter() {
        this(null);
    }

    /**
     * Constructor that allows the cluster manager to be injected.
     *
     * @param nodeStateService the cluster manager to set
     */
    @VisibleForTesting
    PassiveNodeFilter(@ComponentImport @Nullable final NodeStateService nodeStateService) {
        this.nodeStateService = nodeStateService;
    }

    @Override
    public void init(final FilterConfig filterConfig) {
        disabledForTesting = JiraSystemProperties.getInstance().getBoolean("jira.ha.webfilter.disabled");
    }

    @Override
    public void destroy() {
    }

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
            throws IOException, ServletException {
        if (isAllowed((HttpServletRequest) request)) {
            LOGGER.debug("Accepting request");
            chain.doFilter(request, response);
        } else {
            LOGGER.debug("Rejecting request");
            rejectRequest((HttpServletResponse) response);
        }
    }

    private boolean isAllowed(final HttpServletRequest request) {
        return getNodeStateService().isActive() || request.getServletPath().startsWith(HA_REST_PATH_PREFIX) || disabledForTesting;
    }

    private void rejectRequest(final HttpServletResponse response) throws IOException {
        response.setCharacterEncoding(RESPONSE_ENCODING);
        response.setContentType(ERROR_CONTENT_TYPE);
        response.setStatus(SC_SERVICE_UNAVAILABLE);
        response.getWriter().print(PASSIVE_NODE_ERROR);
    }

    private NodeStateService getNodeStateService() {
        if (nodeStateService == null) {
            this.nodeStateService = ComponentAccessor.getComponent(NodeStateService.class);
        }
        return nodeStateService;
    }
}
