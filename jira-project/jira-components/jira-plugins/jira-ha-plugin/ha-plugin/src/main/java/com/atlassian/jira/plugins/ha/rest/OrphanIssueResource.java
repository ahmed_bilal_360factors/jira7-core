package com.atlassian.jira.plugins.ha.rest;

import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.UpdateIssueRequest;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.ofbiz.core.entity.GenericValue;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import static java.util.Objects.requireNonNull;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 * The web resource for adding issues without a project.  Only used for testing error conditions.
 *
 * @since 6.3
 */
@Path("/orphanIssue")
public class OrphanIssueResource {

    private final IssueManager issueManager;
    private final JiraAuthenticationContext authenticationContext;
    private final OfBizDelegator ofBizDelegator;

    public OrphanIssueResource(@ComponentImport final IssueManager issueManager,
                               @ComponentImport final JiraAuthenticationContext authenticationContext,
                               @ComponentImport final OfBizDelegator ofBizDelegator) {
        this.authenticationContext = requireNonNull(authenticationContext);
        this.issueManager = requireNonNull(issueManager);
        this.ofBizDelegator = requireNonNull(ofBizDelegator);
    }

    /**
     * Creates an orphan issue in the database.
     *
     * @param issueId              id of the orphan issue
     * @param nonExistingProjectId id of project
     * @return the ID of the created issue
     */
    @GET
    @Path("create")
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    public String createOrphanIssue(@QueryParam("issueId") final Long issueId,
                                    @QueryParam("nonExistingProjectId") final Long nonExistingProjectId) {
        final GenericValue gv = issueManager.getIssue(issueId);
        gv.set("project", nonExistingProjectId);
        ofBizDelegator.store(gv);

        // Now we force an update of the issue so that indexes for this issue are rebuilt
        final MutableIssue mi = issueManager.getIssueObject(issueId);
        final ApplicationUser user = authenticationContext.getLoggedInUser();
        issueManager.updateIssue(user, mi, UpdateIssueRequest.builder().build());

        return issueId.toString();
    }

    /**
     * Deletes an orphan issue directly.
     *
     * @param issueId the ID of the issue to delete.
     * @return the ID of the deleted issue.
     */
    @GET
    @Path("delete")
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    public String deleteOrphanIssue(@QueryParam("issueId") Long issueId) {
        final GenericValue issueGV = issueManager.getIssue(issueId);
        if (issueGV != null) {
            ofBizDelegator.removeValue(issueGV);
        }
        return String.valueOf(issueId);
    }
}
