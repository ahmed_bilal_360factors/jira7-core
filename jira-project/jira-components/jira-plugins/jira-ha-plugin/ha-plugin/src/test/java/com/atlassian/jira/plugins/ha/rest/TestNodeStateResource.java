package com.atlassian.jira.plugins.ha.rest;

import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.cluster.NodeStateService;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import static com.atlassian.jira.util.ErrorCollection.Reason.FORBIDDEN;
import static java.util.Collections.singleton;
import static javax.ws.rs.core.Response.Status.OK;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

/**
 * Unit test of NodeStateResource.
 */
public class TestNodeStateResource {
    // Constants
    private static final String FORBIDDEN_MESSAGE = "Not allowed!";

    private static void assertForbidden(final Response response) {
        assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
        assertEquals(singleton(FORBIDDEN_MESSAGE), response.getEntity());
        final MultivaluedMap<String, Object> responseMetadata = response.getMetadata();
        assertNotNull(responseMetadata);
    }

    private static void assertOk(final Response response) {
        assertEquals(OK.getStatusCode(), response.getStatus());
        assertNull(response.getEntity());
    }

    // Fixture
    @InjectMocks
    private NodeStateResource nodeStateResource;

    @Mock
    private ApplicationUser mockUser;
    @Mock
    private ErrorCollection mockErrorCollection;
    @Mock
    private JiraAuthenticationContext mockJiraAuthenticationContext;
    @Mock
    private NodeStateService mockNodeStateService;
    @Mock
    private ServiceResult mockServiceResult;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        when(mockJiraAuthenticationContext.getLoggedInUser()).thenReturn(mockUser);
        when(mockServiceResult.getErrorCollection()).thenReturn(mockErrorCollection);
    }

    @Test
    public void resourceShouldDelegateToServiceUponValidActivateRequest() {
        // Set up
        when(mockServiceResult.isValid()).thenReturn(true);
        when(mockNodeStateService.activate(mockUser)).thenReturn(mockServiceResult);

        // Invoke and check
        assertOk(nodeStateResource.setState(true));
    }

    @Test
    public void resourceShouldDelegateToServiceUponValidDeactivateRequest() {
        // Set up
        when(mockServiceResult.isValid()).thenReturn(true);
        when(mockNodeStateService.deactivate(mockUser)).thenReturn(mockServiceResult);

        // Invoke and check
        assertOk(nodeStateResource.setState(false));
    }

    @Test
    public void resourceShouldForbidActivationWhenUserIsNotAuthorised() {
        // Set up
        setUpUnauthorisedAction();
        when(mockNodeStateService.activate(mockUser)).thenReturn(mockServiceResult);

        // Invoke and check
        assertForbidden(nodeStateResource.setState(true));
    }

    @Test
    public void resourceShouldForbidDeactivationWhenUserIsNotAuthorised() {
        // Set up
        setUpUnauthorisedAction();
        when(mockNodeStateService.deactivate(mockUser)).thenReturn(mockServiceResult);

        // Invoke and check
        assertForbidden(nodeStateResource.setState(false));
    }

    private void setUpUnauthorisedAction() {
        when(mockServiceResult.isValid()).thenReturn(false);
        when(mockErrorCollection.getReasons()).thenReturn(singleton(FORBIDDEN));
        when(mockErrorCollection.getErrorMessages()).thenReturn(singleton(FORBIDDEN_MESSAGE));
    }
}
