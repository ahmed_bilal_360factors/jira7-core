package com.atlassian.jira.plugins.ha.rest;

import com.atlassian.jira.cluster.NodeStateService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.PrintWriter;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.atlassian.jira.plugins.ha.rest.PassiveNodeFilter.ERROR_CONTENT_TYPE;
import static com.atlassian.jira.plugins.ha.rest.PassiveNodeFilter.HA_REST_PATH_PREFIX;
import static com.atlassian.jira.plugins.ha.rest.PassiveNodeFilter.PASSIVE_NODE_ERROR;
import static com.atlassian.jira.plugins.ha.rest.PassiveNodeFilter.RESPONSE_ENCODING;
import static javax.servlet.http.HttpServletResponse.SC_SERVICE_UNAVAILABLE;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * Unit test of {@link PassiveNodeFilter}.
 */
public class TestPassiveNodeFilter {
    // Constants
    private static final String NORMAL_REQUEST_SERVLET_PATH = "/browse/JRADEV-21588";
    private static final String HA_REQUEST_SERVLET_PATH = HA_REST_PATH_PREFIX + "foo/bar";

    // Fixture
    private Filter filter;
    @Mock
    private NodeStateService mockNodeStateService;
    @Mock
    private FilterChain mockFilterChain;
    @Mock
    private FilterConfig mockFilterConfig;
    @Mock
    private HttpServletRequest mockRequest;
    @Mock
    private HttpServletResponse mockResponse;
    @Mock
    private PrintWriter mockResponseWriter;
    @Mock
    private ServletContext mockServletContext;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        when(mockResponse.getWriter()).thenReturn(mockResponseWriter);
        this.filter = new PassiveNodeFilter(mockNodeStateService);
        when(mockFilterConfig.getServletContext()).thenReturn(mockServletContext);
        filter.init(mockFilterConfig);
    }

    @Test
    public void filterShouldAllowHaRestRequestsEvenWhenNodeIsPassive() throws Exception {
        // Set up
        when(mockRequest.getServletPath()).thenReturn(HA_REQUEST_SERVLET_PATH);
        when(mockNodeStateService.isActive()).thenReturn(false);

        // Invoke and check
        assertRequestAllowed();
    }

    @Test
    public void filterShouldAllowHaRestRequestsWhenNodeIsActive() throws Exception {
        // Set up
        when(mockRequest.getServletPath()).thenReturn(HA_REQUEST_SERVLET_PATH);
        when(mockNodeStateService.isActive()).thenReturn(true);

        // Invoke and check
        assertRequestAllowed();
    }

    @Test
    public void filterShouldAllowNormalRequestsWhenNodeIsActive() throws Exception {
        // Set up
        when(mockRequest.getServletPath()).thenReturn(NORMAL_REQUEST_SERVLET_PATH);
        when(mockNodeStateService.isActive()).thenReturn(true);

        // Invoke and check
        assertRequestAllowed();
    }

    @Test
    public void filterShouldReturnServiceUnavailableForNormalRequestsWhenNodeIsPassive() throws Exception {
        // Set up
        when(mockRequest.getServletPath()).thenReturn(NORMAL_REQUEST_SERVLET_PATH);
        when(mockNodeStateService.isActive()).thenReturn(false);
        when(mockResponse.getWriter()).thenReturn(mockResponseWriter);

        // Invoke and check
        assertRequestRejected();
    }

    private void assertRequestAllowed() throws Exception {
        // Invoke
        filter.doFilter(mockRequest, mockResponse, mockFilterChain);

        // Check
        verify(mockFilterChain).doFilter(mockRequest, mockResponse);
        verifyNoMoreInteractions(mockResponse, mockResponseWriter);
    }

    private void assertRequestRejected() throws Exception {
        // Invoke
        filter.doFilter(mockRequest, mockResponse, mockFilterChain);

        // Check
        verify(mockResponse).setCharacterEncoding(RESPONSE_ENCODING);
        verify(mockResponse).setContentType(ERROR_CONTENT_TYPE);
        verify(mockResponse).setStatus(SC_SERVICE_UNAVAILABLE);
        verify(mockResponseWriter).print(PASSIVE_NODE_ERROR);
        verifyNoMoreInteractions(mockResponseWriter);
    }
}
