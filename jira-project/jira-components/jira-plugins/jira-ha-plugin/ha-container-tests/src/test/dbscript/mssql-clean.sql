USE master
GO

IF EXISTS(SELECT name
          FROM sys.databases
          WHERE name = N'${hadb.instance}')
  BEGIN
    -- Disconnect all the users NOW so I can delete the database.
    ALTER DATABASE ${hadb.instance} SET SINGLE_USER WITH ROLLBACK IMMEDIATE
    DROP DATABASE ${hadb.instance}
  END
GO

IF EXISTS(SELECT *
          FROM sys.server_principals
          WHERE name = N'${hadb.username}')
  DROP LOGIN ${hadb.username}
GO
