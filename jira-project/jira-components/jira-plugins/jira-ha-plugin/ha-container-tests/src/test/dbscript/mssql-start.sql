USE master
GO

-- Create the JIRA user.
IF NOT EXISTS (SELECT * FROM sys.server_principals WHERE name = N'${hadb.username}')
CREATE LOGIN ${hadb.username} WITH PASSWORD=N'${hadb.password}', DEFAULT_DATABASE=master, CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
GO

-- Create the Database.
CREATE DATABASE ${hadb.instance} ON (NAME = N'jira', FILENAME = N'${mssql.datadir}\jira.mdf') LOG ON (NAME = N'jira_log', FILENAME = N'${mssql.datadir}\jira.ldf') COLLATE SQL_Latin1_General_CP1_CI_AS
GO

USE ${hadb.instance}
GO

--Set the database to snapshot isolation
ALTER DATABASE ${hadb.instance} SET ALLOW_SNAPSHOT_ISOLATION ON
GO

ALTER DATABASE ${hadb.instance} SET READ_COMMITTED_SNAPSHOT ON
GO

-- Change the database owner.
ALTER AUTHORIZATION ON DATABASE::${hadb.instance} TO ${hadb.username}
GO

-- Create a schema for the new user in the database.
-- CREATE SCHEMA ${hadb.schema} AUTHORIZATION dbo
-- GO

-- make sure turn NOCOUNT off http://confluence.atlassian.com/display/JIRA/Connecting+JIRA+to+SQL+Server+2008
SET NOCOUNT OFF
GO

