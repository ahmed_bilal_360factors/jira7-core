package com.atlassian.jira.plugins.ha.containertest;

import com.atlassian.jira.functest.framework.backdoor.ZeroDowntimeControl;
import com.atlassian.jira.pageobjects.pages.clustered.ViewUpgradesPage;
import com.atlassian.jira.plugins.ha.container.Container;
import com.atlassian.jira.plugins.ha.containertest.suites.UpgradeTests;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import org.junit.experimental.categories.Category;

/**
 * Tests a simple 'normal' ZDU upgrade where nodes are taken down one-by-one and upgraded.
 */
@Category(UpgradeTests.class)
public class TestZduSimpleAdminUI extends AbstractTestZduSimple {

    @Override
    protected void startUpgrade(final Container node1) {
        node1.jiraNode().product().quickLoginAsAdmin();
        ViewUpgradesPage upgradesPage = node1.jiraNode().product().goTo(ViewUpgradesPage.class);
        upgradesPage.startUpgrade();
    }

    @Override
    protected TimedQuery<ZeroDowntimeControl.UpgradeState> getUpgradeState(final Container node1) {
        node1.jiraNode().product().quickLoginAsAdmin();
        ViewUpgradesPage upgradesPage = node1.jiraNode().product().goTo(ViewUpgradesPage.class);
        return upgradesPage.getUpgradeState();
    }

    @Override
    protected void approveUpgrade(final Container node1) {
        node1.jiraNode().product().quickLoginAsAdmin();
        ViewUpgradesPage upgradesPage = node1.jiraNode().product().goTo(ViewUpgradesPage.class);
        upgradesPage.finishUpgrade();
    }
}
