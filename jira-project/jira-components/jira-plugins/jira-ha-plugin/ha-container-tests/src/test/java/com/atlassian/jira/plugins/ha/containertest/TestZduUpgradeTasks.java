package com.atlassian.jira.plugins.ha.containertest;

import com.atlassian.jira.index.request.ReindexStatus;
import com.atlassian.jira.plugins.ha.container.Container;
import com.atlassian.jira.plugins.ha.container.ContainerRule;
import com.atlassian.jira.plugins.ha.container.ContainerRuleFactory;
import com.atlassian.jira.plugins.ha.container.ContainerSet;
import com.atlassian.jira.plugins.ha.container.JiraType;
import com.atlassian.jira.plugins.ha.container.warhack.AddUpgradeTaskOperation;
import com.atlassian.jira.plugins.ha.container.warhack.FileEntry;
import com.atlassian.jira.plugins.ha.container.warhack.SimpleFileEntry;
import com.atlassian.jira.plugins.ha.container.warhack.WarHacker;
import com.atlassian.jira.rest.v2.index.ReindexRequestBean;
import com.atlassian.jira.webtests.ztests.indexing.ReindexRequestClient;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

import static com.atlassian.jira.functest.framework.backdoor.ZeroDowntimeControl.UpgradeState.MIXED;
import static com.atlassian.jira.functest.framework.backdoor.ZeroDowntimeControl.UpgradeState.READY_TO_RUN_UPGRADE_TASKS;
import static com.atlassian.jira.functest.framework.backdoor.ZeroDowntimeControl.UpgradeState.READY_TO_UPGRADE;
import static com.atlassian.jira.functest.framework.backdoor.ZeroDowntimeControl.UpgradeState.RUNNING_UPGRADE_TASKS;
import static com.atlassian.jira.functest.framework.backdoor.ZeroDowntimeControl.UpgradeState.STABLE;
import static com.atlassian.jira.plugins.ha.testapi.matchers.ClusterMatchers.allNodes;
import static com.atlassian.jira.plugins.ha.testapi.matchers.ClusterStateMatchers.isInUpgradeState;
import static com.atlassian.jira.plugins.ha.testapi.matchers.ClusterStateMatchers.upgradeState;
import static com.atlassian.jira.plugins.ha.testapi.matchers.UsersAndGroupsMatchers.userCount;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isOneOf;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class TestZduUpgradeTasks {

    @Rule
    public ContainerRule<?> containerSet = ContainerRuleFactory.create(builder -> builder.numNodes(2));

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    private Path upgradeTaskRecordFile(ContainerSet<?> containerSet) {
        return containerSet.getSharedHome().resolve("specialupgradetask.txt");
    }

    private void assertUpgradeTaskHasNotRun(ContainerSet<?> containerSet) {
        assertTrue("Upgrade task should not have run.", Files.notExists(upgradeTaskRecordFile(containerSet)));
    }

    /**
     * @return a list of reindex request IDs that the upgrade task created.
     */
    private List<Long> checkUpgradeTaskHasRunOnce(ContainerSet<?> containerSet) {
        assertTrue("Upgrade task should have run.", Files.exists(upgradeTaskRecordFile(containerSet)));
        try {
            List<String> lines = Files.readAllLines(upgradeTaskRecordFile(containerSet));
            assertThat("Upgrade task should have run once.", lines, hasSize(1));
            return SpecialUpgradeTask.parseReindexRequestIdsFromRecordLine(lines.get(0));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void testUpgradeTask() throws IOException {

        //Set up instances
        containerSet.resetDatabase();
        Files.deleteIfExists(upgradeTaskRecordFile(containerSet));
        containerSet.installJira(JiraType.BASE);
        containerSet.installTestingPlugins(JiraType.BASE);

        //Create our own upgrade WAR
        WarHacker warHacker = new WarHacker(containerSet.jiraWarFile(JiraType.BASE));
        warHacker.incrementBuildNumber();
        warHacker.addOperation(new AddUpgradeTaskAndMakePropertiesOperation());
        Path ourUpgradeWar = temporaryFolder.newFile().toPath();
        warHacker.writeWarFile(ourUpgradeWar);

        Container node1 = containerSet.getContainers().get(0);
        Container node2 = containerSet.getContainers().get(1);

        //Start both instances
        containerSet.startAll();
        containerSet.cluster().waitForStartup();

        //Verify they are both working by doing query to each
        //Should be able to query users and stuff
        assertThat(containerSet.cluster(), allNodes(userCount(greaterThanOrEqualTo(1))));

        //Verify ZDU state on each node
        assertThat(containerSet.cluster(), allNodes(isInUpgradeState(STABLE)));

        //Upgrade task should not have run yet
        assertUpgradeTaskHasNotRun(containerSet);

        //Commence ZDU on node 1
        node1.jiraNode().backdoor().zdu().start();
        assertThat(containerSet.cluster(), allNodes(isInUpgradeState(READY_TO_UPGRADE)));

        //Upgrade node 1
        node1.stop();
        node1.installWar(ourUpgradeWar, "jira");
        node1.start();
        containerSet.cluster().waitForStartup();

        //Verify both nodes still work
        assertThat(containerSet.cluster(), allNodes(userCount(greaterThanOrEqualTo(1))));

        //Should be in mixed mode now
        assertThat(containerSet.cluster(), allNodes(isInUpgradeState(MIXED)));

        //Upgrade task should not have run yet
        assertUpgradeTaskHasNotRun(containerSet);

        //Upgrade node 2
        node2.stop();
        node2.installWar(ourUpgradeWar, "jira");
        node2.start();
        containerSet.cluster().waitForStartup();

        //Verify both nodes still work
        assertThat(containerSet.cluster(), allNodes(userCount(greaterThanOrEqualTo(1))));

        //Should be in new state now
        assertThat(containerSet.cluster(), allNodes(isInUpgradeState(READY_TO_RUN_UPGRADE_TASKS)));

        //Upgrade task should not have run yet
        assertUpgradeTaskHasNotRun(containerSet);

        //Finalize the upgrade, this will run upgrade tasks
        node1.jiraNode().backdoor().zdu().approve();

        //Should be either running upgrade tasks or stable again
        assertThat(containerSet.cluster(), allNodes(upgradeState(isOneOf(STABLE, RUNNING_UPGRADE_TASKS))));

        //Poll until we get into STABLE state, allow 10 seconds or so to run upgrade tasks
        containerSet.waitForUpgradeState(STABLE, Duration.ofSeconds(120));

        //Once we are fully upgraded the upgrade task should have run
        List<Long> reindexRequestIds = checkUpgradeTaskHasRunOnce(containerSet);
        assertThat(reindexRequestIds, not(empty()));

        //Might still be running or complete, as long as we have one or more that got submitted and are on the queue
        //and not failed we are fine
        ReindexRequestClient reindexRequestClient = new ReindexRequestClient(containerSet.cluster().anyNode().environment());
        for (long reindexRequestId : reindexRequestIds) {
            ReindexRequestBean reindexRequest = reindexRequestClient.getReindexRequest(reindexRequestId);
            assertThat(reindexRequest.getStatus(), anyOf(is(ReindexStatus.ACTIVE), is(ReindexStatus.COMPLETE), is(ReindexStatus.RUNNING)));
        }
    }

    /**
     * Adds an upgrade task to JIRA WAR and also adds the necessary properties file to configure the upgrade
     * task's build number.
     *
     * @see SpecialUpgradeTask#makeProperties(String)
     */
    private static class AddUpgradeTaskAndMakePropertiesOperation extends AddUpgradeTaskOperation {
        public AddUpgradeTaskAndMakePropertiesOperation() {
            super(SpecialUpgradeTask.class);
        }

        @Override
        public Collection<? extends FileEntry> additionalFiles() throws IOException {
            List<FileEntry> files = new ArrayList<>(super.additionalFiles());
            String propertiesResourceName = SpecialUpgradeTask.class.getName().replace('.', '/') + ".properties";
            files.add(new SimpleFileEntry("WEB-INF/classes/" + propertiesResourceName, this::generatePropertiesStream));
            return files;
        }

        private InputStream generatePropertiesStream() throws IOException {
            Properties p = SpecialUpgradeTask.makeProperties(getGeneratedBuildNumber());
            try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
                p.store(bos, SpecialUpgradeTask.class.getName());
                return new ByteArrayInputStream(bos.toByteArray());
            }
        }
    }
}
