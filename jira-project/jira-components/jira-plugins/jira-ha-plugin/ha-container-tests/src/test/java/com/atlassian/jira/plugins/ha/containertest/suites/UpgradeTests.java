package com.atlassian.jira.plugins.ha.containertest.suites;

/**
 * These tests are only concerned about actually upgrading JIRA from one version to another. For example, a normal
 * Zero Downtime Upgrade might surface a problem when upgrading from JIRA version A to B where a test around upgrade
 * tasks does not add any more value.
 */
public interface UpgradeTests {
}
