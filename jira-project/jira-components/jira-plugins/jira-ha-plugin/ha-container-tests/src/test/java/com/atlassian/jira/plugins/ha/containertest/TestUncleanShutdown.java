package com.atlassian.jira.plugins.ha.containertest;

import com.atlassian.jira.plugins.ha.container.Container;
import com.atlassian.jira.plugins.ha.container.ContainerRule;
import com.atlassian.jira.plugins.ha.container.ContainerRuleFactory;
import com.atlassian.jira.plugins.ha.container.JiraType;
import com.atlassian.jira.plugins.ha.container.warhack.WarHacker;
import com.atlassian.jira.plugins.ha.testapi.test.JiraCluster;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

import static com.atlassian.jira.functest.framework.backdoor.ZeroDowntimeControl.UpgradeState.MIXED;
import static com.atlassian.jira.functest.framework.backdoor.ZeroDowntimeControl.UpgradeState.READY_TO_RUN_UPGRADE_TASKS;
import static com.atlassian.jira.functest.framework.backdoor.ZeroDowntimeControl.UpgradeState.READY_TO_UPGRADE;
import static com.atlassian.jira.functest.framework.backdoor.ZeroDowntimeControl.UpgradeState.RUNNING_UPGRADE_TASKS;
import static com.atlassian.jira.functest.framework.backdoor.ZeroDowntimeControl.UpgradeState.STABLE;
import static com.atlassian.jira.plugins.ha.testapi.matchers.ClusterMatchers.allNodes;
import static com.atlassian.jira.plugins.ha.testapi.matchers.ClusterMatchers.everyNode;
import static com.atlassian.jira.plugins.ha.testapi.matchers.ClusterStateMatchers.isInUpgradeState;
import static com.atlassian.jira.plugins.ha.testapi.matchers.ClusterStateMatchers.upgradeState;
import static com.atlassian.jira.plugins.ha.testapi.matchers.UsersAndGroupsMatchers.userCount;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.isOneOf;
import static org.junit.Assert.assertThat;

/**
 * Tests killing nodes instead of cleanly shutting them down before upgrading their binaries.
 */
public class TestUncleanShutdown {

    public static class TestZduTwoNodes {
        @Rule
        public ContainerRule<?> containerSet = ContainerRuleFactory.create(builder -> builder.numNodes(2));

        @Rule
        public TemporaryFolder temporaryFolder = new TemporaryFolder();

        /**
         * Simple test that spins up a 2-node cluster and performs ZDU from BASE version to UPGRADE version, but kill
         * nodes uncleanly instead of performing clean shutdown.
         */
        @Test
        public void testZduTwoNodes() throws IOException {
            runZdu(node -> node.installJira(JiraType.UPGRADE));
        }

        @Test
        public void testUncleanShutdownSameBuildNumber() throws IOException {
            //Create our own upgrade WAR
            WarHacker warHacker = new WarHacker(containerSet.jiraWarFile(JiraType.BASE));
            warHacker.incrementVersionString();
            Path ourUpgradeWar = temporaryFolder.newFile().toPath();
            warHacker.writeWarFile(ourUpgradeWar);

            runZdu(node -> node.installWar(ourUpgradeWar, "jira"));
        }

        private void runZdu(Consumer<Container> jiraUpgrader) {
            //Set up instances
            containerSet.resetDatabase();
            containerSet.installJira(JiraType.BASE);
            containerSet.installTestingPlugins(JiraType.BASE);

            Container node1 = containerSet.getContainers().get(0);
            Container node2 = containerSet.getContainers().get(1);

            //Start both instances
            containerSet.startAll();
            containerSet.cluster().waitForStartup();

            //Verify they are both working by doing query to each
            //Should be able to query users and stuff
            assertThat(containerSet.cluster(), allNodes(userCount(greaterThanOrEqualTo(1))));

            //Verify ZDU state on each node
            assertThat(containerSet.cluster(), allNodes(isInUpgradeState(STABLE)));

            //Commence ZDU on node 1
            node1.jiraNode().backdoor().zdu().start();
            assertThat(containerSet.cluster(), allNodes(isInUpgradeState(READY_TO_UPGRADE)));

            //Upgrade node 1, but shut it down uncleanly
            node1.jiraNode().backdoor().server().kill();
            node1.stop();
            jiraUpgrader.accept(node1);
            node1.start();
            containerSet.cluster().waitForStartup();

            //Verify both nodes still work
            assertThat(containerSet.cluster(), allNodes(userCount(greaterThanOrEqualTo(1))));

            //Should be in mixed mode now
            assertThat(containerSet.cluster(), allNodes(isInUpgradeState(MIXED)));

            //Upgrade node 2, shutting it down uncleanly
            node2.jiraNode().backdoor().server().kill();
            node2.stop();
            jiraUpgrader.accept(node2);
            node2.start();
            containerSet.cluster().waitForStartup();

            //Verify both nodes still work
            assertThat(containerSet.cluster(), allNodes(userCount(greaterThanOrEqualTo(1))));

            //Should be in new state now
            assertThat(containerSet.cluster(), allNodes(isInUpgradeState(READY_TO_RUN_UPGRADE_TASKS)));

            //Finalize the upgrade
            node1.jiraNode().backdoor().zdu().approve();

            //Should be either running upgrade tasks or stable again
            assertThat(containerSet.cluster(), allNodes(upgradeState(isOneOf(STABLE, RUNNING_UPGRADE_TASKS))));
        }
    }

    public static class TestZduWithReplacementNode {
        @Rule
        public ContainerRule<?> containerSet = ContainerRuleFactory.create(builder -> builder.numNodes(3));

        /**
         * Start 2 nodes, upgrade node 1, then instead of upgrading the second node put a third node in the cluster instead after
         * uncleanly killing node 2.  Expect that node 2 should stay registered in the cluster for 5 minutes.
         */
        @Test
        public void testZduWithReplacementNode() throws IOException {

            //Set up instances
            containerSet.resetDatabase();
            containerSet.installJira(JiraType.BASE);
            containerSet.installTestingPlugins(JiraType.BASE);

            Container node1 = containerSet.getContainers().get(0);
            Container node2 = containerSet.getContainers().get(1);
            Container node3 = containerSet.getContainers().get(2);

            //Start both instances
            node1.start();
            node2.start();
            containerSet.waitForStartupOfActiveContainers();

            List<JiraCluster.Node> originalNodes = Arrays.asList(node1.jiraNode(), node2.jiraNode());

            //Verify they are both working by doing query to each
            //Should be able to query users and stuff
            assertThat(originalNodes, everyNode(userCount(greaterThanOrEqualTo(1))));

            //Verify ZDU state on each node
            assertThat(originalNodes, everyNode(isInUpgradeState(STABLE)));

            //Commence ZDU on node 1
            node1.jiraNode().backdoor().zdu().start();
            assertThat(originalNodes, everyNode(isInUpgradeState(READY_TO_UPGRADE)));

            //Upgrade node 1 (clean shutdown)
            node1.stop();
            node1.installJira(JiraType.UPGRADE);
            node1.start();
            containerSet.waitForStartupOfActiveContainers();

            //Verify both nodes still work
            assertThat(originalNodes, everyNode(userCount(greaterThanOrEqualTo(1))));

            //Should be in mixed mode now
            assertThat(originalNodes, everyNode(isInUpgradeState(MIXED)));

            //Shut down node 2 uncleanly
            node2.jiraNode().backdoor().server().kill();
            node2.stop();

            //Bring up node 3 with upgraded JIRA
            node3.installJira(JiraType.UPGRADE);
            node3.start();
            containerSet.waitForStartupOfActiveContainers();

            List<JiraCluster.Node> endNodes = Arrays.asList(node1.jiraNode(), node3.jiraNode());

            //Verify both nodes still work
            assertThat(endNodes, everyNode(userCount(greaterThanOrEqualTo(1))));

            // Because node2 was killed, it is still considered active for another 5 minutes, so the state will either be
            // MIXED (if node2 is still active), or READY_TO_RUN_UPGRADE_TASKS (if node3 took long enough to startup that
            // node2 is no longer active).
            assertThat(endNodes, everyNode(upgradeState(isOneOf(MIXED, READY_TO_RUN_UPGRADE_TASKS))));
        }

    }
}
