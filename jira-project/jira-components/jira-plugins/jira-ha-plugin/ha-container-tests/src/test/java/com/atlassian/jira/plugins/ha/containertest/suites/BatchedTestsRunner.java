package com.atlassian.jira.plugins.ha.containertest.suites;

import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.config.properties.JiraSystemProperties;
import com.atlassian.jira.functest.framework.suite.JUnit4WebTestDescription;
import com.atlassian.jira.functest.framework.suite.JUnit4WebTestListener;
import com.atlassian.jira.pageobjects.config.junit4.LogTestInformationListener;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableSet;
import org.junit.Test;
import org.junit.experimental.categories.Categories.ExcludeCategory;
import org.junit.experimental.categories.Categories.IncludeCategory;
import org.junit.runner.Description;
import org.junit.runner.manipulation.Filter;
import org.junit.runner.manipulation.NoTestsRemainException;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.Suite;
import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ConfigurationBuilder;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;

import static com.google.common.collect.Lists.newLinkedList;
import static java.lang.Math.max;
import static java.lang.Math.min;
import static java.util.Collections.emptyList;
import static java.util.Collections.singleton;
import static java.util.Collections.unmodifiableSet;
import static java.util.Comparator.comparing;
import static java.util.Comparator.naturalOrder;
import static java.util.Comparator.nullsFirst;
import static java.util.stream.Collectors.toSet;
import static org.junit.experimental.categories.Categories.CategoryFilter.categoryFilter;

public class BatchedTestsRunner extends Suite {
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.TYPE)
    public @interface DoNotSplit {
    }

    private static final JiraProperties SYSTEM_PROPERTIES = JiraSystemProperties.getInstance();
    private static final int BATCH_COUNT = SYSTEM_PROPERTIES.getInteger("ha.containertest.batches", 0);
    private static final int BATCH_NUMBER = SYSTEM_PROPERTIES.getInteger("ha.containertest.batch", 0);
    private static final String TEST_PACKAGE = "com.atlassian.jira.plugins.ha.containertest";
    private static final Comparator<Description> DESCRIPTION_COMPARATOR = comparing(Description::getClassName, nullsFirst(naturalOrder()))
            .thenComparing(comparing(Description::getMethodName, nullsFirst(naturalOrder())));

    private static class DescriptionCollector extends Filter {
        private final Set<Description> descriptions = new TreeSet<>(DESCRIPTION_COMPARATOR);

        @Override
        public boolean shouldRun(final Description description) {
            // only discover 'leaf' descriptions
            if (description.getChildren().isEmpty()) {
                descriptions.add(description);
            }
            return true;
        }

        @Override
        public String describe() {
            return "Collecting filter";
        }

        private Set<Description> getDescriptions() {
            return unmodifiableSet(descriptions);
        }
    }

    public BatchedTestsRunner(final Class<?> klass) throws Throwable {
        super(klass, discoverTestClasses());
        try {
            // filter categories if present
            final Set<Class<?>> includeCategories = new HashSet<>(Optional.ofNullable(klass.getAnnotation(IncludeCategory.class))
                    .map(IncludeCategory::value)
                    .map(Arrays::asList)
                    .orElse(emptyList()));
            final Set<Class<?>> excludeCategories = new HashSet<>(Optional.ofNullable(klass.getAnnotation(ExcludeCategory.class))
                    .map(ExcludeCategory::value)
                    .map(Arrays::asList)
                    .orElse(emptyList()));
            if (!includeCategories.isEmpty() || !excludeCategories.isEmpty()) {
                filter(categoryFilter(true, includeCategories, true, excludeCategories));
            }

            // discover all test descriptions being run
            DescriptionCollector descriptionCollector = new DescriptionCollector();
            filter(descriptionCollector);

            if (BATCH_COUNT != 0) {
                // create batch of descriptions that should be run
                Set<Description> descriptionsForBatch = getDescriptionsForBatch(descriptionCollector.getDescriptions(), BATCH_COUNT, BATCH_NUMBER);
                filter(new Filter() {
                    @Override
                    public boolean shouldRun(final Description description) {
                        // non-leaf descriptions should always be run and filter the leaf descriptions
                        return !description.getChildren().isEmpty() || descriptionsForBatch.contains(description);
                    }

                    @Override
                    public String describe() {
                        return "Batch runner filter";
                    }
                });
            }
        } catch (NoTestsRemainException e) {
            // no-op
        }
    }

    @Override
    public void run(final RunNotifier notifier) {
        final LogTestInformationListener listener = new LogTestInformationListener();
        final JUnit4WebTestListener adaptedListener = new JUnit4WebTestListener(listener);
        final JUnit4WebTestDescription suiteDescription = new JUnit4WebTestDescription(getDescription());

        notifier.addListener(adaptedListener);
        try {
            listener.suiteStarted(suiteDescription);
            super.run(notifier);
        } finally {
            listener.suiteFinished(suiteDescription);
        }
    }

    private static Set<Description> getDescriptionsForBatch(final Set<Description> descriptions, final int batchCount, final int batchNumber) {
        // create slices of tests which should not be split
        LinkedList<List<Description>> testGroups = newLinkedList();
        descriptions.forEach(description -> {
            Class<?> testClass = description.getTestClass();

            List<Description> lastElement = testGroups.peekLast();
            if (testClass.getAnnotation(DoNotSplit.class) != null
                    && lastElement != null
                    && Objects.equal(lastElement.get(0).getTestClass(), testClass)) {
                testGroups.getLast().add(description);
            } else {
                testGroups.add(newLinkedList(singleton(description)));
            }
        });

        // This algorithm fills all available buckets evenly
        // by calculating which buckets will be filled completely
        // and which buckets will have one fewer element
        int bucketSize = testGroups.size() / batchCount;
        int rest = testGroups.size() % batchCount;
        int start = ((bucketSize + 1) * min(rest, batchNumber)) + (bucketSize * max(0, batchNumber - rest));
        int count = bucketSize;
        if (batchNumber < rest) {
            count++;
        }

        return testGroups.subList(start, start + count).stream().flatMap(List::stream).collect(toSet());
    }

    private static Class<?>[] discoverTestClasses() {
        final Reflections reflections = new Reflections(new ConfigurationBuilder()
                .forPackages(TEST_PACKAGE)
                .setScanners(new MethodAnnotationsScanner(), new SubTypesScanner()));

        final Set<Class> foundOnPath = reflections
                .getMethodsAnnotatedWith(Test.class)
                .stream()
                .map(Method::getDeclaringClass)
                .map(c -> reflections.getSubTypesOf(c).isEmpty() ? ImmutableSet.of(c) : reflections.getSubTypesOf(c))
                .flatMap(Collection::stream)
                .collect(toSet());

        return foundOnPath.stream().toArray(Class[]::new);
    }
}
