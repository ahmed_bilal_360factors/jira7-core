package com.atlassian.jira.plugins.ha.containertest;

import com.atlassian.jira.functest.framework.backdoor.ZeroDowntimeControl;
import com.atlassian.jira.plugins.ha.container.Container;
import com.atlassian.jira.plugins.ha.container.ContainerRule;
import com.atlassian.jira.plugins.ha.container.ContainerRuleFactory;
import com.atlassian.jira.plugins.ha.container.JiraType;
import com.atlassian.jira.plugins.ha.testapi.test.JiraCluster;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import org.junit.Rule;
import org.junit.Test;

import java.io.IOException;

import static com.atlassian.jira.functest.framework.backdoor.ZeroDowntimeControl.UpgradeState.MIXED;
import static com.atlassian.jira.functest.framework.backdoor.ZeroDowntimeControl.UpgradeState.READY_TO_RUN_UPGRADE_TASKS;
import static com.atlassian.jira.functest.framework.backdoor.ZeroDowntimeControl.UpgradeState.READY_TO_UPGRADE;
import static com.atlassian.jira.functest.framework.backdoor.ZeroDowntimeControl.UpgradeState.RUNNING_UPGRADE_TASKS;
import static com.atlassian.jira.functest.framework.backdoor.ZeroDowntimeControl.UpgradeState.STABLE;
import static com.atlassian.jira.plugins.ha.testapi.matchers.ClusterMatchers.allNodes;
import static com.atlassian.jira.plugins.ha.testapi.matchers.UsersAndGroupsMatchers.userCount;
import static com.atlassian.pageobjects.elements.query.Poller.by;
import static java.util.concurrent.TimeUnit.MINUTES;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Tests a simple 'normal' ZDU upgrade where nodes are taken down one-by-one and upgraded.
 */
public abstract class AbstractTestZduSimple {

    private static final Poller.WaitTimeout DEFAULT_TIMEOUT = by(1, MINUTES);
    @Rule
    public ContainerRule<?> containerSet = ContainerRuleFactory.create(builder -> builder.numNodes(2));

    /**
     * Simple test that spins up a 2-node cluster and performs ZDU from BASE version to UPGRADE version.
     */
    @Test
    public void testZduTwoNodes() throws IOException {

        //Set up instances
        containerSet.resetDatabase();
        containerSet.installJira(JiraType.BASE);
        containerSet.installTestingPlugins(JiraType.BASE);

        Container node1 = containerSet.getContainers().get(0);
        Container node2 = containerSet.getContainers().get(1);

        //Start both instances
        containerSet.startAll();
        containerSet.cluster().waitForStartup();

        //Verify they are both working by doing query to each
        //Should be able to query users and stuff
        assertThat(containerSet.cluster(), allNodes(userCount(greaterThanOrEqualTo(1))));

        onAfterClusterStart(containerSet.cluster());

        //Verify ZDU state on each node
        containerSet.getContainers().forEach(node -> Poller.waitUntil(getUpgradeState(node), is(STABLE), DEFAULT_TIMEOUT));

        //Commence ZDU on node 1
        startUpgrade(node1);
        containerSet.getContainers().forEach(node -> Poller.waitUntil(getUpgradeState(node), is(READY_TO_UPGRADE), DEFAULT_TIMEOUT));

        //Upgrade node 1
        node1.stop();
        node1.installJira(JiraType.UPGRADE);
        node1.start();
        containerSet.cluster().waitForStartup();

        //Verify both nodes still work
        assertThat(containerSet.cluster(), allNodes(userCount(greaterThanOrEqualTo(1))));

        //Should be in mixed mode now
        containerSet.getContainers().forEach(node -> Poller.waitUntil(getUpgradeState(node), is(MIXED), DEFAULT_TIMEOUT));

        //Upgrade node 2
        node2.stop();
        node2.installJira(JiraType.UPGRADE);
        node2.start();
        containerSet.cluster().waitForStartup();

        //Verify both nodes still work
        assertThat(containerSet.cluster(), allNodes(userCount(greaterThanOrEqualTo(1))));

        //Should be in new state now
        containerSet.getContainers().forEach(node -> Poller.waitUntil(getUpgradeState(node), is(READY_TO_RUN_UPGRADE_TASKS), DEFAULT_TIMEOUT));
        approveUpgrade(node1);


        //Should be either running upgrade tasks or stable again
        containerSet.getContainers().forEach(node -> Poller.waitUntil(getUpgradeState(node), anyOf(is(STABLE), is(RUNNING_UPGRADE_TASKS)), DEFAULT_TIMEOUT));
    }

    protected void onAfterClusterStart(final JiraCluster cluster) {
    }

    protected abstract void startUpgrade(final Container node1);

    protected abstract TimedQuery<ZeroDowntimeControl.UpgradeState> getUpgradeState(final Container node1);

    protected abstract void approveUpgrade(final Container node1);
}
