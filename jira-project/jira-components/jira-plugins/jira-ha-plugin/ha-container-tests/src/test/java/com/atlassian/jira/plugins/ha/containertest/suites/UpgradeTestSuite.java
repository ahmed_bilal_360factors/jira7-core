package com.atlassian.jira.plugins.ha.containertest.suites;

import org.junit.experimental.categories.Categories.IncludeCategory;
import org.junit.runner.RunWith;

@RunWith(BatchedTestsRunner.class)
@IncludeCategory(UpgradeTests.class)
public class UpgradeTestSuite {
}
