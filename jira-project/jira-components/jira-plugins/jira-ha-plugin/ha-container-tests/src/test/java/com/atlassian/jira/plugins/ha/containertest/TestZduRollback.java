package com.atlassian.jira.plugins.ha.containertest;

import com.atlassian.jira.plugin.PluginPath;
import com.atlassian.jira.plugins.ha.container.Container;
import com.atlassian.jira.plugins.ha.container.ContainerRule;
import com.atlassian.jira.plugins.ha.container.ContainerRuleFactory;
import com.atlassian.jira.plugins.ha.container.JiraType;
import com.atlassian.jira.plugins.ha.containertest.suites.UpgradeTests;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.io.IOException;
import java.nio.file.Path;

import static com.atlassian.jira.functest.framework.backdoor.ZeroDowntimeControl.UpgradeState.MIXED;
import static com.atlassian.jira.functest.framework.backdoor.ZeroDowntimeControl.UpgradeState.READY_TO_RUN_UPGRADE_TASKS;
import static com.atlassian.jira.functest.framework.backdoor.ZeroDowntimeControl.UpgradeState.READY_TO_UPGRADE;
import static com.atlassian.jira.functest.framework.backdoor.ZeroDowntimeControl.UpgradeState.STABLE;
import static com.atlassian.jira.matchers.NioFileMatchers.exists;
import static com.atlassian.jira.plugins.ha.testapi.matchers.ClusterMatchers.allNodes;
import static com.atlassian.jira.plugins.ha.testapi.matchers.ClusterStateMatchers.isInUpgradeState;
import static com.atlassian.jira.plugins.ha.testapi.matchers.UsersAndGroupsMatchers.userCount;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

/**
 * Tests partially upgrading ZDU and then rolling back.
 */
@Category(UpgradeTests.class)
public class TestZduRollback {

    @Rule
    public ContainerRule<?> containerSet = ContainerRuleFactory.create(builder -> builder.numNodes(2));

    /**
     * Simple test that spins up a 2-node cluster and performs ZDU from BASE version to UPGRADE version.
     */
    @Test
    public void testZduRollback() throws IOException {

        //Set up instances
        containerSet.resetDatabase();
        containerSet.installJira(JiraType.BASE);
        containerSet.installTestingPlugins(JiraType.BASE);

        Container node1 = containerSet.getContainers().get(0);
        Container node2 = containerSet.getContainers().get(1);

        //Start both instances
        containerSet.startAll();
        containerSet.cluster().waitForStartup();

        //Verify they are both working by doing query to each
        //Should be able to query users and stuff
        assertThat(containerSet.cluster(), allNodes(userCount(greaterThanOrEqualTo(1))));

        //Verify ZDU state on each node
        assertThat(containerSet.cluster(), allNodes(isInUpgradeState(STABLE)));

        //Commence ZDU on node 1
        node1.jiraNode().backdoor().zdu().start();
        assertThat(containerSet.cluster(), allNodes(isInUpgradeState(READY_TO_UPGRADE)));

        //Verify freeze file exists
        Path freezeFile = containerSet.getSharedHome().resolve(PluginPath.FREEZE_FILE_PATH);
        assertThat(freezeFile, exists());

        //Upgrade node 1
        node1.stop();
        node1.installJira(JiraType.UPGRADE);
        node1.start();
        containerSet.cluster().waitForStartup();

        //Verify both nodes still work
        assertThat(containerSet.cluster(), allNodes(userCount(greaterThanOrEqualTo(1))));

        //Verify freeze file
        assertThat(freezeFile, exists());

        //Should be in mixed mode now
        assertThat(containerSet.cluster(), allNodes(isInUpgradeState(MIXED)));

        //Upgrade node 2
        node2.stop();
        node2.installJira(JiraType.UPGRADE);
        node2.start();
        containerSet.cluster().waitForStartup();

        //Verify both nodes still work
        assertThat(containerSet.cluster(), allNodes(userCount(greaterThanOrEqualTo(1))));

        //Verify freeze file
        assertThat(freezeFile, exists());

        //Should be in new state now
        assertThat(containerSet.cluster(), allNodes(isInUpgradeState(READY_TO_RUN_UPGRADE_TASKS)));

        //-------------------------------------

        //Now let's downgrade each node
        node2.stop();
        node2.installJira(JiraType.BASE);
        node2.start();
        containerSet.cluster().waitForStartup();

        //Verify both nodes still work
        assertThat(containerSet.cluster(), allNodes(userCount(greaterThanOrEqualTo(1))));

        //Verify freeze file
        assertThat(freezeFile, exists());

        //Should be in mixed mode again
        assertThat(containerSet.cluster(), allNodes(isInUpgradeState(MIXED)));

        //Downgrade other node
        node1.stop();
        node1.installJira(JiraType.BASE);
        node1.start();
        containerSet.cluster().waitForStartup();

        //Verify both nodes still work
        assertThat(containerSet.cluster(), allNodes(userCount(greaterThanOrEqualTo(1))));

        //Verify freeze file
        assertThat(freezeFile, exists());

        //Should be in mixed mode again
        assertThat(containerSet.cluster(), allNodes(isInUpgradeState(READY_TO_UPGRADE)));

        //Cancel the upgrade
        node1.jiraNode().backdoor().zdu().cancel();

        //Verify freeze file is removed
        assertThat(freezeFile, not(exists()));

        //Should be back to stable
        assertThat(containerSet.cluster(), allNodes(isInUpgradeState(STABLE)));
    }
}
