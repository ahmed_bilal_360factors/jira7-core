package com.atlassian.jira.plugins.ha.containertest;

import com.atlassian.jira.functest.framework.backdoor.ZeroDowntimeControl;
import com.atlassian.jira.plugins.ha.container.Container;
import com.atlassian.jira.plugins.ha.containertest.suites.UpgradeTests;
import com.atlassian.pageobjects.elements.query.AbstractTimedQuery;
import com.atlassian.pageobjects.elements.query.ExpirationHandler;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.pageobjects.elements.timeout.DefaultTimeouts;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import org.junit.experimental.categories.Category;

/**
 * Tests a simple 'normal' ZDU upgrade where nodes are taken down one-by-one and upgraded.
 */
@Category(UpgradeTests.class)
public class TestZduSimpleBackdoor extends AbstractTestZduSimple {

    @Override
    protected void startUpgrade(final Container node1) {
        node1.jiraNode().backdoor().zdu().start();
    }

    @Override
    protected TimedQuery<ZeroDowntimeControl.UpgradeState> getUpgradeState(final Container node1) {
        return new AbstractTimedQuery<ZeroDowntimeControl.UpgradeState>(DefaultTimeouts.DEFAULT, Timeouts.DEFAULT_INTERVAL, ExpirationHandler.RETURN_CURRENT) {
            @Override
            protected boolean shouldReturn(final ZeroDowntimeControl.UpgradeState upgradeState) {
                return upgradeState != null;
            }

            @Override
            protected ZeroDowntimeControl.UpgradeState currentValue() {
                return node1.jiraNode().backdoor().zdu().currentState().getState();
            }
        };
    }

    @Override
    protected void approveUpgrade(final Container node1) {
        //Finalize the upgrade
        node1.jiraNode().backdoor().zdu().approve();
    }
}
