package com.atlassian.jira.plugins.ha.containertest;

import com.atlassian.core.util.FileUtils;
import com.atlassian.jira.functest.framework.backdoor.SchedulerControl.JobRunBean;
import com.atlassian.jira.functest.framework.backdoor.ZeroDowntimeControl;
import com.atlassian.jira.pageobjects.pages.admin.cluster.ReplicationSettingsPage;
import com.atlassian.jira.plugins.ha.container.ContainerRule;
import com.atlassian.jira.plugins.ha.container.ContainerRuleFactory;
import com.atlassian.jira.plugins.ha.container.ContainerSet;
import com.atlassian.jira.plugins.ha.container.JiraType;
import com.atlassian.jira.plugins.ha.containertest.suites.BatchedTestsRunner;
import com.atlassian.jira.plugins.ha.testapi.test.JiraCluster;
import com.atlassian.pageobjects.elements.query.AbstractTimedCondition;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.timeout.DefaultTimeouts;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import com.atlassian.scheduler.config.JobId;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import static com.atlassian.jira.plugins.ha.testapi.matchers.ClusterMatchers.allNodes;
import static com.atlassian.jira.plugins.ha.testapi.matchers.UsersAndGroupsMatchers.userCount;
import static com.atlassian.jira.util.collect.MapBuilder.build;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static java.nio.file.Files.list;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@BatchedTestsRunner.DoNotSplit
public class TestDisasterRecoveryNode {
    private static final String JIRA_CONFIG_PROPERTIES_FILE = "jira-config.properties";
    private static final JobId HOME_REPLICATOR_DELAYED_JOB_ID = JobId.of("com.atlassian.jira.cluster.disasterrecovery.JiraHomeReplicatorService.delayedReplicationId");
    private static final JobId HOME_REPLICATOR_FULL_JOB_ID = JobId.of("com.atlassian.jira.cluster.disasterrecovery.JiraHomeReplicatorService.fullReplicationId");
    private static final Path FREEZE_FILE_PATH = Paths.get("plugins", "installed-plugins-freeze.list");
    private static final Path INSTALLED_PLUGINS_PATH = Paths.get("plugins", "installed-plugins");
    private static final Poller.WaitTimeout DEFAULT_JOB_RUN_TIMEOUT = Poller.by(5, TimeUnit.MINUTES);
    private static Path disasterHome;
    private static JiraCluster.Node liveNode;

    @ClassRule
    public static ContainerRule<?> live = ContainerRuleFactory.create(builder -> builder
            .name("dr-live")
            .numNodes(1)
            .systemProperty("jira.websudo.is.disabled")
            .systemProperty("jira.secondary.home.idleSeconds", "5"));

    @BeforeClass
    public static void setUpAll() throws IOException {
        live.resetDatabase();
        live.installJira(JiraType.BASE);
        live.installTestingPlugins(JiraType.BASE);

        disasterHome = live.getSharedHome().resolve("..").resolve("disaster-home").toAbsolutePath();
        FileUtils.deleteDir(disasterHome.toFile());
        Files.createDirectories(disasterHome);
        createDisasterRecoveryConfig(live);

        live.startAll();
        live.cluster().waitForStartup();

        // Should be able to query users and such if cluster is live.
        assertThat(live.cluster(), allNodes(userCount(greaterThanOrEqualTo(1))));

        liveNode = live.cluster().anyNode();
        liveNode.login();

        goToReplicationSettings().editSettings().checkAll().update();
    }

    @After
    public void tearDown() throws IOException {
        FileUtils.deleteDir(disasterHome.toFile());
        Files.createDirectories(disasterHome);
        if (liveNode.backdoor().zdu().currentState().getState() == ZeroDowntimeControl.UpgradeState.READY_TO_UPGRADE) {
            liveNode.backdoor().zdu().cancel();
        }
    }

    @Test
    public void fullReplicationPlugins() throws IOException {
        ReplicationSettingsPage replicationPage = goToReplicationSettings();

        executeAndWaitForJobRun(replicationPage::replicate, HOME_REPLICATOR_FULL_JOB_ID);

        assertThatInstalledPluginsMatch(live, disasterHome);
    }

    @Test
    public void fullReplicationFreezeFile() throws IOException {
        writeFakeFreezeFile();

        executeAndWaitForJobRun(goToReplicationSettings()::replicate, HOME_REPLICATOR_FULL_JOB_ID);

        assertThat(Files.exists(disasterHome.resolve(FREEZE_FILE_PATH)), is(true));

        deleteFakeFreezeFile();

        executeAndWaitForJobRun(goToReplicationSettings()::replicate, HOME_REPLICATOR_FULL_JOB_ID);

        assertThat(Files.exists(disasterHome.resolve(FREEZE_FILE_PATH)), is(false));
    }

    @Test
    public void liveReplicationFreezeFile() {
        executeAndWaitForJobRun(liveNode.backdoor().zdu()::start, HOME_REPLICATOR_DELAYED_JOB_ID);

        assertThat(Files.exists(disasterHome.resolve(FREEZE_FILE_PATH)), is(true));

        executeAndWaitForJobRun(liveNode.backdoor().zdu()::cancel, HOME_REPLICATOR_DELAYED_JOB_ID);

        assertThat(Files.exists(disasterHome.resolve(FREEZE_FILE_PATH)), is(false));
    }

    private static ReplicationSettingsPage goToReplicationSettings() {
        return liveNode.product().goTo(ReplicationSettingsPage.class);
    }

    private static void assertThatInstalledPluginsMatch(final ContainerSet<?> live, final Path disasterHome) throws IOException {
        assertThat(
                list(live.getSharedHome().resolve(INSTALLED_PLUGINS_PATH))
                        .map(live.getSharedHome()::relativize)
                        .collect(toList()),
                contains(
                        list(disasterHome.resolve(INSTALLED_PLUGINS_PATH))
                                .map(disasterHome::relativize).toArray(Path[]::new)));
    }

    private static void executeAndWaitForJobRun(final Runnable mutationAction, final JobId jobId) {
        Optional<JobRunBean> job = ofNullable(live.cluster().anyNode().backdoor().scheduler().getLastRun(jobId));
        Function<JobRunBean, Date> getStartTime = jobRunBean -> jobRunBean.startTime;
        Date startTimeBeforeMutation = job.map(getStartTime).orElse(null);

        mutationAction.run();

        waitUntil(new AbstractTimedCondition(DefaultTimeouts.DEFAULT, Timeouts.DEFAULT_INTERVAL) {
            @Override
            protected Boolean currentValue() {
                Optional<JobRunBean> lastRun = ofNullable(live.cluster().anyNode().backdoor().scheduler().getLastRun(jobId));
                return !Objects.equals(lastRun.map(getStartTime).orElse(null), startTimeBeforeMutation);
            }
        }, is(true), DEFAULT_JOB_RUN_TIMEOUT);
    }

    private void deleteFakeFreezeFile() throws IOException {
        Files.delete(live.getSharedHome().resolve(FREEZE_FILE_PATH));
    }

    private void writeFakeFreezeFile() throws IOException {
        Files.write(live.getSharedHome().resolve(FREEZE_FILE_PATH), "TestPlugin.jar".getBytes(StandardCharsets.UTF_8));
    }

    private static void createDisasterRecoveryConfig(final ContainerSet<?> live) throws IOException {
        Path liveLocalHome = live.getContainers().get(0).getLocalHome();

        createPropertiesFile(build("jira.secondary.home", disasterHome.toAbsolutePath().toString()), liveLocalHome.resolve(JIRA_CONFIG_PROPERTIES_FILE));
    }

    private static void createPropertiesFile(final Map<String, String> values, final Path path) throws IOException {
        Properties props = new Properties();
        props.putAll(values);
        try (OutputStream os = Files.newOutputStream(path)) {
            props.store(os, "JIRA Config properties file");
        }
    }
}
