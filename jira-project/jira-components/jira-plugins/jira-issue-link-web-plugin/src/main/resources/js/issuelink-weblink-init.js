require([
    'jira-issue-link/web-link',
    'jira/util/events',
    'jira/util/events/types'
], function(WebLink, Events, Types) {
    'use strict';

    Events.bind(Types.NEW_PAGE_ADDED, function (e, context) {
        WebLink.init(context);
    });

});
