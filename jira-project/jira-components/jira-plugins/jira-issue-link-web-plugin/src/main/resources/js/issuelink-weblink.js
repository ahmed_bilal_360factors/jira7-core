define('jira-issue-link/web-link', ['require'], function(require) {
    'use strict';

    var jQuery = require('jquery');

    var $hiddenImg = jQuery("<img>");
    var $iconUrlInput;
    var $urlInput;
    var faviconUrl;
    var throbberTimeoutId;

    $hiddenImg.load(function() {
        $iconUrlInput.val(faviconUrl);
        $urlInput.css("background-image", 'url("' + faviconUrl + '")');

        clearTimeout(throbberTimeoutId);
        $urlInput.removeClass("loading");
    });
    $hiddenImg.error(function() {
        clearTimeout(throbberTimeoutId);
        $urlInput.removeClass("loading");
    });

    function fetchFavicon() {
        // Initialise state
        $hiddenImg.attr("src", '');
        $iconUrlInput.val('');
        $urlInput.css("background-image", '');
        faviconUrl = parseFaviconUrl($urlInput.val());

        if (!faviconUrl) {
            return;
        }

        /**
         * IE specific hack: For some reason I cannot change the class inside event handler for the event source.
         */
        setTimeout(function() { $urlInput.addClass("loading"); }, 0);
        throbberTimeoutId = setTimeout(function() { $urlInput.removeClass("loading"); }, 3000);

        $hiddenImg.attr("src", faviconUrl);
    }

    function parseFaviconUrl(url) {
        var parts = url.match(/^([^/]*\/\/[^/]+)/);
        var hostUrl = parts && parts.length > 1 ? parts[1] : false;

        if (!hostUrl) {
            return;
        }

        return hostUrl + "/favicon.ico";
    }

    return {
        init: function(context) {
            $iconUrlInput = jQuery("#web-link-icon-url", context);
            $urlInput = jQuery("#web-link-url", context).bind("change", fetchFavicon);

            if ($iconUrlInput.val()) {
                $urlInput.css("background-image", "url(" + $iconUrlInput.val() + ")");
            }
        }
    };

});
