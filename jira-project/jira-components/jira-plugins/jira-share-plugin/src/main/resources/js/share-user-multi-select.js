define('jira/share-plugin/share-user-picker', [
    'jira/skate',
    'jira/field/multi-user-list-picker',
    'jquery'
], function(skate, MultiUserListPicker, $) {
    var $doc = $(document);

    return skate('share-user-picker', {
        type: skate.type.CLASSNAME,
        attached: function shareUserPickerAttached(element) {
            var $el = $(element);
            var control = new MultiUserListPicker({
                layerId: $el.attr('id') + '-layer',
                element: $el,
                freeEmailInput: true
            });
            $doc.trigger('ready.multi-select.share-user', control);
        }
    });
});
