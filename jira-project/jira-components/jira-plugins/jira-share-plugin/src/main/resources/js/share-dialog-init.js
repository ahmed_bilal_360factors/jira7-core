// There may be multiple share button on the page, for example split view
// This is to ensure both works within its own scope and not affect each other

define('jira/share-plugin/initialiser', [
    'underscore',
    'jira/share-plugin/share-plugin-dialog',
    'jquery'
], function (
    _,
    SharePluginDialog,
    jQuery
) {
    return {
        init: _.once (function initShareDialogs() {
            var exports = {};
            exports.viewIssueDialog = new SharePluginDialog(jQuery);
            exports.issueNavDialog = new SharePluginDialog(jQuery);

            jQuery(_.bind(exports.viewIssueDialog._initShareDialog, exports.viewIssueDialog,
                    "share-entity-popup-viewissue", "#jira-share-trigger.viewissue-share"));
            jQuery(_.bind(exports.issueNavDialog._initShareDialog, exports.issueNavDialog,
                    "share-entity-popup-issuenav", "#jira-share-trigger.issuenav-share"));

            return exports;
        })
    };
});

// This IIFE will immediately and synchronously require the initialiser
// (thus create the dialogs), and immediately place the dialogs
// in to the global namespace. Use of AJS.namespace will add
// a deprecation warning whenever the global is accessed by other code.
(function(initialiser) {
    var dialogs = initialiser.init();
    AJS.namespace('JIRA.JiraSharePlugin.viewIssueDialog', null, dialogs.viewIssueDialog);
    AJS.namespace('JIRA.JiraSharePlugin.issueNavDialog', null, dialogs.issueNavDialog);
}(require('jira/share-plugin/initialiser')));
