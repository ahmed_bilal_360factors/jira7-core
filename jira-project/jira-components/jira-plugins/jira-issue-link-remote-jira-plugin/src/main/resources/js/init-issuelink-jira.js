require([
    'jira-issue-link/remote-jira/init-jira-search-dialog',
    'jira-issue-link/remote-jira/init-jira-issuelink',
    'jquery'
], function(JiraSearchDialog, IssueLink, $) {
    'use strict';

    IssueLink.init();

    $(function() {
        JiraSearchDialog.init();
    });
});
