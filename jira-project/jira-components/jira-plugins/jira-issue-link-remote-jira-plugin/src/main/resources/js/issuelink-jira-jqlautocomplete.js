/**
 * Instantiates jql autocomplete functionality on request instead of on page load.
 */
define('jira-issue-link/remote-jira/jql-autocomplete', ['require'], function(require) {
    'use strict';

    var FeatureFlagManager = require('jira/featureflags/feature-manager');
    var Forms = require('jira/util/forms');
    var $ = require('jquery');
    var wrmRequire = require('wrm/require');

    function loadJqlAutocomplete() {
        return wrmRequire('wr!jira.webresources:jqlautocomplete');
    }

    function init(options) {
        loadJqlAutocomplete().then(function() {
            var JQLAutoComplete = require('jira/autocomplete/jql-autocomplete');
            initialize(JQLAutoComplete, options);
        });
    }

    /**
     * Initializes an auto complete field
     * @param {JQLAutoComplete} JQLAutoComplete - the constructor function for the control
     * @param {Object} options
     */
    function initialize(JQLAutoComplete, options) {

        var fieldID = options.fieldID;
        var errorID = options.errorID;
        var autoCompleteUrl = options.autoCompleteUrl;
        var autoCompleteData = options.autoCompleteData;
        var formSubmitFunction = options.formSubmitFunction;

        var $field = $('#' + fieldID);
        var hasFocus = $field.length > 0 && $field[0] === document.activeElement;

        var jqlFieldNames = autoCompleteData.visibleFieldNames || [];
        var jqlFunctionNames = autoCompleteData.visibleFunctionNames || [];
        var jqlReservedWords = autoCompleteData.jqlReservedWords || [];

        var isAutoSelectFirstEnabled = function() {
            return FeatureFlagManager.isFeatureEnabled("jira.jql.autoselectfirst");
        };

        var jqlAutoComplete = new JQLAutoComplete({
            fieldID: fieldID,
            parser: new JQLAutoComplete.MyParser(jqlReservedWords),
            queryDelay: .65,
            jqlFieldNames: jqlFieldNames,
            jqlFunctionNames: jqlFunctionNames,
            minQueryLength: 0,
            allowArrowCarousel: true,
            autoSelectFirst: isAutoSelectFirstEnabled(),
            errorID: errorID,
            autoCompleteUrl: autoCompleteUrl
        });

        $field.unbind("keypress", Forms.submitOnEnter);

        if (formSubmitFunction) {
            $field.keypress(function (e) {
                if (jqlAutoComplete.dropdownController === null || !jqlAutoComplete.dropdownController.displayed || jqlAutoComplete.selectedIndex < 0) {
                    if (e.keyCode === 13 && !e.ctrlKey && !e.shiftKey)
                    {
                        formSubmitFunction();
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            });
        }

        jqlAutoComplete.buildResponseContainer();
        jqlAutoComplete.parse($field.text());
        jqlAutoComplete.updateColumnLineCount();

        $field.click(function(){
            jqlAutoComplete.dropdownController.hideDropdown();
        });

        if (hasFocus) {
            $field.select();
        }
    }

    return {
        initialize: init
    };
});

AJS.namespace('IssueLinkJQLAutoComplete', null, require('jira-issue-link/remote-jira/jql-autocomplete'));
