package com.atlassian.jira.projectconfig.rest.project;

import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.bc.projectroles.ProjectRoleService;
import com.atlassian.jira.bc.security.login.LoginService;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.roles.MockUserRoleActor;
import com.atlassian.jira.security.roles.ProjectRoleActor;
import com.atlassian.jira.security.roles.ProjectRoleActors;
import com.atlassian.jira.security.roles.RoleActor;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.EmailFormatter;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestRolesResource {
    @Mock
    private ProjectRoleService roleService;

    @Mock
    private ProjectService projectService;

    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;

    @Mock
    private AvatarService avatarService;

    @Mock
    private LoginService loginService;

    @Mock
    private DateTimeFormatter dateTimeFormatter;

    @Mock
    private EmailFormatter emailFormatter;

    @Mock
    private ApplicationRoleManager applicationRoleManager;

    @Mock
    private UserManager userManager;

    private RolesResource rolesResource;

    @Before
    public void setup() {
        rolesResource = new RolesResource(
                roleService,
                projectService,
                jiraAuthenticationContext,
                avatarService,
                loginService,
                dateTimeFormatter,
                emailFormatter,
                applicationRoleManager,
                userManager);
    }

    @Test
    public void testNonExistentUserIsNotReturned() {
        ApplicationUser nonExistentUser = new MockApplicationUser("nonexistent");
        when(userManager.isUserExisting(nonExistentUser)).thenReturn(false);

        RoleActor userRoleActor = new MockUserRoleActor(1l, 1l, nonExistentUser);
        Set<RoleActor> roleActors = Sets.newHashSet(userRoleActor);

        ProjectRoleActors projectRoleActors = mock(ProjectRoleActors.class);
        when(projectRoleActors.getRoleActorsByType(ProjectRoleActor.USER_ROLE_ACTOR_TYPE)).thenReturn(roleActors);

        List<ApplicationUser> users = rolesResource.getUsersByQuery(projectRoleActors, Optional.of("non"));

        assertFalse(users.contains(nonExistentUser));
    }
}
