package com.atlassian.jira.projectconfig.workflow;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.issuetype.MockIssueType;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.order.MockOrderFactory;
import com.atlassian.jira.projectconfig.order.OrderFactory;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.workflow.DraftWorkflowScheme;
import com.atlassian.jira.workflow.MockDraftWorkflowScheme;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.jira.util.collect.MapBuilder.build;
import static com.atlassian.jira.workflow.JiraWorkflow.DEFAULT_WORKFLOW_NAME;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.stub;

@RunWith(MockitoJUnitRunner.class)
public class TestDefaultWorkflowSchemeEditor {
    public static final String WF = "WF";
    public static final String WF_2 = "WF 2";
    public static final String WF_3 = "WF 3";
    public static final String WF_4 = "WF 4";
    public static final String ISSUE_TYPE_A = "A";
    public static final String ISSUE_TYPE_B = "B";
    public static final String ISSUE_TYPE_C = "C";

    @Mock
    private Project project;
    private DefaultWorkflowSchemeEditor issueTypeToWorkflowAssigner;
    private MockDraftWorkflowScheme draftWorkflowScheme;

    @Before
    public void setUp() {
        Collection<IssueType> projectIssueTypes = Arrays.<IssueType>asList(
                new MockIssueType(ISSUE_TYPE_A, ISSUE_TYPE_A),
                new MockIssueType(ISSUE_TYPE_B, ISSUE_TYPE_B),
                new MockIssueType(ISSUE_TYPE_C, ISSUE_TYPE_C));

        stub(project.getIssueTypes()).toReturn(projectIssueTypes);

        OrderFactory orderFactory = new MockOrderFactory();
        issueTypeToWorkflowAssigner = new DefaultWorkflowSchemeEditor(orderFactory);
        draftWorkflowScheme = new MockDraftWorkflowScheme();
    }

    /**
     * [A, B -> WF]
     * WF_2 -> A
     * [null -> jira], [A -> WF_2], [B -> WF]
     */
    @Test
    public void assignSomeIfThereIsImplicitAndSomeExplicit() {
        draftWorkflowScheme.setMapping(ISSUE_TYPE_A, WF);
        draftWorkflowScheme.setMapping(ISSUE_TYPE_B, WF);

        checkAssign(asList(ISSUE_TYPE_A), WF_2,
                build(null, DEFAULT_WORKFLOW_NAME, ISSUE_TYPE_B, WF, ISSUE_TYPE_A, WF_2));
    }

    /**
     * [A -> WF], [B -> WF_2]
     * WF_3 -> A
     * [null -> jira], [A -> WF_3], [B -> WF_2]
     */
    @Test
    public void assignSomeIfThereIsImplicitAndSomeExplicit_v2() {
        draftWorkflowScheme.setMapping(ISSUE_TYPE_A, WF);
        draftWorkflowScheme.setMapping(ISSUE_TYPE_B, WF_2);

        checkAssign(asList(ISSUE_TYPE_A), WF_3,
                build(null, DEFAULT_WORKFLOW_NAME, ISSUE_TYPE_A, WF_3, ISSUE_TYPE_B, WF_2));
    }

    /**
     * [A, B -> WF_2]
     * WF -> A
     * [null -> jira], [A -> WF], [B->WF_2]
     */
    @Test
    public void assignSomeIfThereIsImplicitAndSomeExplicit_v3() {
        draftWorkflowScheme.setMapping(ISSUE_TYPE_A, WF_2);
        draftWorkflowScheme.setMapping(ISSUE_TYPE_B, WF_2);

        checkAssign(asList(ISSUE_TYPE_A), WF,
                build(null, DEFAULT_WORKFLOW_NAME, ISSUE_TYPE_A, WF, ISSUE_TYPE_B, WF_2));
    }

    /**
     * [A, B -> WF]
     * WF_2 -> A, B
     * [null -> jira, A,B -> WF_2]
     */
    @Test
    public void assignAllIfThereIsImplicitAndSomeExplicit() {
        draftWorkflowScheme.setMapping(ISSUE_TYPE_A, WF);
        draftWorkflowScheme.setMapping(ISSUE_TYPE_B, WF);

        checkAssign(asList(ISSUE_TYPE_A, ISSUE_TYPE_B), WF_2,
                build(null, DEFAULT_WORKFLOW_NAME, ISSUE_TYPE_A, WF_2, ISSUE_TYPE_B, WF_2));
    }

    /**
     * [null -> WF]
     * WF_2 -> All (A, B, C)
     * [null -> WF_2]
     */
    @Test
    public void assignAllToAnotherDefaultIfAllAreUnassigned() {
        draftWorkflowScheme.setMapping(null, WF);

        checkAssign(asList(ISSUE_TYPE_A, ISSUE_TYPE_B, ISSUE_TYPE_C), WF_2,
                MapBuilder.<String, String>build(null, WF_2));
    }

    /**
     * []
     * WF -> All (A, B, C)
     * [null -> WF]
     */
    @Test
    public void assignAllIfThereIsOnlyImplicit() {
        checkAssign(asList(ISSUE_TYPE_A, ISSUE_TYPE_B, ISSUE_TYPE_C), WF,
                MapBuilder.<String, String>build(null, WF));
    }

    /**
     * []
     * WF -> A, B
     * [null -> jira, A,B-> WF]
     */
    @Test
    public void assignSomeIfThereIsOnlyImplicit() {
        checkAssign(asList(ISSUE_TYPE_A, ISSUE_TYPE_B), WF,
                build(null, DEFAULT_WORKFLOW_NAME, ISSUE_TYPE_A, WF, ISSUE_TYPE_B, WF));
    }

    /**
     * [null -> WF], [A -> WF_2]
     * WF_3 -> All (A, B, C)
     * [null -> WF_3]
     */
    @Test
    public void assignAllToAnotherDefaultIfSomeAreExplicit() {
        draftWorkflowScheme.setMapping(null, WF);
        draftWorkflowScheme.setMapping(ISSUE_TYPE_A, WF_2);

        checkAssign(asList(ISSUE_TYPE_A, ISSUE_TYPE_B, ISSUE_TYPE_C), WF_3,
                MapBuilder.<String, String>build(null, WF_3));
    }

    /**
     * [null -> WF]
     * WF_2 -> A
     * [null -> WF], [A -> WF_2]
     */
    @Test
    public void assignSomeIfAllAreUnassigned() {
        draftWorkflowScheme.setMapping(null, WF);

        checkAssign(asList(ISSUE_TYPE_A), WF_2, build(null, WF, ISSUE_TYPE_A, WF_2));
    }

    /**
     * [null -> WF_2]
     * WF -> A
     * [null -> WF_2], [A -> WF]
     */
    @Test
    public void assignSomeIfAllAreUnassigned_v2() {
        draftWorkflowScheme.setMapping(null, WF_2);

        checkAssign(asList(ISSUE_TYPE_A), WF, build(null, WF_2, ISSUE_TYPE_A, WF));
    }

    /**
     * [null -> WF], [A -> WF_2]
     * WF_3 -> A
     * [null -> WF], [A -> WF_3]
     */
    @Test
    public void assignExplicitToAnotherExplicitIfSomeAreUnassigned() {
        draftWorkflowScheme.setMapping(null, WF);
        draftWorkflowScheme.setMapping(ISSUE_TYPE_A, WF_2);

        checkAssign(asList(ISSUE_TYPE_A), WF_3, build(null, WF, ISSUE_TYPE_A, WF_3));
    }

    /**
     * [null -> WF], [A -> WF_2]
     * WF_3 -> B
     * [null -> WF], [A -> WF_3], [B -> WF_3]
     */
    @Test
    public void assignSomeUnassignedToExplicit() {
        draftWorkflowScheme.setMapping(null, WF);
        draftWorkflowScheme.setMapping(ISSUE_TYPE_A, WF_2);

        checkAssign(asList(ISSUE_TYPE_B), WF_3,
                build(null, WF, ISSUE_TYPE_A, WF_2, ISSUE_TYPE_B, WF_3));
    }

    /**
     * [null -> WF], [A -> WF_2]
     * WF -> A
     * [null -> WF]
     */
    @Test
    public void assignAllExplicitToUnassigned() {
        draftWorkflowScheme.setMapping(null, WF);
        draftWorkflowScheme.setMapping(ISSUE_TYPE_A, WF_2);

        checkAssign(asList(ISSUE_TYPE_A), WF, MapBuilder.<String, String>build(null, WF));
    }

    /**
     * [null -> WF], [A, B -> WF_2]
     * WF -> A
     * [null -> WF], [B -> WF_2]
     */
    @Test
    public void assignSomeExplicitToUnassigned() {
        draftWorkflowScheme.setMapping(null, WF);
        draftWorkflowScheme.setMapping(ISSUE_TYPE_A, WF_2);
        draftWorkflowScheme.setMapping(ISSUE_TYPE_B, WF_2);

        checkAssign(asList(ISSUE_TYPE_A), WF,
                build(null, WF, ISSUE_TYPE_B, WF_2));
    }

    /**
     * [null -> WF], [A -> WF_2]
     * WF_3 -> B, C (All except A)
     * [B, C -> WF_3], [null -> WF_2]
     */
    @Test
    public void assignAllUnassignedToAnotherDefault() {
        draftWorkflowScheme.setMapping(null, WF);
        draftWorkflowScheme.setMapping(ISSUE_TYPE_A, WF_2);

        checkAssign(asList(ISSUE_TYPE_B, ISSUE_TYPE_C), WF_3,
                build(null, WF_2, ISSUE_TYPE_B, WF_3, ISSUE_TYPE_C, WF_3));
    }

    /**
     * [null -> WF], [A, B -> WF_2]
     * WF_3 -> C
     * [C -> WF_3], [null -> WF_2]
     */
    @Test
    public void assignAllUnassignedToAnotherDefault_v2() {
        draftWorkflowScheme.setMapping(null, WF);
        draftWorkflowScheme.setMapping(ISSUE_TYPE_A, WF_2);
        draftWorkflowScheme.setMapping(ISSUE_TYPE_B, WF_2);

        checkAssign(asList(ISSUE_TYPE_C), WF_3, build(null, WF_2, ISSUE_TYPE_C, WF_3));
    }

    /**
     * [null -> WF], [A -> WF_2], [B -> WF_3]
     * WF_4 -> C
     * [C -> WF_4], [null -> WF_2], [B -> WF_3]
     */
    @Test
    public void assignAllUnassignedToAnotherDefault_v3() {
        draftWorkflowScheme.setMapping(null, WF);
        draftWorkflowScheme.setMapping(ISSUE_TYPE_A, WF_2);
        draftWorkflowScheme.setMapping(ISSUE_TYPE_B, WF_3);

        checkAssign(asList(ISSUE_TYPE_C), WF_4,
                build(null, WF_2, ISSUE_TYPE_B, WF_3, ISSUE_TYPE_C, WF_4));
    }

    /**
     * [null -> WF], [A -> WF_2], [B ->WF_2], [C -> WF_3]
     * Delete: WF_2
     * [null -> WF], [C->WF_3]
     */
    @Test
    public void deleteNonDefault() {
        draftWorkflowScheme.setDefaultWorkflow(WF).setMapping(ISSUE_TYPE_A, WF_2).setMapping(ISSUE_TYPE_B, WF_2)
                .setMapping(ISSUE_TYPE_C, WF_3);
        checkDelete(WF_2, build(null, WF, ISSUE_TYPE_C, WF_3));
    }

    /**
     * [null -> WF], [A -> WF_2], [B ->WF_2], [C -> WF]
     * Delete: WF_2
     * [null -> WF]
     */
    @Test
    public void deleteNonDefaultCompress() {
        draftWorkflowScheme.setDefaultWorkflow(WF).setMapping(ISSUE_TYPE_A, WF_2).setMapping(ISSUE_TYPE_B, WF_2)
                .setMapping(ISSUE_TYPE_C, WF);
        checkDelete(WF_2, MapBuilder.<String, String>build(null, WF));
    }

    /**
     * [null -> WF], [A -> WF_2], [B ->WF_2], [C -> WF]
     * Delete: WF
     * [null -> WF_2],
     */
    @Test
    public void deleteDefaultAllMapped() {
        draftWorkflowScheme.setDefaultWorkflow(WF).setMapping(ISSUE_TYPE_A, WF_2).setMapping(ISSUE_TYPE_B, WF_2)
                .setMapping(ISSUE_TYPE_C, WF);
        checkDelete(WF, MapBuilder.<String, String>build(null, WF_2));
    }

    /**
     * [null -> WF], [A -> WF_2], [C -> WF_3]
     * Delete: WF
     * [null -> WF_2], [C -> WF_3]
     */
    @Test
    public void deleteDefaultSomeMapped() {
        draftWorkflowScheme.setDefaultWorkflow(WF).setMapping(ISSUE_TYPE_A, WF_2).setMapping(ISSUE_TYPE_C, WF_3);
        checkDelete(WF, build(null, WF_2, ISSUE_TYPE_C, WF_3));
    }

    /**
     * [null -> WF], [A -> WF_2], [C -> WF]
     * Delete: WF_3
     * [null -> WF], [A -> WF_2]
     */
    @Test
    public void deleteNoUsed() {
        draftWorkflowScheme.setDefaultWorkflow(WF).setMapping(ISSUE_TYPE_A, WF_2)
                .setMapping(ISSUE_TYPE_C, WF);
        checkDelete(WF_3, build(null, WF, ISSUE_TYPE_A, WF_2));
    }

    /**
     * [null -> WF], [B -> WF]
     * Delete: WF
     * []
     */
    @Test
    public void deleteAll() {
        draftWorkflowScheme.setDefaultWorkflow(WF).setMapping(ISSUE_TYPE_B, WF);
        checkDelete(WF, Collections.<String, String>emptyMap());
    }

    private void checkAssign(Iterable<String> issueTypesToAssign, String newWorkflow, Map<String, String> expectedMapping) {
        DraftWorkflowScheme actual = issueTypeToWorkflowAssigner.assign(draftWorkflowScheme, issueTypesToAssign, newWorkflow, project);
        assertEquals(expectedMapping, actual.getMappings());
    }

    private void checkDelete(String deleteWorkflow, Map<String, String> expectedMapping) {
        DraftWorkflowScheme actual = issueTypeToWorkflowAssigner.delete(draftWorkflowScheme, deleteWorkflow, project);
        assertEquals(expectedMapping, actual.getMappings());
    }
}
