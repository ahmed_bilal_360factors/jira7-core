package com.atlassian.jira.projectconfig.contextproviders;

import com.atlassian.jira.issue.security.IssueSecuritySchemeManager;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.util.TabUrlFactory;
import com.atlassian.jira.util.collect.MapBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.ofbiz.core.entity.GenericValue;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * @since v4.4
 */
@RunWith(MockitoJUnitRunner.class)
public class TestPermissionsSummaryPanelContextProvider {
    private static final String PERMISSION_SCHEME_URL = "something in the way she moves";
    private static final String ISSUE_SECURITY_SCHEME_URL = "forIssueSecurity";

    @Mock
    private IssueSecuritySchemeManager issueSecuritySchemeManager;
    @Mock
    private ContextProviderUtils providerUtils;
    @Mock
    private PermissionsSummaryPanelContextProvider.SimpleIssueSecurityScheme simpleIssueSecurityScheme;
    @Mock
    private TabUrlFactory tabUrlFactory;
    @Mock
    private PermissionsSummaryPanelContextProvider.SimpleProjectPermissionsScheme simpleProjectPermissionsScheme;

    private Project project;
    private GenericValue issueSecuritySchemeGV;
    private GenericValue projectPermissionsSchemeGV;

    @Before
    public void setUp() throws Exception {
        project = new MockProject(678L, "ABC");
        issueSecuritySchemeGV = new MockGenericValue("IssueSecurityScheme");
        projectPermissionsSchemeGV = new MockGenericValue("ProjectPermissionsScheme");
    }

    @Test
    public void testAdminNoSchemes() {
        // setup
        when(providerUtils.getProject()).thenReturn(project);

        when(tabUrlFactory.forIssueSecurity()).thenReturn(ISSUE_SECURITY_SCHEME_URL);
        when(tabUrlFactory.forPermissions()).thenReturn(PERMISSION_SCHEME_URL);


        PermissionsSummaryPanelContextProvider provider = getProvider(null, null);

        Map<String, Object> paramMap = MapBuilder.<String, Object>build("param", true);

        // execute
        Map<String, Object> actualMap = provider.getContextMap(paramMap);

        MapBuilder<String, Object> expectedMap = MapBuilder.newBuilder(paramMap)
                .add("issueSecuritySchemeUrl", ISSUE_SECURITY_SCHEME_URL)
                .add("projectPermissionUrl", PERMISSION_SCHEME_URL);

        // verify
        assertEquals(expectedMap.toMap(), actualMap);
    }

    @Test
    public void testAdminWithProjectPermissionScheme() {
        // setup
        PermissionsSummaryPanelContextProvider provider = getProvider(null, projectPermissionsSchemeGV);

        when(providerUtils.getProject()).thenReturn(project);
        when(simpleProjectPermissionsScheme.getId()).thenReturn("12312");

        when(tabUrlFactory.forIssueSecurity()).thenReturn(ISSUE_SECURITY_SCHEME_URL);
        when(tabUrlFactory.forPermissions()).thenReturn(PERMISSION_SCHEME_URL);

        Map<String, Object> paramMap = MapBuilder.<String, Object>build("param", true);

        // execute
        Map<String, Object> actualMap = provider.getContextMap(paramMap);

        MapBuilder<String, Object> expectedMap = MapBuilder.newBuilder(paramMap)
                .add("issueSecuritySchemeUrl", ISSUE_SECURITY_SCHEME_URL)
                .add("projectPermissionsScheme", simpleProjectPermissionsScheme)
                .add("projectPermissionUrl", PERMISSION_SCHEME_URL);

        // verify
        assertEquals(expectedMap.toMap(), actualMap);
    }


    @Test
    public void testAdminWithProjectPermissionAndIssueScheme() {
        // setup
        PermissionsSummaryPanelContextProvider provider = getProvider(issueSecuritySchemeGV, projectPermissionsSchemeGV);

        when(providerUtils.getProject()).thenReturn(project);
        when(simpleProjectPermissionsScheme.getId()).thenReturn("12312");
        when(simpleIssueSecurityScheme.getId()).thenReturn("5555");
        when(tabUrlFactory.forIssueSecurity()).thenReturn(ISSUE_SECURITY_SCHEME_URL);
        when(tabUrlFactory.forPermissions()).thenReturn(PERMISSION_SCHEME_URL);

        Map<String, Object> paramMap = MapBuilder.<String, Object>build("param", true);
        // execute
        Map<String, Object> actualMap = provider.getContextMap(paramMap);

        // verify
        MapBuilder<String, Object> expectedMap = MapBuilder.newBuilder(paramMap)
                .add("issueSecurityScheme", simpleIssueSecurityScheme)
                .add("issueSecuritySchemeUrl", ISSUE_SECURITY_SCHEME_URL)
                .add("projectPermissionsScheme", simpleProjectPermissionsScheme)
                .add("projectPermissionUrl", PERMISSION_SCHEME_URL);

        assertEquals(expectedMap.toMap(), actualMap);
    }


    private PermissionsSummaryPanelContextProvider getProvider(final GenericValue issueSecurityScheme, final GenericValue projectPermissionsScheme) {
        return new PermissionsSummaryPanelContextProvider(null, issueSecuritySchemeManager, providerUtils, tabUrlFactory) {

            @Override
            SimpleIssueSecurityScheme gvToIssueSecurityScheme(GenericValue issueSecurityGV) {
                return simpleIssueSecurityScheme;
            }

            @Override
            SimpleProjectPermissionsScheme gvToProjectPermissionsScheme(GenericValue projectPermssionsGV) {
                return simpleProjectPermissionsScheme;
            }

            @Override
            GenericValue getProjectPermissionsScheme(GenericValue projectGV) {
                return projectPermissionsScheme;
            }

            @Override
            GenericValue getIssueSecuritySchemes(GenericValue projectGV) {
                return issueSecurityScheme;
            }

        };
    }
}
