package com.atlassian.jira.projectconfig.order;

import java.util.Comparator;

import com.atlassian.jira.issue.IssueConstant;
import com.atlassian.jira.issue.IssueConstants;
import com.atlassian.jira.projectconfig.beans.NamedDefault;
import com.atlassian.jira.projectconfig.beans.SimpleIssueType;

import com.google.common.collect.Ordering;

/**
 * @since v4.4
 */
public class MockOrderFactory implements OrderFactory {
    @Override
    public Comparator<String> createStringComparator() {
        return NativeComparator.getInstance();
    }

    @Override
    public Comparator<NamedDefault> createNamedDefaultComparator() {
        return new NamedDefaultComparator(createStringComparator());
    }

    @Override
    public Comparator<SimpleIssueType> createIssueTypeComparator() {
        return new SimpleIssueTypeComparator(createStringComparator());
    }

    @Override
    public Ordering<IssueConstant> createTranslatedNameOrder() {
        return Ordering.from(createStringComparator())
                .onResultOf(IssueConstants.getTranslatedNameFunc());
    }
}
