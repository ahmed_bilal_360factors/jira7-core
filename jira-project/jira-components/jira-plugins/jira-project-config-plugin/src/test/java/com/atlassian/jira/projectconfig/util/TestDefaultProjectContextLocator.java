package com.atlassian.jira.projectconfig.util;

import java.util.Locale;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.ServiceOutcomeMatchers;
import com.atlassian.jira.bc.project.ProjectAction;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.issuetype.MockIssueType;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.beans.ProjectContext;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

/**
 * @since v6.2
 */
@RunWith(MockitoJUnitRunner.class)
public class TestDefaultProjectContextLocator {
    public static final String ERROR_MESSAGE = "Error Found";
    public static final ErrorCollection.Reason ERROR_REASON = ErrorCollection.Reason.NOT_FOUND;

    @Mock
    private ProjectService service;

    private final MockApplicationUser adminUser = new MockApplicationUser("admin");

    private JiraAuthenticationContext authCtx
            = new MockSimpleAuthenticationContext(adminUser, Locale.ENGLISH, new NoopI18nHelper());

    private DefaultProjectContextLocator projectContextLocator;

    private IssueType inProjectIt = new MockIssueType(1001, "InProject");
    private IssueType notInProjectIt = new MockIssueType(1002, "NotInProject");

    private Project projectVisible = new MockProject(10101L, "ONE").setIssueTypes(inProjectIt);
    private Project projectInvisible = new MockProject(10102L, "Two");

    @Before
    public void init() {
        //Always good result for visible project.
        when(service.getProjectByKeyForAction(authCtx.getUser(), projectVisible.getKey(), ProjectAction.EDIT_PROJECT_CONFIG))
                .thenReturn(new ProjectService.GetProjectResult(projectVisible));

        final SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        errorCollection.addErrorMessage(ERROR_MESSAGE);
        errorCollection.addReason(ERROR_REASON);

        //Always error result for invisible project.
        when(service.getProjectByKeyForAction(authCtx.getUser(), projectInvisible.getKey(), ProjectAction.EDIT_PROJECT_CONFIG))
                .thenReturn(new ProjectService.GetProjectResult(errorCollection));

        projectContextLocator = new DefaultProjectContextLocator(service, authCtx);
    }

    @Test
    public void errorReturnedOnBadProject() {
        final ServiceOutcome<ProjectContext> context = projectContextLocator.getContext(projectInvisible.getKey(), 10101L);
        assertThat(context, ServiceOutcomeMatchers.errorMatcher().addErrorMessage(ERROR_MESSAGE, ERROR_REASON));
    }

    @Test
    public void errorReturnedWhenIssueTypeNotInProject() {
        final long issueTypeId = Long.parseLong(notInProjectIt.getId());
        final ServiceOutcome<ProjectContext> context = projectContextLocator.getContext(projectVisible.getKey(), issueTypeId);
        assertThat(context, ServiceOutcomeMatchers.errorMatcher().addErrorMessage(NoopI18nHelper.makeTranslation("admin.issue.type.not.in.project"),
                ErrorCollection.Reason.NOT_FOUND));
    }

    @Test
    public void contextReturnedForGoodProjectAndIssueType() {
        final long issueTypeId = Long.parseLong(inProjectIt.getId());
        final ServiceOutcome<ProjectContext> context = projectContextLocator.getContext(projectVisible.getKey(), issueTypeId);
        assertThat(context, ServiceOutcomeMatchers.noError());

        final ProjectContext value = context.getReturnedValue();
        assertThat(value, Matchers.notNullValue(ProjectContext.class));
    }
}
