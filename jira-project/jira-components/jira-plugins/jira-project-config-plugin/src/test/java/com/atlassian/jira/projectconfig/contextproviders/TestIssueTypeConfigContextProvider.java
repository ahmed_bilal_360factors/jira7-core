package com.atlassian.jira.projectconfig.contextproviders;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.atlassian.gzipfilter.org.apache.commons.lang.builder.ToStringBuilder;
import com.atlassian.gzipfilter.org.apache.commons.lang.builder.ToStringStyle;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.issuetype.MockIssueType;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.order.MockOrderFactory;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Iterables.transform;
import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * @since v6.2
 */
public class TestIssueTypeConfigContextProvider {
    private static final String ISSUE_TYPES_DATA_KEY = "issueTypesData";

    private IssueTypeConfigContextProvider contextProvider;
    private ObjectMapper objectMapper;

    @Before
    public void before() {
        contextProvider = new IssueTypeConfigContextProvider(new MockOrderFactory());
        objectMapper = new ObjectMapper();
    }

    @Test
    public void projectIssueTypesAdded() throws IOException {
        MockIssueType type1 = new MockIssueType(10, "A");
        MockIssueType type2 = new MockIssueType(10, "B");
        MockIssueType type3 = new MockIssueType(10, "C");

        MockProject project = new MockProject(10101, "ABC").setIssueTypes(type1, type2, type3);

        checkProviderWithIssueTypes(project, type1, type2, type3);
    }

    @Test
    public void projectIssueTypesAddedInCorrectOrder() throws IOException {
        MockIssueType type1 = new MockIssueType(10, "A");
        type1.setTranslatedName("C");

        MockIssueType type2 = new MockIssueType(10, "B");
        type2.setTranslatedName("B");

        MockIssueType type3 = new MockIssueType(10, "C");
        type3.setTranslatedName("A");

        MockProject project = new MockProject(10101, "ABC").setIssueTypes(type1, type2, type3);

        checkProviderWithIssueTypes(project, type3, type2, type1);
    }

    private void checkProviderWithIssueTypes(Project project, IssueType... types)
            throws IOException {
        final ImmutableMap<String, Object> context = ImmutableMap.<String, Object>of("project", project);
        final Map<String, Object> output = contextProvider.getContextMap(context);

        assertThat(output, Matchers.hasKey(ISSUE_TYPES_DATA_KEY));
        List<TestData> outputData = objectMapper.readValue((String) output.get(ISSUE_TYPES_DATA_KEY), TestData.LIST_TYPE);
        assertThat(outputData, equalTo(TestData.toData(types)));
    }


    public static class TestData {
        private static TypeReference<List<TestData>> LIST_TYPE = new TypeReference<List<TestData>>() {
        };

        private static Function<IssueType, TestData> TO_DATA_FUNC = new Function<IssueType, TestData>() {
            @Override
            public TestData apply(final IssueType input) {
                return new TestData(input);
            }
        };

        private static List<TestData> toData(IssueType... type) {
            return copyOf(transform(asList(type), TO_DATA_FUNC));
        }

        @JsonProperty
        private String id;

        @JsonProperty
        private String name;

        public TestData(IssueType issueType) {
            this.id = issueType.getId();
            this.name = issueType.getNameTranslation();
        }

        public TestData() {
        }

        public String getId() {
            return id;
        }

        public void setId(final String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(final String name) {
            this.name = name;
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            final TestData that = (TestData) o;

            if (!id.equals(that.id)) {
                return false;
            }
            if (!name.equals(that.name)) {
                return false;
            }

            return true;
        }

        @Override
        public int hashCode() {
            int result = id.hashCode();
            result = 31 * result + name.hashCode();
            return result;
        }

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        }
    }
}
