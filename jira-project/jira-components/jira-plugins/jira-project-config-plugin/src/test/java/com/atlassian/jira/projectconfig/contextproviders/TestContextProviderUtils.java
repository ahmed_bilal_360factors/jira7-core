package com.atlassian.jira.projectconfig.contextproviders;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.order.MockOrderFactory;
import com.atlassian.jira.projectconfig.util.MockProjectConfigRequestCache;
import com.atlassian.jira.projectconfig.util.ProjectConfigRequestCache;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.collect.MapBuilder;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Test for {@link ContextProviderUtils}
 *
 * @since v4.4
 */
@RunWith(MockitoJUnitRunner.class)
public class TestContextProviderUtils {
    @Mock
    private Project project;
    @Mock
    private I18nHelper i18nHelper;
    @Mock
    private ContextProviderHelper contextProviderHelper;

    private ProjectConfigRequestCache cache;
    private MockOrderFactory comparatorFactory;

    @Before
    public void setUp() {
        cache = new MockProjectConfigRequestCache();
        comparatorFactory = new MockOrderFactory();
    }

    @Test
    public void testDefaultContext() throws Exception {
        // setup
        String projectKey = "KEY";

        cache.setProject(project);

        when(project.getKey()).thenReturn(projectKey);

        when(contextProviderHelper.encode(projectKey)).thenReturn("{encoded}" + projectKey);
        when(contextProviderHelper.getI18nHelper()).thenReturn(i18nHelper);
        when(contextProviderHelper.hasAdminPermission()).thenReturn(false);
        when(contextProviderHelper.hasProjectAdminPermission(project)).thenReturn(true);

        // execute
        final ContextProviderUtils contextProviderUtils = new ContextProviderUtils(cache, comparatorFactory, contextProviderHelper);

        // verify
        final Map<String, Object> expectedMap = MapBuilder.<String, Object>newBuilder()
                .add(ContextProviderUtils.CONTEXT_PROJECT_KEY, project)
                .add(ContextProviderUtils.CONTEXT_IS_ADMIN_KEY, false)
                .add(ContextProviderUtils.CONTEXT_IS_PROJECT_ADMIN_KEY, true)
                .add(ContextProviderUtils.CONTEXT_I18N_KEY, i18nHelper)
                .add(ContextProviderUtils.CONTEXT_PROJECT_KEY_ENCODED, "{encoded}KEY")
                .toMap();

        assertEquals(expectedMap, contextProviderUtils.getDefaultContext());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFlattenErrorsNoArgument() {
        // setup
        final ContextProviderUtils contextProviderUtils = new ContextProviderUtils(cache, comparatorFactory, contextProviderHelper);

        // execute
        contextProviderUtils.flattenErrors(null);
    }

    @Test
    public void testFlattenErrors() {
        // setup
        SimpleErrorCollection collection = new SimpleErrorCollection();
        collection.addErrorMessage("one");
        collection.addErrorMessage("two");
        collection.addErrorMessage("three");
        collection.addError("random", "three");
        collection.addError("random", "four");

        final ContextProviderUtils contextProviderUtils = new ContextProviderUtils(cache, comparatorFactory, contextProviderHelper);
        // execute
        Set<String> actualErrors = contextProviderUtils.flattenErrors(collection);

        // verify
        Set<String> expectedErrors = new HashSet<String>();
        expectedErrors.addAll(collection.getErrorMessages());
        expectedErrors.addAll(collection.getErrors().values());
        assertEquals(actualErrors.size(), expectedErrors.size());
        assertTrue(expectedErrors.containsAll(actualErrors));
    }
}
