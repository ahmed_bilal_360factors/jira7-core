package com.atlassian.jira.projectconfig.contextproviders;

import com.atlassian.jira.bc.project.version.VersionService;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.mock.project.MockVersion;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.projectconfig.util.TabUrlFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.DateFieldFormat;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.collect.MapBuilder;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import static com.atlassian.jira.projectconfig.contextproviders.VersionsSummaryPanelContextProvider.SimpleVersion;
import static java.lang.String.format;
import static java.lang.String.valueOf;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * @since v4.4
 */
public class TestVersionsSummaryPanelContextProvider {
    private static final String CONFIG_URL = "?panel=versions&projectKey=";

    @Rule
    public final InitMockitoMocks initMocks = new InitMockitoMocks(this);

    @Mock
    private ContextProviderUtils utils;

    @Mock
    private VersionService versionService;

    @Mock
    private TabUrlFactory tabFactory;

    @Mock
    DateFieldFormat dateFieldFormat;

    private MockApplicationUser user;
    private JiraAuthenticationContext context;

    @Before
    public void setUp() throws Exception {
        user = new MockApplicationUser("bbain");
        context = new MockSimpleAuthenticationContext(user, Locale.ENGLISH, new NoopI18nHelper());
    }

    @Test
    public void testNone() throws Exception {
        ErrorCollection errorCollection = new SimpleErrorCollection();
        Project project = new MockProject(48484848L, "KEY");

        Map<String, Object> arguments = MapBuilder.<String, Object>build("argument", true);

        when(utils.getProject()).thenReturn(project);
        when(versionService.getVersionsByProject(user, project))
                .thenReturn(new VersionService.VersionsResult(errorCollection, Collections.<Version>emptyList()));
        when(utils.flattenErrors(errorCollection)).thenReturn(Collections.<String>emptySet());
        when(tabFactory.forVersions()).thenReturn(CONFIG_URL);

        VersionsSummaryPanelContextProvider testing = new VersionsSummaryPanelContextProvider(utils, versionService, context, tabFactory, dateFieldFormat);
        Map<String, Object> actualContext = testing.getContextMap(arguments);

        MapBuilder<String, Object> expectedContext = MapBuilder.newBuilder(arguments)
                .add("versions", Collections.<Version>emptyList())
                .add("errors", Collections.<String>emptySet())
                .add("totalSize", 0)
                .add("actualSize", 0)
                .add("manageVersionLink", CONFIG_URL);

        assertEquals(expectedContext.toMap(), actualContext);
    }

    @Test
    public void testAll() throws Exception {
        ErrorCollection errorCollection = new SimpleErrorCollection();
        Project project = new MockProject(48484848L, "KEY2");

        Map<String, Object> arguments = MapBuilder.<String, Object>build("argument", true);

        MockVersion version1 = new MockVersion(162727, "Jack1");
        MockVersion version2 = new MockVersion(162728, "Jack2");
        version2.setReleaseDate(new Date(1200000000000L));
        MockVersion version3 = new MockVersion(162799, "Jack3");
        version3.setReleased(true);
        version3.setReleaseDate(new Date(1300000000000L));
        MockVersion version4 = new MockVersion(162800, "Jack4");
        version4.setArchived(true);
        version4.setReleaseDate(new Date(1400000000000L));

        List<Version> versions = Arrays.<Version>asList(version1, version2, version3, version4);

        when(utils.getProject()).thenReturn(project);
        when(versionService.getVersionsByProject(user, project))
                .thenReturn(new VersionService.VersionsResult(errorCollection, versions));
        when(utils.flattenErrors(errorCollection)).thenReturn(Collections.<String>emptySet());
        when(tabFactory.forVersions()).thenReturn(CONFIG_URL);

        List<SimpleVersion> simpleVersions = new ArrayList<SimpleVersion>(versions.size());
        int count = 0;
        for (Version version : reverse(versions)) {
            if (!version.isArchived()) {
                boolean overdue = version.getId() % 2 == 0;
                String releaseDate = null;
                when(versionService.isOverdue(version)).thenReturn(overdue);
                if (version.getReleaseDate() != null) {
                    releaseDate = valueOf(count);
                    when(dateFieldFormat.format(version.getReleaseDate())).thenReturn(releaseDate);
                }
                simpleVersions.add(new SimpleVersion(version.getName(), version.isReleased(), version.isArchived(),
                        overdue, releaseDate));
                count++;
            }
        }

        VersionsSummaryPanelContextProvider testing = new VersionsSummaryPanelContextProvider(utils, versionService, context, tabFactory, dateFieldFormat);
        Map<String, Object> actualContext = testing.getContextMap(arguments);

        MapBuilder<String, Object> expectedContext = MapBuilder.newBuilder(arguments)
                .add("versions", simpleVersions)
                .add("errors", Collections.<String>emptySet())
                .add("totalSize", simpleVersions.size())
                .add("actualSize", simpleVersions.size())
                .add("manageVersionLink", CONFIG_URL);

        assertEquals(expectedContext.toMap(), actualContext);
    }

    @Test
    public void testSome() throws Exception {
        ErrorCollection errorCollection = new SimpleErrorCollection();

        Project project = new MockProject(48484848L, "TEST4");

        Map<String, Object> arguments = MapBuilder.<String, Object>build("argument", true);

        MockVersion version1 = new MockVersion(162727, "Jack1");
        version1.setArchived(true);
        MockVersion version2 = new MockVersion(162728, "Jack2");
        version2.setReleaseDate(new Date());
        version2.setReleased(true);

        List<Version> versions = Lists.<Version>newArrayList(version1, version2);

        for (int i = 0; i < 20; i++) {
            versions.add(new MockVersion(i, format("Version-%d", i)));
        }


        when(utils.getProject()).thenReturn(project);
        when(versionService.getVersionsByProject(user, project))
                .thenReturn(new VersionService.VersionsResult(errorCollection, versions));
        when(utils.flattenErrors(errorCollection)).thenReturn(Collections.<String>emptySet());
        when(tabFactory.forVersions()).thenReturn(CONFIG_URL);

        List<SimpleVersion> simpleVersions = new ArrayList<SimpleVersion>(versions.size());
        int count = 0;
        for (Version version : reverse(versions)) {
            if (!version.isArchived()) {
                if (simpleVersions.size() < 5) {
                    boolean overdue = version.getId() % 2 == 0;
                    String releaseDate = null;
                    when(versionService.isOverdue(version)).thenReturn(overdue);
                    if (version.getReleaseDate() != null) {
                        releaseDate = valueOf(count);
                        when(dateFieldFormat.format(version.getReleaseDate())).thenReturn(releaseDate);
                    }
                    simpleVersions.add(new SimpleVersion(version.getName(), version.isReleased(), version.isArchived(), overdue, releaseDate));
                }
                count++;
            }
        }

        VersionsSummaryPanelContextProvider testing = new VersionsSummaryPanelContextProvider(utils, versionService, context, tabFactory, dateFieldFormat);
        Map<String, Object> actualContext = testing.getContextMap(arguments);

        MapBuilder<String, Object> expectedContext = MapBuilder.newBuilder(arguments)
                .add("versions", simpleVersions)
                .add("errors", Collections.<String>emptySet())
                .add("totalSize", count)
                .add("actualSize", simpleVersions.size())
                .add("manageVersionLink", CONFIG_URL);

        assertEquals(expectedContext.toMap(), actualContext);
    }

    @Test
    public void testErrors() throws Exception {
        final ErrorCollection errorCollection = new SimpleErrorCollection();
        errorCollection.addErrorMessage("brenden");
        final Set<String> expectedErrors = new HashSet<String>(Arrays.asList("error1", "error2"));

        Project project = new MockProject(48484848L, "TEST6");

        Map<String, Object> arguments = MapBuilder.<String, Object>build("argument", true);


        when(utils.getProject()).thenReturn(project);
        when(versionService.getVersionsByProject(user, project))
                .thenReturn(new VersionService.VersionsResult(errorCollection));
        when(utils.flattenErrors(errorCollection)).thenReturn(expectedErrors);
        when(tabFactory.forVersions()).thenReturn(CONFIG_URL);

        VersionsSummaryPanelContextProvider testing = new VersionsSummaryPanelContextProvider(utils, versionService, context, tabFactory, dateFieldFormat);
        Map<String, Object> actualContext = testing.getContextMap(arguments);

        MapBuilder<String, Object> expectedContext = MapBuilder.newBuilder(arguments)
                .add("versions", Collections.<Version>emptyList())
                .add("errors", expectedErrors)
                .add("totalSize", 0)
                .add("actualSize", 0)
                .add("manageVersionLink", CONFIG_URL);

        assertEquals(expectedContext.toMap(), actualContext);
    }

    private <T> List<T> reverse(Collection<? extends T> ver) {
        List<T> reverseList = new ArrayList<T>(ver);
        Collections.reverse(reverseList);
        return reverseList;
    }
}
