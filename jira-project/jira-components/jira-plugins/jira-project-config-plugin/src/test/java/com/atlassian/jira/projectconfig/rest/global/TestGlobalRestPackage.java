package com.atlassian.jira.projectconfig.rest.global;

import com.atlassian.jira.projectconfig.rest.interceptor.SecurityInterceptor;
import com.atlassian.plugins.rest.common.interceptor.InterceptorChain;
import org.hamcrest.Matchers;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @since v6.1
 */
public class TestGlobalRestPackage {
    @Test
    public void interceptors() {
        final Package p = Package.getPackage("com.atlassian.jira.projectconfig.rest.global");
        final InterceptorChain interceptorChain = p.getAnnotation(InterceptorChain.class);
        assertNotNull(interceptorChain);
        assertThat(interceptorChain.value(), Matchers.<Class<?>>arrayContaining(SecurityInterceptor.class));
    }
}
