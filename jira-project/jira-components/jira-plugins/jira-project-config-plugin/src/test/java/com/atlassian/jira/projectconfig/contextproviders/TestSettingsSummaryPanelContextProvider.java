package com.atlassian.jira.projectconfig.contextproviders;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.jira.bc.project.projectoperation.ProjectOperationManager;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.plugin.projectoperation.AbstractPluggableProjectOperation;
import com.atlassian.jira.plugin.projectoperation.PluggableProjectOperation;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.jira.util.collect.MapBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.ofbiz.core.entity.GenericEntityException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * @since v4.4
 */
@RunWith(MockitoJUnitRunner.class)
public class TestSettingsSummaryPanelContextProvider {
    @Mock
    private ProjectOperationManager projectOperationManager;
    @Mock
    private ContextProviderUtils contextProviderUtils;
    @Mock
    private ApplicationLinkService applicationLinkService;
    @Mock
    private InternalHostApplication hostApplication;

    private MockApplicationUser user;
    private MockApplicationUser oldUser;
    private JiraAuthenticationContext authenticationContext;

    @Before
    public void setUp() throws Exception {
        user = new MockApplicationUser("bbain");
        oldUser = new MockApplicationUser("bbain");
        authenticationContext = new MockSimpleAuthenticationContext(user, Locale.ENGLISH, new NoopI18nHelper());
    }

    @Test
    public void testHappyPath() throws GenericEntityException {
        // setup
        final MockProject project = new MockProject(167L, "HSP");
        final Map<String, Object> argument = MapBuilder.<String, Object>build("argument", true);

        when(contextProviderUtils.getProject()).thenReturn(project);
        when(projectOperationManager.getVisibleProjectOperations(project, user)).thenReturn(operations("one", "two"));

        when(applicationLinkService.getApplicationLinks()).thenReturn(Collections.<ApplicationLink>emptyList());

        SettingsSummaryPanelContextProvider provider = new SettingsSummaryPanelContextProvider(projectOperationManager,
                contextProviderUtils, authenticationContext, applicationLinkService, null);

        // execute
        Map<String, Object> actualMap = provider.getContextMap(argument);

        // verify
        MapBuilder<String, Object> expectedContext = MapBuilder.<String, Object>newBuilder(argument)
                .add("pluginsHtml", Arrays.asList("one", "two"))
                .add("showAppLinks", false);

        assertEquals(expectedContext.toMap(), actualMap);
    }

    private static List<PluggableProjectOperation> operations(String... values) {
        List<PluggableProjectOperation> operations = new ArrayList<PluggableProjectOperation>();
        for (final String value : values) {
            operations.add(new AbstractPluggableProjectOperation() {
                @Override
                public String getHtml(Project project, ApplicationUser user) {
                    return value;
                }

                @Override
                public boolean showOperation(Project project, ApplicationUser user) {
                    return true;
                }
            });
        }
        return operations;
    }

    private static <T extends Comparable<? super T>> Comparator<T> naturalComparator() {
        return new Comparator<T>() {
            public int compare(T o1, T o2) {
                return o1.compareTo(o2);
            }
        };
    }
}
