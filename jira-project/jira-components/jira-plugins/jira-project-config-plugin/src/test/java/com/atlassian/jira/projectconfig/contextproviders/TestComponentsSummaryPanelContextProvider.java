package com.atlassian.jira.projectconfig.contextproviders;

import com.atlassian.jira.bc.project.component.MockProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponentService;
import com.atlassian.jira.plugin.profile.UserFormat;
import com.atlassian.jira.plugin.userformat.ProfileLinkUserFormat;
import com.atlassian.jira.plugin.userformat.UserFormats;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.util.TabUrlFactory;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.collect.MapBuilder;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import static com.atlassian.jira.projectconfig.contextproviders.ComponentsSummaryPanelContextProvider.SimpleComponent;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.notNull;
import static org.mockito.Mockito.when;

/**
 * @since v4.4
 */
@RunWith(MockitoJUnitRunner.class)
public class TestComponentsSummaryPanelContextProvider {
    private static final String COMPONENT_LEAD_ID = "component-summary-panel-lead";
    private static final String URL = "something";

    @Mock
    private ContextProviderUtils utils;
    @Mock
    private ProjectComponentService pcs;
    @Mock
    private UserFormats ufm;
    @Mock
    private UserFormat userFormat;
    @Mock
    private TabUrlFactory tabUrlFactory;

    @Test
    public void testGetContextMapNone() throws Exception {
        // set up
        final Map<String, Object> arguments = MapBuilder.<String, Object>build("argument", true);
        final Project mockProject = new MockProject(181818L, "KEY");

        when(utils.getProject()).thenReturn(mockProject);
        when(pcs.findAllForProject(notNull(ErrorCollection.class), eq(mockProject.getId()))).thenReturn(Collections.<ProjectComponent>emptyList());
        when(ufm.forType(ProfileLinkUserFormat.TYPE)).thenReturn(userFormat);
        when(utils.flattenErrors(notNull(ErrorCollection.class))).thenReturn(Collections.<String>emptySet());
        when(tabUrlFactory.forComponents()).thenReturn(URL);

        // execute
        final ComponentsSummaryPanelContextProvider testing = new ComponentsSummaryPanelContextProvider(utils, pcs, ufm, tabUrlFactory);
        final Map<String, Object> actualContext = testing.getContextMap(arguments);

        // verify
        final MapBuilder<String, Object> expectedContext = MapBuilder.newBuilder("components", Collections.<Object>emptyList(),
                "errors", Collections.<String>emptySet(),
                "totalSize", 0,
                "actualSize", 0);
        expectedContext.add("manageUrl", URL);
        expectedContext.addAll(arguments);

        assertEquals(expectedContext.toMap(), actualContext);
    }

    @Test
    public void testGetContextMapAll() throws Exception {
        // set up
        final String leaderFormatted = "<user>leader</user>";
        final Map<String, Object> arguments = MapBuilder.<String, Object>build("argument", true);
        final Project mockProject = new MockProject(181818L, "KEY");
        final MockProjectComponent projectComponent1 = new MockProjectComponent(67L, "name1").setLead("leader");
        final MockProjectComponent projectComponent2 = new MockProjectComponent(56L, "name2");
        final List<ProjectComponent> components = Lists.<ProjectComponent>newArrayList(projectComponent1, projectComponent2);

        when(utils.getProject()).thenReturn(mockProject);
        when(pcs.findAllForProject(notNull(ErrorCollection.class), eq(mockProject.getId()))).thenReturn(components);
        when(ufm.forType(ProfileLinkUserFormat.TYPE)).thenReturn(userFormat);
        when(utils.flattenErrors(notNull(ErrorCollection.class))).thenReturn(Collections.<String>emptySet());
        when(tabUrlFactory.forComponents()).thenReturn(URL);

        when(userFormat.format(projectComponent1.getLead(), COMPONENT_LEAD_ID)).thenReturn(leaderFormatted);

        // execute
        final ComponentsSummaryPanelContextProvider testing = new ComponentsSummaryPanelContextProvider(utils, pcs, ufm, tabUrlFactory);
        final Map<String, Object> actualContext = testing.getContextMap(arguments);

        // verify
        final List<SimpleComponent> simpleComponents = Lists.newArrayList(
                new SimpleComponent(projectComponent1.getName(), leaderFormatted),
                new SimpleComponent(projectComponent2.getName(), null));

        final MapBuilder<String, Object> expectedContext = MapBuilder.newBuilder("components", simpleComponents,
                "errors", Collections.<String>emptySet(),
                "totalSize", 2,
                "actualSize", 2);
        expectedContext.add("manageUrl", URL);
        expectedContext.addAll(arguments);

        assertEquals(expectedContext.toMap(), actualContext);
    }

    @Test
    public void testGetContextMapSome() throws Exception {
        // setup
        final Map<String, Object> arguments = MapBuilder.<String, Object>build("argument", true);
        final Project mockProject = new MockProject(181818L, "KEY");
        final List<ProjectComponent> components = Lists.newArrayList();

        for (int i = 0; i < 20; i++) {
            String user = i % 2 == 0 ? String.format("User-%d", i) : null;
            components.add(new MockProjectComponent((long) i, String.format("Component-%d", i)).setLead(user));
        }

        when(utils.getProject()).thenReturn(mockProject);
        when(pcs.findAllForProject(notNull(ErrorCollection.class), eq(mockProject.getId()))).thenReturn(components);
        when(ufm.forType(ProfileLinkUserFormat.TYPE)).thenReturn(userFormat);
        when(utils.flattenErrors(notNull(ErrorCollection.class))).thenReturn(Collections.<String>emptySet());
        when(tabUrlFactory.forComponents()).thenReturn(URL);

        List<SimpleComponent> expectedComponents = Lists.newArrayList();

        for (ListIterator<ProjectComponent> iterator = components.listIterator(); iterator.hasNext() && iterator.nextIndex() < 5; ) {
            ProjectComponent next = iterator.next();
            String user = null;
            if (next.getLead() != null) {
                user = String.format("<user>%s</user>", next.getLead());
                when(userFormat.format(next.getLead(), COMPONENT_LEAD_ID))
                        .thenReturn(user);
            }
            expectedComponents.add(new SimpleComponent(next.getName(), user));
        }

        // execute
        final ComponentsSummaryPanelContextProvider testing = new ComponentsSummaryPanelContextProvider(utils, pcs, ufm, tabUrlFactory);
        final Map<String, Object> actualContext = testing.getContextMap(arguments);

        // verify
        final MapBuilder<String, Object> expectedContext = MapBuilder.newBuilder("components", expectedComponents,
                "errors", Collections.<String>emptySet(),
                "totalSize", 20,
                "actualSize", 5);
        expectedContext.add("manageUrl", URL);
        expectedContext.addAll(arguments);

        assertEquals(expectedContext.toMap(), actualContext);
    }

    @Test
    public void testGetContextMapError() throws Exception {
        // set up
        final Map<String, Object> arguments = MapBuilder.<String, Object>build("argument", true);
        final Project mockProject = new MockProject(181818L, "KEY");
        final ProjectComponent component = new MockProjectComponent(78L, "Name");
        final List<ProjectComponent> components = Lists.newArrayList(component);
        final String error1 = "Error1";
        final String error2 = "Error1";

        when(utils.getProject()).thenReturn(mockProject);
        when(ufm.forType(ProfileLinkUserFormat.TYPE)).thenReturn(userFormat);
        when(pcs.findAllForProject(notNull(ErrorCollection.class), eq(mockProject.getId()))).thenReturn(components);
        when(utils.flattenErrors(notNull(ErrorCollection.class)))
                .thenReturn(Sets.<String>newLinkedHashSet(Arrays.asList(error1, error2)));
        when(tabUrlFactory.forComponents()).thenReturn(URL);

        final List<SimpleComponent> expectedComponents = Lists.newArrayList(new SimpleComponent(component.getName(), null));

        // execute
        final ComponentsSummaryPanelContextProvider testing = new ComponentsSummaryPanelContextProvider(utils, pcs, ufm, tabUrlFactory);
        final Map<String, Object> actualContext = testing.getContextMap(arguments);

        // verify
        final MapBuilder<String, Object> expectedContext = MapBuilder.newBuilder("components", expectedComponents,
                "errors", Sets.<Object>newLinkedHashSet(Arrays.asList(error1, error2)),
                "totalSize", 1,
                "actualSize", 1);
        expectedContext.add("manageUrl", URL);
        expectedContext.addAll(arguments);

        assertEquals(expectedContext.toMap(), actualContext);
    }
}
