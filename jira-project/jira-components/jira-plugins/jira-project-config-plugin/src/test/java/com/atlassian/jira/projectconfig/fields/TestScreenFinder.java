package com.atlassian.jira.projectconfig.fields;

import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.issue.fields.screen.MockFieldScreen;
import com.atlassian.jira.issue.fields.screen.MockFieldScreenScheme;
import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenSchemeManager;
import com.atlassian.jira.issue.operation.IssueOperations;
import com.atlassian.jira.mock.issue.MockIssue;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestScreenFinder {
    @Mock
    private IssueTypeScreenSchemeManager issueTypeScreenSchemeManager;
    private ScreenFinder finder;

    @Before
    public void setup() {
        finder = new ScreenFinder(issueTypeScreenSchemeManager);
    }

    @Test
    public void getIssueScreens() {
        MockIssue issue = new MockIssue(1000, "RandomKey-1");

        MockFieldScreen firstScreen = new MockFieldScreen(10);
        MockFieldScreen secondScreen = new MockFieldScreen(11);
        MockFieldScreen thirdScreen = new MockFieldScreen(12);

        final MockFieldScreenScheme scheme = new MockFieldScreenScheme()
                .setDefaultScreen(firstScreen)
                .addFieldScreen(IssueOperations.EDIT_ISSUE_OPERATION, secondScreen)
                .addFieldScreen(IssueOperations.MOVE_ISSUE_OPERATION, thirdScreen);

        when(issueTypeScreenSchemeManager.getFieldScreenScheme(issue)).thenReturn(scheme);

        Set<FieldScreen> actualScreens = Sets.newHashSet(finder.getIssueScreens(issue));
        Set<FieldScreen> expectedScreens = Sets.<FieldScreen>newHashSet(firstScreen, secondScreen);
        assertEquals(expectedScreens, actualScreens);
    }
}
