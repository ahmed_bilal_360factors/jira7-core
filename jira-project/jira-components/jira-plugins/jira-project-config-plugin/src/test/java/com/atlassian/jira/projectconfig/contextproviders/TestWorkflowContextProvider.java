package com.atlassian.jira.projectconfig.contextproviders;

import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.discover.EditWorkflowDiscoverHelper;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.task.MockTaskDescriptor;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.web.action.admin.workflow.WorkflowMigrationResult;
import com.atlassian.jira.workflow.AssignableWorkflowScheme;
import com.atlassian.jira.workflow.MockAssignableWorkflowScheme;
import com.atlassian.jira.workflow.WorkflowSchemeManager;
import com.atlassian.jira.workflow.migration.WorkflowSchemeMigrationTaskAccessor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.Map;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestWorkflowContextProvider {
    public static final String LEARN_MORE_URL = "SOME_URL";
    @Mock
    private ContextProviderUtils contextProviderUtils;
    @Mock
    private WorkflowSchemeManager workflowSchemeManager;
    @Mock
    private WorkflowSchemeMigrationTaskAccessor taskAccessor;
    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;
    @Mock
    EditWorkflowDiscoverHelper editWorkflowDiscoverHelper;
    @Mock
    ApplicationUser user;

    private WorkflowContextProvider workflowContextProvider;

    private Project project = new MockProject(1L);

    @Before
    public void setUp() {
        workflowContextProvider = new WorkflowContextProvider(contextProviderUtils, taskAccessor, workflowSchemeManager, editWorkflowDiscoverHelper, jiraAuthenticationContext);
        when(contextProviderUtils.getProject()).thenReturn(project);
        when(jiraAuthenticationContext.getLoggedInUser()).thenReturn(user);
        when(editWorkflowDiscoverHelper.shouldShowEditWorkflowDiscover(user)).thenReturn(true);
        when(editWorkflowDiscoverHelper.getLearnMoreUrl(project)).thenReturn(LEARN_MORE_URL);
    }

    @Test
    public void testContextMap() {
        AssignableWorkflowScheme workflowScheme = new MockAssignableWorkflowScheme(1L, "Scheme name", "Scheme description");
        when(workflowSchemeManager.getWorkflowSchemeObj(project)).thenReturn(workflowScheme);

        // No draft, not shared, not migrating.
        when(workflowSchemeManager.hasDraft(workflowScheme)).thenReturn(false);
        when(workflowSchemeManager.getProjectsUsing(workflowScheme)).thenReturn(asList(project));

        MapBuilder<String, Object> expectedMap = MapBuilder.<String, Object>newBuilder()
                .add("hasDraft", false)
                .add("isShared", false)
                .add("isDefault", false)
                .add("showEditWorkflowDiscover", true)
                .add("learnMoreUrl", LEARN_MORE_URL);

        Map<String, Object> actualMap = workflowContextProvider.getContextMap(Collections.<String, Object>emptyMap());
        assertEquals(expectedMap.toMap(), actualMap);

        // With draft, shared, not migrating.
        when(workflowSchemeManager.hasDraft(workflowScheme)).thenReturn(true);
        when(workflowSchemeManager.getProjectsUsing(workflowScheme)).thenReturn(asList(project, new MockProject(2L)));
        when(editWorkflowDiscoverHelper.shouldShowEditWorkflowDiscover(user)).thenReturn(false);

        expectedMap = MapBuilder.<String, Object>newBuilder()
                .add("hasDraft", true)
                .add("isShared", true)
                .add("isDefault", false)
                .add("showEditWorkflowDiscover", false)
                .add("learnMoreUrl", LEARN_MORE_URL);

        actualMap = workflowContextProvider.getContextMap(Collections.<String, Object>emptyMap());
        assertEquals(expectedMap.toMap(), actualMap);

        // With draft, shared, migrating project.
        MockTaskDescriptor<WorkflowMigrationResult> taskDescriptor = new MockTaskDescriptor<WorkflowMigrationResult>();
        when(taskAccessor.getActive(project)).thenReturn(taskDescriptor);

        expectedMap = MapBuilder.<String, Object>newBuilder()
                .add("hasDraft", true)
                .add("isShared", true)
                .add("isDefault", false)
                .add("projectMigration", true)
                .add("progressURL", taskDescriptor.getProgressURL())
                .add("showEditWorkflowDiscover", false)
                .add("learnMoreUrl", LEARN_MORE_URL);

        actualMap = workflowContextProvider.getContextMap(Collections.<String, Object>emptyMap());
        assertEquals(expectedMap.toMap(), actualMap);

        // With draft, shared, migrating scheme.
        when(taskAccessor.getActive(project)).thenReturn(null);
        when(taskAccessor.getActive(workflowScheme)).thenReturn(taskDescriptor);

        expectedMap = MapBuilder.<String, Object>newBuilder()
                .add("hasDraft", true)
                .add("isShared", true)
                .add("isDefault", false)
                .add("projectMigration", false)
                .add("progressURL", taskDescriptor.getProgressURL())
                .add("showEditWorkflowDiscover", false)
                .add("learnMoreUrl", LEARN_MORE_URL);

        actualMap = workflowContextProvider.getContextMap(Collections.<String, Object>emptyMap());
        assertEquals(expectedMap.toMap(), actualMap);
    }
}
