package com.atlassian.jira.projectconfig.issuetypes.fields;

import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.issue.fields.screen.MockFieldScreen;
import com.atlassian.jira.issue.fields.screen.MockFieldScreenScheme;
import com.atlassian.jira.issue.fields.screen.ProjectFieldScreenHelper;
import com.atlassian.jira.issue.fields.screen.issuetype.MockIssueTypeScreenScheme;
import com.atlassian.jira.issue.fields.screen.issuetype.MockIssueTypeScreenSchemeManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.issuetype.MockIssueType;
import com.atlassian.jira.issue.operation.IssueOperations;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.beans.ProjectContext;
import com.atlassian.jira.projectconfig.order.MockOrderFactory;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

/**
 * @since v6.2
 */
@RunWith(MockitoJUnitRunner.class)
public class TestDefaultIssueTypeConfigViewHelper {
    private MockIssueTypeScreenScheme issueTypeScreenScheme;
    private MockFieldScreen viewScreen;
    private MockFieldScreenScheme screenScheme;

    @Mock
    private ProjectFieldScreenHelper projectFieldScreenHelper;

    private DefaultIssueTypeConfigFieldsHelper viewHelper;

    private IssueType issueType = new MockIssueType(1, "OneIt");
    private IssueType issueType2 = new MockIssueType(2, "ZTwoIt");
    private IssueType issueType3 = new MockIssueType(3, "AThreeIt");
    private Project project = new MockProject(1, "One").setIssueTypes(issueType, issueType2, issueType3);

    @Before
    public void setup() {
        //Create ITSS:
        // * -> SomeScreen
        // issueType -> viewScreen
        final MockIssueTypeScreenSchemeManager issueTypeScreenSchemeManager = new MockIssueTypeScreenSchemeManager();
        issueTypeScreenScheme = issueTypeScreenSchemeManager.createIssueTypeScreenScheme(project);
        screenScheme = issueTypeScreenScheme.createEntity(issueType).createFieldScreenScheme(0);
        viewScreen = screenScheme.createFieldScreen(IssueOperations.VIEW_ISSUE_OPERATION);

        //Must create default mapping.
        issueTypeScreenScheme.createDefaultEntity().createFieldScreenScheme(1).createDefaultScreen();

        viewHelper = new DefaultIssueTypeConfigFieldsHelper(issueTypeScreenSchemeManager, projectFieldScreenHelper, new MockOrderFactory());
    }

    @Test
    public void findTheRightScreen() {
        final IssueTypeConfigFieldsHelper.FieldsResult fieldsResult
                = viewHelper.getFieldsData(new ProjectContext(project, issueType));

        final ResultMatcher matcher = new ResultMatcher()
                .screen(viewScreen);

        assertThat(fieldsResult, matcher);
    }

    @Test
    public void findTheRightSharedProjects() {
        final MockProject other1 = new MockProject(18, "Other");
        final MockProject other2 = new MockProject(19, "Other Project Again");

        when(projectFieldScreenHelper.getProjectsForFieldScreen(viewScreen))
                .thenReturn(Arrays.asList(project, other1, other2));

        final IssueTypeConfigFieldsHelper.FieldsResult fieldsResult
                = viewHelper.getFieldsData(new ProjectContext(project, issueType));

        final ResultMatcher matcher = new ResultMatcher()
                .screen(viewScreen)
                .sharedProjects(other1, other2);

        assertThat(fieldsResult, matcher);
    }

    @Test
    public void findTheRightSharedIssueTypes() {
        issueTypeScreenScheme.createEntity(issueType2).setFieldScreenScheme(screenScheme);
        issueTypeScreenScheme.createEntity(issueType3).createFieldScreenScheme(191919).createDefaultScreen();

        final IssueTypeConfigFieldsHelper.FieldsResult fieldsResult
                = viewHelper.getFieldsData(new ProjectContext(project, issueType));

        final ResultMatcher matcher = new ResultMatcher()
                .screen(viewScreen)
                .sharedIssueTypes(issueType2);

        assertThat(fieldsResult, matcher);
    }

    @Test
    public void returnSharedIssueTypesInOrder() {
        issueTypeScreenScheme.getDefaultEntity().setFieldScreenScheme(screenScheme);

        final IssueTypeConfigFieldsHelper.FieldsResult fieldsResult
                = viewHelper.getFieldsData(new ProjectContext(project, issueType));

        final ResultMatcher matcher = new ResultMatcher()
                .screen(viewScreen)
                .sharedIssueTypes(issueType3, issueType2);

        assertThat(fieldsResult, matcher);
    }

    public static class ResultMatcher extends TypeSafeMatcher<IssueTypeConfigFieldsHelper.FieldsResult> {
        private FieldScreen fieldScreen;
        private List<Project> sharedProjects = Collections.emptyList();
        private List<IssueType> sharedIssueTypes = Collections.emptyList();

        @Override
        protected boolean matchesSafely(final IssueTypeConfigFieldsHelper.FieldsResult item) {
            return fieldScreen.equals(item.getFieldScreen())
                    && sharedProjects.equals(item.getSharedBy().getProjects())
                    && sharedIssueTypes.equals(item.getSharedBy().getIssueTypes());
        }

        @Override
        protected void describeMismatchSafely(final IssueTypeConfigFieldsHelper.FieldsResult item, final Description mismatchDescription) {
            mismatchDescription.appendText(asString(item));
        }

        private String asString(final IssueTypeConfigFieldsHelper.FieldsResult item) {
            return asString(item.getFieldScreen(), item.getSharedBy().getProjects(), item.getSharedBy().getIssueTypes());
        }

        @Override
        public void describeTo(final Description description) {
            description.appendText(asString(fieldScreen, sharedProjects, sharedIssueTypes));
        }

        private String asString(final FieldScreen field, final List<Project> sharedProjects,
                                final List<IssueType> sharedIssueTypes) {
            return String.format("[field: %s, projects: %s, issueTypes: %s]", field, sharedProjects, sharedIssueTypes);
        }

        public ResultMatcher screen(FieldScreen fieldScreen) {
            this.fieldScreen = fieldScreen;
            return this;
        }

        public ResultMatcher sharedProjects(Project... projects) {
            this.sharedProjects = Arrays.asList(projects);
            return this;
        }

        public ResultMatcher sharedIssueTypes(IssueType... types) {
            this.sharedIssueTypes = Arrays.asList(types);
            return this;
        }
    }
}
