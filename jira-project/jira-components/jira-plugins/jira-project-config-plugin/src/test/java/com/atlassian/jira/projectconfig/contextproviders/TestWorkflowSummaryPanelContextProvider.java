package com.atlassian.jira.projectconfig.contextproviders;


import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.projectconfig.order.MockOrderFactory;
import com.atlassian.jira.projectconfig.shared.SharedEntitiesHelper;
import com.atlassian.jira.projectconfig.shared.SharedIssueTypeWorkflowData;
import com.atlassian.jira.projectconfig.util.TabUrlFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.jira.util.UrlBuilder;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.WorkflowSchemeManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.ofbiz.core.entity.GenericEntityException;

import static com.atlassian.jira.projectconfig.contextproviders.WorkflowSummaryPanelContextProvider.SimpleWorkflow;
import static com.google.common.collect.Lists.newArrayList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.4
 */
@RunWith(MockitoJUnitRunner.class)
public class TestWorkflowSummaryPanelContextProvider {
    @Mock
    private ContextProviderUtils utils;
    @Mock
    private WorkflowSchemeManager schemeManager;
    @Mock
    private TabUrlFactory urlFactory;
    @Mock
    private WorkflowManager workflowManager;
    @Mock
    private SharedEntitiesHelper sharedHelper;

    private JiraAuthenticationContext context;
    private MockOrderFactory comparatorFactory;

    @Before
    public void setUp() throws Exception {
        context = new MockSimpleAuthenticationContext(new MockApplicationUser("bbain"), Locale.ENGLISH,
                new NoopI18nHelper());
        comparatorFactory = new MockOrderFactory();
    }

    @Test
    public void testSchemeWithoutDefaultWorkflow() throws GenericEntityException {
        // setup
        String changeLink = "test";
        MockProject project = new MockProject(678L, "ABC").setIssueTypes("one", "two", "three", "four");

        Map<String, String> workflowMap = MapBuilder.<String, String>newBuilder("one", "abc")
                .add("two", "zzzz").add("three", "ccc").add("four", null).toMap();

        SharedIssueTypeWorkflowData sharedBy = new SharedIssueTypeWorkflowData(newArrayList(project));
        when(sharedHelper.getSharedData(eq(project), any(IssueType.class), anyString())).thenReturn(sharedBy);

        when(utils.getProject()).thenReturn(project);
        when(schemeManager.getWorkflowMap(project)).thenReturn(workflowMap);
        when(schemeManager.isUsingDefaultScheme(project)).thenReturn(false);
        JiraWorkflow workflow = getWorkflow("abc", "abc-description");
        when(workflowManager.getWorkflow("abc")).thenReturn(workflow);
        workflow = getWorkflow("zzzz", "zzzz-description");
        when(workflowManager.getWorkflow("zzzz")).thenReturn(workflow);
        workflow = getWorkflow("ccc", "ccc-description");
        when(workflowManager.getWorkflow("ccc")).thenReturn(workflow);
        workflow = getWorkflow("jira", null);
        when(workflowManager.getWorkflow("jira")).thenReturn(workflow);
        when(schemeManager.getWorkflowScheme(project.getGenericValue())).thenReturn(null);
        when(urlFactory.forWorkflows()).thenReturn(changeLink);

        WorkflowSummaryPanelContextProvider provider = new WorkflowSummaryPanelContextProvider(utils, schemeManager, context, urlFactory, comparatorFactory, workflowManager, sharedHelper) {
            @Override
            String getWorkflowUrl(String workflowName) {
                return getWorkflowLink(workflowName);
            }
        };

        Map<String, Object> paramMap = MapBuilder.<String, Object>build("param", true);
        // execute
        Map<String, Object> actualMap = provider.getContextMap(paramMap);

        // verify
        MapBuilder<String, Object> expectedMap = MapBuilder.newBuilder(paramMap)
                .add("project", 678L)
                .add("workflows", workflows("jira", sharedBy, "jira", "abc", "ccc", "zzzz"))
                .add("schemeLink", changeLink)
                .add("schemeName", NoopI18nHelper.makeTranslation("admin.schemes.workflows.default"))
                .add("schemeDefault", false);

        assertEquals(expectedMap.toMap(), actualMap);
    }

    @Test
    public void testSchemeWithDefaultWorkflow() throws GenericEntityException {
        // setup
        String changeLink = "http://htfu.com";
        MockProject project = new MockProject(678L, "ABC").setIssueTypes("something");
        MockGenericValue mockGenericValue = new MockGenericValue("dontCare");
        long schemeId = 120L;
        String schemeName = "name";

        mockGenericValue.set("id", schemeId);
        mockGenericValue.set("name", schemeName);

        Map<String, String> workflowMap = MapBuilder.<String, String>newBuilder(null, "abc").toMap();

        SharedIssueTypeWorkflowData sharedBy = new SharedIssueTypeWorkflowData(newArrayList(project));
        when(sharedHelper.getSharedData(eq(project), any(IssueType.class), anyString())).thenReturn(sharedBy);

        when(utils.getProject()).thenReturn(project);
        when(schemeManager.getWorkflowMap(project)).thenReturn(workflowMap);
        JiraWorkflow workflow = getWorkflow("abc", "abc-description");
        when(workflowManager.getWorkflow("abc")).thenReturn(workflow);

        when(schemeManager.isUsingDefaultScheme(project)).thenReturn(false);
        when(schemeManager.getWorkflowScheme(project.getGenericValue())).thenReturn(mockGenericValue);

        when(urlFactory.forWorkflows()).thenReturn(changeLink);

        WorkflowSummaryPanelContextProvider provider = new WorkflowSummaryPanelContextProvider(utils, schemeManager, context, urlFactory, comparatorFactory, workflowManager, sharedHelper) {
            @Override
            String getWorkflowUrl(String workflowName) {
                return getWorkflowLink(workflowName);
            }
        };

        Map<String, Object> paramMap = MapBuilder.<String, Object>build("param", true);

        // execute
        Map<String, Object> actualMap = provider.getContextMap(paramMap);

        // verify
        MapBuilder<String, Object> expectedMap = MapBuilder.newBuilder(paramMap)
                .add("project", 678L)
                .add("workflows", workflows("abc", sharedBy, "abc"))
                .add("schemeName", schemeName)
                .add("schemeDescription", null)
                .add("schemeLink", changeLink)
                .add("schemeDefault", false);

        assertEquals(expectedMap.toMap(), actualMap);
    }

    /**
     * This test hits the case where every issue type is assigned to a workflow and the default workflow is not used.
     * In this case we should not see the default in the list of workflows.
     *
     * @throws org.ofbiz.core.entity.GenericEntityException its a test, who cares.
     */
    @Test
    public void testSchemeAllIssueTypesAssigned() throws GenericEntityException {
        // setup
        MockProject project = new MockProject(678L, "ABC").setIssueTypes("one");
        MockGenericValue mockGenericValue = new MockGenericValue("dontCare");
        long schemeId = 120L;
        String schemeName = "name";
        String schemeDescription = "description";
        String changeLink = "http://htfu.com";

        SharedIssueTypeWorkflowData sharedBy = new SharedIssueTypeWorkflowData(newArrayList(project));
        when(sharedHelper.getSharedData(eq(project), any(IssueType.class), anyString())).thenReturn(sharedBy);

        mockGenericValue.set("id", schemeId);
        mockGenericValue.set("name", schemeName);
        mockGenericValue.set("description", schemeDescription);

        Map<String, String> workflowMap = MapBuilder.<String, String>newBuilder(null, "default", "one", "one").toMap();

        when(utils.getProject()).thenReturn(project);

        when(schemeManager.getWorkflowMap(project)).thenReturn(workflowMap);
        JiraWorkflow workflow = getWorkflow("one", "one-description");
        when(workflowManager.getWorkflow("one")).thenReturn(workflow);

        when(schemeManager.getWorkflowScheme(project.getGenericValue())).thenReturn(mockGenericValue);
        when(schemeManager.isUsingDefaultScheme(project)).thenReturn(true);

        when(urlFactory.forWorkflows()).thenReturn(changeLink);

        WorkflowSummaryPanelContextProvider provider = new WorkflowSummaryPanelContextProvider(utils, schemeManager, context, urlFactory, comparatorFactory, workflowManager, sharedHelper) {
            @Override
            String getWorkflowUrl(String workflowName) {
                return getWorkflowLink(workflowName);
            }
        };

        Map<String, Object> paramMap = MapBuilder.<String, Object>build("param", true);

        // execute
        Map<String, Object> actualMap = provider.getContextMap(paramMap);

        // verify
        MapBuilder<String, Object> expectedMap = MapBuilder.newBuilder(paramMap)
                .add("project", 678L)
                .add("workflows", workflows("abc", sharedBy, "one"))
                .add("schemeName", schemeName)
                .add("schemeDescription", schemeDescription)
                .add("schemeLink", changeLink)
                .add("schemeDefault", true);

        assertEquals(expectedMap.toMap(), actualMap);
    }

    @Test
    public void testSchemeReadError() throws GenericEntityException {
        // setup
        MockProject project = new MockProject(678L, "ABC").setIssueTypes("something");

        SharedIssueTypeWorkflowData sharedBy = new SharedIssueTypeWorkflowData(newArrayList(project));
        when(sharedHelper.getSharedData(eq(project), any(IssueType.class), anyString())).thenReturn(sharedBy);

        Map<String, String> workflowMap = MapBuilder.<String, String>newBuilder(null, "abc").toMap();
        when(utils.getProject()).thenReturn(project);
        when(schemeManager.getWorkflowMap(project)).thenReturn(workflowMap);
        JiraWorkflow workflow = getWorkflow("abc", "abc-description");
        when(workflowManager.getWorkflow("abc")).thenReturn(workflow);

        when(schemeManager.getWorkflowScheme(project.getGenericValue())).thenThrow(new GenericEntityException());

        WorkflowSummaryPanelContextProvider provider = new WorkflowSummaryPanelContextProvider(utils, schemeManager, context, urlFactory, comparatorFactory, workflowManager, sharedHelper) {
            @Override
            String getWorkflowUrl(String workflowName) {
                return getWorkflowLink(workflowName);
            }
        };

        Map<String, Object> paramMap = MapBuilder.<String, Object>build("param", true);

        // execute
        Map<String, Object> actualMap = provider.getContextMap(paramMap);

        // verify
        MapBuilder<String, Object> expectedMap = MapBuilder.newBuilder(paramMap)
                .add("project", 678L)
                .add("workflows", workflows("abc", sharedBy, "abc"))
                .add("error", true);

        assertEquals(expectedMap.toMap(), actualMap);
    }

    @Test
    public void testWorkflowUrl() {
        // setup
        String workflow = "name";
        when(utils.createUrlBuilder("/secure/admin/workflows/ViewWorkflowSteps.jspa?workflowMode=live"))
                .thenReturn(new UrlBuilder("workflow", "UTF-8", false));

        // execute
        WorkflowSummaryPanelContextProvider provider = new WorkflowSummaryPanelContextProvider(utils, schemeManager, context, urlFactory, comparatorFactory, workflowManager, sharedHelper);

        // verify
        assertEquals("workflow?workflowName=" + workflow, provider.getWorkflowUrl(workflow));
    }

    private JiraWorkflow getWorkflow(String name, String description) {
        final JiraWorkflow mockWorkflow = mock(JiraWorkflow.class);

        when(mockWorkflow.getName()).thenReturn(name);
        when(mockWorkflow.getDescription()).thenReturn(description);
        when(mockWorkflow.isSystemWorkflow()).thenReturn(false);
        when(mockWorkflow.isDefault()).thenReturn(JiraWorkflow.DEFAULT_WORKFLOW_NAME.equals(name));
        when(mockWorkflow.getDisplayName()).thenReturn(getDisplayName(name));

        return mockWorkflow;
    }

    private List<SimpleWorkflow> workflows(String defaultName, SharedIssueTypeWorkflowData sharedBy, String... names) {
        List<SimpleWorkflow> flows = new ArrayList<SimpleWorkflow>();
        for (String name : names) {
            flows.add(new SimpleWorkflow(name, getWorkflowLink(name), name.equals("jira") ? null : name + "-description", defaultName.equals(name), false, name.equals("jira"), getDisplayName(name), sharedBy));
        }
        return flows;
    }

    private static String getDisplayName(String name) {
        return "Display:" + name;
    }

    private static String getWorkflowLink(String workflowName) {
        return "workflow?" + workflowName;
    }
}
