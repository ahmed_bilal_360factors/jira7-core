package com.atlassian.jira.projectconfig.contextproviders;

import com.atlassian.jira.projectconfig.util.UrlEncoder;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.util.velocity.VelocityRequestContext;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Test for {@link ContextProviderHelper}
 *
 * @since v6.2
 */
@RunWith(MockitoJUnitRunner.class)
public class TestContextProviderHelper {
    @Mock
    private VelocityRequestContextFactory requestContextFactory;
    @Mock
    private VelocityRequestContext requestContext;
    @Mock
    private UrlEncoder urlEncoder;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private JiraAuthenticationContext authenticationContext;

    @Before
    public void setUp() {

    }

    @Test
    public void testGetBaseUrl() throws Exception {
        String baseurl = "baseurl";

        when(requestContextFactory.getJiraVelocityRequestContext()).thenReturn(requestContext);
        when(requestContext.getBaseUrl()).thenReturn(baseurl);

        final ContextProviderHelper contextProviderHelper = new ContextProviderHelper(urlEncoder, permissionManager,
                authenticationContext, requestContextFactory);
        assertEquals(baseurl, contextProviderHelper.getBaseUrl());
    }

    @Test
    public void testCreateUrlBuilder() throws Exception {
        String baseurl = "baseurl";

        when(requestContextFactory.getJiraVelocityRequestContext()).thenReturn(requestContext);
        when(requestContext.getBaseUrl()).thenReturn(baseurl);

        final ContextProviderHelper providerHelper = new ContextProviderHelper(urlEncoder, permissionManager, authenticationContext, requestContextFactory);
        assertEquals(baseurl, providerHelper.createUrlBuilder(null).asUrlString());
        assertEquals(baseurl, providerHelper.createUrlBuilder("        ").asUrlString());
        assertEquals(baseurl + "jack", providerHelper.createUrlBuilder(" jack  ").asUrlString());
    }
}
