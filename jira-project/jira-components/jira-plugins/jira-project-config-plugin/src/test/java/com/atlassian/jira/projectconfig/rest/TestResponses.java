package com.atlassian.jira.projectconfig.rest;

import javax.ws.rs.core.Response;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.util.ErrorCollection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @since v6.2
 */
@RunWith(MockitoJUnitRunner.class)
public class TestResponses {
    @Test
    public void okResponse() {
        final Response ok = Responses.ok(1);
        assertThat(ok, ResponseMatchers.noCache());
        assertThat(ok, ResponseMatchers.body(Integer.class, 1));
        assertThat(ok, ResponseMatchers.status(Response.Status.OK));
    }

    @Test
    public void forOutcomeReturnsErrorWhenPassedBadOutcome() {
        final String errorMessage = "What Error";
        ServiceOutcome<Integer> outcome = ServiceOutcomeImpl.error(errorMessage, ErrorCollection.Reason.NOT_FOUND);

        final Response error = Responses.forOutcome(outcome);

        assertThat(error, ResponseMatchers.noCache());
        assertThat(error, ResponseMatchers.status(Response.Status.NOT_FOUND));
        assertThat(error, ResponseMatchers.errorBody(errorMessage));
    }

    @Test
    public void forOutcomeReturnsErrorWhenPassedBadOutcomeWithNoReason() {
        final String errorMessage = "What Error";
        ServiceOutcome<Integer> outcome = ServiceOutcomeImpl.error(errorMessage);

        final Response error = Responses.forOutcome(outcome);

        assertThat(error, ResponseMatchers.noCache());
        assertThat(error, ResponseMatchers.status(Response.Status.INTERNAL_SERVER_ERROR));
        assertThat(error, ResponseMatchers.errorBody(errorMessage));
    }

    @Test
    public void forOutcomeReturnsOkResultWhenDataReturned() {
        final int value = 6;
        ServiceOutcome<Integer> outcome = ServiceOutcomeImpl.ok(value);

        final Response error = Responses.forOutcome(outcome);

        assertThat(error, ResponseMatchers.noCache());
        assertThat(error, ResponseMatchers.status(Response.Status.OK));
        assertThat(error, ResponseMatchers.body(Integer.class, value));
    }
}
