package com.atlassian.jira.projectconfig.rest;

import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Response;
import java.util.List;

import static com.atlassian.jira.rest.api.http.CacheControl.never;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

/**
 * @since v5.2
 */
public class RestAssertions {
    private static final String CACHE_CHECK = "Cache-Control";

    private static void assertCache(CacheControl control, Response response) {
        List<Object> object = response.getMetadata().get(CACHE_CHECK);
        assertNotNull("No cache control set.", object);
        assertFalse("No cache control set.", object.isEmpty());
        assertEquals("Unexpected cache control.", 1, object.size());
        assertEquals("Cache control is wrong.", control, object.get(0));
    }

    public static void assertResponseCacheNever(Response response) {
        assertCache(never(), response);
    }
}
