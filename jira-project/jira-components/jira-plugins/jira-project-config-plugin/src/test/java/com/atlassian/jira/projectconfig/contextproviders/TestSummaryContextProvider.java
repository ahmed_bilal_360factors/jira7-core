package com.atlassian.jira.projectconfig.contextproviders;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.renderer.ProjectDescriptionRenderer;
import com.atlassian.jira.projectconfig.beans.SimplePanel;
import com.atlassian.jira.projectconfig.beans.SimpleProject;
import com.atlassian.jira.projectconfig.discover.EditWorkflowDiscoverHelper;
import com.atlassian.jira.projectconfig.project.MockProject;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.web.component.ModuleWebComponent;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.descriptors.WebPanelModuleDescriptor;
import com.atlassian.plugin.web.model.WebLabel;
import com.atlassian.plugin.web.model.WebPanel;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests {@link SummaryContextProvider}
 *
 * @since v4.4
 */
@SuppressWarnings("unchecked")
public class TestSummaryContextProvider {
    private final ContextProviderUtils contextProviderUtils = mock(ContextProviderUtils.class);
    private final WebInterfaceManager webInterfaceManager = mock(WebInterfaceManager.class);
    private final ModuleWebComponent webComponent = mock((ModuleWebComponent.class));
    private Project project;
    private Map<String, Object> testContext;
    private Map<String, Object> defaultContext;
    private final JiraAuthenticationContext authenticationContext = mock(JiraAuthenticationContext.class);
    private final MockApplicationUser user = new MockApplicationUser("admin", "administrator", "admin@admin.com");
    private final ProjectDescriptionRenderer projectDescriptionRenderer = mock(ProjectDescriptionRenderer.class);
    private final EditWorkflowDiscoverHelper editWorkflowDiscoverHelper = mock(EditWorkflowDiscoverHelper.class);

    @Before
    public void setUp() {
        when(authenticationContext.getLoggedInUser()).thenReturn(user);

        testContext = MapBuilder.build(
                "a", "aValue",
                "b", "bValue",
                "c", new Object()
        );

        project = projectFixture();

        defaultContext = MapBuilder.<String, Object>newBuilder()
                .addAll(testContext)
                .add(ContextProviderUtils.CONTEXT_PROJECT_KEY, project)
                .add(ContextProviderUtils.CONTEXT_I18N_KEY, new NoopI18nHelper())
                .toMap();

    }

    @After
    public void tearDown() {
        testContext = null;
        project = null;
        defaultContext = null;
    }


    @Test
    public void testGetContextMapWithDisplayablePanels() throws Exception {
        final WebPanelModuleDescriptor webPanelModuleDescriptorA = panelFixture("FIXTURE_A");
        final WebPanelModuleDescriptor webPanelModuleDescriptorB = panelFixture("FIXTURE_B");

        final List<WebPanelModuleDescriptor> leftPanels = Lists.newArrayList(webPanelModuleDescriptorA);
        final List<WebPanelModuleDescriptor> rightPanels = Lists.newArrayList(webPanelModuleDescriptorB);

        final String expectedLearnMoreUrl = "expected.url://docz.atlassian.com/DUPA/Workflows";

        when(webInterfaceManager.getDisplayableWebPanelDescriptors(eq(SummaryContextProvider.SUMMARY_LEFT_PANELS_LOCATION),
                eq(MapBuilder.<String, Object>build("project", project))))
                .thenReturn(leftPanels);
        when(webInterfaceManager.getDisplayableWebPanelDescriptors(eq(SummaryContextProvider.SUMMARY_RIGHT_PANELS_LOCATION),
                eq(MapBuilder.<String, Object>build("project", project))))
                .thenReturn(rightPanels);
        when(webComponent.renderModule(any(ApplicationUser.class), any(HttpServletRequest.class), eq(webPanelModuleDescriptorA), any(Map.class))).thenReturn("PANEL_HTML_FIXTURE_A");
        when(webComponent.renderModule(any(ApplicationUser.class), any(HttpServletRequest.class), eq(webPanelModuleDescriptorB), any(Map.class))).thenReturn("PANEL_HTML_FIXTURE_B");

        when(contextProviderUtils.getDefaultContext()).thenReturn(defaultContext);

        when(editWorkflowDiscoverHelper.getLearnMoreUrl(eq(project))).thenReturn(expectedLearnMoreUrl);

        final SummaryContextProvider summaryContextProvider = getSummaryContextProviderUnderTest(webInterfaceManager, contextProviderUtils, authenticationContext, webComponent, editWorkflowDiscoverHelper);
        final Map<String, Object> summaryContextMap = summaryContextProvider.getContextMap(testContext);

        final List<SimplePanel> expectedLeftPanels = Lists.newArrayList(expectedPanelFixture("FIXTURE_A"));
        final List<SimplePanel> expectedRightPanels = Lists.newArrayList(expectedPanelFixture("FIXTURE_B"));
        final SimpleProject expectedProject = expectedSimpleProjectFixture();

        final Map<String, Object> expectedContext = MapBuilder.<String, Object>newBuilder()
                .addAll(testContext)
                .addAll(defaultContext)
                .add(SummaryContextProvider.CONTEXT_SIMPLE_PROJECT_KEY, expectedProject)
                .add(SummaryContextProvider.CONTEXT_LEFT_COLUMN_KEY, expectedLeftPanels)
                .add(SummaryContextProvider.CONTEXT_RIGHT_COLUMN_KEY, expectedRightPanels)
                .add(SummaryContextProvider.CONTEXT_PROJECT_DESCRIPTION_RENDERER_KEY, projectDescriptionRenderer)
                .add(SummaryContextProvider.CONTEXT_SHOW_EDIT_WORKFLOW_DISCOVER_KEY, false)
                .add(SummaryContextProvider.CONTEXT_LEARN_MORE_URL, expectedLearnMoreUrl)
                .toMap();

        assertThat(summaryContextMap, is(expectedContext));
    }

    @Test
    public void testGetContextMapWithNoDisplayablePanels() throws Exception {
        final List<WebPanelModuleDescriptor> leftPanels = Lists.newArrayList();
        final List<WebPanelModuleDescriptor> rightPanels = Lists.newArrayList();

        final String expectedLearnMoreUrl = "expected.url://docz.atlassian.com/DUPA/Workflows";

        when(webInterfaceManager.getDisplayableWebPanelDescriptors(eq(SummaryContextProvider.SUMMARY_LEFT_PANELS_LOCATION),
                eq(MapBuilder.<String, Object>build("project", project))))
                .thenReturn(leftPanels);
        when(webInterfaceManager.getDisplayableWebPanelDescriptors(eq(SummaryContextProvider.SUMMARY_RIGHT_PANELS_LOCATION),
                eq(MapBuilder.<String, Object>build("project", project))))
                .thenReturn(rightPanels);
        when(contextProviderUtils.getDefaultContext()).thenReturn(defaultContext);
        when(editWorkflowDiscoverHelper.getLearnMoreUrl(eq(project))).thenReturn(expectedLearnMoreUrl);

        final SummaryContextProvider summaryContextProvider = getSummaryContextProviderUnderTest(webInterfaceManager, contextProviderUtils, authenticationContext, webComponent, editWorkflowDiscoverHelper);
        final Map<String, Object> summaryContextMap = summaryContextProvider.getContextMap(testContext);

        final List<SimplePanel> expectedLeftPanels = Collections.emptyList();
        final List<SimplePanel> expectedRightPanels = Collections.emptyList();
        final SimpleProject expectedProject = expectedSimpleProjectFixture();

        final Map<String, Object> expectedContext = MapBuilder.<String, Object>newBuilder()
                .addAll(testContext)
                .addAll(defaultContext)
                .add(SummaryContextProvider.CONTEXT_SIMPLE_PROJECT_KEY, expectedProject)
                .add(SummaryContextProvider.CONTEXT_LEFT_COLUMN_KEY, expectedLeftPanels)
                .add(SummaryContextProvider.CONTEXT_RIGHT_COLUMN_KEY, expectedRightPanels)
                .add(SummaryContextProvider.CONTEXT_PROJECT_DESCRIPTION_RENDERER_KEY, projectDescriptionRenderer)
                .add(SummaryContextProvider.CONTEXT_SHOW_EDIT_WORKFLOW_DISCOVER_KEY, false)
                .add(SummaryContextProvider.CONTEXT_LEARN_MORE_URL, expectedLearnMoreUrl)
                .toMap();

        assertThat(summaryContextMap, is(expectedContext));
    }

    @Test
    public void testGetContextMapWithNoWebLabel() throws Exception {
        final WebPanelModuleDescriptor webPanelModuleDescriptorC = panelFixtureWithNoWebDisplayableName("FIXTURE_C");
        final WebPanelModuleDescriptor webPanelModuleDescriptorD = panelFixtureWithNoWebDisplayableName("FIXTURE_D");

        final List<WebPanelModuleDescriptor> leftPanels = Lists.newArrayList(webPanelModuleDescriptorC);
        final List<WebPanelModuleDescriptor> rightPanels = Lists.newArrayList(webPanelModuleDescriptorD);

        final String expectedLearnMoreUrl = "expected.url://docz.atlassian.com/DUPA/Workflows";

        when(webInterfaceManager.getDisplayableWebPanelDescriptors(eq(SummaryContextProvider.SUMMARY_LEFT_PANELS_LOCATION),
                eq(MapBuilder.<String, Object>build("project", project))))
                .thenReturn(leftPanels);
        when(webInterfaceManager.getDisplayableWebPanelDescriptors(eq(SummaryContextProvider.SUMMARY_RIGHT_PANELS_LOCATION),
                eq(MapBuilder.<String, Object>build("project", project))))
                .thenReturn(rightPanels);
        when(contextProviderUtils.getDefaultContext()).thenReturn(defaultContext);
        when(webComponent.renderModule(any(ApplicationUser.class), any(HttpServletRequest.class), eq(webPanelModuleDescriptorC), any(Map.class))).thenReturn("PANEL_HTML_FIXTURE_C");
        when(webComponent.renderModule(any(ApplicationUser.class), any(HttpServletRequest.class), eq(webPanelModuleDescriptorD), any(Map.class))).thenReturn("PANEL_HTML_FIXTURE_D");
        when(editWorkflowDiscoverHelper.shouldShowEditWorkflowDiscover(authenticationContext.getLoggedInUser())).thenReturn(true);
        when(editWorkflowDiscoverHelper.getLearnMoreUrl(eq(project))).thenReturn(expectedLearnMoreUrl);

        final SummaryContextProvider summaryContextProvider = getSummaryContextProviderUnderTest(webInterfaceManager, contextProviderUtils, authenticationContext, webComponent, editWorkflowDiscoverHelper);
        final Map<String, Object> summaryContextMap = summaryContextProvider.getContextMap(testContext);

        final List<SimplePanel> expectedLeftPanels = Lists.newArrayList(expectedPanelFixtureWithNoWebDisplayableName("FIXTURE_C"));
        final List<SimplePanel> expectedRightPanels = Lists.newArrayList(expectedPanelFixtureWithNoWebDisplayableName("FIXTURE_D"));
        final SimpleProject expectedProject = expectedSimpleProjectFixture();

        final Map<String, Object> expectedContext = MapBuilder.<String, Object>newBuilder()
                .addAll(testContext)
                .addAll(defaultContext)
                .add(SummaryContextProvider.CONTEXT_SIMPLE_PROJECT_KEY, expectedProject)
                .add(SummaryContextProvider.CONTEXT_LEFT_COLUMN_KEY, expectedLeftPanels)
                .add(SummaryContextProvider.CONTEXT_RIGHT_COLUMN_KEY, expectedRightPanels)
                .add(SummaryContextProvider.CONTEXT_PROJECT_DESCRIPTION_RENDERER_KEY, projectDescriptionRenderer)
                .add(SummaryContextProvider.CONTEXT_SHOW_EDIT_WORKFLOW_DISCOVER_KEY, true)
                .add(SummaryContextProvider.CONTEXT_LEARN_MORE_URL, expectedLearnMoreUrl)
                .toMap();

        assertThat(summaryContextMap, is(expectedContext));
    }

    @Test
    public void testGetContextMapWithNoPanelLink() throws Exception {
        final WebPanelModuleDescriptor webPanelModuleDescriptorE = panelFixtureWithNoWebPanelLink("FIXTURE_E");
        final WebPanelModuleDescriptor webPanelModuleDescriptorF = panelFixtureWithNoWebPanelLink("FIXTURE_F");

        final List<WebPanelModuleDescriptor> leftPanels = Lists.newArrayList(webPanelModuleDescriptorE);
        final List<WebPanelModuleDescriptor> rightPanels = Lists.newArrayList(webPanelModuleDescriptorF);

        final String expectedLearnMoreUrl = "expected.url://docz.atlassian.com/DUPA/Workflows";

        when(webInterfaceManager.getDisplayableWebPanelDescriptors(eq(SummaryContextProvider.SUMMARY_LEFT_PANELS_LOCATION),
                eq(MapBuilder.<String, Object>build("project", project))))
                .thenReturn(leftPanels);
        when(webInterfaceManager.getDisplayableWebPanelDescriptors(eq(SummaryContextProvider.SUMMARY_RIGHT_PANELS_LOCATION),
                eq(MapBuilder.<String, Object>build("project", project))))
                .thenReturn(rightPanels);

        when(contextProviderUtils.getDefaultContext()).thenReturn(defaultContext);
        when(webComponent.renderModule(any(ApplicationUser.class), any(HttpServletRequest.class), eq(webPanelModuleDescriptorE), any(Map.class))).thenReturn("PANEL_HTML_FIXTURE_E");
        when(webComponent.renderModule(any(ApplicationUser.class), any(HttpServletRequest.class), eq(webPanelModuleDescriptorF), any(Map.class))).thenReturn("PANEL_HTML_FIXTURE_F");
        when(editWorkflowDiscoverHelper.getLearnMoreUrl(eq(project))).thenReturn(expectedLearnMoreUrl);

        final SummaryContextProvider summaryContextProvider = getSummaryContextProviderUnderTest(webInterfaceManager, contextProviderUtils, authenticationContext, webComponent, editWorkflowDiscoverHelper);
        final Map<String, Object> summaryContextMap = summaryContextProvider.getContextMap(testContext);

        final List<SimplePanel> expectedLeftPanels = Lists.newArrayList(expectedPanelFixtureWithNoWebPanelLink("FIXTURE_E"));
        final List<SimplePanel> expectedRightPanels = Lists.newArrayList(expectedPanelFixtureWithNoWebPanelLink("FIXTURE_F"));
        final SimpleProject expectedProject = expectedSimpleProjectFixture();

        final Map<String, Object> expectedContext = MapBuilder.<String, Object>newBuilder()
                .addAll(testContext)
                .addAll(defaultContext)
                .add(SummaryContextProvider.CONTEXT_SIMPLE_PROJECT_KEY, expectedProject)
                .add(SummaryContextProvider.CONTEXT_LEFT_COLUMN_KEY, expectedLeftPanels)
                .add(SummaryContextProvider.CONTEXT_RIGHT_COLUMN_KEY, expectedRightPanels)
                .add(SummaryContextProvider.CONTEXT_PROJECT_DESCRIPTION_RENDERER_KEY, projectDescriptionRenderer)
                .add(SummaryContextProvider.CONTEXT_SHOW_EDIT_WORKFLOW_DISCOVER_KEY, false)
                .add(SummaryContextProvider.CONTEXT_LEARN_MORE_URL, expectedLearnMoreUrl)
                .toMap();

        assertThat(summaryContextMap, is(expectedContext));
    }

    private SummaryContextProvider getSummaryContextProviderUnderTest(WebInterfaceManager webInterfaceManager,
                                                                      ContextProviderUtils contextProviderUtils,
                                                                      JiraAuthenticationContext authenticationContext,
                                                                      ModuleWebComponent webComponent,
                                                                      EditWorkflowDiscoverHelper editWorkflowDiscoverHelper) {
        return new SummaryContextProvider(webInterfaceManager, contextProviderUtils, webComponent, authenticationContext, projectDescriptionRenderer, editWorkflowDiscoverHelper);
    }

    private WebPanelModuleDescriptor panelFixtureWithNoWebPanelLink(final String id) {
        return MockWebPanelModuleDescriptorBuilder.builder(defaultContext)
                .setKey("KEY_" + id)
                .setWebPanelHtml("PANEL_HTML_" + id)
                .setWebLabelDisplayableName("WEBLABEL_" + id)
                .build();
    }

    private SimplePanel expectedPanelFixtureWithNoWebPanelLink(final String id) {
        return new SimplePanel("WEBLABEL_" + id, "KEY_" + id, "PANEL_HTML_" + id);
    }

    private WebPanelModuleDescriptor panelFixtureWithNoWebDisplayableName(final String id) {
        return MockWebPanelModuleDescriptorBuilder.builder(defaultContext)
                .setKey("KEY_" + id)
                .setCompleteKey("COMPLETE_KEY_" + id)
                .setWebPanelHtml("PANEL_HTML_" + id)
                .setNoWebLabelDisplayableName()
                .build();
    }

    private SimplePanel expectedPanelFixtureWithNoWebDisplayableName(final String id) {
        return new SimplePanel("COMPLETE_KEY_" + id, "KEY_" + id, "PANEL_HTML_" + id);
    }

    private WebPanelModuleDescriptor panelFixture(final String id) {
        return MockWebPanelModuleDescriptorBuilder.builder(defaultContext)
                .setKey("KEY_" + id)
                .setWebPanelHtml("PANEL_HTML_" + id)
                .setWebLabelDisplayableName("WEBLABEL_" + id)
                .build();
    }

    private SimplePanel expectedPanelFixture(final String id) {
        return new SimplePanel("WEBLABEL_" + id, "KEY_" + id, "PANEL_HTML_" + id);
    }

    private Project projectFixture() {
        final MockProject mockProject = new MockProject(1L, "PROJECT_KEY", "PROJECT_NAME");
        mockProject.setUrl("PROJECT_URL");
        mockProject.setDescription("PROJECT_DESCRIPTION");
        mockProject.setProjectTypeKey("PROJECT_TYPE_KEY");

        return mockProject;
    }

    private SimpleProject expectedSimpleProjectFixture() {
        return new SimpleProject(projectFixture());
    }

    /**
     * Builds a mocked out web panel module descriptor, making it easier to specify specific mocked objects it ought to
     * produce.
     */
    private static class MockWebPanelModuleDescriptorBuilder {
        private final Map<String, Object> testContext;
        private final WebPanelModuleDescriptor webPanelModuleDescriptor;
        private final WebLabel webLabel;
        private final WebPanel webPanel;
        private String key;

        public MockWebPanelModuleDescriptorBuilder(final Map<String, Object> testContext) {
            this.testContext = testContext;
            this.webPanelModuleDescriptor = mock(WebPanelModuleDescriptor.class);
            this.webLabel = mock(WebLabel.class);
            this.webPanel = mock(WebPanel.class);
        }

        public MockWebPanelModuleDescriptorBuilder setKey(final String key) {
            when(webPanelModuleDescriptor.getKey()).thenReturn(key);
            this.key = key;
            return this;
        }

        public MockWebPanelModuleDescriptorBuilder setCompleteKey(final String completeKey) {
            when(webPanelModuleDescriptor.getCompleteKey()).thenReturn(completeKey);
            return this;
        }

        public MockWebPanelModuleDescriptorBuilder setWebLabelDisplayableName(final String displayableName) {
            when(webPanelModuleDescriptor.getWebLabel()).thenReturn(webLabel);
            when(webLabel.getDisplayableLabel(isNull(HttpServletRequest.class), eq(testContext))).thenReturn(displayableName);

            return this;
        }

        public MockWebPanelModuleDescriptorBuilder setNoWebLabelDisplayableName() {
            when(webPanelModuleDescriptor.getWebLabel()).thenReturn(null);

            return this;
        }

        public MockWebPanelModuleDescriptorBuilder setWebPanelHtml(final String panelHtml) {
            final Map<String, Object> testContextWithDescriptorKey =
                    Maps.newHashMap(testContext);
            testContextWithDescriptorKey.put(SummaryContextProvider.CONTEXT_PANEL_KEY, key);
            when(webPanelModuleDescriptor.getModule()).thenReturn(webPanel);
            when(webPanel.getHtml(eq(testContextWithDescriptorKey))).thenReturn(panelHtml);

            return this;
        }

        public WebPanelModuleDescriptor build() {
            return webPanelModuleDescriptor;
        }

        public static MockWebPanelModuleDescriptorBuilder builder(final Map<String, Object> testContext) {
            return new MockWebPanelModuleDescriptorBuilder(testContext);
        }
    }
}
