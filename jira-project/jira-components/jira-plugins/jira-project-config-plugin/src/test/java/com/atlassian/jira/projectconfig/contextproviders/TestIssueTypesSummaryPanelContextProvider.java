package com.atlassian.jira.projectconfig.contextproviders;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.issue.fields.config.manager.IssueTypeSchemeManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.issuetype.MockIssueType;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.projectconfig.beans.SimpleIssueType;
import com.atlassian.jira.projectconfig.beans.SimpleIssueTypeImpl;
import com.atlassian.jira.projectconfig.order.MockOrderFactory;
import com.atlassian.jira.projectconfig.util.TabUrlFactory;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.collect.MapBuilder;

import com.google.common.collect.Lists;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.when;

/**
 * Tests {@link IssueTypesSummaryPanelContextProvider}
 *
 * @since v4.4
 */
@RunWith(MockitoJUnitRunner.class)
public class TestIssueTypesSummaryPanelContextProvider {
    @Mock
    private IssueTypeSchemeManager issueTypeSchemeManager;
    @Mock
    private FieldConfigScheme issueTypeScheme;
    @Mock
    private IssueType issueType;
    @Mock
    private I18nHelper i18nHelper;
    @Mock
    private TabUrlFactory tabUrlFactory;

    private MockProject project;
    private MockGenericValue projectGV;
    private Collection<IssueType> issueTypes;
    private Map<String, Object> testContext;
    private MockOrderFactory comparatorFactory;

    @Before
    public void setUp() {
        projectGV = new MockGenericValue("foo");
        project = new MockProject(null, null, null, projectGV);
        issueTypes = Lists
                .newArrayList(issueType);
        testContext = MapBuilder.<String, Object>newBuilder()
                .add("a", "aValue")
                .add("b", "bValue")
                .add("c", new Object())
                .add(ContextProviderUtils.CONTEXT_PROJECT_KEY, project)
                .add(ContextProviderUtils.CONTEXT_I18N_KEY, i18nHelper)
                .toMap();
        comparatorFactory = new MockOrderFactory();
    }

    @Test
    public void testGetContextMap() throws Exception {
        // setup
        when(issueTypeSchemeManager.getConfigScheme(eq(project))).thenReturn(issueTypeScheme);
        when(issueTypeSchemeManager.getIssueTypesForProject(eq(project))).thenReturn(issueTypes);
        when(issueTypeSchemeManager.getDefaultIssueType(eq(project))).thenReturn(null);
        when(tabUrlFactory.forIssueTypes()).thenReturn("");

        when(issueType.getId()).thenReturn("1818");
        when(issueType.getNameTranslation()).thenReturn("issueType");
        when(issueType.getDescTranslation()).thenReturn(null);
        when(issueType.getIconUrl()).thenReturn("url");
        when(issueType.isSubTask()).thenReturn(false);

        final SimpleIssueType simpleIssueType =
                new SimpleIssueTypeImpl("1818", "issueType", "", "url", false, false);
        final List<SimpleIssueType> simpleIssueTypes = Lists
                .newArrayList(simpleIssueType);

        final IssueTypesSummaryPanelContextProvider issueTypesSummaryPanelContextProvider =
                new IssueTypesSummaryPanelContextProvider(issueTypeSchemeManager, tabUrlFactory, comparatorFactory);
        // execute
        final Map<String, Object> contextMap = issueTypesSummaryPanelContextProvider.getContextMap(testContext);

        // verify
        final Map<String, Object> expectedContextMap = MapBuilder.newBuilder(testContext)
                .add(IssueTypesSummaryPanelContextProvider.CONTEXT_ISSUE_TYPES_KEY, simpleIssueTypes)
                .add(IssueTypesSummaryPanelContextProvider.CONTEXT_ISSUE_TYPE_SCHEME_KEY, issueTypeScheme)
                .add(IssueTypesSummaryPanelContextProvider.MANAGE_URL, "")
                .add(IssueTypesSummaryPanelContextProvider.CONTEXT_ERRORS_KEY, Collections.EMPTY_LIST)
                .toMap();

        assertThat(contextMap, is(expectedContextMap));
    }

    @Test
    public void testGetContextMapWithMultipleIssueTypes() throws Exception {
        // setup
        IssueType issueType = new MockIssueType("1818", "issueType", false);
        IssueType issueType2 = new MockIssueType("2222", "subTask z", true);
        IssueType issueType3 = new MockIssueType("3333", "SubTask A", true);
        IssueType issueType4 = new MockIssueType("4444", "an Issue Type", false);
        IssueType issueType5 = new MockIssueType("5555", "another Issue Type", false);

        when(issueTypeSchemeManager.getConfigScheme(eq(project))).thenReturn(issueTypeScheme);
        when(issueTypeSchemeManager.getIssueTypesForProject(eq(project)))
                .thenReturn(Lists.<IssueType>newArrayList(issueType, issueType2, issueType3, issueType4, issueType5));
        when(issueTypeSchemeManager.getDefaultIssueType(eq(project))).thenReturn(issueType4);
        when(tabUrlFactory.forIssueTypes()).thenReturn("");

        final List<SimpleIssueType> simpleIssueTypes = Lists.
                <SimpleIssueType>newArrayList(new SimpleIssueTypeImpl(issueType4, true),
                        new SimpleIssueTypeImpl(issueType5, false),
                        new SimpleIssueTypeImpl(issueType, false),
                        new SimpleIssueTypeImpl(issueType3, false),
                        new SimpleIssueTypeImpl(issueType2, false));

        final IssueTypesSummaryPanelContextProvider issueTypesSummaryPanelContextProvider =
                new IssueTypesSummaryPanelContextProvider(issueTypeSchemeManager, tabUrlFactory, comparatorFactory);

        // execute
        final Map<String, Object> contextMap = issueTypesSummaryPanelContextProvider.getContextMap(testContext);

        // verify
        final Map<String, Object> expectedContextMap = MapBuilder.newBuilder(testContext)
                .add(IssueTypesSummaryPanelContextProvider.CONTEXT_ISSUE_TYPES_KEY, simpleIssueTypes)
                .add(IssueTypesSummaryPanelContextProvider.CONTEXT_ISSUE_TYPE_SCHEME_KEY, issueTypeScheme)
                .add(IssueTypesSummaryPanelContextProvider.MANAGE_URL, "")
                .add(IssueTypesSummaryPanelContextProvider.CONTEXT_ERRORS_KEY, Collections.EMPTY_LIST)
                .toMap();

        assertThat(contextMap, is(expectedContextMap));
    }

    @Test
    public void testGetContextMapWithDefaultIssueType() throws Exception {
        // setup
        when(issueTypeSchemeManager.getConfigScheme(eq(project))).thenReturn(issueTypeScheme);
        when(issueTypeSchemeManager.getIssueTypesForProject(eq(project))).thenReturn(issueTypes);
        when(issueTypeSchemeManager.getDefaultIssueType(eq(project))).thenReturn(issueType);
        when(tabUrlFactory.forIssueTypes()).thenReturn("");

        when(issueType.getId()).thenReturn("1818");
        when(issueType.getNameTranslation()).thenReturn("issueType");
        when(issueType.getDescTranslation()).thenReturn(null);
        when(issueType.getIconUrl()).thenReturn("url");
        when(issueType.isSubTask()).thenReturn(false);

        final SimpleIssueType simpleIssueType =
                new SimpleIssueTypeImpl("1818", "issueType", "", "url", false, true);
        final List<SimpleIssueType> simpleIssueTypes = Lists
                .newArrayList(simpleIssueType);

        final IssueTypesSummaryPanelContextProvider issueTypesSummaryPanelContextProvider =
                new IssueTypesSummaryPanelContextProvider(issueTypeSchemeManager, tabUrlFactory, comparatorFactory);

        // execute
        final Map<String, Object> contextMap = issueTypesSummaryPanelContextProvider.getContextMap(testContext);

        // verify
        final Map<String, Object> expectedContextMap = MapBuilder.newBuilder(testContext)
                .add(IssueTypesSummaryPanelContextProvider.CONTEXT_ISSUE_TYPES_KEY, simpleIssueTypes)
                .add(IssueTypesSummaryPanelContextProvider.CONTEXT_ISSUE_TYPE_SCHEME_KEY, issueTypeScheme)
                .add(IssueTypesSummaryPanelContextProvider.MANAGE_URL, "")
                .add(IssueTypesSummaryPanelContextProvider.CONTEXT_ERRORS_KEY, Collections.EMPTY_LIST)
                .toMap();

        assertThat(contextMap, is(expectedContextMap));
    }

    @Test
    public void testGetContextMapWithInvalidIssueTypeScheme() throws Exception {
        // setup
        when(issueTypeSchemeManager.getConfigScheme(eq(project))).thenReturn(null);
        when(issueTypeSchemeManager.getIssueTypesForProject(eq(project))).thenReturn(issueTypes);
        when(issueTypeSchemeManager.getDefaultIssueType(eq(project))).thenReturn(null);
        when(i18nHelper.getText(IssueTypesSummaryPanelContextProvider.ISSUE_TYPE_SCHEME_ERROR_I18N_KEY))
                .thenReturn("Unable to find an issue type scheme for your project.");
        when(tabUrlFactory.forIssueTypes()).thenReturn("");

        when(issueType.getId()).thenReturn("1818");
        when(issueType.getNameTranslation()).thenReturn("issueType");
        when(issueType.getDescTranslation()).thenReturn(null);
        when(issueType.getIconUrl()).thenReturn("url");
        when(issueType.isSubTask()).thenReturn(false);

        final SimpleIssueType simpleIssueType =
                new SimpleIssueTypeImpl("1818", "issueType", "", "url", false, false);
        final List<SimpleIssueType> simpleIssueTypes = Lists
                .newArrayList(simpleIssueType);

        final IssueTypesSummaryPanelContextProvider issueTypesSummaryPanelContextProvider = new IssueTypesSummaryPanelContextProvider(issueTypeSchemeManager, tabUrlFactory, comparatorFactory);

        // execute
        final Map<String, Object> contextMap = issueTypesSummaryPanelContextProvider.getContextMap(testContext);

        // verify
        final Map<String, Object> expectedContextMap = MapBuilder.newBuilder(testContext)
                .add(IssueTypesSummaryPanelContextProvider.CONTEXT_ISSUE_TYPES_KEY, simpleIssueTypes)
                .add(IssueTypesSummaryPanelContextProvider.CONTEXT_ISSUE_TYPE_SCHEME_KEY, null)
                .add(IssueTypesSummaryPanelContextProvider.MANAGE_URL, "")
                .add(IssueTypesSummaryPanelContextProvider.CONTEXT_ERRORS_KEY, Lists.<Object>newArrayList("Unable to find an issue type scheme for your project."))
                .toMap();

        assertThat(contextMap, is(expectedContextMap));
    }
}
