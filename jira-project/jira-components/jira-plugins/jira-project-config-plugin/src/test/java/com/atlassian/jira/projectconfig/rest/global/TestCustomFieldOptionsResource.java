package com.atlassian.jira.projectconfig.rest.global;

import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.bc.customfield.CustomFieldService;
import com.atlassian.jira.issue.context.IssueContext;
import com.atlassian.jira.issue.customfields.manager.OptionsService;
import com.atlassian.jira.issue.customfields.option.Options;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.projectconfig.rest.AdminRequired;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import javax.ws.rs.core.Response;

import static com.atlassian.jira.projectconfig.rest.RestAssertions.assertResponseCacheNever;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestCustomFieldOptionsResource {
    private final MockSimpleAuthenticationContext context = new MockSimpleAuthenticationContext(new MockApplicationUser("admin"),
            Locale.ENGLISH, new NoopI18nHelper());

    @Mock
    private CustomFieldService customFieldService;

    @Mock
    private FieldConfig config;

    @Mock
    private OptionsService optionsService;

    @Mock
    private CustomField customField;

    @Mock
    private Options options;

    private CustomFieldOptionsResource resource;

    @Before
    public void setup() {
        when(customField.getRelevantConfig(IssueContext.GLOBAL))
                .thenReturn(config);

        when(customField.getId()).thenReturn("myid");

        when(customFieldService.getCustomFieldForEditConfig(context.getUser(), customField.getId()))
                .thenReturn(ServiceOutcomeImpl.ok(customField));

        when(customFieldService.getCustomFieldForEditConfig(null, customField.getId()))
                .thenReturn(ServiceOutcomeImpl.<CustomField>error("error", ErrorCollection.Reason.FORBIDDEN));

        resource = new CustomFieldOptionsResource(customFieldService, optionsService, context);
    }

    @Test
    public void testWebsudo() {
        assertThat(CustomFieldOptionsResource.class.getAnnotation(WebSudoRequired.class),
                Matchers.not(Matchers.nullValue()));
    }

    @Test
    public void testAdminOnlyWebsudo() {
        assertThat(CustomFieldOptionsResource.class.getAnnotation(AdminRequired.class),
                Matchers.not(Matchers.nullValue()));
    }

    @Test
    public void setOptionsCustomFieldId() {
        Response response = resource.setOptions(null, null);

        assertResponseCacheNever(response);
        assertThat(response.getStatus(), is(Response.Status.BAD_REQUEST.getStatusCode()));
    }

    @Test
    public void setOptionsCantEditCustomField() {
        context.setLoggedInUser((ApplicationUser) null);
        Response response = resource.setOptions(customField.getId(), null);

        assertResponseCacheNever(response);
        assertThat(response.getStatus(), is(Response.Status.FORBIDDEN.getStatusCode()));
        assertThat(response.getEntity(), Matchers.is(Matchers.notNullValue()));
    }

    @Test
    public void setOptionsBadOptions() {
        final List<CustomFieldOptionsResource.Option> good = options("good", null);

        when(optionsService.validateSetOptions(Mockito.any(OptionsService.SetOptionParams.class)))
                .thenReturn(ServiceOutcomeImpl.<OptionsService.SetValidateResult>error("Error", ErrorCollection.Reason.VALIDATION_FAILED));

        Response response = resource.setOptions(customField.getId(), good);

        assertResponseCacheNever(response);
        assertErrorResponse(response, Response.Status.BAD_REQUEST, "Error");

        final ArgumentCaptor<OptionsService.SetOptionParams> captor =
                ArgumentCaptor.forClass(OptionsService.SetOptionParams.class);
        Mockito.verify(optionsService).validateSetOptions(captor.capture());
        final OptionsService.SetOptionParams value = captor.getValue();

        assertSetOptions(value);
    }

    @Test
    public void setOptionsGoodOptionsButError() {
        final List<CustomFieldOptionsResource.Option> good = options("good", "gooder", "bestest");
        final OptionsService.SetValidateResult result = new OptionsService.SetValidateResult() {
        };

        when(optionsService.validateSetOptions(Mockito.any(OptionsService.SetOptionParams.class)))
                .thenReturn(ServiceOutcomeImpl.<OptionsService.SetValidateResult>ok(result));

        when(optionsService.setOptions(result))
                .thenReturn(ServiceOutcomeImpl.<Options>error("Error", ErrorCollection.Reason.VALIDATION_FAILED));

        Response response = resource.setOptions(customField.getId(), good);

        assertResponseCacheNever(response);
        assertErrorResponse(response, Response.Status.BAD_REQUEST, "Error");

        final ArgumentCaptor<OptionsService.SetOptionParams> captor =
                ArgumentCaptor.forClass(OptionsService.SetOptionParams.class);
        Mockito.verify(optionsService).validateSetOptions(captor.capture());

        assertSetOptions(captor.getValue());
    }

    @Test
    public void setOptionsGoodOptions() {
        final List<CustomFieldOptionsResource.Option> good = options("good", "gooder", "bestest");
        final OptionsService.SetValidateResult result = new OptionsService.SetValidateResult() {
        };

        when(optionsService.validateSetOptions(Mockito.any(OptionsService.SetOptionParams.class)))
                .thenReturn(ServiceOutcomeImpl.<OptionsService.SetValidateResult>ok(result));

        when(optionsService.setOptions(result))
                .thenReturn(ServiceOutcomeImpl.<Options>ok(options));

        Response response = resource.setOptions(customField.getId(), good);

        assertResponseCacheNever(response);
        assertThat(response.getStatus(), is(Response.Status.NO_CONTENT.getStatusCode()));

        final ArgumentCaptor<OptionsService.SetOptionParams> captor =
                ArgumentCaptor.forClass(OptionsService.SetOptionParams.class);
        Mockito.verify(optionsService).validateSetOptions(captor.capture());

        assertSetOptions(captor.getValue());
    }

    private static void assertErrorResponse(final Response response, final Response.Status status, String... errors) {
        assertThat(response.getStatus(), is(status.getStatusCode()));
        assertThat(response.getEntity(), Matchers.is(Matchers.notNullValue()));

        final com.atlassian.jira.rest.api.util.ErrorCollection errorCollection =
                (com.atlassian.jira.rest.api.util.ErrorCollection) response.getEntity();

        assertThat(errorCollection.getErrorMessages(), Matchers.contains(errors));
    }

    private void assertSetOptions(final OptionsService.SetOptionParams value) {
        assertThat(value.customField(), Matchers.is(customField));
        assertThat(value.issueContext(), Matchers.is(IssueContext.GLOBAL));
        assertThat(value.user(), Matchers.is(context.getUser()));
    }

    private List<CustomFieldOptionsResource.Option> options(String... options) {
        return Arrays.asList(options).stream().map(s -> new CustomFieldOptionsResource.Option(null, s)).collect(Collectors.toList());
    }
}
