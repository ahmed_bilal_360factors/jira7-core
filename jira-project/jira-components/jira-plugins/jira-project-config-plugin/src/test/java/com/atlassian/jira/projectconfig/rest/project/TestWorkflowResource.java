package com.atlassian.jira.projectconfig.rest.project;

import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.bc.project.ProjectAction;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.issue.fields.screen.MockFieldScreen;
import com.atlassian.jira.issue.status.MockStatus;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.issue.status.category.StatusCategory;
import com.atlassian.jira.issue.status.category.StatusCategoryImpl;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.projectconfig.workflow.ProjectConfigWorkflowDispatcher;
import com.atlassian.jira.rest.api.util.ErrorCollection;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.task.TaskManager;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.lang.Pair;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowActionsBean;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.WorkflowSchemeManager;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.opensymphony.workflow.loader.ActionDescriptor;
import com.opensymphony.workflow.loader.DescriptorFactory;
import com.opensymphony.workflow.loader.StepDescriptor;
import com.opensymphony.workflow.loader.WorkflowDescriptor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.w3c.dom.Document;

import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import javax.ws.rs.core.Response;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import static com.atlassian.jira.projectconfig.rest.RestAssertions.assertResponseCacheNever;
import static java.lang.String.format;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.stub;
import static org.mockito.Mockito.when;

/**
 * @since v5.1
 */
@RunWith(MockitoJUnitRunner.class)
public class TestWorkflowResource {
    @Mock
    private ProjectConfigWorkflowDispatcher workflowDispatcher;
    @Mock
    private TaskManager taskManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private WorkflowManager workflowManager;
    @Mock
    private ProjectService projectService;
    @Mock
    private WorkflowSchemeManager workflowSchemeManager;

    private JiraAuthenticationContext authCtx;

    private WorkflowResource workflowResource;

    @Before
    public void setup() {
        authCtx = new MockSimpleAuthenticationContext(new MockApplicationUser("username"));
        workflowResource = new WorkflowResource(workflowDispatcher, workflowManager,
                authCtx, permissionManager, projectService, workflowSchemeManager);
    }

    @Test
    public void testEditWorkflowDispatcherValid() {
        long projectId = 12345L;
        Pair<String, Long> pair = Pair.of("abc", 54321L);
        stub(workflowDispatcher.editWorkflow(projectId)).toReturn(ServiceOutcomeImpl.ok(pair));
        Response response = workflowResource.editWorkflowDispatcher(projectId);
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        WorkflowResource.EditWorkflowResult actual = (WorkflowResource.EditWorkflowResult) response.getEntity();
        assertEquals(pair.first(), actual.getWorkflowName());
        assertEquals(pair.second(), actual.getTaskId());
        assertResponseCacheNever(response);
    }

    @Test
    public void testEditWorkflowDispatcherInvalid() {
        long projectId = 12345L;
        String errorMessage = "error message";
        stub(workflowDispatcher.editWorkflow(projectId)).toReturn(ServiceOutcomeImpl.<Pair<String, Long>>error(errorMessage));
        Response response = workflowResource.editWorkflowDispatcher(projectId);
        assertEquals(Response.Status.UNAUTHORIZED.getStatusCode(), response.getStatus());
        assertEquals(new ErrorCollection().addErrorMessage(errorMessage).reason(null), response.getEntity());
        assertResponseCacheNever(response);
    }

    @Test
    public void testGetWorkflowNoName() {
        Response response = workflowResource.getWorkflow(null, null);
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
        assertResponseCacheNever(response);
    }

    @Test
    public void testGetWorkflowNoPermission() {
        final String workflowName = "something";
        Response expectedResponse = Response.ok().build();

        workflowResource = spy(workflowResource);
        doReturn(expectedResponse).when(workflowResource).hasPermission(null, workflowName);

        assertSame(expectedResponse, workflowResource.getWorkflow(workflowName, null));
    }

    @Test
    public void testGetWorkflowDoesNotExist() {
        final String workflow = "wofklow";
        when(workflowManager.getWorkflow(workflow)).thenReturn(null);
        when(permissionManager.hasPermission(Permissions.ADMINISTER, authCtx.getLoggedInUser())).thenReturn(true);

        Response response = workflowResource.getWorkflow(workflow, null);
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
        assertResponseCacheNever(response);
    }

    @Test
    public void testGetWorkflowWithPermission() {
        final JiraWorkflow jiraWorkflow = mock(JiraWorkflow.class);
        final WorkflowResource.WorkflowResponse expectedResponse = createSimpleWorkflow();

        final String workflow = "wofklow";
        when(permissionManager.hasPermission(Permissions.ADMINISTER, authCtx.getLoggedInUser())).thenReturn(true);
        when(workflowManager.getWorkflow(workflow)).thenReturn(jiraWorkflow);

        workflowResource = spy(workflowResource);
        doReturn(expectedResponse).when(workflowResource).createWorkflowResponse(jiraWorkflow);

        Response response = workflowResource.getWorkflow(workflow, null);
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        assertResponseCacheNever(response);
        assertSame(expectedResponse, response.getEntity());
    }

    private WorkflowResource.WorkflowResponse createSimpleWorkflow() {
        return new WorkflowResource.WorkflowResponse(1, "something", "something", "description",
                Collections.<WorkflowResource.SimpleWorkflowSource>emptyList(), false);
    }

    @Test
    public void testCreateWorkflowResponse() throws Exception {
        Document document = readDocument("xml/TestWorkflow.xml");
        WorkflowDescriptor descriptor = createDescriptor(document);

        JiraWorkflow workflow = mockJiraWorkflow(descriptor);
        when(permissionManager.hasPermission(Permissions.ADMINISTER, authCtx.getLoggedInUser())).thenReturn(true);
        when(workflowManager.getWorkflow(workflow.getName())).thenReturn(workflow);

        WorkflowResource.DelegatedJsonSimpleStatus status1 = createStatusForId(String.valueOf(1));
        WorkflowResource.DelegatedJsonSimpleStatus status6 = createStatusForId(String.valueOf(6));
        WorkflowResource.DelegatedJsonSimpleStatus status3 = createStatusForId(String.valueOf(3));

        FieldScreen screen3 = createFieldScreenForView("3");
        FieldScreen screen1 = createFieldScreenForView("1");

        List<WorkflowResource.SimpleWorkflowSource> sources = Lists.newArrayList();

        //First Open Status.
        List<WorkflowResource.SimpleWorkflowTarget> targets = Lists.newArrayList();
        targets.add(new WorkflowResource.SimpleWorkflowTarget(status6, "Close", screen1));
        targets.add(new WorkflowResource.SimpleWorkflowTarget(status1, "Loop", (FieldScreen) null));
        targets.add(new WorkflowResource.SimpleWorkflowTarget(status3, "Start Progress", (FieldScreen) null));
        sources.add(new WorkflowResource.SimpleWorkflowSource(status1, targets));

        //Close Status
        sources.add(new WorkflowResource.SimpleWorkflowSource(status6, Collections.<WorkflowResource.SimpleWorkflowTarget>emptyList()));

        //In Progress Status
        targets = Lists.newArrayList();
        targets.add(new WorkflowResource.SimpleWorkflowTarget(status6, "Close", screen3));
        targets.add(new WorkflowResource.SimpleWorkflowTarget(status3, "Log Work", (FieldScreen) null));
        sources.add(new WorkflowResource.SimpleWorkflowSource(status3, targets));

        WorkflowResource.WorkflowResponse expectedResponse
                = new WorkflowResource.WorkflowResponse(descriptor.getEntityId(), workflow.getName(), workflow.getDisplayName(), workflow.getDescription(), sources, true);

        final WorkflowResource.WorkflowResponse response = createCreateWorkflowProvider().createWorkflowResponse(workflow);
        assertEquals(expectedResponse, response);
    }

    //Admins can always see workflows, so we are good.
    @Test
    public void testHasPermissionAdminPermission() throws Exception {
        final String workflow = "wofklow";
        final String project = "project";
        when(permissionManager.hasPermission(Permissions.ADMINISTER, authCtx.getLoggedInUser())).thenReturn(true);

        assertNull(workflowResource.hasPermission(project, workflow));
    }

    //Project admins must pass the project through.
    @Test
    public void testHasPermissionProjectAdminWithNoProject() throws Exception {
        final String workflow = "wofklow";
        final String project = "      ";

        final Response actualResponse = workflowResource.hasPermission(project, workflow);

        assertResponseCacheNever(actualResponse);
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), actualResponse.getStatus());
    }

    //Project admins must be able to see the project configuration to get workflow details.
    @Test
    public void testHasPermissionProjectAdminWithHiddenProject() throws Exception {
        final String workflow = "wofklow";
        final String project = "projectKey";

        when(projectService.getProjectByKeyForAction(authCtx.getLoggedInUser(), project, ProjectAction.EDIT_PROJECT_CONFIG))
                .thenReturn(new ProjectService.GetProjectResult(errors("Something bad")));

        final Response actualResponse = workflowResource.hasPermission(project, workflow);

        assertResponseCacheNever(actualResponse);
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), actualResponse.getStatus());
    }

    //Default scheme for a project is {null -> jira}. Make sure this implict workflow scheme works correctly. In this
    //case the project admin can see the Workflow("jira") because IssueType(one) -> jira by default.
    @Test
    public void testHasPermissionProjectAdminUsingDefaultScheme() throws Exception {
        final String workflow = JiraWorkflow.DEFAULT_WORKFLOW_NAME;
        final MockProject project = new MockProject(12, "projectKey");
        project.setIssueTypes("one");

        when(projectService.getProjectByKeyForAction(authCtx.getLoggedInUser(), project.getKey(), ProjectAction.EDIT_PROJECT_CONFIG))
                .thenReturn(new ProjectService.GetProjectResult(project));

        when(workflowSchemeManager.getWorkflowMap(project)).thenReturn(map(null, JiraWorkflow.DEFAULT_WORKFLOW_NAME));

        final Response actualResponse = workflowResource.hasPermission(project.getKey(), workflow);
        assertNull(actualResponse);
    }

    //Test for scheme {jack -> other}. In this case the WF scheme is actually {jack -> other, null -> jira}. In this
    //case the project admin can see the Workflow("jira") because IssueType("one") -> jira.
    @Test
    public void testHasPermissionProjectAdminImplicitlyThroughImplicitDefault() throws Exception {
        final String workflow = JiraWorkflow.DEFAULT_WORKFLOW_NAME;
        final MockProject project = new MockProject(12, "projectKey");
        project.setIssueTypes("one");

        when(projectService.getProjectByKeyForAction(authCtx.getLoggedInUser(), project.getKey(), ProjectAction.EDIT_PROJECT_CONFIG))
                .thenReturn(new ProjectService.GetProjectResult(project));

        when(workflowSchemeManager.getWorkflowMap(project)).thenReturn(map("jack", "other"));

        final Response actualResponse = workflowResource.hasPermission(project.getKey(), workflow);
        assertNull(actualResponse);
    }

    //Test for scheme {null -> workflowName}. In this case the project admin can see the Workflow("workflowName") because
    // IssueType("one") -> workflowName.
    @Test
    public void testHasPermissionNonAdminImplicitlyThroughExplictDefault() throws Exception {
        final String workflow = "workflowName";
        final MockProject project = new MockProject(12, "projectKey");
        final String issueType = "one";
        project.setIssueTypes(issueType);

        when(projectService.getProjectByKeyForAction(authCtx.getLoggedInUser(), project.getKey(), ProjectAction.EDIT_PROJECT_CONFIG))
                .thenReturn(new ProjectService.GetProjectResult(project));

        when(workflowSchemeManager.getWorkflowMap(project)).thenReturn(map(null, workflow));

        final Response actualResponse = workflowResource.hasPermission(project.getKey(), workflow);
        assertNull(actualResponse);
    }

    //Test for scheme {one -> workflowName}. In this case the project admin can see the Workflow("workflowName") because
    // IssueType("one") -> workflowName.
    @Test
    public void testHasPermissionNonAdminExplicitly() throws Exception {
        final String workflow = "workflowName";
        final MockProject project = new MockProject(12, "projectKey");
        final String issueType = "one";
        project.setIssueTypes(issueType);

        when(projectService.getProjectByKeyForAction(authCtx.getLoggedInUser(), project.getKey(), ProjectAction.EDIT_PROJECT_CONFIG))
                .thenReturn(new ProjectService.GetProjectResult(project));

        when(workflowSchemeManager.getWorkflowMap(project)).thenReturn(map(issueType, workflow));

        final Response actualResponse = workflowResource.hasPermission(project.getKey(), workflow);
        assertNull(actualResponse);
    }

    //Test for scheme {two -> workflowName} => {two->workflowName, null->jira}. In this case the project admin CANNOT
    // see the Workflow("workflowName") because IssueType("one") -> jira.
    @Test
    public void testHasPermissionNonAdminNoMapping() throws Exception {
        final String workflow = "workflowName";
        final MockProject project = new MockProject(12, "projectKey");
        final String issueType = "one";
        project.setIssueTypes(issueType);

        when(projectService.getProjectByKeyForAction(authCtx.getLoggedInUser(), project.getKey(), ProjectAction.EDIT_PROJECT_CONFIG))
                .thenReturn(new ProjectService.GetProjectResult(project));

        when(workflowSchemeManager.getWorkflowMap(project))
                .thenReturn(map("two", workflow));

        final Response actualResponse = workflowResource.hasPermission(project.getKey(), workflow);

        assertResponseCacheNever(actualResponse);
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), actualResponse.getStatus());
    }

    //Test for scheme {one -> workflowNameExtra, null -> workflowName, anotherIssueType -> workflowName}.
    // In this case the project admin CANNOT see the Workflow("workflowName")
    // because IssueType("one") -> workflowNameExtra.
    @Test
    public void testHasPermissionNonAdminAllMapedDontUseDefault() throws Exception {
        final String workflow = "workflowName";
        final MockProject project = new MockProject(12, "projectKey");
        final String issueType = "one";
        project.setIssueTypes(issueType);

        when(projectService.getProjectByKeyForAction(authCtx.getLoggedInUser(), project.getKey(), ProjectAction.EDIT_PROJECT_CONFIG))
                .thenReturn(new ProjectService.GetProjectResult(project));

        when(workflowSchemeManager.getWorkflowMap(project))
                .thenReturn(map(issueType, workflow + "extra", null, workflow, "anotherIssueType", workflow));

        final Response actualResponse = workflowResource.hasPermission(project.getKey(), workflow);

        assertResponseCacheNever(actualResponse);
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), actualResponse.getStatus());
    }

    @Test
    public void testDelegatedJsonStatusCategoryForAliases() {
        StatusCategory statusCategory = StatusCategoryImpl.findByName("New");

        WorkflowResource.DelegatedJsonStatusCategory delegatedCategory = new WorkflowResource.DelegatedJsonStatusCategory(statusCategory);

        assertEquals("To Do", delegatedCategory.getPrimaryAlias());
        assertEquals("To Do", statusCategory.getAliases().get(0));

        assertSame(delegatedCategory.getPrimaryAlias(), statusCategory.getPrimaryAlias());
        assertSame(delegatedCategory.getAliases(), statusCategory.getAliases());
    }

    @Test
    public void testDelegatedJsonStatusCategoryForNonAliases() {
        StatusCategory statusCategory = StatusCategoryImpl.findByName("In Progress");

        WorkflowResource.DelegatedJsonStatusCategory delegatedCategory = new WorkflowResource.DelegatedJsonStatusCategory(statusCategory);

        assertEquals("In Progress", delegatedCategory.getPrimaryAlias());
        assertEquals(0, statusCategory.getAliases().size());
        assertSame(delegatedCategory.getPrimaryAlias(), statusCategory.getPrimaryAlias());
    }

    public static <T> Map<T, T> map(T... args) {
        Map<T, T> map = Maps.newHashMap();
        int i = 0;
        for (; i < args.length; i += 2) {
            map.put(args[i], args[i + 1]);
        }

        return map;
    }

    private static com.atlassian.jira.util.ErrorCollection errors(String... errors) {
        SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        for (String error : errors) {
            errorCollection.addErrorMessage(error);
        }
        return errorCollection;
    }

    private static WorkflowDescriptor createDescriptor(Document document) throws Exception {
        return DescriptorFactory.getFactory().createWorkflowDescriptor(document.getDocumentElement());
    }

    private static Document readDocument(String name) throws Exception {
        InputStream stream = TestWorkflowResource.class.getClassLoader().getResourceAsStream(name);
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setValidating(false);
            DocumentBuilder builder = factory.newDocumentBuilder();
            return builder.parse(stream);
        } finally {
            stream.close();
        }
    }

    private WorkflowResource createCreateWorkflowProvider() {
        return new WorkflowResource(workflowDispatcher, workflowManager, authCtx, permissionManager, projectService, workflowSchemeManager) {
            @Override
            WorkflowActionsBean createActionsBean() {
                return new WorkflowActionsBean() {
                    @Override
                    public FieldScreen getFieldScreenForView(ActionDescriptor actionDescriptor) {
                        String id = (String) actionDescriptor.getMetaAttributes().get("jira.fieldscreen.id");
                        return createFieldScreenForView(id);
                    }
                };
            }
        };
    }

    private static MockFieldScreen createFieldScreenForView(String view) {
        if (view == null) {
            return null;
        } else {
            MockFieldScreen screen = new MockFieldScreen();
            screen.setId(Long.parseLong(view));
            screen.setName(view);
            screen.setDescription(format("Screen Description: %s", view));
            return screen;
        }
    }

    private static WorkflowResource.DelegatedJsonSimpleStatus createStatusForId(String statusId) {
        return new WorkflowResource.DelegatedJsonSimpleStatus(new MockStatus(statusId, String.format("Status-%s", statusId)).getSimpleStatus());
    }

    private JiraWorkflow mockJiraWorkflow(WorkflowDescriptor workflowDescriptor) {
        JiraWorkflow mockWorkflow = mock(JiraWorkflow.class);
        when(mockWorkflow.getDescriptor()).thenReturn(workflowDescriptor);
        when(mockWorkflow.getLinkedStatusObject(any(StepDescriptor.class))).thenAnswer(new Answer<Status>() {
            @Override
            public Status answer(InvocationOnMock invocation) throws Throwable {
                //First argument is a StepDescriptor object
                StepDescriptor stepDescriptor = (StepDescriptor) invocation.getArguments()[0];
                final String statusId = (String) stepDescriptor.getMetaAttributes().get(JiraWorkflow.STEP_STATUS_KEY);
                return statusId == null ? null : new MockStatus(statusId, "Status-" + statusId);
            }
        });
        return mockWorkflow;
    }
}
