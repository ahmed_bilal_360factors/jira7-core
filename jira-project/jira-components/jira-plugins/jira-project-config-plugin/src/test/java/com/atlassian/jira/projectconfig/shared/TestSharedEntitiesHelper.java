package com.atlassian.jira.projectconfig.shared;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.order.OrderFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.workflow.ProjectWorkflowSchemeHelper;
import com.atlassian.jira.workflow.WorkflowManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static com.atlassian.jira.bc.project.ProjectAction.EDIT_PROJECT_CONFIG;
import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.isIn;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestSharedEntitiesHelper {
    @Mock
    private OrderFactory orderFactory;
    @Mock
    private ProjectService projectService;
    @Mock
    private WorkflowManager workflowManager;
    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private ProjectWorkflowSchemeHelper projectWorkflowSchemeHelper;
    @Mock
    private ApplicationUser applicationUser;

    private SharedEntitiesHelper sharedHelper;

    @Before
    public void setUp() throws Exception {
        when(authenticationContext.getLoggedInUser()).thenReturn(applicationUser);

        sharedHelper = new SharedEntitiesHelper(orderFactory, projectService, workflowManager, authenticationContext, projectWorkflowSchemeHelper);
    }

    @Test
    public void getProjectsForScheme() throws Exception {
        long schemeId = 42L;
        Project project = new MockProject(1488L);
        List<Project> expectedProjects = createProjects(3);

        List<Project> allProjects = new ArrayList<>(expectedProjects);
        allProjects.add(project);
        when(projectWorkflowSchemeHelper.getProjectsForScheme(schemeId)).thenReturn(allProjects);

        List<Project> schemeProjects = sharedHelper.getProjectsForScheme(project, schemeId);

        assertEquals(expectedProjects, schemeProjects);
    }

    @Test
    public void getProjectsForWorkflow() throws Exception {
        String workflowName = "myWorkflow1";
        Project project = new MockProject(42L);
        List<Project> expectedProjects = createProjects(3);

        List<Project> allProjects = new ArrayList<>(expectedProjects);
        allProjects.add(project);
        when(projectWorkflowSchemeHelper.getProjectsForWorkflow(workflowName)).thenReturn(allProjects);

        List<Project> workflowProjects = sharedHelper.getProjectsForWorkflow(project, workflowName);

        assertEquals(expectedProjects, workflowProjects);
    }

    @Test
    public void filterSharingProjects() throws Exception {
        int expectedSize = 5;

        List<Project> allProjects = createProjects(1, 10);

        List<Project> accessibleProjects = newArrayList(allProjects.subList(0, expectedSize));
        accessibleProjects.addAll(createProjects(11, 15));

        ServiceOutcome<List<Project>> serviceOutcome = new ServiceOutcomeImpl<>(new SimpleErrorCollection(), accessibleProjects);
        when(projectService.getAllProjectsForAction(applicationUser, EDIT_PROJECT_CONFIG)).thenReturn(serviceOutcome);

        List<Project> filteredProjects = sharedHelper.filterSharingProjects(allProjects);

        assertThat(filteredProjects, hasSize(expectedSize));
        assertThat(filteredProjects, allOf(everyItem(isIn(allProjects)), everyItem(isIn(accessibleProjects))));
    }

    private List<Project> createProjects(int count) {
        return createProjects(1, count);
    }

    private List<Project> createProjects(int startIndex, int endIndex) {
        List<Project> projects = new ArrayList<>();
        for (int i = startIndex; i <= endIndex; i++) {
            projects.add(new MockProject(i));
        }
        return projects;
    }
}