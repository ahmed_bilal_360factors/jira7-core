package com.atlassian.jira.projectconfig.contextproviders;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.atlassian.jira.issue.fields.layout.field.FieldLayout;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutScheme;
import com.atlassian.jira.mock.issue.fields.layout.field.MockFieldConfigurationScheme;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.projectconfig.beans.SimpleFieldConfigScheme;
import com.atlassian.jira.projectconfig.contextproviders.FieldsSummaryPanelContextProvider.SimpleFieldConfig;
import com.atlassian.jira.projectconfig.order.MockOrderFactory;
import com.atlassian.jira.projectconfig.util.TabUrlFactory;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.jira.util.UrlBuilder;
import com.atlassian.jira.util.collect.MapBuilder;

import com.google.common.collect.Lists;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.when;

/**
 * Test for {@link FieldsSummaryPanelContextProvider}
 *
 * @since v4.4
 */
@RunWith(MockitoJUnitRunner.class)
public class TestFieldsSummaryPanelContextProvider {
    @Mock
    private FieldLayoutManager fieldLayoutManager;
    @Mock
    private FieldLayout fieldLayout;
    @Mock
    private ContextProviderUtils contextProviderUtils;
    @Mock
    private FieldLayout anotherFieldLayout;
    @Mock
    private TabUrlFactory tabUrlFactory;

    private MockOrderFactory comparatorFactory;

    @Before
    public void setUp() {
        comparatorFactory = new MockOrderFactory();
    }

    @Test
    public void testGetContextMapWithSystemDefaultFieldConfig() throws Exception {
        // setup
        final MockProject mockProject = new MockProject(888L);
        mockProject.setIssueTypes("type a", "type b");

        final Map<String, Object> testContext = MapBuilder.<String, Object>build(
                ContextProviderUtils.CONTEXT_PROJECT_KEY, mockProject,
                ContextProviderUtils.CONTEXT_I18N_KEY, new NoopI18nHelper()
        );

        when(fieldLayoutManager.getFieldConfigurationScheme(eq(mockProject))).thenReturn(null);
        when(fieldLayoutManager.getFieldLayoutSchemes()).thenReturn(Collections.<FieldLayoutScheme>emptyList());

        final FieldsSummaryPanelContextProvider fieldsSummaryPanelContextProvider =
                getFieldsSummaryPanelContextProviderUnderTest(fieldLayoutManager, contextProviderUtils);

        // execute
        final Map<String, Object> contextMap = fieldsSummaryPanelContextProvider.getContextMap(testContext);

        // verify
        final Map<String, Object> expectedMap = MapBuilder.<String, Object>newBuilder()
                .addAll(testContext)
                .add(FieldsSummaryPanelContextProvider.CONTEXT_FIELD_CONFIG_SCHEME_KEY,
                        new SimpleFieldConfigScheme(null, NoopI18nHelper.makeTranslation("admin.projects.system.default.field.config"), "",
                                null, getEditUrl()))
                .add(FieldsSummaryPanelContextProvider.CONTEXT_FIELD_CONFIGS_KEY, Collections.<SimpleFieldConfig>
                        singletonList(SimpleFieldConfig.getSystemDefaultSimpleFieldConfig(getSystemDefaultUrl(), true)))
                .toMap();

        assertEquals(expectedMap, contextMap);
    }

    @Test
    public void testGetContextMapWithFieldConfigsIncludingSystemDefault() throws Exception {
        // setup
        final MockProject mockProject = new MockProject(888L);
        mockProject.setIssueTypes("type a", "type b");

        final Map<String, Object> testContext = MapBuilder.<String, Object>build(
                ContextProviderUtils.CONTEXT_PROJECT_KEY, mockProject
        );

        final MockFieldConfigurationScheme fieldConfigurationScheme = new MockFieldConfigurationScheme(2828L, "Field Config Scheme", "");
        fieldConfigurationScheme.setIssueTypeToFieldLayoutIdMapping(MapBuilder.<String, Long>build(
                null, null
        ));

        when(fieldLayoutManager.getFieldConfigurationScheme(eq(mockProject))).thenReturn(fieldConfigurationScheme);
        when(fieldLayoutManager.getFieldLayoutSchemes()).thenReturn(Lists.<FieldLayoutScheme>newArrayList(null, null));

        when(fieldLayoutManager.getFieldLayout(eq(mockProject), eq("type a"))).thenReturn(null);
        when(fieldLayoutManager.getFieldLayout(eq(mockProject), eq("type b"))).thenReturn(fieldLayout);

        when(fieldLayoutManager.getFieldLayout()).thenReturn(fieldLayout);

        when(fieldLayout.getId()).thenReturn(null);

        final FieldsSummaryPanelContextProvider fieldsSummaryPanelContextProvider =
                getFieldsSummaryPanelContextProviderUnderTest(fieldLayoutManager, contextProviderUtils);

        // execute
        final Map<String, Object> contextMap = fieldsSummaryPanelContextProvider.getContextMap(testContext);

        // verify
        final Map<String, Object> expectedMap = MapBuilder.<String, Object>newBuilder()
                .addAll(testContext)
                .add(FieldsSummaryPanelContextProvider.CONTEXT_FIELD_CONFIG_SCHEME_KEY,
                        new SimpleFieldConfigScheme(2828L, "Field Config Scheme", "", null, getEditUrl()))
                .add(FieldsSummaryPanelContextProvider.CONTEXT_FIELD_CONFIGS_KEY, Lists.<SimpleFieldConfig>
                        newArrayList(SimpleFieldConfig.getSystemDefaultSimpleFieldConfig(getSystemDefaultUrl(), true)))
                .toMap();

        assertEquals(expectedMap, contextMap);
    }

    @Test
    public void testGetContextMapWithOtherFieldConfigs() throws Exception {
        // setup
        final MockProject mockProject = new MockProject(888L);
        mockProject.setIssueTypes("type a", "type b");

        final Map<String, Object> testContext = MapBuilder.<String, Object>build(
                ContextProviderUtils.CONTEXT_PROJECT_KEY, mockProject
        );

        final MockFieldConfigurationScheme fieldConfigurationScheme = new MockFieldConfigurationScheme(2828L, "Field Config Scheme", "");
        fieldConfigurationScheme.setIssueTypeToFieldLayoutIdMapping(MapBuilder.<String, Long>build(
                null, null
        ));

        when(fieldLayoutManager.getFieldConfigurationScheme(eq(mockProject))).thenReturn(fieldConfigurationScheme);
        when(fieldLayoutManager.getFieldLayoutSchemes()).thenReturn(Lists.<FieldLayoutScheme>newArrayList(null, null));

        when(fieldLayoutManager.getFieldLayout(eq(mockProject), eq("type a"))).thenReturn(fieldLayout);
        when(fieldLayoutManager.getFieldLayout(eq(mockProject), eq("type b"))).thenReturn(anotherFieldLayout);

        when(fieldLayoutManager.getFieldLayout()).thenReturn(fieldLayout);

        when(fieldLayout.getId()).thenReturn(null);

        when(anotherFieldLayout.getId()).thenReturn(4848L);
        when(anotherFieldLayout.getName()).thenReturn("Simple Field Layout");
        when(anotherFieldLayout.getDescription()).thenReturn("Simple Field Layout Description");

        final FieldsSummaryPanelContextProvider fieldsSummaryPanelContextProvider =
                getFieldsSummaryPanelContextProviderUnderTest(fieldLayoutManager, contextProviderUtils);

        final Map<String, Object> contextMap = fieldsSummaryPanelContextProvider.getContextMap(testContext);

        final List<SimpleFieldConfig> simpleFieldConfigs =
                Lists.newArrayList(
                        SimpleFieldConfig.getSystemDefaultSimpleFieldConfig(getSystemDefaultUrl(), true),
                        new SimpleFieldConfig(4848L, "Simple Field Layout", "Simple Field Layout Description",
                                getFieldConfigUrl(4848L), false)
                );

        final Map<String, Object> expectedMap = MapBuilder.<String, Object>newBuilder()
                .addAll(testContext)
                .add(FieldsSummaryPanelContextProvider.CONTEXT_FIELD_CONFIG_SCHEME_KEY,
                        new SimpleFieldConfigScheme(2828L, "Field Config Scheme", "", null, getEditUrl()))
                .add(FieldsSummaryPanelContextProvider.CONTEXT_FIELD_CONFIGS_KEY, simpleFieldConfigs)
                .toMap();

        assertEquals(expectedMap, contextMap);
    }

    @Test
    public void testGetContextMapWithDefaultFieldConfigThatIsNotSystemDefault() throws Exception {
        // setup
        final MockProject mockProject = new MockProject(888L);
        mockProject.setIssueTypes("type a");

        final Map<String, Object> testContext = MapBuilder.<String, Object>build(
                ContextProviderUtils.CONTEXT_PROJECT_KEY, mockProject
        );

        final MockFieldConfigurationScheme fieldConfigurationScheme = new MockFieldConfigurationScheme(2828L, "Field Config Scheme", "");
        fieldConfigurationScheme.setIssueTypeToFieldLayoutIdMapping(MapBuilder.<String, Long>build(
                null, 4848L
        ));

        when(fieldLayoutManager.getFieldConfigurationScheme(eq(mockProject))).thenReturn(fieldConfigurationScheme);
        when(fieldLayoutManager.getFieldLayoutSchemes()).thenReturn(Lists.<FieldLayoutScheme>newArrayList(null, null));
        when(fieldLayoutManager.getFieldLayout(eq(mockProject), eq("type a"))).thenReturn(fieldLayout);

        when(fieldLayout.getId()).thenReturn(4848L);
        when(fieldLayout.getName()).thenReturn("Custom Field Config");
        when(fieldLayout.getDescription()).thenReturn("Custom Field Description");

        final FieldsSummaryPanelContextProvider fieldsSummaryPanelContextProvider =
                getFieldsSummaryPanelContextProviderUnderTest(fieldLayoutManager, contextProviderUtils);

        // execute
        final Map<String, Object> contextMap = fieldsSummaryPanelContextProvider.getContextMap(testContext);

        // verify
        final Map<String, Object> expectedMap = MapBuilder.<String, Object>newBuilder()
                .addAll(testContext)
                .add(FieldsSummaryPanelContextProvider.CONTEXT_FIELD_CONFIG_SCHEME_KEY,
                        new SimpleFieldConfigScheme(2828L, "Field Config Scheme", "", null, getEditUrl()))
                .add(FieldsSummaryPanelContextProvider.CONTEXT_FIELD_CONFIGS_KEY, Lists.<SimpleFieldConfig>
                        newArrayList(new SimpleFieldConfig(4848L, "Custom Field Config", "Custom Field Description", getFieldConfigUrl(4848L), true)))
                .toMap();

        assertEquals(expectedMap, contextMap);
    }

    @Test
    public void testFieldConfigUrl() {
        // setup
        final long fieldConfigId = 4L;
        when(contextProviderUtils.createUrlBuilder("/secure/admin/ConfigureFieldLayout!default.jspa"))
                .thenReturn(new UrlBuilder("fieldConfig", "UTF-8", false));

        // execute
        FieldsSummaryPanelContextProvider provider = new FieldsSummaryPanelContextProvider(fieldLayoutManager, contextProviderUtils,
                tabUrlFactory, comparatorFactory);

        // verify
        assertEquals("fieldConfig?id=" + fieldConfigId, provider.getFieldConfigUrl(fieldConfigId));
    }

    @Test
    public void testGetEditSchemeUrl() {
        // setup
        final long fieldConfigId = 2L;
        when(tabUrlFactory.forFields()).thenReturn("fieldConfig");

        // execute
        FieldsSummaryPanelContextProvider provider = new FieldsSummaryPanelContextProvider(fieldLayoutManager, contextProviderUtils,
                tabUrlFactory, comparatorFactory);

        // verify
        assertEquals("fieldConfig", provider.getEditSchemeUrl());
    }

    @Test
    public void testGetSystemDefaultEditConfigUrl() {
        // setup
        when(contextProviderUtils.createUrlBuilder("/secure/admin/ViewIssueFields.jspa"))
                .thenReturn(new UrlBuilder("fieldConfig", "UTF-8", false));

        // execute
        FieldsSummaryPanelContextProvider provider = new FieldsSummaryPanelContextProvider(fieldLayoutManager,
                contextProviderUtils, tabUrlFactory, new MockOrderFactory());

        // verify
        assertEquals("fieldConfig", provider.getSystemDefaultEditConfigUrl());
    }

    private FieldsSummaryPanelContextProvider getFieldsSummaryPanelContextProviderUnderTest(final FieldLayoutManager fieldLayoutManager,
                                                                                            final ContextProviderUtils contextProviderUtils) {
        return new FieldsSummaryPanelContextProvider(fieldLayoutManager, contextProviderUtils, tabUrlFactory, comparatorFactory) {
            @Override
            String getEditSchemeUrl() {
                return getEditUrl();
            }

            @Override
            String getFieldConfigUrl(Long id) {
                return TestFieldsSummaryPanelContextProvider.getFieldConfigUrl(id);
            }

            @Override
            String getSystemDefaultEditConfigUrl() {
                return getSystemDefaultUrl();
            }
        };
    }

    private static String getFieldConfigUrl(Long id) {
        return "fieldConfigUrl" + id;
    }

    private static String getEditUrl() {
        return "editUrl";
    }

    private static String getSystemDefaultUrl() {
        return "systemDefaultEditUrl";
    }


}
