package com.atlassian.jira.projectconfig.rest.project;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.core.Response;

import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.issuetype.MockIssueType;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.beans.ProjectContext;
import com.atlassian.jira.projectconfig.beans.SimpleRestProjectMatcher;
import com.atlassian.jira.projectconfig.rest.ResponseMatchers;
import com.atlassian.jira.projectconfig.rest.beans.SimpleRestProject;
import com.atlassian.jira.projectconfig.shared.SharedIssueTypeWorkflowData;
import com.atlassian.jira.projectconfig.util.ProjectContextLocator;
import com.atlassian.jira.projectconfig.issuetypes.workflow.IssueTypeConfigWorkflowHelper;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.MockJiraWorkflow;

import com.google.common.base.Objects;
import com.google.common.collect.Lists;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.jira.projectconfig.rest.project.IssueTypeConfigWorkflowResource.WorkflowDetails;
import static com.atlassian.jira.projectconfig.issuetypes.workflow.IssueTypeConfigWorkflowHelper.WorkflowState;
import static com.atlassian.jira.projectconfig.issuetypes.workflow.IssueTypeConfigWorkflowHelper.WorkflowState.EDITABLE;
import static com.google.common.collect.ImmutableList.of;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

/**
 * @since v6.2
 */
@RunWith(MockitoJUnitRunner.class)
public class TestIssueTypeConfigWorkflowResource {
    @Mock
    private ProjectContextLocator projectContextLocator;

    @Mock
    private IssueTypeConfigWorkflowHelper workflowHelper;

    private IssueTypeConfigWorkflowResource resource;

    @Before
    public void before() {
        resource = new IssueTypeConfigWorkflowResource(workflowHelper, projectContextLocator);
    }

    @Test
    public void getWorkflowDataWhenProjectOrIssueTypeIsInvalid() {
        final String projectKey = "ABC";
        final long issueTypeId = 1010L;
        final String errorMessage = "Wtf";

        when(projectContextLocator.getContext(projectKey, issueTypeId))
                .thenReturn(ServiceOutcomeImpl.<ProjectContext>error(errorMessage,
                        com.atlassian.jira.util.ErrorCollection.Reason.NOT_FOUND));

        final Response response = resource.getWorkflowData(projectKey, issueTypeId);

        assertThat(response, ResponseMatchers.noCache());
        assertThat(response, ResponseMatchers.status(Response.Status.NOT_FOUND));
        assertThat(response, ResponseMatchers.errorBody(errorMessage));
    }

    @Test
    public void getWorkflowDataReturnsCorrectData() {
        final int issueTypeId = 1819;
        final IssueType type = new MockIssueType(issueTypeId, "Name");
        final Project project = new MockProject(10101028282L, "ABC").setIssueTypes(type);
        final ProjectContext context = new ProjectContext(project, type);
        final JiraWorkflow workflow = new MockJiraWorkflow("ABC");

        //Shared with these issue types.
        final IssueType otherType = new MockIssueType(383838, "SharedType");

        //Shared with these projects.
        final Project otherProject = new MockProject(101010282823L, "DEF");

        final SharedIssueTypeWorkflowData sharedByData =
                new SharedIssueTypeWorkflowData(of(otherProject), of(otherType));
        final IssueTypeConfigWorkflowHelper.WorkflowResult workflowResult =
                new IssueTypeConfigWorkflowHelper.WorkflowResult(workflow, EDITABLE, sharedByData);

        when(projectContextLocator.getContext(project.getKey(), issueTypeId))
                .thenReturn(ServiceOutcomeImpl.ok(context));

        when(workflowHelper.getWorkflowData(context))
                .thenReturn(workflowResult);

        final Response response = this.resource.getWorkflowData(project.getKey(), issueTypeId);
        assertThat(response, ResponseMatchers.noCache());
        assertThat(response, ResponseMatchers.status(Response.Status.OK));

        assertThat(response, ResponseMatchers.body(WorkflowDetails.class,
                new WorkflowDetailsMatcher()
                        .workflow(workflow)
                        .workflowState(EDITABLE)
                        .issueTypes(otherType)
                        .projects(otherProject)));
    }

    private static class WorkflowDetailsMatcher extends TypeSafeMatcher<WorkflowDetails> {
        private String name;
        private String displayName;
        private String workflowState;
        private List<String> issueTypes = Collections.emptyList();
        private List<SimpleRestProject> projects = Collections.emptyList();

        private WorkflowDetailsMatcher workflow(JiraWorkflow workflow) {
            this.name = workflow.getName();
            this.displayName = workflow.getDisplayName();

            return this;
        }

        private WorkflowDetailsMatcher workflowState(WorkflowState state) {
            this.workflowState = state.simpleName();
            return this;
        }

        private WorkflowDetailsMatcher issueTypes(IssueType... types) {
            this.issueTypes = Arrays.asList(types).stream().map(t -> t.getId()).collect(Collectors.toList());
            return this;
        }

        private WorkflowDetailsMatcher projects(Project... projects) {
            this.projects = Arrays.asList(projects).stream().map(p -> SimpleRestProject.shortProject(p)).collect(Collectors.toList());
            return this;
        }

        @Override
        public void describeTo(final Description description) {
            description
                    .appendText(String.format("[%s(%s) State: %s, Issue Types: %s, Projects: %s]",
                            name, displayName, workflowState, issueTypes, toString(projects)));
        }

        @Override
        protected boolean matchesSafely(final WorkflowDetails item) {
            return Objects.equal(name, item.getName())
                    && Objects.equal(displayName, item.getDisplayName())
                    && Objects.equal(workflowState, item.getState())
                    && Objects.equal(issueTypes, item.getSharedWithIssueTypes())
                    && createProjectMatcher().matches(item.getSharedWithProjects());
        }

        @Override
        protected void describeMismatchSafely(final WorkflowDetails item, final Description mismatchDescription) {
            mismatchDescription
                    .appendText(String.format("[%s(%s) State: %s, Issue Types: %s, Projects: %s]",
                            item.getName(), item.getDisplayName(), item.getState(),
                            item.getSharedWithIssueTypes(), toString(item.getSharedWithProjects())));
        }

        private Matcher<Iterable<? extends SimpleRestProject>> createProjectMatcher() {
            List<Matcher<? super SimpleRestProject>> matchers = Lists.newArrayList();
            for (SimpleRestProject project : projects) {
                matchers.add(new SimpleRestProjectMatcher().simpleProject(project));
            }
            return Matchers.contains(matchers);
        }

        private static String toString(Iterable<SimpleRestProject> projects) {
            StringBuilder builder = new StringBuilder("[");
            boolean first = true;
            for (SimpleRestProject project : projects) {
                if (first) {
                    first = false;
                } else {
                    builder.append(", ");
                }
                builder.append(String.format("Proj[%s (%d) - %s]", project.getKey(), project.getId(), project.getName()));
            }

            return builder.append("]").toString();
        }
    }
}
