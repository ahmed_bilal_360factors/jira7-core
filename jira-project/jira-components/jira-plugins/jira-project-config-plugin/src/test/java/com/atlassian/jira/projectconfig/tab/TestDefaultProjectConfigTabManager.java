package com.atlassian.jira.projectconfig.tab;

import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.osgi.util.tracker.ServiceTracker;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.ListableBeanFactory;

import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.4
 */
public class TestDefaultProjectConfigTabManager {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private ListableBeanFactory listableBeanFactory;

    @Mock
    private ServiceTracker<ProjectConfigTab, ProjectConfigTab> serviceTracker;

    private DefaultProjectConfigTabManager manager;

    @Before
    public void setUp() {
        when(serviceTracker.getServices(new ProjectConfigTab[0])).thenReturn(new ProjectConfigTab[0]);

        manager = new DefaultProjectConfigTabManager(null);
        manager.serviceTracker = serviceTracker;
        manager.setBeanFactory(listableBeanFactory);
    }

    @Test(expected = BeanInitializationException.class)
    public void testBadFactory() {
        manager.setBeanFactory(mock(BeanFactory.class));
    }

    @Test
    public void testRegistrationsThroughSpringBeans() {
        ProjectConfigTab mockTab1 = mockTab("5");
        ProjectConfigTab mockTab2 = mockTab("6");

        when(listableBeanFactory.getBeansOfType(ProjectConfigTab.class)).thenReturn(ImmutableMap.of("one", mockTab1, "two", mockTab2));

        assertSame(mockTab1, manager.getTabForId("5"));
        assertSame(mockTab2, manager.getTabForId("6"));
    }

    @Test
    public void testRegistrationsThroughOSGiServices() {
        ProjectConfigTab mockTab = mockTab("tab-id");
        when(serviceTracker.getServices(new ProjectConfigTab[0])).thenReturn(new ProjectConfigTab[]{mockTab});

        assertSame(mockTab, manager.getTabForId("tab-id"));
    }

    private static ProjectConfigTab mockTab(String id) {
        ProjectConfigTab tab = mock(ProjectConfigTab.class, "ProjectConfigTab-" + id);
        when(tab.getId()).thenReturn(id);
        return tab;
    }

}
