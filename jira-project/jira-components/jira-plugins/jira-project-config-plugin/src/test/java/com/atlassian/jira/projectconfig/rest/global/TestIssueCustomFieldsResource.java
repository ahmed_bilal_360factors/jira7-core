package com.atlassian.jira.projectconfig.rest.global;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.fields.MockCustomField;
import com.atlassian.jira.issue.fields.MockOrderableField;
import com.atlassian.jira.issue.fields.OrderableField;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.fields.IssueFields;
import com.atlassian.jira.projectconfig.rest.AdminRequired;
import com.atlassian.jira.projectconfig.rest.beans.SimpleRestProject;
import com.atlassian.jira.util.lang.Pair;
import com.atlassian.sal.api.websudo.WebSudoRequired;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.jira.projectconfig.rest.RestAssertions.assertResponseCacheNever;
import static com.atlassian.jira.projectconfig.rest.global.IssueCustomFieldsResource.FieldBean;
import static com.google.common.collect.Lists.newArrayList;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestIssueCustomFieldsResource {
    @Mock
    private CustomFieldManager customFieldManager;

    @Mock
    private IssueManager issueManager;

    @Mock
    private IssueFields issueFields;

    private IssueCustomFieldsResource resource;

    @Before
    public void beforeTest() {
        resource = new IssueCustomFieldsResource(issueManager, customFieldManager, issueFields);
    }

    @Test
    public void addCustomFieldNoIssue() {
        final Response response = resource.addCustomField(10101, "20");

        assertResponseCacheNever(response);
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }

    @Test
    public void addCustomFieldNoField() {
        final long issueKey = 202929;

        when(issueManager.getIssueObject(issueKey)).thenReturn(new MockIssue(10L));

        final Response response = resource.addCustomField(issueKey, "20");
        assertResponseCacheNever(response);
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }

    @Test
    public void addCustomField() {
        final long issueKey = 302929;
        final String fieldId = "20";
        final MockCustomField mockCustomField = new MockCustomField().setId(fieldId);
        final MockIssue mockIssue = new MockIssue(10L);

        when(issueManager.getIssueObject(issueKey)).thenReturn(mockIssue);
        when(customFieldManager.getCustomFieldObject(fieldId)).thenReturn(mockCustomField);

        final Response response = resource.addCustomField(issueKey, fieldId);

        assertResponseCacheNever(response);
        assertEquals(Response.Status.NO_CONTENT.getStatusCode(), response.getStatus());

        verify(issueFields).addFieldToIssueScreens(mockCustomField, mockIssue);
    }

    @Test
    public void affectedProjects() {
        Project one = new MockProject(10, "ABC", "ABC");
        Project two = new MockProject(11, "DEF", "DEF");
        Project three = new MockProject(12, "HIJ", "HIJ");

        final long issueId = 3838393802094820934L;
        final MockIssue issue = new MockIssue(issueId);
        when(issueManager.getIssueObject(issueId)).thenReturn(issue);
        when(issueFields.getAffectedProjects(issue)).thenReturn(newArrayList(one, two, three));

        final Response response = resource.getAffectedProjects(issueId);
        assertResponseCacheNever(response);

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        assertEquals(toMap(one, two, three), toMap((List<?>) response.getEntity()));
    }

    @Test
    public void affectedProjectNoIssue() {
        final long issueId = 0x29749389;

        final Response response = resource.getAffectedProjects(issueId);
        assertResponseCacheNever(response);
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }

    @Test
    public void fieldsWithOnScreenFlagOff() {
        final long issueId = 3838393802094820934L;
        final MockIssue issue = new MockIssue(issueId);

        MockOrderableField orderableField1 = new MockOrderableField("field1", "Field1");
        MockOrderableField orderableField2 = new MockOrderableField("field2", "Field2");

        when(issueFields.getAllCustomFields(issue))
                .thenReturn(Arrays.<OrderableField>asList(orderableField1, orderableField2));
        when(issueManager.getIssueObject(issueId))
                .thenReturn(issue);

        final Response fields = resource.getFields(issueId, false);
        assertEquals(toMap(orderableField1, orderableField2), toMap((Iterable<?>) fields.getEntity()));
        assertResponseCacheNever(fields);
    }

    @Test
    public void fieldsWithOnScreenFlagOn() {
        final long issueId = 3838393802094820934L;
        final MockIssue issue = new MockIssue(issueId);

        MockOrderableField orderableField1 = new MockOrderableField("field1", "Field1");
        MockOrderableField orderableField2 = new MockOrderableField("field2", "Field2");

        Pair onScreenPair = Pair.nicePairOf(orderableField1, true);
        Pair notOnScreenPair = Pair.nicePairOf(orderableField2, false);

        when(issueFields.getAllCustomFieldsWithOnAllScreensFlag(issue))
                .thenReturn(Arrays.<Pair<OrderableField, Boolean>>asList(onScreenPair, notOnScreenPair));
        when(issueManager.getIssueObject(issueId))
                .thenReturn(issue);

        final Response fields = resource.getFields(issueId, true);
        assertEquals(toMap(onScreenPair, notOnScreenPair), toMap((Iterable<?>) fields.getEntity()));
        assertResponseCacheNever(fields);
    }

    @Test
    public void fieldsNoIssue() {
        final long issueId = 0x29749389;

        final Response response = resource.getFields(issueId, true);
        assertResponseCacheNever(response);
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }

    @Test
    public void websudoEnabled() {
        assertNotNull(resource.getClass().getAnnotation(WebSudoRequired.class));
    }

    @Test
    public void ensureAdminOnly() {
        assertNotNull(resource.getClass().getAnnotation(AdminRequired.class));
    }

    @Test
    public void onlyJson() {
        final Consumes consumes = CustomFieldTypesResource.class.getAnnotation(Consumes.class);
        final Produces produces = CustomFieldTypesResource.class.getAnnotation(Produces.class);

        assertArrayEquals(new String[]{MediaType.APPLICATION_JSON}, consumes.value());
        assertArrayEquals(new String[]{MediaType.APPLICATION_JSON}, produces.value());
    }

    private List<Object> toMap(Object... projects) {
        return toMap(Arrays.asList(projects));
    }

    private List<Object> toMap(Iterable<?> objects) {
        return Lists.newArrayList(Iterables.transform(objects, new Function<Object, Object>() {
            @Override
            public Object apply(@Nullable final Object input) {
                if (input instanceof Project) {
                    return asMap((Project) input);
                } else if (input instanceof SimpleRestProject) {
                    return asMap((SimpleRestProject) input);
                } else if (input instanceof OrderableField) {
                    return asMap((OrderableField) input);
                } else if (input instanceof FieldBean) {
                    return asMap((FieldBean) input);
                } else if (input instanceof IssueCustomFieldsResource.FieldBeanWithVisiblityFlag) {
                    return asMap((IssueCustomFieldsResource.FieldBeanWithVisiblityFlag) input);
                } else if (input instanceof Pair) {
                    return asMap((Pair) input);
                } else {
                    throw new RuntimeException();
                }
            }
        }));
    }

    private static Map<Object, Object> asMap(Pair<OrderableField, Boolean> fieldWithOnScreenFlag) {
        final HashMap<Object, Object> map = Maps.newHashMap();
        map.put("id", fieldWithOnScreenFlag.first().getId());
        map.put("name", fieldWithOnScreenFlag.first().getName());
        map.put("onScreen", fieldWithOnScreenFlag.second());
        return map;
    }

    private static Map<Object, Object> asMap(OrderableField field) {
        final HashMap<Object, Object> map = Maps.newHashMap();
        map.put("id", field.getId());
        map.put("name", field.getName());
        return map;
    }

    private static Map<Object, Object> asMap(FieldBean field) {
        final HashMap<Object, Object> map = Maps.newHashMap();
        map.put("id", field.getId());
        map.put("name", field.getName());
        return map;
    }

    private static Map<Object, Object> asMap(IssueCustomFieldsResource.FieldBeanWithVisiblityFlag field) {
        final HashMap<Object, Object> map = Maps.newHashMap();
        map.put("id", field.getId());
        map.put("name", field.getName());
        map.put("onScreen", field.isOnAllScreens());
        return map;
    }

    private static Map<Object, Object> asMap(SimpleRestProject project) {
        final HashMap<Object, Object> map = Maps.newHashMap();
        map.put("name", project.getName());
        map.put("description", project.getDescription());
        map.put("key", project.getKey());
        map.put("id", project.getId());

        return Collections.unmodifiableMap(map);
    }

    private static Map<Object, Object> asMap(Project project) {
        final HashMap<Object, Object> map = Maps.newHashMap();
        map.put("name", project.getName());
        map.put("description", project.getDescription());
        map.put("key", project.getKey());
        map.put("id", project.getId());

        return Collections.unmodifiableMap(map);
    }
}
