package com.atlassian.jira.projectconfig.contextproviders;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.plugin.profile.UserFormatManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestProjectPeopleContextProvider {

    private ProjectPeopleContextProvider projectPeopleContextProvider;
    private ApplicationUser user = new MockApplicationUser("admin");
    private Map<String, Object> context, parameterMap;
    private URI uri;

    @Mock
    private PermissionManager permissionManager;
    @Mock
    UserFormatManager userFormatManager;
    @Mock
    UserManager userManager;
    @Mock
    AvatarService avatarService;
    @Mock
    ContextProviderUtils contextProviderUtils;
    @Mock
    JiraAuthenticationContext authenticationContext;
    @Mock
    Project project;

    @Before
    public void setUp() throws Exception {
        context = new HashMap<>();
        parameterMap = new HashMap<>();
        context.put(ContextProviderUtils.CONTEXT_PROJECT_KEY, project);
        when(contextProviderUtils.getDefaultContext()).thenReturn(context);
        ComponentAccessor.initialiseWorker(new MockComponentWorker());
        projectPeopleContextProvider = new ProjectPeopleContextProvider(permissionManager, userFormatManager,
                userManager, avatarService, contextProviderUtils, authenticationContext);
        uri = new URI("http://www.example.com/pianocat.gif");

        when(avatarService.getAvatarURL(any(ApplicationUser.class), any(ApplicationUser.class),
                any(Avatar.Size.class))).thenReturn(uri);
    }

    @After
    public void tearDown() {
        ComponentAccessor.initialiseWorker(null);
    }

    @Test
    public void testValidProjectLeadDoesNotExistViaTheContextMap() {
        when(userManager.getUserByKey(anyString())).thenReturn(null);
        Map<String, Object> map = projectPeopleContextProvider.getContextMap(parameterMap);
        assertThat("The project lead in the context map should not exist", map.get(ProjectPeopleContextProvider.CONTEXT_PROJECT_LEAD_EXISTS_KEY), is(false));
    }

    @Test
    public void testValidProjectLeadExistsViaTheContextMap() {
        when(userManager.getUserByKey(anyString())).thenReturn(user);
        Map<String, Object> map = projectPeopleContextProvider.getContextMap(parameterMap);
        assertThat("The project lead in the context map should exist", map.get(ProjectPeopleContextProvider.CONTEXT_PROJECT_LEAD_EXISTS_KEY), is(true));
    }
}
