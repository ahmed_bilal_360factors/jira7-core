package com.atlassian.jira.projectconfig.contextproviders;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import com.atlassian.jira.issue.security.IssueSecurityLevelManager;
import com.atlassian.jira.issue.security.IssueSecuritySchemeManager;
import com.atlassian.jira.issue.security.ProjectIssueSecuritySchemeHelper;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.contextproviders.ProjectIssueSecurityContextProvider.IssueSecurity;
import com.atlassian.jira.security.SecurityTypeManager;
import com.atlassian.jira.security.type.AbstractSecurityType;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class TestProjectIssueSecurityContextProvider {
    private SecurityTypeManager securityTypeManager;
    private ProjectIssueSecuritySchemeHelper helper;
    private IssueSecuritySchemeManager issueSecuritySchemeManager;
    private IssueSecurityLevelManager issueSecurityLevelManager;
    private ContextProviderUtils contextProviderUtils;

    private ProjectIssueSecurityContextProvider service;

    @Before
    public void setUp() {
        securityTypeManager = mock(SecurityTypeManager.class);
        helper = mock(ProjectIssueSecuritySchemeHelper.class);
        issueSecuritySchemeManager = mock(IssueSecuritySchemeManager.class);
        issueSecurityLevelManager = mock(IssueSecurityLevelManager.class);
        contextProviderUtils = mock(ContextProviderUtils.class);
        service = new ProjectIssueSecurityContextProvider(issueSecuritySchemeManager, contextProviderUtils, issueSecurityLevelManager, securityTypeManager, helper);
    }

    /**
     * This is a white box test to ensure that null security types don't NPE, and to ensure they keep handling null security types
     */
    @Test
    public void testGetContextMapWithNullSecurityType() throws GenericEntityException {
        Project project = mock(Project.class);
        GenericValue projectGenericValue = mock(GenericValue.class);
        when(project.getGenericValue()).thenReturn(projectGenericValue);

        GenericValue issueTypeSchemeGenericValue = mock(GenericValue.class);
        when(issueSecuritySchemeManager.getSchemes(any(GenericValue.class))).thenReturn(Lists.newArrayList(issueTypeSchemeGenericValue));

        Map<String, Object> defaultContext = Maps.newHashMap();
        defaultContext.put(ContextProviderUtils.CONTEXT_PROJECT_KEY, project);
        defaultContext.put(ContextProviderUtils.CONTEXT_I18N_KEY, new MockI18nHelper());
        when(contextProviderUtils.getDefaultContext()).thenReturn(defaultContext);

        List<GenericValue> securityLevels = Lists.newArrayList();
        securityLevels.add(mock(GenericValue.class));
        when(issueSecurityLevelManager.getSchemeIssueSecurityLevels(anyLong())).thenReturn(securityLevels);

        List<GenericValue> securityEntities = Lists.newArrayList();
        GenericValue validEntity = mock(GenericValue.class);
        when(validEntity.getString("type")).thenReturn("valid");
        GenericValue invalidEntity = mock(GenericValue.class);
        when(invalidEntity.getString("type")).thenReturn("invalid");
        securityEntities.add(validEntity);
        securityEntities.add(invalidEntity);
        when(issueSecuritySchemeManager.getEntities(any(GenericValue.class), anyLong())).thenReturn(securityEntities);

        AbstractSecurityType valid = mock(AbstractSecurityType.class);
        when(valid.getDisplayName()).thenReturn("Valid");
        when(valid.getType()).thenReturn("valid");
        when(securityTypeManager.getSchemeType("valid")).thenReturn(valid);
        when(securityTypeManager.getSchemeType("invalid")).thenReturn(null);

        Map<String, Object> initialMap = Maps.newHashMap();
        Map<String, Object> result = service.getContextMap(initialMap);

        final List<IssueSecurity> issueSecurities = (List<IssueSecurity>) result.get("issueSecurities");
        assertNotNull(issueSecurities);
        assertEquals(1, issueSecurities.size());
        assertEquals(2, issueSecurities.get(0).getEntities().size());
    }
}
