package com.atlassian.jira.projectconfig.beans;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.rest.beans.SimpleRestProject;

import com.google.common.base.Objects;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

/**
 * @since v6.2
 */
public class SimpleRestProjectMatcher extends TypeSafeMatcher<SimpleRestProject> {
    private String name;
    private String key;
    private long id;
    private String description;
    private String url;

    public SimpleRestProjectMatcher() {
    }

    public SimpleRestProjectMatcher shortProject(Project project) {
        this.name = project.getName();
        this.key = project.getKey();
        this.id = project.getId();

        return this;
    }

    public SimpleRestProjectMatcher simpleProject(SimpleRestProject project) {
        this.name = project.getName();
        this.key = project.getKey();
        this.id = project.getId();
        this.description = project.getDescription();
        this.url = project.getUrl();

        return this;
    }

    public SimpleRestProjectMatcher fullProject(final Project project) {
        shortProject(project);

        this.url = project.getUrl();
        this.description = project.getDescription();
        return this;
    }


    @Override
    protected boolean matchesSafely(final SimpleRestProject item) {
        return Objects.equal(item.getId(), id) &&
                Objects.equal(item.getKey(), key) &&
                Objects.equal(item.getName(), name) &&
                Objects.equal(item.getDescription(), description) &&
                Objects.equal(item.getUrl(), url);
    }

    @Override
    protected void describeMismatchSafely(final SimpleRestProject item, final Description mismatchDescription) {
        mismatchDescription.appendText(String.format("Proj[%s (%d) - %s, desc: %s, url: %s]",
                item.getKey(), item.getId(), item.getName(), item.getDescription(), item.getUrl()));
    }

    @Override
    public void describeTo(final Description description) {
        description.appendText(String.format("Proj[%s (%d) - %s, desc: %s, url: %s]",
                key, id, name, description, url));
    }
}
