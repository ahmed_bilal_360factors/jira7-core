package com.atlassian.jira.projectconfig.contextproviders;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import com.atlassian.jira.config.managedconfiguration.ManagedConfigurationItemService;
import com.atlassian.jira.issue.RendererManager;
import com.atlassian.jira.issue.comparator.ProjectNameComparator;
import com.atlassian.jira.issue.fields.MockOrderableField;
import com.atlassian.jira.issue.fields.MockProjectFieldLayoutSchemeHelper;
import com.atlassian.jira.issue.fields.ProjectFieldLayoutSchemeHelper;
import com.atlassian.jira.issue.fields.config.manager.IssueTypeSchemeManager;
import com.atlassian.jira.issue.fields.layout.field.EditableDefaultFieldLayout;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutScheme;
import com.atlassian.jira.issue.fields.layout.field.MockFieldLayout;
import com.atlassian.jira.issue.fields.layout.field.MockFieldLayoutManager;
import com.atlassian.jira.issue.fields.screen.FieldScreenManager;
import com.atlassian.jira.issue.fields.screen.FieldScreenTab;
import com.atlassian.jira.issue.fields.screen.MockFieldScreen;
import com.atlassian.jira.issue.issuetype.MockIssueType;
import com.atlassian.jira.mock.MockRendererManager;
import com.atlassian.jira.mock.issue.fields.layout.field.MockFieldConfigurationScheme;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.beans.SimpleFieldConfigScheme;
import com.atlassian.jira.projectconfig.beans.SimpleIssueType;
import com.atlassian.jira.projectconfig.beans.SimpleIssueTypeImpl;
import com.atlassian.jira.projectconfig.order.MockOrderFactory;
import com.atlassian.jira.projectconfig.order.OrderFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.jira.util.UrlBuilder;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.jira.util.collect.MapBuilder;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.4
 */
@RunWith(MockitoJUnitRunner.class)
public class TestProjectFieldsContextProvider {
    @Mock
    private ContextProviderUtils providerUtils;
    @Mock
    private IssueTypeSchemeManager issueTypeSchemeManager;
    @Mock
    private FieldLayoutScheme fieldLayoutScheme;
    @Mock
    private FieldScreenManager fieldScreenManager;

    private JiraAuthenticationContext authenticationContext;
    private MockFieldLayoutManager fieldLayoutManager;
    private MockRendererManager rendererManager;
    private MockApplicationUser user;
    private MockProjectFieldLayoutSchemeHelper helper;
    private MockOrderFactory comparatorFactory;
    private ManagedConfigurationItemService managedConfigurationItemService = null;

    @Before
    public void setUp() throws Exception {
        user = new MockApplicationUser("mtan");
        authenticationContext = new MockSimpleAuthenticationContext(user, Locale.ENGLISH, new NoopI18nHelper());
        fieldLayoutManager = new MockFieldLayoutManager();
        rendererManager = new MockRendererManager();
        helper = new MockProjectFieldLayoutSchemeHelper();
        comparatorFactory = new MockOrderFactory();
    }

    @Test
    public void testGetContextMapForSystemDefault() throws Exception {
        // setup
        final MockProject expectedProject = new MockProject(588L, "Something");
        Map<String, Object> argumentContext = MapBuilder.<String, Object>build("argument", true);
        Map<String, Object> defaultContext = MapBuilder.build("default", true, "project", expectedProject);


        final SimpleFieldConfigBuilder simpleFieldConfigBuilder = new SimpleFieldConfigBuilder();
        final ProjectFieldsContextProvider.SimpleFieldConfig fieldConfig = simpleFieldConfigBuilder
                .setName("System default")
                .build();
        final List<ProjectFieldsContextProvider.SimpleFieldConfig> systemFieldConfigs = CollectionBuilder.newBuilder(fieldConfig).asMutableList();

        final SimpleFieldConfigScheme systemFieldConfigScheme = new SimpleFieldConfigScheme(null, null, null, null, null);

        // no field layout schemes
        fieldLayoutManager.setFieldLayoutSchemes(Collections.<FieldLayoutScheme>emptyList())
                .setFieldConfigurationScheme(expectedProject, null);

        ProjectFieldsContextProvider provider = new ProjectFieldsContextProvider(providerUtils, fieldLayoutManager,
                rendererManager, authenticationContext, fieldScreenManager, helper, issueTypeSchemeManager, comparatorFactory, managedConfigurationItemService) {
            @Override
            List<SimpleFieldConfig> getSystemDefaultSimpleFieldConfig(Project project) {
                assertEquals(expectedProject, project);
                return systemFieldConfigs;
            }

            @Override
            SimpleFieldConfigScheme getSystemDefaultFieldConfigScheme(Project project) {
                assertEquals(expectedProject, project);
                return systemFieldConfigScheme;
            }
        };

        when(providerUtils.getDefaultContext()).thenReturn(defaultContext);

        // execute
        Map<String, Object> actualMap = provider.getContextMap(argumentContext);

        // verify
        MapBuilder expectedMap = MapBuilder.newBuilder(argumentContext).addAll(defaultContext)
                .add(ProjectFieldsContextProvider.CONTEXT_FIELD_CONFIGS, systemFieldConfigs)
                .add(ProjectFieldsContextProvider.CONTEXT_FIELDS_SCHEME, systemFieldConfigScheme);

        assertEquals(expectedMap.toMap(), actualMap);

        // no field configuration scheme
        fieldLayoutManager.setFieldLayoutSchemes(Collections.singletonList(fieldLayoutScheme))
                .setFieldConfigurationScheme(expectedProject, null);

        // execute again
        actualMap = provider.getContextMap(argumentContext);

        // verify new map
        expectedMap = MapBuilder.newBuilder(argumentContext).addAll(defaultContext)
                .add(ProjectFieldsContextProvider.CONTEXT_FIELD_CONFIGS, systemFieldConfigs)
                .add(ProjectFieldsContextProvider.CONTEXT_FIELDS_SCHEME, systemFieldConfigScheme);

        assertEquals(expectedMap.toMap(), actualMap);
    }

    @Test
    public void testGetContextMapForSystemDefaultUsedByOtherProjects() throws Exception {
        // setup
        final List<SimpleIssueType> expectedProjectIssueTypes = new ArrayList<SimpleIssueType>();
        final MockIssueType bug = new MockIssueType("Bug", "Bug");
        expectedProjectIssueTypes.add(new SimpleIssueTypeImpl(bug, true));
        final MockIssueType task = new MockIssueType("Task", "Task");
        expectedProjectIssueTypes.add(new SimpleIssueTypeImpl(task, false));

        final MockProject expectedProject = new MockProject(588L, "Something")
                .setIssueTypes(bug, task);

        Map<String, Object> argumentContext = MapBuilder.<String, Object>build("argument", true);
        Map<String, Object> defaultContext = MapBuilder.build("default", true, "project", expectedProject);

        final MockProject sharedByProject1 = new MockProject(688L, "Something 1");
        final MockProject sharedByProject2 = new MockProject(788L, "Something 2");
        final Set<Project> expectedSharedProjects = Sets.newTreeSet(ProjectNameComparator.COMPARATOR);
        expectedSharedProjects.add(sharedByProject1);
        expectedSharedProjects.add(sharedByProject2);

        final SimpleFieldConfigBuilder simpleFieldConfigBuilder = new SimpleFieldConfigBuilder();
        final SimpleFieldConfigScheme systemFieldConfigScheme = new SimpleFieldConfigScheme(null, null, null, null, null);

        final MockOrderableField mockField = new MockOrderableField("fieldId", "fieldName");

        final MockFieldLayout expectedFieldLayout = new MockFieldLayout();
        expectedFieldLayout
                .setId(999L)
                .addFieldLayoutItem(mockField)
                .setRendererType("layoutItemRendererType");

        final MockFieldScreen expectedFieldScreen = new MockFieldScreen();
        expectedFieldScreen.setId(9898L);
        expectedFieldScreen.setName("fieldScreen");


        fieldLayoutManager.setFieldLayoutSchemes(Collections.<FieldLayoutScheme>emptyList())
                .setFieldLayout(null, expectedFieldLayout)
                .setFieldConfigurationScheme(expectedProject, null);

        final List<ProjectFieldsContextProvider.SimpleFieldLayoutItem> expectedFieldLayoutItems =
                Lists.newArrayList(
                        new ProjectFieldsContextProvider.SimpleFieldLayoutItem("fieldId", "fieldName", null, false, true, true, "rendererType", false, 1)
                );

        final ProjectFieldsContextProvider.SimpleFieldConfig expectedFieldConfig = simpleFieldConfigBuilder
                .setSharedProjects(expectedSharedProjects)
                .setName(EditableDefaultFieldLayout.NAME)
                .setDescription(EditableDefaultFieldLayout.DESCRIPTION)
                .setUrl("default")
                .setDefaultFieldConfig(true)
                .setIssueTypes(expectedProjectIssueTypes)
                .setFieldLayoutItems(expectedFieldLayoutItems)
                .build();

        final List<ProjectFieldsContextProvider.SimpleFieldConfig> expectedFieldConfigs = Lists.newArrayList(expectedFieldConfig);

        final FieldScreenTab fieldScreenTab = mock(FieldScreenTab.class);
        when(fieldScreenTab.getFieldScreen()).thenReturn(expectedFieldScreen);
        when(providerUtils.getDefaultContext()).thenReturn(defaultContext);
        when(fieldScreenManager.getFieldScreenTabs(mockField.getId()))
                .thenReturn(Collections.<FieldScreenTab>singleton(fieldScreenTab));

        helper.setProjectsForFieldLayout(expectedFieldLayout, Lists.<Project>newArrayList(sharedByProject1, sharedByProject2));

        when(issueTypeSchemeManager.getDefaultIssueType(expectedProject)).thenReturn(bug);


        ProjectFieldsContextProvider provider = new PartiallyStubbedProjectFieldsContextProvider(providerUtils, fieldLayoutManager,
                rendererManager, authenticationContext, fieldScreenManager, helper, issueTypeSchemeManager, comparatorFactory, managedConfigurationItemService) {
            @Override
            SimpleFieldConfigScheme getSystemDefaultFieldConfigScheme(Project project) {
                assertEquals(expectedProject, project);
                return systemFieldConfigScheme;
            }

        };

        // execute
        Map<String, Object> actualMap = provider.getContextMap(argumentContext);

        // verify
        assertEquals(expectedFieldConfigs, actualMap.get(ProjectFieldsContextProvider.CONTEXT_FIELD_CONFIGS));
        assertEquals(systemFieldConfigScheme, actualMap.get(ProjectFieldsContextProvider.CONTEXT_FIELDS_SCHEME));
    }

    @Test
    public void testGetContextMapForNonSystemDefaultSchemeUsedByOtherProjects() throws Exception {
        // setup
        final List<SimpleIssueType> expectedProjectIssueTypes = new ArrayList<SimpleIssueType>();
        final MockIssueType bug = new MockIssueType("Bug", "Bug");
        expectedProjectIssueTypes.add(new SimpleIssueTypeImpl(bug, true));
        final MockIssueType task = new MockIssueType("Task", "Task");
        expectedProjectIssueTypes.add(new SimpleIssueTypeImpl(task, false));

        final MockProject expectedProject = new MockProject(588L, "Something")
                .setIssueTypes(bug, task);

        Map<String, Object> argumentContext = MapBuilder.<String, Object>build("argument", true);
        Map<String, Object> defaultContext = MapBuilder.build("default", true, "project", expectedProject);

        final MockProject sharedByProject1 = new MockProject(688L, "Something 1");
        final MockProject sharedByProject2 = new MockProject(788L, "Something 2");
        final Set<Project> expectedSharedProjects = Sets.newTreeSet(ProjectNameComparator.COMPARATOR);
        expectedSharedProjects.add(sharedByProject1);
        expectedSharedProjects.add(sharedByProject2);

        final SimpleFieldConfigBuilder simpleFieldConfigBuilder = new SimpleFieldConfigBuilder();
        final SimpleFieldConfigScheme expectedFieldConfigScheme = new SimpleFieldConfigScheme(
                3838L, "fieldConfigSchemeName", "fieldConfigSchemeDescription", "change588", "edit3838");

        final MockOrderableField mockField = new MockOrderableField("fieldId", "fieldName");

        final MockFieldLayout expectedFieldLayout = new MockFieldLayout();
        expectedFieldLayout
                .setId(999L)
                .addFieldLayoutItem(mockField)
                .setRendererType("layoutItemRendererType");
        final MockFieldScreen expectedFieldScreen = new MockFieldScreen();
        expectedFieldScreen.setId(9898L);
        expectedFieldScreen.setName("fieldScreen");


        final MockFieldConfigurationScheme fieldConfigScheme = new MockFieldConfigurationScheme()
                .setId(3838L)
                .setDescription("fieldConfigSchemeDescription")
                .setName("fieldConfigSchemeName")
                .setIssueTypeToFieldLayoutIdMapping(MapBuilder.<String, Long>build(
                        "Bug", 999L,
                        "Task", 999L
                ));


        final FieldLayoutScheme fieldLayoutscheme = mock(FieldLayoutScheme.class);

        fieldLayoutManager.setFieldLayoutSchemes(Collections.singletonList(fieldLayoutscheme))
                .setFieldLayout(null, expectedFieldLayout)
                .setFieldConfigurationScheme(expectedProject, fieldConfigScheme)
                .setFieldLayout(expectedProject, "Bug", expectedFieldLayout)
                .setFieldLayout(expectedProject, "Task", expectedFieldLayout);

        final List<ProjectFieldsContextProvider.SimpleFieldLayoutItem> expectedFieldLayoutItems =
                Lists.newArrayList(
                        new ProjectFieldsContextProvider.SimpleFieldLayoutItem("fieldId", "fieldName", null, false, true, true, "rendererType", false,
                                1)
                );

        final ProjectFieldsContextProvider.SimpleFieldConfig expectedFieldConfig = simpleFieldConfigBuilder
                .setSharedProjects(expectedSharedProjects)
                .setName(EditableDefaultFieldLayout.NAME)
                .setDescription(EditableDefaultFieldLayout.DESCRIPTION)
                .setUrl("default")
                .setDefaultFieldConfig(false)
                .setIssueTypes(expectedProjectIssueTypes)
                .setFieldLayoutItems(expectedFieldLayoutItems)
                .build();

        final List<ProjectFieldsContextProvider.SimpleFieldConfig> expectedFieldConfigs = Lists.newArrayList(expectedFieldConfig);

        final FieldScreenTab fieldScreenTab = mock(FieldScreenTab.class);
        when(fieldScreenTab.getFieldScreen()).thenReturn(expectedFieldScreen);
        when(providerUtils.getDefaultContext()).thenReturn(defaultContext);
        when(fieldScreenManager.getFieldScreenTabs(mockField.getId()))
                .thenReturn(Collections.<FieldScreenTab>singleton(fieldScreenTab));

        when(issueTypeSchemeManager.getDefaultIssueType(expectedProject)).thenReturn(bug);

        helper.setProjectsForFieldLayout(expectedFieldLayout, Lists.<Project>newArrayList(sharedByProject1, sharedByProject2));

        ProjectFieldsContextProvider provider = new PartiallyStubbedProjectFieldsContextProvider(providerUtils, fieldLayoutManager,
                rendererManager, authenticationContext, fieldScreenManager, helper, issueTypeSchemeManager, comparatorFactory, managedConfigurationItemService);

        // execute
        Map<String, Object> actualMap = provider.getContextMap(argumentContext);

        // verify
        MapBuilder expectedMap = MapBuilder.newBuilder(argumentContext).addAll(defaultContext)
                .add(ProjectFieldsContextProvider.CONTEXT_FIELD_CONFIGS, expectedFieldConfigs)
                .add(ProjectFieldsContextProvider.CONTEXT_FIELDS_SCHEME, expectedFieldConfigScheme);

        assertEquals(expectedMap.toMap(), actualMap);
    }

    @Test
    public void testFieldConfigUrl() {
        // setup
        final long fieldConfigId = 4L;
        when(providerUtils.createUrlBuilder("/secure/admin/ConfigureFieldLayout!default.jspa"))
                .thenReturn(new UrlBuilder("fieldConfig", "UTF-8", false));

        // execute
        ProjectFieldsContextProvider provider = new ProjectFieldsContextProvider(providerUtils, fieldLayoutManager,
                rendererManager, authenticationContext, fieldScreenManager, helper, issueTypeSchemeManager, comparatorFactory, managedConfigurationItemService);

        // verify
        assertEquals("fieldConfig?id=" + fieldConfigId, provider.getFieldConfigUrl(fieldConfigId));
    }

    @Test
    public void testGetChangeSchemeUrl() {
        // setup
        final long projectId = 5L;
        when(providerUtils.createUrlBuilder("/secure/admin/SelectFieldLayoutScheme!default.jspa"))
                .thenReturn(new UrlBuilder("fieldConfig", "UTF-8", false));

        // execute
        ProjectFieldsContextProvider provider = new ProjectFieldsContextProvider(providerUtils, fieldLayoutManager,
                rendererManager, authenticationContext, fieldScreenManager, helper, issueTypeSchemeManager, comparatorFactory, managedConfigurationItemService);

        // verify
        assertEquals("fieldConfig?projectId=" + projectId, provider.getChangeSchemeUrl(projectId));
    }

    @Test
    public void testGetEditSchemeUrl() {
        // verify
        final long fieldConfigId = 2L;
        when(providerUtils.createUrlBuilder("/secure/admin/ConfigureFieldLayoutScheme.jspa"))
                .thenReturn(new UrlBuilder("fieldConfig", "UTF-8", false));

        // execute
        ProjectFieldsContextProvider provider = new ProjectFieldsContextProvider(providerUtils, fieldLayoutManager,
                rendererManager, authenticationContext, fieldScreenManager, helper, issueTypeSchemeManager, comparatorFactory, managedConfigurationItemService);

        // verify
        assertEquals("fieldConfig?id=" + fieldConfigId, provider.getEditSchemeUrl(fieldConfigId));
    }

    @Test
    public void testGetSystemDefaultEditSchemeUrl() {
        // verify
        when(providerUtils.createUrlBuilder("/secure/admin/ViewIssueFields.jspa"))
                .thenReturn(new UrlBuilder("fieldConfig", "UTF-8", false));

        // execute
        ProjectFieldsContextProvider provider = new ProjectFieldsContextProvider(providerUtils, fieldLayoutManager,
                rendererManager, authenticationContext, fieldScreenManager, helper, issueTypeSchemeManager, comparatorFactory, managedConfigurationItemService);

        // verify
        assertEquals("fieldConfig", provider.getSystemDefaultEditSchemeUrl());
    }

    public static class PartiallyStubbedProjectFieldsContextProvider extends ProjectFieldsContextProvider {

        public PartiallyStubbedProjectFieldsContextProvider(final ContextProviderUtils contextProviderUtils,
                                                            final FieldLayoutManager fieldLayoutManager, final RendererManager rendererManager,
                                                            final JiraAuthenticationContext jiraAuthenticationContext, final FieldScreenManager fieldScreenManager,
                                                            final ProjectFieldLayoutSchemeHelper projectFieldLayoutSchemeHelper, final IssueTypeSchemeManager issueTypeSchemeManager,
                                                            final OrderFactory orderFactory, final ManagedConfigurationItemService managedConfigurationItemService) {
            super(contextProviderUtils, fieldLayoutManager, rendererManager, jiraAuthenticationContext,
                    fieldScreenManager, projectFieldLayoutSchemeHelper, issueTypeSchemeManager, orderFactory, managedConfigurationItemService);
        }

        @Override
        String getChangeSchemeUrl(Long id) {
            return "change" + id;
        }

        @Override
        String getEditSchemeUrl(Long id) {
            return "edit" + id;
        }

        @Override
        String getFieldConfigUrl(Long id) {
            return "fieldconfig" + id;
        }

        @Override
        String getSystemDefaultEditSchemeUrl() {
            return "default";
        }

        @Override
        String getRendererType(String rendererType) {
            assertEquals("layoutItemRendererType", rendererType);
            return "rendererType";
        }

    }

    public static class SimpleFieldConfigBuilder {

        private Long id;
        private String name;
        private String description;
        private String url;
        private boolean defaultFieldConfig;
        private Collection<Project> sharedProjects;
        private List<SimpleIssueType> issueTypes;
        private List<ProjectFieldsContextProvider.SimpleFieldLayoutItem> fieldLayoutItems;

        public SimpleFieldConfigBuilder() {
            this.sharedProjects = Lists.newArrayList();
            this.issueTypes = Lists.newArrayList();
            this.fieldLayoutItems = Lists.newArrayList();
        }

        public SimpleFieldConfigBuilder setId(Long id) {
            this.id = id;
            return this;
        }

        public SimpleFieldConfigBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public SimpleFieldConfigBuilder setDescription(String description) {
            this.description = description;
            return this;
        }

        public SimpleFieldConfigBuilder setUrl(String url) {
            this.url = url;
            return this;
        }

        public SimpleFieldConfigBuilder setDefaultFieldConfig(boolean defaultFieldConfig) {
            this.defaultFieldConfig = defaultFieldConfig;
            return this;
        }

        public SimpleFieldConfigBuilder setSharedProjects(Collection<Project> sharedProjects) {
            this.sharedProjects = sharedProjects;
            return this;
        }

        public SimpleFieldConfigBuilder setIssueTypes(List<SimpleIssueType> issueTypes) {
            this.issueTypes = issueTypes;
            return this;
        }

        public SimpleFieldConfigBuilder setFieldLayoutItems(List<ProjectFieldsContextProvider.SimpleFieldLayoutItem> fieldLayoutItems) {
            this.fieldLayoutItems = fieldLayoutItems;
            return this;
        }

        public ProjectFieldsContextProvider.SimpleFieldConfig build() {
            return new ProjectFieldsContextProvider.SimpleFieldConfig(id, name, description, url, defaultFieldConfig, sharedProjects, fieldLayoutItems, issueTypes);
        }

    }

}
