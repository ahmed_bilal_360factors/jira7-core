package com.atlassian.jira.projectconfig.listener;

import com.atlassian.event.api.EventPublisher;

import com.atlassian.jira.event.user.LogoutEvent;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockComponentContainer;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserPropertyManager;
import com.atlassian.jira.web.SessionKeys;
import com.atlassian.mock.propertyset.MockPropertySet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestLogoutEventListener {
    private static final String SAMPLE_PROJECT_KEY = "PRJ";
    private static final String SAMPLE_TAB = "tab123";
    private static final String SAMPLE_BACK_TO_PROJECT_PATH = "/path456";

    @Rule
    public MockComponentContainer container = new MockComponentContainer(this);

    @Mock
    @AvailableInContainer
    private JiraAuthenticationContext authenticationContext;

    @Mock
    @AvailableInContainer
    private UserPropertyManager userPropertyManager;

    @Mock
    private ApplicationUser user;

    @Mock
    private EventPublisher eventPublisher;

    @InjectMocks
    private LogoutEventListener logoutEventListener;

    private MockPropertySet propertySet = new MockPropertySet();

    private LogoutEvent logoutEvent;

    @Before
    public void setUp() throws Exception {
        when(authenticationContext.getLoggedInUser()).thenReturn(user);

        this.logoutEventListener = new LogoutEventListener(eventPublisher, userPropertyManager);
        this.logoutEvent = new LogoutEvent(user);

        when(userPropertyManager.getPropertySet(user)).thenReturn(propertySet);
        when(authenticationContext.getLoggedInUser()).thenReturn(user);

        propertySet.setString(SessionKeys.CURRENT_ADMIN_PROJECT, SAMPLE_PROJECT_KEY);
        propertySet.setString(SessionKeys.CURRENT_ADMIN_PROJECT_TAB, SAMPLE_TAB);
        propertySet.setString(SessionKeys.CURRENT_ADMIN_PROJECT_RETURN_URL, SAMPLE_BACK_TO_PROJECT_PATH);
    }

    @Test
    public void willClearKeysOnLogout() {
        logoutEventListener.onLogoutEvent(logoutEvent);

        assertFalse(propertySet.exists(SessionKeys.CURRENT_ADMIN_PROJECT));
        assertFalse(propertySet.exists(SessionKeys.CURRENT_ADMIN_PROJECT_TAB));
        assertFalse(propertySet.exists(SessionKeys.CURRENT_ADMIN_PROJECT_RETURN_URL));
    }

    @Test
    public void willClearNothingWhenEventIsNull() {
        logoutEventListener.onLogoutEvent(null);

        assertTrue(propertySet.exists(SessionKeys.CURRENT_ADMIN_PROJECT));
        assertTrue(propertySet.exists(SessionKeys.CURRENT_ADMIN_PROJECT_TAB));
        assertTrue(propertySet.exists(SessionKeys.CURRENT_ADMIN_PROJECT_RETURN_URL));

        verify(userPropertyManager, never()).getPropertySet(any(ApplicationUser.class));
    }

    @Test
    public void willClearNothingWhenEventUserIsNull() {
        LogoutEvent nullUserlogoutEvent = new LogoutEvent(null);
        logoutEventListener.onLogoutEvent(nullUserlogoutEvent);

        assertTrue(propertySet.exists(SessionKeys.CURRENT_ADMIN_PROJECT));
        assertTrue(propertySet.exists(SessionKeys.CURRENT_ADMIN_PROJECT_TAB));
        assertTrue(propertySet.exists(SessionKeys.CURRENT_ADMIN_PROJECT_RETURN_URL));

        verify(userPropertyManager, never()).getPropertySet(any(ApplicationUser.class));
    }
}