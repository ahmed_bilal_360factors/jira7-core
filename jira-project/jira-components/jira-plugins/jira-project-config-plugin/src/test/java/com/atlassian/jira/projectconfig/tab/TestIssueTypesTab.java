package com.atlassian.jira.projectconfig.tab;

import java.io.IOException;
import java.io.Writer;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.issuetype.MockIssueType;
import com.atlassian.jira.mock.servlet.MockHttpServletRequest;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.servlet.MockVelocityRequestFactory;
import com.atlassian.jira.projectconfig.util.VelocityContextFactory;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.jira.web.pagebuilder.PageBuilder;
import com.atlassian.plugin.web.WebFragmentHelper;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.descriptors.WebItemModuleDescriptor;
import com.atlassian.plugin.web.descriptors.WebPanelModuleDescriptor;
import com.atlassian.plugin.web.descriptors.WebSectionModuleDescriptor;
import com.atlassian.plugin.web.model.WebPanel;
import com.atlassian.plugin.webresource.WebResourceManager;

import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * @since v6.2
 */
@RunWith(MockitoJUnitRunner.class)
public class TestIssueTypesTab {
    private final IssueType typeOne = new MockIssueType("1", "ONE");
    private final Project project = new MockProject(10101, "ABC").setIssueTypes(typeOne);
    private final ProjectConfigTabRenderContext ctx = new MockProjectConfigTabRenderContext();
    private final VelocityContextFactory contextFactory = new MockVelocityRequestFactory();
    private final WebInterfaceManager webInterfaceManager = new ToStringWebInterfaceManager();

    private MockHttpServletRequest request;

    private static final String ISSUE_TYPES_URL = "http://something.com/jira/project-config/ABC/issuetypes";

    /*@Mock
    private IssueTypeUrls urls;*/

    @Mock
    private WebResourceManager webResourceManager;

    @Mock
    private PageBuilderService pageBuilderService;

    private IssueTypesTab tab;

    @Before
    public void before() {
        tab = new IssueTypesTab(webInterfaceManager, contextFactory);
        request = new MockHttpServletRequest();
        request.setRequestURL(ISSUE_TYPES_URL);
    }

    @Test
    public void testId() {
        assertThat(tab.getId(), equalTo("issuetypes"));
    }

    @Test
    public void getLinkId() {
        assertThat(tab.getLinkId(), equalTo("view_project_issuetypes"));
    }

    @Test
    public void getTitle() {
        final String operand = NoopI18nHelper.makeTranslation("admin.project.issuetypes.title", project.getName());
        assertThat(tab.getTitle(ctx), equalTo(operand));
    }

    @Test
    public void addResourceForWorkflow() {
        tab.addResourceForProject(ctx);

        verify(webResourceManager).requireResource("com.atlassian.jira.jira-project-config-plugin:issuetypes-tab");
    }

    @Test
    public void getTabForSummary() {
        final String render = tab.getTab(ctx);

        final Map<String, Project> ctx = ImmutableMap.of(WebPanelTab.CURRENT_PROJECT, project);
        assertThat(render, equalTo(ToStringWebPanel.write("tabs.admin.projectconfig.issuetypes", ctx)));
    }

    @Test
    public void getTabForWorkflow() {
        request.setRequestURL(ISSUE_TYPES_URL + "/ONE/workflow");

        final String render = tab.getTab(ctx);

        assertThat(render, equalTo(""));
    }

    private class MockProjectConfigTabRenderContext implements ProjectConfigTabRenderContext {
        @Override
        public Project getProject() {
            return project;
        }

        @Override
        public HttpServletRequest getRequest() {
            return request;
        }

        @Override
        public String getPathInfo() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public Locale getLocale() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public ApplicationUser getUser() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public I18nHelper getI18NHelper() {
            return new NoopI18nHelper();
        }

        @Override
        public WebResourceManager getResourceManager() {
            return webResourceManager;
        }

        @Override
        public PageBuilderService getPageBuilderService() {
            return pageBuilderService;
        }
    }

    private static class ToStringWebPanel implements WebPanel {
        private final String location;

        private ToStringWebPanel(final String location) {
            this.location = location;
        }

        private static String write(String location, Map<String, ?> ctx) {
            StringBuilder builder = new StringBuilder(location);
            List<String> keys = Lists.newArrayList(ctx.keySet());
            Collections.sort(keys);

            for (String key : keys) {
                builder.append(key).append('=').append(ctx.get(key).toString());
            }
            return builder.toString();
        }

        @Override
        public String getHtml(final Map<String, Object> context) {
            return write(location, context);
        }

        @Override
        public void writeHtml(final Writer writer, final Map<String, Object> context) throws IOException {
            throw new UnsupportedOperationException("Not implemented");
        }
    }

    public static class ToStringWebInterfaceManager implements WebInterfaceManager {
        @Override
        public boolean hasSectionsForLocation(final String location) {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public List<WebSectionModuleDescriptor> getSections(final String location) {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public List<WebSectionModuleDescriptor> getDisplayableSections(final String location, final Map<String, Object> context) {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public List<WebItemModuleDescriptor> getItems(final String section) {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public List<WebItemModuleDescriptor> getDisplayableItems(final String section, final Map<String, Object> context) {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public List<WebPanel> getWebPanels(final String location) {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public List<WebPanel> getDisplayableWebPanels(final String location, final Map<String, Object> context) {
            return ImmutableList.<WebPanel>of(new ToStringWebPanel(location));
        }

        @Override
        public List<WebPanelModuleDescriptor> getWebPanelDescriptors(final String location) {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public List<WebPanelModuleDescriptor> getDisplayableWebPanelDescriptors(final String location, final Map<String, Object> context) {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public void refresh() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public WebFragmentHelper getWebFragmentHelper() {
            throw new UnsupportedOperationException("Not implemented");
        }
    }
}
