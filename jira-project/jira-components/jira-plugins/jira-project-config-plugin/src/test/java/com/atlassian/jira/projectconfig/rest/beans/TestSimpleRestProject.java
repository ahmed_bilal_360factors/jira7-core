package com.atlassian.jira.projectconfig.rest.beans;

import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.beans.SimpleRestProjectMatcher;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @since v6.2
 */
public class TestSimpleRestProject {
    private Project project;

    @Before
    public void init() {
        final MockProject mockProject = new MockProject(101010, "ABC");
        mockProject.setUrl("url");
        mockProject.setDescription("someDescription");

        project = mockProject;
    }

    @Test
    public void testShortBeanCreatedWithCorrectFields() {
        final SimpleRestProject simpleRestProject = SimpleRestProject.shortProject(project);
        assertThat(simpleRestProject, new SimpleRestProjectMatcher().shortProject(project));
    }

    @Test
    public void testShortBeanFunctionCreatedWithCorrectFields() {
        final SimpleRestProject simpleRestProject = SimpleRestProject.shortBeanFunc().apply(project);
        assertThat(simpleRestProject, new SimpleRestProjectMatcher().shortProject(project));
    }

    @Test
    public void testFullBeanCreatedWithAllFields() {
        final SimpleRestProject simpleRestProject = SimpleRestProject.fullProject(project);
        assertThat(simpleRestProject, new SimpleRestProjectMatcher().fullProject(project));
    }

    @Test
    public void testFullBeanFuncCreatedWithAllFields() {
        final SimpleRestProject simpleRestProject = SimpleRestProject.fullBeanFunc().apply(project);
        assertThat(simpleRestProject, new SimpleRestProjectMatcher().fullProject(project));
    }
}
