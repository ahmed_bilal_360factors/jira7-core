package com.atlassian.jira.projectconfig.util;

/**
 * @since v6.2
 */
public class NoopUrlEncoder implements UrlEncoder {
    @Override
    public String encode(final String value) {
        return value;
    }
}
