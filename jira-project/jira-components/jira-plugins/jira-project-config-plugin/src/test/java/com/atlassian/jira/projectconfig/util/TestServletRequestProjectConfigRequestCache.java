package com.atlassian.jira.projectconfig.util;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.atlassian.jira.project.MockProject;

import com.google.common.collect.Maps;

import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.4
 */
public class TestServletRequestProjectConfigRequestCache {
    @Test(expected = IllegalStateException.class)
    public void testProjectNotConfigured() throws Exception {
        ServletRequestProjectConfigRequestCache cache = getCache();
        cache.getProject();
    }

    @Test
    public void testProjectConfigured() throws Exception {
        MockProject project = new MockProject(67L);
        ServletRequestProjectConfigRequestCache cache = getCache();
        cache.setProject(project);
        assertSame(project, cache.getProject());
        assertSame(project, cache.getProject());
    }

    private ServletRequestProjectConfigRequestCache getCache() {
        final Map<String, Object> attributes = Maps.newHashMap();
        final HttpServletRequest request = mock(HttpServletRequest.class);
        doAnswer(new Answer<Object>() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                attributes.put((String) invocation.getArguments()[0], invocation.getArguments()[1]);
                return null;
            }
        }).when(request).setAttribute(anyString(), anyObject());
        when(request.getAttribute(anyString())).then(new Answer<Object>() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                return attributes.get(invocation.getArguments()[0]);
            }
        });
        return new ServletRequestProjectConfigRequestCache() {
            @Override
            HttpServletRequest getRequest() {
                return request;
            }
        };
    }
}
