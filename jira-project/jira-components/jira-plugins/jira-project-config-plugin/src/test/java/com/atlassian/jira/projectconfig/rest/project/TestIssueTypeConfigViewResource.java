package com.atlassian.jira.projectconfig.rest.project;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.core.Response;

import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.issue.fields.screen.MockFieldScreen;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.issuetype.MockIssueType;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.beans.ProjectContext;
import com.atlassian.jira.projectconfig.beans.SimpleRestProjectMatcher;
import com.atlassian.jira.projectconfig.rest.ResponseMatchers;
import com.atlassian.jira.projectconfig.rest.beans.SharedByData;
import com.atlassian.jira.projectconfig.rest.beans.SimpleRestProject;
import com.atlassian.jira.projectconfig.shared.SharedIssueTypeWorkflowData;
import com.atlassian.jira.projectconfig.util.ProjectContextLocator;
import com.atlassian.jira.projectconfig.issuetypes.fields.IssueTypeConfigFieldsHelper;

import com.google.common.base.Objects;
import com.google.common.collect.Lists;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.google.common.collect.ImmutableList.of;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

/**
 * @since v6.2
 */
@RunWith(MockitoJUnitRunner.class)
public class TestIssueTypeConfigViewResource {
    @Mock
    private ProjectContextLocator projectContextLocator;

    @Mock
    private IssueTypeConfigFieldsHelper fieldsHelper;

    private IssueTypeConfigFieldsResource resource;

    @Before
    public void before() {
        resource = new IssueTypeConfigFieldsResource(fieldsHelper, projectContextLocator);
    }

    @Test
    public void getWorkflowDataWhenProjectOrIssueTypeIsInvalid() {
        final String projectKey = "ABC";
        final long issueTypeId = 1010L;
        final String errorMessage = "Wtf";

        when(projectContextLocator.getContext(projectKey, issueTypeId))
                .thenReturn(ServiceOutcomeImpl.<ProjectContext>error(errorMessage,
                        com.atlassian.jira.util.ErrorCollection.Reason.NOT_FOUND));

        final Response response = resource.getFieldsData(projectKey, issueTypeId);

        assertThat(response, ResponseMatchers.noCache());
        assertThat(response, ResponseMatchers.status(Response.Status.NOT_FOUND));
        assertThat(response, ResponseMatchers.errorBody(errorMessage));
    }

    @Test
    public void getViewDataReturnsCorrectData() {
        final int issueTypeId = 1819;
        final IssueType type = new MockIssueType(issueTypeId, "Name");
        final Project project = new MockProject(10101028282L, "ABC").setIssueTypes(type);
        final ProjectContext context = new ProjectContext(project, type);
        final FieldScreen screen = new MockFieldScreen(10101);

        //Shared with these issue types.
        final IssueType otherType1 = new MockIssueType(383832828, "SharedType1");
        final IssueType otherType2 = new MockIssueType(89448, "SharedType2");

        //Shared with these projects.
        final Project otherProject = new MockProject(58699, "DEF");

        final SharedIssueTypeWorkflowData sharedByData =
                new SharedIssueTypeWorkflowData(of(otherProject), of(otherType1, otherType2));
        final IssueTypeConfigFieldsHelper.FieldsResult fieldsResult =
                new IssueTypeConfigFieldsHelper.FieldsResult(screen, sharedByData);

        when(projectContextLocator.getContext(project.getKey(), issueTypeId))
                .thenReturn(ServiceOutcomeImpl.ok(context));

        when(fieldsHelper.getFieldsData(context))
                .thenReturn(fieldsResult);

        final Response response = this.resource.getFieldsData(project.getKey(), issueTypeId);

        assertThat(response, ResponseMatchers.noCache());
        assertThat(response, ResponseMatchers.status(Response.Status.OK));
        assertThat(response, ResponseMatchers.body(IssueTypeConfigFieldsResource.FieldsDetails.class,
                new ViewDetailsMatcher()
                        .screen(screen)
                        .issueTypes(otherType1, otherType2)
                        .projects(otherProject)));
    }

    private static class ViewDetailsMatcher extends TypeSafeMatcher<IssueTypeConfigFieldsResource.FieldsDetails> {
        private long id;
        private String displayName;
        private List<String> issueTypes = Collections.emptyList();
        private List<SimpleRestProject> projects = Collections.emptyList();

        private ViewDetailsMatcher screen(FieldScreen screen) {
            this.id = screen.getId();
            this.displayName = screen.getName();

            return this;
        }

        private ViewDetailsMatcher issueTypes(IssueType... types) {
            this.issueTypes = Arrays.asList(types).stream().map(t -> t.getId()).collect(Collectors.toList());
            return this;
        }

        private ViewDetailsMatcher projects(Project... projects) {
            this.projects = Arrays.asList(projects).stream().map(p -> SimpleRestProject.shortProject(p)).collect(Collectors.toList());
            return this;
        }

        @Override
        public void describeTo(final Description description) {
            description
                    .appendText(String.format("[%s(%d) Issue Types: %s, Projects: %s]",
                            displayName, id, issueTypes, toString(projects)));
        }

        @Override
        protected boolean matchesSafely(final IssueTypeConfigFieldsResource.FieldsDetails item) {
            return id == item.getViewScreenId()
                    && Objects.equal(displayName, item.getScreenName())
                    && Objects.equal(issueTypes, item.getSharedWithIssueTypes())
                    && createProjectMatcher().matches(item.getSharedWithProjects());
        }

        @Override
        protected void describeMismatchSafely(final IssueTypeConfigFieldsResource.FieldsDetails item, final Description mismatchDescription) {
            mismatchDescription
                    .appendText(String.format("[%s(%d) Issue Types: %s, Projects: %s]",
                            item.getScreenName(), item.getViewScreenId(),
                            item.getSharedWithIssueTypes(), toString(item.getSharedWithProjects())));
        }

        private Matcher<Iterable<? extends SimpleRestProject>> createProjectMatcher() {
            List<Matcher<? super SimpleRestProject>> matchers = Lists.newArrayList();
            for (SimpleRestProject project : projects) {
                matchers.add(new SimpleRestProjectMatcher().simpleProject(project));
            }
            return Matchers.contains(matchers);
        }

        private static String toString(Iterable<SimpleRestProject> projects) {
            StringBuilder builder = new StringBuilder("[");
            boolean first = true;
            for (SimpleRestProject project : projects) {
                if (first) {
                    first = false;
                } else {
                    builder.append(", ");
                }
                builder.append(String.format("Proj[%s (%d) - %s]", project.getKey(), project.getId(), project.getName()));
            }

            return builder.append("]").toString();
        }
    }
}
