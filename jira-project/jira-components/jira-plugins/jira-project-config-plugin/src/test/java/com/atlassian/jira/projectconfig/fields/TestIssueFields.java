package com.atlassian.jira.projectconfig.fields;

import java.util.Arrays;
import java.util.List;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.MockOrderableField;
import com.atlassian.jira.issue.fields.OrderableField;
import com.atlassian.jira.issue.fields.layout.field.MockFieldLayout;
import com.atlassian.jira.issue.fields.layout.field.MockFieldLayoutItem;
import com.atlassian.jira.issue.fields.layout.field.MockFieldLayoutManager;
import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.issue.fields.screen.MockFieldScreen;
import com.atlassian.jira.issue.fields.screen.ProjectFieldScreenHelper;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.util.lang.Pair;

import com.google.common.collect.Lists;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestIssueFields {
    @Mock
    private ScreenFinder finder;

    @Mock
    private ProjectFieldScreenHelper helper;

    @Mock
    private CustomFieldManager customFieldManager;

    private MockFieldLayoutManager fieldLayoutManager = new MockFieldLayoutManager();

    @Mock
    private CustomField customField;

    @Mock
    private CustomField nonVisibleCustomField;

    private final MockOrderableField field = new MockOrderableField("yes");
    private final MockIssue issue = new MockIssue(10, "ABC-1");
    private final MockIssue issueOutOfScope = new MockIssue(11, "ABC-2");
    private MockFieldLayout fieldLayout;
    private MockFieldLayoutItem fieldLayoutItem;

    @Before
    public void beforeTest() {
        issue.setProjectId(191919L);
        issue.setIssueTypeId("issueTypeOfSomething");

        issueOutOfScope.setProjectId(23232L);
        issueOutOfScope.setIssueTypeId("outOfScopeIssueTypeId");

        when(customField.getId()).thenReturn("CustomFieldId");
        when(customField.getName()).thenReturn("CustomFieldName");

        when(nonVisibleCustomField.getId()).thenReturn("CustomFieldId2");
        when(nonVisibleCustomField.getName()).thenReturn("CustomFieldName2");

        when(customFieldManager.getCustomFieldObjects(issue))
                .thenReturn(Arrays.asList(customField));

        this.fieldLayout = fieldLayoutManager.addLayoutItem(issue);
        this.fieldLayoutItem = fieldLayout.addFieldLayoutItem(customField);
        this.fieldLayoutItem.setHidden(false);
    }

    @Test
    public void addFieldToIssueScreens() {
        final FieldScreen screenOne = createScreen(2);
        final FieldScreen screenTwo = createScreen(1);

        when(finder.getIssueScreens(issue)).thenReturn(asList(screenOne, screenTwo));

        final IssueFields fields = new IssueFields(finder, helper, customFieldManager, fieldLayoutManager);
        fields.addFieldToIssueScreens(field, issue);

        assertNotNull(screenOne.getTab(0).getFieldScreenLayoutItem(field.getId()));
        assertNotNull(screenTwo.getTab(0).getFieldScreenLayoutItem(field.getId()));
    }

    @Test
    public void addFieldToIssueScreensAlreadyOnScreen() {
        final FieldScreen screenOne = createScreen(2);
        screenOne.getTab(1).addFieldScreenLayoutItem(field.getId());
        final FieldScreen screenTwo = createScreen(1);

        when(finder.getIssueScreens(issue)).thenReturn(asList(screenOne, screenTwo));

        final IssueFields fields = new IssueFields(finder, helper, customFieldManager, fieldLayoutManager);
        fields.addFieldToIssueScreens(field, issue);

        //The field should already be on this screen so don't add it twice.
        assertNull(screenOne.getTab(0).getFieldScreenLayoutItem(field.getId()));
        assertNotNull(screenTwo.getTab(0).getFieldScreenLayoutItem(field.getId()));
    }

    @Test
    public void addFieldToIssueScreensNoTabs() {
        final FieldScreen screenOne = createScreen(0);
        final FieldScreen screenTwo = createScreen(1);

        when(finder.getIssueScreens(issue)).thenReturn(asList(screenOne, screenTwo));

        final IssueFields fields = new IssueFields(finder, helper, customFieldManager, fieldLayoutManager);
        fields.addFieldToIssueScreens(field, issue);

        assertNotNull(screenTwo.getTab(0).getFieldScreenLayoutItem(field.getId()));
    }

    @Test
    public void getAffectedProjects() {
        Project one = new MockProject(10, "ABC", "ABC");
        Project two = new MockProject(11, "DEF", "DEF");
        Project three = new MockProject(12, "HIJ", "HIJ");
        Project four = new MockProject(13, "abb", "abb");

        final FieldScreen screenOne = createScreen(0);
        final FieldScreen screenTwo = createScreen(1);

        when(finder.getIssueScreens(issue)).thenReturn(asList(screenOne, screenTwo));
        when(helper.getProjectsForFieldScreen(screenOne)).thenReturn(asList(three, two));
        when(helper.getProjectsForFieldScreen(screenTwo)).thenReturn(asList(three, one, four));

        final IssueFields fields = new IssueFields(finder, helper, customFieldManager, fieldLayoutManager);
        fields.addFieldToIssueScreens(field, issue);

        assertThat(fields.getAffectedProjects(issue), Matchers.contains(four, one, two, three));
    }

    @Test
    public void getAllCustomFields() {
        final IssueFields fields = new IssueFields(finder, helper, customFieldManager, fieldLayoutManager);
        Iterable<OrderableField> customFields = fields.getAllCustomFields(issue);

        assertEquals(1, Lists.newArrayList(customFields).size());
        assertThat(customFields, Matchers.<OrderableField>contains(customField));
    }

    @Test
    public void getAllCustomFieldsWithHiddenField() {
        final IssueFields fields = new IssueFields(finder, helper, customFieldManager, fieldLayoutManager);
        fieldLayoutItem.setHidden(true);
        Iterable<OrderableField> customFields = fields.getAllCustomFields(issue);

        assertEquals(0, Lists.newArrayList(customFields).size());
    }

    @Test
    public void getAllCustomFieldsWithOnScreenFlagReturnsCustomFieldWithOnScreenFlagSetToTrueWhenOnlyScreenHasIt() {
        final FieldScreen screen = createScreen(1);
        screen.getTab(0).addFieldScreenLayoutItem(customField.getId());
        when(finder.getIssueScreens(issue)).thenReturn(Arrays.asList(screen));

        final IssueFields issueFields = new IssueFields(finder, helper, customFieldManager, fieldLayoutManager);
        List<Pair<OrderableField, Boolean>> resultFields = fieldVisibilityPairIterableToList(issueFields.getAllCustomFieldsWithOnAllScreensFlag(issue));
        assertEquals(1, resultFields.size());
        assertEquals(customField, resultFields.get(0).first());
        assertTrue("Field that is on an issue's only screen should have the onScreen flag.", resultFields.get(0).second());
    }

    @Test
    public void getAllCustomFieldsWithOnScreenFlagReturnsCustomFieldWithOnScreenFlagSetToFalseWhenOnlyScreenDoesNotHaveIt() {
        final FieldScreen screen = createScreen(1);
        when(finder.getIssueScreens(issue)).thenReturn(Arrays.asList(screen));

        final IssueFields issueFields = new IssueFields(finder, helper, customFieldManager, fieldLayoutManager);
        List<Pair<OrderableField, Boolean>> resultFields = fieldVisibilityPairIterableToList(issueFields.getAllCustomFieldsWithOnAllScreensFlag(issue));
        assertEquals(1, resultFields.size());
        assertEquals(customField, resultFields.get(0).first());
        assertFalse("Field that is not on a screen should not have the onScreen flag.", resultFields.get(0).second());
    }

    @Test
    public void getAllCustomFieldsWithOnScreenFlagReturnsCustomFieldWithOnScreenFlagSetToFalseWhenCustomFieldOnOnlyOneOfTwoScreens() {
        final FieldScreen screen1 = createScreen(1);
        screen1.getTab(0).addFieldScreenLayoutItem(customField.getId());
        final FieldScreen screen2 = createScreen(2);
        when(finder.getIssueScreens(issue)).thenReturn(Arrays.asList(screen1, screen2));

        final IssueFields issueFields = new IssueFields(finder, helper, customFieldManager, fieldLayoutManager);
        List<Pair<OrderableField, Boolean>> resultFields = fieldVisibilityPairIterableToList(issueFields.getAllCustomFieldsWithOnAllScreensFlag(issue));
        assertEquals(1, resultFields.size());
        assertEquals(customField, resultFields.get(0).first());
        assertFalse("Field that is only on one of the issue's screens should have the onScreen flag set to false.", resultFields.get(0).second());
    }

    @Test
    public void getAllCustomFieldsWithOnScreenFlagReturnsCustomFieldWithOnScreenFlagSetToTrueWhenCustomFieldOnBothOfTwoScreens() {
        final FieldScreen screen1 = createScreen(1);
        screen1.getTab(0).addFieldScreenLayoutItem(customField.getId());
        final FieldScreen screen2 = createScreen(2);
        screen2.getTab(0).addFieldScreenLayoutItem(customField.getId());
        when(finder.getIssueScreens(issue)).thenReturn(Arrays.asList(screen1, screen2));

        final IssueFields issueFields = new IssueFields(finder, helper, customFieldManager, fieldLayoutManager);
        List<Pair<OrderableField, Boolean>> resultFields = fieldVisibilityPairIterableToList(issueFields.getAllCustomFieldsWithOnAllScreensFlag(issue));
        assertEquals(1, resultFields.size());
        assertEquals(customField, resultFields.get(0).first());
        assertTrue("Field that is on both of the issue's screens should have the onScreen flag set to true.", resultFields.get(0).second());
    }

    @Test
    public void getAllCustomFieldsWithOnAllScreensFlagDoesNotReturnHiddenFields() {
        this.fieldLayoutItem.setHidden(true);
        final IssueFields issueFields = new IssueFields(finder, helper, customFieldManager, fieldLayoutManager);
        List<Pair<OrderableField, Boolean>> resultFields = fieldVisibilityPairIterableToList(issueFields.getAllCustomFieldsWithOnAllScreensFlag(issue));
        assertEquals(0, resultFields.size());
    }

    private FieldScreen createScreen(final int tabCount) {
        final MockFieldScreen mockScreen = new MockFieldScreen();
        for (int i = 0; i < tabCount; i++) {
            mockScreen.addMockTab();
        }
        return mockScreen;
    }

    private List<Pair<OrderableField, Boolean>> fieldVisibilityPairIterableToList(Iterable<Pair<OrderableField, Boolean>> iterables) {
        return Lists.newArrayList(iterables);
    }
}
