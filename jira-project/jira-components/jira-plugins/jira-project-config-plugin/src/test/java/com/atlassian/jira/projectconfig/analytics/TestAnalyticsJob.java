package com.atlassian.jira.projectconfig.analytics;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.issue.issuetype.MockIssueType;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.projectconfig.event.WorkflowsStatisticEvent;
import com.atlassian.jira.workflow.MockJiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.WorkflowSchemeManager;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestAnalyticsJob {

    @Mock
    private ProjectManager projectManager;

    @Mock
    private WorkflowSchemeManager workflowSchemeManager;

    @Mock
    private WorkflowManager workflowManager;

    @Mock
    private EventPublisher eventPublisher;

    private final MockProject PROJECT_ONE = new MockProject(1L, "ABC", "ABC");
    private final MockProject PROJECT_TWO = new MockProject(2L, "CBA", "CBA");

    private final MockIssueType ISSUE_TYPE_ONE = new MockIssueType("1", "1");
    private final MockIssueType ISSUE_TYPE_TWO = new MockIssueType("2", "2");
    private final MockIssueType ISSUE_TYPE_THREE = new MockIssueType("3", "3");

    private AnalyticsJob job;

    @Before
    public void before() {
        when(projectManager.getProjects()).thenReturn(ImmutableList.of(PROJECT_ONE, PROJECT_TWO));
        when(workflowManager.getWorkflows()).thenReturn(ImmutableList.of());
        when(workflowManager.getActiveWorkflows()).thenReturn(ImmutableList.of());


        job = new AnalyticsJob(projectManager, workflowSchemeManager, workflowManager, eventPublisher);
    }

    @Test
    public void shouldReturnStatisticsAboutWorkflows(){
        setWorkflowMapForProject(PROJECT_ONE, ISSUE_TYPE_ONE);
        setWorkflowMapForProject(PROJECT_TWO, ISSUE_TYPE_TWO);

        job.runJob(null);

        assertPublishedEvent(2,2,0,0);
    }

    @Test
    public void shouldReturnInformationAboutSharedWorkflows(){
        setWorkflowMapForProject(PROJECT_ONE, ISSUE_TYPE_ONE);
        setWorkflowMapForProject(PROJECT_TWO, ISSUE_TYPE_ONE, ISSUE_TYPE_TWO);

        job.runJob(null);

        assertPublishedEvent(2,1,1,0);
    }

    @Test
    public void shouldTreatWorkflowsUsedInTheSameProjectAsIsolated(){
        setWorkflowMapForProject(PROJECT_ONE, new MockIssueType("1", "Test"), new MockIssueType("2", "Test"));
        setWorkflowMapForProject(PROJECT_TWO, ISSUE_TYPE_THREE);

        job.runJob(null);

        assertPublishedEvent(2,2,0,0);
    }

    @Test
    public void shouldCountInactiveWorkflows() throws Exception {
        setWorkflowMapForProject(PROJECT_ONE, ISSUE_TYPE_ONE);
        setWorkflowMapForProject(PROJECT_TWO, ISSUE_TYPE_ONE, ISSUE_TYPE_TWO);

        when(workflowManager.getWorkflows()).thenReturn(ImmutableList.of(new MockJiraWorkflow()));

        job.runJob(null);

        assertPublishedEvent(3,1,1,1);
    }

    @Test
    public void shouldUseDefaultWorkflowIfNeeded() throws Exception {
        setWorkflowMapForProject(PROJECT_ONE, new MockIssueType("test", null));
        setWorkflowMapForProject(PROJECT_TWO, ISSUE_TYPE_ONE);

        job.runJob(null);

        assertPublishedEvent(2,2,0,0);
    }


    private void assertPublishedEvent(long workflowsTotal, long isolatedWorkflows, long sharedWorkflows, long inactiveWorkflows) {
        final WorkflowsStatisticEvent event = new WorkflowsStatisticEvent(workflowsTotal, isolatedWorkflows, sharedWorkflows, inactiveWorkflows);
        ArgumentCaptor<Object> argument = ArgumentCaptor.forClass(Object.class);

        verify(eventPublisher).publish(argument.capture());
        assertEquals(event, argument.getValue());
    }

    /*
        Issue type name is treated as workflow id
     */
    private void setWorkflowMapForProject(MockProject project, MockIssueType... issueTypes) {
        project.setIssueTypes(issueTypes);

        // I can't put null as a value in Collectors.toMap

        final Map<String, String> mapKeys = new HashMap<>();
        Arrays.stream(issueTypes).forEach(it -> mapKeys.put(it.getId(), it.getName()));

        when(workflowSchemeManager.getWorkflowMap(project)).thenReturn(mapKeys);

    }
}
