package com.atlassian.jira.projectconfig.util;

import com.atlassian.jira.config.Feature;
import com.atlassian.jira.mock.MockFeatureManager;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.projectconfig.contextproviders.ContextProviderUtils;
import com.google.common.base.Function;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.4
 */
public class TestDefaultTabUrlFactory {

    private MockFeatureManager featureManager;
    final private Feature versionsFeature = () -> DefaultTabUrlFactory.ENABLE_NEW_MENU_FEATURE_KEY;
    final private Feature componentsFeature = () -> DefaultTabUrlFactory.ENABLE_NEW_COMPONENTS_FEATURE_KEY;

    @Before
    public void setUp() {
        featureManager = new MockFeatureManager();
        featureManager.enable(componentsFeature);
        featureManager.disable(versionsFeature);
    }

    @Test
    public void testForSummary() {
        assertFunctionGivesCorrectUriForPathAndPrefixes(null, DefaultTabUrlFactory::forSummary);
    }

    @Test
    public void testForVersions() {
        assertFunctionGivesCorrectUriForPathAndPrefixes("versions", DefaultTabUrlFactory::forVersions);
    }

    @Test
    public void testForAdministerVersions() {
        featureManager.enable(versionsFeature);
        assertFunctionGivesCorrectUriForPathAndPrefixes("administer-versions", DefaultTabUrlFactory::forVersions);
    }

    @Test
    public void testForComponents() {
        featureManager.disable(componentsFeature);
        assertFunctionGivesCorrectUriForPathAndPrefixes("components", DefaultTabUrlFactory::forComponents);
    }

    @Test
    public void testForAdministerComponents() {
        assertFunctionGivesCorrectUriForPathAndPrefixes("administer-components", DefaultTabUrlFactory::forComponents);
    }

    @Test
    public void testForIssueSecurity() {
        assertFunctionGivesCorrectUriForPathAndPrefixes("issuesecurity", DefaultTabUrlFactory::forIssueSecurity);
    }

    @Test
    public void testForPermissions() {
        assertFunctionGivesCorrectUriForPathAndPrefixes("permissions", DefaultTabUrlFactory::forPermissions);
    }

    @Test
    public void testForWorkflows() {
        assertFunctionGivesCorrectUriForPathAndPrefixes("workflows", DefaultTabUrlFactory::forWorkflows);
    }

    @Test
    public void testForFields() {
        assertFunctionGivesCorrectUriForPathAndPrefixes("fields", DefaultTabUrlFactory::forFields);
    }

    @Test
    public void testForScreens() {
        assertFunctionGivesCorrectUriForPathAndPrefixes("screens", DefaultTabUrlFactory::forScreens);
    }

    private void assertFunctionGivesCorrectUriForPathAndPrefixes(String path, Function<DefaultTabUrlFactory, String> callback) {
        assertFunctionGivesCorrectUriForPathAndPrefix(path, callback, "base/", "/base/");
        assertFunctionGivesCorrectUriForPathAndPrefix(path, callback, "base", "/base/");
        assertFunctionGivesCorrectUriForPathAndPrefix(path, callback, "/base", "/base/");
        assertFunctionGivesCorrectUriForPathAndPrefix(path, callback, "/base/", "/base/");
        assertFunctionGivesCorrectUriForPathAndPrefix(path, callback, "", "/");
    }

    private void assertFunctionGivesCorrectUriForPathAndPrefix(String path, Function<DefaultTabUrlFactory, String> callback, final String base,
                                                                 final String urlStart) {
        // setup
        MockProject project = new MockProject(2929L, "KEY");
        ContextProviderUtils utils = mock(ContextProviderUtils.class);
        UrlEncoder encoder = mock(UrlEncoder.class);

        when(utils.getBaseUrl()).thenReturn(base);
        when(utils.getProject()).thenReturn(project);
        when(encoder.encode(project.getKey())).thenReturn(project.getKey());

        if (path != null) {
            when(encoder.encode(path)).thenReturn(path);
        }

        DefaultTabUrlFactory factory = new DefaultTabUrlFactory(utils, encoder, featureManager);

        String expectedString = urlStart + "plugins/servlet/project-config/" + project.getKey();
        if (path != null) {
            expectedString = expectedString + "/" + path;
        }

        // execute
        String actualInput = callback.apply(factory);

        // verify
        assertEquals(expectedString, actualInput);
    }
}
