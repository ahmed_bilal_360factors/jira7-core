package com.atlassian.jira.projectconfig.servlet;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.project.ProjectAction;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.event.mau.MauApplicationKey;
import com.atlassian.jira.event.mau.MauEventService;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.type.warning.InaccessibleProjectTypeDialogDataProvider;
import com.atlassian.jira.projectconfig.event.SummaryPageLoadedEvent;
import com.atlassian.jira.projectconfig.tab.ProjectConfigTab;
import com.atlassian.jira.projectconfig.tab.ProjectConfigTabManager;
import com.atlassian.jira.projectconfig.tab.SummaryTab;
import com.atlassian.jira.projectconfig.util.MockProjectConfigRequestCache;
import com.atlassian.jira.projectconfig.util.ProjectConfigRequestCache;
import com.atlassian.jira.projectconfig.util.VelocityContextFactory;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.UserProjectHistoryManager;
import com.atlassian.jira.user.UserPropertyManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.web.SessionKeys;
import com.atlassian.mock.propertyset.MockPropertySet;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.descriptors.WebItemModuleDescriptor;
import com.atlassian.plugin.web.model.WebLink;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;

import static org.apache.commons.lang.StringUtils.removeStart;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v4.4
 */
@RunWith(MockitoJUnitRunner.class)
public class TestPanelServlet {
    private static final String SAMPLE_PROJECT_KEY = "PROJKEY";
    private static final long SAMPLE_PROJECT_ID = 6373L;

    private MockSimpleAuthenticationContext authCtx;
    @Mock
    private ProjectConfigTabManager tabManager;
    @Mock
    private ProjectService projectService;
    @Mock
    private TemplateRenderer templateRenderer;
    @Mock
    private WebResourceManager webResourceManager;
    @Mock
    private UserProjectHistoryManager userProjectHistoryManager;
    @Mock
    private ApplicationProperties applicationProperties;
    @Mock
    private VelocityContextFactory contextFactory;
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private HttpServletResponse response;
    @Mock
    private PageBuilderService pageBuilderService;
    @Mock
    private InaccessibleProjectTypeDialogDataProvider inaccessibleProjectTypeDialogDataProvider;
    @Mock
    private MauEventService mauEventService;
    @Mock
    private UserPropertyManager userPropertyManager;
    @Mock
    private WebInterfaceManager webInterfaceManager;

    @Mock
    private HttpServletRequest request;

    private ProjectConfigRequestCache cache;

    private MockPropertySet propertySet;

    final String PROJECT_CONFIG_SETTINGS_GROUP = "atl.jira.proj.config/projectgroup1";

    @Before
    public void setUp() throws Exception {
        ApplicationUser user = new MockApplicationUser("bbain");
        authCtx = new MockSimpleAuthenticationContext(user, Locale.ENGLISH, new NoopI18nHelper());
        propertySet = new MockPropertySet();
        when(userPropertyManager.getPropertySet(user)).thenReturn(propertySet);
        cache = new MockProjectConfigRequestCache();
    }

    @Test
    public void testPathMatching() {
        assertMatchesPatten("/project-config/B", "B", null, null);
        assertMatchesPatten("//project-config/B/", "B", null, null);
        assertMatchesPatten("/project-config/B///", "B", null, null);
        assertMatchesPatten("project-config////B///", "B", null, null);

        assertMatchesPatten("/project-config/B/aaaa", "B", "aaaa", null);
        assertMatchesPatten("//project-config/B/aaa//", "B", "aaa", null);
        assertMatchesPatten("/project-config/B///aaaaa", "B", "aaaaa", null);
        assertMatchesPatten("project-config////B///aaaa///", "B", "aaaa", null);

        assertMatchesPatten("/project-config/B/aaaa/bbb", "B", "aaaa", "bbb");
        assertMatchesPatten("//project-config/B/aaa//bbbb", "B", "aaa", "bbbb");
        assertMatchesPatten("/project-config/B///aaaaa/bbbbb////", "B", "aaaaa", "bbbbb////");
        assertMatchesPatten("project-config////BJ///aaaa///bbbbbbb//////", "BJ", "aaaa", "bbbbbbb//////");

        assertNotMatchesPatten("qwerty");
        assertNotMatchesPatten("project-config/");
        assertNotMatchesPatten("project-config/////");
    }

    @Test
    public void willReturnNoProjectErrorWhenNoProjectSpecified() throws Exception {
        when(request.getPathInfo()).thenReturn("/project-config////");

        final AtomicBoolean called = new AtomicBoolean(false);
        PanelServlet servlet = new PanelServlet(
                authCtx, tabManager, projectService, templateRenderer, webResourceManager,
                pageBuilderService, applicationProperties, contextFactory, userProjectHistoryManager,
                cache, eventPublisher, inaccessibleProjectTypeDialogDataProvider, mauEventService, userPropertyManager, webInterfaceManager) {
            @Override
            void outputError(HttpServletResponse response, Collection<String> errorMessage, String title)
                    throws IOException {
                assertEquals(Collections.singleton(NoopI18nHelper.makeTranslation("admin.project.servlet.no.project")), errorMessage);
                assertEquals(NoopI18nHelper.makeTranslation("common.words.error"), title);
                called.set(true);
            }
        };

        servlet.doGet(request, response);
        assertTrue(called.get());
    }

    @Test
    public void willTagMauAsFamilyWhenNoProjectSpecified() throws Exception {
        when(request.getPathInfo()).thenReturn("/project-config////");

        final AtomicBoolean called = new AtomicBoolean(false);
        PanelServlet servlet = new PanelServlet(
                authCtx, tabManager, projectService, templateRenderer, webResourceManager,
                pageBuilderService, applicationProperties, contextFactory, userProjectHistoryManager,
                cache, eventPublisher, inaccessibleProjectTypeDialogDataProvider, mauEventService, userPropertyManager, webInterfaceManager) {
            @Override
            void outputError(HttpServletResponse response, Collection<String> errorMessage, String title)
                    throws IOException {
                assertEquals(Collections.singleton(NoopI18nHelper.makeTranslation("admin.project.servlet.no.project")), errorMessage);
                assertEquals(NoopI18nHelper.makeTranslation("common.words.error"), title);
                called.set(true);
            }
        };

        servlet.doGet(request, response);

        verify(mauEventService).setApplicationForThread(MauApplicationKey.family());
    }

    @Test
    public void willReturnErrorWhenProjectDoesNotExist() throws Exception {
        final String error = "my error";

        when(request.getPathInfo()).thenReturn("/project-config/abc");
        when(projectService.getProjectByKeyForAction(authCtx.getUser(), "abc", ProjectAction.EDIT_PROJECT_CONFIG))
                .thenReturn(new ProjectService.GetProjectResult(errors(error)));

        final AtomicBoolean called = new AtomicBoolean(false);
        PanelServlet servlet = new PanelServlet(authCtx, tabManager, projectService, templateRenderer, webResourceManager,
                pageBuilderService, applicationProperties, contextFactory, userProjectHistoryManager, cache, eventPublisher,
                inaccessibleProjectTypeDialogDataProvider, mauEventService, userPropertyManager, webInterfaceManager) {
            @Override
            void outputError(HttpServletResponse response, Collection<String> errorMessage, String title)
                    throws IOException {
                assertEquals(Collections.singleton(error), errorMessage);
                assertEquals(NoopI18nHelper.makeTranslation("common.words.error"), title);
                called.set(true);
            }
        };

        servlet.doGet(request, response);
        verify(mauEventService).setApplicationForThread(MauApplicationKey.family());
    }

    @Test
    public void willTagMauAsFamilyWhenProjectDoesNotExist() throws Exception {
        final String error = "my error";

        when(request.getPathInfo()).thenReturn("/project-config/abc");
        when(projectService.getProjectByKeyForAction(authCtx.getUser(), "abc", ProjectAction.EDIT_PROJECT_CONFIG))
                .thenReturn(new ProjectService.GetProjectResult(errors(error)));

        final AtomicBoolean called = new AtomicBoolean(false);
        PanelServlet servlet = new PanelServlet(authCtx, tabManager, projectService, templateRenderer, webResourceManager,
                pageBuilderService, applicationProperties, contextFactory, userProjectHistoryManager, cache, eventPublisher,
                inaccessibleProjectTypeDialogDataProvider, mauEventService, userPropertyManager, webInterfaceManager) {
            @Override
            void outputError(HttpServletResponse response, Collection<String> errorMessage, String title)
                    throws IOException {
                assertEquals(Collections.singleton(error), errorMessage);
                assertEquals(NoopI18nHelper.makeTranslation("common.words.error"), title);
                called.set(true);
            }
        };

        servlet.doGet(request, response);
        assertTrue(called.get());
    }

    @Test
    public void testDoGetProjectDoesNotExistAnonymous() throws Exception {
        final String error = "my error";

        authCtx.clearLoggedInUser();

        when(request.getPathInfo()).thenReturn("/project-config/abc");
        when(projectService.getProjectByKeyForAction(authCtx.getUser(), "abc", ProjectAction.EDIT_PROJECT_CONFIG))
                .thenReturn(new ProjectService.GetProjectResult(errors(error)));

        final AtomicBoolean called = new AtomicBoolean(false);
        PanelServlet servlet = new PanelServlet(authCtx, tabManager, projectService, templateRenderer, webResourceManager,
                pageBuilderService, applicationProperties, contextFactory, userProjectHistoryManager, cache,
                eventPublisher, inaccessibleProjectTypeDialogDataProvider, mauEventService, userPropertyManager, webInterfaceManager) {
            @Override
            void redirectToLogin(HttpServletRequest req, HttpServletResponse resp) throws IOException {
                called.set(true);
            }
        };

        servlet.doGet(request, response);
        assertTrue(called.get());
    }

    @Test
    public void wilLReturnErrorWhenProjectTabDoesNotExist() throws Exception {
        final Project project = new MockProject(6373L);

        when(request.getPathInfo()).thenReturn("/project-config/abc/dontexist");
        when(projectService.getProjectByKeyForAction(authCtx.getUser(), "abc", ProjectAction.EDIT_PROJECT_CONFIG))
                .thenReturn(new ProjectService.GetProjectResult(errors(), project));
        when(tabManager.getTabForId("dontexist")).thenReturn(null);

        final AtomicBoolean called = new AtomicBoolean(false);
        PanelServlet servlet = new PanelServlet(authCtx, tabManager, projectService, templateRenderer, webResourceManager,
                pageBuilderService, applicationProperties, contextFactory, userProjectHistoryManager, cache,
                eventPublisher, inaccessibleProjectTypeDialogDataProvider, mauEventService, userPropertyManager, webInterfaceManager) {
            @Override
            void outputError(HttpServletResponse response, Collection<String> errorMessage, String title)
                    throws IOException {
                assertEquals(Collections.singleton(NoopI18nHelper.makeTranslation("admin.project.servlet.no.tab", "dontexist")), errorMessage);
                assertEquals(NoopI18nHelper.makeTranslation("common.words.error"), title);
                called.set(true);
            }
        };

        servlet.doGet(request, response);
        assertTrue(called.get());
    }

    @Test
    public void willTagMauAsFamilyWhenProjectTabDoesNotExist() throws Exception {
        final Project project = new MockProject(6373L);

        when(request.getPathInfo()).thenReturn("/project-config/abc/dontexist");
        when(projectService.getProjectByKeyForAction(authCtx.getUser(), "abc", ProjectAction.EDIT_PROJECT_CONFIG))
                .thenReturn(new ProjectService.GetProjectResult(errors(), project));
        when(tabManager.getTabForId("dontexist")).thenReturn(null);

        final AtomicBoolean called = new AtomicBoolean(false);
        PanelServlet servlet = new PanelServlet(authCtx, tabManager, projectService, templateRenderer, webResourceManager,
                pageBuilderService, applicationProperties, contextFactory, userProjectHistoryManager, cache,
                eventPublisher, inaccessibleProjectTypeDialogDataProvider, mauEventService, userPropertyManager, webInterfaceManager) {
            @Override
            void outputError(HttpServletResponse response, Collection<String> errorMessage, String title)
                    throws IOException {
                assertEquals(Collections.singleton(NoopI18nHelper.makeTranslation("admin.project.servlet.no.tab", "dontexist")), errorMessage);
                assertEquals(NoopI18nHelper.makeTranslation("common.words.error"), title);
                called.set(true);
            }
        };

        servlet.doGet(request, response);
        verify(mauEventService).setApplicationForThread(MauApplicationKey.family());
    }

    @Test
    public void testDoGetHappyPath() throws Exception {
        final String tabId = "someId";
        final String extra = "extra";

        final Project project = new MockProject(SAMPLE_PROJECT_ID, SAMPLE_PROJECT_KEY);
        final ProjectConfigTab tabPanel = mock(ProjectConfigTab.class);

        prepareMocksForPanelServlet(project, tabPanel, tabId, extra, "/jira");

        final AtomicBoolean called = new AtomicBoolean(false);
        PanelServlet servlet = createPanelServlet(project, tabPanel, extra, called);

        servlet.doGet(request, response);
        assertTrue(called.get());

        //Check that the session is set correctly.
        assertEquals(propertySet.getString(SessionKeys.CURRENT_ADMIN_PROJECT_RETURN_URL),
                removeStart(request.getRequestURI(), request.getContextPath()));
    }

    @Test
    public void testDoGetSummary() throws Exception {
        final String tabId = SummaryTab.NAME;
        final String extra = null;

        final Project project = new MockProject(SAMPLE_PROJECT_ID, SAMPLE_PROJECT_KEY);
        final ProjectConfigTab tabPanel = mock(ProjectConfigTab.class);

        prepareMocksForPanelServlet(project, tabPanel, tabId, extra, "/jira");

        final AtomicBoolean called = new AtomicBoolean(false);
        final PanelServlet servlet = createPanelServlet(project, tabPanel, extra, called);

        servlet.doGet(request, response);
        assertTrue(called.get());

        verify(eventPublisher, times(1)).publish(new SummaryPageLoadedEvent(SAMPLE_PROJECT_ID, SAMPLE_PROJECT_KEY));
    }

    @Test
    public void testNoSetTabIdRedirectsToFirstTab() throws Exception {
        final String tabId = "";
        final String extra = null;
        final String redirectUrl = "someUrl";

        final Project project = new MockProject(SAMPLE_PROJECT_ID, SAMPLE_PROJECT_KEY);
        final ProjectConfigTab tabPanel = mock(ProjectConfigTab.class);

        prepareMocksForPanelServlet(project, tabPanel, tabId, extra, "/jira");

        final AtomicBoolean called = new AtomicBoolean(false);
        final PanelServlet servlet = createPanelServlet(project, tabPanel, extra, called);

        mockProjectSettingsMenuLinks(redirectUrl);

        servlet.doGet(request, response);
        assertFalse(called.get());

        verify(response, times(1)).sendRedirect(redirectUrl);
    }

    @Test
    public void testNoSetTabIdAndNoTabsInMenuThrowsRuntimeException() throws Exception {
        final String tabId = "";
        final String extra = null;

        final Project project = new MockProject(SAMPLE_PROJECT_ID, SAMPLE_PROJECT_KEY);
        final ProjectConfigTab tabPanel = mock(ProjectConfigTab.class);

        prepareMocksForPanelServlet(project, tabPanel, tabId, extra, "/jira");

        final AtomicBoolean called = new AtomicBoolean(false);
        final PanelServlet servlet = createPanelServlet(project, tabPanel, extra, called);

        when(webInterfaceManager.getDisplayableItems(eq(PROJECT_CONFIG_SETTINGS_GROUP), any(Map.class))).thenReturn(Lists.newArrayList());

        try {
            servlet.doGet(request, response);
            fail();
        } catch (RuntimeException e) {
            assertThat(e.getMessage().contains("atl.jira.proj.config/projectgroup1"), is(true));
        }
    }

    @Test
    public void willTagMauWithApplicationFromProject() throws Exception {
        final String tabId = "someId";
        final String extra = "extra";

        final Project project = new MockProject(SAMPLE_PROJECT_ID, SAMPLE_PROJECT_KEY);
        final ProjectConfigTab tabPanel = mock(ProjectConfigTab.class);

        prepareMocksForPanelServlet(project, tabPanel, tabId, extra, "/jira");

        final AtomicBoolean called = new AtomicBoolean(false);
        PanelServlet servlet = createPanelServlet(project, tabPanel, extra, called);

        servlet.doGet(request, response);

        verify(mauEventService).setApplicationForThreadBasedOnProject(project);
    }

    private void mockProjectSettingsMenuLinks(String expectedRedirectUrl) {
        final WebItemModuleDescriptor smallestWebItem = mock(WebItemModuleDescriptor.class);
        final WebLink smallestWebLink = mock(WebLink.class);
        final WebItemModuleDescriptor largestWebItem = mock(WebItemModuleDescriptor.class);

        when(smallestWebItem.getWeight()).thenReturn(10);
        when(largestWebItem.getWeight()).thenReturn(20);
        when(smallestWebItem.getLink()).thenReturn(smallestWebLink);
        when(smallestWebLink.getDisplayableUrl(any(HttpServletRequest.class), any(Map.class))).thenReturn(expectedRedirectUrl);

        List<WebItemModuleDescriptor> webItemModuleDescriptors = Lists.newArrayList(smallestWebItem, largestWebItem);

        when(webInterfaceManager.getDisplayableItems(eq(PROJECT_CONFIG_SETTINGS_GROUP), any(Map.class))).thenReturn(webItemModuleDescriptors);
    }


    private void prepareMocksForPanelServlet(Project project, ProjectConfigTab tabPanel, String tabId, String extra,
                                             String context) {
        final String projectKey = project.getKey();
        when(tabPanel.getId()).thenReturn(tabId);
        final ProjectService.GetProjectResult result = new ProjectService.GetProjectResult(errors(), project);

        String configPath = "/project-config/" + projectKey + "/" + tabId +
                (extra == null ? "" : ("/" + extra));
        String fullPath = context + "/some/path/" + configPath;

        when(request.getPathInfo()).thenReturn(configPath);
        when(request.getRequestURI()).thenReturn(fullPath);
        when(request.getContextPath()).thenReturn(context);
        when(projectService.getProjectByKeyForAction(authCtx.getUser(), projectKey, ProjectAction.EDIT_PROJECT_CONFIG))
                .thenReturn(result);
        when(tabManager.getTabForId(tabId)).thenReturn(tabPanel);
    }

    private PanelServlet createPanelServlet(final Project project, final ProjectConfigTab tabPanel, final String extra, final AtomicBoolean called) {
        return new PanelServlet(authCtx, tabManager, projectService, templateRenderer, webResourceManager, pageBuilderService,
                applicationProperties, contextFactory, userProjectHistoryManager,
                cache, eventPublisher, inaccessibleProjectTypeDialogDataProvider, mauEventService, userPropertyManager, webInterfaceManager) {
            @Override
            void outputTab(HttpServletRequest actualRequest, HttpServletResponse actualResponse, Project actualProject, ProjectConfigTab actualTab, String actualExtra)
                    throws IOException {
                assertSame(request, actualRequest);
                assertSame(response, actualResponse);
                assertSame(project, actualProject);
                assertSame(tabPanel, actualTab);
                assertEquals(extra, actualExtra); // if extra is null, expect actualExtra to be null, too

                called.set(true);
            }
        };
    }

    private void assertMatchesPatten(String url, String project, String pannel, String extra) {
        Matcher matcher = PanelServlet.PATTERN.matcher(url);
        assertTrue(matcher.matches());
        assertEquals(project, matcher.group(1));
        assertEquals(pannel, matcher.group(2));
        assertEquals(extra, matcher.group(3));
    }

    private void assertNotMatchesPatten(String url) {
        assertFalse(PanelServlet.PATTERN.matcher(url).matches());
    }

    private ErrorCollection errors(String... errors) {
        SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        errorCollection.addErrorMessages(Arrays.asList(errors));
        return errorCollection;
    }
}
