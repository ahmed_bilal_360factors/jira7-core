package com.atlassian.jira.projectconfig.rest.project;

import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.rest.api.util.ErrorCollection;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.task.MockTaskDescriptor;
import com.atlassian.jira.task.TaskManager;
import com.atlassian.jira.task.TaskProgressEvent;
import com.atlassian.jira.task.TaskProgressIndicator;
import com.atlassian.jira.task.TaskProgressListener;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.web.action.admin.workflow.WorkflowMigrationResult;
import com.atlassian.jira.web.action.admin.workflow.WorkflowMigrationSuccess;
import com.atlassian.jira.web.action.admin.workflow.WorkflowMigrationTerminated;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import javax.ws.rs.core.Response;

import static com.atlassian.jira.projectconfig.rest.RestAssertions.assertResponseCacheNever;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.stub;

/**
 * @since v5.2
 */
@RunWith(MockitoJUnitRunner.class)
public class TestMigrationStatusResource {
    @Mock
    private TaskManager taskManager;
    @Mock
    private PermissionManager permissionManager;
    private JiraAuthenticationContext authCtx;
    private MigrationStatusResource resource;

    @Before
    public void setup() {
        authCtx = new MockSimpleAuthenticationContext(new MockApplicationUser("username"));
        resource = new MigrationStatusResource(authCtx, permissionManager, taskManager);
    }

    @Test
    public void testGetMigrationStatusNoPermission() {
        long taskId = 12345L;
        stub(permissionManager.hasPermission(Permissions.ADMINISTER, authCtx.getLoggedInUser())).toReturn(false);
        Response response = resource.getMigrationStatus(taskId);
        assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
        assertResponseCacheNever(response);
    }

    @Test
    public void testGetMigrationStatusInvalidTask() {
        long taskId = 12345L;
        stub(permissionManager.hasPermission(Permissions.ADMINISTER, authCtx.getLoggedInUser())).toReturn(true);
        stub(taskManager.getTask(taskId)).toReturn(null);
        Response response = resource.getMigrationStatus(taskId);
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
        assertResponseCacheNever(response);
    }

    @Test
    public void testGetMigrationStatusFinished() {
        long taskId = 12345L;
        stub(permissionManager.hasPermission(Permissions.ADMINISTER, authCtx.getLoggedInUser())).toReturn(true);
        MockTaskDescriptor<Serializable> taskDescriptor = new MockTaskDescriptor<Serializable>();
        WorkflowMigrationResult migrationResult = new WorkflowMigrationSuccess(Collections.<Long, String>emptyMap());
        taskDescriptor.setFinishedTime(new Date());
        taskDescriptor.setResult(migrationResult);
        stub(taskManager.getTask(taskId)).toReturn(taskDescriptor);
        Response response = resource.getMigrationStatus(taskId);
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        assertEquals(new MigrationStatusResource.TaskStatus(migrationResult), response.getEntity());
        assertResponseCacheNever(response);
    }

    @Test
    public void testGetMigrationStatusNotStarted() {
        long taskId = 12345L;
        stub(permissionManager.hasPermission(Permissions.ADMINISTER, authCtx.getLoggedInUser())).toReturn(true);
        MockTaskDescriptor<Serializable> taskDescriptor = new MockTaskDescriptor<Serializable>();
        taskDescriptor.setTaskProgressIndicator(new TaskProgressIndicatorImpl(null));
        stub(taskManager.getTask(taskId)).toReturn(taskDescriptor);
        Response response = resource.getMigrationStatus(taskId);
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        assertEquals(new MigrationStatusResource.TaskStatus(0L), response.getEntity());
        assertResponseCacheNever(response);
    }

    @Test
    public void testGetMigrationStatusInProgress() {
        long taskId = 12345L;
        long progress = 50L;
        stub(permissionManager.hasPermission(Permissions.ADMINISTER, authCtx.getLoggedInUser())).toReturn(true);
        MockTaskDescriptor<Serializable> taskDescriptor = new MockTaskDescriptor<Serializable>();
        taskDescriptor.setTaskProgressIndicator(new TaskProgressIndicatorImpl(new TaskProgressEvent(123L, 0, progress, null, null)));
        stub(taskManager.getTask(taskId)).toReturn(taskDescriptor);
        Response response = resource.getMigrationStatus(taskId);
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        assertEquals(new MigrationStatusResource.TaskStatus(progress), response.getEntity());
        assertResponseCacheNever(response);
    }

    @Test
    public void testTaskStatusOnSuccess() {
        Map<Long, String> failedIssues = Collections.emptyMap();
        WorkflowMigrationResult result = new WorkflowMigrationSuccess(failedIssues);
        MigrationStatusResource.TaskStatus taskStatus = new MigrationStatusResource.TaskStatus(result);
        assertTrue(taskStatus.isFinished());
        assertTrue(taskStatus.isSuccessful());
        assertEquals(ErrorCollection.of(result.getErrorCollection()), taskStatus.getErrorCollection());
        assertEquals(result.getNumberOfFailedIssues(), taskStatus.getNumberOfFailedIssues().longValue());
        assertEquals(ImmutableList.copyOf(result.getFailedIssues().values()), taskStatus.getFailedIssues());
    }

    @Test
    public void testTaskStatusOnError() {
        Map<Long, String> failedIssues = ImmutableMap.of(1L, "string");
        WorkflowMigrationResult result = new WorkflowMigrationTerminated(failedIssues);
        MigrationStatusResource.TaskStatus taskStatus = new MigrationStatusResource.TaskStatus(result);
        assertTrue(taskStatus.isFinished());
        assertFalse(taskStatus.isSuccessful());
        assertEquals(ErrorCollection.of(result.getErrorCollection()), taskStatus.getErrorCollection());
        assertEquals(result.getNumberOfFailedIssues(), taskStatus.getNumberOfFailedIssues().longValue());
        assertEquals(ImmutableList.copyOf(result.getFailedIssues().values()), taskStatus.getFailedIssues());
        assertNull(taskStatus.getProgress());
    }

    @Test
    public void testTaskStatusInProgress() {
        long progress = 50L;
        MigrationStatusResource.TaskStatus taskStatus = new MigrationStatusResource.TaskStatus(progress);
        assertFalse(taskStatus.isFinished());
        assertEquals(progress, taskStatus.getProgress().longValue());
        assertNull(taskStatus.isSuccessful());
        assertNull(taskStatus.getErrorCollection());
        assertNull(taskStatus.getNumberOfFailedIssues());
        assertNull(taskStatus.getFailedIssues());
    }

    @Test
    public void testDeleteMigrationStatusNoPermission() {
        long taskId = 12345L;
        stub(permissionManager.hasPermission(Permissions.ADMINISTER, authCtx.getLoggedInUser())).toReturn(false);
        Response response = resource.deleteMigrationStatus(taskId);
        assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
        assertResponseCacheNever(response);
    }

    @Test
    public void testDeleteMigrationStatusInvalidTask() {
        long taskId = 12345L;
        stub(permissionManager.hasPermission(Permissions.ADMINISTER, authCtx.getLoggedInUser())).toReturn(true);
        stub(taskManager.getTask(taskId)).toReturn(null);
        Response response = resource.deleteMigrationStatus(taskId);
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
        assertResponseCacheNever(response);
    }

    @Test
    public void testDeleteMigrationStatusNotOwnerOfTask() {
        long taskId = 12345L;
        stub(permissionManager.hasPermission(Permissions.ADMINISTER, authCtx.getLoggedInUser())).toReturn(true);
        MockTaskDescriptor<Serializable> taskDescriptor = new MockTaskDescriptor<Serializable>();
        MockApplicationUser user = new MockApplicationUser("differentusername");
        taskDescriptor.setUserName(user.getName());
        stub(taskManager.getTask(taskId)).toReturn(taskDescriptor);
        Response response = resource.deleteMigrationStatus(taskId);
        assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
        assertResponseCacheNever(response);
    }

    @Test
    public void testDeleteMigrationStatusSuccess() {
        long taskId = 12345L;
        stub(permissionManager.hasPermission(Permissions.ADMINISTER, authCtx.getLoggedInUser())).toReturn(true);
        MockTaskDescriptor<Serializable> taskDescriptor = new MockTaskDescriptor<Serializable>();
        MockApplicationUser user = new MockApplicationUser("username");
        taskDescriptor.setUserName(user.getName());
        stub(taskManager.getTask(taskId)).toReturn(taskDescriptor);
        Response response = resource.deleteMigrationStatus(taskId);
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        assertResponseCacheNever(response);
    }

    @Test
    public void testDeleteMigrationStatusOwnerIsAnonymousUserIsLoggedIn() {
        long taskId = 12345L;
        stub(permissionManager.hasPermission(Permissions.ADMINISTER, authCtx.getLoggedInUser())).toReturn(true);
        MockTaskDescriptor<Serializable> taskDescriptor = new MockTaskDescriptor<Serializable>();
        stub(taskManager.getTask(taskId)).toReturn(taskDescriptor);
        Response response = resource.deleteMigrationStatus(taskId);
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        assertResponseCacheNever(response);
    }

    @Test
    public void testDeleteMigrationStatusOwnerIsAnonymousUserIsAnonymous() {
        long taskId = 12345L;
        JiraAuthenticationContext authCtx = new MockSimpleAuthenticationContext(null);
        MigrationStatusResource workflowResource = new MigrationStatusResource(authCtx, permissionManager, taskManager);
        stub(permissionManager.hasPermission(Permissions.ADMINISTER, authCtx.getLoggedInUser())).toReturn(true);
        MockTaskDescriptor<Serializable> taskDescriptor = new MockTaskDescriptor<Serializable>();
        stub(taskManager.getTask(taskId)).toReturn(taskDescriptor);
        Response response = workflowResource.deleteMigrationStatus(taskId);
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        assertResponseCacheNever(response);
    }

    @Test
    public void testDeleteMigrationStatusRemoveFailed() {
        long taskId = 12345L;
        stub(permissionManager.hasPermission(Permissions.ADMINISTER, authCtx.getLoggedInUser())).toReturn(true);
        stub(taskManager.getTask(taskId)).toReturn(null);
        Response response = resource.deleteMigrationStatus(taskId);
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
        assertResponseCacheNever(response);
    }

    private static class TaskProgressIndicatorImpl implements TaskProgressIndicator {
        private TaskProgressEvent lastProgressEvent;

        public TaskProgressIndicatorImpl(TaskProgressEvent lastProgressEvent) {
            this.lastProgressEvent = lastProgressEvent;
        }

        @Override
        public void addListener(TaskProgressListener listener) {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public void removeListener(TaskProgressListener listener) {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public TaskProgressEvent getLastProgressEvent() {
            return lastProgressEvent;
        }
    }
}
