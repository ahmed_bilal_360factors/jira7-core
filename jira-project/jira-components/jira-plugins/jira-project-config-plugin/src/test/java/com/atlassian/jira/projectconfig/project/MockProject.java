package com.atlassian.jira.projectconfig.project;

import com.atlassian.jira.project.ProjectCategory;

/**
 * This was created because JIRA 6.3 MockProject does not implement getProjectCategoryObject().
 * We can probably delete this once we compile against a newer version of JIRA.
 */
public class MockProject extends com.atlassian.jira.project.MockProject {
    public MockProject(final Long id, final String key, final String name) {
        super(id, key, name);
    }

    @Override
    public ProjectCategory getProjectCategoryObject() {
        // JIRA 6.3 throws UnsupportedOperationException here...
        return null;
    }
}
