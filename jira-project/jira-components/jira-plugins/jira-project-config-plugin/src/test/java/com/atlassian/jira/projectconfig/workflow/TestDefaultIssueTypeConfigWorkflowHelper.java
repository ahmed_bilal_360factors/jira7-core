package com.atlassian.jira.projectconfig.workflow;

import com.atlassian.jira.bc.workflow.DefaultWorkflowService;
import com.atlassian.jira.bc.workflow.WorkflowService;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.issuetype.MockIssueType;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.beans.ProjectContext;
import com.atlassian.jira.projectconfig.issuetypes.workflow.DefaultIssueTypeConfigWorkflowHelper;
import com.atlassian.jira.projectconfig.issuetypes.workflow.IssueTypeConfigWorkflowHelper;
import com.atlassian.jira.projectconfig.issuetypes.workflow.IssueTypeConfigWorkflowHelper.WorkflowResult;
import com.atlassian.jira.projectconfig.order.MockOrderFactory;
import com.atlassian.jira.projectconfig.shared.SharedEntitiesHelper;
import com.atlassian.jira.projectconfig.shared.SharedIssueTypeWorkflowData;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.MockJiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.WorkflowSchemeManager;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static com.atlassian.jira.projectconfig.issuetypes.workflow.IssueTypeConfigWorkflowHelper.WorkflowState;
import static com.atlassian.jira.projectconfig.issuetypes.workflow.IssueTypeConfigWorkflowHelper.WorkflowState.EDITABLE;
import static com.atlassian.jira.projectconfig.issuetypes.workflow.IssueTypeConfigWorkflowHelper.WorkflowState.EDITABLE_DELEGATED;
import static com.atlassian.jira.projectconfig.issuetypes.workflow.IssueTypeConfigWorkflowHelper.WorkflowState.MIGRATE;
import static com.atlassian.jira.projectconfig.issuetypes.workflow.IssueTypeConfigWorkflowHelper.WorkflowState.NO_PERMISSION;
import static com.atlassian.jira.projectconfig.issuetypes.workflow.IssueTypeConfigWorkflowHelper.WorkflowState.READ_ONLY;
import static com.atlassian.jira.projectconfig.issuetypes.workflow.IssueTypeConfigWorkflowHelper.WorkflowState.READ_ONLY_DELEGATED_SHARED;
import static com.atlassian.jira.projectconfig.issuetypes.workflow.IssueTypeConfigWorkflowHelper.WorkflowState.READ_ONLY_DELEGATED_SYSTEM;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.Collections.emptyList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

/**
 * @since v6.2
 */
@RunWith(MockitoJUnitRunner.class)
public class TestDefaultIssueTypeConfigWorkflowHelper {
    private final IssueType typeProject = new MockIssueType("101010", "Issue Type");
    private final Project project = new MockProject(1910, "ABD").setIssueTypes(typeProject);
    private final MockApplicationUser admin = new MockApplicationUser("admin");
    private final MockApplicationUser userWithEditWorkflowPermission = new MockApplicationUser("editWorkflow");
    private final MockSimpleAuthenticationContext context
            = new MockSimpleAuthenticationContext(admin, Locale.ENGLISH, new NoopI18nHelper());

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private WorkflowSchemeManager workflowSchemeManager;

    @Mock
    private WorkflowManager workflowManager;

    @Mock
    private WorkflowService workflowService;

    @Mock
    private JiraWorkflow jiraWorkflow;

    @Mock
    private SharedEntitiesHelper sharedHelper;

    @Mock
    private FeatureManager featureManager;

    private IssueTypeConfigWorkflowHelper state;

    @Before
    public void setup() {
        when(jiraWorkflow.getName()).thenReturn("name");

        when(permissionManager.hasPermission(Permissions.ADMINISTER, context.getUser()))
                .thenReturn(true);

        when(permissionManager.hasPermission(ProjectPermissions.ADMINISTER_PROJECTS, project, userWithEditWorkflowPermission)).thenReturn(true);
        when(featureManager.isEnabled(DefaultWorkflowService.PROJECT_LEVEL_ADMIN_EDIT_WORKFLOW_FEATURE)).thenReturn(true);

        when(workflowManager.getWorkflow(project.getId(), typeProject.getId()))
                .thenReturn(jiraWorkflow);

        when(sharedHelper.getSharedData(project, typeProject, jiraWorkflow.getName()))
                .thenReturn(new SharedIssueTypeWorkflowData(emptyList()));

        state = new DefaultIssueTypeConfigWorkflowHelper(context, permissionManager, workflowSchemeManager,
                workflowManager, workflowService, sharedHelper, featureManager);
    }

    @Test
    public void nonAdminHasNoPermissionToEditWorkflow() {
        final ResultMatcher matcher = new ResultMatcher()
                .workflow(jiraWorkflow)
                .workflowState(NO_PERMISSION);

        context.setLoggedInUser(new MockApplicationUser("jack"));
        assertThat(state.getWorkflowData(new ProjectContext(project, typeProject)), matcher);
    }

    @Test
    public void nonAdminWithEditWorkflowPermissionHasPermissionToEditWorkflow() {
        final ResultMatcher matcher = new ResultMatcher()
                .workflow(jiraWorkflow)
                .workflowState(EDITABLE_DELEGATED);
        when(workflowService.hasEditWorkflowPermission(any(ApplicationUser.class), eq(jiraWorkflow))).thenReturn(true);
        context.setLoggedInUser(userWithEditWorkflowPermission);
        assertThat(state.getWorkflowData(new ProjectContext(project, typeProject)), matcher);
    }

    @Test
    public void nonAdminWithEditWorkflowPermissionHasNoPermissionToEditWorkflowWhenFeatureIsDisabled() {
        final ResultMatcher matcher = new ResultMatcher()
                .workflow(jiraWorkflow)
                .workflowState(NO_PERMISSION);
        when(featureManager.isEnabled(DefaultWorkflowService.PROJECT_LEVEL_ADMIN_EDIT_WORKFLOW_FEATURE)).thenReturn(false);
        context.setLoggedInUser(userWithEditWorkflowPermission);
        assertThat(state.getWorkflowData(new ProjectContext(project, typeProject)), matcher);
    }

    @Test
    public void nonAdminWithoutEditWorkflowPermissionHasNoPermissionToEditWorkflow() {
        final ResultMatcher matcher = new ResultMatcher()
                .workflow(jiraWorkflow)
                .workflowState(READ_ONLY_DELEGATED_SHARED);
        when(workflowService.hasEditWorkflowPermission(any(ApplicationUser.class), eq(jiraWorkflow))).thenReturn(false);
        context.setLoggedInUser(userWithEditWorkflowPermission);
        assertThat(state.getWorkflowData(new ProjectContext(project, typeProject)), matcher);
    }

    @Test
    public void nonAdminWithEditWorkflowPermissionHasNoPermissionToEditSystemWorkflow() {
        final ResultMatcher matcher = new ResultMatcher()
                .workflow(jiraWorkflow)
                .workflowState(READ_ONLY_DELEGATED_SYSTEM);
        when(jiraWorkflow.isSystemWorkflow()).thenReturn(true);
        when(workflowService.hasEditWorkflowPermission(any(ApplicationUser.class), eq(jiraWorkflow))).thenReturn(true);
        context.setLoggedInUser(userWithEditWorkflowPermission);
        assertThat(state.getWorkflowData(new ProjectContext(project, typeProject)), matcher);

        when(workflowService.hasEditWorkflowPermission(any(ApplicationUser.class), eq(jiraWorkflow))).thenReturn(false);
        context.setLoggedInUser(userWithEditWorkflowPermission);
        assertThat(state.getWorkflowData(new ProjectContext(project, typeProject)), matcher);
    }

    @Test
    public void adminNeedsToMirgateProjectBeforeEditing() {
        when(workflowSchemeManager.isUsingDefaultScheme(project))
                .thenReturn(true);

        final ResultMatcher matcher = new ResultMatcher()
                .workflow(jiraWorkflow)
                .workflowState(MIGRATE);

        assertThat(state.getWorkflowData(new ProjectContext(project, typeProject)), matcher);
    }

    @Test
    public void adminCantEditAReadOnlyWorkflow() {
        when(jiraWorkflow.isSystemWorkflow())
                .thenReturn(true);

        final ResultMatcher matcher = new ResultMatcher()
                .workflow(jiraWorkflow)
                .workflowState(READ_ONLY);

        assertThat(state.getWorkflowData(new ProjectContext(project, typeProject)), matcher);
    }

    @Test
    public void adminCanEditWorkflow() {
        final ResultMatcher matcher = new ResultMatcher()
                .workflow(jiraWorkflow)
                .workflowState(EDITABLE);

        assertThat(state.getWorkflowData(new ProjectContext(project, typeProject)), matcher);
    }

    @Test
    public void sharedProjectsAreReported() {
        Project otherProject1 = new MockProject(1010147847L, "DEF");
        Project otherProject2 = new MockProject(1010147887L, "HGJ");

        when(sharedHelper.getSharedData(project, typeProject, jiraWorkflow.getName()))
                .thenReturn(new SharedIssueTypeWorkflowData(newArrayList(otherProject1, otherProject2)));

        final ResultMatcher matcher = new ResultMatcher()
                .workflow(jiraWorkflow)
                .workflowState(EDITABLE)
                .sharedProjects(otherProject1, otherProject2);

        assertThat(state.getWorkflowData(new ProjectContext(project, typeProject)), matcher);
    }

    @Test
    public void sharedIssueTypesAreReported() {
        //Project has two three issue types with a WFS that looks like:
        // - one -> name1
        // - two -> name2
        // - three -> name1
        final MockIssueType one = new MockIssueType(10191817L, "One");
        final MockIssueType two = new MockIssueType(10191818L, "Two");
        final MockIssueType three = new MockIssueType(10191819L, "Three");

        final JiraWorkflow workflow1 = new MockJiraWorkflow("name1");
        final JiraWorkflow workflow2 = new MockJiraWorkflow("name2");

        final MockProject project = new MockProject(10101088262L, "ZZZ")
                .setIssueTypes(one, two, three);

        when(workflowManager.getWorkflow(project.getId(), one.getId())).thenReturn(workflow1);
        when(workflowManager.getWorkflow(project.getId(), two.getId())).thenReturn(workflow2);
        when(workflowManager.getWorkflow(project.getId(), three.getId())).thenReturn(workflow1);
        when(sharedHelper.getSharedData(project, one, workflow1.getName()))
                .thenReturn(new SharedIssueTypeWorkflowData(emptyList(), newArrayList(three)));

        final ResultMatcher matcher = new ResultMatcher()
                .workflow(workflow1)
                .workflowState(EDITABLE)
                .sharedIssueTypes(three);

        WorkflowResult workflowData = state.getWorkflowData(new ProjectContext(project, one));
        assertThat(workflowData, matcher);
    }

    @Test
    public void sharedIssueTypesAreReportedInOrder() {
        //Project has two three issue types with a WFS that looks like:
        // - * -> name1
        final MockIssueType one = new MockIssueType(10191817L, "ZLast");
        final MockIssueType two = new MockIssueType(10191818L, "Middle");
        final MockIssueType three = new MockIssueType(10191819L, "AFirst");
        final MockIssueType four = new MockIssueType(10191819L, "ZZLastLast");

        final JiraWorkflow workflow1 = new MockJiraWorkflow("name1");

        final MockProject project = new MockProject(10101088262L, "ZZZ")
                .setIssueTypes(one, two, three, four);

        when(workflowManager.getWorkflow(eq(project.getId()), Matchers.anyString()))
                .thenReturn(workflow1);
        when(sharedHelper.getSharedData(project, one, workflow1.getName()))
                .thenReturn(new SharedIssueTypeWorkflowData(emptyList(), newArrayList(three, two, four)));


        final ResultMatcher matcher = new ResultMatcher()
                .workflow(workflow1)
                .workflowState(EDITABLE)
                .sharedIssueTypes(three, two, four);

        assertThat(state.getWorkflowData(new ProjectContext(project, one)), matcher);
    }

    public static class ResultMatcher extends TypeSafeMatcher<WorkflowResult> {
        private JiraWorkflow workflow;
        private WorkflowState state;
        private List<Project> sharedProjects = emptyList();
        private List<IssueType> sharedIssueTypes = emptyList();

        @Override
        protected boolean matchesSafely(final WorkflowResult item) {
            return workflow.equals(item.getWorkflow())
                    && item.getWorkflowState() == state
                    && sharedProjects.equals(item.getSharedBy().getProjects())
                    && sharedIssueTypes.equals(item.getSharedBy().getIssueTypes());
        }

        @Override
        protected void describeMismatchSafely(final WorkflowResult item, final Description mismatchDescription) {
            mismatchDescription.appendText(asString(item));
        }

        private String asString(final WorkflowResult item) {
            return asString(item.getWorkflow(), item.getWorkflowState(), item.getSharedBy().getProjects(),
                    item.getSharedBy().getIssueTypes());
        }

        @Override
        public void describeTo(final Description description) {
            description.appendText(asString(workflow, state, sharedProjects, sharedIssueTypes));
        }

        private String asString(final JiraWorkflow workflow, final WorkflowState state,
                                final List<Project> sharedProjects, final List<IssueType> sharedIssueTypes) {
            return String.format("[wf: %s, wfstate: %s, projects: %s, issueTypes: %s]",
                    workflow, state, sharedProjects, sharedIssueTypes);
        }

        public ResultMatcher workflow(JiraWorkflow workflow) {
            this.workflow = workflow;
            return this;
        }

        public ResultMatcher workflowState(WorkflowState state) {
            this.state = state;
            return this;
        }

        public ResultMatcher sharedProjects(Project... projects) {
            this.sharedProjects = Arrays.asList(projects);
            return this;
        }

        public ResultMatcher sharedIssueTypes(IssueType... types) {
            this.sharedIssueTypes = Arrays.asList(types);
            return this;
        }
    }
}
