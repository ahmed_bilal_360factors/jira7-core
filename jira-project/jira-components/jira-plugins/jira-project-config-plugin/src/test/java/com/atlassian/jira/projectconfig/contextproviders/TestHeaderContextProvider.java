package com.atlassian.jira.projectconfig.contextproviders;

import com.atlassian.jira.admin.ProjectAdminSidebarFeature;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.mock.security.MockAuthenticationContext;
import com.atlassian.jira.plugin.webfragment.descriptors.JiraWebItemModuleDescriptor;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.plugin.webfragment.model.JiraWebLabel;
import com.atlassian.jira.plugin.webfragment.model.JiraWebLink;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.type.ProjectTypeIconRenderer;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.project.type.warning.InaccessibleProjectTypeDialogDataProvider;
import com.atlassian.jira.projectconfig.beans.SimpleProject;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.MockUserManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.descriptors.WebItemModuleDescriptor;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.4
 */
@RunWith(MockitoJUnitRunner.class)
public class TestHeaderContextProvider {
    private static final String DEFAULT_KEY = "default";
    private static final String LINK_ID = "linkId";
    private static final String RENDERED_URL = "renderedUrl";
    private static final String DISPLAYABLE_LABEL = "displayableLabel";
    private static final String LINK_STYLE = "testStyle";
    public static final String SERVICE_DESK = "service_desk";
    public static final String SERVICE_DESK_ICON = "icon";
    public static final String SERVICE_DESK_NAME = "Service Desk";

    @Mock
    private WebInterfaceManager webInterfaceManager;
    @Mock
    private ProjectTypeIconRenderer projectTypeIconRenderer;
    @Mock
    private ContextProviderHelper contextProviderHelper;
    @Mock
    private InaccessibleProjectTypeDialogDataProvider inaccessibleProjectTypeDialogDataProvider;

    @Mock
    private ProjectAdminSidebarFeature projectAdminSidebarFeature;

    private Project project;
    private Map<String, Object> testContext;
    private Map<String, Object> defaultContext;
    private Map<String, Object> webItemContext;
    private JiraAuthenticationContext authenticationContext;
    private ApplicationUser user;
    private JiraHelper helper;
    private MockI18nHelper i18nHelper;

    @Before
    public void setUp() {
        new MockComponentWorker().addMock(UserManager.class, new MockUserManager()).init();

        user = new MockApplicationUser("admin", "administrator", "admin@admin.com");
        authenticationContext = new MockAuthenticationContext(user);
        i18nHelper = new MockI18nHelper();

        project = projectFixture();
        testContext = MapBuilder.newBuilder(
                "a", "aValue",
                "b", "bValue",
                "c", new Object(),
                "project", project
        ).toHashMap();

        helper = new JiraHelper(null, project);

        webItemContext = MapBuilder.build(
                "project", project,
                "user", user,
                "request", null,
                "helper", helper
        );

        defaultContext = MapBuilder.<String, Object>newBuilder()
                .addAll(testContext)
                .add(ContextProviderUtils.CONTEXT_PROJECT_KEY, project)
                .add(ContextProviderUtils.CONTEXT_I18N_KEY, authenticationContext.getI18nHelper())
                .toMap();

        setUpContextProviderHelper();
    }

    private void setUpContextProviderHelper() {
        when(contextProviderHelper.getAuthenticationContext()).thenReturn(authenticationContext);
        when(contextProviderHelper.hasAdminPermission()).thenReturn(true);
        when(contextProviderHelper.getI18nHelper()).thenReturn(i18nHelper);
        when(projectAdminSidebarFeature.shouldDisplay()).thenReturn(true);
    }

    @Test
    public void testPluggableProjectOperations() throws Exception {
        JiraWebLink webLink = webLinkWith(LINK_ID, RENDERED_URL);
        JiraWebLabel webLabel = webLabelWith(DISPLAYABLE_LABEL);

        JiraWebItemModuleDescriptor webItemModuleDescriptor = webItemModuleDescriptorWith(
                "any-key",
                webLink,
                webLabel,
                LINK_STYLE
        );

        when(webInterfaceManager.getDisplayableItems("system.view.project.operations", webItemContext))
                .thenReturn(Lists.<WebItemModuleDescriptor>newArrayList(webItemModuleDescriptor));
        testContext.put(HeaderContextProvider.CURRENT_TAB, "view_project_summary");

        HeaderContextProvider headerContextProvider = getHeaderContextProviderUnderTest();
        Map<String, Object> headerContextMap = headerContextProvider.getContextMap(testContext);

        SimpleProject expectedProject = expectedSimpleProjectFixture();
        HeaderContextProvider.SimpleViewableProjectOperation expectedViewableProjectOperation =
                new HeaderContextProvider.SimpleViewableProjectOperation(LINK_ID, urlEncode(RENDERED_URL), DISPLAYABLE_LABEL, LINK_STYLE);

        Map<String, Object> expectedContext = MapBuilder.<String, Object>newBuilder()
                .addAll(testContext)
                .addAll(defaultContext)
                .add(HeaderContextProvider.CONTEXT_SIMPLE_PROJECT_KEY, expectedProject)
                .add(HeaderContextProvider.CONTEXT_VIEW_PROJECT_OPERATIONS_KEY, Lists.newArrayList(expectedViewableProjectOperation))
                .add(HeaderContextProvider.SHOW_ACTIONS_MENU, true)
                .add(DEFAULT_KEY, true)
                .add(HeaderContextProvider.CONTEXT_SIDEBAR_PRESENT, true)
                .add(HeaderContextProvider.CONTEXT_PROJECT_LEAD_EXISTS_KEY, false)
                .add(HeaderContextProvider.CONTEXT_IS_ADMIN_KEY, true)
                .add(HeaderContextProvider.CONTEXT_I18N_KEY, i18nHelper)
                .add(HeaderContextProvider.PROJECT_TYPE_KEY, SERVICE_DESK)
                .add(HeaderContextProvider.PROJECT_TYPE_NAME, SERVICE_DESK_NAME)
                .add(HeaderContextProvider.PROJECT_TYPE_ICON, "icon")
                .add(HeaderContextProvider.SHOULD_DISPLAY_PROJECT_TYPE_WARNING, false)

                .toMap();

        assertEquals(expectedContext, headerContextMap);

        testContext.remove(HeaderContextProvider.CURRENT_TAB);
        testContext.put(HeaderContextProvider.CURRENT_TAB, "blah");

        headerContextMap = headerContextProvider.getContextMap(testContext);

        assertEquals("Expected show action menu flag to be false if not on summary panel",
                false, headerContextMap.get(HeaderContextProvider.SHOW_ACTIONS_MENU));
    }

    @Test
    public void addsProjectTypesInfoWhenProjectTypesAreEnabled() {
        mockWebItemModuleDescriptor();
        mockProjectType(SERVICE_DESK, SERVICE_DESK_ICON);
        warningShouldBeDisplayedForProjectType(project);

        HeaderContextProvider headerContextProvider = getHeaderContextProviderUnderTest();
        Map<String, Object> headerContextMap = headerContextProvider.getContextMap(testContext);

        assertThat(headerContextMap.get(HeaderContextProvider.PROJECT_TYPE_KEY), is(SERVICE_DESK));
        assertThat(headerContextMap.get(HeaderContextProvider.PROJECT_TYPE_NAME), is(SERVICE_DESK_NAME));
        assertThat(headerContextMap.get(HeaderContextProvider.PROJECT_TYPE_ICON), is(SERVICE_DESK_ICON));
        assertThat(headerContextMap.get(HeaderContextProvider.SHOULD_DISPLAY_PROJECT_TYPE_WARNING), is(true));
    }

    private void warningShouldBeDisplayedForProjectType(Project project) {
        when(inaccessibleProjectTypeDialogDataProvider.shouldDisplayInaccessibleWarning(user, project)).thenReturn(true);
    }

    private void mockProjectType(String projectTypeKey, String projectTypeIcon) {
        ProjectTypeKey key = new ProjectTypeKey(projectTypeKey);
        when(project.getProjectTypeKey()).thenReturn(key);
        when(projectTypeIconRenderer.getIconToRender(key)).thenReturn(Optional.ofNullable(projectTypeIcon));
    }

    private void mockWebItemModuleDescriptor() {
        JiraWebLink webLink = webLinkWith(LINK_ID, RENDERED_URL);
        JiraWebLabel webLabel = webLabelWith(DISPLAYABLE_LABEL);

        JiraWebItemModuleDescriptor webItemModuleDescriptor1 = webItemModuleDescriptorWith(
                "jira.webfragments.view.project.operations:edit_project",
                webLink,
                webLabel,
                null
        );

        when(webInterfaceManager.getDisplayableItems("system.view.project.operations", webItemContext))
                .thenReturn(Lists.<WebItemModuleDescriptor>newArrayList(webItemModuleDescriptor1));
    }

    private HeaderContextProvider getHeaderContextProviderUnderTest() {
        return new HeaderContextProvider(
                webInterfaceManager,
                contextProviderHelper,
                authenticationContext,
                projectTypeIconRenderer,
                inaccessibleProjectTypeDialogDataProvider, projectAdminSidebarFeature) {
            @Override
            JiraHelper getJiraHelper(Project project) {
                return helper;
            }

            @Override
            String getUrlEncodedRenderedUrl(String renderedUrl) {
                return urlEncode(renderedUrl);
            }

            @Override
            Map<String, Object> createContext(Map<String, Object> params) {
                return MapBuilder.newBuilder(params).add(DEFAULT_KEY, true).toMap();
            }
        };
    }

    private static String urlEncode(String renderedUrl) {
        return "urlEncoded:" + renderedUrl;
    }

    private Project projectFixture() {
        Project project = mock(Project.class);
        when(project.getId()).thenReturn(1L);
        when(project.getKey()).thenReturn("PROJECT_KEY");
        when(project.getName()).thenReturn("PROJECT_NAME");
        when(project.getUrl()).thenReturn("PROJECT_URL");
        when(project.getDescription()).thenReturn("PROJECT_DESCRIPTION");
        when(project.getProjectLead()).thenReturn(user);
        ProjectTypeKey key = new ProjectTypeKey(SERVICE_DESK);
        when(project.getProjectTypeKey()).thenReturn(key);
        when(projectTypeIconRenderer.getIconToRender(key)).thenReturn(Optional.ofNullable(SERVICE_DESK_ICON));
        return project;
    }

    private SimpleProject expectedSimpleProjectFixture() {
        return new SimpleProject(projectFixture());
    }

    private JiraWebLink webLinkWith(String linkId, String url) {
        JiraWebLink webLink = mock(JiraWebLink.class);
        when(webLink.getId()).thenReturn(linkId);
        when(webLink.getRenderedUrl(user, helper)).thenReturn(url);
        return webLink;
    }

    private JiraWebLabel webLabelWith(String label) {
        JiraWebLabel webLabel = mock(JiraWebLabel.class);
        when(webLabel.getDisplayableLabel(user, helper)).thenReturn(label);
        return webLabel;
    }

    private JiraWebItemModuleDescriptor webItemModuleDescriptorWith(String completeKey, JiraWebLink webLink, JiraWebLabel webLabel,
                                                                    String styleClass) {
        JiraWebItemModuleDescriptor webItemModuleDescriptor = mock(JiraWebItemModuleDescriptor.class);
        when(webItemModuleDescriptor.getCompleteKey()).thenReturn(completeKey);
        when(webItemModuleDescriptor.getLink()).thenReturn(webLink);
        when(webItemModuleDescriptor.getLabel()).thenReturn(webLabel);
        when(webItemModuleDescriptor.getStyleClass()).thenReturn(styleClass);
        return webItemModuleDescriptor;
    }
}
