package com.atlassian.jira.projectconfig.rest.interceptor;

import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.projectconfig.rest.AdminRequired;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.plugins.rest.common.interceptor.MethodInvocation;
import com.sun.jersey.api.core.HttpContext;
import com.sun.jersey.api.core.HttpResponseContext;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.lang.reflect.InvocationTargetException;
import java.util.Locale;
import javax.ws.rs.core.Response;

import static com.atlassian.jira.projectconfig.rest.RestAssertions.assertResponseCacheNever;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v6.1
 */
@RunWith(MockitoJUnitRunner.class)
public class TestSecurityInterceptor {
    private final MockApplicationUser user = new MockApplicationUser("admin");
    private final MockSimpleAuthenticationContext jac = new MockSimpleAuthenticationContext(user, Locale.ENGLISH, new NoopI18nHelper());

    @Mock
    private PermissionManager permissionManager;

    @Test
    public void interceptNoAnnotation() throws InvocationTargetException, IllegalAccessException {
        final SecurityInterceptor interceptor = new SecurityInterceptor(jac, permissionManager);
        final MethodInvocation invocation = createInvocation(new Object());
        interceptor.intercept(invocation);

        verify(invocation).invoke();
    }

    @Test
    public void interceptWithAnnotationNoPermission() throws InvocationTargetException, IllegalAccessException {
        assertSecurityError(user, Response.Status.FORBIDDEN);
    }

    @Test
    public void interceptWithAnnotationAnonymous() throws InvocationTargetException, IllegalAccessException {
        assertSecurityError(null, Response.Status.UNAUTHORIZED);
    }

    private void assertSecurityError(ApplicationUser user, final Response.Status error) throws IllegalAccessException, InvocationTargetException {
        jac.setLoggedInUser(user);

        final SecurityInterceptor interceptor = new SecurityInterceptor(jac, permissionManager);
        final MethodInvocation invocation = createInvocation(new WithAuth());
        interceptor.intercept(invocation);

        verify(invocation, never()).invoke();

        final HttpResponseContext httpContext = invocation.getHttpContext().getResponse();
        final ArgumentCaptor<Response> captor = ArgumentCaptor.forClass(Response.class);
        verify(httpContext).setResponse(captor.capture());
        final Response value = captor.getValue();

        assertResponseCacheNever(value);
        assertEquals(error.getStatusCode(), value.getStatus());
    }

    @Test
    public void interceptWithAnnotationWithPermission() throws InvocationTargetException, IllegalAccessException {
        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(true);

        final SecurityInterceptor interceptor = new SecurityInterceptor(jac, permissionManager);
        final MethodInvocation invocation = createInvocation(new WithAuth());
        interceptor.intercept(invocation);

        verify(invocation).invoke();
    }

    @AdminRequired
    public static class WithAuth {
    }

    private MethodInvocation createInvocation(Object resource) {
        final HttpContext context = Mockito.mock(HttpContext.class);
        when(context.getResponse()).thenReturn(Mockito.mock(HttpResponseContext.class));

        final MethodInvocation mock = Mockito.mock(MethodInvocation.class);
        when(mock.getResource()).thenReturn(resource);
        when(mock.getHttpContext()).thenReturn(context);

        return mock;
    }
}
