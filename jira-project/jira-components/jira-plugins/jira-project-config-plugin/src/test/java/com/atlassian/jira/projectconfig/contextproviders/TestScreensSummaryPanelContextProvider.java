package com.atlassian.jira.projectconfig.contextproviders;

import java.util.List;
import java.util.Map;

import com.atlassian.jira.issue.fields.screen.FieldScreenScheme;
import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenSchemeEntity;
import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenSchemeManager;
import com.atlassian.jira.mock.issue.fields.screen.issuetype.MockIssueTypeScreenScheme;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.projectconfig.contextproviders.ScreensSummaryPanelContextProvider.SimpleIssueTypeScreenScheme;
import com.atlassian.jira.projectconfig.contextproviders.ScreensSummaryPanelContextProvider.SimpleScreenScheme;
import com.atlassian.jira.projectconfig.order.MockOrderFactory;
import com.atlassian.jira.projectconfig.util.TabUrlFactory;
import com.atlassian.jira.util.UrlBuilder;
import com.atlassian.jira.util.collect.MapBuilder;

import com.google.common.collect.Lists;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.ofbiz.core.entity.GenericValue;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.when;

/**
 * Test for {@link ScreensSummaryPanelContextProvider}
 *
 * @since v4.4
 */
@RunWith(MockitoJUnitRunner.class)
public class TestScreensSummaryPanelContextProvider {
    private static final String SCREEN_SCHEME_NAME = "Screen Scheme Name";
    private static final String SCREEN_SCHEME_DESCRIPTION = "Screen Scheme Description";
    private static final String OTHER_SCREEN_SCHEME_NAME = "zScreen Scheme Name";
    private static final String OTHER_SCREEN_SCHEME_DESCRIPTION = "zScreen Scheme Description";

    @Mock
    private IssueTypeScreenSchemeManager issueTypeScreenSchemeManager;
    @Mock
    private IssueTypeScreenSchemeEntity issueTypeScreenSchemeEntity;
    @Mock
    private IssueTypeScreenSchemeEntity otherIssueTypeScreenSchemeEntity;
    @Mock
    private FieldScreenScheme fieldScreenScheme;
    @Mock
    private FieldScreenScheme otherFieldScreenScheme;
    @Mock
    private ContextProviderUtils contextProviderUtils;
    @Mock
    private TabUrlFactory tabUrlFactory;

    private GenericValue projectGV;
    private MockOrderFactory comparatorFactory;

    @Before
    public void setUp() throws Exception {
        projectGV = new MockGenericValue("Project");
        comparatorFactory = new MockOrderFactory();
    }

    @Test
    public void testGetContextMap() throws Exception {
        // setup
        final MockProject mockProject = new MockProject(888L, null, null, projectGV);
        mockProject.setIssueTypes("type a", "type b");

        final Map<String, Object> testContext = MapBuilder.<String, Object>build(
                ContextProviderUtils.CONTEXT_PROJECT_KEY, mockProject
        );

        final MockIssueTypeScreenScheme mockIssueTypeScreenScheme = new MockIssueTypeScreenScheme(5858L, "screen scheme", "screen scheme description")
                .setEntities(MapBuilder.build(
                        null, issueTypeScreenSchemeEntity,
                        "type a", issueTypeScreenSchemeEntity,
                        "type b", otherIssueTypeScreenSchemeEntity
                        )
                );
        when(issueTypeScreenSchemeManager.getIssueTypeScreenScheme(eq(projectGV))).thenReturn(mockIssueTypeScreenScheme);

        when(issueTypeScreenSchemeEntity.getFieldScreenScheme()).thenReturn(fieldScreenScheme);
        when(otherIssueTypeScreenSchemeEntity.getFieldScreenScheme()).thenReturn(otherFieldScreenScheme);


        when(fieldScreenScheme.getId()).thenReturn(4848L);
        when(fieldScreenScheme.getName()).thenReturn(SCREEN_SCHEME_NAME);
        when(fieldScreenScheme.getDescription()).thenReturn(SCREEN_SCHEME_DESCRIPTION);

        when(otherFieldScreenScheme.getId()).thenReturn(3838L);
        when(otherFieldScreenScheme.getName()).thenReturn(OTHER_SCREEN_SCHEME_NAME);
        when(otherFieldScreenScheme.getDescription()).thenReturn(OTHER_SCREEN_SCHEME_DESCRIPTION);

        final ScreensSummaryPanelContextProvider screensSummaryPanelContextProvider =
                getScreensSummarypanelContextProviderUnderTest(issueTypeScreenSchemeManager, contextProviderUtils);

        // execute
        final Map<String, Object> contextMap = screensSummaryPanelContextProvider.getContextMap(testContext);

        // verify
        final List<SimpleScreenScheme> expectedScreenSchemes =
                Lists.newArrayList(
                        new SimpleScreenScheme(4848L, SCREEN_SCHEME_NAME, SCREEN_SCHEME_DESCRIPTION, getScreenUrl(4848L), true),
                        new SimpleScreenScheme(3838L, OTHER_SCREEN_SCHEME_NAME, OTHER_SCREEN_SCHEME_DESCRIPTION, getScreenUrl(3838L), false)
                );

        final SimpleIssueTypeScreenScheme expectedSimpleIssueTypeScreenScheme =
                new SimpleIssueTypeScreenScheme("screen scheme", "screen scheme description", getChangeUrl(888L), getEditUrl());

        final Map<String, Object> expectedContextMap = MapBuilder.<String, Object>newBuilder()
                .addAll(testContext)
                .add(ScreensSummaryPanelContextProvider.CONTEXT_ISSUE_TYPE_SCREEN_SCHEME_KEY, expectedSimpleIssueTypeScreenScheme)
                .add(ScreensSummaryPanelContextProvider.CONTEXT_SCREEN_SCHEMES_KEY, expectedScreenSchemes)
                .toMap();

        assertEquals(contextMap, expectedContextMap);
    }

    @Test
    public void testGetContextMapWithNoDefaultScreenSchemes() throws Exception {
        // setup
        final MockProject mockProject = new MockProject(888L, null, null, projectGV);
        mockProject.setIssueTypes("type a", "type b");

        final Map<String, Object> testContext = MapBuilder.<String, Object>build(
                ContextProviderUtils.CONTEXT_PROJECT_KEY, mockProject
        );

        final MockIssueTypeScreenScheme mockIssueTypeScreenScheme = new MockIssueTypeScreenScheme(5858L, "screen scheme", null)
                .setEntities(MapBuilder.build(
                        null, otherIssueTypeScreenSchemeEntity,
                        "type a", issueTypeScreenSchemeEntity,
                        "type b", issueTypeScreenSchemeEntity
                        )
                );
        when(issueTypeScreenSchemeManager.getIssueTypeScreenScheme(eq(projectGV))).thenReturn(mockIssueTypeScreenScheme);

        when(issueTypeScreenSchemeEntity.getFieldScreenScheme()).thenReturn(fieldScreenScheme);
        when(otherIssueTypeScreenSchemeEntity.getFieldScreenScheme()).thenReturn(otherFieldScreenScheme);

        when(fieldScreenScheme.getId()).thenReturn(4848L);
        when(fieldScreenScheme.getName()).thenReturn(SCREEN_SCHEME_NAME);
        when(fieldScreenScheme.getDescription()).thenReturn(SCREEN_SCHEME_DESCRIPTION);

        when(otherFieldScreenScheme.getId()).thenReturn(3838L);
        when(otherFieldScreenScheme.getName()).thenReturn(OTHER_SCREEN_SCHEME_NAME);
        when(otherFieldScreenScheme.getDescription()).thenReturn(OTHER_SCREEN_SCHEME_DESCRIPTION);

        final ScreensSummaryPanelContextProvider screensSummaryPanelContextProvider =
                getScreensSummarypanelContextProviderUnderTest(issueTypeScreenSchemeManager, contextProviderUtils);

        // execute
        final Map<String, Object> contextMap = screensSummaryPanelContextProvider.getContextMap(testContext);

        // verify
        final List<SimpleScreenScheme> expectedScreenSchemes =
                Lists.newArrayList(
                        new SimpleScreenScheme(4848L, SCREEN_SCHEME_NAME, SCREEN_SCHEME_DESCRIPTION, getScreenUrl(4848L), false)
                );

        final SimpleIssueTypeScreenScheme expectedSimpleIssueTypeScreenScheme =
                new SimpleIssueTypeScreenScheme("screen scheme", null, getChangeUrl(888L), getEditUrl());

        final Map<String, Object> expectedContextMap = MapBuilder.<String, Object>newBuilder()
                .addAll(testContext)
                .add(ScreensSummaryPanelContextProvider.CONTEXT_ISSUE_TYPE_SCREEN_SCHEME_KEY, expectedSimpleIssueTypeScreenScheme)
                .add(ScreensSummaryPanelContextProvider.CONTEXT_SCREEN_SCHEMES_KEY, expectedScreenSchemes)
                .toMap();

        assertEquals(contextMap, expectedContextMap);
    }

    @Test
    public void testScreenSchemeUrl() {
        // setup
        final long screenSchemeId = 4L;
        when(contextProviderUtils.createUrlBuilder("/secure/admin/ConfigureFieldScreenScheme.jspa"))
                .thenReturn(new UrlBuilder("screenScheme", "UTF-8", false));

        // execute
        ScreensSummaryPanelContextProvider provider = new ScreensSummaryPanelContextProvider(issueTypeScreenSchemeManager,
                contextProviderUtils, tabUrlFactory, comparatorFactory);

        // verify
        assertEquals("screenScheme?id=" + screenSchemeId, provider.getScreenSchemeUrl(screenSchemeId));
    }

    @Test
    public void testGetChangeSchemeUrl() {
        // setup
        final long projectId = 5L;
        when(contextProviderUtils.createUrlBuilder("/secure/admin/SelectIssueTypeScreenScheme!default.jspa"))
                .thenReturn(new UrlBuilder("screenScheme", "UTF-8", false));

        // execute
        ScreensSummaryPanelContextProvider provider =
                new ScreensSummaryPanelContextProvider(issueTypeScreenSchemeManager, contextProviderUtils, tabUrlFactory, comparatorFactory);

        // verify
        assertEquals("screenScheme?projectId=" + projectId, provider.getChangeSchemeUrl(projectId));
    }

    @Test
    public void testGetEditSchemeUrl() {
        // setup
        when(tabUrlFactory.forScreens()).thenReturn("screens");

        // execute
        ScreensSummaryPanelContextProvider provider =
                new ScreensSummaryPanelContextProvider(issueTypeScreenSchemeManager, contextProviderUtils, tabUrlFactory, comparatorFactory);

        // verify
        assertEquals("screens", provider.getEditSchemeUrl());
    }

    private ScreensSummaryPanelContextProvider getScreensSummarypanelContextProviderUnderTest(IssueTypeScreenSchemeManager issueTypeScreenSchemeManager, ContextProviderUtils contextProviderUtils) {
        return new ScreensSummaryPanelContextProvider(issueTypeScreenSchemeManager, contextProviderUtils, tabUrlFactory, comparatorFactory) {
            @Override
            String getChangeSchemeUrl(final Long id) {
                return getChangeUrl(id);
            }

            @Override
            String getEditSchemeUrl() {
                return getEditUrl();
            }

            @Override
            String getScreenSchemeUrl(final Long id) {
                return getScreenUrl(id);
            }
        };
    }

    private static String getScreenUrl(Long id) {
        return "screenSchemeUrl" + id;
    }

    private static String getEditUrl() {
        return "editUrl";
    }

    private static String getChangeUrl(Long id) {
        return "changeUrl" + id;
    }

}
