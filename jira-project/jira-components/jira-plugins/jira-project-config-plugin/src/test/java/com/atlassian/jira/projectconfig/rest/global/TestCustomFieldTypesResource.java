package com.atlassian.jira.projectconfig.rest.global;

import com.atlassian.jira.action.issue.customfields.MockCustomFieldType;
import com.atlassian.jira.bc.customfield.CustomFieldService;
import com.atlassian.jira.config.managedconfiguration.ConfigurationItemAccessLevel;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.customfields.CustomFieldSearcher;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.customfields.CustomFieldTypeCategory;
import com.atlassian.jira.issue.customfields.config.item.SettableOptionsConfigItem;
import com.atlassian.jira.issue.fields.config.FieldConfigItemType;
import com.atlassian.jira.issue.search.searchers.MockCustomFieldSearcher;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.plugin.customfield.CustomFieldTypeModuleDescriptor;
import com.atlassian.jira.projectconfig.rest.AdminRequired;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.type.TypeReference;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.Locale;
import java.util.Map;
import javax.ws.rs.core.Response;

import static com.atlassian.jira.projectconfig.rest.RestAssertions.assertResponseCacheNever;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.stub;
import static org.mockito.Mockito.when;

/**
 * @since v6.1
 */
@RunWith(MockitoJUnitRunner.class)
public class TestCustomFieldTypesResource {
    private static final TypeReference<Map<String, Object>> MAP_TYPE = new TypeReference<Map<String, Object>>() {
    };
    private static final String RESOURCE_TYPE = "download";
    private static final String RESOURCE_NAME = "customfieldpreview.png";

    @Mock
    private CustomFieldService customFieldService;

    @Mock
    private WebResourceUrlProvider webResourceUrlProvider;

    @Mock
    private CustomFieldManager customFieldManager;

    private ObjectMapper mapper;

    private final ApplicationUser user = new MockApplicationUser("admin");
    private JiraAuthenticationContext authenticationContext = new MockSimpleAuthenticationContext(user,
            Locale.ENGLISH, new NoopI18nHelper(Locale.ENGLISH));

    @Before
    public void initObjectMapper() {
        mapper = new ObjectMapper();
        mapper.setSerializationConfig(mapper.getSerializationConfig()
                .withSerializationInclusion(JsonSerialize.Inclusion.NON_NULL)
                .with(SerializationConfig.Feature.USE_STATIC_TYPING));
    }

    @After
    public void destroyObjectMapper() {
        mapper = null;
    }

    @Test
    public void getAllCustomFieldsWithAdministrator() {
        final String url = "/some/random/url";

        ResourceDescriptor descriptor = Mockito.mock(ResourceDescriptor.class);
        CustomFieldTypeModuleDescriptor moduleDescriptor = Mockito.mock(CustomFieldTypeModuleDescriptor.class);

        //This module descriptor will return a resource.
        MockCustomFieldType customFieldType = new MockCustomFieldType("key", "name", "description");
        customFieldType.init(moduleDescriptor);
        when(moduleDescriptor.getResourceDescriptor(RESOURCE_TYPE, RESOURCE_NAME))
                .thenReturn(descriptor);
        when(moduleDescriptor.getCategories()).thenReturn(EnumSet.of(CustomFieldTypeCategory.STANDARD));
        when(webResourceUrlProvider.getStaticPluginResourceUrl(moduleDescriptor, RESOURCE_NAME, UrlMode.RELATIVE))
                .thenReturn(url);
        when(customFieldManager.getCustomFieldSearchers(customFieldType))
                .thenReturn(Arrays.<CustomFieldSearcher>asList(new MockCustomFieldSearcher("10")));

        //This module descriptor will return no resource.
        CustomFieldTypeModuleDescriptor moduleDescriptorNoScreenShot = Mockito.mock(CustomFieldTypeModuleDescriptor.class);
        MockCustomFieldType customFieldTypeNoSceenShot = new MockCustomFieldType("key2", "name2", "description2");
        customFieldTypeNoSceenShot.init(moduleDescriptorNoScreenShot);

        stub(customFieldService.getCustomFieldTypesForUser(user))
                .toReturn(Arrays.<CustomFieldType<?, ?>>asList(customFieldType, customFieldTypeNoSceenShot));

        final CustomFieldTypesResource customFieldTypesResource = createResource();

        Response response = customFieldTypesResource.getTypes();
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        assertEquals(toMap("customfield-type-response-simple.json"), toMap(response.getEntity()));
        assertResponseCacheNever(response);
    }

    @Test
    public void getAllCustomFieldsSomeLocked() {
        //This module descriptor will return a resource.
        MockCustomFieldType customFieldTypeSysAdmin = createCustomFieldForLevel(ConfigurationItemAccessLevel.SYS_ADMIN, true);
        MockCustomFieldType customFieldTypeNull = createCustomFieldForLevel(null, false);

        stub(customFieldService.getCustomFieldTypesForUser(user))
                .toReturn(Arrays.<CustomFieldType<?, ?>>asList(customFieldTypeSysAdmin, customFieldTypeNull));

        final CustomFieldTypesResource customFieldTypesResource = createResource();

        Response response = customFieldTypesResource.getTypes();
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        assertEquals(toMap("customfield-type-response-managed.json"), toMap(response.getEntity()));
        assertResponseCacheNever(response);
    }

    @Test
    public void getAllCustomFieldsOptionsAndCascading() {
        final MockCustomFieldType one = createForName("one");
        final MockCustomFieldType two = createForName("two");
        final MockCustomFieldType three = createForName("three");

        stub(customFieldService.getCustomFieldTypesForUser(user))
                .toReturn(Arrays.<CustomFieldType<?, ?>>asList(one, two, three));

        final CustomFieldTypesResource customFieldTypesResource = new CustomFieldTypesResource(customFieldService, customFieldManager, authenticationContext, webResourceUrlProvider) {
            @Override
            boolean isCascading(final CustomFieldType<?, ?> type) {
                return type == one;
            }

            @Override
            boolean hasOptions(final CustomFieldType<?, ?> type) {
                return type == one || type == two;
            }
        };

        Response response = customFieldTypesResource.getTypes();
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        assertEquals(toMap("customfield-type-response-options.json"), toMap(response.getEntity()));
        assertResponseCacheNever(response);
    }

    @Test
    public void hasOption() {
        final MockCustomFieldType one = createForName("one");
        one.setConfigurationItemTypes(Arrays.<FieldConfigItemType>asList(new SettableOptionsConfigItem(one, null)));

        final MockCustomFieldType two = createForName("two");

        final CustomFieldTypesResource resource = createResource();
        assertTrue(resource.hasOptions(one));
        assertFalse(resource.hasOptions(two));
    }

    @Test
    public void ensureAllCustomFieldsRequiresWebSudo() {
        assertNotNull(CustomFieldTypesResource.class.getAnnotation(WebSudoRequired.class));
    }

    @Test
    public void ensureSecurityAnnotation() {
        assertNotNull(CustomFieldTypesResource.class.getAnnotation(AdminRequired.class));
    }

    private MockCustomFieldType createCustomFieldForLevel(final ConfigurationItemAccessLevel level, boolean isManaged) {
        CustomFieldTypeModuleDescriptor moduleDescriptorLocked = Mockito.mock(CustomFieldTypeModuleDescriptor.class);
        MockCustomFieldType customFieldTypeLocked = new MockCustomFieldType("key-" + level, "name-" + level, "description-" + level);
        customFieldTypeLocked.init(moduleDescriptorLocked);
        when(moduleDescriptorLocked.getManagedAccessLevel()).thenReturn(level);
        when(moduleDescriptorLocked.isTypeManaged()).thenReturn(isManaged);
        if (isManaged) {
            when(moduleDescriptorLocked.getManagedDescriptionKey()).thenReturn("managedDescription-" + level);
        }
        return customFieldTypeLocked;
    }

    private MockCustomFieldType createForName(String name) {
        CustomFieldTypeModuleDescriptor descriptor = Mockito.mock(CustomFieldTypeModuleDescriptor.class);
        final MockCustomFieldType mockCustomFieldType = new MockCustomFieldType("key-" + name, "name-" + name, "description-" + name);
        mockCustomFieldType.init(descriptor);
        return mockCustomFieldType;
    }

    private Map<String, Object> toMap(Object bean) {
        return mapper.convertValue(bean, new TypeReference<Map<String, Object>>() {
        });
    }

    private CustomFieldTypesResource createResource() {
        return new CustomFieldTypesResource(customFieldService, customFieldManager,
                authenticationContext, webResourceUrlProvider);
    }

    private Map<String, Object> toMap(String json) {
        try {
            return mapper.readValue(this.getClass().getResourceAsStream(json), MAP_TYPE);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
