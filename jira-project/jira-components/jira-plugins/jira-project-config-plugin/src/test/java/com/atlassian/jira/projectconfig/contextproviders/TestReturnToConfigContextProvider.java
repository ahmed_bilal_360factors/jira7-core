package com.atlassian.jira.projectconfig.contextproviders;

import com.atlassian.jira.bc.project.ProjectAction;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.datetime.DateTimeStyle;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockComponentContainer;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.xsrf.XsrfTokenGenerator;
import com.atlassian.jira.template.soy.SoyTemplateRendererProvider;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserPropertyManager;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.velocity.VelocityRequestContext;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.util.velocity.VelocityRequestSession;
import com.atlassian.jira.web.SessionKeys;
import com.atlassian.mock.propertyset.MockPropertySet;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestReturnToConfigContextProvider {
    private static final String SAMPLE_PROJECT_KEY = "PRJ";
    private static final String SAMPLE_TAB = "tab123";
    private static final String SAMPLE_BACK_TO_PROJECT_PATH = "/path456";

    @Rule
    public MockComponentContainer container = new MockComponentContainer(this);

    @Mock
    @AvailableInContainer
    private DateTimeFormatter dateTimeFormatter;

    @Mock
    @AvailableInContainer
    private PermissionManager permissionManager;

    @Mock
    @AvailableInContainer
    private SoyTemplateRendererProvider soyTemplateRendererProvider;

    @Mock
    @AvailableInContainer
    private XsrfTokenGenerator xsrfTokenGenerator;

    @Mock
    private ProjectService projectService;

    @Mock
    @AvailableInContainer
    private JiraAuthenticationContext authenticationContext;


    @Mock
    @AvailableInContainer
    private UserPropertyManager userPropertyManager;

    @Mock
    private I18nHelper i18nHelper;

    @Mock
    private ApplicationUser user;

    @Mock
    private Project project;

    @InjectMocks
    private ReturnToConfigContextProvider returnToConfigContextProvider;

    private MockPropertySet propertySet = new MockPropertySet();

    @Before
    public void setUp() throws Exception {
        when(authenticationContext.getLoggedInUser()).thenReturn(user);
        when(projectService.getProjectByKeyForAction(eq(user), eq(SAMPLE_PROJECT_KEY), eq(ProjectAction.EDIT_PROJECT_CONFIG)))
                .thenReturn(new ProjectService.GetProjectResult(project));

        when(dateTimeFormatter.withStyle(any(DateTimeStyle.class))).thenReturn(dateTimeFormatter);
        when(dateTimeFormatter.forLoggedInUser()).thenReturn(dateTimeFormatter);
        when(authenticationContext.getI18nHelper()).thenReturn(i18nHelper);

        when(userPropertyManager.getPropertySet(user)).thenReturn(propertySet);

        propertySet.setString(SessionKeys.CURRENT_ADMIN_PROJECT, SAMPLE_PROJECT_KEY);
        propertySet.setString(SessionKeys.CURRENT_ADMIN_PROJECT_TAB, SAMPLE_TAB);
    }

    @Test
    public void testShouldDisplayYes() throws Exception {
        assertThat(returnToConfigContextProvider.shouldDisplay(null), is(true));
    }

    @Test
    public void testShouldDisplayNo() throws Exception {
        propertySet.remove();
        propertySet.setString(SessionKeys.CURRENT_ADMIN_PROJECT_TAB, "");
        assertThat(returnToConfigContextProvider.shouldDisplay(null), is(false));
    }

    @Test
    public void testGetContextMapAll() throws Exception {
        propertySet.setString(SessionKeys.CURRENT_ADMIN_PROJECT_RETURN_URL, SAMPLE_BACK_TO_PROJECT_PATH);

        Map<String, Object> context = returnToConfigContextProvider.getContextMap(ImmutableMap.of("key1", (Object) "value2"));
        assertThat(context, hasEntry("key1", (Object) "value2"));
        assertThat(context, hasEntry(ReturnToConfigContextProvider.VM_CONTEXT_KEY_PROJECT, (Object) project));
        assertThat(context, hasEntry(ReturnToConfigContextProvider.VM_CONTEXT_KEY_BACK_TO_PROJECT_PATH,
                (Object) SAMPLE_BACK_TO_PROJECT_PATH));
    }
}
