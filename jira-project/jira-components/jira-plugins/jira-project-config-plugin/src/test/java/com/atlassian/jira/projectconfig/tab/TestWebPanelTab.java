package com.atlassian.jira.projectconfig.tab;

import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.util.VelocityContextFactory;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.descriptors.WebPanelModuleDescriptor;
import com.atlassian.plugin.web.model.WebPanel;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test case for {@link com.atlassian.jira.projectconfig.tab.WebPanelTab}.
 *
 * @since v4.4
 */
@RunWith(MockitoJUnitRunner.class)
public class TestWebPanelTab {
    @Mock
    private WebInterfaceManager webInterfaceManager;
    @Mock
    private VelocityContextFactory factory;
    @Mock
    private ProjectConfigTabRenderContext context;
    @Mock
    private Project project = new MockProject(1L);

    @Test
    public void testGetTabNoWebPanels() throws Exception {
        String id = "id";
        String link = "link";
        String pathInfo = "extra";

        when(context.getProject()).thenReturn(project);
        when(context.getPathInfo()).thenReturn(pathInfo);

        Map<String, Object> ctx = ImmutableMap.<String, Object>of("default", true);
        Map<String, Object> expectedCtx = ImmutableMap.<String, Object>builder().putAll(ctx)
                .put(WebPanelTab.EXTRA_URL_PARAM, pathInfo)
                .put(WebPanelTab.CURRENT_PROJECT, project)
                .put(WebPanelTab.CURRENT_TAB_NAME, id)
                .build();

        when(factory.createDefaultVelocityContext()).thenReturn(ctx);

        when(webInterfaceManager.getDisplayableWebPanelDescriptors(WebPanelTab.HEADER_TAB_LOCATION, expectedCtx))
                .thenReturn(Collections.<WebPanelModuleDescriptor>emptyList());
        when(webInterfaceManager.getDisplayableWebPanelDescriptors(createLocationForId(id), expectedCtx))
                .thenReturn(Collections.<WebPanelModuleDescriptor>emptyList());

        WebPanelTab panelTab = new WebPanelTab(webInterfaceManager, factory, id, link) {
            @Override
            public String getTitle(ProjectConfigTabRenderContext context) {
                return "";
            }
        };

        assertEquals(id, panelTab.getId());
        assertEquals(link, panelTab.getLinkId());
        assertEquals("", panelTab.getTab(context));
        assertEquals("", panelTab.getTab(context));
    }

    @Test
    public void testGetTabOneWebPanels() throws Exception {
        String id = "id";
        String link = "link";
        String data1 = "data1";
        String data2 = "data2";
        String pathInfo = "extra";

        when(context.getProject()).thenReturn(project);
        when(context.getPathInfo()).thenReturn(pathInfo);

        Map<String, Object> ctx = ImmutableMap.<String, Object>of("default", true);
        when(factory.createDefaultVelocityContext()).thenReturn(ctx);

        Map<String, Object> expectedCtx = ImmutableMap.<String, Object>builder().putAll(ctx)
                .put(WebPanelTab.CURRENT_PROJECT, project)
                .put(WebPanelTab.CURRENT_TAB_NAME, id)
                .put(WebPanelTab.EXTRA_URL_PARAM, pathInfo)
                .build();

        WebPanel panel = mock(WebPanel.class);
        when(panel.getHtml(expectedCtx)).thenReturn(data1);

        WebPanelModuleDescriptor descriptor = mock(WebPanelModuleDescriptor.class);
        when(descriptor.getModule()).thenReturn(panel);

        when(webInterfaceManager.getDisplayableWebPanelDescriptors(WebPanelTab.HEADER_TAB_LOCATION, expectedCtx))
                .thenReturn(Collections.<WebPanelModuleDescriptor>emptyList());
        when(webInterfaceManager.getDisplayableWebPanelDescriptors(createLocationForId(id), expectedCtx))
                .thenReturn(Collections.singletonList(descriptor));

        WebPanelTab panelTab = new WebPanelTab(webInterfaceManager, factory, id, link) {
            @Override
            public String getTitle(ProjectConfigTabRenderContext context) {
                return "";
            }
        };

        assertEquals(id, panelTab.getId());
        assertEquals(link, panelTab.getLinkId());
        assertEquals(data1, panelTab.getTab(context));

        when(panel.getHtml(expectedCtx)).thenReturn(data2);

        assertEquals(data2, panelTab.getTab(context));
    }

    @Test
    public void testGetTabOneMultiplePanels() throws Exception {
        String id = "id";
        String link = "link";
        String data1 = "data1";
        String data2 = "data2";
        String pathInfo = "extra";

        when(context.getProject()).thenReturn(project);
        when(context.getPathInfo()).thenReturn(pathInfo);

        Map<String, Object> ctx = ImmutableMap.<String, Object>of("default", true);
        when(factory.createDefaultVelocityContext()).thenReturn(ctx);

        Map<String, Object> expectedCtx = ImmutableMap.<String, Object>builder().putAll(ctx)
                .put(WebPanelTab.CURRENT_PROJECT, project)
                .put(WebPanelTab.CURRENT_TAB_NAME, id)
                .put(WebPanelTab.EXTRA_URL_PARAM, pathInfo)
                .build();

        WebPanel panel = mock(WebPanel.class);
        when(panel.getHtml(expectedCtx)).thenReturn(data1);

        WebPanelModuleDescriptor descriptor = mock(WebPanelModuleDescriptor.class);
        when(descriptor.getModule()).thenReturn(panel);

        WebPanelModuleDescriptor descriptor2 = mock(WebPanelModuleDescriptor.class);

        when(webInterfaceManager.getDisplayableWebPanelDescriptors(WebPanelTab.HEADER_TAB_LOCATION, expectedCtx))
                .thenReturn(Collections.<WebPanelModuleDescriptor>emptyList());
        when(webInterfaceManager.getDisplayableWebPanelDescriptors(createLocationForId(id), expectedCtx))
                .thenReturn(Arrays.asList(descriptor, descriptor2));

        WebPanelTab panelTab = new WebPanelTab(webInterfaceManager, factory, id, link) {
            @Override
            public String getTitle(ProjectConfigTabRenderContext context) {
                return "";
            }
        };

        assertEquals(id, panelTab.getId());
        assertEquals(link, panelTab.getLinkId());
        assertEquals(data1, panelTab.getTab(context));

        when(panel.getHtml(expectedCtx)).thenReturn(data2);

        assertEquals(data2, panelTab.getTab(context));
    }

    @Test
    public void testGetTabWithHeader() throws Exception {
        String id = "id";
        String link = "link";
        String headerData = "headerData:";
        String data1 = "data1";
        String data2 = "data2";
        String pathInfo = "extra";

        when(context.getProject()).thenReturn(project);
        when(context.getPathInfo()).thenReturn(pathInfo);

        Map<String, Object> ctx = ImmutableMap.<String, Object>of("default", true);
        when(factory.createDefaultVelocityContext()).thenReturn(ctx);

        Map<String, Object> expectedCtx = ImmutableMap.<String, Object>builder().putAll(ctx)
                .put(WebPanelTab.CURRENT_PROJECT, project)
                .put(WebPanelTab.CURRENT_TAB_NAME, id)
                .put(WebPanelTab.EXTRA_URL_PARAM, pathInfo)
                .build();

        WebPanel headerPanel = mock(WebPanel.class);
        when(headerPanel.getHtml(expectedCtx)).thenReturn(headerData);

        WebPanelModuleDescriptor headerDescriptor = mock(WebPanelModuleDescriptor.class);
        when(headerDescriptor.getModule()).thenReturn(headerPanel);

        WebPanel panel = mock(WebPanel.class);
        when(panel.getHtml(expectedCtx)).thenReturn(data1);

        WebPanelModuleDescriptor descriptor = mock(WebPanelModuleDescriptor.class);
        when(descriptor.getModule()).thenReturn(panel);

        WebPanelModuleDescriptor descriptor2 = mock(WebPanelModuleDescriptor.class);

        when(webInterfaceManager.getDisplayableWebPanelDescriptors(WebPanelTab.HEADER_TAB_LOCATION, expectedCtx))
                .thenReturn(Arrays.asList(headerDescriptor));
        when(webInterfaceManager.getDisplayableWebPanelDescriptors(createLocationForId(id), expectedCtx))
                .thenReturn(Arrays.asList(descriptor, descriptor2));

        WebPanelTab panelTab = new WebPanelTab(webInterfaceManager, factory, id, link) {
            @Override
            public String getTitle(ProjectConfigTabRenderContext context) {
                return "";
            }
        };

        assertEquals(id, panelTab.getId());
        assertEquals(link, panelTab.getLinkId());
        assertEquals("headerData:data1", panelTab.getTab(context));

        when(panel.getHtml(expectedCtx)).thenReturn(data2);

        assertEquals("headerData:data2", panelTab.getTab(context));

        verify(factory, times(2)).createDefaultVelocityContext();
    }

    private String createLocationForId(String id) {
        return "tabs.admin.projectconfig." + id;
    }
}
