package com.atlassian.jira.projectconfig.servlet;

import com.atlassian.jira.bc.project.ProjectAction;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.issue.fields.screen.FieldScreenManager;
import com.atlassian.jira.issue.fields.screen.FieldScreenTab;
import com.atlassian.jira.issue.fields.screen.MockFieldScreen;
import com.atlassian.jira.issue.fields.screen.MockFieldScreenTab;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.projectconfig.order.MockOrderFactory;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.google.common.collect.Lists.newArrayList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v4.4
 */
@RunWith(MockitoJUnitRunner.class)
public class TestInlineDialogServlet {
    @Mock
    private FieldScreenManager fieldScreenManager;
    @Mock
    private TemplateRenderer templateRenderer;
    @Mock
    private ProjectService projectService;
    @Mock
    private HttpServletResponse response;
    @Mock
    private HttpServletRequest request;

    private MockOrderFactory comparatorFactory;
    private MockApplicationUser user;
    private MockSimpleAuthenticationContext authContext;
    private MockVelocityRequestFactory contextFactory;

    @Before
    public void setUp() throws Exception {
        comparatorFactory = new MockOrderFactory();
        user = new MockApplicationUser("bbain");
        authContext = new MockSimpleAuthenticationContext(user, Locale.ENGLISH, new NoopI18nHelper());
        contextFactory = new MockVelocityRequestFactory();
    }

    @Test
    public void testPermissionNoProjectPassed() throws Exception {
        // setup
        InlineDialogServlet dialogServlet = new InlineDialogServlet(comparatorFactory, fieldScreenManager, templateRenderer, authContext, projectService, contextFactory);

        PrintWriter writer = new PrintWriter(new StringWriter());
        when(request.getParameter("projectKey")).thenReturn(null);
        when(response.getWriter()).thenReturn(writer);

        // execute
        dialogServlet.doGet(request, response);

        // verify
        Map<String, Object> expectedContext = ImmutableMap.<String, Object>of("message",
                NoopI18nHelper.makeTranslation("admin.project.fields.screens.perm.error"));
        verify(templateRenderer).render("screens/warningpanel.vm", expectedContext, writer);
    }

    @Test
    public void testPermissionNoProjectWithError() throws Exception {
        // setup
        InlineDialogServlet dialogServlet = new InlineDialogServlet(comparatorFactory, fieldScreenManager, templateRenderer, authContext, projectService, contextFactory);

        PrintWriter writer = new PrintWriter(new StringWriter());
        String key = "SOMETHING";
        when(request.getParameter("projectKey")).thenReturn(key);
        when(response.getWriter()).thenReturn(writer);

        SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        errorCollection.addErrorMessage("ImAnError");
        when(projectService.getProjectByKeyForAction(user, key, ProjectAction.EDIT_PROJECT_CONFIG))
                .thenReturn(new ProjectService.GetProjectResult(errorCollection));

        // execute
        dialogServlet.doGet(request, response);

        // verify
        Map<String, Object> expectedContext = ImmutableMap.<String, Object>of("message",
                NoopI18nHelper.makeTranslation("admin.project.fields.screens.perm.error"));
        verify(templateRenderer).render("screens/warningpanel.vm", expectedContext, writer);
    }

    @Test
    public void testPermissionNoProject() throws Exception {
        // setup
        InlineDialogServlet dialogServlet = new InlineDialogServlet(comparatorFactory, fieldScreenManager, templateRenderer, authContext, projectService, contextFactory);

        String key = "SOMETHING";
        when(request.getParameter("projectKey")).thenReturn(key);

        PrintWriter writer = new PrintWriter(new StringWriter());
        when(projectService.getProjectByKeyForAction(user, key, ProjectAction.EDIT_PROJECT_CONFIG))
                .thenReturn(new ProjectService.GetProjectResult(new SimpleErrorCollection()));
        when(response.getWriter()).thenReturn(writer);

        // execute
        dialogServlet.doGet(request, response);

        // verify
        Map<String, Object> expectedContext = ImmutableMap.<String, Object>of("message",
                NoopI18nHelper.makeTranslation("admin.project.fields.screens.perm.error"));
        verify(templateRenderer).render("screens/warningpanel.vm", expectedContext, writer);
    }

    @Test
    public void testNoFieldId() throws Exception {
        // setup
        MockProject project = new MockProject();
        InlineDialogServlet dialogServlet = new InlineDialogServlet(comparatorFactory, fieldScreenManager, templateRenderer, authContext, projectService, contextFactory);

        String key = "SOMETHING";
        when(request.getParameter("projectKey")).thenReturn(key);
        when(request.getParameter("fieldId")).thenReturn(null);

        when(projectService.getProjectByKeyForAction(user, key, ProjectAction.EDIT_PROJECT_CONFIG))
                .thenReturn(new ProjectService.GetProjectResult(new SimpleErrorCollection(), project));

        // execute
        dialogServlet.doGet(request, response);

        // verify
        verify(response).sendError(HttpServletResponse.SC_BAD_REQUEST, "No fieldId specified");
    }

    @Test
    public void testHappyPath() throws Exception {
        // setup
        MockProject project = new MockProject();

        MockFieldScreen fieldScreen1 = new MockFieldScreen();
        fieldScreen1.setId(4848L);
        fieldScreen1.setName("ZZZ");

        MockFieldScreen fieldScreen2 = new MockFieldScreen();
        fieldScreen2.setId(48482L);
        fieldScreen2.setName("AAA");

        MockFieldScreenTab tab1 = new MockFieldScreenTab();
        tab1.setName("ZZZZZ");
        tab1.setId(5l);
        tab1.setFieldScreen(fieldScreen1);
        MockFieldScreenTab tab2 = new MockFieldScreenTab();
        tab2.setName("AAAAA");
        tab2.setId(6l);
        tab2.setFieldScreen(fieldScreen2);

        String key = "SOMETHING";
        String field = "version";
        when(request.getParameter("projectKey")).thenReturn(key);
        when(request.getParameter("fieldId")).thenReturn(field);
        PrintWriter printWriter = new PrintWriter(new StringWriter());
        when(response.getWriter()).thenReturn(printWriter);

        when(projectService.getProjectByKeyForAction(user, key, ProjectAction.EDIT_PROJECT_CONFIG))
                .thenReturn(new ProjectService.GetProjectResult(new SimpleErrorCollection(), project));
        when(fieldScreenManager.getFieldScreenTabs(field)).thenReturn(Lists.<FieldScreenTab>newArrayList(tab1, tab2));

        InlineDialogServlet dialogServlet = new InlineDialogServlet(comparatorFactory, fieldScreenManager, templateRenderer, authContext, projectService, contextFactory);

        // execute
        dialogServlet.doGet(request, response);

        // verify
        Map<String, Object> expectedContext = ImmutableMap.<String, Object>of("screens",
                newArrayList(fieldScreen2, fieldScreen1));
        verify(templateRenderer).render("screens/screensdialog.vm", expectedContext, printWriter);
    }
}
