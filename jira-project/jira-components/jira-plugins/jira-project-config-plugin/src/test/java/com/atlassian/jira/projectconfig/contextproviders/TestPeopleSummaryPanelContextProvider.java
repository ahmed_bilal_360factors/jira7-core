package com.atlassian.jira.projectconfig.contextproviders;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.mock.MockApplicationProperties;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.plugin.profile.UserFormatManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectAssigneeTypes;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.collect.MapBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestPeopleSummaryPanelContextProvider {

    private static final String ASSIGNEE_PRETTY_STRING = "assigneePrettyString";
    private static final String MOCK_USERNAME = "hhartigan";
    private static final String MOCK_USERKEY = "henryhartigan";
    private static final String FORMATTED_PROJECT_LEAD = "FORMATTED";

    @Mock
    private PermissionManager permissionManager;
    @Mock
    private UserFormatManager userFormatManager;
    @Mock
    private UserManager userManager;
    @Mock
    private Project project;
    @Mock
    private AvatarService avatarService;
    @Mock
    private ApplicationUser user;
    @Mock
    private RolesSummaryPanelContextProvider rolesSummaryPanelContextProvider;

    private URI uri;
    private Map<String, Object> testContext;

    @Before()
    public void setUp() throws URISyntaxException {
        uri = new URI("http://www.example.com/pianocat.gif");
        testContext = MapBuilder.<String, Object>build(
                ContextProviderUtils.CONTEXT_PROJECT_KEY, project
        );
        when(project.getLeadUserKey()).thenReturn(MOCK_USERKEY);
        when(project.getLeadUserName()).thenReturn(MOCK_USERNAME);
        when(project.getAssigneeType()).thenReturn(ProjectAssigneeTypes.PROJECT_LEAD);
        when(project.getProjectLead()).thenReturn(user);

        when(userManager.getUserByKey(MOCK_USERKEY)).thenReturn(user);
        when(user.getUsername()).thenReturn(MOCK_USERNAME);

        when(avatarService.getAvatarURL(any(ApplicationUser.class), any(ApplicationUser.class), eq(Avatar.Size.SMALL))).thenReturn(uri);
        when(userFormatManager.formatUser(MOCK_USERKEY, "profileLink", "projectLead")).thenReturn(FORMATTED_PROJECT_LEAD);

        MockApplicationProperties mockApplicationProperties = new MockApplicationProperties();
        mockApplicationProperties.setOption(APKeys.JIRA_OPTION_ALLOWUNASSIGNED, true);

        new MockComponentWorker()
                .addMock(ApplicationProperties.class, mockApplicationProperties)
                .init();
    }

    @Test
    public void testGetContextMapWhereNoLeadExists() {
        when(userManager.getUserByKey(MOCK_USERKEY)).thenReturn(null);
        when(project.getProjectLead()).thenReturn(null);

        RolesSummaryPanelContextProvider contextProviderUnderTest = getRolesSummaryPanelContextProvider();

        // TODO JRADEV-19029 - Update avatar service to use ApplicationUser + extract boilerplate

        final Map<String, Object> contextMap = contextProviderUnderTest.getContextMap(testContext);
        final Map<String, Object> expectedContextMap = MapBuilder.<String, Object>newBuilder()
                .addAll(testContext)
                .add(RolesSummaryPanelContextProvider.CONTEXT_PROJECT_LEAD_EXISTS_KEY, false)
                .add(RolesSummaryPanelContextProvider.CONTEXT_PROJECT_LEAD_KEY, MOCK_USERKEY)
                .add(RolesSummaryPanelContextProvider.CONTEXT_IS_DEFAULT_ASSIGNEE_ASSIGNABLE_KEY, false)
                .add(RolesSummaryPanelContextProvider.CONTEXT_DEFAULT_ASSIGNEE_KEY, ASSIGNEE_PRETTY_STRING)
                .add(RolesSummaryPanelContextProvider.CONTEXT_DEFAULT_ASSIGNEE_EDITABLE, true)
                .add(RolesSummaryPanelContextProvider.CONTEXT_LEAD_USER_AVATAR_URL_KEY, uri.toString())
                .toMap();

        assertEquals(expectedContextMap, contextMap);
    }

    @Test
    public void testGetContextMapWhereLeadExistsAndLeadIsNotAssignable() {
        when(permissionManager.hasPermission(any(Integer.class), eq(project), eq(user))).thenReturn(false);

        RolesSummaryPanelContextProvider contextProviderUnderTest = getRolesSummaryPanelContextProvider();
        final Map<String, Object> contextMap = contextProviderUnderTest.getContextMap(testContext);
        final Map<String, Object> expectedContextMap = MapBuilder.<String, Object>newBuilder()
                .addAll(testContext)
                .add(RolesSummaryPanelContextProvider.CONTEXT_PROJECT_LEAD_EXISTS_KEY, true)
                .add(RolesSummaryPanelContextProvider.CONTEXT_PROJECT_LEAD_KEY, FORMATTED_PROJECT_LEAD)
                .add(RolesSummaryPanelContextProvider.CONTEXT_IS_DEFAULT_ASSIGNEE_ASSIGNABLE_KEY, false)
                .add(RolesSummaryPanelContextProvider.CONTEXT_DEFAULT_ASSIGNEE_KEY, ASSIGNEE_PRETTY_STRING)
                .add(RolesSummaryPanelContextProvider.CONTEXT_DEFAULT_ASSIGNEE_EDITABLE, true)
                .add(RolesSummaryPanelContextProvider.CONTEXT_LEAD_USER_AVATAR_URL_KEY, uri.toString())
                .toMap();
        assertEquals(expectedContextMap, contextMap);
    }

    @Test
    public void testGetContextMapWhereAssigneeTypeIsNull() throws Exception {
        when(project.getAssigneeType()).thenReturn(null);

        RolesSummaryPanelContextProvider contextProviderUnderTest = getRolesSummaryPanelContextProvider(null);
        final Map<String, Object> contextMap = contextProviderUnderTest.getContextMap(testContext);
        final Map<String, Object> expectedContextMap = MapBuilder.<String, Object>newBuilder()
                .addAll(testContext)
                .add(RolesSummaryPanelContextProvider.CONTEXT_PROJECT_LEAD_EXISTS_KEY, true)
                .add(RolesSummaryPanelContextProvider.CONTEXT_PROJECT_LEAD_KEY, FORMATTED_PROJECT_LEAD)
                .add(RolesSummaryPanelContextProvider.CONTEXT_IS_DEFAULT_ASSIGNEE_ASSIGNABLE_KEY, true)
                .add(RolesSummaryPanelContextProvider.CONTEXT_DEFAULT_ASSIGNEE_KEY, ASSIGNEE_PRETTY_STRING)
                .add(RolesSummaryPanelContextProvider.CONTEXT_DEFAULT_ASSIGNEE_EDITABLE, true)
                .add(RolesSummaryPanelContextProvider.CONTEXT_LEAD_USER_AVATAR_URL_KEY, uri.toString())
                .toMap();
        assertEquals(expectedContextMap, contextMap);
    }

    @Test
    public void testGetContextMapWhereUserIsDeletedButAssignableDueToLDAP() throws Exception {
        when(project.getAssigneeType()).thenReturn(null);
        when(userManager.getUserByKey(MOCK_USERKEY)).thenReturn(null);

        RolesSummaryPanelContextProvider contextProviderUnderTest = getRolesSummaryPanelContextProvider(null);
        final Map<String, Object> contextMap = contextProviderUnderTest.getContextMap(testContext);

        final Map<String, Object> expectedContextMap = MapBuilder.<String, Object>newBuilder()
                .addAll(testContext)
                .add(RolesSummaryPanelContextProvider.CONTEXT_PROJECT_LEAD_EXISTS_KEY, false)
                .add(RolesSummaryPanelContextProvider.CONTEXT_PROJECT_LEAD_KEY, MOCK_USERKEY)
                .add(RolesSummaryPanelContextProvider.CONTEXT_IS_DEFAULT_ASSIGNEE_ASSIGNABLE_KEY, true)
                .add(RolesSummaryPanelContextProvider.CONTEXT_DEFAULT_ASSIGNEE_KEY, ASSIGNEE_PRETTY_STRING)
                .add(RolesSummaryPanelContextProvider.CONTEXT_DEFAULT_ASSIGNEE_EDITABLE, true)
                .add(RolesSummaryPanelContextProvider.CONTEXT_LEAD_USER_AVATAR_URL_KEY, uri.toString())
                .toMap();
        assertEquals(expectedContextMap, contextMap);
    }

    @Test
    public void testGetContextMapWhereUserAvatarIsEnabled() {
        when(permissionManager.hasPermission(any(Integer.class), eq(project), eq(user))).thenReturn(true);

        RolesSummaryPanelContextProvider contextProviderUnderTest = getRolesSummaryPanelContextProvider();

        final Map<String, Object> contextMap = contextProviderUnderTest.getContextMap(testContext);
        final Map<String, Object> expectedContextMap = MapBuilder.<String, Object>newBuilder()
                .addAll(testContext)
                .add(RolesSummaryPanelContextProvider.CONTEXT_PROJECT_LEAD_EXISTS_KEY, true)
                .add(RolesSummaryPanelContextProvider.CONTEXT_PROJECT_LEAD_KEY, FORMATTED_PROJECT_LEAD)
                .add(RolesSummaryPanelContextProvider.CONTEXT_IS_DEFAULT_ASSIGNEE_ASSIGNABLE_KEY, true)
                .add(RolesSummaryPanelContextProvider.CONTEXT_DEFAULT_ASSIGNEE_KEY, ASSIGNEE_PRETTY_STRING)
                .add(RolesSummaryPanelContextProvider.CONTEXT_DEFAULT_ASSIGNEE_EDITABLE, true)
                .add(RolesSummaryPanelContextProvider.CONTEXT_LEAD_USER_AVATAR_URL_KEY, uri.toString())
                .toMap();
        assertEquals(expectedContextMap, contextMap);
    }

    private RolesSummaryPanelContextProvider getRolesSummaryPanelContextProvider(final Long expectedAssigneeType) {
        return new RolesSummaryPanelContextProvider(permissionManager, userFormatManager, userManager, avatarService) {
            @Override
            String getPrettyAssigneeTypeString(Long assigneeType) {
                assertEquals(expectedAssigneeType, assigneeType);
                return ASSIGNEE_PRETTY_STRING;
            }
        };
    }

    private RolesSummaryPanelContextProvider getRolesSummaryPanelContextProvider() {
        return getRolesSummaryPanelContextProvider(ProjectAssigneeTypes.PROJECT_LEAD);
    }
}
