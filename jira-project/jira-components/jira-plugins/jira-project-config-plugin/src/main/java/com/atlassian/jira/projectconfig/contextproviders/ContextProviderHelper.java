package com.atlassian.jira.projectconfig.contextproviders;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.util.UrlEncoder;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.UrlBuilder;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Abstract Context Provider for project config.
 *
 * @since v6.2
 */
@Component
public class ContextProviderHelper {
    private final UrlEncoder encoder;
    private final PermissionManager permissionManager;
    private final VelocityRequestContextFactory requestContextFactory;
    private final JiraAuthenticationContext authenticationContext;

    @Autowired
    public ContextProviderHelper(final UrlEncoder urlEncoder, final PermissionManager permissionManager,
                                 final JiraAuthenticationContext authenticationContext, final VelocityRequestContextFactory requestContextFactory) {
        this.encoder = urlEncoder;
        this.permissionManager = permissionManager;
        this.authenticationContext = authenticationContext;
        this.requestContextFactory = requestContextFactory;
    }

    public String encode(String value) {
        return encoder.encode(value);
    }

    public boolean hasProjectAdminPermission(Project project) {
        return permissionManager.hasPermission(Permissions.PROJECT_ADMIN, project, authenticationContext.getLoggedInUser());
    }

    public boolean hasAdminPermission() {
        return permissionManager.hasPermission(Permissions.ADMINISTER, authenticationContext.getLoggedInUser());
    }

    public I18nHelper getI18nHelper() {
        return authenticationContext.getI18nHelper();
    }

    public JiraAuthenticationContext getAuthenticationContext() {
        return authenticationContext;
    }

    public UrlBuilder createUrlBuilder(String basename) {
        return new UrlBuilder(getBaseUrl() + StringUtils.stripToEmpty(basename));
    }

    public String getBaseUrl() {
        return requestContextFactory.getJiraVelocityRequestContext().getBaseUrl();
    }
}
