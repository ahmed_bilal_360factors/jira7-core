package com.atlassian.jira.projectconfig.analytics;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.WorkflowSchemeManager;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.Schedule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@ExportAsService
public class AnalyticsScheduler implements LifecycleAware {

    private static final Logger LOG = LoggerFactory.getLogger(AnalyticsScheduler.class);
    private final static String JOB_KEY = AnalyticsScheduler.class.getName();
    private final static JobId JOB_ID = JobId.of(AnalyticsScheduler.class.getName());

    // run it once a week
    private final static long SCHEDULER_INTERVAL = 60000L * 60 * 24 * 7;

    private final SchedulerService schedulerService;
    private final ProjectManager projectManager;
    private final WorkflowSchemeManager workflowSchemeManager;
    private final WorkflowManager workflowManager;
    private final EventPublisher eventPublisher;

    @Autowired
    public AnalyticsScheduler(@ComponentImport SchedulerService schedulerService,
                              @ComponentImport ProjectManager projectManager,
                              @ComponentImport WorkflowSchemeManager workflowSchemeManager,
                              @ComponentImport WorkflowManager workflowManager,
                              @ComponentImport EventPublisher eventPublisher) {
        this.projectManager = projectManager;
        this.workflowSchemeManager = workflowSchemeManager;
        this.workflowManager = workflowManager;
        this.eventPublisher = eventPublisher;
        this.schedulerService = schedulerService;
    }

    private JobConfig getJobConfig() {
        return JobConfig.forJobRunnerKey(JobRunnerKey.of(JOB_KEY))
                .withSchedule(Schedule.forInterval(SCHEDULER_INTERVAL, new Date(System.currentTimeMillis() + (60000L * 60))));
    }

    @Override
    public void onStart() {
        final JobRunnerKey jobRunnerKey = JobRunnerKey.of(JOB_KEY);
        LOG.trace("Registering and scheduling AnalyticsJob");

        schedulerService.registerJobRunner(jobRunnerKey, new AnalyticsJob(projectManager, workflowSchemeManager, workflowManager, eventPublisher));
        try {
            schedulerService.scheduleJob(JOB_ID, getJobConfig());
            LOG.trace("AnalyticsJob schedule successfully completed");
        } catch (SchedulerServiceException e) {
            LOG.error("Error was thrown during scheduling an AnalyticsJob", e);
        }
    }

    @Override
    public void onStop() {
        schedulerService.unscheduleJob(JOB_ID);
        final JobRunnerKey jobRunnerKey = JobRunnerKey.of(JOB_KEY);
        schedulerService.unregisterJobRunner(jobRunnerKey);
    }
}
