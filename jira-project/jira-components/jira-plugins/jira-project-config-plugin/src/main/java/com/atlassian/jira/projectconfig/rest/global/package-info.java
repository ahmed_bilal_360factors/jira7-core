@InterceptorChain(SecurityInterceptor.class)
package com.atlassian.jira.projectconfig.rest.global;

import com.atlassian.jira.projectconfig.rest.interceptor.SecurityInterceptor;
import com.atlassian.plugins.rest.common.interceptor.InterceptorChain;

