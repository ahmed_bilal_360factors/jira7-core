package com.atlassian.jira.projectconfig.contextproviders;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.issue.fields.config.manager.IssueTypeSchemeManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.plugin.webfragment.CacheableContextProvider;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.beans.SimpleIssueType;
import com.atlassian.jira.projectconfig.beans.SimpleIssueTypeImpl;
import com.atlassian.jira.projectconfig.order.OrderFactory;
import com.atlassian.jira.projectconfig.util.TabUrlFactory;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.plugin.PluginParseException;

import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;

/**
 * Provides context for the issue types summary panel, in particular the "issueTypes" object
 * containing the list of a project's {@link IssueType}s.
 *
 * @since v4.4
 */
public class IssueTypesSummaryPanelContextProvider implements CacheableContextProvider {
    static final String CONTEXT_ISSUE_TYPES_KEY = "issueTypes";
    static final String CONTEXT_ISSUE_TYPE_SCHEME_KEY = "issueTypeScheme";
    static final String CONTEXT_ERRORS_KEY = "errors";

    static final String ISSUE_TYPE_SCHEME_ERROR_I18N_KEY = "admin.project.config.summary.issuetypes.no.issuetypescheme.error";
    static final String MANAGE_URL = "manageUrl";

    private final IssueTypeSchemeManager issueTypeSchemeManager;
    private final TabUrlFactory tabUrlFactory;
    private final OrderFactory orderFactory;

    public IssueTypesSummaryPanelContextProvider(final IssueTypeSchemeManager issueTypeSchemeManager,
                                                 TabUrlFactory tabUrlFactory, OrderFactory orderFactory) {
        this.issueTypeSchemeManager = issueTypeSchemeManager;
        this.tabUrlFactory = tabUrlFactory;
        this.orderFactory = orderFactory;
    }

    public void init(Map<String, String> params) throws PluginParseException {
    }

    public Map<String, Object> getContextMap(final Map<String, Object> context) {
        final Project project = (Project) context.get(ContextProviderUtils.CONTEXT_PROJECT_KEY);

        final I18nHelper i18nHelper = (I18nHelper) context.get(ContextProviderUtils.CONTEXT_I18N_KEY);

        final Collection<SimpleIssueType> simpleIssueTypes = getSimpleIssueTypes(project);

        final FieldConfigScheme issueTypeScheme = issueTypeSchemeManager.getConfigScheme(project);

        final List<String> errors = Lists.newArrayList();
        if (issueTypeScheme == null) {
            errors.add(i18nHelper.getText(ISSUE_TYPE_SCHEME_ERROR_I18N_KEY));
        }

        return MapBuilder.<String, Object>newBuilder()
                .addAll(context)
                .add(CONTEXT_ISSUE_TYPES_KEY, simpleIssueTypes)
                .add(CONTEXT_ISSUE_TYPE_SCHEME_KEY, issueTypeScheme)
                .add(CONTEXT_ERRORS_KEY, errors)
                .add(MANAGE_URL, tabUrlFactory.forIssueTypes())
                .toMap();
    }

    private Collection<SimpleIssueType> getSimpleIssueTypes(final Project project) {
        final List<IssueType> issueTypes = new ArrayList<>(issueTypeSchemeManager.getIssueTypesForProject(project));
        Collections.sort(issueTypes, (IssueType i1, IssueType i2) -> i1.getName().compareToIgnoreCase(i2.getName()));
        final IssueType defaultIssueType = issueTypeSchemeManager.getDefaultIssueType(project);

        final List<SimpleIssueType> simpleIssueTypes = Lists.newArrayList();

        for (final IssueType issueType : issueTypes) {
            if (defaultIssueType != null && defaultIssueType.equals(issueType)) {
                simpleIssueTypes.add(new SimpleIssueTypeImpl(issueType, true));
            } else {
                simpleIssueTypes.add(new SimpleIssueTypeImpl(issueType, false));
            }
        }

        return simpleIssueTypes;
    }

    @Override
    public String getUniqueContextKey(Map<String, Object> context) {
        return getClass().getName();
    }
}
