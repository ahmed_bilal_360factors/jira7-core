package com.atlassian.jira.projectconfig.tab;

import com.google.common.annotations.VisibleForTesting;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

/**
 * A tab manager that gets all the {@link ProjectConfigTab}s out of the {@link BeanFactory} associated with the plugin.
 *
 * @since v4.4
 */
@Component
public class DefaultProjectConfigTabManager implements ProjectConfigTabManager, BeanFactoryAware, InitializingBean, DisposableBean {
    private final BundleContext bundleContext;

    private volatile ListableBeanFactory factory;

    @VisibleForTesting
    ServiceTracker<ProjectConfigTab, ProjectConfigTab> serviceTracker;

    @Autowired
    public DefaultProjectConfigTabManager(BundleContext bundleContext) {
        this.bundleContext = bundleContext;
    }

    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        if (!(beanFactory instanceof ListableBeanFactory)) {
            throw new BeanInitializationException("Expecting a ListableBeanFactory.");
        }
        factory = (ListableBeanFactory) beanFactory;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        //noinspection unchecked
        serviceTracker = new ServiceTracker(bundleContext, ProjectConfigTab.class, null);
        serviceTracker.open();
    }

    public ProjectConfigTab getTabForId(String id) {
        return getTabs()
                .filter(tab -> tab.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    private Stream<ProjectConfigTab> getTabs() {
        final List<ProjectConfigTab> services = Arrays.asList(serviceTracker.getServices(new ProjectConfigTab[0]));
        final Collection<ProjectConfigTab> localBeans = factory.getBeansOfType(ProjectConfigTab.class).values();

        return Stream.concat(localBeans.stream(), services.stream());
    }

    @Override
    public void destroy() throws Exception {
        serviceTracker.close();
    }

}
