package com.atlassian.jira.projectconfig.rest.project;

import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.bc.project.ProjectAction;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.bc.projectroles.ProjectRoleService;
import com.atlassian.jira.bc.security.login.LoginInfo;
import com.atlassian.jira.bc.security.login.LoginService;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.issue.comparator.UserCachingComparator;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.rest.beans.GroupResponse;
import com.atlassian.jira.projectconfig.rest.beans.RoleActorsAddResponse;
import com.atlassian.jira.projectconfig.rest.beans.RoleMembersResponse;
import com.atlassian.jira.projectconfig.rest.beans.RolesResponse;
import com.atlassian.jira.projectconfig.rest.beans.UserResponse;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleActor;
import com.atlassian.jira.security.roles.ProjectRoleActors;
import com.atlassian.jira.security.roles.RoleActor;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.EmailFormatter;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Path("roles/{projectKey}")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class RolesResource {

    private final static String APPLICATION_ROLE_PREFIX = "JIRA ";

    private final ProjectRoleService roleService;
    private final ProjectService projectService;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final AvatarService avatarService;
    private final LoginService loginService;
    private final DateTimeFormatter dateTimeFormatter;
    private final EmailFormatter emailFormatter;
    private final ApplicationRoleManager applicationRoleManager;
    private final UserManager userManager;

    public RolesResource(ProjectRoleService roleService,
                         ProjectService projectService,
                         JiraAuthenticationContext jiraAuthenticationContext,
                         AvatarService avatarService,
                         LoginService loginService,
                         DateTimeFormatter dateTimeFormatter,
                         EmailFormatter emailFormatter,
                         ApplicationRoleManager applicationRoleManager,
                         UserManager userManager) {
        this.roleService = roleService;
        this.projectService = projectService;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.avatarService = avatarService;
        this.loginService = loginService;
        this.dateTimeFormatter = dateTimeFormatter;
        this.emailFormatter = emailFormatter;
        this.applicationRoleManager = applicationRoleManager;
        this.userManager = userManager;
    }

    @GET
    public Response getRoles(@PathParam("projectKey") String projectKey,
                             @QueryParam("roleId") Long roleId,
                             @QueryParam("query") String query,
                             @QueryParam("pageNumber") @DefaultValue("1") int pageNumber,
                             @QueryParam("pageSize") @DefaultValue("5") int pageSize) {
        ProjectService.GetProjectResult projectResult = getProjectByKey(projectKey);

        if (!projectResult.isValid()) {
            return toErrorResponse(projectResult.getErrorCollection());
        }

        final Project project = projectResult.getProject();

        final UserCachingComparator userComparator = new UserCachingComparator(jiraAuthenticationContext.getLocale());
        final SimpleErrorCollection errors = new SimpleErrorCollection();
        Optional<String> optionalQuery = Optional.ofNullable(query)
                .filter(q -> !q.isEmpty())
                .map(String::trim);

        Collection<ProjectRole> projectRoles = getProjectRolesById(Optional.ofNullable(roleId), errors);

        if (errors.hasAnyErrors()) {
            return toErrorResponse(errors);
        }

        if (pageNumber <= 0) {
            errors.addErrorMessage(jiraAuthenticationContext.getI18nHelper().getText("admin.project.roles.actions.search.invalid.page.num"));
            return toErrorResponse(errors);
        }

        if (pageSize <= 0) {
            errors.addErrorMessage(jiraAuthenticationContext.getI18nHelper().getText("admin.project.roles.actions.search.invalid.page.size"));
            return toErrorResponse(errors);
        }

        final ApplicationUser loggedInUser = jiraAuthenticationContext.getLoggedInUser();
        final int startIndex = pageSize * (pageNumber - 1);
        List<RoleMembersResponse> roleResponses = projectRoles
                .stream()
                .map(toRoleMembersResponse(loggedInUser, project, optionalQuery, startIndex, pageSize, userComparator, errors))
                .collect(Collectors.<RoleMembersResponse>toList());

        final boolean showApplicationRole = applicationRoleManager.rolesEnabled() && applicationRoleManager.getRoles().size() > 1;

        return Response.ok(new RolesResponse(roleResponses, showApplicationRole)).build();
    }

    @POST
    @Path("{roleId}")
    public Response addActorsToRole(@PathParam("projectKey") String projectKey,
                                    @PathParam("roleId") Long roleId,
                                    Map<String, String[]> actors) {
        ProjectService.GetProjectResult projectResult = getProjectByKey(projectKey);

        if (!projectResult.isValid()) {
            return toErrorResponse(projectResult.getErrorCollection());
        }

        final Project project = projectResult.getProject();

        final SimpleErrorCollection errors = new SimpleErrorCollection();
        final ProjectRole projectRole = roleService.getProjectRole(roleId, errors);

        if (errors.hasAnyErrors()) {
            return toErrorResponse(errors);
        }

        final Optional<List<String>> groups = Optional.ofNullable(actors.get("groups")).map(Arrays::asList);
        final Optional<List<String>> users = Optional.ofNullable(actors.get("users")).map(Arrays::asList);

        return addActorsToProjectRole(project, projectRole, groups, users, errors);
    }

    private ProjectService.GetProjectResult getProjectByKey(String projectIdOrKey) {
        return projectService.getProjectByKeyForAction(jiraAuthenticationContext.getLoggedInUser(),
                projectIdOrKey, ProjectAction.EDIT_PROJECT_CONFIG);
    }

    private Response toErrorResponse(ErrorCollection errors) {
        return Response
                .status(Response.Status.BAD_REQUEST)
                .entity(com.atlassian.jira.rest.api.util.ErrorCollection.of(errors))
                .build();
    }

    private Collection<ProjectRole> getProjectRolesById(Optional<Long> roleId, SimpleErrorCollection errors) {
        if (roleId.isPresent()) {
            List<ProjectRole> result = Lists.newArrayList();

            ProjectRole projectRole = roleService.getProjectRole(roleId.get(), errors);
            if (projectRole == null) {
                // JIRA ProjectRoleService doesn't handle this case
                String error = jiraAuthenticationContext.getI18nHelper().getText("project.roles.service.error.project.role.actors.null.project.role");
                errors.addErrorMessage(error);
            } else {
                result.add(projectRole);
            }

            return result;
        } else {
            return roleService.getProjectRoles(errors);
        }
    }

    private Function<ProjectRole, RoleMembersResponse> toRoleMembersResponse(ApplicationUser loggedInUser, Project project,
                                                                             Optional<String> optionalQuery,
                                                                             int startIndex, int pageSize,
                                                                             UserCachingComparator userComparator,
                                                                             SimpleErrorCollection errors) {
        return projectRole -> {
            final ProjectRoleActors projectRoleActors = roleService.getProjectRoleActors(projectRole, project, errors);

            // fetch groups and get total
            final List<RoleActor> groupRoleActors = getGroupRoleActorsByQuery(projectRoleActors, optionalQuery);
            final int totalGroups = groupRoleActors.size();

            // fetch according to startIndex and pageSize
            final List<GroupResponse> groupResults = sortAndLimitGroupResults(groupRoleActors, startIndex, pageSize);

            // fetch user and get total
            final List<ApplicationUser> users = getUsersByQuery(projectRoleActors, optionalQuery);
            final long totalUsers = users.size();

            // calculate number of users to fetch according to fetched groups
            final int remainingPageSize = pageSize - groupResults.size();

            List<UserResponse> userResults;
            if (remainingPageSize > 0) {
                // calculate start index for user list, taking into account that the current page might still show some groups
                final int userListStartIndex = startIndex + groupResults.size() - totalGroups;
                userResults = sortAndLimitUserResults(users, userComparator, userListStartIndex, remainingPageSize, loggedInUser);
            } else {
                userResults = Lists.<UserResponse>newArrayList();
            }

            return new RoleMembersResponse(projectRole.getId(), projectRole.getName(), totalGroups + totalUsers, groupResults, userResults);
        };
    }

    private List<RoleActor> getGroupRoleActorsByQuery(ProjectRoleActors projectRoleActors, Optional<String> query) {
        Stream<RoleActor> roleActorStream = projectRoleActors.getRoleActorsByType(ProjectRoleActor.GROUP_ROLE_ACTOR_TYPE).stream();

        if (query.isPresent()) {
            roleActorStream = roleActorStream.filter(roleActor -> StringUtils.containsIgnoreCase(roleActor.getParameter(), query.get()));
        }

        return roleActorStream.collect(Collectors.<RoleActor>toList());
    }

    private List<GroupResponse> sortAndLimitGroupResults(List<RoleActor> groupRoleActors, int startIndex, int maxResults) {
        return groupRoleActors
                .stream()
                .sorted((group1, group2) -> group1.getParameter().compareTo(group2.getParameter()))
                .skip(startIndex)
                .limit(maxResults)
                .map(group -> new GroupResponse(group.getId(), group.getParameter()))
                .collect(Collectors.<GroupResponse>toList());
    }

    @VisibleForTesting
    List<ApplicationUser> getUsersByQuery(ProjectRoleActors projectRoleActors, Optional<String> query) {
        Stream<ApplicationUser> userStream = projectRoleActors.getRoleActorsByType(ProjectRoleActor.USER_ROLE_ACTOR_TYPE)
                .stream()
                .flatMap(actor -> actor.getUsers().stream())
                .filter(userManager::isUserExisting);

        if (query.isPresent()) {
            String queryStr = query.get();
            userStream = userStream.filter(user -> StringUtils.containsIgnoreCase(user.getName(), queryStr)
                    || StringUtils.containsIgnoreCase(user.getDisplayName(), queryStr)
                    || StringUtils.containsIgnoreCase(user.getEmailAddress(), queryStr));
        }

        return userStream.collect(Collectors.<ApplicationUser>toList());
    }

    private List<UserResponse> sortAndLimitUserResults(List<ApplicationUser> users, UserCachingComparator userComparator,
                                                       int userListStartIndex, int remainingPageSize, ApplicationUser loggedInUser) {
        boolean isEmailVisible = emailFormatter.emailVisible(loggedInUser);

        return users.stream()
                .sorted(userComparator::compare)
                .skip(userListStartIndex)
                .limit(remainingPageSize)
                .map(toUserResponse(loggedInUser, isEmailVisible))
                .collect(Collectors.<UserResponse>toList());
    }

    private Function<ApplicationUser, UserResponse> toUserResponse(ApplicationUser loggedInUser, boolean isEmailVisible) {
        return user -> {
            final String avatarUrl = avatarService.getAvatarURL(loggedInUser, user, Avatar.Size.SMALL).toString();
            final LoginInfo loginInfo = loginService.getLoginInfo(user.getName());
            final Option<String> lastSession = Option.option(loginInfo.getLastLoginTime())
                    .map(lastLoginTime -> dateTimeFormatter.forLoggedInUser().format(new Date(lastLoginTime)));
            List<String> applicationRoleNames = Collections.emptyList();
            if (applicationRoleManager.rolesEnabled()) {
                // Get shorten the application role names, i.e. Software, Service Desk, Core instead of JIRA Software, JIRA Service Desk, etc.
                applicationRoleNames = applicationRoleManager.getRolesForUser(user).stream()
                        .map(applicationRole -> {
                            String name = applicationRole.getName();
                            if (name.indexOf(APPLICATION_ROLE_PREFIX) > -1) {
                                name = name.substring(APPLICATION_ROLE_PREFIX.length());
                            }
                            return name;
                        })
                        .collect(Collectors.<String>toList());
            }

            return UserResponse.fromApplicationUser(user, avatarUrl, lastSession, isEmailVisible, applicationRoleNames);
        };
    }

    private Response addActorsToProjectRole(Project project, ProjectRole projectRole,
                                            Optional<List<String>> groups, Optional<List<String>> users,
                                            SimpleErrorCollection errors) {
        if (!groups.isPresent() && !users.isPresent()) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        ProjectRoleActors allExistingRoleActors = roleService.getProjectRoleActors(projectRole, project, errors);
        if (errors.hasAnyErrors()) {
            return toErrorResponse(errors);
        }

        int numberOfGroupsAdded = 0;
        if (groups.isPresent()) {
            List<String> groupsToAdd = filterActorsToAdd(groups, allExistingRoleActors, ProjectRoleActor.GROUP_ROLE_ACTOR_TYPE);
            roleService.addActorsToProjectRole(groupsToAdd, projectRole, project, ProjectRoleActor.GROUP_ROLE_ACTOR_TYPE, errors);
            if (errors.hasAnyErrors()) {
                return toErrorResponse(errors);
            } else {
                numberOfGroupsAdded = groupsToAdd.size();
            }
        }

        int numberOfUsersAdded = 0;
        if (users.isPresent()) {
            List<String> usersToAdd = filterActorsToAdd(users, allExistingRoleActors, ProjectRoleActor.USER_ROLE_ACTOR_TYPE);
            roleService.addActorsToProjectRole(usersToAdd, projectRole, project, ProjectRoleActor.USER_ROLE_ACTOR_TYPE, errors);
            if (errors.hasAnyErrors()) {
                return toErrorResponse(errors);
            } else {
                numberOfUsersAdded = usersToAdd.size();
            }
        }

        return Response.ok(new RoleActorsAddResponse(numberOfGroupsAdded, numberOfUsersAdded)).build();
    }

    private List<String> filterActorsToAdd(Optional<List<String>> actorsToAdd, ProjectRoleActors allExistingRoleActors, String actorType) {
        Set<String> existingActors = allExistingRoleActors.getRoleActorsByType(actorType)
                .stream()
                .map(RoleActor::getParameter)
                .collect(Collectors.<String>toSet());

        return actorsToAdd.get()
                .stream()
                .filter(actorToAdd -> !existingActors.contains(actorToAdd))
                .collect(Collectors.<String>toList());
    }
}
