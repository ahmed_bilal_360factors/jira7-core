package com.atlassian.jira.projectconfig.tab;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.projectconfig.util.VelocityContextFactory;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.webresource.WebResourceManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Previously named PeopleTab.
 *
 * @since v4.4
 */
@Component
public class RolesTab extends WebPanelTab {
    private static final String OLD_ROLES_SCREEN_FEATURE_KEY = "jira.project.config.old.roles.screen";

    private final FeatureManager featureManager;

    @Autowired
    public RolesTab(final WebInterfaceManager webInterfaceManager,
                    final VelocityContextFactory factory,
                    final FeatureManager featureManager) {
        // empty linkId because we will override getLinkId to return the correct value based on dark feature
        super(webInterfaceManager, factory, "roles", "");
        this.featureManager = featureManager;
    }

    @Override
    public String getTitle(ProjectConfigTabRenderContext context) {
        if (featureManager.isEnabled(OLD_ROLES_SCREEN_FEATURE_KEY)) {
            return context.getI18NHelper().getText("admin.project.people.title", context.getProject().getName());
        } else {
            return context.getI18NHelper().getText("admin.project.roles.title", context.getProject().getName());
        }
    }

    @Override
    public void addResourceForProject(ProjectConfigTabRenderContext renderContext) {
        if (featureManager.isEnabled(OLD_ROLES_SCREEN_FEATURE_KEY)) {
            WebResourceManager manager = renderContext.getResourceManager();
            manager.requireResource("com.atlassian.jira.jira-project-config-plugin:project-config-people");
        } else {
            renderContext
                    .getPageBuilderService()
                    .assembler()
                    .resources()
                    .requireWebResource("com.atlassian.jira.jira-project-config-plugin:project-config-roles-init");
        }
    }

    @Override
    public String getLinkId() {
        if (featureManager.isEnabled(OLD_ROLES_SCREEN_FEATURE_KEY)) {
            return "view_project_people";
        } else {
            return "view_project_roles";
        }
    }
}
