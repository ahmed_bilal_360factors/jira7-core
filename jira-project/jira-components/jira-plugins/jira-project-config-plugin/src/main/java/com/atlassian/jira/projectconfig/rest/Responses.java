package com.atlassian.jira.projectconfig.rest;

import javax.ws.rs.core.Response;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.util.ErrorCollection;

import static com.atlassian.jira.rest.api.http.CacheControl.never;

/**
 * Common utility for for REST responses.
 *
 * @since v6.2
 */
public final class Responses {
    private Responses() {
    }

    public static Response ok(final Object bean) {
        return build(Response.ok(bean));
    }

    public static Response forOutcome(ServiceOutcome<?> outcome) {
        if (outcome.isValid()) {
            return Responses.ok(outcome.get());
        } else {
            return Responses.forCollection(outcome.getErrorCollection());
        }
    }

    private static Response forCollection(ErrorCollection collection) {
        return forCollection(collection, ErrorCollection.Reason.SERVER_ERROR);
    }

    private static Response forCollection(ErrorCollection collection, ErrorCollection.Reason defaultReason) {
        if (!collection.hasAnyErrors()) {
            throw new IllegalArgumentException("collection has no errors.");
        }
        if (defaultReason == null) {
            throw new IllegalArgumentException("defaultReason is null");
        }

        ErrorCollection.Reason worstReason = ErrorCollection.Reason.getWorstReason(collection.getReasons());
        if (worstReason == null) {
            worstReason = defaultReason;
        }
        return build(Response.status(worstReason.getHttpStatusCode())
                .entity(com.atlassian.jira.rest.api.util.ErrorCollection.of(collection)));
    }

    private static Response build(Response.ResponseBuilder builder) {
        return builder.cacheControl(never()).build();
    }
}
