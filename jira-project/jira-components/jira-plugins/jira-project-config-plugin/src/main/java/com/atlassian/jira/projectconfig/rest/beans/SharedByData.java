package com.atlassian.jira.projectconfig.rest.beans;

import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.projectconfig.shared.SharedIssueTypeWorkflowData;
import org.codehaus.jackson.annotate.JsonAutoDetect;

import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

/**
 * A class used by beans that want to pass around {@link com.atlassian.jira.project.Project} and
 * {@link com.atlassian.jira.issue.issuetype.IssueType} sharing information.
 *
 * @since 7.3
 */
@JsonAutoDetect
public class SharedByData {
    private final List<SimpleRestProject> sharedWithProjects;

    private final List<String> sharedWithIssueTypes;

    private final long totalProjectsCount;

    public SharedByData(final List<SimpleRestProject> allowedProjects, final List<String> issueTypes,
                        final long totalProjectsCount) {
        this.sharedWithProjects = allowedProjects;
        this.sharedWithIssueTypes = issueTypes;
        this.totalProjectsCount = totalProjectsCount;
    }

    public SharedByData(final List<SimpleRestProject> allowedProjects, final List<String> issueTypes) {
        this(allowedProjects, issueTypes, allowedProjects.size());
    }

    public SharedByData(final List<SimpleRestProject> projects, final long totalProjectsCount) {
        this(projects, emptyList(), totalProjectsCount);
    }

    public SharedByData(final List<SimpleRestProject> projects) {
        this(projects, projects.size());
    }

    public SharedByData(final SharedIssueTypeWorkflowData sharedIssueTypeWorkflowData) {
        this.sharedWithProjects = sharedIssueTypeWorkflowData.getProjects().stream()
                .map(SimpleRestProject::fullProject)
                .collect(toList());
        this.sharedWithIssueTypes = sharedIssueTypeWorkflowData.getIssueTypes().stream()
                .map(IssueType::getId)
                .collect(toList());
        this.totalProjectsCount = sharedIssueTypeWorkflowData.getTotalProjectsCount();
    }

    public List<SimpleRestProject> getSharedWithProjects() {
        return sharedWithProjects;
    }

    public List<String> getSharedWithIssueTypes() {
        return sharedWithIssueTypes;
    }

    public long getHiddenProjectsCount() {
        return totalProjectsCount - sharedWithProjects.size();
    }

    public long getTotalProjectsCount() {
        return totalProjectsCount;
    }
}
