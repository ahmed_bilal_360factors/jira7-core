package com.atlassian.jira.projectconfig.util;

import com.atlassian.jira.util.JiraUrlCodec;

import org.springframework.stereotype.Component;

/**
 * @since v4.4
 */
@Component
public class DefaultUrlEncoder implements UrlEncoder {
    public String encode(String value) {
        return JiraUrlCodec.encode(value);
    }
}
