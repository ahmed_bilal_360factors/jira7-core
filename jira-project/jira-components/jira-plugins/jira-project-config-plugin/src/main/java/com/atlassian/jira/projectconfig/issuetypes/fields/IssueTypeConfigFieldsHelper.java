package com.atlassian.jira.projectconfig.issuetypes.fields;

import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.projectconfig.beans.ProjectContext;
import com.atlassian.jira.projectconfig.shared.SharedIssueTypeWorkflowData;

import javax.annotation.Nonnull;

/**
 * Service for dealing with the fields of the view screen of a project.
 *
 * @since v6.2
 */
public interface IssueTypeConfigFieldsHelper {
    /**
     * Return the fields {@link com.atlassian.jira.issue.fields.screen.FieldScreen}, shared {@link com.atlassian.jira.project.Project}s
     * and shared {@link com.atlassian.jira.issue.issuetype.IssueType}s for the passed
     * {@link com.atlassian.jira.projectconfig.beans.ProjectContext}.
     *
     * @return The {@link IssueTypeConfigFieldsHelper.FieldsResult} of the lookup.
     */
    @Nonnull
    IssueTypeConfigFieldsHelper.FieldsResult getFieldsData(@Nonnull ProjectContext context);

    /**
     * A class that contains the result of a fields lookup.
     */
    public class FieldsResult {
        private final FieldScreen fieldScreen;
        private final SharedIssueTypeWorkflowData sharedBy;

        public FieldsResult(final FieldScreen fieldScreen, final SharedIssueTypeWorkflowData sharedBy) {
            this.fieldScreen = fieldScreen;
            this.sharedBy = sharedBy;
        }

        public FieldScreen getFieldScreen() {
            return fieldScreen;
        }

        public SharedIssueTypeWorkflowData getSharedBy() {
            return sharedBy;
        }
    }
}
