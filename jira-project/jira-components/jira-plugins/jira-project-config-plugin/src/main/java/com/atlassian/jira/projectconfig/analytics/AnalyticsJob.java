package com.atlassian.jira.projectconfig.analytics;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.workflow.DefaultWorkflowService;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.projectconfig.event.WorkflowsStatisticEvent;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.WorkflowSchemeManager;
import com.atlassian.scheduler.JobRunner;
import com.atlassian.scheduler.JobRunnerRequest;
import com.atlassian.scheduler.JobRunnerResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

public class AnalyticsJob implements JobRunner {

    private static final Logger LOG = LoggerFactory.getLogger(AnalyticsJob.class);

    private final ProjectManager projectManager;
    private final WorkflowSchemeManager workflowSchemeManager;
    private final WorkflowManager workflowManager;
    private final EventPublisher eventPublisher;

    public AnalyticsJob(ProjectManager projectManager,
                        WorkflowSchemeManager workflowSchemeManager,
                        WorkflowManager workflowManager,
                        EventPublisher eventPublisher) {
        this.projectManager = projectManager;
        this.workflowSchemeManager = workflowSchemeManager;
        this.workflowManager = workflowManager;
        this.eventPublisher = eventPublisher;
    }

    @Nullable
    @Override
    public JobRunnerResponse runJob(JobRunnerRequest request) {
        LOG.trace("Counting workflow statistics");

        final Map<String, Long> workflowProjectOccurrence = projectManager.getProjects().stream()
                .map(p -> {
                    Map<String, String> workflowMap = DefaultWorkflowService.ADD_DEFAULT_WORKFLOW_IF_MISSING(workflowSchemeManager.getWorkflowMap(p));
                    final String defaultWorkflowName = workflowMap.get(null);
                    return p.getIssueTypes().stream()
                            .map(it -> workflowMap.get(it.getId()))
                            .map(wf -> isNull(wf) ? defaultWorkflowName : wf)
                            .distinct()
                            .collect(Collectors.toList());

                })
                .flatMap(Collection::stream)
                .collect(Collectors.groupingBy(wf -> wf,
                    Collectors.counting()));

        final long usedWorkflowsCount = workflowProjectOccurrence.size();
        final long isolatedWorkflowsCount = workflowProjectOccurrence.entrySet().stream().filter(entry -> entry.getValue() == 1).count();
        final long sharedWorkflowsCount = usedWorkflowsCount - isolatedWorkflowsCount;
        final long inactiveWorkflowsCount = workflowManager.getWorkflows().size() - workflowManager.getActiveWorkflows().size();
        final long totalWorkflowsCount = inactiveWorkflowsCount + usedWorkflowsCount;

        eventPublisher.publish(new WorkflowsStatisticEvent(totalWorkflowsCount, isolatedWorkflowsCount, sharedWorkflowsCount, inactiveWorkflowsCount));
        LOG.trace("Workflows statistics send");

        return JobRunnerResponse.success();
    }
}
