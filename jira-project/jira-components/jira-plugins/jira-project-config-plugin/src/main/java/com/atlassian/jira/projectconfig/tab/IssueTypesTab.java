package com.atlassian.jira.projectconfig.tab;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.util.VelocityContextFactory;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.model.WebPanel;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @since v4.4
 */
@Component
public class IssueTypesTab implements ProjectConfigTab {
    private final WebInterfaceManager webInterfaceManager;
    private final VelocityContextFactory contextFactory;

    /*
     * Matches:
     *  project-config/PROJECT/issuetypes or
     *  project-config/PROJECT/issuetypes/ISSUETYPE
     *  project-config/PROJECT/issuetypes/ISSUETYPE/TAB
     */
    private final static Pattern URL_PARSER = Pattern.compile("project-config/+"
            + "([^/]+)/+issuetypes/*" //Matches  /PROJECT/issuetypes
            + "(?:(?<=/)(\\d+)/*"       //Matches ISSUETYPE/*
            + ")?$");

    public static final String URL = "url";
    public static final String ERROR = "error";

    @Autowired
    public IssueTypesTab(final WebInterfaceManager webInterfaceManager, VelocityContextFactory contextFactory) {
        this.webInterfaceManager = webInterfaceManager;
        this.contextFactory = contextFactory;
    }

    @Override
    public String getId() {
        return "issuetypes";
    }

    @Override
    public String getLinkId() {
        return "view_project_issuetypes";
    }

    @Override
    public String getTab(final ProjectConfigTabRenderContext context) {
        if (isSummaryRequest(context.getRequest().getRequestURI())) {
            return renderSummary(context.getProject());
        }
        return "";
    }

    @Override
    public String getTitle(ProjectConfigTabRenderContext context) {
        return context.getI18NHelper().getText("admin.project.issuetypes.title", context.getProject().getName());
    }

    @Override
    public void addResourceForProject(final ProjectConfigTabRenderContext context) {
        context.getResourceManager()
                .requireResource("com.atlassian.jira.jira-project-config-plugin:issuetypes-tab");
    }

    private String renderSummary(Project project) {
        final Map<String, Object> ourContext = ImmutableMap.<String, Object>of(WebPanelTab.CURRENT_PROJECT, project);
        final Map<String, Object> context = contextFactory.createVelocityContext(ourContext);

        List<WebPanel> panels = webInterfaceManager.getDisplayableWebPanels(
                "tabs.admin.projectconfig.issuetypes",
                contextFactory.createVelocityContext(ourContext));
        WebPanel panel = Iterables.getFirst(panels, null);
        if (panel == null) {
            return "";
        } else {
            return panel.getHtml(context);
        }
    }

    private boolean isSummaryRequest(String uri) {
        final Matcher matcher = URL_PARSER.matcher(uri);
        if (matcher.find()) {
            if (matcher.group(2) == null) {
                return true;
            }
        }

        return false;
    }
}
