package com.atlassian.jira.projectconfig.contextproviders;

import com.atlassian.gzipfilter.org.apache.commons.lang.builder.ToStringBuilder;
import com.atlassian.gzipfilter.org.apache.commons.lang.builder.ToStringStyle;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.plugin.webfragment.CacheableContextProvider;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.beans.NamedDefault;
import com.atlassian.jira.projectconfig.order.OrderFactory;
import com.atlassian.jira.projectconfig.shared.SharedEntitiesHelper;
import com.atlassian.jira.projectconfig.shared.SharedIssueTypeWorkflowData;
import com.atlassian.jira.projectconfig.util.TabUrlFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.UrlBuilder;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.WorkflowSchemeManager;
import com.atlassian.plugin.PluginParseException;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.jira.projectconfig.shared.SharedEntitiesUtils.getOrDefaultWorkflowName;
import static com.atlassian.jira.workflow.JiraWorkflow.DEFAULT_WORKFLOW_NAME;

/**
 * Produces the velocity context for the workflow summary panel.
 *
 * @since v4.4
 */
public class WorkflowSummaryPanelContextProvider implements CacheableContextProvider {
    private final ContextProviderUtils utils;
    private final WorkflowSchemeManager workflowSchemeManager;
    private final JiraAuthenticationContext authenticationContext;
    private final TabUrlFactory factory;
    private final OrderFactory orderFactory;
    private final WorkflowManager workflowManager;
    private final SharedEntitiesHelper sharedHelper;

    public WorkflowSummaryPanelContextProvider(ContextProviderUtils utils, WorkflowSchemeManager workflowSchemeManager,
                                               JiraAuthenticationContext authenticationContext, TabUrlFactory factory, OrderFactory orderFactory,
                                               WorkflowManager workflowManager, SharedEntitiesHelper sharedHelper) {
        this.utils = utils;
        this.workflowSchemeManager = workflowSchemeManager;
        this.authenticationContext = authenticationContext;
        this.factory = factory;
        this.orderFactory = orderFactory;
        this.workflowManager = workflowManager;
        this.sharedHelper = sharedHelper;
    }

    public void init(Map<String, String> params) throws PluginParseException {
        //Nothing to do.
    }

    public Map<String, Object> getContextMap(Map<String, Object> context) {
        Project project = utils.getProject();
        MapBuilder<String, Object> builder = MapBuilder.newBuilder(context);
        builder.add("project", project.getId());
        addWorkflows(builder, project);
        addSchemeData(builder, project);
        return builder.toMap();
    }

    private MapBuilder<String, Object> addWorkflows(final MapBuilder<String, Object> builder, final Project project) {
        final Map<String, String> issueTypeToWorkflowMap = workflowSchemeManager.getWorkflowMap(project);
        final String defaultWorkflow = getOrDefaultWorkflowName(issueTypeToWorkflowMap, null, DEFAULT_WORKFLOW_NAME);

        final Set<String> addedWorkflows = new HashSet<>();
        final List<SimpleWorkflow> ordered = new ArrayList<>();

        for (IssueType issueType : project.getIssueTypes()) {
            final String workflowName = getOrDefaultWorkflowName(issueTypeToWorkflowMap, issueType.getId(), defaultWorkflow);
            if (!addedWorkflows.add(workflowName)) continue;

            final SharedIssueTypeWorkflowData sharedBy = sharedHelper.getSharedData(project, issueType, workflowName);
            final JiraWorkflow jiraWorkflow = workflowManager.getWorkflow(workflowName);
            final String workflowUrl = getWorkflowUrl(workflowName);
            final boolean isDefaultWorkflow = defaultWorkflow.equals(workflowName);

            ordered.add(new SimpleWorkflow(jiraWorkflow, workflowUrl, isDefaultWorkflow, sharedBy));
        }

        Collections.sort(ordered, orderFactory.createNamedDefaultComparator());

        return builder.add("workflows", ordered);
    }

    private MapBuilder<String, Object> addSchemeData(MapBuilder<String, Object> builder, Project project) {
        try {
            GenericValue workflowScheme = workflowSchemeManager.getWorkflowScheme(project.getGenericValue());
            if (workflowScheme != null) {
                builder.add("schemeName", workflowScheme.getString("name"));
                builder.add("schemeDescription", workflowScheme.getString("description"));
            } else {
                builder.add("schemeName", authenticationContext.getI18nHelper().getText("admin.schemes.workflows.default"));
            }
            builder.add("schemeLink", factory.forWorkflows());
            builder.add("schemeDefault", workflowSchemeManager.isUsingDefaultScheme(project));
        } catch (GenericEntityException e) {
            builder.add("error", true);
        }
        return builder;
    }

    String getWorkflowUrl(String workflowName) {
        return createUrlBuilder("/secure/admin/workflows/ViewWorkflowSteps.jspa?workflowMode=live")
                .addParameter("workflowName", workflowName).asUrlString();
    }

    private UrlBuilder createUrlBuilder(final String operation) {
        return utils.createUrlBuilder(operation);
    }

    @Override
    public String getUniqueContextKey(Map<String, Object> context) {
        return getClass().getName();
    }

    public static class SimpleWorkflow implements NamedDefault {
        private final String name;
        private final String urlName;
        private final String description;
        private final boolean isDefault;
        private final boolean isSystem;
        private final boolean isJiraDefault;
        private final String displayName;
        private final SharedIssueTypeWorkflowData sharedBy;

        SimpleWorkflow(final String name, final String urlName, final String description, final boolean isDefault,
                       final boolean system, boolean isJiraDefault, String displayName, final SharedIssueTypeWorkflowData sharedBy) {
            this.description = description;
            this.name = name;
            this.urlName = urlName;
            this.isDefault = isDefault;
            this.isSystem = system;
            this.isJiraDefault = isJiraDefault;
            this.displayName = displayName;
            this.sharedBy = sharedBy;
        }

        SimpleWorkflow(JiraWorkflow workflow, final String urlName, final boolean isDefault, final SharedIssueTypeWorkflowData sharedBy) {
            this.name = workflow.getName();
            this.urlName = urlName;
            this.description = workflow.getDescription();
            this.isDefault = isDefault;
            this.isSystem = workflow.isSystemWorkflow();
            this.isJiraDefault = workflow.isDefault();
            this.displayName = workflow.getDisplayName();
            this.sharedBy = sharedBy;
        }

        public String getName() {
            return name;
        }

        public String getUrl() {
            return urlName;
        }

        public String getDescription() {
            return description;
        }

        public boolean isDefault() {
            return isDefault;
        }

        public boolean isSystem() {
            return isSystem;
        }

        public String getDisplayName() {
            return displayName;
        }

        public SharedIssueTypeWorkflowData getSharedBy() {
            return sharedBy;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            SimpleWorkflow that = (SimpleWorkflow) o;

            if (isDefault != that.isDefault) {
                return false;
            }
            if (isJiraDefault != that.isJiraDefault) {
                return false;
            }
            if (isSystem != that.isSystem) {
                return false;
            }
            if (description != null ? !description.equals(that.description) : that.description != null) {
                return false;
            }
            if (displayName != null ? !displayName.equals(that.displayName) : that.displayName != null) {
                return false;
            }
            if (name != null ? !name.equals(that.name) : that.name != null) {
                return false;
            }
            if (urlName != null ? !urlName.equals(that.urlName) : that.urlName != null) {
                return false;
            }
            if (sharedBy != null ? !sharedBy.equals(that.sharedBy) : that.sharedBy != null) {
                return false;
            }

            return true;
        }

        @Override
        public int hashCode() {
            int result = name != null ? name.hashCode() : 0;
            result = 31 * result + (urlName != null ? urlName.hashCode() : 0);
            result = 31 * result + (description != null ? description.hashCode() : 0);
            result = 31 * result + (isDefault ? 1 : 0);
            result = 31 * result + (isSystem ? 1 : 0);
            result = 31 * result + (isJiraDefault ? 1 : 0);
            result = 31 * result + (displayName != null ? displayName.hashCode() : 0);
            result = 31 * result + (sharedBy != null ? sharedBy.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        }
    }
}
