package com.atlassian.jira.projectconfig.rest.beans;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonAutoDetect
public class GroupResponse {

    @JsonProperty
    private Long id;

    @JsonProperty
    private String name;

    private GroupResponse() {
    }

    public GroupResponse(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
