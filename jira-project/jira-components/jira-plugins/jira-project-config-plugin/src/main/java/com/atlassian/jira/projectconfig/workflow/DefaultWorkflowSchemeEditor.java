package com.atlassian.jira.projectconfig.workflow;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.order.OrderFactory;
import com.atlassian.jira.workflow.DraftWorkflowScheme;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @since v5.2
 */
@Component
public class DefaultWorkflowSchemeEditor implements DraftWorkflowSchemeEditor {
    private final OrderFactory orderFactory;

    @Autowired
    public DefaultWorkflowSchemeEditor(OrderFactory orderFactory) {
        this.orderFactory = orderFactory;
    }

    public DraftWorkflowScheme assign(DraftWorkflowScheme workflowScheme,
                                      Iterable<String> issueTypesToAssign, String newWorkflow, Project project) {
        Set<String> types = Sets.newHashSet(issueTypesToAssign);

        //workflow -> issueType
        Multimap<String, String> invertedWorkflow = HashMultimap.create();
        for (IssueType issueType : project.getIssueTypes()) {
            String issueTypeId = issueType.getId();

            if (types.contains(issueTypeId)) {
                invertedWorkflow.put(newWorkflow, issueTypeId);
            } else {
                String actualWorkflow = workflowScheme.getActualWorkflow(issueTypeId);
                invertedWorkflow.put(actualWorkflow, issueTypeId);
            }
        }

        String defaultWorkflow = workflowScheme.getActualDefaultWorkflow();
        if (!invertedWorkflow.containsKey(defaultWorkflow)) {
            Ordering<String> ordering = Ordering.from(orderFactory.createStringComparator());
            defaultWorkflow = ordering.min(invertedWorkflow.keySet());
        }

        invertedWorkflow.removeAll(defaultWorkflow);

        final DraftWorkflowScheme.Builder builder = workflowScheme.builder().clearMappings().setDefaultWorkflow(defaultWorkflow);
        for (Map.Entry<String, String> mapping : invertedWorkflow.entries()) {
            builder.setMapping(mapping.getValue(), mapping.getKey());
        }
        return builder.build();
    }

    @Override
    public DraftWorkflowScheme delete(DraftWorkflowScheme workflowScheme, String deleteWorkflow, Project project) {
        //workflow -> issueType
        Multimap<String, String> invertedWorkflow = HashMultimap.create();
        for (IssueType issueType : project.getIssueTypes()) {
            String issueTypeId = issueType.getId();

            String actualWorkflow = workflowScheme.getActualWorkflow(issueTypeId);
            invertedWorkflow.put(actualWorkflow, issueTypeId);
        }

        invertedWorkflow.removeAll(deleteWorkflow);

        if (invertedWorkflow.isEmpty()) {
            return workflowScheme.builder().setMappings(Collections.<String, String>emptyMap()).build();
        }

        String defaultWorkflow = workflowScheme.getActualDefaultWorkflow();
        if (defaultWorkflow.equals(deleteWorkflow)) {
            Ordering<String> ordering = Ordering.from(orderFactory.createStringComparator());
            defaultWorkflow = ordering.min(invertedWorkflow.keySet());
        }

        invertedWorkflow.removeAll(defaultWorkflow);
        final DraftWorkflowScheme.Builder builder = workflowScheme.builder().clearMappings().setDefaultWorkflow(defaultWorkflow);
        for (Map.Entry<String, String> mapping : invertedWorkflow.entries()) {
            builder.setMapping(mapping.getValue(), mapping.getKey());
        }
        return builder.build();
    }
}

