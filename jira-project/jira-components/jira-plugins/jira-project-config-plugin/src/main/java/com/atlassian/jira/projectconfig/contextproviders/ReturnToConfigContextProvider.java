package com.atlassian.jira.projectconfig.contextproviders;

import com.atlassian.jira.bc.project.ProjectAction;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserPropertyManager;
import com.atlassian.jira.util.JiraVelocityUtils;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.util.velocity.VelocityRequestContext;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.util.velocity.VelocityRequestSession;
import com.atlassian.jira.web.SessionKeys;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;
import com.atlassian.plugin.web.ContextProvider;
import com.opensymphony.module.propertyset.PropertySet;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Nullable;
import java.util.Map;

/**
 * Context Provider for the "Back to Project Config link"
 *
 * @since v4.4
 */
public class ReturnToConfigContextProvider implements ContextProvider, Condition {
    static final String VM_CONTEXT_KEY_PROJECT = "project";
    static final String VM_CONTEXT_KEY_BACK_TO_PROJECT_PATH = "backToProjectPath";

    private final JiraAuthenticationContext authenticationContext;
    private final ProjectService projectService;
    private final UserPropertyManager userPropertyManager;

    public ReturnToConfigContextProvider(final JiraAuthenticationContext authenticationContext,
                                         final ProjectService projectService,
                                         final UserPropertyManager userPropertyManager) {
        this.authenticationContext = authenticationContext;
        this.projectService = projectService;
        this.userPropertyManager = userPropertyManager;
    }

    @Override
    public void init(final Map<String, String> params) throws PluginParseException {
    }

    @Override
    public boolean shouldDisplay(final Map<String, Object> context) {
        return StringUtils.isNotBlank(getCurrentAdminProject());
    }

    @Override
    public Map<String, Object> getContextMap(final Map<String, Object> context) {
        final MapBuilder<String, Object> contextBuilder = MapBuilder.newBuilder(context);

        final String projectKey = getCurrentAdminProject();

        if (StringUtils.isNotBlank(projectKey)) {
            final ProjectService.GetProjectResult projectResult = canEditProject(projectKey);
            if (projectResult.isValid()) {
                final Project project = projectResult.getProject();
                contextBuilder.add(VM_CONTEXT_KEY_PROJECT, project);
            }
        }

        // this path is for returning to the project config page when navigating to other page
        // it's set by either PanelServlet or by other project config related plugins
        contextBuilder.add(VM_CONTEXT_KEY_BACK_TO_PROJECT_PATH, getUserPropertySet().getString(SessionKeys.CURRENT_ADMIN_PROJECT_RETURN_URL));

        return JiraVelocityUtils.getDefaultVelocityParams(contextBuilder.toMap(), authenticationContext);
    }

    private PropertySet getUserPropertySet() {
        ApplicationUser user = authenticationContext.getLoggedInUser();
        return userPropertyManager.getPropertySet(user);
    }

    @Nullable
    private String getCurrentAdminProject() {
        return getUserPropertySet().getString(SessionKeys.CURRENT_ADMIN_PROJECT);
    }

    private ProjectService.GetProjectResult canEditProject(final String projectKey) {
        return projectService.getProjectByKeyForAction(authenticationContext.getLoggedInUser(),
                projectKey,
                ProjectAction.EDIT_PROJECT_CONFIG);
    }
}
