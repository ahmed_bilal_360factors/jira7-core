package com.atlassian.jira.projectconfig.discover;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.workflow.DefaultWorkflowService;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.help.HelpUrls;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.discover.events.EditWorkflowDiscoverShow;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.preferences.ExtendedPreferences;
import com.atlassian.jira.user.preferences.UserPreferencesManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.collect.ImmutableMap;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

import static com.atlassian.jira.application.ApplicationKeys.CORE;
import static com.atlassian.jira.application.ApplicationKeys.SERVICE_DESK;
import static com.atlassian.jira.application.ApplicationKeys.SOFTWARE;

@Component
public class EditWorkflowDiscoverHelper {
    private static final Logger log = Logger.getLogger(EditWorkflowDiscoverHelper.class);

    private static final Map<String, ApplicationKey> PROJECT_TYPE_TO_APPKEY_MAP = ImmutableMap.of(
            "software", SOFTWARE,
            "service_desk", SERVICE_DESK);
    public static final String EDIT_WORKFLOW_DISCOVER_DIALOG_SHOWN_USER_PROPERTY = "jira.projectconfig.workflow.edit.summary.discover.dialog";
    public static final String MARK_SEEN = "seen";
    public static final String PROJECT_CONFIG_DISCOVERY_HELP_URL = "project.config.discovery.workflows";

    private final FeatureManager featureManager;
    private final HelpUrls helpUrls;
    private final UserPreferencesManager userPreferencesManager;
    private final EventPublisher eventPublisher;

    @Autowired
    public EditWorkflowDiscoverHelper(final FeatureManager featureManager,
                                      @ComponentImport final HelpUrls helpUrls,
                                      @ComponentImport final UserPreferencesManager userPreferencesManager,
                                      @ComponentImport final EventPublisher eventPublisher) {
        this.featureManager = featureManager;
        this.helpUrls = helpUrls;
        this.userPreferencesManager = userPreferencesManager;
        this.eventPublisher = eventPublisher;
    }

    public boolean shouldShowEditWorkflowDiscover(final ApplicationUser user) {
        if (!featureManager.isEnabled(DefaultWorkflowService.PROJECT_LEVEL_ADMIN_EDIT_WORKFLOW_FEATURE)) {
            return false;
        }
        final ExtendedPreferences preferences = this.userPreferencesManager.getExtendedPreferences(user);
        final boolean shouldShow = !MARK_SEEN.equals(preferences.getString(EDIT_WORKFLOW_DISCOVER_DIALOG_SHOWN_USER_PROPERTY));
        if (shouldShow) {
            eventPublisher.publish(new EditWorkflowDiscoverShow());
        }
        return shouldShow;
    }

    public String getLearnMoreUrl(final Project project) {
        return helpUrls.getUrlForApplication(getApplicationKey(project), PROJECT_CONFIG_DISCOVERY_HELP_URL).getUrl();
    }

    private ApplicationKey getApplicationKey(final Project project) {
        final String projectTypeKey = project.getProjectTypeKey().getKey();
        return PROJECT_TYPE_TO_APPKEY_MAP.getOrDefault(projectTypeKey, CORE);
    }
}
