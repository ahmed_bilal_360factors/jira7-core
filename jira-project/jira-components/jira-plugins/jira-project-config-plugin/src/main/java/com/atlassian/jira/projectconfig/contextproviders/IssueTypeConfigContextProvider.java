package com.atlassian.jira.projectconfig.contextproviders;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.plugin.webfragment.CacheableContextProvider;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.order.OrderFactory;
import com.atlassian.jira.util.collect.CompositeMap;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * Context Provider for rendering the list of Issue Types in the project admin sidebar.
 *
 * @since v6.2
 */
public class IssueTypeConfigContextProvider implements CacheableContextProvider {
    private static final String ISSUE_TYPES_DATA_KEY = "issueTypesData";

    private final OrderFactory orderFactory;
    private ObjectMapper objectMapper;

    public IssueTypeConfigContextProvider(final OrderFactory orderFactory) {
        this.orderFactory = orderFactory;
    }

    @Override
    public String getUniqueContextKey(final Map<String, Object> context) {
        return getClass().getName();
    }

    @Override
    public void init(final Map<String, String> params) {
    }

    @Override
    public Map<String, Object> getContextMap(final Map<String, Object> context) {
        final Project project = (Project) context.get("project");
        final List<IssueTypeConfigData> issueTypeConfigDatas = getIssueTypeLinks(project);

        try {
            final ImmutableMap<String, Object> newContext = ImmutableMap.<String, Object>of(
                    ISSUE_TYPES_DATA_KEY, getObjectMapper().writeValueAsString(issueTypeConfigDatas));
            return CompositeMap.of(newContext, context);
        } catch (IOException e) {
            throw new RuntimeException("An error occurred while parsing the issue types to a JSON string", e);
        }
    }

    private List<IssueTypeConfigData> getIssueTypeLinks(Project project) {
        final List<IssueTypeConfigData> issueTypeConfigDatas = Lists.newArrayList();
        for (IssueType issueType : project.getIssueTypes()) {
            issueTypeConfigDatas.add(new IssueTypeConfigData(issueType));
        }
        return Ordering.from(orderFactory.createStringComparator()).onResultOf(IssueTypeConfigData.GET_NAME_FUNC)
                .immutableSortedCopy(issueTypeConfigDatas);
    }

    private ObjectMapper getObjectMapper() {
        if (objectMapper == null) {
            objectMapper = new ObjectMapper();
        }
        return objectMapper;
    }

    public static class IssueTypeConfigData {
        private static Function<IssueTypeConfigData, String> GET_NAME_FUNC = new Function<IssueTypeConfigData, String>() {
            @Override
            public String apply(final IssueTypeConfigData input) {
                return input.getName();
            }
        };

        @JsonProperty
        private String id;

        @JsonProperty
        private String name;

        public IssueTypeConfigData(IssueType issueType) {
            this.id = issueType.getId();
            this.name = issueType.getNameTranslation();
        }

        public String getId() {
            return id;
        }

        public void setId(final String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(final String name) {
            this.name = name;
        }
    }
}
