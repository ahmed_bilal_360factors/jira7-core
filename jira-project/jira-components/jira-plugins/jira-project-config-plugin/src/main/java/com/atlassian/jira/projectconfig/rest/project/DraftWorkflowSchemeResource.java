package com.atlassian.jira.projectconfig.rest.project;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.project.ProjectAction;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.bc.workflow.WorkflowSchemeService;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.rest.WorkflowSchemeRestHelper;
import com.atlassian.jira.projectconfig.workflow.DraftWorkflowSchemeEditor;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.task.TaskDescriptor;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.workflow.AssignableWorkflowScheme;
import com.atlassian.jira.workflow.DraftWorkflowScheme;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.migration.WorkflowSchemeMigrationTaskAccessor;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Set;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.FORBIDDEN;
import static org.apache.commons.lang3.StringUtils.stripToNull;

@Path("draftworkflowscheme")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class DraftWorkflowSchemeResource {
    private final DraftWorkflowSchemeEditor draftWorkflowSchemeEditor;
    private final JiraAuthenticationContext authContext;
    private final ProjectService projectService;
    private final WorkflowSchemeService workflowSchemeService;
    private final WorkflowSchemeRestHelper helper;
    private final WorkflowManager workflowManager;
    private final PermissionManager permissionManager;
    private final WorkflowSchemeMigrationTaskAccessor taskAccessor;

    public DraftWorkflowSchemeResource(JiraAuthenticationContext authContext,
                                       ProjectService projectService, WorkflowSchemeService workflowSchemeService,
                                       WorkflowSchemeRestHelper helper, WorkflowManager workflowManager,
                                       DraftWorkflowSchemeEditor draftWorkflowSchemeEditor,
                                       PermissionManager permissionManager, WorkflowSchemeMigrationTaskAccessor taskAccessor) {

        this.authContext = authContext;
        this.projectService = projectService;
        this.workflowSchemeService = workflowSchemeService;
        this.helper = helper;
        this.workflowManager = workflowManager;
        this.draftWorkflowSchemeEditor = draftWorkflowSchemeEditor;
        this.permissionManager = permissionManager;
        this.taskAccessor = taskAccessor;
    }

    @WebSudoRequired
    @PUT
    @Path("{projectKey}")
    public Response assignIssueTypes(@PathParam("projectKey") String projectKey, final AssignIssueTypesRequest request) {
        projectKey = stripToNull(projectKey);
        if (projectKey == null || !request.isValid()) {
            return WorkflowSchemeRestHelper.badRequest();
        }

        return changeWorkflowScheme(new ModifyOperation() {
            @Override
            public Response validate(DraftWorkflowScheme scheme, Project project, JiraWorkflow workflow) {
                // Return an error if the project doesn't have some of the input issue types.
                Set<String> projectIssueTypeIds = Sets.newHashSet(Iterables.transform(project.getIssueTypes(), IssueType::getId));

                for (String issueTypeId : request.issueTypes) {
                    if (!projectIssueTypeIds.contains(issueTypeId)) {
                        return WorkflowSchemeRestHelper.createErrorResponse(BAD_REQUEST,
                                "admin.project.workflows.bad.issue.type.for.project",
                                authContext.getI18nHelper());
                    }
                }
                return null;
            }

            @Override
            public DraftWorkflowScheme execute(DraftWorkflowScheme scheme, Project project, JiraWorkflow workflow) {
                return draftWorkflowSchemeEditor.assign(scheme, request.issueTypes, request.name, project);
            }
        }, projectKey, request.name);
    }

    @WebSudoRequired
    @DELETE
    @Path("{projectKey}")
    public Response removeWorkflow(@PathParam("projectKey") String projectKey, String workflowName) {
        projectKey = stripToNull(projectKey);
        workflowName = stripToNull(workflowName);
        if (projectKey == null || workflowName == null) {
            return WorkflowSchemeRestHelper.badRequest();
        }

        return changeWorkflowScheme(new ModifyOperation() {
            @Override
            public Response validate(DraftWorkflowScheme scheme, Project project, JiraWorkflow workflow) {
                return null;
            }

            @Override
            public DraftWorkflowScheme execute(DraftWorkflowScheme scheme, Project project, JiraWorkflow workflow) {
                return draftWorkflowSchemeEditor.delete(scheme, workflow.getName(), project);
            }
        }, projectKey, workflowName);
    }

    private interface ModifyOperation {
        Response validate(DraftWorkflowScheme scheme, Project project, JiraWorkflow workflow);

        DraftWorkflowScheme execute(DraftWorkflowScheme scheme, Project project, JiraWorkflow workflow);
    }

    private Response changeWorkflowScheme(ModifyOperation mutator, String projectKey, String workflowName) {
        ApplicationUser user = authContext.getLoggedInUser();

        I18nHelper i18nHelper = authContext.getI18nHelper();
        if (!permissionManager.hasPermission(Permissions.ADMINISTER, user)) {
            return WorkflowSchemeRestHelper.createErrorResponse(FORBIDDEN,
                    "admin.workflowschemes.service.error.no.admin.permission",
                    i18nHelper);
        }

        ProjectService.GetProjectResult projectResult = projectService.getProjectByKeyForAction(user, projectKey,
                ProjectAction.EDIT_PROJECT_CONFIG);
        if (!projectResult.isValid()) {
            return WorkflowSchemeRestHelper.createErrorResponse(projectResult);
        }
        Project project = projectResult.getProject();

        TaskDescriptor taskDescriptor = taskAccessor.getActive(project);
        if (taskDescriptor != null) {
            return WorkflowSchemeRestHelper.createErrorResponse(BAD_REQUEST, "admin.project.workflows.migration.progress", i18nHelper);
        }

        JiraWorkflow workflow = workflowManager.getWorkflow(workflowName);
        if (workflow == null) {
            return WorkflowSchemeRestHelper.notFound();
        }

        ServiceOutcome<AssignableWorkflowScheme> schemeResult = workflowSchemeService.getSchemeForProject(user, project);
        if (!schemeResult.isValid()) {
            return WorkflowSchemeRestHelper.createErrorResponse(schemeResult);
        }
        AssignableWorkflowScheme originalScheme = schemeResult.getReturnedValue();

        ServiceOutcome<DraftWorkflowScheme> draftSchemeResult = workflowSchemeService.getDraftWorkflowSchemeNotNull(user, originalScheme);
        if (!draftSchemeResult.isValid()) {
            return WorkflowSchemeRestHelper.createErrorResponse(draftSchemeResult);
        }

        if (workflowSchemeService.getUsageCount(originalScheme) != 1) {
            return WorkflowSchemeRestHelper.createErrorResponse(BAD_REQUEST,
                    "admin.project.workflows.only.one.project",
                    authContext.getI18nHelper());
        }

        final Response validate = mutator.validate(draftSchemeResult.getReturnedValue(), project, workflow);
        if (validate != null) {
            return validate;
        }

        DraftWorkflowScheme draftWorkflowScheme = mutator.execute(draftSchemeResult.getReturnedValue(), project, workflow);

        ServiceOutcome<DraftWorkflowScheme> schemeSaveResult = workflowSchemeService.updateWorkflowScheme(user, draftWorkflowScheme);
        if (!schemeSaveResult.isValid()) {
            return WorkflowSchemeRestHelper.createErrorResponse(schemeSaveResult);
        }

        return helper.builder().setUser(user).setProject(project)
                .setAssignableWorkflowScheme(originalScheme)
                .setDraftWorkflowScheme(schemeSaveResult.getReturnedValue())
                .build();
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class AssignIssueTypesRequest {
        private final String name;
        private final List<String> issueTypes;

        @JsonCreator
        public AssignIssueTypesRequest(@JsonProperty("name") String name,
                                       @JsonProperty("issueTypes") List<String> issueTypes) {
            this.issueTypes = issueTypes;
            this.name = stripToNull(name);
        }

        public boolean isValid() {
            return name != null && issueTypes != null && !issueTypes.isEmpty();
        }
    }
}
