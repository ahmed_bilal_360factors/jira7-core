package com.atlassian.jira.projectconfig.discover.events;

import com.atlassian.analytics.api.annotations.EventName;

@EventName("jira.projectconfig.summary.editworkflow.discover.show")
public class EditWorkflowDiscoverShow {
}
