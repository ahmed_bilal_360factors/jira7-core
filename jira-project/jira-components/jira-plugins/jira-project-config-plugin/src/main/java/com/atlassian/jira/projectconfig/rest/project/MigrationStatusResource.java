package com.atlassian.jira.projectconfig.rest.project;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.rest.api.util.ErrorCollection;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.task.TaskDescriptor;
import com.atlassian.jira.task.TaskManager;
import com.atlassian.jira.task.TaskProgressEvent;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.action.admin.workflow.WorkflowMigrationResult;
import com.google.common.collect.ImmutableList;
import org.codehaus.jackson.annotate.JsonAutoDetect;

import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.atlassian.jira.rest.api.http.CacheControl.never;
import static com.atlassian.jira.rest.api.util.ErrorCollection.of;

/**
 * Resource used to query the migration status.
 *
 * @since v5.1
 */
@Path("migrationStatus")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class MigrationStatusResource {
    private final JiraAuthenticationContext authCtx;
    private final PermissionManager permissionManager;
    private final TaskManager taskManager;

    public MigrationStatusResource(JiraAuthenticationContext authCtx, PermissionManager permissionManager, TaskManager taskManager) {
        this.authCtx = authCtx;
        this.permissionManager = permissionManager;
        this.taskManager = taskManager;
    }

    @GET
    @Path("{id}")
    public Response getMigrationStatus(@PathParam("id") long id) {
        if (!permissionManager.hasPermission(Permissions.ADMINISTER, authCtx.getLoggedInUser())) {
            return forbidden();
        }

        TaskDescriptor<WorkflowMigrationResult> task = taskManager.getTask(id);
        if (task == null) {
            return notFound();
        }

        if (task.isFinished()) {
            TaskStatus taskStatus = new TaskStatus(task.getResult());
            return Response.ok(taskStatus).cacheControl(never()).build();
        } else {
            TaskProgressEvent event = task.getTaskProgressIndicator().getLastProgressEvent();
            long progress = event == null ? 0 : event.getTaskProgress();
            TaskStatus taskStatus = new TaskStatus(progress);
            return Response.ok(taskStatus).cacheControl(never()).build();
        }
    }

    @DELETE
    @Path("{id}")
    public Response deleteMigrationStatus(@PathParam("id") long id) {
        ApplicationUser user = authCtx.getLoggedInUser();
        if (!permissionManager.hasPermission(Permissions.ADMINISTER, user)) {
            return forbidden();
        }

        TaskDescriptor<?> task = taskManager.getTask(id);
        if (task == null) {
            return notFound();
        }

        String taskOwner = task.getUserName();
        if (taskOwner != null && !taskOwner.equals(user.getName())) {
            return forbidden();
        }

        taskManager.removeTask(id);
        return Response.ok().cacheControl(never()).build();
    }

    private static Response notFound() {
        return Response.status(Response.Status.NOT_FOUND).cacheControl(never()).build();
    }

    private static Response forbidden() {
        return Response.status(Response.Status.FORBIDDEN).cacheControl(never()).build();
    }

    @JsonAutoDetect
    public static class TaskStatus {
        private final boolean finished;
        private final Long progress;
        private final Boolean successful;
        private final ErrorCollection errorCollection;
        private final Integer numberOfFailedIssues;
        private final List<String> failedIssues;

        public TaskStatus(WorkflowMigrationResult result) {
            finished = true;
            progress = null;
            successful = result.getResult() == WorkflowMigrationResult.SUCCESS;
            errorCollection = of(result.getErrorCollection());
            numberOfFailedIssues = result.getNumberOfFailedIssues();
            failedIssues = ImmutableList.copyOf(result.getFailedIssues().values());
        }

        public TaskStatus(long progress) {
            finished = false;
            this.progress = progress;
            successful = null;
            errorCollection = null;
            numberOfFailedIssues = null;
            failedIssues = null;
        }

        public Boolean isSuccessful() {
            return successful;
        }

        public ErrorCollection getErrorCollection() {
            return errorCollection;
        }

        public Integer getNumberOfFailedIssues() {
            return numberOfFailedIssues;
        }

        public List<String> getFailedIssues() {
            return failedIssues;
        }

        public boolean isFinished() {
            return finished;
        }

        public Long getProgress() {
            return progress;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            TaskStatus that = (TaskStatus) o;

            if (finished != that.finished) {
                return false;
            }
            if (errorCollection != null ? !errorCollection.equals(that.errorCollection) : that.errorCollection != null) {
                return false;
            }
            if (failedIssues != null ? !failedIssues.equals(that.failedIssues) : that.failedIssues != null) {
                return false;
            }
            if (numberOfFailedIssues != null ? !numberOfFailedIssues.equals(that.numberOfFailedIssues) : that.numberOfFailedIssues != null) {
                return false;
            }
            if (progress != null ? !progress.equals(that.progress) : that.progress != null) {
                return false;
            }
            if (successful != null ? !successful.equals(that.successful) : that.successful != null) {
                return false;
            }

            return true;
        }

        @Override
        public int hashCode() {
            int result = (finished ? 1 : 0);
            result = 31 * result + (progress != null ? progress.hashCode() : 0);
            result = 31 * result + (successful != null ? successful.hashCode() : 0);
            result = 31 * result + (errorCollection != null ? errorCollection.hashCode() : 0);
            result = 31 * result + (numberOfFailedIssues != null ? numberOfFailedIssues.hashCode() : 0);
            result = 31 * result + (failedIssues != null ? failedIssues.hashCode() : 0);
            return result;
        }
    }
}
