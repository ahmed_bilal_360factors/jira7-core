package com.atlassian.jira.projectconfig.servlet;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Option;
import com.atlassian.jira.bc.project.ProjectAction;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.datetime.DateTimeFormatUtils;
import com.atlassian.jira.event.mau.MauApplicationKey;
import com.atlassian.jira.event.mau.MauEventService;
import com.atlassian.jira.plugin.webfragment.JiraWebInterfaceManager;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.type.warning.InaccessibleProjectTypeDialogDataProvider;
import com.atlassian.jira.projectconfig.event.SummaryPageLoadedEvent;
import com.atlassian.jira.projectconfig.tab.DefaultTabRenderContext;
import com.atlassian.jira.projectconfig.tab.ProjectConfigTab;
import com.atlassian.jira.projectconfig.tab.ProjectConfigTabManager;
import com.atlassian.jira.projectconfig.tab.ProjectConfigTabRenderContext;
import com.atlassian.jira.projectconfig.tab.SummaryTab;
import com.atlassian.jira.projectconfig.util.ProjectConfigRequestCache;
import com.atlassian.jira.projectconfig.util.VelocityContextFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserProjectHistoryManager;
import com.atlassian.jira.user.UserPropertyManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.JiraUrlCodec;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.util.http.JiraHttpUtils;
import com.atlassian.jira.web.SessionKeys;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.descriptors.WebItemModuleDescriptor;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.seraph.util.RedirectUtils;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.opensymphony.module.propertyset.PropertySet;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.apache.commons.lang.StringUtils.removeStart;

/**
 * Servlet used to render the project configuration panels. It matches URLs of the form
 * "/project-config/(projectkey)/(pannel)/(other)" and tries to render the {@link ProjectConfigTab} with id "pannel"
 * for the project with key "projectkey". The "other" is passed to the pannel when rendering as the path info.
 *
 * @since v4.4
 */
public class PanelServlet extends HttpServlet {
    /**
     * Matches "/project-config/(projectkey)/(pannel)/(other)"
     */
    static final Pattern PATTERN = Pattern.compile("/*project-config/+([^/]+)/*(?:(?<=/)([^/]+)/*(?:(?<=/)(.+))?)?");

    private static final Logger log = Logger.getLogger(PanelServlet.class);

    private static final String CONTEXT_PROJECT_KEY_ENCODED = "projectKeyEncoded";

    private static final String TEMPLATE_TAB = "global/tab.vm";
    private static final String TEMPLATE_ERROR = "global/taberror.vm";

    private static final String PROJECT_SETTINGS_GROUP_ONE_KEY = "atl.jira.proj.config/projectgroup1";

    private static final ObjectMapper OBJECT_MAPPER;

    private final JiraAuthenticationContext authenticationContext;
    private final ProjectConfigTabManager tabManager;
    private final ProjectService service;
    private final TemplateRenderer templateRenderer;
    private final WebResourceManager webResourceManager;
    private final PageBuilderService pageBuilderService;
    private final ApplicationProperties properties;
    private final VelocityContextFactory velocityContextFactory;
    private final UserProjectHistoryManager userProjectHistoryManager;
    private final ProjectConfigRequestCache cache;
    private final EventPublisher eventPublisher;
    private final InaccessibleProjectTypeDialogDataProvider inaccessibleProjectTypeDialogDataProvider;
    private final MauEventService mauEventService;
    private final UserPropertyManager userPropertyManager;
    private final WebInterfaceManager webInterfaceManager;

    static {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET, false);
        OBJECT_MAPPER = mapper;
    }

    public PanelServlet(JiraAuthenticationContext authenticationContext,
                        ProjectConfigTabManager tabManager,
                        ProjectService service,
                        TemplateRenderer templateRenderer,
                        WebResourceManager webResourceManager,
                        PageBuilderService pageBuilderService,
                        ApplicationProperties properties,
                        VelocityContextFactory velocityContextFactory,
                        UserProjectHistoryManager userHistoryManager,
                        ProjectConfigRequestCache cache,
                        EventPublisher eventPublisher,
                        InaccessibleProjectTypeDialogDataProvider inaccessibleProjectTypeDialogDataProvider,
                        MauEventService mauEventService,
                        UserPropertyManager userPropertyManager,
                        WebInterfaceManager webInterfaceManager) {
        this.authenticationContext = authenticationContext;
        this.tabManager = tabManager;
        this.service = service;
        this.templateRenderer = templateRenderer;
        this.webResourceManager = webResourceManager;
        this.pageBuilderService = pageBuilderService;
        this.properties = properties;
        this.velocityContextFactory = velocityContextFactory;
        this.userProjectHistoryManager = userHistoryManager;
        this.cache = cache;
        this.eventPublisher = eventPublisher;
        this.inaccessibleProjectTypeDialogDataProvider = inaccessibleProjectTypeDialogDataProvider;
        this.mauEventService = mauEventService;
        this.userPropertyManager = userPropertyManager;
        this.webInterfaceManager = webInterfaceManager;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final Matcher matcher = PATTERN.matcher(req.getPathInfo());
        if (matcher.matches()) {
            String projectKey = matcher.group(1);

            final ApplicationUser user = authenticationContext.getLoggedInUser();
            ProjectService.GetProjectResult result = service.getProjectByKeyForAction(
                    user, projectKey, ProjectAction.EDIT_PROJECT_CONFIG);

            if (!result.isValid()) {
                if (user == null) {
                    redirectToLogin(req, resp);
                } else {
                    tagMauEventWithFamily();
                    I18nHelper i18nHelper = authenticationContext.getI18nHelper();
                    outputError(resp, flattenMessages(result.getErrorCollection()), i18nHelper.getText("common.words.error"));
                }
            } else {
                final Project project = result.getProject();
                String projectPanel = matcher.group(2);
                if (projectPanel == null) {
                    redirectToFirstMenuItem(req, resp, project);
                } else {
                    ProjectConfigTab tab = tabManager.getTabForId(projectPanel);
                    if (tab == null) {
                        tagMauEventWithFamily();
                        I18nHelper i18nHelper = authenticationContext.getI18nHelper();
                        outputError(resp, i18nHelper.getText("admin.project.servlet.no.tab", projectPanel),
                                i18nHelper.getText("common.words.error"));
                    } else {
                        tagMauEventWithProject(project);
                        userProjectHistoryManager.addProjectToHistory(authenticationContext.getLoggedInUser(), project);

                        // Lets also place it in the users session so we can "Navigate back to project config"
                        final PropertySet userProperties = userPropertyManager.getPropertySet(user);
                        final String projectKeyFromResult = project.getKey();
                        final String tabId = tab.getId();
                        userProperties.setString(SessionKeys.CURRENT_ADMIN_PROJECT, projectKeyFromResult);
                        userProperties.setString(SessionKeys.CURRENT_ADMIN_PROJECT_TAB, tabId);
                        userProperties.setString(SessionKeys.CURRENT_ADMIN_PROJECT_RETURN_URL, getUrlWithoutContext(req));

                        if (SummaryTab.NAME.equals(projectPanel)) {
                            // send analytics when showing summary page, for Fusion, FUZE-101
                            eventPublisher.publish(new SummaryPageLoadedEvent(project.getId(), projectKeyFromResult));
                        }
                        outputTab(req, resp, project, tab, matcher.group(3));
                    }
                }
            }
        } else {
            tagMauEventWithFamily();
            I18nHelper i18nHelper = authenticationContext.getI18nHelper();
            outputError(resp, i18nHelper.getText("admin.project.servlet.no.project"),
                    i18nHelper.getText("common.words.error"));
        }
    }

    private String getUrlWithoutContext(final HttpServletRequest req) {
        return removeStart(req.getRequestURI(), req.getContextPath());
    }

    @VisibleForTesting
    void redirectToLogin(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.sendRedirect(RedirectUtils.getLoginUrl(req));
    }

    @VisibleForTesting
    void outputTab(HttpServletRequest request, HttpServletResponse reponse,
                   Project project, ProjectConfigTab tab, String extra) throws IOException {
        cache.setProject(project);

        JiraHttpUtils.setNoCacheHeaders(reponse);
        requireResources();
        requireData(project);

        ProjectConfigTabRenderContext ctx = new DefaultTabRenderContext(extra, project, authenticationContext, webResourceManager, pageBuilderService);
        tab.addResourceForProject(ctx);
        final Map<String, Object> context = velocityContextFactory.createVelocityContext(MapBuilder.<String, Object>newBuilder()
                .add("project", project)
                .add("tabHtml", tab.getTab(ctx))
                .add("req", request)
                .add("linkId", tab.getLinkId())
                .add("title", tab.getTitle(ctx))
                .add("dateFormat", DateTimeFormatUtils.getDateFormat())
                .add("timeFormat", DateTimeFormatUtils.getTimeFormat())
                .toMap());

        writeTemplate(reponse, TEMPLATE_TAB, context);
    }

    private void redirectToFirstMenuItem(final HttpServletRequest request, final HttpServletResponse response, final Project project) throws IOException {
        final Map<String, Object> context = getMenuItemContext(request, project);
        Option<WebItemModuleDescriptor> smallestWeightItem = getSmallestWeightWebItem(PROJECT_SETTINGS_GROUP_ONE_KEY, context);

        if (smallestWeightItem.isDefined()) {
            response.sendRedirect(getWebItemUrl(request, smallestWeightItem.get(), context));
        } else {
            throw new RuntimeException(String.format("Unexpected error: There should be at least one element in the first project group: '%s'", PROJECT_SETTINGS_GROUP_ONE_KEY));
        }
    }

    private Option<WebItemModuleDescriptor> getSmallestWeightWebItem(final String section, final Map<String, Object> context) {
        return Option.option(Lists.newArrayList(webInterfaceManager.getDisplayableItems(section, context)).stream()
                .sorted((o1, o2) -> o1.getWeight() - o2.getWeight())
                .findFirst().orElse(null));
    }

    private String getWebItemUrl(final HttpServletRequest request, final WebItemModuleDescriptor webItem, Map<String, Object> context) {
        return webItem.getLink().getDisplayableUrl(request, context);
    }

    private Map<String, Object> getMenuItemContext(final HttpServletRequest request, final Project project) {
        final MapBuilder<String, Object> contextBuilder = MapBuilder.newBuilder();

        contextBuilder.add(CONTEXT_PROJECT_KEY_ENCODED, JiraUrlCodec.encode(project.getKey(), true));
        contextBuilder.add(JiraWebInterfaceManager.CONTEXT_KEY_USER, authenticationContext.getLoggedInUser());
        contextBuilder.add(JiraWebInterfaceManager.CONTEXT_KEY_HELPER, new JiraHelper(request, project));

        return contextBuilder.toMap();
    }

    private void outputError(HttpServletResponse reponse, String errorMessage, String title) throws IOException {
        outputError(reponse, Collections.singleton(errorMessage), title);
    }

    void outputError(HttpServletResponse reponse, Collection<String> errorMessage, String title) throws IOException {
        JiraHttpUtils.setNoCacheHeaders(reponse);
        requireResources();

        final Map<String, Object> context = velocityContextFactory.createVelocityContext(MapBuilder.<String, Object>newBuilder()
                .add("errorMessages", errorMessage)
                .add("title", title)
                .toMap());

        writeTemplate(reponse, TEMPLATE_ERROR, context);
    }

    private void writeTemplate(HttpServletResponse reponse, String template, Map<String, Object> context) throws IOException {
        //We need to so this to ensure that site-mesh actually runs on the page.
        reponse.setContentType(getContentType());
        PrintWriter writer = reponse.getWriter();
        try {
            templateRenderer.render(template, context, writer);
        } catch (IOException e) {
            //Don't cause an error on close so we throw the original error.
            IOUtils.closeQuietly(writer);
            throw e;
        } finally {
            //Let this error be thrown if it happens.
            writer.close();
        }
    }

    private String getContentType() {
        try {
            return properties.getContentType();
        } catch (Exception e) {
            return "text/html; charset=UTF-8";
        }
    }

    private Collection<String> flattenMessages(ErrorCollection collection) {
        if (!collection.hasAnyErrors()) {
            return Collections.emptyList();
        }

        final Set<String> messages = Sets.newLinkedHashSet();
        messages.addAll(collection.getErrorMessages());
        messages.addAll(collection.getErrors().values());

        return messages;
    }

    private void requireData(Project project) {
        requireProjectTypesData(project);
        pageBuilderService.assembler().data().requireData("com.atlassian.jira.jira-project-config-plugin:app-data", writer -> {
            OBJECT_MAPPER.writeValue(writer, ImmutableMap.of(
                    "projectKey", project.getKey()
            ));
        });
    }

    private void requireProjectTypesData(Project project) {
        ApplicationUser loggedInUser = authenticationContext.getLoggedInUser();
        if (inaccessibleProjectTypeDialogDataProvider.shouldDisplayInaccessibleWarning(loggedInUser, project)) {
            inaccessibleProjectTypeDialogDataProvider.provideData(pageBuilderService.assembler(), loggedInUser, project);
        }
    }

    private void requireResources() {
        webResourceManager.requireResourcesForContext("jira.admin.conf");
        webResourceManager.requireResource("com.atlassian.jira.jira-project-config-plugin:project-config-global");
    }

    /**
     * Tag a MAU event with the current project. This is non fatal should it fail.
     */
    private void tagMauEventWithProject(Project project) {
        try {
            mauEventService.setApplicationForThreadBasedOnProject(project);
        } catch (Exception e) {
            log.warn("Exception thrown from MAUEventService.setApplicationForThreadBasedOnProject:" + e.getMessage(), e);
        }
    }

    /**
     * Tag a MAU event with the JIRA family. This is non fatal should it fail.
     */
    private void tagMauEventWithFamily() {
        try {
            mauEventService.setApplicationForThread(MauApplicationKey.family());
        } catch (Exception e) {
            log.warn("Exception thrown from MAUEventService.setApplicationForThread:" + e.getMessage(), e);
        }

    }
}
