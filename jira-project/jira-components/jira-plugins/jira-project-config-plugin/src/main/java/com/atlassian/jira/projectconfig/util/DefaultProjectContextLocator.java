package com.atlassian.jira.projectconfig.util;

import javax.annotation.Nonnull;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.bc.project.ProjectAction;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.beans.ProjectContext;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @since v6.2
 */
@Component
public class DefaultProjectContextLocator implements ProjectContextLocator {
    private final ProjectService projectService;
    private final JiraAuthenticationContext authContext;

    @Autowired
    public DefaultProjectContextLocator(
            @ComponentImport final ProjectService projectService,
            @ComponentImport final JiraAuthenticationContext authContext) {
        this.projectService = projectService;
        this.authContext = authContext;
    }

    @Nonnull
    @Override
    public ServiceOutcome<ProjectContext> getContext(@Nonnull final String projectKey, final long issueTypeId) {
        final ProjectService.GetProjectResult projectResult = projectService
                .getProjectByKeyForAction(authContext.getUser(), projectKey, ProjectAction.EDIT_PROJECT_CONFIG);

        if (!projectResult.isValid()) {
            return ServiceOutcomeImpl.from(projectResult.getErrorCollection());
        }

        final IssueType type = getIssueType(projectResult.getProject(), issueTypeId);
        if (type != null) {
            return ServiceOutcomeImpl.ok(new ProjectContext(projectResult.getProject(), type));
        } else {
            return ServiceOutcomeImpl.error(authContext.getI18nHelper().getText("admin.issue.type.not.in.project"), ErrorCollection.Reason.NOT_FOUND);
        }
    }

    private IssueType getIssueType(final Project project, final long issueTypeId) {
        final String issueTypeIdString = String.valueOf(issueTypeId);
        for (IssueType issueType : project.getIssueTypes()) {
            if (issueType.getId().equals(issueTypeIdString)) {
                return issueType;
            }
        }
        return null;
    }
}
