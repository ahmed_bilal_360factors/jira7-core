package com.atlassian.jira.projectconfig.rest;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.config.IssueTypeManager;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.issue.fields.config.manager.IssueTypeSchemeManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.beans.NamedDefault;
import com.atlassian.jira.projectconfig.order.OrderFactory;
import com.atlassian.jira.projectconfig.rest.beans.SharedByData;
import com.atlassian.jira.projectconfig.rest.beans.SimpleRestProject;
import com.atlassian.jira.projectconfig.shared.SharedEntitiesHelper;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.task.TaskDescriptor;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection.Reason;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.action.admin.workflow.WorkflowMigrationResult;
import com.atlassian.jira.workflow.AssignableWorkflowScheme;
import com.atlassian.jira.workflow.DraftWorkflowScheme;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.WorkflowScheme;
import com.atlassian.jira.workflow.migration.WorkflowSchemeMigrationTaskAccessor;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.jira.rest.api.http.CacheControl.never;
import static com.atlassian.jira.rest.api.util.ErrorCollection.of;
import static com.google.common.collect.Iterables.transform;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.stripToNull;

/**
 * @since v5.2
 */
@Component
public class WorkflowSchemeRestHelper {
    private final PermissionManager permissionManager;
    private final WorkflowManager workflowManager;
    private final SharedEntitiesHelper helper;
    private final IssueTypeSchemeManager issueTypeSchemeManager;
    private final OrderFactory orderFactory;
    private final DateTimeFormatter dateTimeFormatter;
    private final AvatarService avatarService;
    private final IssueTypeManager issueTypeManager;
    private final WorkflowSchemeMigrationTaskAccessor workflowSchemeMigrationTaskAccessor;

    @Autowired
    public WorkflowSchemeRestHelper(PermissionManager permissionManager,
                                    WorkflowManager workflowManager, SharedEntitiesHelper helper,
                                    IssueTypeSchemeManager issueTypeSchemeManager, OrderFactory orderFactory,
                                    DateTimeFormatter dateTimeFormatter, AvatarService avatarService,
                                    IssueTypeManager issueTypeManager, WorkflowSchemeMigrationTaskAccessor workflowSchemeMigrationTaskAccessor) {
        this.permissionManager = permissionManager;
        this.workflowManager = workflowManager;
        this.helper = helper;
        this.issueTypeSchemeManager = issueTypeSchemeManager;
        this.orderFactory = orderFactory;
        this.dateTimeFormatter = dateTimeFormatter;
        this.avatarService = avatarService;
        this.issueTypeManager = issueTypeManager;
        this.workflowSchemeMigrationTaskAccessor = workflowSchemeMigrationTaskAccessor;
    }

    public static Response notFound() {
        return Response.status(Response.Status.NOT_FOUND).cacheControl(never()).build();
    }

    public static Response badRequest() {
        return Response.status(Response.Status.BAD_REQUEST).cacheControl(never()).build();
    }

    public static Response ok(Object response) {
        return Response.ok(response).cacheControl(never()).build();
    }

    public static Response createErrorResponse(ServiceResult result) {
        Set<Reason> reasons = result.getErrorCollection().getReasons();
        Reason worstReason = Reason.getWorstReason(reasons);
        if (worstReason == null) {
            worstReason = Reason.NOT_FOUND;
        }
        return Response.status(worstReason.getHttpStatusCode())
                .entity(com.atlassian.jira.rest.api.util.ErrorCollection.of(result.getErrorCollection()))
                .cacheControl(never()).build();
    }

    public static Response createErrorResponse(Response.Status status, String key, I18nHelper i18nHelper) {
        return Response.status(status)
                .entity(of(i18nHelper.getText(key)))
                .cacheControl(never())
                .build();
    }

    public ResponseDataBuilder builder() {
        return new ResponseDataBuilder();
    }

    public class ResponseDataBuilder {
        private AssignableWorkflowScheme assignableWorkflowScheme;
        private DraftWorkflowScheme draftWorkflowScheme;
        private ApplicationUser user;
        private Project project;

        private Comparator<NamedDefault> namedDefaultComparator;
        private Comparator<com.atlassian.jira.projectconfig.beans.SimpleIssueType> simpleIssueTypeComparator;

        private ResponseDataBuilder() {
        }

        public ResponseDataBuilder setAssignableWorkflowScheme(AssignableWorkflowScheme assignableWorkflowScheme) {
            this.assignableWorkflowScheme = assignableWorkflowScheme;
            return this;
        }

        public ResponseDataBuilder setDraftWorkflowScheme(DraftWorkflowScheme draftWorkflowScheme) {
            this.draftWorkflowScheme = draftWorkflowScheme;
            return this;
        }

        public ResponseDataBuilder setUser(ApplicationUser user) {
            this.user = user;
            return this;
        }

        public ResponseDataBuilder setProject(Project project) {
            this.project = project;
            return this;
        }

        private WorkflowScheme getWorkflowScheme() {
            return draftWorkflowScheme != null ? draftWorkflowScheme : assignableWorkflowScheme;
        }

        public Response build() {
            WorkflowScheme workingScheme = getWorkflowScheme();
            final WorkflowSchemeResponse response = new WorkflowSchemeResponse(workingScheme);
            if (project != null) {
                addWorkflowData(project, workingScheme, response);
            } else {
                addWorkflowData(workingScheme, response);
            }

            if (user != null) {
                response.setCurrentUser(user.getName());
            }
            response.setAdmin(permissionManager.hasPermission(Permissions.ADMINISTER, user));
            response.setSysAdmin(permissionManager.hasPermission(Permissions.SYSTEM_ADMIN, user));
            response.setTotalWorkflows(workflowManager.getWorkflows().size());
            response.setParentId(assignableWorkflowScheme.getId());

            addProjectInformation(response);
            addDraftInfo(workingScheme, response);

            TaskDescriptor<WorkflowMigrationResult> taskDescriptor;
            if (draftWorkflowScheme != null) {
                taskDescriptor = workflowSchemeMigrationTaskAccessor.getActiveByProjects(draftWorkflowScheme, true);
            } else {
                taskDescriptor = workflowSchemeMigrationTaskAccessor.getActive(assignableWorkflowScheme);
            }
            if (taskDescriptor != null) {
                response.setMigrationProgressURL(taskDescriptor.getProgressURL());
            }

            return ok(response);
        }

        private void addDraftInfo(WorkflowScheme scheme, WorkflowSchemeResponse response) {
            if (scheme instanceof DraftWorkflowScheme) {
                final DraftWorkflowScheme draftScheme = (DraftWorkflowScheme) scheme;
                ApplicationUser lastModifiedUser = draftScheme.getLastModifiedUser();
                if (lastModifiedUser != null) {
                    URI avatarUrl = avatarService.getAvatarAbsoluteURL(lastModifiedUser, lastModifiedUser, Avatar.Size.SMALL);
                    response.setLastModifiedUser(draftScheme.getLastModifiedUser(), avatarUrl);
                }

                response.setLastModifiedDate(dateTimeFormatter.forUser(user).format(draftScheme.getLastModifiedDate()));
            }
        }

        private void addProjectInformation(WorkflowSchemeResponse response) {
            final List<Project> projects = helper.getProjectsForScheme(project, assignableWorkflowScheme.getId());
            final SharedByData sharedByData = getSharedData(projects);

            response.setShared(sharedByData);
        }

        private SharedByData getWorkflowSharedByData(String workflowName) {
            final List<Project> projects = helper.getProjectsForWorkflow(project, workflowName);

            return getSharedData(projects);
        }

        private SharedByData getSharedData(List<Project> projects) {
            final List<Project> allowedProjects = helper.filterSharingProjects(projects);
            final List<SimpleRestProject> restProjects = allowedProjects.stream().map(SimpleRestProject::fullProject).collect(toList());

            return new SharedByData(restProjects, projects.size());
        }

        private Multimap<String, IssueType> getInvertedWorkflowMap(Project project, WorkflowScheme workflowScheme) {
            ImmutableMultimap.Builder<String, IssueType> builder = ImmutableMultimap.builder();
            for (IssueType type : project.getIssueTypes()) {
                builder.put(workflowScheme.getActualWorkflow(type.getId()), type);
            }

            return builder.build();
        }

        private void addWorkflowData(Project project, WorkflowScheme workflowScheme, WorkflowSchemeResponse response) {
            final Multimap<String, IssueType> invertedWorkflowMap = getInvertedWorkflowMap(project, workflowScheme);
            final List<SimpleWorkflow> mappings = Lists.newArrayList();
            final List<SimpleIssueType> usedIssueTypes = Lists.newArrayList();
            final IssueType defaultIssueType = issueTypeSchemeManager.getDefaultIssueType(project);
            final String defaultWorkflowName = workflowScheme.getActualDefaultWorkflow();

            for (Map.Entry<String, Collection<IssueType>> entry : invertedWorkflowMap.asMap().entrySet()) {
                String name = entry.getKey();
                JiraWorkflow workflow = workflowManager.getWorkflow(name);

                List<SimpleIssueType> entryTypes = createIssueTypes(entry.getValue(), defaultIssueType);
                usedIssueTypes.addAll(entryTypes);

                boolean isDefault = name.equals(defaultWorkflowName);
                Iterable<String> issueTypeIds = transform(entryTypes, SimpleIssueType::getId);
                SharedByData sharedByData = getWorkflowSharedByData(name);

                mappings.add(new SimpleWorkflow(workflow, isDefault, issueTypeIds, sharedByData));
            }

            response.setMappings(sortNamedDefault(mappings)).setIssueTypes(sortTypes(usedIssueTypes));
        }

        private List<SimpleIssueType> createIssueTypes(Iterable<? extends IssueType> value, IssueType defaultIssueType) {
            List<SimpleIssueType> issueTypes = Lists.newArrayList();
            for (IssueType issueType : value) {
                issueTypes.add(new SimpleIssueType(issueType, defaultIssueType != null && issueType.getId().equals(defaultIssueType.getId())));
            }
            return sortTypes(issueTypes);
        }

        private void addWorkflowData(WorkflowScheme workflowScheme, WorkflowSchemeResponse response) {
            Map<String, SimpleIssueType> issueTypeMap = Maps.newHashMap();
            for (IssueType issueType : issueTypeManager.getIssueTypes()) {
                SimpleIssueType type = new SimpleIssueType(issueType);
                issueTypeMap.put(type.getId(), type);
            }

            response.setMappings(sortNamedDefault(getMappings(workflowScheme, issueTypeMap)));
            if (workflowScheme instanceof DraftWorkflowScheme) {
                List<SimpleWorkflow> originalMappings = getMappings(((DraftWorkflowScheme) workflowScheme).getParentScheme(), issueTypeMap);
                response.setOriginalMappings(sortNamedDefault(originalMappings));
            }

            response.setIssueTypes(sortTypes(Lists.newArrayList(issueTypeMap.values())));
        }

        private List<SimpleWorkflow> getMappings(WorkflowScheme workflowScheme, Map<String, SimpleIssueType> issueTypeMap) {
            String defaultWorkflow = null;
            Multimap<String, SimpleIssueType> invertedMap = HashMultimap.create();
            for (Map.Entry<String, String> entry : workflowScheme.getMappings().entrySet()) {
                String issueType = entry.getKey();
                String workflow = entry.getValue();

                if (issueType != null) {
                    SimpleIssueType type = issueTypeMap.get(issueType);
                    if (type != null) {
                        invertedMap.put(workflow, type);
                    }
                } else {
                    defaultWorkflow = workflow;
                }
            }

            if (defaultWorkflow == null) {
                defaultWorkflow = JiraWorkflow.DEFAULT_WORKFLOW_NAME;
            }

            final List<SimpleWorkflow> mappings = Lists.newArrayList();
            for (Map.Entry<String, Collection<SimpleIssueType>> entry : invertedMap.asMap().entrySet()) {
                final JiraWorkflow workflow = workflowManager.getWorkflow(entry.getKey());
                final String workflowName = workflow.getName();
                final boolean isDefault = defaultWorkflow.equals(workflowName);
                final List<SimpleIssueType> types = sortTypes(Lists.newArrayList(entry.getValue()));
                final Iterable<String> issueTypeIds = transform(types, SimpleIssueType::getId);
                final SharedByData sharedByData = getWorkflowSharedByData(workflowName);

                mappings.add(new SimpleWorkflow(workflow, isDefault, issueTypeIds, sharedByData));
            }

            if (!invertedMap.containsKey(defaultWorkflow)) {
                final JiraWorkflow workflow = workflowManager.getWorkflow(defaultWorkflow);
                final SharedByData sharedByData = getWorkflowSharedByData(defaultWorkflow);

                mappings.add(new SimpleWorkflow(workflow, true, emptyList(), sharedByData));
            }
            return mappings;
        }

        public <T extends NamedDefault> List<T> sortNamedDefault(List<T> list) {
            Collections.sort(list, getNamedDefaultComparator());
            return list;
        }

        public List<SimpleIssueType> sortTypes(List<SimpleIssueType> list) {
            Collections.sort(list, getIssueTypeCompatator());
            return list;
        }

        private Comparator<NamedDefault> getNamedDefaultComparator() {
            if (namedDefaultComparator == null) {
                namedDefaultComparator = orderFactory.createNamedDefaultComparator();
            }
            return namedDefaultComparator;
        }

        private Comparator<? super SimpleIssueType> getIssueTypeCompatator() {
            if (simpleIssueTypeComparator == null) {
                simpleIssueTypeComparator = orderFactory.createIssueTypeComparator();
            }
            return simpleIssueTypeComparator;
        }
    }

    public static class SimpleWorkflow implements NamedDefault {
        private final String name;
        private final String displayName;
        private final String description;
        private final boolean defaultWorkflow;
        private final boolean systemWorkflow;
        private final Iterable<String> issueTypes;
        private final boolean jiraDefault;
        private final SharedByData sharedBy;

        public SimpleWorkflow(JiraWorkflow workflow, boolean defaultWorkflow, Iterable<String> issueTypes, SharedByData sharedBy) {
            this.name = workflow.getName();
            this.displayName = workflow.getDisplayName();
            this.description = stripToNull(workflow.getDescription());
            this.defaultWorkflow = defaultWorkflow;
            this.systemWorkflow = workflow.isSystemWorkflow();
            this.jiraDefault = workflow.isDefault();
            this.issueTypes = issueTypes;
            this.sharedBy = sharedBy;
        }

        @JsonProperty
        public Iterable<String> getIssueTypes() {
            return issueTypes;
        }

        @JsonProperty
        @Override
        public String getName() {
            return name;
        }

        @JsonProperty
        @Override
        public boolean isDefault() {
            return defaultWorkflow;
        }

        @JsonProperty
        public String getDescription() {
            return description;
        }

        @JsonProperty
        public boolean isSystem() {
            return systemWorkflow;
        }

        @JsonProperty
        public boolean isJiraDefault() {
            return jiraDefault;
        }

        @JsonProperty
        public String getDisplayName() {
            return displayName;
        }

        @JsonProperty
        public SharedByData getSharedBy() {
            return sharedBy;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            SimpleWorkflow that = (SimpleWorkflow) o;

            if (name != null ? !name.equals(that.name) : that.name != null) {
                return false;
            }

            return true;
        }

        @Override
        public int hashCode() {
            return name != null ? name.hashCode() : 0;
        }
    }

    public static class WorkflowSchemeResponse {
        private String name;
        private String description;
        private Long id;
        private SharedByData shared;
        private Iterable<SimpleWorkflow> mappings;
        private Iterable<SimpleWorkflow> originalMappings;
        private Iterable<SimpleIssueType> issueTypes;
        private boolean admin;
        private boolean sysAdmin;
        private String lastModifiedDate;
        private SimpleUser lastModifiedUser;
        private boolean defaultScheme;
        private int totalWorkflows;
        private boolean draftScheme;
        private String currentUser;
        private Long parentId;
        private String migrationProgressURL;

        public WorkflowSchemeResponse(WorkflowScheme scheme) {
            setWorkflowScheme(scheme);
        }

        public WorkflowSchemeResponse setWorkflowScheme(WorkflowScheme scheme) {
            setDefaultScheme(scheme.isDefault());
            setDescription(scheme.getDescription());
            setId(scheme.getId());
            setName(scheme.getName());
            setDraftScheme(scheme.isDraft());

            return this;
        }

        public WorkflowSchemeResponse setAdmin(boolean admin) {
            this.admin = admin;
            return this;
        }

        public WorkflowSchemeResponse setSysAdmin(boolean sysAdmin) {
            this.sysAdmin = sysAdmin;
            return this;
        }

        public WorkflowSchemeResponse setDefaultScheme(boolean defaultScheme) {
            this.defaultScheme = defaultScheme;
            return this;
        }

        public WorkflowSchemeResponse setDescription(String description) {
            this.description = stripToNull(description);
            return this;
        }

        public WorkflowSchemeResponse setDraftScheme(boolean draftScheme) {
            this.draftScheme = draftScheme;
            return this;
        }

        public WorkflowSchemeResponse setMigrationProgressURL(String migrationProgressURL) {
            this.migrationProgressURL = migrationProgressURL;
            return this;
        }

        public WorkflowSchemeResponse setId(Long id) {
            this.id = id;
            return this;
        }

        public WorkflowSchemeResponse setIssueTypes(Iterable<SimpleIssueType> issueTypes) {
            this.issueTypes = issueTypes;
            return this;
        }

        public WorkflowSchemeResponse setLastModifiedDate(String lastModifiedDate) {
            this.lastModifiedDate = stripToNull(lastModifiedDate);
            return this;
        }

        public WorkflowSchemeResponse setLastModifiedUser(ApplicationUser user, URI avatarUrl) {
            this.lastModifiedUser = SimpleUser.asSimpleUser(user, avatarUrl);
            return this;
        }

        public WorkflowSchemeResponse setName(String name) {
            this.name = stripToNull(name);
            return this;
        }

        public WorkflowSchemeResponse setShared(SharedByData shared) {
            this.shared = shared;
            return this;
        }

        public WorkflowSchemeResponse setTotalWorkflows(int totalWorkflows) {
            this.totalWorkflows = totalWorkflows;
            return this;
        }

        public WorkflowSchemeResponse setMappings(Iterable<SimpleWorkflow> mappings) {
            this.mappings = mappings;
            return this;
        }

        public WorkflowSchemeResponse setOriginalMappings(Iterable<SimpleWorkflow> mappings) {
            this.originalMappings = mappings;
            return this;
        }

        @JsonProperty
        public String getDescription() {
            return description;
        }

        @JsonProperty
        public Long getId() {
            return id;
        }

        @JsonProperty
        public String getName() {
            return name;
        }

        @JsonProperty
        public Iterable<SimpleWorkflow> getMappings() {
            return mappings;
        }

        @JsonProperty
        public Iterable<SimpleIssueType> getIssueTypes() {
            return issueTypes;
        }

        @JsonProperty
        public SharedByData getShared() {
            return shared;
        }

        @JsonProperty
        public boolean isAdmin() {
            return admin;
        }

        @JsonProperty
        public boolean isSysAdmin() {
            return sysAdmin;
        }

        @JsonProperty
        public boolean isDefaultScheme() {
            return defaultScheme;
        }

        @JsonProperty
        public int getTotalWorkflows() {
            return totalWorkflows;
        }

        @JsonProperty
        public boolean isDraftScheme() {
            return draftScheme;
        }

        @JsonProperty
        public String getLastModifiedDate() {
            return lastModifiedDate;
        }

        @JsonProperty
        public SimpleUser getLastModifiedUser() {
            return lastModifiedUser;
        }

        @JsonProperty
        public Iterable<SimpleWorkflow> getOriginalMappings() {
            return originalMappings;
        }

        @JsonProperty
        public String getCurrentUser() {
            return currentUser;
        }

        @JsonProperty
        public String getMigrationProgressURL() {
            return migrationProgressURL;
        }

        public WorkflowSchemeResponse setCurrentUser(String currentUser) {
            this.currentUser = currentUser;
            return this;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            WorkflowSchemeResponse that = (WorkflowSchemeResponse) o;

            if (draftScheme != that.draftScheme) {
                return false;
            }
            if (id != null ? !id.equals(that.id) : that.id != null) {
                return false;
            }

            return true;
        }

        @Override
        public int hashCode() {
            int result = id != null ? id.hashCode() : 0;
            result = 31 * result + (draftScheme ? 1 : 0);
            return result;
        }

        @JsonProperty
        public Long getParentId() {
            return parentId;
        }

        public WorkflowSchemeResponse setParentId(Long parentId) {
            this.parentId = parentId;
            return this;
        }
    }

    public static class SimpleUser {
        private final String name;
        private final String displayName;
        private final boolean active;
        private final Map<String, URI> avatarUrls;

        public SimpleUser(ApplicationUser applicationUser, URI avatarUrl) {
            this.displayName = applicationUser.getDisplayName();
            this.name = applicationUser.getUsername();
            this.active = applicationUser.isActive();
            this.avatarUrls = Collections.singletonMap("16x16", avatarUrl);
        }

        @JsonProperty
        public String getDisplayName() {
            return displayName;
        }

        @JsonProperty
        public String getName() {
            return name;
        }

        @JsonProperty
        public boolean isActive() {
            return active;
        }

        @JsonProperty
        public Map<String, URI> getAvatarUrls() {
            return avatarUrls;
        }

        public static SimpleUser asSimpleUser(ApplicationUser user, URI avatarUrl) {
            if (user == null) {
                return null;
            } else {
                return new SimpleUser(user, avatarUrl);
            }
        }
    }

    public static class SimpleIssueType implements com.atlassian.jira.projectconfig.beans.SimpleIssueType {
        private final String iconUrl;
        private final String name;
        private final String description;
        private final String id;
        private final boolean subTask;
        private final boolean defaultIssueType;

        public SimpleIssueType(IssueType issueType) {
            this(issueType, false);
        }

        public SimpleIssueType(IssueType issueType, boolean defaultIssueType) {
            this.id = issueType.getId();
            this.name = stripToNull(issueType.getNameTranslation());
            this.description = stripToNull(issueType.getDescTranslation());
            this.iconUrl = issueType.getCompleteIconUrl();
            this.subTask = issueType.isSubTask();
            this.defaultIssueType = defaultIssueType;
        }

        @JsonProperty
        @Override
        public String getIconUrl() {
            return iconUrl;
        }

        @JsonProperty
        @Override
        public String getName() {
            return name;
        }

        @JsonIgnore
        @Override
        public boolean isDefault() {
            return defaultIssueType;
        }

        @JsonProperty
        @Override
        public String getDescription() {
            return description;
        }

        @JsonProperty
        @Override
        public String getId() {
            return id;
        }

        @JsonProperty
        @Override
        public boolean isSubTask() {
            return subTask;
        }

        @Override
        @JsonProperty
        public boolean isDefaultIssueType() {
            return defaultIssueType;
        }

        @Override
        public IssueType getConstant() {
            return null;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            SimpleIssueType that = (SimpleIssueType) o;

            if (!id.equals(that.id)) {
                return false;
            }

            return true;
        }

        @Override
        public int hashCode() {
            return id.hashCode();
        }
    }
}
