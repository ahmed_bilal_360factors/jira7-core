package com.atlassian.jira.projectconfig.issuetypes.workflow;

import com.atlassian.jira.projectconfig.beans.ProjectContext;
import com.atlassian.jira.projectconfig.shared.SharedIssueTypeWorkflowData;
import com.atlassian.jira.workflow.JiraWorkflow;

import javax.annotation.Nonnull;
import java.util.Locale;

/**
 * Used to classify a project based on its workflow and workflow scheme state.
 *
 * @since v6.2
 */
public interface IssueTypeConfigWorkflowHelper {
    /**
     * Return the {@link JiraWorkflow}, {@link WorkflowState}, shared {@link com.atlassian.jira.project.Project}s
     * and shared {@link com.atlassian.jira.issue.issuetype.IssueType}s for the passed
     * {@link com.atlassian.jira.projectconfig.beans.ProjectContext}.
     *
     * @return The {@link IssueTypeConfigWorkflowHelper.WorkflowResult} of the lookup.
     */
    @Nonnull
    WorkflowResult getWorkflowData(@Nonnull ProjectContext context);

    /**
     * A class that contains the result of a workflow lookup.
     */
    public class WorkflowResult {
        private final JiraWorkflow workflow;
        private final WorkflowState workflowState;
        private final SharedIssueTypeWorkflowData sharedBy;

        public WorkflowResult(final JiraWorkflow workflow, final WorkflowState workflowState,
                              final SharedIssueTypeWorkflowData sharedBy) {
            this.workflow = workflow;
            this.workflowState = workflowState;
            this.sharedBy = sharedBy;
        }

        public JiraWorkflow getWorkflow() {
            return workflow;
        }

        public WorkflowState getWorkflowState() {
            return workflowState;
        }

        public SharedIssueTypeWorkflowData getSharedBy() {
            return sharedBy;
        }
    }

    /**
     * Represents the state of a workflow associated with a project.
     */
    public enum WorkflowState {
        /**
         * The user does not have permission to edit the workflow.
         */
        NO_PERMISSION,

        /**
         * The workflow is not editable.
         */
        READ_ONLY,

        /**
         * The workflow is not editable to the user that has edit workflow permission because it is shared.
         */
        READ_ONLY_DELEGATED_SHARED,

        /**
         * The workflow is not editable to the user that has edit workflow permission because it is system workflow.
         */
        READ_ONLY_DELEGATED_SYSTEM,

        /**
         * The workflow is editable.
         */
        EDITABLE,

        /**
         * The workflow is editable because user has edit workflow permission.
         */
        EDITABLE_DELEGATED,

        /**
         * The workflow will be editable after a migration.
         */
        MIGRATE;

        /**
         * Return a non-java constant simple name for the value.
         *
         * @return a non-java constant simple name for the value.
         */
        public String simpleName() {
            return name().toLowerCase(Locale.ENGLISH).replace("_", "");
        }
    }
}
