package com.atlassian.jira.projectconfig.rest.project;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.projectconfig.beans.ProjectContext;
import com.atlassian.jira.projectconfig.issuetypes.workflow.IssueTypeConfigWorkflowHelper;
import com.atlassian.jira.projectconfig.rest.Responses;
import com.atlassian.jira.projectconfig.rest.beans.SharedByData;
import com.atlassian.jira.projectconfig.util.ProjectContextLocator;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Gets the issue type configuration associated with a workflow.
 *
 * @since v6.2
 */
@Path("issuetype/{project}/{issuetype}/workflow")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class IssueTypeConfigWorkflowResource {
    private final IssueTypeConfigWorkflowHelper workflowHelper;
    private final ProjectContextLocator projectContextLocator;

    public IssueTypeConfigWorkflowResource(final IssueTypeConfigWorkflowHelper workflowHelper,
                                           final ProjectContextLocator projectContextLocator) {
        this.workflowHelper = workflowHelper;
        this.projectContextLocator = projectContextLocator;
    }

    @GET
    public Response getWorkflowData(@PathParam("project") String projectKey,
                                    @PathParam("issuetype") long issueTypeId) {
        final ServiceOutcome<ProjectContext> context = projectContextLocator.getContext(projectKey, issueTypeId);
        if (!context.isValid()) {
            return Responses.forOutcome(context);
        } else {
            return Responses.ok(new WorkflowDetails(workflowHelper.getWorkflowData(context.get())));
        }
    }

    public static class WorkflowDetails extends SharedByData {
        @JsonProperty
        private String name;

        @JsonProperty
        private String state;

        @JsonProperty
        private String displayName;

        private WorkflowDetails(IssueTypeConfigWorkflowHelper.WorkflowResult workflowResult) {
            super(workflowResult.getSharedBy());
            this.name = workflowResult.getWorkflow().getName();
            this.displayName = workflowResult.getWorkflow().getDisplayName();
            this.state = workflowResult.getWorkflowState().simpleName();
        }

        public String getName() {
            return name;
        }

        public String getState() {
            return state;
        }

        public String getDisplayName() {
            return displayName;
        }
    }
}
