package com.atlassian.jira.projectconfig.fields;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.OrderableField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayout;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.issue.fields.screen.FieldScreenTab;
import com.atlassian.jira.issue.fields.screen.ProjectFieldScreenHelper;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.util.Projects;
import com.atlassian.jira.util.lang.Pair;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * A utility class for adding
 *
 * @since v6.1
 */
@Component
public class IssueFields {
    private static final Logger log = Logger.getLogger(IssueFields.class);

    private final ScreenFinder screenFinder;
    private final ProjectFieldScreenHelper screenHelper;
    private final CustomFieldManager fieldManager;
    private final FieldLayoutManager fieldLayoutManager;

    @Autowired
    public IssueFields(ScreenFinder screenFinder, final ProjectFieldScreenHelper screenHelper,
                       final CustomFieldManager fieldManager, FieldLayoutManager fieldLayoutManager) {
        this.screenFinder = screenFinder;
        this.screenHelper = screenHelper;
        this.fieldManager = fieldManager;
        this.fieldLayoutManager = fieldLayoutManager;
    }

    public void addFieldToIssueScreens(Field field, Issue issue) {
        for (FieldScreen screen : screenFinder.getIssueScreens(issue)) {
            if (!screen.containsField(field.getId())) {
                final FieldScreenTab firstTab = Iterables.getFirst(screen.getTabs(), null);
                if (firstTab == null) {
                    log.debug(String.format("Unable to add field %s to screen for issue %s. Screen has no tab.",
                            field.getId(), issue.getKey()));
                } else {
                    firstTab.addFieldScreenLayoutItem(field.getId());
                }
            }
        }
    }

    /**
     * Returns the list of projects that will be affected by adding a field to a screen.
     *
     * @param issue the issue to check.
     * @return the list of projects that will be affected.
     */
    public Iterable<Project> getAffectedProjects(Issue issue) {
        Set<Project> projects = Sets.newTreeSet(Projects.nameOrder());
        for (FieldScreen fieldScreen : screenFinder.getIssueScreens(issue)) {
            projects.addAll(screenHelper.getProjectsForFieldScreen(fieldScreen));
        }
        return Collections.unmodifiableSet(projects);
    }

    /**
     * Returns the list of Custom Fields that are in the context of a passed issue and are visible.
     *
     * @param issue the issue in the query.
     * @return a list of Custom Fields that are in the context of the passed issue.
     */
    public Iterable<OrderableField> getAllCustomFields(Issue issue) {
        final List<OrderableField> allOrderableFields = new ArrayList<OrderableField>(fieldManager.getCustomFieldObjects(issue));
        final FieldLayout fieldLayout = fieldLayoutManager.getFieldLayout(issue);
        List<OrderableField> visibleFields = Lists.newArrayList();
        for (OrderableField field : allOrderableFields) {
            final FieldLayoutItem fieldLayoutItem = fieldLayout.getFieldLayoutItem(field);
            if (fieldLayoutItem == null || !fieldLayoutItem.isHidden()) {
                visibleFields.add(field);
            }
        }
        Collections.sort(visibleFields);
        return Collections.unmodifiableCollection(visibleFields);
    }

    /**
     * Returns the a list of pairs where each pair contains a Custom Field and an indication whether that field is visible for a passed issue.
     *
     * @param issue the issue in the query
     * @return a list of pairs containing a Custom Field and a visibility flag. The Custom Fields are in the context of the passed issue.
     */
    public Iterable<Pair<OrderableField, Boolean>> getAllCustomFieldsWithOnAllScreensFlag(Issue issue) {
        Iterable<OrderableField> customFields = getAllCustomFields(issue);
        final Iterable<FieldScreen> screens = screenFinder.getIssueScreens(issue);
        final Set<Pair<OrderableField, Boolean>> fieldsWithFlags = Sets.newHashSet();
        for (OrderableField field : customFields) {
            boolean onAScreen = true;

            for (FieldScreen fieldScreen : screens) {
                onAScreen = fieldScreen.containsField(field.getId());
                if (!onAScreen) {
                    break;
                }
            }
            fieldsWithFlags.add(Pair.nicePairOf(field, onAScreen));
        }

        return fieldsWithFlags;
    }
}
