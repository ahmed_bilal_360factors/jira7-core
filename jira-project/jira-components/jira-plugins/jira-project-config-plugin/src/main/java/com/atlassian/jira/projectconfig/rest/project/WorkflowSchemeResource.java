package com.atlassian.jira.projectconfig.rest.project;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.project.ProjectAction;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.bc.workflow.WorkflowSchemeService;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.rest.WorkflowSchemeRestHelper;
import com.atlassian.jira.projectconfig.workflow.ProjectConfigWorkflowDispatcher;
import com.atlassian.jira.rest.api.util.ErrorCollection;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.task.TaskDescriptor;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.workflow.AssignableWorkflowScheme;
import com.atlassian.jira.workflow.DraftWorkflowScheme;
import com.atlassian.jira.workflow.migration.WorkflowSchemeMigrationTaskAccessor;
import com.atlassian.sal.api.websudo.WebSudoRequired;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.atlassian.jira.rest.api.http.CacheControl.never;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;

/**
 * REST resource to provide the JSON represention of the edit Workflow Scheme page.
 *
 * @since v5.2
 */
@Path("workflowscheme")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class WorkflowSchemeResource {
    private final JiraAuthenticationContext authContext;
    private final ProjectService projectService;
    private final ProjectConfigWorkflowDispatcher dispatcher;
    private final WorkflowSchemeService workflowSchemeService;
    private final WorkflowSchemeRestHelper helper;
    private final PermissionManager permissionManager;
    private final WorkflowSchemeMigrationTaskAccessor taskAccessor;

    public WorkflowSchemeResource(JiraAuthenticationContext authContext, ProjectService projectService,
                                  ProjectConfigWorkflowDispatcher dispatcher, WorkflowSchemeService workflowSchemeService,
                                  WorkflowSchemeRestHelper helper, PermissionManager permissionManager, WorkflowSchemeMigrationTaskAccessor taskAccessor) {
        this.authContext = authContext;
        this.projectService = projectService;
        this.dispatcher = dispatcher;
        this.workflowSchemeService = workflowSchemeService;
        this.helper = helper;
        this.permissionManager = permissionManager;
        this.taskAccessor = taskAccessor;
    }

    @GET
    @Path("{projectKey}")
    public Response getWorkflowSchemeInfo(@PathParam("projectKey") String projectKey, @QueryParam("original") boolean original) {
        ProjectService.GetProjectResult result = projectService.getProjectByKeyForAction(authContext.getLoggedInUser(), projectKey,
                ProjectAction.EDIT_PROJECT_CONFIG);

        if (result.getErrorCollection().hasAnyErrors()) {
            ErrorCollection errors = ErrorCollection.of(result.getErrorCollection());
            return Response.status(Response.Status.NOT_FOUND).entity(errors).cacheControl(never()).build();
        }

        return generateSchemeData(result.getProject(), original);
    }

    @WebSudoRequired
    @POST
    @Path("{projectKey}")
    public Response createDraftWorkflowSchemeForProject(@PathParam("projectKey") String projectKey) {
        ServiceOutcome<ProjectConfigWorkflowDispatcher.EditSchemeResult> editResult = dispatcher.editScheme(projectKey);
        if (editResult.isValid()) {
            ProjectConfigWorkflowDispatcher.EditSchemeResult returnedValue = editResult.getReturnedValue();
            return generateSchemeData(returnedValue.getProject(), returnedValue.getWorkflowScheme(), returnedValue.getDraftWorkflowScheme());
        } else {
            return WorkflowSchemeRestHelper.createErrorResponse(editResult);
        }
    }

    @WebSudoRequired
    @DELETE
    @Path("{projectKey}")
    public Response discardDraftWorkflowSchemeForProject(@PathParam("projectKey") String projectKey) {
        ApplicationUser user = authContext.getLoggedInUser();
        I18nHelper i18nHelper = authContext.getI18nHelper();

        ProjectService.GetProjectResult projectResult = projectService.getProjectByKeyForAction(user, projectKey, ProjectAction.EDIT_PROJECT_CONFIG);
        if (!projectResult.isValid()) {
            return WorkflowSchemeRestHelper.createErrorResponse(projectResult);
        }
        Project project = projectResult.getProject();

        TaskDescriptor taskDescriptor = taskAccessor.getActive(project);
        if (taskDescriptor != null) {
            return WorkflowSchemeRestHelper.createErrorResponse(BAD_REQUEST, "admin.project.workflows.migration.progress", i18nHelper);
        }

        ServiceOutcome<AssignableWorkflowScheme> workflowSchemeOutcome = workflowSchemeService.getSchemeForProject(user, project);
        if (!workflowSchemeOutcome.isValid()) {
            return WorkflowSchemeRestHelper.createErrorResponse(workflowSchemeOutcome);
        }
        AssignableWorkflowScheme scheme = workflowSchemeOutcome.getReturnedValue();

        ServiceOutcome<DraftWorkflowScheme> draftWorkflowSchemeOutcome = workflowSchemeService.getDraftWorkflowSchemeNotNull(user, scheme);
        if (!draftWorkflowSchemeOutcome.isValid()) {
            return WorkflowSchemeRestHelper.createErrorResponse(draftWorkflowSchemeOutcome);
        }
        DraftWorkflowScheme draftWorkflowScheme = draftWorkflowSchemeOutcome.getReturnedValue();

        ServiceOutcome<Void> deleteWorkflowSchemeOutcome = workflowSchemeService.deleteWorkflowScheme(user, draftWorkflowScheme);
        if (!deleteWorkflowSchemeOutcome.isValid()) {
            return WorkflowSchemeRestHelper.createErrorResponse(deleteWorkflowSchemeOutcome);
        }

        return generateSchemeData(project, scheme, null);
    }

    private Response generateSchemeData(Project project, boolean original) {
        ApplicationUser user = authContext.getLoggedInUser();

        ServiceOutcome<? extends AssignableWorkflowScheme> originalResult = workflowSchemeService.getSchemeForProject(user, project);
        if (!originalResult.isValid()) {
            return WorkflowSchemeRestHelper.createErrorResponse(originalResult);
        }
        final AssignableWorkflowScheme originalScheme = originalResult.getReturnedValue();


        WorkflowSchemeRestHelper.ResponseDataBuilder builder = helper.builder().setUser(user)
                .setProject(project).setAssignableWorkflowScheme(originalScheme);

        if (original || !permissionManager.hasPermission(Permissions.ADMINISTER, user)) {
            return builder.build();
        }

        ServiceOutcome<? extends DraftWorkflowScheme> draftResult = workflowSchemeService.getDraftWorkflowScheme(user, originalScheme);
        if (!draftResult.isValid()) {
            return WorkflowSchemeRestHelper.createErrorResponse(draftResult);
        } else {
            return builder.setDraftWorkflowScheme(draftResult.getReturnedValue()).build();
        }
    }

    private Response generateSchemeData(Project project, AssignableWorkflowScheme currentWorkflowScheme, DraftWorkflowScheme workflowScheme) {
        return helper.builder().setUser(authContext.getLoggedInUser())
                .setProject(project)
                .setAssignableWorkflowScheme(currentWorkflowScheme)
                .setDraftWorkflowScheme(workflowScheme)
                .build();
    }
}
