package com.atlassian.jira.projectconfig.event;

import com.atlassian.analytics.api.annotations.EventName;

@EventName("instance.statistics.workflows.event")
public class WorkflowsStatisticEvent {
    private long workflowsTotal;
    private long isolatedWorkflows;
    private long sharedWorkflows;
    private long inactiveWorkflows;

    public WorkflowsStatisticEvent(long workflowsTotal, long isolatedWorkflows, long sharedWorkflows, long inactiveWorkflows) {
        this.workflowsTotal = workflowsTotal;
        this.isolatedWorkflows = isolatedWorkflows;
        this.sharedWorkflows = sharedWorkflows;
        this.inactiveWorkflows = inactiveWorkflows;
    }

    public long getWorkflowsTotal() {
        return workflowsTotal;
    }

    public void setWorkflowsTotal(long workflowsTotal) {
        this.workflowsTotal = workflowsTotal;
    }

    public long getIsolatedWorkflows() {
        return isolatedWorkflows;
    }

    public void setIsolatedWorkflows(long isolatedWorkflows) {
        this.isolatedWorkflows = isolatedWorkflows;
    }

    public long getSharedWorkflows() {
        return sharedWorkflows;
    }

    public void setSharedWorkflows(long sharedWorkflows) {
        this.sharedWorkflows = sharedWorkflows;
    }

    public long getInactiveWorkflows() {
        return inactiveWorkflows;
    }

    public void setInactiveWorkflows(long inactiveWorkflows) {
        this.inactiveWorkflows = inactiveWorkflows;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WorkflowsStatisticEvent)) return false;

        WorkflowsStatisticEvent that = (WorkflowsStatisticEvent) o;

        if (workflowsTotal != that.workflowsTotal) return false;
        if (isolatedWorkflows != that.isolatedWorkflows) return false;
        if (sharedWorkflows != that.sharedWorkflows) return false;
        return inactiveWorkflows == that.inactiveWorkflows;

    }

    @Override
    public int hashCode() {
        int result = (int) (workflowsTotal ^ (workflowsTotal >>> 32));
        result = 31 * result + (int) (isolatedWorkflows ^ (isolatedWorkflows >>> 32));
        result = 31 * result + (int) (sharedWorkflows ^ (sharedWorkflows >>> 32));
        result = 31 * result + (int) (inactiveWorkflows ^ (inactiveWorkflows >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "WorkflowsStatisticEvent{" +
                "workflowsTotal=" + workflowsTotal +
                ", isolatedWorkflows=" + isolatedWorkflows +
                ", sharedWorkflows=" + sharedWorkflows +
                ", inactiveWorkflows=" + inactiveWorkflows +
                '}';
    }
}
