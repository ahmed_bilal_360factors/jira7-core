package com.atlassian.jira.projectconfig.util;

import com.atlassian.jira.project.Project;
import com.google.common.base.Function;
import com.google.common.collect.Ordering;

/**
 * @since v6.1
 */
public class Projects {
    private static final Function<Project, String> GET_NAME = new Function<Project, String>() {
        @Override
        public String apply(final Project input) {
            return input.getName();
        }
    };

    private Projects() {
    }

    public static Function<Project, String> getProjectName() {
        return GET_NAME;
    }

    public static Ordering<Project> nameOrder() {
        return Ordering.from(String.CASE_INSENSITIVE_ORDER)
                .onResultOf(Projects.getProjectName())
                .nullsLast();
    }
}
