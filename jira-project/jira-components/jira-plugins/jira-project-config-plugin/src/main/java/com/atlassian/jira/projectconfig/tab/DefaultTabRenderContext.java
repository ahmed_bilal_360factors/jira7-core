package com.atlassian.jira.projectconfig.tab;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.ExecutingHttpRequest;
import com.atlassian.jira.web.pagebuilder.PageBuilder;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.webresource.api.assembler.PageBuilderService;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

/**
 * @since v4.4
 */
public class DefaultTabRenderContext implements ProjectConfigTabRenderContext {
    private final Project project;
    private final String pathInfo;
    private final JiraAuthenticationContext authenticationContext;
    private final WebResourceManager webResourceManager;
    private final PageBuilderService pageBuilderService;

    public DefaultTabRenderContext(String pathInfo, Project project, JiraAuthenticationContext ctx, WebResourceManager webResourceManager, PageBuilderService pageBuilderService) {
        this.pathInfo = pathInfo;
        this.project = project;
        this.authenticationContext = ctx;
        this.webResourceManager = webResourceManager;
        this.pageBuilderService = pageBuilderService;
    }

    public Project getProject() {
        return project;
    }

    public HttpServletRequest getRequest() {
        return ExecutingHttpRequest.get();
    }

    public String getPathInfo() {
        return pathInfo;
    }

    public I18nHelper getI18NHelper() {
        return authenticationContext.getI18nHelper();
    }

    @Override
    public WebResourceManager getResourceManager() {
        return webResourceManager;
    }

    public Locale getLocale() {
        return authenticationContext.getLocale();
    }

    public ApplicationUser getUser() {
        return authenticationContext.getLoggedInUser();
    }

    @Override
    public PageBuilderService getPageBuilderService() {
        return pageBuilderService;
    }
}
