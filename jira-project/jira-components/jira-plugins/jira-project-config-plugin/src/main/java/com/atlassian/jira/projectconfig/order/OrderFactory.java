package com.atlassian.jira.projectconfig.order;

import java.util.Comparator;

import com.atlassian.jira.issue.IssueConstant;
import com.atlassian.jira.projectconfig.beans.NamedDefault;
import com.atlassian.jira.projectconfig.beans.SimpleIssueType;

import com.google.common.collect.Ordering;

/**
 * Factory class for some of the common orders used in project ignite.
 *
 * @since v4.4
 */
public interface OrderFactory {
    public Comparator<String> createStringComparator();

    public Comparator<NamedDefault> createNamedDefaultComparator();

    public Comparator<SimpleIssueType> createIssueTypeComparator();

    public Ordering<IssueConstant> createTranslatedNameOrder();
}
