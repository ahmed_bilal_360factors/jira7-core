package com.atlassian.jira.projectconfig.rest.project;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.project.ProjectAction;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.status.SimpleStatus;
import com.atlassian.jira.issue.status.category.StatusCategory;
import com.atlassian.jira.projectconfig.workflow.ProjectConfigWorkflowDispatcher;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.lang.Pair;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowActionsBean;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.WorkflowSchemeManager;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.opensymphony.workflow.loader.ActionDescriptor;
import com.opensymphony.workflow.loader.StepDescriptor;
import com.opensymphony.workflow.loader.WorkflowDescriptor;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.codehaus.jackson.annotate.JsonAutoDetect;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.atlassian.jira.rest.api.http.CacheControl.never;
import static com.atlassian.jira.rest.api.util.ErrorCollection.of;
import static org.apache.commons.lang3.StringUtils.stripToNull;

/**
 * Called by project ignite panels to deal with workflows.
 *
 * @since v5.1
 */
@Path("workflow")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class WorkflowResource {
    private final ProjectConfigWorkflowDispatcher wfDispatcher;
    private final WorkflowManager workflowManager;
    private final JiraAuthenticationContext authenticationContext;
    private final PermissionManager permissionManager;
    private final ProjectService service;
    private final WorkflowSchemeManager workflowSchemeManager;

    public WorkflowResource(ProjectConfigWorkflowDispatcher wfDispatcher, WorkflowManager workflowManager,
                            JiraAuthenticationContext authenticationContext, PermissionManager permissionManager,
                            ProjectService service, WorkflowSchemeManager workflowSchemeManager) {
        this.wfDispatcher = wfDispatcher;
        this.workflowManager = workflowManager;
        this.authenticationContext = authenticationContext;
        this.permissionManager = permissionManager;
        this.service = service;
        this.workflowSchemeManager = workflowSchemeManager;
    }

    @POST
    @WebSudoRequired
    public Response editWorkflowDispatcher(long projectId) {
        final ServiceOutcome<Pair<String, Long>> outcome = wfDispatcher.editWorkflow(projectId);
        if (outcome.isValid()) {
            Pair<String, Long> pair = outcome.getReturnedValue();
            return Response.ok(new EditWorkflowResult(pair.first(), pair.second())).cacheControl(never()).build();
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).entity(of(outcome.getErrorCollection()))
                    .cacheControl(never()).build();
        }
    }

    @GET
    public Response getWorkflow(@QueryParam("workflowName") String workflowName,
                                @QueryParam("projectKey") String projectKey) {
        workflowName = stripToNull(workflowName);
        if (workflowName == null) {
            return Response.status(Response.Status.BAD_REQUEST).cacheControl(never()).build();
        }

        Response response = hasPermission(projectKey, workflowName);
        if (response != null) {
            return response;
        }

        JiraWorkflow workflow = workflowManager.getWorkflow(workflowName);
        if (workflow == null) {
            return Response.status(Response.Status.NOT_FOUND).cacheControl(never()).build();
        }

        return Response.ok(createWorkflowResponse(workflow)).cacheControl(never()).build();
    }

    WorkflowResponse createWorkflowResponse(JiraWorkflow workflow) {
        WorkflowActionsBean bean = createActionsBean();
        WorkflowDescriptor descriptor = workflow.getDescriptor();
        List<StepDescriptor> steps = descriptor.getSteps();
        List<SimpleWorkflowSource> sources = new ArrayList<SimpleWorkflowSource>(steps.size());

        for (StepDescriptor step : steps) {
            List<ActionDescriptor> actions = step.getActions();
            List<SimpleWorkflowTarget> targets = new ArrayList<SimpleWorkflowTarget>(actions.size());
            SimpleStatus sourceStatus = workflow.getLinkedStatusObject(step).getSimpleStatus();

            for (ActionDescriptor action : actions) {
                int targetStepId = action.getUnconditionalResult().getStep();
                final SimpleStatus targetStatus;
                if (targetStepId == JiraWorkflow.ACTION_ORIGIN_STEP_ID) {
                    targetStatus = sourceStatus;
                } else {
                    targetStatus = workflow.getLinkedStatusObject(descriptor.getStep(targetStepId)).getSimpleStatus();
                }

                FieldScreen fieldScreen = bean.getFieldScreenForView(action);
                SimpleScreen transitionScreen = null;
                if (fieldScreen != null) {
                    transitionScreen = new SimpleScreen(fieldScreen);
                }

                targets.add(new SimpleWorkflowTarget(status(targetStatus), action.getName(), transitionScreen));
            }
            sources.add(new SimpleWorkflowSource(status(sourceStatus), targets));
        }

        boolean isAdmin = permissionManager.hasPermission(Permissions.ADMINISTER, authenticationContext.getLoggedInUser());
        return new WorkflowResponse(descriptor.getEntityId(), workflow.getName(), workflow.getDisplayName(), workflow.getDescription(), sources, isAdmin);
    }

    Response hasPermission(String projectKey, String workflowName) {
        final ApplicationUser loggedInUser = authenticationContext.getLoggedInUser();
        if (permissionManager.hasPermission(Permissions.ADMINISTER, loggedInUser)) {
            return null;
        }

        projectKey = stripToNull(projectKey);
        if (projectKey == null) {
            return Response.status(Response.Status.BAD_REQUEST).cacheControl(never()).build();
        }

        final ProjectService.GetProjectResult result
                = service.getProjectByKeyForAction(loggedInUser, projectKey, ProjectAction.EDIT_PROJECT_CONFIG);
        if (!result.isValid() || result.getProject() == null) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(result.getProject()).cacheControl(never()).build();
        }

        final Map<String, String> workflowMap = workflowSchemeManager.getWorkflowMap(result.getProject());
        for (IssueType types : result.getProject().getIssueTypes()) {
            String workflow = workflowMap.get(types.getId());
            if (workflow == null) {
                workflow = workflowMap.get(null);
                if (workflow == null) {
                    workflow = JiraWorkflow.DEFAULT_WORKFLOW_NAME;
                }
            }

            if (workflow.equals(workflowName)) {
                return null;
            }
        }
        return Response.status(Response.Status.NOT_FOUND).cacheControl(never()).build();
    }

    WorkflowActionsBean createActionsBean() {
        return new WorkflowActionsBean();
    }

    @JsonAutoDetect
    public static class EditWorkflowResult {
        private final String workflowName;
        private final Long taskId;

        EditWorkflowResult(String workflowName, Long taskId) {
            this.workflowName = workflowName;
            this.taskId = taskId;
        }

        public String getWorkflowName() {
            return workflowName;
        }

        public Long getTaskId() {
            return taskId;
        }
    }

    @JsonAutoDetect
    public static class WorkflowResponse {
        private final String name;
        private final String description;
        private final List<SimpleWorkflowSource> sources;
        private final Integer id;
        private final boolean isAdmin;
        private final String displayName;

        WorkflowResponse(Integer id, String name, String displayName, String description, List<SimpleWorkflowSource> sources, boolean isAdmin) {
            this.id = id;
            this.displayName = displayName;
            this.description = description;
            this.name = name;
            this.sources = sources;
            this.isAdmin = isAdmin;
        }

        public String getName() {
            return name;
        }

        public String getDescription() {
            return description;
        }

        public List<SimpleWorkflowSource> getSources() {
            return sources;
        }

        public Integer getId() {
            return id;
        }

        public boolean isAdmin() {
            return isAdmin;
        }

        public String getDisplayName() {
            return displayName;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            WorkflowResponse that = (WorkflowResponse) o;

            if (isAdmin != that.isAdmin) {
                return false;
            }
            if (description != null ? !description.equals(that.description) : that.description != null) {
                return false;
            }
            if (displayName != null ? !displayName.equals(that.displayName) : that.displayName != null) {
                return false;
            }
            if (id != null ? !id.equals(that.id) : that.id != null) {
                return false;
            }
            if (name != null ? !name.equals(that.name) : that.name != null) {
                return false;
            }
            if (sources != null ? !sources.equals(that.sources) : that.sources != null) {
                return false;
            }

            return true;
        }

        @Override
        public int hashCode() {
            int result = name != null ? name.hashCode() : 0;
            result = 31 * result + (description != null ? description.hashCode() : 0);
            result = 31 * result + (sources != null ? sources.hashCode() : 0);
            result = 31 * result + (id != null ? id.hashCode() : 0);
            result = 31 * result + (isAdmin ? 1 : 0);
            result = 31 * result + (displayName != null ? displayName.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).
                    append("description", description).
                    append("name", name).
                    append("sources", sources).
                    append("id", id).
                    append("isAdmin", isAdmin).
                    toString();
        }
    }

    @JsonAutoDetect
    public static class SimpleWorkflowSource {
        private final DelegatedJsonSimpleStatus fromStatus;
        private final List<SimpleWorkflowTarget> targets;

        SimpleWorkflowSource(DelegatedJsonSimpleStatus fromStatus, List<SimpleWorkflowTarget> targets) {
            this.fromStatus = fromStatus;
            this.targets = targets;
        }

        public DelegatedJsonSimpleStatus getFromStatus() {
            return fromStatus;
        }

        public List<SimpleWorkflowTarget> getTargets() {
            return targets;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            SimpleWorkflowSource that = (SimpleWorkflowSource) o;

            if (fromStatus != null ? !fromStatus.equals(that.fromStatus) : that.fromStatus != null) {
                return false;
            }
            if (targets != null ? !targets.equals(that.targets) : that.targets != null) {
                return false;
            }

            return true;
        }

        @Override
        public int hashCode() {
            int result = fromStatus != null ? fromStatus.hashCode() : 0;
            result = 31 * result + (targets != null ? targets.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).
                    append("fromStatus", fromStatus).
                    append("targets", targets).
                    toString();
        }
    }

    @JsonAutoDetect
    public static class SimpleWorkflowTarget {
        private final DelegatedJsonSimpleStatus toStatus;
        private final String transitionName;
        private final SimpleScreen screen;

        SimpleWorkflowTarget(DelegatedJsonSimpleStatus toStatus, String transitionName, SimpleScreen screen) {
            this.screen = screen;
            this.toStatus = toStatus;
            this.transitionName = transitionName;
        }

        SimpleWorkflowTarget(DelegatedJsonSimpleStatus toStatus, String transitionName, FieldScreen screen) {
            this(toStatus, transitionName, screen == null ? null : new SimpleScreen(screen));
        }

        public SimpleScreen getScreen() {
            return screen;
        }

        public DelegatedJsonSimpleStatus getToStatus() {
            return toStatus;
        }

        public String getTransitionName() {
            return transitionName;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            SimpleWorkflowTarget that = (SimpleWorkflowTarget) o;

            if (screen != null ? !screen.equals(that.screen) : that.screen != null) {
                return false;
            }
            if (toStatus != null ? !toStatus.equals(that.toStatus) : that.toStatus != null) {
                return false;
            }
            if (transitionName != null ? !transitionName.equals(that.transitionName) : that.transitionName != null) {
                return false;
            }

            return true;
        }

        @Override
        public int hashCode() {
            int result = toStatus != null ? toStatus.hashCode() : 0;
            result = 31 * result + (transitionName != null ? transitionName.hashCode() : 0);
            result = 31 * result + (screen != null ? screen.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).
                    append("screen", screen).
                    append("toStatus", toStatus).
                    append("transitionName", transitionName).
                    toString();
        }
    }

    private static DelegatedJsonSimpleStatus status(SimpleStatus simpleStatus) {
        return new DelegatedJsonSimpleStatus(simpleStatus);
    }

    @JsonAutoDetect
    public static class DelegatedJsonSimpleStatus implements SimpleStatus {
        private final SimpleStatus delegate;
        private final DelegatedJsonStatusCategory statusCategory;

        public DelegatedJsonSimpleStatus(final SimpleStatus delegate) {
            this.delegate = delegate;
            if (delegate.getStatusCategory() != null) {
                statusCategory = new DelegatedJsonStatusCategory(delegate.getStatusCategory());
            } else {
                statusCategory = null;
            }
        }

        @Override
        public String getId() {
            return delegate.getId();
        }

        @Override
        public String getName() {
            return delegate.getName();
        }

        @Override
        public String getDescription() {
            return delegate.getDescription();
        }

        @Override
        public StatusCategory getStatusCategory() {
            return statusCategory;
        }

        @Override
        public String getIconUrl() {
            return delegate.getIconUrl();
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            final DelegatedJsonSimpleStatus that = (DelegatedJsonSimpleStatus) o;

            if (delegate != null ? !delegate.equals(that.delegate) : that.delegate != null) {
                return false;
            }

            return true;
        }

        @Override
        public int hashCode() {
            return delegate != null ? delegate.hashCode() : 0;
        }
    }

    @JsonAutoDetect
    public static class DelegatedJsonStatusCategory implements StatusCategory {
        private final StatusCategory statusCategory;

        public DelegatedJsonStatusCategory(final StatusCategory statusCategory) {
            this.statusCategory = statusCategory;
        }

        @Override
        public String getTranslatedName() {
            return statusCategory.getTranslatedName();
        }

        @Override
        public String getTranslatedName(final I18nHelper i18n) {
            return statusCategory.getTranslatedName(i18n);
        }

        @Override
        public String getTranslatedName(final String locale) {
            return statusCategory.getTranslatedName(locale);
        }

        @Override
        public Long getId() {
            return statusCategory.getId();
        }

        @Override
        public String getKey() {
            return statusCategory.getKey();
        }

        @Override
        public String getName() {
            return statusCategory.getName();
        }

        @Override
        public List<String> getAliases() {
            return statusCategory.getAliases();
        }

        @Override
        public String getPrimaryAlias() {
            return statusCategory.getPrimaryAlias();
        }

        @Override
        public String getColorName() {
            return statusCategory.getColorName();
        }

        @Override
        public Long getSequence() {
            return statusCategory.getSequence();
        }

        @Override
        public int compareTo(final StatusCategory o) {
            return statusCategory.compareTo(o);
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            final DelegatedJsonStatusCategory that = (DelegatedJsonStatusCategory) o;

            if (statusCategory != null ? !statusCategory.equals(that.statusCategory) : that.statusCategory != null) {
                return false;
            }

            return true;
        }

        @Override
        public int hashCode() {
            return statusCategory != null ? statusCategory.hashCode() : 0;
        }
    }

    @JsonAutoDetect
    public static class SimpleScreen {
        private final Long id;
        private final String name;

        SimpleScreen(FieldScreen screen) {
            this.id = screen.getId();
            this.name = screen.getName();
        }

        public Long getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            SimpleScreen that = (SimpleScreen) o;

            if (id != null ? !id.equals(that.id) : that.id != null) {
                return false;
            }
            if (name != null ? !name.equals(that.name) : that.name != null) {
                return false;
            }

            return true;
        }

        @Override
        public int hashCode() {
            int result = id != null ? id.hashCode() : 0;
            result = 31 * result + (name != null ? name.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).
                    append("id", id).
                    append("name", name).
                    toString();
        }
    }
}
