package com.atlassian.jira.projectconfig.rest.global;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.customfield.CustomFieldService;
import com.atlassian.jira.issue.context.IssueContext;
import com.atlassian.jira.issue.customfields.manager.OptionsService;
import com.atlassian.jira.issue.customfields.option.SimpleOption;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.projectconfig.rest.AdminRequired;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Collections;
import java.util.List;
import javax.annotation.Nonnull;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.atlassian.jira.rest.api.http.CacheControl.never;

/**
 * @since v6.1
 */
@AdminRequired
@WebSudoRequired
@Path("customfieldoptions")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class CustomFieldOptionsResource {
    private final OptionsService optionsService;
    private final CustomFieldService customFieldService;
    private final JiraAuthenticationContext context;

    public CustomFieldOptionsResource(final CustomFieldService customFieldManager,
                                      OptionsService optionsService, JiraAuthenticationContext context) {
        this.optionsService = optionsService;
        this.customFieldService = customFieldManager;
        this.context = context;
    }

    @Path("{customFieldId}")
    @POST
    public Response setOptions(@PathParam("customFieldId") String customFieldId, List<Option> request) {
        if (customFieldId == null) {
            return badRequest();
        }

        if (request == null) {
            request = Collections.emptyList();
        }

        final ServiceOutcome<CustomField> editConfig = customFieldService.getCustomFieldForEditConfig(context.getUser(), customFieldId);
        if (!editConfig.isValid()) {
            return buildResponseFrom(editConfig);
        }

        final OptionsService.SetOptionParams update = new OptionsService.SetOptionParams()
                .user(context.getUser())
                .customField(editConfig.getReturnedValue(), IssueContext.GLOBAL)
                .option(request);

        final ServiceOutcome<OptionsService.SetValidateResult> optionsOutcome = optionsService.validateSetOptions(update);
        if (!optionsOutcome.isValid()) {
            return buildResponseFrom(optionsOutcome);
        }

        return buildResponseFrom(optionsService.setOptions(optionsOutcome.getReturnedValue()));
    }

    private Response buildResponseFrom(final ServiceOutcome<?> response) {
        final ErrorCollection collection = response.getErrorCollection();
        if (collection.hasAnyErrors()) {
            ErrorCollection.Reason reason = ErrorCollection.Reason.getWorstReason(collection.getReasons());
            if (reason == null) {
                reason = ErrorCollection.Reason.VALIDATION_FAILED;
            }
            return Response.status(reason.getHttpStatusCode())
                    .entity(com.atlassian.jira.rest.api.util.ErrorCollection.of(collection))
                    .cacheControl(never()).build();
        } else {
            return Response.status(Response.Status.NO_CONTENT).cacheControl(never()).build();
        }
    }

    private Response badRequest() {
        return Response.status(Response.Status.BAD_REQUEST).cacheControl(never()).build();
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Option implements SimpleOption<Option> {
        private final String name;
        private final Long id;

        @JsonCreator
        public Option(@JsonProperty("id") final Long id,
                      @JsonProperty("name") final String name) {
            this.id = id;
            this.name = name;
        }

        public Long getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        @Override
        public Long getOptionId() {
            return getId();
        }

        @Override
        public String getValue() {
            return getName();
        }

        @Nonnull
        @Override
        public List<Option> getChildOptions() {
            return Collections.emptyList();
        }
    }
}
