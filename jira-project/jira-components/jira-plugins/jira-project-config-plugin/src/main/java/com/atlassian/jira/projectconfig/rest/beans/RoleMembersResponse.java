package com.atlassian.jira.projectconfig.rest.beans;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

@JsonAutoDetect
public class RoleMembersResponse {

    @JsonProperty
    private Long id;

    @JsonProperty
    private String name;

    @JsonProperty
    private long total;

    @JsonProperty
    private List<UserResponse> users;

    @JsonProperty
    private List<GroupResponse> groups;

    private RoleMembersResponse() {
    }

    public RoleMembersResponse(Long id, String name, long total, List<GroupResponse> groups, List<UserResponse> users) {
        this.id = id;
        this.name = name;
        this.total = total;
        this.users = users;
        this.groups = groups;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public long getTotal() {
        return total;
    }

    public List<UserResponse> getUsers() {
        return users;
    }

    public List<GroupResponse> getGroups() {
        return groups;
    }
}
