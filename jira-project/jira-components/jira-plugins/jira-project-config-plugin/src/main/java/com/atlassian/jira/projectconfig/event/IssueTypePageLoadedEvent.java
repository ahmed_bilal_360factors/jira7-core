package com.atlassian.jira.projectconfig.event;

import com.atlassian.analytics.api.annotations.EventName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * An analytics event to indicate when an issue type config page is shown through project configuration.
 *
 * @since v6.2
 */
@EventName("administration.issuetype.viewed.project")
public class IssueTypePageLoadedEvent {
    // using EqualsBuilder and HashCodeBuilder here as they are used in tests only
    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(this, o);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
}
