package com.atlassian.jira.projectconfig.contextproviders;

import com.atlassian.jira.admin.ProjectAdminSidebarFeature;
import com.atlassian.jira.plugin.webfragment.JiraWebInterfaceManager;
import com.atlassian.jira.plugin.webfragment.descriptors.JiraWebItemModuleDescriptor;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.plugin.webfragment.model.JiraWebLabel;
import com.atlassian.jira.plugin.webfragment.model.JiraWebLink;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.type.ProjectTypeIconRenderer;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.project.type.ProjectTypeKeyFormatter;
import com.atlassian.jira.project.type.warning.InaccessibleProjectTypeDialogDataProvider;
import com.atlassian.jira.projectconfig.beans.SimpleProject;
import com.atlassian.jira.projectconfig.tab.SummaryTab;
import com.atlassian.jira.projectconfig.tab.WebPanelTab;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserUtils;
import com.atlassian.jira.util.JiraVelocityUtils;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.web.ExecutingHttpRequest;
import com.atlassian.jira.web.sitemesh.AdminDecoratorHelper;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.web.ContextProvider;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.descriptors.WebItemModuleDescriptor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Context provider for the header.
 *
 * @since v4.4
 */
public class HeaderContextProvider implements ContextProvider {
    // Keys for context items provided in the default context
    static final String CONTEXT_SIDEBAR_PRESENT = "isSidebarPresent";
    static final String CONTEXT_PROJECT_KEY = "project";
    static final String CONTEXT_IS_ADMIN_KEY = "isAdmin";
    static final String CONTEXT_I18N_KEY = "i18n";
    static final String CONTEXT_SIMPLE_PROJECT_KEY = "simpleProject";
    static final String CONTEXT_VIEW_PROJECT_OPERATIONS_KEY = "viewableProjectOperations";
    static final String CONTEXT_PROJECT_LEAD_EXISTS_KEY = "projectLeadExists";
    static final String CURRENT_TAB = AdminDecoratorHelper.ACTIVE_TAB_LINK_KEY;
    static final String SHOW_ACTIONS_MENU = "showActionsMenu";
    static final String PROJECT_TYPE_KEY = "projectTypeKey";
    static final String PROJECT_TYPE_NAME = "projectTypeName";
    static final String PROJECT_TYPE_ICON = "projectTypeIcon";
    static final String SHOULD_DISPLAY_PROJECT_TYPE_WARNING = "displayProjectTypeWarning";

    private final JiraWebInterfaceManager jiraWebInterfaceManager;
    private final ContextProviderHelper contextProviderHelper;
    private final JiraAuthenticationContext authenticationContext;
    private final ProjectTypeIconRenderer projectTypeIconRenderer;
    private final InaccessibleProjectTypeDialogDataProvider inaccessibleProjectTypeDialogDataProvider;
    private final ProjectAdminSidebarFeature projectAdminSidebarFeature;

    public HeaderContextProvider(
            final WebInterfaceManager webInterfaceManager,
            final ContextProviderHelper contextProviderHelper,
            @ComponentImport final JiraAuthenticationContext authenticationContext,
            @ComponentImport final ProjectTypeIconRenderer projectTypeIconRenderer,
            @ComponentImport final InaccessibleProjectTypeDialogDataProvider inaccessibleProjectTypeDialogDataProvider,
            @ComponentImport final ProjectAdminSidebarFeature projectAdminSidebarFeature) {
        this.jiraWebInterfaceManager = new JiraWebInterfaceManager(webInterfaceManager);
        this.contextProviderHelper = contextProviderHelper;
        this.authenticationContext = authenticationContext;
        this.projectTypeIconRenderer = projectTypeIconRenderer;
        this.inaccessibleProjectTypeDialogDataProvider = inaccessibleProjectTypeDialogDataProvider;
        this.projectAdminSidebarFeature = projectAdminSidebarFeature;
    }

    public void init(Map<String, String> params) throws PluginParseException {
        //Nothing to do.
    }

    public Map<String, Object> getContextMap(Map<String, Object> context) {
        final MapBuilder<String, Object> contextBuilder = MapBuilder.newBuilder(context);

        ApplicationUser user = contextProviderHelper.getAuthenticationContext().getLoggedInUser();

        final Project project = (Project) context.get("project");
        final SimpleProject wrappedProject = new SimpleProject(project);

        addProjectTypeInformation(contextBuilder, project);

        contextBuilder.add(CONTEXT_SIDEBAR_PRESENT, projectAdminSidebarFeature.shouldDisplay());
        contextBuilder.add(SHOW_ACTIONS_MENU, isSummaryTab(context));
        contextBuilder.add(CONTEXT_SIMPLE_PROJECT_KEY, wrappedProject);
        contextBuilder.add(CONTEXT_PROJECT_LEAD_EXISTS_KEY, UserUtils.userExists(project.getLeadUserName()));
        contextBuilder.add(CONTEXT_IS_ADMIN_KEY, contextProviderHelper.hasAdminPermission());
        contextBuilder.add(CONTEXT_I18N_KEY, contextProviderHelper.getI18nHelper());

        addOperations(user, project, contextBuilder);

        return createContext(contextBuilder.toMap());
    }

    private void addProjectTypeInformation(MapBuilder<String, Object> contextBuilder, Project project) {
        ProjectTypeKey projectTypeKey = project.getProjectTypeKey();
        contextBuilder.add(PROJECT_TYPE_KEY, projectTypeKey.getKey());
        contextBuilder.add(PROJECT_TYPE_NAME, ProjectTypeKeyFormatter.format(projectTypeKey));
        contextBuilder.add(PROJECT_TYPE_ICON, projectTypeIconRenderer.getIconToRender(projectTypeKey).orElse(null));
        contextBuilder.add(SHOULD_DISPLAY_PROJECT_TYPE_WARNING, shouldDisplayProjectTypeWarning(project));
    }

    private boolean shouldDisplayProjectTypeWarning(Project project) {
        ApplicationUser loggedInUser = authenticationContext.getLoggedInUser();
        return inaccessibleProjectTypeDialogDataProvider.shouldDisplayInaccessibleWarning(loggedInUser, project);
    }

    private boolean isSummaryTab(Map<String, Object> context) {
        return "view_project_summary".equals(context.get(CURRENT_TAB))
                || SummaryTab.NAME.equals(context.get(WebPanelTab.CURRENT_TAB_NAME));
    }

    private void addOperations(ApplicationUser user, Project project, MapBuilder<String, Object> ctx) {
        final JiraHelper jiraHelper = getJiraHelper(project);

        final List<WebItemModuleDescriptor> displayableItems =
                jiraWebInterfaceManager.getDisplayableItems("system.view.project.operations", user, jiraHelper);

        final List<SimpleViewableProjectOperation> ops = new ArrayList<>();
        for (WebItemModuleDescriptor displayableItem : displayableItems) {
            JiraWebItemModuleDescriptor jiraDisplayableItem = (JiraWebItemModuleDescriptor) displayableItem;
            final SimpleViewableProjectOperation operation = createViewableProjectOperation(jiraDisplayableItem, user, jiraHelper);
            ops.add(operation);
        }

        ctx.add(CONTEXT_VIEW_PROJECT_OPERATIONS_KEY, ops);
    }

    private SimpleViewableProjectOperation createViewableProjectOperation(final JiraWebItemModuleDescriptor displayableItem,
                                                                          final ApplicationUser user, final JiraHelper jiraHelper) {
        final JiraWebLink link = (JiraWebLink) displayableItem.getLink();
        final JiraWebLabel jiraWebLabel = (JiraWebLabel) displayableItem.getLabel();

        final String displayableUrl = getUrlEncodedRenderedUrl(link.getRenderedUrl(user, jiraHelper));
        final String displayableLabel = jiraWebLabel.getDisplayableLabel(user, jiraHelper);

        return new SimpleViewableProjectOperation(link.getId(), displayableUrl, displayableLabel, displayableItem.getStyleClass());
    }

    JiraHelper getJiraHelper(Project project) {
        return new JiraHelper(ExecutingHttpRequest.get(), project);
    }

    String getUrlEncodedRenderedUrl(final String renderedUrl) {
        return contextProviderHelper.createUrlBuilder(renderedUrl).asUrlString();
    }

    Map<String, Object> createContext(Map<String, Object> params) {
        return JiraVelocityUtils.getDefaultVelocityParams(params, contextProviderHelper.getAuthenticationContext());
    }

    public static class SimpleViewableProjectOperation {
        private final String linkId;
        private final String displayableUrl;
        private final String displayableLabel;
        private final String styleClass;

        public SimpleViewableProjectOperation(final String linkId, final String displayableUrl, final String displayableLabel, String styleClass) {
            this.linkId = linkId;
            this.displayableUrl = displayableUrl;
            this.displayableLabel = displayableLabel;
            this.styleClass = styleClass;
        }

        public String getLinkId() {
            return linkId;
        }

        public String getDisplayableUrl() {
            return displayableUrl;
        }

        public String getDisplayableLabel() {
            return displayableLabel;
        }

        public String getStyleClass() {
            return styleClass;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            SimpleViewableProjectOperation that = (SimpleViewableProjectOperation) o;

            if (displayableLabel != null ? !displayableLabel.equals(that.displayableLabel) : that.displayableLabel != null) {
                return false;
            }
            if (styleClass != null ? !styleClass.equals(that.styleClass) : that.styleClass != null) {
                return false;
            }
            if (displayableUrl != null ? !displayableUrl.equals(that.displayableUrl) : that.displayableUrl != null) {
                return false;
            }
            if (linkId != null ? !linkId.equals(that.linkId) : that.linkId != null) {
                return false;
            }

            return true;
        }

        @Override
        public int hashCode() {
            int result = linkId != null ? linkId.hashCode() : 0;
            result = 31 * result + (displayableUrl != null ? displayableUrl.hashCode() : 0);
            result = 31 * result + (displayableLabel != null ? displayableLabel.hashCode() : 0);
            result = 31 * result + (styleClass != null ? styleClass.hashCode() : 0);
            return result;
        }
    }
}
