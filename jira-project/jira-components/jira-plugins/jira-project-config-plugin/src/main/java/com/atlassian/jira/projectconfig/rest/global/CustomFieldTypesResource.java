package com.atlassian.jira.projectconfig.rest.global;

import com.atlassian.jira.bc.customfield.CustomFieldService;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.customfields.CustomFieldSearcher;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.customfields.CustomFieldTypeCategory;
import com.atlassian.jira.issue.customfields.config.item.SettableOptionsConfigItem;
import com.atlassian.jira.issue.customfields.impl.CascadingSelectCFType;
import com.atlassian.jira.projectconfig.rest.AdminRequired;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Function;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.atlassian.jira.rest.api.http.CacheControl.never;

/**
 * @since v6.1
 */
@AdminRequired
@WebSudoRequired
@Path("customfieldtypes")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class CustomFieldTypesResource {
    private static final String RESOURCE_TYPE = "download";

    private final CustomFieldService customFieldService;
    private final CustomFieldManager customFieldManager;
    private final JiraAuthenticationContext jac;
    private final WebResourceUrlProvider provider;

    public CustomFieldTypesResource(final CustomFieldService customFieldService, CustomFieldManager customFieldManager,
                                    JiraAuthenticationContext jac, WebResourceUrlProvider provider) {
        this.customFieldService = customFieldService;
        this.customFieldManager = customFieldManager;
        this.jac = jac;
        this.provider = provider;
    }

    @GET
    public Response getTypes() {
        final ApplicationUser user = jac.getUser();
        final List<ResponseBean.CustomFieldTypeBean> fieldTypeBeans = Lists.newArrayList();
        final I18nHelper i18nHelper = jac.getI18nHelper();
        for (CustomFieldType<?, ?> fieldType : customFieldService.getCustomFieldTypesForUser(user)) {
            final ResponseBean.CustomFieldTypeBean bean = new ResponseBean.CustomFieldTypeBean(fieldType);
            bean.setManagedDescription(i18nHelper.getText(fieldType.getDescriptor().getManagedDescriptionKey()));
            bean.setPreviewImageUrl(getThumbUrl(fieldType));
            bean.setOptions(hasOptions(fieldType));
            bean.setCascading(isCascading(fieldType));
            bean.setSearchers(getSearchers(fieldType));
            fieldTypeBeans.add(bean);
        }

        return Response.ok(new ResponseBean(fieldTypeBeans, i18nHelper)).cacheControl(never()).build();
    }

    @VisibleForTesting
    boolean isCascading(final CustomFieldType<?, ?> type) {
        return type instanceof CascadingSelectCFType;
    }

    @VisibleForTesting
    boolean hasOptions(final CustomFieldType<?, ?> type) {
        return Iterables.any(type.getConfigurationItemTypes(), Predicates.instanceOf(SettableOptionsConfigItem.class));
    }

    private String getThumbUrl(final CustomFieldType<?, ?> fieldType) {
        ResourceDescriptor imageUrl = fieldType.getDescriptor().getResourceDescriptor(RESOURCE_TYPE, CustomFieldType.RESOURCE_PREVIEW);
        String fieldPreviewImageUrl = null;
        if (imageUrl != null) {
            fieldPreviewImageUrl = provider.getStaticPluginResourceUrl(fieldType.getDescriptor(), CustomFieldType.RESOURCE_PREVIEW, UrlMode.RELATIVE);
        }
        return fieldPreviewImageUrl;
    }

    private Iterable<String> getSearchers(CustomFieldType<?, ?> type) {
        return Iterables.transform(customFieldManager.getCustomFieldSearchers(type), new Function<CustomFieldSearcher, String>() {
            @Override
            public String apply(final CustomFieldSearcher input) {
                return input.getDescriptor().getCompleteKey();
            }
        });
    }

    public static class ResponseBean {
        private final Iterable<CategoryBean> categories;
        private final Iterable<CustomFieldTypeBean> types;

        private ResponseBean(final Iterable<CustomFieldTypeBean> types, I18nHelper helper) {
            this.categories = CategoryBean.toBeans(helper);
            this.types = types;
        }

        @JsonProperty
        public Iterable<CategoryBean> getCategories() {
            return categories;
        }

        @JsonProperty
        public Iterable<CustomFieldTypeBean> getTypes() {
            return types;
        }

        public static class CategoryBean {
            private final String id;
            private final String name;

            private CategoryBean(final CustomFieldTypeCategory category, I18nHelper helper) {
                this.id = category.name(); // e.g. "ADVANCED"
                this.name = helper.getText(category.getNameI18nKey());
            }

            @JsonProperty
            public String getId() {
                return id;
            }

            @JsonProperty
            public String getName() {
                return name;
            }

            public static Iterable<CategoryBean> toBeans(final I18nHelper helper) {
                List<CategoryBean> list = Lists.newArrayList();
                for (CustomFieldTypeCategory category : CustomFieldTypeCategory.orderedValues()) {
                    list.add(new CategoryBean(category, helper));
                }
                return Collections.unmodifiableList(list);
            }
        }

        public static class CustomFieldTypeBean {
            private String name;
            private String description;
            private String key;
            private String previewImageUrl;
            private Set<CustomFieldTypeCategory> categories;
            private boolean isManaged;
            private String managedDescription;
            private boolean options;
            private boolean cascading;
            private Iterable<String> searchers;

            private CustomFieldTypeBean(CustomFieldType<?, ?> type) {
                this.name = type.getName();
                this.description = type.getDescription();
                this.key = type.getKey();
                this.categories = type.getDescriptor().getCategories();
                this.isManaged = type.getDescriptor().isTypeManaged();
            }

            public CustomFieldTypeBean setCascading(final boolean cascading) {
                this.cascading = cascading;
                return this;
            }

            public CustomFieldTypeBean setManagedDescription(final String managedDescription) {
                this.managedDescription = managedDescription;
                return this;
            }

            public CustomFieldTypeBean setOptions(final boolean options) {
                this.options = options;
                return this;
            }

            public CustomFieldTypeBean setPreviewImageUrl(final String previewImageUrl) {
                this.previewImageUrl = previewImageUrl;
                return this;
            }

            public CustomFieldTypeBean setSearchers(final Iterable<String> searchers) {
                this.searchers = searchers;
                return this;
            }

            @JsonProperty
            public String getManagedDescription() {
                return managedDescription;
            }

            @JsonProperty("isManaged")
            public boolean isManaged() {
                return isManaged;
            }

            @JsonProperty
            public String getName() {
                return name;
            }

            @JsonProperty
            public String getDescription() {
                return description;
            }

            @JsonProperty
            public String getKey() {
                return key;
            }

            @JsonProperty
            public String getPreviewImageUrl() {
                return previewImageUrl;
            }

            @JsonProperty
            public Set<CustomFieldTypeCategory> getCategories() {
                return categories;
            }

            @JsonProperty
            public Iterable<String> getSearchers() {
                return searchers;
            }

            @JsonProperty
            public boolean isCascading() {
                return cascading;
            }

            @JsonProperty
            public boolean isOptions() {
                return options;
            }
        }
    }
}

