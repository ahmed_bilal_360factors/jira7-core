package com.atlassian.jira.projectconfig.shared;

import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.project.Project;

import java.util.List;
import java.util.Objects;

import static java.util.Collections.emptyList;
import static java.util.Collections.unmodifiableList;

public class SharedIssueTypeWorkflowData {
    private final List<Project> projects;
    private final List<IssueType> issueTypes;
    private final long totalProjectsCount;

    public SharedIssueTypeWorkflowData(final List<Project> allowedProjects, final List<IssueType> issueTypes, final long totalProjectsCount) {
        this.projects = unmodifiableList(allowedProjects);
        this.issueTypes = unmodifiableList(issueTypes);
        this.totalProjectsCount = totalProjectsCount;
    }

    public SharedIssueTypeWorkflowData(final List<Project> allowedProjects, final List<IssueType> issueTypes) {
        this(allowedProjects, issueTypes, allowedProjects.size());
    }

    public SharedIssueTypeWorkflowData(final List<Project> allowedProjects, final long totalProjectsCount) {
        this(allowedProjects, emptyList(), totalProjectsCount);
    }

    public SharedIssueTypeWorkflowData(final List<Project> allowedProjects) {
        this(allowedProjects, allowedProjects.size());
    }

    public List<Project> getProjects() {
        return projects;
    }

    public List<IssueType> getIssueTypes() {
        return issueTypes;
    }

    public long getHiddenProjectsCount() {
        return totalProjectsCount - projects.size();
    }

    public long getTotalProjectsCount() {
        return totalProjectsCount;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final SharedIssueTypeWorkflowData that = (SharedIssueTypeWorkflowData) o;
        return totalProjectsCount == that.totalProjectsCount &&
                Objects.equals(projects, that.projects) &&
                Objects.equals(issueTypes, that.issueTypes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(projects, issueTypes, totalProjectsCount);
    }

    @Override
    public String toString() {
        return "SharedIssueTypeWorkflowData{" +
                "projects=" + projects +
                ", totalProjectsCount=" + totalProjectsCount +
                ", hiddenProjectsCount=" + getHiddenProjectsCount() +
                ", issueTypes=" + issueTypes +
                '}';
    }
}
