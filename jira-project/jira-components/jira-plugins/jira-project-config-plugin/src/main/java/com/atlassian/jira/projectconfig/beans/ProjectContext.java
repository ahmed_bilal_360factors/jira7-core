package com.atlassian.jira.projectconfig.beans;

import javax.annotation.Nonnull;

import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.project.Project;

/**
 * Represents a {@link com.atlassian.jira.project.Project} and {@link com.atlassian.jira.issue.issuetype.IssueType}.
 *
 * @since v6.2
 */
public class ProjectContext {
    private final Project project;
    private final IssueType type;

    public ProjectContext(@Nonnull final Project project, @Nonnull final IssueType type) {
        this.project = project;
        this.type = type;
    }

    public Project getProject() {
        return project;
    }

    public IssueType getType() {
        return type;
    }
}
