package com.atlassian.jira.projectconfig.shared;

import com.atlassian.jira.bc.project.ProjectAction;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.workflow.ProjectWorkflowSchemeHelper;
import com.atlassian.jira.workflow.WorkflowSchemeManager;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import static com.atlassian.jira.projectconfig.shared.SharedEntitiesUtils.getOrDefaultWorkflowName;
import static com.atlassian.jira.workflow.JiraWorkflow.DEFAULT_WORKFLOW_NAME;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.Collections.singleton;
import static java.util.stream.Collectors.toList;

/**
 * Retrieves really ALL projects instead of only those the user has enough permissions to do {@link ProjectAction#EDIT_PROJECT_CONFIG}.
 *
 * @see com.atlassian.jira.workflow.DefaultProjectWorkflowSchemeHelper
 */
@Component
public class AllProjectsWorkflowSchemeHelper implements ProjectWorkflowSchemeHelper {
    private final ProjectManager projectManager;
    private final WorkflowSchemeManager workflowSchemeManager;

    @Autowired
    public AllProjectsWorkflowSchemeHelper(
            final ProjectManager projectManager,
            final WorkflowSchemeManager workflowSchemeManager) {
        this.projectManager = projectManager;
        this.workflowSchemeManager = workflowSchemeManager;
    }

    @Override
    public List<Project> getProjectsForScheme(final Long schemeId) {
        return getAllProjects().stream()
                .filter(project -> Objects.equals(getSchemeIdForProject(project), schemeId))
                .collect(toList());
    }

    @Override
    public List<Project> getProjectsForWorkflow(final String workflowName) {
        Multimap<String, Project> projectsForWorkflow = getProjectsForWorkflow(singleton(workflowName));
        return newArrayList(projectsForWorkflow.get(workflowName));
    }

    @Override
    public Multimap<String, Project> getProjectsForWorkflow(final Set<String> workflows) {
        Multimap<String, Project> result = LinkedHashMultimap.create();

        for (Project project : getAllProjects()) {
            final Map<String, String> issueTypeWorkflowMap = workflowSchemeManager.getWorkflowMap(project);
            String defaultWorkflow = getOrDefaultWorkflowName(issueTypeWorkflowMap, null, DEFAULT_WORKFLOW_NAME);

            project.getIssueTypes().stream()
                    .map(IssueType::getId)
                    .map(issueTypeWorkflowMap::get)
                    .map(workflow -> (workflow == null) ? defaultWorkflow : workflow)
                    .filter(workflows::contains)
                    .forEach(workflow -> result.put(workflow, project));
        }

        return result;
    }

    private List<Project> getAllProjects() {
        return projectManager.getProjectObjects();
    }

    private Long getSchemeIdForProject(Project project) {
        try {
            final GenericValue workflowScheme = workflowSchemeManager.getWorkflowScheme(project);
            if (workflowScheme == null) {
                return null;
            }

            return workflowScheme.getLong("id");
        } catch (GenericEntityException e) {
            throw new DataAccessException(e);
        }
    }

}
