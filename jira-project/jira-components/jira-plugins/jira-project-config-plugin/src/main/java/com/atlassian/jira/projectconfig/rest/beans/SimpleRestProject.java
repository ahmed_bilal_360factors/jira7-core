package com.atlassian.jira.projectconfig.rest.beans;

import com.atlassian.gzipfilter.org.apache.commons.lang.builder.ToStringBuilder;
import com.atlassian.gzipfilter.org.apache.commons.lang.builder.ToStringStyle;
import com.atlassian.jira.project.Project;

import com.google.common.base.Function;

import org.codehaus.jackson.annotate.JsonProperty;

import static org.apache.commons.lang3.StringUtils.stripToNull;

/**
 * @since v6.2
 */
public class SimpleRestProject {
    private static final Function<Project, SimpleRestProject> FULL_BEAN_FUNC = new Function<Project, SimpleRestProject>() {
        @Override
        public SimpleRestProject apply(final Project input) {
            return fullProject(input);
        }
    };

    private static final Function<Project, SimpleRestProject> SHORT_BEAN_FUNC = new Function<Project, SimpleRestProject>() {
        @Override
        public SimpleRestProject apply(final Project input) {
            return shortProject(input);
        }
    };

    public static Function<Project, SimpleRestProject> fullBeanFunc() {
        return FULL_BEAN_FUNC;
    }

    public static Function<Project, SimpleRestProject> shortBeanFunc() {
        return SHORT_BEAN_FUNC;
    }

    public static SimpleRestProject fullProject(Project project) {
        return new SimpleRestProject(project);
    }

    public static SimpleRestProject shortProject(Project project) {
        return new SimpleRestProject(project.getId(), project.getKey(), project.getName());
    }

    private final long id;
    private final String key;
    private final String name;
    private final String url;
    private final String description;

    private SimpleRestProject(Project project) {
        this.key = stripToNull(project.getKey());
        this.id = project.getId();
        this.name = stripToNull(project.getName());
        this.url = stripToNull(project.getUrl());
        this.description = stripToNull(project.getDescription());
    }

    private SimpleRestProject(final long id, final String key, final String name) {
        this.id = id;
        this.key = key;
        this.name = name;
        this.url = null;
        this.description = null;
    }

    @JsonProperty
    public long getId() {
        return id;
    }

    @JsonProperty
    public String getKey() {
        return key;
    }

    @JsonProperty
    public String getName() {
        return name;
    }

    @JsonProperty
    public String getUrl() {
        return url;
    }

    @JsonProperty
    public String getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SimpleRestProject that = (SimpleRestProject) o;

        if (id != that.id) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
