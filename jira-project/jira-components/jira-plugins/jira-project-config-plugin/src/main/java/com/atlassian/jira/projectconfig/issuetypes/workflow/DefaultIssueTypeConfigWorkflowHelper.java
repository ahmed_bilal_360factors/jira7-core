package com.atlassian.jira.projectconfig.issuetypes.workflow;

import com.atlassian.jira.bc.workflow.DefaultWorkflowService;
import com.atlassian.jira.bc.workflow.WorkflowService;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.beans.ProjectContext;
import com.atlassian.jira.projectconfig.order.OrderFactory;
import com.atlassian.jira.projectconfig.shared.SharedEntitiesHelper;
import com.atlassian.jira.projectconfig.shared.SharedIssueTypeWorkflowData;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.WorkflowSchemeManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;

/**
 * @since v6.2
 */
@Component
public class DefaultIssueTypeConfigWorkflowHelper implements IssueTypeConfigWorkflowHelper {
    private final JiraAuthenticationContext context;
    private final PermissionManager permissionManager;
    private final WorkflowSchemeManager workflowSchemeManager;
    private final WorkflowManager workflowManager;
    private final WorkflowService workflowService;
    private final SharedEntitiesHelper sharedHelper;
    private final FeatureManager featureManager;

    @Autowired
    public DefaultIssueTypeConfigWorkflowHelper(final JiraAuthenticationContext context,
                                                final PermissionManager permissionManager,
                                                final WorkflowSchemeManager workflowSchemeManager,
                                                final WorkflowManager workflowManager,
                                                final WorkflowService workflowService,
                                                final SharedEntitiesHelper sharedHelper,
                                                final FeatureManager featureManager) {
        this.context = context;
        this.permissionManager = permissionManager;
        this.workflowSchemeManager = workflowSchemeManager;
        this.workflowManager = workflowManager;
        this.workflowService = workflowService;
        this.sharedHelper = sharedHelper;
        this.featureManager = featureManager;
    }

    @Nonnull
    @Override
    public WorkflowResult getWorkflowData(@Nonnull ProjectContext issueContext) {
        return getWorkflowData(issueContext.getProject(), issueContext.getType());
    }

    private WorkflowResult getWorkflowData(@Nonnull final Project project, @Nonnull final IssueType type) {
        final JiraWorkflow jiraWorkflow = workflowManager.getWorkflow(project.getId(), type.getId());

        final WorkflowState workflowState = getWorkflowState(project, jiraWorkflow);
        final SharedIssueTypeWorkflowData sharedBy = sharedHelper.getSharedData(project, type, jiraWorkflow.getName());

        return new WorkflowResult(jiraWorkflow, workflowState, sharedBy);
    }

    private WorkflowState getWorkflowState(final Project project, final JiraWorkflow workflow) {
        final boolean editWorkflowPermissionInProject = hasEditWorkflowPermissionInProjects(project);
        final boolean notGlobalAdmin = !permissionManager.hasPermission(Permissions.ADMINISTER, context.getUser());
        if (notGlobalAdmin) {
            if (!editWorkflowPermissionInProject) {
                return WorkflowState.NO_PERMISSION;
            } else {
                if (workflow.isSystemWorkflow()) {
                    return WorkflowState.READ_ONLY_DELEGATED_SYSTEM;
                }
                final boolean canEditWorkflowPermission = workflowService.hasEditWorkflowPermission(context.getLoggedInUser(), workflow);
                if (canEditWorkflowPermission) {
                    // not shared
                    return WorkflowState.EDITABLE_DELEGATED;
                } else {
                    //shared
                    return WorkflowState.READ_ONLY_DELEGATED_SHARED;
                }
            }
        } else {
            if (workflowSchemeManager.isUsingDefaultScheme(project)) {
                return WorkflowState.MIGRATE;
            } else if (workflow.isSystemWorkflow()) {
                return WorkflowState.READ_ONLY;
            } else {
                return WorkflowState.EDITABLE;
            }
        }
    }

    private boolean hasEditWorkflowPermissionInProjects(Project project) {
        return
                featureManager.isEnabled(DefaultWorkflowService.PROJECT_LEVEL_ADMIN_EDIT_WORKFLOW_FEATURE)
                        && permissionManager.hasPermission(ProjectPermissions.ADMINISTER_PROJECTS, project, context.getLoggedInUser());
    }
}
