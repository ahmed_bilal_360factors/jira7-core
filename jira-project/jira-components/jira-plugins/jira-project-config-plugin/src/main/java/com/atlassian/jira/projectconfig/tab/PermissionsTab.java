package com.atlassian.jira.projectconfig.tab;

import com.atlassian.jira.projectconfig.util.VelocityContextFactory;
import com.atlassian.plugin.web.WebInterfaceManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @since v4.4
 */
@Component
public class PermissionsTab extends WebPanelTab {
    @Autowired
    public PermissionsTab(final WebInterfaceManager webInterfaceManager, VelocityContextFactory factory) {
        super(webInterfaceManager, factory, "permissions", "view_project_permissions");
    }

    @Override
    public String getTitle(ProjectConfigTabRenderContext context) {
        return context.getI18NHelper().getText("admin.project.permissions.title", context.getProject().getName());
    }

    @Override
    public void addResourceForProject(ProjectConfigTabRenderContext renderContext) {
        renderContext
                .getPageBuilderService()
                .assembler()
                .resources()
                .requireWebResource("com.atlassian.jira.jira-project-config-plugin:permissions-resources");
    }
}
