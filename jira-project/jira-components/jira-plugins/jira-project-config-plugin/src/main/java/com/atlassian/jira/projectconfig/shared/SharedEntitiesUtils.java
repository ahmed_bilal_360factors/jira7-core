package com.atlassian.jira.projectconfig.shared;

import com.atlassian.jira.project.Project;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Predicate;

import static java.util.stream.Collectors.toList;

public class SharedEntitiesUtils {

    public static List<Project> filterNotEqualProject(@Nullable Project currentProject, List<Project> projects) {
        return projects.stream().filter(projectNotEqual(currentProject)).collect(toList());
    }

    public static String getOrDefaultWorkflowName(Map<String, String> issueTypeToWorkflowMap, @Nullable String issueTypeId, String defaultWorkflowName) {
        String workflowName = issueTypeToWorkflowMap.get(issueTypeId);
        return workflowName == null ? defaultWorkflowName : workflowName;
    }

    private static Predicate<Project> projectNotEqual(@Nullable Project currentProject) {
        if (currentProject == null) {
            return project -> true;
        }
        return project -> !Objects.equals(currentProject.getId(), project.getId());
    }
}
