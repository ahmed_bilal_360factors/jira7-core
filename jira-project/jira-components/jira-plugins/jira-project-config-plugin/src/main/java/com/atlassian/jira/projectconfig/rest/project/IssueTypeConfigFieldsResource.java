package com.atlassian.jira.projectconfig.rest.project;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.projectconfig.beans.ProjectContext;
import com.atlassian.jira.projectconfig.issuetypes.fields.IssueTypeConfigFieldsHelper;
import com.atlassian.jira.projectconfig.rest.Responses;
import com.atlassian.jira.projectconfig.rest.beans.SharedByData;
import com.atlassian.jira.projectconfig.util.ProjectContextLocator;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Gets the issue type configuration associated with a view.
 *
 * @since v6.2
 */
@Path("issuetype/{project}/{issuetype}/fields")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class IssueTypeConfigFieldsResource {
    private final IssueTypeConfigFieldsHelper fieldsHelper;
    private final ProjectContextLocator projectContextLocator;

    public IssueTypeConfigFieldsResource(final IssueTypeConfigFieldsHelper fieldsHelper,
                                         final ProjectContextLocator projectContextLocator) {
        this.fieldsHelper = fieldsHelper;
        this.projectContextLocator = projectContextLocator;
    }

    @GET
    public Response getFieldsData(@PathParam("project") String projectKey,
                                  @PathParam("issuetype") long issueTypeId) {
        final ServiceOutcome<ProjectContext> context = projectContextLocator.getContext(projectKey, issueTypeId);
        if (!context.isValid()) {
            return Responses.forOutcome(context);
        } else {
            return Responses.ok(new FieldsDetails(fieldsHelper.getFieldsData(context.get())));
        }
    }

    public static class FieldsDetails extends SharedByData {
        @JsonProperty
        private long viewScreenId;

        @JsonProperty
        private String screenName;

        private FieldsDetails(IssueTypeConfigFieldsHelper.FieldsResult fieldsResult) {
            super(fieldsResult.getSharedBy());
            this.viewScreenId = fieldsResult.getFieldScreen().getId();
            this.screenName = fieldsResult.getFieldScreen().getName();
        }

        public long getViewScreenId() {
            return viewScreenId;
        }

        public String getScreenName() {
            return screenName;
        }

        public void setScreenName(final String screenName) {
            this.screenName = screenName;
        }

        public void setViewScreenId(final long viewScreenId) {
            this.viewScreenId = viewScreenId;
        }
    }
}
