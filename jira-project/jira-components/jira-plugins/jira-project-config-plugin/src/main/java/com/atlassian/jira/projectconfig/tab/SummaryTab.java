package com.atlassian.jira.projectconfig.tab;

import com.atlassian.jira.projectconfig.util.VelocityContextFactory;
import com.atlassian.plugin.web.WebInterfaceManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @since v4.4
 */
@Component
public class SummaryTab extends WebPanelTab implements ProjectConfigTab {
    public static final String NAME = "summary";
    public static final String LINK_ID = "view_project_summary";

    @Autowired
    public SummaryTab(final WebInterfaceManager webInterfaceManager, VelocityContextFactory contextFactory) {
        super(webInterfaceManager, contextFactory, NAME, LINK_ID);
    }

    public String getTitle(ProjectConfigTabRenderContext context) {
        return context.getI18NHelper().getText("admin.project.config.summary.title", context.getProject().getName());
    }

    @Override
    public void addResourceForProject(ProjectConfigTabRenderContext context) {
        context.getPageBuilderService().assembler().resources()
                .requireContext("com.atlassian.jira.jira-project-config-plugin:summary-browser-metrics");
    }
}
