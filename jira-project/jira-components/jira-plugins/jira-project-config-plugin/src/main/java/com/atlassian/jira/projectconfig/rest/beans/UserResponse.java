package com.atlassian.jira.projectconfig.rest.beans;

import com.atlassian.fugue.Option;
import com.atlassian.jira.user.ApplicationUser;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class UserResponse {

    @JsonProperty
    private String name;

    @JsonProperty
    private String emailAddress;
    @JsonProperty
    private String avatarUrl;

    @JsonProperty
    private String lastSession;

    @JsonProperty
    private String displayName;

    @JsonProperty
    private boolean active;

    @JsonProperty
    private String key;

    @JsonProperty
    private List<String> applicationRoleNames;


    public UserResponse(String key, String name, String displayName, String emailAddress, boolean active, String avatarUrl, String lastSession, List<String> applicationRoleNames) {
        this.key = key;
        this.name = name;
        this.displayName = displayName;
        this.emailAddress = emailAddress;
        this.active = active;
        this.avatarUrl = avatarUrl;
        this.lastSession = lastSession;
        this.applicationRoleNames = applicationRoleNames;
    }

    private UserResponse() {
    }

    public String getName() {
        return name;
    }

    public String getEmailAddress() {
        return emailAddress;
    }


    public String getAvatarUrl() {
        return avatarUrl;
    }

    public String getLastSession() {
        return lastSession;
    }

    public String getDisplayName() {
        return displayName;
    }

    public boolean isActive() {
        return active;
    }

    public String getFormattedDate() {
        return lastSession;
    }

    public String getKey() {
        return key;
    }

    public List<String> getApplicationRoleNames() {
        return applicationRoleNames;
    }

    public static UserResponse fromApplicationUser(ApplicationUser user, String avatarUrl, Option<String> lastSession, boolean isEmailVisible, List<String> applicationRoleNames) {
        String emailAddress = null;
        if (isEmailVisible) {
            emailAddress = user.getEmailAddress();
        }

        return new UserResponse(user.getKey(), user.getName(), user.getDisplayName(), emailAddress, user.isActive(), avatarUrl, lastSession.getOrNull(), applicationRoleNames);
    }
}
