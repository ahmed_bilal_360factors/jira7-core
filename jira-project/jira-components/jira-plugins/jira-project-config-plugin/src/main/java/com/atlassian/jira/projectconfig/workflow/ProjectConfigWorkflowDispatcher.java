package com.atlassian.jira.projectconfig.workflow;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.util.lang.Pair;
import com.atlassian.jira.workflow.AssignableWorkflowScheme;
import com.atlassian.jira.workflow.DraftWorkflowScheme;

/**
 * Helps the workflow panel work out which workflow to edit when the user asks to do so.
 *
 * @since v5.1
 */
public interface ProjectConfigWorkflowDispatcher {
    /**
     * Called when the user tries to edit a workflow associated with the passed project. This method may try and create
     * a new workflow for that project.
     *
     * @param projectId the project.
     * @return an outcome with the name of the newly created workflow contained or null if the workflow was not created.
     * Any errors that occur will returned in the outcome.
     */
    ServiceOutcome<Pair<String, Long>> editWorkflow(long projectId);

    /**
     * Creates a draft workflow scheme for the passed project. Will create a new scheme if the project is using the
     * default workflow scheme.
     *
     * @param projectKey the project.
     * @return the draft workflow scheme for the passed project, creating one if necessary.
     */
    ServiceOutcome<EditSchemeResult> editScheme(String projectKey);

    public static class EditSchemeResult {
        private final AssignableWorkflowScheme workflowScheme;
        private final DraftWorkflowScheme draftWorkflowScheme;
        private final Project project;

        EditSchemeResult(AssignableWorkflowScheme workflowScheme, DraftWorkflowScheme draftWorkflowScheme, Project project) {
            this.workflowScheme = workflowScheme;
            this.draftWorkflowScheme = draftWorkflowScheme;
            this.project = project;
        }

        public AssignableWorkflowScheme getWorkflowScheme() {
            return workflowScheme;
        }

        public DraftWorkflowScheme getDraftWorkflowScheme() {
            return draftWorkflowScheme;
        }

        public Project getProject() {
            return project;
        }
    }
}
