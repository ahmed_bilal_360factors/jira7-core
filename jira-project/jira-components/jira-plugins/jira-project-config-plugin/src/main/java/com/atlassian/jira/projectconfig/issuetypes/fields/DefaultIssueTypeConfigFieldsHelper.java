package com.atlassian.jira.projectconfig.issuetypes.fields;

import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.issue.fields.screen.ProjectFieldScreenHelper;
import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenSchemeManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.operation.IssueOperations;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.beans.ProjectContext;
import com.atlassian.jira.projectconfig.order.OrderFactory;
import com.atlassian.jira.projectconfig.shared.SharedIssueTypeWorkflowData;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Objects;

import static com.atlassian.jira.projectconfig.shared.SharedEntitiesUtils.filterNotEqualProject;
import static java.util.stream.Collectors.toList;

/**
 * @since v6.2
 */
@Component
public class DefaultIssueTypeConfigFieldsHelper implements IssueTypeConfigFieldsHelper {
    private final IssueTypeScreenSchemeManager issueTypeScreenManager;
    private final ProjectFieldScreenHelper projectFieldScreenHelper;
    private final OrderFactory orderFactory;

    @Autowired
    public DefaultIssueTypeConfigFieldsHelper(
            @ComponentImport final IssueTypeScreenSchemeManager issueTypeScreenManager,
            @ComponentImport final ProjectFieldScreenHelper projectFieldScreenHelper,
            final OrderFactory orderFactory) {
        this.issueTypeScreenManager = issueTypeScreenManager;
        this.projectFieldScreenHelper = projectFieldScreenHelper;
        this.orderFactory = orderFactory;
    }

    @Nonnull
    @Override
    public IssueTypeConfigFieldsHelper.FieldsResult getFieldsData(@Nonnull final ProjectContext context) {
        final FieldScreen fieldScreen = getFieldScreen(context.getProject(), context.getType());
        final List<Project> otherProject = getOtherProjectsSharingScreen(fieldScreen, context.getProject());
        final List<IssueType> otherIssueTypes = getOtherIssueTypesSharingScreen(fieldScreen, context);

        final SharedIssueTypeWorkflowData sharedBy = new SharedIssueTypeWorkflowData(otherProject, otherIssueTypes);

        return new FieldsResult(fieldScreen, sharedBy);
    }

    private FieldScreen getFieldScreen(final Project project, final IssueType type) {
        return issueTypeScreenManager.getIssueTypeScreenScheme(project)
                .getEffectiveFieldScreenScheme(type)
                .getFieldScreen(IssueOperations.VIEW_ISSUE_OPERATION);
    }

    private List<Project> getOtherProjectsSharingScreen(final FieldScreen fieldScreen, final Project project) {
        return filterNotEqualProject(project, projectFieldScreenHelper.getProjectsForFieldScreen(fieldScreen));
    }

    private List<IssueType> getOtherIssueTypesSharingScreen(final FieldScreen fieldScreen, final ProjectContext context) {
        final Project project = context.getProject();
        final String currentIssueTypeId = context.getType().getId();
        final Long currentViewId = fieldScreen.getId();

        List<IssueType> result = project.getIssueTypes().stream()
                .filter(otherIssueType -> !Objects.equals(currentIssueTypeId, otherIssueType.getId()))
                .filter(otherIssueType -> {
                    final FieldScreen otherFieldScreen = getFieldScreen(project, otherIssueType);
                    return currentViewId.equals(otherFieldScreen.getId());
                })
                .collect(toList());

        return orderFactory.createTranslatedNameOrder().immutableSortedCopy(result);
    }
}
