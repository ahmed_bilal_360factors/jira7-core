package com.atlassian.jira.projectconfig.listener;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.event.user.LogoutEvent;
import com.atlassian.jira.user.UserPropertyManager;
import com.atlassian.jira.web.SessionKeys;
import com.opensymphony.module.propertyset.PropertySet;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * A simple event listener for logout events sent from JIRA.
 * This will clear down a user property to prevent the "Return to project" link being displayed
 * after logout.
 */
@SuppressWarnings("unused")
@Component
public class LogoutEventListener implements InitializingBean, DisposableBean {
    private final EventPublisher eventPublisher;
    private final UserPropertyManager userPropertyManager;

    @Autowired
    public LogoutEventListener(EventPublisher eventPublisher, UserPropertyManager userPropertyManager) {
        this.eventPublisher = eventPublisher;
        this.userPropertyManager = userPropertyManager;
    }

    @EventListener
    public void onLogoutEvent(LogoutEvent logoutEvent) {
        if (logoutEvent != null && logoutEvent.getUser() != null) {
            PropertySet userProperties = userPropertyManager.getPropertySet(logoutEvent.getUser());
            userProperties.remove(SessionKeys.CURRENT_ADMIN_PROJECT);
            userProperties.remove(SessionKeys.CURRENT_ADMIN_PROJECT_TAB);
            userProperties.remove(SessionKeys.CURRENT_ADMIN_PROJECT_RETURN_URL);
        }
    }

    @Override
    public void destroy() throws Exception {
        eventPublisher.unregister(this);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        eventPublisher.register(this);
    }
}