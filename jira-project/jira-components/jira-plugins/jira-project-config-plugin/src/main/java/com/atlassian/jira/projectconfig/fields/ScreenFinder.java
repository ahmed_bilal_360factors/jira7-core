package com.atlassian.jira.projectconfig.fields;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.issue.fields.screen.FieldScreenScheme;
import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenSchemeManager;
import com.atlassian.jira.issue.operation.IssueOperations;
import com.atlassian.jira.issue.operation.ScreenableIssueOperation;
import com.google.common.collect.Maps;

import java.util.Collections;
import java.util.Map;
import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * A simple service that encapsulates the logic of finding the screens for an issue.
 *
 * @since v6.1
 */
@Component
public class ScreenFinder {
    private final IssueTypeScreenSchemeManager issueTypeScreenSchemeManager;

    @Autowired
    public ScreenFinder(final IssueTypeScreenSchemeManager issueTypeScreenSchemeManager) {
        this.issueTypeScreenSchemeManager = issueTypeScreenSchemeManager;
    }

    /**
     * Return the create, view and edit screens for the passed issue.
     *
     * @param issue the issue to query.
     * @return the create, view and edit screens for the passed issue.
     */
    @Nonnull
    public Iterable<FieldScreen> getIssueScreens(@Nonnull Issue issue) {
        final FieldScreenScheme fieldScreenScheme = issueTypeScreenSchemeManager.getFieldScreenScheme(issue);
        Map<Long, FieldScreen> screens = Maps.newHashMap();
        for (ScreenableIssueOperation issueOperation : IssueOperations.getIssueOperations()) {
            final FieldScreen fieldScreen = fieldScreenScheme.getFieldScreen(issueOperation);
            screens.put(fieldScreen.getId(), fieldScreen);
        }
        return Collections.unmodifiableCollection(screens.values());
    }
}