package com.atlassian.jira.projectconfig.rest.global;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.fugue.Either;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.OrderableField;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.fields.IssueFields;
import com.atlassian.jira.projectconfig.rest.AdminRequired;
import com.atlassian.jira.projectconfig.rest.beans.SimpleRestProject;
import com.atlassian.jira.util.lang.Pair;
import com.atlassian.sal.api.websudo.WebSudoRequired;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;

import org.codehaus.jackson.annotate.JsonProperty;

import static com.atlassian.jira.rest.api.http.CacheControl.never;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Lists.newArrayList;

/**
 * Allows custom fields to be added to an issue's create, edit, and view screens.
 *
 * @since v6.1
 */
@AdminRequired
@WebSudoRequired
@Path("issuecustomfields")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class IssueCustomFieldsResource {
    private final IssueManager issueManager;
    private final CustomFieldManager fieldManager;
    private final IssueFields fields;

    public IssueCustomFieldsResource(final IssueManager issueManager, final CustomFieldManager fieldManager,
                                     final IssueFields fields) {
        this.issueManager = issueManager;
        this.fieldManager = fieldManager;
        this.fields = fields;
    }

    /**
     * Add a custom field to the create, edit, and view screens that are currently used by an issue.
     *
     * @param issueId the ID of the issue
     * @param fieldId the ID of the field to add to the screens.
     */
    @POST
    @Path("{issue}")
    public Response addCustomField(@PathParam("issue") final long issueId, String fieldId) {
        final Either<Issue, Response> either = getIssue(issueId);
        if (either.isRight()) {
            return either.right().get();
        }

        final OrderableField field = fieldManager.getCustomFieldObject(fieldId);
        if (field == null) {
            return Response.status(Response.Status.NOT_FOUND)
                    .cacheControl(never()).build();
        }
        fields.addFieldToIssueScreens(field, either.left().get());
        return Response.status(Response.Status.NO_CONTENT).cacheControl(never()).build();
    }

    /**
     * Return the projects that would be affected if a field was added to the passed screen.
     *
     * @param issueId the ID of the issue to query.
     * @return a list of project names affected by the change.
     */
    @GET
    @Path("{issue}/affectedProjects")
    public Response getAffectedProjects(@PathParam("issue") final long issueId) {
        final Either<Issue, Response> either = getIssue(issueId);
        if (either.isRight()) {
            return either.right().get();
        } else {
            final Issue issue = either.left().get();
            final Iterable<Project> affectedProjects = filter(fields.getAffectedProjects(issue), not(equalTo(issue.getProjectObject())));
            final Iterable<SimpleRestProject> names = transform(affectedProjects, SimpleRestProject.fullBeanFunc());
            return Response.ok(newArrayList(names)).cacheControl(never()).build();
        }
    }

    @GET
    @Path("{issue}/fields")
    public Response getFields(@PathParam("issue") final long issueId, @DefaultValue("false") @QueryParam("onAllScreensFlag") boolean onAllScreensFlag) {
        final Either<Issue, Response> either = getIssue(issueId);
        if (either.isRight()) {
            return either.right().get();
        } else {
            final Issue issue = either.left().get();
            if (onAllScreensFlag) {
                final Iterable<FieldBeanWithVisiblityFlag> beans = Iterables.transform(fields.getAllCustomFieldsWithOnAllScreensFlag(issue), FieldBeanWithVisiblityFlag.toBean());
                return Response.ok(newArrayList(beans)).cacheControl(never()).build();
            } else {
                final Iterable<FieldBean> beans = Iterables.transform(fields.getAllCustomFields(issue), FieldBean.toBean());
                return Response.ok(newArrayList(beans)).cacheControl(never()).build();
            }
        }
    }

    private Either<Issue, Response> getIssue(long issueId) {
        final Issue issueObject = issueManager.getIssueObject(issueId);
        if (issueObject == null) {
            return Either.right(Response.status(Response.Status.NOT_FOUND)
                    .cacheControl(never()).build());
        } else {
            return Either.left(issueObject);
        }
    }

    public static abstract class AbstractFieldBean {
        private final String name;
        private final String id;

        private AbstractFieldBean(Field field) {
            this.id = field.getId();
            this.name = field.getName();
        }

        @JsonProperty
        public String getId() {
            return id;
        }

        @JsonProperty
        public String getName() {
            return name;
        }
    }

    public static class FieldBean extends AbstractFieldBean {
        private static final Function<Field, FieldBean> TO_BEAN = new Function<Field, FieldBean>() {
            @Override
            public FieldBean apply(final Field input) {
                return new FieldBean(input);
            }
        };

        public static Function<Field, FieldBean> toBean() {
            return TO_BEAN;
        }

        private FieldBean(Field field) {
            super(field);
        }
    }

    public static class FieldBeanWithVisiblityFlag extends AbstractFieldBean {

        private static final Function<Pair<OrderableField, Boolean>, FieldBeanWithVisiblityFlag> TO_BEAN = new Function<Pair<OrderableField, Boolean>, FieldBeanWithVisiblityFlag>() {
            @Override
            public FieldBeanWithVisiblityFlag apply(final Pair<OrderableField, Boolean> input) {
                return new FieldBeanWithVisiblityFlag(input.first(), input.second());
            }
        };

        public static Function<Pair<OrderableField, Boolean>, FieldBeanWithVisiblityFlag> toBean() {
            return TO_BEAN;
        }

        private final boolean onAllScreens;

        private FieldBeanWithVisiblityFlag(Field field, boolean onAllScreens) {
            super(field);
            this.onAllScreens = onAllScreens;
        }

        @JsonProperty
        public boolean isOnAllScreens() {
            return onAllScreens;
        }
    }
}
