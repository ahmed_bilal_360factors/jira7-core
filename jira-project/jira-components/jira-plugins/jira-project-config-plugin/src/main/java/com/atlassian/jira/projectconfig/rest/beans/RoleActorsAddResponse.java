package com.atlassian.jira.projectconfig.rest.beans;

import org.codehaus.jackson.annotate.JsonProperty;

public class RoleActorsAddResponse {
    @JsonProperty
    private int numberOfGroupsAdded;

    @JsonProperty
    private int numberOfUsersAdded;

    private RoleActorsAddResponse() {
    }

    public RoleActorsAddResponse(int numberOfGroupsAdded, int numberOfUsersAdded) {
        this.numberOfGroupsAdded = numberOfGroupsAdded;
        this.numberOfUsersAdded = numberOfUsersAdded;
    }

    public int getNumberOfGroupsAdded() {
        return numberOfGroupsAdded;
    }

    public int getNumberOfUsersAdded() {
        return numberOfUsersAdded;
    }
}
