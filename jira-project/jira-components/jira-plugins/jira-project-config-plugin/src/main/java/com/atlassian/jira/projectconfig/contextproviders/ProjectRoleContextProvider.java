package com.atlassian.jira.projectconfig.contextproviders;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.plugin.profile.UserFormatManager;
import com.atlassian.jira.project.AssigneeTypes;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.collect.MapBuilder;

import java.util.Map;

public class ProjectRoleContextProvider extends ProjectPeopleContextProvider {
    static final String CONTEXT_DEFAULT_ASSIGNEE_AVATAR_URL_KEY = "defaultAssigneeAvatarUrl";

    private final AvatarService avatarService;
    private final I18nHelper i18nHelper;

    public ProjectRoleContextProvider(final PermissionManager permissionManager, final UserFormatManager userFormatManager,
                                      final UserManager userManager, final AvatarService avatarService,
                                      final ContextProviderUtils contextProviderUtils, final JiraAuthenticationContext authenticationContext,
                                      final I18nHelper i18nHelper) {
        super(permissionManager, userFormatManager, userManager, avatarService, contextProviderUtils, authenticationContext);
        this.avatarService = avatarService;
        this.i18nHelper = i18nHelper;
    }

    @Override
    public Map<String, Object> getContextMap(Map<String, Object> context) {
        Map<String, Object> contextMap = super.getContextMap(context);
        MapBuilder<String, Object> contextMapBuilder = MapBuilder.<String, Object>newBuilder().addAll(contextMap);

        // We need to translate default assignee here because we can't in soy
        String defaultAssigneeKey = (String) contextMap.get(CONTEXT_DEFAULT_ASSIGNEE_KEY);
        String defaultAssignee = i18nHelper.getText(defaultAssigneeKey);
        contextMapBuilder.add(CONTEXT_DEFAULT_ASSIGNEE_KEY, defaultAssignee);

        // We will show anonymous avatar in case default assignee is Unassigned
        if (AssigneeTypes.PRETTY_UNASSIGNED.equals(defaultAssigneeKey)) {
            contextMapBuilder.add(CONTEXT_DEFAULT_ASSIGNEE_AVATAR_URL_KEY, getAnonymousAvatar());
        }

        return contextMapBuilder.toMap();
    }

    private String getAnonymousAvatar() {
        ApplicationUser anonymousUser = null;
        return avatarService.getAvatarURL(anonymousUser, anonymousUser, Avatar.Size.SMALL).toString();
    }
}
