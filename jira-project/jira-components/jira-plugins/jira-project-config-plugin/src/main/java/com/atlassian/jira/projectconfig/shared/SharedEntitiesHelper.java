package com.atlassian.jira.projectconfig.shared;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.order.OrderFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.workflow.ProjectWorkflowSchemeHelper;
import com.atlassian.jira.workflow.WorkflowManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

import static com.atlassian.jira.bc.project.ProjectAction.EDIT_PROJECT_CONFIG;
import static com.atlassian.jira.projectconfig.shared.SharedEntitiesUtils.filterNotEqualProject;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

/**
 * Helper which helps retrieve projects and issue types which share common workflow/workflow scheme.
 *
 * @see AllProjectsWorkflowSchemeHelper
 */
@Component
public class SharedEntitiesHelper {
    private final OrderFactory orderFactory;
    private final ProjectService projectService;
    private final WorkflowManager workflowManager;
    private final JiraAuthenticationContext authenticationContext;
    private final ProjectWorkflowSchemeHelper projectWorkflowSchemeHelper;

    @Autowired
    public SharedEntitiesHelper(
            final OrderFactory orderFactory,
            final ProjectService projectService,
            final WorkflowManager workflowManager,
            final JiraAuthenticationContext authenticationContext,
            final ProjectWorkflowSchemeHelper projectWorkflowSchemeHelper) {
        this.orderFactory = orderFactory;
        this.projectService = projectService;
        this.workflowManager = workflowManager;
        this.authenticationContext = authenticationContext;
        this.projectWorkflowSchemeHelper = projectWorkflowSchemeHelper;
    }

    public SharedIssueTypeWorkflowData getSharedData(final Project project, final IssueType issueType, final String workflowName) {
        final List<Project> projects = getProjectsForWorkflow(project, workflowName);
        final List<IssueType> issueTypes = getSharingIssueTypes(project, issueType, workflowName);
        final List<Project> allowedProjects = filterSharingProjects(projects);

        return new SharedIssueTypeWorkflowData(allowedProjects, issueTypes, projects.size());
    }

    public List<Project> getProjectsForScheme(Project currentProject, Long schemeId) {
        return filterNotEqualProject(currentProject, projectWorkflowSchemeHelper.getProjectsForScheme(schemeId));
    }

    public List<Project> getProjectsForWorkflow(Project currentProject, String workflowName) {
        return filterNotEqualProject(currentProject, projectWorkflowSchemeHelper.getProjectsForWorkflow(workflowName));
    }

    public List<Project> filterSharingProjects(List<Project> allProjects) {
        return getAllAccessibleProjects().stream().filter(allProjects::contains).collect(toList());
    }

    private List<IssueType> getSharingIssueTypes(Project project, IssueType currentIssueType, String workflowName) {
        final List<IssueType> result = project.getIssueTypes().stream()
                .filter(issueType -> !Objects.equals(issueType.getId(), currentIssueType.getId()))
                .filter(issueType -> {
                    String issueTypeWorkflowName = workflowManager.getWorkflow(project.getId(), issueType.getId()).getName();
                    return Objects.equals(issueTypeWorkflowName, workflowName);
                })
                .collect(toList());

        return orderFactory.createTranslatedNameOrder().immutableSortedCopy(result);
    }

    /**
     * We use this particular action for consistency since
     * {@link com.atlassian.jira.workflow.DefaultProjectWorkflowSchemeHelper#getAllProjects DefaultProjectWorkflowSchemeHelper#getAllProjects}
     * is using it also.
     *
     * @return list of projects the user is able to perform EDIT_PROJECT_CONFIG action on
     */
    private List<Project> getAllAccessibleProjects() {
        final ApplicationUser user = authenticationContext.getLoggedInUser();
        final ServiceOutcome<List<Project>> result = projectService.getAllProjectsForAction(user, EDIT_PROJECT_CONFIG);
        if (!result.isValid()) {
            return emptyList();
        }

        return result.getReturnedValue();
    }
}
