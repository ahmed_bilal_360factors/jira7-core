package com.atlassian.jira.projectconfig.event;

import com.atlassian.analytics.api.annotations.EventName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * An analytic event to indicate when the project admin summary page is shown.
 *
 * @since v6.1
 */
@EventName("administration.project.summary")
public class SummaryPageLoadedEvent {
    private final long projectId;
    private final String projectKey;

    public SummaryPageLoadedEvent(long projectId, String projectKey) {
        this.projectId = projectId;
        this.projectKey = projectKey;
    }

    public long getProjectId() {
        return projectId;
    }

    public String getProjectKey() {
        return projectKey;
    }

    // using EqualsBuilder and HashCodeBuilder here as they are used in tests only
    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(this, o);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
}
