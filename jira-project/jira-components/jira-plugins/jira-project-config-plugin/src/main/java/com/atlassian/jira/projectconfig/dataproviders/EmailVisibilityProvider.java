package com.atlassian.jira.projectconfig.dataproviders;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.EmailFormatter;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.webresource.api.data.WebResourceDataProvider;
import com.google.common.collect.ImmutableMap;

public class EmailVisibilityProvider implements WebResourceDataProvider {
    private final JiraAuthenticationContext authenticationContext;
    private final EmailFormatter emailFormatter;

    public EmailVisibilityProvider(final JiraAuthenticationContext authenticationContext,
                                   final EmailFormatter emailFormatter) {
        this.authenticationContext = authenticationContext;
        this.emailFormatter = emailFormatter;
    }

    @Override
    public Jsonable get() {
        return writer -> {
            try {
                getJsonData().write(writer);
            } catch (JSONException e) {
                throw new Jsonable.JsonMappingException(e);
            }
        };
    }

    private JSONObject getJsonData() {
        final ApplicationUser loggedInUser = authenticationContext.getLoggedInUser();
        final ImmutableMap.Builder<String, Object> builder = ImmutableMap.builder();

        builder.put("isEmailVisible", emailFormatter.emailVisible(loggedInUser));

        return new JSONObject(builder.build());
    }
}

