package com.atlassian.jira.projectconfig;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.bc.customfield.CustomFieldService;
import com.atlassian.jira.bc.project.projectoperation.ProjectOperationManager;
import com.atlassian.jira.bc.project.version.VersionService;
import com.atlassian.jira.bc.projectroles.ProjectRoleService;
import com.atlassian.jira.bc.security.login.LoginService;
import com.atlassian.jira.bc.user.search.UserPickerSearchService;
import com.atlassian.jira.config.IssueTypeManager;
import com.atlassian.jira.config.managedconfiguration.ManagedConfigurationItemService;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.RendererManager;
import com.atlassian.jira.issue.customfields.manager.OptionsService;
import com.atlassian.jira.issue.fields.ProjectFieldLayoutSchemeHelper;
import com.atlassian.jira.issue.fields.config.manager.IssueTypeSchemeManager;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.fields.screen.FieldScreenManager;
import com.atlassian.jira.notification.NotificationSchemeManager;
import com.atlassian.jira.project.type.BrowseProjectTypeManager;
import com.atlassian.jira.user.UserProjectHistoryManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.workflow.ProjectWorkflowSchemeHelper;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.migration.MigrationHelperFactory;
import com.atlassian.jira.workflow.migration.WorkflowSchemeMigrationTaskAccessor;
import com.atlassian.mail.server.MailServerManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.webresource.api.assembler.PageBuilderService;

@SuppressWarnings("unused")
public class ComponentImports {
    @ComponentImport
    JiraBaseUrls jiraBaseUrls;

    @ComponentImport
    LoginService loginService;

    @ComponentImport
    ProjectRoleService projectRoleService;

    @ComponentImport
    ManagedConfigurationItemService managedConfigurationItemService;

    @ComponentImport
    FieldScreenManager fieldScreenManager;

    @ComponentImport
    RendererManager rendererManager;

    @ComponentImport
    MailServerManager mailServerManager;

    @ComponentImport
    NotificationSchemeManager notificationSchemeManager;

    @ComponentImport
    UserManager userManager;

    @ComponentImport
    UserPickerSearchService userPickerSearchService;

    @ComponentImport
    com.atlassian.jira.plugin.userformat.UserFormats userFormats;

    @ComponentImport
    com.atlassian.jira.bc.project.component.ProjectComponentService projectComponentService;

    @ComponentImport
    com.atlassian.jira.util.DateFieldFormat dateFieldFormat;

    @ComponentImport
    VersionService versionService;

    @ComponentImport
    com.atlassian.jira.plugin.profile.UserFormatManager userFormatManager;

    @ComponentImport
    com.atlassian.jira.permission.PermissionSchemeManager permissionSchemeManager;

    @ComponentImport
    com.atlassian.jira.security.SecurityTypeManager securityTypeManager;

    @ComponentImport
    com.atlassian.jira.permission.ProjectPermissionSchemeHelper projectPermissionSchemeHelper;

    @ComponentImport
    com.atlassian.jira.issue.security.IssueSecuritySchemeManager issueSecuritySchemeManager;

    @ComponentImport
    com.atlassian.jira.issue.security.IssueSecurityLevelManager issueSecurityLevelManager;

    @ComponentImport
    com.atlassian.applinks.host.spi.InternalHostApplication internalHostApplication;

    @ComponentImport
    com.atlassian.jira.project.renderer.ProjectDescriptionRenderer projectDescriptionRenderer;

    @ComponentImport
    com.atlassian.jira.web.component.ModuleWebComponent moduleWebComponent;

    @ComponentImport
    com.atlassian.jira.bc.workflow.WorkflowService workflowService;

    @ComponentImport
    com.atlassian.jira.bc.project.ProjectService projectService;

    @ComponentImport
    com.atlassian.jira.workflow.WorkflowSchemeManager workflowSchemeManager;

    @ComponentImport
    com.atlassian.jira.bc.workflow.WorkflowSchemeService workflowSchemeService;

    @ComponentImport
    com.atlassian.jira.util.velocity.VelocityRequestContextFactory velocityRequestContextFactory;

    @ComponentImport
    com.atlassian.templaterenderer.TemplateRenderer templateRenderer;

    @ComponentImport
    com.atlassian.applinks.api.ApplicationLinkService applicationLinkService;

    @ComponentImport
    com.atlassian.jira.security.xsrf.XsrfTokenGenerator xsrfTokenGenerator;

    @ComponentImport
    com.atlassian.jira.security.PermissionManager permissionManager;

    @ComponentImport
    com.atlassian.jira.security.GlobalPermissionManager globalPermissionManager;

    @ComponentImport
    com.atlassian.plugin.web.WebInterfaceManager webInterfaceManager;

    @ComponentImport
    com.atlassian.jira.security.JiraAuthenticationContext jiraAuthenticationContext;

    @ComponentImport
    com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenSchemeManager issueTypeScreenSchemeManager;

    @ComponentImport
    com.atlassian.jira.issue.fields.screen.ProjectFieldScreenHelper projectFieldScreenHelper;

    @ComponentImport
    CustomFieldManager customFieldManager;

    @ComponentImport
    FieldLayoutManager fieldLayoutManager;

    @ComponentImport
    WorkflowManager workflowManager;

    @ComponentImport
    IssueTypeSchemeManager issueTypeSchemeManager;

    @ComponentImport
    com.atlassian.jira.config.FeatureManager featureManager;

    @ComponentImport
    DateTimeFormatter dateTimeFormatter;

    @ComponentImport
    AvatarService avatarService;

    @ComponentImport
    IssueTypeManager issueTypeManager;

    @ComponentImport
    WorkflowSchemeMigrationTaskAccessor workflowSchemeMigrationTaskAccessor;

    @ComponentImport
    MigrationHelperFactory migrationHelperFactory;

    @ComponentImport
    WebResourceUrlProvider webResourceUrlProvider;

    @ComponentImport
    PageBuilderService pageBuilderService;

    @ComponentImport
    EventPublisher eventPublisher;

    @ComponentImport
    CustomFieldService customFieldService;

    @ComponentImport
    ProjectOperationManager projectOperationManager;

    @ComponentImport
    ApplicationProperties applicationProperties;

    @ComponentImport
    OptionsService optionsService;

    @ComponentImport
    ProjectFieldLayoutSchemeHelper projectFieldLayoutSchemeHelper;

    @ComponentImport
    UserProjectHistoryManager userProjectHistoryManager;

    @ComponentImport
    I18nHelper i18nHelper;

    @ComponentImport
    com.atlassian.jira.issue.security.ProjectIssueSecuritySchemeHelper projectIssueSecuritySchemeHelper;

    @ComponentImport
    com.atlassian.jira.notification.ProjectNotificationsSchemeHelper projectNotificationsSchemeHelper;

    @ComponentImport
    com.atlassian.jira.issue.fields.screen.issuetype.ProjectIssueTypeScreenSchemeHelper projectIssueTypeScreenSchemeHelper;

    @ComponentImport
    com.atlassian.jira.project.ProjectManager projectManager;

    @ComponentImport
    com.atlassian.plugin.webresource.WebResourceManager webResourceManager;

    @ComponentImport
    com.atlassian.jira.task.TaskManager taskManager;

    @ComponentImport
    com.atlassian.jira.issue.IssueManager issueManager;

    @ComponentImport
    com.atlassian.jira.util.EmailFormatter emailFormatter;

    @ComponentImport
    com.atlassian.jira.application.ApplicationRoleManager applicationRoleManager;

    @ComponentImport
    com.atlassian.jira.event.mau.MauEventService mauEventService;

    @ComponentImport
    BrowseProjectTypeManager browseProjectTypeManager;

    @ComponentImport
    com.atlassian.jira.user.UserPropertyManager userPropertyManager;
}
