@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
package com.atlassian.jira.projectconfig.shared;

import com.atlassian.annotations.nonnull.ReturnValuesAreNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;
