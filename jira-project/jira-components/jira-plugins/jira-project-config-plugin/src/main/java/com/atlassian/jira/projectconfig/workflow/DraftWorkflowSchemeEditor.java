package com.atlassian.jira.projectconfig.workflow;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.workflow.DraftWorkflowScheme;

/**
 * @since v5.2
 */
public interface DraftWorkflowSchemeEditor {
    /**
     * Re-assignes the given project's issue types to the given workflow.
     * Possibly re-formats the workflow mapping so that the new workflow is treated as the default one.
     *
     * @param workflowScheme     the scheme we want to change
     * @param issueTypesToAssign issue types that the new workflow will be assigned to
     * @param newWorkflow        workflow to assign to the issue types
     * @param project            JIRA project that the scheme belongs to
     * @return the new workflow scheme.
     */
    DraftWorkflowScheme assign(DraftWorkflowScheme workflowScheme,
                               Iterable<String> issueTypesToAssign, String newWorkflow, Project project);

    /**
     * Deletes the passed workflow from the passed workflow scheme.
     *
     * @param workflowScheme currently active workflow scheme
     * @param deleteWorkflow the workflow to delete.
     * @param project        the project we are doing the operation for. This defines what issue types need to be mapped.
     * @return the new workflow scheme.
     */
    DraftWorkflowScheme delete(DraftWorkflowScheme workflowScheme, String deleteWorkflow, Project project);
}