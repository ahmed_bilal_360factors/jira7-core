package com.atlassian.jira.projectconfig.tab;

import com.atlassian.jira.projectconfig.util.VelocityContextFactory;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.webresource.WebResourceManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @since v4.4
 */
@Component
public class FieldsTab extends WebPanelTab {
    @Autowired
    public FieldsTab(final WebInterfaceManager webInterfaceManager, VelocityContextFactory factory) {
        super(webInterfaceManager, factory, "fields", "view_project_fields");
    }

    @Override
    public String getTitle(ProjectConfigTabRenderContext context) {
        return context.getI18NHelper().getText("admin.project.fields.title", context.getProject().getName());
    }

}
