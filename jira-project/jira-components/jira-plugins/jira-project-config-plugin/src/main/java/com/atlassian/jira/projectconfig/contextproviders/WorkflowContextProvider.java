package com.atlassian.jira.projectconfig.contextproviders;

import com.atlassian.jira.plugin.webfragment.CacheableContextProvider;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.discover.EditWorkflowDiscoverHelper;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.task.TaskDescriptor;
import com.atlassian.jira.util.collect.CompositeMap;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.workflow.AssignableWorkflowScheme;
import com.atlassian.jira.workflow.WorkflowSchemeManager;
import com.atlassian.jira.workflow.migration.WorkflowSchemeMigrationTaskAccessor;
import com.atlassian.plugin.PluginParseException;

import java.util.Map;

/**
 * Context provider for the workflow tab.
 *
 * @since v5.2
 */
public class WorkflowContextProvider implements CacheableContextProvider {
    private final ContextProviderUtils contextProviderUtils;
    private final WorkflowSchemeMigrationTaskAccessor taskAccessor;
    private final WorkflowSchemeManager workflowSchemeManager;
    private final EditWorkflowDiscoverHelper editWorkflowDiscoverHelper;
    private final JiraAuthenticationContext jiraAuthenticationContext;

    public WorkflowContextProvider(ContextProviderUtils contextProviderUtils, WorkflowSchemeMigrationTaskAccessor taskAccessor, WorkflowSchemeManager workflowSchemeManager, EditWorkflowDiscoverHelper editWorkflowDiscoverHelper, JiraAuthenticationContext jiraAuthenticationContext) {
        this.contextProviderUtils = contextProviderUtils;
        this.taskAccessor = taskAccessor;
        this.workflowSchemeManager = workflowSchemeManager;
        this.editWorkflowDiscoverHelper = editWorkflowDiscoverHelper;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    private AssignableWorkflowScheme getWorkflowScheme() {
        return workflowSchemeManager.getWorkflowSchemeObj(contextProviderUtils.getProject());
    }

    private boolean hasDraft() {
        return workflowSchemeManager.hasDraft(getWorkflowScheme());
    }

    private boolean isShared() {
        return workflowSchemeManager.getProjectsUsing(getWorkflowScheme()).size() > 1;
    }

    private boolean isDefault() {
        return getWorkflowScheme().isDefault();
    }

    @Override
    public void init(Map<String, String> params) throws PluginParseException {
    }

    @Override
    public Map<String, Object> getContextMap(Map<String, Object> context) {
        final Project project = contextProviderUtils.getProject();
        MapBuilder<String, Object> contextBuilder = MapBuilder.newBuilder();
        contextBuilder.add("hasDraft", hasDraft());
        contextBuilder.add("isShared", isShared());
        contextBuilder.add("isDefault", isDefault());
        TaskDescriptor taskDescriptor = taskAccessor.getActive(project);
        if (taskDescriptor != null) {
            contextBuilder.add("projectMigration", true);
        } else {
            taskDescriptor = taskAccessor.getActive(getWorkflowScheme());
            if (taskDescriptor != null) {
                contextBuilder.add("projectMigration", false);
            }
        }
        if (taskDescriptor != null) {
            contextBuilder.add("progressURL", taskDescriptor.getProgressURL());
        }
        final boolean discoverShow = editWorkflowDiscoverHelper.shouldShowEditWorkflowDiscover(jiraAuthenticationContext.getLoggedInUser());
        final String learnMoreUrl = editWorkflowDiscoverHelper.getLearnMoreUrl(project);

        contextBuilder.add("showEditWorkflowDiscover", discoverShow);
        contextBuilder.add("learnMoreUrl", learnMoreUrl);

        Map<String, Object> defaults = CompositeMap.of(context, contextProviderUtils.getDefaultContext());
        return CompositeMap.of(contextBuilder.toMap(), defaults);
    }

    @Override
    public String getUniqueContextKey(Map<String, Object> context) {
        return getClass().getName();
    }
}
