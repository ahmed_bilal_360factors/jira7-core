package com.atlassian.jira.projectconfig.util;

import javax.annotation.Nonnull;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.projectconfig.beans.ProjectContext;

/**
 * Locator for a {@link com.atlassian.jira.projectconfig.beans.ProjectContext}.
 * <p/>
 * All arguments are assumed to be non-null unless otherwise noted.
 *
 * @since v6.2
 */
@Nonnull
public interface ProjectContextLocator {
    /**
     * Find a {@link com.atlassian.jira.projectconfig.beans.ProjectContext} for the passed project and issue type.
     *
     * @param projectKey  the projectKey to search.
     * @param issueTypeId the issueTypeId to search.
     * @return the result of the operation. The {@link com.atlassian.jira.bc.ServiceOutcome} either contains errors
     * or the {@link com.atlassian.jira.projectconfig.beans.ProjectContext} if the operation was successful.
     */
    @Nonnull
    ServiceOutcome<ProjectContext> getContext(@Nonnull String projectKey, long issueTypeId);
}
