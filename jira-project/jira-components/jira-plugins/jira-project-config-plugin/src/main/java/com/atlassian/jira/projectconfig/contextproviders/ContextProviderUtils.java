package com.atlassian.jira.projectconfig.contextproviders;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.projectconfig.order.OrderFactory;
import com.atlassian.jira.projectconfig.util.ProjectConfigRequestCache;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.UrlBuilder;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.util.dbc.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 * Provides common utilities aimed at context providers, such as retrieving the current project.
 *
 * @since v4.4
 */
@Component
public class ContextProviderUtils {
    // Keys for context items provided in the default context
    static final String CONTEXT_PROJECT_KEY = "project";
    static final String CONTEXT_IS_ADMIN_KEY = "isAdmin";
    static final String CONTEXT_IS_PROJECT_ADMIN_KEY = "isProjectAdmin";
    static final String CONTEXT_I18N_KEY = "i18n";
    static final String CONTEXT_PROJECT_KEY_ENCODED = "projectKeyEncoded";

    private final ProjectConfigRequestCache cache;
    private final OrderFactory orderFactory;
    private final ContextProviderHelper contextProviderHelper;

    @Autowired
    public ContextProviderUtils(final ProjectConfigRequestCache cache, final OrderFactory orderFactory,
                                final ContextProviderHelper contextProviderHelper) {
        this.cache = cache;
        this.orderFactory = orderFactory;
        this.contextProviderHelper = contextProviderHelper;
    }

    public Project getProject() {
        return cache.getProject();
    }

    public Comparator<String> getStringComparator() {
        return orderFactory.createStringComparator();
    }

    /**
     * Provides a default context that should be provided to all context providers.
     * <p/>
     * In particular, contains:
     * <p/>
     * <dl>
     * <dt>project</dt><dd>The current project, null if no project was selected</dd>
     * <dt>isAdmin</dt><dd>True if current user is a system admin</dd>
     * <dt>isProjectAdmin</dt><dd>True if current user is a project admin</dd>
     * <dt>i18n</dt><dd>An i18nHelper object</dd>
     * </dl>
     *
     * @return the default context
     */
    public Map<String, Object> getDefaultContext() {
        Project project = getProject();
        return MapBuilder.<String, Object>newBuilder()
                .add(CONTEXT_PROJECT_KEY, project)
                .add(CONTEXT_PROJECT_KEY_ENCODED, contextProviderHelper.encode(project.getKey()))
                .add(CONTEXT_IS_ADMIN_KEY, contextProviderHelper.hasAdminPermission())
                .add(CONTEXT_IS_PROJECT_ADMIN_KEY, contextProviderHelper.hasProjectAdminPermission(getProject()))
                .add(CONTEXT_I18N_KEY, contextProviderHelper.getI18nHelper())
                .toMap();
    }

    public Set<String> flattenErrors(ErrorCollection collection) {
        Assertions.notNull("collection", collection);
        Set<String> errors = new LinkedHashSet<String>();
        errors.addAll(collection.getErrorMessages());
        errors.addAll(collection.getErrors().values());
        return errors;
    }

    public String getBaseUrl() {
        return contextProviderHelper.getBaseUrl();
    }

    public UrlBuilder createUrlBuilder(String basename) {
        return contextProviderHelper.createUrlBuilder(basename);
    }
}
