package com.atlassian.jira.projectconfig.rest.global;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.workflow.WorkflowSchemeService;
import com.atlassian.jira.projectconfig.rest.WorkflowSchemeRestHelper;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.workflow.AssignableWorkflowScheme;
import com.atlassian.jira.workflow.DraftWorkflowScheme;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowScheme;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.Set;

/**
 * REST resource for the global workflow scheme editor.
 *
 * @since 6.0
 */
@Path("workflowschemeeditor")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@WebSudoRequired
public class WorkflowSchemeEditorResource {
    private final WorkflowSchemeService workflowSchemeService;
    private final JiraAuthenticationContext authenticationContext;
    private final WorkflowSchemeRestHelper helper;

    public WorkflowSchemeEditorResource(WorkflowSchemeService workflowSchemeService,
                                        JiraAuthenticationContext authenticationContext, WorkflowSchemeRestHelper helper) {
        this.workflowSchemeService = workflowSchemeService;
        this.authenticationContext = authenticationContext;
        this.helper = helper;
    }

    @GET
    @Path("{id}")
    public Response getScheme(@PathParam("id") long id) {
        ApplicationUser user = getUser();

        ServiceOutcome<AssignableWorkflowScheme> workflowSchemeOutcome = workflowSchemeService.getWorkflowScheme(user, id);
        if (!workflowSchemeOutcome.isValid()) {
            return WorkflowSchemeRestHelper.createErrorResponse(workflowSchemeOutcome);
        }
        AssignableWorkflowScheme workflowScheme = workflowSchemeOutcome.getReturnedValue();

        ServiceOutcome<DraftWorkflowScheme> draftWorkflowSchemeOutcome = workflowSchemeService.getDraftWorkflowScheme(user, workflowScheme);
        if (!draftWorkflowSchemeOutcome.isValid()) {
            return WorkflowSchemeRestHelper.createErrorResponse(draftWorkflowSchemeOutcome);
        }
        DraftWorkflowScheme draftWorkflowScheme = draftWorkflowSchemeOutcome.getReturnedValue();

        return helper.builder().setUser(user)
                .setAssignableWorkflowScheme(workflowScheme)
                .setDraftWorkflowScheme(draftWorkflowScheme)
                .build();
    }

    @DELETE
    @Path("{id}/draft")
    public Response discardDraft(@PathParam("id") long id) {
        ApplicationUser user = getUser();

        ServiceOutcome<AssignableWorkflowScheme> workflowSchemeOutcome = workflowSchemeService.getWorkflowScheme(user, id);
        if (!workflowSchemeOutcome.isValid()) {
            return WorkflowSchemeRestHelper.createErrorResponse(workflowSchemeOutcome);
        }
        AssignableWorkflowScheme workflowScheme = workflowSchemeOutcome.getReturnedValue();

        ServiceOutcome<DraftWorkflowScheme> draftWorkflowSchemeOutcome = workflowSchemeService.getDraftWorkflowScheme(user, workflowScheme);
        if (!draftWorkflowSchemeOutcome.isValid()) {
            return WorkflowSchemeRestHelper.createErrorResponse(draftWorkflowSchemeOutcome);
        }
        DraftWorkflowScheme draftWorkflowScheme = draftWorkflowSchemeOutcome.getReturnedValue();
        if (draftWorkflowScheme != null) {
            ServiceOutcome<Void> deleteOutcome = workflowSchemeService.deleteWorkflowScheme(user, draftWorkflowScheme);
            if (!deleteOutcome.isValid()) {
                return WorkflowSchemeRestHelper.createErrorResponse(deleteOutcome);
            }
        }

        return helper.builder().setUser(user)
                .setAssignableWorkflowScheme(workflowScheme)
                .build();
    }

    @PUT
    @Path("{id}")
    public Response assignIssueType(@PathParam("id") long id, final WorkflowSchemeRequest request) {
        if (StringUtils.isEmpty(request.workflow)) {
            return WorkflowSchemeRestHelper.badRequest();
        }

        return editScheme(id, new Editor() {
            @Override
            public AssignableWorkflowScheme edit(AssignableWorkflowScheme scheme) {
                AssignableWorkflowScheme.Builder builder = scheme.builder();
                processAssignRequest(builder, request);
                return builder.build();
            }

            @Override
            public DraftWorkflowScheme edit(DraftWorkflowScheme scheme) {
                DraftWorkflowScheme.Builder builder = scheme.builder();
                processAssignRequest(builder, request);
                return builder.build();
            }
        });
    }

    @DELETE
    @Path("{id}/workflow")
    public Response removeWorkflow(@PathParam("id") long id, final RemoveWorkflowRequest request) {
        if (StringUtils.isEmpty(request.workflow)) {
            return WorkflowSchemeRestHelper.badRequest();
        }

        return editScheme(id, new Editor() {
            @Override
            public AssignableWorkflowScheme edit(AssignableWorkflowScheme scheme) {
                AssignableWorkflowScheme.Builder builder = scheme.builder();
                removeAndReassignDefault(builder, request);
                return builder.build();
            }

            @Override
            public DraftWorkflowScheme edit(DraftWorkflowScheme scheme) {
                DraftWorkflowScheme.Builder builder = scheme.builder();
                removeAndReassignDefault(builder, request);
                return builder.build();
            }
        });
    }

    @DELETE
    @Path("{id}/issuetype")
    public Response removeIssueTypes(@PathParam("id") long id, final WorkflowSchemeRequest request) {
        if (StringUtils.isEmpty(request.workflow)) {
            return WorkflowSchemeRestHelper.badRequest();
        }

        return editScheme(id, new Editor() {
            @Override
            public AssignableWorkflowScheme edit(AssignableWorkflowScheme scheme) {
                AssignableWorkflowScheme.Builder builder = scheme.builder();
                removeIssueTypes(builder, request);
                return builder.build();
            }

            @Override
            public DraftWorkflowScheme edit(DraftWorkflowScheme scheme) {
                DraftWorkflowScheme.Builder builder = scheme.builder();
                removeIssueTypes(builder, request);
                return builder.build();
            }
        });
    }

    @PUT
    @Path("{id}/name")
    public Response updateName(@PathParam("id") long id, final String name) {
        if (StringUtils.isEmpty(name)) {
            return WorkflowSchemeRestHelper.badRequest();
        }

        return editScheme(id, new Function<AssignableWorkflowScheme.Builder, Void>() {
            @Override
            public Void apply(AssignableWorkflowScheme.Builder input) {
                input.setName(name);
                return null;
            }
        });
    }

    @PUT
    @Path("{id}/description")
    public Response updateDescription(@PathParam("id") long id, final String description) {
        return editScheme(id, new Function<AssignableWorkflowScheme.Builder, Void>() {
            @Override
            public Void apply(AssignableWorkflowScheme.Builder input) {
                input.setDescription(description);
                return null;
            }
        });
    }

    private Response editScheme(long id, Function<AssignableWorkflowScheme.Builder, Void> function) {
        ApplicationUser user = getUser();

        ServiceOutcome<AssignableWorkflowScheme> workflowSchemeOutcome = workflowSchemeService.getWorkflowScheme(user, id);
        if (!workflowSchemeOutcome.isValid()) {
            return WorkflowSchemeRestHelper.createErrorResponse(workflowSchemeOutcome);
        }
        AssignableWorkflowScheme workflowScheme = workflowSchemeOutcome.getReturnedValue();

        AssignableWorkflowScheme.Builder builder = workflowScheme.builder();
        function.apply(builder);
        workflowScheme = builder.build();

        workflowSchemeOutcome = workflowSchemeService.updateWorkflowScheme(user, workflowScheme);
        if (!workflowSchemeOutcome.isValid()) {
            return WorkflowSchemeRestHelper.createErrorResponse(workflowSchemeOutcome);
        }
        workflowScheme = workflowSchemeOutcome.getReturnedValue();

        ServiceOutcome<DraftWorkflowScheme> draftWorkflowSchemeOutcome = workflowSchemeService.getDraftWorkflowScheme(user, workflowScheme);
        if (!draftWorkflowSchemeOutcome.isValid()) {
            return WorkflowSchemeRestHelper.createErrorResponse(draftWorkflowSchemeOutcome);
        }
        DraftWorkflowScheme draftWorkflowScheme = draftWorkflowSchemeOutcome.getReturnedValue();

        return helper.builder().setUser(user)
                .setAssignableWorkflowScheme(workflowScheme)
                .setDraftWorkflowScheme(draftWorkflowScheme)
                .build();
    }

    private Response editScheme(long id, Editor editor) {
        ApplicationUser user = getUser();

        ServiceOutcome<AssignableWorkflowScheme> workflowSchemeOutcome = workflowSchemeService.getWorkflowScheme(user, id);
        if (!workflowSchemeOutcome.isValid()) {
            return WorkflowSchemeRestHelper.createErrorResponse(workflowSchemeOutcome);
        }
        AssignableWorkflowScheme workflowScheme = workflowSchemeOutcome.getReturnedValue();

        if (workflowSchemeService.isActive(workflowScheme)) {
            ServiceOutcome<DraftWorkflowScheme> draftWorkflowSchemeOutcome = workflowSchemeService.getDraftWorkflowScheme(user, workflowScheme);
            if (!draftWorkflowSchemeOutcome.isValid()) {
                return WorkflowSchemeRestHelper.createErrorResponse(draftWorkflowSchemeOutcome);
            }
            DraftWorkflowScheme draftWorkflowScheme = draftWorkflowSchemeOutcome.getReturnedValue();

            if (draftWorkflowScheme == null) {
                draftWorkflowSchemeOutcome = workflowSchemeService.createDraft(user, workflowScheme.getId());
                if (!draftWorkflowSchemeOutcome.isValid()) {
                    return WorkflowSchemeRestHelper.createErrorResponse(draftWorkflowSchemeOutcome);
                }
                draftWorkflowScheme = draftWorkflowSchemeOutcome.getReturnedValue();
            }

            draftWorkflowScheme = editor.edit(draftWorkflowScheme);

            draftWorkflowSchemeOutcome = workflowSchemeService.updateWorkflowScheme(user, draftWorkflowScheme);
            if (!draftWorkflowSchemeOutcome.isValid()) {
                return WorkflowSchemeRestHelper.createErrorResponse(draftWorkflowSchemeOutcome);
            }
            draftWorkflowScheme = draftWorkflowSchemeOutcome.getReturnedValue();

            return helper.builder().setUser(user)
                    .setAssignableWorkflowScheme(workflowScheme)
                    .setDraftWorkflowScheme(draftWorkflowScheme)
                    .build();
        } else {
            workflowScheme = editor.edit(workflowScheme);

            workflowSchemeOutcome = workflowSchemeService.updateWorkflowScheme(user, workflowScheme);
            if (!workflowSchemeOutcome.isValid()) {
                return WorkflowSchemeRestHelper.createErrorResponse(workflowSchemeOutcome);
            }
            workflowScheme = workflowSchemeOutcome.getReturnedValue();

            return helper.builder().setUser(user)
                    .setAssignableWorkflowScheme(workflowScheme)
                    .build();
        }
    }

    private ApplicationUser getUser() {
        return authenticationContext.getLoggedInUser();
    }

    private static void processAssignRequest(WorkflowScheme.Builder<?> builder, WorkflowSchemeRequest request) {
        if (request.issueTypes != null) {
            for (String issueType : request.issueTypes) {
                builder.setMapping(issueType, request.workflow);
            }
        }
        if (request.defaultWorkflow) {
            builder.setDefaultWorkflow(request.workflow);
        }
    }

    private static void removeIssueTypes(WorkflowScheme.Builder<?> builder, WorkflowSchemeRequest request) {
        for (String issueType : request.issueTypes) {
            if (Objects.equal(builder.getMapping(issueType), request.workflow)) {
                builder.removeMapping(issueType);
            }
        }
    }

    private static void removeAndReassignDefault(WorkflowScheme.Builder<?> builder, RemoveWorkflowRequest request) {
        String defaultWorkflow = builder.getDefaultWorkflow();
        defaultWorkflow = defaultWorkflow != null ? defaultWorkflow : JiraWorkflow.DEFAULT_WORKFLOW_NAME;
        builder.removeWorkflow(request.workflow);

        if (StringUtils.isNotBlank(request.nextDefaultWorkflow) && Iterables.contains(builder.getMappings().values(), request.nextDefaultWorkflow)) {
            builder.setDefaultWorkflow(request.nextDefaultWorkflow);
        } else if (request.workflow.equals(defaultWorkflow) && !builder.getMappings().isEmpty()) {
            builder.setDefaultWorkflow(Collections.min(builder.getMappings().values(), String.CASE_INSENSITIVE_ORDER));
        }
    }

    private interface Editor {
        AssignableWorkflowScheme edit(AssignableWorkflowScheme scheme);

        DraftWorkflowScheme edit(DraftWorkflowScheme scheme);
    }

    public static class WorkflowSchemeRequest {
        @JsonProperty
        private String workflow;

        @JsonProperty
        private Set<String> issueTypes;

        @JsonProperty
        private boolean defaultWorkflow;
    }

    public static class RemoveWorkflowRequest {
        @JsonProperty
        private String workflow;

        @JsonProperty
        private String nextDefaultWorkflow;
    }
}
