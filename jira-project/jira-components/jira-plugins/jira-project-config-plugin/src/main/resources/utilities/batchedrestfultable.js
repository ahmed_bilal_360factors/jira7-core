define("jira-project-config/utils/batched-restful-table", ["require"], function(require){
    "use strict";

    var Deferred = require('jira/jquery/deferred');
    var jQuery = require('jquery');
    var RestfulTable = require('aui/restful-table');
    var AJSHelper = require('jira-project-config/libs/ajshelper');

    /**
     * An extension of AUI RestfulTable, used for batching requests, rather than a single fetch of all of the entries.
     *
     * To implement, provide options:
     * @property {string} base Base URL
     * @property {?number} startAtKey (default: "startAt")
     * @property {?number} maxResultsKey (default: "maxResults")
     * @property {?string} valuesKey (required if entries are nested in JSON object)
     * @property {?number} pageSize (default on server)
     */
    var BatchedRestfulTable = RestfulTable.extend({

        functionsToExecuteAfterFetch: [],

        //
        // PUBLIC API
        //
        /**
         * Gets isLastLast page to verify if more entries are still to come
         *
         * @returns {boolean} isLastPage
         */
        getIsLastPage: function () {
            return this.isLastPage;
        },

        /**
         * Action for table to fetch the next page of entries
         *
         * @returns {promise} fetch and DOM update complete
         */
        fetchMore: function () {
            var instance = this;

            instance.showGlobalLoading();
            var promise = instance._fetchMore();
            promise.always(function () {
                instance.hideGlobalLoading();
            });

            return promise;
        },

        move: function (context) {
            var instance = this;

            var move = function () {
                context.nextItem = context.item.next();
                context.prevItem = context.item.prev();
                RestfulTable.prototype.move.call(instance, context);
            };

            if (!context.nextItem.length && !this.getIsLastPage()) {
                context.row.showLoading();
                this.functionsToExecuteAfterFetch.push(move);
            } else {
                move();
            }
        },

        //
        // OVERRIDING METHODS
        //
        /**
         * Overriding RestfulTable to use BatchedRestfulTable's _fetchMore() and _update() methods
         *
         * @private
         */
        fetchInitialResources: function () {
            var instance = this;
            if (instance.options.resources.base) {
                instance.isLastPage = false;
                var promise = instance._fetchMore(instance);
                promise.done(function () {
                    instance._notifiyInitialisation();
                });
                promise.fail(function () {
                    instance.isLastPage = true;
                });
            }
            else {
                // fail because not enough base, yo
                instance.isLastPage = true;
            }
        },

        //
        // PRIVATE API
        //
        /**
         * Update the table with entries
         * Called by _fetchMore()
         *
         * Code duplicated from section of AUI.RestfulTable.populate();
         *
         * @param entries Array of entries to rendered into rows
         * @private
         */
        _update: function (entries) {
            this.hideGlobalLoading();
            if (entries && entries.length) {
                // Add all the entries to collection and render them
                this.renderRows(entries);
                // show message to user if we have no entries
                if (this.isEmpty()) {
                    this.showNoEntriesMsg();
                }
            } else {
                this.showNoEntriesMsg();
            }
        },

        /**
         * Initialise table and trigger events
         * Called by _fetchInitialResource.
         *
         * Code duplicated from section of AUI.RestfulTable.populate();
         *
         * @private
         */
        _notifiyInitialisation: function () {
            // from AUI RestfulTable
            this.$table.append(this.$thead);

            if (this.options.createPosition === "bottom") {
                this.$table.append(this.$tbody)
                    .append(this.$create);
            } else {
                this.$table
                    .append(this.$create)
                    .append(this.$tbody);
            }

            this.$table.removeClass(this.classNames.LOADING)
                .trigger(this._event.INITIALIZED, [this]);

            AJSHelper.triggerEvtForInst(this._event.INITIALIZED, this, [this]);
        },

        /**
         * Gets default options
         *
         * @param {Object} options
         */
        _getDefaultOptions: function (options) {
            var defaultOptions = {
                resources: {
                    startAtKey: options.startAtKey || "startAt",
                    maxResultsKey: options.maxResultsKey || "maxResults",
                    pageSize: options.pageSize || 100
                }
            };
            return jQuery.extend(RestfulTable.prototype._getDefaultOptions(options), defaultOptions);
        },

        /**
         * Fetch a single page of results then _update()
         *
         * @returns {promise} fetch and DOM update complete
         * @private
         */
        _fetchMore: function () {
            var instance = this;
            var control = Deferred();

            if (this.getIsLastPage()) {
                return control.reject().promise();
            }

            var startAt = instance._models.length;
            var getUrl = this._getPaginatedGetUrl(instance.options, startAt);
            var promise = jQuery.get(getUrl);

            promise.then(function (response) {
                var entries = instance._entries(response);
                instance.isLastPage = !!response.isLast;
                instance._update(entries);
                AJSHelper.triggerEvtForInst(
                    BatchedRestfulTable.Events.MORE_FETCHED,
                    instance,
                    [instance]
                );
                while (instance.functionsToExecuteAfterFetch.length > 0) {
                    instance.functionsToExecuteAfterFetch.pop()();
                }
            });
            promise.done(function () {
                control.resolve();
            });
            promise.fail(function () {
                control.reject();
                AJSHelper.messages.error(jQuery("#project-config-panel-versions"), {
                    body: AJS.escapeHTML(AJS.I18n.getText("admin.manageversions.loading.failed"))
                });
            });

            return control.promise();
        },

        /**
         * Takes a REST response, and returns the entries.
         *
         * @param response REST response from server
         * @returns {[entries]} An array of entries
         * @private
         */
        _entries: function (response) {
            var valuesKey = this.options.resources.valuesKey;
            if (valuesKey) {
                return response[valuesKey];
            } else {
                return response;
            }
        },

        /**
         * Builds REST fetch URL based on user provided options
         *
         * @param options Instance options
         * @param startAt Calculated start at point
         * @returns {string} REST fetch URL
         * @private
         */
        _getPaginatedGetUrl: function (options, startAt) {
            var maxSize = options.resources.pageSize;
            var url = options.resources.base +
                "&" + options.resources.startAtKey + "=" + startAt +
                "&" + options.resources.maxResultsKey + "=" + maxSize;

            return url;
        }
    });

    // Custom events
    BatchedRestfulTable.Events = {
        MORE_FETCHED: "BatchedRestfulTable.moreFetched"
    };

    return BatchedRestfulTable;
});
AJS.namespace("JIRA.ProjectConfig.BatchedRestfulTable", null, require("jira-project-config/utils/batched-restful-table"));
