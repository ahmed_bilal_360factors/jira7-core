define("jira-project-config/utils/region", [
    "jira-project-config/marionette",
    "jira/util/events",
    "jira/util/events/types",
    "jira/util/events/reasons"
], function (Marionette,
             Events,
             Types,
             Reasons) {
    "use strict";

    return Marionette.Region.extend(
        /** @lends JIRA.ProjectConfig.Region# */
        {
            /**
             * @classdesc A region tailored to JIRA that fires the NEW_CONTENT_ADDED event when a view shown. Any
             * region that may contain a view that has content that needs enhancement via NEW_CONTENT_ADDED hooks
             * should either use this region directly, or extend it.
             *
             * When writing an extension to this region, it's essential to call the prototype's initialize() method.
             * @constructs
             * @example
             * var Layout = Marionette.Layout.extend({
             *     regions: {
             *         content: {
             *             selector: "#content",
             *             regionType: JIRA.ProjectConfig.Region
             *         }
             *     }
             * });
             *
             * var layout = new Layout();
             * layout.render()
             * layout.content.show(new Backbone.View());
             * @extends Marionette.Region
             */
            initialize: function () {
                this.listenTo(this, "show", function () {
                    Events.trigger(Types.NEW_CONTENT_ADDED, [this.$el, Reasons.contentRefreshed]);
                });
            }
        });
});
AJS.namespace("JIRA.ProjectConfig.Region", null, require("jira-project-config/utils/region"));
