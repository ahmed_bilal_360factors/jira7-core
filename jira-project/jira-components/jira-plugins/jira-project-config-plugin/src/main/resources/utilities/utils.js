define("jira-project-config/utils", [
    "jira/dialog/form-dialog",
    "jira/ajs/ajax/smart-ajax",
    "wrm/context-path",
    "underscore",
    "jquery"
], function (FormDialog,
             SmartAjax,
             contextPath,
             _,
             $) {
    var restVersion = "2";
    var restBaseUrl = contextPath() + "/rest/api/" + restVersion;

    return {
        REST_VERSION: restVersion,
        REST_BASE_URL: restBaseUrl,
        getRestVersion: function () {
            return restVersion;
        },
        getRestBaseUrl: function () {
            return restBaseUrl;
        },
        getProjectKey: function () {
            return $("meta[name=projectKey]").attr("content");
        },
        getKey: function () {
            return $("meta[name=projectKey]").attr("content");
        },
        getProjectId: function () {
            return $("meta[name=projectId]").attr("content");
        },
        getId: function () {
            return $("meta[name=projectId]").attr("content");
        },
        convertXHRToSmartAjaxResult: function (xhr) {
            var textStatus = xhr.status >= 400 ? "error" : "success";
            return SmartAjax.SmartAjaxResult(xhr, new Date().getTime(), textStatus, JSON.parse(xhr.responseText));
        },
        /**
         * Creates and shows a JIRA.FormDialog. The default submit handler is overridden to serialize the form values and
         * update the associated model with them. Updating of model triggers a change event which re-renders the table row.
         *
         * @param {Backbone.View} View - dialog contents
         * @param dialogOptions - additional options that can be used to override options of {JIRA.FormDialog}
         * @return {JIRA.FormDialog.}
         */
        openDialogForRow: function (view, row, id, dialogOptions) {
            dialogOptions = dialogOptions || {};

            var dialogForm = new view({
                    model: row.model
                });

            var dialog = new FormDialog(_.extend({
                id: id,
                content: function (callback) {
                    dialogForm.render(function (el) {
                        callback(el);
                    });
                },
                submitHandler: function (e) {
                    dialogForm.submit(this.$form.serializeObject(), row, function () {
                        dialog.hide();
                    });
                    e.preventDefault();
                }
            }, dialogOptions));

            dialog.show();

            return dialog;
        }
    };
});
/**
 * Legacy namespacing
 * @deprecated since 7.1
 */
AJS.namespace("JIRA.REST_VERSION", null, require("jira-project-config/utils").getRestVersion());
AJS.namespace("JIRA.REST_BASE_URL", null, require("jira-project-config/utils").getRestBaseUrl());

AJS.namespace("JIRA.ProjectConfig.getKey", null, require("jira-project-config/utils").getProjectKey);
AJS.namespace("JIRA.ProjectConfig.getId", null, require("jira-project-config/utils").getProjectId);

AJS.namespace("AJS.convertXHRToSmartAjaxResult", null, require("jira-project-config/utils").convertXHRToSmartAjaxResult);
AJS.namespace("AJS.openDialogForRow", null, require("jira-project-config/utils").openDialogForRow);

