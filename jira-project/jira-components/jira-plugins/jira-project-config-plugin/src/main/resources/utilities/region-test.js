AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:project-config-utilities"], function(){
    "use strict";

    var jQuery = require("jquery");
    var Reasons = require("jira/util/events/reasons");
    var Types = require("jira/util/events/types");
    var Events = require("jira/util/events");
    var Backbone = require("jira-project-config/backbone");
    var Region = require("jira-project-config/utils/region");

    module("JIRA.ProjectConfig.Region");

    test("standalone", 4, function () {
        var el;
        var handler;
        var region;

        el = jQuery("<div></div>")[0];
        handler = function (e, context, reason) {
            strictEqual(reason, Reasons.contentRefreshed);
            ok(context[0] === el);
        };
        region = new Region({
            el: el
        });

        try {
            Events.bind(Types.NEW_CONTENT_ADDED, handler);
            region.show(new Backbone.View());
            region.show(new Backbone.View());
        } finally {
            Events.unbind(Types.NEW_CONTENT_ADDED, handler);
        }
    });

    test("extended", 5, function () {
        var el;
        var ExtendedRegion;
        var handler;
        var region;

        el = jQuery("<div></div>");
        ExtendedRegion = Region.extend({
            initialize: function (options) {
                this.dummy = options.dummy;
                Region.prototype.initialize.apply(this, arguments);
            }
        });
        handler = function (e, context, reason) {
            strictEqual(reason, Reasons.contentRefreshed);
            ok(context[0] === el[0]);
        };
        region = new ExtendedRegion({
            el: el,
            dummy: "dummy"
        });

        strictEqual(region.dummy, "dummy");

        try {
            Events.bind(Types.NEW_CONTENT_ADDED, handler);
            region.show(new Backbone.View());
            region.show(new Backbone.View());
        } finally {
            Events.unbind(Types.NEW_CONTENT_ADDED, handler);
        }
    });
});

