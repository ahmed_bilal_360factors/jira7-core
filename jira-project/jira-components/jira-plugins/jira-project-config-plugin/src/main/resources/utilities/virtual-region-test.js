AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:project-config-utilities"], function(){
    "use strict";

    var Backbone = require("jira-project-config/backbone");
    var Marionette = require("jira-project-config/marionette");
    var VirtualRegion = require("jira-project-config/utils/virtual-region");
    var jQuery = require("jquery");

    module("JIRA.ProjectConfig.VirtualRegion");

    test("standalone", 4, function () {
        var region;
        var iView;
        var ttView;

        region = new VirtualRegion({
            el: jQuery("<div><b></b></div>")[0],
            target: "b"
        });
        iView = new Backbone.View({
            tagName: "i"
        });
        ttView = new Backbone.View({
            tagName: "tt"
        });

        strictEqual(jQuery(region.el).html(), "<b></b>");
        region.show(iView);
        strictEqual(jQuery(region.el).html(), "<i></i>");
        region.show(iView);
        strictEqual(jQuery(region.el).html(), "<i></i>");
        region.show(ttView);
        strictEqual(jQuery(region.el).html(), "<tt></tt>");
    });

    test("in a layout", 1, function () {
        var Layout;
        var layout;
        var view;

        Layout = Marionette.Layout.extend({
            initialize: function () {
                var test;
                test = new VirtualRegion({
                    el: this.el,
                    target: "b"
                });
                this.addRegion("test", test);
            },

            template: function () {
                return '<div><b></b></div>';
            }
        });
        view = new Backbone.View({tagName: "i"});

        layout = new Layout();
        layout.render();

        layout.test.show(view);
        strictEqual(layout.$el.html(), '<div><i></i></div>');
    });
});
