define("jira-project-config/utils/paged-scrollable", ["require"], function(require){
    "use strict";

    var jQuery = require('jquery');
    var _ = require('underscore');

    /**
     * A widget that will handle scroll events and load new data as the user nears the edge of the page.
     *
     * @param scrollPaneSelector - the element with overflow: auto | scroll
     * @param options see PagedScrollable.defaults.
     */
    function PagedScrollable(scrollPaneSelector, options) {

        var self = this;

        this.$scrollElement = jQuery(scrollPaneSelector || window);
        if (jQuery.isWindow(this.$scrollElement[0])) {
            // we still want to attach to window.scroll, but documentElement has the properties we need to look at.
            var docEl = window.document.documentElement;
            this.getPaneHeight = function () {
                return docEl.clientHeight;
            };
            this.getContentHeight = function () {
                return docEl.scrollHeight;
            };
        }

        this.options = jQuery.extend({}, PagedScrollable.defaults, options);
        this.functionCall = this.options.functionCall;
        this._eventHandlers = [];
        PagedScrollable.prototype.reset.call(this);

        this.addScrollListener(function () {
            self.loadIfRequired();
        });

        if (this.options.preventOverscroll) {
            this._bindOverscrollPrevention();
        }

        jQuery(window).on('resize', this._resizeHandler = function () {
            self.loadIfRequired();
        });

        self.loadIfRequired();

        return this;
    }

    /**
     * PagedScrollable default options
     *
     * @property {number} scrollDelay - the number of milliseconds to debounce before handling a scroll event.
     * @property {number} bufferPixels - load more data if the user scrolls within this many pixels of the edge of the
     *         loaded data.
     * @property {boolean} preventOverscroll - Whether to prevent scrolling past the beginning or end of a scrollable
     *         element from causing the window to scroll
     * @property {boolean} functionCall - function to execute when the page is scrolled to the bottom, if it's a promise, then the scroll handling will be suspended until the promise is resolved
     */
    PagedScrollable.defaults = {
        scrollDelay: 250,
        bufferPixels: 0,
        preventOverscroll: false,
        functionCall: function () {
        }
    };

    PagedScrollable.prototype.reset = function () {
        this.clearScrollListeners();

        if (this._resizeHandler) {
            jQuery(window).off('resize', this._resizeHandler);
            this._resizeHandler = null;
        }

        // must happen after this.cancelRequest() to avoid the scrollable becoming suspended on a reinit.
        this._suspended = false;
    };

    PagedScrollable.prototype.destroy = function () {
        this.reset();
        delete this.$scrollElement;
    };

    /**
     * Stop requesting new data.  Any requests already in the pipeline will complete.
     * To resume requesting data, call PagedScrollable.resume();
     */
    PagedScrollable.prototype.suspend = function () {
        this._suspended = true;
    };

    /**
     * Resume requesting new data.
     */
    PagedScrollable.prototype.resume = function () {
        this._suspended = false;

        // if they are near the top/bottom of the page, request the data they need immediately.
        return this.loadIfRequired();
    };

    PagedScrollable.prototype.isSuspended = function () {
        return this._suspended;
    };

    /**
     * @returns {Number} the scroll top of the scrollable element
     */
    PagedScrollable.prototype.getScrollTop = function () {
        return this.$scrollElement.scrollTop();
    };

    /**
     * Set the scroll top of the scrollable element
     *
     * @param {Number} scrollTop the scroll to to set
     */
    PagedScrollable.prototype.setScrollTop = function (scrollTop) {
        this.$scrollElement.scrollTop(scrollTop);
    };

    /**
     * @returns {jQuery} a jQuery object for the scrollable element
     */
    PagedScrollable.prototype.getPane = function () {
        return this.$scrollElement;
    };

    /**
     * @returns {Number} the visible height of the scrollable element
     */
    PagedScrollable.prototype.getPaneHeight = function () {
        return this.$scrollElement[0].clientHeight;
    };

    /**
     * @returns {Number} the actual height of the scrollable element
     */
    PagedScrollable.prototype.getContentHeight = function () {
        return this.$scrollElement[0].scrollHeight;
    };

    /**
     * @param {String} opt the option to return
     * @returns {*} the value of the object
     */
    PagedScrollable.prototype.getOption = function (opt) {
        if (Object.prototype.hasOwnProperty.call(this.options, opt)) {
            return this.options[opt];
        }
        return undefined;
    };

    /**
     * Override the existing options
     *
     * @param {Object} opts the options to override
     */
    PagedScrollable.prototype.setOptions = function (opts) {
        if (jQuery.isPlainObject(opts)) {
            this.options = jQuery.extend(this.options, opts);
        }
    };

    /**
     * Bind a scroll listener to the paged scrollable
     *
     * @param {Function} func the scroll listener
     */
    PagedScrollable.prototype.addScrollListener = function (func) {
        var handler = this.scrollDelay ? _.debounce(func, this.scrollDelay) : func;
        this._eventHandlers.push(handler);
        this.$scrollElement.on('scroll.paged-scrollable', handler);
    };

    PagedScrollable.prototype._bindOverscrollPrevention = function () {
        var _this = this;

        function overscrollPrevention(e, delta) {
            var height = jQuery(_this).outerHeight();
            var scrollHeight = _this.scrollHeight;

            if ((_this.scrollTop === (scrollHeight - height) && delta < 0) || (_this.scrollTop === 0 && delta > 0)) {
                //If at the bottom scrolling down, or at the top scrolling up
                e.preventDefault();
            }
        }

        this._eventHandlers.push(overscrollPrevention);
        this.$scrollElement.on('mousewheel.paged-scrollable', overscrollPrevention);
    };

    /**
     * Unbind all listeners
     */
    PagedScrollable.prototype.clearScrollListeners = function () {
        var self = this;
        _.each(this._eventHandlers, function (handler) {
            self.$scrollElement.unbind('.paged-scrollable', handler);
        });
        this._eventHandlers.length = 0;
    };

    PagedScrollable.prototype.loadIfRequired = function () {
        if (this.isSuspended()) {
            return;
        }

        if (!jQuery.isWindow(this.getPane()[0]) && this.getPane().is(":hidden")) {
            return;
        }

        var scrollTop = this.getScrollTop();
        var scrollPaneHeight = this.getPaneHeight();
        var contentHeight = this.getContentHeight();
        var scrollBottom = scrollPaneHeight + scrollTop;

        // In Chrome on Windows at some font sizes (Ctrl +), the scrollPaneHeight is rounded down, but contentHeight is
        // rounded up (I think). This means there is a 1px difference between them and the event won't fire.
        var chromeWindowsFontChangeBuffer = 1;

        if (scrollBottom + chromeWindowsFontChangeBuffer >= contentHeight - this.options.bufferPixels) {

            var self = this;

            var functionResult = self.functionCall();
            if (functionResult.done) { // if function result is a promise then lets wait until it is resolved
                self.suspend();
                functionResult.done(function () {
                    self.resume();
                });
            }
        }
    };

    return PagedScrollable;
});
AJS.namespace("JIRA.ProjectConfig.PageScroller", null, require("jira-project-config/utils/paged-scrollable"));
