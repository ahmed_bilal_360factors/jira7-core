define("jira-project-config/utils/widgets/progress-indicator", ["require"], function(require){
    "use strict";

    var Marionette = require('jira-project-config/marionette');
    var Template = require('jira-project-config/utils/widgets/progress-indicator/templates');
    var frameHeight = 40;
    var imageHeight = 440;

    /**
     * @class
     * @classdesc A view that displays a progress indicator and blanket over the closest relative parent.
     * @extends Marionette.ItemView
     */
    return Marionette.ItemView.extend(
        /** @lends JIRA.ProjectConfig.ProgressIndicator.View# */
        {
            className: "project-config-progress-indicator-blanket",

            template: Template.render,

            ui: {
                spinner: ".project-config-spinner"
            },

            onBeforeClose: function () {
                clearInterval(this._intervalID);
            },

            onRender: function () {
                var currentOffset = 0;
                var instance = this;

                function showNextFrame() {
                    currentOffset = (currentOffset - frameHeight) % imageHeight;
                    instance.ui.spinner.css("background-position", "0 " + currentOffset + "px");
                }

                clearInterval(this._intervalID);
                this._intervalID = setInterval(showNextFrame, 50);
            }
        });
});
AJS.namespace("JIRA.ProjectConfig.ProgressIndicator.View", null, require("jira-project-config/utils/widgets/progress-indicator"));
