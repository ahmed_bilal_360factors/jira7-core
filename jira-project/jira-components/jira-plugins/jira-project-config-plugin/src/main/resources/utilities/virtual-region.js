define("jira-project-config/utils/virtual-region", [
    "jira-project-config/utils/region",
    "jira-project-config/marionette",
    "underscore",
    "jquery"
], function (Region,
             Marionette,
             _,
             $) {
    "use strict";

    return Region.extend(
        /** @lends JIRA.ProjectConfig.VirtualRegion# */
        {
            /**
             * @classdesc A region that uses a view's element to *replace* the region element, rather than insert
             * it as a child.
             *
             * Additionally, as with a normal region, it's necessary for the region's target to exist.
             * @constructs
             * @example
             * var ExampleLayout = Marionette.Layout.extend({
         *     initialize: function () {
         *         var first,
         *             second;
         *
         *         first = new JIRA.ProjectConfig.VirtualRegion({
         *             el: this.$el,
         *             target: "#first"
         *         });
         *
         *         second = new JIRA.ProjectConfig.VirtualRegion({
         *             el: this.$el,
         *             target: "#second"
         *         });
         *
         *         this.addRegion("first", first);
         *         this.addRegion("second" second);
         *     },
         *
         *     template: function () {
         *         return [
         *             '&lt;div id="first"&gt;&lt;/div&gt;',
         *             '&lt;div id="second"&gt;&lt;/div&gt;'
         *         ].join('')
         *     }
         * });
             *
             * var layout = new ExampleLayout();
             * layout.render()
             * layout.first.show(new Backbone.View());
             * layout.second.show(new Backbone.View());
             * @extends JIRA.ProjectConfig.Region
             * @param {DOMElement} options.el Required like any normal region.
             * @param {string} options.target A selector targeting the element which gets swapped out by a view's element
             */
            initialize: function (options) {
                this.target = options.target;
                delete options.target;
                Region.prototype.initialize.apply(this, arguments);
            },

            show: function (view) {
                var isViewClosed = view.isClosed || _.isUndefined(view.$el);
                var isDifferentView = view !== this.currentView;

                if (this.currentView && (isViewClosed || isDifferentView)) {
                    this.stub = $('<div style="display: none"></div>');
                    this.currentView.$el.replaceWith(this.stub);
                }
                return Marionette.Region.prototype.show.apply(this, arguments);
            },

            /**
             * @private
             */
            open: function (view) {
                var existing = this.stub || this.$el.find(this.target);
                existing.replaceWith(view.el);
            }
        });
});

AJS.namespace("JIRA.ProjectConfig.VirtualRegion", null, require("jira-project-config/utils/virtual-region"));
