define(
    "jira-project-config/component/view/edit-row",
    ['aui/restful-table'],
    function(RestfulTable) {
        "use strict";

        var EditRow = RestfulTable.EditRow;
        /**
         * View for create/edit component
         */
        return EditRow.extend({
            initialize: function () {
                // call super
                EditRow.prototype.initialize.apply(this, arguments);

                this.bind(this._event.RENDER, function () {
                    this.$el.addClass("project-config-components-add-fields");
                });
            },
            submit: function () {
                var $leadError = this.$(".project-config-component-lead .error");

                if ($leadError.length === 0) {
                    EditRow.prototype.submit.apply(this, arguments);
                } else {
                    this.focus();
                }
            }
        });
    }
);

AJS.namespace("JIRA.Admin.Component.EditComponentRow", null, require("jira-project-config/component/view/edit-row"));
