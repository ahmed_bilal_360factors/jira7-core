define("jira-project-config/component/view/row", [
    'aui/restful-table',
    "jira-project-config/component/view/delete-form",
    "jira-project-config/utils",
    "jira/userhover/userhover"
], function(RestfulTable, DeleteForm, Utils, userhover) {
    "use strict";

    var Row = RestfulTable.Row;

    /**
     * Handles rendering, interaction and updating (delegating to model) of a single version
     */
    return Row.extend({

        initialize: function () {

            // call super
            Row.prototype.initialize.apply(this, arguments);

            // crap work around to handle backbone not extending events
            // (https://github.com/documentcloud/backbone/issues/244)
            this.events["click .project-config-component-delete"] = "destroy";
            this.delegateEvents();

            this.bind(this._event.RENDER, function () {

                var id = this.model.get("id");

                this.$el.addClass("project-config-component")
                    .attr("id", "component-" + id + "-row")
                    .attr("data-id", id);

                userhover(this.el); // Add user hover for component lead
            });
        },

        destroy: function (e) {
            this.trigger("focus");
            Utils.openDialogForRow(DeleteForm, this, "component-" + this.model.get("id") + "-delete-dialog");
            e && e.preventDefault();
        }
    });
});

AJS.namespace("JIRA.Admin.Component.ComponentRow", null, require("jira-project-config/component/view/row"));
