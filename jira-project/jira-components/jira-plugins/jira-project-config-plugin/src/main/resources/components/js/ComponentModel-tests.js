AJS.test.require("com.atlassian.jira.jira-project-config-plugin:project-config-components", function () {
    module("Component Model");
    test("getInvalidAssigneeMsg", function () {

        var ComponentModel = require("jira-project-config/component/model");
        var componentModel = new ComponentModel();

        var formatter = require("jira/util/formatter");
        this.spy(formatter, "format");

        ok(!componentModel.getAssigneeInvalidMsg(), "Expected Undefined to be returned if the assignee is valid");

        componentModel.set({
            assigneeType: "UNASSIGNED"
        });

        componentModel.getAssigneeInvalidMsg();
        sinon.assert.calledWithExactly(formatter.format, "admin.project.assigneeType.unassigned.invalid", "");
        formatter.format.reset();

        componentModel.set({
            assigneeType: "COMPONENT_LEAD",
            assignee: {
                active: true,
                name: "scott",
                displayName: "Scott"
            }
        });

        componentModel.getAssigneeInvalidMsg();
        sinon.assert.calledWithExactly(formatter.format, "admin.project.assigneeType.component.lead.not.assignable", "Scott");
        formatter.format.reset();

        componentModel.set({
            assigneeType: "COMPONENT_LEAD",
            assignee: {
                name: "scott",
                displayName: "Scott",
                active: false
            }
        });

        componentModel.getAssigneeInvalidMsg();
        sinon.assert.calledWithExactly(formatter.format, "admin.project.assigneeType.component.lead.not.active", "Scott");
        formatter.format.reset();

        componentModel.set({
            assigneeType: "PROJECT_LEAD",
            assignee: {
                active: true,
                name: "scott",
                displayName: "Scott"
            }
        });

        componentModel.getAssigneeInvalidMsg();
        sinon.assert.calledWithExactly(formatter.format, "admin.project.assigneeType.project.lead.not.assignable", "Scott");
        formatter.format.reset();

        componentModel.set({
            assigneeType: "PROJECT_LEAD",
            assignee: {
                name: "scott",
                displayName: "Scott",
                active: false
            }
        });

        componentModel.getAssigneeInvalidMsg();
        sinon.assert.calledWithExactly(formatter.format, "admin.project.assigneeType.project.lead.not.active", "Scott");
        formatter.format.reset();

        componentModel.set({
            assigneeType: "PROJECT_DEFAULT",
            assignee: {
                active: true,
                name: "scott",
                displayName: "Scott"
            }
        });

        componentModel.getAssigneeInvalidMsg();
        sinon.assert.calledWithExactly(formatter.format, "admin.project.assigneeType.project.default.not.assignable", "Scott");
        formatter.format.reset();
    });
});
