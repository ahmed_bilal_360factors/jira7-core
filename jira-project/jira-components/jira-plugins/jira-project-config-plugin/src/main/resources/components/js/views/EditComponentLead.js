define("jira-project-config/component/view/edit-lead", [
    'jira-project-config/component/templates',
    'aui/restful-table',
    "underscore"
], function(ComponentTemplates, RestfulTable, _) {
    "use strict";

    var CustomEditView = RestfulTable.CustomEditView;
    var TEMPLATES = ComponentTemplates;

    return CustomEditView.extend({
        render: function () {
            var all = _.clone(this.model.attributes);
            all.isLeadPickerDisabled = this.model.isLeadPickerDisabled();
            all.assigneeInvalidMsg = this.model.getAssigneeInvalidMsg();
            return TEMPLATES.editComponentLead(all);
        }
    });
});

AJS.namespace("JIRA.Admin.Component.RealAssigneeEditView", null, require("jira-project-config/component/view/edit-lead"));
