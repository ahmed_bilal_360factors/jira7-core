define(
    "jira-project-config/component/view/edit-name",
    ['jira-project-config/component/templates', 'aui/restful-table'],
    function(ComponentTemplates, RestfulTable) {
        "use strict";

        var CustomEditView = RestfulTable.CustomEditView;
        var TEMPLATES = ComponentTemplates;

        return CustomEditView.extend({
            render: function (args) {
                args.project = this.model.getProject();
                args.update = false; // HACK
                return TEMPLATES.editComponentName(args);
            }
        });

    }
);

AJS.namespace("JIRA.Admin.Component.NameEditView", null, require("jira-project-config/component/view/edit-name"));
