define("jira-project-config/component/model", [
    'aui/restful-table',
    'jira/util/formatter',
    "aui/params",
    "jira/ajs/ajax/smart-ajax",
    "jquery"
], function(RestfulTable, formatter, params, SmartAjax, $) {
    "use strict";

    var EntryModel = RestfulTable.EntryModel;
    /**
     * Model for a Project component
     *
     * @class ComponentModel
     * @namespace JIRA.Admin.Component
     */
    return EntryModel.extend({

        // Rest resources
        RELATED_ISSUES_PATH: "/relatedIssueCounts",

        /**
         * Some additional mapping of leadUserName
         *
         * @param {Object} attributes
         * @return {Object}
         */
        changedAttributes: function (attributes) {
            var currentLead = this.get("lead");
            var leadUserName = attributes.leadUserName;
            var changed = EntryModel.prototype.changedAttributes.call(this, attributes);

            if (currentLead && leadUserName === "") {
                changed = changed || {};
                changed.leadUserName = leadUserName;
            }

            return changed;
        },

        /**
         * Is the user picker disabled
         *
         * @return {Boolean}
         */
        isLeadPickerDisabled: function () {
            return params.leadPickerDisabled;
        },

        /**
         * What is the project lead assignee
         *
         * @return {String}
         */
        getProjectLeadAssignee: function () {
            return params.projectLeadAssignee;
        },

        /**
         * Gets project key
         *
         * @return {String}
         */
        getProject: function () {
            return $("meta[name=projectKey]").attr("content");
        },

        /**
         * Is the default assignee the project lead
         *
         * @return {Boolean}
         */
        isDefaultAssigneeProjectLead: function () {
            return params.isDefaultAssigneeProjectLead;
        },

        /**
         * If the assignee type is invalid returns a message indicating why.
         *
         * @return {string|undefined}
         */
        getAssigneeInvalidMsg: function () {

            if (this.get("isAssigneeTypeValid")) {
                // No error message is returned if assignee is valid.
                return undefined;
            }

            var assignee = this.get("assignee") || {};
            var assigneeDisplay = assignee.displayName || assignee.name || "";

            switch (this.get("assigneeType")) {

                case "UNASSIGNED":
                    return formatter.I18n.getText("admin.project.assigneeType.unassigned.invalid", assigneeDisplay);

                case "PROJECT_DEFAULT":
                    return formatter.I18n.getText(
                        "admin.project.assigneeType.project.default.not.assignable",
                        assigneeDisplay
                    );

                case "PROJECT_LEAD":
                    return (assignee.active) ?
                        formatter.I18n.getText("admin.project.assigneeType.project.lead.not.assignable", assigneeDisplay) :
                        formatter.I18n.getText("admin.project.assigneeType.project.lead.not.active", assigneeDisplay);

                case "COMPONENT_LEAD":
                    return (assignee.active) ?
                        formatter.I18n.getText(
                            "admin.project.assigneeType.component.lead.not.assignable",
                            assigneeDisplay
                        ) :
                        formatter.I18n.getText("admin.project.assigneeType.component.lead.not.active", assigneeDisplay);
            }
        },


        /**
         * Gets count for issues with either affects version or fix version containing this version
         *
         * @param options
         * ... {function} success - Server success callback
         * ... {function} error - Server error callback
         * @return JIRA.Admin.Component.ComponentModel
         */
        getRelatedIssueCount: function (options) {

            var instance = this;

            options = options || {};

            SmartAjax.makeRequest({
                url: this.url() + this.RELATED_ISSUES_PATH,
                complete: function (xhr, status, smartAjaxResponse) {
                    if (smartAjaxResponse.successful) {
                        options.success.call(instance, smartAjaxResponse.data.issueCount);
                    } else {
                        instance._serverErrorHandler(smartAjaxResponse);
                    }
                }
            });

            return this;
        },

        /**
         * Gets JSON representation of available components to migrate issues of this component into.
         *
         * @return {Array}
         */
        getSwapComponentsJSON: function () {
            var instance = this;
            var swapComponents = [];

            this.collection.each(function (model) {
                if (model !== instance) {
                    swapComponents.push(model.toJSON());
                }
            });

            return swapComponents;
        }
    });
});

AJS.namespace("JIRA.Admin.Component.ComponentModel", null, require("jira-project-config/component/model"));
