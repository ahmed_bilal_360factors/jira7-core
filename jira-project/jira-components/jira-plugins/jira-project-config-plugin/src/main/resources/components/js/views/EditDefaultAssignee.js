define("jira-project-config/component/view/edit-default-assignee", [
    'jira-project-config/component/templates',
    'aui/restful-table',
    "underscore"
], function(ComponentTemplates, RestfulTable, _) {
    "use strict";

    var CustomEditView = RestfulTable.CustomEditView;
    var TEMPLATES = ComponentTemplates;

    return CustomEditView.extend({
        render: function () {
            var all = _.clone(this.model.attributes);
            all.isDefaultAssigneeProjectLead = this.model.isDefaultAssigneeProjectLead();
            all.projectLeadAssignee = this.model.getProjectLeadAssignee();
            return TEMPLATES.editDefaultAssignee(all);
        }
    });
});

AJS.namespace("JIRA.Admin.Component.RealAssigneeTypeEditView", null, require("jira-project-config/component/view/edit-default-assignee"));
