define("jira-project-config/component/view/default-assignee",
    ['jira-project-config/component/templates', "aui/restful-table"],
    function(ComponentTemplates, RestfulTable) {
        "use strict";

        var CustomReadView = RestfulTable.CustomReadView;
        var TEMPLATES = ComponentTemplates;


        return CustomReadView.extend({
            render: function () {
                return TEMPLATES.defaultAssignee({
                    component: this.model.attributes
                });
            }
        });
    }
);

AJS.namespace("JIRA.Admin.Component.RealAssigneeTypeReadView", null, require("jira-project-config/component/view/default-assignee"));
