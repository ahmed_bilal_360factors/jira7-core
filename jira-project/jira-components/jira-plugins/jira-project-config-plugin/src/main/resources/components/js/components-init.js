require([
    'aui/restful-table',
    'jira/util/formatter',
    "jira-project-config/component/view/icon",
    "jira-project-config/component/view/edit-name",
    "jira-project-config/component/view/lead",
    "jira-project-config/component/view/edit-lead",
    "jira-project-config/component/view/default-assignee",
    "jira-project-config/component/view/edit-default-assignee",
    "jira-project-config/component/view/row",
    "jira-project-config/component/view/edit-row",
    "jira-project-config/component/model",
    "jira-project-config/utils",
    "jira/util/events",
    "jira/util/events/types",
    "jira/util/events/reasons",
    "wrm/context-path",
    "jquery"
], function(
    RestfulTable,
    formatter,
    IconView,
    NameEditView,
    RealAssigneeReadView,
    RealAssigneeEditView,
    RealAssigneeTypeReadView,
    RealAssigneeTypeEditView,
    ComponentRow,
    EditComponentRow,
    ComponentModel,
    Utils,
    Events,
    Types,
    Reasons,
    contextPath,
    $
) {

    $(function () {
        var $componentsTable = $("#project-config-components-table");

        if (!$componentsTable.length) {
            return;
        }

        new RestfulTable({
            el: $componentsTable,
            autoFocus: true,
            resources: {
                all: Utils.getRestBaseUrl() + "/project/" + Utils.getProjectKey() + "/components",
                self: contextPath() + "/rest/api/" + Utils.getRestVersion() + "/component"
            },
            columns: [
                {
                    id: "icon",
                    header: "",
                    styleClass: "project-config-component-icon",
                    allowEdit: false,
                    readView: IconView,
                    editView: IconView
                },
                {
                    id: "name",
                    header: formatter.I18n.getText("common.words.name"),
                    styleClass: "project-config-component-name",
                    editView: NameEditView
                },
                {
                    id: "description",
                    header: formatter.I18n.getText("common.words.description"),
                    emptyText: formatter.I18n.getText('admin.project.add.description'),
                    styleClass: "project-config-component-description"
                },
                {
                    id: "realAssignee",
                    styleClass: "project-config-component-lead",
                    header: formatter.I18n.getText("admin.projects.component.lead"),
                    emptyText: formatter.I18n.getText("admin.project.components.lead"),
                    fieldName: "leadUserName-field",
                    readView: RealAssigneeReadView,
                    editView: RealAssigneeEditView
                },
                {
                    id: 'realAssigneeType',
                    header: formatter.I18n.getText("admin.projects.default.assignee"),
                    emptyText: formatter.I18n.getText("admin.project.components.lead"),
                    fieldName: "assigneeType",
                    styleClass: "project-config-component-assignee",
                    readView: RealAssigneeTypeReadView,
                    editView: RealAssigneeTypeEditView
                }
            ],
            allowEdit: true,
            noEntriesMsg: formatter.I18n.getText("admin.project.components.none"),
            model: ComponentModel,
            views: {
                row: ComponentRow,
                editRow: EditComponentRow
            }
        });

        $componentsTable.one(RestfulTable.Events.INITIALIZED, function () {
            Events.trigger(Types.NEW_CONTENT_ADDED, [$componentsTable, Reasons.componentsTableReady]);
        });

        $componentsTable.closest("form").addClass("ajs-dirty-warning-exempt"); // Stop dirty form warnings from firing on table
    });
});
