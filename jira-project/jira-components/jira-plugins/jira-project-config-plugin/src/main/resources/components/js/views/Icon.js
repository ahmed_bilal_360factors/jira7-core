define(
    "jira-project-config/component/view/icon",
    ['jira-project-config/component/templates', 'aui/restful-table'],
    function(ComponentTemplates, RestfulTable) {
        "use strict";

        var CustomReadView = RestfulTable.CustomReadView;
        var TEMPLATES = ComponentTemplates;

        return CustomReadView.extend({
            render: function () {
                return TEMPLATES.icon();
            }
        });
    }
);

AJS.namespace("JIRA.Admin.Component.IconView", null, require("jira-project-config/component/view/icon"));
