define("jira-project-config/component/view/lead", [
    'jira-project-config/component/templates',
    'aui/restful-table',
    "underscore"
], function(ComponentTemplates, RestfulTable, _) {
    "use strict";

    var CustomReadView = RestfulTable.CustomReadView;
    var TEMPLATES = ComponentTemplates;

    return CustomReadView.extend({
        render: function () {
            var all = _.clone(this.model.attributes);
            if (all.lead || all.leadUserName) {
                all.assigneeInvalidMsg = this.model.getAssigneeInvalidMsg();
                return TEMPLATES.componentLead({
                    component: all
                });
            }
        }
    });
});

AJS.namespace("JIRA.Admin.Component.RealAssigneeReadView", null, require("jira-project-config/component/view/lead"));
