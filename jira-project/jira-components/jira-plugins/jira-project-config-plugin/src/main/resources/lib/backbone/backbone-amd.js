define("jira-project-config/backbone", ["require"], function (require) {
    "use strict";

    var backboneFactory = require('atlassian/libs/factories/backbone-1.0.0');
    var _ = require('underscore');
    var $ = require('jquery');

    return backboneFactory(_, $);
});