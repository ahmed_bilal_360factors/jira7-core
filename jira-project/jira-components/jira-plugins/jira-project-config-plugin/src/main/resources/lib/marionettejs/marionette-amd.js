define("jira-project-config/marionette", ['require'], function (require) {
    "use strict";

    var marionetteFactory = require('atlassian/libs/factories/marionette-1.6.1');
    var Backbone = require('jira-project-config/backbone');
    var _ = require('underscore');

    return marionetteFactory(_, Backbone);
});