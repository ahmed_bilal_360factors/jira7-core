(function(){
    "use strict";

    var chance = window.Chance;
    delete window.Chance;

    define("jira-project-config/libs/chance", ['require'], function (require) {
        return chance;
    });
}());
