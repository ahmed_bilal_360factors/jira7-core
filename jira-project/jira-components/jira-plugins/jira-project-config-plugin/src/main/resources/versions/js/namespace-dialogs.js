AJS.namespace("JIRA.Admin.Version.DeleteForm", null, require("jira-project-config/versions/delete-form"));
AJS.namespace("JIRA.Admin.Version.MergeForm", null, require("jira-project-config/versions/merge-form"));
AJS.namespace("JIRA.Admin.Version.ReleaseForm", null, require("jira-project-config/versions/release-form"));
AJS.namespace("JIRA.Templates.Versions", null, require("jira-project-config/versions/templates"));
