define("jira-project-config/versions/start-date-read-view", ["require"], function(require){
    "use strict";

    var TemplateVersions = require('jira-project-config/versions/templates');
    var RestfulTable = require('aui/restful-table');

    return RestfulTable.CustomReadView.extend({
       render: function (args) {
           args.userStartDate = this.model.get("userStartDate");
           return TemplateVersions.readStartDate(args);
       }
   });
});
