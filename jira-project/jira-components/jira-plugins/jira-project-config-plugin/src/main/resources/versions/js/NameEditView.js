define("jira-project-config/versions/name-edit-view", ["require"], function(require){
    "use strict";

    var jQuery = require('jquery');
    var TemplateVersions = require('jira-project-config/versions/templates');
    var RestfulTable = require('aui/restful-table');

    return RestfulTable.CustomEditView.extend({
        render: function (args) {
            args.project = jQuery("meta[name=projectKey]").attr("content");
            args.update = false; // HACK
            return TemplateVersions.editName(args);
        }
    });
});
