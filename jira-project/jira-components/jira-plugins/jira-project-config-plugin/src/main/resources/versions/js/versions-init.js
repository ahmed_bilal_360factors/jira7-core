require([
    'aui/restful-table',
    'jira-project-config/utils',
    'jira-project-config/utils/paged-scrollable',
    'jira-project-config/utils/batched-restful-table',
    'jira-project-config/versions/model',
    'jira-project-config/versions/row',
    'jira-project-config/versions/status-view',
    'jira-project-config/versions/start-date-read-view',
    'jira-project-config/versions/start-date-edit-view',
    'jira-project-config/versions/release-date-read-view',
    'jira-project-config/versions/release-date-edit-view',
    'jira-project-config/versions/name-edit-view',
    'jira-project-config/versions/merge-form',
    'jira-project-config/versions/edit-version-row',
    'jira/ajs/select/multi-select',
    'jira/dialog/form-dialog',
    'jquery'
], function(
    RestfulTable,
    ProjectConfigUtils,
    PageScroller,
    BatchedRestfulTable,
    VersionModel,
    VersionRow,
    StatusView,
    StartDateReadView,
    StartDateEditView,
    ReleaseDateReadView,
    ReleaseDateEditView,
    NameEditView,
    MergeForm,
    EditVersionRow,
    MultiSelect,
    FormDialog,
    jQuery) {

    jQuery(function () {

        var versionsTableEl = jQuery("#project-config-versions-table");
        var versionTable = new BatchedRestfulTable({
            autoFocus: true,
            el: versionsTableEl,
            model: VersionModel,
            reverseOrder: true, // this is needed for correct reordering, but our infinite table doesn't reverse anything
            noEntriesMsg: AJS.I18n.getText("admin.project.versions.none"),
            loadingMsg: AJS.I18n.getText('admin.project.versions.loading'),
            allowReorder: true,
            resources: {
                base: ProjectConfigUtils.getRestBaseUrl() + "/project/" + ProjectConfigUtils.getKey() + "/version?expand=operations&orderBy=-sequence",
                pageSize: 50,
                self: ProjectConfigUtils.getRestBaseUrl() + "/version",
                valuesKey: "values"
            },
            // if you edit columns remember to update versions.soy template .versionRow
            columns: [
                {
                    id: "status",
                    styleClass: "project-config-release-status",
                    header: "",
                    allowEdit: false,
                    readView: StatusView,
                    editView: StatusView
                },
                {
                    id: "name",
                    styleClass: "project-config-version-name",
                    header: AJS.I18n.getText("common.words.name"),
                    editView: NameEditView
                },
                {
                    id: "description",
                    styleClass: "project-config-version-description",
                    header: AJS.I18n.getText("common.words.description")
                },
                {
                    id: "startDate",
                    styleClass: "project-config-version-start-date",
                    header: AJS.I18n.getText("version.startdate"),
                    fieldName: "userStartDate",
                    readView: StartDateReadView,
                    editView: StartDateEditView
                },
                {
                    id: "releaseDate",
                    styleClass: "project-config-version-release-date",
                    header: AJS.I18n.getText("version.releasedate"),
                    fieldName: "userReleaseDate",
                    readView: ReleaseDateReadView,
                    editView: ReleaseDateEditView
                }
            ],
            views: {
                editRow: EditVersionRow,
                row: VersionRow
            }
        });

        versionsTableEl.closest("form").addClass("ajs-dirty-warning-exempt"); // Stop dirty form warnings from firing on table

        var $mergeLink = jQuery('#project-config-operations-merge').hide();

        var dialogForm = new MergeForm({
            collection: versionTable.getModels()
        });

        var dialog = new FormDialog({
            id: "versionsMergeDialog",
            trigger: $mergeLink,
            content: function (callback) {
                dialogForm.render(function (el) {
                    callback(el);
                });
            },
            submitHandler: function (e) {
                dialogForm.submit(this.$form.serializeObject(), function () {
                    dialog.hide();
                });
                e.preventDefault();
            }
        });

        dialog.onContentReady(function () {
            var el = jQuery("#versionsMergeDialog select[multiple]");
            new MultiSelect({
                element: el,
                itemAttrDisplayed: "label"
            });
        });

        var scroller;

        function initScroller() {
            if (!scroller) {
                scroller = new PageScroller(window, {
                    bufferPixels: 800,
                    functionCall: function () {
                        return versionTable.fetchMore();
                    }
                });
            }
        }

        jQuery(versionTable).bind(RestfulTable.Events.ROW_ADDED, function () {
            refreshMergeLink(this);
        }).bind(RestfulTable.Events.INITIALIZED, function () {
            refreshMergeLink(this);
            initScroller();
        }).bind(RestfulTable.Events.ROW_REMOVED, function () {
            refreshMergeLink(this);
        }).bind(RestfulTable.Events.MORE_FETCHED, function () {
            if (this.getIsLastPage()) {
                scroller.suspend();
            }
        });

        function refreshMergeLink(table) {
            if (table.getModels().length > 1) {
                $mergeLink.show();
            } else {
                $mergeLink.hide();
            }
        }
    });
});
