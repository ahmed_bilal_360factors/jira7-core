define("jira-project-config/versions/merge-form", ["require"], function(require){
    "use strict";

    var _ = require('underscore');
    var Backbone = require('jira-project-config/backbone');
    var TemplateVersions = require('jira-project-config/versions/templates');

    /**
     * Renders and handles merge form used in dialog
     */
    return Backbone.View.extend({

        /**
         * Merges a version on the server.
         *
         * @param {Object} values
         * @param complete
         * @return {JIRA.Admin.Version.MergeForm}
         */
        submit: function (values, complete) {
            if (this.$("#idsToMerge + .error").length > 0) {
                // Don't submit the form while there's an unresolved error.
                this.$("#idsToMerge-textarea").focus();
                this.$(".button:disabled").attr("disabled", "disabled");
                return this;
            }

            this.$(".throbber").addClass("loading");

            var instance = this;
            var idsToMerge = values.idsToMerge;
            var mergeToVersion = values.idMergeTo;

            if (idsToMerge.length > 1) {
                idsToMerge = _.without(idsToMerge, mergeToVersion);
            }

            if (!idsToMerge || idsToMerge.length === 0) {
                complete();
            }

            var errorMessages = [];

            var versionCount = idsToMerge.length;
            var noValidationErrors = true;

            var mergedCount = 0;

            var triggerMerge = function () {
                var versionToMerge = instance.collection.get(idsToMerge[mergedCount]);

                versionToMerge.merge({
                    data: {
                        mergeTo: mergeToVersion
                    },
                    complete: function (status, responseData) {

                        if (status === 400 && responseData && responseData.errorMessages) {
                            noValidationErrors = false;
                        }

                        if (responseData && responseData.errorMessages && responseData.errorMessages.length > 0) {
                            errorMessages = errorMessages.concat(responseData.errorMessages);
                        }

                        mergedCount++;

                        if (mergedCount === versionCount) {

                            if (noValidationErrors) {
                                complete();
                            } else {
                                instance.$(".throbber").removeClass("loading");
                                instance.render(instance.ready, {
                                    errorMessages: errorMessages
                                });
                            }
                        } else {
                            triggerMerge();
                        }
                    }
                });
            };

            triggerMerge();

            return this;
        },

        /**
         *
         * Renders the release form. This differs from standard render methods, as it requires async request/s to the server.
         * As a result when this method is called the first argument is a function that is called when the content has been
         * rendered.
         *
         * @param {function} ready - callback to declare content is ready
         * @return {JIRA.Admin.Version.DeleteForm}
         */
        render: function (ready, options) {

            this.ready = ready;

            var instance = this;

            options = options || {};

            instance.el.innerHTML = TemplateVersions.mergeForm({
                versions: instance.collection.toJSON(),
                errorMessages: options.errorMessages || []
            });

            ready.call(instance, instance.el);

            return this;
        }

    });
});
