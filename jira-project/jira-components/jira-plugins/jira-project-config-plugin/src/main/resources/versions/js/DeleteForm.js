define("jira-project-config/versions/delete-form", ["require"], function(require){
    "use strict";

    var jQuery = require('jquery');
    var _ = require('underscore');
    var TemplateVersions = require('jira-project-config/versions/templates');
    var Backbone = require('jira-project-config/backbone');

    /**
     * Renders and handles submission of delete form used in dialog
     */
    return Backbone.View.extend({

        /**
         * Destorys model on server
         *
         * @param {Object} values
         * @param complete
         * @return {JIRA.Admin.Version.DeleteForm}
         */
        submit: function (values, row, complete) {

            this.$(".throbber").addClass("loading");


            if (values.fix !== "swap") {
                delete values.moveFixIssuesTo;
            }

            if (values.affects !== "swap") {
                delete values.moveAffectedIssuesTo;
            }

            this.model.destroy({
                data: values,
                success: function () {
                    complete();
                },
                error: function () {
                    complete();
                }
            });

            return this;
        },

        /**
         *
         * Renders delete form. This differs from standard render methods, as it requires async request/s to the server.
         * As a result when this method is called the first argument is a function that is called when the content has been
         * rendered.
         *
         * @param {function} ready - callback to declare content is ready
         * @return {JIRA.Admin.Version.DeleteForm}
         */
        render: function (ready) {

            var instance = this;

            this.model.getRelatedIssueCount({

                success: function (relatedIssueCount) {

                    var areRelatedIssues = true;
                    if (relatedIssueCount.issuesFixedCount === 0 && relatedIssueCount.issuesAffectedCount === 0 && relatedIssueCount.issueCountWithCustomFieldsShowingVersion === 0) {
                        areRelatedIssues = false;
                    }

                    var customFieldUsage = relatedIssueCount.customFieldUsage || [];
                    var versionId = instance.model.toJSON().id;
                    var projectId = jQuery("meta[name=projectId]").attr("content");

                    _.each(customFieldUsage, function (cfUsage) {
                        cfUsage.issueLink = encodeURI("project = " + projectId + " " +
                            "AND '" + cfUsage.fieldName + "' = " + versionId);
                    });

                    instance.el.innerHTML = TemplateVersions.deleteForm({
                        version: instance.model.toJSON(),
                        relatedIssueCount: relatedIssueCount,
                        areRelatedIssues: areRelatedIssues,
                        versions: instance.model.getSwapVersionsJSON(),
                        projectId: projectId
                    });

                    ready.call(instance, instance.el);

                    // if we focus select, check the radio associated with it also
                    jQuery("#moveAffectedIssuesTo").focus(function () {
                        jQuery("#affects-swap").attr("checked", "checked");
                    });

                    jQuery("#moveFixIssuesTo").focus(function () {
                        jQuery("#fix-swap").attr("checked", "checked");
                    });

                    _.each(customFieldUsage, function (cfUsage) {
                        var customFieldId = cfUsage.customFieldId;
                        jQuery("#move-custom-" + customFieldId + "-to").focus(function () {
                            jQuery("#custom-swap-" + customFieldId).attr("checked", "checked");
                        });
                    });

                }
            });

            return this;
        }
    });
});
