define("jira-project-config/versions/edit-version-row", ["require"], function(require){
    "use strict";

    var RestfulTable = require('aui/restful-table');

    /**
     * Renders and assigns controls to table row responsible for creating versions
     *
     */
    return RestfulTable.EditRow.extend({

        initialize: function () {
            RestfulTable.EditRow.prototype.initialize.apply(this, arguments);

            this.bind(this._event.RENDER, function () {

                this.el.className = "project-config-versions-add-fields";

                Calendar.setup({
                    singleClick: true,
                    align: "Bl",
                    firstDay: AJS.params.firstDay,
                    button: this.$el.find("#project-config-versions-start-date-trigger")[0],
                    inputField: this.$el.find("#project-config-version-start-date-field")[0],
                    currentMillis: AJS.params.currentMillis,
                    useISO8601WeekNumbers: AJS.params.useISO8601,
                    ifFormat: AJS.params.dateFormat
                });

                Calendar.setup({
                    singleClick: true,
                    align: "Bl",
                    firstDay: AJS.params.firstDay,
                    button: this.$el.find("#project-config-versions-release-date-trigger")[0],
                    inputField: this.$el.find("#project-config-version-release-date-field")[0],
                    currentMillis: AJS.params.currentMillis,
                    useISO8601WeekNumbers: AJS.params.useISO8601,
                    ifFormat: AJS.params.dateFormat
                });
            });
        },

        /**
         * Renders errors with special handling for userReleaseDate as the property name that comes back from the servers
         * error collection does not match that of the input.
         *
         * @param errors
         */
        renderErrors: function (errors) {

            RestfulTable.EditRow.prototype.renderErrors.apply(this, arguments); // call super

            if (errors.startDate) {
                this.$(".project-config-version-start-date").append(this.renderError("userStartDate", errors.startDate));
            }
            if (errors.releaseDate) {
                this.$(".project-config-version-release-date").append(this.renderError("userReleaseDate", errors.releaseDate));
            }

            return this;
        }

    });
});
