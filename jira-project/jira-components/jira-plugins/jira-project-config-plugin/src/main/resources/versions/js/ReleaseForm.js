define("jira-project-config/versions/release-form", ["require"], function(require){
    "use strict";

    var jQuery = require('jquery');
    var Backbone = require('jira-project-config/backbone');
    var TemplateVersions = require('jira-project-config/versions/templates');

    /**
     * Renders release form used in dialog
     */
    return Backbone.View.extend({
        /**
         * Releases a version on the server.
         *
         * @param {Object} values
         * @param complete
         * @return {JIRA.Admin.Version.ReleaseForm}
         */
        submit: function (values, row, complete) {

            this.$(".throbber").addClass("loading");

            var instance = this;

            var updatedValues = {userReleaseDate: values.userReleaseDate, released: true, expand: "operations"};

            if (values.unresolved === "move") {
                updatedValues.moveUnfixedIssuesTo = values.moveUnfixedIssuesTo;
            }

            this.model.save(updatedValues, {
                success: function () {
                    row.render();
                    complete();
                },
                error: function (model, data, xhr) {
                    if (xhr.status === 400) {
                        var options = {
                            errors: data.errors,
                            values: values
                        };
                        instance.render(instance.ready, options);
                    } else {
                        complete();
                    }
                    instance.$(".throbber").removeClass("loading");
                }

            });
            return this;
        },

        /**
         *
         * Renders the release form. This differs from standard render methods, as it requires async request/s to the server.
         * As a result when this method is called the first argument is a function that is called when the content has been
         * rendered.
         *
         * @param {function} ready - callback to declare content is ready
         * @return {JIRA.Admin.Version.DeleteForm}
         */
        render: function (ready, options) {

            this.ready = ready;

            var instance = this;

            options = options || {};

            this.model.getUnresolvedIssueCount({

                success: function (unresolvedIssueCount) {

                    if (unresolvedIssueCount.issuesUnresolvedCount === 0 && unresolvedIssueCount.issuesUnresolvedCount === 0) {
                        unresolvedIssueCount = false;
                    }

                    instance.el.innerHTML = TemplateVersions.releaseForm({
                        version: instance.model.toJSON(),
                        unresolvedIssueCount: unresolvedIssueCount,
                        versions: instance.model.getMoveVersionsJSON(),
                        projectId: jQuery("meta[name=projectId]").attr("content"),
                        errors: options.errors || {},
                        errorMessages: options.errorMessages || []
                    });

                    ready.call(instance, instance.el);

                    var inputField = instance.$("#project-config-version-release-form-release-date-field")[0];
                    Calendar.setup({
                        singleClick: true,
                        align: "Bl",
                        firstDay: AJS.params.firstDay,
                        button: instance.$("#project-config-versions-release-form-release-date-trigger")[0],
                        inputField: inputField,
                        currentMillis: AJS.params.currentMillis,
                        useISO8601WeekNumbers: AJS.params.useISO8601,
                        ifFormat: AJS.params.dateFormat
                    });
                    if (instance.model.get("releaseDate") && Date.prototype.print) {
                        inputField.value = new Date(instance.model.get("releaseDate")).print(AJS.params.dateFormat);
                    }

                    // if we focus select, check the radio associated with it also
                    jQuery("#moveUnfixedIssuesTo").focus(function () {
                        jQuery("#unresolved-move").attr("checked", "checked");
                    });
                }
            });

            return this;
        }

    });
});
