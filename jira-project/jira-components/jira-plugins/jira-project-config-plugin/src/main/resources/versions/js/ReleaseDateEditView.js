define("jira-project-config/versions/release-date-edit-view", ["require"], function (require) {
    "use strict";

    var jQuery = require('jquery');
    var TemplateVersions = require('jira-project-config/versions/templates');
    var RestfulTable = require('aui/restful-table');

    return RestfulTable.CustomEditView.extend({
        render: function (args) {
            // Format the date so it works with the format expected by the server.
            var userDate = jQuery.trim(this.model.get("releaseDate")).replace(/-/g, '/');
            if (userDate && Date.prototype.print) {
                userDate = new Date(userDate);
                args.userReleaseDate = userDate.print(AJS.params.dateFormat);
            }

            return TemplateVersions.editReleaseDate(args);
        }
    });
});
