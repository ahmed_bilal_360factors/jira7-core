define("jira-project-config/versions/status-view", ["require"], function(require){
    "use strict";

    var TemplateVersions = require('jira-project-config/versions/templates');
    var RestfulTable = require('aui/restful-table');

    return RestfulTable.CustomReadView.extend({
        render: function (args) {
            args.released = this.model.get("released");
            args.archived = this.model.get("archived");
            return TemplateVersions.releaseStatus(args);
        }
    });
});
