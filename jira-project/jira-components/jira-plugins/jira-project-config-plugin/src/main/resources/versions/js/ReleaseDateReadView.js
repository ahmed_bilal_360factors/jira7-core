define("jira-project-config/versions/release-date-read-view", ["require"], function(require){
    "use strict";

    var TemplateVersions = require('jira-project-config/versions/templates');
    var RestfulTable = require('aui/restful-table');

    return RestfulTable.CustomReadView.extend({
        render: function (args) {
            args.userReleaseDate = this.model.get("userReleaseDate");
            return TemplateVersions.readReleaseDate(args);
        }
    });
});
