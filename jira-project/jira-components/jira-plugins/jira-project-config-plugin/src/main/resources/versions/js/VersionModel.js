define("jira-project-config/versions/model", ["require"], function(require){
    "use strict";

    var SmartAjax = require('jira/ajs/ajax/smart-ajax');
    var RestfulTable = require('aui/restful-table');

    var convertFormToReplacementData = function (formData) {
        var replacementData = {};
        if (formData.affects === "swap") {
            replacementData.moveAffectedIssuesTo = formData.moveAffectedIssuesTo;
        }
        if (formData.fix === "swap") {
            replacementData.moveFixIssuesTo = formData.moveFixIssuesTo;
        }
        for (var dataName in formData) {
            var customFieldMatch = dataName.match(/^customField([0-9]+)Action$/);
            if (customFieldMatch) {
                replacementData.customFieldReplacementList = replacementData.customFieldReplacementList || [];
                var cfAction = formData[dataName];
                var cfId = customFieldMatch[1];
                var customFieldReplaceInfo = {
                    customFieldId: Number(cfId),
                    moveTo: cfAction === 'swap' ? (formData['customField' + cfId + 'MoveTo']) : null
                };

                replacementData.customFieldReplacementList.push(customFieldReplaceInfo);
            }
        }
        return replacementData;
    };
    return RestfulTable.EntryModel.extend({

        // rest resources
        RELATED_ISSUES_PATH: "/relatedIssueCounts",
        UNRESOLVED_ISSUES_PATH: "/unresolvedIssueCount",
        MERGE_PATH: "/mergeto/",
        WITH_CUSTOM_FIELDS_PATH: "/removeAndSwap",


        addExpand: function (changed) {
            changed.expand = "operations";
        },

        /**
         * Destroys the model on the server. We need to override the default method as it does not support sending of
         * query paramaters.
         *
         * @override
         * @param options
         * ... {function} success - Server success callback
         * ... {function} error - Server error callback
         * ... {object} data
         * ... ... {String} moveFixIssuesTo - The version to set fixVersion to on issues where the deleted version is the
         * fix version, If null then the fixVersion is removed.
         * ... ... {String}  moveAffectedIssuesTo The version to set affectedVersion to on issues where the deleted version
         * is the affected version, If null then the affectedVersion is removed.
         *
         * @return JIRA.VersionModel
         */
        destroy: function (options) {
            var instance = this;
            var url = this.url() + this.WITH_CUSTOM_FIELDS_PATH;
            var replacementData = convertFormToReplacementData(options.data);

            SmartAjax.makeRequest({
                url: url,
                type: "POST",
                dataType: "json",
                data: JSON.stringify(replacementData),
                contentType: "application/json",

                complete: function (xhr, status, smartAjaxResponse) {

                    var smartAjaxResponseData = smartAjaxResponse.data;

                    if (typeof smartAjaxResponse.data === "string") {
                        try {
                            smartAjaxResponseData = JSON.parse(smartAjaxResponse.data);
                        } catch (e) {
                            smartAjaxResponseData = {
                                successful: false,
                                errorMessages: [String.valueOf(e)]
                            };
                            smartAjaxResponse.data = JSON.stringify(smartAjaxResponseData);
                        }
                    }

                    var isValidationError = !(xhr.status === 400 && smartAjaxResponseData && smartAjaxResponseData.errors);

                    if (smartAjaxResponse.successful) {
                        instance.collection.remove(instance);
                        if (options.success) {
                            options.success.call(instance, smartAjaxResponseData);
                        }
                    } else if (isValidationError) {
                        instance._serverErrorHandler(smartAjaxResponse);
                        if (options.error) {
                            options.error.call(instance, smartAjaxResponseData);
                        }
                    }
                    if (options.complete) {
                        options.complete.call(instance, xhr.status, smartAjaxResponseData);
                    }
                }
            });

            return this;
        },

        /**
         * Merges versions on the server
         *
         * @override
         * @param options
         * ... {function} success - Server success callback
         * ... {function} error - Server error callback
         * ... {object} data
         * ... ... {String} mergeTo - Merge target, all issues that have assigned this version will have version
         *  mergeTo assigned after merge
         *
         * @return JIRA.VersionModel
         */
        merge: function (options) {
            var instance = this;
            var url = this.url();
            var data = options.data;

            if (typeof data.mergeTo !== "string") {
                options.error.call(this, {status: 400, errorMessages: ["parameters"]});
            }

            SmartAjax.makeRequest({
                url: url + this.MERGE_PATH + data.mergeTo,
                type: "PUT",
                dataType: "json",
                timeout: 120000,

                complete: function (xhr, status, smartAjaxResponse) {

                    var smartAjaxResponseData = smartAjaxResponse.data;

                    if (typeof smartAjaxResponse.data === "string") {
                        try {
                            smartAjaxResponseData = JSON.parse(smartAjaxResponse.data);
                        } catch (e) {
                            smartAjaxResponseData = {
                                successful: false,
                                errorMessages: [String.valueOf(e)]
                            };
                            smartAjaxResponse.data = JSON.stringify(smartAjaxResponseData);
                        }
                    }

                    var isValidationError = !(xhr.status === 400 && smartAjaxResponseData && smartAjaxResponseData.errors);

                    if (smartAjaxResponse.successful) {
                        instance.collection.remove(instance);
                        if (options.success) {
                            options.success.call(instance, smartAjaxResponseData);
                        }
                    } else if (isValidationError) {
                        instance._serverErrorHandler(smartAjaxResponse);
                        if (options.error) {
                            options.error.call(instance, smartAjaxResponseData);
                        }
                    }
                    if (options.complete) {
                        options.complete.call(instance, xhr.status, smartAjaxResponseData);
                    }
                }
            });

            return this;
        },

        /**
         * Gets count for issues with either affects version or fix version containing this version
         *
         * @param options
         * ... {function} success - Server success callback
         * ... {function} error - Server error callback
         * @return JIRA.VersionModel
         */
        getRelatedIssueCount: function (options) {

            var instance = this;

            options = options || {};

            SmartAjax.makeRequest({
                url: this.url() + this.RELATED_ISSUES_PATH,
                complete: function (xhr, status, smartAjaxResponse) {
                    if (smartAjaxResponse.successful) {
                        options.success.call(instance, smartAjaxResponse.data);
                    } else {
                        instance._serverErrorHandler(smartAjaxResponse);
                    }
                }
            });

            return this;
        },

        /**
         * Gets JSON representation of available versions to migrate issues of this version into.
         *
         * @return {Array}
         */
        getSwapVersionsJSON: function () {
            var instance = this;
            var availableSwapVersions = [];

            this.collection.sort().each(function (model) {
                if (!model.get("archived") && model !== instance) {
                    availableSwapVersions.push(model.toJSON());
                }
            });

            return availableSwapVersions.reverse();
        },

        /**
         * Gets JSON representation of available versions to migrate issues of this version into.
         *
         * @return {Array}
         */
        getMoveVersionsJSON: function () {
            var instance = this;
            var availableMoveVersions = [];

            this.collection.sort().each(function (model, i) {

                var json = model.toJSON();

                if (instance.collection.at(i + 1) === instance) {
                    json.nextScheduled = true;
                }

                if (!model.get("released") && model !== instance) {
                    availableMoveVersions.push(json);
                }
            });

            return availableMoveVersions;
        },


        /**
         * Gets count for unresolved issues in this version
         *
         * @param options
         * ... {function} success - Server success callback
         * ... {function} error - Server error callback
         * @return JIRA.VersionModel
         */
        getUnresolvedIssueCount: function (options) {

            var instance = this;

            options = options || {};

            SmartAjax.makeRequest({
                url: this.url() + this.UNRESOLVED_ISSUES_PATH,
                complete: function (xhr, status, smartAjaxResponse) {
                    if (smartAjaxResponse.successful) {
                        options.success.call(instance, smartAjaxResponse.data);
                    } else {
                        instance._serverErrorHandler(smartAjaxResponse);
                    }
                }
            });

            return this;
        }

    });
});
