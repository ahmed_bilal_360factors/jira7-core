require([
    "jira-project-config/people/table",
    "jquery"
], function(
    RoleTable,
    $
){
    "use strict";

    $(function () {
        var el = $("#project-config-people-table");
        AJS.namespace("JIRA.Admin.RoleTable", null, RoleTable.init(el));
        el.closest("form").addClass("ajs-dirty-warning-exempt"); // Stop dirty form warnings from firing on table
    });
});
