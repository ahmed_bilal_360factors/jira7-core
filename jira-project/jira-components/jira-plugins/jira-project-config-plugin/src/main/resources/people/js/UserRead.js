define("jira-project-config/people/user-read", ["require"], function(require){
    "use strict";

    var TemplatesPeople = require('jira-project-config/people/templates');
    var RestfulTable = require('aui/restful-table');

    return RestfulTable.CustomReadView.extend({
        render: function () {
            return TemplatesPeople.users({
                users: this.model.getFormattedUsers()
            });
        }
    });
});
