/**
 * Renders and assigns controls to table row responsible for editing roles on the people tab
 *
 */
define("jira-project-config/people/row-edit", ["require"], function(require){
    "use strict";

    var RestfulTable = require('aui/restful-table');

    return RestfulTable.EditRow.extend({

        /**
         * Some special mapping of data sent to server. For the ProjectRoleResource we need to send all the changed categorisedActors.
         * Not just the ones that have changed
         *
         * @override
         * @param {Object} params - serialized form data
         * @return data sent to server
         */
        mapSubmitParams: function (params) {
            return this.model.mapSubmitParams(params);
        }
    });
});
