define("jira-project-config/people/table", ["require"], function(require){
    "use strict";

    var jQuery = require('jquery');
    var SmartAjax = require('jira/ajs/ajax/smart-ajax');
    var EditPeopleRow = require('jira-project-config/people/row-edit');
    var GroupEditView = require('jira-project-config/people/group-edit');
    var GroupReadView = require('jira-project-config/people/group-read');
    var PeopleRow = require('jira-project-config/people/row-read');
    var UserEditView = require('jira-project-config/people/user-edit');
    var UserReadView = require('jira-project-config/people/user-read');
    var PeopleModel = require('jira-project-config/people/model');
    var RestfulTable = require('aui/restful-table');
    var ProjectConfigUtils = require('jira-project-config/utils');

    return {
        init: function(el) {
            function getResourceURL() {
                return ProjectConfigUtils.getRestBaseUrl() + "/project/" + ProjectConfigUtils.getKey() + "/role";
            }

            function getRole(callback) {
                SmartAjax.makeRequest({
                    url: getResourceURL(),
                    complete: function (xhr, status, response) {
                        if (response.successful) {
                            var roles = [];

                            var completeHandler = function (xhr, status, response) {
                                if (response.successful) {
                                    var categorisedActors = {};
                                    jQuery.each(response.data.actors, function (index, elt) {
                                        if (!(elt.type in categorisedActors)) {
                                            categorisedActors[elt.type] = [];
                                        }
                                        categorisedActors[elt.type].push(elt.name);
                                    });
                                    response.data.categorisedActors = categorisedActors;
                                    roles.push(response.data);
                                } else {
                                    el.trigger("serverError",
                                        [SmartAjax.buildSimpleErrorContent(response)]);
                                }
                            };

                            for (var roleName in response.data) {
                                SmartAjax.makeRequest({
                                    async: false,
                                    url: response.data[roleName],
                                    complete: completeHandler
                                });
                            }
                            roles.sort(function (a, b) {
                                var aName = a.name.toLowerCase();
                                var bName = b.name.toLowerCase();
                                return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
                            });

                            callback(roles);
                        } else {
                            el.trigger("serverError",
                                [SmartAjax.buildSimpleErrorContent(response)]);
                        }
                    }
                });
            }

            return new RestfulTable({
                allowEdit: true,
                allowCreate: false,
                model: PeopleModel,
                el: el,
                resources: {
                    all: getRole,
                    self: getResourceURL()
                },
                columns: [
                    {
                        id: "name",
                        styleClass: "project-config-role-name",
                        header: AJS.I18n.getText("common.words.project.roles"),
                        allowEdit: false
                    },
                    {
                        id: "users",
                        styleClass: "project-config-role-users",
                        header: AJS.I18n.getText("admin.common.words.users"),
                        emptyText: AJS.I18n.getText("user.picker.add.users"),
                        fieldName: "project-config-people-users-select-textarea",
                        readView: UserReadView,
                        editView: UserEditView
                    },
                    {
                        id: "groups",
                        styleClass: "project-config-role-groups",
                        header: AJS.I18n.getText("common.words.groups"),
                        emptyText: AJS.I18n.getText('admin.usersandgroups.add.group'),
                        fieldName: "project-config-people-groups-select-textarea",
                        readView: GroupReadView,
                        editView: GroupEditView
                    }
                ],
                noEntriesMsg: AJS.I18n.getText("admin.project.role.none"),
                views: {
                    editRow: EditPeopleRow,
                    row: PeopleRow
                }
            });
        }
    };
});
AJS.namespace("JIRA.Admin.RoleTable.Table", null, require("jira-project-config/people/table"));
