define("jira-project-config/people/user-edit", ["require"], function(require){
    "use strict";

    var TemplatesPeople = require('jira-project-config/people/templates');
    var RestfulTable = require('aui/restful-table');

    return RestfulTable.CustomEditView.extend({
        render: function () {
            return TemplatesPeople.editUsers({
                actors: this.model.get('actors')
            });
        }
    });
});
