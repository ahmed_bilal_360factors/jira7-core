define("jira-project-config/people/group-read", ["require"], function(require){
    "use strict";

    var TemplatesPeople = require('jira-project-config/people/templates');
    var RestfulTable = require('aui/restful-table');

    return RestfulTable.CustomReadView.extend({
        render: function () {
            return TemplatesPeople.groups({
                groups: this.model.getFormattedGroups()
            });
        }
    });
});
