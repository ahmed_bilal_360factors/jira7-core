define("jira-project-config/people/role", ["require"], function (require) {
    "use strict";

    var TemplatesPeople = require('jira-project-config/people/templates');
    var RestfulTable = require('aui/restful-table');

    return RestfulTable.Row.extend({

        render: function () {
            var data = this.model.toJSON();

            this.$el.html(TemplatesPeople.roleRow({
                role: data
            }));

            return this;
        }
    });
});
