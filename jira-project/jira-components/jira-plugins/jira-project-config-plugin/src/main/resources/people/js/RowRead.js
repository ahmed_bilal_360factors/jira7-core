define("jira-project-config/people/row-read", ["require"], function(require){
    "use strict";

    var RestfulTable = require('aui/restful-table');

    return RestfulTable.Row.extend({

        initialize: function () {

            RestfulTable.Row.prototype.initialize.apply(this, arguments);

            this.bind(this._event.RENDER, function () {
                var id = this.model.get("id");
                this.$el.attr("id", "people-" + id + "-row").attr("data-id", id);
            });
        },

        renderOperations: function () {
            return "";
        }

    });
});
