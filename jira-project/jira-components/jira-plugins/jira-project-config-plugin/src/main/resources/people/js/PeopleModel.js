define("jira-project-config/people/model", ["require"], function(require){
    "use strict";

    var jQuery = require('jquery');
    var _ = require('underscore');
    var RestfulTable = require('aui/restful-table');

    return RestfulTable.EntryModel.extend({
        _formatModelJSON: function (actors) {
            var formattedData = {groups: [], users: []};
            jQuery.each(actors, function (index, elt) {
                if (elt.type === "atlassian-user-role-actor") {
                    formattedData.users.push(elt);
                } else if (elt.type === "atlassian-group-role-actor") {
                    formattedData.groups.push(elt);
                }
            });
            return formattedData;
        },

        getFormattedGroups: function () {
            return this._formatModelJSON(this.get('actors')).groups;
        },

        getFormattedUsers: function () {
            return this._formatModelJSON(this.get('actors')).users;
        },

        /**
         * Some special mapping of data sent to server. For the ProjectRoleResource we need to send all the changed categorisedActors.
         * Not just the ones that have changed
         *
         * @param {Object} params - serialized form data
         * @return data sent to server
         */
        mapSubmitParams: function (params) {
            var current = this.toJSON();
            var currentUserRole = current.categorisedActors["atlassian-user-role-actor"];
            var currentGroupRole = current.categorisedActors["atlassian-group-role-actor"];
            var data = {categorisedActors: {}};

            if (params.users) {
                if (currentUserRole && !_.isEqual(currentUserRole, params.users)) {
                    data.categorisedActors["atlassian-user-role-actor"] = params.users;
                } else if (!currentUserRole && params.users.length > 0) {
                    data.categorisedActors["atlassian-user-role-actor"] = params.users;
                }
            }

            if (params.groups) {
                if (currentGroupRole && !_.isEqual(currentGroupRole, params.groups)) {
                    data.categorisedActors["atlassian-group-role-actor"] = params.groups;
                } else if (!currentGroupRole && params.groups.length > 0) {
                    data.categorisedActors["atlassian-group-role-actor"] = params.groups;
                }
            }

            if (data.categorisedActors["atlassian-group-role-actor"]) {
                data.categorisedActors["atlassian-user-role-actor"] = params.users;
            }

            if (data.categorisedActors["atlassian-user-role-actor"]) {
                data.categorisedActors["atlassian-group-role-actor"] = params.groups;
            }


            if (!_.isEmpty(data.categorisedActors)) {
                return data;
            }
        }

    });
});
