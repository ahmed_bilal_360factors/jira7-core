AJS.namespace("JIRA.Admin.People.EditPeopleRow", null, require("jira-project-config/people/row-edit"));
AJS.namespace("JIRA.Admin.People.GroupEditView", null, require("jira-project-config/people/group-edit"));
AJS.namespace("JIRA.Admin.People.GroupReadView", null, require("jira-project-config/people/group-read"));
AJS.namespace("JIRA.Admin.People.PeopleRow", null, require("jira-project-config/people/row-read"));
AJS.namespace("JIRA.Admin.People.RoleRow", null, require("jira-project-config/people/role"));
AJS.namespace("JIRA.Admin.People.UserEditView", null, require("jira-project-config/people/user-edit"));
AJS.namespace("JIRA.Admin.People.UserReadView", null, require("jira-project-config/people/user-read"));
AJS.namespace("JIRA.PeopleModel", null, require("jira-project-config/people/model"));
AJS.namespace("JIRA.Templates.People", null, require("jira-project-config/people/templates"));
