define("jira-project-config/people/group-edit", ["require"], function(require){
    "use strict";

    var TemplatesPeople = require('jira-project-config/people/templates');
    var RestfulTable = require('aui/restful-table');

    return RestfulTable.CustomEditView.extend({
        render: function () {
            return TemplatesPeople.editGroups({
                actors: this.model.get('actors')
            });
        }
    });
});
