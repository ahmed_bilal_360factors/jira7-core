define('jira-project-config/libs/ajshelper', ["jquery"], function(jquery) {
    "use strict";

    /**
     * Implementation copied from AUI. This method was deleted in AUI 5.9.0.
     * More info: https://bitbucket.org/atlassian/aui/pull-requests/1128/aui-2911-deprecating-ajs-functions-and
     */
    function triggerEvt(name, args) {
        jquery(document).trigger(name, args);
    }
    function triggerEvtForInst (evt, inst, args) {
        jquery(inst).trigger(evt, args);
        triggerEvt(evt, args);
        if (inst.id) {
            triggerEvt(inst.id + "_" + evt, args);
        }
    }

    var AJS = window.AJS;
    return {
        trigger: AJS.trigger.bind(AJS),
        dim: AJS.dim.bind(AJS),
        undim: AJS.undim.bind(AJS),
        bind: AJS.bind.bind(AJS),
        triggerEvtForInst: triggerEvtForInst,
        messages: AJS.messages
    };
});
