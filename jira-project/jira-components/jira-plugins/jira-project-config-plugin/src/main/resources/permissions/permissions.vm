#enable_html_escaping()
#* @vtlvariable name="permissionGroups" type="java.util.List<com.atlassian.jira.projectconfig.contextproviders.ProjectPermissionContextProvider.SimplePermissionGroup>" *#
<div id="project-config-panel-permissions" class="project-config-panel">
    <div class="aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-image">
                <div class="aui-avatar aui-avatar-large">
                    <div class="aui-avatar-inner project-config-icon48-permissions"></div>
                </div>
            </div>
            <div class="aui-page-header-main">
                <h2 class="project-config-scheme-heading">
                    #set ($hasSchemeDescription = ${schemeDescription} && ${schemeDescription} != "")
                    <span id="project-config-permissions-scheme-name"
                          class="project-config-scheme-name"#if($hasSchemeDescription)
                          title="${schemeDescription}"#end>${schemeName}</span>
                </h2>
                #if ($sharedProjects && $sharedProjects.size() > 1)
                    <span class="aui-lozenge shared-by"><em>$i18n.getText('admin.project.shared.by')</em> <a
                            href="#project-config-permissions-${schemeId}-shared"
                            class="shared-item-trigger">$i18n.getText(
                        'admin.project.shared.projects', ${sharedProjects.size()})</a></span>
                #end
            </div>
            #if ($isAdmin)
                <div class="aui-page-header-actions">
                    <div class="aui-buttons">
                        <button id="project-config-tab-actions" aria-owns="project-config-tab-actions-list"
                                class="aui-button aui-dropdown2-trigger">
                            <span class="aui-icon aui-icon-small aui-iconfont-configure icon-default"></span>
                            ${i18n.getText('common.words.actions')}
                        </button>
                    </div>
                </div>
                <div id="project-config-tab-actions-list" class="aui-dropdown2 aui-style-default">
                    <ul>
                        <li>
                            <a id="project-config-permissions-scheme-edit" data-id="${schemeId}"
                               href="${baseurl}/secure/admin/EditPermissions!default.jspa?schemeId=${schemeId}">$i18n.getText(
                                'admin.project.permissions.edit')</a>
                        </li>
                        <li>
                            <a id="project-config-permissions-scheme-change" data-id="${project.id}"
                               href="${baseurl}/secure/project/SelectProjectPermissionScheme!default.jspa?projectId=${project.id}">$i18n.getText(
                                'admin.project.config.change.scheme')</span></a>
                        </li>
                    </ul>
                </div>
            #end
        </div>
    </div>

    <p>${i18n.getText("admin.project.config.summary.permissions.desc")}</p>
    <p>${i18n.getText("admin.project.config.summary.permissions.desc.extended")}</p>

    $soyRenderer.render('jira.webresources:view-permission-schemes', 'JIRA.Templates.ViewPermissionSchemes.help', {})

    #foreach ($permissionGroup in $permissionGroups)
        <h4 id="project-config-permissions-group-name-${permissionGroup.id}"
            class="project-permissions-category-header">$permissionGroup.name</h4>
        <table id="project-config-permissions-group-${permissionGroup.id}" data-id="${permissionGroup.id}" border="0"
               cellpadding="0" cellspacing="0"
               class="aui project-config-datatable project-config-permissions jira-admin-table">
            <thead>
            <tr>
                <th>${i18n.getText("admin.common.words.permission")}</th>
                <th style="width: 240px;">${i18n.getText("admin.common.words.users.groups.roles")}</th>
            </tr>
            </thead>
            <tbody>
                #foreach($permission in $permissionGroup.permissions)
                <tr id="project-config-permissions-${permission.shortName}" class="project-config-permission"
                    data-id="${permission.shortName}">
                    <td>
                        <span class="project-config-permission-name">$permission.name</span><br/>
                        <span class="project-config-permission-description">$permission.description</span>
                    </td>
                    <td>
                        #if ($permission.entities.size() > 0)
                            <ul class="project-config-list">
                                #foreach ($entity in $permission.entities)
                                    <li class="project-config-permission-entity">$entity</li>
                                #end
                            </ul>
                        #end
                    </td>
                </tr>
                #end
            </tbody>
        </table>
    #end
    #if ($sharedProjects && $sharedProjects.size() > 1)
        <div id="project-config-permissions-${schemeId}-shared" class="shared-item-target">
            <div class="shared-item-content">
                <h3>$i18n.getText('admin.project.shared.list.heading.scheme'):</h3>
                <ul class="shared-project-list">
                    #foreach ($project in $sharedProjects)
                        <li><a class="shared-project-name"
                               href="${baseurl}/plugins/servlet/project-config/${project.key}"><img
                                class="shared-project-icon" width="16" height="16" alt=""
                                src="${baseurl}/secure/projectavatar?size=small&amp;pid=${project.id}&amp;avatarId=${project.avatar.id}"/> $project.name
                        </a></li>
                    #end
                </ul>
            </div>
        </div>
    #end

</div>
