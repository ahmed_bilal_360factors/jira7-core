define("jira-project-config/screen-editor/utils", ["require"], function(require){
    "use strict";

    var ErrorDialog = require('jira/dialog/error-dialog');
    var jQuery = require('jquery');

    function showErrorDialogForData(data) {

        var content;
        //We don't want to use the errors from the server for the time being - JDEV-15776
        if (false && data && data.errorMessages && data.errorMessages.length) {
            content = data.errorMessages.join(' ');
        } else {
            content = AJS.I18n.getText("common.forms.ajax.servererror");
        }

        var dialog = new ErrorDialog({
            message: content
        });

        dialog.show();
    }

    function showErrorDialogForXhr(xhr) {

        var data = null;
        try {
            data = xhr.responseText && jQuery.parseJSON(xhr.responseText);
        } catch (e) {
            //just pass through for generic message
        }
        showErrorDialogForData(data);
    }


    return {
        showErrorDialogForData: showErrorDialogForData,
        showErrorDialogForXhr: showErrorDialogForXhr
    };
});
