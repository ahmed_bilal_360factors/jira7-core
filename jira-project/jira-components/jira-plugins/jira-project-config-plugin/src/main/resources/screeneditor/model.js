define("jira-project-config/screen-editor/model", ["require"], function(require){
    "use strict";

    var Deferred = require('jira/jquery/deferred');
    var jQuery = require('jquery');
    var wrmContextPath = require('wrm/context-path');
    var _ = require('underscore');
    var Backbone = require("jira-project-config/backbone");
    var Utils = require('jira-project-config/screen-editor/utils');

    var safeAjax = function () {
        var def = jQuery.ajax.apply(jQuery, arguments);
        def.fail(Utils.showErrorDialogForXhr);
        return def;
    };

    /**
     * Screen editor model talks to the server.
     */
    return Backbone.Model.extend({

        /**
         * Gets all the tabs for screen
         * @return {$.Deferred}
         */
        getTabs: function () {
            var projectKey = this.get("projectKey");
            return safeAjax({
                url: wrmContextPath() + "/rest/api/2/screens/" + this.get("id") + "/tabs" +
                (projectKey ? "?projectKey=" + encodeURI(projectKey) : "")
            });
        },
        /**
         * Adds a tab to screen
         * @param {Number} tab - TabId
         * @param {String} field - field id
         * @return {$.Deferred}
         */
        addFieldToTab: function (tab, currentTab, field) {
            var deferred = Deferred();
            var instance = this;
            safeAjax({
                url: wrmContextPath() + "/rest/api/2/screens/" + this.get("id") + "/tabs/" + currentTab + "/fields/" + field,
                type: "DELETE",
                contentType: "application/json"
            }).done(function () {
                safeAjax({
                    url: wrmContextPath() + "/rest/api/2/screens/" + instance.get("id") + "/tabs/" + tab + "/fields",
                    type: "POST",
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        fieldId: field
                    })
                }).done(function () {
                    deferred.resolve();
                }).fail(function () {
                    deferred.reject();
                });
            }).fail(function () {
                deferred.reject();
            });
            return deferred.promise();
        },
        /**
         * Renames a tab
         * @param {Number} tab - tab id
         * @param {String} name - New name
         * @return {$.Deferred}
         */
        renameTab: function (tab, name) {
            //The errors returned on rename are handled in method that call this.
            return jQuery.ajax({
                type: "PUT",
                url: wrmContextPath() + "/rest/api/2/screens/" + this.get("id") + "/tabs/" + tab,
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    name: name
                })
            });
        },
        /**
         * Gets all the fields for specified tab
         * @param {Number} tab - tab id
         * @return {$.Deferred}
         */
        getFieldsForTab: function (tab) {
            return safeAjax({
                url: wrmContextPath() + "/rest/api/2/screens/" + this.get("id") + "/tabs/" + tab + "/fields"
            });
        },
        /**
         * Deletes tab
         * @param {Number} tab
         * @return {$.Deferred}
         */
        deleteTab: function (tab) {
            return safeAjax({
                type: "DELETE",
                url: wrmContextPath() + "/rest/api/2/screens/" + this.get("id") + "/tabs/" + tab
            });
        },

        /**
         * Moves tab position
         * @param {Number} tab - tab id
         * @param {Number} pos - position to move to
         * @return {$.Deferred}
         */
        updateTabPosition: function (tab, pos) {
            return safeAjax({
                type: "POST",
                url: wrmContextPath() + "/rest/api/2/screens/" + this.get("id") + "/tabs/" + tab + "/move/" + pos
            });
        },

        /**
         * Adds a tab
         * @param {String} name - Name of tab
         * @return {$.Deferred}
         */
        addTab: function (name) {
            //Errors rendered by calling code.
            return jQuery.ajax({
                url: wrmContextPath() + "/rest/api/2/screens/" + this.get("id") + "/tabs/",
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    name: name
                }),
                type: "POST"
            }).done(_.bind(function (tab) {
                this.trigger("tabAdded", tab.id);
            }, this));
        },
        /**
         * Gets available fields - List of fields that are not already on the screen
         * @return {$.Deferred}
         */
        getAvailableFields: function () {
            var projectKey = this.get("projectKey");
            return safeAjax({
                url: wrmContextPath() + "/rest/api/2/screens/" + this.get("id") + "/availableFields" +
                (projectKey ? "?projectKey=" + encodeURI(projectKey) : "")
            });
        }
    });
});
