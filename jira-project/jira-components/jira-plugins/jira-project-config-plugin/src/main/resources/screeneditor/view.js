define("jira-project-config/screen-editor/view", ["require"], function(require){
    "use strict";

    var Deferred = require('jira/jquery/deferred');
    var jQuery = require('jquery');
    var wrmContextPath = require('wrm/context-path');
    var _ = require('underscore');
    var FormDialog = require('jira/dialog/form-dialog');
    var SingleSelect = require('jira/ajs/select/single-select');
    var RestfulTable = require('aui/restful-table');
    var Backbone = require("jira-project-config/backbone");
    var ScreenAddTabView = require('jira-project-config/screen-editor/add-tab');
    var TemplateScreenEditor = require('jira-project-config/screen-editor/template');
    var Utils = require('jira-project-config/screen-editor/utils');
    var logger = require('jira/util/logger');

    return Backbone.View.extend({
        initialize: function () {

            jQuery(document).on("mousedown", _.bind(function (e) {
                this.blurRename(e);
            }, this));

            // Whenever we add a new tab. We activate it.
            this.model.bind("tabAdded", _.bind(function (tab) {
                this.render(tab);
                jQuery(this.el).trigger("tabAdded");
            }, this));

            jQuery(document).keydown(_.bind(function (e) {
                if (document.activeElement === document.body && e.keyCode === jQuery.ui.keyCode.TAB) {
                    e.preventDefault();
                    if (e.shiftKey) {
                        this.prev();
                    } else {
                        this.next();
                    }
                }
            }, this));
        },

        // Delegate events
        events: {
            "click .delete-tab": "deleteTab",
            "click .menu-item:not(.active-tab)": "activateTab",
            "click .tab-edit": "renameTab"
        },

        next: function () {
            var active = this.$el.find(".active-tab");
            var next = active.next(".menu-item");

            if (!next.length) {
                next = this.$el.find(".menu-item").first();
            }

            next.click();
        },

        prev: function () {
            var active = this.$el.find(".active-tab");
            var prev = active.prev(".menu-item");

            if (!prev.length) {
                prev = this.$el.find(".menu-item").last();
            }

            prev.click();
        },

        blurRename: function (e) {
            if (jQuery("body").hasClass("renaming-tab") && jQuery(e.target).closest(".rename-active").length === 0) {
                this.trigger("applyRename");
                e.preventDefault();
            }
        },

        /**
         * Handles the rendering and submission of inline tab rename form
         * @param e
         */
        renameTab: function (e) {

            e.stopPropagation(); // Stop aui tabs from activating tab
            e.preventDefault(); // Don't follow link

            this.undelegateEvents();
            var instance = this;
            var $currentTarget = jQuery(e.currentTarget);
            var tab = $currentTarget.data("tab");
            var $parent = $currentTarget.closest(".menu-item").addClass("rename-active");
            var $container = $parent.find("strong");
            var $existingLabel = $container.find(".tab-label");
            var value = jQuery.trim($existingLabel.text());
            var $existing = $container.contents().detach();
            jQuery("body").addClass("renaming-tab");

            $container.html(TemplateScreenEditor.renameTab({
                value: value
            }));

            var $tabName = $container.find(".tab-name").focus().select();
            var $submit = $container.find(":submit");
            var $cancel = $container.find(".cancel");

            $cancel.click(function (e) {
                jQuery("body").removeClass("renaming-tab");
                $parent.removeClass("rename-active");
                $container.empty();
                $container.append($existing);
                instance.updateMinWidth();
                instance.delegateEvents();
                instance.unbind("applyRename");
                e.preventDefault();
            });

            $tabName.keydown(function (e) {
                if (e.keyCode === jQuery.ui.keyCode.ESCAPE) {
                    $cancel.click();
                }
            });

            instance.updateMinWidth();

            $submit.click(function (e) {
                var name = $tabName.val();
                if (name !== $tabName.prop("defaultValue")) {
                    instance.model.renameTab(tab, name).done(function () {
                        instance.delegateEvents();
                        jQuery("body").removeClass("renaming-tab");
                        $parent.removeClass("rename-active");
                        $existingLabel.text(name);
                        $container.html($existing);
                        instance.updateMinWidth();
                        instance.unbind("applyRename");
                        jQuery(instance.el).trigger("tabRenamed");
                    }).fail(function (xhr) {
                        var res = JSON.parse(xhr.responseText);
                        if (res && res.errors && res.errors.name) {
                            // if there is something wrong with the name input. Display inline error.
                            var $error = jQuery("<div class='error'/>").text(res.errors.name);
                            AJS.InlineDialog(".tab-name", "rename-error", function ($contents, control, show) {
                                $contents.html($error);
                                show();
                                $tabName.focus().select();
                            }).show();
                        } else {
                            Utils.showErrorDialogForData(res);
                        }

                    }).always(function () {
                        logger.trace("tab.rename.complete"); // Webdriver wants to know when we have finished

                    });
                } else {
                    instance.unbind("applyRename");
                    $cancel.click();
                }

                e.preventDefault();
            });

            function apply() {
                instance.unbind("applyRename");
                if ($submit.is(":visible")) {
                    $submit.click();
                }
            }

            this.bind("applyRename", apply);
        },

        /**
         * Shows a confirmation dialog for deleting tab
         * @param {Event} e
         */
        deleteTab: function (e) {

            var instance = this;
            var $tab = jQuery(e.currentTarget);
            var tab = $tab.data("tab");
            var dialog = new FormDialog({
                id: "delete-tab-" + tab,
                content: function (ready) {
                    var tabName = $tab.closest(".menu-item").find(".tab-label").text();
                    tabName = soy.$$escapeHtml(tabName);
                    instance.model.getFieldsForTab(tab).done(function (fields) {
                        ready(jQuery(TemplateScreenEditor.deleteTabConfirmation({
                            name: tabName,
                            fields: fields
                        })));
                    });
                }
            });

            dialog._submitForm = _.bind(function (e) {
                e.preventDefault();
                this.model.deleteTab(tab).done(_.bind(function () {
                    jQuery(this.el).trigger("tabDeleted");
                    this.render(this.selectedTab);
                }, this));
                dialog.hide();
            }, this);

            dialog.show();
            e.stopPropagation(); // Stop aui tabs from activating tab
            e.preventDefault(); // Don't follow link
        },

        /**
         * Used for draggin between tabs. This will open the specified tab, add the field to it, then activate sorting on it.
         * @param {$} $tab - The tab to activate
         * @param {String} field - field id
         */
        activateTabWithField: function ($tab, field) {
            var instance = this;
            var tab = $tab.data("tab");
            var currentTab = this.$el.find(".active-tab").data("tab");
            this.model.addFieldToTab($tab.data("tab"), currentTab, field).done(_.bind(function () {
                instance.table.$tbody.sortable("destroy");
                jQuery(instance.el).trigger("fieldMovedTab");
                this.render(tab);
            }, this)).fail(function () {
                instance.table.$tbody.sortable("cancel");
            });
        },

        _hoverShimEventHandler: function (event) {
            // When we upgrade to jQuery 1.9+ we will need to verify this continues to work.
            // The semantics around event.srcElement and event.target change in 1.9.
            // It's likely that we will need to use event.target instead.
            var $target = jQuery(event.srcElement);
            if (!$target.hasClass("hover-shim")) {
                // jQuery detects the incorrect target in FF; get it from the original event instead.
                $target = jQuery(event.originalEvent.target);
            }
            if ($target.hasClass("hover-shim")) {
                this.table.$tbody.sortable({
                    update: null
                });
                $target.trigger("drop", event);
            }
        },

        /**
         * When dragging between tabs, we position transparent <div>'s over the top of the tabs with a higher zIndex then the field
         * we are dragging. This is so that when the mouse hovers over the tab the mouse enter event is fired. We then wait a
         * little bit and if we are still on the tab we activate it, adding the field we are dragging.
         * @param {String} field
         */
        activateHoverShims: function (field) {
            var instance = this;

            this.$el.find(".menu-item:not(.active-tab)").each(function () {
                var $tab = jQuery(this);
                $tab.addClass("field-drop-target");
                var offset = $tab.offset();
                jQuery("<div class='hover-shim'/>")
                    .css({
                        position: "absolute",
                        top: offset.top,
                        left: offset.left,
                        width: $tab.outerWidth(),
                        height: $tab.outerHeight(),
                        zIndex: 9999
                    })
                    .mouseenter(function () {
                        $tab.addClass("hover");
                    })
                    .mouseleave(function () {
                        $tab.removeClass("hover");
                    })
                    .on("drop", function () {
                        instance.activateTabWithField($tab, field);
                    })
                    .appendTo("body");
            });
        },

        /**
         * Removes all the hover shims
         */
        disableHoverShims: function () {
            this.$el.find(".field-drop-target").removeClass("field-drop-target");
            jQuery(".hover-shim").each(function () {
                var $shim = jQuery(this);
                $shim.remove();
            });
        },

        /**
         * Ensure tabs do not wrap
         */
        updateMinWidth: function () {
            var width = 20;
            this.$el.find(".menu-item, .add-tab").each(function () {
                width += jQuery(this).outerWidth();
            });
            this.$el.css("minWidth", width);

        },

        /**
         * Activates the tab from $ element
         * @param {Event} e
         */
        activateTab: function (e) {
            this.render(jQuery(e.currentTarget).data("tab"));
            e.preventDefault();
            e.stopPropagation();
        },

        /**
         * Renders screen editor. Including tabs and restful table
         *
         * @param {Long} tab - Active tab id
         * @return {*}
         */
        render: function (tab) {
            var deferred = Deferred();
            var instance = this;
            var model = this.model;
            var selectedTab = tab;
            var $el = this.$el;

            $el.addClass("screen-editor");
            $el.toggleClass("read-only", model.get("readOnly"));

            model.getTabs().done(function (tabs) {
                // If supplied tab doesn't exist make first tab selected
                if (!_.any(tabs, function (thisTab) {
                        return thisTab.id === parseInt(tab, 10);
                    })) {
                    selectedTab = tabs[0].id;
                }

                // Renders the tabs
                $el.html(TemplateScreenEditor.tabs({
                    selectedTab: selectedTab,
                    tabs: tabs
                }));

                instance._renderTabs($el, tabs, selectedTab);
                instance._renderTable($el, selectedTab).done(deferred.resolve);

                if (window.sessionStorage) {
                    window.sessionStorage.setItem("selectedScreenTab", selectedTab);
                }
                instance.selectedTab = selectedTab;
            }).fail(function () {
                deferred.reject();
            });
            return deferred.promise();
        },

        _renderTabs: function ($container, tabs, selectedTab) {
            var model = this.model;
            var readOnly = model.get("readOnly");
            // Renders the tabs
            $container.html(TemplateScreenEditor.tabs({
                selectedTab: selectedTab,
                tabs: tabs
            }));

            if (!readOnly) {
                // Setup sortable tabs
                var $tabMenu = jQuery(".tabs-menu", $container);
                $tabMenu.sortable({
                    axis: "x",
                    delay: 0,
                    cursor: "move",
                    items: ".menu-item",
                    update: function (event, ui) {
                        var index = jQuery(".tabs-menu .menu-item", $container).index(ui.item);
                        model.updateTabPosition(ui.item.data("tab"), index).done(function () {
                            logger.trace("screen.tab.order.updated");
                            $container.trigger("tabReordered");
                        }).fail(function () {
                            $tabMenu.sortable("cancel");
                        });
                    }
                });

                // Initialize the add tab dialog
                AJS.InlineDialog(".add-tab", "add-tab", function (contents, trigger, doShowPopup) {
                    contents.html(new ScreenAddTabView({model: model}).render());
                    doShowPopup();
                    _.defer(function () {
                        contents.find(":text").focus();
                    });
                });
            }
        },

        _renderTable: function ($container, selectedTab) {
            var AddFieldView;
            var deferred = Deferred();
            var firstRender = true;
            var instance = this;
            var model = this.model;
            var projectKey = model.get("projectKey");
            var readOnly = model.get("readOnly");
            var RowModel;
            var ViewRow;

            // A custom render for the name column in restful table.
            AddFieldView = RestfulTable.CustomCreateView.extend({
                render: function () {
                    var deferred = Deferred();

                    // We need to update available fields every time we render. Things can change, fields can be added
                    // and removed etc...
                    model.getAvailableFields().done(function (availableFields) {
                        var $content = jQuery(TemplateScreenEditor.availableFields({fields: availableFields}));
                        var $picker = jQuery("#field-picker", $content);
                        var singleSelect = new SingleSelect({
                            element: $picker,
                            setMaxHeightToWindow: true
                        });

                        $picker.on('selected', function () {
                            singleSelect.submitForm();
                        });

                        singleSelect.showErrorMessage = jQuery.noop;

                        deferred.resolve($content);
                        // Only focus this field if we add a field (not on tab load).
                        if (!firstRender) {
                            //IE8 needs an extra call here to work.
                            singleSelect.$field.focus();
                            singleSelect.$field.focus();
                        }
                        firstRender = false;

                    });
                    return deferred.promise();
                }
            });

            RowModel = RestfulTable.EntryModel.extend({
                save: function (data, options) {
                    var oldError = options.error;
                    options.error = function (model, data, xhr) {

                        //If a 400 error comes back with no field related errors, then display a global
                        //error message because it will be ignored otherwise.
                        if (xhr.status === 400 && (!data || _.isEmpty(data.errors))) {
                            Utils.showErrorDialogForData(data);
                        }

                        if (oldError) {
                            return oldError.apply(this, arguments);
                        }
                    };
                    //JS === super(data, options);
                    return RestfulTable.EntryModel.prototype.save.call(this, data, options);
                }
            });

            ViewRow = RestfulTable.Row.extend({
                renderOperations: function () {
                    var row = this;
                    if (!readOnly) {
                        return jQuery("<a href='#' class='aui-button'/>")
                            .addClass(this.classNames.DELETE)
                            .text(AJS.I18n.getText("admin.common.words.remove")).click(function (e) {
                                e.preventDefault();
                                row.destroy();
                            });
                    }
                    return null;
                },
                destroy: function () {
                    this.model.destroy({
                        error: function (xhr) {
                            //Non 400 errors are processed by the AJS.RestfulTable.Events.SERVER_ERROR handler.
                            if (xhr.status === 400) {
                                Utils.showErrorDialogForXhr(xhr);
                            }
                        }
                    });
                }
            });

            // https://developer.atlassian.com/display/AUI/AJS.RestfulTable
            this.table = new RestfulTable({
                el: jQuery("#tab-" + selectedTab),
                allowCreate: !readOnly,
                allowEdit: false,
                allowReorder: !readOnly,
                createPosition: "bottom",
                resources: {
                    all: wrmContextPath() + "/rest/api/2/screens/" + model.get("id") + "/tabs/" + selectedTab + "/fields" +
                    (projectKey ? "?projectKey=" + encodeURI(projectKey) : ""),
                    self: wrmContextPath() + "/rest/api/2/screens/" + model.get("id") + "/tabs/" + selectedTab + "/fields"
                },
                noEntriesMsg: AJS.I18n.getText('admin.screens.no.fields'),
                columns: [
                    {
                        id: "name",
                        styleClass: "field-name-cell",
                        header: "Field",
                        createView: !readOnly && AddFieldView
                    }
                ],
                model: RowModel,
                views: {
                    row: ViewRow
                }
            });

            if (readOnly) {
                jQuery(this.table).on(RestfulTable.Events.INITIALIZED, function () {
                    instance.table.$el.removeClass("aui-restfultable-allowhover");
                });
            } else {
                // Modifying the default restfultable sortable behaviour
                this.table.$tbody.sortable({
                    axis: false,
                    appendTo: "body",
                    containment: false
                });

                // Activating and disabling our hover shims on sortstart/end
                this.table.$tbody.on("sortstart", function (event, ui) {
                    if (jQuery.browser.msie && jQuery.browser.version === 8) {
                        ui.placeholder.height(ui.helper.height());
                    }
                    jQuery("body").addClass("sorting-fields");
                    instance.activateHoverShims(instance.table.getRowFromElement(ui.item[0]).model.id);
                }).on("sortstop", function () {
                    jQuery("body").removeClass("sorting-fields");
                    instance.disableHoverShims();
                    instance.updateMinWidth();
                }).on("sortbeforestop", _.bind(instance._hoverShimEventHandler, this));

                // When we remove a field we need to re render the fields list as it will now include
                // the remove field
                jQuery(this.table).on(RestfulTable.Events.ROW_REMOVED, function () {
                    $container.trigger("fieldRemoved");
                    new AddFieldView().render().done(function ($content) {
                        jQuery(".aui-restfultable-create .field-name-cell").html($content);
                    });
                });

                jQuery(this.table).on(RestfulTable.Events.ROW_ADDED, function () {
                    $container.trigger("fieldAdded");
                });

                jQuery(this.table).on(RestfulTable.Events.REORDER_SUCCESS, function () {
                    $container.trigger("fieldReordered");
                });
            }

            // Once the restful table has finished loading. So have we.
            jQuery(this.table).on(RestfulTable.Events.INITIALIZED, function () {
                deferred.resolve();
                instance.updateMinWidth();
            });

            return deferred.promise();
        }
    });
});
