define("jira-project-config/screen-editor", ["require"], function(require){
    "use strict";

    var jQuery = require('jquery');
    var AJSHelper = require('jira-project-config/libs/ajshelper');
    var ScreenEditorModel = require('jira-project-config/screen-editor/model');
    var ScreenEditorView = require('jira-project-config/screen-editor/view');

    /**
     * @classdesc The JIRA screen editor.
     *
     * @param {Object} options
     * @param {Element} options.element The element to show the screen editor in.
     * @param {String} [options.analyticsName] The postfix to use when firing analytics events.
     * @param {String} [options.projectKey] The key of the project this screen editor instance is associated with
     * @param {String} [options.screenId] The ID of the screen config to load.
     * @param {Number} [options.selectedTab] The active tab ID.
     * @param {boolean} [options.readOnly] Whether the Screen Editor should be read only or not.
     * @constructor
     */
    return function (options) {
        options.element = jQuery(options.element);

        if (!options.selectedTab && window.sessionStorage) {
            options.selectedTab = window.sessionStorage.getItem("selectedScreenTab");
        }

        this.screenEditorModel = new ScreenEditorModel({
            id: options.screenId,
            projectKey: options.projectKey,
            readOnly: !!options.readOnly
        });

        new ScreenEditorView({
            el: options.element,
            model: this.screenEditorModel
        }).render(options.selectedTab);

        addScreenEditorAnalytics(options.element, options.analyticsName);
    };

    function addScreenEditorAnalytics(screenEditorEl, suffix) {
        if (!suffix) {
            return;
        }
        if (screenEditorEl.length) {
            AJSHelper.trigger('analyticsEvent', {name: "administration.screen.designer.view." + suffix});
        }
        jQuery(screenEditorEl).bind("fieldAdded", function () {
            AJSHelper.trigger(
                'analyticsEvent',
                {name: "administration.screen.designer.field.added." + suffix}
            );
        });
        jQuery(screenEditorEl).bind("fieldRemoved", function () {
            AJSHelper.trigger(
                'analyticsEvent',
                {name: "administration.screen.designer.field.removed." + suffix}
            );
        });
        jQuery(screenEditorEl).bind("fieldReordered", function () {
            AJSHelper.trigger(
                'analyticsEvent',
                {name: "administration.screen.designer.field.reordered." + suffix}
            );
        });
        jQuery(screenEditorEl).bind("tabAdded", function () {
            AJSHelper.trigger(
                'analyticsEvent',
                {name: "administration.screen.designer.tab.added." + suffix}
            );
        });
        jQuery(screenEditorEl).bind("tabDeleted", function () {
            AJSHelper.trigger(
                'analyticsEvent',
                {name: "administration.screen.designer.tab.removed." + suffix}
            );
        });
        jQuery(screenEditorEl).bind("tabRenamed", function () {
            AJSHelper.trigger(
                'analyticsEvent',
                {name: "administration.screen.designer.tab.renamed." + suffix}
            );
        });
        jQuery(screenEditorEl).bind("tabReordered", function () {
            AJSHelper.trigger(
                'analyticsEvent',
                {name: "administration.screen.designer.tab.reordered." + suffix}
            );
        });
        jQuery(screenEditorEl).bind("fieldMovedTab", function () {
            AJSHelper.trigger(
                'analyticsEvent',
                {name: "administration.screen.designer.field.moved.tab." + suffix}
            );
        });
    }
});
