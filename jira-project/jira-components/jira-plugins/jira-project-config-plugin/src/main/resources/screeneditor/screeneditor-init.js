require([
    'aui/restful-table',
    'jira-project-config/screen-editor',
    'jira/util/events',
    'jira/dialog/error-dialog',
    'jquery',
    'jira-project-config/screen-editor/utils'
], function(RestfulTable, ScreenEditorApplication, Events, ErrorDialog, jQuery, Utils) {

    jQuery(function () {
        /**
         * When we drag a field from tab to tab. We store this so that we can start the sorting of the restful table
         * in the destination tab. To do this we need a reference the mouse coordinates.
         */
        var mouse;
        jQuery(document).mousemove(function (e) {
            mouse = e;
        });

        Events.bind(RestfulTable.Events.SERVER_ERROR, function (e, data) {
            Utils.showErrorDialogForData(data);
        });

        /**
         * Global error for forbidden. Show a dialog that lets them log in if logged out.
         */
        jQuery(document).ajaxError(function (e, xhr, ajaxOptions) {
            if (ajaxOptions.url && ajaxOptions.url.indexOf("/rest/api/2/screens/") !== -1 && xhr.status === 401) {
                ErrorDialog.openErrorDialogForXHR(xhr);
            }
        });

        var $screenEditor = jQuery("#screen-editor");
        if ($screenEditor.length) {
            new ScreenEditorApplication({
                element: $screenEditor,
                screenId: $screenEditor.data("screen"),
                analyticsName: "global"
            });
        }
    });
});
