AJS.namespace("JIRA.ScreenEditor.Application", null, require("jira-project-config/screen-editor"));
AJS.namespace("JIRA.Screens.AddTabView", null, require("jira-project-config/screen-editor/add-tab"));
AJS.namespace("JIRA.Screens.ScreenEditorModel", null, require("jira-project-config/screen-editor/model"));
AJS.namespace("JIRA.Screens.ScreenEditorView", null, require("jira-project-config/screen-editor/view"));
AJS.namespace("JIRA.Templates.screenEditor", null, require("jira-project-config/screen-editor/template"));
