define("jira-project-config/screen-editor/add-tab", ["require"], function(require){
    "use strict";

    var jQuery = require('jquery');
    var Backbone = require("jira-project-config/backbone");
    var TemplateScreenEditor = require('jira-project-config/screen-editor/template');
    var Utils = require('jira-project-config/screen-editor/utils');

    /**
     * Contents of add tab inline dialog
     */
    return Backbone.View.extend({

        // Delegate events
        events: {
            "submit form": "submit",
            "click": "preventHide"
        },

        className: "add-tab-contents",

        /**
         * hack: The only way to prevent hiding inline dialog when clicking it's contents
         * @param {Event} e
         */
        preventHide: function (e) {
            e.stopPropagation();
        },

        /**
         * Handles form submission
         * @param {Event} e
         */
        submit: function (e) {
            var name = this.$el.find("input[name='name']").val();
            this.model.addTab(name).done(function () {
                AJS.InlineDialog.current.hide(); // on success hid the dialog
            }).fail(function (xhr) {
                var res = JSON.parse(xhr.responseText);
                if (res && res.errors && res.errors.name) {
                    // if there is something wrong with the name input. Display inline error.
                    var $error = jQuery("<div class='error'/>").text(res.errors.name);
                    var $popup = AJS.InlineDialog.current.popup;
                    $popup.find(".error").remove();
                    $popup.find("form").append($error);
                } else {
                    Utils.showErrorDialogForData(res);
                }
            });
            e.preventDefault();
        },

        /**
         * Renders soy template
         * @return {$}
         */
        render: function () {
            this.$el = jQuery(this.el);
            this.$el.html(TemplateScreenEditor.addTabDialog());
            return this.$el;
        }
    });
});
