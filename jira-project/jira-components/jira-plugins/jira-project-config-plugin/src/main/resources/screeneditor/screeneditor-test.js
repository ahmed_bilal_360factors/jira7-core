AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:screens-editor"], function(){
    "use strict";

    var jQuery = require("jquery");
    var ScreenEditorModel = require("jira-project-config/screen-editor/model");

    var instance = {};

    module("JIRA.Screens.ScreenEditorView", {
        setup: function () {
            var sandbox = this.sandbox = sinon.sandbox.create();
            this.context = AJS.test.mockableModuleContext();

            this.context.mock('aui/restful-table', function() {
                return {
                    $tbody: {
                        sortable: sandbox.spy()
                    }
                };
            });
            var RestfulTable = this.context.require("aui/restful-table");
            var ScreenEditorView = this.context.require("jira-project-config/screen-editor/view");

            var model = new ScreenEditorModel();
            instance = new ScreenEditorView({model: model});
            instance.table = new RestfulTable({});
        },

        teardown: function () {
            this.sandbox.restore();
        }
    });

    test("jQuery UI is the correct version", function () {
        /*
         This test is a flag to ensure that drag-n-drop behaviour is maintained after a jQuery update because we
         haven't been able to work out a reasonable way of automatically testing it.
         Please manually test that you can still drag fields between tabs on the screen designer
         The typical URL is http://localhost:8090/jira/secure/admin/ConfigureFieldScreen.jspa?id=1
         You should test in all supported browsers, as behaviour is inconsistent :(
         If it's broken, you'll probably need to update _hoverShimEventHandler in screeneditor.js. In particular,
         event.srcElement might need to instead fetch a different element (in jquery-ui 1.9 we expect it to be
         event.target). If that functionality no longer exists, you can delete this test. Otherwise, once you've
         verified it's working or fixed it, please either update the version check in this test for the next lucky
         individual to update jQuery or write a better test.
         */
        var version = jQuery.ui.version;
        equal(version, "1.8.24");
    });

    test("drop event is fired on srcElement when it has hover-shim class", function () {
        var $div = jQuery('<div class="hover-shim"></div>');
        var event = {
            srcElement: $div
        };

        var spy = this.sandbox.spy();

        $div.on("drop", spy);

        instance._hoverShimEventHandler(event);

        ok(spy.calledOnce);
        ok(spy.args[0][1] === event);
    });

    test("event.originalEvent.target is used when srcElement lacks the hover-shim class", function () {
        var badDiv = jQuery('<div></div>');
        var $div = jQuery('<div class="hover-shim"></div>');
        var event = {
            srcElement: badDiv,
            originalEvent: {target: $div}
        };

        var spy = this.sandbox.spy();

        $div.on("drop", spy);

        instance._hoverShimEventHandler(event);

        ok(spy.calledOnce);
        ok(spy.args[0][1] === event);
    });
});

