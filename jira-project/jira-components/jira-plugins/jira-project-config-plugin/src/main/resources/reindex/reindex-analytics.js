require(['jira-project-config/libs/ajshelper', 'jquery', 'jira/project/projectdata'], function(AJSHelper, $, projectdata) {
    "use strict";

    $(function () {
        $(document).ready(function () {
            $('#confirm-project-reindex-button').on("click", function () {
                AJSHelper.trigger('analyticsEvent', {
                    name: 'jira.projectReindex.confirm',
                    data: {projectType: projectdata.getProjectType()}
                });
            });
        });
    });
});
