define("jira-project-config/custom-fields/views/empty-type-item", [
    'jira-project-config/custom-fields/templates',
    "jira-project-config/marionette",
    "wrm/context-path"
], function(CustomFieldTemplates, Marionette, contextPath) {
    "use strict";

    var TEMPLATES = CustomFieldTemplates;

    /**
     * A view used to indicate that no types are available.
     */
    var EmptyTypeItemView = Marionette.ItemView.extend({
        tagName: "li",

        className: "empty",

        triggers: {
            "click .customfields-all-category-link": "ongotoall"
        },

        setEmptyState: function (state) {
            this.emptyState = state;
            this.render();
        },

        serializeData: function () {
            return this.emptyState;
        },

        template: function (serializedData) {
            var html;

            switch (serializedData) {
                case EmptyTypeItemView.CHANGE_TAB:
                    html = TEMPLATES.emptyTypeOtherTab();
                    break;

                case EmptyTypeItemView.MARKETPLACE:
                    html = TEMPLATES.emptyTypeMarketplace({
                        contextPath: contextPath()
                    });
                    break;

                // case: "empty"
                default:
                    html = TEMPLATES.emptyType();
            }

            return html;
        }
    }, {
        CHANGE_TAB: "otherTab",
        MARKETPLACE: "marketplace",
        EMPTY: "empty"
    });

    return EmptyTypeItemView;
});

AJS.namespace("JIRA.Admin.CustomFields.EmptyTypeItemView", null, require("jira-project-config/custom-fields/views/empty-type-item"));
