AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:custom-fields", "com.atlassian.jira.jira-project-config-plugin:custom-fields-impl"], function(){
    "use strict";

    var _ = require("underscore");
    var wrmContextPath = require("wrm/context-path");
    var CustomFieldUtils = require("jira-project-config/custom-fields/util");

    var Model = function (value) {
        this.foo = value;
    };

    _.extend(Model.prototype, {
        get: function (name) {
            return this[name];
        }
    });

    module("JIRA.Admin.CustomFields.utils", {});

    test("lowerCaseLocaleComparator honors the locale", function () {
        function compare(a, b) {
            var aModel = new Model(a);
            var bModel = new Model(b);
            return CustomFieldUtils.lowerCaseLocaleComparator('foo')(aModel, bModel);
        }

        strictEqual(compare("a", "B"), "a".localeCompare("b"), "localeComparator should match case insensitive localeCompare()");
        strictEqual(compare("a", "b"), "a".localeCompare("b"), "localeComparator should match localeCompare()");
        strictEqual(compare("\u00E4", "b"), "\u00E4".localeCompare("b"), "localeComparator should match localeCompare()");
        strictEqual(compare(null, "a"), -1, "nulls should come first");
        strictEqual(compare("a", null), 1, "nulls should come first");
        strictEqual(compare(null, null), 0, "two nulls are equal");
        strictEqual(compare(undefined, "a"), -1, "undefined should come first");
        strictEqual(compare("a", undefined), 1, "undefined should come first");
        strictEqual(compare(undefined, undefined), 0, "two undefined arguments are equal");
    });

    test("url follows modern conventions", function () {
        var url = CustomFieldUtils.url;
        var contextPath = wrmContextPath();

        equal(url("/foo/"), contextPath + "/foo/", "without params");
        equal(url("/foo/", {a: "b"}), contextPath + "/foo/?a=b", "single param");
        equal(url("/foo/", {a: ["b", "c"]}), contextPath + "/foo/?a=b&a=c", "multi value single param");

        // don't rely on the order of object keys
        ok(_.contains([contextPath + "/foo/?a=b&c=d", contextPath + "/foo/?c=d&a=b"],
            url("/foo/", {a: "b", c: "d"})),
            "multi param, order agnostic");
    });

    test("url allows encoded array elements", function () {
        var url = CustomFieldUtils.url;
        var contextPath = wrmContextPath();

        equal(url(['abc', 'def']), contextPath + "/abc/def", "Build URL from path components.");
        equal(url(['abc', '<def>']), contextPath + "/abc/" + encodeURIComponent('<def>'),
            "Build URL from path components with escaping.");
        equal(url(['abc', '<def></def>', 'jkl'], {a: "b"}), contextPath + "/abc/" + encodeURIComponent('<def></def>') + '/jkl?a=b',
            "Build URL from path components with escaping and params.");
        equal(url(['abc', 101], {a: "b"}), contextPath + "/abc/101?a=b",
            "Build URL from path components with escaping and params.");
    });
});
