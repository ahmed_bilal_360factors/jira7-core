define("jira-project-config/custom-fields/wizard/step", [
 "jira/jquery/deferred",
 "jira-project-config/custom-fields/wizard/events",
 "jira-project-config/backbone",
 "underscore"
], function(Deferred, Events, Backbone, _) {
    /**
     * A single step within a Wizard. This object is designed to be extended and have a set of methods overridden which
     * provide the custom behaviour for the specific step.
     *
     * This interface is that which the Wizard interacts with. Extending this object provides the barebones methods
     * required for the Wizard.
     *
     * If you require some additional functionality, see AbstractStep - it provides some implementations for the methods
     * of this object.
     *
     * @fires Step#next
     * @fires Step#complete
     * @fires Step#cancel
     */
    return _.extend({}, {

        // Convenience methods

        /**
         * Called by an extended Step when it wishes to transition to another step.
         */
        fireNext: function () {
            this.trigger(Events.next);
        },

        /**
         * Called by an extended Step when it wishes to transition to the previous step.
         */
        firePrevious: function () {
            this.trigger(Events.previous);
        },

        /**
         * Called by an extended Step when it wishes to cancel the Wizard.
         * @param {boolean} [confirm] whether or not dirtiness should be queried for displaying dirty form warning
         */
        fireCancel: function (confirm) {
            this.trigger(Events.cancel, confirm || false);
        },

        /**
         * Called by an extended Step when it wishes to complete.
         */
        fireComplete: function () {
            this.trigger(Events.complete);
        },

        // Methods intended for overriding

        /**
         * Called by the wizard to start this step. Should return a promise that should be resolved when it has finished starting.
         * Implementations of this should normally involve completely preparing the step to be displayed, but not actually displaying it.
         *
         * This method should be overridden.
         */
        startStep: function () {

        },

        /**
         * Called by the wizard when it has been requested to close.
         * Implementations of this should normally involve destroying the UI of the step, and doing general clean up.
         *
         * This method should be overridden.
         */
        stopStep: function () {

        },

        /**
         * Called by the wizard when transitioning to another step, and the new step is ready to display.
         * Implementations of this should normally involve hiding the UI of the step.
         *
         * This method should be overridden.
         */
        pauseStep: function () {

        },

        /**
         * Called by the wizard when transitioning to this step, and this step has finished starting.
         * Implementations of this should normally involve showing the UI of the step.
         *
         * This method should be overridden.
         */
        show: function () {

        },

        /**
         * Called by the wizard before transitioning to another step, and the user should be informed that the new step is loading.
         * Implementations of this should normally involve displaying feedback representing that something is being loaded.
         *
         * This method should be overridden.
         */
        showLoading: function () {

        },

        /**
         * Called by the wizard after transitioning to another step, and the loading feedback on this step should be cleaned up.
         * Implementations of this should normally involve reversing the effect of showLoading()
         *
         * This method should be overridden.
         */
        hideLoading: function () {

        },

        /**
         * Does any completion required by the step. Should return a promise.
         *
         * This method should be overridden if complete behaviour is required by the step.
         */
        doComplete: function () {
            return Deferred().resolve();
        },

        /**
         * Present errors to the user.
         *
         * This method should be overridden.
         *
         * @param {object} fieldErrors An object describing errors for individual fields. Keys are field names, and
         *     values are human readable error messages.
         * @param {string[]} generalErrors An array of human readable error messages.
         */
        /*jshint unused: vars */
        showErrors: function (fieldErrors, generalErrors) {

        },

        /**
         * Called by the wizard to determine whether the proposed next step is appropriate, given the step's state
         *
         * @param {Step} proposedNextStep the proposed next step
         * @return {boolean} whether the proposed step is appropriate
         */
        /*jshint unused: vars */
        isExistingNextStepAppropriate: function (proposedNextStep) {

        },

        /**
         * Resolves the next step for the wizard to transition to.
         *
         * This method should be overridden.
         *
         * @param {Step} proposedNext the proposed next step - may be null if there is nothing proposed
         * @param {object} sharedStepOptions the options that can be shared across all steps
         */
        /*jshint unused: vars */
        getNextStep: function (proposedNext, sharedStepOptions) {

        },

        /**
         * Returns the data associated with this step, and all steps prior.
         *
         * This method should be overridden.
         */
        getAccumulatedData: function () {

        },

        /**
         * Sets the previous data on this step
         *
         * This method should be overridden.
         *
         * @param previousData the accumulated data of all steps prior to this one
         */
        setPreviousData: function (previousData) {

        },

        /**
         * Called by the wizard to find out if a step is dirty (aka has unchanged state).
         *
         * This method should be overridden.
         */
        isDirty: function () {

        }
    }, Backbone.Events);
});

AJS.namespace("JIRA.Admin.CustomFields.Wizard.Step", null, require("jira-project-config/custom-fields/wizard/step"));
