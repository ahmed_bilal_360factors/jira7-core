define("jira-project-config/custom-fields/steps/create-field", [
    "jira/jquery/deferred",
    "jira-project-config/custom-fields/steps/create-field/views/create-dialog",
    "jira-project-config/custom-fields/wizard/abstract-step",
    "jira-project-config/custom-fields/steps/helper",
    "underscore"
], function(
    Deferred,
    CreateDialogView,
    AbstractStep,
    CustomFieldsStepHelper,
    _) {
    "use strict";

    /**
     * A step that collects options for the new field.
     *
     * @param {IssueCustomFields} options.api The REST API.
     * @param {boolean} options.inIssueContext whether the step is created in the context of an issue or not.
     */
    var CreateFieldStep = function (options) {
        this.options = _.extend({
            view: CreateDialogView
        }, options);
        this.api = options.api;
        this.inIssueContext = !!options.inIssueContext;
        this.stepData = {};
    };

    _.extend(CreateFieldStep.prototype, AbstractStep, {
        /**
         * Shows a step that can be used to create a custom field.
         */
        start: function () {
            var previousData = this.getPreviousData();

            var type = previousData.type;
            this.fieldTypeName = type.name;
            var viewOptions = _.extend(_.pick(this.options, ['nextLabel', 'previousLabel']), {
                configureOptions: !!type.options && !type.cascading,
                typeName: type.name,
                fieldName: (previousData.field && previousData.field.name) || ""
            });

            var view = this.view = new this.options.view(viewOptions);
            this.listenTo(view, "submit", function (data) {
                this.stepData.field = {};
                this.stepData.field.name = data.name;
                this.stepData.field.description = data.description;
                this.stepData.field.options = data.options;
                this.view.disable();
                this.fireComplete();
            });
            this.listenTo(view, "previous", function () {
                this.firePrevious();
            });
            this.listenTo(view, "cancel", function (check) {
                this.fireCancel(check);
            });

            return Deferred().resolve().promise();
        },

        restart: function () {
            var previousData = this.getPreviousData();
            if (this._hasFieldTypeChanged(previousData.type.name)) {
                this.stopStep();
                return this.start();
            } else {
                this.view.updateData({
                    typeName: previousData.type.name,
                    fieldName: (previousData.field && previousData.field.name) || ""
                });
                return Deferred().resolve().promise();
            }
        },

        stopStep: function () {
            if (this.view) {
                this.stopListening(this.view);
                this.view.destroy();
                delete this.view;
            }
        },

        pauseStep: function () {
            this.view && this.view.hide();
        },

        show: function () {
            this.view && this.view.show();
        },

        doComplete: function () {
            var completes;
            var data = this.getAccumulatedData();

            completes = this._resolveCompletesToExecute(data);
            return this.executeCompletes(completes, {
                api: this.api,
                data: data
            });
        },

        showErrors: function (fieldErrors, generalErrors) {
            this.view.enable();
            this.view.showErrors(fieldErrors, generalErrors);
        },

        showLoading: function () {
            this.view && this.view.showLoading();
        },

        hideLoading: function () {
            this.view && this.view.hideLoading();
        },

        getStepData: function () {
            return this.stepData;
        },

        isDirty: function () {
            var field = this.stepData.field;
            var isStepDataDirty = field && (field.name || field.options || field.description);
            return isStepDataDirty || (this.view && this.view.isDirty());
        },

        _hasFieldTypeChanged: function (currentTypeName) {
            return this.fieldTypeName !== currentTypeName;
        },

        _resolveCompletesToExecute: function (data) {
            var completes = [];
            completes.push(this.createField);
            if (data.field.options) {
                completes.push(this.addOptions);
            }
            if (this.inIssueContext) {
                completes.push(this.putFieldOnIssue);
                completes.push(this.refreshIssue);
            }
            return completes;
        }
    }, CustomFieldsStepHelper);

    return CreateFieldStep;
});


AJS.namespace("JIRA.Admin.CustomFields.CreateFieldStep", null,
    require("jira-project-config/custom-fields/steps/create-field"));

AJS.namespace("JIRA.Admin.CustomFields.CreateFieldStep.Views.CreateDialog", null,
    require("jira-project-config/custom-fields/steps/create-field/views/create-dialog"));

AJS.namespace("JIRA.Admin.CustomFields.CreateFieldStep.Views.CreatePanel", null,
    require("jira-project-config/custom-fields/steps/create-field/views/create-panel"));
