define("jira-project-config/custom-fields/views/forms/options-field", [
    'jira-project-config/libs/auihelper',
    'jira-project-config/custom-fields/templates',
    'jira/util/formatter',
    "jira-project-config/custom-fields/views/forms/form-field",
    "jira-project-config/custom-fields/views/forms/field-mixins",
    "jira-project-config/backbone",
    "jira-project-config/marionette",
    "underscore",
    "jquery"
], function(
    AUITemplates,
    CustomFieldTemplates,
    formatter,
    FormField,
    FieldMixins,
    Backbone,
    Marionette,
    _,
    $) {
    "use strict";

    var TEMPLATES = CustomFieldTemplates;
    var AUI_TEMPLATES = AUITemplates.form;

    var Bus = function () {
    };
    _.extend(Bus.prototype, Backbone.Events);

    var OptionsField = FormField.extend({
        mixin: [FieldMixins.NamedFieldMixin],
        initialize: function () {
            this.bus = new Bus();
            this.options = new Options.Models.Options();
            this.view = new Options({
                collection: this.options,
                bus: this.bus
            });
            this.enabled = true;
            this.listenTo(this.bus, "removeOption", this._removeOption);
            this.listenTo(this.bus, "moveOption", this._moveOption);
        },
        focus: function () {
            return this.view.focus();
        },
        blur: function () {
            return this.view.blur();
        },
        validate: function () {
            if (!this.options.length) {
                this.setError(formatter.I18n.getText("admin.project.customfieldadd.no.options.error"));
                return false;
            } else {
                this.clearError();
                return true;
            }
        },
        setError: function (error) {
            this.view.setError(error);
            return this;
        },
        clearError: function () {
            this.view.clearError();
            return this;
        },
        value: function (value) {
            if (_.isUndefined(value)) {
                return this.options.map(function (input) {
                    return input.pick("name");
                });
            } else {
                //not implemented.
                return this;
            }
        },
        disable: function () {
            this.enabled = false;
            this.view.disable();
            return this;
        },
        enable: function () {
            this.enabled = true;
            this.view.enable();
            return this;
        },
        render: function () {
            this.setElement(this.view.render().$el);
            return this;
        },
        handleSubmit: function () {
            return this.view.handleSubmit();
        },
        isDirty: function () {
            return this.view.isDirty();
        },
        _removeOption: function (option) {
            if (this.enabled) {
                this.options.remove(option);
                this.focus();
            }
        },
        _moveOption: function (options) {
            if (this.enabled) {
                var option = options.option;
                this.options.remove(option, {silent: true});
                this.options.add(option, {silent: true, at: options.newIndex});
                this.focus();
            }
        }
    });

    var Options = Marionette.ItemView.extend({
        initialize: function (options) {
            this.bus = options.bus;
            this.options = options.collection;
            this.optionsView = new Options.Views.OptionViews({
                collection: this.options,
                bus: this.bus
            });
        },
        ui: {
            "input": "#custom-field-options-input",
            "addBtn": "#custom-field-options-add",
            "listContainer": "#custom-field-options-list-container"
        },
        template: TEMPLATES.singleSelectOptionsComponent,
        onRender: function () {
            this.ui.listContainer.append(this.optionsView.render().$el);
            var add = _.bind(this._addOptionFromUi, this);
            this.ui.addBtn.click(add);
            this.ui.input.keydown(function (e) {
                if (e.keyCode === $.ui.keyCode.ENTER) {
                    e.preventDefault();
                    add();
                }
            });
        },
        _addOptionFromUi: function () {
            var errors = [];
            if (!this.addOption(this.input(), errors)) {
                this.setError(errors[0]);
            } else {
                this.clearError().clearInput();
            }
            this.focus();
            return this;
        },
        handleSubmit: function () {
            return false;
        },
        getOptions: function () {
            return this.options.pick('name');
        },
        addOption: function (optionName, errors) {
            errors = errors || [];
            optionName = $.trim(optionName);
            if (!optionName.length) {
                errors.push(formatter.I18n.getText("admin.options.empty.name"));
                return false;
            } else {
                var id = this._nameToId(optionName);
                if (this.options.get(id)) {
                    errors.push(formatter.I18n.getText("admin.errors.customfields.value.already.exists"));
                    return false;
                } else {
                    this.options.add({
                        name: optionName,
                        id: id
                    });
                    return true;
                }
            }
        },
        _getFocus: function () {
            return this.$(":focus");
        },
        focus: function () {
            if (!this.ui.input.prop("disabled")) {
                this.ui.input.focus();
                return true;
            } else {
                return false;
            }
        },
        blur: function () {
            var focusEl = this._getFocus();
            if (focusEl.length) {
                focusEl.blur();
                return true;
            } else {
                return false;
            }
        },
        clearInput: function () {
            this.ui.input.val('');
            return this;
        },
        input: function (value) {
            if (_.isUndefined(value)) {
                return this.ui.input.val();
            } else {
                this.ui.input.val(value);
                return this;
            }
        },
        setError: function (text) {
            if (this.$error) {
                this.$error.text(text);
            } else {
                this.$error = $(AUI_TEMPLATES.fieldError({message: text}));
                this.ui.addBtn.after(this.$error);
            }
            return this;
        },
        clearError: function () {
            if (this.$error) {
                this.$error.remove();

                delete this.$error;
            }
            return this;
        },
        _nameToId: function (name) {
            return name.toLowerCase();
        },
        enable: function () {
            this.ui.listContainer.removeClass("disabled");
            this._setInputState(true);
        },
        disable: function () {
            this.ui.listContainer.addClass("disabled");
            this._setInputState(false);
        },
        _setInputState: function (enabled) {
            this.ui.input.prop("disabled", !enabled);
            this.ui.addBtn.prop("disabled", !enabled);
        },
        isDirty: function () {
            return this.ui.input.val() || this.options.length;
        }
    });

    Options.Models = {};
    Options.Models.Option = Backbone.Model.extend({});
    Options.Models.Options = Backbone.Collection.extend({
        model: Options.Models.Option
    });

    Options.Views = {};
    Options.Views.OptionView = Marionette.ItemView.extend({
        events: {
            "click .custom-field-options-delete": "_delete",
            "moveOption": "_moveOption"
        },
        tagName: "li",
        template: TEMPLATES.singleSelectOptionsItem,
        initialize: function (options) {
            this.bus = options.bus;
        },
        _delete: function (e) {
            e.preventDefault();
            this.bus.trigger("removeOption", this.model);
        },
        _moveOption: function (e, newIndex) {
            this.bus.trigger("moveOption", {option: this.model, newIndex: newIndex});
        }
    });
    Options.Views.OptionViews = Marionette.CollectionView.extend({
        itemView: Options.Views.OptionView,
        tagName: "ul",
        id: "custom-field-options-list",
        initialize: function (options) {
            this.bus = options.bus;

            this.$el.sortable({
                axis: "y",
                containment: "#custom-field-options-list-container",
                tolerance: 'pointer',
                cursor: "move",
                handle: ".custom-field-option-handle",
                update: this._handleReorder
            });
        },
        itemViewOptions: function () {
            return {
                bus: this.bus
            };
        },
        _handleReorder: function (e, ui) {
            ui.item.trigger("moveOption", ui.item.index());
        }
    });

    return OptionsField;
});

AJS.namespace("JIRA.Admin.CustomFields.OptionsField", null, require("jira-project-config/custom-fields/views/forms/options-field"));
