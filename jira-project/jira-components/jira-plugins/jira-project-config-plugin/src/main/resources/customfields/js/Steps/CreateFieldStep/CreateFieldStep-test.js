AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:custom-fields",
    "com.atlassian.jira.jira-project-config-plugin:custom-fields-impl"], function () {

    var Backbone = require("jira-project-config/backbone");
    var Deferred = require("jira/jquery/deferred");
    var CreateFieldStep = require("jira-project-config/custom-fields/steps/create-field");

    module("JIRA.Admin.CustomFields.CreateFieldStep", {
        setup: function () {
            this.sandbox = sinon.sandbox.create();

            var view = this.view = new Backbone.View({});
            view.show = this.sandbox.stub();
            view.hide = this.sandbox.stub();
            view.showErrors = this.sandbox.stub();
            view.disable = this.sandbox.stub();
            view.enable = this.sandbox.stub();
            view.isDirty = this.sandbox.stub();
            view.updateData = this.sandbox.stub();
            view.destroy = this.sandbox.stub();

            var sharedStepOptions = {
                api: this.sandbox.stub()
            };
            this.step = new CreateFieldStep({
                sharedStepOptions: sharedStepOptions,
                view: function (options) {
                    view.lastArgs = options;
                    return view;
                },
                nextLabel: "Label",
                somethingElse: "Else"
            });
            this.step.setPreviousData({
                type: {
                    name: "test"
                },
                field: {
                    name: "name"
                }
            });
        },
        teardown: function () {
            this.sandbox.restore();
        }
    });

    test("view's show() only called once", function () {
        this.step.start();
        this.step.show();
        ok(this.view.show.calledOnce, "Show on the view should have been called.");
    });

    test("doComplete() passes all completes to executeCompletes when all are required", function () {
        this.step.inIssueContext = true;

        this.step.stepData.field = {options: {}};

        var executeCompletesDeferred = Deferred();
        var executeCompletesStub = this.sandbox.stub(this.step, "executeCompletes").returns(executeCompletesDeferred.promise());

        var spy = this.sandbox.spy();
        this.step.doComplete().done(spy);

        var expectedCompletes = [this.step.createField, this.step.addOptions, this.step.putFieldOnIssue, this.step.refreshIssue];

        ok(executeCompletesStub.calledOnce, "executeCompletes() should have been called once");
        ok(executeCompletesStub.firstCall.calledWith(expectedCompletes), "executeCompletes() should have been passed the expected completes");
        ok(!spy.called, "spy should not yet have been called");

        executeCompletesDeferred.resolve();

        ok(spy.calledOnce, "spy should have been called once");

    });

    test("doComplete() only passes the createField complete to executeCompletes when not in issue context and has no options", function () {
        this.step.inIssueContext = false;

        this.step.stepData.field = {};

        var executeCompletesDeferred = Deferred();
        var executeCompletesStub = this.sandbox.stub(this.step, "executeCompletes").returns(executeCompletesDeferred.promise());

        var spy = this.sandbox.spy();
        this.step.doComplete().done(spy);

        var expectedCompletes = [this.step.createField];

        ok(executeCompletesStub.calledOnce, "executeCompletes() should have been called once");
        ok(executeCompletesStub.firstCall.calledWith(expectedCompletes), "executeCompletes() should have been passed the expected completes");
        ok(!spy.called, "spy should not yet have been called");

        executeCompletesDeferred.resolve();

        ok(spy.calledOnce, "spy should have been called once");

    });

    test("doComplete() returns rejected promise when call to executeCompletes returns rejected promise", function () {
        var spy = this.sandbox.spy();

        this.step.inIssueContext = true;

        this.step.stepData.field = {options: {}};

        var executeCompletesDeferred = Deferred();
        this.sandbox.stub(this.step, "executeCompletes").returns(executeCompletesDeferred.reject().promise());

        this.step.doComplete().fail(spy);

        ok(spy.calledOnce, "spy should have been called once");
    });

    test("submit when view indicates submit", function () {
        this.sandbox.stub(this.step, "fireComplete");
        this.step.start();
        this.step.show();

        this.view.trigger("submit", {name: "Name"});

        ok(this.step.fireComplete.calledOnce, "Complete called on submit.");
        deepEqual(this.step.stepData.field.name, "Name", "Submit stashes data in the step.");
        ok(this.view.disable.calledOnce, "Disabled the UI on submit.");
    });

    test(".showErrors", function () {
        var errors = {"name": "Error"};
        var generalErrors = ["error"];

        this.step.start();
        this.step.show();
        this.step.showErrors(errors, generalErrors);
        ok(this.view.enable.calledOnce, "Enabled on the UI.");
        ok(this.view.showErrors.firstCall.calledWith(errors, generalErrors), "Delegate error to the view.");
    });

    test("isDirty", function () {
        ok(!this.step.isDirty(), "Step should not be dirty before being displayed.");

        this.step.start();
        this.step.show();
        ok(!this.step.isDirty(), "Step should not be dirty.");

        this.view.isDirty.returns(true);
        ok(this.step.isDirty(), "Step should be dirty.");

        this.view.isDirty.returns(false);
        this.step.stepData = {field: {name: "name", description: "description", options: "options"}};
        ok(this.step.isDirty(), "Step should be dirty.");
    });

    test("restartTypeNotChanged", function () {
        this.step.start();
        this.step.fieldTypeName = "test";
        this.step._previousData.type.name = "test";

        var restartDeferred = this.step.restart();
        ok(this.view.updateData.called, "View data updated.");
        ok(restartDeferred.isResolved(), "Successful restart.");
    });

    test("restartTypeChanged", function () {
        this.step.start();
        this.step.fieldTypeName = "test";
        this.step._previousData.type.name = "other";

        var startSpy = this.sandbox.spy(this.step, "start");

        var restartDeferred = this.step.restart();
        ok(!this.view.updateData.called, "Update data not called on view because start() was called.");
        ok(this.view.destroy.calledOnce, "The dialog should have been destroyed");
        ok(startSpy.calledOnce, "The step should have been started");
        ok(restartDeferred.isResolved(), "Successful restart.");
    });

    /**
     * Check that the restart method works when the previous data is not passed or is invalid.
     *
     * @param [fieldData] the previous steps field data.
     */
    var checkRestartNoPreviousName = function (fieldData) {
        var data = {
            type: {
                name: "test"
            }
        };

        if (fieldData) {
            data.field = fieldData;
        }

        this.step.fieldTypeName = "test";
        this.step.setPreviousData(data);
        this.step.start();

        var restartDeferred = this.step.restart();
        ok(this.view.updateData.calledWith({typeName: "test", fieldName: ""}), "View data updated with defaults.");
        ok(restartDeferred.isResolved(), "Successful restart.");
    };

    test("restartTypeNotChanged no previous field data", function () {
        checkRestartNoPreviousName.call(this, null);
    });

    test("restartTypeNotChanged undefined previous name", function () {
        checkRestartNoPreviousName.call(this, {name: null});
    });
});
