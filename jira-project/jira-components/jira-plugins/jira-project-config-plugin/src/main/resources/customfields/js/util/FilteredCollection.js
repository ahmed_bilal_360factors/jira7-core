define("jira-project-config/custom-fields/filtered-collection", ["jira-project-config/backbone", "underscore"], function(Backbone, _) {

    /**
     * A Backbone collection that supports filtering.
     *
     * @param {function} [options.matcher] A function that searches a single model to
     * @type {*}
     */
    return Backbone.Collection.extend({
        initialize: function () {
            this.filteredModels = [];
        },

        /**
         * Apply a search-based filter to the collection, which adds and removes elements.
         *
         * @param query
         */
        filter: function (query) {
            var instance = this;
            var state = {};

            // Break the hidden items into two groups, "show" and "hide".
            var partition = _.groupBy(this.filteredModels, function (model) {
                return instance.matcher(model, query, state) ? "show" : "hide";
            });

            // The "hide" group should still be hidden.
            this.filteredModels = partition.hide || [];

            // The "show" group should be visible.
            _.each(partition.show, function (model) {
                instance.add(model);
            });

            // Hide visible items that no longer satisfy the filter.
            _.chain(this.models)
                // Find all the visible models that should now be hidden.
                .filter(function (model) {
                    return !instance.matcher(model, query, state);
                })
                // Hide them.
                .each(function (model) {
                    instance.remove(model);
                    instance.filteredModels.push(model);
                });
        }
    });
});

AJS.namespace("JIRA.Admin.CustomFields.FilteredCollection", null, require("jira-project-config/custom-fields/filtered-collection"));
