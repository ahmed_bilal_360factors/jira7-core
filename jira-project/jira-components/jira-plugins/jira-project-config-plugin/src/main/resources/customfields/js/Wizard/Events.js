define("jira-project-config/custom-fields/wizard/events", {
    /**
     * Fired when changing to next step.
     */
    next: "next",
    /**
     * Fired when changing to previous step.
     */
    previous: "previous",
    /**
     * Fired when the step/complete wants to kill the dialog because it is no longer possible to proceed.
     */
    fail: "fail",
    /**
     * Fired by a step/complete when an error occurs that can be rendered and handled in the wizard.
     */
    error: "error",
    /**
     * Triggered by a step when it wishes to complete the wizard.
     */
    complete: "complete",
    /**
     * Triggered by a step/complete when the websudo dialog is show
     */
    websudoBeforeShow: "websudo:before:show",
    /**
     * Triggered when the user successfully enters websudo.
     */
    websudoSuccess: "websudo:success",
    /**
     * Triggered by a step when the user wants cancel the wizard.
     */
    cancel: "cancel"
});
