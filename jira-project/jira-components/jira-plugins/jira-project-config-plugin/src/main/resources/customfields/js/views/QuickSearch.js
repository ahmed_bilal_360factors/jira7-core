define("jira-project-config/custom-fields/views/quick-search", [
    'jira-project-config/custom-fields/templates',
    "jira-project-config/marionette",
    "underscore"
], function(CustomFieldTemplates, Marionette, _) {
    "use strict";

    var TEMPLATES = CustomFieldTemplates;
    /**
     * A search input widget.
     *
     * @constructor
     *
     * @fires QuickSearch#change
     */
    return Marionette.ItemView.extend({
        className: "search",

        ui: {
            input: "input"
        },

        initialize: function (options) {
            options = options || {};
            if (options.delay) {
                this.onInputInput = _.debounce(this.onInputInput, options.delay);
            }
        },

        /**
         * Move the browser focus to the search input.
         *
         * @returns {*}
         */
        focus: function () {
            this.ui.input.focus();
            return this;
        },

        /**
         * Handler for value changes to input.
         */
        onInputInput: function () {
            /**
             * change:query event
             *
             * @event QuickSearch#change
             * @type {object}
             * @property {string} value The current search query
             */

            this.triggerMethod("change", {
                value: this.value()
            });
        },

        template: function () {
            return TEMPLATES.quickSearch();
        },

        /**
         * Return the search query.
         *
         * @returns {string} The current search query
         */
        value: function () {
            return this.ui.input.val();
        },

        render: function () {
            Marionette.ItemView.prototype.render.apply(this, arguments);
            this.ui.input.on("input", _.bind(this.onInputInput, this));
            return this;
        }
    });
});

AJS.namespace("JIRA.Admin.CustomFields.QuickSearch", null, require("jira-project-config/custom-fields/views/quick-search"));
