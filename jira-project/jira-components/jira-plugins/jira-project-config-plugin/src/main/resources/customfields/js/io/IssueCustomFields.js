define("jira-project-config/custom-fields/service", [
    "jira/jquery/deferred",
    "jira-project-config/custom-fields/util",
    "jira/ajs/ajax/smart-ajax",
    "jira/ajs/ajax/smart-ajax/web-sudo",
    "underscore"
], function(Deferred, Util, SmartAjax, WebSudoSmartAjax, _) {
    "use strict";

    /**
     * @param {boolean} [options.autoCloseWebsudo] If true, the websudo dialog will automatically be closed on success.
     * @param {string} [options.issue] An issue's ID. This is required by some methods.
     * @constructor
     */
    var IssueCustomFields = function (options) {
        options || (options = {});

        _.extend(this, _.defaults(options, {
            autoCloseWebsudo: true
        }));
    };

    var standardErrorHandler = function (promise, statusText, smartAjaxResult) {
        if (statusText !== "abort") {
            promise.reject(IssueCustomFields.STATUS.error, SmartAjax.buildSimpleErrorContent(smartAjaxResult));
        } else {
            promise.reject(IssueCustomFields.STATUS.abort, null);
        }
    };

    /**
     * Partially apply a function
     *
     * @param {function} func The function to partially apply.
     * @returns {*}
     */
    var partial = function (func) {
        return _.bind.apply(_, [func, undefined].concat(_.toArray(arguments).slice(1)));
    };

    _.extend(IssueCustomFields.prototype, {

        /**
         * Wrap the passed promise such that its success will trigger a call to the server to find all the projects
         * that will be affected by adding a new field to an issue. The result of this calculation will be added
         * to the data returned during the outer call. An error on the passed promise will not trigger a call to server.
         *
         * @param {jQuery.Deferred} wrap the promise the wrap.
         * @returns {jQuery.Promise}
         */
        getSharedBy: function (wrap) {
            var fields = this;
            var def = Deferred();
            wrap.done(function (origData) {
                var instance = this;
                var args;
                if (_.isUndefined(fields.issue)) {
                    origData.sharedBy = [];
                    def.resolveWith(this, arguments);
                } else {
                    args = _.toArray(arguments);
                    var ajax = fields._ajax({
                        type: "GET",
                        url: Util.url(["rest", "globalconfig", 1, "issuecustomfields", fields.issue, "affectedProjects"]),
                        dataType: "json",
                        contentType: "application/json"
                    });
                    ajax.done(function (sharedBy) {
                        origData.sharedBy = sharedBy;
                        def.resolveWith(instance, args);
                    });
                    ajax.fail(function () {
                        origData.sharedBy = [];
                        def.resolveWith(instance, args);
                    });
                    ajax.progress(function () {
                        def.notifyWith(instance, arguments);
                    });
                }
            });
            wrap.fail(function () {
                def.rejectWith(this, arguments);
            });
            wrap.progress(function () {
                def.notifyWith(this, arguments);
            });
            return def.promise();
        },

        /**
         * Put a field on all of an issue's screens.
         *
         * @param {string} issue The issue's ID -- e.g. ABC-1
         * @param {string} field The field's ID -- e.g. customfield_12345
         * @returns {jQuery.Promise}
         * @private
         */
        addFieldToScreen: function (field) {
            var adding = Deferred();
            if (_.isUndefined(this.issue)) {
                adding.reject(IssueCustomFields.STATUS.error, "No issue provided.");
            } else {
                var ajax = this._ajax({
                    type: "POST",
                    url: Util.url(["rest", "globalconfig", 1, "issuecustomfields", this.issue]),
                    data: field,
                    dataType: "json",
                    contentType: "application/json"
                });

                ajax.done(adding.resolve);
                ajax.progress(adding.notify);
                ajax.fail(partial(standardErrorHandler, adding));
            }

            return adding.promise();
        },

        /**
         * Create a custom field.
         *
         * @param {string} field.name The field's name
         * @param {string} field.description The field's description
         * @param {string} field.searcherKey
         * @param {string} field.type The custom field type that the new field is an instance of.
         * @returns {jQuery.Promise} resolved with the field, rejected with errors
         */
        addField: function (field) {
            var instance = this;
            var data = _.pick(field, "name", "description", "searcherKey", "type");

            var def = this._ajax({
                type: 'POST',
                url: Util.url("/rest/api/2/field"),
                context: this,
                data: JSON.stringify(data),
                dataType: "json",
                contentType: "application/json"
            });

            // Massage the standard smart ajax 'fail' params into something friendly.
            return def.pipe(undefined, function (statusText, smartAjaxResult) {
                return instance._parseErrorCollection(smartAjaxResult, {fieldName: "name"});
            });
        },

        /**
         * Retrieves the custom fields that can be added to an issue's screens. Can provide
         * an additional boolean flag to also retrieve the fields that are already on the screen.
         *
         * @param {boolean} [onAllScreensFlag] optional flag to indicate whether to include fields that are on the screen
         * @returns {Array.<{id: {string}, name: {string}}>}
         */
        addableFields: function (onAllScreensFlag) {
            var fields = Deferred();
            if (_.isUndefined(this.issue)) {
                fields.reject(IssueCustomFields.STATUS.error, "No issue provided.");
            } else {
                var ajax = this._ajax({
                    type: "GET",
                    url: Util.url(["rest", "globalconfig", 1, "issuecustomfields", this.issue, "fields"], {onAllScreensFlag: !!onAllScreensFlag}),
                    contentType: "application/json"
                });

                ajax.done(fields.resolve);
                ajax.progress(fields.notify);
                ajax.fail(partial(standardErrorHandler, fields));
            }

            return fields.promise();
        },

        /**
         * Set the options of a custom field.
         *
         * @param {string} fieldId the id of the field to configure.
         * @param {[Object]} options the options to set for the field as {name: OptionName}
         * @returns {jQuery.Promise}
         */
        setOptions: function (fieldId, options) {
            var def = this._ajax({
                type: 'POST',
                url: Util.url(["rest", "globalconfig", "1", "customfieldoptions", fieldId]),
                context: this,
                data: JSON.stringify(options),
                dataType: "json",
                contentType: "application/json"
            });

            var wrapped = Deferred();
            def.done(wrapped.resolve);
            def.progress(wrapped.notify);
            def.fail(partial(standardErrorHandler, wrapped));

            return wrapped;
        },
        getCustomFieldTypes: function () {
            var ajaxing = this._ajax({
                type: "GET",
                url: Util.url("/rest/globalconfig/1/customfieldtypes"),
                dataType: "json",
                contentType: "application/json"
            });

            var wrapped = Deferred();

            ajaxing.done(wrapped.resolve);
            ajaxing.progress(wrapped.notify);
            ajaxing.fail(partial(standardErrorHandler, wrapped));

            return wrapped;
        },

        /**
         * @private
         */
        _remapObject: function (object, mapping) {
            var remapped = {};
            _.each(object, function (value, key) {
                var newName = mapping[key] || key;
                remapped[newName] = value;
            });
            return remapped;
        },

        /**
         * Extract general and field errors from a smart ajax result.
         *
         * @param {SmartAjaxResponse} response
         * @param {object} mapping {From: To, ...} mapping describing translation of keys
         * @returns {{fields: {object.<{string}, {string}>}, general: {string[]}}}
         * @private
         */
        _parseErrorCollection: function (response, mapping) {
            var generalErrors;
            var fieldErrors;

            if (response.hasData) {
                try {
                    var parsed = JSON.parse(response.data);
                    if (!_.isEmpty(parsed.errorMessages)) {
                        generalErrors = parsed.errorMessages;
                    }
                    if (!_.isEmpty(parsed.errors)) {
                        fieldErrors = parsed.errors;
                        if (mapping) {
                            fieldErrors = this._remapObject(fieldErrors, mapping);
                        }
                    }
                } catch (e) {
                    //parse fail. Fall through
                }
            }

            if (!generalErrors && !fieldErrors) {
                generalErrors = [SmartAjax.buildSimpleErrorContent(response)];
            }

            return {
                fields: fieldErrors || {},
                general: generalErrors || []
            };
        },

        /**
         * @private
         */
        _ajax: function (ajaxOptions) {
            var instance = this;
            var wrapped = Deferred();
            var ajax = WebSudoSmartAjax.makeWebSudoRequest(ajaxOptions, {
                beforeShow: function () {
                    wrapped.notify(IssueCustomFields.PROGRESS.SHOW);
                },
                success: function (closeFunction) {
                    wrapped.notify(IssueCustomFields.PROGRESS.SUCCESS, closeFunction);
                    instance.autoCloseWebsudo && closeFunction();
                },
                cancel: function (e) {
                    e.preventDefault();
                    //Undefined because we don't actually have an XHR when they hit cancel on WebSudo.
                    wrapped.rejectWith(instance, ["abort", undefined]);
                }
            });

            ajax.done(function () {
                wrapped.resolveWith(this, _.toArray(arguments));
            });
            ajax.fail(function (xhr, statusText, errorThrown, smartAjaxResult) {
                wrapped.rejectWith(this, [statusText, smartAjaxResult]);
            });

            return wrapped.promise();
        }
    });

    IssueCustomFields.STATUS = {
        abort: "abort",
        error: "error"
    };

    IssueCustomFields.PROGRESS = {
        SHOW: "websudoShow",
        SUCCESS: "websudoSuccess"
    };

    return IssueCustomFields;
});

AJS.namespace("JIRA.Admin.CustomFields.IssueCustomFields", null, require("jira-project-config/custom-fields/service"));
