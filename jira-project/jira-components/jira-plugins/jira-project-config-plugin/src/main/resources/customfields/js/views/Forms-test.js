AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:custom-fields",
    "com.atlassian.jira.jira-project-config-plugin:custom-fields-impl"], function () {
    "use strict";

    var Form = require("jira-project-config/custom-fields/views/forms/form");
    var jQuery = require("jquery");
    var _ = require("underscore");

    var MockField = Form.Fields.FormField.extend({
        initialize: function (options) {
            this.parent = Form.Fields.FormField.prototype;
            this.parent.initialize.call(this, options || {});
        },
        value: function () {
            if (arguments.length) {
                this._value = arguments[0];
                return this;
            } else {
                return this._value;
            }
        },
        setError: function (error) {
            this._error = error;
        },
        clearError: function () {
            delete this._error;
        },
        error: function () {
            return this._error;
        }
    });

    module("JIRA.Admin.CustomFields.Form.Validators", {
        setup: function () {
            this.sandbox = sinon.sandbox.create();
        },
        teardown: function () {
            this.sandbox.restore();
        }
    });

    test("always", function () {
        var always = Form.Validators.always();
        ok(always(), "Always should always validate.");
    });

    test("notEmpty no error", function () {
        var field = new MockField().value(20);
        var error = "Error";
        var notEmpty = Form.Validators.notEmpty(error);

        notEmpty.call(field);
        ok(!field.error(), "Error not set?");
    });

    test("notEmpty error", function () {
        var field = new MockField({});
        var error = "Error";
        var notEmpty = Form.Validators.notEmpty(error);

        notEmpty.call(field);
        equal(field.error(), error, "Error set?");
    });

    module("JIRA.Admin.CustomFields.Form.Fields.NamedFieldMixin", {
        setup: function () {
            this.sandbox = sinon.sandbox.create();
            this.Field = MockField.extend({
                mixin: [Form.Fields.NamedFieldMixin]
            });
        },
        teardown: function () {
            this.sandbox.restore();
        }
    });

    test("showRelevantErrors", function () {
        var field = new this.Field({name: "name"});
        var error = "Error";

        ok(field.showRelevantErrors({name: error}), "Shown error?");
        equal(field.error(), error, "Correct error show?");

        ok(!field.showRelevantErrors({other: "Errors"}), "Ignored errors.");
        ok(!field.error(), "Previous error cleared?");
    });

    test("addValueToData", function () {
        var field = new this.Field({name: "name"});
        field.value(20);

        var data = {};
        field.addValueToData(data);
        deepEqual(data, {name: 20}, "Data pulled from field?");

        field.value(undefined);
        data = {};
        field.addValueToData(data);
        deepEqual(data, {}, "Data ignored?");

        field.value(null);
        data = {name: "Random"};
        field.addValueToData(data);
        deepEqual(data, {name: "Random"}, "Data ignored?");
    });

    module("JIRA.Admin.CustomFields.Form.Fields.ValidatorMixin", {
        setup: function () {
            this.sandbox = sinon.sandbox.create();
            this.Field = MockField.extend({
                mixin: [Form.Fields.ValidatorMixin]
            });
        },
        teardown: function () {
            this.sandbox.restore();
        }
    });

    test("No validator", function () {
        var field = new this.Field({name: "name"});
        ok(field.validate(), "No validator always validates?");
    });

    test("With validator", function () {

        var validator = this.sandbox.stub();
        var field = new this.Field({validator: validator});
        validator.returns(false);

        ok(!field.validate(), "Validate method failed when validator failed?");
        ok(validator.calledOnce, "Validator called");
        ok(validator.firstCall.calledOn(field), "Called with field as this?");

        validator.returns(true);

        ok(field.validate(), "Validate method passes when validator failed?");
        ok(validator.calledTwice, "Validator called");
        ok(validator.secondCall.calledOn(field), "Called with field as this?");
    });

    var FormPageObject = function ($el) {
        this.$el = $el;
    };

    _.extend(FormPageObject.prototype, {
        submit: function () {
            this.$el.submit();
        }
    });

    var Field = function ($el) {
        this.$el = $el;
    };

    _.extend(Field.prototype, {
        value: function () {
            if (arguments.length > 0) {
                this.$el.val(arguments[0]);
                return this;
            } else {
                return this.$el.val();
            }
        },
        label: function () {
            return this.$el.siblings().filter("label").text();
        },
        error: function () {
            return this.$el.siblings().filter("div.error").text();
        },
        id: function () {
            return this.$el.attr("id");
        },
        enabled: function () {
            return !this.$el.prop("disabled");
        },
        disable: function () {
            this.$el.prop("disabled", true);
            return this;
        },
        enable: function () {
            this.$el.prop("disabled", false);
            return this;
        },
        isLong: function () {
            return this.$el.hasClass("long-field");
        }
    });

    var TextInputField = function () {
        Field.apply(this, arguments);
    };

    _.extend(TextInputField.prototype, Field.prototype, {
        maxLength: function () {
            return this.$el.attr("maxLength") || 0;
        },
        isTextField: function () {
            return this.$el.is("input") && this.$el.prop("type") === "text";
        }
    });

    var TextAreaField = function () {
        Field.apply(this, arguments);
    };

    _.extend(TextAreaField.prototype, Field.prototype, {
        cols: function () {
            return this.$el.attr("cols") || 0;
        },
        isTextArea: function () {
            return this.$el.is("textarea");
        }
    });

    module("JIRA.Admin.CustomFields.Form", {
        setup: function () {
            this.sandbox = sinon.sandbox.create();
            this.Field = MockField.extend({
                mixin: [Form.Fields.ValidatorMixin, Form.Fields.NamedFieldMixin]
            });
            this.$fixture = jQuery("#qunit-fixture");

            jQuery(document).focus();
        },
        teardown: function () {
            jQuery(document).focus();
            this.sandbox.restore();
        }
    });

    var assertTextStyleFields = function (options) {
        var field1Parser = options.fieldParsers[0];
        var field2Parser = options.fieldParsers[1];
        var field1 = options.formFields[0];
        var field2 = options.formFields[1];
        var validator = options.validator;

        //Check error handling
        field2.setError("Error");
        equal(field2Parser.error(), "Error", "Field has correct error");
        field2.clearError();
        ok(!field2Parser.error(), "Field no longer has error");

        //Check show relevant errors
        var fieldErrors = {field1: "Error", field2: "Error2"};
        var globalErrors = ["global"];
        ok(field2.showRelevantErrors(fieldErrors, globalErrors));
        equal(field2Parser.error(), "Error2", "Field rendered errors.");
        deepEqual(fieldErrors, {field1: "Error"}, "Removed errors that were displayed.");
        deepEqual(globalErrors, ["global"], "Did not touch global errors.");

        fieldErrors = {field1: "Error"};
        ok(!field2.showRelevantErrors(fieldErrors, globalErrors));
        ok(!field2Parser.error(), "Field removed errors.");
        deepEqual(fieldErrors, {field1: "Error"}, "Did not touch field errors.");
        deepEqual(globalErrors, ["global"], "Did not touch global errors.");

        //Check disable/enable handling.
        field2.disable();
        ok(!field2Parser.enabled(), "Disabled the field?");
        field2.enable();
        ok(field2Parser.enabled(), "Enabled the field?");

        //Check value handling.
        ok(!field1.isDirty(), "The field should not be dirty.");
        field1.value("something");
        ok(field1.isDirty(), "The field should be dirty.");
        equal(field1Parser.value(), "something", "Value of the field correctly set.");
        equal(field1.value(), "something", "Value correctly returned.");
        ok(!field1.value('').isDirty(), "The field should no longer be dirty.");

        //Check validate
        ok(field1.validate(), "Field with no validator should always validate");

        ok(!field2.validate(), "Field should delegate to the validator.");
        ok(validator.calledOnce, "Validator called through the field.");
        ok(validator.calledOn(field2), "Validator called with correct scope.");

        //Check value
        ok(field2.value(" something ") === field2, "SetValue returns field.");
        equal(field2Parser.value(), " something ", "Value set correctly.");
        equal(field2.value(), "something", "Value returned correctly.");

        ok(field2.value("  ") === field2, "SetValue returns field.");
        equal(field2Parser.value(), "  ", "Value set correctly.");
        equal(field2.value(), "", "Value returned correctly.");

        //Check addValueToData
        field2Parser.value("");
        var data = {};
        field2.addValueToData(data);
        deepEqual(data, {}, "Empty field does not add data.");

        field2Parser.value("   ");
        field2.addValueToData(data);
        deepEqual(data, {}, "Empty field does not add data.");

        field2Parser.value("sadjads   \t\n");
        field2.addValueToData(data);
        deepEqual(data, {field2: "sadjads"}, "Empty field does not add data.");
    };

    test("text field ", function () {
        var validator = this.sandbox.stub();
        var form = new Form({
            fields: [{
                id: "field1",
                type: "text",
                label: "Field 1"
            }, {
                id: "field2",
                type: "text",
                label: "Field 2",
                maxLength: 255,
                validator: validator,
                size: Form.Fields.FormField.SIZE.large
            }]
        });

        this.$fixture.html(form.render().$el);
        var field1 = new TextInputField(this.$fixture.find("#field1"));
        var field2 = new TextInputField(this.$fixture.find("#field2"));

        //Check starting states.
        equal(field1.label(), "Field 1", "Correct label?");
        equal(field1.id(), "field1", "Correct Id?");
        equal(field1.maxLength(), 0, "Correct maxlength?");
        equal(field1.value(), "", "Field should be empty?");
        ok(field1.enabled(), "Field should be enabled?");
        ok(!field1.isLong(), "Field should not be long?");
        ok(field1.isTextField(), "Should be a text field");

        //Check starting states of second field.
        equal(field2.label(), "Field 2", "Correct label?");
        equal(field2.id(), "field2", "Correct Id?");
        equal(field2.maxLength(), 255, "Correct maxlength?");
        equal(field2.value(), "", "Field should be empty?");
        ok(field2.enabled(), "Field should be enabled?");
        ok(field2.isLong(), "Field should be long?");
        ok(field2.isTextField(), "Should be a text field");

        assertTextStyleFields.call(this, {
            fieldParsers: [field1, field2],
            formFields: [form.fields.field1, form.fields.field2],
            validator: validator
        });
    });

    test("textarea field", function () {
        var validator = this.sandbox.stub();
        var form = new Form({
            fields: [{
                id: "field1",
                type: "textarea",
                label: "Field 1"
            }, {
                id: "field2",
                type: "textarea",
                label: "Field 2",
                cols: 10,
                validator: validator,
                size: Form.Fields.FormField.SIZE.large
            }]
        });

        this.$fixture.html(form.render().$el);
        var field1 = new TextAreaField(this.$fixture.find("#field1"));
        var field2 = new TextAreaField(this.$fixture.find("#field2"));

        //Check starting states.
        equal(field1.label(), "Field 1", "Correct label?");
        equal(field1.id(), "field1", "Correct Id?");
        equal(field1.cols(), 0, "Correct maxlength?");
        equal(field1.value(), "", "Field should be empty?");
        ok(field1.enabled(), "Field should be enabled?");
        ok(!field1.isLong(), "Field should not be long?");
        ok(field1.isTextArea(), "Field is text area");

        //Check starting states of second field.
        equal(field2.label(), "Field 2", "Correct label?");
        equal(field2.id(), "field2", "Correct Id?");
        equal(field2.cols(), 10, "Correct maxlength?");
        equal(field2.value(), "", "Field should be empty?");
        ok(field2.enabled(), "Field should be enabled?");
        ok(field2.isLong(), "Field should be long?");
        ok(field2.isTextArea(), "Field is text area");

        assertTextStyleFields.call(this, {
            fieldParsers: [field1, field2],
            formFields: [form.fields.field1, form.fields.field2],
            validator: validator
        });
    });

    test("append field", function () {
        var validator = this.sandbox.stub();
        var form = new Form({
            fields: [{
                id: "field1",
                type: "textarea",
                label: "Field 1"
            }]
        });

        this.$fixture.html(form.render().$el);

        var result = form.addField({
            id: "field2",
            type: "text",
            label: "Field 2",
            validator: validator,
            size: Form.Fields.FormField.SIZE.large
        });

        ok(result === form, "this returned from form.addField");

        var field1 = new TextAreaField(this.$fixture.find("#field1"));
        var field2 = new TextInputField(this.$fixture.find("#field2"));

        //Check starting states.
        equal(field1.label(), "Field 1", "Correct label?");
        equal(field1.id(), "field1", "Correct Id?");
        equal(field1.cols(), 0, "Correct maxlength?");
        equal(field1.value(), "", "Field should be empty?");
        ok(field1.enabled(), "Field should be enabled?");
        ok(!field1.isLong(), "Field should not be long?");
        ok(field1.isTextArea(), "Field is text area?");

        //Check starting states of second field.
        equal(field2.label(), "Field 2", "Correct label?");
        equal(field2.id(), "field2", "Correct Id?");
        equal(field2.value(), "", "Field should be empty?");
        ok(field2.enabled(), "Field should be enabled?");
        ok(field2.isLong(), "Field should be long?");
        ok(field2.isTextField(), "Field is text input?");

        assertTextStyleFields.call(this, {
            fieldParsers: [field1, field2],
            formFields: [form.fields.field1, form.fields.field2],
            validator: validator
        });
    });

    test("submit on form", function () {
        var callback = this.sandbox.stub();
        var validator = this.sandbox.stub().returns(false);
        var form = new Form({
            fields: [{
                id: "name",
                type: "text",
                label: "Name"
            }, {
                id: "description",
                type: "textarea",
                label: "Description",
                validator: validator
            }]
        });
        this.$fixture.html(form.render().$el);
        var formPageObject = new FormPageObject(this.$fixture.find("form"));
        form.on("submit", callback);

        //Submit should fail unless the form validates.
        formPageObject.submit();
        ok(!callback.called, "Submit not called when form invalid.");

        validator.returns(true);
        formPageObject.submit();
        ok(callback.calledOnce, "Submit called when form valid.");
    });

    test("submit on object", function () {
        var callback = this.sandbox.stub();
        var validator = this.sandbox.stub().returns(false);
        var form = new Form({
            fields: [{
                id: "name",
                type: "text",
                label: "Name"
            }, {
                id: "description",
                type: "textarea",
                label: "Description",
                validator: validator
            }]
        });

        this.$fixture.html(form.render().$el);

        form.on("submit", callback);

        //Submit should fail unless the form validates.
        ok(form.submit() === form, "Form returns this on submit");
        ok(!callback.called, "Submit not called when form invalid.");

        validator.returns(true);
        form.submit();
        ok(callback.calledOnce, "Submit called when form valid.");
    });

    test("validate", function () {
        var validator = this.sandbox.stub().returns(false);
        var form = new Form({
            fields: [{
                id: "name",
                type: "text",
                label: "Name",
                validator: validator
            }, {
                id: "description",
                type: "textarea",
                label: "Description",
                validator: validator
            }]
        });
        this.$fixture.html(form.render().$el);

        var name = new TextAreaField(this.$fixture.find("#name"));

        //Submit should fail unless the form validates.
        ok(!form.validate(), "Validate must fail.");

        name.disable();

        ok(!form.validate(), "Validate must fail.");

        validator.returns(true);
        ok(form.validate(), "Validate must succeed.");
    });

    test("showErrors", function () {
        var form = new Form({
            fields: [{
                id: "name",
                type: "text",
                label: "Name"
            }, {
                id: "description",
                type: "textarea",
                label: "Description"
            }]
        });

        var globalErrors = ["Global Errors"];
        var fieldErrors = {};

        this.$fixture.html(form.render().$el);

        var name = new TextAreaField(this.$fixture.find("#name"));
        var description = new TextInputField(this.$fixture.find("#description"));

        //No errors should not change anything.
        ok(!form.showErrors(fieldErrors, globalErrors), "No errors rendered returns false.");

        //Error on name and description.
        fieldErrors = {name: "Name", description: "Description", other: "Other"};
        var fieldErrorsClone = _.clone(fieldErrors);
        var globalErrorsClone = _.clone(globalErrors);

        ok(form.showErrors(fieldErrorsClone, globalErrorsClone), "Some errors returns true.");
        deepEqual(fieldErrorsClone, {other: "Other"}, "Removed errors not displayed.");
        deepEqual(globalErrorsClone, globalErrors, "Didn't touch global errors.");
        equal(name.error(), "Name", "Name field has error");
        equal(description.error(), "Description", "Description field has error");

        //Error on description only.
        fieldErrors = {description: "Description"};
        fieldErrorsClone = _.clone(fieldErrors);
        globalErrorsClone = _.clone(globalErrorsClone);

        ok(form.showErrors(fieldErrorsClone, globalErrorsClone), "Some errors returns true.");
        deepEqual(fieldErrorsClone, {}, "Removed errors not displayed.");
        deepEqual(globalErrorsClone, globalErrors, "Didn't touch global errors.");
        ok(!name.error(), "Name field error should have been removed.");
        equal(description.error(), "Description", "Description field has error");

        //Error on name and description but name disabled.
        fieldErrors = {description: "Description", name: "Name"};
        fieldErrorsClone = _.clone(fieldErrors);
        globalErrorsClone = _.clone(globalErrorsClone);

        form.blur();
        name.disable();

        ok(form.showErrors(fieldErrorsClone, globalErrorsClone), "Some errors returns true.");
        deepEqual(fieldErrorsClone, {}, "Removed errors not displayed.");
        deepEqual(globalErrorsClone, globalErrors, "Didn't touch global errors.");
        equal(name.error(), "Name", "Name field error should be displayed");
        equal(description.error(), "Description", "Description field has error");

        form.blur();
    });

    test("enable & disable", function () {
        var form = new Form({
            fields: [{
                id: "name",
                type: "text",
                label: "Name"
            }, {
                id: "description",
                type: "textarea",
                label: "Description"
            }]
        });

        this.$fixture.html(form.render().$el);

        var name = new TextAreaField(this.$fixture.find("#name"));
        var description = new TextInputField(this.$fixture.find("#description"));

        ok(name.enabled(), "The name is enabled.");
        ok(description.enabled(), "The description is enabled.");
        ok(form.disable() === form, "Form returned from disable.");
        ok(!name.enabled(), "The name is disabled.");
        ok(!description.enabled(), "The description is disabled.");
        ok(form.enable() === form, "Form returned from enable.");
        ok(name.enabled(), "The name is enabled.");
        ok(description.enabled(), "The description is enabled.");
    });

    test("toJson", function () {
        var form = new Form({
            fields: [{
                id: "name",
                type: "text",
                label: "Name"
            }, {
                id: "description",
                type: "textarea",
                label: "Description"
            }]
        });

        this.$fixture.html(form.render().$el);

        var name = new TextAreaField(this.$fixture.find("#name"));
        var description = new TextInputField(this.$fixture.find("#description"));
        name.value("name");
        description.value("description");

        deepEqual(form.toJSON(), {name: "name", description: "description"}, "Form correctly parsed.");

        name.value("       ");
        description.value("description");

        deepEqual(form.toJSON(), {description: "description"}, "Form correctly parsed.");
    });

    test("isDirty", function () {
        var form = new Form({
            fields: [{
                id: "name",
                type: "text",
                label: "Name"
            }, {
                id: "description",
                type: "textarea",
                label: "Description"
            }]
        });

        this.$fixture.html(form.render().$el);

        var name = new TextAreaField(this.$fixture.find("#name"));
        var description = new TextInputField(this.$fixture.find("#description"));

        ok(!form.isDirty(), "Should not be dirty.");
        name.value("name");
        ok(form.isDirty(), "Should be dirty.");
        name.value("");
        ok(!form.isDirty(), "Should not be dirty.");
        description.value("name");
        ok(form.isDirty(), "Should be dirty.");
        name.value("name");
        ok(form.isDirty(), "Should be dirty.");
        name.value("");
        description.value("");
        ok(!form.isDirty(), "Should not be dirty.");
    });

    test("Add Field With Content", function () {
        var form = new Form({
            fields: [{
                content: function () {
                    return jQuery("<div>").attr("id", "test-div");
                }
            }, {
                id: "description",
                type: "textarea",
                label: "Description"
            }]
        });

        this.$fixture.html(form.render().$el);

        ok(this.$fixture.find("#test-div").length, "Added element with content.");
    });
});
