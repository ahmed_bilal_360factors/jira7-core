define("jira-project-config/custom-fields/steps/type", [
    "jira/jquery/deferred",
    "jira-project-config/custom-fields/steps/type/models/types",
    "jira-project-config/custom-fields/steps/create-field",
    "jira-project-config/custom-fields/steps/helper",
    "jira-project-config/custom-fields/wizard/abstract-step",
    "jira-project-config/custom-fields/error-dialog",
    "jira-project-config/custom-fields/steps/type/views/text",
    "jira-project-config/custom-fields/steps/type/views/type-choice",
    "jira-project-config/custom-fields/views/shared-by",
    "jira-project-config/custom-fields/wizard/fail-reasons",
    "underscore"
], function(
    Deferred,
    TypesModel,
    CreateFieldStep,
    CustomFieldsStepHelper,
    AbstractStep,
    ErrorDialog,
    TypeChoiceTexts,
    TypeChoiceView,
    SharedByView,
    FailReasons,
    _) {
    "use strict";

    /**
     * A step in the wizard.
     *
     * @param {string} [options.initialCategory] The key for the initial category.
     * @param {IssueCustomFields} options.api The REST API.
     * @constructor
     */
    var TypeStep = function (options) {
        this.options = _.extend({
            initialCategory: "STANDARD",
            view: TypeChoiceView
        }, options);

        this.api = options.api;
        this.inIssueContext = !!options.inIssueContext;

        this.categories = null;
        this.stepData = {};
    };
    _.extend(TypeStep.prototype, AbstractStep, {
        showLoading: function () {
            this.view && this.view.showLoading();
        },

        hideLoading: function () {
            this.view && this.view.hideLoading();
        },

        /**
         * Shows a dialog that can be used select a custom field type for creating. Passed the type selected and
         * the passed field info to the next step.
         *
         * @param {Object} field will be passed along to the next step
         */
        start: function () {
            var self = this;
            var promise = this._getCategories();
            var view;
            promise.done(function (categories, sharedBy) {
                var sharedView = sharedBy && sharedBy.length && new SharedByView({projects: sharedBy});
                view = self.view = new self.options.view(categories, 200, sharedView, self.inIssueContext);
                self.listenTo(view, "submit", function (model) {
                    self.stepData.type = self._getUpdateData(model);
                    self.fireNext();
                });
                self.listenTo(view, "previous", function () {
                    self.firePrevious();
                });

                self.listenTo(view, "cancel", function (check) {
                    self.fireCancel(check);
                });

            });
            promise.fail(function (reason, error) {
                if (FailReasons.abort === reason) {
                    self.fireCancel();
                } else {
                    view = self.view = new ErrorDialog({
                        heading: TypeChoiceTexts.heading,
                        message: error
                    });
                    self.listenTo(view, "cancel", self.fireCancel);
                }
            });
            return promise;
        },

        /**
         * Restarts the step. Nothing to do for this step, so just return a resolved promise.
         * @returns {jQuery.Promise} a resolved promise
         */
        restart: function () {
            return Deferred().resolve().promise();
        },

        pauseStep: function () {
            this.view && this.view.hide();
        },

        stopStep: function () {
            if (this.view) {
                this.stopListening(this.view);
                this.view.destroy();
                delete this.view;
            }
        },

        show: function () {
            this.view && this.view.show();
        },

        getStepData: function () {
            return this.stepData;
        },

        getNextStep: function (proposedNextStep, sharedStepOptions) {
            if (proposedNextStep instanceof CreateFieldStep) {
                return proposedNextStep;
            }
            return new CreateFieldStep({api: sharedStepOptions.api, inIssueContext: sharedStepOptions.inIssueContext});
        },

        _getUpdateData: function (model) {
            var data = {};
            if (model) {
                data.key = model.get('key');
                data.name = model.get('name');
                data.options = model.get('options');
                data.cascading = model.get('cascading');
                var searchers = model.get('searchers');
                if (searchers && searchers.length > 0) {
                    data.searcherKey = searchers[0];
                }
            }
            return data;
        },

        _getCategories: function () {
            var deferred = Deferred();
            var instance = this;
            var getting = this.api.getSharedBy(this.api.getCustomFieldTypes());
            getting.done(function (data) {
                var categories = [];
                _.each(data.categories, function (cat) {
                    var matchedTypes = _.filter(data.types, function (type) {
                        return _.contains(type.categories, cat.id);
                    });
                    var types = new TypesModel(matchedTypes, {parse: true});
                    categories.push({
                        initial: cat.id === instance.options.initialCategory,
                        id: cat.id,
                        name: cat.name,
                        types: types
                    });
                });
                deferred.resolveWith(instance, [categories, data.sharedBy]);
            });
            getting.fail(_.bind(this.genericFailHandler, this, deferred));
            getting.progress(_.bind(this.genericProgressHandler, this, deferred));
            return deferred.promise();
        }
    }, CustomFieldsStepHelper);


    return TypeStep;
});

define("jira-project-config/custom-fields/steps/type/models/type", ["jira-project-config/backbone", "underscore"], function(Backbone, _) {

    /**
     * A custom field type within JIRA.
     *
     * @param {string} attributes.key A unique identifier
     * @param {string} attributes.name Human readable name (defaults to *key* if not provided).
     * @param {string} attributes.description Human readable description
     * @param {string} [attributes.previewImageUrl] A URL to a preview of the image.
     */
    return Backbone.Model.extend({
        initialize: function () {
            this.get("name") || this.set("name", this.get("key"));
        },

        parse: function (data) {
            return _.omit(data, "categories");
        }
    });
});

define("jira-project-config/custom-fields/steps/type/models/types", [
    "jira-project-config/custom-fields/filtered-collection",
    "jira-project-config/custom-fields/steps/type/models/type",
    "jira-project-config/custom-fields/util",
    "underscore"
], function(FilteredCollection, TypeModel, Util, _) {

    /**
     * Collection of {@link JIRA.Admin.CustomFields.TypeStep.Models.Type}
     */
    return FilteredCollection.extend({
        model: TypeModel,
        comparator: Util.lowerCaseLocaleComparator("name"),
        matcher: function (model, query, state) {
            var haystack = model.get("name").toLowerCase();
            var needles = state.needles;
            if (!needles) {
                state.needles = needles = _.filter(query.toLowerCase().split(/\s+/), function (needle) {
                    return needle;
                });
            }
            return _.all(needles, function (needle) {
                return haystack.indexOf(needle) !== -1;
            });
        }
    });
});

AJS.namespace("JIRA.Admin.CustomFields.TypeStep", null, require("jira-project-config/custom-fields/steps/type"));

AJS.namespace("JIRA.Admin.CustomFields.TypeStep.Models.Type", null, require("jira-project-config/custom-fields/steps/type/models/type"));
AJS.namespace("JIRA.Admin.CustomFields.TypeStep.Models.Types", null, require("jira-project-config/custom-fields/steps/type/models/types"));

//defining globals from other files, so that they won't get overwritten by this module
AJS.namespace("JIRA.Admin.CustomFields.TypeStep.Views.Text", null, require("jira-project-config/custom-fields/steps/type/views/text"));
AJS.namespace("JIRA.Admin.CustomFields.TypeStep.Views.TypeChoice", null, require("jira-project-config/custom-fields/steps/type/views/type-choice"));
