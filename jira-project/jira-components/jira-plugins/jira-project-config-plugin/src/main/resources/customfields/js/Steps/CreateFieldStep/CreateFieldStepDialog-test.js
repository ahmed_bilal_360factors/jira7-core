AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:custom-fields",
    "com.atlassian.jira.jira-project-config-plugin:custom-fields-impl"], function () {

    var $ = require("jquery");
    var Marionette = require("jira-project-config/marionette");
    var formatter = require("jira/util/formatter");
    var _ = require("underscore");
    var OptionsField = require("jira-project-config/custom-fields/views/forms/options-field");
    var Backbone = require("jira-project-config/backbone");
    var CreateFieldStep = require("jira-project-config/custom-fields/steps/create-field");
    var CreateDialog = require("jira-project-config/custom-fields/steps/create-field/views/create-dialog");
    var CreatePanel = require("jira-project-config/custom-fields/steps/create-field/views/create-panel");

    module("JIRA.Admin.CustomFields.CreateFieldStep.Views.CreateDialog", {
        setup: function () {
            var sandbox = this.sandbox = sinon.sandbox.create();
            var panel = this.panel = new MockPanel(sandbox);

            this.dialog = new CreateDialog({
                panel: function () {
                    return panel;
                },
                typeName: "Name",
                fieldName: ""
            });
        },
        teardown: function () {
            this.dialog.destroy();
            this.sandbox.restore();
        }
    });

    var MockPanel = Marionette.ItemView.extend({
        initialize: function (sandbox) {
            this.getData = sandbox.stub();
            this.enable = sandbox.stub();
            this.disable = sandbox.stub();
            this.showErrors = sandbox.stub();
            this.focus = sandbox.stub();
            this.isDirty = sandbox.stub();
        },
        template: function () {
            return $("<div>");
        },
        triggerSubmit: function () {
            this.triggerMethod("submit");
        }
    });

    var CreateFieldPageObject = function () {
        this.$el = $("#customfields-configure-field");
    };

    _.extend(CreateFieldPageObject.prototype, {
        submit: function () {
            this.panel.submit();
            return this;
        },
        _getNext: function () {
            return this.$el.find("#customfields-configure-next");
        },
        next: function () {
            this._getNext().click();
            return this;
        },
        nextText: function () {
            return this._getNext().text();
        },
        _getPrevious: function () {
            return this.$el.find("#customfields-configure-previous");
        },
        previous: function () {
            this._getPrevious().click();
        },
        cancel: function () {
            this.$el.find("a:contains('common.forms.cancel')").click();
        },
        isDestroyed: function () {
            return !this.$el.parent().length;
        },
        title: function () {
            return this.$el.find(":header").text();
        },
        globalError: function () {
            return this.$el.find("div.error.aui-message").text();
        },
        esc: function () {
            var e = $.Event("keydown", {keyCode: $.ui.keyCode.ESCAPE});
            $(document).trigger(e);
        }
    });

    test("Test Dialog Title", function () {
        this.sandbox.spy(formatter, "format");
        this.dialog.show();
        var pageObject = new CreateFieldPageObject();
        sinon.assert.calledWith(formatter.format, "admin.project.customfieldadd.title", "Name");
        equal(pageObject.title(), "admin.project.customfieldadd.title", "Title correctly set.");
        this.dialog.hide();
    });

    test("Test Submit form", function () {
        var submit = this.sandbox.stub();
        var data = {};

        this.dialog.on("submit", submit);
        this.dialog.show();
        this.panel.getData.returns(data);

        this.panel.triggerSubmit();

        ok(submit.firstCall.calledWith(data), "Submit on form triggers submit.");
    });

    test("Test Submit next", function () {
        var submit = this.sandbox.stub();
        var data = {};

        this.dialog.on("submit", submit);
        this.dialog.show();
        this.panel.getData.returns(data);

        var pageObject = new CreateFieldPageObject();
        pageObject.next();

        ok(submit.firstCall.calledWith(data), "Submit on form triggers submit.");
    });

    var assertClose = function (func, confirm) {
        this.dialog.show();

        var cancel = this.sandbox.stub();
        this.dialog.on("cancel", cancel);

        var pageObject = new CreateFieldPageObject();
        func.call(pageObject);
        ok(cancel.calledOnce, "Cancel triggered.");
        deepEqual(cancel.firstCall.args, [confirm], "Cancel triggered with right arguments.");
    };

    test("Cancel triggers cancel event", function () {
        assertClose.call(this, CreateFieldPageObject.prototype.cancel, false);
    });

    test("ESC triggers cancel event", function () {
        assertClose.call(this, CreateFieldPageObject.prototype.esc, true);
    });

    test("Show errors", function () {

        this.dialog.show();

        var pageObject = new CreateFieldPageObject();
        var fieldErrors = {name: "NameError", description: "DescriptionError", searcher: "SearcherError"};
        var generalErrors = ["Global"];

        this.panel.showErrors = function (field, general) {
            deepEqual(field, fieldErrors, "Panel passed field errors.");
            deepEqual(general, generalErrors, "Panel passed general errors");

            var result = false;
            if (field.name) {
                delete field.name;
                result = true;
            }

            if (field.description) {
                delete field.description;
                result = true;
            }
            return result;
        };

        this.dialog.showErrors(_.clone(fieldErrors), _.clone(generalErrors));
        equal(pageObject.globalError(), "Global SearcherError", "Global error merged with field errors that can't be displayed.");

        fieldErrors = {name: "NameError2"};
        generalErrors = [];

        this.dialog.showErrors(_.clone(fieldErrors), _.clone(generalErrors));
        ok(!pageObject.globalError(), "Cleared global error");

        ok(!this.panel.focus.called, "Focus should not have been called while errors on the form.");

        fieldErrors = {};
        generalErrors = ["Global"];
        this.dialog.showErrors(_.clone(fieldErrors), _.clone(generalErrors));

        equal(pageObject.globalError(), "Global", "Global error displayed.");
        ok(this.panel.focus.calledOnce, "Focus the form when only global errors.");
    });

    test("enable/disable", function () {

        this.dialog.show();
        var pageObject = new CreateFieldPageObject();

        this.dialog.disable();

        ok(this.panel.disable.calledOnce, "Disable disables the form.");

        pageObject.next();
        pageObject.previous();
        pageObject.cancel();
        pageObject.esc();
        this.panel.triggerSubmit();
        ok(!pageObject.isDestroyed(), "Should not be able to close the dialog by any means when disabled.");
        ok(this.panel.disable.calledOnce, "Disable still called once.");

        this.dialog.enable();

        ok(this.panel.enable.calledOnce, "Enable enables the form.");
    });

    test("isDirty", function () {
        this.dialog.show();

        this.panel.isDirty.returns(true);
        ok(this.dialog.isDirty(), "Should not be dirty.");
        ok(this.panel.isDirty.calledOnce, "IsDirty should delegate to the panel.");
    });

    module("JIRA.Admin.CustomFields.CreateFieldStep.Views.CreatePanel", {
        setup: function () {
            this.sandbox = sinon.sandbox.create();
            CreateFieldStep.Views = CreateFieldStep.Views;
            this.form = _.extend(Backbone.Events, {
                toJSON: this.sandbox.stub(),
                enable: this.sandbox.stub(),
                disable: this.sandbox.stub(),
                focus: this.sandbox.stub(),
                showErrors: this.sandbox.stub(),
                isDirty: this.sandbox.stub()
            });
            this.panel = new CreatePanel({
                form: this.form
            });
        },
        teardown: function () {
            this.sandbox.restore();
        }
    });

    test("submit on form triggers submit on panel", function () {
        var submit = this.sandbox.stub();
        this.panel.on("submit", submit);

        this.form.trigger("submit");
        ok(submit.calledOnce, "Submit on form triggered submit on panel.");
    });


    test("getData", function () {
        var data = {};
        this.form.toJSON.returns(data);

        strictEqual(this.panel.getData(), data, "Data returned from from");
    });

    test("enable/disable", function () {

        this.panel.disable();
        ok(this.form.disable.calledOnce, "Form should now be disabled.");
        this.panel.enable();
        ok(this.form.enable.calledOnce, "Form should now be enabled.");
    });

    test(".focus", function () {
        this.panel.focus();
        ok(this.form.focus.calledOnce, "Form should now be disabled.");
    });

    test("showErrors", function () {
        var errors = {};
        var globalErrors = [];
        this.panel.showErrors(errors, globalErrors);
        ok(this.form.showErrors.firstCall.calledWith(errors, globalErrors), "Asking form to render errors.");
    });

    test("_createFormParams", function () {
        var options = {};
        var data = this.panel._createFormParams(options);
        var fields = data.fields;
        equal(fields.length, 3, "Three fields generated.");

        var name = fields[1];
        deepEqual(_.pick(name, "id", "label", "type", "maxLength", "required", "name"),
            {
                id: "custom-field-name",
                label: "common.concepts.name",
                name: "name",
                type: "text",
                maxLength: 255,
                required: true
            },
            "Name field has correct parameters");

        ok(name.validator, "Validator supplied.");

        var description = fields[2];
        deepEqual(_.pick(description, "id", "label", "type", "rows"),
            {id: "custom-field-description", label: "common.concepts.description", type: "textarea", rows: 2},
            "Description field has correct parameters.");
    });

    test("_createFormParams with options", function () {
        var options = {configureOptions: true};
        var data = this.panel._createFormParams(options);
        var fields = data.fields;
        equal(fields.length, 4, "Four fields generated.");

        var name = fields[1];
        deepEqual(_.pick(name, "id", "label", "type", "maxLength", "required", "name"),
            {
                id: "custom-field-name",
                label: "common.concepts.name",
                name: "name",
                type: "text",
                maxLength: 255,
                required: true
            },
            "Name field has correct parameters");

        ok(name.validator, "Validator supplied.");

        var description = fields[2];
        deepEqual(_.pick(description, "id", "label", "type", "rows"),
            {id: "custom-field-description", label: "common.concepts.description", type: "textarea", rows: 2},
            "Description field has correct parameters.");

        var configOptions = fields[3];
        equal("admin.common.words.options", configOptions.label, "Label correctly configured.");
        ok(configOptions.field instanceof OptionsField, "Options field correctly added.");
    });

    test("isDirty with options", function () {
        this.form.isDirty.returns(false);

        ok(!this.panel.isDirty(), "Should not be dirty.");
        ok(this.form.isDirty.calledOnce, "isDirty should delegate to the form.");
    });
});
