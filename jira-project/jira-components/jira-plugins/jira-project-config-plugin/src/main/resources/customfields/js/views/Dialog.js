define("jira-project-config/custom-fields/views/dialog", [
    'jira-project-config/templates/messages',
    'jira-project-config/custom-fields/templates',
    'jira/util/navigator',
    'jira/util/formatter',
    "jira-project-config/custom-fields/views/dialog/panel",
    "jira-project-config/custom-fields/views/dialog/button",
    "jira-project-config/custom-fields/views/dialog/notification",
    "aui/dialog",
    "jira-project-config/backbone",
    "jira-project-config/marionette",
    "underscore",
    "jquery"
], function(
    TemplateMessages,
    CustomFieldTemplates,
    Navigator,
    formatter,
    Panel,
    Button,
    Notification,
    AuiDialog,
    Backbone,
    Marionette,
    _,
    $) {

    /**
     * @constructor
     */
    var Dialog = function (options) {
        options || (options = {});

        _.defaults(options, {
            width: 840,
            height: 530
        });

        options.keypressListener = _.bind(this.onKeydown, this);

        this._dialog = new AuiDialog(options);
        this._page = this._dialog.getPage(0);
        this._currentPanel = null;
        this._notification = null;
        this._isShown = false;
        this.$el = this._dialog.popup.element;

        this._page.header = CustomFieldTemplates.dialogHeader();
        this.$el.prepend(this._page.header);
        this.$el.addClass("jira-customfields-wizard");
    };

    _.extend(Dialog.prototype, Backbone.Events, {
        _recalcPageSize: function () {
            if (this.$notification) {
                var height = null;
                for (var i = 0; i < this._page.panel.length; i++) {
                    var panel = this._page.panel[i];
                    if (!panel.isSizeRecalculated) {
                        height = height || panel.body.outerHeight() - this.$notification.outerHeight();
                        var body = panel.body;
                        body.css("height", height);
                        panel.isSizeRecalculated = true;
                        if (!this._page.menu.isSizeRecalculated) {
                            this._page.menu.css("height", height);
                            this._page.menu.isSizeRecalculated = true;
                        }
                    }
                }
            }
            return this;
        },
        onKeydown: function (event) {
            if (event.keyCode === $.ui.keyCode.ESCAPE) {
                this.trigger("close");
            }
        },

        triggerMethod: Marionette.triggerMethod,

        /**
         * Returns the HTML node for the last button added to the page.
         *
         * @returns {jQuery}
         */
        _lastButtonElement: function () {
            var button = _.last(this._page.button);
            return button.item;
        },
        _setNotification: function (html, view) {
            if (this._notification && this._isShown) {
                this._notification._triggerHide();
            }

            if (!this.$notification) {
                this.$notification = $(CustomFieldTemplates.dialogNotification());
                this.$notification.append(html);
                this.$el.find(".dialog-components").prepend(this.$notification);
            } else {
                this.$notification.html(html);
            }
            this._recalcPageSize();
            this._notification = new Notification(view);

            if (this._isShown) {
                this._notification._triggerShow();
            }
            return this;
        },
        setWarningNotification: function (view) {
            var el = $(CustomFieldTemplates.dialogNotificationWarning());
            el.find(".jira-dialog-notification-content").html(view.render().$el);
            return this._setNotification(el, view);
        },
        addHeaderMain: function (html) {
            this.$el.find(".jira-dialog-title-main").append(html);
            this._page.recalcSize();
            return this;
        },

        addOrSetHeaderMainText: function (text) {
            var headerMain = this.$el.find(".jira-dialog-title-main .jira-dialog-title-text");
            if (headerMain.length > 0) {
                headerMain.text(text);
                return this;
            } else {
                return this.addHeaderMain($("<h2 class='jira-dialog-title-text'/>").text(text));
            }
        },


        addHeaderAction: function (view) {
            var $actions = this.$el.find(".jira-dialog-title-actions");
            $actions.append(view.render().$el);
            this._page.recalcSize();
            return this;
        },
        showLoading: function () {
            if (!this.$loading) {
                var $before = this.$el.find('.dialog-button-panel button').eq(0);
                this.$loading = $("<span>").addClass("icon dialog-spinner");
                this.$loading.insertBefore($before);
            }
            this.$loading.spin();
        },
        hideLoading: function () {
            if (this.$loading) {
                this.$loading.spin(false);
            }
        },
        /**
         * Adds an AUI button to the dialog.
         *
         * @param {string} options.label The text displayed on the button.
         * @param {string} options.id The ID value for the button's DOM element.
         * @param {function} [options.click] A callback function for when the button is clicked.
         * @returns {Button}
         */
        addButton: function (options) {
            var buttonClass = options.secondary ? " aui-button-secondary" : " aui-button-primary";
            this._dialog.addButton(options.label, options.click, "aui-button" + buttonClass);
            // Remove the default class to allow AUI button styling to take effect.
            var element = this._lastButtonElement();
            element.removeClass("button-panel-button");
            options.id && element.attr("id", options.id);

            if (options.submitAccessKey) {
                this._addSubmitKey(element);
            }

            return new Button(element);
        },

        addLeftLink: function (options) {
            var element;

            this._dialog.addLink(options.label, options.click, "secondary", options.url);
            element = this._lastButtonElement();
            // Remove the default class to allow AUI button styling to take effect.
            element = element.removeClass("button-panel-button");

            if (options.iconClass) {
                $('<span class="icon"/>').addClass(options.iconClass).prependTo(element);
            }

            return this;
        },

        addLink: function (options) {
            this._dialog.addLink(options.label, options.click);
            var element = this._lastButtonElement();
            if (options.cancelAccessKey) {
                this._addCancelKey(element);
            }
            return this;
        },

        /**
         * Add a panel to the dialog.
         *
         * @param name
         * @param view
         * @returns {Dialog.Panel}
         */
        addPanel: function (name, view) {
            this._dialog.addPanel(name, view.render().el);
            var dialogPanel = this._page.getCurrentPanel();
            var panel = new Panel(this, name, view, dialogPanel);
            dialogPanel.button.click(_.bind(function () {
                this._onShowPanel(panel);
            }, this));
            this._onShowPanel(panel);
            return panel;
        },

        show: function () {
            if (!this._isShown) {
                this._dialog.show();
                this._recalcPageSize();
                this._isShown = true;
                this._currentPanel && this._currentPanel._triggerShow();
                this._notification && this._notification._triggerShow();
            }
            return this;
        },
        hide: function () {
            this.hideLoading();
            if (this._isShown) {
                this._currentPanel && this._currentPanel._triggerHide();
                this._notification && this._notification._triggerHide();
                this._dialog && this._dialog.hide();
                this._isShown = false;
            }
        },
        destroy: function () {
            this.hide();
            this._dialog && this._dialog.remove();
            delete this._dialog;
        },
        _addSubmitKey: function (el) {
            var key = formatter.I18n.getText('AUI.form.submit.button.accesskey');
            el.prop("accessKey", key);
            el.attr("title", formatter.I18n.getText('AUI.form.submit.button.tooltip', key, Navigator.modifierKey()));

            return this;
        },
        _addCancelKey: function (el) {
            var key = formatter.I18n.getText('AUI.form.cancel.link.accesskey');
            el.prop("accessKey", key);
            el.attr("title", formatter.I18n.getText('AUI.form.cancel.link.tooltip', key, Navigator.modifierKey()));

            return this;
        },
        _onShowPanel: function (panel) {
            if (!this._isShown) {
                this._currentPanel = panel;
                return;
            }

            if (this._currentPanel) {
                if (this._currentPanel !== panel) {
                    this._currentPanel._triggerHide();

                    this._currentPanel = panel;
                    this._currentPanel._triggerShow();
                }
            } else {
                this._currentPanel = panel;
                this._currentPanel._triggerShow();
            }
        },
        currentPanel: function () {
            return this._currentPanel;
        },
        showErrors: function (errors) {
            if (errors && errors.length) {
                var newError = $(TemplateMessages.errorMessage({message: errors.join(' ')}));
                if (this.$error) {
                    this.$error.replaceWith(newError);
                } else {
                    var $before = this.$el.find(".dialog-panel-body").filter(":visible");
                    var $form = $before.find("form").filter(":first");
                    if ($form.length) {
                        $before = $form;
                    }
                    $before.prepend(newError);
                }
                this.$error = newError;
            } else if (this.$error) {
                this.$error.remove();
                delete this.$error;
            }
            return this;
        },
        hideErrors: function () {
            this.$el.find(".dialog-panel-body .error").hide();
        }
    });
    return Dialog;
});

define("jira-project-config/custom-fields/views/dialog/panel", [
    'underscore',
    "jira-project-config/marionette",
    "jira-project-config/backbone"
], function(_, Marionette, Backbone) {
    var Panel = function (dialog, name, view, panel) {
        this.dialog = dialog;
        this.name = name;
        this.view = view;
        this.panel = panel;
    };

    _.extend(Panel.prototype, Backbone.Events, {
        _triggerHide: function () {
            return this.triggerMethod("dialog:panel:hide");
        },
        _triggerShow: function () {
            return this.triggerMethod("dialog:panel:show");
        },
        triggerMethod: function (event) {
            Marionette.triggerMethod.call(this, event);
            this.view.triggerMethod(event);
            return this;
        },
        show: function () {
            this.panel.select();
            return this;
        }
    });

    return Panel;
});


define(
    "jira-project-config/custom-fields/views/dialog/button",
    ['underscore'],
    function(_) {
        /**
         * A button on the dialog.
         *
         * @param {jQuery} element The jQuery wrapped DOM element used to present the button.
         * @constructor
         */
        var Button = function (element) {
            this.element = element;
        };

        _.extend(Button.prototype, {
            /**
             * Disable user interaction with the button.
             */
            disable: function () {
                this.element.prop("disabled", true);
                return this;
            },

            /**
             * Enable user interaction with the button.
             */
            enable: function () {
                this.element.prop("disabled", false);
                return this;
            },

            click: function () {
                this.element.click.apply(this.element, arguments);
                return this;
            },

            /**
             * Change the label text.
             *
             * @param {string} text
             */
            setLabel: function (text) {
                this.element.text(text);
                return this;
            }
        });

        return Button;
    }
);


define(
    "jira-project-config/custom-fields/views/dialog/notification",
    ['underscore'],
    function(_) {
        var Notification = function (view) {
            this.view = view;
        };

        _.extend(Notification.prototype, {
            _triggerHide: function () {
                return this.triggerMethod("dialog:notification:hide");
            },
            _triggerShow: function () {
                return this.triggerMethod("dialog:notification:show");
            },
            triggerMethod: function (event) {
                this.view.triggerMethod(event);
                return this;
            }
        });

        return Notification;
    }
);

AJS.namespace("JIRA.Admin.CustomFields.Dialog", null, require("jira-project-config/custom-fields/views/dialog"));

AJS.namespace("JIRA.Admin.CustomFields.Dialog.Panel", null, require("jira-project-config/custom-fields/views/dialog/panel"));

AJS.namespace("JIRA.Admin.CustomFields.Dialog.Button", null, require("jira-project-config/custom-fields/views/dialog/button"));
