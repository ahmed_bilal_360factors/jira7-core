define("jira-project-config/custom-fields/steps/create-field/views/create-dialog", [
    'jira/util/formatter',
    "jira-project-config/custom-fields/steps/create-field/views/create-panel",
    "jira-project-config/custom-fields/views/dialog",
    "jira-project-config/backbone",
    "underscore"
], function(formatter, CreatePanel, Dialog, Backbone, _) {
    /**
     * A dialog that allows field options to be specified.
     *
     * @param {string} [options.nextLabel] The label for the 'Next' button.
     * @param {boolean} options.configureOptions If true, the field is a <select>ish and requires the user to be able to
     *     add items as options.
     * @param {string} [options.fieldName] The default name for the field.
     * @constructor
     */
    var CreateDialogView = function (options) {
        this.options = _.defaults({}, options, {
            previousLabel: formatter.I18n.getText("common.forms.previous"),
            nextLabel: formatter.I18n.getText("common.forms.create"),
            cancelLabel: formatter.I18n.getText("common.forms.cancel"),
            panel: CreatePanel,
            fieldName: ""
        });
        this.enabled = true;
    };

    _.extend(CreateDialogView.prototype, Backbone.Events, {
        initialize: function (options) {
            this.options = options;
        },
        show: function () {
            if (!this.dialog) {
                var dialog = this.dialog = new Dialog({
                    id: "customfields-configure-field",
                    focusSelector: "#custom-field-name"
                });

                dialog.addOrSetHeaderMainText(formatter.I18n.getText("admin.project.customfieldadd.title", this.options.typeName));

                var panelOptions = _.defaults({}, _.pick(this.options, "configureOptions"), {
                    fieldName: this.options.fieldName
                });

                var panel = this.panel = dialog.addPanel("", new this.options.panel(panelOptions));
                this.listenTo(panel.view, "submit", _.bind(this._submit, this));
                this.previousButton = dialog.addButton({
                    secondary: true,
                    label: this.options.previousLabel,
                    id: "customfields-configure-previous",
                    click: _.bind(this._previous, this)
                });
                this.nextButton = dialog.addButton({
                    label: this.options.nextLabel,
                    id: "customfields-configure-next",
                    submitAccessKey: true,
                    click: function () {
                        //We trigger the form so that dirty form warning does not display. It expects the submit
                        //to come from the form and not directly.
                        panel.view.triggerSubmit();
                    }
                });

                dialog.addLink({
                    label: this.options.cancelLabel,
                    click: _.bind(this._cancel, this, false),
                    cancelAccessKey: true
                });
                this.listenTo(dialog, "close", _.bind(this._cancel, this, true));

            } else {
                this.dialog.hideErrors();
            }
            this.dialog.show();
            return this;
        },

        updateData: function (data) {
            this.dialog.addOrSetHeaderMainText(formatter.I18n.getText("admin.project.customfieldadd.title", data.typeName));
            this.panel.view.updateData(data);
        },

        _previous: function () {
            this.trigger("previous");
        },
        _submit: function () {
            var view = this.panel.view;
            if (this.enabled) {
                this.trigger("submit", view.getData());
            }
        },
        hide: function () {
            this.dialog && this.dialog.hide();
        },
        destroy: function () {
            this.stopListening(this.panel);
            if (this.dialog) {
                this.dialog.destroy();
                delete this.dialog;
            }
            return this;
        },
        showErrors: function (fieldErrors, generalErrors) {
            //No error to show, then lets just focus the panel.
            if (!this.panel.view.showErrors(fieldErrors, generalErrors)) {
                this.panel.view.focus();
            }
            var allErrors = _.union(generalErrors, _.values(fieldErrors));
            this.dialog.showErrors(allErrors);
        },
        showLoading: function () {
            this.dialog && this.dialog.showLoading();
        },
        hideLoading: function () {
            this.dialog && this.dialog.hideLoading();
        },
        disable: function () {
            this.enabled = false;
            this.panel.view.disable();
            this.nextButton.disable();
            this.previousButton.disable();
        },
        enable: function () {
            this.enabled = true;
            this.panel.view.enable();
            this.nextButton.enable();
            this.previousButton.enable();
        },
        isDirty: function () {
            return this.panel.view.isDirty();
        },
        _cancel: function (check) {
            this.enabled && this.trigger("cancel", check);
        }
    });

    return CreateDialogView;
});

define("jira-project-config/custom-fields/steps/create-field/views/create-panel", [
    'jira-project-config/custom-fields/templates',
    'jira/util/formatter',
    "jira-project-config/custom-fields/views/forms/form",
    "jira-project-config/custom-fields/views/forms/options-field",
    "jira-project-config/custom-fields/views/forms/form-field",
    "jira-project-config/custom-fields/views/forms/validators",
    "jira-project-config/marionette",
    "underscore"
], function(
    CustomFieldTemplates,
    formatter,
    Form,
    OptionsField,
    FormField,
    Validators,
    Marionette,
    _) {
    var TEMPLATES = CustomFieldTemplates;

    return Marionette.ItemView.extend({
        initialize: function (options) {
            options = options || {};
            this.template = _.bind(this.template, this);
            this.form = options.form || this._createForm(options);
            this.listenTo(this.form, Form.EVENTS.submit, this._submit);
        },
        template: function () {
            return this.form.render().$el;
        },
        _submit: function () {
            this.triggerMethod("submit");
        },
        triggerSubmit: function () {
            this.form.submit();
            return this;
        },
        updateData: function (data) {
            this.form.setValueOfFieldWithName('name', data.fieldName);
        },
        getData: function () {
            return this.form.toJSON();
        },
        onDialogPanelShow: function () {
            //IE8 has trouble focusing the field initially. This seems to fix the problem.
            this.form.blur().focus();
        },
        showErrors: function (fieldErrors, generalErrors) {
            return this.form.showErrors(fieldErrors, generalErrors);
        },
        focus: function () {
            this.form.focus();
            return this;
        },
        disable: function () {
            this.form.disable();
            return this;
        },
        enable: function () {
            this.form.enable();
            return this;
        },
        isDirty: function () {
            return this.form.isDirty();
        },
        setValueOfFieldWithName: function (name, value) {
            this.form.setValueOfFieldWithName(name, value);
        },
        //Following are visible for testing.
        _createForm: function (options) {
            return new Form(this._createFormParams(options));
        },
        _createFormParams: function (options) {
            var fields = [{
                //Adding a submit button directly to make sure the form will always submit.
                content: TEMPLATES.submitHack
            }, {
                id: "custom-field-name",
                label: formatter.I18n.getText('common.concepts.name'),
                name: 'name',
                type: 'text',
                maxLength: 255,
                required: true,
                size: FormField.SIZE.large,
                validator: Validators.notEmpty(formatter.I18n.getText("admin.errors.customfields.no.name")),
                value: options.fieldName
            }, {
                id: "custom-field-description",
                label: formatter.I18n.getText('common.concepts.description'),
                name: 'description',
                type: 'textarea',
                rows: 2,
                size: FormField.SIZE.large
            }];

            if (options.configureOptions) {
                fields.push({
                    required: true,
                    label: formatter.I18n.getText("admin.common.words.options"),
                    field: new OptionsField({name: "options"}),
                    id: "custom-field-options-input"
                });
            }

            return {
                fields: fields
            };
        }
    });
});
