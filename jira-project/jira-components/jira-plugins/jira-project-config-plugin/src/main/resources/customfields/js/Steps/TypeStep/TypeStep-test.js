AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:custom-fields",
    "com.atlassian.jira.jira-project-config-plugin:custom-fields-impl"], function () {

    var _ = require('underscore');
    var TypeStep = require("jira-project-config/custom-fields/steps/type");
    var Backbone = require("jira-project-config/backbone");
    var Deferred = require("jira/jquery/deferred");
    var IssueCustomFields = require("jira-project-config/custom-fields/service");
    var Type = require("jira-project-config/custom-fields/steps/type/models/type");
    var FAIL_REASON = require("jira-project-config/custom-fields/wizard/fail-reasons");

    module("JIRA.Admin.CustomFields.TypeStep", {
        setup: function () {
            this.Backbone = Backbone;
            this.sandbox = sinon.sandbox.create();
            this.typeStep = TypeStep;
            this.Type = Type;
            this.api = {
                getCustomFieldTypes: this.sandbox.stub(),
                getSharedBy: function (wrap) {
                    return wrap;
                }
            };
        },
        teardown: function () {
            this.sandbox.restore();
        }
    });

    test("_getCategories() mangles the REST data appropriately", function () {
        var xhr = Deferred();

        var step = new this.typeStep({
            initialCategory: "b",
            api: this.api
        });

        var REST = {
            types: [
                {key: "x", name: "X", categories: ["a"], description: "X.", previewImageUrl: "/usr/jack.png"},
                {key: "y", name: "Y", categories: ["b"], description: "Y."},
                {key: "z", name: "Z", categories: ["a", "b"], description: "Z."}
            ],
            categories: [
                {id: "a", name: "A"},
                {id: "b", name: "B"}
            ]
        };

        this.api.getCustomFieldTypes.returns(xhr);

        var promise = step._getCategories();
        xhr.resolveWith(xhr, [REST]);

        promise.done(function (categories) {
            var types = {
                x: {
                    key: "x",
                    name: "X",
                    description: "X.",
                    previewImageUrl: "/usr/jack.png"
                },
                y: {
                    key: "y",
                    name: "Y",
                    description: "Y."
                },
                z: {
                    key: "z",
                    name: "Z",
                    description: "Z."
                }
            };

            var expected = [
                {
                    initial: false,
                    id: "a",
                    name: "A",
                    types: [types.x, types.z]
                },
                {
                    initial: true,
                    id: "b",
                    name: "B",
                    types: [types.y, types.z]
                }
            ];

            _.each(categories, function (category) {
                category.types = category.types.toJSON();
            });

            deepEqual(categories, expected);
        });
    });

    test("_getCategories() translates abort", function () {
        var getting = Deferred();

        var step = new this.typeStep({
            initialCategory: "b",
            api: this.api
        });

        this.api.getCustomFieldTypes.returns(getting);

        var promise = step._getCategories();
        getting.reject(IssueCustomFields.STATUS.abort);

        var good = this.sandbox.stub();
        var bad = this.sandbox.stub();
        promise.done(good).fail(bad);

        ok(!good.called, "Good should not be called.");
        ok(bad.calledOnce, "Bad callback fired.");
        deepEqual(bad.firstCall.args, [FAIL_REASON.abort], "Bad called with aborted.");
    });

    test("_getCategories() translates errors", function () {
        var getting = Deferred();

        var step = new this.typeStep({
            initialCategory: "b",
            api: this.api
        });

        var reason = "Reason!";

        this.api.getCustomFieldTypes.returns(getting);

        var promise = step._getCategories();
        getting.reject(IssueCustomFields.STATUS.error, reason);

        var good = this.sandbox.stub();
        var bad = this.sandbox.stub();
        promise.done(good).fail(bad);

        ok(!good.called, "Good should not be called.");
        ok(bad.calledOnce, "Bad callback fired.");
        deepEqual(bad.firstCall.args, [FAIL_REASON.error, reason], "Bad called fired with error.");
    });

    test("start() handles successfully getting categories", function () {
        var context = AJS.test.mockableModuleContext();
        var TypeChoice = this.sandbox.stub().returns(new this.typeStep.Views.TypeChoice([]));

        context.mock('jira-project-config/custom-fields/steps/type/views/type-choice', TypeChoice);
        var TypeStep = context.require('jira-project-config/custom-fields/steps/type');
        var _getCategories = this.sandbox.stub(TypeStep.prototype, "_getCategories");

        var step = new TypeStep({
            api: this.api
        });
        var categories = ["foo", "bar"];
        var happy = Deferred().resolveWith(step, [categories]).promise();
        var startFinished = this.sandbox.spy();

        // Configure our stubs.
        _getCategories.returns(happy);

        step.start().done(startFinished);

        ok(TypeChoice.calledWith(categories), "Expected TypeChoice to be instantiated in the happy case.");
        ok(startFinished.calledOnce, "TypeChoice should resolve the promise.");
    });

    test("showLoading() calls showLoading on the view", function () {
        var view = new this.Backbone.View({});
        var showLoading = view.showLoading = this.sandbox.stub();
        var step = new this.typeStep({
            api: this.api
        });

        // Call showLoading() when there is no view to confirm there are no errors
        step.showLoading();

        step.view = view;
        ok(!showLoading.called, "showLoading() on the view should not yet have been called");
        step.showLoading();
        ok(showLoading.called, "hideLoading() on the view should have been called");
    });

    test("hideLoading() calls hideLoading on the view", function () {
        var view = new this.Backbone.View({});
        var hideLoading = view.hideLoading = this.sandbox.stub();
        var step = new this.typeStep({
            api: this.api
        });

        // Call showLoading() when there is no view to confirm there are no errors
        step.hideLoading();

        step.view = view;
        ok(!hideLoading.called, "hideLoading() on the view should not yet have been called");
        step.hideLoading();
        ok(hideLoading.called, "hideLoading() on the view should have been called");
    });

    test("start() handles not getting categories error", function () {
        var context = AJS.test.mockableModuleContext();
        var error = new (this.Backbone.View.extend({show: this.sandbox.stub()}))();
        var ErrorDialog = this.sandbox.stub();

        context.mock('jira-project-config/custom-fields/error-dialog', ErrorDialog);

        var TypeStep = context.require('jira-project-config/custom-fields/steps/type');
        var _getCategories = this.sandbox.stub(TypeStep.prototype, "_getCategories");
        var step = new TypeStep({
            api: this.api
        });
        var errorMsg = "Error";
        var unhappy = Deferred().rejectWith(step, [FAIL_REASON.error, errorMsg]).promise();
        var startFinished = this.sandbox.spy();

        // Configure our stubs.
        _getCategories.returns(unhappy);
        ErrorDialog.returns(error);

        step.start().always(startFinished);

        ok(ErrorDialog.firstCall.args[0].heading, "has heading");
        equal(ErrorDialog.firstCall.args[0].message, errorMsg, "has message");
        ok(startFinished.calledOnce, "TypeChoice should reject the promise.");
    });

    test("start() handles not getting categories abort", function () {
        var _getCategories = this.sandbox.stub(this.typeStep.prototype, "_getCategories");

        var step = new this.typeStep({
            api: this.api
        });

        var abort = Deferred().rejectWith(step, [FAIL_REASON.abort]).promise();

        // Configure our stubs.
        _getCategories.returns(abort);

        var cancel = this.sandbox.spy();

        step.on("cancel", cancel);
        step.start();

        ok(cancel.calledOnce, "Cancel called on user abort.");
    });

    test("Submission of view updates the step's data and calls next with correct step type", function () {
        var View = this.Backbone.View.extend({
            show: function () {
            }
        });

        var _getCategories = this.sandbox.stub(this.typeStep.prototype, "_getCategories");
        var next = this.sandbox.stub(this.typeStep.prototype, "fireNext");
        var categories = ["foo", "bar"];

        var step = new this.typeStep({
            api: this.api,
            view: View
        });
        // Configure our stubs.
        _getCategories.returns(Deferred().resolveWith(step, [categories]).promise());
        step.start();

        var model = new this.Backbone.Model({
            key: "key",
            name: "name",
            options: "options",
            cascading: "cascading"

        });
        step.view.trigger("submit", model);

        ok(next.calledOnce, "Next should have been called on submit.");
        deepEqual(step.getStepData(), {
            type: {
                key: "key",
                name: "name",
                options: "options",
                cascading: "cascading"
            }
        }, "Step's data should have been correctly updated");
    });

    test("restart() returns a resolved promise", function () {
        var step = new this.typeStep({
            api: this.api
        });
        var restartHandler = this.sandbox.spy();
        step.restart().done(restartHandler);
        ok(restartHandler.calledOnce, "The restartHandler should have been called once");
    });

    test("show() shows the UI.", function () {
        var view = new this.Backbone.View({});
        var show = view.show = this.sandbox.stub();
        var step = new this.typeStep({
            api: this.api
        });

        //Make sure there is no error when no view exists.
        step.show();

        step.view = view;
        step.show();
        ok(show.calledOnce, "Is the view shown?");
    });

    test("pauseStep() hides the UI.", function () {
        var view = new this.Backbone.View({});
        var hide = view.hide = this.sandbox.stub();
        var step = new this.typeStep({
            api: this.api
        });

        //Make sure there is no error when no view exists.
        step.pauseStep();

        step.view = view;
        step.pauseStep();
        ok(hide.calledOnce, "Is the view hidden?");
    });

    test("stopStep() destroys the view and deletes it", function () {
        var view = new this.Backbone.View({});
        var destroy = view.destroy = this.sandbox.stub();
        var step = new this.typeStep({
            api: this.api
        });
        //Make sure there is no error when no view exists.
        step.stopStep();

        step.view = view;
        step.stopStep();
        ok(destroy.calledOnce, "Is the view hidden?");
        ok(step.view === void 0, "Is the view deleted?");
    });

    module("JIRA.Admin.CustomFields.TypeStep.Models.Types", {
        setup: function () {
            this.Models = TypeStep.Models;
        }
    });

    test("Matcher filters based on case insensitive name, split on spaces", function () {
        var matcher = this.Models.Types.prototype.matcher;
        var a = new this.Models.Type({key: "a", name: "aBc dEf gHi", description: "Aaa."});

        ok(matcher(a, "ABc", {}), "Expected case insensitive searching.");
        ok(matcher(a, "a b", {}), "Expected space separated predicates to be AND together");
        ok(!matcher(a, "a x", {}), "Expected space separated predicates to be AND together");
        ok(matcher(a, " bc", {}), "Expected leading spaces to be ignored.");
        ok(matcher(a, "ab ", {}), "Expected trailing spaces to be ignored.");
        ok(!matcher(a, "fg", {}), "Expected misses to not match");
    });
});
