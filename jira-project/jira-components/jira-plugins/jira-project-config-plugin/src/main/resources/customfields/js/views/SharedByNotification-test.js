AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:custom-fields",
    "com.atlassian.jira.jira-project-config-plugin:custom-fields-impl"], function () {

    var $ = require('jquery');
    var _ = require("underscore");
    var AJSHelper = window.AJS;

    module("JIRA.Admin.CustomFields.SharedByNotification", {
        setup: function () {
            this.sandbox = sinon.sandbox.create();
            this.inlineDialog = this.sandbox.stub();
            this.popup = {
                hide: this.sandbox.spy(),
                remove: this.sandbox.spy()
            };
            this.inlineDialog.returns(this.popup);
            this.context = AJS.test.mockableModuleContext();
            this.context.mock('aui/inline-dialog', this.inlineDialog);
            this.SharedByView = this.context.require('jira-project-config/custom-fields/views/shared-by');
            this.$fixture = $("#qunit-fixture");
            this.abc = {
                key: 'ABC',
                name: 'ABC',
                id: 0,
                url: "/abc"
            };
            this.def = {
                key: 'DEF',
                name: 'DEF',
                id: 1,
                url: "/def"
            };
            this.sandbox.spy(AJSHelper, 'format');
        },
        teardown: function () {
            this.sandbox.restore();
        }
    });

    var ProjectList = function () {
        this.$el = $("#inline-dialog-customfield-project-list");
    };

    _.extend(ProjectList.prototype, {
        visible: function () {
            return this.$el.is(":visible");
        },
        projects: function () {
            var projects = [];
            this.$el.find("li").each(function () {
                projects.push($.trim($(this).text()));
            });
            return projects;
        },
        exists: function () {
            return !!this.$el.parent();
        }
    });


    var Driver = function ($el) {
        this.$el = $el;
    };

    _.extend(Driver.prototype, {
        text: function () {
            return $.trim(this.$el.text());
        }
    });

    var testMessage = function (formatArgs) {
        var sharedByView = new this.SharedByView({projects: _.toArray(arguments).slice(1)});
        this.$fixture.append(sharedByView.render().$el);

        sinon.assert.calledWith.apply(sinon.assert, [AJSHelper.format].concat(formatArgs));
    };

    test("View with one project", function () {
        testMessage.call(this, ["admin.project.customfieldadd.sharedby.single", "<a>", "</a>"], this.abc);
    });

    test("View with two project", function () {
        testMessage.call(this, ["admin.project.customfieldadd.sharedby.plural", "<a>", 2, "</a>"], this.abc, this.def);
    });

    test("List projects", function () {
        var parseProjects = function ($el) {
            var project = [];
            $el.find("li").each(function () {
                project.push($.trim($(this).text()));
            });
            return project;
        };

        var sharedByView = new this.SharedByView({projects: [this.abc, this.def]});
        this.$fixture.append(sharedByView.render().$el);

        ok(!this.inlineDialog.called, "No inline dialog created.");

        //Inline dialog inited on dialog show.
        sharedByView.onDialogNotificationShow();
        ok(this.inlineDialog.calledOnce, "Inline dialog created.");

        //Inline dialog has correct contents and is shown.
        var $el = $("<div>");
        var show = this.sandbox.stub();
        var callback = this.inlineDialog.firstCall.args[2];
        callback($el, null, show);

        ok(show.calledOnce, "Inline dialog show.");
        deepEqual(parseProjects($el), [this.abc.name, this.def.name], "Projects rendered.");

        //Make sure the inline dialog is hidden and removed on dialog hide.
        sharedByView.onDialogNotificationHide();
        ok(this.popup.hide.calledOnce, "Inline dialog hidden");
        ok(this.popup.remove.calledOnce, "Inline dialog removed.");
    });
});
