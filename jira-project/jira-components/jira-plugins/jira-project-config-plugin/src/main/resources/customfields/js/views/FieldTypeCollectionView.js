define("jira-project-config/custom-fields/views/field-type-collection", [
    'jira-project-config/custom-fields/templates',
    "jira-project-config/custom-fields/views/type-item",
    "jira-project-config/custom-fields/views/empty-type-item",
    "jira/util/users/logged-in-user",
    "jira-project-config/marionette",
    "jira/ajs/input/keyboard",
    "underscore",
    "jquery"
], function(
    CustomFieldTemplates,
    TypeItemView,
    EmptyTypeItemView,
    User,
    Marionette,
    Keyboard,
    _,
    $) {
    "use strict";

    var TEMPLATES = CustomFieldTemplates;

    /**
     * A collection view of all the types.
     *
     * @fires FieldTypeCollectionView#changedSelection Fired when the selected type changes.
     */
    return Marionette.CompositeView.extend({
        itemView: TypeItemView,
        emptyView: EmptyTypeItemView,
        itemViewContainer: ".customfields-types",

        /**
         * Returns the 0-index of an item (view) in the collection.
         *
         * @param {TypeItemView} item The item in the collection.
         * @returns {Number} 0-based index of the view
         */
        indexOf: function (item) {
            if (!item) {
                return -1;
            }

            return this.collection.indexOf(item.model);
        },

        /**
         * Attach the view's element back into the DOM.
         */
        attach: function () {
            if (this.placeholder) {
                this.placeholder.replaceWith(this.$el);
                this.$el.parent().scrollTop(this.scrollTopBeforeDetach);
                delete this.placeholder;
                delete this.scrollTopBeforeDetach;
            }
        },

        /**
         * Detach the view's element from the DOM. We store what the current scrollTop value is so that we can restore
         * it when reattaching the panel to the DOM.
         *
         * This is used to improve performance (mainly for IE).
         */
        detach: function () {
            this.placeholder = $("<div/>").insertBefore(this.$el);
            this.scrollTopBeforeDetach = this.$el.parent().scrollTop();
            this.$el.detach();
        },

        initialize: function () {
            this.selectedView = null;
            _.bindAll.apply(_, [this].concat(_.functions(this)));
        },

        onItemviewSelect: function (view) {
            this.select(view);
        },

        onItemRemoved: function () {
            this.selectByIndex(0);
        },

        onAfterItemAdded: function () {
            this.selectByIndex(0);
        },

        onItemviewSubmit: function (view, key) {
            this.triggerMethod("submit", key);
        },

        onItemviewOngotoall: function () {
            this.triggerMethod("changeToAll");
        },

        isEmpty: function () {
            return !this.collection.length;
        },

        _getEmptyView: function () {
            return this.isEmpty() && this.children.findByIndex(0);
        },

        setHasMatches: function (hasMatches) {
            var emptyView = this._getEmptyView();
            var state;

            // We only need to update the empty view content if we need to show it.
            if (!emptyView) {
                return;
            }

            if (hasMatches) {
                state = EmptyTypeItemView.CHANGE_TAB;
            } else if (User.isSysadmin()) {
                state = EmptyTypeItemView.MARKETPLACE;
            } else {
                state = EmptyTypeItemView.EMPTY;
            }

            emptyView.setEmptyState(state);
        },

        onKeydown: function (event) {
            switch (Keyboard.SpecialKey.fromKeyCode(event.keyCode)) {
                case Keyboard.SpecialKey.UP:
                    this.collection.length && this.moveSelect(-1);
                    break;
                case Keyboard.SpecialKey.DOWN:
                    this.collection.length && this.moveSelect(1);
                    break;
                case Keyboard.SpecialKey.RETURN:
                    this.collection.length && this.triggerMethod("submit", this.getSelectedKey());
                    break;
                default:
                    return;
            }
            event.preventDefault();
        },

        /**
         * Move the selection to another item, using an offset
         *
         * @param offset
         */
        moveSelect: function (offset) {
            var index = this.indexOf(this.selectedView);
            if (index >= 0) {
                // Find the next index, bounded to 0->length
                index = Math.min(this.children.length - 1, Math.max(0, index + offset));
                this.selectByIndex(index);
            }
        },

        select: function (view) {
            if (this.selectedView) {
                this.selectedView.deselect();
            }


            this.selectedView = view;
            view && this.selectedView.select();

            /**
             * An event to notify listeners that the selected type has changed.
             *
             * @event FieldTypeCollectionView#changedSelection
             * @type {object}
             * @property {string|null} key - The currently selected type's key.
             */
            this.triggerMethod("changedSelection", {
                key: this.getSelectedKey()
            });
        },

        selectByIndex: function (index) {
            var view = this._viewAtIndex(index);
            this.select(view);
        },

        _viewAtIndex: function (index) {
            var model = this.collection.at(index);
            return model && this.children.findByModel(model);
        },

        template: function () {
            return TEMPLATES.types();
        },

        onDialogPanelHide: function () {
            this.detach(); // Remove the element for performance, since the panel is hidden.
            $(document).unbind("keydown", this.onKeydown);
        },

        onDialogPanelShow: function () {
            this.attach();  // Put the element back in the DOM so that it's visible.
            $(document).bind("keydown", this.onKeydown);
        },

        getSelectedKey: function () {
            if (this.selectedView) {
                return this.selectedView.getKey();
            } else {
                return null;
            }
        },

        getSelectedModel: function () {
            return this.selectedView && this.selectedView.model;
        },

        appendHtml: function (cv, iv, index) {
            // Marionette's CollectionView doesn't honor the index when inserting items into
            // the DOM...
            var $container = this.getItemViewContainer(cv);
            if (index === 0) {
                $container.prepend(iv.el);
            } else {
                $container.children().eq(index - 1).after(iv.el);
            }
        }
    });
});

AJS.namespace("JIRA.Admin.CustomFields.FieldTypeCollectionView", null, require("jira-project-config/custom-fields/views/field-type-collection"));
