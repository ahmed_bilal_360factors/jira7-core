define("jira-project-config/custom-fields/wizard/abstract-step", [
    "jira-project-config/custom-fields/wizard/step",
    "underscore"
], function (Step,
             _) {
    /**
     * An extension of Step. Provides some convenience method implementations of Step which should make creating Steps
     * for a Wizard easier.
     *
     * Such convenience implementations are:
     * startStep():
     * provides the ability to distinguish between a first start, and subsequent starts - first starts call
     * "start()" (to be implemented) and subsequent starts call "restart()" (by default, just calls "start()", but can be overridden).
     *
     * setPreviousData():
     * provides a storage facility for previousData. Can be accessed by extensions via "getPreviousData()".
     * The previousData should not be mutated.
     *
     * getAccumulatedData():
     * returns the combination of both the previousData, and the step's data, which it retrieves
     * via a call to "getStepData()" (to be implemented).
     *
     * For more information, see Step.
     */
    return _.extend({}, Step, {
        // Overridden methods

        /**
         * Convenience implementation for startStep. Has the concept of a first start (which calls an abstract method, start()),
         * and any other starts (which call an abstract method, restart()).
         *
         * Shows the step after it has finished starting/restarting.
         *
         * @returns {jQuery.Promise}
         */
        startStep: function () {
            if (this.hasStarted) {
                return this.restart();
            } else {
                this.hasStarted = true;
                return this.start();
            }
        },

        /**
         * Returns an object that is the merge of all previous data and all data in this step.
         *
         * @returns {object} the combined data of this step and all previous data
         */
        getAccumulatedData: function () {
            return _.extend({}, this._previousData, this.getStepData());
        },

        /**
         * Sets the previous data.
         *
         * @param previousData the data to set into _previousData
         */
        setPreviousData: function (previousData) {
            this._previousData = previousData;
        },

        // Methods intended for overriding

        /**
         * Called by the wizard when this step should start.
         *
         * This method should be overridden.
         *
         * @returns {jQuery.Promise}
         */
        start: function () {

        },

        /**
         * Called after going back and forward. By default this calls this.start() but can be overridden.
         * @returns {jQuery.Promise}
         */
        restart: function () {
            return this.start();
        },

        /**
         * Returns the data associated with this step.
         *
         * This method should be overridden.
         */
        getStepData: function () {

        },

        // Convenience methods

        /**
         * Returns the previousData. The returned object should only be read from, not modified.
         * @returns {object} the data of all previous steps
         */
        getPreviousData: function () {
            return this._previousData;
        }

    });
});

AJS.namespace("JIRA.Admin.CustomFields.Wizard.AbstractStep", null, require("jira-project-config/custom-fields/wizard/abstract-step"));
