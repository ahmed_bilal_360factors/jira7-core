define("jira-project-config/custom-fields/util", [
    "wrm/context-path",
    "underscore",
    "jquery",
    "exports"
], function (contextPath,
             _,
             $,
             util) {
    function isEmpty(value) {
        return value === undefined || value === null;
    }

    /**
     * Creates a Backbone compatible comparator based on a single model attribute.
     *
     * Nulls & undefined first.
     *
     * @param {string} attr The model attribute to compare
     * @returns {Function}
     */
    util.lowerCaseLocaleComparator = function (attr) {
        return function (a, b) {
            var aAttr = a.get(attr);
            var bAttr = b.get(attr);


            if (isEmpty(aAttr) && isEmpty(bAttr)) {
                return 0;
            } else if (isEmpty(aAttr)) {
                return -1;
            } else if (isEmpty(bAttr)) {
                return 1;
            }

            return aAttr.toLocaleLowerCase().localeCompare(bAttr.toLocaleLowerCase());
        };
    };

    /**
     * Build a JIRA URL with path and querystring parameters.
     *
     * @param {string|Array} path The path URL component (excluding JIRA's context path) (e.g. '/secure/admin/delete/11'),
     * or the array of path components that can be used to produce the path (e.g. ['secure', 'admin', 'delete', 11]).
     * @param {object|object[]} [params] The querystring parameters. jQuery.param is used for serialisation
     */
    util.url = function (path, params) {

        if (_.isArray(path)) {
            var realPath = "";
            for (var i = 0; i < path.length; i++) {
                realPath = realPath + "/" + encodeURIComponent("" + path[i]);
            }
            path = realPath;
        }

        var querystring = "";
        if (params) {
            querystring = "?" + $.param(params, true);
        }

        return contextPath() + path + querystring;
    };
});

AJS.namespace("JIRA.Admin.CustomFields.url", null, require("jira-project-config/custom-fields/util").url);
AJS.namespace("JIRA.Admin.CustomFields.lowerCaseLocaleComparator", null, require("jira-project-config/custom-fields/util").lowerCaseLocaleComparator);
