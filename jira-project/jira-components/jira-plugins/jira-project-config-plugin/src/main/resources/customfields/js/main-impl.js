define("jira-project-config/custom-fields/api", [
    'jira-project-config/libs/ajshelper',
    'jira-project-config/custom-fields/templates',
    'jira/util/formatter',
    "jira-project-config/issues/api",
    "jira-project-config/custom-fields/steps/field-wisher",
    "jira-project-config/custom-fields/steps/type",
    "jira-project-config/custom-fields/wizard",
    "jira-project-config/custom-fields/wizard/fail-reasons",
    "jira-project-config/custom-fields/service",
    "jira/message",
    "jira-project-config/custom-fields/util",
    "jira/util/browser",
    "underscore",
    "exports"
], function(
    AJSHelper,
    CustomFieldTemplates,
    formatter,
    issuesApi,
    FieldWisherStep,
    TypeStep,
    Wizard,
    FailReasons,
    CustomFieldsService,
    Messages,
    util,
    Browser,
    _,
    customFieldsApi) {

    var TEMPLATES = CustomFieldTemplates;

    /**
     * Used on the 'Custom Fields' admin page to present the 'Add Custom Field' wizard to the user, and
     * then redirect through to the 'Associate field to screens' page once the custom field has been created.
     */
    customFieldsApi.addCustomFieldViaAdminPage = function () {
        customFieldsApi.addCustomField().done(function (data) {
            var destination = util.url("/secure/admin/AssociateFieldToScreens!default.jspa", {
                fieldId: data.field.id,
                returnUrl: "ViewCustomFields.jspa"
            });
            Browser.reloadViaWindowLocation(destination);
        });
    };

    /**
     * Present the 'Add Custom Field' wizard to the user.
     *
     * @returns {jQuery.Promise} A promise that represents the creation of the custom field.
     *
     * When the promise is resolved, an object describing the field is passed to the handler.
     */
    customFieldsApi.addCustomField = function () {
        var api;
        var sharedStepOptions;

        api = new CustomFieldsService({autoCloseWebsudo: false});
        sharedStepOptions = {api: api, inIssueContext: false};

        return new Wizard({
            sharedStepOptions: sharedStepOptions,
            firstStep: new TypeStep(sharedStepOptions),
            autoClose: false,
            dirtyMessage: formatter.I18n.getText("admin.project.customfieldadd.dirty")
        }).start()
            .done(function () {
                AJSHelper.trigger("analytics", {
                    name: "administration.customfields.created.from.global"
                });
            });
    };

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Present a series of dialogs that allow a (existing or new) custom field to be added to the current issue's
     * screens.
     *
     * @param {string} issue The ID of the issue whose screens the field should be added to.
     * @returns {jQuery.Promise} Resolved after adding a field, rejected on cancel or error.
     */
    customFieldsApi.addCustomFieldToIssue = function (issue) {
        var api;
        var sharedStepOptions;
        var wizard;

        api = new CustomFieldsService({autoCloseWebsudo: false, issue: issue});
        sharedStepOptions = {api: api, inIssueContext: true};

        wizard = new Wizard({
            sharedStepOptions: sharedStepOptions,
            firstStep: new FieldWisherStep(sharedStepOptions),
            autoClose: false,
            dirtyMessage: formatter.I18n.getText("admin.project.customfieldadd.dirty")
        });

        return wizard
            .start()
            .fail(customFieldsApi.addCustomFieldToIssue._notifyFailure)
            .done(customFieldsApi.addCustomFieldToIssue._analytics)
            .done(function (data) {
                // Notify the user that the field was added. There's two options:
                // - Pop the 'edit field' dialog, or
                // - Show a 'success' message
                wizard.close();
                customFieldsApi.addCustomFieldToIssue.editOrNotify(data.field);
            });
    };

    _.extend(customFieldsApi.addCustomFieldToIssue, {
        /**
         * Edit the field if it's possible, otherwise pop a success message.
         *
         * @param {string} field.id The ID of the field.
         * @returns {*}
         */
        editOrNotify: function (field) {
            if (!customFieldsApi.addCustomFieldToIssue._editValue(field)) {
                customFieldsApi.addCustomFieldToIssue._notifySuccess();
            }
        },

        /**
         * Pop a 'edit field value' dialog.
         * @private
         * @param {string} field.id The ID of the field.
         * @returns {boolean} true if the field will be edited, otherwise false.
         */
        _editValue: function (field) {
            var edit = issuesApi && issuesApi.editFieldOnSelectedIssue;

            return edit && edit(field.id);
        },

        /**
         * Pop a 'field not created' message.
         * @private
         */
        _notifyFailure: (function () {
            var stepsMessage = function (message, fails) {
                showMessage(TEMPLATES.addCompleteError({
                    message: message,
                    failedSteps: fails || []
                }));
            };

            var showMessage = function (message) {
                Messages.showErrorMsg(message, {
                    closeable: true
                });
            };

            return function (reason, options) {
                var data;

                options = options || {};
                data = options.data || {};

                if (reason === FailReasons.abort) {
                    if (options.dirty) {
                        if (data.createField) {
                            stepsMessage(formatter.I18n.getText("admin.project.customfieldadd.create.aborted.operations"),
                                options.notCompletedDescriptions);
                        } else {
                            showMessage(formatter.I18n.getText("admin.project.customfieldadd.failed"));
                        }
                    }
                } else if (reason === FailReasons.error) {
                    if (data.createField) {
                        stepsMessage(formatter.I18n.getText("admin.project.customfieldadd.create.failed.operations"),
                            options.notCompletedDescriptions);
                    } else {
                        showMessage(formatter.I18n.getText("admin.project.customfieldadd.failed"));
                    }
                }
            };
        }()),

        /**
         * Pop a 'field created' message.
         * @private
         */
        _notifySuccess: function () {
            Messages.showSuccessMsg(formatter.I18n.getText("issue.operations.fields.add.no.edit.permission"), {
                closeable: true
            });
        },

        /**
         * @private
         */
        _analytics: function (data) {
            if (data.createField) {
                AJSHelper.trigger("analytics", {
                    name: "administration.customfields.created.and.added.to.issue"
                });
            } else if (data.addField) {
                AJSHelper.trigger("analytics", {
                    name: "administration.customfields.existing.added.to.issue"
                });
            } else {
                // If the field is visible on the screen
                if (customFieldsApi.addCustomFieldToIssue._isFieldVisible(data.field.id)) {
                    AJSHelper.trigger("analytics", {
                        name: "administration.customfields.existing.on.issue.screens.edited.has.value"
                    });
                } else {
                    AJSHelper.trigger("analytics", {
                        name: "administration.customfields.existing.on.issue.screens.edited.has.no.value"
                    });
                }
            }
        },

        /**
         * @private
         */
        _isFieldVisible: function (fieldId) {
            var field;
            var fields;
            var getFieldsOnSelectedIssue;

            getFieldsOnSelectedIssue = issuesApi && issuesApi.getFieldsOnSelectedIssue;

            fields = getFieldsOnSelectedIssue && getFieldsOnSelectedIssue();
            field = fields && fields.get(fieldId);

            return field && field.matchesFieldSelector();
        }
    });
});

AJS.namespace("JIRA.Admin.CustomFields.addCustomFieldViaAdminPage", null,
    require("jira-project-config/custom-fields/api").addCustomFieldViaAdminPage);

AJS.namespace("JIRA.Admin.CustomFields.addCustomField", null,
    require("jira-project-config/custom-fields/api").addCustomField);

AJS.namespace("JIRA.Admin.CustomFields.addCustomFieldToIssue", null,
    require("jira-project-config/custom-fields/api").addCustomFieldToIssue);
