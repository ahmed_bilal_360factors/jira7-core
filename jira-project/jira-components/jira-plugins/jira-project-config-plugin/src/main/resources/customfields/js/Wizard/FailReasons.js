define("jira-project-config/custom-fields/wizard/fail-reasons", {
    cancel: "cancel", // User cancelled the operation
    abort: "abort", // User cancelled the operation while in progress.
    recoverableError: "recoverableError", // A recoverable error occurred
    error: "error" // An error occurred.
});
