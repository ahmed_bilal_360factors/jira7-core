require([
    'wrm/require',
    'jquery'
], function (wrmRequire,
             $) {

    var loadApi = function (callback) {
        wrmRequire(["wrc!com.atlassian.jira.jira-project-config-plugin.custom-fields-impl"], function () {
            require(['jira-project-config/custom-fields/api'], callback);
        });
    };

    // Adding a custom field from the Administration -> Custom Fields page
    $(document).on("click", "#add_custom_fields", function (event) {
        event.preventDefault();

        loadApi(function (api) {
            api.addCustomFieldViaAdminPage();
        });
    });

});
