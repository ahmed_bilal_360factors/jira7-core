define("jira-project-config/custom-fields/error-dialog", [
    'jira-project-config/custom-fields/templates',
    'jira/util/formatter',
    "jira-project-config/custom-fields/views/dialog",
    "jira-project-config/backbone",
    "jira-project-config/marionette",
    "underscore",
    "jquery"
], function(
    CustomFieldTemplates,
    formatter,
    Dialog,
    Backbone,
    Marionette,
    _,
    $) {
    "use strict";

    var TEMPLATES = CustomFieldTemplates.ErrorDialog;
    /**
     * A simple dialog suitable for displaying bail-out errors.
     *
     * @param {string} [options.heading] Text to display in the header.
     * @param {string} [options.message] Text to display in an aui error message.
     * @constructor
     * @fires ErrorDialog#close
     * @property {Element} $el
     */
    var ErrorDialog = function (options) {
        var content;
        var dialog;
        var header;

        options || (options = {});

        _.defaults(options, {
            heading: "",
            message: ""
        });

        content = new Marionette.View({
            el: $(TEMPLATES.content({text: options.message}))
        });

        dialog = this.dialog = new Dialog();
        this.$el = dialog.$el;

        header = TEMPLATES.header({text: options.heading});

        dialog.addHeaderMain(header);
        dialog.addPanel("", content);
        dialog.addLink({
            label: formatter.I18n.getText("common.forms.cancel"),
            click: _.bind(this.cancel, this)
        });

        this.listenTo(dialog, "close", this.cancel);
    };

    _.extend(ErrorDialog.prototype, Backbone.Events, {
        show: function () {
            this.dialog.show();
        },

        hide: function () {
            this.dialog && this.dialog.hide();
        },

        /**
         * @private
         */
        cancel: function () {
            this.trigger("cancel");
        },

        destroy: function () {
            if (this.dialog) {
                this.dialog.destroy();
                delete this.dialog;
            }
        },

        hideLoading: function () {
            return this;
        },

        showLoading: function () {
            return this;
        },

        isDirty: function () {
            return false;
        }
    });

    // -- Events -------------------------------------------------------------------------------------------------------

    /**
     * Announcement that the dialog has been closed.
     * @event ErrorDialog#close
     */

    return ErrorDialog;
});

AJS.namespace("JIRA.Admin.CustomFields.ErrorDialog", null, require("jira-project-config/custom-fields/error-dialog"));
