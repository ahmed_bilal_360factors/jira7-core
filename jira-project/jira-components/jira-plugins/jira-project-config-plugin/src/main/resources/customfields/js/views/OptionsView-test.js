AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:custom-fields",
    "com.atlassian.jira.jira-project-config-plugin:custom-fields-impl"], function () {

    var $ = require('jquery');
    var _ = require("underscore");
    var OptionsField = require("jira-project-config/custom-fields/views/forms/options-field");

    var enabled = function (el) {
        return el.is(":enabled");
    };

    var FieldOption = function (el, parent) {
        this.$el = $(el);
        this.$parent = $(parent);
    };

    _.extend(FieldOption.prototype, {
        value: function () {
            return $.trim(this.$el.text());
        },
        deleteOption: function () {
            this.$el.find(".custom-field-options-delete").click();
            return this;
        },
        isDeleted: function () {
            return this.$el.closest(this.$parent).length === 0;
        },
        moveOption: function (newIndex) {
            this.$el.trigger("moveOption", newIndex);
            return this;
        },
        /**
         * We manually move on the DOM as we are not actually testing this component.
         */
        moveOnDom: function (newIndex) {
            this.$el.remove();
            if (newIndex > 0) {
                this.$el.after(this.$parent.get(newIndex - 1));
            } else {
                this.$el.prependTo(this.$parent);
            }
        }
    });

    var FieldOptions = function ($el) {
        this.$el = $el;
        this.input = $el.find("#custom-field-options-input");
        this.addButton = $el.find("#custom-field-options-add");
        this.items = $el.find("#custom-field-options-list-container");
    };

    _.extend(FieldOptions.prototype, {
        error: function () {
            return this.$el.find("div.error").text();
        },
        optionsText: function () {
            return _.map(this.options(), function (option) {
                return option.value();
            });
        },
        options: function () {
            var parent = this.items;
            return _.map(this.items.find("li"), function (el) {
                return new FieldOption(el, parent);
            });
        },
        isEnabled: function () {
            return enabled(this.input) && enabled(this.addButton) && !this.items.hasClass("disabled");
        },
        isDisabled: function () {
            return !enabled(this.input) && !enabled(this.addButton) && this.items.hasClass("disabled");
        },
        option: function (option) {
            if (_.isUndefined(option)) {
                return this.input.val();
            } else {
                this.input.val(option);
                return this;
            }
        },
        add: function () {
            this.addButton.click();
            return this;
        },
        addOption: function (option) {
            return this.option(option).add();
        },
        addOptions: function () {
            _.each(arguments, _.bind(this.addOption, this));
            return this;
        },
        enter: function () {
            var e = $.Event("keydown", {keyCode: $.ui.keyCode.ENTER});
            this.input.trigger(e);

            return this;
        }
    });

    module("JIRA.Admin.CustomFields.OptionsField", {
        setup: function () {
            this.sandbox = sinon.sandbox.create();
            this.$fixture = $("#qunit-fixture");

            this.field = new OptionsField({
                name: "name"
            });
            this.$fixture.append(this.field.render().$el);
            this.pageObject = new FieldOptions(this.$fixture);

            $(document.body).focus();
        },
        teardown: function () {
            this.sandbox.restore();
        }
    });

    test("initial state", function () {
        ok(this.pageObject.isEnabled(), "Enabled by default?");
        deepEqual(this.pageObject.optionsText(), [], "No options by default.");
    });

    test("validate", function () {
        ok(!this.field.validate(), "Should not validate with no options.");
        ok(this.pageObject.error(), "Error with no options.");

        this.pageObject.option("One").add();

        ok(this.field.validate(), "Should validate with options.");
        ok(!this.pageObject.error(), "Error should have been removed.");
    });

    test("setError && clearError", function () {
        var error = "Error";
        ok(this.field.setError(error) === this.field, "Returned this from setError");
        equal(this.pageObject.error(), "Error", "Error displayed on the UI.");

        ok(this.field.clearError() === this.field, "Returned this from clearError");
        ok(!this.pageObject.error(), "Error cleared from UI.");
    });

    test("value", function () {
        deepEqual(this.field.value(), [], "No options to start with.");
        this.pageObject.option("  a  ").add().option("b").add();
        deepEqual(this.field.value(), [{name: "a"}, {name: "b"}], "Correct options returned.");
    });

    test("enable && disable", function () {
        ok(this.field.disable() === this.field, "this returned from disable.");
        ok(this.pageObject.isDisabled(), "FieldOptions are disabled.");

        ok(this.field.enable() === this.field, "this returned from enable.");
        ok(this.pageObject.isEnabled(), "FieldOptions are enabled.");
    });

    var assertAddOptions = function (submit) {
        //Add an empty option.
        this.pageObject.option("    ")[submit]();
        deepEqual(this.pageObject.optionsText(), [], "FieldOption not added?");
        equal(this.pageObject.option(), "    ", "FieldOption input not changed.");
        ok(this.pageObject.error(), "Error displayed for empty option.");

        //Check good case.
        this.pageObject.option("something")[submit]();
        deepEqual(this.pageObject.optionsText(), ["something"], "Added option.");
        ok(!this.pageObject.option(), "FieldOption cleared.");

        //Check duplicate error
        this.pageObject.option("Something")[submit]();
        deepEqual(this.pageObject.optionsText(), ["something"], "FieldOption not added.");
        equal(this.pageObject.option(), "Something", "FieldOption input not cleared.");
        ok(this.pageObject.error(), "FieldOption error returned.");

        this.pageObject.option("something2")[submit]();
        deepEqual(this.pageObject.optionsText(), ["something", "something2"], "FieldOption added.");
        ok(!this.pageObject.option(), "FieldOption cleared.");
    };

    test("add options btn", function () {
        assertAddOptions.call(this, "add");
    });

    test("add options enter", function () {
        assertAddOptions.call(this, "enter");
    });

    test("isDirty", function () {
        ok(!this.field.isDirty(), "Field should not be dirty.");

        //Setting value should leave it dirty.
        this.pageObject.option("something");
        ok(this.field.isDirty(), "Field is dirty while option being added.");
        this.pageObject.add();
        ok(this.field.isDirty(), "Field is dirty after option added.");
    });

    test("Delete", function () {
        this.pageObject.addOptions("jack", "jill", "hill");
        var options = this.pageObject.options();

        ok(options[0].deleteOption().isDeleted(), "Option was removed from DOM.");
        deepEqual(this.pageObject.optionsText(), ["jill", "hill"], "Option removed from model.");


        ok(options[2].deleteOption().isDeleted(), "Option was removed from DOM.");
        deepEqual(this.pageObject.optionsText(), ["jill"], "Option removed from model.");

        //Should not be possible to delete elements while disabled.
        this.field.disable();

        ok(!options[1].deleteOption().isDeleted(), "Option should not be deleted from DOM when view disabled.");
        deepEqual(this.pageObject.optionsText(), ["jill"], "Option not removed from model.");

        //Enable so we can delete the last element.
        this.field.enable();

        ok(options[1].deleteOption().isDeleted(), "Option was removed from DOM.");
        deepEqual(this.pageObject.optionsText(), [], "All options now removed.");
    });

    test("Reorder", function () {
        this.pageObject.addOptions("lemon", "melon", "apple", "banana", "strawberry");

        // Test basic reorder
        var oldIndex = 0;
        var newIndex = 4;
        var options = this.pageObject.options();
        options[oldIndex].moveOption(newIndex).moveOnDom(newIndex);
        deepEqual(this.field.value(), [{name: "melon"}, {name: "apple"}, {name: "banana"}, {name: "strawberry"}, {name: "lemon"}], "Correct options should be returned.");


        // Test second basic reorder
        oldIndex = 2;
        newIndex = 0;
        options = this.pageObject.options();
        options[oldIndex].moveOption(newIndex).moveOnDom(newIndex);
        deepEqual(this.field.value(), [{name: "banana"}, {name: "melon"}, {name: "apple"}, {name: "strawberry"}, {name: "lemon"}], "Correct options should be returned.");

        // Test when field is disabled, that options cannot be moved
        oldIndex = 3;
        newIndex = 2;
        this.field.disable();
        options = options[oldIndex].moveOption(newIndex);
        deepEqual(this.field.value(), [{name: "banana"}, {name: "melon"}, {name: "apple"}, {name: "strawberry"}, {name: "lemon"}], "Correct options should be returned.");
    });
});
