AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:custom-fields", "com.atlassian.jira.jira-project-config-plugin:custom-fields-impl"], function(){
    "use strict";

    var _ = require("underscore");
    var AbstractStep = require("jira-project-config/custom-fields/wizard/abstract-step");

    module("JIRA.Admin.CustomFields.Wizard.AbstractStep", {
        setup: function () {
            this.sandbox = sinon.sandbox.create();
            var StepImpl = function () {
            };
            _.extend(StepImpl.prototype, AbstractStep);
            this.step = new StepImpl();
        },
        teardown: function () {
            this.sandbox.restore();
        }
    });

    test("startStep() calls start() if not already started and restart() if already started", function () {
        var startStub = this.sandbox.stub(this.step, "start");
        var restartStub = this.sandbox.stub(this.step, "restart");

        this.step.startStep();
        ok(startStub.calledOnce, "start() should have been called once");
        ok(!restartStub.called, "restart() should not have been called");

        this.step.startStep();
        ok(startStub.calledOnce, "start() should have been called once");
        ok(restartStub.calledOnce, "restart() should have been called once");
    });

    test("getAccumulatedData() returns the combination of both previous data and current data", function () {
        this.step.setPreviousData({value1: "value1"});
        deepEqual(this.step.getAccumulatedData(), {value1: "value1"}, "getAccumulatedData() should return just the previous data");
        var getStepDataStub = this.sandbox.stub(this.step, "getStepData").returns({value2: "value2", value3: "value3"});
        deepEqual(this.step.getAccumulatedData(), {value1: "value1", value2: "value2", value3: "value3"},
            "getAccumulatedData() should return the combination of previous data and current data");
        getStepDataStub.returns({value1: "updatedValue", value2: "value2", value3: "value3"});
        deepEqual(this.step.getAccumulatedData(), {value1: "updatedValue", value2: "value2", value3: "value3"},
            "getAccumulatedData() should return the combination of previous data and current data and updated value1");
    });
});
