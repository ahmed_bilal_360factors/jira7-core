AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:custom-fields", "com.atlassian.jira.jira-project-config-plugin:custom-fields-impl"], function () {
    var Deferred = require("jira/jquery/deferred");
    var IssueCustomFields = require("jira-project-config/custom-fields/service");
    var FAIL_REASON = require("jira-project-config/custom-fields/wizard/fail-reasons");

    module("JIRA.Admin.CustomFields.CustomFieldsStepHelper", {
        createComplete: function (description) {
            return {
                execute: this.sandbox.stub(),
                description: this.sandbox.stub().returns(description)
            };
        },
        setup: function () {
            this.sandbox = sinon.sandbox.create();

            this.api = {
                addField: this.sandbox.stub(),
                addFieldToScreen: this.sandbox.stub(),
                setOptions: this.sandbox.stub()
            };
            this.data = {
                field: {
                    name: "name",
                    description: "description"
                },
                type: {
                    key: "typeKey",
                    searcherKey: "searcherKey"
                }
            };
            this.options = {
                api: this.api,
                data: this.data
            };
            this.issuesApi = {
                refreshSelectedIssue: this.sandbox.stub()
            };
            this.mockedContext = AJS.test.mockableModuleContext();
            this.mockedContext.mock("jira-project-config/issues/api", this.issuesApi);

            this.stepCompleteHelper = this.mockedContext.require("jira-project-config/custom-fields/steps/helper");
        },

        teardown: function () {
            this.sandbox.restore();
        }
    });

    test("createField.execute() returns resolved promise and sets field id when api resolves its promise with data", function () {
        var rejectedSpy = this.sandbox.spy();
        var resolvedSpy = this.sandbox.spy();
        var saving = Deferred();

        this.api.addField.returns(saving.promise());

        this.stepCompleteHelper.createField.execute(this.options).done(resolvedSpy).fail(rejectedSpy);

        ok(this.api.addField.calledOnce, "_createField called once.");

        ok(!resolvedSpy.called, "resolved has not yet been called");
        ok(!rejectedSpy.called, "rejected has not been called");

        saving.resolve({id: "id"});

        ok(resolvedSpy.calledOnce, "resolved has been called");
        ok(!rejectedSpy.called, "rejected has not been called");

        equal(this.data.field.id, "id", "field.id should have been set to 'id'");
    });

    test("createField.execute() returns rejected promise with correct status when api rejects its promise", function () {
        var rejectedSpy = this.sandbox.spy();
        var resolvedSpy = this.sandbox.spy();
        var saving = Deferred();

        this.api.addField.returns(saving.promise());

        this.stepCompleteHelper.createField.execute(this.options).done(resolvedSpy).fail(rejectedSpy);

        ok(this.api.addField.calledOnce, "_createField called once.");

        ok(!resolvedSpy.called, "resolved has not been called");
        ok(!rejectedSpy.called, "rejected has not yet been called");

        var errors = {error: "error"};
        saving.reject(errors);

        ok(!resolvedSpy.called, "resolved has not been called");
        ok(rejectedSpy.calledOnce, "rejected has been called once");
        ok(rejectedSpy.firstCall.calledWith(FAIL_REASON.recoverableError, errors), "rejected was called with recoverable error status and the errors");
    });

    test("addOptions.execute() returns resolved promise when api resolves its promise", function () {
        var adding = Deferred();
        var rejectedSpy = this.sandbox.spy();
        var resolvedSpy = this.sandbox.spy();

        this.api.setOptions.returns(adding.promise());
        this.stepCompleteHelper.addOptions.execute(this.options).done(resolvedSpy).fail(rejectedSpy);

        ok(!resolvedSpy.called, "resolved has not yet been called");
        ok(!rejectedSpy.called, "rejected has not been called");

        adding.resolve();

        ok(resolvedSpy.called, "resolved has been called called once");
        ok(!rejectedSpy.calledOnce, "rejected has not been called");
    });

    test("addOptions.execute() returns rejected promise with abort status when api rejects its promise with abort status", function () {
        var adding = Deferred();
        var rejectedSpy = this.sandbox.spy();
        var resolvedSpy = this.sandbox.spy();

        this.api.setOptions.returns(adding.promise());
        this.stepCompleteHelper.addOptions.execute(this.options).done(resolvedSpy).fail(rejectedSpy);

        ok(!resolvedSpy.called, "resolved has not been called");
        ok(!rejectedSpy.called, "rejected has not yet been called");

        adding.reject(IssueCustomFields.STATUS.abort);

        ok(!resolvedSpy.called, "resolved has not been called");
        ok(rejectedSpy.calledOnce, "rejected has been called once");
        ok(rejectedSpy.firstCall.calledWith(FAIL_REASON.abort), "rejected was called with abort status");
    });

    test("addOptions.execute() returns rejected promise with error status when api rejects its promise with not abort status", function () {
        var adding = Deferred();
        var rejectedSpy = this.sandbox.spy();
        var resolvedSpy = this.sandbox.spy();

        this.api.setOptions.returns(adding.promise());
        this.stepCompleteHelper.addOptions.execute(this.options).done(resolvedSpy).fail(rejectedSpy);

        ok(!resolvedSpy.called, "resolved has not been called");
        ok(!rejectedSpy.called, "rejected has not yet been called");

        adding.reject("any other status");

        ok(!resolvedSpy.called, "resolved has not been called");
        ok(rejectedSpy.calledOnce, "rejected has been called once");
        ok(rejectedSpy.firstCall.calledWith(FAIL_REASON.error), "rejected was called with error status");
    });

    test("putFieldOnIssue.execute() returns resolved promise when api resolves its promise", function () {
        var putting = Deferred();
        var rejectedSpy = this.sandbox.spy();
        var resolvedSpy = this.sandbox.spy();

        this.api.addFieldToScreen.returns(putting.promise());
        this.stepCompleteHelper.putFieldOnIssue.execute(this.options).done(resolvedSpy).fail(rejectedSpy);

        ok(!resolvedSpy.called, "resolved has not yet been called");
        ok(!rejectedSpy.called, "rejected has not been called");

        putting.resolve();

        ok(resolvedSpy.called, "resolved has been called called once");
        ok(!rejectedSpy.calledOnce, "rejected has not been called");
    });

    test("putFieldOnIssue.execute() returns rejected promise with abort status when api rejects its promise with abort status", function () {
        var putting = Deferred();
        var rejectedSpy = this.sandbox.spy();
        var resolvedSpy = this.sandbox.spy();

        this.api.addFieldToScreen.returns(putting.promise());
        this.stepCompleteHelper.putFieldOnIssue.execute(this.options).done(resolvedSpy).fail(rejectedSpy);

        ok(!resolvedSpy.called, "resolved has not been called");
        ok(!rejectedSpy.called, "rejected has not yet been called");

        putting.reject(IssueCustomFields.STATUS.abort);

        ok(!resolvedSpy.called, "resolved has not been called");
        ok(rejectedSpy.calledOnce, "rejected has been called once");
        ok(rejectedSpy.firstCall.calledWith(FAIL_REASON.abort), "rejected was called with abort status");
    });

    test("putFieldOnIssue.execute() returns rejected promise with error status when api rejects its promise with non abort status", function () {
        var putting = Deferred();
        var rejectedSpy = this.sandbox.spy();
        var resolvedSpy = this.sandbox.spy();

        this.api.addFieldToScreen.returns(putting.promise());
        this.stepCompleteHelper.putFieldOnIssue.execute(this.options).done(resolvedSpy).fail(rejectedSpy);

        ok(!resolvedSpy.called, "resolved has not been called");
        ok(!rejectedSpy.called, "rejected has not yet been called");

        putting.reject("any other status");

        ok(!resolvedSpy.called, "resolved has not been called");
        ok(rejectedSpy.calledOnce, "rejected has been called once");
        ok(rejectedSpy.firstCall.calledWith(FAIL_REASON.error), "rejected was called with error status");
    });

    test("refreshIssue.execute() returns resolved promise when api resolves its promise", function () {
        var refreshing = Deferred();
        var rejectedSpy = this.sandbox.spy();
        var resolvedSpy = this.sandbox.spy();

        this.issuesApi.refreshSelectedIssue.returns(refreshing.promise());
        this.stepCompleteHelper.refreshIssue.execute().done(resolvedSpy).fail(rejectedSpy);

        ok(!resolvedSpy.called, "resolved has not yet been called");
        ok(!rejectedSpy.called, "rejected has not been called");

        refreshing.resolve();

        ok(resolvedSpy.called, "resolved has been called called once");
        ok(!rejectedSpy.calledOnce, "rejected has not been called");
    });

    test("refreshIssue.execute() returns rejected promise with abort status when api rejects its promise with abort status", function () {
        var refreshing = Deferred();
        var rejectedSpy = this.sandbox.spy();
        var resolvedSpy = this.sandbox.spy();

        this.issuesApi.refreshSelectedIssue.returns(refreshing.promise());
        this.stepCompleteHelper.refreshIssue.execute().done(resolvedSpy).fail(rejectedSpy);

        ok(!resolvedSpy.called, "resolved has not been called");
        ok(!rejectedSpy.called, "rejected has not yet been called");

        refreshing.reject(IssueCustomFields.STATUS.abort);

        ok(!resolvedSpy.called, "resolved has not been called");
        ok(rejectedSpy.calledOnce, "rejected has been called once");
        ok(rejectedSpy.firstCall.calledWith(FAIL_REASON.abort), "rejected was called with abort status");
    });

    test("refreshIssue.execute() returns rejected promise with error status when api rejects its promise with any non abort status", function () {
        var refreshing = Deferred();
        var rejectedSpy = this.sandbox.spy();
        var resolvedSpy = this.sandbox.spy();

        this.issuesApi.refreshSelectedIssue.returns(refreshing.promise());
        this.stepCompleteHelper.refreshIssue.execute().done(resolvedSpy).fail(rejectedSpy);

        ok(!resolvedSpy.called, "resolved has not been called");
        ok(!rejectedSpy.called, "rejected has not yet been called");

        refreshing.reject("any other status");

        ok(!resolvedSpy.called, "resolved has not been called");
        ok(rejectedSpy.calledOnce, "rejected has been called once");
        ok(rejectedSpy.firstCall.calledWith(FAIL_REASON.error), "rejected was called with error status");
    });

    test("executeCompletes() returns resolved promise if no completes", function () {
        var spy = this.sandbox.spy();
        this.stepCompleteHelper.executeCompletes([]).done(spy);

        ok(spy.calledOnce, "spy should have been called once");
    });

    test("executeCompletes() returns resolved promise if all completes have resolved", function () {
        var completes;
        var firstDeferred = Deferred();
        var firstComplete = this.createComplete("first");
        var secondDeferred = Deferred();
        var secondComplete = this.createComplete("second");
        var thirdDeferred = Deferred();
        var thirdComplete = this.createComplete("third");
        var spy;

        firstComplete.execute.returns(firstDeferred.promise());
        secondComplete.execute.returns(secondDeferred.promise());
        thirdComplete.execute.returns(thirdDeferred.promise());

        completes = [firstComplete, secondComplete, thirdComplete];
        spy = this.sandbox.spy();
        this.stepCompleteHelper.executeCompletes(completes).done(spy);

        ok(firstComplete.execute.calledOnce, "firstComplete's execute should have been called once");
        ok(!secondComplete.execute.called, "secondComplete's execute should not yet have been called");
        ok(!thirdComplete.execute.called, "thirdComplete's execute should not yet have been called");
        ok(!spy.called, "executeCompletes' returned promise should not yet have been resolved");

        firstDeferred.resolve();

        ok(firstComplete.execute.calledOnce, "firstComplete's execute should have been called once");
        ok(secondComplete.execute.calledOnce, "secondComplete's execute should been called once");
        ok(!thirdComplete.execute.called, "thirdComplete's execute should not yet have been called");
        ok(!spy.called, "executeCompletes' returned promise should not yet have been resolved");

        secondDeferred.resolve();

        ok(firstComplete.execute.calledOnce, "firstComplete's execute should have been called once");
        ok(secondComplete.execute.calledOnce, "secondComplete's execute should been called once");
        ok(thirdComplete.execute.calledOnce, "thirdComplete's execute should been called once");
        ok(!spy.called, "executeCompletes' returned promise should not yet have been resolved");

        thirdDeferred.resolve();

        ok(spy.calledOnce, "executeCompletes' returned promise should have been resolved");
    });

    test("executeCompletes() returns rejected promise if any completes have rejected", function () {
        var completes;
        var firstDeferred = Deferred();
        var secondDeferred = Deferred();
        var thirdDeferred = Deferred();
        var firstComplete = this.createComplete("first");
        var secondComplete = this.createComplete("second");
        var thirdComplete = this.createComplete("third");
        var spy;

        completes = [firstComplete, secondComplete, thirdComplete];

        firstComplete.execute.returns(firstDeferred.promise());
        secondComplete.execute.returns(secondDeferred.promise());
        thirdComplete.execute.returns(thirdDeferred.promise());

        spy = this.sandbox.spy();
        this.stepCompleteHelper.executeCompletes(completes).fail(spy);

        ok(firstComplete.execute.calledOnce, "firstComplete's execute should have been called once");

        firstDeferred.reject();

        ok(spy.calledOnce, "executeCompletes' returned promise should have been rejected once");

        firstDeferred = Deferred();
        secondDeferred = Deferred();
        thirdDeferred = Deferred();
        firstComplete.execute.returns(firstDeferred.promise());
        secondComplete.execute.returns(secondDeferred.promise());
        thirdComplete.execute.returns(thirdDeferred.promise());

        this.stepCompleteHelper.executeCompletes(completes).fail(spy);
        firstDeferred.resolve();
        ok(firstComplete.execute.calledTwice, "firstComplete's execute should have been called twice");
        ok(secondComplete.execute.calledOnce, "secondComplete's execute should been called once");

        secondDeferred.reject();

        ok(spy.calledTwice, "executeCompletes' returned promise shoud have been rejected twice");

        firstDeferred = Deferred();
        secondDeferred = Deferred();
        thirdDeferred = Deferred();
        firstComplete.execute.returns(firstDeferred.promise());
        secondComplete.execute.returns(secondDeferred.promise());
        thirdComplete.execute.returns(thirdDeferred.promise());

        this.stepCompleteHelper.executeCompletes(completes).fail(spy);
        firstDeferred.resolve();
        secondDeferred.resolve();

        ok(firstComplete.execute.calledThrice, "firstComplete's execute should have been called thrice");
        ok(secondComplete.execute.calledTwice, "secondComplete's execute should been called twice");
        ok(thirdComplete.execute.calledOnce, "thirdComplete's execute should been called once");

        thirdDeferred.reject();

        ok(spy.calledThrice, "executeCompletes' returned promise should have been rejected thrice");
    });

    test("executeCompletes() returns rejects returned promise with correct parameters", function () {
        var completes;
        var complete = this.createComplete("myComplete");
        var deferred = Deferred();
        var otherParam = {value: "value"};
        var spy;

        completes = [complete];
        spy = this.sandbox.spy();
        complete.execute.returns(deferred.promise());

        this.stepCompleteHelper.executeCompletes(completes).fail(spy);
        deferred.reject(FAIL_REASON.error);

        ok(spy.calledOnce, "spy should have been called once");
        ok(spy.firstCall.calledWith(FAIL_REASON.error, ["myComplete"]),
            "Should have been rejected with correct parameters");

        deferred = Deferred();
        complete.execute.returns(deferred.promise());

        this.stepCompleteHelper.executeCompletes(completes).fail(spy);
        deferred.reject(FAIL_REASON.abort, otherParam);

        ok(spy.calledTwice, "spy should have been called twice");
        ok(spy.secondCall.calledWith(FAIL_REASON.abort, otherParam, ["myComplete"]),
            "Should have been rejected with correct parameters");

        deferred = Deferred();
        complete.execute.returns(deferred.promise());

        this.stepCompleteHelper.executeCompletes(completes).fail(spy);
        deferred.reject(FAIL_REASON.recoverableError, otherParam);

        ok(spy.calledThrice, "spy should have been called thrice");
        ok(spy.thirdCall.calledWith(FAIL_REASON.recoverableError, otherParam),
            "Should have been rejected with correct parameters");
    });
});
