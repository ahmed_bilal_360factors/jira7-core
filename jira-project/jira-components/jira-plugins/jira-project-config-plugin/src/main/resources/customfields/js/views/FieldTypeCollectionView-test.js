AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:custom-fields",
    "com.atlassian.jira.jira-project-config-plugin:custom-fields-impl"], function () {

    var $ = require("jquery");
    var _ = require("underscore");
    var Marionette = require("jira-project-config/marionette");
    var EmptyTypeItemView = require("jira-project-config/custom-fields/views/empty-type-item");
    var Types = require("jira-project-config/custom-fields/steps/type/models/types");
    var Backbone = require("jira-project-config/backbone");

    module("JIRA.Admin.CustomFields.FieldTypeCollectionView", {
        setup: function () {
            this.sandbox = sinon.sandbox.create();
            this.isSysadmin = this.sandbox.stub();
            this.context = AJS.test.mockableModuleContext();
            this.context.mock("jira/util/users/logged-in-user", {
                isSysadmin: this.isSysadmin
            });
            this.FieldTypeCollectionView = this.context.require('jira-project-config/custom-fields/views/field-type-collection');
            this.EmptyTypeItemView = EmptyTypeItemView;
            this.Backbone = Backbone;
            this.Types = Types;
            this.types = new this.Types([
                {
                    key: "a",
                    name: "A",
                    description: "A description"
                },
                {
                    key: "b",
                    name: "B",
                    description: "B description"
                },
                {
                    key: "c",
                    name: "C",
                    description: "C description"
                }
            ]);
            this.a = this.types.models[0];
            this.b = this.types.models[1];
            this.c = this.types.models[2];
        },
        teardown: function () {
            this.sandbox.restore();
        }
    });

    var PageObject = function ($el) {
        this.$el = $el;
    };

    _.extend(PageObject.prototype, {
        /**
         * Return the data represented in the DOM.
         *
         * @returns {Array}
         */
        data: function () {
            var result = [];

            this.$el.find("li.item").each(function () {
                var el = $(this);

                result.push({
                    name: el.find("h3").text(),
                    description: el.find("p").text(),
                    selected: el.hasClass("selected")
                });
            });

            return result;
        },

        hasEmpty: function () {
            return !!this.$el.find("li.empty").length;
        },

        /**
         * Returns the 0index of the selected item.
         *
         * @returns {number}
         */
        selectedIndex: function () {
            return this.$el.find(".customfields-types li.item.selected").index();
        },
        select: function (name) {
            this.$el.find("li.item").each(function () {
                var $el = $(this);
                if ($el.find("h3").text() === name) {
                    $el.click();
                    return false;
                } else {
                    return true;
                }
            });
        }
    });

    test("Custom field type items ordering is maintained despite add/remove from the collection", function () {
        var view = new this.FieldTypeCollectionView({
                collection: this.types
            });

        var pageObject = new PageObject(view.render().$el);

        deepEqual(_.pluck(pageObject.data(), "name"), ["A", "B", "C"], "Expected all types to be listed.");

        this.types.remove(this.b);

        deepEqual(_.pluck(pageObject.data(), "name"), ["A", "C"], "Expected B to be omitted.");

        this.types.add(this.b);

        deepEqual(_.pluck(pageObject.data(), "name"), ["A", "B", "C"], "Expected all types, in their original order.");

        this.types.remove(this.a);
        this.types.remove(this.b);
        this.types.remove(this.c);

        deepEqual(_.pluck(pageObject.data(), "name"), [], "Expected no types.");
    });

    test("When no types are in the collection, an empty message is displayed.", function () {
        var types = new this.Types([]);

        var view = new this.FieldTypeCollectionView({
            collection: types
        });

        var pageObject = new PageObject(view.render().$el);

        ok(pageObject.hasEmpty(), "Expected an empty message.");

        types.add(this.a);

        ok(!pageObject.hasEmpty(), "Expected the empty message to be removed.");
    });

    test("Modifying the collection resets the selection to the first item.", function () {
        var view = new this.FieldTypeCollectionView({
                collection: this.types
            });

        var pageObject = new PageObject(view.render().$el);

        equal(pageObject.selectedIndex(), 0, "Expected the first item to be selected.");

        view.selectByIndex(2);
        this.types.remove(this.a);
        equal(pageObject.selectedIndex(), 0, "Expected the new first item to still be selected.");

        view.selectByIndex(1);
        this.types.add(this.a);
        equal(pageObject.selectedIndex(), 0, "Expected the first item to be selected.");
    });

    test(".selectByIndex() uses 0-index", function () {
        var view = new this.FieldTypeCollectionView({
                collection: this.types
            });

        var pageObject = new PageObject(view.render().$el);

        equal(pageObject.selectedIndex(), 0, "Expected the first item to be selected.");

        view.selectByIndex(2);
        equal(pageObject.selectedIndex(), 2, "Expected the third item to be selected.");
    });

    test(".isEmpty() correctly reflects the current state", function () {
        var notEmpty = new this.FieldTypeCollectionView({
                collection: this.types
            });

        var empty = new this.FieldTypeCollectionView({
            collection: new this.Types([])
        });

        equal(notEmpty.isEmpty(), false, "Expected the view to think it's not empty.");
        equal(empty.isEmpty(), true, "Expected the view to think it is empty.");
    });

    test("._getEmptyView() returns the empty view instance when empty, otherwise returns false", function () {
        var view = new this.FieldTypeCollectionView();
        var isEmpty = this.sandbox.stub(view, "isEmpty");
        var EmptyView = Marionette.ItemView;
        var emptyView = new EmptyView();

        view.children.add(emptyView);

        isEmpty.returns(true);
        ok(view._getEmptyView() === emptyView, "Expected the empty view to be returned.");

        isEmpty.returns(false);
        ok(!view._getEmptyView(), "Expected no view to be returned.");
    });

    test("When empty, the empty view changes depending on if there are matches in other collections.", function () {
        var view = new this.FieldTypeCollectionView();

        var emptyView = {
            setEmptyState: this.sandbox.spy()
        };

        var _getEmptyView = this.sandbox.stub(view, "_getEmptyView");

        // If we don't have any types to show, the empty view should be configured.
        this.isSysadmin.returns(false);
        _getEmptyView.returns(emptyView); // means that we have no types to show.

        emptyView.setEmptyState.reset();
        //Searcher is *not* admin and there are no matches, then we should just show a simple no matches info message.
        view.setHasMatches(false); // normally called when the search changes
        ok(emptyView.setEmptyState.calledOnce, "Expected the empty view to be configured.");
        deepEqual(emptyView.setEmptyState.firstCall.args, [this.EmptyTypeItemView.EMPTY], "Expected the empty view to be told not to make any suggestions.");

        //Searcher is admin and their are no matches, then we should just show a marketplace info message.
        emptyView.setEmptyState.reset();
        this.isSysadmin.returns(true);
        view.setHasMatches(false); // normally called when the search changes
        ok(emptyView.setEmptyState.calledOnce, "Expected the empty view to be configured.");
        deepEqual(emptyView.setEmptyState.firstCall.args, [this.EmptyTypeItemView.MARKETPLACE], "Expected the empty view to suggest checking Marketplace.");

        //Searcher is admin and there are matches in other categories, just redirect them to the other panels.
        emptyView.setEmptyState.reset();
        this.isSysadmin.returns(true);
        view.setHasMatches(true); // normally called when the search changes
        ok(emptyView.setEmptyState.calledOnce, "Expected the empty view to be configured.");
        deepEqual(emptyView.setEmptyState.firstCall.args, [this.EmptyTypeItemView.CHANGE_TAB], "Expected the empty view to suggest other category.");

        //Searcher is *not* admin and there are matches in other categories, just redirect them to the other panels.
        emptyView.setEmptyState.reset();
        this.isSysadmin.returns(false);
        view.setHasMatches(true); // normally called when the search changes
        ok(emptyView.setEmptyState.calledOnce, "Expected the empty view to be configured.");
        deepEqual(emptyView.setEmptyState.firstCall.args, [this.EmptyTypeItemView.CHANGE_TAB], "Expected the empty view to suggest other category even when not sysadmin.");

        // There are currently matches, so we don't render the empty view.
        emptyView.setEmptyState.reset();
        _getEmptyView.returns(false);
        view.setHasMatches(true);
        view.setHasMatches(false); // cover both inputs
        ok(!emptyView.setEmptyState.called, "Never tried to render the view.");
    });

    test("When the collection is empty, there should not be a selected item.", function () {
        var initiallyEmpty = new this.FieldTypeCollectionView({
                collection: new this.Types([])
            });

        var initiallyFull = new this.FieldTypeCollectionView({
            collection: this.types
        });

        ok(!initiallyEmpty.getSelectedKey(), "When there's nothing in the collection, nothing should be selected.");
        ok(!initiallyEmpty.getSelectedModel(), "When there's nothing in the collection, nothing should be selected.");

        this.types.reset();
        ok(!initiallyFull.getSelectedKey(), "When the collection becomes empty, nothing should be selected.");
        ok(!initiallyFull.getSelectedModel(), "When the collection becomes empty, nothing should be selected.");
    });

    test("When the collection is populated, it should have a selected item.", function () {
        var empty = new this.Types([]);

        var initiallyEmpty = new this.FieldTypeCollectionView({
            collection: empty
        });

        var initiallyFull = new this.FieldTypeCollectionView({
            collection: this.types
        });

        initiallyEmpty.render();
        initiallyFull.render();

        empty.add({key: "foo", name: "Foo"});
        ok(initiallyEmpty.getSelectedKey(), "As soon as an item is added to an empty collection, it should be selected.");
        ok(initiallyEmpty.getSelectedModel(), "As soon as an item is added to an empty collection, it should be selected.");

        ok(initiallyFull.getSelectedKey(), "Collections that are already full, should have a selected item.");
        ok(initiallyFull.getSelectedModel(), "Collections that are already full, should have a selected item.");
    });

    test("Able to get selected key from view.", function () {
        var view = new this.FieldTypeCollectionView({
            collection: this.types
        });

        var driver = new PageObject(view.render().$el);

        equal(this.types.at(0).get('key'), view.getSelectedKey(), "Correct key being returned.");
        ok(this.types.at(0) === view.getSelectedModel(), "Correct model being returned.");

        for (var i = 0; i < this.types.length; i++) {
            var model = this.types.at(i);
            var name = model.get('name');
            driver.select(name);

            equal(view.getSelectedKey(), model.get('key'), "Correct key being returned for '" + name + "'.");
            ok(model === view.getSelectedModel(), "Correct model being returned for '" + name + "'.");
        }
    });
});
