AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:custom-fields",
    "com.atlassian.jira.jira-project-config-plugin:custom-fields-impl"], function () {

    var _ = require('underscore');
    var AJSHelper = require('jira-project-config/libs/ajshelper');
    var Loading = require("jira/loading/loading");
    var Wizard = require("jira-project-config/custom-fields/wizard");
    var Deferred = require("jira/jquery/deferred");
    var EVENTS = require("jira-project-config/custom-fields/wizard/events");
    var FAIL_REASON = require("jira-project-config/custom-fields/wizard/fail-reasons");
    var IssueCustomFields = require("jira-project-config/custom-fields/service");

    //Here to make sure that global references don't work.
    var LOADING_TIMEOUT = 1000;

    module("Wizard", {
        setup: function () {
            this.sandbox = sinon.sandbox.create();
            this.showLoading = this.sandbox.stub(Loading, "showLoadingIndicator");
            this.hideLoading = this.sandbox.stub(Loading, "hideLoadingIndicator");
            this.dim = this.sandbox.stub(AJSHelper, "dim");

            this.context = AJS.test.mockableModuleContext();
            this.context.mock("jira-project-config/libs/ajshelper", AJSHelper);
            this.context.mock("jira/loading/loading", Loading);
            this.Wizard = this.context.require('jira-project-config/custom-fields/wizard');
            this.WizardStep = this.context.require('jira-project-config/custom-fields/wizard/step');

            this.wizard = new this.Wizard({
                firstStep: this.createNewStep()
            });

            this.oldUnload = window.onbeforeunload;
            window.onbeforeunload = null;
        },

        createNewStep: function () {
            var stepClass = function () {
            };
            _.extend(stepClass.prototype, this.WizardStep, {
                stopStep: this.sandbox.stub(),
                pauseStep: this.sandbox.stub(),
                show: this.sandbox.stub(),
                showErrors: this.sandbox.stub(),
                showLoading: this.sandbox.stub(),
                hideLoading: this.sandbox.stub(),
                isDirty: this.sandbox.stub(),
                getNextStep: this.sandbox.stub(),
                doComplete: this.sandbox.stub(),
                getAccumulatedData: this.sandbox.stub()
            });
            this.sandbox.stub(stepClass.prototype, "startStep", function () {
                this.stepStartDeferred = Deferred();
                return this.stepStartDeferred.promise();
            });
            return new stepClass();
        },

        teardown: function () {
            this.wizard.close();
            this.sandbox.restore();
            window.onbeforeunload = this.oldUnload;
        },

        resolveStepStartDeferred: function (step) {
            step.stepStartDeferred.resolve();
        },

        notifyStepStartDeferred: function (step, args) {
            step.stepStartDeferred.notify.apply(step.stepStartDeferred, args);
        }
    });

    //We need to ensure that the setTimeout is restored before the test is finished as otherwise Qunit will probably
    //fail (it uses setTimeout internally).
    var timerTest = function (func) {
        return function () {
            var clock = this.clock = this.sandbox.useFakeTimers();
            try {
                func.apply(this, arguments);
            } finally {
                clock.restore();
            }
        };
    };

    var safeTest = function (name, func) {
        test.call(this, name, timerTest(func));
    };

    safeTest("Loading hidden on first step done", function () {
        this.wizard.start();

        ok(!this.showLoading.called, "Showed loading not called.");
        ok(!this.dim.called, "Dimmed not called.");

        this.clock.tick(LOADING_TIMEOUT);

        ok(this.showLoading.calledOnce, "Showed loading");
        ok(this.dim.calledOnce, "Dimmed the screen");
        ok(!this.hideLoading.calledOnce, "Hide loading should not be called until after ready.");

        this.resolveStepStartDeferred(this.wizard.currentStepWrapper.step);

        ok(this.hideLoading.calledOnce, "Hide loading called after ready.");
    });

    safeTest("_bindToStep called when first step done", function () {
        this.sandbox.spy(this.wizard, "_bindToStep");

        this.wizard.start();
        ok(!this.wizard._bindToStep.called, "_bindToStep should not yet be called.");
        this.resolveStepStartDeferred(this.wizard.currentStepWrapper.step);
        ok(this.wizard._bindToStep.calledOnce, "_bindToStep should have been called once.");
    });

    safeTest("Events bound to current step on call to _bindCurrentStep", function () {
        this.sandbox.spy(this.wizard, "listenTo");

        this.wizard.start();

        ok(!this.wizard.listenTo.called, "listenTo should not yet be called.");

        var firstStep = this.wizard.currentStepWrapper.step;

        this.resolveStepStartDeferred(firstStep);

        equal(this.wizard.listenTo.callCount, 4, "listenTo should have been called twice.");
        ok(this.wizard.listenTo.getCall(0).calledWith(firstStep, EVENTS.next), "Should have had next event bound");
        ok(this.wizard.listenTo.getCall(1).calledWith(firstStep, EVENTS.previous), "Should have had previous event bound");
        ok(this.wizard.listenTo.getCall(2).calledWith(firstStep, EVENTS.complete), "Should have had complete event bound");
        ok(this.wizard.listenTo.getCall(3).calledWith(firstStep, EVENTS.cancel), "Should have had cancel event bound");
    });

    safeTest("Step firing next event should call _onNextStep()", function () {
        this.sandbox.stub(this.wizard, "_onNextStep");

        this.wizard.start();
        var firstStep = this.wizard.currentStepWrapper.step;
        this.resolveStepStartDeferred(firstStep);

        ok(!this.wizard._onNextStep.called, "_onNextStep should not have been called.");

        firstStep.trigger(EVENTS.next);

        ok(this.wizard._onNextStep.calledOnce, "_onNextStep should have been called once.");

    });

    safeTest("Step firing previous event should call _onPreviousStep()", function () {
        this.sandbox.spy(this.wizard, "_onPreviousStep");

        this.wizard.start();
        var firstStep = this.wizard.currentStepWrapper.step;
        this.resolveStepStartDeferred(firstStep);

        ok(!this.wizard._onPreviousStep.called, "_onPreviousStep should not have been called.");

        firstStep.trigger(EVENTS.previous);

        ok(this.wizard._onPreviousStep.calledOnce, "_onPreviousStep should have been called once.");

    });

    safeTest("Wizard goes to next step when no existing next step", function () {
        this.wizard.start();
        var firstStepWrapper = this.wizard.currentStepWrapper;
        var firstStep = firstStepWrapper.step;
        this.resolveStepStartDeferred(firstStep);

        var secondStep = this.createNewStep();
        firstStep.getNextStep.returns(secondStep);

        this.wizard._onNextStep();

        var secondStepWrapper = this.wizard.currentStepWrapper;

        ok(firstStepWrapper !== secondStepWrapper, "New current step wrapper should not be the same as the old one");
        ok(secondStepWrapper.previousStepWrapper === firstStepWrapper, "New current step wrapper's previousStepWrapper should be the old one.");
        ok(!secondStepWrapper.nextStepWrapper, "New current step wrapper's nextStepWrapper should be falsey.");
    });

    safeTest("Wizard goes to next step when existing next step is appropriate for being the new next step", function () {
        this.wizard.start();
        var firstStepWrapper = this.wizard.currentStepWrapper;
        var firstStep = firstStepWrapper.step;
        var data = {value: "value"};
        firstStep.getAccumulatedData.returns(data);

        this.resolveStepStartDeferred(firstStep);

        var secondStep = this.createNewStep();
        firstStep.getNextStep.returns(secondStep);

        this.wizard._onNextStep();
        var secondStepWrapper = this.wizard.currentStepWrapper;

        this.sandbox.spy(secondStep, "setPreviousData");

        this.resolveStepStartDeferred(secondStep);

        this.wizard._onPreviousStep();
        this.resolveStepStartDeferred(firstStep);

        this.wizard._onNextStep();
        this.resolveStepStartDeferred(secondStep);

        ok(firstStepWrapper !== this.wizard.currentStepWrapper, "Current step wrapper should not be the first step wrapper");
        ok(secondStepWrapper === this.wizard.currentStepWrapper, "Current step wrapper should be the old second step wrapper");

        ok(secondStepWrapper.previousStepWrapper === firstStepWrapper, "New current step wrapper's previousStepWrapper should be the old one.");
        ok(!secondStepWrapper.nextStepWrapper, "New current step wrapper's nextStepWrapper should be falsey.");

        ok(secondStep.setPreviousData.calledOnce, "setData() should have been called once.");
        ok(secondStep.setPreviousData.calledWith(data), "setData() should have been called with expected data.");
    });

    safeTest("Wizard goes to next step when existing next step is not appropriate for being the new next step", function () {
        this.wizard.start();
        var firstStepWrapper = this.wizard.currentStepWrapper;
        var firstStep = firstStepWrapper.step;

        this.resolveStepStartDeferred(firstStep);

        var secondStepA = this.createNewStep();
        firstStep.getNextStep.returns(secondStepA);
        this.wizard._onNextStep();
        var secondStepWrapper = this.wizard.currentStepWrapper;

        this.resolveStepStartDeferred(secondStepA);

        this.wizard._onPreviousStep();
        this.resolveStepStartDeferred(firstStep);

        var secondStepB = this.createNewStep();
        firstStep.getNextStep.returns(secondStepB);

        this.wizard._onNextStep();
        this.resolveStepStartDeferred(secondStepB);

        ok(firstStepWrapper !== this.wizard.currentStepWrapper, "Current step wrapper should not be the first step wrapper");
        ok(secondStepWrapper !== this.wizard.currentStepWrapper, "Current step wrapper should not be the old second step wrapper");

        ok(secondStepWrapper.previousStepWrapper === firstStepWrapper, "New current step wrapper's previousStepWrapper should be the old one.");
        ok(!secondStepWrapper.nextStepWrapper, "New current step wrapper's nextStepWrapper should be falsey.");
    });

    safeTest("Wizard goes to previous step", function () {
        this.wizard.start();
        var firstStepWrapper = this.wizard.currentStepWrapper;
        var firstStep = firstStepWrapper.step;
        this.resolveStepStartDeferred(firstStep);

        var secondStep = this.createNewStep();
        firstStep.getNextStep.returns(secondStep);

        this.wizard._onNextStep();

        var secondStepWrapper = this.wizard.currentStepWrapper;
        this.resolveStepStartDeferred(secondStepWrapper.step);

        ok(secondStepWrapper !== firstStepWrapper, "The current step wrapper should not match the first step wrapper");

        this.wizard._onPreviousStep();

        var previousStepWrapper = this.wizard.currentStepWrapper;
        this.resolveStepStartDeferred(previousStepWrapper.step);

        ok(previousStepWrapper === firstStepWrapper, "The previous step wrapper should match the first step wrapper");
    });

    safeTest("_bindToStepProgress() shows loading on previous step", function () {
        var fromStep = this.createNewStep();

        this.wizard._bindToStepProgress(Deferred().promise(), fromStep);

        ok(!fromStep.showLoading.called, "showLoading() on the fromStep should not yet have been called.");
        this.clock.tick(LOADING_TIMEOUT);
        ok(fromStep.showLoading.calledOnce, "showLoading() on the fromStep should have been called once.");
    });

    safeTest("_bindToStepProgress() hides loading on wizard when WebSudo show detected, if no currentlyVisibleStep provided", function () {
        var hideLoadingSpy = this.sandbox.spy(this.wizard, "hideLoading");
        var deferred = Deferred();
        this.wizard._bindToStepProgress(deferred.promise(), null);

        ok(!hideLoadingSpy.called, "hideLoading() should not yet have been called");
        deferred.notify(EVENTS.websudoBeforeShow);
        this.clock.tick(LOADING_TIMEOUT);
        ok(hideLoadingSpy.calledOnce, "hideLoading() should have been called once");
    });

    safeTest("_bindToStepProgress() pauses currently visible step when WebSudo show detected", function () {
        var deferred = Deferred();
        this.wizard.start();
        var currentlyVisibleStep = this.createNewStep();

        this.wizard._bindToStepProgress(deferred.promise(), currentlyVisibleStep);

        ok(!currentlyVisibleStep.pauseStep.called, "pauseStep() on the currently visible step should not yet have been called");
        deferred.notify(EVENTS.websudoBeforeShow);
        ok(currentlyVisibleStep.pauseStep.calledOnce, "pauseStep() on the currently visible step should have been called once");
    });

    safeTest("_bindToStepProgress() hides loading on currently visible step when WebSudo show detected", function () {
        var deferred = Deferred();
        var currentlyVisibleStep = this.createNewStep();

        this.wizard._bindToStepProgress(deferred.promise(), currentlyVisibleStep);

        ok(!currentlyVisibleStep.hideLoading.called, "hideLoading() on the from step should not yet have been called");
        deferred.notify(EVENTS.websudoBeforeShow);

        this.clock.tick(LOADING_TIMEOUT);

        ok(currentlyVisibleStep.hideLoading.calledOnce, "hideLoading() on the from step should have been called once");
    });

    safeTest("_bindToStepProgress() always stops listening to currently visible step when defined", function () {
        var deferred = Deferred();
        var stopListeningSpy = this.sandbox.spy(this.wizard, "stopListening");

        var currentlyVisibleStep = this.createNewStep();

        this.wizard._bindToStepProgress(deferred.promise(), currentlyVisibleStep);

        ok(stopListeningSpy.calledOnce, "stopListening() should have been called once");
    });

    safeTest("_transitionTo binds to step progress", function () {
        var firstStep = this.createNewStep();
        var secondStep = this.createNewStep();

        var bindToStepProgressSpy = this.sandbox.spy(this.wizard, "_bindToStepProgress");
        this.wizard._transitionTo(firstStep, secondStep);
        ok(bindToStepProgressSpy.calledOnce, "_bindToStepProgress() should have been called once");
    });

    safeTest("_transitionTo() always calls show() on the next step", function () {
        var fromStep;
        var toStep;
        // No previous step
        toStep = this.createNewStep();

        this.wizard._transitionTo(null, toStep);

        ok(!toStep.show.called, "toStep's show() should not yet have been called");
        this.resolveStepStartDeferred(toStep);
        ok(toStep.show.calledOnce, "toStep's show() should have been called once");

        // With previous step
        fromStep = this.createNewStep();

        this.wizard._transitionTo(fromStep, toStep);

        this.resolveStepStartDeferred(toStep);
        ok(toStep.show.calledTwice, "toStep's show() should have been called twice");

        // With WebSudo show and success
        this.wizard._transitionTo(fromStep, toStep);

        this.notifyStepStartDeferred(toStep, [IssueCustomFields.PROGRESS.SHOW]);
        this.notifyStepStartDeferred(toStep, [IssueCustomFields.PROGRESS.SUCCESS, this.sandbox.stub()]);
        this.resolveStepStartDeferred(toStep);
        ok(toStep.show.calledThrice, "toStep's show() should have been called thrice");
    });

    safeTest("_transitionTo() always calls _bindToStep() passing through the next step", function () {
        var bindToStepSpy = this.sandbox.spy(this.wizard, "_bindToStep");
        var fromStep;
        var toStep;
        // No previous step
        toStep = this.createNewStep();

        this.wizard._transitionTo(null, toStep);

        ok(!bindToStepSpy.called, "Wizard's _bindToStep() should not yet have been called");
        this.resolveStepStartDeferred(toStep);
        ok(bindToStepSpy.calledOnce, "Wizard's _bindToStep() should have been called once");

        // With previous step
        fromStep = this.createNewStep();

        this.wizard._transitionTo(fromStep, toStep);

        this.resolveStepStartDeferred(toStep);
        ok(bindToStepSpy.calledTwice, "Wizard's _bindToStep() should have been called twice");

        // With WebSudo show and success
        this.wizard._transitionTo(fromStep, toStep);

        this.notifyStepStartDeferred(toStep, [EVENTS.websudoBeforeShow]);
        this.notifyStepStartDeferred(toStep, [EVENTS.websudoSuccess, this.sandbox.stub()]);
        this.resolveStepStartDeferred(toStep);
        ok(bindToStepSpy.calledThrice, "Wizard's _bindToStep() should have been called thrice");
    });

    safeTest("_transitionTo() always pauses previous step if provided and there is no WebSudo show detected", function () {
        var fromStep = this.wizard.currentStepWrapper.step;
        var toStep = this.createNewStep();

        this.wizard._transitionTo(fromStep, toStep);

        ok(!fromStep.pauseStep.called, "currentlyVisibleStep's stop should not yet have been called");
        this.resolveStepStartDeferred(toStep);
        ok(fromStep.pauseStep.calledOnce, "currentlyVisibleStep's stop should have been called once");
    });

    safeTest("_transitionTo() always calls close function of WebSudo dialog after its success, and promise done", function () {
        var toStep = this.createNewStep();
        var closeFunction = this.sandbox.spy();

        this.wizard._transitionTo(this.wizard.currentStepWrapper.step, toStep);

        ok(!closeFunction.called, "close function should not yet have been called");
        this.notifyStepStartDeferred(toStep, [EVENTS.websudoSuccess, closeFunction]);
        ok(!closeFunction.called, "close function should not yet have been called");
        this.resolveStepStartDeferred(toStep);
        ok(closeFunction.calledOnce, "close function should have been called once");
    });

    safeTest("_onComplete() completes the current step and calls appropriate success and failure handlers", function () {
        var deferred = Deferred();
        var onCompleteSuccessStub = this.sandbox.stub(this.wizard, "_onCompleteSuccess");
        var onCompleteFailureStub = this.sandbox.stub(this.wizard, "_onCompleteFailure");

        this.wizard.start();
        var currentStep = this.wizard.currentStepWrapper.step;
        currentStep.doComplete.returns(deferred.promise());

        this.wizard._onComplete();
        ok(currentStep.doComplete.calledOnce, "doComplete should have been called");
        ok(!onCompleteSuccessStub.called, "_onCompleteSuccess() should not yet have been called");
        ok(!onCompleteFailureStub.called, "_onCompleteFailure() should not yet have been called");

        deferred.resolve();

        ok(onCompleteSuccessStub.calledOnce, "_onCompleteSuccess() should have been called");

        deferred = Deferred();
        currentStep.doComplete.returns(deferred.promise());

        this.wizard._onComplete();
        ok(!onCompleteFailureStub.called, "_onCompleteFailure() should not yet have been called");

        deferred.reject();

        ok(onCompleteFailureStub.calledOnce, "_onCompleteFailure() should have been called");
    });

    safeTest("_onComplete() binds to step progress", function () {
        var deferred = Deferred();
        this.wizard.start();

        var bindToStepProgressStub = this.sandbox.stub(this.wizard, "_bindToStepProgress");

        this.wizard.currentStepWrapper.step.doComplete.returns(deferred.promise());

        this.wizard._onComplete();
        ok(bindToStepProgressStub.calledOnce, "_bindToStepProgress() should have been called once");
    });


    safeTest("Close wizard stops all steps", function () {
        this.wizard.start();
        var firstStep = this.wizard.currentStepWrapper.step;
        this.resolveStepStartDeferred(firstStep);

        var secondStep = this.createNewStep();
        firstStep.getNextStep.returns(secondStep);
        this.wizard._onNextStep();
        this.resolveStepStartDeferred(secondStep);

        var thirdStep = this.createNewStep();
        secondStep.getNextStep.returns(thirdStep);
        this.wizard._onNextStep();
        this.resolveStepStartDeferred(thirdStep);

        this.wizard.close();

        ok(firstStep.stopStep.calledOnce, "First step's close method should be called once");
        ok(secondStep.stopStep.calledOnce, "Second step's close method should be called once");
        ok(thirdStep.stopStep.calledOnce, "Third step's close method should be called once");

    });

    safeTest("Cancel on step closes the wizard.", function () {
        var creating = this.wizard.start();
        var onReject = this.sandbox.stub();
        var onResolve = this.sandbox.stub();

        this.sandbox.spy(this.wizard, "close");

        creating.done(onResolve);
        creating.fail(onReject);

        var step = this.wizard.currentStepWrapper.step;
        this.resolveStepStartDeferred(step);
        step.fireCancel(false);

        ok(this.wizard.close.calledOnce, "Cancel closes the wizard.");

        ok(!onResolve.called, "wizard result not resolved");
        ok(onReject.calledOnce, "wizard result rejected");
        ok(onReject.firstCall.calledWith("cancel"), "Cancel reason passed.");
    });

    safeTest("isDirty() checks dirtiness of all steps", function () {
        this.wizard.start();
        var firstStep = this.wizard.currentStepWrapper.step;
        this.resolveStepStartDeferred(firstStep);

        var secondStep = this.createNewStep();
        firstStep.getNextStep.returns(secondStep);
        this.wizard._onNextStep();
        this.resolveStepStartDeferred(secondStep);

        var thirdStep = this.createNewStep();
        secondStep.getNextStep.returns(thirdStep);
        this.wizard._onNextStep();
        this.resolveStepStartDeferred(thirdStep);

        firstStep.isDirty.returns(false);
        secondStep.isDirty.returns(false);
        thirdStep.isDirty.returns(false);

        ok(!this.wizard.isDirty(), "Wizard should not be dirty");

        firstStep.isDirty.returns(true);

        ok(this.wizard.isDirty(), "Wizard should be dirty");
    });

    safeTest("Wizard leave page while dirty.", function () {
        var dirtyMessage = "dirty";
        var wizard = new Wizard({
            firstStep: this.createNewStep(),
            dirtyMessage: dirtyMessage
        });

        wizard.start();
        this.sandbox.stub(wizard, "isDirty").returns(true);

        equal(window.onbeforeunload(), dirtyMessage, "Dirty message returned.");

        wizard.close();
    });

    safeTest("Wizard leave page while clean.", function () {
        var dirtyMessage = "dirty";
        var wizard = new Wizard({
            firstStep: this.createNewStep(),
            dirtyMessage: dirtyMessage
        });

        wizard.start();
        this.sandbox.stub(wizard, "isDirty").returns(false);

        ok(!window.onbeforeunload(), "No form is not dirty.");

        wizard.close();
    });

    safeTest("completes successfully finish.", function () {
        var dirtyMessage = "dirty";
        var firstStep = this.createNewStep();
        var data = {value: "value"};
        firstStep.getAccumulatedData.returns(data);
        var wizard = new Wizard({
            firstStep: firstStep,
            dirtyMessage: dirtyMessage
        });

        var finishedSpy = this.sandbox.spy();
        wizard.start().done(finishedSpy);
        //The wizard has successfully completed.
        wizard._onCompleteSuccess();

        ok(finishedSpy.calledOnce, "finishedSpy should have been called once");
        ok(finishedSpy.firstCall.calledWith(data), "finishedSpy should have been called with data");

        ok(!window.onbeforeunload(), "No form is not dirty.");

        wizard.close();

        ok(!window.onbeforeunload, "No onbeforeunload after close.");
    });

    safeTest("completes do not successfully finish with recoverable error with no websudo dialog.", function () {
        var dirtyMessage = "dirty";
        var firstStep = this.createNewStep();
        var wizard = new Wizard({
            firstStep: firstStep,
            dirtyMessage: dirtyMessage
        });

        var finishedSpy = this.sandbox.spy();
        this.sandbox.spy(wizard, "_bindToStep");

        wizard.start().fail(finishedSpy);
        //The wizard has unsuccessfully completed.
        var errors = {value: "value"};
        wizard._onCompleteFailure(FAIL_REASON.recoverableError, errors);

        ok(wizard._bindToStep.calledOnce, "_bindToStep() should have been called once");
        ok(firstStep.showErrors.calledOnce, "step should show errors");
    });

    safeTest("completes do not successfully finish with recoverable error with websudo dialog.", function () {
        var dirtyMessage = "dirty";
        var firstStep = this.createNewStep();
        var wizard = new Wizard({
            firstStep: firstStep,
            dirtyMessage: dirtyMessage
        });
        var finishedSpy = this.sandbox.spy();
        this.sandbox.spy(wizard, "_bindToStep");

        wizard.start().fail(finishedSpy);
        var websudoDialog = wizard.websudoDialog = {
            close: this.sandbox.spy()
        };
        //The wizard has unsuccessfully completed.
        var errors = {value: "value"};
        wizard._onCompleteFailure(FAIL_REASON.recoverableError, errors);

        ok(websudoDialog.close.calledOnce, "Websudo dialog should have been closed");
        ok(!wizard.websudoDialog, "websudoDialog should no longer exist");
        ok(firstStep.show.calledOnce, "step should have been reshown");
        ok(wizard._bindToStep.calledOnce, "_bindToStep() should have been called once");
        ok(firstStep.showErrors.calledOnce, "step should show errors");
    });

    safeTest("completes do not successfully finish with non recoverable error.", function () {
        var dirtyMessage = "dirty";
        var firstStep = this.createNewStep();
        var data = {value: "value"};
        firstStep.getAccumulatedData.returns(data);
        var wizard = new Wizard({
            firstStep: firstStep,
            dirtyMessage: dirtyMessage
        });

        var finishedSpy = this.sandbox.spy();
        wizard.start().fail(finishedSpy);

        var notCompletedDescriptions = {notCompleted: "blah"};
        wizard._onCompleteFailure(FAIL_REASON.error, notCompletedDescriptions);

        var expectedOptions = {
            data: data,
            dirty: false,
            notCompletedDescriptions: notCompletedDescriptions
        };

        ok(finishedSpy.calledOnce, "finishedSpy should have been called once");
        ok(finishedSpy.firstCall.calledWith(FAIL_REASON.error, expectedOptions),
            "finishedSpy should have been called with correct failure information");

        ok(!window.onbeforeunload, "No onbeforeunload after close.");
    });

    safeTest("Wizard leave page when no dirty message.", function () {
        var wizard = new Wizard({
            firstStep: this.createNewStep()
        });

        wizard.start();
        this.sandbox.stub(wizard, "isDirty").returns(true);

        ok(!window.onbeforeunload, "Called event handler not active.");

        wizard.close();
    });

    safeTest("Wizard onbeforeunload delegates to old onbeforeunload correctly", function () {
        var dirtyMessage = "Message";
        var wizard = new Wizard({
            firstStep: this.createNewStep(),
            dirtyMessage: dirtyMessage
        });

        var oldBefore = window.onbeforeunload = this.sandbox.stub();
        oldBefore.returns("Bad");

        wizard.start();
        var isDirtyStub = this.sandbox.stub(wizard, "isDirty").returns(false);

        equal(window.onbeforeunload(), "Bad", "Delegated call to old onbefore.");
        oldBefore.returns(undefined);
        ok(!window.onbeforeunload(), "Delegated call to old onbefore.");

        //Make sure we don't call old method if wizard in error.
        oldBefore.reset();
        isDirtyStub.returns(true);
        equal(window.onbeforeunload(), dirtyMessage, "Wizard message has priority.");
        ok(!oldBefore.called, "Delegate method not called when wizard has error.");

        wizard.close();
        strictEqual(window.onbeforeunload, oldBefore, "On close the wizard should no longer be dirty.");
    });

    safeTest("Wizard onbeforeunload works when dirty", function () {
        var dirtyMessage = "Message";
        var wizard = new Wizard({
            firstStep: this.createNewStep(),
            dirtyMessage: dirtyMessage
        });

        wizard.start();
        var isDirtyStub = this.sandbox.stub(wizard, "isDirty").returns(false);

        strictEqual(window.onbeforeunload(), void 0, "No wizard error returns undefined.");

        isDirtyStub.returns(true);
        equal(window.onbeforeunload(), dirtyMessage, "Wizard message has priority.");

        wizard.close();
        strictEqual(window.onbeforeunload, null, "Set the handler to null when wizard closes.");
    });

    safeTest("Wizard cancel with confirm when step dirty.", function () {
        var confirm = this.sandbox.stub(window, "confirm");
        //Check that the user can cancel the cancel.
        confirm.returns(false);

        var dirtyMessage = "dirty";
        var wizard = new Wizard({
            firstStep: this.createNewStep(),
            dirtyMessage: dirtyMessage
        });

        var onCancel = this.sandbox.stub();
        wizard.start().fail(onCancel);
        var step = wizard.currentStepWrapper.step;
        this.resolveStepStartDeferred(step);

        this.sandbox.stub(wizard, "isDirty").returns(true);
        step.fireCancel(true);

        ok(confirm.calledOnce, "Confirm called");
        ok(confirm.firstCall.calledWith(dirtyMessage), "Confirm called with right message");
        ok(!onCancel.called, "The cancel operation cancelled by user.");
        ok(!step.stopStep.called, "The step should not be closed since cancel cancelled.");

        //Check that the user can okay the cancel.
        confirm.returns(true);
        step.fireCancel(true);

        ok(confirm.calledTwice, "Confirm called");
        ok(confirm.secondCall.calledWith(dirtyMessage), "Confirm called with right message");
        ok(onCancel.calledOnce, "The cancel operation performed.");
        ok(step.stopStep.calledOnce, "The step should be closed");

        wizard.close();
    });

    safeTest("Wizard cancel confirm not shown when not dirty.", function () {
        var confirm = this.sandbox.stub(window, "confirm");
        var dirtyMessage = "dirty";
        var onCancel = this.sandbox.stub();
        var wizard = new Wizard({
            firstStep: this.createNewStep(),
            dirtyMessage: dirtyMessage
        });

        this.sandbox.stub(wizard, "isDirty").returns(false);

        //Check that the non dirty form is cancelled straight away.
        wizard.start().fail(onCancel);
        var step = wizard.currentStepWrapper.step;
        this.resolveStepStartDeferred(step);
        step.fireCancel(true);

        ok(!confirm.called, "Confirm not called.");
        ok(onCancel.calledOnce, "The cancel operation performed.");
        ok(step.stopStep.calledOnce, "The step should be closed");

        wizard.close();
    });

    safeTest("Wizard cancel confirm not shown when not asked to by step.", function () {
        var confirm = this.sandbox.stub(window, "confirm");
        var dirtyMessage = "dirty";
        var onCancel = this.sandbox.stub();
        var wizard = new Wizard({
            firstStep: this.createNewStep(),
            dirtyMessage: dirtyMessage
        });

        //The wizard is dirty, but this should be ignored.
        this.sandbox.stub(wizard, "isDirty").returns(true);

        //Check that the dirty form is cancelled straight away if the step asks.
        wizard.start().fail(onCancel);

        var step = wizard.currentStepWrapper.step;
        this.resolveStepStartDeferred(step);

        step.fireCancel(false);

        ok(!confirm.called, "Confirm not called.");
        ok(onCancel.calledOnce, "The cancel operation performed.");
        ok(step.stopStep.calledOnce, "The step should be closed");

        wizard.close();
    });

    safeTest("Wizard cancel confirm not shown when no dirty message", function () {
        var confirm = this.sandbox.stub(window, "confirm");
        var onCancel = this.sandbox.stub();
        var wizard = new Wizard({
            firstStep: this.createNewStep()
        });

        //The wizard is dirty, but this should be ignored.
        this.sandbox.stub(wizard, "isDirty").returns(true);

        //Check that the dirty form is cancelled straight away if no message.
        wizard.start().fail(onCancel);

        var step = wizard.currentStepWrapper.step;
        this.resolveStepStartDeferred(step);

        step.fireCancel(true);

        ok(!confirm.called, "Confirm not called.");
        ok(onCancel.calledOnce, "The cancel operation performed.");
        ok(step.stopStep.calledOnce, "The step should be closed");

        wizard.close();
    });
});
