require(['wrm/require', 'jquery'], function(wrmRequire, $) {
    function loadApi(callback) {
        wrmRequire(["wrc!com.atlassian.jira.jira-project-config-plugin.custom-fields-impl"], function () {
            /**
             * JSEV-834 - Require the APIs late, because their backing impl's may not be defined.
             * This is obviously bad pseudo-AMD. A proper, guaranteed-to-exist, promise-based API
             * should exist for issue data.
             */
            require(['jira-project-config/custom-fields/api', 'jira-project-config/issues/api'], callback);
        });
    }

    // Adding a custom field from the View Issue page.
    $(document).on("click", ".issueaction-fields-add", function (event) {
        event.preventDefault();
        loadApi(function (api, IssuesApi) {
            var issue = IssuesApi.getSelectedIssueId();
            api.addCustomFieldToIssue(issue);
        });
    });
});
