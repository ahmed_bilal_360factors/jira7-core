AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:custom-fields", "com.atlassian.jira.jira-project-config-plugin:custom-fields-impl"], function(){
    "use strict";

    var _ = require("underscore");
    var Deferred = require("jira/jquery/deferred");
    var SmartAjax = require("jira/ajs/ajax/smart-ajax");
    var WebSudoSmartAjax = require("jira/ajs/ajax/smart-ajax/web-sudo");
    var IssueCustomFields = require("jira-project-config/custom-fields/service");

    /**
     * Test error handling for an API method.
     *
     * @param {string} options.func The function to execute/test
     * @param {string} options.statusText The status text from the XHR
     * @param {string} [options.humanErrorMessage] A human readable error message describing an AJAX failure
     * @param {*[]} options.expectedErrors The errors passed to the fail handler.
     *
     * This function should be executed in the context of a test.
     */
    var assertStandardErrorHandler = function (options) {
        var smartAjaxResult = {};
        var errorBuilder = this.sandbox.stub(SmartAjax, "buildSimpleErrorContent");
        var ajaxing = Deferred();
        var _ajax = this.sandbox.stub(this.api, "_ajax");

        errorBuilder.withArgs(smartAjaxResult).returns(options.humanErrorMessage);

        _ajax.returns(ajaxing);

        var onDone = this.sandbox.stub();
        var onFail = this.sandbox.stub();

        this.api[options.func]("<ABC></ABC>", "field")
            .done(onDone)
            .fail(onFail);

        ok(!onDone.called, "Success should not be called.");
        ok(!onFail.called, "Fail should not have been called until ajaxing complete.");

        ajaxing.reject(options.statusText, smartAjaxResult);

        ok(onFail.calledOnce, "Fail should have been called.");
        deepEqual(onFail.firstCall.args, options.expectedErrors, "Should have failed with error and reason.");
    };

    module("JIRA.Admin.CustomFields.IssueCustomFields", {
        setup: function () {
            var self = this;

            this.issue = "<issue></issue>";
            this.sandbox = sinon.sandbox.create();
            this.api = new IssueCustomFields({issue: this.issue});
            this.assertError = function (response, mapping, errors, hasData) {
                var data = _.isString(response) ? response : JSON.stringify(response);

                var smartAjaxResponse = {
                    hasData: _.isUndefined(hasData) ? !!data : hasData,
                    data: data
                };

                self.sandbox.stub(SmartAjax, "buildSimpleErrorContent").returns("Simple error");
                var actual = this.api._parseErrorCollection(smartAjaxResponse, mapping);

                deepEqual(actual, errors);
            };
        },
        teardown: function () {
            this.sandbox.restore();
        }
    });

    var assertaddFieldAjaxCall = function (arg) {
        equal(arg.type, "POST", "AJAX call should have been a POST.");
        ok(arg.url.match(new RegExp("/rest/api/2/field$")), "Is AJAX URL correct?");
        equal(arg.dataType, "json", "Data type correct");
        equal(arg.contentType, "application/json", "Data type correct");
        deepEqual(JSON.parse(arg.data), _.omit(JSON.parse(arg.data), "random"), "AJAX call has correct body?");
    };

    test("addField creates a field (happy)", function () {
        var request = {
                name: "name",
                description: "description",
                searcherKey: "searcherKey",
                type: "type",
                random: "value"
            };

        var response = {id: "customField"};
        var ajax = this.sandbox.stub(this.api, "_ajax");
        var ajaxing = Deferred();
        var onDone = this.sandbox.stub();

        ajax.returns(ajaxing.promise());
        var adding = this.api.addField(request);
        adding.done(onDone);

        ok(ajax.calledOnce, "Ajax request should have been made.");
        var arg = ajax.firstCall.args[0];
        assertaddFieldAjaxCall(arg);

        ok(!onDone.called, "done handler not called until ajax returns");
        ajaxing.resolve(response);
        ok(onDone.calledOnce, "done handler called once");
        ok(onDone.firstCall.calledWith(response), "AJAX response returned verbatim.");
    });

    test("addField handles server error response", function () {
        var errors = {general: ["error"], fields: {}};

        var request = {
                name: "name",
                description: "description",
                searcherKey: "searcherKey",
                type: "type",
                random: "value"
            };

        var response = {something: "true"};
        var ajax = this.sandbox.stub(this.api, "_ajax");
        var ajaxing = Deferred();
        var onError = this.sandbox.stub();
        var _parseErrorCollection = this.sandbox.stub(this.api, "_parseErrorCollection").returns(errors);

        ajax.returns(ajaxing.promise());
        var adding = this.api.addField(request);
        adding.fail(onError);

        ok(ajax.calledOnce, "Ajax request should have been made.");
        var arg = ajax.firstCall.args[0];
        assertaddFieldAjaxCall(arg);

        ok(!onError.called, "error handler not called until ajax returns");
        ok(!_parseErrorCollection.called, "_parseErrorCollection not called yet.");
        ajaxing.reject("status text", response);

        ok(_parseErrorCollection.calledOnce, "Called _parseErrorCollection to deal with response.");
        ok(_parseErrorCollection.firstCall.calledWith(response, {fieldName: "name"}), "Called _parseErrorCollection with response.");

        ok(onError.calledOnce, "error handler called once");
        deepEqual(onError.firstCall.args[0], errors, "AJAX response returned verbatim.");
    });

    test("_parseErrorCollection splits errors into field and general", function () {
        var data = {
                errorMessages: ["General Error"],
                errors: {fieldName: "name", description: "something"}
            };

        var mapping = {fieldName: "foo"};

        this.assertError(data, mapping, {
            general: ["General Error"],
            fields: {
                foo: "name",
                description: "something"
            }
        });
    });

    test("_parseErrorCollection handles only general errors", function () {
        var data = {
                errorMessages: ["General Error"]
            };

        var mapping = {};

        this.assertError(data, mapping, {
            general: ["General Error"],
            fields: {}
        });
    });

    test("_parseErrorCollection handles only field errors", function () {
        var data = {
                errors: {fieldName: "name", description: "something"}
            };

        var mapping = {fieldName: "name"};

        this.assertError(data, mapping, {
            general: [],
            fields: {name: "name", description: "something"}
        });
    });

    test("_parseErrorCollection handles void", function () {
        this.assertError({}, {}, {
            general: ["Simple error"],
            fields: {}
        });
    });

    test("_parseErrorCollection handles hasData=false (but with data)", function () {
        var data = {
                errorMessages: ["General Error"],
                errors: {fieldName: "name", description: "something"}
            };

        var mapping = {};
        var hasData = false;

        this.assertError(data, mapping, {
            general: ["Simple error"],
            fields: {}
        }, hasData);
    });

    test("_parseErrorCollection handles JSON parse errors", function () {
        var data = "bro[k]en";
        var mapping = {};

        this.assertError(data, mapping, {
            general: ["Simple error"],
            fields: {}
        });
    });

    test("Default options", function () {
        var api = new IssueCustomFields();
        equal(api.autoCloseWebsudo, true, "Autoclose enabled by default.");
    });

    test("getSharedBy handles happy case", function () {
        var ajaxing = Deferred();
        var _ajax = this.sandbox.stub(this.api, "_ajax");
        var onDone = this.sandbox.stub();
        var onFail = this.sandbox.stub();
        var wrap = Deferred();
        var sharedBy = {a: true, b: false};

        _ajax.returns(ajaxing);

        this.api.getSharedBy(wrap)
            .done(onDone)
            .fail(onFail);

        ok(!_ajax.called, "Ajax should not be called until success of wrapped.");

        wrap.resolve({}, 1, 2);
        ok(_ajax.calledOnce, "Ajax should have been called once.");
        var ajaxOptions = _ajax.firstCall.args[0];
        strictEqual(ajaxOptions.type, "GET", "Ajax should be a get.");
        strictEqual(ajaxOptions.contentType, "application/json", "Body should be json");
        strictEqual(ajaxOptions.dataType, "json", "Response should be json.");

        var regex = new RegExp("/rest/globalconfig/1/issuecustomfields/" + encodeURIComponent(this.issue) + "/affectedProjects$");
        ok(regex.test(ajaxOptions.url), "URL is correct and encoded.");

        ok(!onDone.called, "Success should not be called until ajax returns.");
        ok(!onFail.called, "Fail should never be called.");

        ajaxing.resolve(sharedBy);

        ok(onDone.calledOnce, "Success should now have been called.");
        ok(onDone.firstCall.calledWith({sharedBy: sharedBy}), "Success should just pass the arguments through.");
    });

    test("getSharedBy delegates errors/progress in wrapper.", function () {
        var ajaxing = Deferred();
        var _ajax = this.sandbox.stub(this.api, "_ajax");
        var onDone = this.sandbox.stub();
        var onFail = this.sandbox.stub();
        var onProgress = this.sandbox.stub();
        var wrap = Deferred();

        _ajax.returns(ajaxing);

        this.api.getSharedBy(wrap)
            .done(onDone)
            .fail(onFail)
            .progress(onProgress);

        ok(!_ajax.called, "Ajax should not be called until success of wrapped.");

        wrap.notify("Something");
        wrap.notify("Else", true, 1);

        //Progress events propagated.
        ok(onProgress.calledTwice, "Two progress events.");
        ok(onProgress.firstCall.calledWith("Something"), "First progress event received.");
        ok(onProgress.secondCall.calledWith("Else", true, 1), "Second progress event received.");

        //Rejection works as expected.
        wrap.reject({}, 1, 2);
        ok(!_ajax.called, "Ajax should not be called on error.");

        ok(!onDone.called, "Success should not be called on error.");
        ok(onFail.calledOnce, "Fail should be called.");
        ok(onFail.firstCall.calledWith({}, 1, 2), "Fail called with correct arguments.");
    });

    test("getSharedBy returns error cases as no projects", function () {
        var ajaxing = Deferred();
        var _ajax = this.sandbox.stub(this.api, "_ajax");
        var onDone = this.sandbox.stub();
        var onFail = this.sandbox.stub();
        var wrap = Deferred();

        _ajax.returns(ajaxing);

        this.api.getSharedBy(wrap)
            .done(onDone)
            .fail(onFail);

        ok(!_ajax.called, "Ajax should not be called until success of wrapped.");

        wrap.resolve({}, 1, 2);
        ok(_ajax.calledOnce, "Ajax should have been called once.");
        var ajaxOptions = _ajax.firstCall.args[0];
        strictEqual(ajaxOptions.type, "GET", "Ajax should be a get.");
        strictEqual(ajaxOptions.contentType, "application/json", "Body should be json");
        strictEqual(ajaxOptions.dataType, "json", "Response should be json.");

        var regex = new RegExp("/rest/globalconfig/1/issuecustomfields/" + encodeURIComponent(this.issue) + "/affectedProjects$");
        ok(regex.test(ajaxOptions.url), "URL is correct and encoded.");

        ok(!onDone.called, "Success should not be called until ajax returns.");
        ok(!onFail.called, "Fail should never be called.");

        ajaxing.reject("Something should be ignored.");

        ok(onDone.calledOnce, "Success should now have been called.");
        ok(onDone.firstCall.calledWith({sharedBy: []}), "Success should just pass the arguments through.");
    });

    test("getSharedBy no issue returns empty projects", function () {
        var api = new IssueCustomFields();
        var _ajax = this.sandbox.stub(api, "_ajax");
        var onDone = this.sandbox.stub();
        var onFail = this.sandbox.stub();
        var wrap = Deferred();

        api.getSharedBy(wrap).done(onDone).fail(onFail);

        wrap.resolve({}, 1, 2);

        ok(!_ajax.called, "Ajax is never called when no issue.");
        ok(onDone.calledOnce, "Success should now have been called.");
        ok(onDone.firstCall.calledWith({sharedBy: []}), "Success should just pass the arguments through.");
        ok(!onFail.called, "Fail should never be called.");
    });

    test("addFieldToScreen handles happy case", function () {
        var ajaxing = Deferred();
        var _ajax = this.sandbox.stub(this.api, "_ajax");
        var field = "field";
        var onDone = this.sandbox.stub();
        var onFail = this.sandbox.stub();

        _ajax.returns(ajaxing);

        this.api.addFieldToScreen(field)
            .done(onDone)
            .fail(onFail);

        ok(_ajax.calledOnce, "Ajax should have been called once.");
        var ajaxOptions = _ajax.firstCall.args[0];
        strictEqual(ajaxOptions.type, "POST", "Ajax should be a post.");
        strictEqual(ajaxOptions.data, field, "Body should be the field name.");
        strictEqual(ajaxOptions.contentType, "application/json", "Body should be json");
        strictEqual(ajaxOptions.dataType, "json", "Response should be json.");

        var regex = new RegExp("/rest/globalconfig/1/issuecustomfields/" + encodeURIComponent(this.issue) + "$");
        ok(regex.test(ajaxOptions.url), "URL is correct and encoded.");

        ok(!onDone.called, "Success should not be called until ajax resturns.");
        ok(!onFail.called, "Fail should never be called.");

        ajaxing.resolve(1, 2, 3, "four");

        ok(onDone.calledOnce, "Success should now have been called.");
        ok(onDone.firstCall.calledWith(1, 2, 3, "four"), "Success should just pass the arguments through.");
    });

    test("addFieldToScreen handles happy case no issue", function () {
        var api = new IssueCustomFields();
        var _ajax = this.sandbox.stub(api, "_ajax");
        var field = "field";
        var onDone = this.sandbox.stub();
        var onFail = this.sandbox.stub();

        api.addFieldToScreen(field)
            .done(onDone)
            .fail(onFail);

        ok(!_ajax.called, "AJAX not called.");
        ok(!onDone.called, "Success should not be called.");
        ok(onFail.calledOnce, "Fail should be called.");
    });

    test("addFieldToScreen handles error", function () {
        var error = "Error";

        assertStandardErrorHandler.call(this, {
            func: "addFieldToScreen",
            statusText: "error",
            humanErrorMessage: error,
            expectedErrors: [IssueCustomFields.STATUS.error, error]
        });
    });

    test("addFieldToScreen handles abort", function () {
        assertStandardErrorHandler.call(this, {
            func: "addFieldToScreen",
            statusText: "abort",
            expectedErrors: [IssueCustomFields.STATUS.abort, null]
        });
    });

    test("getCustomFieldsTypes handles successful AJAX", function () {
        var ajaxing = Deferred();
        var _ajax = this.sandbox.stub(this.api, "_ajax");
        var onDone = this.sandbox.stub();
        var onFail = this.sandbox.stub();

        _ajax.returns(ajaxing);

        this.api.getCustomFieldTypes()
            .done(onDone)
            .fail(onFail);

        ok(_ajax.calledOnce, "Ajax should have been called once.");
        var ajaxOptions = _ajax.firstCall.args[0];
        strictEqual(ajaxOptions.type, "GET", "Ajax should be a GET.");
        strictEqual(ajaxOptions.contentType, "application/json", "Body should be json");
        strictEqual(ajaxOptions.dataType, "json", "Response should be json.");

        var regex = new RegExp("/rest/globalconfig/1/customfieldtypes$");
        ok(regex.test(ajaxOptions.url), "URL is correct and encoded.");

        ok(!onDone.called, "Success should not be called until ajax resturns.");
        ok(!onFail.called, "Fail should never be called.");

        var data = 1;
        var statusText = 2;
        var jqXHR = 3;

        ajaxing.resolve(data, statusText, jqXHR);

        ok(onDone.calledOnce, "Success should now have been called.");
        strictEqual(onDone.firstCall.args[0], data, "Success should just pass the arguments through.");
    });

    test("getCustomFieldsTypes handles error", function () {
        var error = "Error";

        assertStandardErrorHandler.call(this, {
            func: "getCustomFieldTypes",
            statusText: "error",
            humanErrorMessage: error,
            expectedErrors: [IssueCustomFields.STATUS.error, error]
        });
    });

    test("getCustomFieldsTypes handles abort", function () {
        assertStandardErrorHandler.call(this, {
            func: "getCustomFieldTypes",
            statusText: "abort",
            expectedErrors: [IssueCustomFields.STATUS.abort, null]
        });
    });

    test("setOptions handles successful AJAX", function () {
        var ajaxing = Deferred();
        var _ajax = this.sandbox.stub(this.api, "_ajax");
        var onDone = this.sandbox.stub();
        var onFail = this.sandbox.stub();

        _ajax.returns(ajaxing);

        var fieldId = "<fieldId>";
        var options = [
            {name: "Name"}
        ];

        this.api.setOptions(fieldId, options)
            .done(onDone)
            .fail(onFail);

        ok(_ajax.calledOnce, "Ajax should have been called once.");
        var ajaxOptions = _ajax.firstCall.args[0];
        strictEqual(ajaxOptions.type, "POST", "Ajax should be a POST.");
        strictEqual(ajaxOptions.contentType, "application/json", "Body should be json");
        strictEqual(ajaxOptions.dataType, "json", "Response should be json.");
        strictEqual(ajaxOptions.data, JSON.stringify(options));

        var regex = new RegExp("rest/globalconfig/1/customfieldoptions/" + encodeURIComponent(fieldId) + "$");
        ok(regex.test(ajaxOptions.url), "URL is correct and encoded.");

        ok(!onDone.called, "Success should not be called until ajax resturns.");
        ok(!onFail.called, "Fail should never be called.");

        var data = 1;
        var statusText = 2;
        var jqXHR = 3;

        ajaxing.resolve(data, statusText, jqXHR);

        ok(onDone.calledOnce, "Success should now have been called.");
        strictEqual(onDone.firstCall.args[0], data, "Success should just pass the arguments through.");
    });

    test("setOptions handles error", function () {
        var error = "Error";

        assertStandardErrorHandler.call(this, {
            func: "setOptions",
            statusText: "error",
            humanErrorMessage: error,
            expectedErrors: [IssueCustomFields.STATUS.error, error]
        });
    });

    test("setOptions handles abort", function () {
        assertStandardErrorHandler.call(this, {
            func: "setOptions",
            statusText: "abort",
            expectedErrors: [IssueCustomFields.STATUS.abort, null]
        });
    });

    var assertAjax = function (step, callback) {
        var progressCallback = this.sandbox.stub();
        var doneCallback = this.sandbox.stub();
        var failCallback = this.sandbox.stub();
        var xhr = Deferred();

        var request = this.sandbox.stub(WebSudoSmartAjax, "makeWebSudoRequest");
        request.returns(xhr);

        var options = {some: "random", options: true};
        this.api._ajax(options)
            .done(doneCallback)
            .fail(failCallback)
            .progress(progressCallback);

        ok(request.calledOnce, "Called the ajax provider");
        var smartAjaxCall = request.getCall(0);
        deepEqual(options, smartAjaxCall.args[0], "Passing through the AJAX options?");

        var websudoDialogOptions = smartAjaxCall.args[1];
        ok(websudoDialogOptions.beforeShow, "Passed in beforeShow trigger.");
        ok(websudoDialogOptions.success, "Passed in success trigger.");

        ok(!progressCallback.called, "Making sure beforeShow not called yet!");
        websudoDialogOptions.beforeShow();
        ok(progressCallback.calledOnce, "Making sure beforeShow called now");
        deepEqual(progressCallback.firstCall.args, [IssueCustomFields.PROGRESS.SHOW],
            "Making sure beforeShow with correct progress.");

        var closeFunction = function () {
        };
        ok(!doneCallback.called, "Making sure success not called yet!");
        websudoDialogOptions.success(closeFunction);
        ok(progressCallback.calledTwice, "notified of websudo success");
        deepEqual(progressCallback.secondCall.args, [IssueCustomFields.PROGRESS.SUCCESS, closeFunction], "Correct callback?");

        ok(!doneCallback.called, "Done should not be called until AJAX returned.");
        ok(!failCallback.called, "Cancel should not be called until AJAX returned.");

        callback(xhr, websudoDialogOptions.cancel, doneCallback, failCallback);
    };

    var assertAjaxSuccess = function (step) {
        assertAjax.call(this, step, function (xhr, cancel, doneCallback, failCallback) {
            var data = 1;
            var textStatus = 2;
            var jqXHR = 3;

            xhr.resolve(data, textStatus, jqXHR);
            ok(doneCallback.called, "Done called once ajax completes.");
            strictEqual(doneCallback.firstCall.args[0], data, "Done receives data from response.");
            ok(!failCallback.called, "Failed should not have been called.");
        });
    };

    var assertAjaxFailed = function (step) {
        assertAjax.call(this, step, function (xhr, cancel, doneCallback, failCallback) {
            var statusText = 2;
            var errorThrown = 3;
            var smartAjaxResult = 4;

            xhr.reject(xhr, statusText, errorThrown, smartAjaxResult);
            ok(failCallback.called, "failed called when ajax completes.");
            deepEqual(failCallback.firstCall.args, [statusText, smartAjaxResult], "fail called with correct arguments.");
            ok(!doneCallback.called, "Done should not have been called.");
        });
    };

    var assertAjaxCancel = function (step) {
        assertAjax.call(this, step, function (xhr, cancel, doneCallback, failCallback) {
            var event = AJS.$.Event("cancel");
            cancel(event);
            ok(event.isDefaultPrevented(), "Cancel prevents default.");
            ok(failCallback.calledOnce, "Cancel calls fail.");
            ok(failCallback.firstCall.calledWith("abort", undefined), "Cancel fails like an abort.");
            ok(!doneCallback.called, "Dont should not be called on success.");
        });
    };

    test("ajax delegates to smart AJAX success", function () {
        assertAjaxSuccess.call(this, this.step);
    });

    test("ajax delegates to smart AJAX failed", function () {
        assertAjaxFailed.call(this, this.step);
    });

    test("smart AJAX websudo cancel aborts the wizard", function () {
        assertAjaxCancel.call(this, this.step);
    });

});
