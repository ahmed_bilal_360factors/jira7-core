define("jira-project-config/custom-fields/views/shared-by", [
    'jira-project-config/custom-fields/templates',
    "aui/inline-dialog",
    "jira-project-config/marionette"
], function(CustomFieldTemplates, InlineDialog, Marionette) {
    "use strict";

    var TEMPLATES = CustomFieldTemplates;

    return Marionette.ItemView.extend({
        initialize: function (options) {
            this.projects = options.projects;
        },
        template: TEMPLATES.sharedByCreate,
        serializeData: function () {
            return {projects: this.projects};
        },
        onDialogNotificationHide: function () {
            if (this.popup) {
                this.popup.hide();
                this.popup.remove();
                delete this.popup;
            }
        },
        onDialogNotificationShow: function () {
            var projects = this.projects;
            this.popup = new InlineDialog(this.$el.find("a"), "customfield-project-list", function (contents, trigger, showPopup) {
                contents.html(TEMPLATES.shareByInline({
                    projects: projects
                }));
                showPopup();
            }, {width: 240, onHover: true, hideDelay: 500});
        }
    });
});

AJS.namespace("JIRA.Admin.CustomFields.SharedByView", null, require("jira-project-config/custom-fields/views/shared-by"));
