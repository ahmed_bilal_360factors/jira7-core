define("jira-project-config/custom-fields/steps/field-wisher", [
    "jira/jquery/deferred",
    "jira-project-config/custom-fields/steps/type",
    "jira-project-config/custom-fields/steps/field-wisher/views/dialog",
    "jira-project-config/custom-fields/error-dialog",
    "jira-project-config/custom-fields/wizard/abstract-step",
    "jira-project-config/custom-fields/steps/helper",
    "jira-project-config/custom-fields/wizard/fail-reasons",
    "underscore"
], function(
    Deferred,
    TypeStep,
    FieldWisherStepDialog,
    ErrorDialog,
    AbstractStep,
    CustomFieldsStepHelper,
    FailReasons,
    _) {
    "use strict";

    /**
     * Allows a user to search for an existing field, or to choose the name for what they wished existed.
     *
     * @param {IssueCustomFields} options.api The API for accessing server data.
     */
    var FieldWisherStep = function (options) {
        this.api = options.api;
        this.stepData = {};
    };

    _.extend(FieldWisherStep.prototype, AbstractStep, {
        showLoading: function () {
            this.dialog && this.dialog.showLoading();
        },

        hideLoading: function () {
            this.dialog && this.dialog.hideLoading();
        },

        start: function () {
            var self = this;
            var startingPromise = this._getFields();

            startingPromise.done(function (fields) {
                var dialog = self.dialog = new FieldWisherStepDialog({fields: fields});

                self.listenTo(dialog, "cancel", function (checkDirty) {
                    self.fireCancel(checkDirty);
                });
                self.listenTo(dialog, "submit", function (wish) {
                    self.stepData.field = {};
                    self.stepData.field.id = wish.id;
                    self.stepData.field.name = wish.name;

                    if (wish.id) {
                        self.stepData.createField = false;
                        self.stepData.addField = !wish.onAllScreens;
                        self.fireComplete();
                    } else {
                        self.stepData.createField = true;
                        self.stepData.addField = true;
                        self.fireNext();
                    }
                });

            });

            startingPromise.fail(function (kind, message) {
                if (FailReasons.abort === kind) {
                    self.fireCancel();
                } else {
                    var dialog = self.dialog = new ErrorDialog({
                        heading: FieldWisherStepDialog.prototype.text.heading,
                        message: message
                    });

                    self.listenTo(dialog, "cancel", self.fireCancel);
                }
            });

            return startingPromise;
        },

        restart: function () {
            return Deferred().resolve().promise();
        },

        pauseStep: function () {
            if (this.dialog) {
                this.dialog.resetCanSubmit();
                this.dialog.resetIsSubmitted();
                this.dialog.hide();
            }
        },

        stopStep: function () {
            if (this.dialog) {
                this.stopListening(this.dialog);
                this.dialog.destroy();
                delete this.dialog;
            }
        },

        show: function () {
            this.dialog && this.dialog.show();
        },

        doComplete: function () {
            var completes;
            var data = this.getAccumulatedData();

            completes = this._resolveCompletesToExecute(data);

            return this.executeCompletes(completes, {
                api: this.api,
                data: data
            });
        },

        getStepData: function () {
            return this.stepData;
        },

        getNextStep: function (proposedNextStep, sharedStepOptions) {
            if (proposedNextStep instanceof TypeStep) {
                return proposedNextStep;
            }
            return new TypeStep({api: sharedStepOptions.api, inIssueContext: sharedStepOptions.inIssueContext});
        },

        isDirty: function () {
            var field = this.stepData.field;
            var isStepDataDirty = field && (field.id || field.name);
            return isStepDataDirty || (this.dialog && this.dialog.isDirty());
        },

        /**
         * @private
         */
        _getFields: function () {
            var fields = Deferred();
            var getting = this.api.addableFields(true);

            getting.done(fields.resolve);
            getting.fail(_.bind(this.genericFailHandler, this, fields));
            getting.progress(_.bind(this.genericProgressHandler, this, fields));

            return fields.promise();
        },

        _resolveCompletesToExecute: function (data) {
            var completes = [];
            if (data.addField) {
                completes.push(this.putFieldOnIssue);
                completes.push(this.refreshIssue);
            }
            return completes;
        }
    }, CustomFieldsStepHelper);

    return FieldWisherStep;
});

AJS.namespace("JIRA.Admin.CustomFields.FieldWisherStep", null, require("jira-project-config/custom-fields/steps/field-wisher"));

AJS.namespace("JIRA.Admin.CustomFields.FieldWisherStep.Dialog", null,
    require("jira-project-config/custom-fields/steps/field-wisher/views/dialog"));

AJS.namespace("JIRA.Admin.CustomFields.FieldWisherStep.Panel", null,
    require("jira-project-config/custom-fields/steps/field-wisher/views/panel"));
