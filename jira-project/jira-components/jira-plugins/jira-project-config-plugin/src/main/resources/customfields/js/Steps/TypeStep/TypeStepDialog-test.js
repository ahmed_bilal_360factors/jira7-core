AJS.test.require(["jira.webresources:jira-global",
    "jira.webresources:set-focus",
    "com.atlassian.jira.jira-project-config-plugin:custom-fields",
    "com.atlassian.jira.jira-project-config-plugin:custom-fields-impl"], function () {

    var $ = require('jquery');
    var _ = require('underscore');
    var Backbone = require('jira-project-config/backbone');
    var Marionette = require('jira-project-config/marionette');
    var AJSHelper = require('jira-project-config/libs/ajshelper');
    var TypeStep = require("jira-project-config/custom-fields/steps/type");

    // -- TypeStep.Views.TypeChoice ------------------------------------------------------------------------------

    module("JIRA.Admin.CustomFields.TypeStep.Views.TypeChoice", {
        setup: function () {
            this.sandbox = sinon.sandbox.create();
            this.isSysadmin = this.sandbox.stub();
            this.context = AJS.test.mockableModuleContext();
            this.context.mock("jira/util/users/logged-in-user", {
                isSysadmin: this.isSysadmin
            });
            this.context.mock("jira-project-config/libs/ajshelper", AJSHelper);
            this.TypeChoice = this.context.require('jira-project-config/custom-fields/steps/type/views/type-choice');

            this.Models = TypeStep.Models;
            this.categories = [{
                initial: true,
                id: "foo",
                name: "Foo",
                types: new this.Models.Types([
                    {
                        key: "a",
                        name: "AAA",
                        description: "A description"
                    },
                    {
                        key: "b",
                        name: "BBB",
                        description: "B description"
                    }
                ])
            }, {
                initial: false,
                id: "bar",
                name: "Bar",
                types: new this.Models.Types([{
                    key: "c",
                    name: "C name",
                    description: "C description"
                }])
            }];
        },
        teardown: function () {
            this.sandbox.restore();
        }
    });

    var DialogPageObject = function (dialog) {
        this.dialog = dialog;
    };

    _.extend(DialogPageObject.prototype, {
        press: function (name) {
            var code = $.ui.keyCode[name];
            $.event.trigger({
                type: "keydown",
                which: code,
                keyCode: code
            });
            return this;
        },
        data: function () {
            var result = [];

            this.dialog.$el.find("ol.customfields-types:visible > li").each(function () {
                var el = $(this);

                result.push({
                    name: el.find("h3").text(),
                    description: el.find("p").text(),
                    selected: el.hasClass("selected")
                });
            });

            return result;
        },
        hasNotification: function () {
            return this.dialog.$el.find(".jira-dialog-notification").length > 0;
        },
        hasHeading: function () {
            var text = this.dialog.$el.find(".dialog-title").text();
            return !!text;
        },

        hasContent: function () {
            return !!this.dialog.$el.find(".dialog-panel-body > div").children();
        },

        selected: function () {
            return _.find(this.data(), function (item) {
                return item.selected;
            });
        },

        selectCategory: function (index) {
            this.dialog.$el.find(".page-menu-item:nth-child(" + index + ") button").click();
        },

        dblclick: function () {
            return this.selectedElement().dblclick();
        },

        selectedElement: function () {
            var index0 = this.dialog.$el.find(".page-menu-item.selected").index();
            return this.dialog.$el.find(".dialog-panel-body:nth-child(" + (index0 + 1) + ") .customfields-types li.selected");
        },

        marketplaceLink: function () {
            return this.dialog.$el.find(".jira-icon-marketplace").parent();
        },

        visible: function () {
            return this.dialog.$el.is(":visible");
        },

        hasError: function () {
            return !!this.dialog.$el.find(".aui-message").text();
        },

        nextButton: function () {
            return this.dialog.$el.find(".aui-button-primary");
        },

        cancelButton: function () {
            return this.dialog.$el.find(".button-panel-link:last-child");
        },

        searchHasFocus: function () {
            return !!this.dialog.$el.find(".search input:focus").length;
        },

        typeInto: function (text, $element) {
            $element.val(text);
            $element.trigger($.Event("input"));
        },

        search: function (text) {
            var $input = this.dialog.$el.find(".search input");
            this.typeInto(text, $input);
        },
        isDestroyed: function () {
            return !this.dialog.$el.parent().length;
        }
    });

    test("Searching filters the list of types.", function () {
        var view = new this.TypeChoice(this.categories);

        view.show();

        var dialog = view.dialog;
        var parser = new DialogPageObject(dialog);

        var initial = parser.data();
        var name = initial[0].name;
        ok(initial.length > 0, "Expected some types to be displayed.");

        // Limit the search to the name of the first item.
        parser.search(initial[0].name);

        equal(parser.data().length, 1, "Expected the types to be limited to a single item.");
        equal(parser.data()[0].name, name, "Expected the remaining item to be the one we searched for.");

        dialog.destroy();
    });

    test("Marketplace link is not shown on for non-sysadmins", function () {
        var view = new this.TypeChoice(this.categories);

        this.isSysadmin.returns(false);
        view.show();

        var dialog = view.dialog;
        var parser = new DialogPageObject(dialog);

        equal(0, parser.marketplaceLink().length, "Market Place link should not be visible.");
        dialog.destroy();
    });

    test("Marketplace link is shown for sysadmins", function () {
        var view = new this.TypeChoice(this.categories);

        this.isSysadmin.returns(true);
        view.show();

        var dialog = view.dialog;
        var parser = new DialogPageObject(dialog);

        var link = parser.marketplaceLink();
        ok(link.length, "Market Place link should be visible.");
        ok(link.attr("href").match(/marketplace/), "Market Place link URL should probably include 'marketplace'.");

        dialog.destroy();
    });

    test("Type choice dialog has a heading and content", function () {
        var view = new this.TypeChoice(this.categories);

        view.show();

        var dialog = view.dialog;
        var parser = new DialogPageObject(dialog);

        ok(parser.hasHeading(), "Dialog should have a heading.");
        ok(parser.hasContent(), "Dialog should have content.");

        dialog.destroy();
    });

    test("Keyboard navigation works for items", function () {
        var view = new this.TypeChoice(this.categories);

        view.show();

        var dialog = view.dialog;
        var parser = new DialogPageObject(dialog);
        var original = parser.selected().name;

        parser.press("DOWN");
        notEqual(parser.selected().name, original, "Expected another item to be selected.");
        parser.press("UP");
        equal(parser.selected().name, original, "The original item should be selected again.");

        dialog.destroy();
    });

    test("Clicking next, pressing Enter, or double clicking proceeds to the next step", function () {
        var view = new this.TypeChoice(this.categories);
        var events = [];
        var firstItemKey = this.categories[0].types.models[0].get("key");

        view.show();
        view.on("submit", function (model) {
            events.push(model.get('key'));
        });

        var dialog = view.dialog;
        var parser = new DialogPageObject(dialog);

        parser.nextButton().click();
        equal(1, events.length, "'submit' should be triggered when clicking the next button.");
        equal(firstItemKey, events[0], "The current key should be sent with the event.");

        parser.press("ENTER");
        equal(2, events.length, "'submit' should be triggered when pressing enter.");
        equal(firstItemKey, events[1], "The current key should be sent with the event.");

        parser.dblclick();
        equal(3, events.length, "'submit' should be triggered when double clicking an item.");
        equal(firstItemKey, events[2], "The current key should be sent with the event.");

        dialog.destroy();
    });

    test("Clicking cancel or pressing ESC attempts fires cancel event", function () {
        var view = new this.TypeChoice(this.categories);

        view.show();

        var parser = new DialogPageObject(view.dialog);

        var cancel = this.sandbox.stub();
        view.on('cancel', cancel);

        parser.cancelButton().click();
        ok(cancel.calledOnce, "The dialog should be destroyed after clicking cancel.");
        deepEqual(cancel.firstCall.args, [false], "No confirm on cancel link.");

        parser.press("ESCAPE");
        ok(cancel.calledTwice, "The dialog should be destroyed after pressing escape.");
        deepEqual(cancel.secondCall.args, [true], "Confirm on cancel.");

        view.hide();
    });

    test("Shared by warning shown", function () {
        var SharedBy = Marionette.ItemView.extend({
            render: function () {
                return this;
            }
        });
        var view = new this.TypeChoice(this.categories, 0, new SharedBy());

        view.show();

        var parser = new DialogPageObject(view.dialog);
        ok(parser.hasNotification(), "Shared by message should be shown.");

        view.hide();
    });

    test("Shared by warning not shown", function () {
        var view = new this.TypeChoice(this.categories, 0);

        view.show();

        var parser = new DialogPageObject(view.dialog);
        ok(!parser.hasNotification(), "Shared by message should not be shown.");

        view.hide();
    });

    test("Analytics events are fired when the category is changed", function () {
        var view = new this.TypeChoice(this.categories);
        var trigger = this.sandbox.stub(AJSHelper, "trigger");

        view.show();

        var dialog = view.dialog;
        var parser = new DialogPageObject(dialog);

        /**
         * Returns the AJS.trigger("analytics", ...) events that have been made.
         *
         * @returns {Array}
         */
        function analytics() {
            var calls = [];
            var call;

            for (var i = 0, n = trigger.callCount; i < n; i++) {
                call = trigger.getCall(i);
                if (call.args[0] === "analytics") {
                    calls.push(call);
                }
            }

            return calls;
        }

        // At this point only AJS.Dialog should have triggered an event.
        deepEqual(analytics(), [], "No analytics should have occurred yet.");

        // Pressing ENTER selects the current field type.
        parser.press("ENTER");
        equal(analytics().length, 1, "Expected a single analytics event.");
        deepEqual(analytics()[0].args[1], {
            name: "administration.customfields.addwizard.submittype",
            data: {
                key: "a",
                category: "foo"
            }
        }, "Check the content of the event.");

        // We're currently on the first category, changing to the second one should fire an event.
        parser.selectCategory(2);
        equal(analytics().length, 2, "Expected a second analytics event.");
        deepEqual(analytics()[1].args[1], {
            name: "administration.customfields.addwizard.changecategory",
            data: {
                id: "bar",
                name: "Bar"
            }
        }, "Check the content of the event.");

        // Double clicking the current type should also send an event
        // We're currently on the first category, changing to the second one should fire an event.
        parser.dblclick();

        equal(analytics().length, 3, "Expected a third analytics event.");
        deepEqual(analytics()[2].args[1], {
            name: "administration.customfields.addwizard.submittype",
            data: {
                key: "c",
                category: "bar"
            }
        }, "Check the content of the event.");

        // Double clicking the current type should also send an event
        // We're currently on the first category, changing to the second one should fire an event.
        parser.nextButton().click();

        equal(analytics().length, 4, "Expected a fourth analytics event.");
        deepEqual(analytics()[3].args[1], {
            name: "administration.customfields.addwizard.submittype",
            data: {
                key: "c",
                category: "bar"
            }
        }, "Check the content of the event.");

        dialog.destroy();
    });

    test("When a collection view fires 'changeToAll', the first panel is shown.", function () {
        var collectionsViews = [];
        var panels = [];
        var sandbox = this.sandbox;
        this.context.mock('jira-project-config/custom-fields/views/field-type-collection', function () {
            var cv = _.extend({
                render: sandbox.stub().returnsThis()
            }, Backbone.Events);
            collectionsViews.push(cv);
            return cv;
        });
        this.context.mock('jira-project-config/custom-fields/views/dialog/panel', function () {
            var p = {
                show: sandbox.spy(),
                _triggerShow: sandbox.spy(),
                _triggerHide: sandbox.spy()
            };
            panels.push(p);
            return p;
        });

        var TypeChoice = this.context.require('jira-project-config/custom-fields/steps/type/views/type-choice');
        var view = new TypeChoice(this.categories);
        var dialog;
        var firstPanel;
        var secondCollectionView;

        // Create the panels and collection views
        view.show();

        dialog = view.dialog;
        firstPanel = panels[0];
        secondCollectionView = collectionsViews[1];

        // The not-first-view triggers 'changeToAll', should show the first panel.
        ok(firstPanel.show.calledOnce, "Expected the first panel to not be shown again.");
        secondCollectionView.trigger("changeToAll");
        ok(firstPanel.show.calledTwice, "Expected the first panel to be shown once.");

        secondCollectionView.trigger("changeToAll");
        ok(firstPanel.show.calledThrice, "Expected the first panel to be shown a second time.");

        dialog.destroy();
    });

    test("When the search changes, all collection views are notified if there are any matches.", function () {
        var collectionsViews = [];
        var sandbox = this.sandbox;
        this.context.mock('jira-project-config/custom-fields/views/field-type-collection', function () {
            var cv = _.extend({
                render: sandbox.stub().returnsThis(),
                setHasMatches: sandbox.spy(),
                triggerMethod: sandbox.spy()
            }, Backbone.Events);
            collectionsViews.push(cv);
            return cv;
        });
        var quickSearch = _.extend({
            render: this.sandbox.stub().returnsThis()
        }, Backbone.Events);
        this.context.mock('jira-project-config/custom-fields/views/quick-search', this.sandbox.stub().returns(quickSearch));

        var TypeChoice = this.context.require('jira-project-config/custom-fields/steps/type/views/type-choice');
        var view = new TypeChoice(this.categories);
        var dialog;
        var firstCollectionView;
        var secondCollectionView;


        // Create the panels and collection views
        view.show();

        // Grab the newly created instances
        dialog = view.dialog;

        // These should be notified after searches
        firstCollectionView = collectionsViews[0];
        secondCollectionView = collectionsViews[1];

        // Searching for 'AAA' will limit results to a single type. Both views should be notified that *some* results exist.
        quickSearch.trigger("change", {value: "AAA"});
        ok(firstCollectionView.setHasMatches.calledOnce, "Expected the first view's .setHasMatches() to be called.");
        ok(secondCollectionView.setHasMatches.calledOnce, "Expected the second view's .setHasMatches() to be called.");
        deepEqual(firstCollectionView.setHasMatches.firstCall.args, [true], "Expected the view to be told results exist.");
        deepEqual(secondCollectionView.setHasMatches.firstCall.args, [true], "Expected the view to be told results exist.");

        dialog.destroy();
    });
});
