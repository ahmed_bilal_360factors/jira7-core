AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:custom-fields",
    "com.atlassian.jira.jira-project-config-plugin:custom-fields-impl"], function () {

    var $ = require("jquery");
    var _ = require("underscore");
    var TypeItemView = require("jira-project-config/custom-fields/views/type-item");
    var Type = require("jira-project-config/custom-fields/steps/type/models/type");

    module("JIRA.Admin.CustomFields.TypeItemView", {
        setup: function () {
            this.sandbox = sinon.sandbox.create();
            this.TypeItemView = TypeItemView;
            this.Type = Type;
        },
        teardown: function () {
            this.sandbox.restore();
        }
    });

    var Parser = function (el) {
        this.el = $(el);
    };

    _.extend(Parser.prototype, {
        name: function () {
            var h3 = this.el.find("h3");
            return h3.length ? h3.text() : null;
        },

        description: function () {
            var p = this.el.find("p");
            return p.length ? p.text() : null;
        },
        imageUrl: function () {
            var img = this.image();
            if (img) {
                return img.attr("src");
            } else {
                return null;
            }
        },
        isManaged: function () {
            var lozenge = this.el.find(".aui-lozenge");
            return !!lozenge.length;
        },
        managedDescription: function () {
            var lozenge = this.el.find(".aui-lozenge");
            return lozenge.length ? lozenge.attr('title') : null;
        },

        noImageRendered: function () {
            return !!this.el.find(".field-preview-empty").length;
        },
        image: function () {
            return this.el.find("img.field-preview");
        }
    });

    test("missing name should be rendered as the key", function () {
        var item = new this.Type({key: "key", name: null, description: "description"});
        var itemView = new this.TypeItemView({model: item});
        var parser = new Parser(itemView.render().el);

        strictEqual(parser.name(), "key", "Missing name should be rendered using the key.");
        strictEqual(parser.description(), item.get("description"), "Description should have been set.");
        ok(parser.noImageRendered(), "No image when no URL given.");
    });

    test("missing description should render anyway", function () {
        var item = new this.Type({key: "key", name: "name", description: undefined});
        var itemView = new this.TypeItemView({model: item});
        var parser = new Parser(itemView.render().el);

        strictEqual(parser.description(), null, "Missing description should not be rendered.");
        ok(parser.noImageRendered(), "No image when no URL given.");
    });

    test("Provided name and description should be rendered", function () {
        var item = new this.Type({key: "key", name: "name", description: "description"});
        var itemView = new this.TypeItemView({model: item});
        var parser = new Parser(itemView.render().el);

        equal(parser.name(), "name", "Provided name should be rendered.");
        equal(parser.description(), "description", "Provided description should be rendered.");
        ok(parser.noImageRendered(), "No image when no URL given.");
    });

    test("Provided name and description and image URL should be rendered", function () {
        var item = new this.Type({key: "key", name: "name", description: "description", previewImageUrl: "/image/url"});
        var itemView = new this.TypeItemView({model: item});
        var parser = new Parser(itemView.render().el);

        equal(parser.name(), "name", "Provided name should be rendered.");
        equal(parser.description(), "description", "Provided description should be rendered.");
        equal(parser.imageUrl(), item.get("previewImageUrl"), "Image should be rendered as per the URL.");
        ok(!parser.noImageRendered(), "No image fill should not be present.");
    });

    test("XSS immunity", function () {
        var script = "<script>alert(1)</script>";
        var item = new this.Type({key: "key", name: script, description: script});
        var itemView = new this.TypeItemView({model: item});
        var parser = new Parser(itemView.render().el);

        equal(parser.name(), script, "HTML should be escaped.");
        equal(parser.description(), script, "HTML should be escaped.");
    });

    test("Single click events are handled", function () {
        var item = new this.Type({key: "key", name: null, description: "description"});
        var itemView = new this.TypeItemView({model: item});
        var selected;

        itemView.on("select", function () {
            selected = true;
        });
        itemView.$el.click();

        equal(true, selected, "Expected the 'select' event to be triggered.");
    });

    test("Double click events are handled", function () {
        var item = new this.Type({key: "key", name: null, description: "description"});
        var itemView = new this.TypeItemView({model: item});
        var submitted;

        itemView.on("submit", function (key) {
            submitted = key;
        });
        itemView.$el.dblclick();

        equal("key", submitted, "Expected the 'submit' event to be triggered.");
    });

    test("Managed label displayed", function () {
        var item = new this.Type({key: "key", name: "name", description: "description", isManaged: true});
        var itemView = new this.TypeItemView({model: item});
        var parser = new Parser(itemView.render().el);

        equal(parser.name(), "name", "Provided name should be rendered.");
        equal(parser.description(), "description", "Provided description should be rendered.");
        ok(parser.isManaged(), "No manage label displayed.");
    });

    test("Managed label not shown", function () {
        var item = new this.Type({key: "key", name: "name", description: "description", isManaged: false});
        var itemView = new this.TypeItemView({model: item});
        var parser = new Parser(itemView.render().el);

        equal(parser.name(), "name", "Provided name should be rendered.");
        equal(parser.description(), "description", "Provided description should be rendered.");
        ok(!parser.isManaged(), "Manage label displayed when it should be hidden.");
    });

    test("Managed label title correct", function () {
        var item = new this.Type({
                key: "key",
                name: "name",
                description: "description",
                isManaged: true,
                managedDescription: "managed description"
            });

        var itemView = new this.TypeItemView({model: item});
        var parser = new Parser(itemView.render().el);

        equal(parser.name(), "name", "Provided name should be rendered.");
        equal(parser.description(), "description", "Provided description should be rendered.");
        ok(parser.managedDescription(), "managed description", "No manage label displayed.");
    });
});
