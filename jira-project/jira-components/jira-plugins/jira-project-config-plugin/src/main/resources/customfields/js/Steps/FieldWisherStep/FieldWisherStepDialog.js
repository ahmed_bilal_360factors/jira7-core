define("jira-project-config/custom-fields/steps/field-wisher/views/dialog", [
    'jira/util/formatter',
    "jira-project-config/custom-fields/views/dialog",
    "jira-project-config/custom-fields/steps/field-wisher/views/panel",
    "jira-project-config/backbone",
    "jira-project-config/marionette",
    "underscore"
], function (
    formatter,
    Dialog,
    Panel,
    Backbone,
    Marionette,
    _
) {
    "use strict";

    /**
     * @event Dialog#submit
     * @type {(Field|Wish)}
     */

    /**
     * @event Dialog#cancel
     */

    /**
     * @constructor
     * @param {Array.<{Field}>} options.fields Existing custom fields that can be added to the issue.
     * @fires Dialog#submit
     * @fires Dialog#cancel
     */
    var FieldWisherStepDialog = function (options) {
        // bind functions
        _.bindAll(this, "submit");
        this.text = _.clone(this.text);
        this.text.nextButton = _.bind(this.text.nextButton, this);

        this.model = new Backbone.Model({
            // The ID of the selected field. This will be falsey for new fields.
            fieldId: undefined,

            // The name of the selected field.
            fieldName: undefined,

            // Is the selected field on the issue's screen?
            fieldOnAllScreens: undefined,

            // Can the dialog be submitted in its current state?
            canSubmit: undefined,

            // Has the dialog been submitted?
            isSubmitted: undefined,

            // Is the form filled in correctly?
            isValid: undefined,

            // A map of field IDs to boolean, representing whether they are displayed on the screen.
            fieldsOnAllScreens: undefined
        });

        this.dialog = new Dialog({
            id: "customfields-field-wisher",
            height: 220,
            width: 520
        });

        this.$el = this.dialog.$el;

        this.panel = new Panel({
            initialValue: options.initialValue || '',
            fields: options.fields
        });
        this.dialog.addPanel("", this.panel);

        this.dialog.addOrSetHeaderMainText(this.text.heading);

        this.nextButton = this.dialog.addButton({
            label: this.text.nextButton(),
            id: "customfields-field-wisher-next",
            submitAccessKey: true,
            click: this.submit
        });

        this.dialog.addLink({
            label: this.text.cancel,
            click: _.bind(this.cancel, this, false),
            cancelAccessKey: true
        });

        // react to model changes
        this.listenTo(this.model, "change:fieldId change:fieldName", this.calculateIsValid);
        this.listenTo(this.model, "change:isValid change:isSubmitted", this.calculateCanSubmit);
        this.listenTo(this.model, "change:fieldId", this.onChangeFieldId);
        this.listenTo(this.model, "change:canSubmit", this.onChangeCanSubmit);
        this.listenTo(this.model, "change:isSubmitted", this.onChangeIsSubmitted);

        // observe components
        this.listenTo(this.panel, "change:field", this.onPanelChangeField);
        this.listenTo(this.panel, "request:submit", this.onPanelRequestSubmit);
        this.listenTo(this.dialog, "close", _.bind(this.cancel, this, true));

        this.calculateIsValid(this.model);
        this.calculateCanSubmit(this.model);
        this._initializeFieldsOnScreen(this.model, options.fields);
    };

    _.extend(FieldWisherStepDialog.prototype, Backbone.Events, {
        text: {
            cancel: formatter.I18n.getText("common.forms.cancel"),
            create: formatter.I18n.getText("issue.operations.fields.create.button"),
            add: formatter.I18n.getText("issue.operations.fields.add.button"),
            edit: formatter.I18n.getText("issue.operations.fields.edit.button"),
            heading: formatter.I18n.getText("issue.operations.fields.add.title"),

            nextButton: function () {
                if (!this.model.get("fieldId")) {
                    return this.text.create;
                } else if (this.model.get("fieldOnAllScreens")) {
                    return this.text.edit;
                }
                return this.text.add;
            }
        },

        triggerMethod: Marionette.triggerMethod,

        show: function () {
            this.dialog.show();
            return this;
        },

        hide: function () {
            this.dialog && this.dialog.hide();
            return this;
        },
        destroy: function () {
            this.stopListening(this.panel);
            this.dialog && this.dialog.destroy();
            return this;
        },

        showLoading: function () {
            this.dialog && this.dialog.showLoading();
        },

        hideLoading: function () {
            this.dialog && this.dialog.hideLoading();
        },

        resetCanSubmit: function () {
            this.model.set("canSubmit", true);
        },

        resetIsSubmitted: function () {
            this.model.set("isSubmitted", false);
        },

        /**
         * @private
         */
        submit: function () {
            var result = {
                id: this.model.get("fieldId"),
                name: this.model.get("fieldName"),
                onAllScreens: this.model.get("fieldOnAllScreens")
            };

            if (!result.id) {
                // transform a {Field} into a {Wish}
                delete result.id;
            }

            this.model.set("isSubmitted", true);
            this.triggerMethod("submit", result);
        },

        /**
         * @private
         */
        cancel: function (check) {
            this.triggerMethod("cancel", check || false);
        },

        // -- Panel events -------------------------------------------------------------------------------------------

        /**
         * @private
         */
        onPanelChangeField: function (field) {
            var id = field.id;
            this.model.set({
                fieldId: id,
                fieldName: field.name,
                fieldOnAllScreens: !!this.model.get("fieldsOnAllScreens")[id]
            });
        },

        /**
         * @private
         */
        onPanelRequestSubmit: function () {
            if (this.model.get("canSubmit")) {
                this.submit();
            }
        },

        // -- Model -> UI ----------------------------------------------------------------------------------------------

        /**
         * @private
         */
        onChangeFieldId: function () {
            this.nextButton.setLabel(this.text.nextButton());
        },

        /**
         * @private
         */
        onChangeCanSubmit: function (model) {
            if (model.get("canSubmit")) {
                this.nextButton.enable();
            } else {
                this.nextButton.disable();
            }
        },

        /**
         * @private
         */
        onChangeIsSubmitted: function (model) {
            if (model.get("isSubmitted")) {
                this.panel.disable();
            } else {
                this.panel.enable();
            }
        },

        // -- Model -> Model (calculated attributes) -------------------------------------------------------------------

        /**
         * @private
         */
        calculateIsValid: function (model) {
            model.set("isValid", !!model.get("fieldName"));
        },

        /**
         * @private
         */
        calculateCanSubmit: function (model) {
            model.set("canSubmit", model.get("isValid") && !model.get("isSubmitted"));
        },

        isDirty: function () {
            return !!this.model.get('fieldName') || this.panel.isDirty();
        },

        _initializeFieldsOnScreen: function (model, fields) {
            var fieldsOnAllScreens = {};
            _.each(fields, function (field) {
                fieldsOnAllScreens[field.id] = field.onAllScreens;
            });
            model.set("fieldsOnAllScreens", fieldsOnAllScreens);
        }
    });
    return FieldWisherStepDialog;
});
