AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:custom-fields",
    "com.atlassian.jira.jira-project-config-plugin:custom-fields-impl"], function () {

    var $ = require("jquery");
    var Backbone = require("jira-project-config/backbone");
    var Marionette = require("jira-project-config/marionette");
    var _ = require("underscore");
    var Button = require("jira-project-config/custom-fields/views/dialog/button");
    var Dialog = require("jira-project-config/custom-fields/views/dialog");

    var HIDE = "dialog:panel:hide";
    var SHOW = "dialog:panel:show";

    module("JIRA.Admin.CustomFields.Dialog", {
        setup: function () {

            this.Backbone = Backbone;
            this.Marionette = Marionette;
            var sandbox = this.sandbox = sinon.sandbox.create();
            this.Dialog = Dialog;

            this.dialogId = getId();
            this.dialog = new this.Dialog({id: this.dialogId});
            this.pageObject = new DialogPageObject(this.dialogId);

            this.TestView = this.Marionette.ItemView.extend({
                initialize: function (options) {
                    this.name = options.name;
                    this.listenTo(this, 'all', function (name) {
                        this.events.push(name);
                    }, this);
                    _.bindAll(this, "template");
                    this.reset();
                },
                tagName: 'div',
                template: function () {
                    var $templ = $("<div>").append($("<p>").text(this.name));
                    $templ.attr({id: this.name});
                    return $templ;
                },
                containsEvents: function () {
                    return _.intersection(this.events, arguments).length > 0;
                },
                reset: function () {
                    this.events = [];
                    this.onDialogPanelHide = sandbox.stub();
                    this.onDialogPanelShow = sandbox.stub();
                },
                html: function (arg) {
                    this.$el.html(arg);
                    return this;
                }
            });
        },
        teardown: function () {
            this.sandbox.restore();
            this.dialog.destroy();
        }
    });

    var triggerEscape = function () {
        var e = $.Event("keydown", {keyCode: $.ui.keyCode.ESCAPE});
        $(document).trigger(e);
    };

    var DialogPageObject = function (id) {
        this.$el = $(document.getElementById(id));
    };

    _.extend(DialogPageObject.prototype, {
        visible: function () {
            return this.$el.is(":visible");
        },
        headerTitleText: function () {
            return this.$el.find(".dialog-title :header").text();
        },
        headerActions: function () {
            return this.$el.find(".jira-dialog-title-actions *");
        },
        buttons: function () {
            var buttons = [];
            this.$el.find("div.dialog-button-panel button").each(function () {
                buttons.push(new DialogButton($(this)));
            });
            return buttons;
        },
        links: function () {
            var buttons = [];
            this.$el.find("div.dialog-button-panel a").each(function () {
                buttons.push(new DialogLink($(this)));
            });
            return buttons;
        },
        openPanel: function (name) {
            var $buttons = this.$el.find("ul.dialog-page-menu button").filter(function () {
                return $(this).text() === name;
            });

            if ($buttons.length === 1) {
                $buttons.click();
            } else {
                throw "Could not find panel called '" + name + "'.";
            }
        },
        panelBody: function () {
            return this.$el.find(".dialog-panel-body:visible").text();
        },
        errorsInForm: function () {
            var el = this._errorElement();
            return el.length && el.parent().is("form");
        },
        errors: function () {
            return this._errorElement().text();
        },
        _errorElement: function () {
            return this.$el.find("div.aui-message.error");
        },
        notification: function () {
            return $.trim(this.$el.find(".jira-dialog-notification").text());
        }
    });

    var DialogButton = function ($el) {
        this.$el = $el;
    };

    _.extend(DialogButton.prototype, {
        text: function () {
            return this.$el.text();
        },
        click: function () {
            return this.$el.click();
        },
        getId: function () {
            return this.$el.attr("id");
        },
        title: function () {
            return this.$el.attr("title");
        },
        accessKey: function () {
            return this.$el.prop("accessKey");
        }
    });

    var DialogLink = function ($el) {
        this.$el = $el;
    };

    _.extend(DialogLink.prototype, {
        text: function () {
            return this.$el.text();
        },
        click: function () {
            return this.$el.click();
        },
        isLeft: function () {
            return this.$el.hasClass("secondary");
        },
        hasIcon: function () {
            return !!this._getIcon().length;
        },
        iconHasClass: function (cl) {
            return this._getIcon().hasClass(cl);
        },
        _getIcon: function () {
            return this.$el.find("span.icon");
        },
        title: function () {
            return this.$el.attr("title");
        },
        accessKey: function () {
            return this.$el.prop("accessKey");
        }
    });

    var getId = (function () {
        var id = 0;
        return function () {
            return "cf-dialog-test" + (id++);
        };
    })();

    test("Dialog.addHeaderMain() accepts HTML", function () {
        this.dialog.addHeaderMain("<h2>SomeHeader</h2>");
        equal(this.pageObject.headerTitleText(), "SomeHeader", "Heading set on the dialog");
    });

    test("Dialog.addOrSetHeaderMainText() wraps text in appropriate markup.", function () {
        this.dialog.addOrSetHeaderMainText("SomeHeader");
        equal(this.pageObject.headerTitleText(), "SomeHeader", "Heading set on the dialog");
    });

    test("Dialog.addHeaderAction() puts HTML into the header.", function () {
        var text = "foo";

        var view = new (this.Marionette.ItemView.extend({
            template: function () {
                return text;
            }
        }))();

        equal(this.pageObject.headerActions().length, 0, "Expected no actions initially.");

        this.dialog.addHeaderAction(view);

        equal(this.pageObject.headerActions().text(), text, "Expected my single action.");
    });

    test("Dialog addButton", function () {
        ok(!this.pageObject.buttons.length, "No buttons should be present.");

        var onclick = this.sandbox.stub();
        this.dialog.addButton({
            label: "example",
            click: onclick,
            submitAccessKey: true
        });

        var buttons = this.pageObject.buttons();
        deepEqual(_.map(buttons, function (el) {
            return el.text();
        }), ["example"], "One button should be present");
        buttons[0].click();
        ok(onclick.calledOnce, "Callback should have been called on click.");
        ok(!!buttons[0].title(), "Button has title.");
        ok(!!buttons[0].accessKey(), "Button has acccessKey.");

        this.dialog.addButton({
            label: "withId",
            id: "withIdButton"
        });

        buttons = this.pageObject.buttons();
        deepEqual(_.map(buttons, function (el) {
            return el.text();
        }), ["example", "withId"], "One button should be present");
        buttons[0].click();
        ok(!!buttons[0].title(), "Button has title.");
        ok(!!buttons[0].accessKey(), "Button has acccessKey.");
        ok(onclick.calledTwice, "Callback should have been called on click.");
        ok(!buttons[0].getId(), "First button should have no ID.");
        equal(buttons[1].getId(), "withIdButton", "Second button should have an ID.");
        ok(!buttons[1].title(), "Button does not have title.");
        ok(!buttons[1].accessKey(), "Button does not have acccessKey.");
    });

    test("Dialog addButton object", function () {
        ok(!this.pageObject.buttons.length, "No buttons should be present.");

        var onclick = this.sandbox.stub();
        var button = this.dialog.addButton({
            label: "example"
        });

        button.click(onclick);

        var buttons = this.pageObject.buttons();
        deepEqual(_.map(buttons, function (el) {
            return el.text();
        }), ["example"], "One button should be present");
        buttons[0].click();
        ok(onclick.calledOnce, "Callback should have been called on click.");
    });

    test("Dialog addLinks", function () {
        ok(!this.pageObject.buttons.length, "No links should be present.");

        var onclick = this.sandbox.stub();
        var leftClick = this.sandbox.stub();
        var cancelClick = this.sandbox.stub();
        this.dialog.addLink({label: "example", click: onclick});
        this.dialog.addLeftLink({label: "leftExample", click: leftClick});
        this.dialog.addLink({label: "Cancel", click: cancelClick, cancelAccessKey: true});


        var links = this.pageObject.links();
        deepEqual(_.map(links, function (el) {
            return el.text();
        }), ["example", "leftExample", "Cancel"], "Three links should be present");
        ok(!links[0].isLeft(), "Right link should not be left aligned.");
        ok(!links[0].title(), "Right link should not have title.");
        ok(!links[0].accessKey(), "Right link should not have access key.");

        ok(links[1].isLeft(), "Left link should be left aligned.");
        ok(!links[1].title(), "Left link should not have title.");
        ok(!links[1].accessKey(), "Left link should not have access key.");

        ok(!links[2].isLeft(), "Cancel link should not be left aligned.");
        ok(links[2].title(), "Cancel link should have title.");
        ok(links[2].accessKey(), "Cancel link should have access key.");

        links[0].click();
        ok(onclick.calledOnce, "Callback should have been called on click.");
        ok(!leftClick.called, "Left click should not have been called.");
        ok(!cancelClick.called, "Cancel click should not have been called.");

        links[1].click();
        ok(leftClick.calledOnce, "Left click callback should have been fired.");
        ok(!cancelClick.called, "Cancel click should not have been called.");

        links[2].click();
        ok(cancelClick.called, "Cancel click should have been called.");
    });

    test("Dialog addLeftLink", function () {
        ok(!this.pageObject.buttons.length, "No links should be present.");

        this.dialog.addLeftLink({label: "noIcon"});
        this.dialog.addLeftLink({label: "withIcon", iconClass: "iconclass"});

        var links = this.pageObject.links();
        deepEqual(_.map(links, function (el) {
            return el.text();
        }), ["noIcon", "withIcon"], "Two links should be present");
        ok(_.all(links, function (el) {
            return el.isLeft();
        }), "Links should be left aligned");

        ok(!links[0].hasIcon(), "Link should not have icon.");
        ok(links[1].hasIcon(), "Link should have icon.");
        ok(links[1].iconHasClass("iconclass"), "Link should have icon class.");
    });

    test("Panels on hidden dialog.", function () {
        var one = new this.TestView({name: "One"});
        var two = new this.TestView({name: "Two"});

        var assertPanelState = function (expectedName, expectedView, actualPanel) {
            ok(actualPanel.view === expectedView, "Panel has correct view?");
            ok(actualPanel.name === expectedName, "Panel has correct name?");
        };
        var assertNoEvents = function (view) {
            ok(!view.containsEvents(HIDE, SHOW), "No events when hidden.");
            ok(!view.onDialogPanelHide.called, "No events for hide.");
            ok(!view.onDialogPanelShow.called, "No events for show.");
        };

        var onePanel = this.dialog.addPanel("One", one);
        var currentPanel = this.dialog.currentPanel();

        ok(onePanel === currentPanel, "Panel changed?");
        assertPanelState("One", one, onePanel);
        assertNoEvents(one);

        var twoPanel = this.dialog.addPanel("Two", two);
        currentPanel = this.dialog.currentPanel();
        ok(twoPanel === currentPanel, "Panel changed?");
        assertPanelState("Two", two, twoPanel);
        assertNoEvents(one);
        assertNoEvents(two);

        this.pageObject.openPanel("One");
        currentPanel = this.dialog.currentPanel();
        ok(onePanel === currentPanel, "Panel changed?");
        assertNoEvents(one);
        assertNoEvents(two);
    });

    test("Panels on visible dialog.", function () {
        var one = new this.TestView({name: "One"});
        var two = new this.TestView({name: "Two"});

        ok(!this.pageObject.visible(), "Dialog should be hidden.");
        this.dialog.show();
        ok(this.pageObject.visible(), "Dialog should be shown.");

        var assertPanelState = function (expectedName, expectedView, actualPanel) {
            ok(actualPanel.view === expectedView, "Panel has correct view?");
            ok(actualPanel.name === expectedName, "Panel has correct name?");
        };

        var assertShowPanelEvents = function (expectedView) {
            ok(expectedView.containsEvents(SHOW), "Show event on panel?");
            ok(!expectedView.containsEvents(HIDE), "No hide event on panel?");
            ok(expectedView.onDialogPanelShow.calledOnce, "Show method called on panel?");
            ok(!expectedView.onDialogPanelHide.called, "Hide method not called on panel?");
        };

        var assertHidePanelEvents = function (expectedView) {
            ok(!expectedView.containsEvents(SHOW), "No show event on panel?");
            ok(expectedView.containsEvents(HIDE), "Hidden event on panel?");
            ok(!expectedView.onDialogPanelShow.called, "Show method not called on panel?");
            ok(expectedView.onDialogPanelHide.calledOnce, "Hide method called on panel?");
        };

        var assertNoEvents = function (view) {
            ok(!view.containsEvents(HIDE, SHOW), "No events on view.");
            ok(!view.onDialogPanelHide.called, "Hide method not called on panel?");
            ok(!view.onDialogPanelShow.called, "Show method not called on panel?");
        };

        var onePanel = this.dialog.addPanel("One", one);
        var currentPanel = this.dialog.currentPanel();
        ok(onePanel === currentPanel, "Panel changed?");
        assertPanelState("One", one, onePanel);
        assertShowPanelEvents(one);
        equal(this.pageObject.panelBody(), "One", "Panel Body Set?");

        one.reset();
        var twoPanel = this.dialog.addPanel("Two", two);
        currentPanel = this.dialog.currentPanel();
        ok(twoPanel === currentPanel, "Panel changed?");
        assertPanelState("Two", two, twoPanel);
        assertShowPanelEvents(two);
        assertHidePanelEvents(one);
        equal(this.pageObject.panelBody(), "Two", "Panel Body Set?");

        two.reset();
        one.reset();
        this.pageObject.openPanel("One");
        currentPanel = this.dialog.currentPanel();

        ok(onePanel === currentPanel, "Panel changed?");
        assertShowPanelEvents(one);
        assertHidePanelEvents(two);
        equal(this.pageObject.panelBody(), "One", "Panel Body Set?");

        one.reset();
        two.reset();
        this.dialog.destroy();
        assertHidePanelEvents(one);
        assertNoEvents(two);
    });

    test("destroy dialog", function () {
        ok(!this.pageObject.visible(), "Dialog should be hidden.");
        this.dialog.show();
        ok(this.pageObject.visible(), "Dialog should be shown.");
        this.dialog.destroy();
        ok(!this.pageObject.visible(), "Dialog should be hidden.");
        ok(!document.getElementById(this.dialogId), "Dialog element should have nuked!");
    });

    test("ESC fires event", function () {

        var stub = this.sandbox.stub();
        this.dialog.on("close", stub);

        ok(!this.pageObject.visible(), "Dialog should be hidden.");
        triggerEscape();
        ok(!stub.called, "Dialog should not fire event when closed.");

        this.dialog.show();
        ok(this.pageObject.visible(), "Dialog should be visible.");
        triggerEscape();
        ok(stub.calledOnce, "Dialog should fire closed event when open.");
        ok(this.pageObject.visible(), "Dialog should be visible.");

        //Should not get events after dialog close.
        this.dialog.destroy();
        triggerEscape();
        ok(stub.calledOnce, "Dialog should *not* fire closed event when closed.");
        ok(!this.pageObject.visible(), "Dialog should be hidden.");
        ok(!document.getElementById(this.dialogId), "Dialog element should have nuked!");
    });

    test("Errors displayed", function () {
        this.dialog.show();
        var one = new this.TestView({name: "One"});
        this.dialog.addPanel("One", one);

        this.dialog.showErrors(["Error"]);
        equal(this.pageObject.errors(), "Error", "Error should be displayed");

        this.dialog.showErrors(["Error2", "Error3"]);
        equal(this.pageObject.errors(), "Error2 Error3", "Error2 Error3 should be displayed");

        this.dialog.showErrors();
        ok(!this.pageObject.errors(), "Error should have been cleared.");

        one.html("<form><input type='text' name='Random'/></form>");
        this.dialog.showErrors(["Error2", "Error5"]);
        equal(this.pageObject.errors(), "Error2 Error5", "Error2 Error5 should be displayed");
        ok(this.pageObject.errorsInForm(), "Error should be in the first form");

    });

    var createWarningView = (function () {
        var WarningView = Marionette.ItemView.extend({
            initialize: function (options) {
                this.message = options.message;
            },
            render: function () {
                this.$el.text(this.message);
                return this;
            }
        });
        return function (message) {
            return _.extend(new WarningView({message: message}), {
                onDialogNotificationHide: this.sandbox.stub(),
                onDialogNotificationShow: this.sandbox.stub()
            });
        };
    }());

    test("Warning Notification can be changed.", function () {
        var view1 = createWarningView.call(this, "message1");
        var view2 = createWarningView.call(this, "message2");

        this.dialog.show();
        this.dialog.setWarningNotification(view1);

        ok(view1.onDialogNotificationShow.calledOnce, "Notification display method called.");
        equal(this.pageObject.notification(), view1.message, "Notification displayed.");

        ok(!view1.onDialogNotificationHide.called, "Notification hide method not called.");
        this.dialog.setWarningNotification(view2);
        ok(view1.onDialogNotificationHide.calledOnce, "Notification hide called when notification changed.");
        ok(view2.onDialogNotificationShow.calledOnce, "Notification display method called on message change.");

        equal(this.pageObject.notification(), view2.message, "Notification changed.");

        ok(!view2.onDialogNotificationHide.called, "Notification hide not called dialog closed.");
        this.dialog.destroy();
        ok(view2.onDialogNotificationHide.calledOnce, "Notification hide called on dialog close.");
    });

    test("Warning Notification shown on initial load.", function () {
        var view = createWarningView.call(this, "one");
        this.dialog.setWarningNotification(view);

        ok(!view.onDialogNotificationShow.called, "Notification show called until dialog shown.");
        this.dialog.show();
        ok(view.onDialogNotificationShow.calledOnce, "Notification show called once dialog shown.");
        ok(!view.onDialogNotificationHide.called, "Notification hide not called until dialog hidden.");
        equal(this.pageObject.notification(), view.message, "Notification displayed.");

        this.dialog.destroy();
        ok(view.onDialogNotificationHide.calledOnce, "Notification hide called when dialog hidden.");
    });

    module("JIRA.Admin.CustomFields.Dialog.Button", {
        setup: function () {
            this.Button = Button;
            this.Dialog = Dialog;
        }
    });

    /**
     * A page object for verifying the DOM for a button.
     *
     * @param $element
     * @constructor
     */
    var ButtonPageObject = function ($element) {
        this.$element = $element;
    };

    _.extend(ButtonPageObject.prototype, {
        isEnabled: function () {
            return !this.$element.prop("disabled");
        }
    });

    test("Dialog.addButton() returns the button.", function () {
        var dialog = new this.Dialog({id: "dialogId"});

        var button = dialog.addButton({
            label: "label",
            id: "id"
        });

        ok(button instanceof this.Button, "Expected an instance of Button to be returned.");

        dialog.destroy();
    });

    test("Buttons can be disabled", function () {
        var element = $("<button/>");
        var pageObject = new ButtonPageObject(element);
        var button = new this.Button(element);

        ok(pageObject.isEnabled(), "Expected the button to be in it's default state.");
        button.disable();
        ok(!pageObject.isEnabled(), "Expected the button to be disabled.");
        button.enable();
        ok(pageObject.isEnabled(), "Expected the button to be enabled.");
    });
});
