define("jira-project-config/custom-fields/steps/type/views/text", ["jira/util/formatter"], function(formatter) {
    return {
        heading: formatter.I18n.getText("admin.project.customfieldtypes.title"),
        panel: "",
        cancel: formatter.I18n.getText("common.forms.cancel"),
        previous: formatter.I18n.getText("common.forms.previous"),
        next: formatter.I18n.getText("common.forms.next"),
        marketPlace: formatter.I18n.getText("admin.issuefields.customfields.extentions.button")
    };
});


define("jira-project-config/custom-fields/steps/type/views/type-choice", [
    'jira-project-config/libs/ajshelper',
    "jira-project-config/custom-fields/views/field-type-collection",
    "jira-project-config/custom-fields/views/quick-search",
    "jira-project-config/custom-fields/views/dialog",
    "jira-project-config/custom-fields/steps/type/views/text",
    "jira/util/users/logged-in-user",
    "jira-project-config/backbone",
    "wrm/context-path",
    "underscore"
], function(
    AJSHelper,
    FieldTypeCollectionView,
    QuickSearch,
    Dialog,
    texts,
    User,
    Backbone,
    contextPath,
    _) {
    var urls = {
        marketplace: contextPath() + "/plugins/servlet/upm/marketplace/popular?category=Custom+Fields&source=custom_fields_create"
    };

    var TypeChoice = function (categories, searchDelay, sharedBy, inIssueContext) {
        this.categories = categories;
        this.searchDelay = searchDelay || 0;
        this.sharedBy = sharedBy;
        this.inIssueContext = inIssueContext;
    };

    _.extend(TypeChoice.prototype, Backbone.Events, {
        show: function () {
            if (!this.dialog) {
                var allPanel;
                var analyticsInits = [];
                var collectionViews = [];
                var defaultPanel = null;

                var dialog = new Dialog({
                    id: "customfields-select-type",
                    // Avoid the 'Next' button from being focused.
                    focusSelector: ".dialog-title input"
                });

                var instance = this;
                var nextButton;

                var quickSearch = new QuickSearch({
                    delay: this.searchDelay
                });

                this.listenTo(quickSearch, "change", function (query) {
                    _.each(instance.categories, function (category) {
                        category.types.filter(query.value);
                    });

                    var anyMatches = _.any(_.pluck(instance.categories, "types"), function (types) {
                        return types.length;
                    });
                    _.each(collectionViews, function (collectionView) {
                        collectionView.setHasMatches(anyMatches);
                    });
                });

                dialog.addOrSetHeaderMainText(texts.heading);
                dialog.addHeaderAction(quickSearch);
                if (this.sharedBy) {
                    dialog.setWarningNotification(this.sharedBy);
                }

                // Market Place
                if (User.isSysadmin()) {
                    dialog.addLeftLink({
                        label: texts.marketPlace,
                        url: urls.marketplace,
                        iconClass: "jira-icon-marketplace"
                    });
                }

                if (this.inIssueContext) {
                    dialog.addButton({
                        secondary: true,
                        label: texts.previous,
                        click: function () {
                            instance._firePrevious(dialog.currentPanel().view.getSelectedModel());
                        },
                        id: "customfields-select-type-previous"
                    });
                }

                // Navigation
                nextButton = dialog.addButton({
                    label: texts.next,
                    submitAccessKey: true,
                    click: function () {
                        instance._fireSubmit(dialog.currentPanel().view.getSelectedModel());
                    },
                    id: "customfields-select-type-next"
                });

                this.listenTo(dialog, "close", _.bind(this._fireCancel, this, true));
                dialog.addLink({
                    label: texts.cancel,
                    click: _.bind(this._fireCancel, this, false),
                    cancelAccessKey: true
                });

                _.each(this.categories, function (cat, index) {
                    var collectionView = new FieldTypeCollectionView({
                        collection: cat.types
                    });

                    collectionViews.push(collectionView);

                    instance.listenTo(collectionView, "submit", function () {
                        instance._fireSubmit(collectionView.getSelectedModel());
                    });

                    instance.listenTo(collectionView, "dialog:panel:show", function () {
                        quickSearch.focus();

                        var changedSelectionHandler = function (event) {
                            event.key ? nextButton.enable() : nextButton.disable();
                        };

                        instance.listenTo(collectionView, "changedSelection", changedSelectionHandler);
                        collectionView.once("dialog:panel:hide", function () {
                            instance.stopListening(collectionView, "changedSelection", changedSelectionHandler);
                        }, instance);

                        changedSelectionHandler({key: collectionView.getSelectedKey()});
                    });

                    instance.listenTo(collectionView, "changeToAll", function () {
                        allPanel && allPanel.show();
                    });

                    // Setup analytics after the dialog has been shown.
                    analyticsInits.push(function () {
                        instance._initAnalytics(cat, collectionView);
                    });

                    var panel = dialog.addPanel(cat.name, collectionView);
                    if (cat.initial) {
                        defaultPanel = panel;
                        instance.__currentCategory = cat;
                    }

                    if (index === 0) {
                        allPanel = panel;
                    }
                });
                defaultPanel && defaultPanel.show();
                dialog.show();

                // Now that the dialog is shown, setup all the analytics. This avoids the initial view.show() when the
                // dialog is presented.
                _.each(analyticsInits, function (init) {
                    init();
                });
                this.dialog = dialog;
            } else {
                this.dialog.show();
            }
        },

        hide: function () {
            this.dialog && this.dialog.hide();
        },

        destroy: function () {
            if (this.dialog) {
                this.stopListening(this.dialog);
                this.dialog.destroy();
                delete this.dialog;
            }
        },

        _initAnalytics: function (category, view) {
            var instance = this;

            // Record when the user changes category.
            this.listenTo(view, "dialog:panel:show", function () {
                instance.__currentCategory = category;

                AJSHelper.trigger("analytics", {
                    name: "administration.customfields.addwizard.changecategory",
                    data: {
                        id: category.id,
                        name: category.name
                    }
                });
            });
        },

        _fireSubmit: function (model) {
            // Record when a type is chosen; include the current category.
            AJSHelper.trigger("analytics", {
                name: "administration.customfields.addwizard.submittype",
                data: {
                    key: model.get('key'),
                    category: this.__currentCategory.id
                }
            });

            this.trigger("submit", model);
        },
        _firePrevious: function (model) {
            this.trigger("previous", model);
        },
        _fireCancel: function (confirm) {
            this.trigger("cancel", confirm);
        },
        showLoading: function () {
            this.dialog && this.dialog.showLoading();
        },
        hideLoading: function () {
            this.dialog && this.dialog.hideLoading();
        },
        /**
         * Always returns true because there is always a selected type.
         * @returns {boolean} always returns true
         */
        isDirty: function () {
            return true;
        }
    });
    return TypeChoice;
});

