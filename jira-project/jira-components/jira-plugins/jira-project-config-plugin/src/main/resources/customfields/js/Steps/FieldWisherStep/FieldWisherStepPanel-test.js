AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:custom-fields",
    "com.atlassian.jira.jira-project-config-plugin:custom-fields-impl"], function () {

    var GroupDescriptor = require("jira/ajs/list/group-descriptor");
    var ItemDescriptor = require("jira/ajs/list/item-descriptor");
    var formatter = require("jira/util/formatter");
    var FieldWisherStep = require("jira-project-config/custom-fields/steps/field-wisher");
    var Backbone = require("jira-project-config/backbone");

    module("JIRA.Admin.CustomFields.FishWisherStep.Panel", {
        setup: function () {
            this.sandbox = sinon.sandbox.create();
            this.Panel = FieldWisherStep.Panel;
            this.fields = [
                {id: "id1", name: "name1"},
                {id: "id2", name: "name2"}
            ];
            this.panel = new this.Panel({fields: this.fields}).render();
        },

        teardown: function () {
            this.sandbox.restore();
        }
    });

    // ---------------------------------------------------------------------------------------------------------------------

    var PageObject = Backbone.Marionette.View.extend({
        ui: {
            label: "label",
            select: "select",
            frother: "input.aui-ss-field",
            form: "form",
            helptext: "span.description"
        },

        initialize: function () {
            this.bindUIElements();
        },

        // -- API

        label: function () {
            return this.ui.label.text();
        },

        helptext: function () {
            this.ui.helptext.text();
        },

        submit: function () {
            this.ui.form.trigger("submit");
        },

        /**
         * @param options.id
         * @param options.name
         */
        select: function (options) {
            this.ui.select.trigger("selected", {
                properties: {
                    value: options.id,
                    label: options.name
                }
            });
        }
    });

    // ---------------------------------------------------------------------------------------------------------------------

    test("field", function () {
        var po;

        po = new PageObject({el: this.panel.el});

        // has label
        ok(po.label().length > 0);
    });

    test("change:field event", function () {
        var handler;
        var po;

        handler = sinon.stub();
        po = new PageObject({el: this.panel.el});
        this.panel.on("change:field", handler);

        // wish
        po.select({name: "foo"});
        ok(handler.calledWith({name: "foo"}));

        // field
        handler.reset();
        po.select({id: "foo", name: "bar"});
        ok(handler.calledWith({id: "foo", name: "bar"}));
    });

    test("request:submit event", function () {
        var handler;
        var po;

        handler = sinon.stub();
        po = new PageObject({el: this.panel.el});
        this.panel.on("request:submit", handler);

        po.submit();

        ok(handler.called);
    });

    test("frother suggester", function () {
        this.spy(formatter, "format");
        var descriptors;
        var group;
        var item;
        var suggester;

        suggester = new this.Panel.prototype.ExistingOrCreateSuggester({});
        descriptors = suggester.formatSuggestions([], "foobar");
        group = descriptors.pop();
        item = group.properties.items[0];

        ok(group instanceof GroupDescriptor);
        ok(item instanceof ItemDescriptor);
        sinon.assert.calledWith(
            formatter.format,
            "issue.operations.fields.wisher.fields.name.createoption",
            "foobar"
        );
        equal(item.fieldText(), "foobar");
    });
});

