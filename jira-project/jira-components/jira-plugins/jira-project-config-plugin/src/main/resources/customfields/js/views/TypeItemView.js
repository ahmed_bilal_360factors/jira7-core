define("jira-project-config/custom-fields/views/type-item", [
    'jira-project-config/custom-fields/templates',
    "jira-project-config/marionette"
], function(CustomFieldTemplates, Marionette) {
    "use strict";

    var TEMPLATES = CustomFieldTemplates;

    /**
     * A view for a single item in the types collection view.
     */
    return Marionette.ItemView.extend({
        className: "item",

        submit: function () {
            this.select();
            this.triggerMethod("submit", this.getKey());
        },

        events: {
            "click": "onClick",
            "dblclick": "onDoubleClick"
        },

        onClick: function (event) {
            this.triggerMethod("select");
            event.preventDefault();
        },

        onDoubleClick: function (event) {
            this.submit();
            event.preventDefault();
        },

        reveal: function () {
            var padding = 40;
            this.$el.scrollIntoViewForAuto({
                direction: "vertical",
                marginBottom: padding,
                marginTop: padding
            });
        },

        select: function () {
            this.$el.toggleClass("selected", true);
            this.reveal();
        },

        deselect: function () {
            this.$el.toggleClass("selected", false);
        },

        tagName: "li",

        template: function (serializedModel) {
            return TEMPLATES.type({type: serializedModel});
        },
        getKey: function () {
            return this.model.get('key');
        }
    });
});

AJS.namespace("JIRA.Admin.CustomFields.TypeItemView", null, require("jira-project-config/custom-fields/views/type-item"));
