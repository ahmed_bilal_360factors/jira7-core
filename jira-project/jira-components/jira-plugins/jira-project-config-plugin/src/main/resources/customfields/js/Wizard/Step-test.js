AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:custom-fields", "com.atlassian.jira.jira-project-config-plugin:custom-fields-impl"], function(){
    "use strict";

    var _ = require("underscore");
    var Step = require("jira-project-config/custom-fields/wizard/step");

    //We need to ensure that the setTimeout is restored before the test is finished as otherwise Qunit will probably
    //fail (it uses setTimeout internally).
    var timerTest = function (func) {
        return function () {
            var clock = this.clock = this.sandbox.useFakeTimers();
            try {
                func.apply(this, arguments);
            } finally {
                clock.restore();
            }
        };
    };

    var safeTest = function (name, func) {
        test.call(this, name, timerTest(func));
    };

    module("JIRA.Admin.CustomFields.Wizard.Step", {
        setup: function () {
            this.sandbox = sinon.sandbox.create();
            var StepImpl = function () {
            };
            _.extend(StepImpl.prototype, Step);
            this.step = new StepImpl();
        },
        teardown: function () {
            this.sandbox.restore();
        }
    });

    safeTest("fireNext() fires correct event", function () {
        var next = this.sandbox.stub();
        this.step.on("next", next);
        this.step.fireNext();

        ok(next.calledOnce, "Next event fired?");
    });


    safeTest("firePrevious() fires correct event", function () {
        var previous = this.sandbox.stub();
        this.step.on("previous", previous);
        this.step.firePrevious();

        ok(previous.calledOnce, "Previous event fired?");
    });

    safeTest("fireCancel() fires correct event with correct arg when confirm is true", function () {
        var cancel = this.sandbox.stub();
        this.step.on("cancel", cancel);
        this.step.fireCancel(true);

        ok(cancel.calledOnce, "cancel event fired?");
        ok(cancel.firstCall.args[0], "Argument passed with event fire should be true");
    });

    safeTest("fireCancel() fires correct event with correct arg when confirm is false or not provided", function () {
        var cancel = this.sandbox.stub();
        this.step.on("cancel", cancel);
        this.step.fireCancel(false);

        ok(cancel.calledOnce, "cancel event fired?");
        ok(!cancel.firstCall.args[0], "Argument passed with event fire should be false");

        this.step.fireCancel(false);

        ok(cancel.calledTwice, "cancel event fired?");
        ok(!cancel.secondCall.args[0], "Argument passed with event fire should be false");
    });

    safeTest("fireComplete() fires correct event", function () {
        var complete = this.sandbox.stub();
        this.step.on("complete", complete);
        this.step.fireComplete();

        ok(complete.calledOnce, "cancel event fired?");
    });
});
