AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:custom-fields",
    "com.atlassian.jira.jira-project-config-plugin:custom-fields-impl"], function () {

    var FilteredCollection = require("jira-project-config/custom-fields/filtered-collection");
    var Backbone = require("jira-project-config/backbone");

    module("JIRA.Admin.CustomFields.FilteredCollection", {
        setup: function () {
            this.FilteredCollection = FilteredCollection;
            this.Person = Backbone.Model.extend();
            this.People = this.FilteredCollection.extend({
                model: this.Person,
                comparator: 'name',
                matcher: function (model, query) {
                    return model.get("name").match(new RegExp(query));
                }
            });
            this.sandbox = sinon.sandbox.create();
        }
    });

    test("Filtering a collection", function () {
        var people = [
                {name: "Bar"},
                {name: "Baz"},
                {name: "Foo"}
            ];

        var collection = new this.People(people);

        deepEqual(collection.toJSON(), [
            {name: "Bar"},
            {name: "Baz"},
            {name: "Foo"}
        ]);

        collection.filter("(Bar|Foo)");

        deepEqual(collection.toJSON(), [
            {name: "Bar"},
            {name: "Foo"}
        ]);

        collection.filter(".*");

        deepEqual(collection.toJSON(), [
            {name: "Bar"},
            {name: "Baz"},
            {name: "Foo"}
        ]);

        collection.filter("^no match$");

        deepEqual(collection.toJSON(), []);
    });

    test("Filtering is idempotent and doesn't use broken splicing code.", function () {
        var people = [
                {name: "AB"},
                {name: "BC"},
                {name: "CD"}
            ];

        var collection = new this.People(people);

        collection.filter("B");

        deepEqual(collection.toJSON(), [
            {name: "AB"},
            {name: "BC"}
        ]);

        collection.filter("B");

        deepEqual(collection.toJSON(), [
            {name: "AB"},
            {name: "BC"}
        ]);

        collection.filter("Z");
        deepEqual(collection.toJSON(), []);
        collection.filter("Z");
        deepEqual(collection.toJSON(), []);

        collection.filter("B");

        deepEqual(collection.toJSON(), [
            {name: "AB"},
            {name: "BC"}
        ]);

        collection.filter("B");

        deepEqual(collection.toJSON(), [
            {name: "AB"},
            {name: "BC"}
        ]);
    });

    test("Filtered collection passes state between calls of match.", function () {
        var matcher = this.sandbox.stub().returns(true);
        var Collection = this.FilteredCollection.extend({
            model: this.Person,
            comparator: 'name',
            matcher: matcher
        });

        var people = [
                {name: "Bar"},
                {name: "Baz"},
                {name: "Foo"}
            ];

        var collection = new Collection(people);
        collection.filter("Z");

        ok(matcher.calledThrice, "Called filter three times.");
        ok(matcher.firstCall.args[2] === matcher.secondCall.args[2], "Correct state passed between 1st and 2nd call.");
        ok(matcher.secondCall.args[2] === matcher.thirdCall.args[2], "Correct state passed between 2st and 3rd call.");
    });
});
