define("jira-project-config/custom-fields/wizard", [
    'jira-project-config/libs/ajshelper',
    "jira/jquery/deferred",
    "jira/loading/loading",
    "jira-project-config/custom-fields/wizard/fail-reasons",
    "jira-project-config/custom-fields/wizard/events",
    "jira-project-config/backbone",
    "underscore"
], function(AJSHelper, Deferred, Loading, FailReasons, Events, Backbone, _) {
    /**
     * A general purpose wizard that allows for transitioning between Steps.
     * @constructor
     */
    var Wizard = function (options) {
        options = options || {};
        this.sharedStepOptions = options.sharedStepOptions || {};
        this._setCurrentStepWrapper(null, options.firstStep, null);

        this.autoClose = options.autoClose;
        this.dirtyMessage = options.dirtyMessage;
        this.result = Deferred();
        this.loadingTimer = null;
    };

    _.extend(Wizard.prototype, Backbone.Events, {
        /**
         * Unleash the wizard on the user.
         *
         * @returns {jQuery.Deferred} a promise representing the wizard's outcome. The promise is resolved upon successful
         * completion of the wizard. It is rejected whenever it is canceled, or there are other unhandled errors/failures.
         */
        start: function () {
            this._hasCompleted = false;

            this._showDelayedLoading(this);
            this._transitionTo(null, this.currentStepWrapper.step);

            if (this.dirtyMessage) {
                this._addDirty();
            }

            return this.result.promise();
        },

        /**
         * Closes the wizard, and stops all steps that exist.
         */
        close: function () {
            this._closeWebsudoDialogIfExists();
            if (this.currentStepWrapper) {
                if (this.dirtyMessage) {
                    this._removeDirty();
                }
                this.stopListening();
                this._applyToEachStep(function (step) {
                    this._hideDelayedLoading(step);
                    step.stopStep();
                });
                delete this.currentStepWrapper;
            }
            this._hideDelayedLoading(this);
        },

        /**
         * @returns {boolean} whether any step is dirty
         */
        isDirty: function () {
            return this._existsSomeStep(function (step) {
                return step.isDirty();
            });
        },

        /**
         * Shows a loading indicator in the middle of the screen
         */
        showLoading: function () {
            AJSHelper.dim(false);
            Loading.showLoadingIndicator();
        },

        /**
         * Hides any existing loading indicator in the middle of the screen
         */
        hideLoading: function () {
            Loading.hideLoadingIndicator();
        },

        /**
         * Binds event handlers to the provided step.
         *
         * @param step the step to bind event handlers to
         * @private
         */
        _bindToStep: function (step) {
            this.listenTo(step, Events.next, this._onNextStep);
            this.listenTo(step, Events.previous, this._onPreviousStep);

            this.listenTo(step, Events.complete, this._onComplete);
            this.listenTo(step, Events.cancel, this._onCancel);
        },

        /**
         * Handler for when a step fires a next event. Queries the current step for the next step, sets the current step
         * wrapper to that, and transitions to the resolved next step.
         *
         * When querying the next step from the current step, the existing proposed next step is passed too,
         * and if it is appropriate, the current step is expected to return what was passed.
         *
         * Upon resolving and setting the next step as the current step, it is populated with the previous data,
         * and is transitioned to.
         *
         * @private
         */
        _onNextStep: function () {
            var existingNextStepWrapper = this.currentStepWrapper.nextStepWrapper;
            var previousStepWrapper = this.currentStepWrapper;

            var proposedNextStep = existingNextStepWrapper && existingNextStepWrapper.step;
            var nextStep = this.currentStepWrapper.step.getNextStep(proposedNextStep, this.sharedStepOptions);

            if (nextStep === proposedNextStep) {
                this.currentStepWrapper = existingNextStepWrapper;
            } else {
                this._setCurrentStepWrapper(this.currentStepWrapper, nextStep, null);
            }

            this.currentStepWrapper.step.setPreviousData(previousStepWrapper.step.getAccumulatedData());
            this._transitionTo(this.currentStepWrapper.previousStepWrapper.step, this.currentStepWrapper.step);
        },

        /**
         * Handler for when a step fires a previous event. Retrieves the previous step wrapper from the current step
         * wrapper, and if it exists, then it is set as the current step wrapper, and it is transitioned to.
         *
         * @private
         */
        _onPreviousStep: function () {
            var previous = this.currentStepWrapper.previousStepWrapper;
            if (previous) {
                this.currentStepWrapper = previous;
                this._transitionTo(this.currentStepWrapper.nextStepWrapper.step, this.currentStepWrapper.step);
            }
        },

        /**
         * Transitions from one step to another. Triggers nice things like showing loading indicator on the previous step
         * until the starting of the next step has resolved, at which point, the loading indicator is hidden,
         * the previous step is paused, and the next step is shown.
         *
         * Also has handling for Websudo errors. Hides the previous step when it is determined that the Websudo dialog needs
         * to be shown, and shows the next step when Websudo access is granted, and the next step has been started.
         *
         * @param fromStep
         * @param toStep
         * @private
         */
        _transitionTo: function (fromStep, toStep) {
            var self = this;
            var startPromise = toStep.startStep();
            this._bindToStepProgress(startPromise, fromStep);
            /*
             * After the toStep has successfully been started, if there is a websudo dialog, we want to close it before
             * showing the toStep. If there is not one, then if there is a fromStep, we want to pause (hide) that.
             * Otherwise we just hide the loading indicator on the wizard.\
             */
            startPromise.always(function () {
                if (self.websudoDialog) {
                    self._closeWebsudoDialog();
                } else if (fromStep) {
                    self._hideDelayedLoading(fromStep);
                    fromStep.pauseStep();
                } else {
                    self._hideDelayedLoading(self);
                }
            }).always(_.bind(this._makeStepVisible, this, toStep));
        },

        /**
         * Binds to the provided promise the websudo progress handling.
         * @param {jQuery.Promise} promise the promise to bind the progress and completion/fail handling
         * @param {Step} currentlyVisibleStep the step that is currently visible that should be hidden when websudo is to be shown
         * @private
         */
        _bindToStepProgress: function (promise, currentlyVisibleStep) {
            var self = this;

            // If there is a currently visible step, stop listening to it and show loading on it
            currentlyVisibleStep && this.stopListening(currentlyVisibleStep);
            currentlyVisibleStep && this._showDelayedLoading(currentlyVisibleStep);
            promise.progress(function (name) {
                // If Websudo is about to be shown
                if (name === Events.websudoBeforeShow) {
                    // If there is a step to pause, pause it and hide loading on it, otherwise hide loading on the wizard
                    if (currentlyVisibleStep) {
                        self._hideDelayedLoading(currentlyVisibleStep);
                        currentlyVisibleStep.pauseStep();
                    } else {
                        self._hideDelayedLoading(self);
                    }
                    // If Websudo dialog has completed
                } else if (name === Events.websudoSuccess) {
                    var closeFunction = arguments[1];
                    /*
                     * Set the property websudoDialog so that we can close it later (if _transitionTo calls this method, it
                     * will be virtually immediate; whereas if it is _onComplete that calls this, we do not want to close the
                     * websudo dialog until we close the wizard.
                     */
                    self.websudoDialog = {
                        close: closeFunction
                    };
                }
            });
        },

        _makeStepVisible: function (step) {
            step.show();
            this._bindToStep(step);
        },

        _closeWebsudoDialogIfExists: function () {
            if (this.websudoDialog) {
                this._closeWebsudoDialog();
            }
        },

        _closeWebsudoDialog: function () {
            this.websudoDialog.close();
            delete this.websudoDialog;
        },

        /**
         * Invokes the showLoading() method of the provided shower after 500ms.
         *
         * @param shower the object to invoke showLoading() on.
         * @private
         */
        _showDelayedLoading: function (shower) {
            var self = this;
            this._cancelTimer();
            this.loadingTimer = window.setTimeout(_.bind(function () {
                self.loadingTimer = null;
                shower.showLoading();
            }, this), 500);
        },

        /**
         * Invokes the hideLoading() method of the provided hider, and cancels any existing delayed loading
         *
         * @param hider the object to invoke hideLoading() on.
         * @private
         */
        _hideDelayedLoading: function (hider) {
            this._cancelTimer();
            hider.hideLoading();
        },

        /**
         * Cancels the existing delayed loading timer.
         *
         * @private
         */
        _cancelTimer: function () {
            if (this.loadingTimer !== null) {
                window.clearTimeout(this.loadingTimer);
            }
            this.loadingTimer = null;
        },

        /**
         * Adds the special dirty onbeforeunload handler to window. This should only be called once (from the start() method)
         *
         * @private
         */
        _addDirty: function () {
            this._unload = window.onbeforeunload;
            var instance = this;

            //Why not use jQuery .on('beforeunload'). Unfortunately, jQuery replaces the onbeforeunload function
            //with something that does not delegate to the previous check. We do so manually.
            window.onbeforeunload = function () {
                if (!instance._hasCompleted && instance.dirtyMessage && instance.isDirty()) {
                    return instance.dirtyMessage;
                } else if (_.isFunction(instance._unload)) {
                    return instance._unload.apply(this, arguments);
                } else {
                    return void 0;
                }
            };
        },

        /**
         * Removes the special dirty onbeforeunload handler from window. This should only be called once, and only after
         * a call to _addDirty() has been made.
         *
         * @private
         */
        _removeDirty: function () {
            window.onbeforeunload = this._unload;
            delete this._unload;
        },

        _onComplete: function () {
            var currentStep = this.currentStepWrapper.step;
            var completePromise = currentStep.doComplete();
            this._bindToStepProgress(completePromise, currentStep);
            completePromise.done(_.bind(this._onCompleteSuccess, this))
                .fail(_.bind(this._onCompleteFailure, this));
        },

        /**
         * Handler for when a step has requested completion, and has finished completing.
         *
         * @private
         */
        _onCompleteSuccess: function () {
            this._hasCompleted = true;

            this.result.resolve(this.currentStepWrapper.step.getAccumulatedData());
            if (this.autoClose) {
                this.close();
            }
        },

        _onCompleteFailure: function (failReason) {
            var currentStep;
            var errors;
            if (failReason === FailReasons.recoverableError) {
                currentStep = this.currentStepWrapper.step;
                errors = arguments[1];
                if (this.websudoDialog) {
                    // If there is a websudo dialog, the current step has been hidden. We should close the websudo dialog and
                    // reshow the step to get it into its original state
                    this._closeWebsudoDialog();
                    currentStep.show();
                } else {
                    // If there is no websudo dialog, the current step is already visible, so we just need to get rid of the
                    // loading indicator
                    this._hideDelayedLoading(currentStep);
                }
                this._bindToStep(currentStep);
                currentStep.showErrors(errors.fields, errors.general);
            } else {
                this.result.reject(failReason, {
                    data: this.currentStepWrapper.step.getAccumulatedData(),
                    dirty: this.isDirty(),
                    notCompletedDescriptions: arguments[1]
                });
                this.close();
            }
        },

        /**
         * Handler for when a step announces that the wizard should be cancelled. If the provided parameter is true,
         * then dirtiness will be checked, and if dirty, a dirty message will be displayed. Otherwise, it simply closes
         * the dialog and rejects the result.
         *
         * @param {boolean} check whether we should check whether the wizard is dirty
         * @private
         */
        _onCancel: function (check) {
            if (check && !this._hasCompleted && this.dirtyMessage && this.isDirty()) {
                if (!window.confirm(this.dirtyMessage)) {
                    return;
                }
            }
            this.result.reject(FailReasons.cancel);
            this.close();
        },


        /**
         * Iterates over each of the current step's previous steps and existing next steps, and applies the provided
         * iterator to each.
         *
         * @param {Function} iterator the function to apply to each of the steps. Is provided a step as a parameter.
         * @private
         */
        _applyToEachStep: function (iterator) {
            this._forEachStep(function (step) {
                iterator.call(this, step);
            });
        },

        /**
         * Iterates over each of the current step's previous steps and existing next steps, and executes the provided
         * test to each.
         *
         * @param {Function} test the function to apply to each of the steps. Is provided a step as a parameter.
         * @returns {boolean} whether the test passes for any step.
         * @private
         */
        _existsSomeStep: function (test) {
            return !!this._forEachStep(function (step) {
                return test.call(this, step);
            });
        },

        /**
         * Should not be used. Use one of the above methods: _applyToEachStep(iterator) or _existsSomeStep(test)
         *
         * @private
         */
        _forEachStep: function (func) {
            var returnVal;
            var stepWrapper;
            for (stepWrapper = this.currentStepWrapper; stepWrapper; stepWrapper = stepWrapper.previousStepWrapper) {
                returnVal = func.call(this, stepWrapper.step);
                if (returnVal) {
                    return returnVal;
                }
            }
            for (stepWrapper = this.currentStepWrapper.nextStepWrapper; stepWrapper; stepWrapper = stepWrapper.nextStepWrapper) {
                returnVal = func.call(this, stepWrapper.step);
                if (returnVal) {
                    return returnVal;
                }
            }
        },

        /**
         * Convenience method to set the current step wrapper.
         *
         * @param {object} prev the previous step wrapper
         * @param {Step} current the current step
         * @param {object} next the next step wrapper
         * @private
         */
        _setCurrentStepWrapper: function (prev, current, next) {
            this.currentStepWrapper = {
                previousStepWrapper: prev,
                step: current,
                nextStepWrapper: next
            };
            prev && (prev.nextStepWrapper = this.currentStepWrapper);
        }
    });

    return Wizard;
});

AJS.namespace("JIRA.Admin.CustomFields.Wizard", null, require("jira-project-config/custom-fields/wizard"));

AJS.namespace("JIRA.Admin.CustomFields.Wizard.EVENTS", null, require("jira-project-config/custom-fields/wizard/events"));

AJS.namespace("JIRA.Admin.CustomFields.Wizard.FAIL_REASON", null, require("jira-project-config/custom-fields/wizard/fail-reasons"));
