AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:custom-fields", "com.atlassian.jira.jira-project-config-plugin:custom-fields-impl"], function(){
    "use strict";

    var ErrorDialog = require("jira-project-config/custom-fields/error-dialog");
    var jQuery = require("jquery");

    module("JIRA.Admin.CustomFields.ErrorDialog", {
        setup: function () {
            this.sandbox = sinon.sandbox.create();
            this.ErrorDialog = ErrorDialog;
        },

        teardown: function () {
            this.sandbox.restore();
        }
    });

    // ---------------------------------------------------------------------------------------------------------------------

    var PageObject = function (dialog) {
        this.dialog = dialog;
    };

    jQuery.extend(PageObject.prototype, {
        error: function () {
            return this.dialog.$el.find(".aui-message").text();
        },

        heading: function () {
            return this.dialog.$el.find(".dialog-title").text();
        },

        close: function () {
            return this.dialog.$el.find(".button-panel-link:last-child").click();
        },

        isVisible: function () {
            return this.dialog.$el.is(":visible");
        },

        isInDom: function () {
            return !!this.dialog.$el.parent().length;
        },

        press: function (name) {
            var code = jQuery.ui.keyCode[name];
            jQuery.event.trigger({
                type: "keydown",
                which: code,
                keyCode: code
            });
        }
    });

    // ---------------------------------------------------------------------------------------------------------------------

    test("heading", function () {
        var dialog;
        var po;

        dialog = new this.ErrorDialog({
            heading: "Foo"
        });
        po = new PageObject(dialog);

        equal(po.heading(), "Foo");

        dialog.hide();
    });

    test("message", function () {
        var dialog;
        var po;

        dialog = new this.ErrorDialog({
            message: "Bar"
        });
        po = new PageObject(dialog);

        equal(po.error(), "Bar");

        dialog.hide();
    });

    test("show", function () {
        var dialog;
        var po;

        dialog = new this.ErrorDialog();
        po = new PageObject(dialog);

        ok(!po.isVisible());
        dialog.show();
        ok(po.isVisible());

        dialog.hide();
    });

    test("close via button", function () {
        var onClose;
        var dialog;
        var po;

        onClose = sinon.stub();

        dialog = new this.ErrorDialog();
        dialog.on("cancel", onClose);
        po = new PageObject(dialog);

        dialog.show();
        ok(!onClose.called);
        po.close();
        ok(onClose.calledOnce);

        dialog.destroy();
    });

    test("close via keyboard", function () {
        var onClose;
        var dialog;
        var po;

        onClose = sinon.stub();

        dialog = new this.ErrorDialog();
        dialog.on("cancel", onClose);
        po = new PageObject(dialog);

        dialog.show();
        ok(!onClose.called);
        po.press("ESCAPE");
        ok(onClose.calledOnce);

        dialog.destroy();
    });
});

