AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:custom-fields",
    "com.atlassian.jira.jira-project-config-plugin:custom-fields-impl"], function () {

    var _ = require('underscore');
    var IssuesApi = require("jira-project-config/issues/api");
    var FAIL_REASON = require("jira-project-config/custom-fields/wizard/fail-reasons");
    var Backbone = require("jira-project-config/backbone");
    var Deferred = require("jira/jquery/deferred");
    var Types = require("jira/util/events/types");
    var AJSHelper = require("jira-project-config/libs/ajshelper");

    module("JIRA.Admin.CustomFields.Main", {
        setup: function () {
            this._swaps = [];
            this.sandbox = sinon.sandbox.create();
            this.sandbox.inject(this);
            this.context = AJS.test.mockableModuleContext();
            this.deferred = Deferred();
            var promise = this.deferred.promise();

            var wizard = this.wizard = function (options, initial) {
                wizard.last = this;
                this.options = options;
                this.initial = initial;
                this.start = function () {
                    return promise;
                };
                this.close = sinon.stub();
            };
            this.FAIL_REASON = wizard.FAIL_REASON = FAIL_REASON;
            this.context.mock('jira-project-config/custom-fields/wizard', wizard);

            this.FieldWisherStep = this.sandbox.spy();
            this.context.mock('jira-project-config/custom-fields/steps/field-wisher', this.FieldWisherStep);
            this.TypeStep = this.sandbox.spy();
            this.context.mock('jira-project-config/custom-fields/steps/type', this.TypeStep);
            this.Messages = {
                showSuccessMsg: this.sandbox.spy(),
                showErrorMsg: this.sandbox.spy()
            };
            this.context.mock('jira/message', this.Messages);
            // When testing, KA doesn't load and thus JIRA.Events.REFRESH_ISSUE_PAGE isn't defined, however it's
            // required.
            this.swap(Types, "REFRESH_ISSUE_PAGE", "refresh-issue-page");
            this.issuesApi = this.context.mock('jira-project-config/issues/api', {});
        },

        teardown: function () {
            _.each(this._swaps, function (undo) {
                undo();
            });
            this.sandbox.restore();
        },

        requireApi: function() {
            return this.context.require('jira-project-config/custom-fields/api');
        },

        /**
         * Swap an object for another, and restore it after the test.
         *
         * @param {*} object
         * @param {string} attr
         * @param {*} replacement
         */
        swap: function (object, attr, replacement) {
            var hasOriginal;
            var original;

            hasOriginal = attr in object;
            original = object[attr];

            object[attr] = replacement;

            this._swaps.push(function () {
                if (hasOriginal) {
                    object[attr] = original;
                } else {
                    delete object[attr];
                }
            });
        },

        /**
         * Stub an dotted path API.
         *
         * This function takes care of stubbing all intermediate
         *
         * @example
         * this.stubPath("JIRA.Issues.Api.editFieldOnSelectedIssue").returns(function () {
         *     // ...
         * });
         */
        stubPath: function (path) {
            var stub = this.stub;
            var tail = window;

            // fill out intermediary parts -- unfortunately this is more complex than i'd have hoped
            _.each(path.split('.'), function (step) {
                if (_.isObject(tail[step])) {
                    tail = tail[step];
                } else if (step in tail) {
                    tail = tail[step] = stub(tail, step);
                } else {
                    tail = (tail[step] = stub());
                }
            });

            return tail;
        }
    });

    var getAllCompletes = function (complete) {
        if (complete.completes) {
            var completes = [];
            for (var i = 0; i < complete.completes.length; i++) {
                completes = completes.concat(getAllCompletes(complete.completes[i]));
            }
            return completes;
        } else {
            return [complete];
        }
    };

    test("addCustomFieldToIssue construction", function () {
        var api = this.requireApi();
        api.addCustomFieldToIssue(10);
        var last = this.wizard.last;
        ok(last.options.sharedStepOptions.inIssueContext, "inIssueContext flag should be true");
        ok(last.options.firstStep instanceof this.FieldWisherStep, "Expected FieldWisherStep as type of first step");
    });

    test("addCustomFieldToIssue success", function () {
        var data;
        var designing;
        var _editValue;
        var _notifySuccess;
        var _notifyFailure;
        var issue = 10101;
        var api = this.requireApi();

        _editValue = this.stub(api.addCustomFieldToIssue, "_editValue");
        _editValue.returns(false);
        data = {
            field: {
                id: "customfield_1000"
            }
        };
        _notifySuccess = this.stub(api.addCustomFieldToIssue, "_notifySuccess");
        _notifyFailure = this.stub(api.addCustomFieldToIssue, "_notifyFailure");

        designing = api.addCustomFieldToIssue(issue);
        this.deferred.resolve(data);

        ok(_editValue.calledWith(data.field));
        ok(_notifySuccess.calledWith());
        ok(!_notifyFailure.called);
        equal(designing.state(), "resolved");
    });

    test("addCustomFieldToIssue fail", function () {
        var designing;
        var _editValue;
        var _notifySuccess;
        var _notifyFailure;
        var issue = 10101;
        var api = this.requireApi();

        _editValue = this.stub(api.addCustomFieldToIssue, "_editValue");
        _notifySuccess = this.stub(api.addCustomFieldToIssue, "_notifySuccess");
        _notifyFailure = this.stub(api.addCustomFieldToIssue, "_notifyFailure");

        designing = api.addCustomFieldToIssue(issue);
        this.deferred.reject('argument');

        ok(!_editValue.called);
        ok(!_notifySuccess.called);
        ok(_notifyFailure.called);
        equal(designing.state(), "rejected");
    });

    test("addCustomFieldToIssue.notifyFailure about cancel", function () {
        var api = this.requireApi();
        api.addCustomFieldToIssue._notifyFailure(this.FAIL_REASON.cancel, undefined, {});
        ok(!this.Messages.showErrorMsg.called, "Cancel not reported.");
    });

    test("addCustomFieldToIssue.notifyFailure about abort but not dirty ", function () {
        var api = this.requireApi();
        api.addCustomFieldToIssue._notifyFailure(this.FAIL_REASON.abort, undefined, {});
        ok(!this.Messages.showErrorMsg.called, "Abort not reported.");
    });

    var assertContains = function (haystack, needles) {
        for (var i = 0; i < needles.length; i++) {
            var needle = needles[i];
            ok(haystack.indexOf(needle) >= 0, "'" + haystack + "' contains '" + needle);
        }
    };

    test("addCustomFieldToIssue.notifyFailure about abort for create", function () {
        var notCompletedDescriptions = ["jisl"];
        var api = this.requireApi();
        api.addCustomFieldToIssue._notifyFailure(this.FAIL_REASON.abort, {
            data: {createField: true},
            dirty: true,
            notCompletedDescriptions: notCompletedDescriptions
        });

        ok(this.Messages.showErrorMsg.calledOnce, "Partial Abort reported.");
        var message = this.Messages.showErrorMsg.firstCall.args[0];
        assertContains(message, notCompletedDescriptions);
    });

    test("addCustomFieldToIssue.notifyFailure about abort for add existing", function () {
        var api = this.requireApi();

        api.addCustomFieldToIssue._notifyFailure(this.FAIL_REASON.abort, {dirty: true});

        ok(this.Messages.showErrorMsg.calledOnce, "Partial Abort reported.");
    });

    test("addCustomFieldToIssue.notifyFailure about error for create", function () {
        var notCompletedDescriptions = ["jisl", "jsdkajkslad", "sjdasdjhajds"];
        var api = this.requireApi();

        api.addCustomFieldToIssue._notifyFailure(this.FAIL_REASON.error, {
            data: {createField: true},
            dirty: false,
            notCompletedDescriptions: notCompletedDescriptions
        });

        ok(this.Messages.showErrorMsg.calledOnce, "Error reported.");
        var message = this.Messages.showErrorMsg.firstCall.args[0];
        assertContains(message, notCompletedDescriptions);
    });

    test("addCustomFieldToIssue.notifyFailure about error for add existing", function () {
        var api = this.requireApi();

        api.addCustomFieldToIssue._notifyFailure(this.FAIL_REASON.error, {dirty: false});

        ok(this.Messages.showErrorMsg.calledOnce, "Error reported.");
    });

    test("addCustomFieldToIssue.notifySuccess()", function () {
        var api = this.requireApi();

        api.addCustomFieldToIssue._notifySuccess();

        ok(this.Messages.showSuccessMsg.calledOnce, "Message displayed on success.");
    });

    test("addCustomFieldToIssue._editValue() returns false if Issues API is not present", function () {
        var field = {id: "customfield_1000"};
        this.context.mock('jira-project-config/issues/api', {});
        var api = this.requireApi();

        ok(!api.addCustomFieldToIssue._editValue(field), 'return falsey to indicate no editing');

        this.deferred.resolve(field);
    });

    test("addCustomFieldToIssue._editValue() returns truthy if Issues API is present", function () {
        var field = {id: "customfield_1000"};
        var mockApi = {editFieldOnSelectedIssue: sinon.stub().returns(true)};
        this.context.mock('jira-project-config/issues/api', mockApi);
        var api = this.requireApi();

        ok(api.addCustomFieldToIssue._editValue(field), 'return truthy to indicate editing');

        ok(mockApi.editFieldOnSelectedIssue.calledWith(field.id));
    });

    test("addCustomFieldToIssue._analytics", function () {
        var callback = this.stub();
        var mockIssuesApi = {
            getFieldsOnSelectedIssue: sinon.stub()
        };
        this.context.mock('jira-project-config/issues/api', mockIssuesApi);

        var api = this.requireApi();
        AJSHelper.bind("analytics", callback);

        api.addCustomFieldToIssue._analytics({createField: true});
        equal(callback.callCount, 1, "Analytics Event fired.");
        deepEqual(callback.args[0][1], {name: "administration.customfields.created.and.added.to.issue"},
            "Create & Added even fired.");

        api.addCustomFieldToIssue._analytics({addField: true});
        equal(callback.callCount, 2, "Analytics Event fired.");
        deepEqual(callback.args[1][1], {name: "administration.customfields.existing.added.to.issue"},
            "Added event fired.");

        // Editing field which has no value
        api.addCustomFieldToIssue._analytics({field: {id: 5}});
        equal(callback.callCount, 3, "Analytics Event fired.");
        deepEqual(callback.args[2][1], {name: "administration.customfields.existing.on.issue.screens.edited.has.no.value"},
            "Edit event fired.");

        // Editing field which already has a value
        var FieldOnTheIssue = Backbone.Model.extend({
            matchesFieldSelector: function () {
                return true;
            }
        });
        mockIssuesApi.getFieldsOnSelectedIssue.returns(
            new Backbone.Collection([new FieldOnTheIssue({id: 5})])
        );
        api.addCustomFieldToIssue._analytics({field: {id: 5}});
        equal(callback.callCount, 4, "Analytics Event fired.");
        deepEqual(callback.args[3][1], {name: "administration.customfields.existing.on.issue.screens.edited.has.value"},
            "Edit event fired.");

        // Editing field which has no value
        mockIssuesApi.getFieldsOnSelectedIssue.returns(new Backbone.Collection());
        api.addCustomFieldToIssue._analytics({field: {id: 5}});
        equal(callback.callCount, 5, "Analytics Event fired.");
        deepEqual(callback.args[4][1], {name: "administration.customfields.existing.on.issue.screens.edited.has.no.value"},
            "Edit event fired.");
    });

    test("addCustomField construction", function () {
        var api = this.requireApi();
        api.addCustomField();
        var last = this.wizard.last;
        ok(!last.options.sharedStepOptions.inIssueContext, "inIssueContext flag should be false");
        ok(last.options.firstStep instanceof this.TypeStep, "Expected TypeStep as type of first step");
    });

    test("addCustomField success", function () {
        var done = this.stub();
        var error = this.stub();
        var api = this.requireApi();
        api.addCustomField().done(done).fail(error);

        this.deferred.resolve();

        ok(done.calledOnce, "Done called on wizard success.");
        ok(!error.calledOnce, "Error not called on wizard success.");
    });

    test("addCustomField errror", function () {
        var done = this.stub();
        var error = this.stub();
        var api = this.requireApi();
        api.addCustomField().done(done).fail(error);

        this.deferred.reject();

        ok(error.calledOnce, "Error called on wizard error.");
        ok(!done.calledOnce, "Success not called on wizard success.");
    });
});
