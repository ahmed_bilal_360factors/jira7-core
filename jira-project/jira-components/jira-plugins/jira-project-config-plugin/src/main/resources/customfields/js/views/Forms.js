define("jira-project-config/custom-fields/views/forms/form", [
    'jira-project-config/libs/auihelper',
    "jira-project-config/custom-fields/views/forms/text-field",
    "jira-project-config/custom-fields/views/forms/text-area",
    "jira/jquery/plugins/isdirty",
    "jira-project-config/backbone",
    "underscore",
    "jquery"
], function(AUITemplates, TextField, TextArea, DirtyForm, Backbone, _, $) {
    "use strict";

    var TEMPLATES = AUITemplates.form;

    var Form = Backbone.View.extend({
        initialize: function (options) {
            var $el = this.$form = $(TEMPLATES.form(_.defaults(_.pick(options, 'id', 'action'), {
                action: '',
                content: '', //We need this to make sure we return a <form></form> for IE.
                extraClasses: DirtyForm.ClassNames.EXEMPT
            })));

            this.setElement($el);

            this.fields = {};
            this.fieldOrder = [];

            _.each(options.fields, function (field) {
                this.addField(field);
            }, this);
            this.$form.submit(_.bind(this._submit, this));

            //Hitting enter on MSIE input forms will not submit when:
            //
            //  {quote: http://www.thefutureoftheweb.com/blog/submit-a-form-in-ie-with-enter}
            //      There is more than one text/password field, but no <input type="submit"/> or <input type="image"/>
            //      is visible when the page loads.
            //  {quote}
            //
            // This seems to be roughly correct. When we initially load the dialog we do it offscreen which means that
            // enter on text input may not work. To get it to work we explicity listen for enter key.

            if ($.browser.msie) {
                this.$form.bind("keypress", function (e) {
                    if (e.keyCode === $.ui.keyCode.ENTER && $(e.target).is("input")) {
                        e.preventDefault();
                        $(this).submit();
                    }
                });
            }
        },
        addField: function (field) {
            if (field.content) {
                var content = field.content;
                if (_.isFunction(content)) {
                    content = content.call(this);
                }
                this.$form.append(content);
            } else {
                var label = '';
                if (field.label) {
                    label = TEMPLATES.label({
                        forField: field.id,
                        isRequired: !!field.required,
                        content: field.label
                    });
                }

                var fieldGroup = $(TEMPLATES.fieldGroup({
                    content: label
                }));

                var newField = new this._getField(field);
                var name = field.name || field.id;
                if (name) {
                    this.fields[name] = newField;
                }
                this.fieldOrder.push(newField);
                this.$form.append(fieldGroup.append(newField.render().$el));
            }
            return this;
        },
        setValueOfFieldWithName: function (name, value) {
            this.fields[name].value(value);
        },
        _getField: function (field) {
            if (field.field) {
                return field.field;
            } else {
                var Type = field.type;
                if (_.isString(Type)) {
                    Type = Form.DEFAULT_TYPES[Type];
                }
                var options = _.extend({}, field, {
                    form: this
                });
                return new Type(options);
            }
        },
        focus: function () {
            return this._focusFirstField(this.fieldOrder);
        },
        blur: function () {
            _.each(this.fieldOrder, function (field) {
                field.blur();
            });
            return this;
        },
        submit: function () {
            //Do this through the form so that things that events that actually listen to the form actually fire.
            this.$form.submit();
            return this;
        },
        validate: function () {
            var errors = _.reduce(this.fieldOrder, function (errors, field) {
                if (!field.validate()) {
                    errors.push(field);
                }
                return errors;
            }, [], this);

            if (errors.length) {
                this._focusFirstField(errors);
                return false;
            } else {
                return true;
            }
        },
        showErrors: function (fieldErrors, globalErrors) {
            fieldErrors = fieldErrors || {};
            globalErrors = globalErrors || [];
            var errors = _.reduce(this.fieldOrder, function (errors, field) {
                if (field.showRelevantErrors(fieldErrors, globalErrors)) {
                    errors.push(field);
                }
                return errors;
            }, [], this);

            if (errors.length) {
                this._focusFirstField(errors);
                return true;
            } else {
                return false;
            }
        },
        enable: function () {
            _.each(this.fieldOrder, function (item) {
                item.enable();
            });
            return this;
        },
        disable: function () {
            _.each(this.fieldOrder, function (item) {
                item.disable();
            });
            return this;
        },
        isDirty: function () {
            for (var i = 0; i < this.fieldOrder.length; i++) {
                if (this.fieldOrder[i].isDirty()) {
                    return true;
                }
            }
            return false;
        },
        toJSON: function () {
            return _.reduce(this.fieldOrder, function (data, field) {
                field.addValueToData(data);
                return data;
            }, {}, this);
        },
        _submit: function (e) {
            e.preventDefault();
            if (!this._interceptSubmit()) {
                if (this.validate()) {
                    this.trigger(Form.EVENTS.submit);
                }
            }
        },
        _interceptSubmit: function () {
            return _.reduce(this.fieldOrder, function (data, field) {
                return field.handleSubmit() || data;
            }, false, this);
        },
        _focusFirstField: function (fields) {
            for (var i = 0; i < fields.length; i++) {
                if (fields[i].focus()) {
                    break;
                }
            }
            return this;
        }
    }, {
        DEFAULT_TYPES: {
            text: TextField,
            textarea: TextArea
        },
        EVENTS: {
            submit: "submit"
        }
    });

    return Form;
});

define("jira-project-config/custom-fields/views/forms/form-field", ["jira-project-config/backbone", "underscore"], function(Backbone, _) {
    "use strict";

    var FormField = Backbone.View.extend({
        /**
         * Focus the form field.
         * @returns {boolean} true if the element was focused or false if the field could not be focused.
         */
        focus: function () {
            return true;
        },
        /**
         * Blur the form field.
         * @returns {boolean} true if the field was blurred or false if the field did not have focus.
         */
        blur: function () {
            return true;
        },
        /**
         * Validate the element before a submit. Any errors found should be displayed.
         * @returns {boolean} true if the field is valid or false otherwise.
         */
        validate: function () {
            return true;
        },
        /**
         * Show the relevant passed errors on the field. Any error messages "displayed" should be removed
         * from the passed arguments.
         *
         * @param {object} fieldErrors Simple hash of name to error message.
         * @param {[String]} messages A list of general error messages.
         * @returns {boolean} true if the field has consumed an error message.
         */
        // eslint-disable-next-line no-unused-vars
        showRelevantErrors: function (fieldErrors, messages) {
            return false;
        },
        /**
         * Add the data stored in the field to the passed object.
         * @param {Object} data the object to add the fields state to.
         * @returns {*} this.
         */
        // eslint-disable-next-line no-unused-vars
        addValueToData: function (data) {
        },
        /**
         * Show the passed error message on the field.
         *
         * @param error the error message to show.
         * @returns {*} this
         */
        // eslint-disable-next-line no-unused-vars
        setError: function (error) {
            return this;
        },
        /**
         * Clear any error messages shown on the field.
         *
         * @returns {*} this.
         */
        clearError: function () {
            return this;
        },
        /**
         * Return or set the value associated with the field. Passing a value will
         * set the value for the field. Passing no value will return the current state
         * of the field.
         *
         * @param {*} [value]
         * @returns {*} the field representation when getting the value or this when setting a value.
         */
        // eslint-disable-next-line no-unused-vars
        value: function (value) {
            return this;
        },
        /**
         * Disable user input into the field.
         *
         * @returns {*} this.
         */
        disable: function () {
            return this;
        },
        /**
         * Enable user input into the field.
         *
         * @returns {*} this.
         */
        enable: function () {
            return this;
        },
        /**
         * Called on each field before the form is submit. A field returning true can stop the form from submitting.
         *
         * @returns {boolean} true if the field wants to stop the submit or false otherwise.
         */
        handleSubmit: function () {
            return false;
        },
        isDirty: function () {
            return false;
        }
    }, {
        SIZE: {
            large: 'large'
        }
    });

    FormField.extend = function (object) {
        if (object.mixin) {
            var initializeCalls = [];
            object.initialize && initializeCalls.push(object.initialize);
            _.each(object.mixin, function (mixin) {
                _.each(mixin, function (value, key) {
                    if (key === "initialize") {
                        initializeCalls.push(value);
                    } else {
                        object[key] = value;
                    }
                });
            });

            object = _.omit(object, 'mixin');
            if (initializeCalls.length > 1) {
                object.initialize = function () {
                    var args = _.toArray(arguments);
                    _.each(initializeCalls, function (func) {
                        func.apply(this, args);
                    }, this);
                };
            } else if (initializeCalls.length === 1) {
                object.initialize = initializeCalls[0];
            }
        }
        return Backbone.View.extend.call(this, object);
    };

    return FormField;
});


define("jira-project-config/custom-fields/views/forms/field-mixins", [
    "jira-project-config/custom-fields/views/forms/validators",
    "underscore"
], function (Validators, _) {
    "use strict";

    return {
        NamedFieldMixin: {
            initialize: function (options) {
                this.name = options.name || options.id;
            },
            showRelevantErrors: function (fields) {
                var error = fields[this.name];
                if (error) {
                    this.setError(error);
                    delete fields[this.name];
                    return true;
                } else {
                    this.clearError();
                    return false;
                }
            },
            addValueToData: function (data) {
                var val = this.value();
                if (!_.isUndefined(val) && !_.isNull(val) && val !== "") {
                    data[this.name] = val;
                }
                return this;
            }
        },
        ValidatorMixin: {
            initialize: function (options) {
                this.validator = options.validator || Validators.always();
            },
            validate: function () {
                return this.validator.call(this);
            }
        }
    };
});


define("jira-project-config/custom-fields/views/forms/abstract-text-field", [
    'jira-project-config/libs/auihelper',
    "jira-project-config/custom-fields/views/forms/field-mixins",
    "jira-project-config/custom-fields/views/forms/form-field",
    "underscore",
    "jquery"
], function(AUITemplates, FormFieldMixins, FormField, _, $) {
    "use strict";

    var TEMPLATES = AUITemplates.form;

    return FormField.extend({
        mixin: [FormFieldMixins.ValidatorMixin, FormFieldMixins.NamedFieldMixin],
        initialize: function () {
            this.parent = FormField.prototype;
            this.parent.initialize.apply(this, arguments);
        },
        isDirty: function () {
            var el = this.$el[0];
            return el.value !== el.defaultValue;
        },
        setError: function (text) {
            if (this.$error) {
                this.$error.text(text);
            } else {
                this.$error = $(TEMPLATES.fieldError({message: text}));
                this.$el.after(this.$error);
            }
            return this;
        },
        clearError: function () {
            if (this.$error) {
                this.$error.remove();

                delete this.$error;
            }
            return this;
        },
        value: function (value) {
            if (_.isUndefined(value)) {
                return $.trim(this.$el.val()) || "";
            } else {
                this.$el.val("" + value);
                return this;
            }
        },
        focus: function () {
            if (!this.isDisabled()) {
                this.$el.focus();
                return true;
            } else {
                return false;
            }
        },
        blur: function () {
            if (this.$el.is(":focus")) {
                this.$el.blur();
                return true;
            } else {
                return false;
            }
        },
        disable: function () {
            this.$el.prop("disabled", true);
            return this;
        },
        enable: function () {
            this.$el.prop("disabled", false);
            return this;
        },
        isDisabled: function () {
            return this.$el.prop("disabled");
        },
        _getStyle: function (size) {
            switch (size) {
                case FormField.SIZE.large:
                    return 'long-field';
                default:
                    return '';
            }
        },
        _getTemplateOptions: function (options, pick, extra) {
            var templateOptions = _.extend(_.pick(options, pick), extra);
            var size = this._getStyle(options.size);
            if (size) {
                templateOptions.extraClasses = [size];
            }
            return templateOptions;
        }
    });
});

define("jira-project-config/custom-fields/views/forms/text-field", [
    'jira-project-config/libs/auihelper',
    "jira-project-config/custom-fields/views/forms/abstract-text-field",
    "jquery"
], function(AUITemplates, AbstractTextField, $) {
    "use strict";

    var TEMPLATES = AUITemplates.form;

    return AbstractTextField.extend({
        initialize: function (options) {
            this.parent = AbstractTextField.prototype;
            this.parent.initialize.apply(this, arguments);
            var templateOptions = this._getTemplateOptions(options, ['id', 'maxLength', 'value'], {
                type: "text"
            });
            this.setElement($(TEMPLATES.input(templateOptions)));
        }
    });
});

define("jira-project-config/custom-fields/views/forms/text-area", [
    'jira-project-config/libs/auihelper',
    "jira-project-config/custom-fields/views/forms/abstract-text-field",
    "jquery"
], function(AUITemplates, AbstractTextField, $) {
    "use strict";

    var TEMPLATES = AUITemplates.form;

    return AbstractTextField.extend({
        initialize: function (options) {

            this.parent = AbstractTextField.prototype;
            this.parent.initialize.apply(this, arguments);

            var templateOptions = this._getTemplateOptions(options, ['id', 'cols']);
            this.setElement($(TEMPLATES.textarea(templateOptions)));
        }
    });
});

define("jira-project-config/custom-fields/views/forms/validators", function () {
    "use strict";

    var trueFunc = function () {
        return true;
    };

    return {
        notEmpty: function (error) {
            return function () {
                if (!this.value()) {
                    this.setError(error);
                    return false;
                } else {
                    this.clearError();
                    return true;
                }
            };
        },
        always: function () {
            return trueFunc;
        }
    };
});


AJS.namespace("JIRA.Admin.CustomFields.Form", null, require("jira-project-config/custom-fields/views/forms/form"));
AJS.namespace("JIRA.Admin.CustomFields.Form.Fields.FormField", null, require("jira-project-config/custom-fields/views/forms/form-field"));
AJS.namespace("JIRA.Admin.CustomFields.Form.Fields.NamedFieldMixin", null,
    require("jira-project-config/custom-fields/views/forms/field-mixins").NamedFieldMixin);
AJS.namespace("JIRA.Admin.CustomFields.Form.Fields.ValidatorMixin", null,
    require("jira-project-config/custom-fields/views/forms/field-mixins").ValidatorMixin);
AJS.namespace("JIRA.Admin.CustomFields.Form.Validators", null, require("jira-project-config/custom-fields/views/forms/validators"));
