AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:custom-fields", "com.atlassian.jira.jira-project-config-plugin:custom-fields-impl"], function(){
    "use strict";

    var _ = require("underscore");
    var EmptyTypeItemView = require("jira-project-config/custom-fields/views/empty-type-item");
    var CustomFieldTemplates = require("jira-project-config/custom-fields/templates");

    module("JIRA.Admin.CustomFields.EmptyTypeItemView", {
        setup: function () {
            this.EmptyTypeItemView = EmptyTypeItemView;
            this.sandbox = sinon.sandbox.create();
        },
        teardown: function () {
            this.sandbox.restore();
        }
    });

    var PageObject = function ($el) {
        this.$el = $el;
    };

    _.extend(PageObject.prototype, {
        /**
         * Return the data represented in the DOM.
         *
         * @returns {string}
         */
        message: function () {
            return this.$el.text();
        }
    });

    test("By default, the empty view should say there's no results.", function () {
        var view = new this.EmptyTypeItemView();
        var emptyMessage = "empty!";
        var pageObject;

        this.sandbox.stub(CustomFieldTemplates, "emptyType").returns(emptyMessage);
        pageObject = new PageObject(view.render().$el);

        equal(pageObject.message(), emptyMessage, "Expected the 'no results' message.");
    });

    test("When other categories have results, a different template should be used.", function () {
        var view = new this.EmptyTypeItemView();
        var emptyMessage = "other tab!";
        var pageObject;

        this.sandbox.stub(CustomFieldTemplates, "emptyTypeOtherTab").returns(emptyMessage);
        view.setEmptyState(this.EmptyTypeItemView.CHANGE_TAB);
        pageObject = new PageObject(view.render().$el);

        equal(pageObject.message(), emptyMessage, "Expected the 'other tab' message.");
    });

    test("When no results exist in any category.", function () {
        var view = new this.EmptyTypeItemView();
        var emptyMessage = "marketplace!";
        var pageObject;

        this.sandbox.stub(CustomFieldTemplates, "emptyTypeMarketplace").returns(emptyMessage);
        view.setEmptyState(this.EmptyTypeItemView.MARKETPLACE);
        pageObject = new PageObject(view.render().$el);

        equal(pageObject.message(), emptyMessage, "Expected the 'marketplace' message.");
    });
});
