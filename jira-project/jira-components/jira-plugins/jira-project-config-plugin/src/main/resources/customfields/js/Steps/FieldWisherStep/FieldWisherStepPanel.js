define("jira-project-config/custom-fields/steps/field-wisher/views/panel", [
    'jira-project-config/custom-fields/templates',
    'jira/util/formatter',
    "jira/ajs/select/single-select",
    "jira/ajs/select/suggestions/suggest-helper",
    "jira/ajs/select/suggestions/select-suggest-handler",
    "jira/ajs/list/group-descriptor",
    "jira/ajs/list/item-descriptor",
    "jira-project-config/marionette",
    "jira-project-config/backbone",
    "underscore"
], function(
    CustomFieldTemplates,
    formatter,
    SingleSelect,
    SuggestHelper,
    SelectSuggestHandler,
    GroupDescriptor,
    ItemDescriptor,
    Marionette,
    Backbone,
    _) {
    "use strict";

    var TEMPLATES = CustomFieldTemplates.FieldWisherStep;

    /**
     * Please submit the form!
     * @event Panel#request:submit
     */

    /**
     * The selected field changed.
     * @event Panel#change:field
     * @type {(Field|Wish)}
     */

    /**
     * @fires Panel#change:field
     * @fires Panel#request:submit
     */
    return Marionette.ItemView.extend({
        ui: {
            select: "select"
        },

        initialize: function (options) {
            this.template = _.bind(this.template, this);

            // There are two scenarios here:
            // 1. An existing field is selected (in which case both 'fieldId' and 'fieldName' will be non-null).
            // 2. No field will be matched ('fieldId' will be null, 'fieldName' will be the user entered text)
            this.model = new Backbone.Model({
                fieldId: null,
                fieldName: null,
                fields: options.fields
            });
        },

        template: function () {
            return TEMPLATES.form({
                fields: this.model.get('fields')
            });
        },

        onRender: function () {
            this.fieldSelect = new SingleSelect({
                element: this.ui.select,
                itemAttrDisplayed: "label",
                suggestionsHandler: this.ExistingOrCreateSuggester,

                // Hide the error message about not filling in the field. We will handle validation.
                errorMessage: ''
            });

            // SingleSelect adds a bunch of dom nodes, rebind to update `ui`
            this.bindUIElements();
        },

        // -- UI -> Model ----------------------------------------------------------------------------------------------

        events: {
            "selected select": "onFrotherSelected",
            "unselect select": "onFrotherUnselect",
            "submit form": "onSubmit"
        },

        onFrotherSelected: function (event, item) {
            var name;

            name = "fieldText" in item.properties ? item.properties.fieldText : item.properties.label;

            this.model.set({
                fieldId: item.properties.value,
                fieldName: name
            });
        },

        onFrotherUnselect: function () {
            this.model.set({
                fieldId: null,
                fieldName: null
            });
        },

        onSubmit: function (event) {
            event.preventDefault();  // the panel doesn't have authority to *actually* submit
            this.trigger("request:submit");
        },

        enable: function () {
            this.fieldSelect && this.fieldSelect.enable();
        },

        disable: function () {
            this.fieldSelect && this.fieldSelect.disable();
        },

        isDirty: function () {
            return !!(this.fieldSelect && (this.fieldSelect.getQueryVal() || this.fieldSelect.getSelectedDescriptor()));
        },

        // -- Model -> events ------------------------------------------------------------------------------------------

        modelEvents: {
            "change:fieldId": "onChangeField",
            "change:fieldName": "onChangeField"
        },

        onChangeField: function (model) {
            var field = {
                id: model.get('fieldId'),
                name: model.get('fieldName')
            };

            if (!field.id) {
                delete field.id;
            }

            this.trigger("change:field", field);
        },

        // -- Helpers --------------------------------------------------------------------------------------------------

        ExistingOrCreateSuggester: SelectSuggestHandler.extend({
            formatSuggestions: function (descriptors, query) {
                var suggestions;

                suggestions = this.sorted(descriptors);

                if (query.length > 0) {
                    suggestions.push(this.creationSuggestion(query));
                }

                return suggestions;
            },

            /**
             * Returned flatten group of items that's case-insensitive locale sorted on labels.
             * @param descriptors
             * @returns {GroupDescriptor[]}
             */
            sorted: function (descriptors) {
                var items;

                items = SuggestHelper.extractItems(descriptors).sort(function (a, b) {
                    a = a.label().toLowerCase();
                    b = b.label().toLowerCase();
                    return a.localeCompare(b);
                });

                return [new GroupDescriptor({items: items})];
            },

            /**
             * Make a descriptor for the '... (Create new field)' suggestion.
             *
             * @private
             * @param {string} label
             * @returns {GroupDescriptor}
             */
            creationSuggestion: function (label) {
                var item;

                item = new ItemDescriptor({
                    value: "",
                    label: formatter.I18n.getText("issue.operations.fields.wisher.fields.name.createoption", label),
                    fieldText: label,
                    title: label,
                    allowDuplicate: false,
                    noExactMatch: true  // this item doesn't count as an exact query match for selection purposes
                });

                return new GroupDescriptor({
                    label: "user inputted option",
                    showLabel: false,
                    replace: true,
                    items: [item]
                });
            }
        })
    });
});
