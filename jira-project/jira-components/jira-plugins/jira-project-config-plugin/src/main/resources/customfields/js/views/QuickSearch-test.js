AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:custom-fields", "com.atlassian.jira.jira-project-config-plugin:custom-fields-impl"], function(){
    "use strict";

    var _ = require("underscore");
    var QuickSearch = require("jira-project-config/custom-fields/views/quick-search");
    var jQuery = require("jquery");

    module("JIRA.Admin.CustomFields.QuickSearch", {
        setup: function () {
            this.QuickSearch = QuickSearch;
            this.sandbox = sinon.sandbox.create();
        },
        teardown: function () {
            this.sandbox.restore();
        }
    });

    var QuickSearchPageObject = function ($container) {
        this.$container = $container;
        this.$input = $container.find("input");
    };

    _.extend(QuickSearchPageObject.prototype, {
        /**
         * Simulate pressing a single key on the keyboard.
         *
         * @param {string} name A $.ui.keyCode item
         * @returns {*}
         */
        press: function (name) {
            var code = jQuery.ui.keyCode[name];
            this.$input.trigger({
                type: "keydown",
                which: code,
                keyCode: code
            });
            return this;
        },

        type: function (text) {
            this.$input.val(text);
            this.$input.trigger(jQuery.Event("input"));
        },

        value: function () {
            return this.$input.val();
        }
    });

    test("Typing into the input fires change events", function () {
        var quickSearch = new this.QuickSearch();
        var $el = quickSearch.render().$el;
        var pageObject = new QuickSearchPageObject($el);
        var log = [];

        quickSearch.on("change", _.bind(log.push, log));
        pageObject.type("foo");

        equal(log[0].value, "foo");
    });

    test("Debounce delay on input events honoured.", function () {
        var quickSearch = new this.QuickSearch({delay: 10});
        var $el = quickSearch.render().$el;
        var pageObject = new QuickSearchPageObject($el);
        var log = [];

        quickSearch.on("change", _.bind(log.push, log));

        pageObject.type("foo");
        pageObject.type("fee");
        pageObject.type("fum");
        ok(!log.length, "No event should have been fired until after the delay.");
        stop();
        setTimeout(function () {
            start();
            equal(log.length, 1, "All typing held in one event.");
            equal(log[0].value, "fum", "All typing recorded.");
        }, 100);
    });
});
