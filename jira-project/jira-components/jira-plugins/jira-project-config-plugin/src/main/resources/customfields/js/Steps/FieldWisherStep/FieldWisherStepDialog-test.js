AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:custom-fields",
    "com.atlassian.jira.jira-project-config-plugin:custom-fields-impl"], function () {

    var $ = require('jquery');
    var FieldWisherStep = require("jira-project-config/custom-fields/steps/field-wisher");
    var Backbone = require("jira-project-config/backbone");

    module("JIRA.Admin.CustomFields.FishWisherStep.Dialog", {
        setup: function () {
            this.sandbox = sinon.sandbox.create();
            this.Dialog = FieldWisherStep.Dialog;
            this.fields = [
                {id: "id1", name: "name1", onAllScreens: false},
                {id: "id2", name: "name2", onAllScreens: true}
            ];
            this.dialog = new this.Dialog({fields: this.fields});
        },

        teardown: function () {
            this.sandbox.restore();
        }
    });

    // -----------------------------------------------------------------------------------------------------------------
    var PageObject = Backbone.Marionette.View.extend({
        ui: {
            label: "label",
            select: "select",
            frother: "input.aui-ss-field",
            form: "form",
            nextButton: "button.aui-button",
            cancelButton: "a.button-panel-link"
        },

        initialize: function () {
            this.bindUIElements();
        },

        // -- API

        cancel: function () {
            this.ui.cancelButton.click();
        },

        query: function () {
            return this.ui.frother.val();
        },

        submit: function () {
            this.ui.form.trigger("submit");
        },

        unselect: function () {
            this.ui.select.trigger("unselect");
        },

        isVisible: function () {
            return this.$el.is(":visible");
        },

        /**
         * @param options.id
         * @param options.name
         */
        select: function (options) {
            this.ui.select.trigger("selected", {
                properties: {
                    value: options.id,
                    label: options.name
                }
            });
        },

        escape: function () {
            var code = $.ui.keyCode.ESCAPE;
            $.event.trigger({
                type: "keydown",
                which: code,
                keyCode: code
            });
        }
    });

    // ---------------------------------------------------------------------------------------------------------------------

    test("initial state", function () {
        var po;

        po = new PageObject({el: this.dialog.$el});

        equal(po.query(), "");
        ok(po.ui.nextButton.is(":disabled"));
        ok(po.ui.nextButton.text().length > 0);
    });

    test("select option enables next button", function () {
        var po;

        po = new PageObject({el: this.dialog.$el});

        // non existant field
        po.select({name: "non-existant"});
        ok(!po.ui.nextButton.is(":disabled"));

        // existing field
        po.select({id: "id1", name: "name1"});
        ok(!po.ui.nextButton.is(":disabled"));
    });

    test("clearing selection disables next button", function () {
        var po;

        po = new PageObject({el: this.dialog.$el});

        po.select({name: "non-existant"});
        po.unselect();
        ok(po.ui.nextButton.is(":disabled"));
    });

    test("next button text is 'Create field' when field does not exist", function () {
        var createLabel;
        var po;

        po = new PageObject({el: this.dialog.$el});
        createLabel = "issue.operations.fields.create.button";

        // non existant field
        po.select({name: "non-existant"});
        equal(po.ui.nextButton.text(), createLabel, "should say 'Create field'");
    });

    test("next button text is 'Add field' when field exists and is not on the screen", function () {
        var addLabel;
        var po;

        po = new PageObject({el: this.dialog.$el});
        addLabel = "issue.operations.fields.add.button";

        // existing field
        po.select({id: "id1", name: "name1"});
        equal(po.ui.nextButton.text(), addLabel, "should say 'Add field'");
    });

    test("next button text is 'Edit field' when field exists and is on the screen", function () {
        var editLabel;
        var po;

        po = new PageObject({el: this.dialog.$el});
        editLabel = "issue.operations.fields.edit.button";

        // existing field
        po.select({id: "id2", name: "name2"});
        equal(po.ui.nextButton.text(), editLabel, "should say 'Edit field'");
    });

    test("submit fires event", function () {
        var onSubmit;
        var po;

        onSubmit = sinon.stub();
        this.dialog.on("submit", onSubmit);

        po = new PageObject({el: this.dialog.$el});
        po.select({name: "non-existant"});
        po.submit();
        ok(onSubmit.calledWith({name: "non-existant", onAllScreens: false}));
    });

    test("cancel fires event", function () {
        var onCancel;
        var po;

        onCancel = sinon.stub();
        this.dialog.on("cancel", onCancel);

        po = new PageObject({el: this.dialog.$el});
        po.cancel();

        ok(onCancel.firstCall.calledWith(false), "Cancel fires an event.");
    });

    test("dialog cancel fires event", function () {
        var onCancel;
        var po;

        onCancel = sinon.stub();
        this.dialog.on("cancel", onCancel);

        this.dialog.show();

        po = new PageObject({el: this.dialog.$el});
        po.escape();

        ok(onCancel.firstCall.calledWith(true), "Cancel fires an event.");

        this.dialog.hide();
    });

    test("show()", function () {
        var po;

        po = new PageObject({el: this.dialog.$el});

        ok(!po.isVisible());
        this.dialog.show();
        ok(po.isVisible());

        this.dialog.hide();
    });

    test("hide()", function () {
        var po;

        po = new PageObject({el: this.dialog.$el});

        this.dialog.show();
        this.dialog.hide();
        ok(!po.isVisible());
    });

    test("isDirty()", function () {
        var po;

        po = new PageObject({el: this.dialog.$el});

        po.select({name: "non-existant"});

        ok(this.dialog.isDirty(), "Dialog dirty on selected dialog.");
        po.unselect();
        ok(!this.dialog.isDirty(), "Dialog not dirty on unselected value.");
    });

    test("Model is correctly set when panel is changed", function () {
        this.dialog.panel.trigger("change:field", {id: "id1", name: "name1"});
        equal(this.dialog.model.get("fieldId"), "id1");
        equal(this.dialog.model.get("fieldName"), "name1");
        equal(this.dialog.model.get("fieldOnAllScreens"), false);

        this.dialog.panel.trigger("change:field", {id: "id2", name: "name2"});
        equal(this.dialog.model.get("fieldId"), "id2");
        equal(this.dialog.model.get("fieldName"), "name2");
        equal(this.dialog.model.get("fieldOnAllScreens"), true);
    });
});
