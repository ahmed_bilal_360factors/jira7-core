define("jira-project-config/custom-fields/steps/helper", [
    'jira/util/formatter',
    "jira/jquery/deferred",
    "jira-project-config/custom-fields/service",
    "jira-project-config/custom-fields/wizard/fail-reasons",
    "jira-project-config/custom-fields/wizard/events",
    //to be replaced by issues-nav AMD module
    "jira-project-config/issues/api",
    "underscore"
], function(formatter, Deferred, CustomFieldsApi, FailReasons, Events, IssuesApi, _) {
    var genericFailHandler = function genericFailHandler(deferred) {
        var args = _.toArray(arguments);
        if (args[1] === CustomFieldsApi.STATUS.abort) {
            deferred.rejectWith(deferred, [FailReasons.abort].concat(args.slice(2)));
        } else {
            deferred.rejectWith(deferred, [FailReasons.error].concat(args.slice(2)));
        }
    };

    var genericProgressHandler = function genericProgressHandler(deferred, status) {
        if (status === CustomFieldsApi.PROGRESS.SHOW) {
            deferred.notify(Events.websudoBeforeShow);
        } else if (status === CustomFieldsApi.PROGRESS.SUCCESS) {
            deferred.notify(Events.websudoSuccess, arguments[2]);
        }
    };

    return {
        genericFailHandler: genericFailHandler,

        genericProgressHandler: genericProgressHandler,

        createField: {
            execute: function (options) {
                var deferred = Deferred();
                var data = options.data;
                var field = data.field;
                var type = data.type;

                options.api.addField({
                    name: field.name,
                    description: field.description,
                    searcherKey: type.searcherKey,
                    type: type.key
                }).pipe(function (fieldResult) {
                    field.id = fieldResult.id;
                    deferred.resolve();
                }, function (errors) {
                    // Creating field is a recoverable error because it is the first
                    deferred.reject(FailReasons.recoverableError, errors);
                }, deferred.notify);

                return deferred.promise();
            },

            description: function () {
                return formatter.I18n.getText("admin.project.customfieldadd.description.addfield");
            }
        },

        addOptions: {
            execute: function (options) {
                var data = options.data;
                var deferred = Deferred();
                var field = data.field;
                options.api.setOptions(field.id, field.options).pipe(deferred.resolve, _.bind(genericFailHandler, null, deferred), deferred.notify);
                return deferred.promise();
            },

            description: function () {
                return formatter.I18n.getText("admin.project.customfieldadd.description.addoptions");
            }
        },

        putFieldOnIssue: {
            execute: function (options) {
                var deferred = Deferred();
                options.api.addFieldToScreen(options.data.field.id).pipe(deferred.resolve, _.bind(genericFailHandler, null, deferred), deferred.notify);
                return deferred.promise();
            },

            description: function () {
                return formatter.I18n.getText("admin.project.customfieldadd.description.addfieldtoissue");
            }
        },

        refreshIssue: {
            execute: function () {
                var deferred = Deferred();
                IssuesApi.refreshSelectedIssue().pipe(deferred.resolve, _.bind(genericFailHandler, null, deferred), deferred.notify);
                return deferred.promise();
            },

            description: function () {
                return formatter.I18n.getText("admin.project.customfieldadd.description.refresh");
            }
        },

        executeCompletes: function (completes, executionOptions) {
            var deferred = Deferred();
            var promise = Deferred().resolve().promise();
            var rejectionArgs;
            _.each(completes, function (complete, index) {
                promise = promise.pipe(function () {
                    return complete.execute(executionOptions);
                });
                promise.fail(function (statusText) {
                    rejectionArgs = _.toArray(arguments);
                    if (statusText !== FailReasons.recoverableError) {
                        var notCompletedDescriptions = _.map(completes.slice(index), function (item) {
                            return item.description();
                        });
                        rejectionArgs.push(notCompletedDescriptions);
                    }
                    deferred.reject.apply(deferred, rejectionArgs);
                });
            });
            promise.progress(_.bind(this.genericProgressHandler, this, deferred));
            promise.done(deferred.resolve);
            return deferred.promise();
        }
    };
});

AJS.namespace("JIRA.Admin.CustomFields.CustomFieldsStepHelper", null, require("jira-project-config/custom-fields/steps/helper"));
