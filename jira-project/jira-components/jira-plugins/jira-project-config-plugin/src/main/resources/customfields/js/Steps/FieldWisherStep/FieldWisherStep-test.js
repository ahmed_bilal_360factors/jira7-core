AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:custom-fields",
    "com.atlassian.jira.jira-project-config-plugin:custom-fields-impl"], function () {

    var $ = require('jquery');
    var Backbone = require("jira-project-config/backbone");
    var Deferred = require("jira/jquery/deferred");

    module("JIRA.Admin.CustomFields.FieldWisherStep", {
        setup: function () {
            this.sandbox = sinon.sandbox.create();
            this.context = AJS.test.mockableModuleContext();
            this.fields = [{id: "id", name: "name"}];
            this.api = this.mockApi({
                succeeds: true,
                args: [this.fields]
            });
            this.errorApi = this.mockApi({
                succeeds: false,
                args: ["error", "message"]
            });
            this.abortApi = this.mockApi({
                succeeds: false,
                args: ["abort", "message"]
            });

            // mock the happy dialog
            this.dialog = this.mockDialog();
            this.FieldWisherStepDialog = this.sandbox.stub().returns(this.dialog);
            $.extend(this.FieldWisherStepDialog.prototype, {
                text: {
                    heading: "heading"
                }
            });
            this.context.mock('jira-project-config/custom-fields/steps/field-wisher/views/dialog', this.FieldWisherStepDialog);

            // mock the sad dialog
            this.errorDialog = this.mockDialog();

            this.ErrorDialog = this.sandbox.stub().returns(this.errorDialog);

            this.context.mock('jira-project-config/custom-fields/error-dialog', this.ErrorDialog);

            this.FieldWisherStep = this.context.require('jira-project-config/custom-fields/steps/field-wisher');
        },

        teardown: function () {
            this.sandbox.restore();
        },

        /**
         * Build a mock API.
         *
         * @param {boolean} options.succeeds If true, will follow the happy path.
         * @param {*[]} options.args A list of arguments pass along when resolve/reject
         */
        mockApi: function (options) {
            return {
                addableFields: this.sandbox.spy(function () {
                    var deferred;
                    var reply;

                    deferred = Deferred();
                    reply = options.succeeds ? deferred.resolve : deferred.reject;

                    return reply.apply(deferred, options.args).promise();
                })
            };
        },

        /**
         * Build a mock JIRA.Admin.CustomFields.FieldWisherStep.Dialog.
         */
        mockDialog: function () {
            var dialog;
            var sinon = this.sandbox;
            dialog = $.extend({}, Backbone.Events, {
                showLoading: sinon.stub(),
                hideLoading: sinon.stub(),
                show: sinon.stub(),
                hide: sinon.stub(),
                destroy: sinon.stub(),
                isDirty: sinon.stub(),
                resetCanSubmit: sinon.stub(),
                resetIsSubmitted: sinon.stub()
            });

            return dialog;
        }
    });

    // ---------------------------------------------------------------------------------------------------------------------

    test("showLoading() delegates to the dialog", function () {
        var step;

        step = new this.FieldWisherStep({
            api: this.api
        });
        step.start();
        ok(!this.dialog.showLoading.called, "showLoading() should not yet have been called.");
        step.showLoading();
        ok(this.dialog.showLoading.calledOnce, "showLoading() should not yet have been called.");
    });

    test("hideLoading() delegates to the dialog", function () {
        var step;

        step = new this.FieldWisherStep({
            api: this.api
        });
        step.start();
        ok(!this.dialog.hideLoading.called, "hideLoading() should not yet have been called.");
        step.hideLoading();
        ok(this.dialog.hideLoading.calledOnce, "hideLoading() should not yet have been called.");
    });


    test("start() creates a dialog", function () {
        var step;

        step = new this.FieldWisherStep({
            api: this.api
        });

        step.start();

        ok(this.FieldWisherStepDialog({fields: this.fields}));
    });

    test("start() creates an error dialog", function () {
        var step;

        step = new this.FieldWisherStep({
            api: this.errorApi
        });

        step.start();

        deepEqual(this.ErrorDialog.firstCall.args[0], {
            heading: "heading",
            message: "message"
        });
    });

    test("AJAX abort causes cancel", function () {
        var step;

        step = new this.FieldWisherStep({
            api: this.abortApi
        });

        var cancelSpy = this.sandbox.spy(step, "fireCancel");

        step.start();

        ok(!this.errorDialog.show.called, "show() should not have been called on the error dialog");
        ok(!this.dialog.show.called, "show() should not have been called on the normal dialog");
        ok(cancelSpy.calledOnce, "cancel() should have been called once");
    });

    test("dialog cancel bubbles", function () {
        var step;

        step = new this.FieldWisherStep({
            api: this.api
        });

        var cancelSpy = this.sandbox.spy(step, "fireCancel");

        step.start();
        this.dialog.trigger("cancel", true);

        ok(cancelSpy.calledOnce, "cancel() should be called once");
        ok(cancelSpy.calledWith(true), "cancel() should be called with true");
    });

    test("error dialog cancel bubbles", function () {
        var step;

        step = new this.FieldWisherStep({
            api: this.errorApi
        });

        var cancelSpy = this.sandbox.spy(step, "fireCancel");

        step.start();
        this.errorDialog.trigger("cancel");

        ok(cancelSpy.calledOnce, "onCancelRequested should have been called once");
        equal(cancelSpy.getCall(0).args.length, 0, "There should be no arguments");
    });

    test("show() shows dialog", function () {
        var step;

        step = new this.FieldWisherStep({
            api: this.api
        });

        // Confirm there are no errors when the step has not yet been started
        step.show();

        step.start();
        ok(!this.dialog.show.called, "show() should not yet have been called on the dialog");
        step.show();
        ok(this.dialog.show.calledOnce, "show() should have been called once on the dialog");
    });

    test("pauseStep() hides the dialog and resets it", function () {
        var step;

        step = new this.FieldWisherStep({
            api: this.api
        });

        // Confirm there are no errors when the step has not yet been started
        step.pauseStep();

        step.start();
        ok(!step.dialog.hide.called, "pauseStep() should not yet have called hide on the dialog");
        ok(!step.dialog.resetCanSubmit.called, "pauseStep() should not yet have called resetCanSubmit on the dialog");
        ok(!step.dialog.resetIsSubmitted.called, "pauseStep() should not yet have called resetIsSubmitted on the dialog");
        step.pauseStep();
        ok(step.dialog.hide.calledOnce, "pauseStep() should have called hide on the dialog");
        ok(step.dialog.resetCanSubmit.calledOnce, "pauseStep() should have called resetCanSubmit on the dialog");
        ok(step.dialog.resetIsSubmitted.calledOnce, "pauseStep() should have called resetIsSubmitted on the dialog");
    });

    test("stopStep() destroys the dialog appropriately", function () {
        var step;

        step = new this.FieldWisherStep({
            api: this.api
        });

        // Confirm there are no errors when the step has not yet been started
        step.stopStep();

        step.start();
        ok(!this.dialog.destroy.called, "destroy() should not yet have been called on the dialog");
        step.stopStep();
        ok(this.dialog.destroy.calledOnce, "destroy() should have been called once on the dialog");
    });

    test("doComplete() passes putFieldOnIssue and refreshIssue completes to executeCompletes when addField is true", function () {
        var step;

        step = new this.FieldWisherStep({
            api: this.api
        });
        step.stepData.addField = true;
        step.stepData.field = {};

        var executeCompletesDeferred = Deferred();
        var executeCompletesStub = this.sandbox.stub(step, "executeCompletes").returns(executeCompletesDeferred.promise());

        var spy = this.sandbox.spy();
        step.doComplete().done(spy);

        var expectedCompletes = [step.putFieldOnIssue, step.refreshIssue];

        ok(executeCompletesStub.calledOnce, "executeCompletes() should have been called once");
        ok(executeCompletesStub.firstCall.calledWith(expectedCompletes), "executeCompletes() should have been passed the expected completes");
        ok(!spy.called, "spy should not yet have been called");

        executeCompletesDeferred.resolve();

        ok(spy.calledOnce, "spy should have been called once");
    });

    test("doComplete() passes empty completes to executeCompletes when addField is false", function () {
        var step;

        step = new this.FieldWisherStep({
            api: this.api
        });
        step.stepData.addField = false;

        var executeCompletesStub = this.sandbox.stub(step, "executeCompletes");

        step.doComplete();

        ok(executeCompletesStub.calledOnce, "executeCompletes() should have been called once");
        ok(executeCompletesStub.firstCall.calledWith([]), "executeCompletes() should have been passed the expected completes");

    });

    test("doComplete() returns rejected promise when executeCompletes returns rejected promise", function () {
        var spy = this.sandbox.spy();

        var step;

        step = new this.FieldWisherStep({
            api: this.api
        });
        step.stepData.addField = true;
        step.stepData.field = {};

        var executeCompletesDeferred = Deferred();
        this.sandbox.stub(step, "executeCompletes").returns(executeCompletesDeferred.reject().promise());

        step.doComplete().fail(spy);
        ok(spy.calledOnce, "spy should have been called once");
    });

    test("isDirty()", function () {
        var step = new this.FieldWisherStep({
            api: this.api
        });
        ok(!step.isDirty(), "Not dirty on initial load.");

        step.start();
        this.dialog.isDirty.returns(true);
        ok(step.isDirty(), "Dirty if the dialog says so.");

        step.stepData = {field: {name: "foo", id: "10101"}};
        ok(step.isDirty(), "Dirty when currentData exists and dialog is dirty.");

        this.dialog.isDirty.returns(false);
        ok(step.isDirty(), "Dirty when stepData has field, field.name and field.id set.");
    });
});
