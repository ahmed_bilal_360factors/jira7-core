/**
 *  This is module exists only to provide AMD module for JIRA.Issues.Api.
 *  It should be removed as soon as there will be one provided by issue-nav-plugin.
 *
 * @note
 * JSEV-834 - Require this API late, because the backing impl may not yet be defined.
 * This is obviously bad pseudo-AMD. A proper, guaranteed-to-exist, promise-based API
 * should exist for issue data.
 */
define("jira-project-config/issues/api", function () {
    return window.JIRA &&
        window.JIRA.Issues &&
        window.JIRA.Issues.Api;
});
