define("jira-project-config/roles/entities/role-collection", [
    "jira-project-config/backbone",
    "jira-project-config/roles/entities/role-model"
], function(Backbone, RoleModel) {

    return Backbone.Collection.extend({
        model: RoleModel
    });

});
