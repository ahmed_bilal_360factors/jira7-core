AJS.test.require("com.atlassian.jira.jira-project-config-plugin:project-config-roles", function () {

    "use strict";

    require([
        "jira-project-config/backbone",
        "underscore",
        "jquery",
        "jira-project-config/roles/roles-results-view"
    ], function (Backbone,
                 _,
                 jQuery,
                 RolesResultsView
    ) {
        var allRolesViewJson = {
            projectKey: "HR",
            pageNumber: 1,
            viewingRoleId: null,
            query: "",
            showApplicationRole: true,
            roles: [
                {
                    id: 10001,
                    name: "Administrators",
                    total: 6,
                    groups: [
                        {name: "jira-administrators"}
                    ],
                    users: [
                        {
                            active: true,
                            name: "sharwood",
                            key: "sharwood-key",
                            displayName: "Scott Harwood",
                            email: "sh@gmail.com",
                            applicationRoleNames: ["Software", "Core", "ServiceDesk"]
                        },
                        {
                            active: true,
                            name: "bbaker",
                            key: "bbaker-key",
                            displayName: "Brad Baker",
                            email: "bb@gmail.com",
                            applicationRoleNames: ["Software", "Core"]
                        },
                        {
                            active: true,
                            name: "fake1",
                            key: "fake1-key",
                            displayName: "Fake User 1",
                            email: "fake1@example.com",
                            applicationRoleNames: []
                        },
                        {
                            active: true,
                            name: "fake2",
                            key: "fake2-key",
                            displayName: "Fake User 2",
                            email: "fake2@example.com",
                            applicationRoleNames: ["Core"]
                        }
                    ]
                },
                {
                    id: 10002,
                    name: "Customers",
                    total: 3,
                    users: [
                        {
                            active: true,
                            name: "jwilson",
                            key: "jwilson-key",
                            displayName: "Julian Wilson",
                            email: "sh@gmail.com",
                            applicationRoleNames: ["Core", "ServiceDesk"]
                        },
                        {
                            active: true,
                            name: "kslater",
                            key: "kslater-key",
                            displayName: "Kelly Slater",
                            email: "kelly@gmail.com",
                            applicationRoleNames: ["Software", "ServiceDesk"]
                        },
                        {
                            active: true,
                            name: "airons",
                            key: "airons-key",
                            displayName: "Andy Irons",
                            email: "and@gmail.com",
                            applicationRoleNames: ["ServiceDesk"]
                        }
                    ],
                    groups: []
                }
            ]
        };
        var specificRoleViewJson = {
            projectKey: "HR",
            pageNumber: 1,
            viewingRoleId: 10002,
            query: "",
            roles: [
                {
                    id: 10002,
                    name: "Customers",
                    total: 192,
                    users: [{
                        active: true,
                        name: "sharwood",
                        key: "sharwood-key",
                        displayName: "Scott Harwood",
                        email: "sh@gmail.com",
                        applicationRoleNames: ["ServiceDesk"]
                    }],
                    groups: []
                }
            ]
        };
        module("jira-project-config/roles/roles-results-view-test", {
            setup: function () {
                this.sandbox = sinon.sandbox.create();
                this.sandbox.useFakeServer();
                this.SmartAjax = {
                    makeRequest: this.sandbox.stub().returns(new jQuery.Deferred().resolve())
                };
                this.sandbox.stub(AJS, "format", function () {
                    var params = Array.prototype.slice.call(arguments, 0);
                    return params.join(",");
                });

                this.context = AJS.test.mockableModuleContext();
                this.context.mock("wrm/context-path", this.sandbox.stub().returns(""));
                this.context.mock("jira/ajs/ajax/smart-ajax", this.SmartAjax);
                this.RolesResultsModel = this.context.require("jira-project-config/roles/entities/roles-results-model");

                this.allRolesView = new RolesResultsView({
                    model: new this.RolesResultsModel(allRolesViewJson)
                });
                this.specificRoleView = new RolesResultsView({
                    model: new this.RolesResultsModel(specificRoleViewJson)
                });
                this.sandbox.spy(this.allRolesView, "trigger");
                this.sandbox.spy(this.specificRoleView, "trigger");
                this.sandbox.spy(this.specificRoleView.model, "set");

                this.allRolesView.render();
                this.specificRoleView.render();
            },
            teardown: function () {
                this.sandbox.restore();
            }
        });

        test("Renders roles correctly", function () {
            var adminUserNames = _.pluck(this.allRolesView.$("table:eq(0) .roles-user-username-val"), "textContent");
            var adminGroupNames = _.pluck(this.allRolesView.$("table:eq(0) .roles-group-name"), "textContent");
            var adminUserLicenses = _.pluck(this.allRolesView.$("table:eq(0) .roles-user-license-val"), "textContent");

            var customerUserNames = _.pluck(this.allRolesView.$("table:eq(1) .roles-user-username-val"), "textContent");
            var customerGroupNames = _.pluck(this.allRolesView.$("table:eq(1) .roles-group-name"), "textContent");
            var customerUserLicenses = _.pluck(this.allRolesView.$("table:eq(1) .roles-user-license-val"), "textContent");

            deepEqual(adminUserNames, ["sharwood", "bbaker", "fake1", "fake2"]);
            deepEqual(adminGroupNames, ["jira-administrators"]);
            deepEqual(adminUserLicenses, [
                "Software, Core, ServiceDesk",
                "Software, Core",
                "",
                "Core"
            ]);

            deepEqual(customerUserNames, ["jwilson", "kslater", "airons"]);
            deepEqual(customerGroupNames, []);
            deepEqual(customerUserLicenses, [
                "Core, ServiceDesk",
                "Software, ServiceDesk",
                "ServiceDesk"
            ]);
        });

        test("License column should not be visible if showApplicationRole is false", function () {
            this.allRolesView.model.set("showApplicationRole", false);
            this.allRolesView.render();
            deepEqual(this.allRolesView.$("table:eq(0) .roles-user-license-val").size(), 0, "");
        });

        test("AJAX request should be called to remove group from role", function () {
            this.allRolesView.$("table:eq(0) .roles-group-actions .roles-remove").click();

            sinon.assert.calledWith(this.SmartAjax.makeRequest, {
                url: "/rest/api/2/project/HR/role/10001?group=jira-administrators",
                type: "DELETE",
                contentType: "application/json",
                dataType: "json"
            });
            sinon.assert.calledWith(this.allRolesView.trigger, "actorRemoved");
        });

        test("AJAX request should be called with user key (not username) to remove user from role", function () {
            this.allRolesView.$("table:eq(0) .roles-user-actions .roles-remove").click();

            sinon.assert.calledWith(this.SmartAjax.makeRequest, {
                url: "/rest/api/2/project/HR/role/10001?user=sharwood-key",
                type: "DELETE",
                contentType: "application/json",
                dataType: "json"
            });
            sinon.assert.calledWith(this.allRolesView.trigger, "actorRemoved");
        });

        test("Click 'Show all' on a role should trigger an event", function () {
            this.allRolesView.$(".js-roles-show-all:eq(0)").click();
            sinon.assert.calledWith(this.allRolesView.trigger, "showingActorOfARoleClicked");
        });

        test("'Show all' should not be visible for roles with number of actors <= 5 ", function () {
            strictEqual(this.allRolesView.$(".js-roles-show-all[data-role-id=10002]").size(), 0,
                "There should not be any 'Show all' links in the role Customers");
        });

        test("When viewing all roles, there should be a message saying 'showing y of x'", function () {
            var $statusMessages = this.allRolesView.$(".roles-result-header-viewing-status");
            ok($statusMessages.eq(0).text().indexOf("admin.project.roles.showing.all.status.text,5,6") === 0,
                "There should be a message saying 'showing x of y'");
            ok($statusMessages.eq(1).text().indexOf("admin.project.roles.showing.all.status.text,3,3") === 0,
                "There should be a message saying 'showing x of y'");
        });

        test("Click 'Show less' on a role should trigger an event", function () {
            this.specificRoleView.$(".js-roles-show-less:eq(0)").click();
            sinon.assert.calledWith(this.specificRoleView.trigger, "showingActorOfAllRolesClicked");
        });

        test("When viewing a specific role, there should be a message saying 'showing x - y of z'", function () {
            ok(this.specificRoleView.$(".roles-result-header-viewing-status").text()
                    .indexOf("admin.project.roles.showing.less.status.text,1,20,192") === 0,
                "There should be a message saying 'showing x - y of z'");

            this.specificRoleView.model.set("pageNumber", 10);
            this.specificRoleView.render();
            ok(this.specificRoleView.$(".roles-result-header-viewing-status").text()
                    .indexOf("admin.project.roles.showing.less.status.text,181,192,192") === 0,
                "There should be a message saying 'showing x - y of z'");
        });

        test("Pagination should render correctly", function () {
            var pages = _.pluck(this.specificRoleView.$(".js-roles-pagination-navigator li"), "textContent");
            deepEqual(pages, ["1", "2", "3", "admin.project.roles.results.pagination.next.text",
                "admin.project.roles.results.pagination.last.text"]);


            this.specificRoleView.model.set("pageNumber", 5);
            this.specificRoleView.render();
            pages = _.pluck(this.specificRoleView.$(".js-roles-pagination-navigator li"), "textContent");
            deepEqual(pages, [
                "admin.project.roles.results.pagination.first.text",
                "admin.project.roles.results.pagination.previous.text",
                "3",
                "4",
                "5",
                "6",
                "7",
                "admin.project.roles.results.pagination.next.text",
                "admin.project.roles.results.pagination.last.text"]
            );

            this.specificRoleView.model.set("pageNumber", 10);
            this.specificRoleView.render();
            pages = _.pluck(this.specificRoleView.$(".js-roles-pagination-navigator li"), "textContent");
            deepEqual(pages, [
                "admin.project.roles.results.pagination.first.text",
                "admin.project.roles.results.pagination.previous.text",
                "8",
                "9",
                "10"]
            );
        });

        test("There shouldn't be pagination navigator when total number of actors < 20", function () {
            strictEqual(this.allRolesView.$(".js-roles-pagination-navigator").size(), 0, "There shouldn't be pagination navigator when total number of actors < 20");
        });

        test("Click a page in pagination navigator should trigger a change event on pageNumber", function () {
            this.specificRoleView.$(".js-roles-pagination-navigator [data-page-number=2]").click();
            sinon.assert.calledWith(this.specificRoleView.model.set, "pageNumber", 2);
        });
    });

});
