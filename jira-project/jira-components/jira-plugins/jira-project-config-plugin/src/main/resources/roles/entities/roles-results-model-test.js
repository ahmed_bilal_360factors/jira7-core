AJS.test.require([
    "com.atlassian.jira.jira-project-config-plugin:project-config-roles"
], function () {
    "use strict";

        var $ = require("jquery");
        var _ = require("underscore");
        var formatter = require("jira/util/formatter");
        var wrmContextPath = require("wrm/context-path");
        var RolesResultsModel = require("jira-project-config/roles/entities/roles-results-model");
        var RoleCollection = require("jira-project-config/roles/entities/role-collection");

        var sampleJson = {
            projectKey: "?A&QUERY=1",
            roles: [
                {
                    id: 10001,
                    name: "Administrators",
                    total: 4,
                    groups: [
                        {name: "jira-administrators"},
                        {name: "jira-developers"}
                    ],
                    users: [
                        {
                            active: true,
                            name: "sharwood",
                            displayName: "Scott Harwood",
                            email: "sh@gmail.com",
                            applicationRoleNames: []
                        },
                        {
                            active: true,
                            name: "bbaker",
                            displayName: "Brad Baker",
                            email: "bb@gmail.com",
                            applicationRoleNames: []
                        }
                    ]
                },
                {
                    id: 10002,
                    name: "Customers",
                    total: 3,
                    users: [
                        {
                            active: true,
                            name: "jwilson",
                            displayName: "Julian Wilson",
                            email: "sh@gmail.com",
                            applicationRoleNames: []
                        },
                        {
                            active: true,
                            name: "kslater",
                            displayName: "Kelly Slater",
                            email: "kelly@gmail.com",
                            applicationRoleNames: []
                        },
                        {
                            active: true,
                            name: "airons",
                            displayName: "Andy Irons",
                            email: "and@gmail.com",
                            applicationRoleNames: []
                        }
                    ],
                    groups: []
                }
            ]
        };

        var emptyActorsJson = {
            roles: [
                {
                    id: 10001,
                    name: "Administrators",
                    total: 0,
                    groups: [],
                    users: []
                },
                {
                    id: 10002,
                    name: "Customers",
                    total: 0,
                    users: [],
                    groups: []
                }
            ]
        };

        var paginationTestJson = {
            pageNumber: 1,
            viewingRoleId: 10001,
            roles: [{
                id: 10001,
                name: "",
                total: 192,
                groups: new Array(15),
                users: new Array(5)
            }]
        };

        module("jira-project-config/roles/entities/roles-results-model-test", {
            setup: function () {
                this.sandbox = sinon.sandbox.create();
                this.sandbox.stub(formatter, "format", function () {
                    var params = Array.prototype.slice.call(arguments, 0);
                    return params.join(",");
                });
            },
            teardown: function () {
                this.sandbox.restore();
            }

        });

        test("Model should have default values", function () {
            var model = new RolesResultsModel();

            strictEqual(model.get("projectKey"), "");
            strictEqual(model.get("pageNumber"), 1);
            strictEqual(model.get("viewingRoleId"), null);
            strictEqual(model.get("query"), "");
            deepEqual(model.get("roles").toJSON(), []);
        });


        test("The old fetch request should be aborted before a new request is fired", function () {
            var model = new RolesResultsModel();

            var deferred = model.fetch();
            var failCallbackSpy = this.sandbox.spy();
            deferred.fail(failCallbackSpy);
            var fetchRequest = model._currentFetchRequest;

            ok(fetchRequest.abort(), false);
            model.fetch();
            ok(fetchRequest.abort(), true, "The old fetch request should be aborted");
            sinon.assert.callCount(failCallbackSpy, 0, "Fail callback should not be called when the request is aborted.");
        });

        test("toFilterRoleOptions", function () {
            var model = new RolesResultsModel(sampleJson);
            var filterRoleOptions = model.toFilterRoleOptions();

            deepEqual(filterRoleOptions, [
                {text: "common.words.all", value: ""},
                {text: "Administrators", value: 10001},
                {text: "Customers", value: 10002}
            ]);
        });

        test("toRoleOptions", function () {
            var model = new RolesResultsModel(sampleJson);
            var roleOptions = model.toRoleOptions();

            deepEqual(roleOptions, [
                {text: "Administrators", value: 10001},
                {text: "Customers", value: 10002}
            ]);
        });

        test("Should use a nested roles collection when use set roles", function () {
            var model = new RolesResultsModel(sampleJson);

            ok(model.get("roles") instanceof RoleCollection, "Should use a nested roles collection when use set roles");

        });

        test("isViewingDefaultView", function () {
            var model = new RolesResultsModel(sampleJson);
            strictEqual(model.isViewingDefaultView(), true);

            model.set("query", "blah");
            strictEqual(model.isViewingDefaultView(), false);

            model.set("query", "");
            model.set("viewingRoleId", 10002);
            strictEqual(model.isViewingDefaultView(), false);
        });

        test("isRoleResultEmpty", function () {
            var model = new RolesResultsModel(sampleJson);
            strictEqual(model.isRoleResultEmpty(), false);

            model = new RolesResultsModel(emptyActorsJson);
            strictEqual(model.isRoleResultEmpty(), true);
        });

        test("getVisibleActorsCount", function () {
            var model = new RolesResultsModel(paginationTestJson);
            model.set("pageNumber", 1);
            strictEqual(model.getVisibleActorsCount(), 20);
        });

        test("getTotalPageNumber", function () {
            var model = new RolesResultsModel(paginationTestJson);
            strictEqual(model.getTotalPageNumber(), 200 / 20, "Should return total page number when viewing a specific role");

            model.set("viewingRoleId", null);
            strictEqual(model.getTotalPageNumber(), undefined, "Should return undefined when viewing all roles");
        });

        test("Fetcher URL Should be encoded", function () {
            var model = new RolesResultsModel(sampleJson);
            model.set("query", "&tim=&amp;&no");

            strictEqual(model.url(),
                wrmContextPath() + "/rest/projectconfig/latest/roles/%3FA&QUERY=1?query=" + encodeURIComponent("&tim=&amp;&no") + "&pageNumber=1&pageSize=5",
                "URL should encoded");

            model.set("viewingRoleId", 10009);
            strictEqual(model.url(),
                wrmContextPath() + "/rest/projectconfig/latest/roles/%3FA&QUERY=1?query=" + encodeURIComponent("&tim=&amp;&no") + "&pageNumber=1&pageSize=20&roleId=10009",
                "URL should encoded");
        });

        test("_buildPaginationData", function () {
            var model = new RolesResultsModel(paginationTestJson);

            model.set("pageNumber", 1);
            deepEqual(model._buildPaginationData(), {
                firstPage: null,
                previousPage: null,
                pages: [
                    {
                        number: 1,
                        selected: true
                    },
                    {
                        number: 2,
                        selected: false
                    },
                    {
                        number: 3,
                        selected: false
                    }
                ],
                nextPage: 2,
                lastPage: 10,
                showingFrom: 1,
                showingTo: 20
            });

            model.set("pageNumber", 2);
            deepEqual(model._buildPaginationData(), {
                firstPage: 1,
                previousPage: 1,
                pages: [
                    {
                        number: 1,
                        selected: false
                    },
                    {
                        number: 2,
                        selected: true
                    },
                    {
                        number: 3,
                        selected: false
                    },
                    {
                        number: 4,
                        selected: false
                    }
                ],
                nextPage: 3,
                lastPage: 10,
                showingFrom: 21,
                showingTo: 40
            });

            model.set("pageNumber", 4);
            deepEqual(model._buildPaginationData(), {
                firstPage: 1,
                previousPage: 3,
                pages: [
                    {
                        number: 2,
                        selected: false
                    },
                    {
                        number: 3,
                        selected: false
                    },
                    {
                        number: 4,
                        selected: true
                    },
                    {
                        number: 5,
                        selected: false
                    },
                    {
                        number: 6,
                        selected: false
                    }
                ],
                nextPage: 5,
                lastPage: 10,
                showingFrom: 61,
                showingTo: 80
            });

            model.set("pageNumber", 9);
            deepEqual(model._buildPaginationData(), {
                firstPage: 1,
                previousPage: 8,
                pages: [
                    {
                        number: 7,
                        selected: false
                    },
                    {
                        number: 8,
                        selected: false
                    },
                    {
                        number: 9,
                        selected: true
                    },
                    {
                        number: 10,
                        selected: false
                    }
                ],
                nextPage: 10,
                lastPage: 10,
                showingFrom: 161,
                showingTo: 180
            });

            model.set("pageNumber", 10);
            deepEqual(model._buildPaginationData(), {
                firstPage: 1,
                previousPage: 9,
                pages: [
                    {
                        number: 8,
                        selected: false
                    },
                    {
                        number: 9,
                        selected: false
                    },
                    {
                        number: 10,
                        selected: true
                    }
                ],
                nextPage: null,
                lastPage: null,
                showingFrom: 181,
                showingTo: 192
            });
        });

        test("toJSON with extra infomation", function () {
            var model = new RolesResultsModel(sampleJson);
            model.set({
                "query": "&tim=&amp;&no",
                "viewingRoleId": 10002
            });
            var json = model.toJSON();

            strictEqual(json.isViewingDefaultView, false);
            strictEqual(json.isRoleResultEmpty, false);
            strictEqual(json.allRoleViewPageSize, 5);
            strictEqual(json.totalPageNumber, 1);
            ok(json.paginationData !== undefined);
        });

        test("_getPageSize", function () {
            var model = new RolesResultsModel({
                viewingRoleId: null
            });

            strictEqual(model._getPageSize(), 5, "Should have page size of 5 when viewing all roles");

            model.set("viewingRoleId", 10009);
            strictEqual(model._getPageSize(), 20, "Should have page size of 20 when viewing a specific role");
        });

        test("Fetch URL should be correct with or without contextPath", function () {
            var contextPathStub = this.stub();

            var mocks = AJS.test.mockableModuleContext();
            mocks.mock("wrm/context-path", contextPathStub);

            var RolesResultsModel = mocks.require("jira-project-config/roles/entities/roles-results-model");
            var model = new RolesResultsModel(sampleJson);
            model.set("query", "&tim=&amp;&no");

            contextPathStub.returns("/jira");
            ok(model.url().indexOf("/jira/rest/projectconfig/latest/roles/%3FA&QUERY=1?query=") === 0, "Fetch URL should be correct with context path");

            // production mode on cloud, context path is an empty string
            contextPathStub.returns("");
            ok(model.url().indexOf("/rest/projectconfig/latest/roles/%3FA&QUERY=1?query=") === 0, "Fetch URL should be correct without context path");
        });

});
