require([
    'jira/dialog/form-dialog',
    'wrm/data',
    "jira-project-config/application",
    "jira-project-config/roles/roles-controller",
    "jira-project-config/roles/entities/roles-results-model"
], function(
    FormDialog,
    wrmData,
    ProjectConfigApplication,
    RolesController,
    RolesResultsModel
) {
    "use strict";
    ProjectConfigApplication.module("Roles", function (module, application) {
        module.addInitializer(function () {

            // initialize aui inline-dialog2 component
            require("aui/inline-dialog2");

            new FormDialog({
                type: "ajax",
                id: "project-config-project-edit-lead-and-default-assignee-dialog",
                trigger: "#roles-edit-defaults-button",
                autoClose: true
            });

            var emailVisibility = wrmData.claim("com.atlassian.jira.jira-project-config-plugin:project-config-roles.email-visibility");

            var model = new RolesResultsModel({
                projectKey: application.request("projectKey"),
                isEmailVisible: emailVisibility.isEmailVisible
            });

            var controller = new RolesController({
                model: model
            });
            controller.showDefault();
        });
    });
});
