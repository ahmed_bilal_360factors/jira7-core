/* globals define */
define("jira-project-config/roles/roles-filter-view", [
    'jira/util/formatter',
    "jira/jquery/deferred",
    "jira-project-config/backbone",
    "underscore",
    "jquery",
    "jira-project-config/roles/template"
], function(formatter, Deferred, Backbone, _, $, Template) {
    "use strict";

    return Backbone.View.extend({
        template: Template.filter,
        events: {
            "input .js-roles-filter-query-input": "_onQueryInput",
            "change select.js-roles-filter-role-selector": "_onRoleSelectorChanged"
        },

        initialize: function () {
            _.bindAll(this, "_beforeSubmit", "_afterSubmit");
        },

        /**
         * We need to hide this view when we show the page which indicating all roles are empty
         */
        showOrHide: function () {
            if (this.model.isViewingDefaultView() && this.model.isRoleResultEmpty()) {
                this.$el.hide();
            } else {
                this.$el.css("display", "block");
            }
        },

        render: function () {
            this.showOrHide();

            // use role options to render in select elements
            var json = _.extend(this.model.toJSON(), {roles: this.model.toFilterRoleOptions()});

            this.$el.html(this.template(json));
            this._initRoleSelector();
            this._$queryInput = this.$(".js-roles-filter-query-input");
            this._$roleSelector = this.$("select.js-roles-filter-role-selector");
            this._$inProgressIcon = this.$(".js-roles-in-progress-icon");
            return this;
        },

        /**
         * Change the role selector's selected role
         * Not triggering the "change" event, otherwise there will be a event loop
         * @param {String|Number} roleId - Role id to be selected. Null means "All" roles
         */
        selectRole: function (roleId) {
            // this won't trigger the "change" event,
            this._$roleSelector.auiSelect2("val", roleId === null ? "" : roleId);
        },

        _initRoleSelector: function () {
            $("#roles-filter-role-selector").auiSelect2({
                minimumResultsForSearch: -1,
                formatSelection: function (object) {
                    return formatter.I18n.getText("admin.project.roles.rolesfilter.selected.role.label") + ": " + object.text;
                },
                containerCssClass: "js-role-selector-container",
                dropdownCssClass: "js-role-selector-dropdown"
            });
        },

        _onQueryInput: _.debounce(function () {
            var $deferred = Deferred();
            var query = $.trim(this._$queryInput.val());

            $deferred.always(this._afterSubmit);
            this.model.set("query", query);

            this._beforeSubmit();
            this.trigger("queryChanged", $deferred);
        }, 300),

        _onRoleSelectorChanged: function () {
            var $deferred = Deferred();
            var selectedRoleId = this._$roleSelector.auiSelect2("val");
            $deferred.always(this._afterSubmit);

            this._beforeSubmit();
            this.model.set("viewingRoleId", selectedRoleId || null); // "null" means viewing all roles
            this.trigger("viewingRoleChanged", $deferred);
        },

        _beforeSubmit: function () {
            this._$inProgressIcon.css("display", "inline-block");
        },

        _afterSubmit: function () {
            this._$inProgressIcon.hide();
        }
    });
});

