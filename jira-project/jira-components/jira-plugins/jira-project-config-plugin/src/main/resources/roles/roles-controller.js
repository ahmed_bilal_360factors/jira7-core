define("jira-project-config/roles/roles-controller", [
    "jira-project-config/marionette",
    "jira-project-config/roles/roles-results-view",
    "jira-project-config/roles/adduserdialog/add-user-dialog-view",
    "jira-project-config/roles/roles-filter-view"
], function (
    Marionette,
    RolesResultsView,
    AddUserDialogView,
    RolesFilterView
) {
    return Marionette.Controller.extend({

        initialize: function (options) {
            this.model = options.model;
            this.filterView = new RolesFilterView({
                model: this.model,
                el: ".js-role-filter"
            });
            this.resultsView = new RolesResultsView({
                model: this.model,
                el: ".js-role-results"
            });
            this.addUserDialogView = new AddUserDialogView({
                model: this.model,
                el: ".js-roles-add-user-dialog"
            });

            this.listenTo(this.addUserDialogView, "actorsAdded", this._reloadResultsView)
                .listenTo(this.filterView, "queryChanged viewingRoleChanged", this._reloadResultsView)
                .listenTo(this.resultsView, "pageNumberChanged", function (deferred) {
                    this._reloadResultsView(deferred);
                }.bind(this))
                .listenTo(this.resultsView, "actorRemoved", function (deferred) {
                    if ((this.model.getVisibleActorsCount() - 1) === 0) {
                        // If this is the last actor in the current view, current page, we go back 1 page.
                        // we also don't wanna trigger the pageNumber change event
                        this.model.set("pageNumber", Math.max(this.model.get("pageNumber") - 1, 1), {silent: true});
                    }
                    this._reloadResultsView(deferred);
                }.bind(this))
                .listenTo(this.resultsView, "showingActorOfARoleClicked showingActorOfAllRolesClicked", function () {
                    this._reloadResultsView().done(function () {
                        this.filterView.selectRole(this.model.get("viewingRoleId"));
                    }.bind(this));
                });
        },

        showDefault: function () {
            this.model.fetch().done(function () {
                this.filterView.render();
                this.resultsView.render();
                this.addUserDialogView.render();
            }.bind(this));
        },

        _reloadResultsView: function (deferred) {
            return this.model.fetch()
                .done(function () {
                    this.filterView.showOrHide();
                    this.resultsView.render();
                    deferred && deferred.resolve();
                }.bind(this))
                .fail(function () {
                    deferred && deferred.reject();
                }.bind(this));
        }
    });
});
