/* global Event */
AJS.test.require([
    "com.atlassian.jira.jira-project-config-plugin:project-config-roles"
], function () {
    "use strict";

        var $ = require("jquery");
        var _ = require("underscore");
        var formatter = require("jira/util/formatter");
        var RolesResultsModel = require("jira-project-config/roles/entities/roles-results-model");
        var AddUserDialogView = require("jira-project-config/roles/adduserdialog/add-user-dialog-view");

        var allRoles = {
            "roles": [
                {
                    id: 1001,
                    name: "Admin"
                }, {
                    id: 1002,
                    name: '<script>alert("Developers");</script>'
                }, {
                    id: 1003,
                    name: "Users"
                }
            ]
        };
        var $addUserAndGroupDialog = $("<div><div class=\"js-roles-add-user-dialog-content\"></div></div>");
        var samplePickerResponse = {
            "users": {
                "total": 1,
                "header": "Showing 1 of 1 matching users",
                "users": [{
                    "name": "admin-name",
                    "key": "admin-key",
                    "html": "admin (user)",
                    "displayName": "admin",
                    "avatarUrl": "http://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?d=mm&s=16"
                }]
            },
            "groups": {
                "header": "Showing 1 of 1 matching groups",
                "total": 1,
                "groups": [{
                    "name": "jira-administrators-name",
                    "html": "jira-administrators (group)"
                }]
            }
        };
        var sampleSuccessResponse = {"numberOfGroupsAdded": 2, "numberOfUsersAdded": 1};
        var nothingSavedSuccessResponse = {"numberOfGroupsAdded": 0, "numberOfUsersAdded": 0};

        module("jira-project-config/roles/adduserdialog/add-user-dialog-view-test", {
            initDialogView: function () {
                var sandbox = this.sandbox;
                var addUserDialogView = new AddUserDialogView({
                    model: new RolesResultsModel(allRoles)
                });
                addUserDialogView.setElement($addUserAndGroupDialog);
                addUserDialogView.render();
                addUserDialogView.el.dispatchEvent(new Event("aui-layer-show"));

                var userAndGroupPicker = addUserDialogView._userAndGroupPicker;

                var pickAnActor = function (actorToPick) {
                    userAndGroupPicker.$field.val("admin");
                    sandbox.clock.tick(1000);
                    userAndGroupPicker.$field.trigger("input");
                    sandbox.clock.tick(1000);

                    sandbox.server.requests[sandbox.server.requests.length - 1].respond(200, {"Content-Type": "application/json"},
                        JSON.stringify(samplePickerResponse)
                    );
                    var $suggestions = $(".aui-list-item");
                    $suggestions.each(function (i, suggestionElement) {
                        var $suggestion = $(suggestionElement);
                        if ($suggestion.text() === actorToPick) {
                            $suggestions.removeClass("active");
                            $suggestions.addClass("active");
                            $suggestion.click();
                            return;
                        }
                    });
                };

                var removeAllSelectedActor = function () {
                    addUserDialogView.$(".remove-recipient").click();
                };

                var submit = function () {
                    addUserDialogView.$(".js-roles-add-usergroup-action").click();
                };
                var submitWithSuccess = function () {
                    addUserDialogView.$(".js-roles-add-usergroup-action").click();
                    sandbox.server.requests[sandbox.server.requests.length - 1].respond(200,
                        {"Content-Type": "application/json"},
                        JSON.stringify(sampleSuccessResponse)
                    );
                    sandbox.clock.tick(1000);
                };
                var submitWithNothingSaved = function () {
                    addUserDialogView.$(".js-roles-add-usergroup-action").click();
                    sandbox.server.requests[sandbox.server.requests.length - 1].respond(200,
                        {"Content-Type": "application/json"},
                        JSON.stringify(nothingSavedSuccessResponse)
                    );
                    sandbox.clock.tick(1000);
                };

                var selectARole = function (roleNameToSelect) {
                    addUserDialogView.$(".js-roles-role-picker option").each(function (index, roleElement) {
                        var $role = $(roleElement);
                        if ($role.text() === roleNameToSelect) {
                            $role.attr("selected", true);
                            return;
                        }
                    });
                };
                var cleanup = function () {
                    userAndGroupPicker.$field.trigger("blur");
                    userAndGroupPicker.dropdownController.$layer.remove();
                    $("#aui-flag-container").remove();
                };
                return {
                    view: addUserDialogView,
                    pickAnActor: pickAnActor,
                    selectARole: selectARole,
                    removeAllSelectedActor: removeAllSelectedActor,
                    submitWithSuccess: submitWithSuccess,
                    submitWithNothingSaved: submitWithNothingSaved,
                    submit: submit,
                    cleanup: cleanup
                };
            },
            setup: function () {
                this.sandbox = sinon.sandbox.create();
                this.sandbox.useFakeServer();
                this.sandbox.useFakeTimers();
                $("#qunit-fixture").append($addUserAndGroupDialog);

                this.addUserAndGroupDialog = this.initDialogView();
                this.sandbox.spy(this.addUserAndGroupDialog.view, "trigger");
            },
            teardown: function () {
                this.addUserAndGroupDialog.cleanup();
                this.sandbox.restore();
            }
        });

        test("Should disable role selector & 'add' button after dialog is shown", function () {
            ok(this.addUserAndGroupDialog.view.$(".js-roles-add-usergroup-action").is(":disabled"), "Add button should be disabled initially");
            ok(this.addUserAndGroupDialog.view.$(".js-roles-role-picker").is(":disabled"), "Role selector should be disabled initially");
        });

        test("Should enable role selector & 'add' button when there is at least 1 user or group selected", function () {
            this.addUserAndGroupDialog.pickAnActor("admin (user)");

            ok(this.addUserAndGroupDialog.view.$(".js-roles-add-usergroup-action").is(":enabled"), "Add button should be enabled when at least 1 user or group is selected");
            ok(this.addUserAndGroupDialog.view.$(".js-roles-role-picker").is(":enabled"), "Role selector should be enabled when at least 1 user or group is selected");
        });

        test("Should disable role selector & 'add' button when all actors are removed", function () {
            this.addUserAndGroupDialog.pickAnActor("admin (user)");
            this.addUserAndGroupDialog.pickAnActor("jira-administrators (group)");
            this.addUserAndGroupDialog.removeAllSelectedActor();

            ok(this.addUserAndGroupDialog.view.$(".js-roles-add-usergroup-action").is(":disabled"), "Add button should be disabled when all selected actors are removed");
            ok(this.addUserAndGroupDialog.view.$(".js-roles-role-picker").is(":disabled"), "Role selector should be disabled when all selected actors are removed");
        });

        test("Role selector should contain all roles", function () {
            var rolesInDialog = [];
            this.addUserAndGroupDialog.view.$(".js-roles-role-picker option").each(function (index, option) {
                var $option = $(option);
                rolesInDialog.push({
                    id: $option.attr("value"),
                    name: $option.text()
                });
            });

            strictEqual(_.pluck(rolesInDialog, "id").toString(), _.pluck(allRoles.roles, "id").toString(), "Role selector should contain all available roles");
            strictEqual(_.pluck(rolesInDialog, "name").toString(), _.pluck(allRoles.roles, "name").toString(), "Role selector should contain all available roles");
        });

        test("'Add' button should be disabled & loading icon should be visible, while saving in progress", function () {
            this.addUserAndGroupDialog.pickAnActor("admin (user)");
            this.addUserAndGroupDialog.pickAnActor("jira-administrators (group)");
            this.addUserAndGroupDialog.selectARole("Developers");
            this.addUserAndGroupDialog.submit();

            ok(this.addUserAndGroupDialog.view.$(".js-roles-add-usergroup-action").is(":disabled"), "'Add' button should be disabled when saving is in progress");
            ok(this.addUserAndGroupDialog.view.$(".js-roles-in-progress-icon").is(":visible"), "Loading icon should be visible when saving is in progress");
        });

        test("Dialog should be submitted with the selected user, group and role", function () {
            this.addUserAndGroupDialog.pickAnActor("admin (user)");
            this.addUserAndGroupDialog.pickAnActor("jira-administrators (group)");
            this.addUserAndGroupDialog.selectARole('<script>alert("Developers");</script>');
            this.addUserAndGroupDialog.submit();

            strictEqual(this.sandbox.server.requests[this.sandbox.server.requests.length - 1].requestBody,
                JSON.stringify({"users": ["admin-key"], "groups": ["jira-administrators-name"]}),
                "Dialog should be submitted with the selected user, group and role");
        });

        test("The inline dialog should be closed & an success flag should be shown, after saving successfully", function () {
            this.addUserAndGroupDialog.pickAnActor("admin (user)");
            this.addUserAndGroupDialog.pickAnActor("jira-administrators (group)");
            this.addUserAndGroupDialog.selectARole('<script>alert("Developers");</script>');
            this.sandbox.stub(formatter, "format", function () {
                var params = Array.prototype.slice.call(arguments, 0);
                return params.join(",");
            });
            this.addUserAndGroupDialog.submitWithSuccess();

            ok(this.addUserAndGroupDialog.view.trigger.calledWith("actorsAdded"), "'actorAdded' event should be triggered after saving successfully");
            strictEqual(this.addUserAndGroupDialog.view.el.open, false, "Dialog should be closed after saving successfully");
            strictEqual($("#aui-flag-container .aui-message-success").text(),
                'admin.project.roles.adduerandgroup.success.msg,,admin.project.roles.adduerandgroup.success.msg.groups,2 common.words.and admin.project.roles.adduerandgroup.success.msg.users,1,,<script>alert("Developers");</script>', "Should show a success flag (with html-escaped role name) indicating the actual groups & users which are saved");
        });

        test("The inline dialog should be closed & an error flag should be shown, when nothing is actually saved - actors already existing in the role", function () {
            this.addUserAndGroupDialog.pickAnActor("admin (user)");
            this.addUserAndGroupDialog.pickAnActor("jira-administrators (group)");
            this.addUserAndGroupDialog.selectARole('<script>alert("Developers");</script>');
            this.sandbox.stub(formatter, "format", function () {
                var params = Array.prototype.slice.call(arguments, 0);
                return params.join(",");
            });
            this.addUserAndGroupDialog.submitWithNothingSaved();

            strictEqual($("#aui-flag-container .aui-message-error").text(),
                'admin.project.roles.adduerandgroup.users.and.groups.existing.error.msg,<script>alert("Developers");</script>', "Should show an error flag (with html-escaped role name) indicating that nothing is actually saved.");
        });

});
