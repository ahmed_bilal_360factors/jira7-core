/* globals define, AJS */
define("jira-project-config/roles/entities/roles-results-model", [
    'jira/util/formatter',
    'wrm/context-path',
    "jira/jquery/deferred",
    "jira-project-config/backbone",
    "jira-project-config/roles/entities/role-collection",
    "jira/ajs/ajax/smart-ajax",
    "underscore",
    "jira-project-config/uri",
    "jira-project-config/utils"
], function(
    formatter,
    wrmContextPath,
    Deferred,
    Backbone,
    RoleCollection,
    SmartAjax,
    _,
    URI,
    utils) {
    "use strict";

    var ALL_ROLE_VIEW_PAGE_SIZE = 5;
    var SPECIFIC_ROLE_VIEW_PAGE_SIZE = 20;
    var VISIBLE_PAGES_SIZE = 2; // visible pages before and after the current page
    var FIRST_PAGE_NUMBER = 1;

    return Backbone.Model.extend({
        defaults: {
            projectKey: "",
            roles: [],
            pageNumber: FIRST_PAGE_NUMBER,
            viewingRoleId: null, // null means viewing "All" roles
            query: "",
            isEmailVisible: false,
            showApplicationRole: false
        },

        initialize: function () {
            this.listenTo(this, "change:viewingRoleId change:query", function () {
                this.set("pageNumber", FIRST_PAGE_NUMBER, {silent: true}); // use silent because don't wanna trigger the reloading of the results view
            });
        },

        /**
         * Override the <code>Backbone.Model.fetch</code>, to abort the old fetch requests if a new request is fired before an old request is finished.
         * Also prevent the fail callbacks from being called when the a request is aborted.
         * @override
         * @returns {*}
         */
        fetch: function () {
            var $deferred = Deferred();

            if (this._currentFetchRequest) {
                this._currentFetchRequest.abort();
            }
            this._currentFetchRequest = Backbone.Model.prototype.fetch.apply(this, arguments);
            this._currentFetchRequest
                .fail(function (jqXHR, textStatus) {
                    if (textStatus !== "abort") {
                        $deferred.reject.apply($deferred, arguments);
                    }
                })
                .done($deferred.resolve);
            return $deferred;
        },

        /**
         * Generate role options for the role filter in the filter view
         * Add an extra option for "All" roles
         * @return {Array}
         */
        toFilterRoleOptions: function () {
            var json = this.toRoleOptions();
            json.unshift({
                text: formatter.I18n.getText("common.words.all"),
                value: ""
            });
            return json;
        },

        /**
         * Generate role options for the role selector
         * @return {Array}
         */
        toRoleOptions: function () {
            var json = this.get("roles").map(function (role) {
                return {
                    text: role.get("name"),
                    value: role.get("id")
                };
            });
            return json;
        },

        /**
         * Override to support the nested role collection
         * @override
         */
        set: function () {
            var params = Array.prototype.slice.call(arguments, 0);
            var key = params[0];

            if (_.isObject(key)) {
                if (key.hasOwnProperty("roles")) {
                    key.roles = new RoleCollection(key.roles);
                }
            } else {
                if (key === "roles") {
                    params[1] = new RoleCollection(params[1]);
                }
            }
            return Backbone.Model.prototype.set.apply(this, params);
        },

        toJSON: function () {
            var json = Backbone.Model.prototype.toJSON.apply(this, arguments);
            json.roles = json.roles.toJSON();
            json.isRoleResultEmpty = this.isRoleResultEmpty();
            json.isViewingDefaultView = this.isViewingDefaultView();
            json.allRoleViewPageSize = ALL_ROLE_VIEW_PAGE_SIZE;
            json.totalPageNumber = this.getTotalPageNumber();
            json.paginationData = this._buildPaginationData();
            return json;
        },

        /**
         * Default view is the view without any filtering criteria
         * @return {Boolean}
         */
        isViewingDefaultView: function () {
            return this.get("viewingRoleId") === null && this.get("query") === "";
        },

        /**
         * Roles in the current view have no actors
         * @returns {Boolean}
         */
        isRoleResultEmpty: function () {
            var actorsSum = this.get("roles").reduce(function (sum, role) {
                return sum + role.get("total");
            }, 0);
            return actorsSum === 0;
        },

        /**
         * Count all the actors which are visible in the current view, current page
         * @returns {Number} - Actors count
         */
        getVisibleActorsCount: function () {
            return this.get("roles").reduce(function (sum, role) {
                return sum + role.get("users").length + role.get("groups").length;
            }, 0);
        },

        /**
         * Get total page number of actors for the specific role being viewed.
         * @returns {undefined|Number} - Returns 'Undefined' when viewing all roles; page number >= 1
         */
        getTotalPageNumber: function () {
            var viewingRole = this._getViewingRole();
            return viewingRole && Math.ceil(viewingRole.get("total") / this._getPageSize());
        },

        url: function () {
            var viewingRoleId = this.get("viewingRoleId");

            var uri = URI("")
                .segment(wrmContextPath())
                .segment("rest/projectconfig/latest/roles")
                .segment(this.get("projectKey") + "")
                .addQuery("query", this.get("query"))
                .addQuery("pageNumber", this.get("pageNumber"))
                .addQuery("pageSize", this._getPageSize());

            if (viewingRoleId) {
                uri.addQuery("roleId", viewingRoleId);
            }

            return uri.toString();
        },

        /**
         * Remove user or group from a role
         * @param {Object} params
         * @param {String} params.roleId - ID of role to be removed actors from
         * @param {String=} params.userName - Name of user to be removed
         * @param {String=} params.groupName - Name of group to be removed
         * @returns {Object} - Deferred object
         */
        removeActorFromRole: function (params) {
            var userName = params.userName;
            var groupName = params.groupName;

            var uri = URI(utils.getRestBaseUrl())
                .segment("project")
                .segment(this.get("projectKey") + "")
                .segment("role")
                .segment(params.roleId + "");

            if (userName) {
                uri.addQuery("user", userName);
            } else if (groupName) {
                uri.addQuery("group", groupName);
            }

            var deferred = SmartAjax.makeRequest({
                url: uri.toString(),
                type: "DELETE",
                contentType: "application/json",
                dataType: "json"
            });

            deferred.then(null, this._handleFailRequest);

            return deferred;
        },
        /**
         * Add user(s) or group(s) to a role
         * @param {Object} params
         * @param {Number} params.roleId - ID of role to be removed actors from
         * @param {Object} params.data - Data of users and groups to be added
         * @returns {Object} - Deferred object
         */
        addUserOrGroupToRole: function (params) {
            var deferred = SmartAjax.makeRequest({
                url: wrmContextPath() + "/rest/projectconfig/latest/roles/" + encodeURIComponent(this.get("projectKey")) + "/" + params.roleId,
                data: JSON.stringify(params.data),
                type: "POST",
                contentType: "application/json",
                dataType: "json"
            });

            deferred.then(null, this._handleFailRequest);

            return deferred;
        },
        /**
         * Return the currently viewing role
         * @returns {undefined|Role} - <code>undefined</code> when viewing all roles
         * @private
         */
        _getViewingRole: function () {
            return this.get("roles").get(parseInt(this.get("viewingRoleId")));
        },

        _handleFailRequest: function (xhr) {
            var errors;
            try {
                errors = JSON.parse(xhr.responseText);
            } catch (e) {
                // no error collection so return generic error
                errors = {
                    errorMessages: [
                        formatter.I18n.getText("admin.projectconfig.error.generic.message")
                    ]
                };
            }
            return _.extend(xhr, errors);
        },

        _buildPaginationData: function () {
            if (this.get("viewingRoleId") !== null) {
                var currentPageNumber = this.get("pageNumber");
                var lastPageNumber = this.getTotalPageNumber();
                var pageSize = this._getPageSize();

                var result = {
                    firstPage: null,
                    previousPage: null,
                    pages: [],
                    nextPage: null,
                    lastPage: null,
                    showingFrom: null,
                    showingTo: null
                };

                if (currentPageNumber > FIRST_PAGE_NUMBER) {
                    result.firstPage = FIRST_PAGE_NUMBER;
                    result.previousPage = currentPageNumber - 1;
                }

                if (currentPageNumber < lastPageNumber) {
                    result.nextPage = currentPageNumber + 1;
                    result.lastPage = lastPageNumber;
                }

                result.showingFrom = ((currentPageNumber - 1) * pageSize) + 1;
                result.showingTo = Math.min(result.showingFrom - 1 + pageSize, this._getViewingRole().get("total"));


                var from = Math.max(currentPageNumber - VISIBLE_PAGES_SIZE, FIRST_PAGE_NUMBER);
                var to = Math.min(currentPageNumber + VISIBLE_PAGES_SIZE, lastPageNumber);

                for (var number = from; number <= to; number++) {
                    result.pages.push({
                        number: number,
                        selected: number === currentPageNumber
                    });
                }

                return result;
            }
        },
        _getPageSize: function () {
            if (this.get("viewingRoleId") === null) {
                return ALL_ROLE_VIEW_PAGE_SIZE;
            }
            return SPECIFIC_ROLE_VIEW_PAGE_SIZE;
        }
    });
});
