define("jira-project-config/roles/adduserdialog/add-user-dialog-view", [
    'jira/util/formatter',
    'jira/util/logger',
    "jira-project-config/backbone",
    "jquery",
    "underscore",
    "jira-project-config/global/components/userandgrouppicker/user-and-group-picker",
    "aui/flag",
    "jira-project-config/roles/adduserdialog/template"
], function(formatter, logger, Backbone, $, _, UserAndGroupPicker, flag, Template) {

    return Backbone.View.extend({
        template: Template.content,
        events: {
            "submit .js-roles-add-usergroup-form": "_onSubmit"
        },
        render: function () {
            this._$dialogContent = this.$(".js-roles-add-user-dialog-content");

            // This dialog is rendered in the roles screen's default view
            // we store a cache of all available roles
            // in case the roles screen got filtered later
            this._allRoleOptions = this.model.toRoleOptions();

            // re-render dialog content before showing the dialog
            this.el.addEventListener("aui-layer-show", _.bind(function () {
                this._renderDialogContent();
            }, this));

            // auto-focus on the user & group picker whenever the dialog is shown
            // aui inline-dialog2 doesn't have any events for after showing, so we need to use the DOM's transitionend event.
            // transitionend is only supported in IE10+
            this.el.addEventListener("transitionend", _.bind(function (e) {
                if (this.el.open === true) {
                    this._userAndGroupPicker.$field.focus();
                }
            }, this));

            return this;
        },
        _setupAlias: function () {
            this._$addUserAndGroupAction = this.$(".js-roles-add-usergroup-action");
            this._$form = this.$(".js-roles-add-usergroup-form");
            this._$formStatus = this.$(".js-roles-in-progress-icon");
            this._$pickerElement = this.$(".js-roles-usergroup-picker");
            this._$rolerPicker = this.$(".js-roles-role-picker");
        },
        _renderDialogContent: function () {
            // render dialog content
            this._$dialogContent.html(this.template({roles: this._allRoleOptions}));

            this._setupAlias();

            this._userAndGroupPicker = new UserAndGroupPicker({
                element: this._$pickerElement
            });
            this._$pickerElement
                .on("selected", _.bind(function (e, selectedItem, picker) {
                    if (picker.getSelectedDescriptors().total() === 1) {
                        // enable submit button & role picker where there is at least 1 selected user or group
                        this._enableElement(this._$addUserAndGroupAction);
                        this._enableElement(this._$rolerPicker);
                    }
                }, this))
                .on("unselect", _.bind(function (e, unselectedItem, picker) {
                    if (picker.getSelectedDescriptors().total() === 0) {
                        // disable submit button & role picker where there is 0 selected user or group
                        this._disableElement(this._$addUserAndGroupAction);
                        this._disableElement(this._$rolerPicker);
                    }
                }, this));
        },
        _disableElement: function ($element) {
            $element.attr("disabled", true);
            $element.attr("aria-disabled", true);
        },
        _enableElement: function ($element) {
            $element.removeAttr("disabled");
            $element.removeAttr("aria-disabled");
        },
        _beforeSubmit: function () {
            this._disableElement(this._$addUserAndGroupAction);
            // $.show() set "display: inline;" by default
            this._$formStatus.css("display", "inline-block");
        },
        _afterSubmit: function () {
            this._enableElement(this._$addUserAndGroupAction);
            this._$formStatus.hide();
            logger.trace("add.user.and.group.completed");
        },
        _getSelectedRole: function () {
            var $selectedRoleOption = this._$form.find("#roles-role-picker option:selected");
            return {
                id: parseInt($selectedRoleOption.val()),
                name: $selectedRoleOption.text()
            };
        },
        _buildSubmitData: function () {
            var selectedUserAndGroups = this._userAndGroupPicker.getSelectedDescriptors();
            var submitData = {
                "users": [],
                "groups": []
            };
            _.each(selectedUserAndGroups.users, function (user) {
                submitData.users.push(user.value());
            });
            _.each(selectedUserAndGroups.groups, function (group) {
                submitData.groups.push(group.value());
            });

            return submitData;
        },
        _onSubmit: function (e) {
            if (this._$addUserAndGroupAction.is(":disabled")) {
                return false;
            }
            e.preventDefault();
            var selectedRole = this._getSelectedRole();
            var params = {
                roleId: selectedRole.id,
                data: this._buildSubmitData()
            };
            this._beforeSubmit();
            this.model.addUserOrGroupToRole(params)
                .fail(function (xhr) {
                    flag({
                        type: "error",
                        close: "auto",
                        body: AJS.escapeHTML(xhr.errorMessages[0])
                    });
                })
                .done(_.bind(function (data) {
                    if (data.numberOfGroupsAdded === 0 && data.numberOfUsersAdded === 0) {
                        flag({
                            type: "error",
                            close: "auto",
                            body: formatter.I18n.getText(
                                "admin.project.roles.adduerandgroup.users.and.groups.existing.error.msg",
                                AJS.escapeHTML(selectedRole.name)
                            )
                        });
                    } else {
                        var addedActorMsg = [];
                        if (data.numberOfGroupsAdded > 0) {
                            addedActorMsg.push(formatter.I18n.getText(
                                "admin.project.roles.adduerandgroup.success.msg.groups",
                                data.numberOfGroupsAdded
                            ));
                        }
                        if (data.numberOfUsersAdded > 0) {
                            addedActorMsg.push(formatter.I18n.getText(
                                "admin.project.roles.adduerandgroup.success.msg.users",
                                data.numberOfUsersAdded
                            ));
                        }
                        var successMsg = formatter.I18n.getText(
                            "admin.project.roles.adduerandgroup.success.msg",
                            "<strong>",
                            addedActorMsg.join(" " + formatter.I18n.getText("common.words.and") + " "),
                            "</strong>",
                            AJS.escapeHTML(selectedRole.name)
                        );

                        flag({
                            type: "success",
                            close: "auto",
                            body: successMsg
                        });

                        this.trigger("actorsAdded");
                    }
                }, this))
                .always(_.bind(function () {
                    // hide the dialog
                    this.el.open = false;
                    this._afterSubmit();
                }, this));
        }
    });

});
