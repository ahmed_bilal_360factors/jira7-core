AJS.test.require([
    "com.atlassian.jira.jira-project-config-plugin:project-config-roles"
], function () {
    "use strict";

    require([
        "jquery",
        "underscore",
        "jira-project-config/roles/entities/roles-results-model",
        "jira-project-config/roles/roles-filter-view",
        "jira-project-config/roles/roles-controller",
        "wrm/context-path"
    ], function(
        jQuery,
        _,
        RolesResultsModel,
        RolesFilterView,
        RolesController,
        wrmContextPath
    ) {
        var rolesFilterHtml = "<div class=\"js-role-filter\"></div>";
        var rolesResultHtml = "<div class=\"js-role-results\"></div>";
        var addUserAndGroupDialogHtml = "<div><div class=\"js-roles-add-user-dialog-content\"></div></div>";
        var modelJson = {
            projectKey: "HR"
        };
        var defaultViewResponse = {
            "roles": [
                {
                    "id": 10001, "name": "Administrators", "total": 7, "users": [
                    {
                        "active": true,
                        "name": "jeff+0",
                        "displayName": "jeff 0",
                        "email": "jeff+0@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "jeff+1",
                        "displayName": "jeff 1",
                        "email": "jeff+1@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "jeff+2",
                        "displayName": "jeff 2",
                        "email": "jeff+2@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "jeff+3",
                        "displayName": "jeff 3",
                        "email": "jeff+3@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "jeff+4",
                        "displayName": "jeff 4",
                        "email": "jeff+4@example.com",
                        applicationRoleNames: []
                    }
                ],
                    "groups": [
                        {"name": "amin 0"},
                        {"name": "amin 1"}
                    ]
                },
                {
                    "id": 10002, "name": "Customers", "total": 26, "users": [
                    {
                        "active": true,
                        "name": "joe+0",
                        "displayName": "joe 0",
                        "email": "joe+0@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "joe+1",
                        "displayName": "joe 1",
                        "email": "joe+1@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "joe+2",
                        "displayName": "joe 2",
                        "email": "joe+2@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "joe+3",
                        "displayName": "joe 3",
                        "email": "joe+3@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "joe+4",
                        "displayName": "joe 4",
                        "email": "joe+4@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "joe+5",
                        "displayName": "joe 5",
                        "email": "joe+5@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "joe+6",
                        "displayName": "joe 6",
                        "email": "joe+6@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "joe+7",
                        "displayName": "joe 7",
                        "email": "joe+7@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "joe+8",
                        "displayName": "joe 8",
                        "email": "joe+8@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "joe+9",
                        "displayName": "joe 9",
                        "email": "joe+9@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "joe+10",
                        "displayName": "joe 10",
                        "email": "joe+10@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "joe+11",
                        "displayName": "joe 11",
                        "email": "joe+11@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "joe+12",
                        "displayName": "joe 12",
                        "email": "joe+12@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "joe+13",
                        "displayName": "joe 13",
                        "email": "joe+13@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "joe+14",
                        "displayName": "joe 14",
                        "email": "joe+14@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "joe+15",
                        "displayName": "joe 15",
                        "email": "joe+15@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "joe+16",
                        "displayName": "joe 16",
                        "email": "joe+16@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "joe+17",
                        "displayName": "joe 17",
                        "email": "joe+17@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "joe+18",
                        "displayName": "joe 18",
                        "email": "joe+18@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "joe+19",
                        "displayName": "joe 19",
                        "email": "joe+19@example.com",
                        applicationRoleNames: []
                    }
                ],
                    "groups": [
                        {"name": "group 0"},
                        {"name": "group 1"},
                        {"name": "group 2"},
                        {"name": "group 3"},
                        {"name": "group 4"},
                        {"name": "group 5"}
                    ]
                }
            ]
        };

        var filerByRoleResponse = {
            roles: {
                "id": 10002, "name": "Customers", "total": 66, "users": [
                    {
                        "active": true,
                        "name": "joe+0",
                        "displayName": "joe 0",
                        "email": "joe+0@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "joe+1",
                        "displayName": "joe 1",
                        "email": "joe+1@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "joe+2",
                        "displayName": "joe 2",
                        "email": "joe+2@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "joe+3",
                        "displayName": "joe 3",
                        "email": "joe+3@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "joe+4",
                        "displayName": "joe 4",
                        "email": "joe+4@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "joe+5",
                        "displayName": "joe 5",
                        "email": "joe+5@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "joe+6",
                        "displayName": "joe 6",
                        "email": "joe+6@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "joe+7",
                        "displayName": "joe 7",
                        "email": "joe+7@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "joe+8",
                        "displayName": "joe 8",
                        "email": "joe+8@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "joe+9",
                        "displayName": "joe 9",
                        "email": "joe+9@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "joe+10",
                        "displayName": "joe 10",
                        "email": "joe+10@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "joe+11",
                        "displayName": "joe 11",
                        "email": "joe+11@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "joe+12",
                        "displayName": "joe 12",
                        "email": "joe+12@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "joe+13",
                        "displayName": "joe 13",
                        "email": "joe+13@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "joe+14",
                        "displayName": "joe 14",
                        "email": "joe+14@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "joe+15",
                        "displayName": "joe 15",
                        "email": "joe+15@example.com",
                        applicationRoleNames: []
                    },
                    {
                        "active": true,
                        "name": "joe+16",
                        "displayName": "joe 16",
                        "email": "joe+16@example.com",
                        applicationRoleNames: []
                    }
                ],
                "groups": [
                    {"name": "group 0"},
                    {"name": "group 1"},
                    {"name": "group 2"}
                ]
            }
        };

        var oneActorInSecondPageResponse = {
            roles: {
                "id": 10002, "name": "Customers", "total": 21, "users": [
                    {
                        "active": true,
                        "name": "joe+0",
                        "displayName": "joe 0",
                        "email": "joe+0@example.com",
                        applicationRoleNames: []
                    }
                ],
                "groups": []
            }
        };

        var filterWithNoMatchResponse = {
            roles: [
                {
                    id: 10001,
                    name: "Administrators",
                    total: 0,
                    groups: [],
                    users: []
                },
                {
                    id: 10002,
                    name: "Customers",
                    total: 0,
                    users: [],
                    groups: []
                }
            ]
        };
        var filterByQueryResponse = {
            roles: [
                {
                    id: 10001,
                    name: "Administrators",
                    total: 1,
                    users: [],
                    groups: [
                        {name: "jira-administrators"}
                    ]
                },
                {
                    id: 10002,
                    name: "Customers",
                    total: 1,
                    users: [
                        {
                            active: true,
                            name: "airons",
                            displayName: "Andy Irons",
                            email: "and@gmail.com",
                            applicationRoleNames: []
                        }
                    ],
                    groups: []
                }
            ]
        };
        var addedUserResponse = {
            roles: [
                {
                    id: 10001,
                    name: "Administrators",
                    total: 4,
                    groups: [
                        {name: "jira-administrators"}
                    ],
                    users: [
                        {
                            active: true,
                            name: "newuser",
                            displayName: "New User",
                            email: "newuser@example.com",
                            applicationRoleNames: []
                        },
                        {
                            active: true,
                            name: "sharwood",
                            displayName: "Scott Harwood",
                            email: "sh@gmail.com",
                            applicationRoleNames: []
                        },
                        {
                            active: true,
                            name: "bbaker",
                            displayName: "Brad Baker",
                            email: "bb@gmail.com",
                            applicationRoleNames: []
                        }
                    ]
                },
                {
                    id: 10002,
                    name: "Customers",
                    total: 3,
                    users: [
                        {
                            active: true,
                            name: "jwilson",
                            displayName: "Julian Wilson",
                            email: "sh@gmail.com",
                            applicationRoleNames: []
                        },
                        {
                            active: true,
                            name: "kslater",
                            displayName: "Kelly Slater",
                            email: "kelly@gmail.com",
                            applicationRoleNames: []
                        },
                        {
                            active: true,
                            name: "airons",
                            displayName: "Andy Irons",
                            email: "and@gmail.com",
                            applicationRoleNames: []
                        }
                    ],
                    groups: []
                }
            ]
        };

        module("jira-project-config/roles/roles-controller-test", {
            initRolesControler: function () {
                var sandbox = this.sandbox;
                var fakeRequests = this.fakeRequests;
                var model = new RolesResultsModel(modelJson);
                var rolesController = new RolesController({
                    model: model
                });
                var resultsView = rolesController.resultsView;
                var filterView = rolesController.filterView;
                var addUserDialogView = rolesController.addUserDialogView;
                sandbox.stub(addUserDialogView, "render"); // we don't wanna bring in the dependencies from the dialog
                rolesController.showDefault();
                fakeRequests[fakeRequests.length - 1].respond(200,
                    {"Content-Type": "application/json"},
                    JSON.stringify(defaultViewResponse)
                );

                var selectARole = function (roleName) {
                    filterView._$roleSelector.find("option").each(function (i, option) {
                        var $option = jQuery(option);
                        if ($option.text() === roleName) {
                            filterView._$roleSelector.auiSelect2("val", $option.attr("value"));
                            filterView._$roleSelector.trigger("change");
                            fakeRequests[fakeRequests.length - 1].respond(200,
                                {"Content-Type": "application/json"},
                                JSON.stringify(filerByRoleResponse)
                            );
                            return false;
                        }
                    });
                };

                var inputQuery = function (query) {
                    filterView._$queryInput.val(query).trigger("input");
                    sandbox.clock.tick(300); //query input debounce
                    fakeRequests[fakeRequests.length - 1].respond(200,
                        {"Content-Type": "application/json"},
                        JSON.stringify(filterByQueryResponse)
                    );
                };

                var inputQueryWithEmptyResult = function (query) {
                    filterView._$queryInput.val(query).trigger("input");
                    sandbox.clock.tick(300); //query input debounce
                    fakeRequests[fakeRequests.length - 1].respond(200,
                        {"Content-Type": "application/json"},
                        JSON.stringify(filterWithNoMatchResponse)
                    );
                };

                var showActorsInARole = function (roleName) {
                    filterView._$roleSelector.find("option").each(function (i, option) {
                        var $option = jQuery(option);
                        if ($option.text() === roleName) {
                            resultsView.$(".js-roles-show-all[data-role-id=" + $option.attr("value") + "]").click();
                            fakeRequests[fakeRequests.length - 1].respond(200,
                                {"Content-Type": "application/json"},
                                JSON.stringify(filerByRoleResponse)
                            );
                            return false;
                        }
                    });
                };

                var showActorsInAllRole = function () {
                    resultsView.$(".js-roles-show-less").click();
                    fakeRequests[fakeRequests.length - 1].respond(200,
                        {"Content-Type": "application/json"},
                        JSON.stringify(defaultViewResponse)
                    );
                };

                var goToPage = function (pageNumber) {
                    var $pageElement = resultsView.$(".js-roles-pagination-navigator [data-page-number=" + pageNumber + "]:eq(0)");
                    if ($pageElement.size() === 1) {
                        $pageElement.click();
                        fakeRequests[fakeRequests.length - 1].respond(200,
                            {"Content-Type": "application/json"},
                            JSON.stringify(oneActorInSecondPageResponse)
                        );
                    }
                };

                var removeAnActor = function () {
                    resultsView.trigger("actorRemoved");
                    // request of reloading results view
                    fakeRequests[fakeRequests.length - 1].respond(200,
                        {"Content-Type": "application/json"},
                        JSON.stringify(defaultViewResponse));
                };

                var addAUserToARole = function () {
                    addUserDialogView.trigger("actorsAdded");
                    fakeRequests[fakeRequests.length - 1].respond(200,
                        {"Content-Type": "application/json"},
                        JSON.stringify(addedUserResponse));
                };

                var getRolesInResultsView = function () {
                    return _.pluck(resultsView.$(".roles-result-header-role-name"), "textContent");
                };

                var getActorsInResultsView = function () {
                    return resultsView.$(".roles-group-name, .roles-user-name-val").map(function () {
                        return jQuery.trim(jQuery(this).text());
                    }).get();
                };

                return {
                    model: model,
                    instance: rolesController,
                    resultsView: resultsView,
                    filterView: filterView,
                    selectARole: selectARole,
                    inputQuery: inputQuery,
                    inputQueryWithEmptyResult: inputQueryWithEmptyResult,
                    showActorsInARole: showActorsInARole,
                    showActorsInAllRole: showActorsInAllRole,
                    removeAnActor: removeAnActor,
                    addAUserToARole: addAUserToARole,
                    getRolesInResultsView: getRolesInResultsView,
                    getActorsInResultsView: getActorsInResultsView,
                    goToPage: goToPage
                };
            },
            setup: function () {
                this.sandbox = sinon.sandbox.create();
                this.sandbox.useFakeServer();
                this.sandbox.useFakeTimers();
                this.fakeRequests = this.sandbox.server.requests;

                jQuery("#qunit-fixture").append(rolesResultHtml + rolesFilterHtml + addUserAndGroupDialogHtml);
                this.controller = this.initRolesControler();
                this.sandbox.spy(this.controller.instance.model, "fetch");
                this.sandbox.spy(this.controller.resultsView, "render");

            },
            teardown: function () {
                this.sandbox.restore();
            }
        });

        test("Filter by role should reload the results view with the returned results", function () {
            this.controller.selectARole("Customers");
            var request = this.fakeRequests[this.fakeRequests.length - 1];

            ok(request.url.indexOf(wrmContextPath() + "/rest/projectconfig/latest/roles/HR?query=&pageNumber=1&pageSize=20&roleId=10002") === 0,
                "Results should be fetched with the selected role & the size of 20 actors per page");
            strictEqual(request.method, "GET", "Results should be fetched using GET");
            strictEqual(this.controller.resultsView.$(".roles-result-header-role-name").size(), 1,
                "Results view should contain only the selected role");
            strictEqual(this.controller.resultsView.$(".roles-result-header-role-name").text(),
                "Customers", "Results view should contain only the selected role");
            strictEqual(this.controller.resultsView.$(".js-roles-show-less").size(), 1, "There should be a 'Show less' button");
        });

        test("Filter by query should reload the results view with the returned results", function () {
            this.controller.inputQuery("blah");
            var request = this.fakeRequests[this.fakeRequests.length - 1];

            ok(request.url.indexOf(wrmContextPath() + "/rest/projectconfig/latest/roles/HR?query=blah&pageNumber=1&pageSize=5") === 0,
                "Results should be fetched with the input query & the size of 5 actors per page");
            strictEqual(request.method, "GET", "Results should be fetched using GET");
            deepEqual(this.controller.getRolesInResultsView(), ["Administrators", "Customers"],
                "Results view should contain returned role names");
            deepEqual(this.controller.getActorsInResultsView(), ["jira-administrators", "Andy Irons"],
                "Results view should contain returned role names");
        });

        test("Filter by query with no matches should display a no match message in the results view", function () {
            this.controller.inputQueryWithEmptyResult("blahblah");

            strictEqual(this.controller.resultsView.$(".roles-search-no-matches").size(), 1,
                "A no match message should be visible");
        });

        test("Remove an actor should reload the results view", function () {
            this.controller.removeAnActor();
            var reloadRequest = this.fakeRequests[this.fakeRequests.length - 1];

            ok(reloadRequest.url.indexOf(wrmContextPath() + "/rest/projectconfig/latest/roles/HR?query=&pageNumber=1&pageSize=5") === 0,
                "Reloading the results view should be triggered");
            strictEqual(reloadRequest.method, "GET", "Reloading the results view should be triggered");
            sinon.assert.calledOnce(this.controller.resultsView.render, 1);
        });

        test("Add a user to a role should reload the results view", function () {
            this.controller.addAUserToARole();
            var request = this.fakeRequests[this.fakeRequests.length - 1];

            ok(request.url.indexOf(wrmContextPath() + "/rest/projectconfig/latest/roles/HR?query=&pageNumber=1&pageSize=5") === 0,
                "Reloading the results view should be triggered");
            strictEqual(request.method, "GET", "Reloading the results view should be triggered");
            sinon.assert.calledOnce(this.controller.resultsView.render, 1);
        });

        test("Click 'Show All' on a role should filter the results view by that role", function () {
            this.controller.showActorsInARole("Customers");
            var request = this.fakeRequests[this.fakeRequests.length - 1];

            ok(request.url.indexOf(wrmContextPath() + "/rest/projectconfig/latest/roles/HR?query=&pageNumber=1&pageSize=20&roleId=10002") === 0,
                "Results should be fetched with the selected role & the size of 20 actors per page");
            strictEqual(request.method, "GET", "Results should be fetched using GET");
            strictEqual(this.controller.resultsView.$(".roles-result-header-role-name").size(), 1,
                "Results view should contain only the selected role");
            strictEqual(this.controller.resultsView.$(".roles-result-header-role-name").text(),
                "Customers", "Results view should contain only the selected role");
            strictEqual(this.controller.resultsView.$(".js-roles-show-less").size(), 1, "There should be a 'Show less' button");
        });

        test("Click 'Show less' on a role should filter the results view by all role", function () {
            this.controller.showActorsInARole("Customers");
            this.controller.showActorsInAllRole();
            var request = this.fakeRequests[this.fakeRequests.length - 1];

            ok(request.url.indexOf(wrmContextPath() + "/rest/projectconfig/latest/roles/HR?query=&pageNumber=1&pageSize=5") === 0,
                "Reloading the results view should be triggered");
            strictEqual(request.method, "GET", "Results should be fetched using GET");
            strictEqual(this.controller.resultsView.$(".js-roles-show-less").size(), 0,
                "There should not be any 'Show less' buttons");
            // callCount === 2, because the first call is triggered by 'showActorsInARole'
            sinon.assert.callCount(this.controller.resultsView.render, 2);
        });

        test("Go to another page should reload the results view", function () {
            this.controller.showActorsInARole("Customers");
            this.controller.goToPage(4);
            var request = this.fakeRequests[this.fakeRequests.length - 1];

            ok(request.url.indexOf(wrmContextPath() + "/rest/projectconfig/latest/roles/HR?query=&pageNumber=4&pageSize=20&roleId=10002") === 0,
                "Results should be fetched with the selected page number");
            strictEqual(request.method, "GET", "Results should be fetched using GET");
            // callCount === 2, because the first call is triggered by 'showActorsInARole'
            sinon.assert.callCount(this.controller.resultsView.render, 2);
        });

        test("Go to another page should keep the filter criteria", function () {
            this.controller.inputQuery("blahh");
            this.controller.selectARole("Customers");
            this.controller.goToPage(4);

            var request = this.fakeRequests[this.fakeRequests.length - 1];
            ok(request.url.indexOf(wrmContextPath() + "/rest/projectconfig/latest/roles/HR?query=blahh&pageNumber=4&pageSize=20&roleId=10002") === 0,
                "Results should be fetched with the filter criteria");
            strictEqual(this.controller.filterView._$queryInput.val(), "blahh", "Should keep the query string of filter criteria");
            strictEqual(this.controller.filterView._$roleSelector.auiSelect2("val"), "10002", "Should keep the selected role of filter criteria");
        });

        test("Go back to the first page when filtering with new criteria", function () {
            this.controller.selectARole("Customers");
            this.controller.goToPage(4);
            this.controller.inputQuery("blahhh");
            var request = this.fakeRequests[this.fakeRequests.length - 1];

            ok(request.url.indexOf(wrmContextPath() + "/rest/projectconfig/latest/roles/HR?query=blahhh&pageNumber=1&pageSize=20&roleId=10002") === 0,
                "Results should be fetched with the first page number");
            strictEqual(this.controller.model.get("pageNumber"), 1, "Results should be fetched with the first page number");
        });

        test("Add actors should keep the current page number.", function () {
            this.controller.selectARole("Customers");
            this.controller.goToPage(4);
            this.controller.addAUserToARole();
            var request = this.fakeRequests[this.fakeRequests.length - 1];

            ok(request.url.indexOf(wrmContextPath() + "/rest/projectconfig/latest/roles/HR?query=&pageNumber=4&pageSize=20&roleId=10002") === 0,
                "Results should be fetched with current page number");
            strictEqual(this.controller.model.get("pageNumber"), 4, "Results should be fetched with the current page number");
        });

        test("Remove actors should go back one page if there are not any actors for the current page", function () {
            this.controller.selectARole("Customers");
            this.controller.goToPage(4);
            this.controller.removeAnActor();
            var request = this.fakeRequests[this.fakeRequests.length - 1];

            ok(request.url.indexOf(wrmContextPath() + "/rest/projectconfig/latest/roles/HR?query=&pageNumber=3&pageSize=20&roleId=10002") === 0,
                "Results should be fetched with the previous page number");
            strictEqual(this.controller.model.get("pageNumber"), 3, "Results should be fetched with the previous page number");
        });
    });
});
