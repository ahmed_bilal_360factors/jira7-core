/* globals define */
define("jira-project-config/roles/roles-results-view", [
    'jira/util/logger',
    "jira/jquery/deferred",
    "jira-project-config/backbone",
    "jquery",
    "underscore",
    "jira-project-config/roles/template"
], function(logger, Deferred, Backbone, $, _, Template) {
    "use strict";

    return Backbone.View.extend({
        template: Template.results,
        removeErrorTemplate: Template.removeError,
        events: {
            "click .js-roles-remove": "_handleRemoveActor",
            "click .js-roles-show-all": "_onShowActorsInARole",
            "click .js-roles-show-less": "_onShowActorsInAllRole",
            "click .js-roles-pagination-navigator a": "_onGotoPage"
        },
        render: function () {
            this.$el.html(this.template(this.model.toJSON()));
            this.$(".js-roles-remove").tooltip({gravity: 'e'});
            logger.trace("roles.results.rendered");
        },
        _handleRemoveActor: function (e) {
            e.preventDefault();

            var $currentTarget = $(e.currentTarget);
            $currentTarget.blur();

            var roleId = $currentTarget.data("role-id");
            var userName = $currentTarget.data("user-name");
            var groupName = $currentTarget.data("group-name");

            var params = {
                roleId: roleId
            };
            if (userName) {
                _.extend(params, {userName: userName});
            } else if (groupName) {
                _.extend(params, {groupName: groupName});
            }

            this.model.removeActorFromRole(params)
                .fail(_.bind(function (xhr) {
                    this._showRemoveError($currentTarget, xhr.errorMessages[0]);
                }, this))
                .done(_.bind(function () {
                    this._hideRemoveError($currentTarget);

                    // to fix the sticky tooltip issue,
                    // hide the tooltip after reloading the results view,
                    var deferred = Deferred();
                    deferred.done(function () {
                        $currentTarget.tooltip('hide');
                    });

                    this.trigger("actorRemoved", deferred);
                }, this))
                .always(function () {
                    logger.trace("roles.remove.actor.completed");
                });
        },
        _hideRemoveError: function ($target) {
            var $removeError = $target.next();
            if ($removeError.is(".js-roles-remove-error")) {
                $removeError.remove();
            }
        },
        _showRemoveError: function ($target, errorMessage) {
            var $removeError = $target.next();

            if ($removeError.is(".js-roles-remove-error")) {
                // remove error is already existing, re-use it
                $removeError.data("error-message", errorMessage);
            } else {
                $removeError = $(this.removeErrorTemplate({errorMessage: errorMessage})).tooltip({
                    gravity: 'e',
                    title: function () {
                        return $(this).data("error-message");
                    }
                });
                $target.after($removeError);
            }
        },
        _onShowActorsInARole: function (e) {
            var selectedRoleId = $(e.target).data("role-id");
            this.model.set("viewingRoleId", selectedRoleId);
            this.trigger("showingActorOfARoleClicked");
        },
        _onShowActorsInAllRole: function () {
            this.model.set("viewingRoleId", null);
            this.trigger("showingActorOfAllRolesClicked");
        },
        _onGotoPage: function (e) {
            e.preventDefault();
            var targetPageNumber = $(e.target).data("page-number");
            this.model.set("pageNumber", targetPageNumber);
            var deferred = Deferred();
            deferred.done(function () {
                logger.trace("roles.goto.page.completed");
            });
            this.trigger("pageNumberChanged", deferred);
        }
    });
});

