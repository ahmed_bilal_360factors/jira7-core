define("jira-project-config/roles/entities/role-model", ["jira-project-config/backbone", "underscore"], function(Backbone, _) {

    return Backbone.Model.extend({
        defaults: {
            id: null,
            name: "",
            total: null,
            groups: [],
            users: []
        },

        toJSON: function () {
            var json = Backbone.Model.prototype.toJSON.apply(this, arguments);
            _.each(json.users, function (user) {
                user.applicationRolesText = user.applicationRoleNames.join(", ");
            });
            return json;
        }

    });
});
