define("jira-project-config/workflows/tab/utils/active-callbacks", ["require"], function(require){
    "use strict";

    var logger = require('jira/util/logger');

    var activeCallbacks = 0;

    function increment() {
        activeCallbacks++;
    }

    function decrement() {
        if (activeCallbacks > 0) {
            activeCallbacks--;
        }
        if (!activeCallbacks) {
            logger.trace("project.config.complete");
        }
    }

    return {
        increment: increment,
        decrement: decrement
    };
});
