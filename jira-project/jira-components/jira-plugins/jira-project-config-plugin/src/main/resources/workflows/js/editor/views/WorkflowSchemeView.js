define("jira-project-config/workflows/scheme/editor/views/workflow-scheme", ["require"], function(require){
    "use strict";

    var jQuery = require('jquery');
    var _ = require('underscore');
    var Backbone = require('jira-project-config/backbone');
    var WorkflowView = require('jira-project-config/workflows/scheme/editor/views/workflow');
    var WorkflowSchemeHeaderView = require('jira-project-config/workflows/scheme/editor/views/workflow-scheme-header');
    var TemplatesEditor = require('jira-project-config/workflows/scheme/editor/templates');

    return Backbone.View.extend({

        initialize: function (options) {
            this.issueTypes = options.issueTypes;
            this.headerViewCotr = options.headerViewCotr || WorkflowSchemeHeaderView;
            this.workflowViewCotr = options.workflowViewCotr || WorkflowView;

            this.workflowViews = {};
            this.bus = options.bus;
            this.viewingOriginal = false;

            this.model.workflows.on("remove", this.removeWorkflow, this);
            this.model.workflows.on("add", this.renderWorkflow, this);
            this.model.workflows.on("reset", this.reset, this);
            this.issueTypes.on("reset", this.reset, this);
            this.bus.on("viewOriginal", this.viewOriginal, this);
            this.bus.on("viewDraft", this.viewDraft, this);
        },

        render: function () {
            this.headerView = new this.headerViewCotr({
                model: this.model,
                issueTypes: this.issueTypes,
                workflows: this.model.workflows,
                bus: this.bus
            }).render();
            this.$el.append(this.headerView.$el);

            this.$workflows = jQuery(TemplatesEditor.WorkflowSchemeTable()).appendTo(this.$el).find('tbody');

            this.model.workflows.each(this.renderWorkflow, this);

            if (this.model.get("migrationProgressURL")) {
                this.$el.addClass("workflows-migration").append(jQuery("<div>").addClass("workflows-blanket"));
            }

            return this;
        },

        viewDraft: function () {
            if (this.viewingOriginal) {
                this.viewingOriginal = false;
                this.reset(this.model.workflows);
            }
        },
        viewOriginal: function () {
            if (!this.viewingOriginal && this.model.originalWorkflows.length) {
                this.viewingOriginal = true;
                this.reset(this.model.originalWorkflows);
            }
        },

        finalize: function () {
            this.headerView.finalize();
            _.each(_.values(this.workflowViews), function (workflowView) {
                workflowView.finalize();
            });
            this.workflowViews = {};
            this.model.workflows.off(null, null, this);
        },

        renderWorkflow: function (model) {
            var workflowView = new this.workflowViewCotr({
                model: model,
                issueTypes: this.issueTypes,
                workflows: this.model.workflows,
                bus: this.bus,
                editable: !this.viewingOriginal
            }).render();
            this.workflowViews[model.id] = workflowView;
            this.$workflows.append(workflowView.$el);
        },
        reset: function (workflows) {

            workflows = workflows || this.model.workflows;

            _.each(_.values(this.workflowViews), function (workflowView) {
                workflowView.unrender();
            });
            this.workflowViews = {};
            workflows.each(this.renderWorkflow, this);
        },
        removeWorkflow: function (model) {
            delete this.workflowViews[model.id];
        }
    });
});
