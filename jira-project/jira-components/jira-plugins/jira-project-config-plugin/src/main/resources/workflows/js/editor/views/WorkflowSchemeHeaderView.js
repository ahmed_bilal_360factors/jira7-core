define("jira-project-config/workflows/scheme/editor/views/workflow-scheme-header", ["require"], function(require){
    "use strict";

    var jQuery = require('jquery');
    var _ = require('underscore');
    var Reasons = require('jira/util/events/reasons');
    var Types = require('jira/util/events/types');
    var Events = require('jira/util/events');
    var Backbone = require('jira-project-config/backbone');
    var TemplatesEditor = require('jira-project-config/workflows/scheme/editor/templates');
    var AddWorkflowDialogView = require('jira-project-config/workflows/dialogs/add-workflow-dialog-view');
    var AssignIssueTypes = require('jira-project-config/workflows/dialogs/assign-issue-types');
    var DiscardDraft = require('jira-project-config/workflows/dialogs/discard-draft');

    var hasFocus = function ($container) {
        var activeElement = document.activeElement;
        return $container.find(activeElement).length || $container.filter(activeElement).length;
    };

    return Backbone.View.extend({

        tagName: "header",

        events: {
            "click #view-original": "viewOriginal",
            "click #view-draft": "viewDraft",
            "click #discard-draft": "discardDraft",
            "focus .editable-field .text": "setFocus",
            "blur .editable-field .text, .editable-field [tabindex]": "handleBlur",
            "keydown .editable-field .text": "submitOnEnterAndCancelOnEscape",
            "click .editable-field .overlay-icon": "focusOnField",
            "click .editable-field .submit": "submit",
            "click .editable-field .cancel": "cancel"
        },

        initialize: function (options) {
            this.workflows = options.workflows;
            this.issueTypes = options.issueTypes;
            this.bus = options.bus;
            this.model.on('change', this.render, this);
            this.workflows.on('add remove', this.updateAddButtonState, this);
            this.viewingOriginal = false;
        },

        template: function (data) {
            return TemplatesEditor.WorkflowSchemeHeaderView(data);
        },

        render: function () {
            var params = this.model.toJSON();
            params.viewingOriginal = this.viewingOriginal;
            this.$el.html(this.template(params));
            this.updateAddButtonState();

            _.defer(_.bind(function () {
                this.$("textarea").expandOnInput();
            }, this));

            // remove the inline dialog if it already exists since we are recreating it
            jQuery("#inline-dialog-project-config-workflow-scheme-shared").remove();

            // IE 9 and before does not support the "placeholder" attribute
            jQuery.browser.msie && +jQuery.browser.version <= 9 && jQuery.fn.ieImitationPlaceholder && this.$el.ieImitationPlaceholder();

            // the actual add-workflow link is in an inline layer that is moved the end of the body so we cannot use backbone's automatic event delegation
            this.$("#add-workflow").click(_.bind(this.addWorkflow, this));
            Events.trigger(Types.NEW_CONTENT_ADDED, [this.$el, Reasons.workflowHeaderReady]);
            return this;
        },

        updateAddButtonState: function () {
            var $dropdownTrigger = this.$("#add-workflow-dropdown-trigger");
            var $addButton = this.$("#add-workflow");

            if (this.viewingOriginal) {
                $dropdownTrigger.attr('title', AJS.I18n.getText('admin.project.workflows.cannot.modify.original'));
            } else {
                var canAddWorkflow = this.canAddWorkflow();
                if (canAddWorkflow) {
                    $addButton.removeAttr('title');
                } else {
                    $addButton.attr('title', AJS.I18n.getText('admin.project.workflows.no.workflows'));
                }
                $dropdownTrigger.removeAttr('title');
                $addButton.toggleClass("disabled", !canAddWorkflow).parent().toggleClass("disabled", !canAddWorkflow);
            }
            $dropdownTrigger.prop("disabled", this.viewingOriginal).toggleClass("disabled", this.viewingOriginal);
        },

        canAddWorkflow: function () {
            return this.workflows.length < this.model.get('totalWorkflows');
        },

        addWorkflow: function (e) {
            e.preventDefault();
            if (!jQuery(e.target).parent(".disabled").length) {
                new AddWorkflowDialogView({
                    assignedWorkflows: this.model.workflows.pluck("name")
                }).render().on("assignIssueTypes", _.bind(function (e) {
                    this.assignAfterAdd(jQuery(e.target).data('workflow'));
                }, this));
            }
        },

        assignAfterAdd: function (workflow) {
            var defaultWorkflow = this.model.workflows.find(function (workflow) {
                return workflow.get('default');
            });
            AssignIssueTypes({
                workflows: this.workflows.toJSON(),
                issueTypes: this.issueTypes.toJSON(),
                workflowName: workflow.name,
                workflowDisplayName: workflow.displayName,
                draftScheme: true,
                defaultWorkflow: defaultWorkflow.toJSON(),
                showDefault: true,
                callback: _.bind(function (attr, callback) {
                    attr = _.extend({}, workflow, {
                        issueTypes: [],
                        'default': false
                    }, attr);
                    this.bus.trigger('addWorkflow', attr);
                    callback();
                }, this)
            });
        },

        viewOriginal: function (e) {
            e.preventDefault();
            this.bus.trigger('viewOriginal');
            this.viewingOriginal = true;
            this.render();
        },

        viewDraft: function (e) {
            e.preventDefault();
            this.bus.trigger('viewDraft');
            this.viewingOriginal = false;
            this.render();
        },

        discardDraft: function () {
            var bus = this.bus;
            DiscardDraft(function (callback) {
                bus.trigger('discardDraft');
                callback();
            });
        },

        finalize: function () {
            this.model.off(null, null, this);
        },

        submitOnEnterAndCancelOnEscape: function (e) {
            var $target = jQuery(e.target);
            if (e.which === jQuery.ui.keyCode.ENTER) {
                e.preventDefault();
                $target.blur();
            } else if (e.which === jQuery.ui.keyCode.ESCAPE) {
                e.preventDefault();
                $target.val(this.model.get($target.data("property")));
                $target.blur();
            }
        },

        setFocus: function (e) {
            var $target = jQuery(e.target);
            $target.closest(".editable-field").removeClass("inactive").addClass("active");
            _.defer(function () {
                $target.select();
            });
        },

        handleBlur: function (e) {
            _.defer(_.bind(function () {
                if (!hasFocus(jQuery(e.target).closest(".editable-field"))) {
                    // IE8 fix
                    e.preventDefault = jQuery.noop;
                    this.submit(e);
                }
            }, this));
        },

        submit: function (e) {
            e.preventDefault();
            var $target = jQuery(e.target).closest(".editable-field").find(".text");
            var placeholder = $target.attr("placeholder");
            var oldValue = this.model.get($target.data("property"));
            var newValue = jQuery.trim($target.val());

            if (newValue === placeholder) {
                newValue = "";
            }

            if (!placeholder && !newValue.length) {
                $target.val(oldValue);
            } else if (newValue !== oldValue) {
                this.bus.trigger("updateProperty", $target.data("property"), newValue);
            }

            if (newValue) {
                $target.val(newValue);
            }
            this.onActualBlur($target);
        },

        focusOnField: function (e) {
            jQuery(e.target).closest(".editable-field").find(".text").focus();
        },

        cancel: function (e) {
            e.preventDefault();
            var $target = jQuery(e.target).closest(".editable-field").find(".text");
            $target.val(this.model.get($target.data("property"))).blur();
            this.onActualBlur($target);
        },

        onActualBlur: function ($target) {
            $target.closest(".editable-field").removeClass("active").addClass("inactive");
            if ($target.attr("title")) {
                $target.attr("title", $target.val());
            }
            if ($target.is("textarea")) {
                $target.height("");
                $target.expandOnInput();
            }
        }

    });
});
