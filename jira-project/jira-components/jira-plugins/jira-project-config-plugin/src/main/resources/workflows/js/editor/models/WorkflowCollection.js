define("jira-project-config/workflows/scheme/editor/models/workflow-collection", ["require"], function(require){
    "use strict";

    var _ = require('underscore');
    var Backbone = require('jira-project-config/backbone');
    var WorkflowModel = require('jira-project-config/workflows/scheme/editor/models/workflow');

    var equalsIgnoreOrder = function (a, b) {
        return _.isEqual(_.clone(a).sort(), _.clone(b).sort());
    };

    return Backbone.Collection.extend({

        model: WorkflowModel,

        equalsArray: function (mappings) {
            return equalsIgnoreOrder(_.pluck(mappings, 'name'), this.pluck('name')) && _.all(mappings, function (mapping) {
                    var current = this.get(mapping.name);
                    return current && current.equalsObject(mapping);
                }, this);
        },

        load: function (mappings) {
            if (!this.equalsArray(mappings)) {
                this.reset(mappings);
                return true;
            } else {
                return false;
            }
        }
    });
});
