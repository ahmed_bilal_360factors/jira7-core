define("jira-project-config/workflows/scheme/editor/models/issue-type", ["require"], function(require){
    "use strict";

    var Backbone = require('jira-project-config/backbone');

    return Backbone.Model;
});
