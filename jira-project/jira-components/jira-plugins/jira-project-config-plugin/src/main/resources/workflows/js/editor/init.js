require([
    'jira-project-config/workflows/scheme/editor/controller',
    'jira-project-config/workflows/scheme/editor/views/page',
    'jira-project-config/workflows/scheme/editor/models/workflow-scheme',
    'jira-project-config/workflows/scheme/editor/models/issue-type-collection',
    'jira-project-config/backbone',
    "jira/libs/parse-uri",
    "wrm/context-path",
    'underscore',
    'jquery'
], function(
    Controller,
    Page,
    WorkflowSchemeModel,
    IssueTypeCollection,
    Backbone,
    parseUri,
    contextPath,
    _,
    jQuery) {

    jQuery(function () {
        var workflowName = JIRA.Workflows.Scheme.Common.checkWorkflow();
        if (!workflowName) {
            JIRA.Workflows.Scheme.Common.checkMigrationSuccess();
        }

        new Controller({
            url: contextPath() + '/rest/globalconfig/latest/workflowschemeeditor/',
            container: "#workflowscheme-editor",
            schemeId: +parseUri(location).queryKey.schemeId,
            pageView: Page,
            model: WorkflowSchemeModel,
            issueTypeModel: IssueTypeCollection,
            viewEventBus: _.clone(Backbone.Events)
        }).load(function () {
            if (workflowName) {
                this.page.workflowSchemeView.headerView.assignAfterAdd({
                    name: workflowName,
                    displayName: workflowName
                });
            }
        });
    });

});
