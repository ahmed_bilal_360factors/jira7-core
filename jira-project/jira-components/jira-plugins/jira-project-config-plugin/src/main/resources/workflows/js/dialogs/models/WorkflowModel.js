define("jira-project-config/workflows/dialogs/models/workflow", ["require"], function(require){
    "use strict";

    var Backbone = require('jira-project-config/backbone');

    return Backbone.Model.extend({
        defaults: {
            name: "",
            description: "",
            lastModifiedDate: "",
            lastModifiedUser: "",
            steps: 0
        },
        idAttribute: 'name'
    });
});
