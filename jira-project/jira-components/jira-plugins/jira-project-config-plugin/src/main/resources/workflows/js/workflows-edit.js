(function ($) {
    function initInitialWorkflowSchemeLinks($ctx) {
        $ctx.on('click', ".project-config-workflow-default a.project-config-workflow-edit, .workflow-migrate-target", function (e) {
            e.preventDefault();
            var href = $(this).attr("href");
            var dialog = new JIRA.FormDialog({
                id: "wait-migrate-dialog",
                content: function (callback) {
                    callback(JIRA.Templates.Workflows.EditDefault.defaultSchemeDialog());
                },
                submitHandler: function (e, callback) {
                    e.preventDefault();
                    copyWorkflow(href, dialog);
                    callback();
                }
            });
            dialog.addHeading(AJS.I18n.getText("admin.project.workflow.scheme.migration.prompt.heading"));
            dialog.show();
        });
    }

    function copyWorkflow(href, dialog) {
        JIRA.SmartAjax.makeWebSudoRequest({
            url: contextPath + "/rest/projectconfig/latest/workflow",
            type: 'POST',
            data: JIRA.ProjectConfig.getId(),
            contentType: 'application/json'
        }, {
            cancel: function (e) {
                //Don't redirect on websudo cancel. We can display stuff as non-admin.
                e.preventDefault();
            }
        }).done(function (data) {
            var taskId = data.taskId;
            var workflowName = data.workflowName;
            if (taskId) {
                //we have to do this here to ensure that the dialog has content so we can change the elements.
                dialog.show();
                dialog.handleCancel = function () {
                };
                $("#wait-migrate-dialog .buttons").css('visibility', 'hidden');
                dialog.addHeading(AJS.I18n.getText("admin.project.workflow.scheme.migration.wait.heading"));
                $("#wait-migrate-dialog .form-body").html(JIRA.Templates.Workflows.EditDefault.defaultSchemeDialogProgress());
                $("#progress-bar-container").progressBar(0, {
                    height: "20px",
                    showPercentage: true
                });
                checkIfFinished(taskId, href, workflowName);
            } else {
                editWorkflow(href, workflowName);
            }
        }).fail(JIRA.Workflows.Scheme.Common.smartAjaxErrorHandler(JIRA.Workflows.Scheme.Common.errorDialog));
    }

    function checkIfFinished(taskId, successHref, workflowName) {
        JIRA.SmartAjax.makeRequest({
            url: contextPath + "/rest/projectconfig/latest/migrationStatus/" + taskId,
            type: 'GET',
            contentType: 'application/json',
            success: function (taskStatus) {
                if (taskStatus.finished) {
                    $("#progress-bar-container").progressBar(100);

                    if (!taskStatus.successful || taskStatus.numberOfFailedIssues > 0) {
                        var params = $.param({
                            projectId: JIRA.ProjectConfig.getId(),
                            taskId: taskId
                        });
                        AJS.reloadViaWindowLocation(contextPath + "/secure/project/SelectProjectWorkflowSchemeStep3.jspa?" + params);
                    } else {
                        deleteTask(taskId, function () {
                            editWorkflow(successHref, workflowName);
                        });
                    }
                } else {
                    var progress = taskStatus.progress;
                    $("#progress-bar-container").progressBar(progress);
                    setTimeout(function () {
                        checkIfFinished(taskId, successHref, workflowName);
                    }, 1000);
                }
            },
            error: function (a, b, c, response) {
                deleteTask(taskId);
                JIRA.Workflows.Scheme.Common.errorDialog(JIRA.SmartAjax.buildSimpleErrorContent(response));
            }
        });
    }

    function deleteTask(taskId, callback) {
        JIRA.SmartAjax.makeRequest({
            url: contextPath + "/rest/projectconfig/latest/migrationStatus/" + taskId,
            type: 'DELETE',
            contentType: 'application/json',
            complete: callback
        });
    }

    function editWorkflow(href, workflow) {
        href = href.replace(/wfName=[^&]*/, "wfName=" + encodeURIComponent(workflow));
        AJS.reloadViaWindowLocation(href);
    }

    JIRA.bind(JIRA.Events.NEW_CONTENT_ADDED, function (e, $ctx) {
        initInitialWorkflowSchemeLinks($ctx);
    });
})(AJS.$);
