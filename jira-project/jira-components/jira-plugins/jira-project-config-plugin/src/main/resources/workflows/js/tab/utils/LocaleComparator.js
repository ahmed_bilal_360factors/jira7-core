define("jira-project-config/workflows/tab/utils/locale-comparator", function () {
    "use strict";

    return function localeComparator(attr) {
        return function (a, b) {
            return a.get(attr).toLocaleLowerCase().localeCompare(b.get(attr).toLocaleLowerCase());
        };
    };
});
