define("jira-project-config/workflows/tab/view/workflow-schema-draft-message", ["require"], function(require){
    "use strict";

    var jQuery = require('jquery');
    var wrmContextPath = require('wrm/context-path');
    var Backbone = require('jira-project-config/workflows/tab/lib/backbone');
    var DiscardDraft = require('jira-project-config/workflows/dialogs/discard-draft');
    var Dimmer = require('jira-project-config/workflows/tab/utils/dimmer');

    return Backbone.View.extend({
        events: {
            "click #project-config-workflows-view-original": "viewOriginal",
            "click #project-config-workflows-view-draft": "viewDraft",
            "click #project-config-workflows-discard-draft": "discardDraft",
            "click #project-config-workflows-publish-draft": "publishDraft"
        },
        viewOriginal: function (e) {
            e.preventDefault();
            Dimmer.dim();
            this.model.viewOriginal({
                success: function () {
                    Dimmer.undim();
                }
            });
        },
        viewDraft: function (e) {
            e.preventDefault();
            Dimmer.dim();
            this.model.viewDraft({
                success: function () {
                    Dimmer.undim();
                }
            });
        },
        discardDraft: function () {
            var model = this.model;
            DiscardDraft(function (callback) {
                model.discardDraft({
                    success: function () {
                        callback();
                        Dimmer.undim();
                    }
                });
            });
        },
        publishDraft: function () {
            location.href = wrmContextPath() + "/secure/project/SelectProjectWorkflowSchemeStep2!default.jspa?" + jQuery.param({
                    draftMigration: true,
                    projectId: this.model.get("projectId"),
                    schemeId: this.model.id
                });
        }
    });
});
