define("jira-project-config/workflows/tab/view/workflow-scheme", ["require"], function(require){
    "use strict";

    var jQuery = require('jquery');
    var Backbone = require('jira-project-config/workflows/tab/lib/backbone');
    var AssignIssueTypes = require('jira-project-config/workflows/dialogs/assign-issue-types');
    var WorkflowSchemeHeaderView = require('jira-project-config/workflows/tab/view/workflow-scheme-header');
    var WorkflowSchemeListView = require('jira-project-config/workflows/tab/view/workflow-scheme-list');
    var Dimmer = require('jira-project-config/workflows/tab/utils/dimmer');

    return Backbone.View.extend({
        initialize: function () {
            this.collection = this.model.get('mappings');
            this.model.on("change:draftScheme", this.toggleMode, this);
        },
        render: function () {
            this.header = new WorkflowSchemeHeaderView({
                model: this.model
            });

            this.list = new WorkflowSchemeListView({
                model: this.model
            });

            this.$el.append(this.header.render().el);
            this.$el.append(this.list.render().el);

            this.list.on("assignIssueTypes", this.assignIssueTypes, this);
            this.list.on("removeWorkflow", this.removeWorkflow, this);
            this.header.on("assignIssueTypes", this.assignIssueTypes, this);

            this.toggleMode();

            return this;
        },
        toggleMode: function () {
            var draftScheme = this.model.get("draftScheme");
            jQuery(".project-config-workflows-draft-message")
                .toggleClass("draft-view", draftScheme)
                .toggleClass("original-view", !draftScheme)
                .toggleClass("no-draft", !(draftScheme || this.model.get("hasDraft")));
        },
        assignIssueTypes: function (e) {
            var $target = jQuery(e.target);
            var workflow = $target.data('workflow');
            this.showAssignIssueTypes(workflow);
        },
        showAssignIssueTypes: function (workflow) {
            var that = this;

            AssignIssueTypes({
                workflowName: workflow.name,
                workflowDisplayName: workflow.displayName,
                workflows: this.model.get('mappings').toJSON(),
                issueTypes: this.model.get('issueTypes').toJSON(),
                draftScheme: this.model.get('draftScheme'),
                callback: function () {
                    that.actuallyAssignIssueTypes.apply(that, arguments);
                },
                createDraft: function () {
                    that.model.createDraft.apply(that.model, arguments);
                }
            });
        },
        actuallyAssignIssueTypes: function (attr, callback) {
            var workflows = this.collection;
            var options = {
                wait: true,
                success: function (model, resp) {
                    workflows.trigger("sync", model, resp);
                    callback && callback();
                    Dimmer.undim();
                }
            };
            var workflow = workflows.get(attr.name);
            if (workflow) {
                workflow.save(attr, options);
                JIRA.Analytics.sendProjectWorkflowSchemeEvent('assignissuetypestodraft');
            } else {
                this.collection.create(attr, options);
                JIRA.Analytics.sendProjectWorkflowSchemeEvent('addworkflowtodraft');
            }
        },
        removeWorkflow: function (e) {
            var workflowName = jQuery(e.currentTarget).data('workflowname');
            Dimmer.dim();
            if (this.model.get('draftScheme')) {
                this.actuallyRemoveWorkflow(workflowName);
            } else {
                var view = this;
                this.model.createDraft({
                    success: function () {
                        view.actuallyRemoveWorkflow(workflowName);
                    }
                });
            }
        },
        actuallyRemoveWorkflow: function (workflowName) {
            var workflows = this.collection;
            var workflow = workflows.get(workflowName);
            workflow.destroy({
                contentType: 'application/json',
                data: workflow.get('name'),
                success: function (model, resp) {
                    JIRA.Analytics.sendProjectWorkflowSchemeEvent('deleteworkflowfromdraft');
                    workflows.trigger("sync", model, resp);
                    Dimmer.undim();
                },
                wait: true
            });
        }
    });
});
