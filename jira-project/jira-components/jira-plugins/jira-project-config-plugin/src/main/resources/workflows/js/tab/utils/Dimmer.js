define("jira-project-config/workflows/tab/utils/dimmer", ["require"], function(require){
    "use strict";

    var jQuery = require('jquery');
    var Loading = require('jira/loading/loading');
    var AJSHelper = require('jira-project-config/libs/ajshelper');

    var onUndimHandler;
    var dimTimeout;

    function dim() {
        dimTimeout = setTimeout(function () {
            dimTimeout = undefined;
            AJSHelper.dim();
            Loading.showLoadingIndicator();
        }, 800);

        jQuery(AJS).one('Dialog.show', function () {
            clearTimeout(dimTimeout);
            dimTimeout = undefined;
        });
    }

    function onUndim(callback) {
        onUndimHandler = callback;
    }

    function undim() {
        if (typeof onUndimHandler === 'function') {
            onUndimHandler();
            onUndimHandler = undefined;
        }
        if (dimTimeout) {
            clearTimeout(dimTimeout);
            dimTimeout = undefined;
        } else {
            AJSHelper.undim();
            Loading.hideLoadingIndicator();
        }
    }

    return {
        dim: dim,
        onUndim: onUndim,
        undim: undim
    };
});
