define("jira-project-config/workflows/tab/model/ignite-workflows", ["require"], function(require){
    "use strict";

    var _ = require('underscore');
    var Backbone = require('jira-project-config/workflows/tab/lib/backbone');
    var IgniteWorkflowModel = require('jira-project-config/workflows/tab/model/ignite-workflow');
    var localeComparator = require('jira-project-config/workflows/tab/utils/locale-comparator');

    return Backbone.Collection.extend({
        model: IgniteWorkflowModel,
        update: function (newCollection) {
            var oldDefaultWorkflow = this.find(function (model) {
                return model.get('default');
            });

            var oldDefaultName;
            if (oldDefaultWorkflow) {
                oldDefaultName = oldDefaultWorkflow.get('name');
            }

            //remove from our collection all the workflows that are not present in the new collection
            this.remove(_.chain(this.pluck('name')).difference(newCollection.pluck('name')).value());

            //move from the new collection to our collection all the workflows that are not present in our collection
            var newModelNames = _.chain(newCollection.pluck('name')).difference(this.pluck('name'));
            var newModels = newCollection.filter(function (model) {
                return newModelNames.include(model.get('name')).value();
            });
            newCollection.remove(newModels);

            //set the default flag to false for all models in the current collection. this is so that if there is a new
            //default in the new collection, the final collection will be sorted correctly. if not, this flag will get
            //reset correctly in the update part below.
            this.each(function (model) {
                model.set('default', false);
            });

            //update the workflows in our collection with data from the new collection
            newCollection.each(function (workflowModel) {
                var newName = workflowModel.get('name');
                var current = this.get(newName);
                current.set(workflowModel.toJSON());
            }, this);

            this.add(newModels);

            if (oldDefaultName) {
                // If the previously default workflow is still present in the collection, then check if it is still the default.
                // If it's not then reorder the workflow views accordingly.
                var oldDefaultIsStillPresent = this.any(function (model) {
                    return model.get('name') === oldDefaultName;
                });

                if (oldDefaultIsStillPresent) {
                    var newDefaultWorkflow = this.find(function (model) {
                        return model.get('default');
                    });

                    if (newDefaultWorkflow && oldDefaultName !== newDefaultWorkflow.get('name')) {
                        this.sort();
                        this.trigger('reorder');
                    }
                }
            }
        },
        comparator: function (a, b) {
            if (a.get('default')) {
                return -1;
            } else if (b.get('default')) {
                return 1;
            } else {
                return localeComparator('name')(a, b);
            }
        }
    });
});
