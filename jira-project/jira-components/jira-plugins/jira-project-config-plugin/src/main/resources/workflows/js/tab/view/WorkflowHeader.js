define("jira-project-config/workflows/tab/view/workflow-header", ["require"], function(require){
    "use strict";

    var Backbone = require('jira-project-config/workflows/tab/lib/backbone');
    var TemplatesProjectTab = require('jira-project-config/workflows/tab/templates');

    return Backbone.View.extend({
        className: "project-config-workflow-header",
        tagName: "thead",

        template: function () {
            return TemplatesProjectTab.workflowHeader({admin: this.options.admin});
        },
        render: function () {
            this.$el.html(this.template());
            return this;
        }

    });
});
