define("jira-project-config/workflows/dialogs/models/workflows", ["require"], function(require){
    "use strict";

    var WorkflowModel = require("jira-project-config/workflows/dialogs/models/workflow");
    var Backbone = require('jira-project-config/backbone');
    var wrmContextPath = require("wrm/context-path");
    var _ = require("underscore");

    function setDisplayName(workflows, propertyName) {
        var defaultDisplayName = AJS.I18n.getText("common.concepts.default.workflow");
        var existingWithDefaultName = _.find(workflows, function (workflow) {
            return workflow.name === defaultDisplayName;
        });
        var containsExistingWithDefaultName = existingWithDefaultName !== undefined;

        _.each(workflows, function (workflow) {
            var displayName = workflow.name;
            if (!containsExistingWithDefaultName) {
                var isDefault = workflow[propertyName];
                if (isDefault) {
                    displayName = defaultDisplayName;
                }
            }
            workflow.displayName = displayName;
        });
    }

    return Backbone.Collection.extend({
        model: WorkflowModel,
        url: wrmContextPath() + "/rest/api/latest/workflow",
        parse: function (response) {
            setDisplayName(response, "default");
            return response;
        }
    });
});
