define("jira-project-config/workflows/dialogs/add-workflow-dialog-view", ["require"], function(require){
    "use strict";

    var jQuery = require('jquery');
    var FormDialog = require('jira/dialog/form-dialog');
    var Backbone = require('jira-project-config/backbone');
    var TemplatesDialogs = require('jira-project-config/workflows/scheme/dialogs/templates');
    var WorkflowCollection = require('jira-project-config/workflows/dialogs/models/workflows');
    var AddWorkflowDialogWorkflowView = require('jira-project-config/workflows/dialogs/add-workflow-dialog-workflow-view');

    return Backbone.View.extend({
        events: {
            "click .project-config-workflow-add-dialog-list li": "viewWorkflow",
            "click #add-workflow-next": "assignIssueTypes"
        },
        initialize: function () {
            this.collection = new WorkflowCollection();
        },
        template: function (data) {
            return TemplatesDialogs.add(data);
        },
        render: function () {
            var that = this;
            var dialog = new FormDialog({
                id: "add-workflow-dialog",
                widthClass: "large",
                stacked: true,
                content: function (ready) {
                    that.collection.fetch({
                        success: function (collection) {
                            ready(that.template({
                                workflows: collection.remove(that.options.assignedWorkflows).toJSON()
                            }));

                            that.setElement("#add-workflow-dialog");
                            dialog.get$popupContent().find(".project-config-workflow-add-dialog-list li").first().click();
                        }
                    });
                }
            });
            dialog.show();

            return this;
        },
        viewWorkflow: function (e) {
            var $this = jQuery(e.target);
            var workflowName = $this.data("workflowname");
            var workflowModel = this.collection.get(workflowName);

            jQuery("#add-workflow-next").data("workflow", workflowModel.toJSON());

            if (!$this.hasClass("selected")) {
                jQuery(".selected").removeClass("selected");
                $this.addClass("selected");
                new AddWorkflowDialogWorkflowView({
                    el: "#project-config-workflow-add-dialog-preview",
                    model: workflowModel
                }).render();
            }
        },
        assignIssueTypes: function (e) {
            e.preventDefault();
            this.trigger("assignIssueTypes", e);
        }
    });
});
