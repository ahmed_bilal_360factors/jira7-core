define("jira-project-config/workflows/tab/view/workflow", ["require"], function(require){
    "use strict";

    var jQuery = require('jquery');
    var Reasons = require('jira/util/events/reasons');
    var Types = require('jira/util/events/types');
    var Events = require('jira/util/events');
    var Backbone = require('jira-project-config/workflows/tab/lib/backbone');
    var TemplatesProjectTab = require('jira-project-config/workflows/tab/templates');
    var ProjectConfigUtils = require('jira-project-config/utils');
    var IssueTypesListView = require('jira-project-config/workflows/tab/view/issue-types');
    var ActiveCallbacks = require('jira-project-config/workflows/tab/utils/active-callbacks');

    return Backbone.View.extend({
        className: "project-config-workflow-row",
        tagName: "tr",
        events: {
            'click .project-config-workflow-text-link': 'viewText',
            'click .project-config-workflow-assign-issue-types-link': 'assignIssueTypes',
            'click .project-config-workflow-remove': 'removeWorkflow'
        },
        initialize: function () {
            this.model.collection.on('changeDefault', this.changeDefault, this);
            this.model.on('change:system', this.updateEdit, this);
            this.model.on('remove', this.unrender, this);
        },
        template: function (data) {
            return TemplatesProjectTab.workflow(data);
        },
        render: function () {
            this.$el.html(this.template({
                workflow: this.model.toJSON(),
                admin: this.options.admin,
                sharedByKey: this.getPopupTriggerId(),
                defaultScheme: this.options.defaultScheme
            }));

            var issueTypesList = new IssueTypesListView({
                model: this.model,
                issueTypes: this.options.issueTypes,
                admin: this.options.admin
            });

            this.$(".project-config-workflow-issue-types-heading").append(issueTypesList.render().el);

            this.updateEdit();

            Events.trigger(Types.NEW_CONTENT_ADDED, [this.$el, Reasons.workflowReady]);

            return this;
        },
        getPopupTriggerId: function () {
            return this.model.get('name').replace(/\W/g, '') + '-' + new Date().getTime();
        },
        unrender: function () {
            var view = this;

            //remove edit button/read-only lozenge before fading out
            this.$(".project-config-workflow-edit-placeholder").remove();

            ActiveCallbacks.increment();
            this.$el.fadeTo("slow", 0, function () {
                jQuery(this).slideUp(function () {
                    jQuery(this).remove();
                    view.trigger("workflowRemoved");
                    ActiveCallbacks.decrement();
                });
            });
        },
        assignIssueTypes: function (e) {
            e.preventDefault();
            jQuery(e.target).data('workflow', this.model.toJSON());
            this.trigger("assignIssueTypes", e);
        },
        viewText: function (e) {
            e.preventDefault();
            JIRA.Workflows.Scheme.Common.showWorkflowTextView({
                projectKey: ProjectConfigUtils.getKey(),
                workflowName: jQuery(e.target).data("workflowname")
            });
        },
        removeWorkflow: function (e) {
            e.preventDefault();
            this.trigger("removeWorkflow", e);
        },
        updateEdit: function () {
            this.$(".project-config-workflow-edit-placeholder").html(TemplatesProjectTab.editPlaceholder({
                workflow: this.model.toJSON(),
                projectId: ProjectConfigUtils.getId(),
                defaultScheme: this.options.defaultScheme,
                /*jshint camelcase: false */
                atl_token: encodeURIComponent(atl_token())
                /*jshint camelcase: true */
            }));
        },
        changeDefault: function (value) {
            this.options.defaultScheme = value;
            this.updateEdit();
        }
    });
});
