define("jira-project-config/workflows/dialogs/assign-issue-types", ["require"], function(require){
    "use strict";

    var jQuery = require('jquery');
    var _ = require('underscore');
    var FormDialog = require('jira/dialog/form-dialog');
    var TemplatesDialogs = require('jira-project-config/workflows/scheme/dialogs/templates');

    function hide(dialog) {
        return function (e) {
            e && e.preventDefault();
            var prev = dialog.prev;
            dialog.hide();
            prev && prev.hide();
        };
    }

    return function (options) {
        new FormDialog({
            id: "assign-issue-types-dialog",
            widthClass: "medium",
            content: function (ready) {
                var dialog = this;

                var issueTypeToWorkflow = {};
                var workflowCount = {};
                _.each(options.workflows, function (workflow) {
                    _.each(workflow.issueTypes, function (issueType) {
                        issueTypeToWorkflow[issueType] = workflow.displayName;
                    });
                    workflowCount[workflow.displayName] = workflow.issueTypes.length;
                });
                if (options.showDefault && options.defaultWorkflow) {
                    issueTypeToWorkflow['0'] = options.defaultWorkflow.displayName;
                    workflowCount[options.defaultWorkflow.displayName]++;
                }

                ready(TemplatesDialogs.assign({
                    workflow: options.workflowName,
                    workflowDisplayName: options.workflowDisplayName,
                    issueTypes: options.issueTypes,
                    issueTypeToWorkflow: issueTypeToWorkflow,
                    hasPrev: !!dialog.prev,
                    showDefault: !!options.showDefault
                }));

                var $content = dialog.get$popupContent();

                var allWorkflowWarningIcons = {};
                _.each(issueTypeToWorkflow, function (workflow, issueType) {
                    var workflowWarningIcons = allWorkflowWarningIcons[workflow];
                    if (!workflowWarningIcons) {
                        workflowWarningIcons = jQuery([]);
                        allWorkflowWarningIcons[workflow] = workflowWarningIcons;
                    }
                    var icon = $content.find('#project-config-workflows-issue-type-warning-icon-' + issueType);
                    workflowWarningIcons.push(icon);
                });

                if (dialog.prev) {
                    dialog.$buttonContainer.find('a.cancel').off('click').click(hide(dialog));
                }

                var $selectAll = $content.find("#project-config-select-all-issue-types");
                var $submit = $content.find("#assign-issue-types-submit");
                var $issueTypes = $content.find(".project-config-workflows-assign-issue-types-row");

                var workflowsToBeRemoved = {};

                function calculateWorkflowsToBeRemoved() {
                    var $messageBar = $content.find("#project-config-workflows-assign-issue-types-warning");
                    var showWarning = _.size(workflowsToBeRemoved);
                    $messageBar.toggleClass("hidden", !showWarning);

                    _.each(options.workflows, function (wf) {
                        var workflowDisplayName = wf.displayName;

                        var icons = allWorkflowWarningIcons[workflowDisplayName];
                        var showIcons = workflowsToBeRemoved[workflowDisplayName];

                        icons.each(function (index, icon) {
                            icon.toggleClass("invisible", !showIcons);
                        });
                    });
                }

                function setIndeterminate($el, value) {
                    $el.prop("indeterminate", !!value);
                    if (value) {
                        $selectAll.addClass("project-config-indeterminate");
                    } else {
                        $selectAll.removeClass("project-config-indeterminate");
                    }
                }

                $issueTypes.click(function (e) {
                    var $this = jQuery(this);

                    $this.toggleClass('selected');
                    if (e.target.type !== 'checkbox') {
                        var $checkbox = $this.find(":checkbox");
                        $checkbox.prop('checked', !$checkbox.prop("checked"));
                    }

                    var noIssueTypesSelected = !$issueTypes.is(".selected");
                    if (noIssueTypesSelected) {
                        setIndeterminate($selectAll.prop("checked", false), false);
                    } else if ($issueTypes.is(":not(.selected)")) {
                        setIndeterminate($selectAll.prop("checked", false), true);
                    } else {
                        setIndeterminate($selectAll.prop("checked", true), false);
                    }

                    var workflow = $this.find('.project-config-workflows-currently-assigned-name-text').text();
                    if ($this.hasClass('selected') ? --workflowCount[workflow] : ++workflowCount[workflow]) {
                        delete workflowsToBeRemoved[workflow];
                    } else {
                        workflowsToBeRemoved[workflow] = true;
                    }

                    $submit.prop("disabled", noIssueTypesSelected);
                    calculateWorkflowsToBeRemoved();
                });

                $selectAll.click(function () {
                    $selectAll.removeClass("project-config-indeterminate");
                    if (this.checked) {
                        $issueTypes.not(".selected").each(function () {
                            var workflow = jQuery(this).find('.project-config-workflows-currently-assigned-name-text').text();
                            --workflowCount[workflow];
                            workflowsToBeRemoved[workflow] = true;
                        });
                    } else {
                        $issueTypes.filter(".selected").each(function () {
                            var workflow = jQuery(this).find('.project-config-workflows-currently-assigned-name-text').text();
                            ++workflowCount[workflow];
                            delete workflowsToBeRemoved[workflow];
                        });
                    }

                    $issueTypes.toggleClass("selected", this.checked).find(":checkbox").prop('checked', this.checked);
                    $submit.prop("disabled", !this.checked);
                    calculateWorkflowsToBeRemoved();
                }).mouseup(function () {
                    this.indeterminate && (this.checked = this.indeterminate = false);
                });
            },
            submitHandler: function (e, callback) {
                e.preventDefault();

                var dialog = this;
                var newCallback = function () {
                    callback();
                    hide(dialog)();
                };

                var attr = jQuery(e.target).serializeObject();
                attr.issueTypes && !jQuery.isArray(attr.issueTypes) && (attr.issueTypes = [attr.issueTypes]);
                attr['default'] = !!attr['default'];

                if (options.draftScheme) {
                    options.callback(attr, newCallback);
                } else {
                    options.createDraft({
                        success: function () {
                            options.callback(attr, newCallback);
                        }
                    });
                }
            }
        }).show();
    };
});
