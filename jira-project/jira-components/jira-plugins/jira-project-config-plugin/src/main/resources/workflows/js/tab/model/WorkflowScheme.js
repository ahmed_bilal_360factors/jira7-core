define("jira-project-config/workflows/tab/model/workflow-scheme", ["require"], function(require){
    "use strict";

    var wrmContextPath = require('wrm/context-path');
    var _ = require('underscore');
    var Messages = require('jira/message');
    var Events = require('jira/util/events');
    var Backbone = require('jira-project-config/workflows/tab/lib/backbone');
    var ProjectConfigUtils = require('jira-project-config/utils');
    var IssueTypeCollection = require('jira-project-config/workflows/tab/model/issue-types');
    var IgniteWorkflowCollection = require('jira-project-config/workflows/tab/model/ignite-workflows');

    function setDisplayName(workflows, propertyName) {
        var defaultDisplayName = AJS.I18n.getText("common.concepts.default.workflow");
        var existingWithDefaultName = _.find(workflows, function (workflow) {
            return workflow.name === defaultDisplayName;
        });
        var containsExistingWithDefaultName = existingWithDefaultName !== undefined;

        _.each(workflows, function (workflow) {
            var displayName = workflow.name;
            if (!containsExistingWithDefaultName) {
                var isDefault = workflow[propertyName];
                if (isDefault) {
                    displayName = defaultDisplayName;
                }
            }
            workflow.displayName = displayName;
        });
    }

    return Backbone.Model.extend({
        defaults: {
            defaultScheme: false,
            draftScheme: false,
            issueTypes: null,
            lastModifiedDate: "",
            lastModifiedUser: {},
            name: "",
            shared: {},
            totalWorkflows: 0,
            workflows: null
        },
        url: function () {
            return wrmContextPath() + "/rest/projectconfig/latest/workflowscheme/" + ProjectConfigUtils.getKey();
        },
        parse: function (response) {
            var attributes = _.extend({}, this.defaults, response);
            attributes.mappings = new IgniteWorkflowCollection();
            attributes.issueTypes = new IssueTypeCollection();
            var workflows = response.mappings;
            setDisplayName(workflows, "jiraDefault");
            attributes.mappings.add(workflows);
            attributes.issueTypes.add(_.values(response.issueTypes));

            attributes.mappings.on("sync", function (model, resp, options) {
                options = options || {};
                delete options.success;
                this.update(options)(resp);
            }, this);

            return attributes;
        },
        update: function (options) {
            options = options ? _.clone(options) : {};
            var model = this;
            var success = options.success;
            return function (resp, status, xhr) {
                var json = model.parse(resp, xhr);

                var oldWorkflow = model.get('mappings');
                if (oldWorkflow) {
                    oldWorkflow.update(json.mappings);
                    oldWorkflow.sort();
                    json.mappings = oldWorkflow;
                }

                var oldIssueTypes = model.get('issueTypes');
                if (oldIssueTypes) {
                    oldIssueTypes.update(json.issueTypes);
                    oldIssueTypes.sort();
                    json.issueTypes = oldIssueTypes;
                }

                if (!model.set(json, options)) {
                    return false;
                }
                success && success(model, resp);
            };
        },
        editDraft: function (options, method) {
            options = options ? _.clone(options) : {};
            if (method !== "create") {
                options.success = this.update(options);
            }
            return this.sync(method, this, options);
        },
        createDraft: function (options) {
            Events.trigger("draftcreated");
            return this.editDraft(options, 'create');
        },
        discardDraft: function (options) {
            options = options ? _.clone(options) : {};
            this.unset('hasDraft');
            var success = options.success;
            options.success = function () {
                success.apply(this, arguments);
                Messages.showSuccessMsg(AJS.I18n.getText('admin.project.workflows.draft.discarded'));
            };
            return this.editDraft(options, 'delete');
        },
        viewOriginal: function (options) {
            options = options ? _.clone(options) : {};
            this.set('hasDraft', true);
            var success = options.success;
            options = _.extend(options, {
                data: {
                    original: true
                },
                success: function () {
                    success.apply(this, arguments);
                }
            });
            this.editDraft(options, 'read');
        },
        viewDraft: function (options) {
            this.editDraft(options, 'read');
        }
    });
});
