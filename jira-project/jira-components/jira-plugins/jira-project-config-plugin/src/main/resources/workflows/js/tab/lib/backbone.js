/**
 * This module modifies the module 'jira-project-config/backbone' by overwriting .ajax(). Without this,
 * some tests in `TestTabWorkflow` fail, which means other code outside `workflows/js/tab/` is depending
 * on the .ajax() modification.
 * It should be investigated and fixed, it is a bad pattern.
 */
define("jira-project-config/workflows/tab/lib/backbone", ["require"], function(require){
    "use strict";

    var _ = require('underscore');
    var WebSudoSmartAjax = require('jira/ajs/ajax/smart-ajax');
    var ActiveCallbacks = require('jira-project-config/workflows/tab/utils/active-callbacks');
    var Dimmer = require('jira-project-config/workflows/tab/utils/dimmer');
    var Backbone = require("backbone");

    var backboneOriginalAjax = Backbone.ajax;

    function isConfigUrl(testUrl) {
        return _.isString(testUrl) && (testUrl.indexOf("projectconfig") !== -1 || testUrl.indexOf("globalconfig") !== -1);
    }

    function workflowAjax(options) {
        options.error = JIRA.Workflows.Scheme.Common.smartAjaxErrorHandler(JIRA.Workflows.Scheme.Common.refreshErrorDialog);
        var oldComplete = options.complete;
        options.complete = function () {
            oldComplete && oldComplete.apply(this, arguments);
            ActiveCallbacks.decrement();
        };
        ActiveCallbacks.increment();
        WebSudoSmartAjax.makeWebSudoRequest(options, {
            success: Dimmer.onUndim,
            cancel: function (e) {
                //Don't redirect on websudo cancel. We can display stuff as non-admin.
                e.preventDefault();
            }
        });
    }

    // Since Backbone.ajax is used elsewhere we should not apply workflow specific logic globally to all Backbone.ajax requests
    Backbone.ajax = function (options) {
        options = options || {};
        if (isConfigUrl(options.url)) {
            return workflowAjax(options);
        } else {
            return backboneOriginalAjax(options);
        }
    };

    return Backbone;
});
