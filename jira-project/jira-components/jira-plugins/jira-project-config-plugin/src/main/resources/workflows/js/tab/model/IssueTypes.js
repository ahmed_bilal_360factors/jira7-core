define("jira-project-config/workflows/tab/model/issue-types", ["require"], function(require){
    "use strict";

    var _ = require('underscore');
    var Backbone = require('jira-project-config/workflows/tab/lib/backbone');
    var IssueTypeModel = require('jira-project-config/workflows/tab/model/issue-type');
    var localeComparator = require('jira-project-config/workflows/tab/utils/locale-comparator');

    return Backbone.Collection.extend({
        model: IssueTypeModel,
        comparator: localeComparator('name'),
        update: function (collection) {
            var currentIds = _.chain(this.pluck('id'));

            collection.each(function (issueTypeModel) {
                var newId = issueTypeModel.id;
                currentIds = currentIds.without(newId);
                var current = this.get(newId);
                if (current) {
                    current.set(issueTypeModel.toJSON());
                } else {
                    this.add(issueTypeModel);
                }
            }, this);

            currentIds.each(function (id) {
                this.remove(id);
            }, this);
        }
    });
});
