define("jira-project-config/workflows/scheme/editor/views/page", ["require"], function(require){
    "use strict";

    var Backbone = require('jira-project-config/backbone');
    var TemplateMessages = require('jira-project-config/templates/messages');
    var WorkflowSchemeView = require('jira-project-config/workflows/scheme/editor/views/workflow-scheme');

    return Backbone.View.extend({

        initialize: function (options) {
            this.bus = options.bus;
        },

        renderError: function (errorMessage) {
            this.html(TemplateMessages.errorMessage({
                message: errorMessage
            }));
        },

        renderModel: function (model, issueTypes) {
            model.workflows.on("add remove", function (model, collection) {
                this.updateMultiple(collection);
            }, this);
            model.workflows.on("reset", this.updateMultiple, this);
            model.on("change:draftScheme", this.updateDraft, this);
            this.updateMultiple(model.workflows);
            if (this.workflowSchemeView) {
                this.workflowSchemeView.finalize();
            }
            this.workflowSchemeView = new WorkflowSchemeView({
                model: model,
                issueTypes: issueTypes,
                bus: this.bus
            }).render();
            this.html(this.workflowSchemeView.$el);
        },

        html: function (html) {
            this.$el.removeClass("workflowscheme-editor-loading").html(html);
        },

        setLoading: function (loading) {
            this.$el.toggleClass("workflowscheme-loading", loading);
        },

        setMultiple: function (multiple) {
            this.$el.toggleClass("multiple-workflows", multiple);
        },

        updateMultiple: function (collection) {
            this.setMultiple(collection.length > 1);
        }
    });
});
