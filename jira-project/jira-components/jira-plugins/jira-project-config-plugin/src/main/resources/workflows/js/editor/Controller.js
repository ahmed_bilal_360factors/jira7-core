define("jira-project-config/workflows/scheme/editor/controller", ["require"], function(require){
    "use strict";

    var jQuery = require('jquery');
    var logger = require('jira/util/logger');
    var _ = require('underscore');
    var Messages = require('jira/message');
    var WebSudoSmartAjax = require('jira/ajs/ajax/smart-ajax/web-sudo');
    var Events = require('jira/util/events');

    /**
     * @param options
     * @constructor
     */
    var Controller = function (options) {
        this.url = options.url;
        this.schemeId = options.schemeId;
        this.modelClass = options.modelClass;
        this.viewEventBus = options.viewEventBus;

        this.page = new options.pageView({
            el: options.container,
            bus: options.viewEventBus
        });
        this.model = new options.model();
        this.issueTypes = new options.issueTypeModel();
        this.requests = 0;
        jQuery(window).on("beforeunload", _.bind(function () {
            if (this.requests > 0) {
                return AJS.I18n.getText("admin.project.workflowscheme.ajax.in.progress");
            }
        }, this));

        this.viewEventBus.on("removeIssueType", this.removeIssueType, this);
        this.viewEventBus.on("removeWorkflow", this.removeWorkflow, this);
        this.viewEventBus.on("assignIssueTypes", this.assignIssueTypes, this);
        this.viewEventBus.on("addWorkflow", this.addWorkflow, this);
        this.viewEventBus.on("discardDraft", this.discardDraft, this);
        this.viewEventBus.on("updateProperty", this.updateProperty, this);
    };

    _.extend(Controller.prototype, {

        load: function (callback) {
            if (!this.schemeId) {
                this.page.renderError(AJS.I18n.getText('admin.project.workflows.not.specified'));
            } else {
                return WebSudoSmartAjax.makeWebSudoRequest({
                    url: this.resourceUrl(),
                    dataType: 'json'
                })
                    .fail(JIRA.Workflows.Scheme.Common.smartAjaxErrorHandler(_.bind(function (error) {
                        this.page.renderError(error);
                    }, this)))
                    .done(_.bind(function (data) {
                        this.loadModel(data);
                        this.page.renderModel(this.model, this.issueTypes);
                    }, this))
                    .always(function () {
                        logger.trace("edit.workflow.scheme.loaded");
                    })
                    .done(_.bind(callback || jQuery.noop, this));
            }
        },

        loadModel: function (data) {
            if (data.draftScheme && data.id !== this.schemeId) {
                data.parentId = this.schemeId;
            }
            data = _.extend({
                description: ""
            }, data);
            var issueTypesUpdated = this.issueTypes.load(data.issueTypes || []);
            var modelUpdated = this.model.load(data);
            return issueTypesUpdated || modelUpdated;
        },

        resourceUrl: function () {
            return this.url + this.schemeId;
        },

        removeIssueType: function (workflowName, issueType) {
            this.model.removeIssueType(workflowName, issueType);
            this.ajax({
                url: this.resourceUrl() + "/issuetype",
                type: "DELETE",
                contentType: "application/json",
                data: JSON.stringify({
                    issueTypes: [issueType],
                    workflow: workflowName
                })
            });
        },

        removeWorkflow: function (workflow) {
            var nextDefault = this.model.removeWorkflow(workflow);
            this.ajax({
                url: this.resourceUrl() + "/workflow",
                type: "DELETE",
                contentType: "application/json",
                data: JSON.stringify({
                    workflow: workflow,
                    nextDefaultWorkflow: nextDefault
                })
            });
        },

        assignIssueTypes: function (workflow, issueTypes, defaultWorkflow) {
            this.model.assignIssueTypes(workflow, issueTypes, defaultWorkflow);
            Events.trigger("assignissuetypes");
            this.ajax({
                url: this.resourceUrl(),
                type: "PUT",
                contentType: "application/json",
                data: JSON.stringify({
                    workflow: workflow,
                    issueTypes: issueTypes,
                    defaultWorkflow: defaultWorkflow
                })
            });
        },

        addWorkflow: function (attr) {
            this.model.addWorkflow(attr);
            Events.trigger("addworkflow");
            this.ajax({
                url: this.resourceUrl(),
                type: "PUT",
                contentType: "application/json",
                data: JSON.stringify({
                    workflow: attr.name,
                    issueTypes: attr.issueTypes,
                    defaultWorkflow: attr['default']
                })
            });
        },

        discardDraft: function () {
            this.model.discardDraft();
            Messages.showSuccessMsg(AJS.I18n.getText('admin.project.workflows.draft.discarded'));
            this.ajax({
                url: this.resourceUrl() + "/draft",
                type: "DELETE",
                contentType: "application/json"
            });
        },

        updateProperty: function (name, value) {
            this.model.set(name, value);
            this.ajax({
                url: this.resourceUrl() + "/" + name,
                type: "PUT",
                contentType: "application/json",
                data: value
            });
        },

        ajax: function (settings) {
            this.requests++;
            this.page.setLoading(true);

            var abortRequests = function () {
                this.requests = 0;
                logger.trace("edit.workflow.scheme.complete");
            }.bind(this);

            var request = WebSudoSmartAjax.makeWebSudoRequest(settings, {
                cancel: abortRequests
            });
            request.done(_.bind(function (data) {
                if (!--this.requests) {
                    this.page.setLoading(false);

                    if (this.loadModel(data)) {
                        logger.trace("edit.workflow.scheme.changed");

                        Messages.showWarningMsg(AJS.I18n.getText("admin.project.workflowscheme.inconsistent.state"));
                    }
                    logger.trace("edit.workflow.scheme.complete");
                }
            }, this));
            request.fail(abortRequests);
            request.fail(JIRA.Workflows.Scheme.Common.smartAjaxErrorHandler(JIRA.Workflows.Scheme.Common.refreshErrorDialog));
            return request.promise();
        }
    });

    return Controller;
});
