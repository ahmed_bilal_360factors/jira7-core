define("jira-project-config/workflows/dialogs/discard-draft", ["require"], function(require){
    "use strict";

    var jQuery = require('jquery');
    var FormDialog = require('jira/dialog/form-dialog');
    var TemplatesDialogs = require('jira-project-config/workflows/scheme/dialogs/templates');

    return function (submitHandler) {
        new FormDialog({
            id: 'discard-draft-dialog',
            content: jQuery(TemplatesDialogs.discardDraft()),
            submitHandler: function (e, callback) {
                e.preventDefault();
                var dialog = this;
                var onSubmit = function () {
                    callback();
                    dialog.hide();
                };
                if (submitHandler) {
                    submitHandler(onSubmit);
                } else {
                    onSubmit();
                }
            }
        }).show();
    };
});
