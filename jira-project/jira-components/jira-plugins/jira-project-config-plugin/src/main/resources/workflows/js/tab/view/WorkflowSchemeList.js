define("jira-project-config/workflows/tab/view/workflow-scheme-list", ["require"], function(require){
    "use strict";

    var jQuery = require('jquery');
    var _ = require('underscore');
    var Backbone = require('jira-project-config/workflows/tab/lib/backbone');
    var ProjectConfigUtils = require('jira-project-config/utils');
    var WorkflowView = require('jira-project-config/workflows/tab/view/workflow');
    var WorkflowHeaderView = require('jira-project-config/workflows/tab/view/workflow-header');
    var ActiveCallbacks = require('jira-project-config/workflows/tab/utils/active-callbacks');

    return Backbone.View.extend({
        className: "project-config-workflow-panel-body aui",
        tagName: "table",

        initialize: function () {
            this.collection = this.model.get('mappings');
            this.model.on("change:defaultScheme", this.changeDefault, this);
            this.collection.on("add", this.toggleMultiple, this);
            this.collection.on("add", this.insertWorkflow, this);
            this.collection.on("remove", this.removeView, this);
            this.collection.on("reorder sort", this.reorder, this);

            this.model.on("change:draftScheme change:shared change:hasDraft", this.toggleEditable, this);
            this.collection.on("add remove", this.toggleEditable, this);
            this.views = {};
        },
        render: function () {

            this.listHeader = new WorkflowHeaderView({admin: this.model.get('admin')});
            this.$el.append(this.listHeader.render().el);

            this.collection.each(function (workflow) {
                this.insertWorkflow(workflow);
            }, this);

            this.changeDefault(undefined, this.model.get('defaultScheme'));
            this.toggleMultiple();
            this.toggleEditable();

            this.$(".project-config-workflow-diagram").fancybox({
                type: "image",
                title: this.model.get('displayName')
            });

            return this;
        },
        insertWorkflow: function (workflow, collection, options) {
            options = options || {};
            var workflowView = new WorkflowView({
                model: workflow,
                issueTypes: this.model.get('issueTypes'),
                admin: this.model.get('admin'),
                defaultScheme: this.model.get('defaultScheme'),
                shared: this.model.get('shared')
            });
            this.views[workflow.get('name')] = workflowView;

            var $workflowEl = workflowView.render().$el.hide();
            if (options.index !== undefined) {
                if (options.index) {
                    var prevModel = collection.at(options.index - 1);
                    var prevView = this.views[prevModel.get('name')];
                    prevView.$el.after($workflowEl);
                } else {
                    this.$el.prepend($workflowEl);
                }
                ActiveCallbacks.increment();
                $workflowEl.slideDown('slow', ActiveCallbacks.decrement);
                $workflowEl.css("display", "table-row");
            } else {
                ActiveCallbacks.increment();
                $workflowEl.css("display", "table-row");
                this.$el.append($workflowEl);
                ActiveCallbacks.decrement();
            }

            workflowView.on("assignIssueTypes", this.assignIssueTypes, this);
            workflowView.on("removeWorkflow", this.removeWorkflow, this);
            workflowView.on("workflowRemoved", this.toggleMultiple, this);
        },
        reorder: function () {
            var instance = this;

            // Re-arrange the views in the same order as the collection.
            var previous = null;
            this.collection.each(function (workflow) {
                var workflowView = instance.views[workflow.get('name')];
                if (!previous) {
                    instance.$el.prepend(workflowView.$el);
                } else {
                    previous.$el.after(workflowView.$el);
                }
                previous = workflowView;
            });
        },
        assignIssueTypes: function (e) {
            this.trigger("assignIssueTypes", e);
        },
        removeWorkflow: function (e) {
            this.trigger("removeWorkflow", e);
        },
        toggleMultiple: function () {
            if (this.$el.hasClass("project-config-multiple-workflows") && this.collection.length <= 1) {
                var $container = this.$('.project-config-workflow-container').height('auto');
                var height = $container.height();
                $container.height(250);
                $container.animate({
                    height: height
                }, function () {
                    jQuery(this).height('auto');
                });
            } else if (!this.$el.hasClass("project-config-multiple-workflows") && this.collection.length > 1) {
                this.$('.project-config-workflow-container').animate({
                    height: 250
                });
            }
            this.$el.toggleClass("project-config-multiple-workflows", this.collection.length > 1);
        },
        toggleEditable: function () {
            var currentProjectId = +ProjectConfigUtils.getId();
            var editable = this.model.get("draftScheme") || !this.model.get("hasDraft");
            editable = editable && this.collection.length > 1;
            editable = editable && !_(this.model.get('shared').sharedWithProjects).find(function (el) {
                    return el.id !== currentProjectId;
                });

            this.$el.toggleClass("project-config-workflow-editable", editable);
        },
        removeView: function (workflow) {
            delete this.views[workflow.get('name')];
        },
        changeDefault: function (model, value) {
            this.$el.toggleClass('project-config-workflow-default', value);
            this.collection.trigger("changeDefault", value);
        }
    });
});
