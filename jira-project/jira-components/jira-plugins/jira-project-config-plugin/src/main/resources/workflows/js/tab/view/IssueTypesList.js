define("jira-project-config/workflows/tab/view/issue-types", [
    'underscore',
    'jira-project-config/workflows/tab/lib/backbone',
    'wrm/context-path',
    'jira-project-config/utils',
    'jira-project-config/workflows/tab/templates',
    'jira/util/formatter'
], function(
    _,
    Backbone,
    wrmContextPath,
    ProjectConfigUtils,
    TemplatesProjectTab,
    formatter
){
    "use strict";

    return Backbone.View.extend({
        className: "project-config-workflow-issue-types",
        initialize: function () {
            this.model.on('change:issueTypes', this.render, this);
        },
        template: function (data) {
            return TemplatesProjectTab.issueTypesList(data);
        },

        _getUrl: function (issueTypeId) {
            return formatter.format("{0}/plugins/servlet/project-config/{1}/issuetypes/{2}", wrmContextPath(), ProjectConfigUtils.getKey(), issueTypeId);
        },

        render: function () {
            var issueTypes = _.map(this.model.get('issueTypes'), function (issueTypeId) {
                return _.extend(this.options.issueTypes.get(issueTypeId).toJSON(), {url: this._getUrl(issueTypeId)});
            }.bind(this), this);

            this.$el.html(this.template({
                issueTypes: issueTypes,
                workflow: {
                    name: this.model.get('name'),
                    displayName: this.model.get('displayName')
                },
                admin: this.options.admin
            }));

            return this;
        }
    });
});
