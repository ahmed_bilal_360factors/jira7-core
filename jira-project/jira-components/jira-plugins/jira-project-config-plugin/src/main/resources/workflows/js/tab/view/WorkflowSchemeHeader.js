define("jira-project-config/workflows/tab/view/workflow-scheme-header", ["require"], function(require){
    "use strict";

    var jQuery = require('jquery');
    var _ = require('underscore');
    var Reasons = require('jira/util/events/reasons');
    var Types = require('jira/util/events/types');
    var Events = require('jira/util/events');
    var Backbone = require('jira-project-config/workflows/tab/lib/backbone');
    var TemplatesProjectTab = require('jira-project-config/workflows/tab/templates');
    var AddWorkflowDialogView = require('jira-project-config/workflows/dialogs/add-workflow-dialog-view');

    return Backbone.View.extend({
        initialize: function () {
            this.model.on("change:lastModifiedDate change:lastModifiedUser change:draftScheme change:shared change:hasDraft", this.render, this);
            this.model.get('mappings').on("add remove", this.render, this);
        },
        template: function (data) {
            return TemplatesProjectTab.header(data);
        },
        render: function () {
            this.$el.html(this.template(this.model.toJSON()));

            this.$("#project-config-workflow-add-button").click(_.bind(this.addWorkflow, this));

            //remove the inline dialog if it already exists since we are recreating it
            jQuery("#inline-dialog-project-config-workflow-scheme-shared").remove();

            Events.trigger(Types.NEW_CONTENT_ADDED, [this.$el, Reasons.workflowHeaderReady]);

            return this;
        },
        addWorkflow: function (e) {
            e.preventDefault();
            if (!jQuery(e.target).hasClass("disabled")) {
                new AddWorkflowDialogView({
                    assignedWorkflows: this.model.get('mappings').pluck('name')
                }).render().on("assignIssueTypes", this.assignIssueTypes, this);
            }
        },
        assignIssueTypes: function (e) {
            this.trigger("assignIssueTypes", e);
        }
    });
});
