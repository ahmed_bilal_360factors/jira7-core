define("jira-project-config/workflows/dialogs/add-workflow-dialog-workflow-view", ["require"], function(require){
    "use strict";

    var Backbone = require('jira-project-config/backbone');
    var TemplatesDialogs = require('jira-project-config/workflows/scheme/dialogs/templates');

    return Backbone.View.extend({

        template: function (data) {
            return TemplatesDialogs.preview(data);
        },
        render: function () {
            this.$el.html(this.template({
                workflow: this.model.toJSON()
            }));

            this.workflowCanvas = this.$el.find("#workflow-canvas");
            this.workflowDesigner || (this.workflowDesigner = new JIRA.WorkflowDesigner.Application({
                actions: false,
                draft: false,
                element: this.workflowCanvas,
                fullScreenButton: false,
                immutable: true,
                workflowId: this.model.get("name")
            }));

            return this;
        }
    });
});
