define("jira-project-config/workflows/scheme/editor/views/workflow", ["require"], function(require){
    "use strict";

    var jQuery = require('jquery');
    var _ = require('underscore');
    var Backbone = require('jira-project-config/backbone');
    var TemplatesEditor = require('jira-project-config/workflows/scheme/editor/templates');
    var AssignIssueTypes = require('jira-project-config/workflows/dialogs/assign-issue-types');

    /**
     * Dialog
     *
     * @inner
     * @type {JIRA.WorkflowDesigner.Dialog}
     */
    var workflowDialog;

    return Backbone.View.extend({

        tagName: "tr",

        events: {
            "click .remove-issue-type": "removeIssueType",
            "click .remove-all-issue-types": "removeAllIssueTypes",
            "click .assign-issue-types": "assignIssueTypes",
            "click .workflow-text-view": "viewWorkflowText"
        },

        initialize: function (options) {
            this.bus = options.bus;
            this.issueTypes = options.issueTypes;
            this.workflows = options.workflows;
            this.editable = options.editable;
            this.model.on('change', this.render, this);
            this.model.on('destroy', this.unrender, this);
        },

        template: function (data) {
            return TemplatesEditor.WorkflowView(data);
        },

        render: function () {
            var workflowJSON = this.model.toJSON();
            workflowJSON.issueTypes = _.map(workflowJSON.issueTypes, function (issueType) {
                return this.issueTypes.get(issueType).toJSON();
            }, this);
            workflowJSON.editable = this.editable;
            workflowJSON.showAssign = this.model.get('issueTypes').length < this.issueTypes.length || !this.model.get('default');

            this.$el.attr("data-workflow-name", workflowJSON.name);
            this.$el.html(this.template(workflowJSON));

            this.$(".workflow-diagram-view").click(function () {
                workflowDialog = new JIRA.WorkflowDesigner.Dialog({
                    id: "view-workflow-dialog-workflow-schemes",
                    workflowId: function () {
                        return workflowJSON.name;
                    }
                });
                workflowDialog.show();
            });

            return this;
        },

        unrender: function () {
            this.finalize();
            this.$el.remove();
        },

        removeIssueType: function (e) {
            e.preventDefault();
            this.bus.trigger("removeIssueType", this.model.id, jQuery(e.currentTarget).data('issue-type').toString());
        },

        removeAllIssueTypes: function (e) {
            e.preventDefault();
            this.bus.trigger("removeWorkflow", this.model.id);
        },

        assignIssueTypes: function (e) {
            e.preventDefault();
            var defaultWorkflow = this.workflows.find(function (workflow) {
                return workflow.get('default');
            });
            AssignIssueTypes({
                workflows: this.workflows.toJSON(),
                issueTypes: this.issueTypes.toJSON(),
                workflowName: this.model.id,
                workflowDisplayName: this.model.get("displayName"),
                draftScheme: true,
                defaultWorkflow: defaultWorkflow.toJSON(),
                showDefault: true,
                callback: _.bind(function (attr, callback) {
                    this.bus.trigger('assignIssueTypes', attr.name, attr.issueTypes || [], !!attr['default']);
                    callback();
                }, this)
            });
        },

        viewWorkflowText: function (e) {
            e.preventDefault();
            JIRA.Workflows.Scheme.Common.showWorkflowTextView({
                workflowName: this.model.id
            });
        },

        finalize: function () {
            this.model.off(null, null, this);
        }
    });
});
