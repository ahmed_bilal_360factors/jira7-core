define("jira-project-config/workflows/scheme/editor/models/issue-type-collection", ["require"], function(require){
    "use strict";

    var _ = require('underscore');
    var Backbone = require('jira-project-config/backbone');
    var IssueTypeModel = require('jira-project-config/workflows/scheme/editor/models/issue-type');

    return Backbone.Collection.extend({

        model: IssueTypeModel,

        load: function (data) {
            if (!_.isEqual(this.toJSON(), data)) {
                this.reset(data);
                return true;
            } else {
                return false;
            }
        }
    });
});
