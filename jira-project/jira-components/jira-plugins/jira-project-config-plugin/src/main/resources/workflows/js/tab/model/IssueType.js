define("jira-project-config/workflows/tab/model/issue-type", ["require"], function(require){
    "use strict";

    var Backbone = require('jira-project-config/workflows/tab/lib/backbone');

    return Backbone.Model.extend({
        defaults: {
            name: "",
            description: "",
            iconUrl: ""
        }
    });
});
