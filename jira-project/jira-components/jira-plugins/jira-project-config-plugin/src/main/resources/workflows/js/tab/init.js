require([
    'jira-project-config/workflows/tab/view/workflow-schema-draft-message',
    'jira-project-config/workflows/tab/view/workflow-scheme',
    'jira-project-config/workflows/tab/model/workflow-scheme',
    'jira-project-config/utils',
    'jquery'
], function(
    WorkflowSchemeDraftMessageView,
    WorkflowSchemeView,
    WorkflowSchemeModel,
    ProjectConfigUtils,
    jQuery
) {

    jQuery(function () {
        var $container = jQuery("#project-config-panel-workflows");
        if ($container.length) {
            jQuery(document.body).addClass('page-type-workflows');
            $container.addClass("project-config-loading");

            var workflowName = JIRA.Workflows.Scheme.Common.checkWorkflow();
            if (!workflowName) {
                JIRA.Workflows.Scheme.Common.checkMigrationSuccess();
            }

            new WorkflowSchemeModel().fetch({
                success: function (model) {
                    model.set({
                        projectId: ProjectConfigUtils.getId(),
                        currentUser: JIRA.Users.LoggedInUser.userName()
                    });
                    var view = new WorkflowSchemeView({
                        el: "#project-config-panel-workflows",
                        model: model
                    }).render();

                    new WorkflowSchemeDraftMessageView({
                        el: "#project-config-workflows-draft-message",
                        model: model
                    });

                    if (workflowName) {
                        view.showAssignIssueTypes({
                            name: workflowName,
                            displayName: workflowName
                        });
                    }
                },
                complete: function () {
                    $container.removeClass("project-config-loading");
                }
            });
        }
    });
});
