define("jira-project-config/workflows/scheme/editor/models/workflow", ["require"], function(require){
    "use strict";

    var _ = require('underscore');
    var Backbone = require('jira-project-config/backbone');

    var equalsIgnoreOrder = function (a, b) {
        return _.isEqual(_.clone(a).sort(), _.clone(b).sort());
    };

    return Backbone.Model.extend({

        idAttribute: "name",

        destroy: function () {
            this.trigger('destroy', this, this.collection);
        },

        equalsObject: function (data) {
            return data['default'] === this.get('default') && data.name === this.id && equalsIgnoreOrder(this.get('issueTypes'), data.issueTypes);
        }
    });
});
