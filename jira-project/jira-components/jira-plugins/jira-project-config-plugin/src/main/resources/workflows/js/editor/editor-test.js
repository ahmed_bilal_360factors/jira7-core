AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:edit-workflow-scheme"], function(){
    "use strict";

    var _ = require('underscore');
    var Backbone = require("jira-project-config/backbone");
    var Controller = require("jira-project-config/workflows/scheme/editor/controller");
    var Deferred = require("jira/jquery/deferred");
    var Events = require('jira/util/events');
    var IssueTypeCollection = require("jira-project-config/workflows/scheme/editor/models/issue-type-collection");
    var jQuery = require("jquery");
    var Messages = require("jira/message");
    var Page = require("jira-project-config/workflows/scheme/editor/views/page");
    var Reasons = require("jira/util/events/reasons");
    var SmartAjax = require("jira/ajs/ajax/smart-ajax");
    var WebSudoSmartAjax = require("jira/ajs/ajax/smart-ajax/web-sudo");
    var TemplateMessages = require("jira-project-config/templates/messages");
    var TemplatesEditor = require("jira-project-config/workflows/scheme/editor/templates");
    var Types = require("jira/util/events/types");
    var WorkflowCollection = require("jira-project-config/workflows/scheme/editor/models/workflow-collection");
    var WorkflowModel = require("jira-project-config/workflows/scheme/editor/models/workflow");
    var WorkflowSchemeHeaderView = require("jira-project-config/workflows/scheme/editor/views/workflow-scheme-header");
    var WorkflowSchemeModel = require("jira-project-config/workflows/scheme/editor/models/workflow-scheme");
    var WorkflowSchemeView = require("jira-project-config/workflows/scheme/editor/views/workflow-scheme");
    var WorkflowView = require("jira-project-config/workflows/scheme/editor/views/workflow");

    module("JIRA.Workflows.Scheme.Editor.Controller - Tests", {
        teardown: function () {
            this.sandbox.restore();
        },
        setup: function () {
            this.sandbox = sinon.sandbox.create();
            this.bus = _.clone(Backbone.Events);
        }
    });

    test("Controller.load no scheme", function () {
        var makeRequest = this.sandbox.stub(SmartAjax, "makeWebSudoRequest");
        var test = this;

        var PageView = function () {
            this.renderError = test.sandbox.spy();
        };

        var controller = new Controller({
            pageView: PageView,
            model: jQuery.noop,
            issueTypeModel: jQuery.noop,
            viewEventBus: this.bus
        });

        controller.load();

        ok(controller.page.renderError.calledOnce, "Render error called");
        ok(!makeRequest.called, "Never made ajax request.");
    });

    test("Controller.load good load", function () {
        var makeRequest = this.sandbox.stub(WebSudoSmartAjax, "makeWebSudoRequest");
        var test = this;

        var PageView = function () {
            this.renderModel = test.sandbox.spy();
        };

        var SimpleModel = function () {
            this.load = test.sandbox.spy();
            this.workflows = {
                on: test.sandbox.spy()
            };
        };

        var controller = new Controller({
            pageView: PageView,
            model: SimpleModel,
            issueTypeModel: SimpleModel,
            schemeId: 101010,
            url: "test",
            viewEventBus: this.bus
        });

        var value = new Deferred();
        //Make sure we make the correct AJAX call.
        makeRequest.withArgs({url: "test101010", dataType: 'json'}).returns(value.promise());

        controller.load();
        ok(makeRequest.calledOnce, "Never made ajax request.");

        //Simulate a successful call to make sure the success handler works.
        var object = {issueTypes: {"issueTypes": "test"}, a: "c", description: ""};
        value.resolveWith(window, [object]);

        ok(controller.issueTypes.load.calledWithExactly(object.issueTypes), "Called with correct issueTypes data.");
        ok(controller.model.load.calledWithExactly(object), "Called with correct scheme data.");
        ok(controller.page.renderModel.calledWithExactly(controller.model, controller.issueTypes));
    });

    test("Controller.load bad load", function () {
        var makeRequest = this.sandbox.stub(WebSudoSmartAjax, "makeWebSudoRequest");
        var errorHandler = this.sandbox.stub(JIRA.Workflows.Scheme.Common, "smartAjaxErrorHandler");
        var test = this;

        var PageView = function () {
            this.renderError = test.sandbox.spy();
        };

        var controller = new Controller({
            pageView: PageView,
            model: jQuery.noop,
            issueTypeModel: jQuery.noop,
            schemeId: 101010,
            url: "test",
            viewEventBus: this.bus
        });

        var value = new Deferred();
        //Make sure we make the correct AJAX call.
        makeRequest.withArgs({
            url: "test101010",
            dataType: "json"
        }).returns(value.promise());

        controller.load();
        ok(makeRequest.calledOnce, "Never made ajax request.");

        value.rejectWith(window, []);

        ok(errorHandler.calledOnce);
        var errorFunction = errorHandler.args[0][0];

        ok(!controller.page.renderError.called);
        var errorMessage = "With error String";
        errorFunction(errorMessage);

        ok(controller.page.renderError.calledOnce);
        ok(controller.page.renderError.calledWithExactly(errorMessage));
    });

    test("Controller removeIssueType", function () {
        var test = this;
        var controller = new Controller({
            pageView: jQuery.noop,
            model: function () {
                this.removeIssueType = test.sandbox.spy();
            },
            issueTypeModel: jQuery.noop,
            viewEventBus: this.bus,
            url: "/test/",
            schemeId: 10101
        });

        controller.ajax = this.sandbox.spy();

        var workflowName = 'workflow';
        var issueType = 1;
        controller.removeIssueType(workflowName, issueType);

        ok(controller.model.removeIssueType.calledWith(workflowName, issueType));
        ok(controller.ajax.calledWith({
            url: controller.resourceUrl() + "/issuetype",
            type: "DELETE",
            contentType: "application/json",
            data: JSON.stringify({
                issueTypes: [issueType],
                workflow: workflowName
            })
        }));
    });

    test("Controller removeWorkflow", function () {
        var test = this;
        var controller = new Controller({
            pageView: jQuery.noop,
            model: function () {
                this.removeWorkflow = test.sandbox.stub();
            },
            issueTypeModel: jQuery.noop,
            viewEventBus: this.bus,
            url: '/url/',
            schemeId: 100
        });

        var nextWorkflow = "nextDefault";
        var workflowName = 'name';

        controller.ajax = this.sandbox.spy();
        controller.model.removeWorkflow.returns(nextWorkflow);
        controller.removeWorkflow(workflowName);
        ok(controller.model.removeWorkflow.calledWith(workflowName));
        ok(controller.ajax.calledWith({
            url: controller.resourceUrl() + '/workflow',
            type: "DELETE",
            contentType: "application/json",
            data: JSON.stringify({
                workflow: workflowName,
                nextDefaultWorkflow: nextWorkflow
            })
        }));
    });

    test("Controller assignIssueTypes", function () {
        var test = this;
        var controller = new Controller({
            pageView: jQuery.noop,
            model: function () {
                this.assignIssueTypes = test.sandbox.spy();
            },
            issueTypeModel: jQuery.noop,
            viewEventBus: this.bus,
            url: '/url/',
            schemeId: 100
        });

        var workflowName = 'name';
        var issueTypes = ['1', '2'];
        var defaultWorkflow = false;

        controller.ajax = this.sandbox.spy();
        controller.assignIssueTypes(workflowName, issueTypes, defaultWorkflow);
        ok(controller.model.assignIssueTypes.calledWith(workflowName, issueTypes, defaultWorkflow));
        ok(controller.ajax.calledWith({
            url: controller.resourceUrl(),
            type: "PUT",
            contentType: "application/json",
            data: JSON.stringify({
                workflow: workflowName,
                issueTypes: issueTypes,
                defaultWorkflow: defaultWorkflow
            })
        }));
    });

    test("Controller addWorkflow", function () {
        var test = this;
        var controller = new Controller({
            pageView: jQuery.noop,
            model: function () {
                this.addWorkflow = test.sandbox.spy();
            },
            issueTypeModel: jQuery.noop,
            viewEventBus: this.bus,
            url: '/url/',
            schemeId: 100
        });

        var attr = {
            name: 'name',
            'default': true,
            issueTypes: []
        };

        controller.ajax = this.sandbox.spy();
        controller.addWorkflow(attr);
        ok(controller.model.addWorkflow.calledWith(attr));
        ok(controller.ajax.calledWith({
            url: controller.resourceUrl(),
            type: "PUT",
            contentType: "application/json",
            data: JSON.stringify({
                workflow: attr.name,
                issueTypes: attr.issueTypes,
                defaultWorkflow: attr['default']
            })
        }));
    });

    test("Controller discardDraft", function () {
        var test = this;
        var controller = new Controller({
            pageView: jQuery.noop,
            model: function () {
                this.discardDraft = test.sandbox.spy();
            },
            issueTypeModel: jQuery.noop,
            viewEventBus: this.bus,
            url: '/url/',
            schemeId: 100
        });

        controller.ajax = this.sandbox.spy();
        controller.discardDraft();
        ok(controller.model.discardDraft.calledWith());
        ok(controller.ajax.calledWith({
            url: controller.resourceUrl() + "/draft",
            type: "DELETE",
            contentType: "application/json"
        }));
    });

    test("Controller loadModel", function () {
        var test = this;
        var Model = function () {
            this.load = test.sandbox.stub();
        };

        var IssueTypeModel = function () {
            this.load = test.sandbox.stub();
        };

        var controller = new Controller({
            pageView: jQuery.noop,
            model: Model,
            issueTypeModel: IssueTypeModel,
            viewEventBus: {
                on: jQuery.noop
            }
        });

        var data = {
            issueTypes: "something",
            description: ""
        };
        controller.model.load.returns(true);
        controller.issueTypes.load.returns(true);

        ok(controller.loadModel(data));
        ok(controller.model.load.calledWithExactly(data));
        ok(controller.issueTypes.load.calledWithExactly(data.issueTypes));

        controller.model.load.returns(true);
        controller.issueTypes.load.returns(false);
        ok(controller.loadModel(data));

        controller.model.load.returns(false);
        controller.issueTypes.load.returns(true);
        ok(controller.loadModel(data));

        controller.model.load.returns(false);
        controller.issueTypes.load.returns(false);
        ok(!controller.loadModel(data));
    });

    test("Controller ajax success no change", function () {
        var deferred = new Deferred();
        var makeRequest = this.sandbox.stub(WebSudoSmartAjax, "makeWebSudoRequest");
        var showWarning = this.sandbox.stub(Messages, "showWarningMsg");
        makeRequest.returns(deferred.promise());

        var test = this;
        var controller = new Controller({
            pageView: function () {
                this.setLoading = jQuery.noop;
                this.renderModel = test.sandbox.spy();
            },
            model: function () {
                this.setDraft = test.sandbox.spy();
            },
            issueTypeModel: jQuery.noop,
            viewEventBus: {
                on: jQuery.noop
            }
        });
        controller.loadModel = this.sandbox.stub();
        controller.loadModel.returns(false);

        ok(controller.ajax());
        ok(makeRequest.called);
        deferred.resolveWith(undefined, [{}]);
        ok(!showWarning.called);
        ok(!controller.page.renderModel.called);
        ok(!controller.model.setDraft.called);
    });

    test("Controller ajax success change", function () {
        var deferred = new Deferred();
        var makeRequest = this.sandbox.stub(WebSudoSmartAjax, "makeWebSudoRequest");
        var showWarning = this.sandbox.stub(Messages, "showWarningMsg");
        makeRequest.returns(deferred.promise());

        var test = this;
        var controller = new Controller({
            pageView: function () {
                this.setLoading = jQuery.noop;
                this.renderModel = test.sandbox.spy();
            },
            model: function () {
                this.setDraft = test.sandbox.spy();
            },
            issueTypeModel: jQuery.noop,
            viewEventBus: {
                on: jQuery.noop
            }
        });
        controller.loadModel = this.sandbox.stub();
        controller.loadModel.returns(true);

        ok(controller.ajax());
        ok(makeRequest.called);
        deferred.resolveWith(undefined, [{}]);
        ok(showWarning.called);
        ok(!controller.model.setDraft.called);
    });

    test("Controller ajax fail", function () {
        var makeRequest = this.sandbox.stub(WebSudoSmartAjax, "makeWebSudoRequest");
        var errorHandler = this.sandbox.stub(JIRA.Workflows.Scheme.Common, "smartAjaxErrorHandler");
        var deferred = new Deferred();
        makeRequest.returns(deferred.promise());

        var test = this;
        var controller = new Controller({
            pageView: function () {
                this.setLoading = jQuery.noop;
            },
            model: function () {
                this.setDraft = test.sandbox.spy();
            },
            issueTypeModel: jQuery.noop,
            viewEventBus: {
                on: jQuery.noop
            }
        });

        var actualErrorHandler = this.sandbox.spy();
        errorHandler.returns(actualErrorHandler);
        ok(controller.ajax());
        ok(makeRequest.called);
        ok(errorHandler.calledWithExactly(JIRA.Workflows.Scheme.Common.refreshErrorDialog));
        ok(controller.requests);
        deferred.rejectWith(window, []);
        ok(actualErrorHandler.called);
        ok(!controller.requests);
        ok(!controller.model.setDraft.called);
    });

    test("Controller constructor binds to bus events", function () {
        var controller = new Controller({
            pageView: jQuery.noop,
            model: jQuery.noop,
            issueTypeModel: jQuery.noop,
            viewEventBus: {
                on: this.sandbox.spy()
            }
        });
        controller.viewEventBus.on.calledWith("removeIssueType", controller.removeIssueType, controller);
        controller.viewEventBus.on.calledWith("removeWorkflow", controller.removeWorkflow, controller);
        controller.viewEventBus.on.calledWith("assignIssueTypes", controller.assignIssueTypes, controller);
        controller.viewEventBus.on.calledWith("addWorkflow", controller.addWorkflow, controller);
        expect(0);
    });

    module("JIRA.Workflows.Scheme.Editor.Model - Tests", {
        teardown: function () {
            this.sandbox.restore();
        },
        setup: function () {
            this.sandbox = sinon.sandbox.create();
            this.bus = _.clone(Backbone.Events);
        }
    });

    test("WorkflowSchemeModel sets correctly", function () {
        var model = new WorkflowSchemeModel();
        var data = {
            name: "WorkflowSchemeModel",
            mappings: [
                {name: "Workflow1"},
                {name: "Workflow2"}
            ],
            issueTypes: "name"
        };

        ok(model.load(data), "Returns true if changed.");
        deepEqual(model.workflows.toJSON(), data.mappings, "Mappings should be in its own collection.");
        deepEqual(model.toJSON(), {name: "WorkflowSchemeModel"}, "Workflow Scheme Model correctly copied.");
    });


    test("WorkflowSchemeModel load", function () {
        var workflowSchemeModel = new WorkflowSchemeModel({});

        var compare = function (expected) {
            var actual = workflowSchemeModel.toJSON();
            actual.mappings = workflowSchemeModel.workflows.toJSON();
            ok(_.isEqual(expected, actual));
        };

        var data = {
            mappings: [
                {
                    name: 'workflow1',
                    issueTypes: []
                },
                {
                    name: 'workflow2',
                    issueTypes: []
                }
            ],
            name: '',
            description: '',
            draftScheme: '',
            shared: '',
            totalWorkflows: ''
        };
        ok(workflowSchemeModel.load(data));
        ok(!workflowSchemeModel.load(data));
        compare(data);

        data.mappings[1].name = 'workflow3';
        ok(workflowSchemeModel.load(data));
        ok(!workflowSchemeModel.load(data));
        compare(data);

        data.name = 'something';
        ok(workflowSchemeModel.load(data));
        ok(!workflowSchemeModel.load(data));
        compare(data);

        data.ignoredProperty = 'this will not matter';
        ok(!workflowSchemeModel.load(data));
        compare(data);

        data.mappings[1].issueTypes = ['1', '2'];
        ok(workflowSchemeModel.load(data));
        ok(!workflowSchemeModel.load(data));
        compare(data);

        data.mappings[1].issueTypes = ['2', '1'];
        ok(!workflowSchemeModel.load(data));
        data.mappings[1].issueTypes = ['1', '2'];
        compare(data);

        data.mappings[1].issueTypes = ['1'];
        ok(workflowSchemeModel.load(data));
        ok(!workflowSchemeModel.load(data));
        compare(data);

        data.mappings.reverse();
        ok(!workflowSchemeModel.load(data));
        data.mappings.reverse();
        compare(data);
    });

    test("WorkflowSchemeModel removeIssueType on non-default workflow", function () {
        var model = new WorkflowSchemeModel();
        model.workflows.add({
            'default': false,
            issueTypes: ['1', '2', '3'],
            name: 'workflow'
        });

        model.removeIssueType("workflow", '1');

        deepEqual(model.workflows.get("workflow").toJSON(), {
            'default': false,
            issueTypes: ['2', '3'],
            name: 'workflow'
        });

        model.removeIssueType("workflow", '2');

        deepEqual(model.workflows.get("workflow").toJSON(), {
            'default': false,
            issueTypes: ['3'],
            name: 'workflow'
        });

        model.removeIssueType("workflow", '3');

        ok(!model.workflows.get("workflow"));
    });

    test("WorkflowSchemeModel removeIssueType on default workflow", function () {
        var model = new WorkflowSchemeModel();
        model.workflows.add({
            'default': true,
            issueTypes: ['1', '2', '3'],
            name: 'workflow'
        });

        model.removeIssueType("workflow", '1');

        deepEqual(model.workflows.get("workflow").toJSON(), {
            'default': true,
            issueTypes: ['2', '3'],
            name: 'workflow'
        });

        model.removeIssueType("workflow", '2');

        deepEqual(model.workflows.get("workflow").toJSON(), {
            'default': true,
            issueTypes: ['3'],
            name: 'workflow'
        });

        model.removeIssueType("workflow", '3');

        deepEqual(model.workflows.get("workflow").toJSON(), {
            'default': true,
            issueTypes: [],
            name: 'workflow'
        });
    });

    test("WorkflowSchemeModel removeWorkflow on non-default workflow", function () {
        var workflowName = 'workflow';

        var model = new WorkflowSchemeModel();
        model.workflows.add({
            'default': false,
            name: workflowName
        });

        var workflowModel = model.workflows.get(workflowName);
        workflowModel.destroy = this.sandbox.spy();

        ok(model.removeWorkflow("workflow") === undefined);
        ok(workflowModel.destroy.calledWith());
    });

    test("WorkflowSchemeModel removeWorkflow on default workflow with other workflow", function () {
        var workflowName = 'workflow';
        var newDefaultName = 'workflowNewDefault';

        var model = new WorkflowSchemeModel();
        model.workflows.add({
            'default': true,
            name: workflowName
        });

        model.workflows.add({
            'default': false,
            name: newDefaultName
        });

        var workflowModel = model.workflows.get(workflowName);
        this.sandbox.spy(workflowModel, 'destroy');

        ok(model.removeWorkflow(workflowName) === newDefaultName);
        deepEqual(model.workflows.get(newDefaultName).toJSON(), {
            'default': true,
            name: newDefaultName
        });
        ok(workflowModel.destroy.calledWith());
    });

    test("WorkflowSchemeModel removeWorkflow on default workflow with no other workflow", function () {
        var workflowName = 'workflow';

        var model = new WorkflowSchemeModel();
        model.workflows.add({
            'default': true,
            name: workflowName
        });

        var workflowModel = model.workflows.get(workflowName);
        this.sandbox.spy(workflowModel, 'destroy');

        ok(model.removeWorkflow("workflow") === undefined);
        ok(workflowModel.destroy.calledWith());
    });

    test("WorkflowSchemeModel assignIssueTypes do not reassign default", function () {
        var model = new WorkflowSchemeModel();
        model.workflows.add({
            'default': false,
            issueTypes: ["1"],
            name: "workflow"
        });
        model.workflows.add({
            'default': true,
            issueTypes: ["2"],
            name: "workflow2"
        });
        model.assignIssueTypes("workflow", ["2"], false);
        deepEqual(model.workflows.get("workflow").toJSON(), {
            'default': false,
            issueTypes: ["1", "2"],
            name: "workflow"
        });
        deepEqual(model.workflows.get("workflow2").toJSON(), {
            'default': true,
            issueTypes: [],
            name: "workflow2"
        });
    });

    test("WorkflowSchemeModel assignIssueTypes do reassign default", function () {
        var model = new WorkflowSchemeModel();
        model.workflows.add({
            'default': false,
            issueTypes: ["1"],
            name: "workflow"
        });
        model.workflows.add({
            'default': true,
            issueTypes: ["2"],
            name: "workflow2"
        });
        model.workflows.add({
            'default': false,
            issueTypes: ["3", "4"],
            name: "workflow3"
        });
        model.assignIssueTypes("workflow", ["3", "2"], true);

        deepEqual(model.workflows.get("workflow").toJSON(), {
            'default': true,
            issueTypes: ["1", "3", "2"],
            name: "workflow"
        });
        ok(!model.workflows.get("workflow2"));
        deepEqual(model.workflows.get("workflow3").toJSON(), {
            'default': false,
            issueTypes: ["4"],
            name: "workflow3"
        });
    });

    test("WorkflowSchemeModel addWorkflow do not reassign default", function () {
        var model = new WorkflowSchemeModel();
        model.workflows.add({
            'default': false,
            issueTypes: ["1"],
            name: "workflow"
        });
        model.workflows.add({
            'default': true,
            issueTypes: ["2"],
            name: "workflow2"
        });
        model.addWorkflow({
            name: "workflow3",
            issueTypes: ["2", "4"],
            'default': false,
            displayName: 'workflow3',
            description: 'description'
        });

        deepEqual(model.workflows.get("workflow").toJSON(), {
            'default': false,
            issueTypes: ["1"],
            name: "workflow"
        });
        deepEqual(model.workflows.get("workflow2").toJSON(), {
            'default': true,
            issueTypes: [],
            name: "workflow2"
        });
        deepEqual(model.workflows.get("workflow3").toJSON(), {
            'default': false,
            issueTypes: ["2", "4"],
            name: "workflow3",
            displayName: 'workflow3',
            description: 'description'
        });
    });

    test("WorkflowSchemeModel addWorkflow do reassign default", function () {
        var model = new WorkflowSchemeModel();
        model.workflows.add({
            'default': false,
            issueTypes: ["1"],
            name: "workflow"
        });
        model.workflows.add({
            'default': true,
            issueTypes: ["2"],
            name: "workflow2"
        });
        model.addWorkflow({
            name: "workflow3",
            issueTypes: ["2", "4"],
            'default': true,
            displayName: 'workflow3',
            description: 'workflow3'
        });

        deepEqual(model.workflows.get("workflow").toJSON(), {
            'default': false,
            issueTypes: ["1"],
            name: "workflow"
        });
        deepEqual(model.workflows.get("workflow3").toJSON(), {
            'default': true,
            issueTypes: ["2", "4"],
            name: "workflow3",
            displayName: 'workflow3',
            description: 'workflow3'
        });
    });

    test("WorkflowSchemeModel discardDraft", function () {
        var model = new WorkflowSchemeModel();
        model.set({
            draftScheme: true,
            lastModifiedDate: "something",
            lastModifiedUser: "user",
            other: "other"
        });

        var originalWorkflows = [{name: 'workflow3'}, {name: 'workflow4'}];

        model.workflows.reset([{name: 'workflow1'}, {name: 'workflow2'}]);
        model.originalWorkflows.reset(originalWorkflows);

        model.discardDraft();

        deepEqual(model.toJSON(), {
            draftScheme: false,
            other: "other"
        }, "Cleared out the correct state.");

        deepEqual(model.workflows.toJSON(), originalWorkflows, "Original -> Current");
        deepEqual(model.originalWorkflows.length, 0, "Original Cleared");
    });

    test("IssueTypeCollection load", function () {
        var data1 = [{name: 'issuetype1'}, {name: 'issuetype2'}];
        var data2 = [{name: 'issuetype1'}, {name: 'issuetype3'}];

        var issueTypes = new IssueTypeCollection(data1);
        issueTypes.reset = this.sandbox.spy();
        ok(!issueTypes.load(data1));
        ok(!issueTypes.reset.called);

        ok(issueTypes.load(data2));
        ok(issueTypes.reset.called);
    });

    test("WorkflowModel equalsObject", function () {
        var attributes = {
            name: 'workflow',
            'default': true,
            issueTypes: ['1', '2']
        };
        var model = new WorkflowModel(attributes);
        ok(model.equalsObject(attributes));

        var attr1 = jQuery.extend(true, {}, attributes);
        ok(model.equalsObject(attr1));
        attr1['default'] = false;
        ok(!model.equalsObject(attr1));

        var attr2 = jQuery.extend(true, {}, attributes);
        ok(model.equalsObject(attr2));
        attr2.name = 'different name';
        ok(!model.equalsObject(attr2));

        var attr3 = jQuery.extend(true, {}, attributes);
        ok(model.equalsObject(attr3));
        attr3.issueTypes.reverse();
        ok(model.equalsObject(attr3));
        attr3.issueTypes.pop();
        ok(!model.equalsObject(attr3));
    });

    test("WorkflowCollection equalsArray", function () {
        var mappings = [
            {
                name: 'workflow1',
                'default': true,
                issueTypes: ['1', '2']
            },
            {
                name: 'workflow2',
                'default': false,
                issueTypes: ['3', '4']
            }
        ];

        var model = new WorkflowCollection(mappings);
        ok(model.equalsArray(mappings));

        var map1 = jQuery.extend(true, [], mappings);
        map1.pop();
        ok(!model.equalsArray(map1));

        var map2 = jQuery.extend(true, [], mappings);
        map2.push({
            name: 'workflow3',
            'default': false,
            issueTypes: ['5']
        });
        ok(!model.equalsArray(map2));

        var map3 = jQuery.extend(true, [], mappings);
        map3[0].issueTypes.reverse();
        ok(model.equalsArray(map3));
        map3[0].issueTypes.pop();
        ok(!model.equalsArray(map3));
    });

    module("JIRA.Workflows.Scheme.Editor.View - Tests", {
        teardown: function () {
            this.sandbox.restore();
        },
        setup: function () {
            this.sandbox = sinon.sandbox.create();
            this.Templates = TemplatesEditor;
            this.bus = _.clone(Backbone.Events);
            this.sandbox.spy(AJS, "format");
        }
    });

    test("WorkflowSchemeHeaderView", function () {

        var project1 = {key: 'ONE', name: 'Project One', id: 1};
        var project2 = {key: 'TWO', name: 'Project Two', id: 2};
        var workflow1 = {name: 'workflow1'};
        var workflow2 = {name: 'workflow2'};

        var workflowScheme1Data = {
            name: 'Scheme2',
            draftScheme: false,
            shared: {
                sharedWithProjects: [project1, project2],
                sharedWithIssueTypes: [],
                totalProjectsCount: 3,
                hiddenProjectsCount: 1
            },
            totalWorkflows: 3
        };
        var workflowScheme2Data = {
            name: 'Scheme1',
            draftScheme: true,
            shared: {
                sharedWithProjects: [],
                sharedWithIssueTypes: [],
                totalProjectsCount: 0,
                hiddenProjectsCount: 0
            },
            lastModifiedDate: "Last Date",
            lastModifiedUser: {displayName: "User Display", name: "User"},
            currentUser: "User"
        };

        var workflowScheme = new Backbone.Model(workflowScheme1Data);
        var workflows = new WorkflowCollection([workflow1, workflow2]);
        var issueTypes = new IssueTypeCollection([]);

        var context = AJS.test.mockableModuleContext();
        var eventsStub = this.stub(Events);
        context.mock("jira/util/events", eventsStub);

        var discardDialog = this.stub();
        context.mock("jira-project-config/workflows/dialogs/discard-draft", discardDialog);
        var WorkflowSchemeHeaderView = context.require("jira-project-config/workflows/scheme/editor/views/workflow-scheme-header");

        var schemeView = new WorkflowSchemeHeaderView({
            bus: this.bus,
            issueTypes: issueTypes,
            workflows: workflows,
            editable: true,
            model: workflowScheme
        });

        var events = {
            events: [],
            register: function (bus) {
                bus.on('all', function () {
                    this.events.push(_.toArray(arguments));
                }, this);
            },
            clear: function () {
                this.events = [];
            },
            calledWith: function () {
                var args = _.toArray(arguments);
                return _.any(this.events, function (event) {
                    return _.isEqual(event, args);
                });
            }
        };

        var Parser = function (schemeView) {
            this.view = schemeView;
            this.selectors = {
                publish: "#publish-draft",
                discard: "#discard-draft",
                viewOriginal: "#view-original",
                viewDraft: "#view-draft",
                addWorkflow: "#add-workflow-dropdown li:has(#add-workflow), #add-workflow-dropdown-trigger",
                schemeName: "#workflow-scheme-name",
                draftStatus: ".status-draft",
                sharedBy: ".shared-by",
                projects: "#project-config-workflow-scheme-shared li a"
            };
            this.name = function () {
                return this.view.$(this.selectors.schemeName).val();
            };
            this.draft = function () {
                return this.view.$(this.selectors.draftStatus).length > 0;
            };
            this.shared = function () {
                if (this.view.$(this.selectors.sharedBy).length) {
                    var result = [];
                    this.view.$(this.selectors.projects).each(function () {
                        result.push(jQuery.trim(jQuery(this).text()));
                    });
                    return result;
                } else {
                    //return falsy
                    return null;
                }
            };
            this.getDraftInfo = function () {
                var draftInfo = this.findOrNull(this.selectors.draftStatus);
                if (!draftInfo) {
                    return null;
                }
                var title = draftInfo.attr('title');
                if (!title) {
                    return null;
                }

                var formatCall = AJS.format.lastCall;
                if (formatCall.calledWith("admin.workflowscheme.last.edited.by.you.at")) {
                    return {
                        you: true,
                        time: formatCall.args[1]
                    };
                } else if (formatCall.calledWith("admin.workflowscheme.last.edited.by.user.at")) {
                    return {
                        you: false,
                        name: formatCall.args[1],
                        time: formatCall.args[2]
                    };
                } else {
                    throw new Error("Title did not match expected info.");
                }
            };
            this.canPublish = function () {
                return this.findOrNull(this.selectors.publish) !== null;
            };
            this.canDiscard = function () {
                return this.findOrNull(this.selectors.discard) !== null;
            };
            this.canViewOriginal = function () {
                return this.findOrNull(this.selectors.viewOriginal) !== null;
            };
            this.canViewDraft = function () {
                return this.findOrNull(this.selectors.viewDraft) !== null;
            };
            this.canAddWorkflow = function () {
                var $addWorkflow = this.view.$(this.selectors.addWorkflow);
                return $addWorkflow.length > 0 && !$addWorkflow.hasClass('disabled');
            };
            this.viewOriginal = function () {
                return this.click(this.selectors.viewOriginal, "View Original");
            };
            this.viewDraft = function () {
                return this.click(this.selectors.viewDraft, "View Draft");
            };
            this.discard = function () {
                return this.click(this.selectors.discard, "Discard Draft");
            };
            this.findOrNull = function (selector) {
                var $find = this.view.$(selector);
                if ($find.length === 0) {
                    return null;
                } else {
                    return $find;
                }
            };
            this.click = function (sel, operation) {
                var original = this.findOrNull(sel);
                if (!original) {
                    throw new Error("Unable to perform '" + operation + "' link not present.");
                }
                original.click();
                return this;
            };
        };

        var assertHeader = function (scheme, workflows, original, parser) {
            equal(parser.name(), scheme.get('name'), "Workflow has correct name?");

            var draft = scheme.get('draftScheme');
            if (!draft) {
                ok(!parser.draft(), "Workflow scheme is not draft?");
                ok(!parser.canPublish(), "Can't publish inactive ?");
                ok(!parser.canDiscard(), "Can't discard inactive ?");
                ok(!parser.canViewOriginal(), "Can't see original of inactive ?");
                ok(!parser.canViewDraft(), "Can't see original of inactive ?");
                ok(!parser.getDraftInfo(), "No draft info.");
            } else {
                if (original) {
                    ok(!parser.draft(), "Workflow original should not be draft?");
                    ok(!parser.canPublish(), "Can't publish from original screen.");
                    ok(!parser.canDiscard(), "Can't discard from original screen");
                    ok(!parser.canViewOriginal(), "Can't view original from original screen.");
                    ok(parser.canViewDraft(), "Can view draft from original screen.");
                    ok(!parser.getDraftInfo(), "No draft info.");
                } else {
                    var expectedDraftInfo = null;
                    var user = scheme.get('lastModifiedUser');
                    var date = scheme.get('lastModifiedDate');
                    if (user && date) {
                        var you = user.name === scheme.get('currentUser');
                        expectedDraftInfo = {
                            you: you,
                            time: date
                        };
                        if (!you) {
                            expectedDraftInfo.name = user.displayName;
                        }
                    }

                    deepEqual(parser.getDraftInfo(), expectedDraftInfo, "Draft Info correct?");
                    ok(parser.draft(), "Workflow should not be draft?");
                    ok(parser.canPublish(), "Can publish from draft screen.");
                    ok(parser.canDiscard(), "Can discard from draft screen");
                    ok(parser.canViewOriginal(), "Can view original from draft screen.");
                    ok(!parser.canViewDraft(), "Can't view draft from draft screen.");
                }
            }

            var shared = scheme.get('shared');
            if (shared.sharedWithProjects.length > 0) {
                deepEqual(parser.shared(), _.pluck(shared.sharedWithProjects, 'name'), "Sharing information correct");
            } else {
                ok(!parser.shared(), "Not shared.");
            }

            if ((draft && original) || workflows.length >= scheme.get('totalWorkflows')) {
                ok(!parser.canAddWorkflow(), "Should not be able to add workflow.");
            } else {
                ok(parser.canAddWorkflow(), "Should be able to add workflow");
            }
        };

        //View non-draft.
        var p = new Parser(schemeView.render());
        equal(eventsStub.trigger.callCount, 1, "Trigged NEW_CONTEXT_ADDED.");
        ok(eventsStub.trigger.lastCall.calledWith(Types.NEW_CONTENT_ADDED, [schemeView.$el, Reasons.workflowHeaderReady]), "Content added triggered.");
        assertHeader(workflowScheme, workflows, false, p);

        //Add a workflow.
        workflows.add({name: 'workflow3'});
        assertHeader(workflowScheme, workflows, false, p);
        equal(eventsStub.trigger.callCount, 1, "Don't trigger NEW_CONTEXT_ADDED.");

        //Remove a workflow
        workflows.remove(workflows.get('workflow3'));
        assertHeader(workflowScheme, workflows, false, p);
        equal(eventsStub.trigger.callCount, 1, "Don't trigger NEW_CONTEXT_ADDED.");

        //We are going to view a draft, change the data and make sure we are viewing the draft.
        workflowScheme.set(workflowScheme2Data);
        equal(eventsStub.trigger.callCount, 2, "Trigged NEW_CONTEXT_ADDED.");
        ok(eventsStub.trigger.lastCall.calledWith(Types.NEW_CONTENT_ADDED, [schemeView.$el, Reasons.workflowHeaderReady]), "Content added triggered.");
        assertHeader(workflowScheme, workflows, false, p);

        events.register(this.bus);
        //View the original.
        p.viewOriginal();
        equal(eventsStub.trigger.callCount, 3, "Trigged NEW_CONTEXT_ADDED.");
        ok(eventsStub.trigger.lastCall.calledWith(Types.NEW_CONTENT_ADDED, [schemeView.$el, Reasons.workflowHeaderReady]), "Content added triggered.");
        ok(events.calledWith('viewOriginal'), "View Original event triggered.");
        assertHeader(workflowScheme, workflows, true, p);

        //View the draft.
        events.clear();
        p.viewDraft();
        equal(eventsStub.trigger.callCount, 4, "Trigged NEW_CONTEXT_ADDED.");
        ok(eventsStub.trigger.lastCall.calledWith(Types.NEW_CONTENT_ADDED, [schemeView.$el, Reasons.workflowHeaderReady]), "Content added triggered.");
        ok(events.calledWith('viewDraft'), "View Draft event triggered.");
        assertHeader(workflowScheme, workflows, false, p);

        //Should not be able to add workflow now.
        workflows.add({name: 'workflow5'});
        assertHeader(workflowScheme, workflows, false, p);

        //View the original.
        events.clear();
        p.viewOriginal();
        equal(eventsStub.trigger.callCount, 5, "Trigged NEW_CONTEXT_ADDED.");
        ok(eventsStub.trigger.lastCall.calledWith(Types.NEW_CONTENT_ADDED, [schemeView.$el, Reasons.workflowHeaderReady]), "Content added triggered.");
        ok(events.calledWith('viewOriginal'), "View Original event triggered.");
        assertHeader(workflowScheme, workflows, true, p);

        //View the draft.
        events.clear();
        p.viewDraft();
        //Change the draft owner to someone else.
        workflowScheme.set("currentUser", "notMe");
        equal(eventsStub.trigger.callCount, 7, "Trigged NEW_CONTEXT_ADDED.");
        ok(eventsStub.trigger.lastCall.calledWith(Types.NEW_CONTENT_ADDED, [schemeView.$el, Reasons.workflowHeaderReady]), "Content added triggered.");
        ok(events.calledWith('viewDraft'), "View Draft event triggered.");
        assertHeader(workflowScheme, workflows, false, p);

        //Should now be able to remove workflow again.
        workflows.remove(workflows.get('workflow5'));
        assertHeader(workflowScheme, workflows, false, p);
        equal(eventsStub.trigger.callCount, 7, "Don't trigger NEW_CONTEXT_ADDED.");

        //Should now be able to discard the draft.
        var callback = this.sandbox.spy();
        events.clear();
        discardDialog.yields(callback);
        p.discard();

        ok(events.calledWith("discardDraft"), "Discard Draft called.");
        assertHeader(workflowScheme, workflows, false, p);
        ok(callback.calledWith(), "Triggered the callback.");

    });

    test("WorkflowSchemeHeaderView assignAfterAdd", function () {
        var context = AJS.test.mockableModuleContext();
        var AssignIssueTypes = this.sandbox.stub();
        context.mock("jira-project-config/workflows/dialogs/assign-issue-types", AssignIssueTypes);
        var WorkflowSchemeHeaderView = context.require("jira-project-config/workflows/scheme/editor/views/workflow-scheme-header");

        var assignedWorkflows = [
            {
                name: "workflow1",
                'default': true
            }
        ];

        var view = new WorkflowSchemeHeaderView({
            model: {
                workflows: new WorkflowCollection(assignedWorkflows),
                on: jQuery.noop
            },
            workflows: {
                on: jQuery.noop,
                toJSON: jQuery.noop
            },
            issueTypes: {
                toJSON: jQuery.noop
            }
        });

        view.assignAfterAdd({});
        ok(AssignIssueTypes.called);
    });

    test("WorkflowSchemeHeaderView canAddWorkflow", function () {
        var view = new WorkflowSchemeHeaderView({
            model: {
                on: jQuery.noop,
                get: this.sandbox.stub()
            },
            workflows: {
                on: jQuery.noop,
                length: 1
            }
        });

        view.model.get.withArgs("totalWorkflows").returns(2);
        ok(view.canAddWorkflow());

        view.model.get.withArgs("totalWorkflows").returns(1);
        ok(!view.canAddWorkflow());
    });

    test("WorkflowView initialize", function () {
        var options = {
            issueTypes: 'something',
            model: {
                on: this.sandbox.spy()
            }
        };
        var view = new WorkflowView(options);
        strictEqual(view.issueTypes, options.issueTypes);
        ok(options.model.on.calledWithExactly('change', view.render, view));
        ok(options.model.on.calledWithExactly('destroy', view.unrender, view));
    });

    test("WorkflowView template", function () {
        var template = this.sandbox.stub(this.Templates, "WorkflowView");

        template.returnsArg(0);

        var data = {
            someKey: 'some data'
        };

        var options = {
            issueTypes: '',
            model: {
                on: jQuery.noop
            }
        };

        var view = new WorkflowView(options);
        strictEqual(view.template(data), data);
        ok(template.calledWithExactly(data));
    });

    test("WorkflowView render", function () {
        var json = {
            issueTypes: [
                'issueType1'
            ]
        };
        var name = 'some name';

        var options = {
            issueTypes: new IssueTypeCollection([
                {
                    id: 'issueType1',
                    name: 'issueType1'
                },
                {
                    id: 'issueType2',
                    name: 'issueType2'
                }
            ]),
            model: {
                get: this.sandbox.stub(),
                on: jQuery.noop,
                toJSON: this.sandbox.stub(),
                collection: {
                    length: 2
                }
            },
            editable: true
        };

        var view = new WorkflowView(options);

        var html = "some html";
        view.template = this.sandbox.stub();
        view.template.returns(html);
        view.$el.html = this.sandbox.spy();

        options.model.get.returns(name);
        options.model.toJSON.returns(json);

        var retVal = view.render();
        ok(view.template.calledWithExactly({
            issueTypes: [
                {
                    id: 'issueType1',
                    name: 'issueType1'
                }
            ],
            showAssign: false,
            editable: true
        }));
        ok(view.$el.html.calledWithExactly(html));

        //we do not use strictEqal because it attempts to convert values to JSON which is not possible in this case
        ok(view === retVal, "Invalid view");
    });

    test("WorkflowView viewWorkflowText", function () {
        var Common = this.sandbox.stub(JIRA.Workflows.Scheme.Common, "showWorkflowTextView");
        var id = 'some id';
        var options = {
            model: {
                id: id,
                on: jQuery.noop
            }
        };
        var view = new WorkflowView(options);
        var e = {
            preventDefault: this.sandbox.spy()
        };

        view.viewWorkflowText(e);
        ok(e.preventDefault.called);
        ok(Common.calledWithExactly({
            workflowName: id
        }));
    });

    test("WorkflowView removeAllIssueTypes", function () {
        var options = {
            model: {
                id: 'workflow',
                on: jQuery.noop,
                destroy: this.sandbox.spy()
            },
            bus: {
                trigger: this.sandbox.spy()
            }
        };
        var view = new WorkflowView(options);
        var e = {
            preventDefault: this.sandbox.spy()
        };
        view.removeAllIssueTypes(e);
        ok(e.preventDefault.called);
        ok(options.bus.trigger.calledWith("removeWorkflow", options.model.id));
    });

    test("WorkflowView removeIssueType", function () {
        var options = {
            model: {
                id: 'workflow',
                on: jQuery.noop,
                set: this.sandbox.spy(),
                get: this.sandbox.stub()
            },
            bus: {
                trigger: this.sandbox.spy()
            }
        };
        var view = new WorkflowView(options);
        var e = {
            preventDefault: this.sandbox.spy(),
            currentTarget: "<span data-issue-type='1'></span>"
        };
        view.removeIssueType(e);
        ok(e.preventDefault.called);
        ok(options.bus.trigger.calledWith("removeIssueType", options.model.id, "1"));
    });

    test("WorkflowSchemeView", function () {
        function headerViewCotr(options) {
            this.options = options;
            this.render = function () {
                this.$el = jQuery("<div>").attr("id", "header");
                return this;
            };
        }

        function workflowViewCotr(options) {

            this.model = options.model;
            this.name = this.model.id;
            this.options = options;

            this.render = function () {
                var $row = jQuery("<tr>").attr("id", "row-" + this.name);
                jQuery("<td>").text(this.name).data("editable", this.options.editable).appendTo($row);
                this.$el = $row;
                return this;
            };
            this.unrender = function () {
                this.$el.remove();
            };
        }

        var testOptions = {
            issueTypes: new IssueTypeCollection([]),
            model: {
                workflows: new WorkflowCollection([
                    {
                        name: 'workflow1'
                    },
                    {
                        name: 'workflow2'
                    }
                ]),
                originalWorkflows: new WorkflowCollection([
                    {
                        name: 'workflow4'
                    },
                    {
                        name: 'workflow3'
                    }
                ]),
                get: jQuery.noop
            },
            bus: this.bus,
            headerViewCotr: headerViewCotr,
            workflowViewCotr: workflowViewCotr
        };

        var workflows = testOptions.model.workflows;
        var view = new WorkflowSchemeView(testOptions);
        view.render();

        var assertWorkflows = function (expectedWorkflows, editable) {
            var $tds = view.$("td");
            equal(expectedWorkflows.length, $tds.length, "Right number of expectedWorkflows?");

            for (var i = 0; i < expectedWorkflows.length; i++) {
                var $td = jQuery($tds[i]);
                var workflow = expectedWorkflows[i];
                equal($td.text(), workflow, "Expected workflow[" + i + "]  to be " + workflow);
                equal($td.data("editable"), editable, "Expected workflow[" + i + "] to be " + (!editable ? "NOT " : "" ) + " editable.");
            }
        };

        //Make sure it renders the screen correctly.
        equal(view.$el.find("#header").length, 1, "Is header rendered?");
        assertWorkflows(["workflow1", "workflow2"], true);

        //View original should show the original.
        this.bus.trigger("viewOriginal");
        assertWorkflows(["workflow4", "workflow3"], false);

        //View draft should flick back.
        this.bus.trigger("viewDraft");
        assertWorkflows(["workflow1", "workflow2"], true);

        //Adding a workflow should render it.

        workflows.add({name: 'workflow7'});
        assertWorkflows(["workflow1", "workflow2", "workflow7"], true);

        //View original should show original.
        this.bus.trigger("viewOriginal");
        assertWorkflows(["workflow4", "workflow3"], false);

        //View draft should flick back.
        this.bus.trigger("viewDraft");
        assertWorkflows(["workflow1", "workflow2", "workflow7"], true);

        //Rest should redraw the UI.
        workflows.reset([{name: 'workflow8'}, {name: 'workflow9'}]);
        assertWorkflows(["workflow8", "workflow9"], true);
    });

    test("Page view", function () {
        var context = AJS.test.mockableModuleContext();

        var stubTemplates = this.sandbox.stub(TemplateMessages);
        context.mock("jira-project-config/workflows/scheme/editor/templates", stubTemplates);
        var errorMessage = stubTemplates.errorMessage;

        var $el = "something";
        var schemeOptions;
        var finalize = this.sandbox.spy();
        var mockWorkflowSchemeView = function (options) {
            schemeOptions = options;
            this.render = function () {
                this.$el = $el;
                return this;
            };
            this.finalize = finalize;
        };
        context.mock("jira-project-config/workflows/scheme/editor/views/workflow-scheme", mockWorkflowSchemeView);

        var Page = context.require("jira-project-config/workflows/scheme/editor/views/page");

        var bus = {on: jQuery.noop};
        var view = new Page({
            bus: bus
        });

        view.$el = {
            removeClass: this.sandbox.stub(),
            html: this.sandbox.spy(),
            toggleClass: this.sandbox.spy(),
            closest: this.sandbox.stub()
        };

        view.$el.removeClass.returns(view.$el);
        view.$el.closest.returns(view.$el);

        //.html()
        var html = "some html";
        view.html(html);
        ok(view.$el.removeClass.calledWithExactly("workflowscheme-editor-loading"));
        ok(view.$el.html.calledWithExactly(html));

        //.renderError
        var message = "something";
        view.html = this.sandbox.spy();
        errorMessage.returns(message);
        view.renderError(message);
        ok(errorMessage.calledWithExactly({
            message: message
        }));
        ok(view.html.calledWithExactly(message));

        //.renderModel
        var model = {
            workflows: {
                on: this.sandbox.spy(),
                length: 1
            },
            on: jQuery.noop,
            get: jQuery.noop
        };
        var issueTypes = "issueTypes";
        view.html = this.sandbox.spy();
        view.renderModel(model, issueTypes);
        ok(!finalize.called);
        deepEqual(schemeOptions, {
            model: model,
            issueTypes: issueTypes,
            bus: bus
        });
        ok(view.html.calledWithExactly($el));

        //rerender everything
        view.renderModel(model, issueTypes);
        ok(model.workflows.on.calledWith("add remove"));
        ok(model.workflows.on.calledWith("reset"));
        ok(view.$el.toggleClass.calledWith("multiple-workflows", false));
        ok(finalize.called);
    });
});
