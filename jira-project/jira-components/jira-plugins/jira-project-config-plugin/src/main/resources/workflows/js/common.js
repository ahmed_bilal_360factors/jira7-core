(function ($) {

    var Common = AJS.namespace("JIRA.Workflows.Scheme.Common");

    function clearHash(title) {
        if (history && history.replaceState) {
            history.replaceState(null, title, location.href.split('#')[0]);
        } else {
            location.hash = "";
        }
    }

    Common.refreshErrorDialog = function (error) {
        var dialog = new JIRA.FormDialog({
            id: "server-error-dialog",
            content: function (callback) {
                callback(JIRA.Templates.Workflows.Common.serverErrorRefreshModalDialog({
                    message: error
                }));
            }
        });
        dialog.handleCancel = $.noop;
        dialog.show();
        dialog.get$popupContent().find(".cancel.refresh").unbind().click(function (e) {
            e.preventDefault();
            AJS.reloadViaWindowLocation();
        });
    };

    Common.errorDialog = function (error) {
        new JIRA.FormDialog({
            id: "server-error-dialog",
            content: function (callback) {
                callback(JIRA.Templates.Common.serverErrorDialog({
                    message: error
                }));
            }
        }).show();
    };

    Common.smartAjaxErrorHandler = function (dialog) {
        return function (xhr, statusText, errorThrown, smartAjaxResult) {
            var data;
            try {
                data = smartAjaxResult.data && $.parseJSON(smartAjaxResult.data);
            } catch (e) {
                data = null;
            }

            if (data && data.errorMessages && data.errorMessages.length) {
                dialog(data.errorMessages.join(' '));
            } else {
                dialog(JIRA.SmartAjax.buildSimpleErrorContent(smartAjaxResult));
            }
        };
    };

    Common.showWorkflowTextView = function (data) {
        new JIRA.FormDialog({
            id: "workflow-text-view",
            widthClass: "large",
            content: function (ready) {
                JIRA.SmartAjax.makeRequest({
                    url: contextPath + "/rest/projectconfig/latest/workflow",
                    data: data,
                    contentType: 'application/json',
                    success: function (data) {
                        ready(JIRA.Templates.Workflows.Common.text(data));
                    },
                    error: Common.smartAjaxErrorHandler(Common.refreshErrorDialog)
                });
            }
        }).show();
    };

    Common.checkMigrationSuccess = function () {
        var draftMigrationSuccess = parseUri(location).anchor === "draftMigrationSuccess";
        if (draftMigrationSuccess) {
            clearHash("Workflow Page");
            JIRA.Messages.showSuccessMsg(AJS.I18n.getText("admin.selectworkflowscheme.draft.published"));
        }
    };

    Common.checkWorkflow = function () {
        var anchor = parseUri(location).anchor;
        if (anchor && anchor.indexOf("workflowName=") === 0) {
            clearHash("Workflow Page");
            return decodeURIComponent(anchor.split("=")[1].replace(/\+/g, " "));
        }
    };

})(AJS.$);
