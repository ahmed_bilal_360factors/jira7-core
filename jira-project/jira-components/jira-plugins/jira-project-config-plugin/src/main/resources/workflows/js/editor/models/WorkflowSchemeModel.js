define("jira-project-config/workflows/scheme/editor/models/workflow-scheme", ["require"], function(require){
    "use strict";

    var _ = require('underscore');
    var Events = require('jira/util/events');
    var Backbone = require('jira-project-config/backbone');
    var WorkflowCollection = require('jira-project-config/workflows/scheme/editor/models/workflow-collection');
    var WorkflowModel = require('jira-project-config/workflows/scheme/editor/models/workflow');

    return Backbone.Model.extend({

        initialize: function () {
            this.workflows = new WorkflowCollection();
            this.originalWorkflows = new WorkflowCollection();
        },

        load: function (data) {
            data = _.clone(data) || {};

            var result = !this.compareModelAttributes(data, [
                'name',
                'description',
                'draftScheme',
                'shared',
                'totalWorkflows'
            ]);

            if (this.workflows.load(data.mappings || [])) {
                result = true;
            }
            this.originalWorkflows.load(data.originalMappings || []);

            //Cleanup the data before setting the model.
            delete data.mappings;
            delete data.issueTypes;
            delete data.originalMappings;

            this.set(data);

            return result;
        },

        discardDraft: function () {
            //Set the scheme to something we expect.
            this.set("draftScheme", false);
            this.unset("lastModifiedDate");
            this.unset("lastModifiedUser");

            this.workflows.reset(this.originalWorkflows.toJSON());
            this.originalWorkflows.reset([]);
        },

        removeIssueType: function (workflowName, issueType) {
            this.updateDraftState();
            var workflow = this.workflows.get(workflowName);
            var issueTypes = _.without(workflow.get('issueTypes'), issueType);
            if (issueTypes.length || workflow.get('default')) {
                workflow.set('issueTypes', issueTypes);
            } else {
                workflow.destroy();
            }
        },

        removeWorkflow: function (workflowName) {
            this.updateDraftState();
            var workflow = this.workflows.get(workflowName);
            var def = workflow.get('default');
            workflow.destroy();

            if (def) {
                var newDefault = this.workflows.at(0);
                if (newDefault) {
                    newDefault.set('default', true);
                    return newDefault.id;
                }
            }
            return undefined;
        },

        assignIssueTypes: function (workflowName, issueTypes, makeDefault) {
            this.updateDraftState();
            var model = this.workflows.get(workflowName);

            model.set({
                issueTypes: model.get('issueTypes').concat(issueTypes),
                'default': makeDefault || model.get('default')
            });
            this.updateWorkflows(model, issueTypes, makeDefault);
        },

        updateDraftState: function () {
            if (this.get('shared') && this.get('shared').sharedWithProjects.length && !this.get('draftScheme')) {
                this.set('draftScheme', true);
                Events.trigger("draftcreated");
            }
        },

        compareModelAttributes: function (data, attributes) {
            return _.all(attributes, function (value) {
                return _.isEqual(data[value], this.get(value));
            }, this);
        },

        addWorkflow: function (attr) {
            this.updateDraftState();
            var model = new WorkflowModel({
                name: attr.name,
                issueTypes: attr.issueTypes,
                'default': attr['default'],
                displayName: attr.displayName,
                description: attr.description
            });
            this.workflows.add(model);
            this.updateWorkflows(model, attr.issueTypes, attr['default']);
        },

        updateWorkflows: function (ignore, issueTypes, removeDefault) {
            var deleted = [];

            this.workflows.each(function (otherModel) {
                //Only run on differentModels.
                if (otherModel === ignore) {
                    return;
                }
                var newIssueTypes = _.difference(otherModel.get('issueTypes'), issueTypes);
                var newDefault = !removeDefault && otherModel.get('default');
                if (!newIssueTypes.length && !newDefault) {
                    deleted.push(otherModel);
                } else {
                    otherModel.set({issueTypes: newIssueTypes, 'default': newDefault});
                }
            });

            _.each(deleted, function (model) {
                model.destroy();
            });
        }
    });
});
