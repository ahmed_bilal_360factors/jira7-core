define("jira-project-config/workflows/tab/model/ignite-workflow", ["require"], function(require){
    "use strict";

    var Backbone = require('jira-project-config/workflows/tab/lib/backbone');
    var ProjectConfigUtils = require('jira-project-config/utils');
    var wrmContextPath = require('wrm/context-path')();

    return Backbone.Model.extend({
        defaults: {
            name: "",
            description: "",
            issueTypes: [],
            system: false,
            sharedBy: {},
            'default': false
        },
        idAttribute: 'name',
        url: function () {
            return wrmContextPath + "/rest/projectconfig/latest/draftworkflowscheme/" + ProjectConfigUtils.getKey();
        },
        parse: function () {
            return {};
        }
    });
});
