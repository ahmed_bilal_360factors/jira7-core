AJS.namespace("JIRA.AddWorkflowDialogView", null, require("jira-project-config/workflows/dialogs/add-workflow-dialog-view"));
AJS.namespace("JIRA.AddWorkflowDialogWorkflowView", null, require("jira-project-config/workflows/dialogs/add-workflow-dialog-workflow-view"));
AJS.namespace("JIRA.AssignIssueTypes", null, require("jira-project-config/workflows/dialogs/assign-issue-types"));
AJS.namespace("JIRA.DiscardDraft", null, require("jira-project-config/workflows/dialogs/discard-draft"));
