AJS.test.require("com.atlassian.jira.jira-project-config-plugin:application", function () {
    "use strict";

    var Application = require("jira-project-config/application");
    var Region = require("jira-project-config/utils/region");

    module("JIRA.ProjectConfig.Application");

    test("content region uses JIRA.ProjectConfig.Region", function () {
        ok(Application.content instanceof Region);
    });
});
