AJS.namespace("JIRA.ProjectConfig.Urls");

(function (_) {
    "use strict";

    JIRA.ProjectConfig.Application.module("Urls", function (module, application) {
        module.addInitializer(function () {
            var issueTypesUrlManager = new JIRA.ProjectConfig.Urls.IssueTypes.Manager();
            var base = _.bind(issueTypesUrlManager.base, issueTypesUrlManager);
            var viewWorkflow = _.bind(issueTypesUrlManager.viewWorkflow, issueTypesUrlManager);
            var editWorkflow = _.bind(issueTypesUrlManager.editWorkflow, issueTypesUrlManager);
            var viewFields = _.bind(issueTypesUrlManager.viewFields, issueTypesUrlManager);
            var fields = _.bind(issueTypesUrlManager.projectIssueTypeFields, issueTypesUrlManager);
            var workflow = _.bind(issueTypesUrlManager.projectIssueTypeWorkflow, issueTypesUrlManager);

            application.reqres.setHandlers({
                "urls:issueTypes:base": base,
                "urls:issueTypes:viewWorkflow": viewWorkflow,
                "urls:issueTypes:editWorkflow": editWorkflow,
                "urls:issueTypes:viewFields": viewFields,
                "urls:issueTypes:rest:fields": fields,
                "urls:issueTypes:rest:workflow": workflow
            });
        });

        module.addFinalizer(function () {
            application.reqres.removeHandler("urls:issueTypes:base");
            application.reqres.removeHandler("urls:issueTypes:viewWorkflow");
            application.reqres.removeHandler("urls:issueTypes:editWorkflow");
            application.reqres.removeHandler("urls:issueTypes:viewFields");
            application.reqres.removeHandler("urls:issueTypes:rest:fields");
            application.reqres.removeHandler("urls:issueTypes:rest:workflow");
        });
    });
}(_));
