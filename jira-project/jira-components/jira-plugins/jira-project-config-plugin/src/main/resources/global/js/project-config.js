(function ($) {
    AJS.$(function () {

        var $operations = AJS.$(".aui-toolbar");

        AJS.$("a.project-config-inlinedialog-trigger").each(function () {
            AJS.InlineDialog(
                AJS.$(this),
                "project-config-inlinedialog-" + AJS.escapeHtml(parseUri(this.href).queryKey.fieldId),
                this.href,
                {
                    width: 200
                }
            );
        });

        // we are using AUI tabs classes for CSS but get the Javascript behaviour also. Removing JavaScript AJS-638
        AJS.$(".tabs-menu a").unbind("click");

        var dropdown = new AJS.Dropdown({
            trigger: $operations.find(".aui-dd-trigger"),
            content: $operations.find(".aui-list"),
            alignment: AJS.RIGHT
        });

        var operationsMenuState = "closed";

        AJS.$(dropdown).bind({
            "showLayer": function () {
                operationsMenuState = "open";
            },
            "hideLayer": function () {
                operationsMenuState = "closed";
            }
        });

        dropdown.trigger().bind("mouseenter focus", function () {
            if (operationsMenuState === "willopen") {
                dropdown.show();
            }
        });

        var blockSelector = '.project-config-scheme-item';

        new JIRA.ToggleBlock({
            blockSelector: blockSelector,
            triggerSelector: ".project-config-toggle,.expander-icon",
            persist: false
        });

        $(blockSelector).on({
            'expandBlock': function () {
                AJS.$(this).find('.expander-icon').removeClass('aui-iconfont-collapsed').addClass('aui-iconfont-expanded');
            },
            'contractBlock': function () {
                AJS.$(this).find('.expander-icon').removeClass('aui-iconfont-expanded').addClass('aui-iconfont-collapsed');
            }
        });

        require([
            "jira-project-config/application",
            "jira-project-config/issuetypes/entities/module",
            "jira-project-config/error/module"
        ], function(
            Application,
            EntitiesModule,
            ErrorModule
        ) {
            Application.module("Error", ErrorModule);
            Application.module("IssueTypes.Entities", EntitiesModule);
            Application.module("Default", JIRA.ProjectConfig.Default.Module);
            Application.module("IssueTypes", JIRA.ProjectConfig.IssueTypes.Module);

            Application.start();
        });
    });
}(AJS.$));
