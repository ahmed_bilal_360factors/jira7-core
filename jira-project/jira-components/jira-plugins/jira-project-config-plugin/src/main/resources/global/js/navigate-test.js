AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:backbone-navigate"], function() {
    "use strict";
    var Backbone;

    module("JIRA.ProjectConfig.navigate", {
        setup: function () {
            Backbone = JIRA.ProjectConfig.Backbone;
            this.onbeforeunload = window.onbeforeunload;
            this.sandbox = sinon.sandbox.create();
            this._resetHistory();
            Backbone.history.start({
                root: "/foobar/"
            });
        },

        teardown: function () {
            this.sandbox.restore();
            window.onbeforeunload = this.onbeforeunload;
            this._resetHistory();
        },

        _resetHistory: function () {
            Backbone.history.stop();
            Backbone.history.handlers.length = 0;
        }
    });

    test("window.onbeforeunload opens confirm dialog", 3, function () {
        var location = {assign: sinon.stub()};
        this.sandbox.stub(window, "confirm").returns(false);
        window.onbeforeunload = function (e) {
            ok(e !== undefined);
            return "goodbye kind world";
        };

        JIRA.ProjectConfig.navigate("/foobar/matching", {location: location});
        ok(window.confirm.calledOnce);
        strictEqual(window.confirm.firstCall.args[0], "goodbye kind world");
    });

    test("window.onbeforeunload can cancel navigate", 1, function () {
        var location = {assign: sinon.stub()};
        this.sandbox.stub(window, "confirm").returns(false);
        window.onbeforeunload = function() {
            return "message";
        };

        JIRA.ProjectConfig.navigate("/foobar/missing", {location: location});
        ok(location.assign.called === false);
    });

    test("window.onbeforeunload can allow navigate", 2, function () {
        var location = {assign: sinon.stub()};
        this.sandbox.stub(window, "confirm").returns(true);
        window.onbeforeunload = function() {
            return "message";
        };

        JIRA.ProjectConfig.navigate("/foobar/missing", {location: location});
        ok(location.assign.calledOnce);

        window.onbeforeunload = undefined;
        JIRA.ProjectConfig.navigate("/foobar/missing", {location: location});
        ok(location.assign.calledTwice);
    });

    test("missing route", 2, function () {
        var location = sinon.stub();
        var navigate = this.sandbox.stub(Backbone.history, "navigate");

        location.assign = sinon.stub();
        JIRA.ProjectConfig.navigate("/foobar/missing", {location: location});

        equal(location.assign.callCount, 1);
        equal(navigate.callCount, 0);
    });

    test("matching route", 4, function () {
        var location = sinon.stub();
        var navigate = this.sandbox.stub(Backbone.history, "navigate");

        new Backbone.Router({
            routes: {
                'matching': function () {
                }
            }
        });

        location.assign = sinon.stub();
        JIRA.ProjectConfig.navigate("/foobar/matching", {location: location});

        equal(location.assign.callCount, 0);
        equal(navigate.callCount, 1);
        strictEqual(navigate.firstCall.args[0], "matching");
        deepEqual(navigate.firstCall.args[1], {trigger: true});
    });

    test("passthrough options", 3, function () {
        var navigate = this.sandbox.stub(Backbone.history, "navigate");

        new Backbone.Router({
            routes: {
                'matching': function () {
                }
            }
        });

        JIRA.ProjectConfig.navigate("/foobar/matching", {location: location});
        deepEqual(navigate.firstCall.args[1], {trigger: true});

        JIRA.ProjectConfig.navigate("/foobar/matching", {location: location, foo: "bar"});
        deepEqual(navigate.secondCall.args[1], {trigger: true, foo: "bar"});

        JIRA.ProjectConfig.navigate("/foobar/matching", {location: location, trigger: false});
        deepEqual(navigate.thirdCall.args[1], {trigger: false});
    });

    test("hasHandler verifies route", 2, function () {
        new Backbone.Router({
            routes: {
                'matching': function () {
                }
            }
        });

        ok(JIRA.ProjectConfig.navigate.hasHandler("matching"));
        ok(JIRA.ProjectConfig.navigate.hasHandler("missing") === false);
    });
});
