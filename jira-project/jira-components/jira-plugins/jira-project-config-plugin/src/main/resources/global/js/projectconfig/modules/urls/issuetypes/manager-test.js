AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:urls-module"], function(){
    "use strict";
    var Backbone = JIRA.ProjectConfig.Backbone;

    module("JIRA.ProjectConfig.Urls.IssueTypes.Manager", {
        setup: function () {
            this.manager = new JIRA.ProjectConfig.Urls.IssueTypes.Manager();
            this.functionsToRun = [this.manager.base, this.manager.viewWorkflow, this.manager.editWorkflow, this.manager.viewFields];
        }
    });
    test("functions correctly URI encodes variables", 8, function () {
        var issueType = new Backbone.Model({
                id: "id/should/be/escaped"
            });

        var project = new Backbone.Model({
            key: "projectKey/should/be/escaped"
        });

        for (var i = 0; i < this.functionsToRun.length; i++) {
            var url = this.functionsToRun[i].call(this.manager, {
                project: project,
                issueType: issueType
            });

            ok(url.indexOf("projectKey%2Fshould%2Fbe%2Fescaped") > -1, "Should contain escaped project key.");
            ok(url.indexOf("id%2Fshould%2Fbe%2Fescaped") > -1, "Should contain escaped issue type id.");
        }
    });

    test("functions not HTML encoding variables", 8, function () {
        var issueType = new Backbone.Model({
                id: "<id><not><escaped>"
            });

        var project = new Backbone.Model({
            key: "<projectKey></projectKey><not><escaped>"
        });

        for (var i = 0; i < this.functionsToRun.length; i++) {
            var url = this.functionsToRun[i].call(this.manager, {
                project: project,
                issueType: issueType
            });

            ok(url.indexOf("&lt") === -1, "Should contain not HTML encoded project key.");
            ok(url.indexOf("&gt") === -1, "Should contain not HTML encoded issue type id.");
        }
    });

});

