(function ($) {
    "use strict";

    $(function () {
        var ChangeProjectTypeDialog = require('jira/project/admin/change-project-type-dialog');
        new ChangeProjectTypeDialog({
            trigger: AJS.$("#change_project_type_project"),
            projectId: JIRA.ProjectConfig.getId(),
            onProjectTypeChanged: function () {
                window.location.reload();
            }
        });
    });

}(AJS.$));
