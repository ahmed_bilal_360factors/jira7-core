define('jira-project-config/global/components/userandgrouppicker/user-and-group-picker-item', [
    'jquery',
    'jira/field/multi-user-list-picker/item'
], function (
    jQuery,
    MultiUserListPickerItem) {

    return MultiUserListPickerItem.extend({
        _renders: {
            "item": function () {
                var descriptor = this.options.descriptor;
                var data;
                var meta;

                if (descriptor.noExactMatch() !== true) {
                    meta = descriptor.meta() || {};
                    // A user or group selected from the matches
                    if (meta.isUser) {
                        data = {
                            escape: false,
                            username: meta.username,
                            icon: descriptor.icon(),
                            displayName: AJS.escapeHtml(descriptor.label())
                        };
                        return jQuery(JIRA.Templates.Fields.recipientUsername(data));
                    } else if (meta.isGroup) {
                        data = {
                            escape: false,
                            groupName: descriptor.value(),
                            groupDisplayname: AJS.escapeHtml(descriptor.label())
                        };
                        return jQuery(JIRA.Templates.Global.Components.UserAndGroupPickerItem.selectedGroup(data));
                    }
                }
            }
        }
    });

});
