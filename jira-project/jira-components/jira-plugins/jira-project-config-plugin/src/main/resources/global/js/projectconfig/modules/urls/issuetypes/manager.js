AJS.namespace("JIRA.ProjectConfig.Urls.IssueTypes");

(function (_) {
    "use strict";

    /**
     * @class
     * @classdesc An API for retrieving URLs for issue types.
     * @constructor
     */
    JIRA.ProjectConfig.Urls.IssueTypes.Manager = function () {
    };
    /** @lends JIRA.ProjectConfig.IssueTypes.Entities.Manager# */
    _.extend(JIRA.ProjectConfig.Urls.IssueTypes.Manager.prototype, {

        /**
         * Resolves the base URL that corresponds to the provided project key and issue type.
         *
         * @param {JIRA.ProjectConfig.IssueTypes.Entities.Project} options.project the project
         * @param {JIRA.ProjectConfig.IssueTypes.Entities.IssueType} options.issueType the issue type
         * @returns {String} the base URL that represents this project key and issue type
         */
        base: function (options) {
            return JIRA.Templates.IssueTypeConfig.Urls.base({
                projectKey: options.project.get("key"),
                issueTypeId: options.issueType.get("id")
            });
        },

        /**
         * Resolves the View Workflow URL that corresponds to the provided project key and issue type.
         *
         * @param {JIRA.ProjectConfig.IssueTypes.Entities.Project} options.project the project
         * @param {JIRA.ProjectConfig.IssueTypes.Entities.IssueType} options.issueType the issue type
         * @returns {String} the View Workflow URL that represents this project key and issue type
         */
        viewWorkflow: function (options) {
            return JIRA.Templates.IssueTypeConfig.Urls.viewWorkflow({
                projectKey: options.project.get("key"),
                issueTypeId: options.issueType.get("id")
            });
        },

        /**
         * Resolves the Edit Workflow URL that corresponds to the provided project key and issue type.
         *
         * @param {JIRA.ProjectConfig.IssueTypes.Entities.Project} options.project the project
         * @param {JIRA.ProjectConfig.IssueTypes.Entities.IssueType} options.issueType the issue type
         * @returns {String} the Edit Workflow URL that represents this project key and issue type
         */
        editWorkflow: function (options) {
            return JIRA.Templates.IssueTypeConfig.Urls.editWorkflow({
                projectKey: options.project.get("key"),
                issueTypeId: options.issueType.get("id")
            });
        },

        /**
         * Resolves the View Fields URL that corresponds to the provided project key and issue type.
         *
         * @param {JIRA.ProjectConfig.IssueTypes.Entities.Project} options.project the project
         * @param {JIRA.ProjectConfig.IssueTypes.Entities.IssueType} options.issueType the issue type
         * @returns {String} the View Fields URL that represents this project key and issue type
         */
        viewFields: function (options) {
            return JIRA.Templates.IssueTypeConfig.Urls.viewFields({
                projectKey: options.project.get("key"),
                issueTypeId: options.issueType.get("id")
            });
        },

        /**
         * Resolves the REST API URL for the Fields of an issue type in a particular project.
         *
         * @param {JIRA.ProjectConfig.IssueTypes.Entities.Project} options.project the project
         * @param {JIRA.ProjectConfig.IssueTypes.Entities.IssueType} options.issueType the issue type
         * @returns {String} A URL.
         */
        projectIssueTypeFields: function (options) {
            return JIRA.Templates.IssueTypeConfig.Urls.projectIssueTypeFields({
                projectKey: options.project.get("key"),
                issueTypeId: options.issueType.get("id")
            });
        },

        /**
         * Resolves the REST API URL for the workflow of an issue type in a particular project.
         *
         * @param {JIRA.ProjectConfig.IssueTypes.Entities.Project} options.project the project
         * @param {JIRA.ProjectConfig.IssueTypes.Entities.IssueType} options.issueType the issue type
         * @returns {String} A URL.
         */
        projectIssueTypeWorkflow: function (options) {
            return JIRA.Templates.IssueTypeConfig.Urls.projectIssueTypeWorkflow({
                projectKey: options.project.get("key"),
                issueTypeId: options.issueType.get("id")
            });
        }
    });
}(_));
