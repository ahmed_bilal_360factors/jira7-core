(function (_, Marionette) {
    "use strict";

    JIRA.ProjectConfig.Default.Controller = Marionette.Controller.extend({
        initialize: function (options) {
            this.application = options.application;
            this.navigation = new JIRA.ProjectConfig.IssueTypes.Navigation.Controller({
                region: this.application.issueTypesSideNav,
                reqres: this.application.reqres
            });
            this.navigation.show();
        },

        selectIssueType: function (value) {
            this.navigation.selectIssueType(value);
        },

        selectSummary: function () {
            this.navigation.selectSummary();
        }
    });
}(_, JIRA.ProjectConfig.Backbone.Marionette));
