(function ($) {
    "use strict";

    $(function () {
        require("jira/project/types/warning/dialog").init({
            onProjectTypeChanged: function () {
                window.location.reload();
            },
            sectionElement: $(".old-project-config-header")
        });
        JIRA.trace("project.types.warning.messages.init");
    });

}(AJS.$));
