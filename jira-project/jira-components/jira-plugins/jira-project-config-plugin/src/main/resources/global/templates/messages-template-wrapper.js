/**
 * This file is used to wrap the templates exported in the global namespace
 * by Google Closure Compile with an AMD module. The goal of this AMD module
 * is to hold a reference to the templates, even when they are removed from
 * the global namespace.
 */
define("jira-project-config/templates/messages", [], function() {
     "use strict";
     return window.JIRA.Templates.Messages;
});

/**
 * Force an execution of the module factory, in order to capture a reference to
 * the templates in the AMD module. This will allow other AMD modules access the
 * templates even if the global reference is removed.
 */
AJS.namespace("JIRA.Templates.Messages", null, require("jira-project-config/templates/messages"));
