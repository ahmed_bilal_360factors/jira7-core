define("jira-project-config/navigate", ["jira-project-config/backbone", "underscore", "jquery"], function(Backbone, _, $) {
    'use strict';

    /**
     * Trim the context path from a given URL fragment.
     *
     * @param {String} fragment URL path that might begin with Backbone.history.root.
     * @returns {String} fragment with Backbone.history.root trimmed from the start.
     */
    function trimContextPath(fragment) {
        if (fragment.indexOf(Backbone.history.root) === 0) {
            fragment = fragment.slice(Backbone.history.root.length);
        }

        return fragment;
    }

    /**
     * A wrapper over Backbone.history.navigate that implements useful defaults:
     *
     * 1. Automatically trim Backbone.history.root from `path`, simplifying calling code.
     * 2. Automatically do a full page load when no router handles the route.
     * 3. Use `{trigger: true}` when calling Backbone.history.navigate()
     * 4. Honor `window.onbeforeunload`. If a `onbeforeunload` function exists that returns a non-empty string, a
     *    confirmation dialog will be presented to the user. If the user aborts, the `navigate()` call will be
     *    cancelled.
     *
     * @param {string} path The full path, including `Backbone.history.root`
     * @param {object} [options.location] Defaults to window.location, exposed for testing.
     * @param {*} [options.*] Other options are passed through to `Backbone.history.navigate()`
     * @returns {(boolean|undefined)} Returns `false` if the navigate was cancelled.
     */
    var navigate = function (path, options) {
        options || (options = {});

        var beforeUnloadEvent;
        var beforeUnloadMessage;
        var fragment = path;
        var location = options.location || window.location;
        var onbeforeunload;
        var willBeHandled;

        fragment = trimContextPath(fragment);

        // IE8 fix, invoking window.onbeforeunload() blows up with "Object doesn't support this action", but taking a
        // reference to the attribute and invoking it does not suffer the same problem.
        onbeforeunload = window.onbeforeunload;

        // Honor window.onbeforeunload.
        beforeUnloadEvent = $.Event("beforeunload");
        beforeUnloadMessage = typeof onbeforeunload === "function" && onbeforeunload(beforeUnloadEvent);

        if (beforeUnloadMessage) {
            if (window.confirm(beforeUnloadMessage) === false) {
                return false;
            }
        }

        // Taken directly from Backbone.History.navigate();
        fragment = Backbone.history.getFragment(fragment);
        willBeHandled = navigate.hasHandler(fragment);

        if (willBeHandled) {
            delete options.location; // 'location' is not expected by Backbone.History.navigate()
            return Backbone.history.navigate(fragment, _.extend({
                trigger: true
            }, options));
        } else {
            location.assign(Backbone.history.root + fragment);
        }
    };

    /**
     * Determine if a fragment will be handled by any active router.
     *
     * @param {string} fragment A fragment relative to Backbone.history.root
     * @returns {boolean} True if a handler exists, else false.
     * @private
     */
    navigate.hasHandler = function (fragment) {
        fragment = trimContextPath(fragment);
        return _.any(Backbone.history.handlers, function (handler) {
            return handler.route.test(fragment);
        });
    };

    return navigate;
});

AJS.namespace("JIRA.ProjectConfig.navigate", null, require("jira-project-config/navigate"));
