AJS.test.require([
    "com.atlassian.jira.jira-project-config-plugin:project-config-roles"
], function () {
    "use strict";

    require([
        "jquery",
        "jira-project-config/global/components/userandgrouppicker/user-and-group-picker"
    ], function ($,
                 UserAndGroupPicker) {

        var pickerResponse = {
            "users": {
                "users": [{
                    "name": "user-name",
                    "key": "user-key",
                    "html": "user - user@example.com",
                    "displayName": "user-displayname",
                    "avatarUrl": "http://example.com/usersvatar.jpg"
                }],
                "total": 1,
                "header": "Showing 1 of 1 matching users"
            },
            "groups": {
                "header": "Showing 1 of 1 matching groups",
                "total": 1,
                "groups": [{
                    "name": "user-group-name",
                    "html": "jira-user-group"
                }]
            }
        };
        var pickerElement = "<select id=\"test-picker\" multiple=\"multiple\"></select>";

        module("jira-project-config/global/components/userandgrouppicker/user-and-group-picker-test", {
            initPicker: function () {
                var sandbox = this.sandbox;
                var picker = new UserAndGroupPicker({
                    element: $("#qunit-fixture").find("#test-picker")
                });
                var cleanUp = function () {
                    // remove the picker dropdown appended to <body>
                    picker.dropdownController.$layer.remove();
                };

                var search = function (keyword) {
                    picker.$field.val(keyword);
                    sandbox.clock.tick(1000);
                    picker.$field.trigger("input");
                    sandbox.clock.tick(1000);

                    sandbox.server.requests[sandbox.server.requests.length - 1].respond(200,
                        {"Content-Type": "application/json"},
                        JSON.stringify(pickerResponse)
                    );
                };
                var pickAnActor = function (actorToPick) {
                    search("user");
                    var $suggestions = $(".aui-list-item");
                    $suggestions.each(function (i, suggestionElement) {
                        var $suggestion = $(suggestionElement);
                        if ($suggestion.text() === actorToPick) {
                            $suggestions.removeClass("active");
                            $suggestions.addClass("active");
                            $suggestion.click();
                            return;
                        }
                    });
                };

                var removeAllActors = function () {
                    picker.$selectedItemsContainer.find(".remove-recipient").click();
                };

                return {
                    pickAnActor: pickAnActor,
                    instance: picker,
                    cleanUp: cleanUp,
                    search: search,
                    removeAllActors: removeAllActors
                };
            },
            setup: function () {
                this.sandbox = sinon.sandbox.create();
                this.sandbox.useFakeServer();
                this.sandbox.useFakeTimers();
                $("#qunit-fixture").append(pickerElement);

                this.picker = this.initPicker();
            },
            teardown: function () {
                this.picker.cleanUp();
                this.sandbox.restore();
            }
        });

        test("Picker should search using the keyword input by user", function () {
            this.picker.search("tahdah");

            ok(this.sandbox.server.requests[0].url.indexOf("/rest/api/2/groupuserpicker?showAvatar=true&query=tahdah") !== -1, "Picker should search user & group using the keyword input by user");
        });

        test("Picker should display user & group suggestions", function () {
            this.picker.search("tahdah");
            var suggestions = this.picker.instance.dropdownController.$layer.find(".aui-list-item");

            strictEqual(suggestions.size(), 2, "Picker should display suggestions returned from the rest endpoint");
            strictEqual(suggestions.eq(0).text(), "user - user@example.com", "Picker should display user suggestions");
            strictEqual(suggestions.eq(1).text(), "jira-user-group", "Picker should display group suggestions");
        });

        test("Picker should display selected user & group", function () {
            this.picker.pickAnActor("jira-group");
            this.picker.pickAnActor("user - user@example.com");
            var selectedItems = this.picker.instance.$selectedItemsContainer.find("li");

            strictEqual(selectedItems.size(), 2, "Picker should display selected items");
            strictEqual(selectedItems.eq(0).find("span[rel]").text(), "user-group-name", "Picker should display selected users");
            strictEqual(selectedItems.eq(1).find("span[rel]").text(), "user-displayname", "Picker should display selected groups");
        });

        test("Picker should use group name for group, user key for user (not using username, because username is changable)", function () {
            this.picker.pickAnActor("jira-group");
            this.picker.pickAnActor("user - user@example.com");

            strictEqual(this.picker.instance.getSelectedDescriptors().users[0].value(), "user-key", "Picker should use user key");
            strictEqual(this.picker.instance.getSelectedDescriptors().groups[0].value(), "user-group-name", "Picker should use group name");
        });

        test("Selected users & groups should be removable", function () {
            this.picker.pickAnActor("jira-group");
            this.picker.pickAnActor("user - user@example.com");
            var selectedItems = this.picker.instance.$selectedItemsContainer.find("li");

            ok(selectedItems.eq(0).is(":has(.remove-recipient)"), "Selected users should be removable");
            ok(selectedItems.eq(1).is(":has(.remove-recipient)"), "Selected groups should be removable");
        });

        test("Removed users & groups should be removed out of the recipient container", function () {
            this.picker.pickAnActor("jira-group");
            this.picker.pickAnActor("user - user@example.com");
            this.picker.removeAllActors();

            strictEqual(this.picker.instance.$selectedItemsContainer.find("li").size(), 0, "Picker should remove the removed user out of recipient container");
        });
    });
});
