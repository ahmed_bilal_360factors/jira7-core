define("jira-project-config/global/components/userandgrouppicker/user-and-group-picker", [
    'jira/util/formatter',
    "jquery",
    "underscore",
    "jira-project-config/global/components/userandgrouppicker/user-and-group-picker-item",
    "jira/ajs/select/multi-select",
    "jira/ajs/list/group-descriptor",
    "jira/ajs/list/item-descriptor",
    "jira/ajs/select/suggestions/user-list-suggest-handler",
    "jira/ajs/group"
], function(
    formatter,
    $,
    _,
    UserAndGroupPickerItem,
    MultiSelect,
    GroupDescriptor,
    ItemDescriptor,
    UserListSuggestHandler,
    Group) {

    /**
     * A multi-select list for selecting user or group to put in a role
     * (most of the code copied from JIRA.MultiUserListPicker)
     *
     * @extends AJS.MultiSelect
     */
    return MultiSelect.extend({

        init: function (options) {

            var restPath = JIRA.REST_BASE_URL + "/groupuserpicker";

            function formatResponse(response) {
                var formattedResponse = [];
                if (response.users) {
                    var userDescriptors = new GroupDescriptor({
                        weight: 1, // order or groups in suggestions dropdown
                        label: response.users.header // Heading of group
                    });
                    _.each(response.users.users, function (user) {
                        userDescriptors.addItem(new ItemDescriptor({
                            value: user.key,
                            label: user.displayName,
                            html: user.html,
                            icon: user.avatarUrl,
                            allowDuplicate: false,
                            highlighted: true,
                            meta: {
                                username: user.name,
                                isUser: true
                            }
                        }));
                    });
                    formattedResponse.push(userDescriptors);
                }
                if (response.groups) {
                    var groupsDescriptors = new GroupDescriptor({
                        weight: 2, // order or groups in suggestions dropdown
                        label: response.groups.header // Heading of group
                    });
                    _.each(response.groups.groups, function (group) {
                        groupsDescriptors.addItem(new ItemDescriptor({
                            value: group.name,
                            label: group.name,
                            html: group.html,
                            allowDuplicate: false,
                            highlighted: true,
                            meta: {
                                isGroup: true
                            }
                        }));
                    });
                    formattedResponse.push(groupsDescriptors);
                }

                return formattedResponse;
            }

            $.extend(options, {
                itemAttrDisplayed: "label",
                showDropdownButton: false,
                removeOnUnSelect: true,
                ajaxOptions: {
                    url: restPath,
                    query: true,                // keep going back to the server for each keystroke
                    data: {showAvatar: true},
                    formatResponse: formatResponse
                },
                suggestionsHandler: UserListSuggestHandler,
                itemGroup: new Group(),
                itemBuilder: function (descriptor) {
                    return new UserAndGroupPickerItem({
                        descriptor: descriptor,
                        container: this.$selectedItemsContainer
                    });
                }
            });

            this._super(options);

            // prevent the containing inline dialog from closing when clicking on the dropdown
            this.dropdownController.$layer.on("click", function () {
                return false;
            });
        },

        _createFurniture: function (disabled) {
            this._super(disabled);
            if (this.options.description) {
                this._render("description", this.options.description).insertAfter(this.$field);
            }
        },

        /**
         * The share textarea has no lozenges inside it and no need for cursor and indent nonsense.
         * It could even be a plain text field.
         */
        updateItemsIndent: $.noop,

        /**
         * Get selected users and groups
         * @returns {{users: Array, groups: Array}}
         */
        getSelectedDescriptors: function () {
            var selectedDescriptors = this.model.getDisplayableSelectedDescriptors();
            var result = {
                users: [],
                groups: [],
                total: function () {
                    return this.users.length + this.groups.length;
                }
            };
            var meta;
            _.each(selectedDescriptors, function (descriptor) {
                meta = descriptor.meta();
                if (meta) {
                    if (meta.isUser) {
                        result.users.push(descriptor);
                    } else if (meta.isGroup) {
                        result.groups.push(descriptor);
                    }
                }
            });

            return result;
        },

        _renders: {
            selectedItemsWrapper: function () {
                return $('<div class="recipients" />');
            },
            selectedItemsContainer: function () {
                return $("<ol />");
            },
            description: function (description) {
                return $("<div />")
                    .addClass("description")
                    .text(description);
            },
            container: function (idPrefix) {
                return $('<div class="jira-multi-select long-field usergroup-picker-container" />')
                    .attr('id', idPrefix + '-multi-select');
            },
            field: function (idPrefix) {
                var $textarea = $('<textarea class="text long-field"></textarea>');
                $textarea.attr({
                    'autocomplete': 'off',
                    // the wrap="off" attribute prevents text from growing under the labels. It doesn't prevent linebreaks
                    'wrap': 'off',
                    'placeholder': formatter.I18n.getText("admin.project.global.components.usergroupicker.placeholder.text"),
                    'id': idPrefix + '-textarea'
                });
                // https://connect.microsoft.com/IE/feedback/details/811408/ie10-11-a-textareas-placeholder-text-becomes-its-actual-value-when-using-innertext-innerhtml-or-outerhtml
                // JRA-60186: IE 10 and 11 put the placeholder value as the field value if none is defined during its creation. We can safely reset it here because it is intended to always start empty.
                $textarea.val("");

                return $textarea;
            }
        }

    });

});
