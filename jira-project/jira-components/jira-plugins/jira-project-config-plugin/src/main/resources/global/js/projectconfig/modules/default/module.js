AJS.namespace("JIRA.ProjectConfig.Default");

(function () {
    "use strict";

    JIRA.ProjectConfig.Default.Module = function (module, application) {
        module.addInitializer(function () {
            var controller = new JIRA.ProjectConfig.Default.Controller({application: application});
            application.vent.on("issueTypes:issueTypeSelected", _.bind(controller.selectIssueType, controller));
            application.vent.on("issueTypes:summarySelected", _.bind(controller.selectSummary, controller));

        });

        module.addFinalizer(function () {
            application.vent.off("issueTypes:issueTypeSelected");
            application.vent.off("issueTypes:summarySelected");
        });
    };
}());
