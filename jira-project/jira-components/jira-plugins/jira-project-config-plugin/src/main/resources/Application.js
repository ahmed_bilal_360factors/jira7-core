define("jira-project-config/application", [
    "jira-project-config/utils/virtual-region",
    "jira-project-config/utils/region",
    "jira-project-config/navigate",
    "wrm/data",
    "jira/util/data/meta",
    "wrm/context-path",
    "jira-project-config/backbone",
    "jira-project-config/marionette"
], function(
    VirtualRegion,
    Region,
    navigate,
    wrmData,
    Meta,
    contextPath,
    Backbone,
    Marionette) {
    "use strict";

    // Contains static data such as projectKey.
    var appData = wrmData.claim("com.atlassian.jira.jira-project-config-plugin:app-data");

    /**
     * The application used by the project administration.
     * @name JIRA.ProjectConfig.Application
     * @type Marionette.Application
     * @static
     */
    var application = new Marionette.Application();

    /**
     * Returns a backbone model representing the project being viewed
     */
    application.reqres.setHandler("projectKey", function () {
        return appData.projectKey;
    });

    /**
     * Change the title of the window.
     *
     * This method appends the JIRA app title suffix to `title`.
     * @param {string} text The new title of the page (excluding JIRA app title).
     */
    application.title = function (text) {
        document.title = text + " - " + Meta.get("app-title");
    };

    application.addRegions({
        issueTypesSideNav: new VirtualRegion({
            el: "#content .admin-menu-links:visible",
            target: "#project_issuetypes"
        }),
        content: {
            selector: "section.aui-page-panel-content",
            regionType: Region
        }
    });

    application.on("initialize:after", function () {
        // Retrieve the hash before starting history, otherwise the route on the path will be triggered,
        // which will replace the hash (meaning we can't navigate to the hash route).
        var fragment = Backbone.history.location.pathname;

        var hash = Backbone.history.getHash();

        Backbone.history.start({
            hashChange: true,
            pushState: window.history.pushState,
            root: contextPath() + "/"
        });

        // This is here to support IE9: delete after it has been deprecated

        // When it is a browser that does not have push state (e.g. IE 9) and there is no hash defined,
        // then pull of the fragment from the URL path, and loadUrl on it, triggering its route.
        if (!window.history.pushState && !navigate.hasHandler(hash)) {
            if (fragment.indexOf(Backbone.history.root) === 0) {
                fragment = fragment.slice(Backbone.history.root.length);
            }
            Backbone.history.loadUrl(fragment);
            // Otherwise, if it is a browser with push state (e.g. a sane browser) and there is a hash defined,
            // then navigate to the hash, and replace the current URL path with the hash. Make sure to trigger so we
            // render the correct issue type. This is to convert the link to the correct format for a push state browser
            // when the link is provided from IE 8/9.
        } else if (window.history.pushState && navigate.hasHandler(hash)) {
            Backbone.history.navigate(hash, {
                replace: true,
                trigger: true
            });
        }
        // End of IE9 block
    });

    return application;
});

AJS.namespace('JIRA.ProjectConfig.Application', null, require('jira-project-config/application'));
