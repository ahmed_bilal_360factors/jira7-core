define('jira-project-config/issuetypes/entities/models/projects', ['require'], function (require) {
    "use strict";

    var Backbone = require('jira-project-config/backbone');
    var Project = require('jira-project-config/issuetypes/entities/models/project');

    /**
     * @class
     * @classdesc A collection of {@link JIRA.ProjectConfig.IssueTypes.Entities.Project}.
     * @extends Backbone.Collection
     */
    return Backbone.Collection.extend(
        /** @lends JIRA.ProjectConfig.IssueTypes.Entities.Projects# */
        {
            model: Project
        });
});
