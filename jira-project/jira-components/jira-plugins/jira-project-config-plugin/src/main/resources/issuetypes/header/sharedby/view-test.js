AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-tab"], function(){
    "use strict";

    var _ = require("underscore");
    var jQuery = require("jquery");
    var SharedBy = require("jira-project-config/issuetypes/header/shared-by/view");
    var IssueType = require('jira-project-config/issuetypes/entities/models/issue-type');
    var IssueTypes = require('jira-project-config/issuetypes/entities/models/issue-types');
    var Projects = require('jira-project-config/issuetypes/entities/models/projects');
    var Project = require('jira-project-config/issuetypes/entities/models/project');
    var SharedByData = require('jira-project-config/issuetypes/entities/models/shared-by-data');

    var SharedByParser = function ($link) {
        this.$link = $link;
    };

    _.extend(SharedByParser.prototype, {
        selectors: {
            issueType: {
                trigger: "a.shared-by-issuetype",
                popup: "issuetype-shared-info"
            },
            project: {
                trigger: "a.shared-by-project",
                popup: "project-shared-info"
            }
        },

        triggerProjects: function () {
            return this._trigger(this.selectors.project.trigger);
        },
        triggerIssueTypes: function () {
            return this._trigger(this.selectors.issueType.trigger);
        },
        clickAnIssueType: function () {
            this.triggerIssueTypes();
            var popup = MockPopup.find(this.selectors.issueType.popup);
            popup.content().find("a").click();

            return this;
        },
        getProjects: function () {
            return this._triggerAndParseList(this.selectors.project);
        },
        getProjectsTitle: function () {
            return this._triggerAndGetTitle(this.selectors.project);
        },
        getIssueTypesTitle: function () {
            return this._triggerAndGetTitle(this.selectors.issueType);
        },
        getIssueTypes: function () {
            return this._triggerAndParseList(this.selectors.issueType);
        },
        popupsClosed: function () {
            return !MockPopup.find("project-shared-info") && !MockPopup.find("issuetype-shared-info");
        },
        _triggerAndGetTitle: function (selectors) {
            if (!this._trigger(selectors.trigger)) {
                return "";
            } else {
                var text = MockPopup.find(selectors.popup).content().find(":header").text();
                return text.replace(/:$/, "");
            }
        },
        _triggerAndParseList: function (selectors) {
            if (!this._trigger(selectors.trigger)) {
                return [];
            }
            var result = [];
            var popup = MockPopup.find(selectors.popup);
            popup.content().find("li").each(function () {
                result.push(jQuery(this).text());
            });

            return result;
        },
        _trigger: function (trigger) {
            var $trigger = this.$link.find(trigger);
            $trigger.click();
            return !!$trigger.length;
        }
    });

    var createIssueTypes = function () {
        var issueTypes = _.map(arguments, function (value) {
            return new IssueType({
                id: value,
                name: value
            });
        });
        return new IssueTypes(issueTypes);
    };

    /**
     *
     * @returns {Entities.Projects}
     */
    var createProjects = function () {
        var projects = _.map(arguments, function (name) {
            return new Project({
                name: name
            });
        });
        return new Projects(projects);
    };

    var MockPopup = function (trigger, id, contentCallback) {
        this.$content = null;
        this.id = id;

        var self = this;
        trigger.on("click", function () {
            self.$content = jQuery("<div>");
            contentCallback(self.$content);
        });

        MockPopup.popups[id] = this;
    };

    _.extend(MockPopup.prototype, {
        content: function () {
            return this.$content;
        },
        close: function () {
            delete MockPopup.popups[this.id];
        }
    });

    _.extend(MockPopup, {
        popups: {},
        find: function (id) {
            return MockPopup.popups[id];
        }
    });

    module("JIRA.ProjectConfig.IssueTypes.SharedBy", {
        setup: function () {
            this.$fixture = jQuery("#qunit-fixture");
            sinon.stub(AJS, "format", function () {
                return Array.prototype.join.call(arguments, "");
            });
        },
        teardown: function () {
            AJS.format.restore();
        }
    });

    var runTest = function (options) {
        var view = new SharedBy({
            model: new SharedByData({
                projects: createProjects.apply(this, options.projects || []),
                issueTypes: createIssueTypes.apply(this, options.issueTypes || []),
                totalProjectsCount: (options.projects ? options.projects.length : 0) + 1,
                hiddenProjectsCount: options.hiddenProjectsCount || 0,
                projectsTitle: options.projectsTitle || "",
                issueTypesTitle: options.issueTypesTitle || ""
            }),
            popupClass: MockPopup
        });
        this.$fixture.html(view.render().$el);

        var parser = new SharedByParser(this.$fixture);

        options.callback(view, parser);
    };

    var assertSharedBy = function (projects, issueTypes) {
        runTest.call(this, {
            projects: projects,
            issueTypes: issueTypes,
            callback: function (view, parser) {
                deepEqual(parser.getProjects(), projects, "Project matches");
                deepEqual(parser.getIssueTypes(), issueTypes, "Issue Types matches");
            }
        });
    };

    test("Render projects only", function () {
        assertSharedBy.call(this, ["one", "two"], []);
    });

    test("Render issue types only", function () {
        assertSharedBy.call(this, [], ["one", "two"]);
    });

    test("Render issue types and project only", function () {
        assertSharedBy.call(this, ["project1", "project2"], ["issueType1", "issueType2"]);
    });

    test("Popups close when view closes", function () {
        runTest.call(this, {
            projects: ["project1"],
            issueTypes: ["type1"],
            callback: function (view, parser) {
                parser.triggerProjects();
                parser.triggerIssueTypes();
                view.close();
                ok(parser.popupsClosed());
            }
        });
    });

    test("Clicking issueType triggers events", function () {
        runTest.call(this, {
            projects: ["project1"],
            issueTypes: ["type1"],
            callback: function (view, parser) {
                var triggered = false;
                view.on("issueType:selected", function () {
                    triggered = true;
                });
                parser.clickAnIssueType();
                ok(triggered, "Clicking issue type triggered 'issueTypeSelected' event.");
            }
        });
    });

    test("Popups have correct title", function () {
        var typesTitle = "issueTypesTitle";
        var projectsTitle = "projectsTitle";
        runTest.call(this, {
            projects: ["project1"],
            issueTypes: ["type1"],
            issueTypesTitle: typesTitle,
            projectsTitle: projectsTitle,
            callback: function (view, parser) {
                strictEqual(parser.getIssueTypesTitle(), typesTitle);
                strictEqual(parser.getProjectsTitle(), projectsTitle);
            }
        });
    });
});
