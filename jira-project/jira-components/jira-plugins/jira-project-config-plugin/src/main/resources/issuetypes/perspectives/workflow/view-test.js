AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-tab"], function(){
    "use strict";

    var WorkflowView = require("jira-project-config/issuetypes/perspectives/workflow/view");

    module("JIRA.ProjectConfig.IssueTypes.Workflow.View");

    test("workflow designer region exists when view rendered", function () {
        var view = new WorkflowView();

        view.render();

        equal(view.ui.workflowDesigner.length, 1, "workflow designer region exists");
    });
});
