define('jira-project-config/issuetypes/navigation/views/issue-types', ['require'], function(require) {
    "use strict";

    var Marionette = require("jira-project-config/marionette");
    var IssueTypeView = require('jira-project-config/issuetypes/navigation/views/issue-type');
    var navigate = require('jira-project-config/navigate');
    var jQuery = require('jquery');

    var BASE_CLASS = "project-issuetypes";
    var COLLAPSED_CLASS = BASE_CLASS + "-collapsed";
    var LAST_ISSUE_TYPE_CLASS = BASE_CLASS + "-last";
    var TEASER_CLASS = BASE_CLASS + "-teaser";
    var TEASER_LAST_CLASS = BASE_CLASS + "-teaser-last";

    return Marionette.CollectionView.extend(
        /** @lends JIRA.ProjectConfig.IssueTypes.Navigation.IssueTypesView# */
        {
            attributes: function () {
                return {
                    "class": BASE_CLASS + (this.model.get("collapsed") ? " " + COLLAPSED_CLASS : "")
                };
            },

            events: {
                "simpleClick .project-issuetype": "_navigate"
            },

            /**
             * Initialize the view.
             *
             * @classdesc Renders the list of issue type links.
             * @constructs
             * @extends Marionette.CollectionView
             * @param {JIRA.ProjectConfig.IssueTypes.Navigation.Model} options.model The navigation state.
             */
            initialize: function (options) {
                this.model = options.model;
                this.collection = options.model.get("issueTypeLinks");
                this.listenTo(this, "render", this._markSelected);
                this.listenTo(this, "render", this._markTeasers);
            },

            itemView: IssueTypeView,

            modelEvents: {
                "change:collapsed": "_markCollapsed",
                "change:selection": "_markSelected",
                "change:teaserCount": "_markTeasers"
            },

            tagName: "ul",

            /**
             * Apply CSS classes the list to indicate whether it's collapsed.
             * @private
             */
            _markCollapsed: function (model, value) {
                this.$el.toggleClass(COLLAPSED_CLASS, value);
            },

            /**
             * Tell the currently selected issue type to that it's selected.
             * @private
             */
            _markSelected: function () {
                var issueType = this.model.get("selection");

                this.children.each(function (child) {
                    child.setSelected(child.model === issueType);
                });
            },

            /**
             * Add teaser CSS classes to items, so that collapsed/expanded styling targets the correct elements.
             * @private
             */
            _markTeasers: function () {
                var children = this.$el.children();
                var nonTeasers;
                var teasers;

                teasers = children.slice(0, this.model.get("teaserCount"));
                teasers.addClass(TEASER_CLASS);
                teasers.last().addClass(TEASER_LAST_CLASS);

                nonTeasers = children.slice(this.model.get("teaserCount"));
                nonTeasers.removeClass(TEASER_CLASS);
                nonTeasers.removeClass(TEASER_LAST_CLASS);

                children.last().addClass(LAST_ISSUE_TYPE_CLASS);
            },

            /**
             * Handle clicks on issue type links, making them use push state.
             * @private
             */
            _navigate: function (event) {
                event.preventDefault();
                navigate(jQuery(event.target).attr("href"));
            }
        });
});
