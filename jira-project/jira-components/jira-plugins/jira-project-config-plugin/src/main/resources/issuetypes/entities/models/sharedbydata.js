define('jira-project-config/issuetypes/entities/models/shared-by-data', ['require'], function (require) {
    "use strict";

    var Backbone = require('jira-project-config/backbone');
    var IssueTypes = require('jira-project-config/issuetypes/entities/models/issue-types');
    var Projects = require('jira-project-config/issuetypes/entities/models/projects');

    /**
     * @class
     * @classdesc The data required to render the shared by information for an issue type and perspective.
     * @extends Backbone.Model
     */
    return Backbone.Model.extend(
        /** @lends JIRA.ProjectConfig.IssueTypes.Entities.SharedByData# */
        {
            defaults: function () {
                return {
                    /**
                     * Total number of sharing projects (including those not visible to the current user).
                     *
                     * @memberof JIRA.ProjectConfig.IssueTypes.Entities.SharedByData#
                     * @type {number}
                     */
                    totalProjectsCount: 0,

                    /**
                     * The number of sharing projects not visible to the current user.
                     *
                     * @memberof JIRA.ProjectConfig.IssueTypes.Entities.SharedByData#
                     * @type {number}
                     */
                    hiddenProjectsCount: 0,

                    /**
                     * The issue types that data is shared with.
                     *
                     * @memberof JIRA.ProjectConfig.IssueTypes.Entities.SharedByData#
                     * @type {JIRA.ProjectConfig.IssueTypes.Entities.IssueTypes}
                     */
                    issueTypes: new IssueTypes(),

                    /**
                     * The message to display for the issue types that the data is shared with. Should always be provided.
                     *
                     * @memberof JIRA.ProjectConfig.IssueTypes.Entities.SharedByData#
                     * @type {string}
                     */
                    issueTypesTitle: undefined,

                    /**
                     * The projects that data is shared with.
                     *
                     * @memberof JIRA.ProjectConfig.IssueTypes.Entities.SharedByData#
                     * @type {JIRA.ProjectConfig.IssueTypes.Entities.Projects}
                     */
                    projects: new Projects(),

                    /**
                     * The message to display for the projects that the data is shared with. Should always be provided.
                     *
                     * @memberof JIRA.ProjectConfig.IssueTypes.Entities.SharedByData#
                     * @type {string}
                     */
                    projectsTitle: undefined
                };
            }
        });
});
