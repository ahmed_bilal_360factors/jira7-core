define("jira-project-config/issuetypes/module", ["require"], function(require){
    "use strict";

    var IssueTypesController = require('jira-project-config/issuetypes/controller');
    var IssueTypesModel = require('jira-project-config/issuetypes/model');
    var IssueTypesRouter = require('jira-project-config/issuetypes/router');

    return function (module, application) {
        module.addInitializer(function () {
            var controller;
            var model;
            var router;

            model = new IssueTypesModel();

            controller = new IssueTypesController({
                application: application,
                model: model,
                region: application.content
            });

            router = new IssueTypesRouter({controller: controller});
        });
    };
});
