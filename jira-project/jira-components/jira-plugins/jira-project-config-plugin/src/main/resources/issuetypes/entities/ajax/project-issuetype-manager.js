define('jira-project-config/issuetypes/entities/ajax/project-issue-type-manager', ["require"], function(require) {
    "use strict";

    var WebSudoSmartAjax = require('jira/ajs/ajax/smart-ajax/web-sudo');
    var _ = require('underscore');
    var Deferred = require('jira/jquery/deferred');

    /**
     * An object that provides an interface to interact with REST APIs
     *
     * @constructor
     * @param {Marionette.RequestResponse} options.reqres A reqres that supports the following:
     *   - `urls:issueTypes:fields`
     *   - `urls:issueTypes:workflow`
     */
    return function (options) {

        return /** @lends JIRA.ProjectConfig.IssueTypes.Ajax.ProjectIssueTypeManager# */{
            /**
             * Returns the workflow information for an issue type in a project.
             *
             * @param {JIRA.ProjectConfig.IssueTypes.Entities.Project} project The project that the issue type is in.
             * @param {JIRA.ProjectConfig.IssueTypes.Entities.IssueType} issueType The issue type.
             * @returns a jQuery promise that is resolved with JSON decoded data from the REST API, or
             *     rejected with the jqXHR.
             *
             * Example data:
             *
             *     {
             *         name: "jira",
             *         displayName: "JIRA default workflow",
             *         state: "pending",
             *         sharedWithIssueTypes: ["1", "5", "3"],
             *         sharedWithProjects: [
             *             {
             *                 id: 123,
             *                 key: "FOO",
             *                 name: "Fast Or Out",
             *                 url: "/jira/...",
             *                 description: "A bit of this and that."
             *             }
             *         ]
             *     }
             */
            workflow: function (project, issueType) {
                var deferred = Deferred();

                _.bindAll(deferred, "reject");

                this._makeRequest({
                        dataType: "json",
                        url: options.reqres.request("urls:issueTypes:rest:workflow", {
                            project: project,
                            issueType: issueType
                        })
                    })
                    .done(function (data) {
                        deferred.resolve(data);
                    })
                    .fail(deferred.reject);

                return deferred.promise();
            },

            /**
             * Returns the fields information for an issue type in a project.
             *
             * @param {JIRA.ProjectConfig.IssueTypes.Entities.Project} project The project that the issue type is in.
             * @param {JIRA.ProjectConfig.IssueTypes.Entities.IssueType} issueType The issue type.
             * @returns a jQuery promise that is resolved with JSON decoded data from the REST API, or rejected with
             *     the jqXHR.
             *
             * Example data:
             *
             *     {
             *         screenName: "The most beautiful screen that ever was",
             *         sharedWithIssueTypes: ["1", "5", "3"],
             *         sharedWithProjects: [
             *             {
             *                 id: 123,
             *                 key: "FOO",
             *                 name: "Fast Or Out",
             *                 url: "/jira/...",
             *                 description: "A bit of this and that."
             *             }
             *         ],
             *         viewScreenId: 456
             *     }
             */
            fields: function (project, issueType) {
                var deferred = Deferred();

                _.bindAll(deferred, "reject");

                this._makeRequest({
                        dataType: "json",
                        url: options.reqres.request("urls:issueTypes:rest:fields", {
                            project: project,
                            issueType: issueType
                        })
                    })
                    .done(function (data) {
                        deferred.resolve(data);
                    })
                    .fail(deferred.reject);

                return deferred.promise();
            },

            /**
             * Make an AJAX request.
             *
             * Attaches a CSRF token to the request and handles WebSudo.
             *
             * @param {object} options Options to pass to `jQuery.ajax()`.
             * @protected
             */
            _makeRequest: function (options) {
                var deferred = Deferred();

                options.headers = _.extend({}, options.headers, {
                    "X-Atlassian-Token": "nocheck"
                });

                WebSudoSmartAjax.makeWebSudoRequest(options, {
                        cancel: function (e) {
                            // Rejection must be deferred to ensure that the WebSudo
                            // dialog has closed and the original one is restored.
                            _.defer(deferred.reject, undefined, "abort");
                            e.preventDefault();
                        }
                    })
                    .done(_.bind(deferred.resolve, deferred))
                    .fail(function (jqXHR) {
                        deferred.reject(jqXHR);
                    });

                return deferred.promise();
            }
        };
    };
});
