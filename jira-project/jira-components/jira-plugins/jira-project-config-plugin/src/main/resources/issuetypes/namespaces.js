AJS.namespace("JIRA.ProjectConfig.IssueTypes.Controller", null, require("jira-project-config/issuetypes/controller"));
AJS.namespace("JIRA.ProjectConfig.IssueTypes.Model", null, require("jira-project-config/issuetypes/model"));
AJS.namespace("JIRA.ProjectConfig.IssueTypes.Module", null, require("jira-project-config/issuetypes/module"));
AJS.namespace("JIRA.ProjectConfig.IssueTypes.Router", null, require("jira-project-config/issuetypes/router"));
AJS.namespace("JIRA.ProjectConfig.IssueTypes.View", null, require("jira-project-config/issuetypes/view"));
AJS.namespace("JIRA.Templates.IssueTypeConfig", null, require("jira-project-config/issuetypes/templates"));

AJS.namespace("JIRA.Templates.IssueTypeConfig.Header", null, require("jira-project-config/issuetypes/header/templates"));
AJS.namespace("JIRA.Templates.IssueTypeConfig.Header.Descriptor", null, require("jira-project-config/issuetypes/header/descriptor/view/templates"));
AJS.namespace("JIRA.Templates.IssueTypeConfig.Header.Perspectives", null, require("jira-project-config/issuetypes/header/perspectives/view/templates"));
AJS.namespace("JIRA.Templates.IssueTypeConfig.Header.SharedBy", null, require("jira-project-config/issuetypes/header/shared-by/view/templates"));
AJS.namespace("JIRA.ProjectConfig.IssueTypes.Header.Controller", null, require("jira-project-config/issuetypes/header/controller"));
AJS.namespace("JIRA.ProjectConfig.IssueTypes.Header.Descriptor.View", null, require("jira-project-config/issuetypes/header/descriptor/view"));
AJS.namespace("JIRA.ProjectConfig.IssueTypes.Header.Model", null, require("jira-project-config/issuetypes/header/model"));
AJS.namespace("JIRA.ProjectConfig.IssueTypes.Header.Perspectives.View", null, require("jira-project-config/issuetypes/header/perspectives/view"));
AJS.namespace("JIRA.ProjectConfig.IssueTypes.Header.SharedBy.View", null, require("jira-project-config/issuetypes/header/shared-by/view"));
AJS.namespace("JIRA.ProjectConfig.IssueTypes.Header.View", null, require("jira-project-config/issuetypes/header/view"));

AJS.namespace("JIRA.ProjectConfig.IssueTypes.Fields.Controller", null, require("jira-project-config/issuetypes/perspectives/fields/controller"));
AJS.namespace("JIRA.ProjectConfig.IssueTypes.Fields.View", null, require("jira-project-config/issuetypes/perspectives/fields/view"));
AJS.namespace("JIRA.ProjectConfig.IssueTypes.Fields.ViewModel", null, require("jira-project-config/issuetypes/perspectives/fields/model"));
AJS.namespace("JIRA.ProjectConfig.IssueTypes.Workflow.Controller", null, require("jira-project-config/issuetypes/perspectives/workflow/controller"));
AJS.namespace("JIRA.ProjectConfig.IssueTypes.Workflow.Model", null, require("jira-project-config/issuetypes/perspectives/workflow/model"));
AJS.namespace("JIRA.ProjectConfig.IssueTypes.Workflow.PluggableActions.View", null, require("jira-project-config/issuetypes/perspectives/workflow/pluggable-actions/view"));
AJS.namespace("JIRA.ProjectConfig.IssueTypes.Workflow.View", null, require("jira-project-config/issuetypes/perspectives/workflow/model"));
AJS.namespace("JIRA.Templates.IssueTypeConfig.Fields.Tab", null, require("jira-project-config/issuetypes/perspectives/fields/templates"));
AJS.namespace("JIRA.Templates.IssueTypeConfig.Workflow", null, require("jira-project-config/issuetypes/perspectives/workflow/templates"));
AJS.namespace("JIRA.Templates.IssueTypeConfig.Workflow.PluggableActions", null, require("jira-project-config/issuetypes/perspectives/workflow/pluggable-actions/templates"));
