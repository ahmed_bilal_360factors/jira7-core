AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-navigation"], function(){
    "use strict";

    var jQuery = require("jquery");
    var navigate = require("jira-project-config/navigate");
    var IssueType = require("jira-project-config/issuetypes/entities/models/issue-type");
    var IssueTypeLinks = require("jira-project-config/issuetypes/entities/models/issue-type-links");
    var IssueTypesView = require("jira-project-config/issuetypes/navigation/views/issue-types");
    var Model = require("jira-project-config/issuetypes/navigation/model");
    var Navigate = require("jira-project-config/navigate");

    function Inspector(element) {
        this.element = jQuery(element);
    }

    Inspector.prototype = {
        isCollapsed: function () {
            return this.element.hasClass("project-issuetypes-collapsed");
        },

        selectedIssueTypeName: function () {
            return this.element.find(".aui-nav-selected a").text();
        },

        liAtIndex: function (index) {
            return this.element.find("li").eq(index);
        },

        linkAtIndex: function (index) {
            return this.liAtIndex(index).find("a");
        }
    };

    module("JIRA.ProjectConfig.IssueTypes.Navigation.IssueTypesView", {
        setup: function () {
            this.sandbox = sinon.sandbox.create();
            this.issueTypes = {
                foo: new IssueType({
                    id: 1,
                    url: "/foo",
                    name: "Foo"
                }),
                bar: new IssueType({
                    id: 2,
                    url: "/bar",
                    name: "Bar"
                }),
                baz: new IssueType({
                    id: 3,
                    url: "/baz",
                    name: "Baz"
                })
            };
            this.model = new Model({
                issueTypeLinks: new IssueTypeLinks([
                    this.issueTypes.foo,
                    this.issueTypes.bar
                ])
            });
        },
        teardown: function () {
            this.sandbox.restore();
        }
    });

    test("model selection causes setSelected() on item", function () {
        var inspector;
        var view;

        view = new IssueTypesView({
            model: this.model
        });
        view.render();

        inspector = new Inspector(view.el);
        strictEqual(inspector.selectedIssueTypeName(), "");

        this.model.set("selection", this.issueTypes.foo);
        strictEqual(inspector.selectedIssueTypeName(), "Foo");

        this.model.set("selection", "summary");
        strictEqual(inspector.selectedIssueTypeName(), "");

        this.model.set("selection", this.issueTypes.bar);
        strictEqual(inspector.selectedIssueTypeName(), "Bar");
    });

    test("model 'collapsed' toggles class", function () {
        var inspector;
        var view;

        view = new IssueTypesView({
            model: this.model
        });
        view.render();

        inspector = new Inspector(view.el);
        strictEqual(inspector.isCollapsed(), false);

        this.model.set("collapsed", true);
        strictEqual(inspector.isCollapsed(), true);
    });

    test("teaser classes added to items", 11, function () {
        var inspector;
        var view;

        this.model.set("teaserCount", 2);

        view = new IssueTypesView({
            model: this.model
        });
        view.render();

        inspector = new Inspector(view.el);

        ok(inspector.liAtIndex(0).hasClass("project-issuetypes-teaser"));

        ok(inspector.liAtIndex(1).hasClass("project-issuetypes-teaser"));
        ok(inspector.liAtIndex(1).hasClass("project-issuetypes-teaser-last"));

        ok(!inspector.liAtIndex(2).hasClass("project-issuetypes-teaser"));
        ok(!inspector.liAtIndex(2).hasClass("project-issuetypes-teaser-last"));

        this.model.set("teaserCount", 1);
        ok(inspector.liAtIndex(0).hasClass("project-issuetypes-teaser"));
        ok(inspector.liAtIndex(0).hasClass("project-issuetypes-teaser-last"));

        ok(!inspector.liAtIndex(1).hasClass("project-issuetypes-teaser"));
        ok(!inspector.liAtIndex(1).hasClass("project-issuetypes-teaser-last"));

        ok(!inspector.liAtIndex(2).hasClass("project-issuetypes-teaser"));
        ok(!inspector.liAtIndex(2).hasClass("project-issuetypes-teaser-last"));
    });

    test("issue type clicks yield to JIRA.ProjectConfig.navigate", function () {
        var context = AJS.test.mockableModuleContext();
        var spyNavigate = this.sandbox.spy();
        context.mock('jira-project-config/navigate', spyNavigate);
        var IssueTypesView = context.require('jira-project-config/issuetypes/navigation/views/issue-types');

        var view = new IssueTypesView({
            model: this.model
        });
        view.render();

        var inspector = new Inspector(view.el);
        inspector.linkAtIndex(0).click();

        ok(spyNavigate.calledOnce);
        strictEqual(spyNavigate.firstCall.args[0], this.issueTypes.foo.get("url"));
    });
});
