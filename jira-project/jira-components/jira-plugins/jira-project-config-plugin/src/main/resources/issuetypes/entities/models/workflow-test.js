AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-entities"], function(){
    "use strict";

    var IssueTypes = require("jira-project-config/issuetypes/entities/models/issue-types");
    var Projects = require("jira-project-config/issuetypes/entities/models/projects");
    var Workflow = require("jira-project-config/issuetypes/entities/models/workflow");

    module("JIRA.ProjectConfig.IssueTypes.Entities.Workflow");

    test("Defaults", function () {
        var workflow = new Workflow();

        equal(workflow.get("name"), undefined);

        equal(workflow.get("displayName"), undefined);

        ok(workflow.get("sharedWithProjects") instanceof Projects);

        ok(workflow.get("sharedWithIssueTypes") instanceof IssueTypes);

        equal(workflow.get("state"), undefined);

        equal(typeof(workflow.get("totalProjectsCount")), "number");
        equal(workflow.get("totalProjectsCount"), 0);

        equal(typeof(workflow.get("hiddenProjectsCount")), "number");
        equal(workflow.get("hiddenProjectsCount"), 0);
    });

    test("toJSON() serialises child collections", function () {
        var workflow = new Workflow();

        deepEqual(workflow.toJSON(), {
            name: undefined,
            displayName: undefined,
            sharedWithProjects: [],
            sharedWithIssueTypes: [],
            totalProjectsCount: 0,
            hiddenProjectsCount: 0,
            state: undefined
        });
    });
});
