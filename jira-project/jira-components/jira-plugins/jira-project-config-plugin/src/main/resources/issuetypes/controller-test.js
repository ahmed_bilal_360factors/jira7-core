AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-tab", "com.atlassian.jira.jira-project-config-plugin:issuetypes-test-utils"], function(){
    "use strict";

    var _ = require("underscore");
    var Chance = require("jira-project-config/libs/chance");
    var ChanceMixins = require("jira-project-config/issuetypes/entities/test/chance-mixins");
    var Deferred = require("jira/jquery/deferred");
    var Marionette = require("jira-project-config/marionette");
    var IssueTypes = require("jira-project-config/issuetypes/entities/models/issue-types");
    var IssueTypesModel = require("jira-project-config/issuetypes/model");

    var PROGRESS_TIMEOUT = 505;

    var timerTest = function (func) {
        return function () {
            var clock = this.clock = this.sandbox.useFakeTimers();
            try {
                func.apply(this, arguments);
            } finally {
                clock.restore();
            }
        };
    };

    var safeTest = function (name, func) {
        test.call(this, name, timerTest(func));
    };

    module("JIRA.ProjectConfig.IssueTypes.Controller", {
        setup: function () {
            var sandbox = this.sandbox = sinon.sandbox.create();
            this.chance = new Chance(1);
            _.extend(this.chance, new ChanceMixins());

            this.context = AJS.test.mockableModuleContext();

            var project = this.project = this.chance.project();

            var bug = this.bug = this.chance.issueType();
            var fullBug = this.fullBug = bug.clone().set({
                fields: this.chance.fields(),
                workflow: this.chance.workflow()
            });

            var story = this.story = this.chance.issueType();
            var fullStory = story.clone().set({
                fields: this.chance.fields(),
                workflow: this.chance.workflow()
            });

            var workflowPerspectiveModel = this.workflowPerspectiveModel = this.chance.perspective().set("id", "workflow");
            var workflowDescriptorModel = this.chance.headerDescriptor();
            var workflowSharedByDataModel = this.chance.sharedByData();
            var fieldsPerspectiveModel = this.fieldsPerspectiveModel = this.chance.perspective().set("id", "fields");
            var fieldsDescriptorModel = this.chance.headerDescriptor();
            var fieldsSharedByDataModel = this.chance.sharedByData();

            // Mock out the workflow controller
            this.mockWorkflowController = this.sandbox.spy(function (opts) {
                return {
                    getDescriptor: function () {
                        return workflowDescriptorModel;
                    },
                    getPerspectiveModel: function () {
                        return workflowPerspectiveModel;
                    },
                    getSharedByData: function () {
                        return workflowSharedByDataModel;
                    },
                    opts: opts,
                    setRegion: sandbox.spy(),
                    show: sandbox.spy()
                };
            });
            this.context.mock("jira-project-config/issuetypes/perspectives/workflow/controller", this.mockWorkflowController);

            // Mock out the fields controller
            this.mockFieldsController = this.sandbox.spy(function (opts) {
                return {
                    getDescriptor: function () {
                        return fieldsDescriptorModel;
                    },
                    getPerspectiveModel: function () {
                        return fieldsPerspectiveModel;
                    },
                    getSharedByData: function () {
                        return fieldsSharedByDataModel;
                    },
                    opts: opts,
                    setRegion: sandbox.spy(),
                    show: sandbox.spy()
                };
            });
            this.context.mock("jira-project-config/issuetypes/perspectives/fields/controller", this.mockFieldsController);

            var showHeaderSpy = this.showHeaderSpy = this.sandbox.spy();
            this.mockHeaderController = this.sandbox.spy(function () {
                return {
                    setRegion: sandbox.spy(),
                    show: showHeaderSpy
                };
            });
            this.context.mock("jira-project-config/issuetypes/header/controller", this.mockHeaderController);

            // Mock out the application.
            this.application = new Marionette.Application();
            this.application.title = this.sandbox.stub();
            this.application.content = {
                show: this.sandbox.stub()
            };

            this.application.reqres.setHandler("project", function () {
                return new Deferred().resolve(project).promise();
            });

            this.application.reqres.setHandler("issueTypes", function () {
                var issueTypes = new IssueTypes([bug, story]);
                return new Deferred().resolve(issueTypes).promise();
            });

            this.application.reqres.setHandler("issueType", function (options) {
                var deferred = new Deferred();
                _.defaults(options, {fetchRelated: false});
                if (options.id === bug.get("id")) {
                    deferred.resolve(options.fetchRelated ? fullBug : bug);
                } else if (options.id === story.get("id")) {
                    deferred.resolve(options.fetchRelated ? fullStory : story);
                }
                return deferred.promise();
            });

            this.application.reqres.setHandler("workflowLayoutData", function () {
                return new Deferred().promise();
            });

            this.application.reqres.setHandler("getPreferredPerspective", function () {
                return "workflow";
            });

            this.application.reqres.setHandler("urls:issueTypes:viewWorkflow", function (options) {
                return options.project.get("key") + "/" + options.issueType.get("id") + "/workflow";
            });
            this.application.reqres.setHandler("urls:issueTypes:editWorkflow", function (options) {
                return options.project.get("key") + "/" + options.issueType.get("id") + "/workflow/edit";
            });

            this.application.reqres.setHandler("urls:issueTypes:viewFields", function (options) {
                return options.project.get("key") + "/" + options.issueType.get("id") + "/fields";
            });

            this.setPreferredPerspectiveSpy = this.sandbox.spy();
            this.application.reqres.setHandler("setPreferredPerspective", this.setPreferredPerspectiveSpy);

            this.navigateStub = this.sandbox.stub();
            this.context.mock("jira-project-config/navigate", this.navigateStub);

            var IssueTypesController = this.context.require('jira-project-config/issuetypes/controller');
            this.controller = new IssueTypesController({
                application: this.application,
                model: new IssueTypesModel(),
                region: this.application.region
            });
        },

        teardown: function () {
            this.sandbox.restore();
        }
    });

    test("summary", function () {
        var summarySelectedEventSpy = this.sandbox.spy();
        this.application.vent.on("issueTypes:summarySelected", summarySelectedEventSpy);
        this.controller.summary("AR");
        ok(summarySelectedEventSpy.calledOnce);
        ok(this.application.title.calledOnce);
    });

    test("issueType last visited issue type tab is workflow", function () {
        var bugId = this.bug.get("id");
        var projectKey = this.project.get("key");
        this.application.reqres.setHandler("getPreferredPerspective", function () {
            return "workflow";
        });
        this.controller.issueType(projectKey, bugId);
        ok(this.navigateStub.calledOnce, "navigate() should have been called once");
        equal(this.navigateStub.firstCall.args[0], projectKey + "/" + bugId + "/workflow", "Parameter should match.");
    });

    test("issueType last visited issue type tab is fields", function () {
        var bugId = this.bug.get("id");
        var projectKey = this.project.get("key");

        this.application.reqres.setHandler("getPreferredPerspective", function () {
            return "fields";
        });

        this.controller.issueType(projectKey, bugId);
        ok(this.navigateStub.calledOnce, "navigate() should have been called once");
        equal(this.navigateStub.firstCall.args[0], projectKey + "/" + bugId + "/fields", "Parameter should match.");
    });

    test("issueType replaces state", function () {
        this.controller.issueType(this.project.get("key"), this.bug.get("id"));
        ok(this.navigateStub.calledOnce, "navigate() should have been called once");
        deepEqual(this.navigateStub.firstCall.args[1], {replace: true}, "Parameter should match.");
    });

    test("fields", function () {
        var fieldsController;
        var issueTypeSelectedSpy = this.sandbox.spy();

        this.application.vent.on("issueTypes:issueTypeSelected", issueTypeSelectedSpy);

        this.controller.fields(this.project.get("key"), this.bug.get("id"));
        ok(issueTypeSelectedSpy.calledOnce);
        ok(issueTypeSelectedSpy.firstCall.args[0] === this.bug);

        fieldsController = this.mockFieldsController.firstCall.returnValue;
        ok(fieldsController.show.calledOnce);

        equal(fieldsController.opts.model.get("project"), this.project, "project should be set in the model");
        equal(fieldsController.opts.model.get("selectedIssueType"), this.fullBug, "selectedIssueType should be set in the model");
        equal(fieldsController.opts.model.get("selectedPerspective"), this.fieldsPerspectiveModel, "selectedPerspective should be set in the model");
        equal(fieldsController.opts.model.get("editing"), false, "editing should be set in the model");

        ok(this.application.title.calledOnce);

        ok(this.setPreferredPerspectiveSpy.calledOnce, "preferred perspective should have been updated");
        equal(this.setPreferredPerspectiveSpy.firstCall.args[0], "fields");
    });

    test("workflow on successful retrieval of draft workflow details", function () {
        var bugId = this.bug.get("id");
        var deferred = new Deferred();
        var issueTypeSelectedSpy = this.sandbox.spy();
        var projectKey = this.project.get("key");
        var workflowController;

        this.application.reqres.setHandler("workflowLayoutData", function () {
            return deferred.promise();
        });

        this.application.vent.on("issueTypes:issueTypeSelected", issueTypeSelectedSpy);

        this.controller.workflow(projectKey, bugId);
        workflowController = this.mockWorkflowController.firstCall.returnValue;
        ok(issueTypeSelectedSpy.calledOnce);
        ok(issueTypeSelectedSpy.firstCall.args[0] === this.bug);

        equal(workflowController.opts.model.get("project"), this.project, "project should be set in the model");
        equal(workflowController.opts.model.get("selectedIssueType"), this.bug, "selectedIssueType should be set in the model");
        equal(workflowController.opts.model.get("selectedPerspective"), this.workflowPerspectiveModel, "selectedPerspective should be set in the model");

        deferred.resolve({isDraft: true, layoutData: "DATA"});

        ok(this.navigateStub.calledOnce, "navigate() should have been called once");

        equal(this.navigateStub.firstCall.args[0], projectKey + "/" + bugId + "/workflow/edit", "Parameter should match.");
        deepEqual(this.navigateStub.firstCall.args[1], {replace: true, trigger: false}, "Parameter should match.");

        ok(workflowController.show.calledOnce);
        ok(workflowController.show.calledWith("DATA"));
        equal(workflowController.opts.model.get("selectedIssueType"), this.fullBug, "selectedIssueType should be set in the model");
        equal(workflowController.opts.model.get("editing"), true, "editing should be set in the model");

        ok(this.application.title.calledOnce);

        ok(this.setPreferredPerspectiveSpy.calledOnce, "preferred perspective should have been updated");
        equal(this.setPreferredPerspectiveSpy.firstCall.args[0], "workflow");
    });

    test("workflow on successful retrieval of non-draft workflow details", function () {
        var deferred = new Deferred();
        var issueTypeSelectedSpy = this.sandbox.spy();
        var workflowController;

        this.application.reqres.setHandler("workflowLayoutData", function () {
            return deferred.promise();
        });

        this.application.vent.on("issueTypes:issueTypeSelected", issueTypeSelectedSpy);

        this.controller.workflow(this.project.get("key"), this.bug.get("id"));
        workflowController = this.mockWorkflowController.firstCall.returnValue;
        ok(issueTypeSelectedSpy.calledOnce);
        ok(issueTypeSelectedSpy.firstCall.args[0] === this.bug);

        equal(workflowController.opts.model.get("project"), this.project, "project should be set in the model");
        equal(workflowController.opts.model.get("selectedIssueType"), this.bug, "selectedIssueType should be set in the model");
        equal(workflowController.opts.model.get("selectedPerspective"), this.workflowPerspectiveModel, "selectedPerspective should be set in the model");

        deferred.resolve({isDraft: false, layoutData: "DATA"});

        ok(!this.navigateStub.called, "navigate() should not have been called");

        ok(workflowController.show.calledOnce);
        ok(workflowController.show.calledWith("DATA"));
        equal(workflowController.opts.model.get("selectedIssueType"), this.fullBug, "selectedIssueType should be set in the model");
        equal(workflowController.opts.model.get("editing"), false, "editing should be set in the model");

        ok(this.application.title.calledOnce);

        ok(this.setPreferredPerspectiveSpy.calledOnce, "preferred perspective should have been updated");
        equal(this.setPreferredPerspectiveSpy.firstCall.args[0], "workflow");
    });

    test("workflow on successful retrieval of workflow details and different issue type now selected", function () {
        var firstDeferred = new Deferred();
        var secondDeferred = new Deferred();
        var workflowController = this.mockWorkflowController.firstCall.returnValue;

        this.application.reqres.setHandler("workflowLayoutData", function () {
            return firstDeferred.promise();
        });

        this.controller.workflow(this.project.get("key"), this.bug.get("id"));

        this.application.reqres.setHandler("workflowLayoutData", function () {
            return secondDeferred.promise();
        });

        this.controller.workflow(this.project.get("key"), this.story.get("id"));

        firstDeferred.resolve({isDraft: false, layoutData: "FIRST_DATA"});
        secondDeferred.resolve({isDraft: false, layoutData: "SECOND_DATA"});

        equal(workflowController.show.callCount, 1, "workflowController's show() should only have been called once.");
        equal(workflowController.show.args[0], "SECOND_DATA", "The layout passed to workflowController's show() should have been the second");
    });

    test("workflow on successful retrieval of workflow details and different perspective now selected", function () {
        var firstDeferred = new Deferred();
        var workflowController = this.mockWorkflowController.firstCall.returnValue;

        this.application.reqres.setHandler("workflowLayoutData", function () {
            return firstDeferred.promise();
        });

        this.controller.workflow(this.project.get("key"), this.bug.get("id"));
        this.controller.fields(this.project.get("key"), this.bug.get("id"));

        firstDeferred.resolve({isDraft: false, layoutData: "FIRST_DATA"});

        equal(workflowController.show.callCount, 0, "workflowController's show() should not have been called.");
    });

    test("workflow on successful retrieval of workflow details and same issue type and perspective selected, but other instance of same deferred route still in progress", function () {
        var firstDeferred = new Deferred();
        var secondDeferred = new Deferred();
        var workflowController = this.mockWorkflowController.firstCall.returnValue;

        this.application.reqres.setHandler("workflowLayoutData", function () {
            return firstDeferred.promise();
        });

        this.controller.workflow(this.project.get("key"), this.bug.get("id"));

        this.application.reqres.setHandler("workflowLayoutData", function () {
            return secondDeferred.promise();
        });

        this.controller.workflow(this.project.get("key"), this.bug.get("id"));

        firstDeferred.resolve({isDraft: false, layoutData: "FIRST_DATA"});
        secondDeferred.resolve({isDraft: false, layoutData: "SECOND_DATA"});

        equal(workflowController.show.callCount, 1, "workflowController's show() should only have been called once.");
        equal(workflowController.show.args[0], "SECOND_DATA", "The layout passed to workflowController's show() should have been the second");
    });

    test("workflow on failed retrieval of workflow details", function () {
        var errorMessage = "ERROR";
        var genericErrorSpy = this.sandbox.spy();
        this.application.commands.setHandler("error:generic", genericErrorSpy);
        this.application.reqres.setHandler("workflowLayoutData", function () {
            return new Deferred().reject({type: "error", message: errorMessage}).promise();
        });

        this.controller.workflow(this.project.get("key"), this.bug.get("id"));

        equal(genericErrorSpy.callCount, 1, "execute error:generic should only have been called once");
        deepEqual(genericErrorSpy.firstCall.args, [this.application.content, errorMessage], "message should have been the rejection's error message");
    });

    test("workflow on failed retrieval of workflow details and different issue type now selected", function () {
        var firstDeferred = new Deferred();
        var secondDeferred = new Deferred();
        var genericErrorSpy = this.sandbox.spy();

        this.application.commands.setHandler("error:generic", genericErrorSpy);
        this.application.reqres.setHandler("workflowLayoutData", function () {
            return firstDeferred.promise();
        });

        this.controller.workflow(this.project.get("key"), this.bug.get("id"));

        this.application.reqres.setHandler("workflowLayoutData", function () {
            return secondDeferred.promise();
        });

        this.controller.workflow(this.project.get("key"), this.story.get("id"));

        firstDeferred.reject({type: "error", message: "SOME_ERROR_1"});
        secondDeferred.reject({type: "error", message: "SOME_ERROR_2"});

        equal(genericErrorSpy.callCount, 1, "execute error:generic should only have been called once");
        deepEqual(genericErrorSpy.firstCall.args, [this.application.content, "SOME_ERROR_2"], "message should have been the rejection's error message");
    });

    test("workflow on failed retrieval of workflow details and different perspective now selected", function () {
        var firstDeferred = new Deferred();
        var genericErrorSpy = this.sandbox.spy();

        this.application.commands.setHandler("error:generic", genericErrorSpy);
        this.application.reqres.setHandler("workflowLayoutData", function () {
            return firstDeferred.promise();
        });

        this.controller.workflow(this.project.get("key"), this.bug.get("id"));
        this.controller.fields(this.project.get("key"), this.bug.get("id"));

        firstDeferred.reject({type: "error", message: "SOME_ERROR"});

        equal(genericErrorSpy.callCount, 0, "execute error:generic should not have been called");
    });

    test("workflow on failed retrieval of workflow details and same issue type and perspective selected, but other instance of same deferred route still in progress", function () {
        var firstDeferred = new Deferred();
        var secondDeferred = new Deferred();
        var genericErrorSpy = this.sandbox.spy();

        this.application.commands.setHandler("error:generic", genericErrorSpy);
        this.application.reqres.setHandler("workflowLayoutData", function () {
            return firstDeferred.promise();
        });

        this.controller.workflow(this.project.get("key"), this.bug.get("id"));

        this.application.reqres.setHandler("workflowLayoutData", function () {
            return secondDeferred.promise();
        });

        this.controller.workflow(this.project.get("key"), this.bug.get("id"));

        firstDeferred.reject({type: "error", message: "SOME_ERROR_1"});
        secondDeferred.reject({type: "error", message: "SOME_ERROR_2"});

        equal(genericErrorSpy.callCount, 1, "execute error:generic should only have been called once");
        deepEqual(genericErrorSpy.args[0], [this.application.content, "SOME_ERROR_2"], "message should have been the rejection's error message");
    });

    safeTest("workflow while retrieving workflow details displays progress indicator after timeout", function () {
        this.application.reqres.setHandler("workflowLayoutData", function () {
            return new Deferred().promise();
        });

        ok(!(this.controller.view.progressIndicator.currentView instanceof JIRA.ProjectConfig.ProgressIndicator.View));

        this.controller.workflow(this.project.get("key"), this.bug.get("id"));

        ok(!(this.controller.view.progressIndicator.currentView instanceof JIRA.ProjectConfig.ProgressIndicator.View));
        this.clock.tick(PROGRESS_TIMEOUT);
        ok(this.controller.view.progressIndicator.currentView instanceof JIRA.ProjectConfig.ProgressIndicator.View);
    });

    test("editWorkflow on successful retrieval of workflow details", function () {
        var deferred = new Deferred();
        var workflowController;
        var issueTypeSelectedSpy = this.sandbox.spy();

        this.application.reqres.setHandler("workflowLayoutData", function (workflow, requireDraft) {
            equal(requireDraft, true);
            return deferred.promise();
        });

        this.application.vent.on("issueTypes:issueTypeSelected", issueTypeSelectedSpy);

        this.controller.editWorkflow(this.project.get("key"), this.bug.get("id"));
        ok(issueTypeSelectedSpy.calledOnce);
        ok(issueTypeSelectedSpy.firstCall.args[0] === this.bug);

        workflowController = this.mockWorkflowController.firstCall.returnValue;

        equal(workflowController.opts.model.get("project"), this.project, "project should be set in the model");
        equal(workflowController.opts.model.get("selectedIssueType"), this.bug, "selectedIssueType should be set in the model");
        equal(workflowController.opts.model.get("selectedPerspective"), this.workflowPerspectiveModel, "selectedPerspective should be set in the model");

        deferred.resolve({isDraft: true, layoutData: "DATA"});
        ok(workflowController.show.calledOnce);

        ok(workflowController.show.calledWith("DATA"));

        equal(workflowController.opts.model.get("editing"), true, "editing should be set in the model");
        equal(workflowController.opts.model.get("selectedIssueType"), this.fullBug, "selectedIssueType should be set in the model");

        ok(this.application.title.calledOnce);

        ok(this.setPreferredPerspectiveSpy.calledOnce, "preferred perspective should have been updated");
        equal(this.setPreferredPerspectiveSpy.firstCall.args[0], "workflow");
    });

    test("editWorkflow on successful retrieval of workflow details and different issue type now selected", function () {
        var firstDeferred = new Deferred();
        var secondDeferred = new Deferred();
        var workflowController = this.mockWorkflowController.firstCall.returnValue;

        this.application.reqres.setHandler("workflowLayoutData", function (workflow, requireDraft) {
            equal(requireDraft, true);
            return firstDeferred.promise();
        });

        this.controller.editWorkflow(this.project.get("key"), this.bug.get("id"));

        this.application.reqres.setHandler("workflowLayoutData", function (workflow, requireDraft) {
            equal(requireDraft, true);
            return secondDeferred.promise();
        });

        this.controller.editWorkflow(this.project.get("key"), this.story.get("id"));

        firstDeferred.resolve({isDraft: true, layoutData: "FIRST_DATA"});
        secondDeferred.resolve({isDraft: true, layoutData: "SECOND_DATA"});

        equal(workflowController.show.callCount, 1, "workflowController's show() should only have been called once.");
        equal(workflowController.show.args[0], "SECOND_DATA", "The layout passed to workflowController's show() should have been the second");
    });

    test("editWorkflow on successful retrieval of workflow details and different perspective now selected", function () {
        var firstDeferred = new Deferred();
        var workflowController = this.mockWorkflowController.firstCall.returnValue;

        this.application.reqres.setHandler("workflowLayoutData", function (workflow, requireDraft) {
            equal(requireDraft, true);
            return firstDeferred.promise();
        });

        this.controller.editWorkflow(this.project.get("key"), this.bug.get("id"));
        this.controller.fields(this.project.get("key"), this.bug.get("id"));

        firstDeferred.resolve({isDraft: true, layoutData: "FIRST_DATA"});

        equal(workflowController.show.callCount, 0, "workflowController's show() should not have been called.");
    });

    test("editWorkflow on successful retrieval of workflow details and same issue type and perspective selected, but other instance of same deferred route still in progress", function () {
        var firstDeferred = new Deferred();
        var secondDeferred = new Deferred();
        var workflowController = this.mockWorkflowController.firstCall.returnValue;

        this.application.reqres.setHandler("workflowLayoutData", function (workflow, requireDraft) {
            equal(requireDraft, true);
            return firstDeferred.promise();
        });

        this.controller.editWorkflow(this.project.get("key"), this.bug.get("id"));

        this.application.reqres.setHandler("workflowLayoutData", function (workflow, requireDraft) {
            equal(requireDraft, true);
            return secondDeferred.promise();
        });

        this.controller.editWorkflow(this.project.get("key"), this.bug.get("id"));

        firstDeferred.resolve({isDraft: true, layoutData: "FIRST_DATA"});
        secondDeferred.resolve({isDraft: true, layoutData: "SECOND_DATA"});

        equal(workflowController.show.callCount, 1, "workflowController's show() should only have been called once.");
        equal(workflowController.show.args[0], "SECOND_DATA", "The layout passed to workflowController's show() should have been the second");
    });

    test("editWorkflow on failed retrieval of workflow details", function () {
        var errorMessage = "ERROR";
        var genericErrorSpy = this.sandbox.spy();

        this.application.commands.setHandler("error:generic", genericErrorSpy);
        this.application.reqres.setHandler("workflowLayoutData", function (workflow, requireDraft) {
            equal(requireDraft, true);
            return new Deferred().reject({type: "error", message: errorMessage}).promise();
        });

        this.controller.editWorkflow(this.project.get("key"), this.bug.get("id"));

        equal(genericErrorSpy.callCount, 1, "execute error:generic should only have been called once");
        deepEqual(genericErrorSpy.firstCall.args, [this.application.content, errorMessage], "message should have been the rejection's error message");
    });

    test("editWorkflow on failed retrieval of workflow details and different issue type now selected", function () {
        var firstDeferred = new Deferred();
        var secondDeferred = new Deferred();
        var genericErrorSpy = this.sandbox.spy();

        this.application.commands.setHandler("error:generic", genericErrorSpy);
        this.application.reqres.setHandler("workflowLayoutData", function (workflow, requireDraft) {
            equal(requireDraft, true);
            return firstDeferred.promise();
        });

        this.controller.editWorkflow(this.project.get("key"), this.bug.get("id"));

        this.application.reqres.setHandler("workflowLayoutData", function (workflow, requireDraft) {
            equal(requireDraft, true);
            return secondDeferred.promise();
        });

        this.controller.editWorkflow(this.project.get("key"), this.story.get("id"));

        firstDeferred.reject({type: "error", message: "SOME_ERROR_1"});
        secondDeferred.reject({type: "error", message: "SOME_ERROR_2"});

        equal(genericErrorSpy.callCount, 1, "execute error:generic should only have been called once");
        deepEqual(genericErrorSpy.firstCall.args, [this.application.content, "SOME_ERROR_2"], "message should have been the rejection's error message");
    });

    test("editWorkflow on failed retrieval of workflow details and different perspective now selected", function () {
        var firstDeferred = new Deferred();
        var genericErrorSpy = this.sandbox.spy();

        this.application.commands.setHandler("error:generic", genericErrorSpy);
        this.application.reqres.setHandler("workflowLayoutData", function (workflow, requireDraft) {
            equal(requireDraft, true);
            return firstDeferred.promise();
        });

        this.controller.editWorkflow(this.project.get("key"), this.bug.get("id"));
        this.controller.fields(this.project.get("key"), this.bug.get("id"));

        firstDeferred.reject({type: "error", message: "SOME_ERROR"});

        equal(genericErrorSpy.callCount, 0, "execute error:generic should not have been called");
    });

    test("editWorkflow on failed retrieval of workflow details and same issue type and perspective selected, but other instance of same deferred route still in progress", function () {
        var firstDeferred = new Deferred();
        var secondDeferred = new Deferred();
        var genericErrorSpy = this.sandbox.spy();

        this.application.commands.setHandler("error:generic", genericErrorSpy);
        this.application.reqres.setHandler("workflowLayoutData", function (workflow, requireDraft) {
            equal(requireDraft, true);
            return firstDeferred.promise();
        });

        this.controller.editWorkflow(this.project.get("key"), this.bug.get("id"));

        this.application.reqres.setHandler("workflowLayoutData", function (workflow, requireDraft) {
            equal(requireDraft, true);
            return secondDeferred.promise();
        });

        this.controller.editWorkflow(this.project.get("key"), this.bug.get("id"));

        firstDeferred.reject({type: "error", message: "SOME_ERROR_1"});
        secondDeferred.reject({type: "error", message: "SOME_ERROR_2"});

        equal(genericErrorSpy.callCount, 1, "execute error:generic should only have been called once");
        deepEqual(genericErrorSpy.args[0], [this.application.content, "SOME_ERROR_2"], "message should have been the rejection's error message");
    });

    test("application content shows issue type view when rendering route traversed to", function () {
        ok(!this.application.content.show.called, "region should not yet have shown anything");

        this.controller.fields(this.project.get("key"), this.bug.get("id"));

        ok(this.application.content.show.calledOnce, "region should have shown something");
        ok(this.application.content.show.firstCall.calledWith(this.controller.view), "region should have shown view");
    });

    test("issue types view shows header view when rendering route traversed to", function () {
        ok(!this.showHeaderSpy.called, "header controller should not yet have been shown");

        this.controller.fields(this.project.get("key"), this.bug.get("id"));

        ok(this.showHeaderSpy.calledOnce, "header controller should have been shown");
    });

    test("controllers' regions set when rendering route traversed to", function () {
        var headerController;
        var fieldsController;
        var workflowController;

        this.controller.fields(this.project.get("key"), this.bug.get("id"));

        headerController = this.mockHeaderController.firstCall.returnValue;
        fieldsController = this.mockFieldsController.firstCall.returnValue;
        workflowController = this.mockWorkflowController.firstCall.returnValue;

        equal(headerController.setRegion.callCount, 1, "setRegion() should have been called once on the header controller");
        equal(fieldsController.setRegion.callCount, 1, "setRegion() should have been called once on the fields controller");
        equal(workflowController.setRegion.callCount, 1, "setRegion() should have been called once on the workflow controller");
    });

    test("before:perspectiveRerender is fired on internal vent when rendering route traversed to", function () {
        var internalVent = this.mockHeaderController.firstCall.args[0].vent;
        var beforePerspectiveRerenderSpy = this.sandbox.spy();

        internalVent.on("before:perspectiveRerender", beforePerspectiveRerenderSpy);

        equal(beforePerspectiveRerenderSpy.callCount, 0, "before:perspectiveRerender should not yet have been fired");

        this.controller.fields(this.project.get("key"), this.bug.get("id"));

        equal(beforePerspectiveRerenderSpy.callCount, 1, "before:perspectiveRerender should have been fired once");
    });

    test("issue type URL is navigated to when header:issueTypeSelected is fired on internal vent", function () {
        var internalVent = this.mockHeaderController.firstCall.args[0].vent;

        this.controller.workflow(this.project.get("key"), this.bug.get("id"));

        equal(this.navigateStub.callCount, 0, "navigate() should not yet have been called");

        internalVent.trigger("header:issueTypeSelected", this.story);

        equal(this.navigateStub.callCount, 1, "navigate() should have been called once");
        equal(this.navigateStub.firstCall.args[0], this.project.get("key") + "/" + this.story.get("id") + "/workflow",
            "navigate() should have been called with the correct parameter");
    });

    test("base perspective URL is navigated to when header:perspectiveSelected is fired on internal vent", function () {
        var internalVent = this.mockHeaderController.firstCall.args[0].vent;

        this.controller.workflow(this.project.get("key"), this.bug.get("id"));

        equal(this.navigateStub.callCount, 0, "navigate() should not yet have been called");

        internalVent.trigger("header:perspectiveSelected", this.fieldsPerspectiveModel);

        equal(this.navigateStub.callCount, 1, "navigate() should have been called once");
        equal(this.navigateStub.firstCall.args[0], this.project.get("key") + "/" + this.bug.get("id") + "/fields",
            "navigate() should have been called with the correct parameter");
    });

    test("base perspective URL is navigated to when workflow:publishComplete is fired on internal vent", function () {
        var internalVent = this.mockWorkflowController.firstCall.args[0].vent;

        this.controller.workflow(this.project.get("key"), this.bug.get("id"));

        equal(this.navigateStub.callCount, 0, "navigate() should not yet have been called");

        internalVent.trigger("workflow:publishComplete");

        equal(this.navigateStub.callCount, 1, "navigate() should have been called once");
        equal(this.navigateStub.firstCall.args[0], this.project.get("key") + "/" + this.bug.get("id") + "/workflow",
            "navigate() should have been called with the correct parameter");
    });

    test("base perspective URL is navigated to when workflow:discardComplete is fired on internal vent", function () {
        var internalVent = this.mockWorkflowController.firstCall.args[0].vent;

        this.controller.workflow(this.project.get("key"), this.bug.get("id"));

        equal(this.navigateStub.callCount, 0, "navigate() should not yet have been called");

        internalVent.trigger("workflow:discardComplete");

        equal(this.navigateStub.callCount, 1, "navigate() should have been called once");
        equal(this.navigateStub.firstCall.args[0], this.project.get("key") + "/" + this.bug.get("id") + "/workflow",
            "navigate() should have been called with the correct parameter");
    });

    test("edit workflow URL is navigated to when workflow:editWorkflow is fired on internal vent", function () {
        var internalVent = this.mockWorkflowController.firstCall.args[0].vent;

        this.controller.workflow(this.project.get("key"), this.bug.get("id"));

        equal(this.navigateStub.callCount, 0, "navigate() should not yet have been called");

        internalVent.trigger("workflow:editWorkflow");

        equal(this.navigateStub.callCount, 1, "navigate() should have been called once");
        equal(this.navigateStub.firstCall.args[0], this.project.get("key") + "/" + this.bug.get("id") + "/workflow/edit",
            "navigate() should have been called with the correct parameter");
    });
});

