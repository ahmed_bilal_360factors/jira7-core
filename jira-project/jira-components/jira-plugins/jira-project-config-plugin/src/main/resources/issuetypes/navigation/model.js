define("jira-project-config/issuetypes/navigation/model", ["require"], function (require) {
    "use strict";

    var Backbone = require("jira-project-config/backbone");
    var IssueTypeLinks = require('jira-project-config/issuetypes/entities/models/issue-type-links');

    var Model = Backbone.Model.extend(
        /** @lends JIRA.ProjectConfig.IssueTypes.Navigation.Model# */
        {
            defaults: function () {
                return {
                    /**
                     * All issue types to display.
                     *
                     * @memberof JIRA.ProjectConfig.IssueTypes.Navigation.Model#
                     * @type {JIRA.ProjectConfig.IssueTypes.Entities.IssueTypeLinks}
                     */
                    issueTypeLinks: new IssueTypeLinks(),

                    /**
                     * The current project key.
                     *
                     * @memberof JIRA.ProjectConfig.IssueTypes.Navigation.Model#
                     * @type {(null|string)}
                     */
                    projectKey: null,

                    /**
                     * The currently selected item in the navigation.
                     *
                     * @memberof JIRA.ProjectConfig.IssueTypes.Navigation.Model#
                     * @type {(null|JIRA.ProjectConfig.IssueTypes.Navigation.SUMMARY_SELECTION|JIRA.ProjectConfig.IssueTypes.Entities.IssueType)}
                     */
                    selection: null,

                    /**
                     * The number of issue types to *not hide* in "collapsed" mode.
                     *
                     * @memberof JIRA.ProjectConfig.IssueTypes.Navigation.Model#
                     * @type {number}
                     */
                    teaserCount: 10,

                    /**
                     * If true, indicates that at least 1 issue type is being hidden from the menu.
                     *
                     * This attribute should be considered 'readonly' and as such not modified directly. It is updated
                     * automatically in response to other attributes changing.
                     *
                     * @memberof JIRA.ProjectConfig.IssueTypes.Navigation.Model#
                     * @type {boolean}
                     * @readonly
                     */
                    collapsed: false,

                    /**
                     * If true, indicates the user wants the navigation expanded. i.e. they've clicked the 'expand' link.
                     *
                     * @memberof JIRA.ProjectConfig.IssueTypes.Navigation.Model#
                     * @type {boolean}
                     */
                    userWantsExpandedMode: false
                };
            },

            /**
             * Initialises the model.
             *
             * @classdesc Contains the state of the Issue Types navigation, for example if it's collapsed or not, which
             *   issue types are involved, etc.
             * @constructs
             * @extends Backbone.Model
             */
            initialize: function () {
                this.listenTo(this, "change:teaserCount change:selection change:userWantsExpandedMode", this._updateCollapsed);
                this.listenTo(this.get("issueTypeLinks"), "add remove reset", this._updateCollapsed);

                this._updateCollapsed();
            },

            /**
             * Update the 'collapsed' attribute
             * @private
             */
            _updateCollapsed: function () {
                var collapsed;

                if (this.get("userWantsExpandedMode") || this.get("selection")) {
                    collapsed = false;
                } else {
                    // We add 1 so that if there are 1 more issue types than our teaser count, we do not collapse.
                    // This is done as the expand link consumes 1 line - might as well use that line to display the single
                    // issue type
                    collapsed = this.get("issueTypeLinks").length > (this.get("teaserCount") + 1);
                }
                this.set("collapsed", collapsed);
            }
        });

    /**
     * A type to represent the string `"summary"`.
     * @typedef JIRA.ProjectConfig.IssueTypes.Navigation.SUMMARY_SELECTION
     * @type {string}
     */
    Model.SUMMARY_SELECTION = "summary";

    return Model;
});
