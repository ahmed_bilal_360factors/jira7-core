AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-entities"], function () {
    "use strict";

    var BreadcrumbUtil = require('jira-project-config/issuetypes/entities/models/issuetypes-breadcrumb-util');
    var wrmContextPath = require("wrm/context-path");

    module("jira-project-config/issuetypes/entities/models/issuetypes-breadcrumb-util");

    test("Defaults", function () {
        var breadcrumbs = BreadcrumbUtil.makeIssueTypesBreadcrumbs("PONE", "issueTypeRoot", "issueTypeOne");
        var allIssueTypesBreadcrumb = breadcrumbs.at(0);
        var issueTypeBreadcrumb = breadcrumbs.at(1);
        var contextPath = wrmContextPath();

        equal(breadcrumbs.length, 2);

        equal(allIssueTypesBreadcrumb.get("link"), contextPath + "/plugins/servlet/project-config/PONE/issuetypes");
        equal(allIssueTypesBreadcrumb.get("name"), "issueTypeRoot");
        equal(allIssueTypesBreadcrumb.get("selected"), false);

        equal(issueTypeBreadcrumb.get("name"), "issueTypeOne");
        equal(issueTypeBreadcrumb.get("link"), undefined);
        equal(issueTypeBreadcrumb.get("selected"), true);
    });
});
