define('jira-project-config/issuetypes/entities/models/issue-type-links', ['require'], function (require) {
    "use strict";

    var Backbone = require('jira-project-config/backbone');
    var IssueTypeLink = require('jira-project-config/issuetypes/entities/models/issue-type-link');

    /**
     * @class
     * @classdesc A collection of {@link JIRA.ProjectConfig.IssueTypes.Entities.IssueTypeLink}.
     * @extends Backbone.Collection
     */
    return Backbone.Collection.extend({
        model: IssueTypeLink
    });
});
