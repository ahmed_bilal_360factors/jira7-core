define('jira-project-config/issuetypes/entities/models/workflow', ['require'], function (require) {
    "use strict";

    var Backbone = require('jira-project-config/backbone');
    var _ = require('underscore');
    var IssueTypes = require('jira-project-config/issuetypes/entities/models/issue-types');
    var Projects = require('jira-project-config/issuetypes/entities/models/projects');

    /**
     * @class
     * @classdesc A simple description of a workflow, it doesn't contain everything, but it's enough for the moment to
     * build the page.
     * @extends Backbone.Model
     */
    return Backbone.Model.extend(
        /** @lends JIRA.ProjectConfig.IssueTypes.Entities.Workflow# */
        {
            defaults: function () {
                return {
                    /**
                     * Total number of sharing projects (including those not visible to the current user).
                     *
                     * @memberof JIRA.ProjectConfig.IssueTypes.Entities.Workflow#
                     * @type {number}
                     */
                    totalProjectsCount: 0,

                    /**
                     * The number of sharing projects not visible to the current user.
                     *
                     * @memberof JIRA.ProjectConfig.IssueTypes.Entities.Workflow#
                     * @type {number}
                     */
                    hiddenProjectsCount: 0,

                    /**
                     * The name of the workflow. This is also used as its ID. Should always be provided.
                     *
                     * @memberof JIRA.ProjectConfig.IssueTypes.Entities.Workflow#
                     * @type {string}
                     */
                    name: undefined,

                    /**
                     * The display name of the workflow. This is should be used whenever displaying the name to the user.
                     * Should always be provided.
                     *
                     * @memberof JIRA.ProjectConfig.IssueTypes.Entities.Workflow#
                     * @type {string}
                     */
                    displayName: undefined,

                    /**
                     * When non-`null`, the value must be one of:
                     *
                     *  - `"nopermission"` for a user who can't edit the workflow.
                     *  - `"readonly"` for a workflow that can never be edited.
                     *  - `"readonlydelegatedshared"` for a workflow that can not be edited by user that have workflow edit permission because workflow is shared.
                     *  - `"editable"` for a workflow that can be edited.
                     *  - `"editabledelegated"` for a workflow that can be edited by user that have workflow edit permission.
                     *  - `"migrate"` for a workflow that can only be edited after a workflow migration.
                     *
                     * Should always be provided.
                     *
                     * @memberof JIRA.ProjectConfig.IssueTypes.Entities.Workflow#
                     * @type {string}
                     */
                    state: undefined,

                    /**
                     * Other projects that share the same workflow.
                     *
                     * @memberof JIRA.ProjectConfig.IssueTypes.Entities.Workflow#
                     * @type {JIRA.ProjectConfig.IssueTypes.Entities.Projects}
                     */
                    sharedWithProjects: new Projects(),

                    /**
                     * Other issue types that share the same workflow.
                     *
                     * @memberof JIRA.ProjectConfig.IssueTypes.Entities.Workflow#
                     * @type {JIRA.ProjectConfig.IssueTypes.Entities.IssueTypes}
                     */
                    sharedWithIssueTypes: new IssueTypes()
                };
            },

            toJSON: function () {
                return _.extend({}, this.attributes, {
                    totalProjectsCount: this.attributes.totalProjectsCount,
                    hiddenProjectsCount: this.attributes.hiddenProjectsCount,
                    sharedWithProjects: this.attributes.sharedWithProjects.toJSON(),
                    sharedWithIssueTypes: this.attributes.sharedWithIssueTypes.toJSON()
                });
            }
        });
});
