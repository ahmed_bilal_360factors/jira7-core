AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-entities"], function(){
    "use strict";

    var Project = require("jira-project-config/issuetypes/entities/models/project");

    module("JIRA.ProjectConfig.IssueTypes.Entities.Project");

    test("Defaults", function () {
        var project = new Project();

        equal(project.get("id"), undefined);
        equal(project.get("key"), undefined);
        equal(project.get("name"), undefined);
    });
});
