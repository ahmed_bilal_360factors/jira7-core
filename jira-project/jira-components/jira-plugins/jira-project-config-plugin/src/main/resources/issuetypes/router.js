define("jira-project-config/issuetypes/router", ["require"], function(require){
    "use strict";

    var Marionette = require('jira-project-config/marionette');

    return Marionette.AppRouter.extend({
        appRoutes: {
            "plugins/servlet/project-config/:project/issuetypes(/)": "summary",
            "plugins/servlet/project-config/:project/issuetypes/:issueType(/)": "issueType",
            "plugins/servlet/project-config/:project/issuetypes/:issueType/workflow(/)": "workflow",
            "plugins/servlet/project-config/:project/issuetypes/:issueType/workflow/edit(/)": "editWorkflow",
            "plugins/servlet/project-config/:project/issuetypes/:issueType/fields(/)": "fields",
            "plugins/servlet/project-config/:project/issuetypes/*junk": "error404"
        }
    });
});
