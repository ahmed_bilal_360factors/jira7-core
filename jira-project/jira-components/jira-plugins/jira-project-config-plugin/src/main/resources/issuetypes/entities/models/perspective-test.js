AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-entities"], function(){
    "use strict";

    var Perspective = require("jira-project-config/issuetypes/entities/models/perspective");

    module("JIRA.ProjectConfig.IssueTypes.Entities.Perspective");

    test("Defaults", function () {
        var perspective = new Perspective();

        equal(perspective.get("id"), undefined);
        equal(perspective.get("name"), undefined);
    });
});
