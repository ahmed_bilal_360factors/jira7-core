AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-tab"], function(){
    "use strict";

    var IssueTypesView = require("jira-project-config/issuetypes/view");

    module("JIRA.ProjectConfig.IssueTypes.View");

    test("all regions are rendered", function () {
        var view = new IssueTypesView();
        var $viewEl = view.render().$el;

        equal($viewEl.find(view.content.el).length, 1, "content region exists");
        equal($viewEl.find(view.header.el).length, 1, "header region exists");
        equal($viewEl.find(view.progressIndicator.el).length, 1, "progress indicator region exists");
    });
});
