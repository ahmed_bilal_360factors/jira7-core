define('jira-project-config/issuetypes/header/descriptor/view', ['require'], function(require) {
    "use strict";

    var TemplateDescriptor = require('jira-project-config/issuetypes/header/descriptor/view/templates');
    var Marionette = require('jira-project-config/marionette');

    /**
     * @class
     * @classdesc A view for showing the descriptor for the header.
     * @extends Marionette.ItemView
     */
    return Marionette.ItemView.extend(
        /** @lends JIRA.ProjectConfig.IssueTypes.Header.Perspectives.View# */
        {
            className: "aui-page-header-main",
            template: TemplateDescriptor.render
        });
});
