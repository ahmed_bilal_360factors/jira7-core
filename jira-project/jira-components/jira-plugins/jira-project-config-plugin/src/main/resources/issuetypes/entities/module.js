define('jira-project-config/issuetypes/entities/module', ['require'], function (require) {
    "use strict";

    var _ = require('underscore');
    var Manager = require('jira-project-config/issuetypes/entities/manager');

    /**
     * @class
     * @classdesc A module that adds handlers to the application's reqres to expose entities.
     * @constructor
     * @extends Marionette.Module
     */
    return function (module, application) {
        var reqres = application.reqres;
        module.addInitializer(function () {
            var manager = new Manager({
                reqres: reqres
            });

            var getPreferredPerspective = _.bind(manager.getPreferredPerspective, manager);
            var workflowLayoutData = _.bind(manager.workflowLayoutData, manager);
            var issueType = _.bind(manager.issueType, manager);
            var issueTypeLinks = _.bind(manager.issueTypeLinks, manager);
            var setPreferredPerspective = _.bind(manager.setPreferredPerspective, manager);
            var project = _.bind(manager.project, manager);

            reqres.setHandlers({
                getPreferredPerspective: getPreferredPerspective,
                workflowLayoutData: workflowLayoutData,
                issueType: issueType,
                issueTypeLinks: issueTypeLinks,
                project: project,
                setPreferredPerspective: setPreferredPerspective
            });
        });

        module.addFinalizer(function () {
            reqres.removeHandler("getPreferredPerspective");
            reqres.removeHandler("workflowLayoutData");
            reqres.removeHandler("issueType");
            reqres.removeHandler("issueTypeLinks");
            reqres.removeHandler("project");
            reqres.removeHandler("setPreferredPerspective");
        });
    };
});
