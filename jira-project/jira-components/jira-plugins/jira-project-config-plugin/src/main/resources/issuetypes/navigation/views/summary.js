define('jira-project-config/issuetypes/navigation/views/summary', ['require'], function(require) {
    "use strict";

    var Marionette = require("jira-project-config/marionette");
    var TemplateNavigation = require('jira-project-config/issuetypes/navigation/templates');

    return Marionette.ItemView.extend(
        /** @lends JIRA.ProjectConfig.IssueTypes.Navigation.SummaryView# */
        {
            attributes: {
                id: "project-issuetypes-summary"
            },

            /**
             * @classdesc Renders the the 'Issue Types' link at the top of the issue types section.
             * @constructs
             * @extends Marionette.ItemView
             * @param {JIRA.ProjectConfig.IssueTypes.Navigation.Model} model
             */
            initialize: function () {
                this.listenTo(this.model, "change:projectKey", this.render);
                this.listenTo(this.model, "change:selection", this._updateSelected);
                this.listenTo(this, "render", this._updateSelected);
            },

            tagName: "li",

            template: TemplateNavigation.summary,

            /**
             * @private
             */
            _updateSelected: function () {
                this.$el.toggleClass("aui-nav-selected", this.model.get("selection") === "summary");
            }
        });
});
