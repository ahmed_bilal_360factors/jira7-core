define("jira-project-config/issuetypes/perspectives/workflow/controller", ["require"], function (require) {
    "use strict";

    var _ = require('underscore');
    var BreadcrumbUtil = require('jira-project-config/issuetypes/entities/models/issuetypes-breadcrumb-util');
    var HeaderDescriptor = require('jira-project-config/issuetypes/entities/models/header-descriptor');
    var jQuery = require('jquery');
    var Marionette = require('jira-project-config/marionette');
    var Perspective = require('jira-project-config/issuetypes/entities/models/perspective');
    var PluggableActionsView = require('jira-project-config/issuetypes/perspectives/workflow/pluggable-actions/view');
    var PluggableInfoView = require('jira-project-config/issuetypes/perspectives/workflow/workflowinfo/view');
    var SharedByData = require('jira-project-config/issuetypes/entities/models/shared-by-data');
    var WorkflowModel = require('jira-project-config/issuetypes/perspectives/workflow/model');
    var WorkflowView = require("jira-project-config/issuetypes/perspectives/workflow/view");
    var Analytics = require("jira-project-config/issuetypes/perspectives/workflow/analytics");


    return Marionette.Controller.extend(
        /** @lends JIRA.ProjectConfig.IssueTypes.Workflow.Controller# */
        {
            /**
             * Initialises the controller.
             *
             * @classdesc The controller responsible for orchestrating the content area when viewing the workflow for an
             *   issue type in a project.
             * @constructs
             * @extends Marionette.Controller
             * @param {Marionette.Wreqr.Commands} options.commands The channel for giving commands. Must support
             *   - `issueTypes:resetPluggableRegions`
             * @param {JIRA.ProjectConfig.IssueTypes.Model} options.model The state of the Issue Types area of the application.
             * @param {Marionette.Region} options.region The region to render content into.
             * @param {Marionette.Wreqr.EventAggregator} options.vent The channel for firing off events and listening for events.
             *   This controller triggers:
             *    - `workflow:editWorkflow` when the edit button is clicked in the actions view
             *    - `workflow:discardComplete` when a workflow has been successfully discarded
             *    - `workflow:publishComplete` when a workflow has been successfully published
             *   Other entities trigger:
             *    - `before:perspectiveRerender` just before a rendering route is executed.
             */
            initialize: function (options) {
                this.commands = options.commands;
                this.pageModel = options.model;
                this.perspectiveModel = new Perspective({
                    id: "workflow",
                    name: AJS.I18n.getText("admin.issuetypeconfig.perspective.workflow.name")
                });
                this.region = options.region;
                this.sharedByData = new SharedByData({
                    issueTypesTitle: AJS.I18n.getText("admin.issuetypeconfigproject.shared.list.workflow.issuetype"),
                    projectsTitle: AJS.I18n.getText("admin.issuetypeconfigproject.shared.list.workflow.project")
                });
                this.viewModel = new WorkflowModel();
                this.vent = options.vent;

                // Freshen-up now.
                this.pageModel.get("selectedIssueType") && this._refresh();

                // Stay up-to-date as the parent model changes in the future.
                this.listenTo(this.pageModel, "change:editing change:project change:selectedIssueType", this._refresh);
            },

            /**
             * @returns {JIRA.ProjectConfig.IssueTypes.Entities.Perspective} the model that represents this perspective.
             */
            getPerspectiveModel: function () {
                return this.perspectiveModel;
            },

            /**
             * Sets the region.
             *
             * @param {Marionette.Region} region the region to set
             */
            setRegion: function (region) {
                this.region = region;
            },

            /**
             * Shows the workflow view in the region, and also makes requests to display the descriptor,
             * to display the shared by data and to display the pluggable actions.
             *
             * @param {Object} [layoutData] the layout data to optionally pass to the Workflow Designer to display.
             */
            show: function (layoutData) {
                if (this.viewModel.get("editing")) {
                    Analytics.triggerEditProject(this._userWithWorkflowEdit());
                } else {
                    Analytics.triggerViewProject(this._userWithWorkflowEdit());
                }

                // Display the pluggable regions
                this.commands.execute("issueTypes:resetPluggableRegions", {
                    actions: this._createPluggableActions(),
                    info: this._createPluggableInfo(),
                    descriptor: this._getDescriptor(),
                    sharedBy: this.sharedByData.clone(),
                    workflow: this._getIssueTypeWorkflow()
                });

                // Display the main content
                this._renderWorkflowDesigner(layoutData);
            },

            _getIssueTypeWorkflow: function () {
                if (this.viewModel.get('issueType')) {
                    return this.viewModel.get('issueType').get('workflow');
                }
            },

            /**
             * @returns {JIRA.ProjectConfig.IssueTypes.Entities.HeaderDescriptor} the descriptor that describes this perspective.
             * @private
             */
            _getDescriptor: function () {
                var issueType = this.viewModel.get("issueType");
                return new HeaderDescriptor({
                    subtitle: AJS.I18n.getText("admin.issuetypeconfig.workflow.subheading", issueType.get("workflow").get("displayName")),
                    title: AJS.I18n.getText("admin.issuetypeconfig.workflow.heading", issueType.get("name")),
                    breadcrumbs: BreadcrumbUtil.makeIssueTypesBreadcrumbs(this._getProjectKey(), AJS.I18n.getText("admin.schemes.workflow.issuetypes"), issueType.get("name"))
                });
            },

            /**
             * Get project key.
             *
             * @returns {string|null} project key.
             * @private
             */
            _getProjectKey: function () {
                if (_.isUndefined(this.pageModel) || _.isNull(this.pageModel) || _.isEmpty(this.pageModel)) {
                    return null;
                }
                if (_.isUndefined(this.pageModel.get("project")) || _.isNull(this.pageModel.get("project")) || _.isEmpty(this.pageModel.get("project"))) {
                    return null;
                }
                return this.pageModel.get("project").get("key");
            },

            /**
             * @private
             */
            _handlePublishOrDiscardRequest: function (promise, actionsView, completeEvent) {
                var self = this;
                actionsView.disable(true);
                promise.done(function () {
                    self.vent.trigger(completeEvent);
                });
                promise.fail(function () {
                    actionsView.disable(false);
                });
            },

            /**
             * @param {JIRA.ProjectConfig.IssueTypes.Workflow.PluggableActions.View} actionsView the pluggable actions view
             * @private
             */
            _onBeforePerspectiveRerender: function (actionsView) {
                actionsView.disable(true);
            },

            /**
             * @param {JIRA.ProjectConfig.IssueTypes.Workflow.PluggableActions.View} actionsView the pluggable actions view
             * @private
             */
            _onDiscardWorkflow: function (actionsView) {
                var promise;

                promise = this.designer.discardDraft({
                    reloadDesigner: false
                });
                promise.done(function () {
                    Analytics.triggerDiscardEditedWorkflow(this._userWithWorkflowEdit());
                }.bind(this)).fail(function () {
                    Analytics.triggerDiscardEditedWorkflowFail(this._userWithWorkflowEdit());
                }.bind(this));

                this._handlePublishOrDiscardRequest(promise, actionsView, "workflow:discardComplete");
            },

            /**
             * @private
             */
            _onEditWorkflow: function (params) {
                this.vent.trigger("workflow:editWorkflow");
                Analytics.triggerEditedWorkflowClicked(params && params.workflowEditPermission);
            },

            /**
             * @param {JIRA.ProjectConfig.IssueTypes.Workflow.PluggableActions.View} actionsView the pluggable actions view
             * @private
             */
            _onPublishWorkflow: function (actionsView) {
                var promise;

                promise = this.designer.publishDraft({
                    reloadDesigner: false
                });
                promise.done(function () {
                    Analytics.triggerPublishEditedWorkflow(this._userWithWorkflowEdit());
                }.bind(this)).fail(function () {
                    Analytics.triggerPublishEditedWorkflowFail(this._userWithWorkflowEdit());
                }.bind(this));

                this._handlePublishOrDiscardRequest(promise, actionsView, "workflow:publishComplete");
            },

            /**
             * @private
             */
            _refresh: function () {
                this.viewModel.set({
                    editing: this.pageModel.get("editing"),
                    issueType: this.pageModel.get("selectedIssueType"),
                    project: this.pageModel.get("project")
                });
                this._refreshSharedByData();
            },

            /**
             * @private
             */
            _refreshSharedByData: function () {
                var issueType = this.viewModel.get("issueType");
                var sharedWithIssueTypes = null;
                var sharedWithProjects = null;
                var totalProjectsCount = 0;
                var hiddenProjectsCount = 0;
                var workflow;
                if (issueType) {
                    workflow = issueType.get("workflow");
                    if (workflow) {
                        sharedWithIssueTypes = workflow.get("sharedWithIssueTypes");
                        sharedWithProjects = workflow.get("sharedWithProjects");
                        totalProjectsCount = workflow.get("totalProjectsCount");
                        hiddenProjectsCount = workflow.get("hiddenProjectsCount");
                    }
                }
                this.sharedByData.set({
                    projects: sharedWithProjects,
                    issueTypes: sharedWithIssueTypes,
                    totalProjectsCount: totalProjectsCount,
                    hiddenProjectsCount: hiddenProjectsCount
                });
            },

            /**
             * Creates the pluggable actions view and attaches the appropriate listeners.
             * @returns {JIRA.ProjectConfig.IssueTypes.Workflow.PluggableActions.View} the view to display in the actions region.
             * @private
             */
            _createPluggableActions: function () {
                var boundOnBeforePerspectiveRerender;

                var pluggableActionsView = new PluggableActionsView({
                    model: this.viewModel
                });

                this.listenTo(pluggableActionsView, "click:edit", _.bind(this._onEditWorkflow, this));

                boundOnBeforePerspectiveRerender = _.bind(this._onBeforePerspectiveRerender, this, pluggableActionsView);

                this.listenTo(this.vent, "before:perspectiveRerender", boundOnBeforePerspectiveRerender);

                this.listenTo(pluggableActionsView, "close", function () {
                    this.stopListening(pluggableActionsView);
                    this.stopListening(this.vent, "before:perspectiveRerender", boundOnBeforePerspectiveRerender);
                });

                return pluggableActionsView;
            },

            /**
             * Creates the pluggable info view and attaches the appropriate listeners.
             * @returns {JIRA.ProjectConfig.IssueTypes.Workflow.WorkflowInfo.View} the view to display in the info region.
             * @private
             */
            _createPluggableInfo: function () {
                var boundOnBeforePerspectiveRerender;

                var pluggableInfoView = new PluggableInfoView({
                    model: this.viewModel
                });

                this.listenTo(pluggableInfoView, "click:discard", _.bind(this._onDiscardWorkflow, this, pluggableInfoView));
                this.listenTo(pluggableInfoView, "click:publish", _.bind(this._onPublishWorkflow, this, pluggableInfoView));

                boundOnBeforePerspectiveRerender = _.bind(this._onBeforePerspectiveRerender, this, pluggableInfoView);

                this.listenTo(this.vent, "before:perspectiveRerender", boundOnBeforePerspectiveRerender);

                this.listenTo(pluggableInfoView, "close", function () {
                    this.stopListening(pluggableInfoView);
                    this.stopListening(this.vent, "before:perspectiveRerender", boundOnBeforePerspectiveRerender);
                });

                return pluggableInfoView;
            },

            /**
             * Renders the workflow designer with the provided layout data.
             * @param {Object} layoutData the layout data for the workflow designer to display.
             * @private
             */
            _renderWorkflowDesigner: function (layoutData) {
                var contentView = new WorkflowView();
                var workflow = this.viewModel.get("issueType").get("workflow");

                this.region.show(contentView);

                this.designer = new JIRA.WorkflowDesigner.Application({
                    element: contentView.ui.workflowDesigner,
                    immutable: !this.viewModel.get("editing"),
                    layoutData: layoutData,
                    workflowId: workflow.get("name"),
                    fullScreenButton: false,
                    draft: this.viewModel.get("editing"),
                    actions: this.viewModel.get("editing")
                });

                this.listenTo(contentView, "close", function () {
                    this.designer.destroy();
                    jQuery(contentView.el).empty();
                    this.stopListening(contentView);
                });
            },

            _userWithWorkflowEdit: function () {
                return this.pageModel.get("selectedIssueType") &&
                    this.pageModel.get("selectedIssueType").get('workflow') &&
                    'editabledelegated' === this.pageModel.get("selectedIssueType").get('workflow').get('state');
            }
        });
});
