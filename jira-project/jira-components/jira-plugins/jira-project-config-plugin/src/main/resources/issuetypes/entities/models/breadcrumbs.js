define('jira-project-config/issuetypes/entities/models/breadcrumbs', ['require'], function(require) {
    "use strict";

    var Backbone = require('jira-project-config/backbone');
    var Breadcrumb = require('jira-project-config/issuetypes/entities/models/breadcrumb');

    /**
     * @class
     * @classdesc A collection of {@link module:jira-project-config/issuetypes/entities/models/breadcrumb}.
     * @extends Backbone.Collection
     */
    return Backbone.Collection.extend(
        /** @lends module:jira-project-config/issuetypes/entities/models/breadcrumbs */
        {
            model: Breadcrumb
        }
    );
});
