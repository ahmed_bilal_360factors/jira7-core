define("jira-project-config/issuetypes/perspectives/fields/view", ["require"], function (require) {
    "use strict";

    var Marionette = require('jira-project-config/marionette');
    var TemplateTab = require('jira-project-config/issuetypes/perspectives/fields/templates');

    /**
     * @class
     * @classdesc A view that handles rendering the entire right side of the screen. It renders the screen editor
     *   for a single issue type in a project.
     * @constructor
     * @extends Marionette.ItemView
     */
    return Marionette.ItemView.extend(
        /** @lends JIRA.ProjectConfig.IssueTypes.Fields.View */
        {
            ui: {
                screenEditor: "#screenEditor"
            },

            /**
             * Attempts to start the screen editor.
             *
             * @private
             */
            onShow: function () {
                this.editor = new JIRA.ScreenEditor.Application({
                    analyticsName: "project",
                    element: this.ui.screenEditor,
                    projectKey: this.model.get("project").get("key"),
                    readOnly: !JIRA.isAdmin(),
                    screenId: this.model.get("issueType").get("fields").get("viewScreenId")
                });

            },

            template: TemplateTab.render
        });
});
