AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-entities"], function () {
    "use strict";

    var Breadcrumb = require("jira-project-config/issuetypes/entities/models/breadcrumb");

    module("jira-project-config/issuetypes/entities/models/breadcrumb validations");

    test("when no options object is passed in, an Error is thrown", function (asserts) {
        asserts["throws"](function () {
            new Breadcrumb();
        });
    });

    test("options.name is always required", function (asserts) {
        asserts["throws"](function () {
            new Breadcrumb({
                selected: true,
                link: 'link'
            });
        });
    });

    test("options.selected, when defined, must be a valid boolean", function (asserts) {
        asserts["throws"](function () {
            new Breadcrumb({
                name: 'name',
                selected: 'not-a-boolean',
                link: 'link'
            });
        });
    });

    test("options.link cannot be passed in when options.selected is true", function (asserts) {
        asserts["throws"](function () {
            new Breadcrumb({
                name: 'name',
                selected: true,
                link: 'link-should-be-rejected'
            });
        });
    });

    test("selected state can be omitted as it defaults to false", function () {
        var breadcrumb = new Breadcrumb({
            name: 'name',
            link: 'link'
        });
        strictEqual(breadcrumb.get('selected'), false);
    });

    test("selected state can also be explicitly defined as false", function () {
        var breadcrumb = new Breadcrumb({
            name: 'name',
            link: 'link',
            selected: false
        });
        strictEqual(breadcrumb.get('selected'), false);
    });

    test("should respect the selected attribute passed in to the constructor", function () {
        var breadcrumb = new Breadcrumb({
            name: 'name',
            selected: true
        });
        strictEqual(breadcrumb.get('selected'), true);
    });
});
