AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-tab", "com.atlassian.jira.jira-project-config-plugin:issuetypes-test-utils"], function(){
    "use strict";

    var Marionette = require("jira-project-config/marionette");
    var _ = require("underscore");
    var Chance = require("jira-project-config/libs/chance");
    var ChanceMixins = require("jira-project-config/issuetypes/entities/test/chance-mixins");
    var jQuery = require("jquery");
    var IssueTypes = require("jira-project-config/issuetypes/entities/models/issue-types");
    var Projects = require("jira-project-config/issuetypes/entities/models/projects");
    var FieldsView = require("jira-project-config/issuetypes/perspectives/fields/view");
    var FieldsViewModel = require("jira-project-config/issuetypes/perspectives/fields/model");

    module("JIRA.ProjectConfig.IssueTypes.Fields.View", {
        setup: function () {
            this.sandbox = sinon.sandbox.create();
            this.chance = new Chance(1);
            _.extend(this.chance, new ChanceMixins());


            this.project1 = this.chance.project();
            this.project2 = this.chance.project();
            this.project3 = this.chance.project();

            this.bug = this.chance.issueType().set("fields",
                this.chance.fields().set("sharedWithProjects", new Projects([this.project2, this.project3]))
            );
            this.task = this.chance.issueType();

            this.viewModel = new FieldsViewModel({
                issueType: this.bug,
                project: this.project1,
                sharedWithIssueTypes: new IssueTypes([this.task])
            });

            this.screenEditorAppStub = this.sandbox.stub(JIRA.ScreenEditor, "Application");

            this.view = new FieldsView({
                model: this.viewModel
            });
        },
        teardown: function () {
            this.sandbox.restore();
        }
    });

    test("rendering the view produces non null html", 1, function () {
        ok(this.view.render().$el.html() !== "");
    });

    test("Screen designer is instantiated when shown in a region", function () {
        jQuery("#qunit-fixture").append("<div id='region-div'></div>");
        var region = new Marionette.Region({
            el: "#region-div"
        });
        region.show(this.view);

        equal(this.screenEditorAppStub.callCount, 1, "Screen editor app should have been created");
    });
});

