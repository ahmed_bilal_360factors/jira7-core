define('jira-project-config/issuetypes/entities/models/project', ['require'], function (require) {
    "use strict";

    var Backbone = require('jira-project-config/backbone');

    /**
     * @class
     * @classdesc A basic description of a project.
     * @extends Backbone.Model
     */
    return Backbone.Model.extend(
        /** @lends JIRA.ProjectConfig.IssueTypes.Entities.Project# */
        {
            defaults: {
                /**
                 * The project's ID, e.g. `54354`. Should always be provided.
                 *
                 * @memberof JIRA.ProjectConfig.IssueTypes.Entities.Project#
                 * @type {number|null}
                 */
                id: undefined,

                /**
                 * The key of the project, e.g. `"AR"`. Should always be provided.
                 *
                 * @memberof JIRA.ProjectConfig.IssueTypes.Entities.Project#
                 * @type {string|null}
                 */
                key: undefined,

                /**
                 * The human readable name of the project, e.g. `"Arnold"`. Should always be provided.
                 *
                 * @memberof JIRA.ProjectConfig.IssueTypes.Entities.Project#
                 * @type {string|null}
                 */
                name: undefined
            }
        });
});
