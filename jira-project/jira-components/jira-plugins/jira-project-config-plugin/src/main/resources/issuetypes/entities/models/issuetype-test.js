AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-entities", "com.atlassian.jira.jira-project-config-plugin:issuetypes-test-utils"], function(){
    "use strict";

    var _ = require("underscore");
    var IssueType = require("jira-project-config/issuetypes/entities/models/issue-type");
    var Chance = require("jira-project-config/libs/chance");
    var ChanceMixins = require("jira-project-config/issuetypes/entities/test/chance-mixins");

    module("JIRA.ProjectConfig.IssueTypes.Entities.IssueType", {
        setup: function () {
            this.chance = new Chance(1);
            _.extend(this.chance, new ChanceMixins());
        }
    });

    test("Defaults", function () {
        var issueType = new IssueType();

        equal(issueType.get("id"), undefined);
        equal(issueType.get("fields"), undefined);
        equal(issueType.get("name"), undefined);
        equal(issueType.get("workflow"), undefined);
    });

    test("toJSON() serialises empty child models", function () {
        var issueType = new IssueType();

        deepEqual(issueType.toJSON(), {
            id: undefined,
            fields: undefined,
            name: undefined,
            workflow: undefined
        });
    });

    test("toJSON() serialises populated child models", function () {
        var fields = this.chance.fields();
        var workflow = this.chance.workflow();

        var issueType = new IssueType({
            fields: fields,
            workflow: workflow
        });

        deepEqual(issueType.toJSON(), {
            id: undefined,
            fields: fields.toJSON(),
            name: undefined,
            workflow: workflow.toJSON()
        });
    });
});

