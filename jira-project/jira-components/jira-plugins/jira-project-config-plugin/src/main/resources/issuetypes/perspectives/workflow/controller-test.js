AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-tab", "com.atlassian.jira.jira-project-config-plugin:issuetypes-test-utils"], function(){
    "use strict";

    var _ = require("underscore");
    var Backbone = require("jira-project-config/backbone");
    var Chance = require("jira-project-config/libs/chance");
    var ChanceMixins = require("jira-project-config/issuetypes/entities/test/chance-mixins");
    var Deferred = require("jira/jquery/deferred");
    var IssueTypes = require("jira-project-config/issuetypes/entities/models/issue-types");
    var jQuery = require("jquery");
    var Marionette = require("jira-project-config/marionette");
    var PluggableActionsView = require("jira-project-config/issuetypes/perspectives/workflow/pluggable-actions/view");
    var PluggableInfoView = require('jira-project-config/issuetypes/perspectives/workflow/workflowinfo/view');
    var Projects = require("jira-project-config/issuetypes/entities/models/projects");
    var WorkflowView = require("jira-project-config/issuetypes/perspectives/workflow/view");
    var wrmContextPath = require("wrm/context-path");
    var IssueTypesModel = require("jira-project-config/issuetypes/model");
    var Analytics = require("jira-project-config/issuetypes/perspectives/workflow/analytics");

    module("JIRA.ProjectConfig.IssueTypes.Workflow.Controller", {
        setup: function () {
            this.chance = new Chance(1);
            _.extend(this.chance, new ChanceMixins());
            this.$fixture = jQuery("#qunit-fixture");
            var sandbox = this.sandbox = sinon.sandbox.create();
            this.context = AJS.test.mockableModuleContext();

            // Ensure the new context uses the same instance of Backbone
            this.context.mock("jira-project-config/backbone", Backbone);
            this.context.mock("jira-project-config/issuetypes/perspectives/workflow/analytics", Analytics);

            this.bug = this.chance.issueType().set({workflow: this.chance.workflow()});
            this.story = this.chance.issueType().set({workflow: this.chance.workflow()});
            this.issueTypes = new IssueTypes([this.bug, this.story]);
            this.project = this.chance.project();
            this.model = new IssueTypesModel({selectedIssueType: this.bug, editing: true, project: this.project});

            var workflowView = this.workflowView = new WorkflowView();
            this.context.mock("jira-project-config/issuetypes/perspectives/workflow/view", function() {
                return workflowView;
            });

            var workflowDesignerMock = this.workflowDesignerMock = {
                destroy: sandbox.spy(),
                publishDraft: sandbox.stub().returns(new Deferred().resolve().promise()),
                discardDraft: sandbox.stub().returns(new Deferred().resolve().promise())
            };
            this.sandbox.stub(JIRA.WorkflowDesigner, "Application").returns(workflowDesignerMock);

            this.region = {show: sinon.spy()};

            this.resetPluggableRegionsSpy = this.sandbox.spy();

            this.commands = new Marionette.Wreqr.Commands();
            this.commands.setHandler("issueTypes:resetPluggableRegions", this.resetPluggableRegionsSpy);

            this.vent = new Marionette.Wreqr.EventAggregator();
            this.controller = this.buildController();
        },

        teardown: function () {
            this.sandbox.restore();
        },

        buildController: function() {
            var WorkflowController = this.context.require('jira-project-config/issuetypes/perspectives/workflow/controller');
            return new WorkflowController({
                commands: this.commands,
                model: this.model,
                region: this.region,
                vent: this.vent
            });
        },

        setEditWorkflowPermission: function () {
            this.bug.get('workflow').set({state: 'editabledelegated'});
        },

        mockPluggableView: function() {
            var dummyView = new Backbone.View();
            _.extend(dummyView, {
                disable: this.sandbox.spy()
            });
            this.context.mock("jira-project-config/issuetypes/perspectives/workflow/pluggable-actions/view", function() {return dummyView;});
            return dummyView;
        },

        mockPluggableInfoView: function() {
            var dummyView = new Backbone.View();
            _.extend(dummyView, {
                disable: this.sandbox.spy()
            });
            this.context.mock("jira-project-config/issuetypes/perspectives/workflow/workflowinfo/view", function() {return dummyView;});
            return dummyView;
        }
    });

    test("showing shows in the provided region", 3, function () {
        equal(this.region.show.callCount, 0);
        this.controller.show();
        equal(this.region.show.callCount, 1);
        ok(this.region.show.firstCall.args[0] === this.workflowView);
    });

    test("showing shows workflow designer", function () {
        equal(JIRA.WorkflowDesigner.Application.callCount, 0, "Workflow designer should not yet have been instantiated");
        this.controller.show({});
        equal(JIRA.WorkflowDesigner.Application.callCount, 1, "Workflow designer should have been instantiated");
        deepEqual(JIRA.WorkflowDesigner.Application.firstCall.args[0], {
            element: this.workflowView.ui.workflowDesigner,
            immutable: false,
            layoutData: {},
            workflowId: this.bug.get("workflow").get("name"),
            fullScreenButton: false,
            draft: true,
            actions: true
        }, "options should match");
    });

    test("closing content view destroys workflow designer", function () {
        var contentView;

        this.controller.show({});

        contentView = this.region.show.firstCall.args[0];

        equal(this.workflowDesignerMock.destroy.callCount, 0, "workflow designer should not yet have been destroyed");

        contentView.trigger("close");

        equal(this.workflowDesignerMock.destroy.callCount, 1, "workflow designer should have been destroyed");
    });

    test("showing resets pluggable regions", function () {
        var bug = this.chance.issueType();
        var bugWorkflow = this.chance.workflow();
        var options;
        var sharedWithIssueTypes;
        var sharedWithProjects;
        var totalProjectsCount;
        var hiddenProjectsCount;
        var story = this.chance.issueType();
        this.context.mock("jira-project-config/issuetypes/perspectives/workflow/pluggable-actions/view", PluggableActionsView);
        this.context.mock("jira-project-config/issuetypes/perspectives/workflow/workflowinfo/view", PluggableInfoView);
        var controller = this.buildController();

        bugWorkflow.set("sharedWithIssueTypes", new IssueTypes([story]));
        bugWorkflow.set("sharedWithProjects", new Projects([this.project]));
        bugWorkflow.set("totalProjectsCount", Math.round(Math.random() * 100));
        bugWorkflow.set("hiddenProjectsCount", Math.round(Math.random() * 100));
        bug.set("workflow", bugWorkflow);
        this.model.set("selectedIssueType", bug);
        this.spy(AJS, "format");

        equal(this.resetPluggableRegionsSpy.callCount, 0, "issueTypes:resetPluggableRegions should not yet have been executed");

        controller.show({});

        equal(this.resetPluggableRegionsSpy.callCount, 1, "issueTypes:resetPluggableRegions should have been executed");

        options = this.resetPluggableRegionsSpy.firstCall.args[0];

        ok(options.actions instanceof PluggableActionsView,
            "options.actions should have been pluggable actions view");

        ok(options.info instanceof PluggableInfoView,
            "options.actions should have been pluggable info view");

        sinon.assert.calledWith(AJS.format, "admin.issuetypeconfig.workflow.heading", bug.get("name"));
        deepEqual(options.descriptor.toJSON(), {
                title: "admin.issuetypeconfig.workflow.heading",
                subtitle: "admin.issuetypeconfig.workflow.subheading",
                breadcrumbs: [
                    {
                        link: wrmContextPath() + "/plugins/servlet/project-config/" + this.project.get("key") + "/issuetypes",
                        name: "admin.schemes.workflow.issuetypes",
                        selected: false
                    },
                    {
                        name: bug.get("name"),
                        selected: true
                    }
                ],
            },
            "options.descriptor should have had the correct model values");

        sharedWithIssueTypes = options.sharedBy.get("issueTypes");
        sharedWithProjects = options.sharedBy.get("projects");
        totalProjectsCount = options.sharedBy.get("totalProjectsCount");
        hiddenProjectsCount = options.sharedBy.get("hiddenProjectsCount");

        deepEqual(sharedWithIssueTypes.toJSON(), [story.toJSON()]);
        deepEqual(sharedWithProjects.toJSON(), [this.project.toJSON()]);
        equal(totalProjectsCount, bugWorkflow.toJSON().totalProjectsCount);
        equal(hiddenProjectsCount, bugWorkflow.toJSON().hiddenProjectsCount);
    });

    test("workflow:editWorkflow is fired on vent when pluggable actions view fires click:edit", function () {
        var pluggableView = this.mockPluggableView();
        var editWorkflowEventSpy = this.sandbox.spy();
        this.vent.on("workflow:editWorkflow", editWorkflowEventSpy);
        var controller = this.buildController();

        controller.show({});
        equal(editWorkflowEventSpy.callCount, 0, "workflow:editWorkflow should not yet have been fired on the vent");

        pluggableView.trigger("click:edit");
        equal(editWorkflowEventSpy.callCount, 1, "workflow:editWorkflow should have been fired on the vent");
    });

    test("analytics is fired when pluggable actions view fires click:edit", function () {
        this.sandbox.spy(Analytics, "triggerEditedWorkflowClicked");
        var pluggableView = this.mockPluggableView();

        var controller = this.buildController();

        controller.show({});

        sinon.assert.notCalled(Analytics.triggerEditedWorkflowClicked, 'Analytics should not be fired before click:edit event');

        pluggableView.trigger("click:edit");

        sinon.assert.calledWith(Analytics.triggerEditedWorkflowClicked);
    });

    test("analytics for user with edit workflow permission is fired when pluggable actions view fires click:edit with workflowEditPermission parameter set to true", function () {
        this.sandbox.spy(Analytics, "triggerEditedWorkflowClicked");
        var pluggableView = this.mockPluggableView();

        var controller = this.buildController();

        controller.show({});

        sinon.assert.notCalled(Analytics.triggerEditedWorkflowClicked, 'Analytics should not be fired before click:edit event');

        pluggableView.trigger("click:edit", {workflowEditPermission: true});

        sinon.assert.calledWith(Analytics.triggerEditedWorkflowClicked, true);
    });

    test("workflow:discardComplete is fired on vent when pluggable info view fires click:discard and discard request successful", function () {
        var pluggableView = this.mockPluggableInfoView();
        var discardCompleteEventSpy = this.sandbox.spy();
        this.vent.on("workflow:discardComplete", discardCompleteEventSpy);
        var controller = this.buildController();

        controller.show({});

        equal(discardCompleteEventSpy.callCount, 0, "workflow:discardComplete should not yet have been fired on the vent");

        pluggableView.trigger("click:discard");

        equal(discardCompleteEventSpy.callCount, 1, "workflow:discardComplete should have been fired on the vent");
    });

    test("info view disabled when it fires click:discard and discard succeeds", function () {
        var deferred = new Deferred();
        this.workflowDesignerMock.discardDraft.returns(deferred);
        var pluggableView = this.mockPluggableInfoView();
        var controller = this.buildController();

        controller.show({});

        equal(pluggableView.disable.callCount, 0, "info view disable() should not yet have been called");

        pluggableView.trigger("click:discard");

        equal(pluggableView.disable.callCount, 1, "info view disable() should have been called once");
        equal(pluggableView.disable.firstCall.args[0], true, "info view disable() should have been called with true");

        deferred.resolve();

        equal(pluggableView.disable.callCount, 1, "info view disable() should still only have been called once");
    });

    test("info view enabled when it fires click:discard and discard fails", function () {
        var deferred = new Deferred();
        this.workflowDesignerMock.discardDraft.returns(deferred);
        var pluggableView = this.mockPluggableInfoView();
        var controller = this.buildController();

        controller.show({});

        equal(pluggableView.disable.callCount, 0, "info view disable() should not yet have been called");

        pluggableView.trigger("click:discard");

        equal(pluggableView.disable.callCount, 1, "info view disable() should have been called once");
        equal(pluggableView.disable.firstCall.args[0], true, "info view disable() should have been called with true");

        deferred.reject();

        equal(pluggableView.disable.callCount, 2, "info view disable() should have been called once");
        equal(pluggableView.disable.secondCall.args[0], false, "info view disable() should have been called with false");
    });

    test("analytics event fired when info view fires click:discard and discard succeeds", function () {
        this.sandbox.spy(Analytics, "triggerDiscardEditedWorkflow");

        this.workflowDesignerMock.discardDraft.returns(new Deferred().resolve());
        var pluggableView = this.mockPluggableInfoView();
        var controller = this.buildController();

        controller.show({});

        pluggableView.trigger("click:discard");

        sinon.assert.calledWith(Analytics.triggerDiscardEditedWorkflow, false);
    });

    test("analytics event fired when info view fires click:publish and publish fails", function () {
        this.sandbox.spy(Analytics, "triggerDiscardEditedWorkflowFail");
        this.workflowDesignerMock.discardDraft.returns(new Deferred().reject());
        var pluggableView = this.mockPluggableInfoView();
        var controller = this.buildController();

        controller.show({});
        pluggableView.trigger("click:discard");

        sinon.assert.calledWith(Analytics.triggerDiscardEditedWorkflowFail, false);
    });

    test("workflow:publishComplete is fired on vent when pluggable info view fires click:publish and publish request successful", function () {
        var publishCompleteEventSpy = this.sandbox.spy();
        this.vent.on("workflow:publishComplete", publishCompleteEventSpy);
        var pluggableView = this.mockPluggableInfoView();
        var controller = this.buildController();

        controller.show({});

        equal(publishCompleteEventSpy.callCount, 0, "workflow:publishComplete should not yet have been fired on the vent");

        pluggableView.trigger("click:publish");

        equal(publishCompleteEventSpy.callCount, 1, "workflow:publishComplete should have been fired on the vent");
    });

    test("info view disabled when it fires click:publish and publish succeeds", function () {
        var deferred = new Deferred();
        this.workflowDesignerMock.publishDraft.returns(deferred);
        var pluggableView = this.mockPluggableInfoView();
        var controller = this.buildController();

        controller.show({});

        equal(pluggableView.disable.callCount, 0, "actions view disable() should not yet have been called");

        pluggableView.trigger("click:publish");

        equal(pluggableView.disable.callCount, 1, "actions view disable() should have been called once");
        equal(pluggableView.disable.firstCall.args[0], true, "actions view disable() should have been called with true");

        deferred.resolve();

        equal(pluggableView.disable.callCount, 1, "actions view disable() should still only have been called once");
    });

    test("info view enabled when it fires click:publish and publish fails", function () {
        var deferred = new Deferred();
        this.workflowDesignerMock.publishDraft.returns(deferred);
        var pluggableView = this.mockPluggableInfoView();
        var controller = this.buildController();

        controller.show({});

        equal(pluggableView.disable.callCount, 0, "actions view disable() should not yet have been called");

        pluggableView.trigger("click:publish");

        equal(pluggableView.disable.callCount, 1, "actions view disable() should have been called once");
        equal(pluggableView.disable.firstCall.args[0], true, "actions view disable() should have been called with true");

        deferred.reject();

        equal(pluggableView.disable.callCount, 2, "actions view disable() should have been called once");
        equal(pluggableView.disable.secondCall.args[0], false, "actions view disable() should have been called with false");
    });

    test("analytics event fired when info view fires click:publish and publish succeeds", function () {
        this.sandbox.spy(Analytics, "triggerPublishEditedWorkflow");
        this.workflowDesignerMock.publishDraft.returns(new Deferred().resolve());
        var pluggableView = this.mockPluggableInfoView();
        var controller = this.buildController();

        controller.show({});
        pluggableView.trigger("click:publish");

        sinon.assert.calledWith(Analytics.triggerPublishEditedWorkflow, false);
    });

    test("analytics event fired when info view fires click:publish and publish fails", function () {
        this.sandbox.spy(Analytics, "triggerPublishEditedWorkflowFail");
        var pluggableView = this.mockPluggableInfoView();
        this.workflowDesignerMock.publishDraft.returns(new Deferred().reject());
        var controller = this.buildController();

        controller.show({});
        pluggableView.trigger("click:publish");

        sinon.assert.calledWith(Analytics.triggerPublishEditedWorkflowFail, false);
    });

    test("controller stops listening to pluggable actions view and vent when it fires close", function () {
        var pluggableView = this.mockPluggableView();
        var controller = this.buildController();
        var stopListeningSpy = this.sandbox.spy(controller, "stopListening");

        controller.show({});

        equal(stopListeningSpy.callCount, 0, "stopListening() should not yet have been called");

        pluggableView.trigger("close");

        equal(stopListeningSpy.callCount, 2, "stopListening() should have been called twice");
        ok(stopListeningSpy.firstCall.args[0] === pluggableView,
            "first call to stopListening() should have been called with the pluggable actions view");
        ok(stopListeningSpy.secondCall.args[0] === this.vent, "second call to stopListening should be called with the pluggable actions");
        equal(stopListeningSpy.secondCall.args[1], "before:perspectiveRerender",
            "second call to stopListening should be called with the appropriate event");
    });

    test("controller stops listening to pluggable info view and vent when it fires close", function () {
        var pluggableView = this.mockPluggableInfoView();
        var controller = this.buildController();
        var stopListeningSpy = this.sandbox.spy(controller, "stopListening");

        controller.show({});

        equal(stopListeningSpy.callCount, 0, "stopListening() should not yet have been called");

        pluggableView.trigger("close");

        equal(stopListeningSpy.callCount, 2, "stopListening() should have been called twice");
        ok(stopListeningSpy.firstCall.args[0] === pluggableView,
            "first call to stopListening() should have been called with the pluggable info view");
        ok(stopListeningSpy.secondCall.args[0] === this.vent, "second call to stopListening should be called with the pluggable info");
        equal(stopListeningSpy.secondCall.args[1], "before:perspectiveRerender",
            "second call to stopListening should be called with the appropriate event");
    });

    test("pluggable actions view is disabled when vent is fired with before:perspectiveRerender", function () {
        var pluggableView = this.mockPluggableView();
        var controller = this.buildController();

        controller.show({});

        equal(pluggableView.disable.callCount, 0, "stopListening() should not yet have been called");

        this.vent.trigger("before:perspectiveRerender");

        equal(pluggableView.disable.callCount, 1, "stopListening() should have been called once");
    });

    test("pluggable info view is disabled when vent is fired with before:perspectiveRerender", function () {
        var pluggableView = this.mockPluggableInfoView();
        var controller = this.buildController();

        controller.show({});

        equal(pluggableView.disable.callCount, 0, "stopListening() should not yet have been called");

        this.vent.trigger("before:perspectiveRerender");

        equal(pluggableView.disable.callCount, 1, "stopListening() should have been called once");
    });

    test("show triggers an 'administration.workflow.open.view.project' event when not editing.", function () {
        this.sandbox.spy(Analytics, "triggerEditProject");
        this.sandbox.spy(Analytics, "triggerViewProject");

        this.model.set("editing", false);
        this.controller.show();

        sinon.assert.calledWith(Analytics.triggerViewProject, false);
        sinon.assert.notCalled(Analytics.triggerEditProject);
    });

    test("show triggers an 'administration.workflow.open.edit.project' event when editing.", function () {
        this.sandbox.spy(Analytics, "triggerEditProject");
        this.sandbox.spy(Analytics, "triggerViewProject");

        this.model.set("editing", true);
        this.controller.show();

        sinon.assert.calledWith(Analytics.triggerEditProject, false);
        sinon.assert.notCalled(Analytics.triggerViewProject);
    });

    test("getPerspectiveModel() returns correct information", function () {
        var perspectiveModel = this.controller.getPerspectiveModel();

        equal(perspectiveModel.get("id"), "workflow", "id should be 'workflow'");

        equal(perspectiveModel.get("name"), "admin.issuetypeconfig.perspective.workflow.name", "name should be 'Workflow'");
    });

    test("analytics event for user with workflow edit permission fired when info view fires click:discard and discard succeeds", function () {
        this.setEditWorkflowPermission();
        this.sandbox.spy(Analytics, "triggerDiscardEditedWorkflow");

        this.workflowDesignerMock.discardDraft.returns(new Deferred().resolve());
        var pluggableView = this.mockPluggableInfoView();
        var controller = this.buildController();

        controller.show({});

        pluggableView.trigger("click:discard");

        sinon.assert.calledWith(Analytics.triggerDiscardEditedWorkflow, true);
    });

    test("analytics event for user with workflow edit permission  fired when info view fires click:publish and publish succeeds", function () {
        this.setEditWorkflowPermission();
        this.sandbox.spy(Analytics, "triggerPublishEditedWorkflow");
        this.workflowDesignerMock.publishDraft.returns(new Deferred().resolve());
        var pluggableView = this.mockPluggableInfoView();
        var controller = this.buildController();

        controller.show({});
        pluggableView.trigger("click:publish");

        sinon.assert.calledWith(Analytics.triggerPublishEditedWorkflow, true);
    });

    test("show triggers an 'administration.workflow.open.view.project' event for project admin when not editing.", function () {
        this.setEditWorkflowPermission();
        this.sandbox.spy(Analytics, "triggerEditProject");
        this.sandbox.spy(Analytics, "triggerViewProject");

        this.model.set("editing", false);
        this.controller.show();

        sinon.assert.calledWith(Analytics.triggerViewProject, true);
        sinon.assert.notCalled(Analytics.triggerEditProject);
    });

    test("show triggers an 'administration.workflow.open.edit.project' event for project admin when editing.", function () {
        this.setEditWorkflowPermission();
        this.sandbox.spy(Analytics, "triggerEditProject");
        this.sandbox.spy(Analytics, "triggerViewProject");

        this.model.set("editing", true);
        this.controller.show();

        sinon.assert.calledWith(Analytics.triggerEditProject, true);
        sinon.assert.notCalled(Analytics.triggerViewProject);
    });

});
