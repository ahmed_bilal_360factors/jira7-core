define("jira-project-config/issuetypes/perspectives/fields/controller", ["require"], function (require) {
    "use strict";

    var _ = require('underscore');
    var BreadcrumbUtil = require('jira-project-config/issuetypes/entities/models/issuetypes-breadcrumb-util');
    var FieldsView = require('jira-project-config/issuetypes/perspectives/fields/view');
    var FieldsViewModel = require('jira-project-config/issuetypes/perspectives/fields/model');
    var HeaderDescriptor = require('jira-project-config/issuetypes/entities/models/header-descriptor');
    var Marionette = require('jira-project-config/marionette');
    var Perspective = require('jira-project-config/issuetypes/entities/models/perspective');
    var SharedByData = require('jira-project-config/issuetypes/entities/models/shared-by-data');

    return Marionette.Controller.extend(
        /** @lends JIRA.ProjectConfig.IssueTypes.Fields.Controller# */
        {
            /**
             * Initialises the controller.
             *
             * @classdesc The controller responsible for orchestrating the content area of the page when viewing an issue
             *   type's fields for a project.
             * @constructs
             * @extends Marionette.Controller
             * @param {Marionette.Wreqr.Commands} options.commands The channel for giving commands. Must support
             *   - `issueTypes:resetPluggableRegions`
             * @param {JIRA.ProjectConfig.IssueTypes.Model} options.model The state of the page.
             * @param {Marionette.Region} options.region The region to render content into.
             */
            initialize: function (options) {
                this.commands = options.commands;
                this.pageModel = options.model;
                this.perspectiveModel = new Perspective({
                    id: "fields",
                    name: AJS.I18n.getText("admin.issuetypeconfig.perspective.fields.name")
                });
                this.region = options.region;
                this.sharedByData = new SharedByData({
                    issueTypesTitle: AJS.I18n.getText("admin.issuetypeconfigproject.shared.list.fields.issuetype"),
                    projectsTitle: AJS.I18n.getText("admin.issuetypeconfigproject.shared.list.fields.project")
                });
                this.viewModel = new FieldsViewModel();

                // Freshen-up now.
                this.pageModel.get("selectedIssueType") && this._refresh();

                this.listenTo(this.pageModel, "change:editing change:project change:selectedIssueType", this._refresh);
            },

            /**
             * @returns {JIRA.ProjectConfig.IssueTypes.Entities.Perspective} the model that represents this perspective.
             */
            getPerspectiveModel: function () {
                return this.perspectiveModel;
            },

            /**
             * Sets the region.
             *
             * @param {Marionette.Region} region the region to set
             */
            setRegion: function (region) {
                this.region = region;
            },

            /**
             * Shows the fields view in the region and also makes requests to display the descriptor and to
             * display the shared by data.
             */
            show: function () {
                // Display the pluggable regions
                this.commands.execute("issueTypes:resetPluggableRegions", {
                    descriptor: this._getDescriptor(),
                    sharedBy: this.sharedByData.clone()
                });

                // Display the main content
                this.region.show(new FieldsView({
                    model: this.viewModel
                }));
            },

            /**
             * @returns {JIRA.ProjectConfig.IssueTypes.Entities.HeaderDescriptor} the descriptor that describes this perspective.
             * @private
             */
            _getDescriptor: function () {
                var issueType = this.viewModel.get("issueType");
                return new HeaderDescriptor({
                    subtitle: AJS.I18n.getText("admin.issuetypeconfig.fields.subheading", issueType.get("fields").get("screenName")),
                    title: AJS.I18n.getText("admin.issuetypeconfig.fields.heading", issueType.get("name")),
                    breadcrumbs: BreadcrumbUtil.makeIssueTypesBreadcrumbs(this._getProjectKey(), AJS.I18n.getText("admin.schemes.workflow.issuetypes"), issueType.get("name"))
                });
            },

            /**
             * Get project key.
             *
             * @returns {string|null} project key.
             * @private
             */
            _getProjectKey: function () {
                if (_.isObject(this.pageModel) === false || _.isObject(this.pageModel.get("project")) === false) {
                    return null;
                }
                return this.pageModel.get("project").get("key");
            },

            /**
             * Ask for the latest version of the issue types and project.
             * @private
             */
            _refresh: function () {
                this.viewModel.set({
                    issueType: this.pageModel.get("selectedIssueType"),
                    project: this.pageModel.get("project")
                });
                this._refreshSharedByData();
            },

            /**
             * Refreshes the sharedByData property according to the selected issue type's fields.
             *
             * @private
             */
            _refreshSharedByData: function () {
                var issueType = this.viewModel.get("issueType");
                var fields;
                var sharedWithIssueTypes = null;
                var sharedWithProjects = null;
                var totalProjectsCount = 0;
                var hiddenProjectsCount = 0;
                if (issueType) {
                    fields = issueType.get("fields");
                    if (fields) {
                        sharedWithIssueTypes = fields.get("sharedWithIssueTypes");
                        sharedWithProjects = fields.get("sharedWithProjects");
                        totalProjectsCount = fields.get("totalProjectsCount");
                        hiddenProjectsCount = fields.get("hiddenProjectsCount");
                    }
                }
                this.sharedByData.set({
                    projects: sharedWithProjects,
                    issueTypes: sharedWithIssueTypes,
                    totalProjectsCount: totalProjectsCount,
                    hiddenProjectsCount: hiddenProjectsCount
                });
            }
        });
});
