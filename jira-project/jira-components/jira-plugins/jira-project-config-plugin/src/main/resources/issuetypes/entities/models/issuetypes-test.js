AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-entities"], function(){
    "use strict";

    var IssueTypes = require("jira-project-config/issuetypes/entities/models/issue-types");

    module("JIRA.ProjectConfig.IssueTypes.Entities.IssueTypes");

    test("Defaults", function () {
        var issueTypes = new IssueTypes();

        equal(issueTypes.length, 0);
    });
});
