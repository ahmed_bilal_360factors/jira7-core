AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-tab"], function(){
    "use strict";

    var IssueTypesModel = require("jira-project-config/issuetypes/model");

    module("JIRA.ProjectConfig.IssueTypes.Model");

    test("defaults", function () {
        var model = new IssueTypesModel();

        equal(model.get("editing"), false);
        equal(model.get("project"), null);
        equal(model.get("selectedIssueType"), null);
        equal(model.get("selectedPerspective"), null);
    });
});
