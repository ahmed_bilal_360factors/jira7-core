AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-tab"], function(){
    "use strict";

    var _ = require("underscore");
    var jQuery = require("jquery");
    var Backbone = require("jira-project-config/backbone");
    var Perspective = require("jira-project-config/issuetypes/entities/models/perspective");
    var Perspectives = require("jira-project-config/issuetypes/entities/models/perspectives");
    var HeaderPerspectivesView = require("jira-project-config/issuetypes/header/perspectives/view");

    var PerspectivesPageObject = function ($el) {
        this.$el = $el;
    };
    _.extend(PerspectivesPageObject.prototype, {
        getPerspectives: function () {
            return _.map(this.$el.find(".project-issuetype-perspective"), function (el) {
                return new PerspectivePageObject(jQuery(el));
            });
        },

        getPerspectivesText: function () {
            return _.map(this.getPerspectives(), function (perspective) {
                return perspective.getText();
            });
        },

        getPerspectiveWithId: function (id) {
            return _.find(this.getPerspectives(), function (perspective) {
                return "project-issuetypes-perspective-" + id === perspective.getId();
            });
        }
    });

    var PerspectivePageObject = function ($el) {
        this.$el = $el;
    };
    _.extend(PerspectivePageObject.prototype, {
        click: function () {
            this.$el.click();
        },

        isSelected: function () {
            return this.$el.attr("aria-pressed") === "true";
        },

        getId: function () {
            return this.$el.attr("id");
        },

        getText: function () {
            return this.$el.text();
        }
    });

    module("JIRA.ProjectConfig.IssueTypes.Header.Perspectives.View", {
        setup: function () {
            this.perspective1 = new Perspective({
                id: "perspective1",
                name: "Perspective 1"
            });

            this.perspective2 = new Perspective({
                id: "perspective2",
                name: "Perspective 2"
            });

            this.model = new Backbone.Model({
                perspectives: new Perspectives([this.perspective1, this.perspective2]),
                selectedPerspective: this.perspective1
            });

            this.view = new HeaderPerspectivesView({model: this.model});

            this.perspectivesPageObject = new PerspectivesPageObject(this.view.render().$el);
        }
    });

    test("rendering the view produces non null html", function () {
        ok(this.view.render().$el.html() !== "");
    });

    test("Perspective selected when first rendered", function () {
        ok(this.perspectivesPageObject.getPerspectiveWithId("perspective1").isSelected(), "The currently selected perspectives is the first one");
    });

    test("Perspective selected when model sets selected perspective", function () {
        ok(this.perspectivesPageObject.getPerspectiveWithId("perspective1").isSelected(), "The currently selected perspectives is the first one");
        ok(!this.perspectivesPageObject.getPerspectiveWithId("perspective2").isSelected(), "The currently selected perspectives is not the second one");

        this.model.set("selectedPerspective", this.perspective2);

        ok(!this.perspectivesPageObject.getPerspectiveWithId("perspective1").isSelected(), "The currently selected perspectives is not the first one");
        ok(this.perspectivesPageObject.getPerspectiveWithId("perspective2").isSelected(), "The currently selected perspectives is the second one");
    });

    test("perspective:selected event fired with perspective when perspective clicked", function () {
        var eventSpy = this.sandbox.spy();

        this.view.on("perspective:selected", eventSpy);

        this.perspectivesPageObject.getPerspectiveWithId("perspective2").click();

        equal(eventSpy.callCount, 1, "Event should have been fired.");
        equal(eventSpy.firstCall.args[0], this.perspective2, "The parameter fired with the event should be the newly selected perspective");
    });
});

