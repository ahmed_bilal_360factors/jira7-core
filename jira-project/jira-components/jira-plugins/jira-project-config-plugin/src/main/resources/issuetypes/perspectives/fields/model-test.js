AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-tab"], function(){
    "use strict";

    var FieldsViewModel = require("jira-project-config/issuetypes/perspectives/fields/model");

    module("JIRA.ProjectConfig.IssueTypes.Fields.Model");

    test("defaults", function () {
        var model;

        model = new FieldsViewModel();

        strictEqual(model.get('issueType'), null);
        strictEqual(model.get('project'), null);
    });
});
