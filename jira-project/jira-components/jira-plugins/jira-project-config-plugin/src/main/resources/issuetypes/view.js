define("jira-project-config/issuetypes/view", ["require"], function(require){
    "use strict";

    var TemplateIssueTypeConfig = require('jira-project-config/issuetypes/templates');
    var Marionette = require('jira-project-config/marionette');

    /**
     * @class
     * @classdesc The layout of the issue types content.
     * @extends Marionette.Layout
     */
    return Marionette.Layout.extend(
        /** @lends JIRA.ProjectConfig.IssueTypes.View */
        {
            /**
             * The regions of this layout.
             *  - content: the main content region where perspectives render themselves
             *  - header: the header for the page
             *  - progressIndicator: the region where the progress indicator is rendered. The blanket of the progress indicator
             *    will span over this whole layout.
             */
            regions: {
                content: "#project-issuetypes-perspective-content",
                header: "#project-issuetypes-header",
                progressIndicator: "#project-issuetypes-perspective-progress"
            },

            template: TemplateIssueTypeConfig.content,

            setLoading: function () {
                this.$el.addClass("loading");
            },

            hideLoading: function () {
                this.$el.removeClass("loading");
            },
            id: "project-issuetypes-content-container"
        });
});
