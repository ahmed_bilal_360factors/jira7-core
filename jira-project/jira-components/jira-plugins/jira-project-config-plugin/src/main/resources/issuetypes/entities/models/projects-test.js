AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-entities"], function(){
    "use strict";

    var Projects = require("jira-project-config/issuetypes/entities/models/projects");

    module("JIRA.ProjectConfig.IssueTypes.Entities.Projects");

    test("Defaults", function () {
        var projects = new Projects();

        equal(projects.length, 0);
    });
});
