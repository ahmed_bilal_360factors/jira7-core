AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-entities"], function(){
    "use strict";

    var IssueTypeLink = require("jira-project-config/issuetypes/entities/models/issue-type-link");

    module("JIRA.ProjectConfig.IssueTypes.Entities.IssueTypeLink");

    test("Defaults", function () {
        var link = new IssueTypeLink();

        equal(link.get("id"), undefined);
        equal(link.get("name"), undefined);
        equal(link.get("url"), undefined);
    });
});
