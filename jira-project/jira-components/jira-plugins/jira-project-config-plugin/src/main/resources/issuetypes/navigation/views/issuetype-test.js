AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-navigation"], function(){
    "use strict";

    var jQuery = require("jquery");
    var IssueType = require("jira-project-config/issuetypes/entities/models/issue-type");
    var IssueTypeView = require("jira-project-config/issuetypes/navigation/views/issue-type");

    function Inspector(element) {
        this.element = jQuery(element);
    }

    Inspector.prototype = {
        link: function () {
            return this.element.find("a");
        },

        isSelected: function () {
            return this.element.hasClass("aui-nav-selected");
        }
    };

    module("JIRA.ProjectConfig.IssueTypes.Navigation.IssueTypeView", {
        setup: function () {
            this.model = new IssueType({
                id: 100,
                url: "/foo/bar",
                name: "Foo"
            });
        }
    });

    test("link", 4, function () {
        var inspector;
        var view;

        view = new IssueTypeView({
            model: this.model
        });
        view.render();

        inspector = new Inspector(view.el);
        strictEqual(inspector.link().text(), this.model.get('name'));
        strictEqual(inspector.link().attr('href'), this.model.get('url'));
        strictEqual(inspector.link().attr('title'), this.model.get('name'));
        strictEqual(inspector.link().attr('id'), 'view_project_issuetype_' + this.model.get('id'));
    });

    test("setSelected()", 3, function () {
        var inspector;
        var view;

        view = new IssueTypeView({
            model: this.model
        });
        view.render();

        inspector = new Inspector(view.el);
        strictEqual(inspector.isSelected(), false);

        view.setSelected(true);
        strictEqual(inspector.isSelected(), true);

        view.setSelected(false);
        strictEqual(inspector.isSelected(), false);
    });
});
