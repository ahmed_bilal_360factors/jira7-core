define("jira-project-config/issuetypes/entities/test/chance-mixins", ["require"], function(require){
    "use strict";

    var _ = require('underscore');
    var Fields = require('jira-project-config/issuetypes/entities/models/fields');
    var HeaderDescriptor = require('jira-project-config/issuetypes/entities/models/header-descriptor');
    var IssueType = require('jira-project-config/issuetypes/entities/models/issue-type');
    var Perspective = require('jira-project-config/issuetypes/entities/models/perspective');
    var Project = require('jira-project-config/issuetypes/entities/models/project');
    var SharedByData = require('jira-project-config/issuetypes/entities/models/shared-by-data');
    var Workflow = require('jira-project-config/issuetypes/entities/models/workflow');

    /**
     * A collection of mixins for creating entities.
     *
     * @example
     * var chance = new Chance();
     * _.extend(chance, new JIRA.ProjectConfig.IssueTypes.Entities.Test.ChanceMixins());
     * chance.project() // --> {JIRA.ProjectConfig.IssueTypes.Entities.Project}
     */
    return function () {
        return {
            /**
             * Creates a {@link JIRA.ProjectConfig.IssueTypes.Entities.HeaderDescriptor}.
             */
            headerDescriptor: function () {
                return new HeaderDescriptor({
                    subtitle: this.word(),
                    title: this.word()
                });
            },

            /**
             * Creates a {@link JIRA.ProjectConfig.IssueTypes.Entities.IssueType}.
             *
             * The issue type's `id` is guaranteed to be unique.
             */
            issueType: (function () {
                var idCounter = 1;

                return function mixin() {
                    var id = idCounter++;

                    return new IssueType({
                        id: id,
                        name: this.word()
                    });
                };
            }()),

            /**
             * Creates a {@link JIRA.ProjectConfig.IssueTypes.Entities.Fields}.
             */
            fields: function () {
                return new Fields({
                    screenName: this.word(),
                    viewScreenId: this.natural()
                });
            },

            /**
             * Creates a {@link JIRA.ProjectConfig.IssueTypes.Entities.Perspective}.
             *
             * Perspective `id` and `name` are guaranteed to be unique.
             */
            perspective: (function () {
                var idCounter = 1;

                return function mixin() {
                    var id = idCounter++;

                    return new Perspective({
                        id: id,
                        name: this.word()
                    });
                };
            }()),

            /**
             * Creates a {@link JIRA.ProjectConfig.IssueTypes.Entities.Project}.
             *
             * Project `id` and `key` are guaranteed to be unique.
             */
            project: (function () {
                var idCounter = 1;

                return function mixin() {
                    var id = idCounter++;  // The max length is 80.
                    var name = this.sentence({words: 10}).slice(0, 80);

                    function acronym(sentence) {
                        return _.reduce(sentence.split(/\s+/), function (memo, word) {
                            var firstLetter = word.slice(0, 1);
                            return memo + firstLetter;
                        }, "").toUpperCase();
                    }

                    return new Project({
                        id: id,
                        key: acronym(name) + id,
                        name: name
                    });
                };
            }()),

            /**
             * Creates a {@link JIRA.ProjectConfig.IssueTypes.Entities.SharedByData}.
             */
            sharedByData: function () {
                return new SharedByData({
                    issueTypesTitle: this.word(),
                    projectsTitle: this.word()
                });
            },

            /**
             * Creates a {@link JIRA.ProjectConfig.IssueTypes.Entities.Workflow}.
             */
            workflow: function () {
                var name = this.sentence();

                return new Workflow({
                    name: name,
                    displayName: name,
                    state: this.pick(["nopermission", "readonly", "editable", "migrate", "readonlydelegatedshared", "editabledelegated" , null])
                });
            }
        };
    };
});
