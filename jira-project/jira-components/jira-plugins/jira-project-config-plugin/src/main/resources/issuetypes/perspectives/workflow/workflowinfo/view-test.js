AJS.test.require([
    "com.atlassian.jira.jira-project-config-plugin:issuetypes-tab",
    "com.atlassian.jira.jira-project-config-plugin:issuetypes-test-utils"
], function () {
    "use strict";

    var _ = require("underscore");
    var Chance = require("jira-project-config/libs/chance");
    var ChanceMixins = require("jira-project-config/issuetypes/entities/test/chance-mixins");
    var jQuery = require("jquery");
    var WorkflowModel = require("jira-project-config/issuetypes/perspectives/workflow/model");
    var WorkflowInfoView = require("jira-project-config/issuetypes/perspectives/workflow/workflowinfo/view");

    var WorkflowControls = function (el) {
        var $el = jQuery(el);
        this.publishButton = $el.find("#publish_workflow_edit");
        this.discardButton = $el.find("#discard_workflow_edit");
    };
    _.extend(WorkflowControls.prototype, {
        clickPublish: function () {
            this.publishButton.click();
        },

        clickDiscard: function () {
            this.discardButton.click();
        },

        isPublishButtonEnabled: function () {
            return !this.publishButton.is(":disabled");
        },

        isDiscardButtonEnabled: function () {
            return !this.discardButton.is(":disabled");
        }
    });


    module("JIRA.ProjectConfig.IssueTypes.Workflow.WorkflowInfo.View", {
        createView: function (options, workflowState) {
            _.defaults({}, options, {
                editing: false
            });
            return new WorkflowInfoView({
                model: new WorkflowModel({
                    editing: options.editing,
                    issueType: this.chance.issueType().set("workflow", this.chance.workflow().set("state", workflowState))
                })
            });
        },


        setup: function () {
            this.sandbox = sinon.sandbox.create();

            this.chance = new Chance(1);
            _.extend(this.chance, new ChanceMixins());
        },

        teardown: function () {
            this.sandbox.restore();
        }
    });

    test("clicking publish fires a click:publish event", function () {
        var clickPublishSpy = this.sandbox.spy();
        var workflowControls;
        var view = this.createView({editing: true});
        view.on("click:publish", clickPublishSpy);

        workflowControls = new WorkflowControls(view.render().$el);
        workflowControls.clickPublish();

        equal(clickPublishSpy.callCount, 1, "click:publish should have been called once");
    });

    test("clicking discard fires a click:discard event", function () {
        var clickDiscardSpy = this.sandbox.spy();
        var workflowControls;
        var view = this.createView({editing: true});
        view.on("click:discard", clickDiscardSpy);

        workflowControls = new WorkflowControls(view.render().$el);
        workflowControls.clickDiscard();

        equal(clickDiscardSpy.callCount, 1, "click:discard should have been called once");
    });

    test("disable() disables publish and discard buttons", function () {
        var view = this.createView({editing: true});
        var workflowControls = new WorkflowControls(view.render().$el);

        ok(workflowControls.isPublishButtonEnabled(), "publish button should be enabled");
        ok(workflowControls.isDiscardButtonEnabled(), "discard button should be enabled");
        view.disable(true);
        ok(!workflowControls.isPublishButtonEnabled(), "publish button should be disabled");
        ok(!workflowControls.isDiscardButtonEnabled(), "discard button should be disabled");
    });
});
