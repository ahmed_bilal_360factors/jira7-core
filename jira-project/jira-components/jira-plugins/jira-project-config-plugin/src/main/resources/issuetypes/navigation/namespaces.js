AJS.namespace('JIRA.ProjectConfig.IssueTypes.Navigation.ExpanderView', null, require('jira-project-config/issuetypes/navigation/views/expander'));
AJS.namespace('JIRA.ProjectConfig.IssueTypes.Navigation.IssueTypeView', null, require('jira-project-config/issuetypes/navigation/views/issue-type'));
AJS.namespace('JIRA.ProjectConfig.IssueTypes.Navigation.IssueTypesView', null, require('jira-project-config/issuetypes/navigation/views/issue-types'));
AJS.namespace('JIRA.ProjectConfig.IssueTypes.Navigation.Layout', null, require('jira-project-config/issuetypes/navigation/views/layout'));
AJS.namespace('JIRA.ProjectConfig.IssueTypes.Navigation.SummaryView', null, require('jira-project-config/issuetypes/navigation/views/summary'));
AJS.namespace('JIRA.ProjectConfig.IssueTypes.Navigation.Controller', null, require('jira-project-config/issuetypes/navigation/controller'));
AJS.namespace("JIRA.ProjectConfig.IssueTypes.Navigation.Model", null, require("jira-project-config/issuetypes/navigation/model"));
AJS.namespace("JIRA.Templates.IssueTypeConfig.Navigation", null, require("jira-project-config/issuetypes/navigation/templates"));
