AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-tab", "com.atlassian.jira.jira-project-config-plugin:issuetypes-test-utils"], function(){
    "use strict";

    var AJSHelper = require('jira-project-config/libs/ajshelper');
    var Analytics;

    module("JIRA.ProjectConfig.IssueTypes.Workflow.Analytics", {
        setup: function () {
            this.sandbox = sinon.sandbox.create();
            this.context = AJS.test.mockableModuleContext();

            this.sandbox.stub(AJSHelper, "trigger");
            // Ensure the new context uses the same instance of AJSHelper
            this.context.mock("jira-project-config/libs/ajshelper", AJSHelper);
            Analytics = this.context.require('jira-project-config/issuetypes/perspectives/workflow/analytics');
        },

        teardown: function () {
            this.sandbox.restore();
        }
    });

    test("Correct analytic keys are generated", function () {
        var data = [
            ["triggerViewProject", "administration.workflow.open.view.project", "administration.workflow.editworkflowpermission.open.view.project"],
            ["triggerEditProject", "administration.workflow.open.edit.project", "administration.workflow.editworkflowpermission.open.edit.project"],
            ["triggerDiscardEditedWorkflow", "administration.project.discard.edited.workflow", "administration.project.editworkflowpermission.discard.edited.workflow"],
            ["triggerDiscardEditedWorkflowFail", "administration.project.discard.edited.workflow.failed", "administration.project.editworkflowpermission.discard.edited.workflow.failed"],
            ["triggerPublishEditedWorkflow", "administration.project.publish.edited.workflow", "administration.project.editworkflowpermission.publish.edited.workflow"],
            ["triggerPublishEditedWorkflowFail", "administration.project.publish.edited.workflow.failed", "administration.project.editworkflowpermission.publish.edited.workflow.failed"],
            ["triggerEditedWorkflowClicked", "administration.project.edit.workflow", "administration.project.editworkflowpermission.edit.workflow"]
        ];

        for (var i = 0; i < data.length; i++) {
            var method = data[i][0];
            var keyName = data[i][1];
            var padawanKeyName = data[i][2];

            AJSHelper.trigger.reset();
            Analytics[method](false);
            sinon.assert.calledWith(AJSHelper.trigger, "analytics", {name: keyName});

            AJSHelper.trigger.reset();
            Analytics[method](true);
            sinon.assert.calledWith(AJSHelper.trigger, "analytics", {name: padawanKeyName});
        }
    });
});
