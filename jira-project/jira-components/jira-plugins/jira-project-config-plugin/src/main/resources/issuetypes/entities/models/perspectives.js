define('jira-project-config/issuetypes/entities/models/perspectives', ['require'], function (require) {
    "use strict";

    var Backbone = require('jira-project-config/backbone');
    var Perspective = require("jira-project-config/issuetypes/entities/models/perspective");

    /**
     * @class
     * @classdesc A collection of {@link JIRA.ProjectConfig.IssueTypes.Entities.Perspective}.
     * @extends Backbone.Collection
     */
    return Backbone.Collection.extend(
        /** @lends JIRA.ProjectConfig.IssueTypes.Entities.Perspectives# */
        {
            model: Perspective
        });
});
