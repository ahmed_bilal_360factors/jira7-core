define('jira-project-config/issuetypes/navigation/views/issue-type', ['require'], function(require) {
    "use strict";

    var Marionette = require("jira-project-config/marionette");
    var TemplateNavigation = require('jira-project-config/issuetypes/navigation/templates');
    var ISSUE_TYPE_LINK_SELECTED = "aui-nav-selected";

    /**
     * @class
     * @classdesc A view that renders a single issue type link.
     * @param {JIRA.ProjectConfig.IssueTypes.Entities.IssueType} model The issue type.
     */
    return Marionette.ItemView.extend(
        /** @lends JIRA.ProjectConfig.IssueTypes.Navigation.IssueTypeView# */
        {
            /**
             * Set whether or not this issue type should appear selected.
             * @param {boolean} selected If true, this issue type should be selected.
             */
            setSelected: function (selected) {
                this.$el.toggleClass(ISSUE_TYPE_LINK_SELECTED, selected);
            },

            tagName: "li",

            template: TemplateNavigation.issueType
        });
});
