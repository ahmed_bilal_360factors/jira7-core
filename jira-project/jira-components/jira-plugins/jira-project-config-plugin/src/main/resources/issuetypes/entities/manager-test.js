AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-entities", "com.atlassian.jira.jira-project-config-plugin:issuetypes-test-utils"], function(){
    "use strict";

    var Marionette = require("jira-project-config/marionette");
    var _ = require("underscore");
    var Deferred = require("jira/jquery/deferred");
    var jQuery = require("jquery");
    var ProjectIssueTypeManager = require("jira-project-config/issuetypes/entities/ajax/project-issue-type-manager");
    var IssueTypes = require("jira-project-config/issuetypes/entities/models/issue-types");
    var Manager = require("jira-project-config/issuetypes/entities/manager");
    var Chance = require("jira-project-config/libs/chance");
    var ChanceMixins = require("jira-project-config/issuetypes/entities/test/chance-mixins");

    function rejected() {
        var deferred = Deferred();
        return deferred.reject.apply(deferred, arguments).promise();
    }

    function resolved() {
        var deferred = Deferred();
        return deferred.resolve.apply(deferred, arguments).promise();
    }

    /**
     * Parameterised testing for entity managers that handle AJAX errors.
     *
     * @param {string} label The test label.
     * @param {number} assertions The number of expected assertions.
     * @param {function} callback A test that's passed `expectedError` and `jqXHR`.
     */
    function xhrFailManagerTest(label, assertions, callback) {
        [
            [{type: "error"}, {status: 400, statusText: "error", responseText: ""}],
            [{type: "error", message: "ErRoR."}, {status: 400, statusText: "error", responseText: "ErRoR."}],
            [{type: "error", message: "BaNg!"}, {
                status: 400,
                statusText: "error",
                responseText: '{"errorMessages": ["BaNg!"]}'
            }],
            [{type: "unauthenticated"}, {status: 401, statusText: "error", responseText: "websudo"}],
            [{type: "abort"}, {status: 0, statusText: "abort", responseText: ""}]
        ]
            .forEach(function (params) {
            var expectedError = params[0];
            var jqXHR = params[1];

            test(label + " -- " + JSON.stringify(jqXHR), assertions, function () {
                callback.call(this, expectedError, jqXHR);
            });
        });
    }

    // -- Here begins the tests! ---------------------------------------------------------------------------------------

    module("JIRA.ProjectConfig.IssueTypes.Entities.Manager", {
        setup: function () {
            this.sandbox = sinon.sandbox.create();
            this.server = sinon.fakeServer.create();
            this.chance = new Chance(1);
            _.extend(this.chance, new ChanceMixins());

            this.$fixture = jQuery("#qunit-fixture");

            this.reqres = new Marionette.Wreqr.RequestResponse();
            this.reqres.setHandlers({
                "urls:issueTypes:base": function (options) {
                    return options.project.get("key") + "/" + options.issueType.get("id");
                }
            });

            this.projectIssueTypeAjaxManager = new ProjectIssueTypeManager({
                reqres: this.reqres
            });

            this.manager = new Manager({
                projectIssueTypeAjaxManager: this.projectIssueTypeAjaxManager,
                reqres: this.reqres
            });

            this.loadWorkflowDraftIfAvailableStub =
                this.sandbox.stub(JIRA.WorkflowDesigner.IO.AJAX.ProjectConfigAJAXManager, "loadDraftWorkflowIfAvailable");
            this.loadOrCreateDraftWorkflowStub =
                this.sandbox.stub(JIRA.WorkflowDesigner.IO.AJAX.ProjectConfigAJAXManager, "loadOrCreateDraftWorkflow");
        },

        teardown: function () {
            this.sandbox.restore();
        },

        /**
         * Updates the DOM with data for use by the test.
         * @param {{id: number, key: string, name: string}} [options.project] The current project, all attributes are
         *     optional.
         * @param {{id: string, name: string}[]} [options.issueTypes] Issue types for the current project.
         */
        updateDom: function (options) {
            if (options.project) {
                this.$fixture.find("meta[name=projectKey]").remove();
                this.$fixture.find("meta[name=projectId]").remove();
                this.$fixture.find("#project-config-header-name").remove();

                jQuery("<meta name=projectKey />")
                    .attr("content", options.project.key)
                    .appendTo(this.$fixture);
                jQuery("<meta name=projectId />")
                    .attr("content", options.project.id)
                    .appendTo(this.$fixture);
                jQuery("<div id=project-config-header-name />")
                    .text(options.project.name)
                    .appendTo(this.$fixture);
            }

            if (options.issueTypes) {
                this.$fixture.find("#project_issuetypes").remove();
                jQuery("<div id=project_issuetypes />")
                    .attr("data-issue-types", JSON.stringify(options.issueTypes))
                    .appendTo(this.$fixture);
            }
        },

        issueTypePageUrl: function (project, issueType) {
            return this.reqres.request("urls:issueTypes:base", {
                project: project,
                issueType: issueType
            });
        }
    });

    test(".project() should pull data from the DOM", 1, function () {
        var expectedProject = this.chance.project();

        this.updateDom({project: expectedProject.toJSON()});

        this.manager.project()
            .done(function (project) {
                deepEqual(project.toJSON(), expectedProject.toJSON());
            });
    });

    test(".issueTypes() should pull data from the DOM", 1, function () {
        var issueType1 = this.chance.issueType().set({fields: undefined, workflow: undefined});
        var issueType2 = this.chance.issueType().set({fields: undefined, workflow: undefined});

        this.updateDom({
            issueTypes: [
                issueType1.toJSON(),
                issueType2.toJSON()
            ]
        });

        this.manager.issueTypes()
            .done(function (issueTypes) {
                deepEqual(issueTypes.toJSON(), [
                    issueType1.toJSON(),
                    issueType2.toJSON()
                ]);
            });
    });

    test(".fields() resolves the returned promise with fields populated via the REST API", 2, function () {
        var project = this.chance.project();
        var issueType = this.chance.issueType();
        var sharingProject = this.chance.project();
        var sharingIssueType = this.chance.issueType();
        var fields = this.chance.fields();
        var fieldsResponse;

        fields.get("sharedWithIssueTypes").add(sharingIssueType);
        fields.get("sharedWithProjects").add(sharingProject);

        // .fields() should use the AJAX manager to fetch fields data.
        fieldsResponse = fields.toJSON();
        fieldsResponse.sharedWithIssueTypes = _.pluck(fieldsResponse.sharedWithIssueTypes, "id");
        this.sandbox.stub(this.projectIssueTypeAjaxManager, "fields")
            .withArgs(project, issueType)
            .returns(resolved(fieldsResponse));

        // .fields() should use the .issueTypes() API to turn 'sharedWithIssueTypes' ID values into model objects.
        this.sandbox.stub(this.manager, "issueTypes")
            .returns(
                resolved(new IssueTypes([
                    issueType,
                    sharingIssueType
                ]))
            );

        this.manager.fields(project, issueType)
            .done(function (fetchedFields) {
                equal(arguments.length, 1);
                deepEqual(fetchedFields.toJSON(), fields.toJSON());
            });
    });

    xhrFailManagerTest(".fields() rejects the returned promise when the REST API fails", 2, function (expectedError, jqXHR) {
        var project = this.chance.project();
        var issueType = this.chance.issueType();

        // .fields() should use the .issueTypes() API to turn 'sharedWithIssueTypes' ID values into model objects.
        this.sandbox.stub(this.manager, "issueTypes")
            .returns(resolved(new IssueTypes()));

        // .fields() should use the AJAX manager to fetch fields data.
        this.sandbox.stub(this.projectIssueTypeAjaxManager, "fields")
            .withArgs(project, issueType)
            .returns(rejected(jqXHR));

        this.manager.fields(project, issueType)
            .fail(function (error) {
                equal(arguments.length, 1);
                deepEqual(error, expectedError);
            });
    });

    test(".workflow() resolves the returned promise with a layout populated via the REST API", function () {
        var project = this.chance.project();
        var issueType = this.chance.issueType();
        var sharingProject = this.chance.project();
        var sharingIssueType = this.chance.issueType();
        var workflow = this.chance.workflow();
        var workflowResponse;

        workflow.get("sharedWithIssueTypes").add(sharingIssueType);
        workflow.get("sharedWithProjects").add(sharingProject);

        // .workflow() should use the AJAX manager to fetch layout data.
        workflowResponse = workflow.toJSON();
        workflowResponse.sharedWithIssueTypes = _.pluck(workflowResponse.sharedWithIssueTypes, "id");
        this.sandbox.stub(this.projectIssueTypeAjaxManager, "workflow")
            .withArgs(project, issueType)
            .returns(resolved(workflowResponse));

        // .fields() should use the .issueTypes() API to turn 'sharedWithIssueTypes' ID values into model objects.
        this.sandbox.stub(this.manager, "issueTypes")
            .returns(
                resolved(new IssueTypes([
                    issueType,
                    sharingIssueType
                ]))
            );

        this.manager.workflow(project, issueType)
            .done(function (fetchedWorkflow) {
                deepEqual(fetchedWorkflow.toJSON(), workflow.toJSON());
            });
    });

    xhrFailManagerTest(".workflow() rejects the returned promise when the REST API fails", 2, function (expectedError, jqXHR) {
        var project = this.chance.project();
        var issueType = this.chance.issueType();

        // .workflow() should use the .issueTypes() API to turn 'sharedWithIssueTypes' ID values into model objects.
        this.sandbox.stub(this.manager, "issueTypes")
            .returns(resolved(new IssueTypes()));

        // .workflow() should use the AJAX manager to fetch layout data.
        this.sandbox.stub(this.projectIssueTypeAjaxManager, "workflow")
            .withArgs(project, issueType)
            .returns(rejected(jqXHR));

        this.manager.workflow(project, issueType)
            .fail(function (error) {
                equal(arguments.length, 1);
                deepEqual(error, expectedError);
            });
    });

    test(".issueType({..., fetchRelated: true}) returns an issue type with the specified ID from the DOM, then populate its fields and workflow via AJAX", 1, function () {
        var project = this.chance.project();
        var issueType = this.chance.issueType();
        var fields = this.chance.fields();
        var workflow = this.chance.workflow();

        // .issueType() should use the .issueTypes() API to expand IDs into models for `sharedWithIssueTypes`.
        this.sandbox.stub(this.manager, "issueTypes")
            .returns(resolved(new IssueTypes([issueType])));

        // .issueType() should use the .project() API to feed into .fields() and .workflow().
        this.sandbox.stub(this.manager, "project")
            .returns(resolved(project));

        // .issueType() should use the .fields() API to populate the issue type.
        this.sandbox.stub(this.manager, "fields")
            .returns(resolved(fields));

        // .issueType() should use the .workflow() API to populate the issue type.
        this.sandbox.stub(this.manager, "workflow")
            .returns(resolved(workflow));

        this.manager.issueType({id: issueType.get("id"), fetchRelated: true})
            .done(function (fetchedIssueType) {
                deepEqual(fetchedIssueType.toJSON(), {
                    id: issueType.get("id"),
                    name: issueType.get("name"),
                    fields: fields.toJSON(),
                    workflow: workflow.toJSON()
                });
            });
    });

    test(".issueType({..., fetchRelated: true}) should handle .fields() failing by passing along the error.", 1, function () {
        var project = this.chance.project();
        var issueType = this.chance.issueType();
        var error = {type: "error"};

        // .issueType() should use the .issueTypes() API to expand IDs into models for `sharedWithIssueTypes`.
        this.sandbox.stub(this.manager, "issueTypes")
            .returns(resolved(new IssueTypes([issueType])));

        // .issueType() should use the .project() API to feed into .fields() and .workflow().
        this.sandbox.stub(this.manager, "project")
            .returns(resolved(project));

        // .issueType() should use the .fields() API to populate the issue type.
        this.sandbox.stub(this.manager, "fields")
            .returns(rejected(error));

        // .issueType() should use the .workflow() API to populate the issue type.
        this.sandbox.stub(this.manager, "workflow")
            .returns(resolved(null));

        this.manager.issueType({id: issueType.get("id"), fetchRelated: true})
            .fail(function (e) {
                strictEqual(e, error);
            });
    });

    test(".issueType({..., fetchRelated: true}) should handle .workflow() failing by passing along the error.", function () {
        var project = this.chance.project();
        var issueType = this.chance.issueType();
        var error = {type: "error"};

        // .issueType() should use the .issueTypes() API to expand IDs into models for `sharedWithIssueTypes`.
        this.sandbox.stub(this.manager, "issueTypes")
            .returns(resolved(new IssueTypes([issueType])));

        // .issueType() should use the .project() API to feed into .fields() and .workflow().
        this.sandbox.stub(this.manager, "project")
            .returns(resolved(project));

        // .issueType() should use the .fields() API to populate the issue type.
        this.sandbox.stub(this.manager, "fields")
            .returns(resolved(null));

        // .issueType() should use the .workflow() API to populate the issue type.
        this.sandbox.stub(this.manager, "workflow")
            .returns(rejected(error));

        this.manager.issueType({id: issueType.get("id"), fetchRelated: true})
            .fail(function (e) {
                strictEqual(e, error);
            });
    });

    test(".issueType() returns an issue type with the specified ID from the DOM, without attempting to populate its workflow or fields", 3, function () {
        var project = this.chance.project();
        var issueType = this.chance.issueType();

        // .issueType() should use the .issueTypes() API to expand IDs into models for `sharedWithIssueTypes`.
        this.sandbox.stub(this.manager, "issueTypes")
            .returns(resolved(new IssueTypes([issueType])));

        // .issueType() should use the .project() API to feed into .fields() and .workflow().
        this.sandbox.stub(this.manager, "project")
            .returns(resolved(project));

        // Make sure these aren't used...
        this.sandbox.stub(this.manager, "fields");
        this.sandbox.stub(this.manager, "workflow");

        this.manager.issueType({id: issueType.get("id")})
            .done(function (fetchedIssueType) {
                deepEqual(fetchedIssueType.toJSON(), {
                    id: issueType.get("id"),
                    name: issueType.get("name"),
                    fields: undefined,
                    workflow: undefined
                });
            });

        ok(!this.manager.fields.called);
        ok(!this.manager.workflow.called);
    });

    test("issue type links", 1, function () {
        var issueType = this.chance.issueType();
        var project = this.chance.project();
        var url = this.issueTypePageUrl(project, issueType);

        // .issueTypeLinks() should use the .issueTypes() API to seed the list of links.
        this.sandbox.stub(this.manager, "issueTypes")
            .returns(resolved(new IssueTypes([issueType])));

        // .issueTypeLinks() should use the .project() API to help build URLs.
        this.sandbox.stub(this.manager, "project")
            .returns(resolved(project));

        this.manager.issueTypeLinks()
            .done(function (issueTypeLinks) {
                deepEqual(issueTypeLinks.toJSON(), [
                    {
                        id: issueType.get("id"),
                        name: issueType.get("name"),
                        url: url
                    }
                ]);
            });
    });

    test("set preferred perspective", function () {
        this.manager.setPreferredPerspective("TEST_TAB_1");
        equal(localStorage.getItem("projectConfig:issueTypes:preferredPerspective"), "TEST_TAB_1");
        this.manager.setPreferredPerspective("TEST_TAB_2");
        equal(localStorage.getItem("projectConfig:issueTypes:preferredPerspective"), "TEST_TAB_2");
    });

    test("get preferred perspective", function () {
        localStorage.setItem("projectConfig:issueTypes:preferredPerspective", "TEST_TAB_1");
        equal(this.manager.getPreferredPerspective(), "TEST_TAB_1");
        localStorage.setItem("projectConfig:issueTypes:preferredPerspective", "TEST_TAB_2");
        equal(this.manager.getPreferredPerspective(), "TEST_TAB_2");
    });

    test("get preferred perspective, none set yet", function () {
        localStorage.removeItem("projectConfig:issueTypes:preferredPerspective");
        equal(this.manager.getPreferredPerspective(), "workflow");
    });

    test(".workflowLayoutData(...[, false]) should resolve promise with WorkflowData", 2, function () {
        var layoutData = {data: this.chance.string()};
        var workflow = this.chance.workflow();

        this.loadWorkflowDraftIfAvailableStub
            .withArgs({name: workflow.get("name")})
            .returns(resolved(true, layoutData));

        this.manager.workflowLayoutData(workflow)
            .done(function (workflowLayoutData) {
                equal(arguments.length, 1);
                deepEqual(workflowLayoutData, {
                    isDraft: true,
                    layoutData: layoutData
                });
            });
    });

    test(".workflowLayoutData(...[, false]) rejects the returned promise with a workflow designer provided message on failure", 2, function () {
        var errorMessage = this.chance.string();
        var workflow = this.chance.workflow();

        this.loadWorkflowDraftIfAvailableStub
            .withArgs({name: workflow.get("name")})
            .returns(rejected(errorMessage));

        this.manager.workflowLayoutData(workflow)
            .fail(function (error) {
                equal(arguments.length, 1);
                deepEqual(error, {
                    type: "error",
                    message: errorMessage
                });
            });
    });

    test(".workflowLayoutData(..., true) should resolve promise with WorkflowData", 2, function () {
        var layoutData = {data: this.chance.string()};
        var workflow = this.chance.workflow();

        this.loadOrCreateDraftWorkflowStub
            .withArgs({name: workflow.get("name")})
            .returns(resolved(true, layoutData));

        this.manager.workflowLayoutData(workflow, true)
            .done(function (workflowLayoutData) {
                equal(arguments.length, 1);
                deepEqual(workflowLayoutData, {
                    isDraft: true,
                    layoutData: layoutData
                });
            });
    });

    test(".workflowLayoutData(..., true) should reject promise with Workflow Designer provided message when AJAX request failed", 2, function () {
        var errorMessage = this.chance.string();
        var workflow = this.chance.workflow();

        this.loadOrCreateDraftWorkflowStub
            .withArgs({name: workflow.get("name")})
            .returns(rejected(errorMessage));

        this.manager.workflowLayoutData(workflow, true)
            .fail(function (error) {
                equal(arguments.length, 1);
                deepEqual(error, {
                    type: "error",
                    message: errorMessage
                });
            });
    });
});

