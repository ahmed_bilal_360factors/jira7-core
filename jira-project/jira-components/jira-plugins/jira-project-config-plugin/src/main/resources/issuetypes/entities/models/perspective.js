define('jira-project-config/issuetypes/entities/models/perspective', ['require'], function (require) {
    "use strict";

    var Backbone = require('jira-project-config/backbone');

    /**
     * @class
     * @classdesc A perspective is simply a "part" of an issue type. At the time of writing both "fields" and "workflow"
     * are two perspectives of an issue type.
     * @extends Backbone.Model
     */
    return Backbone.Model.extend({
        /** @lends JIRA.ProjectConfig.IssueTypes.Entities.Perspective# */
        defaults: {
            /**
             * The ID of the perspective. Should always be provided. At the time of writing both "fields" and "workflow"
             * are possible IDs.
             *
             * @memberof JIRA.ProjectConfig.IssueTypes.Entities.Perspective#
             * @type {string}
             */
            id: undefined,

            /**
             * The name of the perspective. Should always be provided.
             *
             * @memberof JIRA.ProjectConfig.IssueTypes.Entities.Perspective#
             * @type {string}
             */
            name: undefined
        }
    });
});
