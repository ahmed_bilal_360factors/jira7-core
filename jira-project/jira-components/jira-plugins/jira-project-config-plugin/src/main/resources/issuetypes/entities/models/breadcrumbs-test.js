AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-entities"], function () {
    "use strict";

    var Breadcrumb = require('jira-project-config/issuetypes/entities/models/breadcrumb');
    var Breadcrumbs = require('jira-project-config/issuetypes/entities/models/breadcrumbs');

    module("jira-project-config/issuetypes/entities/models/breadcrumbs");

    test("Defaults", function () {
        var breadcrumbs = new Breadcrumbs();

        equal(breadcrumbs.length, 0);

        var breadcrumbsSingle = new Breadcrumbs(
            [new Breadcrumb({
                link: 'link',
                name: 'name'
            })]
        );

        equal(breadcrumbsSingle.length, 1);
        ok(breadcrumbsSingle.at(0) instanceof Breadcrumb);
    });

});
