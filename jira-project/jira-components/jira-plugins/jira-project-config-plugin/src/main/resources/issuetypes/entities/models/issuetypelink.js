define('jira-project-config/issuetypes/entities/models/issue-type-link', ['require'], function (require) {
    "use strict";

    var Backbone = require('jira-project-config/backbone');

    /**
     * @class
     * @classdesc An issue type link in a project.
     * @extends Backbone.Model
     */
    return Backbone.Model.extend(
        /** @lends JIRA.ProjectConfig.IssueTypes.Entities.IssueTypeLink# */
        {
            defaults: {
                /**
                 * The ID of the issue type link. This ID is used to resolve the anchor id.
                 * This is the ID of the {@link JIRA.ProjectConfig.IssueTypes.Entities.IssueType} that this
                 * corresponds to. Should always be provided on creation.
                 *
                 * @memberof JIRA.ProjectConfig.IssueTypes.Entities.IssueTypeLink#
                 * @type {string}
                 */
                id: undefined,

                /**
                 * The name of the issue type link. This is what is displayed to the user. Should always be provided on creation.
                 *
                 * @memberof JIRA.ProjectConfig.IssueTypes.Entities.IssueTypeLink#
                 * @type {string}
                 */
                name: undefined,

                /**
                 * The url of the issue type link. This is the url of the anchor. Should always be provided on creation.
                 *
                 * @memberof JIRA.ProjectConfig.IssueTypes.Entities.IssueTypeLink#
                 * @type {string}
                 */
                url: undefined
            }
        });
});
