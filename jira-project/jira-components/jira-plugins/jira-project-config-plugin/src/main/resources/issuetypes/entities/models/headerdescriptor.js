define('jira-project-config/issuetypes/entities/models/header-descriptor', ["require"], function(require) {
    "use strict";

    var Breadcrumbs = require('jira-project-config/issuetypes/entities/models/breadcrumbs');
    var _ = require('underscore');
    var Backbone = require('jira-project-config/backbone');

    /**
     * @class
     * @classdesc The data required to render a {@link JIRA.ProjectConfig.IssueTypes.Header.Descriptor.View}. A descriptor
     * provides a description around what the current issue type and perspective are.
     * @extends Backbone.Model
     */
    return Backbone.Model.extend({
        /** @lends JIRA.ProjectConfig.IssueTypes.Entities.HeaderDescriptor# */
        defaults: function () {
            return {
                /**
                 * The subtitle of the header. Should always be provided.
                 *
                 * @memberof JIRA.ProjectConfig.IssueTypes.Entities.HeaderDescriptor#
                 * @type {string}
                 */
                subtitle: undefined,

                /**
                 * The title of the header. Should always be provided.
                 *
                 * @memberof JIRA.ProjectConfig.IssueTypes.Entities.HeaderDescriptor#
                 * @type {string}
                 */
                title: undefined,

                /**
                 * The breadcrumbs that should be displayed on top of the header.
                 *
                 * @memberof JIRA.ProjectConfig.IssueTypes.Entities.HeaderDescriptor#
                 * @type {module:jira-project-config/issuetypes/entities/models/breadcrumbs}
                 */
                breadcrumbs: new Breadcrumbs()
            };
        },

        toJSON: function () {
            return _.extend({}, this.attributes, {
                breadcrumbs: this.attributes.breadcrumbs.toJSON()
            });
        }
    });
});
