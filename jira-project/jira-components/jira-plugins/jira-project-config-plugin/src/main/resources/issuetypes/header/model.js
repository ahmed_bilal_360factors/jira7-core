define('jira-project-config/issuetypes/header/model', ['require'], function(require) {
 "use strict";

 var Perspectives = require('jira-project-config/issuetypes/entities/models/perspectives');
 var Backbone = require('jira-project-config/backbone');

 /**
  * @class
  * @classdesc A model storing the state of a the header.
  * @extends Backbone.Model
  */
 return Backbone.Model.extend(
     /** @lends JIRA.ProjectConfig.IssueTypes.Header.Model# */
     {
         defaults: function () {
             return {
                 /**
                  * The perspectives to display.
                  *
                  * @memberof JIRA.ProjectConfig.IssueTypes.Header.Model#
                  * @type {JIRA.ProjectConfig.IssueTypes.Entities.Perspectives}
                  */
                 perspectives: new Perspectives(),

                 /**
                  * The currently selected issue type.
                  *
                  * @memberof JIRA.ProjectConfig.IssueTypes.Header.Model#
                  * @type {JIRA.ProjectConfig.IssueTypes.Entities.IssueType|null}
                  */
                 selectedIssueType: null,

                 /**
                  * The currently selected perspective.
                  *
                  * @memberof JIRA.ProjectConfig.IssueTypes.Header.Model#
                  * @type {JIRA.ProjectConfig.IssueTypes.Entities.Perspective|null}
                  */
                 selectedPerspective: null
             };
         }
     });
});
