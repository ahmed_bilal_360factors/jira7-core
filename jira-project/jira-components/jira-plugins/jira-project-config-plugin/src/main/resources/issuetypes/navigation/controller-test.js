AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-navigation"], function(){
    "use strict";

    var Marionette = require("jira-project-config/marionette");
    var Deferred = require("jira/jquery/deferred");
    var jQuery = require("jquery");
    var IssueTypeLink = require("jira-project-config/issuetypes/entities/models/issue-type-link");
    var IssueTypeLinks = require("jira-project-config/issuetypes/entities/models/issue-type-links");
    var Project = require("jira-project-config/issuetypes/entities/models/project");
    var Layout = require("jira-project-config/issuetypes/navigation/views/layout");
    var Controller = require("jira-project-config/issuetypes/navigation/controller");
    var Model = require("jira-project-config/issuetypes/navigation/model");
    var ExpanderView = require("jira-project-config/issuetypes/navigation/views/expander");
    var IssueTypesView = require("jira-project-config/issuetypes/navigation/views/issue-types");
    var SummaryView = require("jira-project-config/issuetypes/navigation/views/summary");

    module("JIRA.ProjectConfig.IssueTypes.Navigation.Controller", {
        setup: function () {
            this.$fixture = jQuery("#qunit-fixture");
            this.sandbox = sinon.sandbox.create();

            var issueTypeLinks = this.issueTypeLinks = new IssueTypeLinks([
                new IssueTypeLink({
                    id: 1,
                    name: "Bug"
                }),
                new IssueTypeLink({
                    id: 2,
                    name: "Story"
                })
            ]);

            var project = this.project = new Project({
                id: 1000,
                key: "AR",
                name: "Arnold"
            });

            this.region = {show: sinon.stub()};

            this.reqres = new Marionette.Wreqr.RequestResponse();
            this.reqres.setHandler("project", function () {
                return Deferred().resolve(project).promise();
            });
            this.reqres.setHandler("issueTypeLinks", function () {
                return Deferred().resolve(issueTypeLinks).promise();
            });
        },

        teardown: function () {
            this.sandbox.restore();
        }
    });

    test("uses reqres to requests data", 2, function () {
        var controller;
        var reqres;
        var self = this;

        reqres = new Marionette.Wreqr.RequestResponse();
        reqres.setHandler("project", function () {
            ok(true);
            return self.reqres.request("project");
        });
        reqres.setHandler("issueTypeLinks", function () {
            ok(true);
            return self.reqres.request("issueTypeLinks");
        });

        controller = new Controller({
            reqres: reqres,
            region: this.region
        });
        controller.show();
    });

    test("uses the provided region", 3, function () {
        var controller;
        var region = {show: sinon.stub()};

        controller = new Controller({
            reqres: this.reqres,
            region: region
        });

        equal(region.show.callCount, 0);
        controller.show();
        equal(region.show.callCount, 1);
        ok(region.show.firstCall.args[0] instanceof Layout);
    });

    test("views are constructed with appropriate arguments", function () {
        var context = AJS.test.mockableModuleContext();
        var spyExpanderView = this.sandbox.spy(ExpanderView);
        var spyIssuesTypesView = this.sandbox.spy(IssueTypesView);
        var spySummaryView = this.sandbox.spy(SummaryView);
        context.mock('jira-project-config/issuetypes/navigation/views/expander', spyExpanderView);
        context.mock('jira-project-config/issuetypes/navigation/views/issue-types', spyIssuesTypesView);
        context.mock('jira-project-config/issuetypes/navigation/views/summary', spySummaryView);
        context.mock('jira-project-config/issuetypes/navigation/model', Model);
        var Controller = context.require('jira-project-config/issuetypes/navigation/controller');

        var controller = new Controller({
            reqres: this.reqres,
            region: this.region
        });
        controller.show();

        ok(spyExpanderView.firstCall.args[0].model instanceof Model);
        ok(spyIssuesTypesView.firstCall.args[0].model instanceof Model);
        ok(spySummaryView.firstCall.args[0].model instanceof Model);
    });

    test("selectIssueType()", function () {
        var controller;

        controller = new JIRA.ProjectConfig.IssueTypes.Navigation.Controller({
            reqres: this.reqres,
            region: this.region
        });

        controller.selectIssueType(1);
        strictEqual(controller.model.get("selection").get("name"), "Bug");

        // Selecting a non-existent issue type is a no-op.
        controller.selectIssueType(3);
        strictEqual(controller.model.get("selection").get("name"), "Bug");
    });

    test("selectSummary()", function () {
        var controller;

        controller = new Controller({
            reqres: this.reqres,
            region: this.region
        });

        controller.selectSummary();
        strictEqual(controller.model.get("selection"), "summary");
    });

    test("unselect()", function () {
        var controller;

        controller = new Controller({
            reqres: this.reqres,
            region: this.region
        });

        controller.unselect();
        strictEqual(controller.model.get("selection"), null);
    });
});
