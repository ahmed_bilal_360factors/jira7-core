define('jira-project-config/issuetypes/perspectives/workflow/workflowinfo/view', [
    'jira-project-config/marionette',
    'jira-project-config/issuetypes/perspectives/workflow/workflowinfo/templates'
], function(
    Marionette,
    Templates
) {
    "use strict";

    /**
     * @class
     * @classdesc A view that renders the information panel within the workflow context.
     * @extends Marionette.ItemView
     */
    return Marionette.ItemView.extend({
        events: {
            "click #publish_workflow_edit": "_onPublishWorkflow",
            "click #discard_workflow_edit": "_onDiscardWorkflow"
        },

        /**
         * Disables or enables the buttons in the pluggable actions.
         * @param {Boolean} toggle if true, enables buttons, if false, disables buttons
         */
        disable: function (toggle) {
            this.$el.find("button")
                .attr("aria-disabled", toggle)
                .attr("disabled", toggle);
        },

        /**
         * @private
         */
        serializeData: function () {
            return {
                workflowState: this.model.get("issueType").get("workflow").get("state"),
                isEditing: this.model.get("editing")
            };
        },

        /**
         * @private
         */
        template: Templates.render,

        /**
         * @private
         */
        _onPublishWorkflow: function () {
            this.trigger("click:publish");
        },

        /**
         * @private
         */
        _onDiscardWorkflow: function () {
            this.trigger("click:discard");
        }
    });
});
