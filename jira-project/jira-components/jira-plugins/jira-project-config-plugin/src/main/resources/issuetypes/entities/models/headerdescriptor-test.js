AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-entities"], function(){
    "use strict";

    var Breadcrumbs = require('jira-project-config/issuetypes/entities/models/breadcrumbs');
    var HeaderDescriptor = require("jira-project-config/issuetypes/entities/models/header-descriptor");

    module("JIRA.ProjectConfig.IssueTypes.Entities.HeaderDescriptor");

    test("Defaults", function () {
        var headerDescriptor = new HeaderDescriptor();

        equal(headerDescriptor.get("subtitle"), undefined);
        equal(headerDescriptor.get("title"), undefined);
        ok(headerDescriptor.get("breadcrumbs") instanceof Breadcrumbs);
        equal(headerDescriptor.get("breadcrumbs").length, 0);
    });
});
