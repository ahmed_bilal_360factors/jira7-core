AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-tab"], function(){
    "use strict";

    var HeaderView = require("jira-project-config/issuetypes/header/view");

    module("JIRA.ProjectConfig.IssueTypes.Header.View");

    test("all regions are rendered", function () {
        var view = new HeaderView();
        var $viewEl = view.render().$el;

        equal($viewEl.find(view.descriptor.el).length, 1, "descriptor region exists");
        equal($viewEl.find(view.perspectives.el).length, 1, "perspectives region exists");
        equal($viewEl.find(view.actions.el).length, 1, "pluggable actions region exists");
        equal($viewEl.find(view.sharedBy.el).length, 1, "shared by region exists");
        equal($viewEl.find(view.readOnly.el).length, 1, "readOnly region exists");
        equal($viewEl.find(view.workflowInfo.el).length, 1, "workflowInfo region exists");
    });
});
