define("jira-project-config/issuetypes/controller", ["require"], function(require){
    "use strict";

    var $ = require('jquery');
    var Marionette = require('jira-project-config/marionette');
    var _ = require('underscore');
    var navigate = require('jira-project-config/navigate');
    var TemplateIssueTypeConfig = require('jira-project-config/issuetypes/templates');
    var IssueType = require('jira-project-config/issuetypes/entities/models/issue-type');
    var HeaderController = require('jira-project-config/issuetypes/header/controller');
    var FieldsController = require('jira-project-config/issuetypes/perspectives/fields/controller');
    var WorkflowController = require('jira-project-config/issuetypes/perspectives/workflow/controller');
    var IssueTypesView = require('jira-project-config/issuetypes/view');

    return Marionette.Controller.extend(
        /** @lends JIRA.ProjectConfig.IssueTypes.Controller# */
        {
            /**
             * The events and corresponding functions that handle each event on the internal vent.
             */
            internalEvents: {
                "header:issueTypeSelected": "_onIssueTypeSelected",
                "header:perspectiveSelected": "_onPerspectiveSelected",
                "workflow:publishComplete workflow:discardComplete": "_onPublishDiscardComplete",
                "workflow:editWorkflow": "_onEditWorkflow"
            },

            /**
             * The route functions that should be wrapped with deferred wrappers.
             */
            deferredRoutes: [
                "editWorkflow",
                "fields",
                "workflow"
            ],

            /**
             * The route functions that should be wrapped with rendering wrappers.
             */
            renderingRoutes: [
                "editWorkflow",
                "fields",
                "workflow"
            ],

            /**
             * @classdesc The top level controller for the Issue Types section in Project Administration.
             *
             * This controller is responsible for orchestrating the behaviour of the page by deferring to sub controllers.
             * @constructs
             * @extends Marionette.Controller
             * @param {JIRA.ProjectConfig.Application} options.application The shared application.
             * @param {JIRA.ProjectConfig.IssueTypes.Model} options.model A model representing the high level state of the page.
             * @param {Marionette.Region} options.region The region to render within.
             */
            initialize: function (options) {
                this.application = options.application;
                this.model = options.model;
                this.request = 0;

                this.internalVent = new Marionette.Wreqr.EventAggregator();

                this.view = new IssueTypesView({
                    region: options.region
                });

                this.progressIndicatorView = new JIRA.ProjectConfig.ProgressIndicator.View();

                this.listenTo(this.model, "change:selectedIssueType", function (model, value) {
                    var previouslySelectedIssueType = model.previous("selectedIssueType");
                    if (!previouslySelectedIssueType || previouslySelectedIssueType.get("id") !== value.get("id")) {
                        this.application.vent.trigger("issueTypes:issueTypeSelected", value);
                    }
                });

                this.listenTo(this.model, "change:selectedPerspective", function (model, value) {
                    this.application.reqres.request("setPreferredPerspective", value.get("id"));
                });

                this._initializeControllers();
                this._initializeEventHandlers();
                this._initializeDeferredRouteWrappers();
                this._initializeRenderingRouteWrappers();

                this.deferredRoutesExecuting = 0;
            },

            /**
             * Handler for an issue type page.
             */
            issueType: function (projectId, issueTypeId) {
                var self = this;
                var issueType = new IssueType({id: issueTypeId});
                var perspective = this.application.request("getPreferredPerspective");

                this.application.request("project", projectId)
                    .done(function (project) {
                        navigate(self._getUrlForPerspective(perspective, project, issueType), {
                            replace: true
                        });
                    });
            },

            /**
             * Handler for viewing an issue type's workflow.
             */
            workflow: function (projectId, issueTypeId) {
                var app = this.application;
                var self = this;
                var request = ++this.request;
                var fetchingIssueType = app.request("issueType", {id: issueTypeId, fetchRelated: true});

                var fetchingWorkflowLayoutData = fetchingIssueType.pipe(function (issueType) {
                    return app.request("workflowLayoutData", issueType.get("workflow"));
                });

                var isCurrent = function () {
                    return request === self.request;
                };

                this._workflowQuickUiUpdates(projectId, issueTypeId);

                return $
                    .when(fetchingIssueType, fetchingWorkflowLayoutData)
                    .done(function (issueType, workflowLayoutData) {
                        var url;

                        if (!isCurrent()) {
                            return;
                        }

                        self.model.set({
                            editing: workflowLayoutData.isDraft,
                            // Replace the 'simple' issue type with ours from `fetchRelated=true`.
                            selectedIssueType: issueType
                        });

                        // If there's an existing draft, immediately switch into edit mode so the user can jump straight
                        // into work.
                        if (workflowLayoutData.isDraft) {
                            url = app.request("urls:issueTypes:editWorkflow", {
                                project: self.model.get("project"),
                                issueType: issueType
                            });
                            navigate(url, {
                                replace: true,
                                trigger: false
                            });
                        }

                        self.workflowController.show(workflowLayoutData.layoutData);
                    })
                    .fail(function (error) {
                        if (!isCurrent()) {
                            return;
                        }
                        self._handleError(error);
                    });
            },

            /**
             * Handler for editing an issue type's workflow.
             */
            editWorkflow: function (projectId, issueTypeId) {
                var app = this.application;
                var request = ++this.request;
                var self = this;
                var fetchingIssueType = app.request("issueType", {id: issueTypeId, fetchRelated: true});

                var fetchingWorkflowLayoutData = fetchingIssueType.pipe(function (issueType) {
                    return app.request("workflowLayoutData", issueType.get("workflow"), true);
                });

                var isCurrent = function () {
                    return request === self.request;
                };

                this._workflowQuickUiUpdates(projectId, issueTypeId);

                return $
                    .when(fetchingIssueType, fetchingWorkflowLayoutData)
                    .done(function (issueType, workflowLayoutData) {
                        if (!isCurrent()) {
                            return;
                        }
                        self.model.set({
                            editing: true,
                            // Replace the 'simple' issue type with ours from `fetchRelated=true`.
                            selectedIssueType: issueType
                        });

                        self.workflowController.show(workflowLayoutData.layoutData);
                    })
                    .fail(function (error) {
                        if (!isCurrent()) {
                            return;
                        }
                        self._handleError(error);
                    });
            },

            /**
             * Handler for an issue type's fields page.
             */
            fields: function (projectId, issueTypeId) {
                var app = this.application;
                var fetchingProject = app.request("project", projectId);
                var fetchingSimpleIssueType = app.request("issueType", {id: issueTypeId});
                var fetchingFullIssueType = app.request("issueType", {id: issueTypeId, fetchRelated: true});
                var self = this;
                var request = ++this.request;

                var isCurrent = function () {
                    return request === self.request;
                };

                // Grab a simple issue type to quickly update the navigation and page title.
                $
                    .when(fetchingProject, fetchingSimpleIssueType)
                    .done(function (project, simpleIssueType) {
                        app.title(AJS.I18n.getText("admin.issuetypeconfig.fields.title",
                            project.get("name"), simpleIssueType.get("name")));
                        self.model.set({
                            editing: false,
                            project: project,
                            selectedIssueType: simpleIssueType,
                            selectedPerspective: self.fieldsController.getPerspectiveModel()
                        });
                    });

                // Go to a bit more effort to pull down *all* the information about the issue type.
                return fetchingFullIssueType
                    .done(function (issueType) {
                        if (!isCurrent()) {
                            return;
                        }

                        // Replace the 'simple' issue type with ours from `fetchRelated=true`.
                        self.model.set("selectedIssueType", issueType);

                        self.fieldsController.show();
                    })
                    .fail(function (error) {
                        if (!isCurrent()) {
                            return;
                        }
                        self._handleError(error);
                    });
            },

            /**
             * Handler for the summary page in the "Issue Types" group.
             */
            summary: function (projectId) {
                var app = this.application;

                app.vent.trigger("issueTypes:summarySelected");
                app.request("project", projectId)
                    .done(function (project) {
                        app.title(AJS.I18n.getText("admin.project.issuetypes.title", project.get("name")));
                    });
            },

            /**
             * Handler for when a requested page was not found.
             */
            error404: function () {
                this.application.execute("error:404", this.application.content);
            },

            /**
             * If aa progress indicator timeout exists, it clears it and closes the progress indicator region.
             * @private
             */
            _closeProgressIndicatorIfPresent: function () {
                if (this._progressIndicatorTimeoutId) {
                    clearTimeout(this._progressIndicatorTimeoutId);
                    delete this._progressIndicatorTimeoutId;
                    this.view.progressIndicator && this.view.progressIndicator.close();
                }
            },

            /**
             * Generates the URL that corresponds to the passed in perspective, project and issue type.
             *
             * @param {JIRA.ProjectConfig.IssueTypes.Entities.Perspective} perspective
             * @param {JIRA.ProjectConfig.IssueTypes.Entities.Project} project
             * @param {JIRA.ProjectConfig.IssueTypes.Entities.IssueType} issueType
             * @returns {string} the url that corresponds to the passed in perspective, project and issue type
             * @private
             */
            _getUrlForPerspective: function (perspective, project, issueType) {
                var url;
                if (perspective === "workflow") {
                    url = this.application.reqres.request("urls:issueTypes:viewWorkflow", {
                        project: project,
                        issueType: issueType
                    });
                } else {
                    url = this.application.reqres.request("urls:issueTypes:viewFields", {
                        project: project,
                        issueType: issueType
                    });
                }
                return url;
            },

            /**
             * Do something appropriate with an error.
             * @param {JIRA.ProjectConfig.IssueTypes.Entities.ManagerError} error
             * @private
             */
            _handleError: function (error) {
                if (_.contains(["error", "abort"], error.type)) {
                    this.application.commands.execute("error:generic", this.application.content,
                        error.message || AJS.I18n.getText("admin.projectconfig.error.generic.message"));
                } else if (error.type === "unauthenticated") {
                    navigate(TemplateIssueTypeConfig.Urls.login({returnUrl: window.location.href}));
                }
            },

            /**
             * Initializes the header, fields and workflow controllers.
             * @private
             */
            _initializeControllers: function () {
                this.workflowController = new WorkflowController({
                    commands: this.application.commands,
                    model: this.model,
                    region: this.view.content,
                    vent: this.internalVent
                });

                this.fieldsController = new FieldsController({
                    commands: this.application.commands,
                    model: this.model,
                    region: this.view.content
                });

                this.headerController = new HeaderController({
                    commands: this.application.commands,
                    model: this.model,
                    perspectives: [this.workflowController.getPerspectiveModel(), this.fieldsController.getPerspectiveModel()],
                    region: this.view.header,
                    vent: this.internalVent
                });
            },

            /**
             * Displays the issue types view in the application's content region. Also resets the controllers with their respective
             * regions (as the region they reference is now stale). Shows the header.
             *
             * @private
             */
            _displayIssueTypesView: function () {
                this.application.content.show(this.view);

                this.fieldsController.setRegion(this.view.content);
                this.workflowController.setRegion(this.view.content);

                this.headerController.setRegion(this.view.header);
                this.headerController.show();
            },

            /**
             * Wraps all functions declared in the `deferredRoutes` property with behaviour that needs to occur before, or on completion
             * of the methods' returned deferreds.
             * @private
             */
            _initializeDeferredRouteWrappers: function () {
                var instance = this;

                var getWrapper = function (func) {
                    var toExecute = instance[func];
                    return function () {
                        instance.view.setLoading();
                        instance._maybeShowProgressIndicator();
                        instance.deferredRoutesExecuting++;
                        toExecute.apply(instance, arguments).always(function () {
                            instance.deferredRoutesExecuting--;
                            if (instance.deferredRoutesExecuting === 0) {
                                instance.view.hideLoading();
                                instance._closeProgressIndicatorIfPresent();
                            }
                        });
                    };
                };

                this._wrapFunctions(this.deferredRoutes, getWrapper);
            },

            /**
             * Initializes event listening for the events and corresponding methods declared in the `internalEvents` property.
             * @private
             */
            _initializeEventHandlers: function () {
                _.each(this.internalEvents, function (handler, events) {
                    this.listenTo(this.internalVent, events, _.bind(this[handler], this));
                }, this);
            },

            /**
             * Wraps all functions declared in the `renderingRoutes` property with behaviour that needs to occur before (or after)
             * a rendering route method is executed.
             * @private
             */
            _initializeRenderingRouteWrappers: function () {
                var instance = this;

                var getWrapper = function (func) {
                    var toExecute = instance[func];
                    return function () {
                        if (instance.application.content.currentView !== instance.view) {
                            this._displayIssueTypesView();
                        }
                        instance.internalVent.trigger("before:perspectiveRerender");
                        toExecute.apply(instance, arguments);
                    };
                };

                this._wrapFunctions(this.renderingRoutes, getWrapper);
            },

            /**
             * Sets a timeout for showing the progress indicator if a timeout does not already exist.
             * @private
             */
            _maybeShowProgressIndicator: function () {
                var instance = this;

                if (!this._progressIndicatorTimeoutId) {
                    this._progressIndicatorTimeoutId = setTimeout(function () {
                        instance.view.progressIndicator.show(instance.progressIndicatorView);
                    }, 500);
                }
            },

            /**
             * @param {JIRA.ProjectConfig.IssueTypes.Entities.IssueType} issueType the issue type that was selected
             * @private
             */
            _onIssueTypeSelected: function (issueType) {
                var url = this._getUrlForPerspective(this.model.get("selectedPerspective").get("id"), this.model.get("project"), issueType);
                navigate(url);
            },

            /**
             * @param {JIRA.ProjectConfig.IssueTypes.Entities.Perspective} perspective the perspective that was selected
             * @private
             */
            _onPerspectiveSelected: function (perspective) {
                var url = this._getUrlForPerspective(perspective.get("id"), this.model.get("project"), this.model.get("selectedIssueType"));
                navigate(url);
            },

            /**
             * @private
             */
            _onPublishDiscardComplete: function () {
                var url = this._getUrlForPerspective(this.model.get("selectedPerspective").get("id"), this.model.get("project"), this.model.get("selectedIssueType"));
                navigate(url);
            },

            /**
             * @private
             */
            _onEditWorkflow: function () {
                var url = this.application.reqres.request("urls:issueTypes:editWorkflow", {
                    project: this.model.get("project"),
                    issueType: this.model.get("selectedIssueType")
                });
                navigate(url);
            },

            /**
             * The work horse behind `workflow()` and `editWorkflow()`. Updates the UI as much as possible - should be called
             * before firing off AJAX requests for additional data.
             * @private
             * @param {string} projectId The project ID.
             * @param {number} issueTypeId The issue type ID.
             */
            _workflowQuickUiUpdates: function (projectId, issueTypeId) {
                var self = this;
                var fetchingProject = this.application.request("project");
                var fetchingSimpleIssueType = this.application.request("issueType", {id: issueTypeId});

                // Fetch a simple issue type to quickly update the navigation and page title.
                $
                    .when(fetchingProject, fetchingSimpleIssueType)
                    .done(function (project, simpleIssueType) {
                        self.model.set({
                            project: project,
                            selectedIssueType: simpleIssueType,
                            selectedPerspective: self.workflowController.getPerspectiveModel()
                        });

                        // Update the page title.
                        self.application.title(AJS.I18n.getText("admin.issuetypeconfig.workflow.title",
                            project.get("name"), simpleIssueType.get("name")));
                    });
            },

            _wrapFunctions: function (functions, getWrapper) {
                _.each(functions, function (func) {
                    this[func] = getWrapper(func);
                }, this);
            }
        });
});
