define('jira-project-config/issuetypes/navigation/controller', ['require'], function(require) {
    "use strict";

    var Marionette = require("jira-project-config/marionette");
    var NavigationModel = require('jira-project-config/issuetypes/navigation/model');
    var SummaryView = require('jira-project-config/issuetypes/navigation/views/summary');
    var Layout = require('jira-project-config/issuetypes/navigation/views/layout');
    var IssueTypesView = require('jira-project-config/issuetypes/navigation/views/issue-types');
    var ExpanderView = require('jira-project-config/issuetypes/navigation/views/expander');
    var _ = require('underscore');

    return Marionette.Controller.extend(
        /** @lends JIRA.ProjectConfig.IssueTypes.Navigation.Controller# */
        {
            /**
             * @classdesc A controller responsible for 'Issue Types' portion of the side navigation.
             * @constructs
             * @extends Marionette.Controller
             * @param {Marionette.Wreqr.RequestResponse} options.reqres The RequestResponse object to use for asking for
             *   things. It must handle `project` and `issueTypes` requests.
             * @param {Marionette.Region} options.region The region to render the navigation into
             */
            initialize: function (options) {
                var self = this;

                this.reqres = options.reqres;
                this.region = options.region;

                this.model = new NavigationModel();
                this.layout = new Layout();

                this.reqres.request("project").done(function (project) {
                    self.model.set("projectKey", project.get("key"));
                });

                self.reqres.request("issueTypeLinks").done(function (issueTypeLinks) {
                    self.model.get("issueTypeLinks").reset(issueTypeLinks.models);
                });
            },

            /**
             * Set an issue type as "selected".
             *
             * @param {string} id the ID of the issue type
             */
            selectIssueType: function (id) {
                var issueTypeLink = this.model.get("issueTypeLinks").get(id);
                if (issueTypeLink) {
                    this.model.set("selection", issueTypeLink);
                }
            },

            /**
             * Set the Issue Types summary link as selected.
             */
            selectSummary: function () {
                this.model.set("selection", "summary");
            },

            /**
             * Unselect any selected item in the navigation.
             */
            unselect: function () {
                this.model.set("selection", null);
            },

            /**
             * The controller should become active and start displaying things.
             */
            show: function () {
                var expanderView;
                var issueTypesView;
                var summaryView;

                expanderView = new ExpanderView({
                    model: this.model
                });

                this.listenTo(expanderView, "expand", function () {
                    this.model.set("userWantsExpandedMode", true);
                });

                issueTypesView = new IssueTypesView({
                    model: this.model
                });

                summaryView = new SummaryView({
                    model: this.model
                });

                this.layout.once("show", _.bind(function () {
                    this.layout.expander.show(expanderView);
                    this.layout.issueTypes.show(issueTypesView);
                    this.layout.summary.show(summaryView);
                }, this));

                this.region.show(this.layout);
            }
        });
});
