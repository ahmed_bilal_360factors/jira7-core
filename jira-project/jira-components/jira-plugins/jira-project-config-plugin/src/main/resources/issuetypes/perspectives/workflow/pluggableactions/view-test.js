AJS.test.require([
    "com.atlassian.jira.jira-project-config-plugin:issuetypes-tab",
    "com.atlassian.jira.jira-project-config-plugin:issuetypes-test-utils"
], function () {
    "use strict";

    var _ = require("underscore");
    var Chance = require("jira-project-config/libs/chance");
    var ChanceMixins = require("jira-project-config/issuetypes/entities/test/chance-mixins");
    var jQuery = require("jquery");
    var WorkflowModel = require("jira-project-config/issuetypes/perspectives/workflow/model");
    var PluggableActionsView = require("jira-project-config/issuetypes/perspectives/workflow/pluggable-actions/view");

    var WorkflowControls = function (el) {
        var $el = jQuery(el);
        this.editButton = $el.find("#edit_workflow");
        this.editPermissionButton = $el.find("#edit_workflow_permission");
    };
    _.extend(WorkflowControls.prototype, {
        clickEdit: function () {
            this.editButton.click();
        },

        clickEditPermission: function () {
            this.editPermissionButton.click();
        },

        isEditButtonEnabled: function () {
            return !this.editButton.is(":disabled");
        }
    });


    module("JIRA.ProjectConfig.IssueTypes.Workflow.PluggableActions.View", {
        createView: function (options, workflowState) {
            _.defaults({}, options, {
                editing: false
            });
            return new PluggableActionsView({
                model: new WorkflowModel({
                    editing: options.editing,
                    issueType: this.chance.issueType().set("workflow", this.chance.workflow().set("state", workflowState)),
                    project: this.chance.project()
                })
            });
        },


        setup: function () {
            this.sandbox = sinon.sandbox.create();

            this.chance = new Chance(1);
            _.extend(this.chance, new ChanceMixins());
        },

        teardown: function () {
            this.sandbox.restore();
        }
    });

    test("clicking edit workflow fires a click:edit event", function () {
        var clickEditSpy = this.sandbox.spy();
        var workflowControls;
        var view = this.createView({editing: false});
        view.on("click:edit", clickEditSpy);

        workflowControls = new WorkflowControls(view.render().$el);
        workflowControls.clickEdit();

        equal(clickEditSpy.callCount, 1, "click:edit should have been called once");
    });

    test("clicking edit workflow by user with edit workflow permission fires a click:edit event with workflowEditPermission parameter", function () {
        var clickEditSpy = this.sandbox.spy();
        var workflowControls;
        var view = this.createView({editing: false}, "editabledelegated");
        view.on("click:edit", clickEditSpy);

        workflowControls = new WorkflowControls(view.render().$el);
        workflowControls.clickEditPermission();
        equal(clickEditSpy.callCount, 1, "click:edit should have been called once");
        sinon.assert.calledWith(clickEditSpy, {workflowEditPermission: true});
    });

    test("disable() disables the edit button", function () {
        var view = this.createView({editing: false});
        var workflowControls = new WorkflowControls(view.render().$el);

        ok(workflowControls.isEditButtonEnabled(), "edit button should be enabled");
        view.disable(true);
        ok(!workflowControls.isEditButtonEnabled(), "edit button should be disabled");
    });
});
