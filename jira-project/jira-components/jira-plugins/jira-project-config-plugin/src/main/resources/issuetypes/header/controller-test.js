/*global Chance*/
AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-tab", "com.atlassian.jira.jira-project-config-plugin:issuetypes-test-utils"], function(){
    "use strict";

    var Marionette = require("jira-project-config/marionette");
    var _ = require("underscore");
    var Chance = require("jira-project-config/libs/chance");
    var ChanceMixins = require("jira-project-config/issuetypes/entities/test/chance-mixins");
    var Backbone = require("jira-project-config/backbone");
    var HeaderDescriptorView = require("jira-project-config/issuetypes/header/descriptor/view");
    var HeaderPerspectivesView = require("jira-project-config/issuetypes/header/perspectives/view");
    var HeaderSharedByView = require("jira-project-config/issuetypes/header/shared-by/view");
    var HeaderView = require("jira-project-config/issuetypes/header/view");
    var HeaderReadOnlyView = require('jira-project-config/issuetypes/header/readonly/view');

    module("JIRA.ProjectConfig.IssueTypes.Header.Controller", {
        createMockHeaderView: function() {
            var mockHeaderView = {
                perspectives: {
                    close: this.sandbox.spy(),
                    show: this.sandbox.spy()
                },
                actions: {
                    close: this.sandbox.spy(),
                    show: this.sandbox.spy()
                },
                descriptor: {
                    close: this.sandbox.spy(),
                    show: this.sandbox.spy()
                },
                sharedBy: {
                    close: this.sandbox.spy(),
                    show: this.sandbox.spy()
                },
                readOnly: {
                    close: this.sandbox.spy(),
                    show: this.sandbox.spy()
                },
                workflowInfo: {
                    close: this.sandbox.spy(),
                    show: this.sandbox.spy()
                }
            };
            this.context.mock("jira-project-config/issuetypes/header/view", function() {return mockHeaderView;});
            return mockHeaderView;
        },

        createNewHeaderController: function (options) {
            var Controller = this.context.require("jira-project-config/issuetypes/header/controller");
            return new Controller(_.defaults({}, options, {
                commands: this.commands,
                model: this.model,
                perspectives: this.defaultPerspectives,
                region: this.region,
                reqres: this.reqres,
                vent: this.vent
            }));
        },

        setup: function () {
            this.sandbox = sinon.sandbox.create();

            this.commands = new Marionette.Wreqr.Commands();
            this.context = AJS.test.mockableModuleContext();

            this.reqres = new Marionette.Wreqr.RequestResponse();
            this.vent = new Marionette.Wreqr.EventAggregator();
            this.region = {
                show: this.sandbox.spy()
            };

            this.chance = new Chance(1);
            _.extend(this.chance, new ChanceMixins());

            this.bug = this.chance.issueType();
            this.story = this.chance.issueType();

            this.project = this.chance.project();
            this.project.set({key: "PTE"});

            this.perspective1 = this.chance.perspective();
            this.perspective2 = this.chance.perspective();
            this.perspective3 = this.chance.perspective();

            this.defaultPerspectives = [this.perspective1, this.perspective2, this.perspective3];

            this.model = new Backbone.Model({
                selectedIssueType: this.bug,
                selectedPerspective: this.perspective1,
                project: this.project
            });
        },

        teardown: function () {
            this.sandbox.restore();
        }
    });

    test("Executing issueTypes:resetPluggableRegions closes all regions and re-initializes nothing when no options provided", function () {
        var mockHeaderView = this.createMockHeaderView();
        this.createNewHeaderController().show();
        this.commands.execute("issueTypes:resetPluggableRegions");

        equal(mockHeaderView.actions.close.callCount, 1, "actions close() should have been called once");
        equal(mockHeaderView.descriptor.close.callCount, 1, "descriptor close() should have been called once");
        equal(mockHeaderView.sharedBy.close.callCount, 1, "sharedBy close() should have been called once");
        equal(mockHeaderView.readOnly.close.callCount, 1, "readOnly close() should have been called once");
        equal(mockHeaderView.workflowInfo.close.callCount, 1, "workflowInfo close() should have been called once");

        sinon.assert.notCalled(mockHeaderView.actions.show, "actions show() should not have been called");
        sinon.assert.notCalled(mockHeaderView.descriptor.show, "descriptor show() should not have been called");
        sinon.assert.notCalled(mockHeaderView.sharedBy.show, "sharedBy show() should not have been called");
        sinon.assert.notCalled(mockHeaderView.readOnly.show, "readOnly show() should not have been called");
        sinon.assert.notCalled(mockHeaderView.workflowInfo.show, "workflowInfo show() should not have been called");
    });

    test("Executing issueTypes:resetPluggableRegions shows descriptor view in header when descriptor in options", function () {
        var mockHeaderView = this.createMockHeaderView();
        var descriptorViewSpy = this.sandbox.spy(HeaderDescriptorView);
        this.context.mock('jira-project-config/issuetypes/header/descriptor/view', descriptorViewSpy);
        var headerDescriptorModel = this.chance.headerDescriptor();

        this.createNewHeaderController().show();

        equal(mockHeaderView.descriptor.show.callCount, 0, "show() should not yet have been called");

        this.commands.execute("issueTypes:resetPluggableRegions", {descriptor: headerDescriptorModel});

        equal(mockHeaderView.descriptor.show.callCount, 1, "show() should have been called once");
        ok(mockHeaderView.descriptor.show.firstCall.args[0] instanceof HeaderDescriptorView, "View passed to show() should be the descriptor view");
        deepEqual(descriptorViewSpy.firstCall.args[0], {model: headerDescriptorModel}, "Model passed to View should be that which is passed to issueTypes:resetPluggableRegions");
    });

    test("Executing issueTypes:resetPluggableRegions shows shared by view in header when shared by data in options", function () {
        var mockHeaderView = this.createMockHeaderView();
        var sharedByDataModel = this.chance.sharedByData();
        var sharedByViewSpy = this.sandbox.spy(HeaderSharedByView);
        this.context.mock('jira-project-config/issuetypes/header/shared-by/view', sharedByViewSpy);

        this.createNewHeaderController().show();

        equal(mockHeaderView.sharedBy.show.callCount, 0, "show() should not yet have been called");

        this.commands.execute("issueTypes:resetPluggableRegions", {sharedBy: sharedByDataModel});

        equal(mockHeaderView.sharedBy.show.callCount, 1, "show() should have been called once");
        ok(mockHeaderView.sharedBy.show.firstCall.args[0] instanceof HeaderSharedByView, "View passed to show() should be the shared by view");
        deepEqual(sharedByViewSpy.firstCall.args[0], {model: sharedByDataModel}, "Model passed to View should be that which is passed to issueTypes:resetPluggableRegions");
    });

    test("Executing issueTypes:resetPluggableRegions shows actions view in header when actions in options", function () {
        var mockHeaderView = this.createMockHeaderView();
        var actionsView = new Backbone.View();

        this.createNewHeaderController().show();

        equal(mockHeaderView.actions.show.callCount, 0, "show() should not yet have been called");

        this.commands.execute("issueTypes:resetPluggableRegions", {actions: actionsView});

        equal(mockHeaderView.actions.show.callCount, 1, "show() should have been called once");
        ok(mockHeaderView.actions.show.firstCall.args[0] === actionsView, "View passed to show() should be the view created");
    });

    test("Executing issueTypes:resetPluggableRegions shows info view in header when info in options", function () {
        var mockHeaderView = this.createMockHeaderView();
        var infoView = new Backbone.View();

        this.createNewHeaderController().show();

        equal(mockHeaderView.workflowInfo.show.callCount, 0, "show() should not yet have been called");

        this.commands.execute("issueTypes:resetPluggableRegions", {info: infoView});

        equal(mockHeaderView.workflowInfo.show.callCount, 1, "show() should have been called once");
        ok(mockHeaderView.workflowInfo.show.firstCall.args[0] === infoView, "View passed to show() should be the view created");
    });

    test("Executing issueTypes:resetPluggableRegions shows readonly lozenge in header when workflow in options", function () {
        var mockHeaderView = this.createMockHeaderView();
        var workflow = new Backbone.Model();

        var headerReadOnlyView = this.sandbox.spy(HeaderReadOnlyView);
        this.context.mock('jira-project-config/issuetypes/header/readonly/view', headerReadOnlyView);

        this.createNewHeaderController().show();

        sinon.assert.notCalled(mockHeaderView.readOnly.show, "show() should not yet have been called");

        this.commands.execute("issueTypes:resetPluggableRegions", {workflow: workflow});

        equal(mockHeaderView.readOnly.show.callCount, 1, "show() should have been called once");
        ok(mockHeaderView.readOnly.show.firstCall.args[0] instanceof HeaderReadOnlyView);
    });

    test("show() renders a Header view in the provided region", function () {
        this.context.mock('jira-project-config/issuetypes/header/view', HeaderView);

        this.createNewHeaderController().show();

        equal(this.region.show.callCount, 1, "show() should not yet have been called.");
        ok(this.region.show.firstCall.args[0] instanceof HeaderView);
    });

    test("show() renders Perspectives view in the perspectives region of the Header", function () {
        this.context.mock('jira-project-config/issuetypes/header/perspectives/view', HeaderPerspectivesView);
        var mockHeaderView = this.createMockHeaderView();
        var headerController = this.createNewHeaderController();

        equal(mockHeaderView.perspectives.show.callCount, 0, "show() should not yet have been called.");

        headerController.show();

        equal(mockHeaderView.perspectives.show.callCount, 1, "show() should not yet have been called.");
        ok(mockHeaderView.perspectives.show.firstCall.args[0] instanceof HeaderPerspectivesView, "parameter passed to show() should be a Perspectives View");
    });


    test("When Perspectives view fires perspective:selected, a header:perspectiveSelected event is fired on vent", function () {
        var perspectiveSelectedSpy = this.sandbox.spy();
        this.vent.on("header:perspectiveSelected", perspectiveSelectedSpy);
        var dummyView = new Backbone.View();
        var perspectivesView = this.sandbox.stub().returns(dummyView);
        this.context.mock('jira-project-config/issuetypes/header/perspectives/view', perspectivesView);

        this.createNewHeaderController().show();

        equal(perspectiveSelectedSpy.callCount, 0, "header:perspectiveSelected should not yet have been fired");

        dummyView.trigger("perspective:selected", this.perspective3);

        equal(perspectiveSelectedSpy.callCount, 1, "header:perspectiveSelected should have been fired once");
        equal(perspectiveSelectedSpy.firstCall.args[0], this.perspective3, "header:perspectiveSelected should have been fired with correct parameter");
    });

    test("When Shared by view is displayed, and it fires issueType:selected, a header:issueTypeSelected is fired on vent", function () {
        var issueTypeSelected = this.sandbox.spy();
        this.vent.on("header:issueTypeSelected", issueTypeSelected);
        var dummyView = new Backbone.View();
        var sharedByView = this.sandbox.stub().returns(dummyView);
        this.context.mock('jira-project-config/issuetypes/header/shared-by/view', sharedByView);

        this.createNewHeaderController().show();
        this.commands.execute("issueTypes:resetPluggableRegions", {sharedBy: new Backbone.Model()});

        equal(issueTypeSelected.callCount, 0, "header:issueTypeSelected should not yet have been fired");

        dummyView.trigger("issueType:selected", this.story);

        equal(issueTypeSelected.callCount, 1, "header:issueTypeSelected should have been fired once");
        equal(issueTypeSelected.firstCall.args[0], this.story, "header:issueTypeSelected should have been fired with correct parameter");
    });

    test("When Shared by view is displayed, and it fires close, controller stops listening to it", function () {
        var dummyView = new Backbone.View();
        var sharedByView = this.sandbox.stub().returns(dummyView);
        this.context.mock('jira-project-config/issuetypes/header/shared-by/view', sharedByView);

        var headerController = this.createNewHeaderController();
        headerController.show();

        var stopListeningSpy = this.sandbox.spy(headerController, "stopListening");

        this.commands.execute("issueTypes:resetPluggableRegions", {sharedBy: new Backbone.Model()});

        equal(stopListeningSpy.callCount, 0, "stopListening() should not yet have been called");

        dummyView.trigger("close");

        equal(stopListeningSpy.callCount, 1, "stopListening() should have been called once");
        ok(stopListeningSpy.firstCall.args[0] === dummyView, "stopListening() should have been called with the shared by view");
    });

    test("When Perspectives view is displayed, and it fires close, controller stops listening to it", function () {
        var dummyView = new Backbone.View();
        var perspectives = this.sandbox.stub().returns(dummyView);
        this.context.mock('jira-project-config/issuetypes/header/perspectives/view', perspectives);

        var headerController = this.createNewHeaderController();
        headerController.show();

        var stopListeningSpy = this.sandbox.spy(headerController, "stopListening");

        equal(stopListeningSpy.callCount, 0, "stopListening() should not yet have been called");

        dummyView.trigger("close");

        equal(stopListeningSpy.callCount, 1, "stopListening() should have been called once");
        ok(stopListeningSpy.firstCall.args[0] === dummyView, "stopListening() should have been called with the perspectives view");
    });
});
