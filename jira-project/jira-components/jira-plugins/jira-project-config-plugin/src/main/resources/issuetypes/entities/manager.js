define("jira-project-config/issuetypes/entities/manager", ["require"], function (require) {
    "use strict";

    var Deferred = require('jira/jquery/deferred');
    var jQuery = require('jquery');
    var _ = require('underscore');
    var Project = require('jira-project-config/issuetypes/entities/models/project');
    var Projects = require('jira-project-config/issuetypes/entities/models/projects');
    var IssueTypes = require('jira-project-config/issuetypes/entities/models/issue-types');
    var IssueType = require('jira-project-config/issuetypes/entities/models/issue-type');
    var IssueTypeLink = require('jira-project-config/issuetypes/entities/models/issue-type-link');
    var IssueTypeLinks = require('jira-project-config/issuetypes/entities/models/issue-type-links');
    var Fields = require('jira-project-config/issuetypes/entities/models/fields');
    var Workflow = require('jira-project-config/issuetypes/entities/models/workflow');

    var ProjectIssueTypeManager = require('jira-project-config/issuetypes/entities/ajax/project-issue-type-manager');
    var WorkflowDesignerProjectConfigAJAXManager = JIRA.WorkflowDesigner.IO.AJAX.ProjectConfigAJAXManager;

    var PREFERRED_PERSPECTIVE_LOCAL_STORAGE_KEY = "projectConfig:issueTypes:preferredPerspective";
    var supportsLocalStorage = !!window.localStorage;

    function makeIssueType(data) {
        return new IssueType({
            id: data.id,
            name: data.name
        });
    }

    /**
     * Extract project data from the DOM.
     *
     * @returns {JIRA.ProjectConfig.IssueTypes.Entities.Project}
     */
    function getProjectData() {
        var id = jQuery("meta[name=projectId]").attr("content");
        var key = jQuery("meta[name=projectKey]").attr("content");
        var nameElement = jQuery("#project-config-header-name");
        var data = {};

        // Only include values that we found in the DOM to avoid clobbering defaults in Project
        if (id !== undefined) {
            data.id = parseInt(id, 0);
        }

        if (key !== undefined) {
            data.key = key;
        }

        if (nameElement.length) {
            data.name = nameElement.text();
        }

        return new Project(data);
    }

    /**
     * Extract issue types data from the DOM.
     *
     * @returns {JIRA.ProjectConfig.IssueTypes.Entities.IssueTypes}
     */
    function getIssueTypesData() {
        var data = jQuery("#project_issuetypes").data("issueTypes");
        var issueTypes = _.map(data, makeIssueType);

        return new IssueTypes(issueTypes);
    }

    /**
     * Make a Projects (model) out of plain JavaScript objects.
     *
     * @param {object[]} projectsData An array of project data, e.g. [{id: 1, key: "FOO", name: "Fast Or Out"}]
     * @returns {JIRA.ProjectConfig.IssueTypes.Entities.Projects}
     */
    function makeProjects(projectsData) {
        var projects = _.map(projectsData, function makeProject(projectData) {
            var project = _.pick(projectData, "id", "key", "name");
            return new Project(project);
        });

        return new Projects(projects);
    }

    /**
     * Make a IssueTypes (model) out of plain JavaScript objects.
     *
     * @param {object[]} ids An array of issue type IDs, e.g. [1, 3, 2]
     * @param {JIRA.ProjectConfig.IssueTypes.Entities.IssueTypes} lookup A library of issue type data to pull from.
     * @returns {JIRA.ProjectConfig.IssueTypes.Entities.IssueTypes}
     */
    function makeIssueTypes(ids, lookup) {
        return new IssueTypes(
            _.map(ids, _.bind(lookup.get, lookup)));
    }

    /**
     * Describes an error when retrieving an entity.
     *
     * @typedef {object} JIRA.ProjectConfig.IssueTypes.Entities.ManagerError
     * @property {string} type A classification of the error, one of: "error", "abort", "unauthenticated".
     * @property {string} [message] A description of the error, suitable for presenting to the user.
     */

    /**
     * Translates a failed jqXHR response into an an error suitable for returning from a manager.
     * @param jqXHR A jQuery jqXHR representing the response.
     * @returns {JIRA.ProjectConfig.IssueTypes.Entities.ManagerError}
     */
    function makeError(jqXHR) {
        var statusText = jqXHR.statusText;
        var statusCode = jqXHR.status;
        var error = {};

        if (statusText === "abort") {
            error.type = "abort";
        } else if (statusCode === 401) {
            error.type = "unauthenticated";
        } else {
            error.type = "error";
        }

        if (_.contains([400, 403, 404], statusCode) && jqXHR.responseText) {
            try {
                // Handle serialised ErrorCollections.
                error.message = JSON.parse(jqXHR.responseText).errorMessages[0];
            } catch (e) {
                error.message = jqXHR.responseText;
            }
        }

        return error;
    }

    /**
     * @class
     * @classdesc An API for retrieving entities.
     * @constructor
     * @param {Marionette.RequestResponse} options.reqres A reqres that must respond to `urls:issueTypes:*`
     * @param {JIRA.ProjectConfig.IssueTypes.Ajax.ProjectIssueTypeManager} [options.projectIssueTypeAjaxManager] The
     *     AJAX manager for the project issue type REST APIs.
     */
    var Manager = function (options) {
        _.defaults(options, {
            projectIssueTypeAjaxManager: new ProjectIssueTypeManager({
                reqres: options.reqres
            })
        });

        // Caching.
        this._issueTypes = null;
        this._project = null;

        _.extend(this, _.pick(options, "reqres", "projectIssueTypeAjaxManager"));
    };

    /** @lends JIRA.ProjectConfig.IssueTypes.Entities.Manager# */
    Manager.prototype = {
        /**
         * Retrieve the workflow layout data, preferring the draft layout if it exists.
         *
         * This data is suitable for passing into the workflow designer's `layoutData` constructor argument.
         *
         * @param {JIRA.ProjectConfig.IssueTypes.Entities.Workflow} workflow the workflow to retrieve data for.
         * @param {boolean} [requireDraft=false] If true, a draft layout will be created and returned if one does not
         *     already exist.
         * @returns a jQuery promise that gets resolved with an {{isDraft: boolean, layoutData: object}}, or rejected
         *     with a {@link JIRA.ProjectConfig.IssueTypes.ManagerError}
         */
        workflowLayoutData: function (workflow, requireDraft) {
            requireDraft = arguments.length === 1 ? false : requireDraft;
            var api = requireDraft ? "loadOrCreateDraftWorkflow" : "loadDraftWorkflowIfAvailable";

            return WorkflowDesignerProjectConfigAJAXManager[api]({name: workflow.get("name")})
                .pipe(
                    function (isDraft, layoutData) {
                        return jQuery.when({
                            isDraft: isDraft,
                            layoutData: layoutData
                        });
                    },
                    function (errorMessage) {
                        return Deferred().reject({type: "error", message: errorMessage}).promise();
                    }
                );
        },

        /**
         * Return the issue types for the current project.
         *
         * @returns a jQuery promise that gets resolved with a {@link JIRA.ProjectConfig.IssueTypes.Entities.IssueTypes}
         */
        issueTypes: function () {
            if (!this._issueTypes) {
                this._issueTypes = getIssueTypesData();
            }

            return jQuery.when(this._issueTypes);
        },

        /**
         * Return the issue type links for the current project.
         *
         * @returns a jQuery promise that gets resolved with a {@link JIRA.ProjectConfig.IssueTypes.Entities.IssueTypeLinks}
         */
        issueTypeLinks: function () {
            var deferred = Deferred();
            var self = this;

            jQuery.when(this.issueTypes(), this.project()).done(function (issueTypes, project) {
                var issueTypeLinks = issueTypes.map(function (issueType) {
                    var url = self.reqres.request("urls:issueTypes:base", {
                        project: project,
                        issueType: issueType
                    });
                    return new IssueTypeLink(_.extend(issueType.pick("id", "name"), {url: url}));
                });
                deferred.resolve(new IssueTypeLinks(issueTypeLinks));
            });
            return deferred.promise();
        },

        /**
         * Retrieve the fields for an issue type in a project.
         *
         * @param {JIRA.ProjectConfig.IssueTypes.Entities.Project} project
         * @param {JIRA.ProjectConfig.IssueTypes.Entities.IssueType} issueType
         * @returns a jQuery promise that is resolved with a {@link JIRA.ProjectConfig.IssueTypes.Entities.Fields}, or
         *     rejected with a {@link JIRA.ProjectConfig.IssueTypes.ManagerError}.
         */
        fields: function (project, issueType) {
            var wish = Deferred();
            var fetchingFieldsData = this.projectIssueTypeAjaxManager.fields(project, issueType);
            var fetchingIssueTypes = this.issueTypes();

            jQuery.when(fetchingFieldsData, fetchingIssueTypes)
                .done(function (fieldsData, issueTypes) {
                    var fields = new Fields({
                        screenName: fieldsData.screenName,
                        viewScreenId: fieldsData.viewScreenId,
                        totalProjectsCount: fieldsData.totalProjectsCount,
                        hiddenProjectsCount: fieldsData.hiddenProjectsCount,
                        sharedWithProjects: makeProjects(fieldsData.sharedWithProjects),
                        sharedWithIssueTypes: makeIssueTypes(fieldsData.sharedWithIssueTypes, issueTypes)
                    });

                    wish.resolve(fields);
                })
                // Only fetchingFieldsData can fail, so we can safely assume that any problems are due to it, this may
                // change in the future.
                .fail(_.compose(wish.reject, makeError));

            return wish.promise();
        },

        /**
         * Retrieve the workflow for an issue type in a project.
         *
         * @param {JIRA.ProjectConfig.IssueTypes.Entities.Project} project
         * @param {JIRA.ProjectConfig.IssueTypes.Entities.IssueType} issueType
         * @returns a jQuery promise that is resolved with a {@link JIRA.ProjectConfig.IssueTypes.Entities.Workflow},
         *   or rejected with an {@link JIRA.ProjectConfig.IssueTypes.ManagerError}
         */
        workflow: function (project, issueType) {
            var wish = Deferred();
            var fetchingWorkflowData = this.projectIssueTypeAjaxManager.workflow(project, issueType);
            var fetchingIssueTypes = this.issueTypes();

            jQuery.when(fetchingWorkflowData, fetchingIssueTypes)
                .done(function (workflowData, issueTypes) {
                    var workflow = new Workflow({
                        name: workflowData.name,
                        displayName: workflowData.displayName,
                        state: workflowData.state,
                        totalProjectsCount: workflowData.totalProjectsCount,
                        hiddenProjectsCount: workflowData.hiddenProjectsCount,
                        sharedWithProjects: makeProjects(workflowData.sharedWithProjects),
                        sharedWithIssueTypes: makeIssueTypes(workflowData.sharedWithIssueTypes, issueTypes)
                    });

                    wish.resolve(workflow);
                })
                // Only fetchingWorkflowData can fail, so we can safely assume that any problems are due to it, this may
                // change in the future.
                .fail(_.compose(wish.reject, makeError));

            return wish.promise();
        },

        /**
         * Return the current project.
         *
         * @returns a jQuery promise that gets resolved with a {@link JIRA.ProjectConfig.IssueTypes.Entities.Project}
         */
        project: function () {
            if (!this._project) {
                this._project = getProjectData();
            }

            return Deferred().resolve(this._project).promise();
        },

        /**
         * Retrieve the issue type corresponding to the given issue type ID.
         *
         *
         * @param {number} options.id the ID of the issue type we want to retrieve.
         * @param {boolean} [options.fetchRelated=false] If true, the `workflow` and `fields` attributes of the issue
         *     type will be populated, otherwise they will be `null`.
         * @returns a jQuery promise that gets resolved with {@link JIRA.ProjectConfig.IssueTypes.Entities.IssueType}
         *
         * **NOTE:** This method differs from `.issueTypes().at(0)`, because this method populates the `fields` and
         *     `workflow` attributes, where as issue types from `.issueTypes()` do not.
         */
        issueType: function (options) {
            var self = this;
            var wish = Deferred();

            _.defaults(options, {fetchRelated: false});
            _.bindAll(wish, "reject");

            jQuery.when(this.project(), this.issueTypes())
                .pipe(function (project, issueTypes) {
                    var fetchingFields;
                    var fetchingWorkflow;
                    var issueType = issueTypes.get(options.id);

                    if (issueType === undefined) {
                        return Deferred().reject({type: "error"});
                    }

                    // We clone this so that the cached list of issue types are always simple issue types; i.e. so that
                    // retrieving issue types only returns their ids and descriptions.
                    issueType = issueType.clone();

                    if (options.fetchRelated === false) {
                        return wish.resolve(issueType);
                    }

                    fetchingFields = self.fields(project, issueType);
                    fetchingWorkflow = self.workflow(project, issueType);

                    return jQuery.when(fetchingFields, fetchingWorkflow)
                        .done(function (fields, workflow) {
                            issueType.set({
                                fields: fields,
                                workflow: workflow
                            });
                            wish.resolve(issueType);
                        });
                })
                .fail(wish.reject);

            return wish.promise();
        },

        /**
         * Gets the preferred issue type perspective from local storage.
         *
         * @returns {string} the preferred perspective
         */
        getPreferredPerspective: function () {
            return supportsLocalStorage && localStorage.getItem(PREFERRED_PERSPECTIVE_LOCAL_STORAGE_KEY) || "workflow";
        },

        /**
         * Sets the preferred issue type perspective in local storage.
         *
         * @param {string} preferredPerspective the preferred perspective to set into local storage.
         */
        setPreferredPerspective: function (preferredPerspective) {
            supportsLocalStorage && localStorage.setItem(PREFERRED_PERSPECTIVE_LOCAL_STORAGE_KEY, preferredPerspective);
        }
    };

    return Manager;
});
