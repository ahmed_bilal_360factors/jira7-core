AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-entities"], function(){
    "use strict";

    var IssueTypes = require("jira-project-config/issuetypes/entities/models/issue-types");
    var Projects = require("jira-project-config/issuetypes/entities/models/projects");
    var SharedByData = require("jira-project-config/issuetypes/entities/models/shared-by-data");

    module("JIRA.ProjectConfig.IssueTypes.Entities.SharedByData");

    test("Defaults", function () {
        var sharedByData = new SharedByData();

        ok(sharedByData.get("issueTypes") instanceof IssueTypes);

        equal(sharedByData.get("issueTypesTitle"), undefined);

        ok(sharedByData.get("projects") instanceof Projects);

        equal(sharedByData.get("projectsTitle"), undefined);

        equal(typeof(sharedByData.get("totalProjectsCount")), "number");
        equal(sharedByData.get("totalProjectsCount"), 0);

        equal(typeof(sharedByData.get("hiddenProjectsCount")), "number");
        equal(sharedByData.get("hiddenProjectsCount"), 0);
    });
});
