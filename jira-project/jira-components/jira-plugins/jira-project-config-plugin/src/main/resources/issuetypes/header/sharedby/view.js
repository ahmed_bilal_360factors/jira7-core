define('jira-project-config/issuetypes/header/shared-by/view', ['require'], function(require) {
    "use strict";

    var TemplateSharedBy = require('jira-project-config/issuetypes/header/shared-by/view/templates');
    var _ = require('underscore');
    var jQuery = require('jquery');
    var Marionette = require("jira-project-config/marionette");

    /**
     * Simple wrapper around the `AJS.InlineDialog` that we use in production. The `SharedBy.View` has abstracted
     * away the `AJS.InlineDialog` to make testing easier.
     *
     * @private
     * @param {jQuery} trigger  the elements that trigger the inline dialog.
     * @param {String} id the id of the dialog.
     * @param {function} contentCallback function that will be called when content should be displayed. Its only
     *  argument is a jQuery element where content should be placed.
     * @constructor
     */
    var InlinePopup = function (trigger, id, contentCallback) {
        this.inline = AJS.InlineDialog(trigger, id, function (el, trigger, showPopup) {
            contentCallback(el);
            showPopup();
        }, {width: 240});
    };

    _.extend(InlinePopup.prototype, {
        close: function () {
            this.inline.hide();
        }
    });

    return Marionette.ItemView.extend(
        /** @lends JIRA.ProjectConfig.IssueTypes.SharedBy.View# */
        {
            ui: {
                "projectTrigger": "a.shared-by-project",
                "issueTypeTrigger": "a.shared-by-issuetype"
            },

            /**
             * Initialises the view. Fires `issueType:selected` when a shared issue type is selected in the UI.
             *
             * @classdesc A view for showing the `IssueTypes` and `Projects` something is shared with.
             * @constructs
             * @param {JIRA.ProjectConfig.IssueTypes.Entities.SharedByData} options.model
             */
            initialize: function (options) {
                _.bindAll(this, "_issueTypePopup", "_projectPopup");

                this.dialogs = [];

                //This is only exposed for testing. Not expecting people to actually use this.
                this.popupClass = options.popupClass || InlinePopup;
            },
            onRender: function () {
                if (this.ui.projectTrigger.length) {
                    this.dialogs.push(new this.popupClass(this.ui.projectTrigger, "project-shared-info",
                        this._projectPopup));
                }
                if (this.ui.issueTypeTrigger.length) {
                    this.dialogs.push(new this.popupClass(this.ui.issueTypeTrigger, "issuetype-shared-info",
                        this._issueTypePopup));
                }
            },
            onClose: function () {
                _.each(this.dialogs, function (dialog) {
                    dialog.close();
                });
            },

            serializeData: function () {
                return this.model.toJSON();
            },

            template: TemplateSharedBy.sharedBy,

            _projectPopup: function (contents) {
                contents.html(TemplateSharedBy.projectPopup({
                    projects: this._projectsJSON(),
                    totalProjectsCount: this.model.get("totalProjectsCount"),
                    hiddenProjectsCount: this.model.get("hiddenProjectsCount"),
                    title: this.model.get("projectsTitle")
                }));
            },
            _issueTypePopup: function (contents) {
                var self = this;
                var $el = jQuery(TemplateSharedBy.issueTypePopup({
                    issueTypes: this._issueTypesJSON(),
                    title: this.model.get("issueTypesTitle")
                }));

                $el.on("simpleClick", "a", function (e) {
                    e.preventDefault();
                    var issueType = self.model.get("issueTypes").findWhere({
                        id: jQuery(e.target).data("issueTypeId").toString()
                    });
                    self.trigger("issueType:selected", issueType);
                });

                contents.html($el);
            },
            _projectsJSON: function () {
                return this.model.get("projects").toJSON();
            },
            _issueTypesJSON: function () {
                return this.model.get("issueTypes").map(function (item) {
                    return item.pick("id", "name", "url");
                });
            }
        });
});
