define("jira-project-config/issuetypes/perspectives/workflow/view", ["require"], function (require) {
    "use strict";

    var Marionette = require('jira-project-config/marionette');
    var TemplateWorkflow = require('jira-project-config/issuetypes/perspectives/workflow/templates');

    /**
     * Initialises the view.
     *
     * @class
     * @classdesc A view that handles rendering the workflow designer for an issue type.
     * @extends Marionette.ItemView
     */
    return Marionette.ItemView.extend(
        /** @lends JIRA.ProjectConfig.IssueTypes.Workflow.View */
        {
            ui: {
                workflowDesigner: "#workflow-designer"
            },

            template: TemplateWorkflow.render
        });
});
