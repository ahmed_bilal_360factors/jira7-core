AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-navigation"], function(){
    "use strict";

    var jQuery = require("jquery");
    var ExpanderView = require("jira-project-config/issuetypes/navigation/views/expander");
    var Model = require("jira-project-config/issuetypes/navigation/model");

    function Inspector(element) {
        this.element = jQuery(element);
    }

    Inspector.prototype = {
        link: function () {
            return this.element.find("#project-issuetypes-expand");
        }
    };

    module("JIRA.ProjectConfig.IssueTypes.Navigation.ExpanderView");

    test("click triggers 'expand' event", 1, function () {
        var inspector;
        var model;
        var view;

        model = new Model();
        view = new ExpanderView({
            model: model
        });
        view.render();

        view.on('expand', function () {
            ok(true);
        });

        inspector = new Inspector(view.el);
        inspector.link().click();
    });
});
