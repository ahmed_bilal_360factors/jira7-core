define('jira-project-config/issuetypes/entities/models/breadcrumb', ['require'], function(require) {
    "use strict";

    var _ = require('underscore');
    var Backbone = require('jira-project-config/backbone');

    /**
     * @typedef {Object} BreadcrumbOptions
     *
     * @property {String} options.link The breadcrumb link - can be have a null value if the item is currently selected
     * @property {String} options.name The display value of the breadcrumb. Should always be provided.
     * @property {boolean} options.selected Indicate whether the breadcrumb is selected. Defaults to false.
     **/

    /**
     * @class
     * @classdesc A breadcrumb.
     * @extends Backbone.Model
     */
    var Breadcrumb = Backbone.Model.extend({
        /** @lends module:jira-project-config/issuetypes/entities/models/breadcrumb */

        /**
         * Constructs a Breadcrumb item with proper validations exercised
         * @param {ChangeTriggeredFlagOptions} options
         */
        initialize: function (options) {
            // it's fine that these Errors do not have a String-comparison-friendly attribute as they're not meant to
            // be used for UI related validations in any shape or form.
            if (_.isObject(options) === false) {
                throw new Error('options object must be supplied');
            }
            if (_.isString(options.name) === false) {
                throw new Error('options.name must have a String value');
            }
            if (_.isUndefined(options.selected) === false && _.isBoolean(options.selected) === false) {
                throw new Error('if options.selected is passed in, it must be a boolean');
            }
            if (_.isString(options.link) === true && options.selected === true) {
                throw new Error('link must have a String value, unless selected is true');
            }
        },
        defaults: {
            selected: false
        }
    });

    return Breadcrumb;
});
