define("jira-project-config/issuetypes/model", ["require"], function(require){
    "use strict";

    var Backbone = require('jira-project-config/backbone');

    /**
     * @class
     * @classdesc The state of the high level state of the page when in the issue types area of the administration.
     * @extends Backbone.Model
     */
    return Backbone.Model.extend(
        /** @lends JIRA.ProjectConfig.IssueTypes.Navigation.Model# */
        {
            defaults: {
                /**
                 * If true, means that the user has indicated they want to be in edit mode.
                 *
                 * @memberof JIRA.ProjectConfig.IssueTypes.Model#
                 * @type {boolean}
                 */
                editing: false,

                /**
                 * The project curently being viewed, or null.
                 * @memberof JIRA.ProjectConfig.IssueTypes.Model#
                 * @type {JIRA.ProjectConfig.IssueTypes.Entities.Project|null}
                 */
                project: null,

                /**
                 * The issue type currently being viewed, or null.
                 *
                 * @memberof JIRA.ProjectConfig.IssueTypes.Model#
                 * @type {JIRA.ProjectConfig.IssueTypes.Entities.IssueType|null}
                 */
                selectedIssueType: null,

                /**
                 * The name of the selected tab being viewed.
                 *
                 * @memberof JIRA.ProjectConfig.IssueTypes.Model#
                 * @type {string|null}
                 */
                selectedPerspective: null


            }
        });
});
