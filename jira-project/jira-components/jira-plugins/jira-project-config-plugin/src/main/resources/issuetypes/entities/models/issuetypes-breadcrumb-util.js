define('jira-project-config/issuetypes/entities/models/issuetypes-breadcrumb-util', ['require'], function (require) {
    "use strict";

    var wrmContextPath = require('wrm/context-path');
    var Breadcrumbs = require('jira-project-config/issuetypes/entities/models/breadcrumbs');
    var Breadcrumb = require('jira-project-config/issuetypes/entities/models/breadcrumb');

    /**
     * Create a link to the projects issue types page.
     *
     * @param projectKey the project key.
     * @returns {string} the link to the issue types page for the specified project key.
     * @private
     */
    function getIssueTypesLinkForProject(projectKey) {
        return wrmContextPath() + "/plugins/servlet/project-config/" + projectKey + "/issuetypes";
    }

    /**
     * Make a Breadcrumb (model).
     * Expects the same options as ChangeTriggeredFlagOptions in {module:jira-project-config/issuetypes/entities/models/breadcrumb}.
     *
     * @param {ChangeTriggeredFlagOptions} options
     * @returns {module:jira-project-config/issuetypes/entities/models/breadcrumb}
     * @private
     */
    function makeBreadcrumb(options) {
        return new Breadcrumb(options);
    }

    return {
        /**
         * Make a Breadcrumb trail for an issue type.
         * The first nav breadcrumb is the issue types page then the current issue type as selected breadcrumb.
         * Issue Types / Issue Type
         *
         * @param {string} issueTypeName Display name of an issue type.
         * @param {string} projectKey the project key.
         * @returns {module:jira-project-config/issuetypes/entities/models/breadcrumbs}
         */
        makeIssueTypesBreadcrumbs: function (projectKey, issueTypesBreadcrumbDisplayName, issueTypeName) {
            return new Breadcrumbs([
                makeBreadcrumb({
                    link: getIssueTypesLinkForProject(projectKey),
                    name: issueTypesBreadcrumbDisplayName
                }),
                makeBreadcrumb({
                    name: issueTypeName,
                    selected: true
                })
            ]);
        }
    };
});
