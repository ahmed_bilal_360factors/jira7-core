define('jira-project-config/issuetypes/entities/models/fields', ['require'], function(require) {
    "use strict";

    var Backbone = require('jira-project-config/backbone');
    var _ = require('underscore');
    var Projects = require("jira-project-config/issuetypes/entities/models/projects");
    var IssueTypes = require("jira-project-config/issuetypes/entities/models/issue-types");

    /**
     * @class
     * @classdesc References related entities involved in the fields of an issue type.
     * @extends Backbone.Model
     */
    return Backbone.Model.extend(
        /** @lends JIRA.ProjectConfig.IssueTypes.Entities.Fields# */
        {
            defaults: function () {
                return {
                    /**
                     * Total number of sharing projects (including those not visible to the current user).
                     *
                     * @memberof JIRA.ProjectConfig.IssueTypes.Entities.Fields#
                     * @type {number}
                     */
                    totalProjectsCount: 0,

                    /**
                     * The number of sharing projects not visible to the current user.
                     *
                     * @memberof JIRA.ProjectConfig.IssueTypes.Entities.Fields#
                     * @type {number}
                     */
                    hiddenProjectsCount: 0,

                    /**
                     * The name of the view screen used for by the issue type. Should always be provided.
                     *
                     * @memberof JIRA.ProjectConfig.IssueTypes.Entities.Fields#
                     * @type {string}
                     */
                    screenName: undefined,

                    /**
                     * Other projects that share the same set of fields.
                     *
                     * @memberof JIRA.ProjectConfig.IssueTypes.Entities.Fields#
                     * @type {JIRA.ProjectConfig.IssueTypes.Entities.Projects}
                     */
                    sharedWithProjects: new Projects(),

                    /**
                     * Other issue types that share the same set of fields.
                     *
                     * @memberof JIRA.ProjectConfig.IssueTypes.Entities.Fields#
                     * @type {JIRA.ProjectConfig.IssueTypes.Entities.IssueTypes}
                     */
                    sharedWithIssueTypes: new IssueTypes(),

                    /**
                     * The ID of the 'view screen'. Should always be provided.
                     *
                     * @memberof JIRA.ProjectConfig.IssueTypes.Entities.Fields#
                     * @type {number}
                     */
                    viewScreenId: undefined
                };
            },

            toJSON: function () {
                return _.extend({}, this.attributes, {
                    totalProjectsCount: this.attributes.totalProjectsCount,
                    hiddenProjectsCount: this.attributes.hiddenProjectsCount,
                    sharedWithProjects: this.attributes.sharedWithProjects.toJSON(),
                    sharedWithIssueTypes: this.attributes.sharedWithIssueTypes.toJSON()
                });
            }
        });
});
