AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-tab"], function(){
    "use strict";

    var WorkflowModel = require("jira-project-config/issuetypes/perspectives/workflow/model");

    module("JIRA.ProjectConfig.IssueTypes.Workflow.Model");

    test("Defaults", function () {
        var model = new WorkflowModel();

        strictEqual(model.get("editing"), false);
        strictEqual(model.get("issueType"), null);
        strictEqual(model.get("project"), null);
    });
});
