AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-tab", "com.atlassian.jira.jira-project-config-plugin:issuetypes-test-utils"], function(){
    "use strict";

    var Marionette = require("jira-project-config/marionette");
    var _ = require("underscore");
    var Chance = require("jira-project-config/libs/chance");
    var ChanceMixins = require("jira-project-config/issuetypes/entities/test/chance-mixins");
    var jQuery = require("jquery");
    var wrmContextPath = require("wrm/context-path");
    var IssueTypes = require("jira-project-config/issuetypes/entities/models/issue-types");
    var Projects = require("jira-project-config/issuetypes/entities/models/projects");
    var FieldsController = require("jira-project-config/issuetypes/perspectives/fields/controller");
    var FieldsView = require("jira-project-config/issuetypes/perspectives/fields/view");
    var IssueTypesModel = require("jira-project-config/issuetypes/model");

    module("JIRA.ProjectConfig.IssueTypes.Fields.Controller", {
        setup: function () {
            this.$fixture = jQuery("#qunit-fixture");
            this.sandbox = sinon.sandbox.create();

            this.chance = new Chance(1);
            _.extend(this.chance, new ChanceMixins());

            this.project = this.chance.project();

            this.bug = this.chance.issueType().set({fields: this.chance.fields()});
            this.story = this.chance.issueType();

            this.bug.get("fields").set("sharedWithIssueTypes", new IssueTypes([this.story]));
            this.bug.get("fields").set("sharedWithProjects", new Projects([this.project]));
            this.bug.get("fields").set("totalProjectsCount", Math.round(Math.random() * 100));
            this.bug.get("fields").set("hiddenProjectsCount", Math.round(Math.random() * 100));

            this.model = new IssueTypesModel({
                selectedIssueType: this.bug,
                project: this.project
            });

            this.region = {show: sinon.stub()};

            this.resetPluggableRegionsSpy = this.sandbox.spy();
            this.commands = new Marionette.Wreqr.Commands();
            this.commands.setHandler("issueTypes:resetPluggableRegions", this.resetPluggableRegionsSpy);
        },

        teardown: function () {
            this.sandbox.restore();
        }
    });

    test("uses the provided region", 3, function () {
        var controller;
        var region = {show: sinon.stub()};

        controller = new FieldsController({
            commands: this.commands,
            model: this.model,
            region: region
        });

        equal(region.show.callCount, 0);
        controller.show();
        equal(region.show.callCount, 1);
        ok(region.show.firstCall.args[0] instanceof FieldsView);
    });

    test("showing executes issueTypes:resetPluggableRegions with correct information", function () {
        var controller = new FieldsController({
                commands: this.commands,
                model: this.model,
                region: this.region
            });

        var options;

        this.sandbox.spy(AJS, "format");
        controller.show();

        equal(this.resetPluggableRegionsSpy.callCount, 1, "issueTypes:resetPluggableRegions should have been executed once");
        options = this.resetPluggableRegionsSpy.firstCall.args[0];

        equal(AJS.format.callCount, 2, "AJS.format hould have been called twice");

        deepEqual(AJS.format.firstCall.args, ["admin.issuetypeconfig.fields.subheading", this.bug.get("fields").get("screenName")], "AJS.format() should have been called with correct parameters"); //TODO ??
        deepEqual(AJS.format.secondCall.args, ["admin.issuetypeconfig.fields.heading", this.bug.get("name")], "AJS.format() should have been called with correct parameters"); //TODO ??

        deepEqual(options.descriptor.toJSON(), {
            title: "admin.issuetypeconfig.fields.heading",
            subtitle: "admin.issuetypeconfig.fields.subheading",
            breadcrumbs: [
                {
                    link: wrmContextPath() + "/plugins/servlet/project-config/" + this.project.get("key") + "/issuetypes",
                    name: "admin.schemes.workflow.issuetypes",
                    selected: false
                },
                {
                    name: this.bug.get("name"),
                    selected: true
                }
            ],
        });

        deepEqual(options.sharedBy.get("issueTypes").toJSON(), [this.story.toJSON()], "Correct issue type shared");
        deepEqual(options.sharedBy.get("projects").toJSON(), [this.project.toJSON()], "Correct project shared");
        equal(options.sharedBy.get("totalProjectsCount"), this.bug.toJSON().fields.totalProjectsCount, "Correct total projects count");
        equal(options.sharedBy.get("hiddenProjectsCount"), this.bug.toJSON().fields.hiddenProjectsCount, "Correct hidden projects count");
    });

    test("getPerspectiveModel() returns correct information", function () {
        var controller = new FieldsController({
                commands: this.commands,
                model: this.model,
                region: this.region
            });

        var perspectiveModel = controller.getPerspectiveModel();

        deepEqual(perspectiveModel.toJSON(), {
            id: "fields",
            name: "admin.issuetypeconfig.perspective.fields.name"
        }, "the properties on the perspective model should be correct");
    });
});
