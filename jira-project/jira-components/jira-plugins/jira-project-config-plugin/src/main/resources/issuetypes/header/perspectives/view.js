define('jira-project-config/issuetypes/header/perspectives/view', ['require'], function(require) {
    "use strict";

    var TemplatePerspectives = require('jira-project-config/issuetypes/header/perspectives/view/templates');
    var Marionette = require("jira-project-config/marionette");

    var PerspectiveItemView = Marionette.ItemView.extend({
        className: "aui-button project-issuetype-perspective",

        events: {
            simpleClick: "_perspectiveSelected"
        },

        id: function () {
            return "project-issuetypes-perspective-" + this.model.get("id");
        },

        /**
         * Set whether or not this perspective should appear selected.
         * @param {boolean} selected If true, this perspecive should be selected.
         */
        setSelected: function (selected) {
            this.$el.attr("aria-pressed", selected);
        },

        tagName: "button",

        template: TemplatePerspectives.renderPerspective,

        _perspectiveSelected: function () {
            this.trigger("perspective:selected", this.model);
        }
    });

    return Marionette.CollectionView.extend(
        /** @lends JIRA.ProjectConfig.IssueTypes.Header.Perspectives.View# */
        {
            className: "aui-buttons",

            /**
             * Initializes the view.
             *
             * @classdesc A view for showing controls for each perspective, allowing switching between perspectives.
             * @constructs
             */
            initialize: function (options) {
                this.model = options.model;
                this.collection = options.model.get("perspectives");
            },

            itemEvents: {
                "perspective:selected": function (eventName, childView, model) {
                    this.trigger("perspective:selected", model);
                }

            },

            modelEvents: {
                "change:selectedPerspective": "_markSelected"
            },

            onRender: function () {
                this._markSelected();
            },

            _markSelected: function () {
                var perspective = this.model.get("selectedPerspective");

                this.children.each(function (child) {
                    child.setSelected(child.model === perspective);
                });
            },

            itemView: PerspectiveItemView
        });
});
