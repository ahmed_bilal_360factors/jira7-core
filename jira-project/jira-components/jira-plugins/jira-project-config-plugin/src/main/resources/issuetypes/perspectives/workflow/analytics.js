define("jira-project-config/issuetypes/perspectives/workflow/analytics", [
    "jira-project-config/libs/ajshelper",
    "underscore"
], function (AJSHelper, _) {
    "use strict";

    var enchanceParameters = function (prefix, sufix, userWithEditWorkflow) {
        var infix = userWithEditWorkflow ? "editworkflowpermission." : "";
        return {name: prefix + infix + sufix};
    };

    return {
        triggerViewProject: function (userWithEditWorkflow) {
            AJSHelper.trigger("analytics", enchanceParameters("administration.workflow.", "open.view.project", userWithEditWorkflow));
        },

        triggerEditProject: function (userWithEditWorkflow) {
            AJSHelper.trigger("analytics", enchanceParameters("administration.workflow.", "open.edit.project", userWithEditWorkflow));
        },

        triggerDiscardEditedWorkflow: function (userWithEditWorkflow) {
            AJSHelper.trigger("analytics", enchanceParameters("administration.project.", "discard.edited.workflow", userWithEditWorkflow));
        },

        triggerDiscardEditedWorkflowFail: function (userWithEditWorkflow) {
            AJSHelper.trigger("analytics", enchanceParameters("administration.project.", "discard.edited.workflow.failed", userWithEditWorkflow));
        },

        triggerPublishEditedWorkflow: function (userWithEditWorkflow) {
            AJSHelper.trigger("analytics", enchanceParameters("administration.project.", "publish.edited.workflow", userWithEditWorkflow));
        },

        triggerPublishEditedWorkflowFail: function (userWithEditWorkflow) {
            AJSHelper.trigger("analytics", enchanceParameters("administration.project.", "publish.edited.workflow.failed", userWithEditWorkflow));
        },

        triggerEditedWorkflowClicked: function (userWithEditWorkflow) {
            AJSHelper.trigger("analytics", enchanceParameters("administration.project.", "edit.workflow", userWithEditWorkflow));
        }
    };
});
