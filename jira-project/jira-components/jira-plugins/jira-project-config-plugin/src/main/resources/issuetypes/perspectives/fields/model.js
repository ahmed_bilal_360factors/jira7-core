define("jira-project-config/issuetypes/perspectives/fields/model", ["require"], function (require) {
    "use strict";

    var Backbone = require("jira-project-config/backbone");

    /**
     * @class
     * @classdesc A model storing the state of a {@link JIRA.ProjectConfig.IssueTypes.Fields.View}.
     * @extends Backbone.Model
     */
    return Backbone.Model.extend(
        /** @lends JIRA.ProjectConfig.IssueTypes.Fields.ViewModel# */
        {
            defaults: {
                /**
                 * The issue type to display.
                 *
                 * @memberof JIRA.ProjectConfig.IssueTypes.Fields.ViewModel#
                 * @type {JIRA.ProjectConfig.IssueTypes.Entities.IssueType|null}
                 */
                issueType: null,

                /**
                 * The current project.
                 *
                 * @memberof JIRA.ProjectConfig.IssueTypes.Fields.ViewModel#
                 * @type {JIRA.ProjectConfig.IssueTypes.Entities.Project|null}
                 */
                project: null
            }
        });
});
