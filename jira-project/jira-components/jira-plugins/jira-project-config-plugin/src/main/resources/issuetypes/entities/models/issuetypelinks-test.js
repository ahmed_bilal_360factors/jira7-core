AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-entities"], function(){
    "use strict";

    var IssueTypeLinks = require("jira-project-config/issuetypes/entities/models/issue-type-links");

    module("JIRA.ProjectConfig.IssueTypes.Entities.IssueTypeLinks");

    test("Defaults", function () {
        var issueTypes = new IssueTypeLinks();

        equal(issueTypes.length, 0);
    });
});
