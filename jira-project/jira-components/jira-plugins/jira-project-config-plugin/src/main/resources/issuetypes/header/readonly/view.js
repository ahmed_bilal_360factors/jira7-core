define('jira-project-config/issuetypes/header/readonly/view', [
    'jira-project-config/marionette'
], function(
    Marionette
) {
    "use strict";

    return Marionette.ItemView.extend({
        serializeData: function () {
            return {
                workflowState: this.model ? this.model.get('state') : ''
            };
        },

        template: JIRA.Templates.IssueTypeConfig.Header.ReadOnly.readonly
    });
});
