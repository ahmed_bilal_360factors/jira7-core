AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-navigation"], function(){
    "use strict";

    var jQuery = require("jquery");
    var Backbone = require("jira-project-config/backbone");
    var Layout = require("jira-project-config/issuetypes/navigation/views/layout");

    function Inspector(element) {
        this.element = jQuery(element);
    }

    Inspector.prototype = {
        summary: function () {
            return this.element.find("li#project-issuetypes-summary").html();
        },

        issueTypes: function () {
            return this.element.find("ul.project-issuetypes").html();
        },

        expander: function () {
            return this.element.find("div#project-issuetypes-expand-container").html();
        }
    };

    module("JIRA.ProjectConfig.IssueTypes.Navigation.Layout", {
        setup: function () {
            this.$fixture = jQuery("#qunit-fixture");

            this.ContentView = Backbone.View.extend({
                initialize: function (options) {
                    this.content = options.content;
                },

                render: function () {
                    this.$el.html(this.content);
                    return this;
                }
            });
        }
    });

    test("regions", 3, function () {
        var inspector;
        var layout;
        var summary;
        var issueTypes;
        var expander;

        summary = new this.ContentView({
            content: "summary",
            id: "project-issuetypes-summary",
            tagName: "li"
        });
        issueTypes = new this.ContentView({
            className: "project-issuetypes",
            content: "issueTypes",
            tagName: "ul"

        });
        expander = new this.ContentView({
            content: "expander",
            id: "project-issuetypes-expand-container"
        });

        layout = new Layout();
        layout.render();
        layout.summary.show(summary);
        layout.issueTypes.show(issueTypes);
        layout.expander.show(expander);

        inspector = new Inspector(layout.el);
        strictEqual(inspector.summary(), "summary");
        strictEqual(inspector.issueTypes(), "issueTypes");
        strictEqual(inspector.expander(), "expander");
    });
});
