define("jira-project-config/issuetypes/perspectives/workflow/model", ["require"], function (require) {
    "use strict";

    var Backbone = require('jira-project-config/backbone');

    /**
     * @class
     * @classdesc A model storing the state of the view. This model should be treated as read-only by the view.
     *
     * It allows the state to be stored in a way that allows the view to respond to changes in it via the Backbone
     * `change:*` events. Following this architecture, when the view wants to change the state, it should fire events
     * that the controller can respond to.
     * @extends Backbone.Model
     */
    return Backbone.Model.extend(
        /** @lends JIRA.ProjectConfig.IssueTypes.Workflow.Model# */
        {
            defaults: {
                /**
                 * If true, the interface should be in 'edit' mode.
                 * @memberof JIRA.ProjectConfig.IssueTypes.Workflow.Model
                 * @type {boolean}
                 */
                editing: false,

                /**
                 * The issue type whose workflow should be displayed.
                 * @memberof JIRA.ProjectConfig.IssueTypes.Workflow.Model
                 * @type {JIRA.ProjectConfig.IssueTypes.Entities.IssueType|null}
                 */
                issueType: null,

                /**
                 * The project that the issue type is in.
                 * @memberof JIRA.ProjectConfig.IssueTypes.Workflow.Model
                 * @type {JIRA.ProjectConfig.IssueTypes.Entities.Project|null}
                 */
                project: null
            }
        });
});
