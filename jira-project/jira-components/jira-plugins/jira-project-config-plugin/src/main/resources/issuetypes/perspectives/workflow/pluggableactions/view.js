define("jira-project-config/issuetypes/perspectives/workflow/pluggable-actions/view", ["require"], function (require) {
    "use strict";

    var Marionette = require('jira-project-config/marionette');
    var TemplatePluggableActions = require('jira-project-config/issuetypes/perspectives/workflow/pluggable-actions/templates');

    /**
     * @class
     * @classdesc A view that handles rendering the actions that can be applied to a workflow.
     * @extends Marionette.ItemView
     */
    return Marionette.ItemView.extend(
        /** @lends JIRA.ProjectConfig.IssueTypes.Workflow.PluggableActions.View */
        {
            events: {
                "click #edit_workflow": "_onEditWorkflow",
                "click #edit_workflow_permission": "_onEditWorkflowPermission"
            },

            /**
             * Disables or enables the buttons in the pluggable actions.
             * @param {Boolean} toggle if true, enables buttons, if false, disables buttons
             */
            disable: function (toggle) {
                this.$el.find("button")
                    .attr("aria-disabled", toggle)
                    .attr("disabled", toggle);
            },

            id: "workflow_pluggable_controls",

            /**
             * @private
             */
            serializeData: function () {
                var issueType = this.model.get("issueType");
                var project = this.model.get("project");
                var workflow;

                workflow = issueType.get("workflow");
                return {
                    issueTypeId: issueType.get("id"),
                    workflow: workflow.toJSON(),
                    project: project.toJSON(),
                    /*jshint camelcase: false */
                    atl_token: atl_token(),
                    /*jshint camelcase: true */
                    isEditing: this.model.get("editing"),
                    isBrowserSupported: JIRA.WorkflowDesigner.browserIsSupported()
                };
            },

            /**
             * @private
             */
            template: TemplatePluggableActions.render,

            /**
             * @private
             */
            _onEditWorkflow: function () {
                this.trigger("click:edit");
            },

            /**
             * @private
             */
            _onEditWorkflowPermission: function () {
                this.trigger("click:edit", {workflowEditPermission: true});
            }
        });
});
