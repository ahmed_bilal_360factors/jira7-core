define('jira-project-config/issuetypes/header/controller', ['require'], function(require) {
    "use strict";

    var HeaderView = require('jira-project-config/issuetypes/header/view');
    var HeaderSharedByView = require('jira-project-config/issuetypes/header/shared-by/view');
    var HeaderPerspectivesView = require('jira-project-config/issuetypes/header/perspectives/view');
    var HeaderModel = require('jira-project-config/issuetypes/header/model');
    var HeaderDescriptorView = require('jira-project-config/issuetypes/header/descriptor/view');
    var HeaderReadOnlyView = require('jira-project-config/issuetypes/header/readonly/view');

    var _ = require('underscore');
    var Marionette = require("jira-project-config/marionette");

    return Marionette.Controller.extend(
        /** @lends JIRA.ProjectConfig.IssueTypes.Header.Controller# */
        {
            /**
             * Initialises the controller.
             *
             * @classdesc The controller responsible for orchestrating the header area of the page.
             * @constructs
             * @extends Marionette.Controller
             * @param {Marionette.Wreqr.Commands} options.commands The channel for registering command handlers. Registers handlers
             * on this channel for:
             *  - `issueTypes:resetPluggableRegions`
             * @param {JIRA.ProjectConfig.IssueTypes.Model} options.model The state of the page.
             * @param {JIRA.ProjectConfig.IssueTypes.Entities.Perspective[]} options.perspectives The perspectives to render in the perspectives switcher.
             * @param {Marionette.Region} options.region The region to render the header view into.
             * @param {Marionette.Wreqr.EventAggregator} options.vent The channel for firing events off. Can fire:
             *  - `header:perspectiveSelected` with the selected {@link JIRA.ProjectConfig.IssueTypes.Entities.Perspective} as argument
             *  - `header:issueTypeSelected` with the selected {@link JIRA.ProjectConfig.IssueTypes.Entities.IssueType} as argument
             *
             */
            initialize: function (options) {
                this.commands = options.commands;
                this.pageModel = options.model;
                this.region = options.region;
                this.vent = options.vent;

                this.viewModel = new HeaderModel();
                this.viewModel.get("perspectives").reset(options.perspectives);
                // Initialize model now
                this._refresh();

                this.listenTo(this.pageModel, "change", this._refresh);

                this.view = new HeaderView();

                this.commands.setHandler("issueTypes:resetPluggableRegions", _.bind(this._resetPluggableRegions, this));
            },

            /**
             * Sets the region.
             *
             * @param {Marionette.Region} region the region to set
             */
            setRegion: function (region) {
                this.region = region;
            },

            /**
             * Shows the header view in the region.
             */
            show: function () {
                var perspectivesView = new HeaderPerspectivesView({
                    model: this.viewModel
                });

                this.listenTo(perspectivesView, "perspective:selected", function (perspective) {
                    this.vent.trigger("header:perspectiveSelected", perspective);
                });
                this.listenTo(perspectivesView, "close", function () {
                    this.stopListening(perspectivesView);
                });

                this.region.show(this.view);
                this.view.perspectives.show(perspectivesView);

                if (this.viewModel.get('selectedIssueType') && this.viewModel.get('selectedIssueType').get('workflow')) {
                    var workflow = this.viewModel.get('selectedIssueType').get('workflow');

                    var readOnlyView = new HeaderReadOnlyView({
                        model: workflow
                    });
                    this.view.readOnly.show(readOnlyView);
                }
            },

            /**
             * Handler for `issueTypes:resetPluggableRegions`. Clears all data currently in the header and reinitializes the
             * regions with the data that is provided in the options.
             *
             * @param {JIRA.ProjectConfig.IssueTypes.Entities.HeaderDescriptor} [options.descriptor] the model of the descriptor.
             * @param {Marionette.View} [options.actions] the view to show in the actions region.
             * @param {Marionette.View} [options.info] the view to show in the info region.
             * @param {Backbone.Model} [options.sharedBy] the model of shared by data. Should be immutable.
             * @private
             */
            _resetPluggableRegions: function (options) {
                var sharedByView;

                options = options || {};

                // Clear the pluggable regions
                this.view.descriptor.close();
                this.view.actions.close();
                this.view.sharedBy.close();
                this.view.readOnly.close();
                this.view.workflowInfo.close();

                // Re-initialize the regions conditionally on their existence in the provided options
                if (options.descriptor) {
                    this.view.descriptor.show(new HeaderDescriptorView({
                        model: options.descriptor
                    }));
                }
                if (options.actions) {
                    this.view.actions.show(options.actions);
                }
                if (options.info) {
                    this.view.workflowInfo.show(options.info);
                }
                if (options.sharedBy) {
                    sharedByView = new HeaderSharedByView({
                        model: options.sharedBy
                    });

                    this.listenTo(sharedByView, "issueType:selected", function (issueType) {
                        this.vent.trigger("header:issueTypeSelected", issueType);
                    });
                    this.listenTo(sharedByView, "close", function () {
                        this.stopListening(sharedByView);
                    });

                    this.view.sharedBy.show(sharedByView);
                }
                if (options.workflow) {
                    var readOnlyView = new HeaderReadOnlyView({
                        model: options.workflow
                    });
                    this.view.readOnly.show(readOnlyView);
                }
            },

            /**
             * @private
             */
            _refresh: function () {
                this.viewModel.set({
                    selectedPerspective: this.pageModel.get("selectedPerspective"),
                    selectedIssueType: this.pageModel.get("selectedIssueType")
                });
            }
        });
});
