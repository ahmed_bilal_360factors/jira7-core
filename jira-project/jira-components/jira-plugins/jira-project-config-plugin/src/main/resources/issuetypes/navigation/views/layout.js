define('jira-project-config/issuetypes/navigation/views/layout', ['require'], function(require) {
    "use strict";

    var Marionette = require("jira-project-config/marionette");
    var TemplateNavigation = require('jira-project-config/issuetypes/navigation/templates');
    var VirtualRegion = require('jira-project-config/utils/virtual-region');
    var _ = require('underscore');

    return Marionette.Layout.extend(
        /** @lends JIRA.ProjectConfig.IssueTypes.Navigation.Layout# */
        {
            attributes: {
                "class": "aui-nav",
                id: "project_issuetypes"
            },

            /**
             * Initialises the view.
             *
             * @classdesc A layout used to compose together the top level parts of the navigation.
             * @constructs
             * @extends Marionette.Layout
             */
            initialize: function () {
                var expander;
                var issueTypes;
                var summary;

                _.bindAll(this, "template");

                expander = new VirtualRegion({
                    el: this.$el,
                    target: "#project-issuetypes-expand-container"
                });

                issueTypes = new VirtualRegion({
                    el: this.$el,
                    target: ".project-issuetypes"
                });

                summary = new VirtualRegion({
                    el: this.$el,
                    target: "#project-issuetypes-summary"
                });

                this.addRegion("expander", expander);
                this.addRegion("issueTypes", issueTypes);
                this.addRegion("summary", summary);
            },

            tagName: "ul",

            template: TemplateNavigation.base
        });
});
