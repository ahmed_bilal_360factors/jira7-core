AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-tab"], function(){
    "use strict";

    module("JIRA.ProjectConfig.IssueTypes.Router");

    var _ = require("underscore");
    var IssueTypesController = require("jira-project-config/issuetypes/controller");
    var IssueTypesRouter = require("jira-project-config/issuetypes/router");

    test("routes correspond to controller methods", function () {
        var controller = IssueTypesController.prototype;
        var routes = IssueTypesRouter.prototype.appRoutes;

        _.each(routes, function (value) {
            strictEqual(typeof controller[value], "function");
        });
    });
});
