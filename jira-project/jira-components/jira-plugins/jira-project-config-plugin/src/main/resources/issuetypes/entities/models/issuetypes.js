define('jira-project-config/issuetypes/entities/models/issue-types', ['require'], function (require) {
    "use strict";

    var Backbone = require('jira-project-config/backbone');
    var IssueType = require('jira-project-config/issuetypes/entities/models/issue-type');

    /**
     * @class
     * @classdesc A collection of {@link JIRA.ProjectConfig.IssueTypes.Entities.IssueType}.
     * @extends Backbone.Collection
     */
    return Backbone.Collection.extend(
        /** @lends JIRA.ProjectConfig.IssueTypes.Entities.IssueTypes# */
        {
            model: IssueType
        });
});
