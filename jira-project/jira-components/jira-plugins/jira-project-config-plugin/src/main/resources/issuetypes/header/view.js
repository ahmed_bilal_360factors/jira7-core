define('jira-project-config/issuetypes/header/view', ['require'], function(require) {
    "use strict";

    var TemplateHeader = require('jira-project-config/issuetypes/header/templates');
    var Marionette = require("jira-project-config/marionette");

    /**
     * @class
     * @classdesc A layout that defines the various regions of the issue types header.
     * @extends Marionette.Layout
     */
    return Marionette.Layout.extend(
        /** @lends JIRA.ProjectConfig.IssueTypes.Header.View# */
        {
            /**
             * The regions of this layout.
             *  - descriptor: the region where a description of the perspective is rendered.
             *  - perspectives: the region where the possible perspectives are rendered to be switched between.
             *  - pluggableActions: the region where pluggable actions can be rendered by interested parties.
             *  - sharedBy: the region where the shared by information is rendered.
             */
            regions: {
                descriptor: "#project-issuetypes-descriptor",
                perspectives: "#project-issuetypes-perspectives",
                actions: "#project-issuetypes-actions",
                sharedBy: "#project-issuetypes-shared-by",
                readOnly: "#workflow-read-only",
                workflowInfo: "#workflow-info"

            },

            template: TemplateHeader.render
        });
});
