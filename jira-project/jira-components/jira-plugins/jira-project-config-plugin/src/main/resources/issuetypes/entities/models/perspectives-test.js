AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-entities"], function(){
    "use strict";

    var Perspectives = require("jira-project-config/issuetypes/entities/models/perspectives");

    module("JIRA.ProjectConfig.IssueTypes.Entities.Perspectives");

    test("Defaults", function () {
        var perspectives = new Perspectives();

        equal(perspectives.length, 0);
    });
});
