AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-entities", "com.atlassian.jira.jira-project-config-plugin:issuetypes-test-utils"], function(){
    "use strict";

    var Marionette = require("jira-project-config/marionette");
    var _ = require("underscore");
    var ProjectIssueTypeManager = require("jira-project-config/issuetypes/entities/ajax/project-issue-type-manager");
    var Chance = require("jira-project-config/libs/chance");
    var ChanceMixins = require("jira-project-config/issuetypes/entities/test/chance-mixins");

    function escapeRegExp(str) {
        return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    }

    // Having a regex is useful for sinon's fake server, as it adds a wildcard for jQuery's extra query string
    // parameter that it uses for cache busting.
    function jQueryCacheBustingAwareUrlRegex(url) {
        var queryString = url.indexOf("?") === -1 ? "\\?_=\\d+" : "\\&_=\\d+";
        return new RegExp("^" + escapeRegExp(url) + "(" + queryString + ")?$");
    }

    module("JIRA.ProjectConfig.IssueTypes.Ajax.ProjectIssueTypeManager", {
        setup: function () {
            this.chance = new Chance(1);
            _.extend(this.chance, new ChanceMixins());

            this.sandbox = sinon.sandbox.create();
            this.server = sinon.fakeServer.create();
            this.reqres = new Marionette.Wreqr.RequestResponse();
            this.reqres.setHandlers({
                "urls:issueTypes:rest:fields": function (options) {
                    return options.project.get("key") + "/" + options.issueType.get("id") + "/api/fields";
                },
                "urls:issueTypes:rest:workflow": function (options) {
                    return options.project.get("key") + "/" + options.issueType.get("id") + "/api/workflow";
                }
            });
            this.manager = new ProjectIssueTypeManager({
                reqres: this.reqres
            });
        },

        teardown: function () {
            this.server.restore();
            this.sandbox.restore();
        },

        workflowUrlRegExp: function (project, issueType) {
            var url = this.reqres.request("urls:issueTypes:rest:workflow", {
                project: project,
                issueType: issueType
            });
            return jQueryCacheBustingAwareUrlRegex(url);
        },

        fieldsUrlRegExp: function (project, issueType) {
            var url = this.reqres.request("urls:issueTypes:rest:fields", {
                project: project,
                issueType: issueType
            });
            return jQueryCacheBustingAwareUrlRegex(url);
        }
    });

    test("workflow() resolves the returned promise with decoded JSON from successful HTTP responses", 2, function () {
        var project = this.chance.project();
        var issueType = this.chance.issueType();
        var sample = this.chance.workflow(); // Used to generate data for our dummy response.

        var response = {
            name: sample.get("name"),
            displayName: sample.get("displayName"),
            state: sample.get("state"),
            sharedWithProjects: [this.chance.project().toJSON()],
            sharedWithIssueTypes: [this.chance.issueType().get("id")]
        };

        var url = this.workflowUrlRegExp(project, issueType);

        this.manager.workflow(project, issueType)
            .done(function (workflow) {
                equal(arguments.length, 1);
                deepEqual(workflow, response);
            });

        this.server.respondWith(url, JSON.stringify(response));
        this.server.respond();
    });

    test("workflow() rejects the returned promise on AJAX error", 2, function () {
        var issueType = this.chance.issueType();
        var project = this.chance.project();
        var url = this.workflowUrlRegExp(project, issueType);
        var errorMessage = this.chance.string();

        this.manager.workflow(project, issueType)
            .fail(function (jqXHR) {
                equal(arguments.length, 1);
                equal(jqXHR.responseText, errorMessage);
            });

        this.server.respondWith(url, [400, {}, errorMessage]);
        this.server.respond();
    });

    test("fields() resolves the returned promise with decoded JSON from successful HTTP responses", 2, function () {
        var project = this.chance.project();
        var issueType = this.chance.issueType();
        var sample = this.chance.fields();  // Used to generate data for our dummy response.

        var response = {
            screenName: sample.get("screenName"),
            sharedWithProjects: [this.chance.project().toJSON()],
            sharedWithIssueTypes: [this.chance.issueType().get("id")],
            viewScreenId: sample.get("viewScreenId")
        };

        var url = this.fieldsUrlRegExp(project, issueType);

        this.manager.fields(project, issueType)
            .done(function (fields) {
                equal(arguments.length, 1);
                deepEqual(fields, response);
            });

        this.server.respondWith(url, JSON.stringify(response));
        this.server.respond();
    });

    test("fields() rejects the returned promise on AJAX error", 2, function () {
        var issueType = this.chance.issueType();
        var project = this.chance.project();
        var url = this.fieldsUrlRegExp(project, issueType);
        var errorMessage = this.chance.string();

        this.manager.fields(project, issueType)
            .fail(function (jqXHR) {
                equal(arguments.length, 1);
                equal(jqXHR.responseText, errorMessage);
            });

        this.server.respondWith(url, [400, {}, errorMessage]);
        this.server.respond();
    });
});
