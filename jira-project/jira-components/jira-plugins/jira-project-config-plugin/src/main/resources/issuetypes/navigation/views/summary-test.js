AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-navigation"], function(){
    "use strict";

    var jQuery = require("jquery");
    var SummaryView = require("jira-project-config/issuetypes/navigation/views/summary");
    var Model = require("jira-project-config/issuetypes/navigation/model");

    function Inspector(element) {
        this.element = jQuery(element);
    }

    Inspector.prototype = {
        link: function () {
            return this.element.find("a");
        },

        selected: function () {
            return this.element.hasClass("aui-nav-selected");
        },

        text: function () {
            return this.link().text();
        },

        url: function () {
            return this.link().attr("href");
        }
    };

    module("JIRA.ProjectConfig.IssueTypes.Navigation.SummaryView", {
        setup: function () {
            this.model = new Model();
        }
    });

    test("non-empty text", 1, function () {
        var inspector;
        var view;

        view = new SummaryView({
            model: this.model
        });

        inspector = new Inspector(view.render().el);
        ok(inspector.link().text() !== "");
    });

    test("initial selected-ness ", 2, function () {
        var inspector;
        var view;

        this.model.set("selection", "summary");
        view = new SummaryView({
            model: this.model
        });
        inspector = new Inspector(view.render().el);
        ok(inspector.selected() === true);

        this.model.set("selection", null);
        view = new SummaryView({
            model: this.model
        });
        inspector = new Inspector(view.render().el);
        ok(inspector.selected() === false);
    });

    test("selected-ness updates on model change", 2, function () {
        var inspector;
        var view;

        view = new SummaryView({
            model: this.model
        });

        inspector = new Inspector(view.render().el);

        ok(inspector.selected() === false);
        this.model.set("selection", "summary");
        ok(inspector.selected());
    });

    test("url updates on model change", 3, function () {
        var inspector;
        var view;

        view = new SummaryView({
            model: this.model
        });
        inspector = new Inspector(view.render().el);
        strictEqual(inspector.url(), undefined);

        this.model.set("projectKey", "FOO");
        ok(inspector.url().indexOf("FOO") !== -1);
        this.model.set("projectKey", "BAR");
        ok(inspector.url().indexOf("BAR") !== -1);
    });

    test("initial url", 1, function () {
        var inspector;
        var view;

        this.model.set("projectKey", "FOO");
        view = new SummaryView({
            model: this.model
        });
        inspector = new Inspector(view.render().el);
        ok(inspector.url().indexOf("FOO") !== -1);
    });
});
