AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-navigation"], function(){
    "use strict";

    var jQuery = require("jquery");
    var IssueTypeLink = require("jira-project-config/issuetypes/entities/models/issue-type-link");
    var IssueTypeLinks = require("jira-project-config/issuetypes/entities/models/issue-type-links");
    var Model = require("jira-project-config/issuetypes/navigation/model");

    module("JIRA.ProjectConfig.IssueTypes.Navigation.Model", {
        setup: function () {
            this.$fixture = jQuery("#qunit-fixture");
        }
    });

    test("Defaults", 6, function () {
        var model = new Model();

        ok(model.get("issueTypeLinks") instanceof IssueTypeLinks);
        strictEqual(model.get("projectKey"), null);
        strictEqual(model.get("selection"), null);
        strictEqual(model.get("teaserCount"), 10);
        strictEqual(model.get("collapsed"), false);
        strictEqual(model.get("userWantsExpandedMode"), false);
    });

    test("collapsed honors 'userWantsExpandedMode'", 2, function () {
        var model = new Model({
            teaserCount: 0,
            issueTypeLinks: new IssueTypeLinks([
                new IssueTypeLink(),
                new IssueTypeLink()
            ])
        });

        strictEqual(model.get("collapsed"), true);
        model.set("userWantsExpandedMode", true);
        strictEqual(model.get("collapsed"), false);
    });

    test("collapsed tracks 'teaserCount'", 3, function () {
        var model = new Model({
            teaserCount: 1,
            issueTypeLinks: new IssueTypeLinks([
                new IssueTypeLink(),
                new IssueTypeLink(),
                new IssueTypeLink()
            ])
        });

        strictEqual(model.get("collapsed"), true);
        model.get("issueTypeLinks").pop();
        strictEqual(model.get("collapsed"), false);
        model.get("issueTypeLinks").add(new IssueTypeLink());
        strictEqual(model.get("collapsed"), true);
    });

    test("collapsed honors 'selection'", 4, function () {
        var issueType1 = new IssueTypeLink();
        var issueType2 = new IssueTypeLink();

        var model = new Model({
            teaserCount: 0,
            issueTypeLinks: new IssueTypeLinks([issueType1, issueType2])
        });

        strictEqual(model.get("collapsed"), true);
        model.set("selection", "summary");
        strictEqual(model.get("collapsed"), false);
        model.set("selection", null);
        strictEqual(model.get("collapsed"), true);
        model.set("selection", issueType1);
        strictEqual(model.get("collapsed"), false);
    });

    test("collapsed honors issue types reset", 2, function () {
        var issueType1 = new IssueTypeLink();
        var issueType2 = new IssueTypeLink();

        var model = new Model({
            teaserCount: 0
        });

        strictEqual(model.get("collapsed"), false);
        model.get("issueTypeLinks").reset([issueType1, issueType2]);
        strictEqual(model.get("collapsed"), true);
    });
});
