define('jira-project-config/issuetypes/entities/models/issue-type', ['require'], function (require) {
    "use strict";

    var Backbone = require('jira-project-config/backbone');
    var _ = require('underscore');

    /**
     * @class
     * @classdesc An issue type in a project.
     * @extends Backbone.Model
     */
    return Backbone.Model.extend(
        /** @lends JIRA.ProjectConfig.IssueTypes.Entities.IssueType# */
        {
            defaults: {
                /**
                 * The ID of the issue type. Should always be provided.
                 *
                 * @memberof JIRA.ProjectConfig.IssueTypes.Entities.IssueType#
                 * @type {string}
                 */
                id: undefined,

                /**
                 * The fields for the issue type.
                 *
                 * @memberof JIRA.ProjectConfig.IssueTypes.Entities.IssueType#
                 * @type {JIRA.ProjectConfig.IssueTypes.Entities.Fields|undefined}
                 */
                fields: undefined,

                /**
                 * The name of the issue type. Should always be provided.
                 *
                 * @memberof JIRA.ProjectConfig.IssueTypes.Entities.IssueType#
                 * @type {string}
                 */
                name: undefined,

                /**
                 * The issue type's workflow.
                 *
                 * @memberof JIRA.ProjectConfig.IssueTypes.Entities.IssueType#
                 * @type {JIRA.ProjectConfig.IssueTypes.Entities.Workflow|undefined}
                 */
                workflow: undefined
            },

            toJSON: function () {
                return _.extend({}, this.attributes, {
                    fields: this.attributes.fields && this.attributes.fields.toJSON(),
                    workflow: this.attributes.workflow && this.attributes.workflow.toJSON()
                });
            }
        });
});
