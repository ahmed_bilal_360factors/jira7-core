AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-entities"], function(){
    "use strict";

    var Fields = require("jira-project-config/issuetypes/entities/models/fields");
    var IssueTypes = require("jira-project-config/issuetypes/entities/models/issue-types");
    var Projects = require("jira-project-config/issuetypes/entities/models/projects");

    module("JIRA.ProjectConfig.IssueTypes.Entities.Fields");

    test("Defaults", function () {
        var fields = new Fields();

        equal(fields.get("screenName"), undefined);
        ok(fields.get("sharedWithProjects") instanceof Projects);
        ok(fields.get("sharedWithIssueTypes") instanceof IssueTypes);
        equal(fields.get("viewScreenId"), undefined);

        equal(typeof(fields.get("totalProjectsCount")), "number");
        equal(fields.get("totalProjectsCount"), 0);

        equal(typeof(fields.get("hiddenProjectsCount")), "number");
        equal(fields.get("hiddenProjectsCount"), 0);
    });

    test("toJSON() serialises child collections", function () {
        var fields = new Fields();

        deepEqual(fields.toJSON(), {
            screenName: undefined,
            sharedWithProjects: [],
            sharedWithIssueTypes: [],
            totalProjectsCount: 0,
            hiddenProjectsCount: 0,
            viewScreenId: undefined
        });
    });
});

