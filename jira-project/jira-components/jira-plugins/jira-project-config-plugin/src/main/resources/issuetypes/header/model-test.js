AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-tab"], function(){
    "use strict";

    var Perspectives = require("jira-project-config/issuetypes/entities/models/perspectives");
    var HeaderModel = require("jira-project-config/issuetypes/header/model");

    module("JIRA.ProjectConfig.IssueTypes.Header.Model");

    test("defaults", function () {
        var model;
        var perspectives;

        model = new HeaderModel();

        perspectives = model.get("perspectives");
        ok(perspectives instanceof Perspectives);
        ok(perspectives.length === 0);

        equal(model.get("selectedIssueType"), null);
        equal(model.get("selectedPerspective"), null);
    });
});
