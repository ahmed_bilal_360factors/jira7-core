AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:issuetypes-tab"], function(){
    "use strict";

    var _ = require("underscore");
    var Breadcrumb = require('jira-project-config/issuetypes/entities/models/breadcrumb');
    var Breadcrumbs = require('jira-project-config/issuetypes/entities/models/breadcrumbs');
    var jQuery = require("jquery");
    var HeaderDescriptor = require("jira-project-config/issuetypes/entities/models/header-descriptor");
    var HeaderDescriptorView = require("jira-project-config/issuetypes/header/descriptor/view");

    var DescriptorPageObject = function (el) {
        this.$el = jQuery(el);
    };
    _.extend(DescriptorPageObject.prototype, {
        getTitle: function () {
            return this.$el.find("#project-config-header-descriptor-title").text();
        },
        getSubtitle: function () {
            return this.$el.find("#project-config-header-descriptor-subtitle").text();
        },
        getBreadcrumbsCount: function () {
            return this.getBreadcrumbs().length;
        },
        getBreadcrumbs: function () {
            return this.$el.find('.admin-breadcrumb-trail ol li');
        },
        getBreadcrumbLinkByIndex: function (index) {
            return jQuery(this.getBreadcrumbs()[index]).find('a:first').attr('href');
        },
        getBreadcrumbDisplayNameByIndex: function (index) {
            return jQuery(this.getBreadcrumbs()[index]).text();
        },
        isBreadcrumbAtIndexSelected: function (index) {
            return jQuery(this.getBreadcrumbs()[index]).hasClass('aui-nav-selected');
        }
    });

    module("JIRA.ProjectConfig.IssueTypes.Header.Descriptor.View", {
        createView: function (model) {
            return new HeaderDescriptorView({model: model});
        }
    });

    test("title and subtitle are rendered into the title and subtitle elements respectively", function () {
        var descriptorPageObject;
        var model = new HeaderDescriptor({
            subtitle: "my subtitle",
            title: "my title",
            breadcrumbs: new Breadcrumbs(
                [new Breadcrumb({
                    link: "link1",
                    name: "name1",
                    selected: false
                }), new Breadcrumb({
                    link: "link2",
                    name: "name2",
                    selected: false
                }), new Breadcrumb({
                    name: "name3",
                    selected: true
                })]
            )
        });

        var view = this.createView(model);

        descriptorPageObject = new DescriptorPageObject(view.render().$el);

        equal(descriptorPageObject.getTitle(), "my title", "title in model should be rendered");
        equal(descriptorPageObject.getSubtitle(), "my subtitle", "subtitle in model should be rendered");
        equal(descriptorPageObject.getBreadcrumbsCount(), 3, "three breadcrumbs in model should be rendered");
        equal(descriptorPageObject.getBreadcrumbLinkByIndex(1), "link2", "Expected the link [link2] to be rendered at the specified index [1]");
        equal(descriptorPageObject.getBreadcrumbDisplayNameByIndex(2), "name3", "Expected the breadcrumb display name [name3] to be rendered at the specified index [2]");
        equal(descriptorPageObject.isBreadcrumbAtIndexSelected(1), false, "The second breadcrumb at index 1 should not be selected");
        equal(descriptorPageObject.isBreadcrumbAtIndexSelected(2), true, "The third breadcrumb at index 2 should be selected");
    });
});
