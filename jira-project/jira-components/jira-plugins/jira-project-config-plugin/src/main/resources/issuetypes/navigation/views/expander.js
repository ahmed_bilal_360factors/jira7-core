define('jira-project-config/issuetypes/navigation/views/expander', ['require'], function(require) {
    "use strict";

    var Marionette = require("jira-project-config/marionette");
    var TemplateNavigation = require('jira-project-config/issuetypes/navigation/templates');

    /**
     * @classdesc Renders the 'XXX more issue types' link at the bottom of the navigation.
     * @extends Marionette.ItemView
     * @param {JIRA.ProjectConfig.IssueTypes.Navigation.Model} model The navigation state.
     */
    return Marionette.ItemView.extend(
        /** @lends JIRA.ProjectConfig.IssueTypes.Navigation.ExpanderView# */
        {
            attributes: {
                id: "project-issuetypes-expand-container"
            },

            template: TemplateNavigation.expand,

            triggers: {
                "simpleClick a": "expand"
            }
        });
});
