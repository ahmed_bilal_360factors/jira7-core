define('jira-project-config/error/module', ["require"], function(require) {
    "use strict";

    var _ = require("underscore");

    var ErrorController = require('jira-project-config/error/controller');

    return function (module, application) {
        var commands = application.commands;

        module.addInitializer(function () {
            var controller = new ErrorController({
                    application: application
                });

            var error404 = _.bind(controller.error404, controller);
            var genericError = _.bind(controller.genericError, controller);


            commands.setHandler("error:404", error404);
            commands.setHandler("error:generic", genericError);
        });

        module.addFinalizer(function () {
            commands.removeHandler("error:404");
            commands.removeHandler("error:generic");
        });
    };
});
