AJS.test.require(["com.atlassian.jira.jira-project-config-plugin:error-module"], function(){
    "use strict";

    var $ = require("jquery");
    var ErrorPageNotFound = require('jira-project-config/error/page-not-found');
    var ErrorGeneric = require('jira-project-config/error/generic');
    var ErrorController = require('jira-project-config/error/controller');
    var Marionette = require('jira-project-config/marionette');

    module("JIRA.ProjectConfig.Error.Controller", {
        setup: function () {
            this.$fixture = $("#qunit-fixture");
            this.sandbox = sinon.sandbox.create();

            this.application = new Marionette.Application();
            this.application.title = sinon.stub();
            this.region = {show: sinon.stub()};

            this.controller = new ErrorController({
                application: this.application
            });
        },

        teardown: function () {
            this.sandbox.restore();
        }
    });

    test("error404", 4, function () {
        equal(this.region.show.callCount, 0);
        this.controller.error404(this.region);
        equal(this.region.show.callCount, 1);
        ok(this.region.show.firstCall.args[0] instanceof ErrorPageNotFound);

        ok(this.application.title.calledOnce);
    });

    test("genericError", function () {
        var message = "SOME_MESSAGE";
        equal(this.region.show.callCount, 0);
        this.controller.genericError(this.region, message);
        equal(this.region.show.callCount, 1);
        ok(this.region.show.firstCall.args[0] instanceof ErrorGeneric);
        deepEqual(this.region.show.firstCall.args[0].options, {message: message});

        ok(this.application.title.calledOnce);
    });
});
