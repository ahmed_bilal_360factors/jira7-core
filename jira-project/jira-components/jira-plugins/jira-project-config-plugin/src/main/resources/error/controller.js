define('jira-project-config/error/controller', ["require"], function() {
    "use strict";

    var Marionette = require("jira-project-config/marionette");
    var ErrorGeneric = require('jira-project-config/error/generic');
    var ErrorPageNotFound = require('jira-project-config/error/page-not-found');

    return Marionette.Controller.extend(
        /** @lends JIRA.ProjectConfig.Error.Controller# */
        {
            /**
             * Initialises the controller.
             *
             * @classdesc The controller responsible for orchestrating the content area of the error page.
             * @constructs
             * @extends Marionette.Controller
             * @param {JIRA.ProjectConfig.Application} options.application The shared application.
             */
            initialize: function (options) {
                this.application = options.application;
            },

            /**
             * Show a 404 error in the provided region.
             *
             * @param {Marionette.Region} region The region to render content into.
             */
            error404: function (region) {
                var view = new ErrorPageNotFound();
                region.show(view);
                this.application.title(AJS.I18n.getText("admin.issuetypeconfig.error.bad.url.title"));
            },

            /**
             * Show a generic error in the provided region.
             *
             * @param {Marionette.Region} region The region to render content into.
             * @param {String} message The generic error message to display.
             */
            genericError: function (region, message) {
                var view = new ErrorGeneric({
                    message: message
                });
                region.show(view);
                this.application.title(AJS.I18n.getText("admin.projectconfig.error.generic.title"));
            }
        });
});
