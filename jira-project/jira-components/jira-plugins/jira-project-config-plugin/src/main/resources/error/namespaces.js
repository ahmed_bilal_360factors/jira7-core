AJS.namespace("JIRA.Templates.ProjectConfig.Error.Generic", null, require("jira-project-config/error/generic/templates"));
AJS.namespace("JIRA.Templates.ProjectConfig.Error.PageNotFound", null, require("jira-project-config/error/page-not-found/template"));
AJS.namespace('JIRA.ProjectConfig.Error.Generic.View', null, require('jira-project-config/error/generic'));
AJS.namespace('JIRA.ProjectConfig.Error.PageNotFound.View', null, require('jira-project-config/error/page-not-found'));
AJS.namespace('JIRA.ProjectConfig.Error.Controller', null, require('jira-project-config/error/controller'));
AJS.namespace('JIRA.ProjectConfig.Error.Module', null, require('jira-project-config/error/module'));
