define('jira-project-config/error/generic', ["require"], function() {
    var Marionette = require("jira-project-config/marionette");
    var TemplateErrorGeneric = require('jira-project-config/error/generic/templates');

    /**
     * Initialises the view.
     *
     * @classdesc A view that handles rendering a generic error.
     * @extends Marionette.ItemView
     */
    return Marionette.ItemView.extend({
        serializeData: function () {
            return {
                message: this.options.message
            };
        },
        template: TemplateErrorGeneric.render
    });
});
