define('jira-project-config/error/page-not-found', ["require"], function(require) {
    "use strict";

    var $ = require('jquery');
    var Marionette = require("jira-project-config/marionette");
    var Backbone = require("jira-project-config/backbone");

    var TemplateErrorPageNotFound = require('jira-project-config/error/page-not-found/template');

    /**
     * Initialises the view.
     *
     * @classdesc A view that handles rendering a page not found error.
     * @extends Marionette.ItemView
     */
    return Marionette.ItemView.extend(
        /** @lends JIRA.ProjectConfig.Error.PageNotFound.View# */
        {
            events: {
                "click #page-not-found-go-back": "_goToPreviousPage"
            },

            /**
             * Return to the previous page using push state.
             *
             * @param {DOMEvent} event
             * @private
             */
            _goToPreviousPage: function (event) {
                event.preventDefault();
                Backbone.history.history.back();
            },

            serializeData: function () {
                return {
                    projectKey: $('meta[name=projectKey]').attr("content")
                };
            },

            template: TemplateErrorPageNotFound.render
        });
});
