require(
    [
        'jira-project-config/summary/discover/discover-controller',
        'jquery',
        'jira/skate'
    ],
    function(
        DiscoverController,
        jQuery,
        skate
    ) {
        "use strict";

        skate('workflow-edit-discover', {
            type: skate.type.CLASSNAME,
            attached: function showDiscoverInfo(element) {
                var $el = jQuery(element);
                var controller = new DiscoverController({$el: $el});
                controller.bindToView();
            }
        });
    });
