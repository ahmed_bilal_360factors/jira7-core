define('jira-project-config/summary/discover/discover-view',
    [
        'jira-project-config/marionette'
    ],
    function(
        Marionette
    ) {
    "use strict";

    return Marionette.ItemView.extend({
        ui: {
            "close": ".discover-close button",
            "learnMore": ".learn-more"
        },

        events: {
            "click @ui.close": "close",
            "click @ui.learnMore": "learnMore"
        },

        template: JIRA.Templates.ProjectConfig.EditWorkflow.Discover.info,

        close: function (e) {
            e.preventDefault();
            this.$el.hide();
            this.trigger('close');
        },

        learnMore: function () {
            this.trigger('learnMore');
        }
    });
});
