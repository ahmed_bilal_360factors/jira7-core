require(['jira/dialog/form-dialog', 'jquery'], function(FormDialog, jQuery) {
    jQuery(function () {
        var $emailLink = jQuery("#project-config-email-change");

        if ($emailLink.length === 1) {
            new FormDialog({
                id: "project-email-dialog",
                trigger: $emailLink
            });
        }

        new FormDialog({
            type: "ajax",
            id: "project-config-project-edit-lead-and-default-assignee-dialog",
            trigger: "#edit_project_lead,#edit_default_assignee",
            autoClose: true,
            onSuccessfulSubmit: function () {
            }
        });
    });
});
