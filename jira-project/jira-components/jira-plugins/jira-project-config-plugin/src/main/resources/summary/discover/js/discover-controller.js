define('jira-project-config/summary/discover/discover-controller',
    [
        'jira-project-config/summary/discover/discover-view',
        'jira-project-config/marionette',
        'wrm/context-path',
        'jquery',
        'jira/util/logger',
        'jira/analytics'
    ],
    function(
        DiscoverView,
        Marionette,
        contextPath,
        jQuery,
        logger,
        analytics
    ) {
    "use strict";

    var EDIT_WORKFLOW_DISCOVER_DIALOG_SHOWN_USER_PROPERTY = "jira.projectconfig.workflow.edit.summary.discover.dialog";
    var MARK_SEEN = "seen";

    return Marionette.Controller.extend({

            initialize: function(options){
                this.$el = options.$el;
            },

            bindToView: function(){
                this.view = new DiscoverView({el: this.$el});
                this.listenTo(this.view, 'close', this._close.bind(this));
                this.listenTo(this.view, 'learnMore', this._learnMore.bind(this));
            },

            _close: function () {
                this._markDialogShown().done(function () {
                    logger.trace('workflow.edit.summary.discover.dialog.preference.done');
                });

                analytics.send({name: 'jira.projectconfig.summary.editworkflow.discover.close'});
            },

            _markDialogShown: function () {
                return jQuery.ajax({
                    url: contextPath() + "/rest/api/2/mypreferences?key=" + EDIT_WORKFLOW_DISCOVER_DIALOG_SHOWN_USER_PROPERTY,
                    type: "PUT",
                    contentType: "application/json",
                    dataType: "json",
                    data: MARK_SEEN
                });
            },

            _learnMore: function () {
                analytics.send({name: 'jira.projectconfig.summary.editworkflow.discover.learn.more'});
            }
        });
});
