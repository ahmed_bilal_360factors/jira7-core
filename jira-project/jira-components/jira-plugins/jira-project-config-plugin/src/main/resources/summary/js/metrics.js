/**
 * This script measures the loading time of the summary admin page
 */
AJS.toInit(function () {
    require(["internal/browser-metrics"], function (metrics) {
        var SUMMARY_PAGE_KEY = "jira.administration.project.summary";
        // need isInitial true so that the full page load is measured
        metrics.start({
            key: SUMMARY_PAGE_KEY,
            isInitial: true
        });
        metrics.end({key: SUMMARY_PAGE_KEY});
    });
});
