/**
 * @namespace JIRA
 */

/**
 * @namespace JIRA.ProjectConfig
 */

/**
 * @namespace JIRA.ProjectConfig.IssueTypes
 */

/**
 * @namespace JIRA.ProjectConfig.IssueTypes.Entities
 */

/**
 * @namespace JIRA.ProjectConfig.IssueTypes.Layout
 */

/**
 * @namespace JIRA.ProjectConfig.IssueTypes.Navigation
 */

/**
 * @namespace JIRA.ProjectConfig.IssueTypes.Workflow
 */

/**
 * @namespace JIRA.ProjectConfig.IssueTypes.SharedBy
 */
