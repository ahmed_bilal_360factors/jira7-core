module.exports = function (grunt) {
    "use strict";
    var files;
    var tasks;

    files = [
        "main/resources/**/*.js",
        "!main/resources/**/lib/**/*.js"
    ];
    tasks = [
        "grunt-contrib-jshint",
        "grunt-contrib-watch",
        "grunt-jsdoc"
    ];

    grunt.initConfig({
        jsdoc: {
            dist: {
                options: {
                    configure: "etc/jsdoc-configuration.json",
                    destination: "target/jsdoc"
                },
                src: [
                    "etc/jsdoc-namespaces.js",
                    "src/main/resources/global/js/navigate.js",
                    "src/main/resources/issuetypes/**/*.js",
                    "src/main/resources/utilities/**/*.js",
                    "src/main/resources/Application.js",
                    "!src/**/*-test.js"
                ]
            }
        },

        jshint: {
            all: {
                cwd: "src",
                expand: true,
                src: files
            },
            junit: {
                cwd: "src",
                expand: true,
                options: {
                    reporter: "etc/jshint-junit-xml-formatter/jshint-junit-xml-formatter.js",
                    reporterOutput: "jshint-output.xml"
                },
                src: files
            },
            options: {
                jshintrc: "etc/jshint-configuration.json"
            }
        },

        pkg: grunt.file.readJSON("package.json"),

        watch: {
            all: {
                files: files,
                options: {atBegin: true},
                tasks: ["jshint:all", "jsdoc:dist"]
            }
        }
    });

    tasks.forEach(function (task) {
        grunt.loadNpmTasks(task);
    });
};
