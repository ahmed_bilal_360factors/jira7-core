from maven.Maven import MavenCallable
from module import JmakeModule


class Install(JmakeModule):
    def __init__(self):
        super().__init__()
        self.command = 'install'
        self.description = 'Builds JIRA BTF and installs the snapshots in your local repo.'

    def __call__(self, args, executor):
        maven = MavenCallable(args)
        maven.phase('install')
        maven.profile('distribution').property('skip.final.distributions').property('deploy.distribution.base')
        maven.property('skipTests', 'true')
        if args.func_mode_plugins:
            maven.property('func.mode.plugins').property('reference.plugins')
        executor.append(maven)

    def define_parser(self, parser):
        MavenCallable.add_maven_switches(parser)

        parser.add_argument('-d', '--func-mode-plugins',
                            help='Builds JIRA with plugins needed for func tests..',
                            action='store_true',
                            dest='func_mode_plugins')
