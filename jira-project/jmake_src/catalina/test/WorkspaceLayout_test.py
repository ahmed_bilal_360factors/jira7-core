import platform
from unittest import TestCase
import unittest
from catalina.WorkspaceLayout import WorkspaceBuilder

prev_platform_function = platform.system

class TestWorkspaceLayout(TestCase):
    @classmethod
    def tearDownClass(cls):
        platform.system = prev_platform_function

    def test_get_executable_for_tomcat8_on_linux(self):
        layout = self.__getLayout('tcdir', 'tomcat8')
        platform.system = lambda: 'Linux'
        executable = layout.tomcat_executable()
        self.assertEqual(executable, 'tcdir/apache-tomcat-8.5.6-atlassian-hosted/bin/catalina.sh')

    def test_get_executable_for_tomcat8_on_windows(self):
        layout = self.__getLayout('tcdir', 'tomcat8')
        platform.system = lambda: 'Windows'
        executable = layout.tomcat_executable()
        self.assertEqual(executable, 'tcdir/apache-tomcat-8.5.6-atlassian-hosted/bin/catalina.bat')

    def __getLayout(self, tomcatDir, tomcatVersion):
        layout = WorkspaceBuilder.buildLayout(tomcatDir, tomcatVersion, './jirahome', 'work', False)
        layout._WorkspaceLayout__existing_dir = lambda path: path
        return layout

if __name__ == '__main__':
    unittest.main(verbosity=5)
