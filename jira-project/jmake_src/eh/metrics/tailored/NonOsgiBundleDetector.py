from eh.metrics.MetricsCollector import MetricsCollector, MetricsLogger


class NonOsgiBundleDetector(MetricsCollector):

    def __init__(self, metrics_name: str, description: str=None, metrics_logger: MetricsLogger=None):
        super().__init__(metrics_name, description, metrics_logger)
        self.jar_file = None
        self.jar_ok_flag = True

    def pre_files_scan(self, module: str):
        super().pre_files_scan(module)

        self.jar_file = module
        self.jar_ok_flag = False

    def post_files_scan(self, module: str):
        if not self.jar_ok_flag:
            self.hit('Jar %s treated as bundled plugin is neither an OSGI bundle (no instruction in manifest) nor an '
                     'atlassian plugin (no decscriptor)' % self.jar_file)
        self.jar_file = None
        super().post_files_scan(module)

    def wants_file(self, file_name: str):
        if not self.jar_ok_flag:
            file_in_jar = file_name[file_name.find('/'):]

            if file_in_jar == '/atlassian-plugin.xml':
                self.jar_ok_flag = True

            if not self.jar_ok_flag:
                if file_in_jar == '/META-INF/MANIFEST.MF':
                    return True

        return False

    def on_read_line(self, line: str):

        # this is reading the manifest:
        if any(line.startswith(word) for word in ['Export-Package:', 'DynamicImport-Package:', 'Import-Package:']):
            self.jar_ok_flag = True
            return False

        return True

