import json
import os
import re
import types
import operator

from Logger import Logger
from eh.metrics import FALLING
from eh.metrics.MetricsCollector import MetricsCollector, MetricsLogger
from maven.Maven import MavenCallable


class JavadocDoclintProblems(MetricsCollector):
    def __init__(self, metrics_logger: MetricsLogger = None):

        super().__init__('javadoc.doclint', 'Javadoc doclint problems', metrics_logger)
        self.results = {}

        self.file_name = None
        self.problem_text = None
        self.problem_type = None

        self.javadoc_error_pattern = re.compile('\[ERROR\]\s*(.*java):\d+: (error|warning):?( .*)')
        self.error_position_pattern = re.compile('\[ERROR\]\s*\^$')

        # javadoc reports errors on rest docs annotations and some code imports, ignore those
        self.javadoc_ignore_problem_pattern = re.compile('(unknown tag: (?:response|request)\.)|(cannot find symbol import)')

    def get_values(self):

        def process_output(myself, log: Logger, line: str, num: int):
            # error message for a file may span across up to 3 lines, up to 2 first lines are put together to form a hit
            # first line contains the filename, line number and the problem description
            if '[ERROR]' in line:
                # ignore lines with error position marker, not really useful: [ERROR] ^
                if re.match(self.error_position_pattern, line):
                    return self.log.is_trace()

                match = re.match(self.javadoc_error_pattern, line)
                if match:
                    # report the problem accumulated so far (if there is one)
                    self.report_current_problem()

                    # start processing next file error
                    self.file_name = match.group(1)
                    self.problem_type = match.group(2)
                    self.problem_text = match.group(1).replace(os.getcwd() + '/', '') + match.group(3)
                # 2nd line (if there is one) append to the error from 1st line
                elif self.problem_text is not None:
                    self.problem_text += line.replace('[ERROR]', '')

            return self.log.is_trace()

        maven = MavenCallable()
        maven.phase('compile').phase('javadoc:aggregate-jar').profile('deploy-source-jars').profile('dependency-tracking')
        maven.option('-Djira.generateDocs')
        maven.option('-B')
        maven.option('-Dmaven.test.skip')
        maven.option('-Djira.doclint="-Xdoclint:all -Xmaxerrs 100000 -Xmaxwarns 100000"')
        if self.log.is_trace():
            maven.option('-X')
        self.log.info('Generating javadoc to find doclint problems...')
        maven.process_output = types.MethodType(process_output, maven)

        super().pre_files_scan('irrelevant')
        self.run_maven(maven)
        super().post_files_scan('irrelevant')

        self.log.trace(
            json.JSONEncoder(indent=True).encode(sorted(self.results.items(), key=operator.itemgetter(1))))
        self.log.info('Javadoc generation finished.')

        ret = {}
        self.__append(ret, 'errors.count', 'Get rid of javadoc doclint errors', FALLING, False)
        self.__append(ret, 'warnings.count', 'Get rid of javadoc doclint warnings', FALLING, False)
        return ret

    def get_metrics_keys(self):
        return map(lambda k: '%s.%s' % (self.key, k), ['errors.count', 'warnings.count'])

    def report_current_problem(self):

        if self.problem_text is None:
            return

        if re.search(self.javadoc_ignore_problem_pattern, self.problem_text):
            if self.verbose:
                self.log.debug('Ignoring false error reported by javadoc (%s) in %s.' % (self.problem_text, self.file_name))
            return

        if self.problem_type == 'error':
            self.javadoc_error(self.file_name, self.problem_text)
        elif self.problem_type == 'warning':
            self.javadoc_warning(self.file_name, self.problem_text)

    def javadoc_error(self, file, hit):
        self.__increase('errors.count', hit)
        if self.verbose:
            self.log.debug('Found javadoc error (%s) in %s.' % (self.key + 'errors.count', file))

    def javadoc_warning(self, file, hit):
        self.__increase('warnings.count', hit)
        if self.verbose:
            self.log.debug('Found javadoc warning (%s) in %s.' % (self.key + 'warnings.count', file))

    def __increase(self, key, hit):
        n = self.results[key] if key in self.results else 0
        self.results[key] = n + 1
        self.log_hit(hit, self.key + '.' + key)

    def run_maven(self, maven: MavenCallable):
        return maven(Logger().set_none())

    def __append(self, result: dict, key: str, description: str, direction: str, checked: bool=True):
        value = self.results[key] if key in self.results else 0
        result[self.key + '.' + key] = self.produce_result(value, description, checked, direction)

    def pre_files_scan(self, module: str):
        pass

    def post_files_scan(self, module: str):
        pass
