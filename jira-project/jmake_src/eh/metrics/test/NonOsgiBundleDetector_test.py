from unittest import TestCase
from eh.metrics.tailored.NonOsgiBundleDetector import NonOsgiBundleDetector
from utils.Mocks import Mock


class TestNonOsgiBundleDetector(TestCase):

    def test_OsgiBundleDetectorScansManifestsButNotAtlassianPluginXml(self):

        detector = NonOsgiBundleDetector('nonosgi', '', Mock())
        detector.pre_files_scan('mymodule.jar')
        self.assertTrue(detector.wants_file('mymodule/META-INF/MANIFEST.MF'))
        self.assertFalse(detector.wants_file('mymodule/atlassian-plugin.xml'))

    def test_OsgiBundleDetectorMarksModulesWithoutAtlassianPluginXmlAndManifestAsBad(self):

        detector = NonOsgiBundleDetector('nonosgi', '', Mock())
        module = 'mymodule.jar'
        detector.pre_files_scan(module)
        detector.post_files_scan(module)
        self.assertEqual(detector.value, 1)

    def test_OsgiBundleDetectorMarksModulesWithAtlassianPluginXmlAsGoodWithoutReadingTheDescriptor(self):

        detector = NonOsgiBundleDetector('nonosgi', '', Mock())
        module = 'mymodule.jar'
        detector.pre_files_scan(module)
        self.assertFalse(detector.wants_file('mymodule/atlassian-plugin.xml'))
        detector.post_files_scan(module)
        self.assertEqual(detector.value, 0)

    def test_OsgiBundleDetectorWillNotReadManifestIfPluginDescriptorFound(self):

        detector = NonOsgiBundleDetector('nonosgi', '', Mock())
        module = 'mymodule.jar'
        detector.pre_files_scan(module)
        self.assertFalse(detector.wants_file('mymodule/atlassian-plugin.xml'))
        self.assertFalse(detector.wants_file('mymodule/META-INF/MANIFEST.MF'))
        detector.post_files_scan(module)
        self.assertEqual(detector.value, 0)

    def test_OsgiBundleDetectorMarksNonOsgiModulesAsBad(self):

        detector = NonOsgiBundleDetector('nonosgi', '', Mock())
        module = 'mymodule.jar'
        detector.pre_files_scan(module)
        self.assertTrue(detector.wants_file('mymodule/META-INF/MANIFEST.MF'))
        self.assertTrue(detector.on_read_line('Classpath: omg, none!'))
        detector.post_files_scan(module)
        self.assertEqual(detector.value, 1)

    def test_OsgiBundleDetectorMarksOsgiModulesAsGoodAndStopsReadingFile(self):

        for osgi_manifest_entry in ['Import-Package: ', 'Export-Package: ', 'DynamicImport-Package: ']:

            detector = NonOsgiBundleDetector('nonosgi', '', Mock())
            module = 'mymodule.jar'
            detector.pre_files_scan(module)
            self.assertTrue(detector.wants_file('mymodule/META-INF/MANIFEST.MF'))
            self.assertFalse(detector.on_read_line(osgi_manifest_entry + 'com.atlassian.jira.*'),
                             'Encountering "%s" should have stopped file scanning.' % osgi_manifest_entry)
            detector.post_files_scan(module)
            self.assertEqual(detector.value, 0, 'Failed to recognize "%s" as osgi instruction' % osgi_manifest_entry)

