import argparse
import os
import re
from Diagnostics import LocalPortInspector
from Logger import LOG, Logger
from BundledPluginsUtility import BundledPluginsUtility
from callables.Callable import Callable
from callables.SystemCallable import SystemCallable
from catalina.WorkspaceLayout import WorkspaceBuilder, WorkspaceLayout
from maven.Maven import MavenCallable
from maven.PomParser import PomParser
from module import JmakeModule
from catalina.ContextPreparer import ContextPreparer
from catalina.TomcatDownloader import TomcatDownloader
from catalina.TomcatRunner import TomcatStarter, TomcatStopper
from utils.FileUtils import FileUtils
from utils.XmlUtils import XmlUtils
from utils import PathUtils
from utils.WorkspaceUtils import WorkspaceValidator, WorkspaceUtils
from crowd.SSOSeraphConfig import SSOSeraphConfig
from utils.VersionConverter import VersionConverter


DEFAULT_HTTP_PORT = 8090
DEFAULT_SHUTDOWN_PORT = 8099
DEFAULT_H2_PORT = 9092

BUNDLED_PLUGINS = os.sep.join(['jira-components', 'jira-plugins', 'jira-bundled-plugins'])

class Run(JmakeModule):
    def __init__(self, stale_plugins=None):
        super().__init__()

        self.command = 'run'
        self.description = 'Runs JIRA using your code. This will download a tomcat for you, configure it and '\
                           'start JIRA on your local machine. This will not enable you to hotswap, JRebel or debug '\
                           'your running instance.'
        self.stale_plugins = stale_plugins
        self.prevent_post_commands = True

    def get_tasks_before_build_jira(self, args, executor):
        if args.with_workspace:
            executor.append(WorkspaceValidator())
        self.check_ports(args, executor)

        if args.setup_home:
            if args.postgres:
                executor.append(lambda logger: logger.error('Setup home not available for postgresql yet!'))
            elif args.mysql:
                executor.append(lambda logger: logger.error('Setup home not available for mysql yet!'))
            else:
                executor.append(SetupJiraHomeEmbeddedDb(args))
        if args.postgres:
            executor.append(SetupPostgresqlConfig(args))
        elif args.mysql:
            executor.append(SetupMysqlConfig(args))
        elif args.instrumentation:
            executor.append(SetupInstrumentationConfig(args))

        executor.append(SetupDbConfig(args))

    def get_tasks_post_build_jira(self, args, executor):
        if args.crowd_properties_file:
            ssoSeraphConfig = SSOSeraphConfig()
            executor.append(ssoSeraphConfig.validate_properties_file(args.crowd_properties_file))
            executor.append(ssoSeraphConfig.copy_config())

    def __call__(self, args, executor):

        if self.stale_plugins is None:
            self.create_plugins_manager()

        self.build_layouts(args)

        if args.control == 'shutdown':
            executor.append(TomcatStopper(args))
            return
        if args.control == 'restart':
            executor.append(TomcatStopper(args))


        # common preparation part for all commands running JIRA

        self.get_tasks_before_build_jira(args, executor)

        if args.control == 'quickstart':
            if hasattr(args, 'clustered') and args.clustered:
                executor.append(ContextPreparer(args))
            # just do nothing here as quickstart will require only common tasks
        elif args.control == '':
            executor.append(ensure_reloadable_resources)
            # check BUILDENG-4706 to understand why this plugin requires building JIRA twice
            if args.third_party_licensing:
                executor.append(self.__third_party_licensing(args))

            for task in self.get_tasks_to_build_jira(args): executor.append(task)

            executor.append(TomcatDownloader(args))
            executor.append(ContextPreparer(args))

        # now components that are common for all controls


        self.get_tasks_post_build_jira(args, executor)

        executor.append(process_local_test_settings(args))
        executor.append(TomcatStarter(args, self.debug()))

    def create_plugins_manager(self):
        self.stale_plugins = BundledPluginsUtility([BUNDLED_PLUGINS])

    def check_ports(self, args, executor):
        executor.append(LocalPortInspector(args.port, port_name='http port'))

        if args.sh_port == "auto":
            def set_sh_port(port):
                args.sh_port = port

            executor.append(LocalPortInspector(DEFAULT_SHUTDOWN_PORT, port_name='shutdown port').probe(set_sh_port))
        else:
            executor.append(LocalPortInspector(args.sh_port, port_name='shutdown port'))

        if args.h2_port == "auto":
            def set_h2_port(port):
                args.h2_port = port

            executor.append(LocalPortInspector(DEFAULT_H2_PORT, port_name='H2 server port').probe(set_h2_port))
        elif args.h2_port != "disable":
            executor.append(LocalPortInspector(args.h2_port, port_name='H2 server port'))


    def define_parser(self, parser):
        parser.add_argument('-p', '--jira-port', type=int, default=DEFAULT_HTTP_PORT, help='The tomcat port for JIRA', dest='port')
        parser.add_argument('-sp', '--jira-shutdown-port', default='auto',
                            help='The tomcat shutdown port for JIRA (default is "auto" and will be scanned for, starting from %s)' % DEFAULT_SHUTDOWN_PORT,
                            dest='sh_port')
        parser.add_argument('-j', '--jira-home', default=WorkspaceLayout.JIRA_HOME_DEFAULT_MARKER,
                            help='JIRA home directory',
                            dest='jira_home')
        parser.add_argument('-c', '--jira-context', default='/jira', help='JIRA web application context',
                            dest='jira_context')
        parser.add_argument('-w', '--jira-webapp', default=WorkspaceLayout.JIRA_WEBAPP,
                            help='JIRA web application directory', dest='jira_webapp_dir')
        parser.add_argument('-d', '--tomcat-dir', default=WorkspaceLayout.TOMCAT_DOWNLOAD,
                            help='Directory where tomcat will be downloaded', dest='tomcat_dir')
        parser.add_argument('-v', '--tomcat-version', default=WorkspaceBuilder.TOMCAT8,
                            choices=sorted(WorkspaceBuilder.VERSIONS.keys()),
                            help='Version of tomcat to start',
                            dest='tomcat_version')
        parser.add_argument('-bw', '--build-war', action='store_true',
                            help='If this flag is set jira.war will be produced otherwise only exploded directory is assembled',
                            dest='build_war')
        parser.add_argument('-ws', '--with-workspace', action='store_true',
                            help='If this flag is set - build in workspace mode. '
                                 'Maven will be run in parent of jira directory. '
                                 'It is assumed there is some pom.xml there.',
                            dest='with_workspace')
        parser.add_argument('-as', '--attach-sources', action='store_true',
                            help='If this flag is set build will generate sources jar', dest='attach_sources')
        parser.add_argument('-jr', '--jrebel', action='store_true',
                            help='If this flag is set tomcat will be started with jrebel agent', dest='jrebel')
        parser.add_argument('-rp', '--ref-plugin', action='store_true',
                            help='If this flag is set reference plugins will be deployed with JIRA', dest='ref_plugins')
        parser.add_argument('-tp', '--third-party-licensing', action='store_true',
                            help='If this flag is set, the LGPL list in the about page will be created',
                            dest='third_party_licensing')
        parser.add_argument('-in', '--instance-name',
                            help='this will name your instance, which means you will be able to '
                                 'run and maintain multiple ones on different ports', dest='instance_name',
                            default='work')
        parser.add_argument('control', choices=['shutdown', 'restart', 'quickstart', ''], default='', nargs='?',
                            help='Controls the instance:\n' +
                                 'shutdown shuts the running instance, respects instance names.'
                                 'restart restarts the running instance.' +
                                 'quickstart starts the instance without building it.')
        parser.add_argument('--override-dev-mode', dest='override_dev_mode', action='store_true',
                            help='JIRA will be started in dev mode by default. this overrides the dev_mode setting.')
        parser.add_argument('--hard-mode', dest='hardmode', action='store_true',
                            help='JIRA will be started with additional restrictions which will break code that is not written in a specific way.')
        parser.add_argument('-J', dest='extra_jvm_flags', action='append',
                            help='Pass flag directly to the tomcat JVM. ' +
                                 'Flags are appended to the end of other JVM args, so you can (for example) '
                                 'override memory settings with -J-Xmx2g .')
        parser.add_argument('--max-metaspace-size', dest='max_metaspace_size', type=str, default='256m',
                            help='Amount of Metaspace memory for the tomcat process')
        parser.add_argument('--xmx', dest='xmx', type=str, default='1024m',
            help='Maximum memory setting.')

        parser.add_argument('--plugin-resources', dest='plugin_resources', type=str, default='',
            help='Add all resource sub-folders for the plugin to resources scanned for changes by JIRA')

        parser.add_argument('--disable-plugin-resources', dest='disable_plugin_resources', action='store_true',
            help='Do not use reloadable plugin resources (good for standalone-like testing)')

        parser.add_argument('--enable-mail', dest='enable_mail', action='store_true',
                            help='Start JIRA with the mail handlers enabled even if in dev mode.')
        parser.add_argument('-sh', '--setup-home', dest='setup_home', action='store_true',
                            help='Setup JIRA home directory so you can directly login as admin:admin. '
                                 'If current JIRA home directory is not empty this parameter has no effect')

        parser.add_argument('--postgres', dest='postgres', action='store_true',
                            help='Sets up JIRA with local postgresql db. See ./jmake postgres --help.')

        parser.add_argument('--instrumentation', dest='instrumentation', action='store_true',
                            help='Sets up JIRA with local instrumentation proxy. See ./jmake instrumentation --help.')
        parser.add_argument('--mock-tomcat', dest='mock_tomcat', action='store_true',
                            help="Sets up JIRA from clean state in a way that tomcat won't actually run, used for "
                                 "testing")
        parser.add_argument('--mysql', dest='mysql', action='store_true',
                            help='Sets up JIRA with local mysql db. See ./jmake mysql --help.')

        parser.add_argument('--ssl', dest='ssl', action='store_true',
                            help='Sets up JIRA with ssl. You also need a key produced with java keytool. Bother lukasz w. to '
                                 'add more how-to here.')

        parser.add_argument('--enable-setup-analytics', dest='setup_analytics', action='store_true',
                            help='Enables setup analytics being sent. Warning: may conflict with setting up JIRA using webdriver!')
        parser.add_argument('--skip-onboarding', dest='skip_onboarding', action='store_true',
                            help='Disables onboarding experience for new JIRA users. The initial admin that comes with -sh option '
                                 'will not trigger onboarding flow ever (if it does, raise an issue). If you manually setup JIRA, '
                                 'your admin user will go through onboarding unless you use this flag.')
        parser.add_argument('--enable-websudo', dest='enable_websudo', action='store_true',
                            help='Enables websudo even if in dev mode.')

        parser.add_argument('--software', '-sw',  nargs='?', const='HELP', default=None, type=valid_jira_software_version,
                            help='Runs composition of JIRA with JIRA Software. '
                                 'If JIRA_SOFTWARE_VERSION is not specified latest released version is used.',
                            dest='jira_software_version')
        parser.add_argument('--servicedesk', '-sd',  nargs='?', const='HELP', default=None, type=valid_jira_servicedesk_version,
                            help='Runs composition of JIRA with JIRA Service Desk. '
                                 'If JIRA_SERVICEDESK_VERSION is not specified latest released version is used.',
                            dest='jira_servicedesk_version')

        parser.add_argument('-h2p', '--h2-port', default='disable',
                            help='Embedded H2 server. Default is "disable". Set to "auto" to scan for a free port, starting at %s. Ignored if existing dbconfig.xml does not contain an H2 server JDBC URL' % DEFAULT_H2_PORT,
                            dest='h2_port')
        parser.add_argument('--full-plugin-system', dest='full_plugin_system', action='store_true',
                            help='Enables the mode where no hacks are made on the plugin system for devspeed. '
                                 'Useful to debug the plugin system.')

        parser.add_argument('-ph', '--perf-header', dest='perf_header_plugin', action='store_true',
                            help='Enable performance header plugin')

        parser.add_argument('-sso', '--enable-sso', nargs='?', const=os.sep.join(['.', 'jmake_src', 'data', 'sso', 'crowd.properties']), default=None, type=str, dest='crowd_properties_file',
                            help='REQUIRES CROWD TO BE RUNNING. '
                                 'Enables Single Sign On by using crowd properties file. File location can be changed by argument value. '
                                 'When not specified uses jmake_src/data/sso/crowd.properties. '
                                 'The default properties file assumes that you have Crowd running under http://localhost:8095/crowd/. '
                                 'The Crowd should have JIRA appliaction setup with name "jira" and password "foobar". '
                                 'For details see: https://confluence.atlassian.com/display/CROWD/Integrating+Crowd+with+Atlassian+JIRA')

        self.sample_usage(parser, 'To pass more than one argument with -J use:', [
            './jmake run -J-Dfoo=bar -J-Dfee=baz',
            './jmake run -J "-Dfoo=bar -Dfee=baz"'])

        self.sample_usage(parser, 'To skip the setup steps on clean jirahome using hsqldb (will not work with real db):', [
            './jmake run --setup-home',
            './jmake debug --setup-home'
        ])

        MavenCallable.add_maven_switches(parser)

    def build_layouts(self, args):
        args.layout = WorkspaceBuilder.buildLayout(args.tomcat_dir, args.tomcat_version, args.jira_home,
                                                   args.instance_name, False)

    def debug(self):
        return False

    def get_tasks_to_build_jira(self, args, workspace_utils=WorkspaceUtils()):
        #for workspace build maven must be executed from parent directory
        maven = MavenCallable(args)
        maven.phase('package')

        if not args.build_war:
            maven.property('jira.do.not.prepare.war')
        maven.property('exclude.test.sources')
        maven.property('maven.test.skip', 'true')
        maven.property('jira.lesscss.compile.skip')
        maven.property('jira.minify.skip')
        maven.property('jira.precompile.jsp.skip')

        maven.project('jira-components/jira-webapp').option('-am')
        maven.property('jira.home', args.layout.jira_home())
        maven.can_run_in_parallel()
        maven.projects.extend(workspace_utils.get_workspace_projects_without_jira(args))
        self.add_plugins_maven_goals(args, maven)
        if not args.attach_sources:
            maven.property('skipSources')
        maven.skip_tests()
        if args.third_party_licensing:
            maven.profiles.extend(['third-party-licensing'])

        # return builder and the bundled plugins
        return [prepare_applications(args), maven, self.stale_plugins.get_plugins_task(maven.profiles),
               lambda logger: self.stale_plugins.create_agregate_list()]

    def __third_party_licensing(self, args):

        licensing_maven_executable = MavenCallable(args)
        licensing_maven_executable.phase('verify').phases.extend(['license:bom', 'license:download'])
        licensing_maven_executable.profiles.extend(['distribution', '!build-source-distribution'])
        licensing_maven_executable.property('maven.test.skip', 'true')
        return licensing_maven_executable

    def add_devmode_plugins(self, args, maven):
        profiles_for_plugins = set()
        if not args.override_dev_mode:
            profiles_for_plugins.update({'func-mode-plugins', 'pseudo-loc', 'dev-mode-plugins'})
        if args.ref_plugins:
            profiles_for_plugins.add('reference-plugins')
        if args.perf_header_plugin:
            profiles_for_plugins.add('performance-header-plugin')
        profiles_for_plugins.update({p for p in maven.profiles})
        plugin_list = list(profiles_for_plugins)
        maven.profiles = plugin_list
        return plugin_list

    def add_plugins_maven_goals(self, args, maven):
        profiles_for_plugins = self.add_devmode_plugins(args, maven)

        if args.full_plugin_system:
            maven.property('jira.dev.full.plugin.system')
        else:
            maven.property('jira.exclude.bundled.plugins')

        if args.mvn_clean:
            LOG.info('Clean parameter present recompiling bundled plugins')
            modules_to_build = self.stale_plugins.get_bundled_plugins_modules()
        else:
            modules_to_build = self.stale_plugins.find_plugins_to_recompile(profiles_for_plugins)

        for module in modules_to_build:
            maven.project(module)

        if len(modules_to_build) == 0:
            LOG.info('Bundled plugins up to date skipping compilation')


def process_local_test_settings(args, fileutils=FileUtils()):
    def process_local_test_settings_closure(logger):
        logger.info('Preparing local test settings for your new instance...')
        template = os.sep.join(['jira-functional-tests', 'jira-func-tests', 'src', 'main', 'resources', 'localtest.template'])

        template_renderings = {
            os.sep.join(['jira-functional-tests','jira-func-tests']): os.sep.join(['jira-functional-tests','jira-func-tests']),
            os.sep.join(['jira-functional-tests','jira-embedded-crowd-func-tests']): os.sep.join(['jira-functional-tests','jira-embedded-crowd-func-tests']),
            os.sep.join(['jira-functional-tests','jira-webdriver-tests']): os.sep.join(['jira-functional-tests','jira-webdriver-tests']),
            os.sep.join(['jira-distribution', 'jira-integration-tests']): os.sep.join(['jira-functional-tests','jira-func-tests'])}

        for project, xml_location in template_renderings.items():
            dir = fileutils.existing_dir(os.sep.join(['.', project, 'src', 'main', 'resources']))
            dest = os.sep.join([dir, 'localtest.properties'])

            # just for unit tests this settings dict is not reused:
            settings = {'${jira.port}': str(args.port),
                        '${jira.context}': args.jira_context,
                        '${test.xml.location}': PathUtils.abspath(xml_location)}

            logger.debug('Processing ' + template + ' to ' + dest)
            fileutils.filter_file(template, dest, settings)
        return Callable.success

    return process_local_test_settings_closure


def ensure_reloadable_resources(logger, xml=XmlUtils()):
    artifact_file = os.sep.join(['.idea', 'artifacts', 'atlassian_jira_webapp_common_war_exploded.xml'])
    try:
        tree = xml.parse(artifact_file)
    except IOError:
        return Callable.success

    webinf_element = xml.produce(tree.getroot(),
                                 ('artifact', {}),
                                 ('root', {}),
                                 ('element', {'id': 'directory', 'name': 'WEB-INF'}),
                                 ('element', {'id': 'directory', 'name': 'classes'}))

    modules = ['jira-api', 'jira-core']
    needs_save = False

    for module in modules:
        if not xml.child_exists(webinf_element, 'element', {'name': module}):
            xml.produce(webinf_element, ('element', {'id': 'module-output', 'name': module}))
            needs_save = True

    if needs_save:
        logger.debug('Updating JIRA artifact to enable resources reload...')
        try:
            tree.write(artifact_file)
        except IOError:
            logger.error('Could not save ' + artifact_file)
            return Callable.success - 2

    return Callable.success


def prepare_applications(args, fs: FileUtils=FileUtils()):

    template_file = os.sep.join(['.', 'jmake_src', 'data', 'download_and_unpack_application.xml'])
    pom_location = fs.abs_path(os.sep.join(['.', 'target', 'applications']))

    def toDependency(groupId, artifactId, version):
        return """
<dependency>
<groupId>%s</groupId>
<artifactId>%s</artifactId>
<version>%s</version>
<type>obr</type>
</dependency>
""" % (groupId, artifactId, version)

    def applications_plugins_closure(logger):
        logger.info('Downloading applications...')

        applications = []
        if args.jira_software_version is not None:
            applications.append(('com.atlassian.jira', 'jira-software-application', args.jira_software_version))
        if args.jira_servicedesk_version is not None:
            applications.append(('com.atlassian.servicedesk', 'jira-servicedesk-application', args.jira_servicedesk_version))

        fs.filter_file(template_file,
                       os.sep.join([fs.existing_dir(pom_location), 'pom.xml']),
                       {'${DEPENDENCY_ITEMS}': ''.join(
                           toDependency(groupId, artifactId, version) for groupId, artifactId, version in applications)})

        if not applications:
            args.applications_psd = None
            return 0
        else:
            rc = MavenCallable(path=pom_location).phase('versions:resolve-ranges').option('-B')(logger)
            if (rc != 0):
                return rc
            rc = MavenCallable(path=pom_location).phase('clean').phase('verify').option('-B')(logger)
            args.applications_psd = os.sep.join([pom_location, 'target', 'jars'])
            return rc

    return applications_plugins_closure


def assert_maven_version(string):
    if string != 'RELEASE' and string != 'LATEST' and not re.match('[\d[(*]', string[0]):
        msg = "%r is not a correct version of maven artifact. " \
              "If you have specified positional parameter (like quickstart) please try put it before this one." % string
        raise argparse.ArgumentTypeError(msg)


class SystemCallableOutput(Logger):
    def __init__(self):
        super().__init__(level=Logger.L_TRACE)
        self.lines = []

    def trace(self, line):
        self.lines.append(line.replace('\n', ''))

    def get_lines(self):
        return self.lines


def get_jira_version():
    mvn = MavenCallable()
    mvn.with_logged_output = True
    mvn.log_stderr = False
    mvn.phase('exec:exec')
    mvn.option('-q').options.extend(['--non-recursive', '-f ./pom.xml'])
    mvn.property('exec.executable', 'echo')
    mvn.property('exec.args', '\'${project.version}\'')

    version_output = SystemCallableOutput()
    mvn(version_output)
    return version_output.get_lines()[0]


def get_application_versions(git_repo, version_pattern):
    def get_application_refs(git_repo):
        command = "git ls-remote {0}".format(git_repo)
        versions_output = SystemCallableOutput()
        app_version_finder = SystemCallable([], command)
        app_version_finder.with_logged_output = True
        app_version_finder(versions_output)
        return versions_output.get_lines()

    refs = get_application_refs(git_repo)
    versions = []
    for line in refs:
        match = re.search(r"refs[/]tags[/]({0})".format(version_pattern), line)
        if match is not None:
            versions.append(match.group(1))

    return sorted(list(set(versions)))


def valid_jira_software_version(string):
    if string == 'AUTO':
        string = '[,1000)'
    if string == 'HELP':
        software_repo = 'ssh://git@stash.atlassian.com:7997/jswserver/jira-software-application.git'
        version_pattern = VersionConverter('{a}.{b}').getOutVersion(get_jira_version()).replace('.', '\.') \
                          + '([.][0-9])*[0-9a-zA-Z\-]*'
        msg = 'You need to specify JIRA Software version (e.g. ./jmake -sw 7.1.0).\n' \
              'I found some versions for you:\n' + \
              ' '.join(get_application_versions(software_repo, version_pattern))
        raise argparse.ArgumentTypeError(msg)
    assert_maven_version(string)
    return string


def valid_jira_servicedesk_version(string):
    if string == 'AUTO':
        string = '[,1000)'
    if string == 'HELP':
        servicedesk_repo = 'ssh://git@stash.atlassian.com:7997/sdserver/servicedesk.git'
        version_pattern = VersionConverter('{a-4}.{b}').getOutVersion(get_jira_version()).replace('.', '\.') \
                          + '([.][0-9])*[0-9a-zA-Z\-]*'
        msg = 'You need to specify JIRA Servicedesk version (e.g. ./jmake -sd 3.1.0).\n' \
              'I found some versions for you:\n' + \
              ' '.join(get_application_versions(servicedesk_repo, version_pattern))
        raise argparse.ArgumentTypeError(msg)
    assert_maven_version(string)
    return string


class SetupJiraHomeEmbeddedDb:
    JIRA_HOME_TEMPLATE = os.sep.join(['jmake_src', 'data', 'jirahome'])
    DB_CONFIG_TEMPLATE = 'dbconfig-template.xml'
    DB_CONFIG = 'dbconfig.xml'
    DB_EXPORT = 'h2db.sql'
    DB_SRC_DIRECTORY = os.sep.join(['..', 'jira-components', 'jira-plugins', 'jira-plugin-test-resources', 'src', 'main', 'resources', 'jira-home', 'database'])
    DB_DIRECTORY = 'database'
    DB_VERSION_PROPERTY = 'h2.jdbc.version'
    DB_GROUP_ID = ['com', 'h2database']
    DB_ARTIFACT_ID = 'h2'
    PREFIX_SERVER = 'jdbc:h2:tcp://localhost:%s/'
    PREFIX_FILE = 'jdbc:h2:file:'

    def __init__(self, args, file_utils: FileUtils=FileUtils(), xml_utils: XmlUtils=XmlUtils(), pom_parser: PomParser=PomParser(), system_callable=None):
        super().__init__()
        self.args = args
        self.fs = file_utils
        self.xml = xml_utils
        self.pom = pom_parser
        self.sys = SystemCallable(args) if system_callable is None else system_callable

    def __call__(self, logger):
        jira_home = self.args.layout.jira_home()
        if self.fs.file_exists(jira_home):
            logger.warn('Directory "%s" already exists, leaving as it is. '
                        'If you want clean instance please remove "%s".' % (jira_home, jira_home))
        else:
            logger.info('Copying %s to %s' % (SetupJiraHomeEmbeddedDb.JIRA_HOME_TEMPLATE, jira_home))

            db_export_src_path = os.sep.join([jira_home, SetupJiraHomeEmbeddedDb.DB_SRC_DIRECTORY, SetupJiraHomeEmbeddedDb.DB_EXPORT])
            db_export_path = os.sep.join([jira_home, SetupJiraHomeEmbeddedDb.DB_DIRECTORY, SetupJiraHomeEmbeddedDb.DB_EXPORT])
            db_config_template = os.sep.join([jira_home, SetupJiraHomeEmbeddedDb.DB_CONFIG_TEMPLATE])
            db_config = os.sep.join([jira_home, SetupJiraHomeEmbeddedDb.DB_CONFIG])

            # copy jirahome
            self.fs.copy_tree(SetupJiraHomeEmbeddedDb.JIRA_HOME_TEMPLATE, jira_home)

            # extract url template from dbconfig-template.xml
            db_config_dom = self.xml.parse(db_config_template)
            db_url_template = self.xml.produce(db_config_dom.getroot(), ('jdbc-datasource', {}), ('url', {})).text
            if db_url_template is None:
                logger.error('could not find <url> in ' + db_config)
                return Callable.failure

            # filter dbconfig-template.xml to dbconfig.xml and remove
            if self.args.h2_port == "disable":
                prefix = SetupJiraHomeEmbeddedDb.PREFIX_FILE
            else:
                prefix = SetupJiraHomeEmbeddedDb.PREFIX_SERVER % self.args.h2_port
            self.fs.filter_file(db_config_template, db_config, {'${jirahome}': jira_home, '${prefix}': prefix})
            self.fs.remove(db_config_template)

            # filter h2db.sql from jira-plugin-test-resources to jirahome/database/h2db.sql, replacing port
            self.fs.filter_file(db_export_src_path, db_export_path, {'localhost:2990': 'localhost:%i' % DEFAULT_HTTP_PORT})

            # extract H2 version from root pom.xml
            db_version = self.pom.parse('pom.xml').get_property(SetupJiraHomeEmbeddedDb.DB_VERSION_PROPERTY)
            if db_version is None:
                logger.error('could not find property ' + SetupJiraHomeEmbeddedDb.DB_VERSION_PROPERTY + ' in pom.xml')
                return Callable.failure

            # retrieve H2 jar via maven if necessary
            db_jar = os.sep.join(
                [os.path.expanduser('~'), '.m2', 'repository', os.sep.join(SetupJiraHomeEmbeddedDb.DB_GROUP_ID),
                 SetupJiraHomeEmbeddedDb.DB_ARTIFACT_ID, db_version,
                 SetupJiraHomeEmbeddedDb.DB_ARTIFACT_ID + '-' + db_version + '.jar'])
            if not self.fs.file_exists(db_jar):
                maven = MavenCallable(self.args)
                maven.phase('dependency:get')
                maven.option('-Dartifact=' + '.'.join(SetupJiraHomeEmbeddedDb.DB_GROUP_ID) + ':' + SetupJiraHomeEmbeddedDb.DB_ARTIFACT_ID + ':' + db_version)
                maven(logger)
                if maven.returncode != Callable.success:
                    return maven.returncode

            # import h2db.sql and remove
            db_url_file = db_url_template.replace('${jirahome}', jira_home).replace('${prefix}', SetupJiraHomeEmbeddedDb.PREFIX_FILE)
            self.sys.command('java -cp "%s" org.h2.tools.RunScript -url "%s" -script "%s"' % (db_jar, db_url_file, db_export_path))
            self.sys(logger)
            if self.sys.returncode != Callable.success:
                return self.sys.returncode
            self.fs.remove(db_export_path)

        return Callable.success


class SetupDbConfig:
    DB_CONFIG = 'dbconfig.xml'
    PREFIX_SERVER = 'jdbc:h2:tcp://localhost:'

    def __init__(self, args, file_utils: FileUtils=FileUtils()):
        super().__init__()
        self.args = args
        self.fs = file_utils

    def __call__(self, logger):
        db_config_file = os.sep.join([self.args.layout.jira_home(), SetupDbConfig.DB_CONFIG])
        if self.fs.file_exists(db_config_file):
            url_current = self.fs.search(db_config_file, '%s\d+' % SetupDbConfig.PREFIX_SERVER)

            if self.args.h2_port == "disable":
                if url_current:

                    # find an H2 port and use it if dbconfig.xml is in server mode
                    logger.warn('enabling H2 server as %s contains an H2 server URL' % SetupDbConfig.DB_CONFIG)
                    def set_h2_port(port):
                        self.args.h2_port = port
                    LocalPortInspector(DEFAULT_H2_PORT, port_name='H2 server port').probe(set_h2_port)(logger)

            if self.args.h2_port != "disable":
                if url_current:

                    # update the H2 URL if the port is different
                    url_desired = '%s%s' % (SetupDbConfig.PREFIX_SERVER, self.args.h2_port)
                    if url_current != url_desired:
                        logger.info('updating "%s" to "%s" in %s' % (url_current, url_desired, SetupDbConfig.DB_CONFIG))
                        db_config_file_new = db_config_file + '.updated'
                        self.fs.filter_file(db_config_file, db_config_file_new, {url_current: url_desired})
                        os.renames(db_config_file_new, db_config_file)
                else:

                    # disable H2 server if existing dbconfig.xml doesn't point to server
                    logger.warn('disabling H2 server as %s does not contain an H2 server URL' % SetupDbConfig.DB_CONFIG)
                    self.args.h2_port = 'disable'

        return Callable.success


class SetupPostgresqlConfig:

    DB_CONFIG = 'dbconfig.xml'
    DB_CONFIG_TEMPLATE = os.sep.join(['.', 'jmake_src', 'data', 'dbconfig-postgresql.xml'])

    def __init__(self, args, file_utils: FileUtils=FileUtils()):
        super().__init__()
        self.args = args
        self.fs = file_utils

    def __call__(self, logger):
        jira_home = self.args.layout.jira_home()
        logger.info('Preparing postgresql configuration...')
        self.fs.copy_file(SetupPostgresqlConfig.DB_CONFIG_TEMPLATE,
                          os.sep.join([self.fs.existing_dir(jira_home), SetupJiraHomeEmbeddedDb.DB_CONFIG]))
        return Callable.success


class SetupInstrumentationConfig:

    DB_CONFIG = 'dbconfig.xml'
    DB_CONFIG_TEMPLATE = os.sep.join(['.', 'jmake_src', 'data', 'dbconfig-instrumentation.xml'])

    def __init__(self, args, file_utils: FileUtils=FileUtils()):
        super().__init__()
        self.args = args
        self.fs = file_utils

    def __call__(self, logger):
        jira_home = self.args.layout.jira_home()
        logger.info('Preparing instrumentation proxy configuration...')
        self.fs.copy_file(SetupInstrumentationConfig.DB_CONFIG_TEMPLATE,
                          os.sep.join([self.fs.existing_dir(jira_home), SetupJiraHomeEmbeddedDb.DB_CONFIG]))
        return Callable.success

class SetupMysqlConfig:

    DB_CONFIG = 'dbconfig.xml'
    DB_CONFIG_TEMPLATE = os.sep.join(['.', 'jmake_src', 'data', 'dbconfig-mysql.xml'])

    def __init__(self, args, file_utils: FileUtils=FileUtils()):
        super().__init__()
        self.args = args
        self.fs = file_utils

    def __call__(self, logger):
        jira_home = self.args.layout.jira_home()
        logger.info('Preparing mysql configuration...')
        self.fs.copy_file(SetupMysqlConfig.DB_CONFIG_TEMPLATE,
                          os.sep.join([self.fs.existing_dir(jira_home), SetupJiraHomeEmbeddedDb.DB_CONFIG]))
        return Callable.success

