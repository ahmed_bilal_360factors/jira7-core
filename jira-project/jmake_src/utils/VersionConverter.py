import re

class VersionConverter:
    globalVariables = {}

    def __init__(self, outRule):
        self.rule = outRule
        self.pattern = re.compile('\{([^}]+)}|(.)')

    def registerGlobal(self, key, value):
        self.globalVariables[key] = value

    def getOutVersion(self, inVersion):

        context = dict(self.globalVariables)

        for index, number in enumerate(re.findall('\d+', inVersion)):
            symbol = chr(ord('a') + index)
            exec(symbol + ' = ' + number, context)

        return ''.join((self.__evaluate_match(match, context) for match in self.pattern.finditer(self.rule)))

    def __evaluate_match(self, matchObject, context):
        if matchObject.group(1) is not None:
            return str(eval(matchObject.group(1), context))
        else:
            return matchObject.group(2)