import subprocess

class ProcessUtils:
    def __init__(self):
        super().__init__()

    def check_output(self, *popenargs, **kwargs):
        return subprocess.check_output(*popenargs, **kwargs)

    def execute_read_input(self, args, input, stderr=None):
        p = subprocess.Popen(
            args, stdout=subprocess.PIPE,
            stdin=subprocess.PIPE, stderr=subprocess.STDOUT if stderr is None else stderr
        )

        return p.communicate(input=input)[0]