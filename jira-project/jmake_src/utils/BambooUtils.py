import re
from utils.UrlUtils import UrlUtils

class BambooUtils(object):

    def __init__(self, url: UrlUtils=UrlUtils()):
        super().__init__()
        self.url = url

    def __decode_build_number(self, build_number:str):
        if re.match(r'.*-.*-.*-\d+', build_number):
            return build_number.rsplit('-', 3) # PROJECT-PLAN-JOB-NUM
        else:
            raise Exception('Could not decode build number: ' + build_number)

    def get_artifact_url(self, build_number, artifact_name, with_s3_redirect=False):
        return self.get_s3_artifact_url(build_number, artifact_name) if with_s3_redirect else self.get_server_artifact_url(build_number, artifact_name)


    def get_s3_artifact_url(self, build_number, artifact_name):
        project, plan, job, number = self.__decode_build_number(build_number)

        return '/build/artifactUrlRedirect.action?' + self.url.as_url_params({
            'planKey': '-'.join((project, plan, job)),
            'buildNumber': number,
            'artifactName': artifact_name
        })

    def get_server_artifact_url(self, build_number, artifact_name):
        project, plan, job, number = self.__decode_build_number(build_number)

        return '/browse/%s/artifact/%s/%s/%s' % ('-'.join([project, plan, number]), job, artifact_name, artifact_name)

