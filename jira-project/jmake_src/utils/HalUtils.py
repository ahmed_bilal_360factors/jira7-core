from json import JSONDecoder
from time import time, sleep
from urllib.error import HTTPError

from crowd.JBAC import CrowdAuthentication
from utils.UrlUtils import UrlUtils

RUNNING = 'RUNNING'
ASSIGNING = 'ASSIGNING'
DESTROYED = 'DESTROYED'
DESTROYING = 'DESTROYING'
STOPPING = 'STOPPING'
STOPPED = 'STOPPED'

hal_instance_creation_options = {
    'installOnDemand': 'true',
    'adminUsername': 'admin',
    'adminPasswordHash': 'admin',
    'env': 'dev',
    'timeBombActive': 'true'
}
hal_instance_creation_options_raw = '&'.join(('%s=%s' % (k, v) for k, v in sorted(hal_instance_creation_options.items())))


class HalRestClient:
    def __init__(self, base_url: str=None, url_utils: UrlUtils = UrlUtils()):
        self.url_utils = url_utils
        self.base_url = base_url if base_url is not None else 'https://hal.stg.internal.atlassian.com/1.0/'
        self.auth = None

    def instance_details(self, instance_hostname: str):
        return self.__get(self.base_url + 'instance/' + instance_hostname)

    def create_container(self, instance_hostname: str):
        return self.__post(self.base_url + 'instance/' + instance_hostname, hal_instance_creation_options_raw)

    def __get(self, url):
        request_json_response = {'Accept': 'application/json'}
        try:
            response = self.url_utils.get(url, self.__get_auth().login, self.__get_auth().password, extra_headers=request_json_response)
            return JSONDecoder().decode(response)
        except HTTPError as e:
            return self.__handle_http_error(e)

    def __post(self, url, data):
        try:
            return self.url_utils.post(url, self.__get_auth().login, self.__get_auth().password, data)
        except HTTPError as e:
            return self.__handle_http_error(e)

    @staticmethod
    def __handle_http_error(e: HTTPError):
        if e.code == 401:
            raise Exception('Invalid username or password')
        if e.code == 404:
            return None
        raise Exception(e)

    def __get_auth(self):
        if self.auth is None:
            self.auth = CrowdAuthentication.get()
        return self.auth


class HalInstanceState:
    def __init__(self, instance_hostname, rest_client: HalRestClient = None):
        super().__init__()
        self.instance_hostname = instance_hostname
        self.instance_url = 'https://' + self.instance_hostname
        self.rest_client = rest_client if rest_client is not None else HalRestClient()

        self.exists = False
        self.state = None
        self.zone = None
        self.environment = None
        self.raw_data = None

        self.__get_instance_data()

    @staticmethod
    def for_new_instance(instance_hostname, rest_client:HalRestClient = None):
        rest_client = rest_client if rest_client is not None else HalRestClient()
        rest_client.create_container(instance_hostname)
        return HalInstanceState(instance_hostname, rest_client=rest_client)

    def __get_instance_data(self):
        details_data = self.rest_client.instance_details(self.instance_hostname)
        self.raw_data = details_data
        self.exists = details_data is not None
        if self.exists:
            self.state = details_data['status']
            self.zone = details_data['zone']
            self.environment = details_data['environment']
            # anything else that we need?

    def poll_state(self):
        return HalInstanceState(self.instance_hostname, rest_client=self.rest_client)

    def wait_until_in_state(self, state: str, timeout_seconds: int=300, sleep_fun=lambda: sleep(5), time_fun=time):
        deadline = time_fun() + timeout_seconds
        instance = self
        while time_fun() < deadline:
            current_state = instance.state
            instance = self.poll_state()
            if instance.state != current_state:
                print('Instance just changed state to: ' + instance.state)
            if instance.state == state:
                return instance, True
            sleep_fun()
        return instance, False

    def __str__(self):
        if self.exists:
            return '%s: %s instance in %s, currently: %s' % (self.instance_hostname, self.environment, self.zone, self.state)
        else:
            return '%s: offline' % self.instance_hostname


class InstanceNotApplicableForDev(Exception):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class HalUtils:
    def __init__(self, rest_client: HalRestClient = None):
        super().__init__()
        self.rest_client = rest_client

    def prepare_instance_for_debug(self, instance_hostname:str):
        instance = self.get_hal_instance(instance_hostname)
        if instance.exists:
            if instance.environment != 'dev':
                raise InstanceNotApplicableForDev('environment: ' + instance.environment)
            return instance
        else:
            return self.create_hal_instance(instance_hostname)

    def get_hal_instance(self, instance_name: str):
        return HalInstanceState(instance_name, rest_client=self.rest_client)

    def create_hal_instance(self, instance_hostname: str):
        return HalInstanceState.for_new_instance(instance_hostname, rest_client=self.rest_client)
