from unittest import TestCase
import unittest
from urllib.error import HTTPError

from utils.HalUtils import HalUtils, HalRestClient, hal_instance_creation_options_raw, RUNNING, HalInstanceState, \
    ASSIGNING, InstanceNotApplicableForDev
from utils.Mocks import Mock

base_url = 'https://localhost:1234/'
testy_jira_dev_com = 'testy.jira-dev.com'

login = 'login'
password = 'password'

class TestHalRestClient(TestCase):

    def setUp(self):
        self.url = Mock()

    def test_check_instance_state(self):
        self.url.expect_get('https://localhost:1234/instance/' + testy_jira_dev_com,
                            login,
                            password,
                            extra_headers={'Accept': 'application/json'},
                            toReturn='{"response": "success"}')
        hal_client = HalRestClient(base_url=base_url, url_utils=self.url)
        hal_client.auth = Mock(login = login, password = password)
        self.assertDictEqual({'response': 'success'}, hal_client.instance_details(testy_jira_dev_com))

    def test_create_container(self):
        self.url.expect_post('https://localhost:1234/instance/' + testy_jira_dev_com,
                            login,
                            password,
                             hal_instance_creation_options_raw,
                             toReturn={'response': 'success'})
        hal_client = HalRestClient(base_url=base_url, url_utils=self.url)
        hal_client.auth = Mock(login = login, password = password)
        self.assertDictEqual({'response': 'success'}, hal_client.create_container(testy_jira_dev_com))

    def test_check_instance_state_404(self):

        def throwAtGet():
            e = HTTPError('anyurl', 404, 'Resource does not exist', None, None)
            raise e

        self.url.get = lambda *args, **kwargs: throwAtGet()
        hal_client = HalRestClient(base_url=base_url, url_utils=self.url)
        hal_client.auth = Mock(login = login, password = password)

        self.assertIsNone(hal_client.instance_details(testy_jira_dev_com))


class TestHalInstanceState(TestCase):

    def setUp(self):
        self.hal_rest_client = Mock()

    def test_get_instance_state(self):
        instance_details = {
            'status': RUNNING,
            'zone': 'sc2',
            'environment': 'dev'
        }
        self.hal_rest_client.expect_instance_details(testy_jira_dev_com, toReturn=instance_details)
        instance = HalInstanceState(testy_jira_dev_com, self.hal_rest_client)

        self.assertTrue(instance.exists)
        self.assertEqual(RUNNING, instance.state)
        self.assertEqual('sc2', instance.zone)
        self.assertEqual('dev', instance.environment)

    def test_get_instance_state_does_not_exist(self):
        self.hal_rest_client.expect_instance_details(testy_jira_dev_com, toReturn=None)
        instance = HalInstanceState(testy_jira_dev_com, self.hal_rest_client)

        self.assertFalse(instance.exists)

    def test_get_instance_state_for_new_instance(self):
        instance_details = {
            'status': ASSIGNING,
            'zone': 'sc2',
            'environment': 'dev'
        }
        self.hal_rest_client.expect_instance_details(testy_jira_dev_com, toReturn=instance_details)

        instance = HalInstanceState.for_new_instance(testy_jira_dev_com, self.hal_rest_client)
        self.hal_rest_client.verify_create_new_instance(testy_jira_dev_com)
        self.assertTrue(instance.exists)
        self.assertEqual(ASSIGNING, instance.state)
        self.assertEqual('sc2', instance.zone)
        self.assertEqual('dev', instance.environment)

    def test_wait_until_state(self):
        instance_details = {
            'status': ASSIGNING,
            'zone': 'sc2',
            'environment': 'dev'
        }
        instance_details_running = {
            'status': RUNNING,
            'zone': 'sc2',
            'environment': 'dev'
        }
        responses = (x for x in [instance_details, instance_details, instance_details, instance_details_running])

        def handle_instance_details_call(instance_name):
            self.assertEqual(testy_jira_dev_com, instance_name)
            result = next(responses)
            return result
        self.hal_rest_client.instance_details = handle_instance_details_call

        instance, transitioned = HalInstanceState(testy_jira_dev_com, self.hal_rest_client).wait_until_in_state(RUNNING, sleep_fun=lambda: 0)

        self.assertTrue(transitioned)
        self.assertTrue(instance.exists)
        self.assertEqual(RUNNING, instance.state)
        self.assertEqual('sc2', instance.zone)
        self.assertEqual('dev', instance.environment)

    def test_wait_until_state_timed_out(self):
        instance_details = {
            'status': ASSIGNING,
            'zone': 'sc2',
            'environment': 'dev'
        }
        def handle_instance_details_call(instance_name):
            self.assertEqual(testy_jira_dev_com, instance_name)
            return instance_details
        self.hal_rest_client.instance_details = handle_instance_details_call

        times = (t for t in range(5))
        def time_fun():
            return next(times)
        instance, transitioned = HalInstanceState(testy_jira_dev_com, self.hal_rest_client).wait_until_in_state(RUNNING, timeout_seconds=3, sleep_fun=lambda: 0, time_fun=time_fun)

        self.assertFalse(transitioned)
        self.assertTrue(instance.exists)
        self.assertEqual(ASSIGNING, instance.state)
        self.assertEqual('sc2', instance.zone)
        self.assertEqual('dev', instance.environment)


class TestHalUtils(TestCase):
    def setUp(self):
        self.hal_rest_client = Mock()

    def test_hal_utils_instance_not_in_dev(self):
        hal_utils = HalUtils(self.hal_rest_client)
        instance_details = {
            'status': RUNNING,
            'zone': 'sc2',
            'environment': 'prod'
        }
        self.hal_rest_client.expect_instance_details(testy_jira_dev_com, toReturn=instance_details)

        self.assertRaises(InstanceNotApplicableForDev, lambda: hal_utils.prepare_instance_for_debug(testy_jira_dev_com))

    def test_hal_utils_instance_in_dev(self):
        hal_utils = HalUtils(self.hal_rest_client)
        instance_details = {
            'status': RUNNING,
            'zone': 'sc2',
            'environment': 'dev'
        }
        self.hal_rest_client.expect_instance_details(testy_jira_dev_com, toReturn=instance_details)

        instance = hal_utils.prepare_instance_for_debug(testy_jira_dev_com)
        self.assertTrue(instance.exists)
        self.assertEqual(RUNNING, instance.state)
        self.assertEqual('sc2', instance.zone)
        self.assertEqual('dev', instance.environment)

    def test_hal_utils_new_instance(self):
        hal_utils = HalUtils(self.hal_rest_client)
        self.hal_rest_client.expect_instance_details(testy_jira_dev_com, toReturn=None)
        hal_utils.prepare_instance_for_debug(testy_jira_dev_com)
        self.hal_rest_client.verify_create_new_instance(testy_jira_dev_com)
        self.assertEqual(2, self.hal_rest_client.callcount_instance_details())

if __name__ == '__main__':
    unittest.main(verbosity=2)

