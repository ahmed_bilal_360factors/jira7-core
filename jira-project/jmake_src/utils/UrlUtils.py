import base64
from shutil import copyfileobj
from urllib.request import Request, build_opener, HTTPRedirectHandler
from urllib.parse import quote
import sys
from Logger import LOG
from utils.FileUtils import FileUtils


class UrlUtils:

    def __init__(self, fs: FileUtils = FileUtils()):
        self.fs = fs
        pass

    def get(self, url: str, user: str, password: str, extra_headers = None, output_file_path:str = None):
        req = Request(url, headers=self.__headers(user, password, extra_headers))

        return self.__process_request(req, output_file_path)

    def post(self, url: str, user: str, password: str, data:str, extra_headers = None):
        req = Request(url, data=bytes(data, encoding='utf-8'), headers=self.__headers(user, password, extra_headers))
        req.get_method = lambda: "POST"

        return self.__process_request(req)

    def put(self, url: str, user: str, password: str, data:str, extra_headers = None):
        req = Request(url, data=bytes(data, encoding='utf-8'), headers=self.__headers(user, password, extra_headers))
        req.get_method = lambda: "PUT"

        return self.__process_request(req)

    def __headers(self, user, password, extra_headers=None):
        headers = {}
        if user is not None and password is not None:
            headers.update({'Authorization': self.__build_basic_auth(user, password)})
        if extra_headers is not None:
            headers.update(extra_headers)
        return headers

    def __process_request(self, req: Request, output_file = None):
        LOG.debug(req.full_url)

        with self.__custom_opener().open(req) as remote_file:
            if output_file is not None:
                with self.fs.open(output_file, 'wb', None) as local_file:
                    copyfileobj(remote_file, local_file)
            else:
                return str(remote_file.read(), encoding='utf-8')
        return True

    def __build_basic_auth(self, user, password):
        return 'Basic %s' % (
            base64.urlsafe_b64encode(bytes('%s:%s' % (user, password), sys.getdefaultencoding())).decode()
        )

    def quote(self, text):
        return quote(text)

    @staticmethod
    def as_url_params(params_dict):
        return '&'.join('='.join((k, v)) for k, v in params_dict.items())

    def __custom_opener(self):
        """
        Default python implementation repeats headers on redirect, which causes some cross-origin
        redirects to maintain some authorization information and fail.
        """
        return build_opener(HeaderStrippingRedirectHandler())


class HeaderStrippingRedirectHandler(HTTPRedirectHandler):
    def redirect_request(self, req, fp, code, msg, headers, newurl):
        request = super().redirect_request(req, fp, code, msg, headers, newurl)
        request.headers = {k: v for k, v in request.headers.items() if k != 'Authorization'}
        return request