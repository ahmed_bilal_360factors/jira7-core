import atexit
from functools import partial
import os
import signal
import subprocess
import sys
from Logger import Logger
from callables.Callable import Callable

__author__ = 'lwlodarczyk'


class SystemCallable(Callable):
    def __init__(self, args, command=None, cwd=None):
        super().__init__(args)
        self.__command = command
        self.__cwd = cwd
        self.with_logged_output = os.getenv('JMAKE_TRACE') is not None
        self.run_in_background = False
        self.log_file = None
        self.log_stderr = True

    @staticmethod
    def decode_utf_8(binary_line, logger):

        try:
            return binary_line.decode('utf-8')
        except Exception as ex:
            logger.error("Cannot decode line " + str(binary_line))
            return ""

    def __call__(self, logger):
        command = self.__command
        env = self.env if hasattr(self, 'env') else os.environ

        if 'JMAKE_DRY' in env and env['JMAKE_DRY']:
            command = "echo '%s'" % command
            logger.warn('Call will be suppressed because you have exported JMAKE_DRY env. variable.')

        executable = self.__execute(command, logger, env)
        logger.debug('Executed process pid=%s' % executable.pid)
        prev_signal_handler = signal.signal(signal.SIGINT, partial(self.__signal_handler, executable))

        try:
            self.sig_int = False
            line_no = 0
            while not self.run_in_background:
                try:
                    raw_line = executable.stdout.readline()
                    assert isinstance(raw_line, bytes)
                    try:
                        line = raw_line.decode('utf-8')
                    except UnicodeDecodeError as e:
                        print('Error decoding output: "', raw_line, '" from ', command)
                        print('Check that maven encoding is set to UTF-8.')
                        print('\tYou can check your encoding with mvn --version.')
                        print('\tYou can add <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>'
                              ' to your ~/.m2/settings.xml profile properties to fix it')
                        raise e

                    if not line and executable.poll() is not None:
                        break

                    if self.process_output(logger, line.rstrip('\n'), line_no):
                        if self.with_logged_output:
                            logger.trace(line)
                        else:
                            print(self.__colorize_line(line, logger), end='')
                    line_no += 1
                except IOError:
                    if self.sig_int:
                        logger.debug('Caught SIGINT terminating jmake')
                    else:
                        logger.warn('The spawned process pid=%s was probably killed' % executable.pid)
                    return Callable.do_not_proceed

            if not self.run_in_background:
                executable.communicate()
            else:
                self.__atexit_kill_func = lambda: logger.info('Killing background process %s' % command) or executable.kill()
                atexit.register(self.__atexit_kill_func)
        finally:
            signal.signal(signal.SIGINT, prev_signal_handler)
            if not self.run_in_background:
                self.returncode = executable.returncode
            else:
                self.returncode = Callable.success
                self.__background_process = executable

        return Callable.do_not_proceed if self.sig_int else self.returncode

    def command(self, new_command):
        self.__command = new_command

    def background(self):
        self.run_in_background = True
        return self

    def redirect_output(self, log_file):
        self.log_file = log_file
        return self

    def join(self):
        if self.run_in_background:
            self.__background_process.communicate()
            atexit.unregister(self.__atexit_kill_func)
            return self.__background_process.returncode

    def join_as_callable(self, description: str):

        def join_closure(log: Logger):
            log.info('Waiting for background process "%s" to finish...' % description)
            rc = self.join()
            if not rc == Callable.success:
                log.error('Background process "%s" encountered an error. Return code was: %s' % (description, str(rc)))
                return Callable.do_not_proceed
            return rc

        return join_closure

    def __signal_handler(self, executable, signal, frame):
        self.sig_int = True
        try:
            executable.kill()
        except OSError:
            pass

    def __execute(self, command, logger, env):
        logger.info(
            'Making OS call{0}: {1}'.format(
                ' (in directory: ' + str(self.__cwd) + ')' if self.__cwd is not None else '',
                command))
        if self.log_file is not None:
            logger.info('Redirected output to file %s' % self.log_file)
            out_file = open(self.log_file, mode='a')
        else:
            out_file = subprocess.PIPE

        return subprocess.Popen(command, stdout=out_file, stderr=subprocess.STDOUT if self.log_stderr else None,
                                shell=True, cwd=self.__cwd, env=env)


    def process_output(self, logger: Logger, line: str, num: int):
        """
        Override to catch simple output lines and return True to display it on stdout or False to suppress it.
        """
        return True

    def __colorize_line(self, line: str, logger: Logger):
        if 'WARN' in line:
            return logger.color_text(line, Logger.L_WARN)
        if 'ERROR' in line:
            return logger.color_text(line, Logger.L_ERROR)
        if 'BUILD SUCCESS' in line:
            return logger.color_text(line, Logger.L_INFO)
        return line
