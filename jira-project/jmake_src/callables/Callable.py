class Callable:
    success = 0
    failure = 42
    do_not_proceed = 678

    def __init__(self, args):
        super().__init__()
        self.args = args

    def __call__(self, logger):
        return Callable.success