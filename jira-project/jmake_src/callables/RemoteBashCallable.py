import os
import sys
from callables.Callable import Callable
from utils.FileUtils import FileUtils
from utils.ProcessUtils import ProcessUtils


class RemoteBashCallable(Callable):
    def __init__(self, hostname, local_script_file=None, script=None, script_parameters: list=None,
                 file_utils=FileUtils(), process_utils=ProcessUtils(), callback=None):
        super().__init__([])
        self.params = script_parameters if script_parameters is not None else []
        self.process_utils = process_utils
        self.file_utils = file_utils
        self.script_file = local_script_file
        self.hostname = hostname
        self.callback = callback
        self.script = script

    def __call__(self, logger):
        if self.script_file is None:
            logger.info('Executing script \'%s\' on remote host %s' % (self.script, self.hostname))
        else:
            logger.info('Executing script %s on remote host %s' % (os.path.basename(self.script_file), self.hostname))

        if not os.getenv('JMAKE_DRY', None) is None:
            logger.warn('Skipping execution of script due to detected JMAKE_DRY.')
            return Callable.success

        if self.script_file is not None:
            with self.file_utils.open(self.script_file, 'rb', encoding=None) as f:
                script_bytes = f.read()
        elif self.script is not None:
            script_bytes = self.script.encode('utf-8')
        else:
            raise Exception('Either script_file or script must be provided')

        out = self.execute_script(script_bytes, logger)

        if self.callback is not None:
            return self.callback(out, logger)

        return Callable.success

    def execute_script(self, script, logger):
        parameters_command_text = ' '.join(self.params).strip()
        out = self.process_utils.execute_read_input(['ssh', self.hostname,
                                                     'bash -s -- ' + parameters_command_text], script,
                                                    stderr=sys.stderr)
        logger.debug("Output from remote host:\n<<<BEGIN>>>\n%s\n<<<END>>>" % out)
        return out