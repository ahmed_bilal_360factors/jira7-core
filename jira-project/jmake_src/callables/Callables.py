from callables.Callable import Callable


class CombinedCallable(Callable):

    def __init__(self, args, tasks):
        super().__init__(args)
        self.tasks = tasks

    def __call__(self, logger):
        res = Callable.failure

        for task in self.tasks:
            res = task(logger)
            if res != Callable.success:
                return res

        return res


class ConditionalCallable(Callable):

    class Result:

        def __init__(self, execute_task, result=None):
            super().__init__()
            self.result = result
            self.execute_task = execute_task

        @staticmethod
        def execute_task():
            return ConditionalCallable.Result(True)

        @staticmethod
        def return_value(value):
            return ConditionalCallable.Result(False, value)

    def __init__(self, args, task, condition):
        super().__init__(args)
        self.condition = condition
        self.task = task

    def __call__(self, logger):
        res = self.condition(logger)
        if res.execute_task:
            return self.task(logger)
        else:
            return res.result


class GenericTaskCallable(Callable):
    def __init__(self, args, task):
        super().__init__(args)
        self.args = args
        self.task = task

    def __call__(self, logger):
        return self.task(self.args, logger)