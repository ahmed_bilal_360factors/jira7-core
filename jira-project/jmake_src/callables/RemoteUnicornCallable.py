import os
import sys
from callables.Callable import Callable
from utils.FileUtils import FileUtils
from utils.ProcessUtils import ProcessUtils

"""
Runs a script on a remote host
    :param hostname: the remote host
    :param local_script_file: the script file (local)
    :param script_parameters: (optional) list of parameters to pass to the script
    :param output_variable: (optional) name of an environment variable in which to store the script output
"""
class RemotePython27ScriptCallable(Callable):
    def __init__(self, hostname, local_script_file, script_parameters: list=None,
                 file_utils=FileUtils(), process_utils=ProcessUtils(), callback=None):
        super().__init__([])
        self.params = script_parameters if script_parameters is not None else []
        self.process_utils = process_utils
        self.file_utils = file_utils
        self.script_file = local_script_file
        self.hostname = hostname
        self.callback = callback

    def __call__(self, logger):
        logger.info('Executing script %s on remote host %s' % (os.path.basename(self.script_file), self.hostname))

        with self.file_utils.open(self.script_file, 'rb', encoding=None) as f:
            if not os.getenv('JMAKE_DRY', None) is None:
                logger.warn('Skipping execution of script due to detected JMAKE_DRY.')
                return Callable.success

            parameters_command_text = ' '.join(self.params).strip()

            out = self.process_utils.execute_read_input(['ssh', self.hostname,
                'cat - | /sw/python/python-2.7.8/bin/python - ' + parameters_command_text], f.read(), sys.stderr).decode().strip()

            logger.debug('Output from remote host:\n<<<BEGIN>>>\n%s\n<<<END>>>' % out)

            if self.callback is not None:
                self.callback(out, logger)

        return Callable.success