#!/bin/bash
java -jar horde-tenant-initialiser.jar -a foobar  -i ../studio-initial-data.xml -l 'jdbc:hsqldb:file:database/horde' -r 'org.hsqldb.jdbcDriver'  -u sa  -p ''

echo "Patching horde database - setting hostname=localhost, secure.cookie=false"
(
        echo "update cwd_property set property_value='false' where property_name='secure.cookie'"
        echo "update cwd_property set property_value='localhost' where property_name='domain'"
        echo "COMMIT"
) >> database/horde.log