# IMPORTANT: this code has to work with python 2.7 as this is the best python we have available on unicorns

# This script prints the path of the active JIRA service, e.g. /service/j2ee_jira_12345
# If the active JIRA service cannot be identified, it defaults to /service/j2ee_jira
import os
import yaml

jira_yaml = {}
jira_yaml_path = '/data/icebat/seamless/jira.yaml'
if os.path.exists(jira_yaml_path):
    with open(jira_yaml_path, 'r') as f:
        jira_yaml = yaml.load(f)

service_name = jira_yaml.get('jira::active_service_name', 'j2ee_jira')
service_path = '/service/' + service_name
print service_path
