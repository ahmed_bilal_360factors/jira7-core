#!/usr/bin/env bash
# This was shamelessly copy&pasted from icebat
function build_java_opts() {
        local SERVICE_ROOT=`/sw/scripts/sops-instance/sops-instance-latest/support-scripts/bin/service-dir jira`
        local DEBUG_PORT=$2
        local JAVA_OPTS=""

        if [ -z "${DEBUG_PORT}" ] ; then
            echo "No DEBUG_PORT specified, JVM will not be available on debug mode when used in conjunction with /tmp/env/STUDIO_DEBUG" 1>&2
        elif [ -e /tmp/env/STUDIO_DEBUG ] ; then
            local SUSPEND="n"
            if [ -e /tmp/env/STUDIO_DEBUG_SUSPEND ] ; then
                SUSPEND="y"
            fi
            JAVA_OPTS="${JAVA_OPTS} -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=${SUSPEND},address=${DEBUG_PORT}"
        fi

        local PROP
        # build the JVM tuning properties
        for PROP in $(ls ${SERVICE_ROOT}/java_opts) ; do
            VALUE=$(< ${SERVICE_ROOT}/java_opts/${PROP})
            # This expands any environment variables in the value
            EXPANDED_VALUE=$(eval echo "${VALUE}")
            if [ -z "${EXPANDED_VALUE}" ] ; then
                echo WARN: Empty value for property ${SERVICE_ROOT}/java_opts/${PROP}=${EXPANDED_VALUE} 1>&2
            fi
            if [[ "${PROP}" =~ XX:[+|-]{1}.+ ]] ; then
                JAVA_OPTS="${JAVA_OPTS} -${PROP}"
            elif [[ "${PROP}" =~ X(ms|mx|ss)$ ]] ; then
                JAVA_OPTS="${JAVA_OPTS} -${PROP}${EXPANDED_VALUE}"
            elif [[ "${PROP}" =~ X[X:]?.+ ]] ; then
                JAVA_OPTS="${JAVA_OPTS} -${PROP}=${EXPANDED_VALUE}"
            else
                JAVA_OPTS="${JAVA_OPTS} -${PROP}:${EXPANDED_VALUE}"
            fi
        done

        # Build system properties
        for PROP in $(ls ${SERVICE_ROOT}/sysprops) ; do
            VALUE=$(< ${SERVICE_ROOT}/sysprops/${PROP})
            # This expands any environment variables in the value
            EXPANDED_VALUE=$(eval echo "${VALUE}")
            if [ -z "${EXPANDED_VALUE}" ] ; then
                echo WARN: Empty value for property ${SERVICE_ROOT}/sysprops/${PROP}=${EXPANDED_VALUE} 1>&2
            fi
            JAVA_OPTS="${JAVA_OPTS} -D${PROP}=${EXPANDED_VALUE}"
        done

        echo "${JAVA_OPTS}"
    }

build_java_opts $1 $2