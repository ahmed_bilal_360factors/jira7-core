# IMPORTANT: this code has to work with python 2.7 as this is the best python we have available on unicorns
import os
import sys
import yaml
import subprocess

studio_local_path = '/data/studio_local/jira'

if not os.path.exists(studio_local_path):
    os.makedirs(studio_local_path)

# routines to manipulate yaml:
def safely_set(data, key, value):
    """
    Sets value in the data dict under the given key. Does not set if already there. Throws exception
    if another value is already set under that key. Returns true
    """
    if key in data:
        if data[key] != value:
            raise Exception('%s is set to %s instead of %s, please fix that' % (key, data[key], value))
        else:
            return False
    else:
        data[key] = value
        return True

def traverse_or_create(data, path):
    """
    Returns the node indicated by the path, creating nodes on the way or traversing existing ones.
    Throws exception if a node to traverse is a leaf (not a dict)
    """
    def _traverse_or_create(data, path):
        if path:
            key = path[0]
            if key in data:
                if isinstance(data[key], dict):
                    return _traverse_or_create(data[key], path[1:])
                else:
                    raise Exception('Error traversing the yaml file, element "%s" is not a node, it\'s a leaf.' % (key))
            else:
                node = {}
                data[key] = node
                return _traverse_or_create(node, path[1:])
        else:
            return data
    try:
        return _traverse_or_create(data, path)
    except Exception as e:
        raise Exception('Could not traverse "%s"\n%s' % ('->'.join(path), str(e)))

# load local_yaml:
local_yaml_path = '/data/icebat/local.yaml'
with open(local_yaml_path, 'r') as f:
    local_yaml = yaml.load(f)

# make a copy of the original file:
tmp_yaml_path = local_yaml_path + '.tmp'
if not os.path.exists(tmp_yaml_path):
    print 'Creating a copy of the local.yaml file at: ', tmp_yaml_path
    with open(tmp_yaml_path, 'w') as f:
        yaml.dump(local_yaml, f, default_flow_style=False)

#create empty yaml if there was no documents
if local_yaml is None:
    local_yaml = {}

# set jira version studio_local:
changes_made = safely_set(local_yaml, 'jira::version', 'studio_local')

if changes_made:
    with open(local_yaml_path, 'w') as f:
        yaml.dump(local_yaml, f, default_flow_style=False)

    # trigger icebat to apply this change
    subprocess.Popen('/sw/ondemand/install/icebatstrap/install/install-studio.sh -s -n j2ee_jira'.split(' ')).wait()

