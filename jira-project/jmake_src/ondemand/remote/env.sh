#!/usr/bin/env bash
function build_env() {
        local SERVICE_ROOT=`/sw/scripts/sops-instance/sops-instance-latest/support-scripts/bin/service-dir jira`
        local ENV=""

        local ENV_VAR
        for ENV_VAR in $(ls ${SERVICE_ROOT}/env) ; do
            VALUE=$(< ${SERVICE_ROOT}/env/${ENV_VAR})
            ENV="${ENV}${ENV_VAR}=${VALUE}"$'\n'
        done

        echo "${ENV}"
    }

build_env $1