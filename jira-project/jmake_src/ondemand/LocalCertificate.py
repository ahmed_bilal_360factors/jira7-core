import os
import datetime
import platform

from callables.Callable import Callable
from callables.Callables import CombinedCallable
from callables.SystemCallable import SystemCallable
from utils.FileUtils import FileUtils


class LocalCertificate:
    CERTIFICATES_DIR = 'localhost_self_signed_certifcate'
    CERTIFICATE_FILE = 'cert.pem'
    PRIVATE_KEY_FILE = 'key.pem'
    KEYSTORE_FILE = 'cert.p12'
    CERT_UNDERSTOOD_FILE = 'understood'

    def __init__(self, file_utils=FileUtils()):
        self.file_utils = file_utils

    def certificate_file(self):
        return os.sep.join([self._certificate_dir(), LocalCertificate.CERTIFICATE_FILE]) 

    def private_key_file(self):
        return os.sep.join([self._certificate_dir(), LocalCertificate.PRIVATE_KEY_FILE]) 
        
    def keystore_file(self):
        return os.sep.join([self._certificate_dir(), LocalCertificate.KEYSTORE_FILE])

    def _is_cert_understood(self):
        return self.file_utils.file_exists(os.sep.join([self._certificate_dir(), LocalCertificate.CERT_UNDERSTOOD_FILE]))

    def _store_cert_understood(self):
        self.file_utils.write_lines(os.sep.join([self._certificate_dir(), LocalCertificate.CERT_UNDERSTOOD_FILE]),
                                    ['accepted', str(datetime.datetime.now())])

    def _certificate_dir(self):
        certificates_dir = os.sep.join([self.file_utils.jmake_user_data_dir(), LocalCertificate.CERTIFICATES_DIR])
        return self.file_utils.existing_dir(certificates_dir)

    def make_sure_installed(self, logger):
        if not self.file_utils.file_exists(self.keystore_file()):
            generate_certificate = CombinedCallable(None, [
                SystemCallable(None,
                               'umask 077; openssl req -x509 -newkey rsa:2048 ' +
                               '-keyout ' + self.private_key_file() + ' ' +
                               '-out ' + self.certificate_file() + ' ' +
                               '-days 30000 ' +
                               '-config ' + os.sep.join(['.', 'jmake_src', 'ondemand', 'openssl.cnf']) + ' ' +
                               '-subj "/C=AU/ST=New South Wales/L=Sydney/O=Atlassian/CN=localhost" -nodes'),
                SystemCallable(None,
                               'umask 077; openssl pkcs12 -export ' +
                               '-in ' + self.certificate_file() + ' ' +
                               '-inkey ' + self.private_key_file() + ' ' +
                               '-out ' + self.keystore_file() + ' ' +
                               '-passout pass:changeit'),
                self._remove_private_key_file
            ])
            result = generate_certificate(logger)
            if Callable.success != result:
                return result
            logger.warn("Generated a self-signed certificate for you (" + self.certificate_file() + "). "
                        "This is needed in dev mode because JIRA is configured with a localhost CDN. "
                        "Please make sure this certificate is trusted by your web browser.")

        if not self._is_cert_understood():
            answer = input('Do you understand that in order for JIRA to work you need trust '
                           '(' + self.certificate_file() + ') certificate in your browser? (Yes/No)? [Yes]: ')
            if not (answer.upper() == 'YES' or answer == ''):
                if not answer.upper() == 'NO':
                    logger.info('I am treating this as a No (you typed: "%s").' % answer)
                logger.error('Failing because you need to stop and think for a moment.')
                return Callable.do_not_proceed

            self._store_cert_understood()

            if platform.system() == 'Darwin':
                answer = input('Do you want to trust this certificate in this user\'s keychain? (Yes/No)? [No]: ')
                if not answer.upper() == 'YES':
                    logger.info('I am treating this as a No (you typed: "%s").' % answer)
                    return Callable.success

                logger.warn("Installing self-signed certificate in your keychain. "
                            "Please provide your password in a keychain dialog.")
                return SystemCallable(None,
                                      'security add-trusted-cert -d ' +
                                      '-k "$HOME/Library/Keychains/login.keychain" ' +
                                      self.certificate_file())(logger)
            else:
                return Callable.success

        else:
            if platform.system() == 'Darwin':
                cert_installed = SystemCallable(None,
                                                'security verify-cert -c ' +
                                                self.certificate_file() +
                                                '  -k "$HOME/Library/Keychains/login.keychain"')(logger)
                if cert_installed != Callable.success:
                    logger.warn("You don't have localhost CDN certificate installed in your keychain.")
                    logger.info("You can always install it using command: "
                                'security add-trusted-cert -d ' +
                                '-k "$HOME/Library/Keychains/login.keychain" ' +
                                self.certificate_file()
                                )
            return Callable.success

    def _remove_private_key_file(self, logger):
        self.file_utils.remove(self.private_key_file())
        return Callable.success

