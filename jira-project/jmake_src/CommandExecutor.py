
from Logger import Logger
from callables.Callable import Callable


class CommandExecutor:

    def __init__(self):
        self.commands = []
        self.post_commands = []
        self.perform_console_reset = True

    def append(self, command):
        if command is not None:
            self.commands.append(command)

    def append_post(self, command):
        if command is not None:
            self.post_commands.append(command)

    def set_logger(self, logger):
        self.log = logger
        return self

    def execute(self):
        log = getattr(self, 'log', None)
        if log is None: log = Logger().set_none()

        try:
            return_code = self.__execute_commands(self.commands, log)
            return return_code
        finally:

            self.__execute_commands(self.post_commands, log)
            if self.perform_console_reset:
                print('\x1b[0;0m')

    def __execute_commands(self, commands, log):
        try:
            for callable in commands:
                return_code = callable(log)
                if return_code == Callable.do_not_proceed:
                    return return_code
                if return_code != Callable.success:
                    log.error('Last executed command ' + callable.__class__.__name__ + ' returned ' + str(return_code))
                    return return_code
        except KeyboardInterrupt:
            log.info('\njmake interrupted by keyboard')
            return -1
        except IOError as er:
            log.error('I/O Error %s' % er)
            return -1
        return Callable.success


