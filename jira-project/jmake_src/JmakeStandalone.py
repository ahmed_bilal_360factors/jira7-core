import os
import uuid
from tarfile import TarFile

from Logger import LOG
from callables.Callable import Callable
from callables.SystemCallable import SystemCallable
from maven.Maven import MavenCallable
from module import JmakeModule
from utils.DataBean import DataBean
from utils.FileUtils import FileUtils

DISTRIBUTION_PROJECTS_ROOT = os.sep.join(['target', 'distribution-projects'])
DISCOVER_ARTEFACT_VERSION_TEMPLATE = os.sep.join(['jmake_src', 'data', 'discover_released_artifact_version.xml'])

standalone_products = {
    'core': None,
    'software': DataBean(groupId='com.atlassian.jira', artifactId='jira-software-application'),
    'servicedesk': DataBean(groupId='com.atlassian.servicedesk', artifactId='jira-servicedesk-application')
}


class DistributionBuilder:

    def __init__(self, args, fs: FileUtils=FileUtils()):
        self.fs = fs
        self.args = args
        self.unpacked_path = None

        self.product = args.product
        self.product_version = args.product_version
        self.product_data = standalone_products[args.product]

    # visible for testing
    def determine_product_version(self):
        if self.product_version is None:
            self.product_version = 'RELEASE'
        return self.product_version not in ['RELEASE', 'LATEST']

    def generate_build_standalone_tasks(self):
        pass

    def get_built_standalone_distribution_path(self):
        pass

    def unpack_standalone_task(self):
        def closure(log):
            self.fs.existing_dir('target')
            standalone_distribution = self.get_built_standalone_distribution_path()

            tar = TarFile.open(standalone_distribution)

            # assume the standalone archives have a root dir:
            member = tar.next().name.lstrip('.' + os.sep)
            if os.sep not in member:
                log.error('Unexpected error when reading built tar file. Jmake expects a root directory within '
                          'the archive, but found a flat file.')
                return Callable.do_not_proceed

            archive_root = member[:member.find(os.sep)]
            log.debug('Read archive root: ' + archive_root)
            self.unpacked_path = os.sep.join(['target', archive_root])

            log.info('Unpacking %s...' % standalone_distribution)
            tar.extractall(path='target')
            log.info('Unpacked standalone distribution to %s.' % self.unpacked_path)
            return 0
        return closure

    def run_standalone_task(self):
        def closure(log):
            jira_home = self.fs.existing_dir(os.sep.join([self.unpacked_path, 'jirahome']))
            script_callable = SystemCallable(None, '%s/bin/start-jira.sh -fg' % self.unpacked_path)
            script_callable.env = os.environ.copy()
            script_callable.env.update({'JIRA_HOME' : jira_home})
            return script_callable(log)
        return closure

    # visible for testing
    def discover_product_version(self):
        pom_dir = self.fs.existing_dir(os.sep.join(['jmake_src', 'target', 'version-discovery', str(uuid.uuid4())]))
        LOG.info('Preparing project to determine product version in %s' % pom_dir)

        try:
            replace_map = {
                '${ARTIFACT_GROUP_ID}': self.product_data.groupId,
                '${ARTIFACT_ID}': self.product_data.artifactId,
                '${ARTIFACT_VERSION}': self.product_version
            }
            self.fs.filter_file(DISCOVER_ARTEFACT_VERSION_TEMPLATE, os.sep.join([pom_dir, 'pom.xml']), replace_map)
        except Exception as e:
            LOG.error('Could not prepare maven project to discover version of application ' + str(e))
            return Callable.do_not_proceed

        def closure(logger):

            # rewrite the property placed in the file
            maven = MavenCallable(None, path=pom_dir)
            maven.phase('versions:update-property')
            maven.property('property', 'artifact.version')
            maven.option('-q')
            maven(logger)

            # output the property
            maven = MavenCallable(None, path=pom_dir)
            maven.phase('exec:exec')
            maven.property('exec.executable', 'echo').property('exec.args', "'${artifact.version}'")
            maven.option('--non-recursive').option('-q')
            maven.process_output = lambda log, line, line_num: self.set_product_version(line)
            return maven(logger)

        return closure

    def set_product_version(self, line):
        LOG.info('Discovered product version: %s' % line)
        self.product_version = line
        return False


class JiraCoreStandaloneBuilder(DistributionBuilder):

    def __init__(self, args, fs: FileUtils=FileUtils()):
        super().__init__(args, fs)

    def generate_build_standalone_tasks(self):
        maven = MavenCallable(self.args, path=os.sep.join(['jira-distribution', 'jira-core-distribution']))
        maven.project('jira-core-standalone-distribution').option('-am')
        maven.phase('package')
        maven.property('maven.test.skip', 'true')
        yield maven

    def get_built_standalone_distribution_path(self):
        standalone_project_target = self.fs.abs_path(os.sep.join(['jira-distribution', 'jira-core-distribution', 'jira-core-standalone-distribution', 'target']))
        root, dirs, files = next(self.fs.walk(standalone_project_target))
        for file in files:
            if file.endswith('.tar.gz'):
                return os.sep.join([standalone_project_target, file])

        raise Exception('Could not find built standalone distribution. Did all maven calls and scripts ended up successful?')


class JiraSoftwareStandaloneBuilder(DistributionBuilder):

    def __init__(self, args, fs: FileUtils=FileUtils()):
        super().__init__(args, fs)

    def generate_build_standalone_tasks(self):

        if not self.determine_product_version():
            yield self.discover_product_version()

        def maven_with_updated_product_version(logger):
            maven = MavenCallable(self.args, path=os.sep.join(['jira-distribution', 'jira-software-distribution']))
            maven.project('jira-software-standalone-dist').option('-am')
            maven.phase('package')
            maven.property('maven.test.skip', 'true')
            maven.property('jira.product.version', self.product_version)
            return maven(logger)
        yield maven_with_updated_product_version

    def get_built_standalone_distribution_path(self):
        standalone_project_target = self.fs.abs_path(os.sep.join(['jira-distribution', 'jira-software-distribution', 'jira-software-standalone-dist', 'target']))
        root, dirs, files = next(self.fs.walk(standalone_project_target))
        for file in files:
            if file.endswith('.tar.gz'):
                return os.sep.join([standalone_project_target, file])

        raise Exception('Could not find built standalone distribution. Did all maven calls and scripts ended up successful?')


class JiraServicedeskStandaloneBuilder(DistributionBuilder):

    def __init__(self, args, fs: FileUtils=FileUtils()):
        super().__init__(args, fs)

    def generate_build_standalone_tasks(self):

        if not self.determine_product_version():
            yield self.discover_product_version()

        def maven_with_updated_product_version(logger):
            maven = MavenCallable(self.args, path=os.sep.join(['jira-distribution', 'jira-servicedesk-distribution']))
            maven.project('servicedesk-standalone-dist').option('-am')
            maven.phase('package')
            maven.property('maven.test.skip', 'true')
            maven.property('jira.product.version', self.product_version)
            return maven(logger)
        yield maven_with_updated_product_version

    def get_built_standalone_distribution_path(self):
        standalone_project_target = self.fs.abs_path(os.sep.join(['jira-distribution', 'jira-servicedesk-distribution', 'servicedesk-standalone-dist', 'target']))
        root, dirs, files = next(self.fs.walk(standalone_project_target))
        for file in files:
            if file.endswith('.tar.gz'):
                return os.sep.join([standalone_project_target, file])

        raise Exception('Could not find built standalone distribution. Did all maven calls and scripts ended up successful?')


class Standalone(JmakeModule):
    def __init__(self):
        super().__init__()
        self.command = 'standalone'
        self.description = 'Builds JIRA standalone distribution just as if you unpacked your behind-the-firewall product '\
                           'and run it as an option.'

    def __call__(self, args, executor):

        # build jira:
        maven = MavenCallable(args)
        maven.phase('install')
        maven.property('maven.test.skip', 'true')
        maven.profile('distribution').property('deploy.distribution.base').property('skip.final.distributions')
        executor.append(maven)

        # pick a distribution builder:
        standalone_builder = self.choose_standalone_builder(args)

        # execute its tasks:
        for task in standalone_builder.generate_build_standalone_tasks():
            executor.append(task)

        # execute unpack task:
        executor.append(standalone_builder.unpack_standalone_task())

        # if requested, run:
        if args.run:
            executor.append(standalone_builder.run_standalone_task())

    def define_parser(self, parser):
        MavenCallable.add_maven_switches(parser)
        parser.add_argument('-r', '--run', action='store_true', help='runs the unpacked JIRA package.')
        parser.add_argument('-p', '--product', choices=list(standalone_products.keys()), default='core',
                            help='prepares standalone distribution for core/sw/sd. JIRA Core is the default.')
        parser.add_argument('--product-version', default=None, dest='product_version',
                            help='Specifies the application (sw/sd) version. Do not specify for core. Special version '
                                 'names "RELEASE" and "LATEST" are supported. Default is "RELEASE".')

    def choose_standalone_builder(self, args):
        if args.product == 'core':
            if args.product_version is not None:
                LOG.warn('You specified a version of Core application. This will be disregarded.')
            return JiraCoreStandaloneBuilder(args)
        elif args.product == 'software':
            return JiraSoftwareStandaloneBuilder(args)
        elif args.product == 'servicedesk':
            return JiraServicedeskStandaloneBuilder(args)
        else:
            raise Exception('Invalid product!')

