import os
import subprocess
import re

from node.NodeVersionChecker import NodeVersionChecker
from node.NodeVersionChecker import NodeRegistryMisconfiguredException
from Logger import LOG


class NpmRunner:
    def __init__(self, node_version_checker=NodeVersionChecker()):
        super().__init__()
        self.node_version_checker = node_version_checker

    def run_command(self, args):
        node_executable = self.node_version_checker.get_node_executable()

        new_env = os.environ.copy()
        node_executable_path = re.sub('/node$', '', node_executable)
        new_env['PATH'] = ':'.join([node_executable_path, new_env['PATH']])

        # check NPM config
        registry = subprocess.check_output(['npm', 'config', 'get', '@atlassian:registry'], env=new_env).decode()
        if "https://npm-private.atlassian.io/" not in registry:
            raise NodeRegistryMisconfiguredException("Atlassian NPM registry not configured, please refer to https://extranet.atlassian.com/display/NPM/npm.atlassian.io+Home or type:\n\n\n\n\tnpm login --registry=https://npm-private.atlassian.io --scope=atlassian\n\n\n")

        command = ['npm'] + args
        LOG.info("Using node '%s' (%s) with PATH (%s)" % (node_executable, self.node_version_checker.get_version(node_executable), new_env['PATH']))
        return_code = subprocess.call(command, env=new_env)
        if return_code != 0:
            raise subprocess.CalledProcessError(return_code, command)
        try:
            LOG.info("Trying to cache npm dependencies ...")
            subprocess.call(["npm-cache", "install"])
        except OSError as ex:
            if ex.errno == os.errno.ENOENT:
                LOG.info("You are not using npm-cache, your builds may last longer. Install https://www.npmjs.com/package/npm-cache in order to speed up solving npm dependencies.")
            else:
                LOG.warn("Unknown exception while caching npm dependencies: " + ex )
