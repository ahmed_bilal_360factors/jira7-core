import platform
import os
from distutils.version import LooseVersion

from utils.ProcessUtils import ProcessUtils

MIN_NODE_VERSION = "4"
BAMBOO_NODE_4_2 = 'bamboo_capability_system_builder_node_Node_js_4_2'


class NodeVersionInaccessibleException(Exception):
    pass

class NodeRegistryMisconfiguredException(Exception):
    pass

class NodeVersionChecker:
    def __init__(self, process_utils=ProcessUtils()):
        super().__init__()
        self.process_utils = process_utils

    def get_node_executable(self):
        executables = [
            ('node', None),
            (os.environ.get('NODE_EXECUTABLE'), 'NODE_EXECUTABLE'),
            (os.environ.get(BAMBOO_NODE_4_2), BAMBOO_NODE_4_2)
        ]

        resolve_errors = []
        for executable, system_variable in executables:
            if executable:
                try:
                    self.check_version(executable)
                    return executable
                except NodeVersionInaccessibleException as e:
                    resolve_errors.append(self._format_resolve_error(executable, system_variable, e))

        error_message = 'Node version {min_version} or greater required (https://extranet.atlassian.com/x/VjH4dg). ' \
                        '{platform_msg}. Tried to resolve command:\n{errors}'.format(
            min_version=MIN_NODE_VERSION,
            platform_msg=self.platform_msg(),
            errors='\n'.join(resolve_errors)
        )
        raise NodeVersionInaccessibleException(error_message)

    def _format_resolve_error(self, executable, system_variable, e):
        if system_variable:
            return '\t"%s" from ENV "%s": %s' % (executable, system_variable, e)
        else:
            return '\t"%s": %s' % (executable, e)

    def check_version(self, executable):
        try:
            node_version = self.get_version(executable)
            if LooseVersion(node_version) < LooseVersion(MIN_NODE_VERSION):
                raise NodeVersionInaccessibleException("Node version too old (%s)." % node_version)
        except Exception as e:
            raise NodeVersionInaccessibleException('Unknown error trying to execute node process: %s' % e)

    def get_version(self, executable):
        return self.process_utils.check_output([executable, '-v'], universal_newlines=True).strip().lstrip('v')

    def platform_msg(self):
        os_system = platform.system()
        msg = "Please install node!"
        if os_system == 'Darwin':
            msg += " Run 'brew install node' or use nvm..."
        elif os_system == 'Linux':
            msg += " Run 'apt-get install nodejs' or use nvm..."
        return msg
