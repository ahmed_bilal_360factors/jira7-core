import os
import unittest

from node.NodeVersionChecker import NodeVersionChecker, NodeVersionInaccessibleException

from utils.Mocks import Mock

MOCK_BAMBOO_NODE_EXECUTABLE = '/bamboo/version/of/node'
MOCK_NODE_EXECUTABLE = '/some/other/version/of/node'


class EnvironmentOverride:

    def __init__(self, overrides: dict):
        self.overrides = overrides

    def __enter__(self):
        self.env = dict(os.environ)
        os.environ.update(self.overrides)

    def __exit__(self, exc_type, exc_val, exc_tb):
        os.environ.clear()
        os.environ.update(self.env)


class TestNodeVersionChecker(unittest.TestCase):
    def test_node_executable_doesnt_exist(self):
        utils = Mock(side_effect=Exception('node')) # using a generic error for compatibility between python versions
        nodeVersionChecker = NodeVersionChecker(process_utils=utils)

        self.assertRaisesRegex(NodeVersionInaccessibleException, 'Unknown error trying to execute node process',
                               nodeVersionChecker.get_node_executable)

    def test_node_too_old(self):
        utils = Mock().default_check_output('v0.12.3\n')
        nodeVersionChecker = NodeVersionChecker(process_utils=utils)

        self.assertRaisesRegex(NodeVersionInaccessibleException, 'Node version too old \(0.12.3\)',
                               nodeVersionChecker.get_node_executable)

    def test_good_node_version(self):
        utils = Mock().default_check_output('v4.3.0\n')
        nodeVersionChecker = NodeVersionChecker(process_utils=utils)

        self.assertEqual('node', nodeVersionChecker.get_node_executable())

    def test_executable_resolution(self):
        nodeVersionChecker = NodeVersionChecker(process_utils=self._mock_node_executables(MOCK_NODE_EXECUTABLE))

        with EnvironmentOverride({'NODE_EXECUTABLE': MOCK_NODE_EXECUTABLE}):
            self.assertEqual(MOCK_NODE_EXECUTABLE, nodeVersionChecker.get_node_executable())

    def test_executable_resolution_for_bamboo(self):
        nodeVersionChecker = NodeVersionChecker(process_utils=self._mock_node_executables(MOCK_BAMBOO_NODE_EXECUTABLE))

        with EnvironmentOverride({'bamboo_capability_system_builder_node_Node_js_4_2': MOCK_BAMBOO_NODE_EXECUTABLE}):
            self.assertEqual(MOCK_BAMBOO_NODE_EXECUTABLE, nodeVersionChecker.get_node_executable())

    def _mock_node_executables(self, fake_executable):
        utils = Mock()
        utils.default_check_output('v0.12.3\n')
        utils.expect_check_output([fake_executable, '-v'], universal_newlines=True, toReturn='v4.3.0\n')
        return utils


if __name__ == '__main__':
    unittest.main(verbosity=2)
