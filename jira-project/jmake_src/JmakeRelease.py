from distutils.version import LooseVersion
import fileinput
import fnmatch
import os
from xml.etree import ElementTree

from maven.Maven import MavenCallable
from module import JmakeModule
from utils.GitUtils import GitUtils


IDENTIFIERS = ['MAJOR', 'MINOR', 'PATCH']
ORIGIN_URL = 'ssh://git@stash.atlassian.com:7997/jira/jira.git'


class Release(JmakeModule):
    def __init__(self):
        JmakeModule.__init__(self)
        self.command = 'release'
        self.description = ''

    def get_submodules(self):
        return [ReleaseSetVersion(), ReleaseGetNextVersion(), ReleaseTag()]


class ReleaseGetNextVersion(JmakeModule):
    """
     To be used before ReleaseSetVersion. Invoked via jmake release get-next-version -i {MAJOR, MINOR, PATCH} where
     MAJOR, MINOR or PATCH indicate the part of the version number you want to increment. For example with a starting
     version of 7.2.1
      MAJOR next version is 8.0.0
      MINOR next version is 7.3.0
      PATCH next version is 7.2.2

    """
    def __init__(self):
        JmakeModule.__init__(self)
        self.command = 'get-next-version'
        self.description = ''

    def __call__(self, args, executor):
        pom_version = find_pom_version('pom.xml')
        executor.append(lambda log: log.info('Found the existing pom version to be {}'.format(pom_version)))
        version = self.get_latest_jira_version(pom_version)
        executor.append(
            lambda log: log.info('Determined the version to increment (either latest tag of series or current semver '
                                 'pom version) to be {}'.format(version)))
        incremented_version = self.increment_version_number(version, args.increment_identifier, args.classifier)

        with open('release.properties', mode='w') as release_props:
            release_props.write('version='+str(incremented_version))

    def define_parser(self, parser):
        parser.add_argument('-i', '--increment-identifier',
                            type=str,
                            default='PATCH',
                            help='Which part of the version number to increment',
                            choices=IDENTIFIERS,
                            dest='increment_identifier')
        parser.add_argument('-c', '--classifier',
                            type=str,
                            default='',
                            help='Specify a custom classifier to append to the incremented version number',
                            dest='classifier')
        MavenCallable.add_maven_switches(parser)

    def get_latest_jira_version(self, pom_version, git_utils=GitUtils()):
        """
        This queries the git tags that match the tag format specified in the pom
        :return: Either the latest tag version or the current pom version minus -SNAPSHOT which ever is greater
        :rtype: LooseVersion
        """
        def __convert_to_version(version_string, pom):
            tag = LooseVersion(version_string)
            # Only return if the tag we are looking at is of the same version group as what is currently stored in
            # JIRA's pom and there are only three parts to the version string i.e 1000.0.0
            return tag if tag.version[0] == pom.version[0] and len(tag.version) == 3 else None

        pom_version_string = str(pom_version)
        # Prefilter the git tags we return to only match the first part of the version we are looking at in the pom
        git_tags = git_utils.execute_git('tag -l "{}*"'.format(pom_version_string[0:pom_version_string.find('.')])).output
        filterd_tags = list(filter(lambda v: __convert_to_version(v, pom_version), git_tags))
        semver_pom_version = LooseVersion(str(pom_version).replace('-SNAPSHOT', ''))
        latest_tag = sorted(filterd_tags)[-1] if len(filterd_tags) > 0 else str(semver_pom_version)
        latest_tag_version = LooseVersion(latest_tag)
        return latest_tag_version if latest_tag_version > semver_pom_version else semver_pom_version

    def increment_version_number(self, version, increment_identifier, classifier):
        """
        Increment a SEMVER version number
        :param version: The version to increment
        :type version: LooseVersion
        :param increment_identifier: The SEMVER identifier to increment
        :type increment_identifier: String of MAJOR, MINOR, PATCH
        :return: Incremented version
        :rtype: LooseVersion
        """
        inc_id = IDENTIFIERS.index(increment_identifier)

        version = version.version
        for idx, identifier_value in enumerate(version):
            # Increment the identifier
            if idx == inc_id:
                version[idx] += 1
            # Set everything after the identifier to zero
            if idx > inc_id:
                version[idx] = 0
        semver = '.'.join(str(x) for x in version)
        return LooseVersion(semver + '-' + classifier)


class ReleaseSetVersion(JmakeModule):
    """
    Mimics the release:perform step of the release Maven plugin in that it will tag the current code in the repository
    and push that code back to the main JIRA repository.
    """
    def __init__(self, git_utils=GitUtils()):
        JmakeModule.__init__(self)
        self.command = 'set-version'
        self.description = ''
        self.git_utils = git_utils

    def __call__(self, args, executor):
        version_in_pom = find_pom_version('pom.xml')
        new_version = LooseVersion(args.version_value)
        executor.append(lambda log: log.info("Updating the version to {}".format(new_version)))
        self.update_version_in_files(version_in_pom, new_version)

    def define_parser(self, parser):
        parser.add_argument('-v', '--value',
                            type=str,
                            required=True,
                            help='The version number that we are setting',
                            dest='version_value')
        MavenCallable.add_maven_switches(parser)

    def update_version_in_files(self, existing, replacement):
        """
        This replaces the existing version number in both the pom and dependencies.txt
        :return: None
        :rtype: None
        """
        poms = ['./jira-components/jira-core/dependencies.txt']
        for root, _, files in os.walk('.'):
            for file in files:
                if fnmatch.fnmatch(file, 'pom.xml'):
                    poms.append(os.path.join(root, file))
        existing_version = str(existing)
        replacement_version = str(replacement)
        with fileinput.input(poms, inplace=True) as file:
            for line in file:
                print(line.replace(existing_version, replacement_version), end='')


class ReleaseTag(JmakeModule):
    """
    Tags a release on a branch off master and then only pushes the tag
    """
    def __init__(self, git_utils=GitUtils()):
        JmakeModule.__init__(self)
        self.command = 'tag'
        self.description = ''
        self.git_utils = git_utils

    def __call__(self, args, executor):
        release_version = LooseVersion(args.version_value)

        executor.append(lambda log: log.info("Checking to see if there is an existing tag for {}".format(release_version)))
        self.fail_if_tag_exists(release_version)

        executor.append(lambda log: log.info("Updating origin remote url"))
        self.set_git_remote(ORIGIN_URL)

        executor.append(lambda log: log.info("Creating tags and branch of '{}' for release".format(release_version)))
        self.branch_and_tag(release_version)

        if args.push:
            executor.append(lambda log: log.info("Pushing changes to origin"))
            self.push_to_origin(release_version)
        else:
            executor.append(lambda log: log.info("There was no -p or --push argument so not pushing changes"))

    def define_parser(self, parser):
        parser.add_argument('-v', '--value',
                            type=str,
                            required=True,
                            help='The version number that we are tagging',
                            dest='version_value')
        parser.add_argument('-p', '--push',
                            type=str,
                            help='Push any change that we make to {}'.format(ORIGIN_URL),
                            dest='push')
        MavenCallable.add_maven_switches(parser)

    def fail_if_tag_exists(self, version):
        """
        Tries to find an existing tag for a version. If a tag does exist then an error is raised and the tagging
        and branching is aborted.
        """
        code = self.git_utils.execute_git('rev-parse {}'.format(version), exception_on_error=False).returncode
        if code == 0:
            raise Exception('Found an existing tag for {} so bailing out now and not trying to create another'.format(version))

    def set_git_remote(self, remote):
        """
        Replaces the existing origin url with the value of remote
        :param: Uri of the JIRA repository we're looking to promote to
        :type: String
        """
        self.git_utils.execute_git('remote set-url origin {}'.format(remote))

    def stash_any_uncommited_files(self):
        if len(self.git_utils.execute_git('status --short').output) > 0:
            self.git_utils.execute_git('stash')

    def branch_and_tag(self, version):
        """
        Assuming that the
        :param: The version that we are creating a tag for
        :type: LooseVersion
        """

        self.git_utils.execute_git('checkout -b "{}-branch"'.format(version))
        self.git_utils.execute_git('commit -a -m "Setting version to {} for release"'.format(version))
        self.git_utils.execute_git('tag -m "{}" {}'.format(version, version))
        # Swap back to the previous branch to make JMake happy
        self.git_utils.execute_git('checkout -')

    def push_to_origin(self, version):
        """
        :param: The version that we are push task for
        :type: LooseVersion
        """
        self.git_utils.execute_git('push --tags origin'.format(version))


def find_pom_version(pom_file):
    """
    Assuming we're running in the root JIRA directory get the pom version
    :return: The current pom version
    :rtype: LooseVersion
    """
    et = ElementTree.parse(pom_file)
    prop = et.find('./{http://maven.apache.org/POM/4.0.0}version')
    return LooseVersion(prop.text.strip())