import os
from callables.Callable import Callable
from utils.FileUtils import FileUtils

class SSOSeraphConfig:
    SSO_CONFIG = os.sep.join(['.', 'jmake_src', 'data', 'sso', 'seraph-config-sso.xml'])
    SSO_CONFIG_TARGET = os.sep.join(['.', 'jira-components', 'jira-webapp', 'target', 'jira', 'WEB-INF', 'classes', 'seraph-config.xml'])

    def __init__(self, file_utils: FileUtils=FileUtils()):
        self.fs = file_utils

    def copy_config(self):
        def copy_config_task(logger):
            logger.info('Copying ' + self.SSO_CONFIG + ' to ' + self.SSO_CONFIG_TARGET)
            self.fs.copy_file(self.SSO_CONFIG, self.SSO_CONFIG_TARGET)
            return Callable.success
        return copy_config_task

    def validate_properties_file(self, properties_file_location):
        def validate_properties_file_task(logger):
            if properties_file_location:
                if not self.fs.file_exists(properties_file_location):
                    logger.error('File: ' + properties_file_location + ' does not exist. ')
                    return Callable.failure
                expected_keys = [
                    'application.password',
                    'application.name',
                    'application.login.url',
                    'crowd.server.url',
                    'session.validationinterval',
                    'session.lastvalidation',
                ]
                propertiesFile = PropertiesFile(self.fs).load(properties_file_location)
                missing_keys = propertiesFile.missing_keys(expected_keys)
                if missing_keys:
                    logger.error('Properties file ' + properties_file_location + ' does not contain required keys: ' + ", ".join(str(key) for key in missing_keys))
                    return Callable.failure

            return Callable.success
        return validate_properties_file_task


class PropertiesFile:

    def __init__(self, file_utils: FileUtils=FileUtils()):
        self.fs = file_utils
        self.properties = {}

    def load(self, file_location):
        for file_line in self.fs.read_lines(file_location):
            line = file_line.strip()
            if line and not line.startswith('#'):
                splitted = line.split('=')
                key = splitted[0].strip()
                value = '='.join(splitted[1:])
                self.properties[key] = value
        return self

    def missing_keys(self, expected_keys):
        return set(expected_keys) - set(self.properties.keys())