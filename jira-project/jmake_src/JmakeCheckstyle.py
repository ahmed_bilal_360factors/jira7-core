
from maven.Maven import MavenCallable
from maven.Surefire import Surefire
from module import JmakeModule


class Checkstyle(JmakeModule):
    def __init__(self):
        super().__init__()
        self.command = 'checkstyle'
        self.description = 'Runs JIRA code style check.'
        self.check_branch = False

    def __call__(self, args, executor):

        maven = MavenCallable(args)
        maven.profile('codestyle').profile('distribution').profile('ondemand')
        maven.property('maven.test.ondemand.acceptance.skip', 'false')
        maven.phase('validate')

        executor.append(maven)

    def define_parser(self, parser):
        MavenCallable.add_maven_switches(parser)

