from argparse import ArgumentParser
import os
from unittest import TestCase
from CommandExecutor import Callable
from JmakeAutoComplete import AutoComplete, AutoCompleteInstaller
from utils.FileUtils import MockFileUtils, FileUtils
from utils.Mocks import Mock

class AutoCompleteTest(TestCase):
    def test_simple_parser(self):
        parser = ArgumentParser()
        self.do_test(parser, ['-'], ['-h', '--help'], ['clean'])

    def test_choices_mutually_exclusive(self):
        parser = ArgumentParser()
        parser.add_argument('myFirstChoice', choices=['a', 'b', 'c', 'd', 'e'])
        parser.add_argument('mySecondChoice', choices=['q', 'w', 'e', 'r', 't'])
        self.do_test(parser, [], ['a', 'b', 'c', 'd', 'e', 'q', 'w', 'r', 't'], [])
        self.do_test(parser, ['a'], ['q', 'w', 'e', 'r', 't'], ['a', 'b', 'c', 'd'])
        self.do_test(parser, ['a', 'r'], [], ['a', 'b', 'c', 'd', 'e', 'q', 'w', 'r', 't'])

    def test_partial_completion(self):
        parser = ArgumentParser()

        parser.add_argument('--abcd')
        parser.add_argument('--abef')
        parser.add_argument('--bder')
        parser.add_argument('--abbd')
        parser.add_argument('--acce')

        self.do_test(parser, ['--'], ['--abcd', '--abef', '--bder', '--abbd', '--acce'], [])
        self.do_test(parser, ['--a'], ['--abcd', '--abef', '--abbd', '--acce'], ['--bder'])
        self.do_test(parser, ['--ab'], ['--abcd', '--abef', '--abbd'], ['--bder', '--acce'])
        self.do_test(parser, ['--abcd'], ['--bder', '--acce', '--abef', '--abbd'], [])
        self.do_test(parser, ['--abcd', 'e'], [], ['--bder', '--acce', '--abef', '--abbd'])

    def test_sub_parsers(self):
        parser = ArgumentParser()

        parser.add_argument('--main_level')
        subparser = parser.add_subparsers().add_parser('sub')
        subparser.add_argument('--sub_level')
        subparser = subparser.add_subparsers().add_parser('sub2')
        subparser.add_argument('--sub_level2')

        self.do_test(parser, [], ['--main_level', 'sub'], ['sub2', '--sub_level', '--sub_level2'])
        self.do_test(parser, ['sub'], ['--sub_level', 'sub2'], ['--sub_level2', '--main_level'])
        self.do_test(parser, ['sub', 'sub2'], ['--sub_level2'], ['--sub_level', '--main_level'])

    def test_sub_parsers_are_mutually_exclusive(self):
        parser = ArgumentParser()

        parser.add_argument('-f', '--force')
        subparsers = parser.add_subparsers()
        subparsers.add_parser('save')
        subparsers.add_parser('delete')
        subparsers.add_parser('copy')
        subparsers.add_parser('move')
        subparsers.add_parser('touch')

        self.do_test(parser, [], ['save', 'delete', 'copy', 'move', 'touch', '-f', '--force'], [])
        self.do_test(parser, ['-f'], ['save', 'delete', 'copy', 'move', 'touch'], ['-f', '--force'])
        self.do_test(parser, ['save'], [], ['save', 'delete', 'copy', 'move', 'touch', '-f', '--force'])

    def test_clean_is_considered_as_an_option_always(self):
        parser = ArgumentParser()

        parser.add_argument('--main_level')
        subparser = parser.add_subparsers().add_parser('sub')
        subparser.add_subparsers().add_parser('sub2')

        self.do_test(parser, [], ['clean'], [])
        self.do_test(parser, ['sub'], ['clean'], [])
        self.do_test(parser, ['sub', 'sub2'], ['clean'], [])
        self.do_test(parser, ['sub', 'sub2', 'clean'], [], ['clean'])

    def do_test(self, parser, input, wanted, unwanted):
        module = AutoComplete(parser)
        args = Mock(input = ['jmake'])
        args.input.extend(input)
        #noinspection PyUnresolvedReferences
        res = [x for x in module._AutoComplete__determine_autocomplete(args, None)]
        for item in res:
            self.assertNotIn(item, unwanted)
        for item in wanted:
            self.assertIn(item, res)
        pass


class AutoCompleteIntallerTest(TestCase):

    def setUp(self):
        self.file_utils = FileUtils()

        self.mock_file_utils = MockFileUtils()
        self.mock_file_utils.expect_jmake_user_data_dir(toReturn=self.file_utils.jmake_user_data_dir())

    def bash_env(self):
        return {'SHELL' : '/bin/bash'}

    def zsh_env(self):
        return {'SHELL' : '/bin/zsh'}

    def bash_completion_function_file_exists(self, whether):
        self.mock_file_utils.expect_file_exists(os.sep.join([self.file_utils.jmake_user_data_dir(), 'completion', 'jmake.completion.bash']), toReturn=whether)

    def bash_completion_is_installed(self, whether):
        self.mock_file_utils.expect_dir_exists('/etc/bash_completion.d', toReturn=whether)

    def zsh_completion_init_file_exists(self, whether):
        self.mock_file_utils.expect_file_exists(os.sep.join([self.file_utils.jmake_user_data_dir(), 'completion', 'jmake.completion.zsh']), toReturn=whether)

    def zsh_completion_function_file_exists(self, whether):
        self.mock_file_utils.expect_file_exists(os.sep.join([self.file_utils.jmake_user_data_dir(), 'completion', '_jmake']), toReturn=whether)

    def test_installer_will_not_overwrite_files_bash(self):

        # given
        self.bash_completion_is_installed(True)
        self.bash_completion_function_file_exists(True)
        installer = AutoCompleteInstaller(self.bash_env(), self.mock_file_utils)

        # when
        result = installer(Mock())

        # then
        self.assertEqual(Callable.success, result)
        self.assertEqual(0, self.mock_file_utils.callcount_write_lines())

    def test_installer_will_not_overwrite_files_zsh(self):

        # given
        self.zsh_completion_function_file_exists(True)
        self.zsh_completion_init_file_exists(True)
        installer = AutoCompleteInstaller(self.zsh_env(), self.mock_file_utils)

        # when
        result = installer(Mock())

        # then
        self.assertEqual(Callable.success, result)
        self.assertEqual(0, self.mock_file_utils.callcount_write_lines())

    def test_installer_fails_when_bash_autocomplete_is_not_installed(self):

        # given
        self.bash_completion_is_installed(False)
        installer = AutoCompleteInstaller(self.bash_env(), self.mock_file_utils)

        # when
        result = installer(Mock())

        # then
        self.assertEqual(Callable.do_not_proceed, result)
        self.assertEqual(0, self.mock_file_utils.callcount_write_lines())

    def test_installer_installs_bash_completion_file(self):

        # given
        self.bash_completion_is_installed(True)
        self.bash_completion_function_file_exists(False)
        installer = AutoCompleteInstaller(self.bash_env(), self.mock_file_utils)

        # when
        result = installer(Mock())

        # then
        self.assertEqual(Callable.success, result)
        self.assertEqual(1, self.mock_file_utils.callcount_write_lines())
        self.assertTrue(any(installer.get_completion_function_file() in call for call in self.mock_file_utils.write_lines.made_calls))

    def test_installer_installs_zsh_autocomplete(self):

        # given
        self.zsh_completion_function_file_exists(False)
        self.zsh_completion_init_file_exists(False)
        installer = AutoCompleteInstaller(self.zsh_env(), self.mock_file_utils)

        # when
        result = installer(Mock())

        # then
        self.assertEqual(Callable.success, result)
        self.assertEqual(2, self.mock_file_utils.callcount_write_lines())
        self.assertTrue(any(installer.get_completion_init_file() in call for call in self.mock_file_utils.write_lines.made_calls))
        self.assertTrue(any(installer.get_completion_function_file() in call for call in self.mock_file_utils.write_lines.made_calls))

    def test_installer_installs_only_zsh_function(self):

        # given
        self.zsh_completion_function_file_exists(False)
        self.zsh_completion_init_file_exists(True)
        installer = AutoCompleteInstaller(self.zsh_env(), self.mock_file_utils)

        # when
        result = installer(Mock())

        # then
        self.assertEqual(Callable.success, result)
        self.assertEqual(1, self.mock_file_utils.callcount_write_lines())
        self.assertTrue(any(installer.get_completion_function_file() in call for call in self.mock_file_utils.write_lines.made_calls))

    def test_installer_installs_only_zsh_init_file(self):

        # given
        self.zsh_completion_function_file_exists(True)
        self.zsh_completion_init_file_exists(False)
        installer = AutoCompleteInstaller(self.zsh_env(), self.mock_file_utils)

        # when
        result = installer(Mock())

        # then
        self.assertEqual(Callable.success, result)
        self.assertEqual(1, self.mock_file_utils.callcount_write_lines())
        self.assertTrue(any(installer.get_completion_init_file() in call for call in self.mock_file_utils.write_lines.made_calls))

    def test_zsh_environment(self):

        # when
        installer = AutoCompleteInstaller(self.zsh_env(), self.mock_file_utils)

        # then
        self.assertIn('_jmake', installer.get_completion_function_file())
        self.assertIn('jmake.completion.zsh', installer.get_completion_init_file())

    def test_bash_environment(self):

        # when
        installer = AutoCompleteInstaller(self.bash_env(), self.mock_file_utils)

        # then
        self.assertIn('jmake.completion.bash', installer.get_completion_function_file())
        self.assertIsNone(installer.get_completion_init_file())

    def test_no_exceptions_when_no_shell_in_environment(self):

        # when
        installer = AutoCompleteInstaller({}, self.mock_file_utils)

        # then
        self.assertIsNone(installer.get_completion_function_file())
        self.assertIsNone(installer.get_completion_init_file())

