---------------------------------------------------------------------
JIRA 7.3.8-#73019 README
---------------------------------------------------------------------

Thank you for downloading JIRA 7.3.8 - source distribution.


Building JIRA
-------------

The documentation for building JIRA from source is online at:

  https://developer.atlassian.com/display/JIRADEV/Building+JIRA+from+source

Atlassian Developer Network
---------------------------

Need help or advice?  See the developer network, containing
documentation, tutorials, sample code and forums:

http://developer.atlassian.com


-----------------------------------------------------------
Happy issue tracking and thank you for using JIRA!
- The Atlassian Team
-----------------------------------------------------------
